package facebookcostloader.mundo;
import java.io.*;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class MainFacebookLoader
{
    
    private LocalDBConnectionServidor admin;;
    
    public MainFacebookLoader()
    {
        admin= new LocalDBConnectionServidor();
    }
    
    public void loadFBData()
    {
        // Directory path here
        //String path = "./data/";
    	String path = "/home/vftp/gabriel/Subcategory_Product_Clicks/";
    	//String path = "C:/Users/Charles/Desktop/Webtrekk/Subcategory Product Clicks/";
        System.out.println(path);
       
        
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles(); 
       
        for (int i = 0; i < listOfFiles.length; i++) 
        {
            if (listOfFiles[i].isFile()) 
            {
                loadFile(path+listOfFiles[i].getName());
                
                File file=new File(path+listOfFiles[i].getName());
                file.delete( );
                
            }
        }
    }
    

        public void loadFile(String pathFile)
        {
            String linea= "";
           try
           {
            File f = new File(pathFile);
            FileReader fr= new FileReader(f);
            BufferedReader lector = new BufferedReader(fr);
            
            linea= lector.readLine( );
            System.out.println("Load file: "+linea);
            /*for(int i = 0; i < 2; ++i)
            	lector.readLine();*/
            while(linea!=null)
            {
                //Cargar Linea
                if(!linea.contains( "total" ) && !linea.contains( "time" ))
                    {
                        try
                            {
                                procesarLinea(linea);    
                            }
                        catch(Exception e)
                            {
                                e.printStackTrace( );
                            }
                    
                    }
                linea= lector.readLine( );
            }
         
            lector.close( );
            fr.close( );
           }
           catch(Exception e)
           {
              System.out.println(linea);
               e.printStackTrace( );
              
           }
           

        }
     
    private void procesarLinea( String linea ) throws ParseException
        {
        
        try
        {
            String UTF8Str = new String(linea.getBytes(),"UTF-8");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
            String pages="";
            String events="";
            String clicks="";
            //int impressions=0;
            //int adClicks=0;
            //double conversion_rate=0.0;
            
            /*if(!linea.contains( "\"" ) && !(linea.charAt( 0 ) == 44))
            {*/
            	Calendar yesterday = Calendar.getInstance();
            	yesterday.add(Calendar.DATE, -1);
            	String newDate = sdf.format(yesterday.getTime());
                String[]datos= UTF8Str.split( "	" );
                System.out.println( UTF8Str);
                System.out.println("datos[3]=" +datos[1]);

                /*if(!datos[3].equals( "0" ))
                {
                    String datosf[]=datos[0].split( "/" );
                    campaign=datos[1];
                    value=Integer.parseInt( datos[3] );
                    adClicks=Integer.parseInt( datos[6] );
                    cost=Double.parseDouble(datos[12]);
                    if(datosf[0].length( )<2)
                        datosf[0]="0"+datosf[0];
                    if(datosf[1].length( )<2)
                        datosf[1]="0"+datosf[1];
                    admin.insert_GA_FB_Cost( datosf[2]+datosf[0]+datosf[1], "facebook", "socialmediaads", campaign, impresiones, adClicks, cost*1787.02 );
                }   */
                    //String datosf[]=datos[0].split( "/" );
                	//newDate=datos[0];
                	pages=datos[0];
                	events=datos[1];
                    clicks= datos[2];
                    NumberFormat format = NumberFormat.getInstance(Locale.getDefault());
                    Number clicks2 = format.parse(clicks);
                    int clicks3 = clicks2.intValue();
                    admin.insert_GA_FB_Cost(newDate , pages , events , clicks3);
                
            //}
        
        }
        catch( UnsupportedEncodingException e )
        {
           e.printStackTrace();
        }

       
        
        
        
            
        
        }


    /**
     * @param args
     */
    public static void main( String[] args )
    {
       MainFacebookLoader mfl= new MainFacebookLoader();
       mfl.loadFBData();
        
    }
    


}
