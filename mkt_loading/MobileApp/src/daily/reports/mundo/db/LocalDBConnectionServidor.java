package daily.reports.mundo.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Date;

public class LocalDBConnectionServidor {
	// -------------------------------------------------------------------------
	// Atributos
	// -------------------------------------------------------------------------

	/**
	 * Conecci�n con la base de datos local
	 */
	private Connection con;

	// -------------------------------------------------------------------------
	// Constructor
	// -------------------------------------------------------------------------
	/**
	 * Constructor de la clase
	 */
	public LocalDBConnectionServidor() {

		try {

			String dbUrl = "jdbc:mysql://172.18.1.4:3306/production";
			String dbClass = "com.mysql.jdbc.Driver";

			Class.forName(dbClass);
			System.out.println("-------Conectando DB Local-------");

			con = DriverManager.getConnection(dbUrl, "charles", "yqKdADKRfqt");
			System.out.println("-------Conectado DB Local-------");

		}

		catch (Exception e) {
			e.printStackTrace();
		}
	}

	// -------------------------------------------------------------------------
	// Google Analytics
	// -------------------------------------------------------------------------
	

 /**
  * Fecha Ultima actualizacion
 */

public Date get_Last_Date_transaction_id() 
{

   Date fecha=new Date();
 String query = "select date from mobileapp_transaction_id order by date desc limit 1";

 try {
     java.sql.PreparedStatement ps = con.prepareStatement(query);

     ResultSet rs= ps.executeQuery( );
     while(rs.next( ))
     {
         fecha=rs.getDate( 1 );
     }

 } catch (Exception e) {
     e.printStackTrace();
 }
 return fecha;
}

public Date get_Last_Date_campaign_ad_group() 
{

   Date fecha=new Date();
 String query = "select date from mobileapp_campaign order by date desc limit 1";

 try {
     java.sql.PreparedStatement ps = con.prepareStatement(query);

     ResultSet rs= ps.executeQuery( );
     while(rs.next( ))
     {
         fecha=rs.getDate( 1 );
     }

 } catch (Exception e) {
     e.printStackTrace();
 }
 return fecha;
}

  
    public void insert_transaction_id_mx(String date, String transaction_id, String campaign, String source, String medium, String ad_group, double total_value)
    {
        String query = "insert into mobileapp_transaction_id(date,transaction_id,campaign,source,medium,ad_group,total_value) values (?,?,?,?,?,?,?)";
        
        try {
            java.sql.PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, date.substring(0, 4) + "-" + date.substring(4, 6)+ "-" + date.substring(6));
            ps.setString(2, transaction_id);
            ps.setString(3,campaign );
            ps.setString(4,source );
            ps.setString(5,medium );
            ps.setString(6,ad_group );
            ps.setDouble(7,total_value );
            ps.executeUpdate();
        }
    
        catch (Exception e) {
            e.printStackTrace();
        }
        
    }  
    
   public void insert_campaign_ad_group_mx(String date, String campaign, String source, String medium, String ad_group, int impressions, int clicks, int visits, double ad_cost, int bounce, int cart)
    {
        String query = "insert into mobileapp_campaign(date,campaign,source,medium,ad_group,impressions,clicks,visits,ad_cost,bounce,cart) values (?,?,?,?,?,?,?,?,?,?,?)";
        
        try {
            java.sql.PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, date.substring(0, 4) + "-" + date.substring(4, 6)+ "-" + date.substring(6));
            ps.setString(2, campaign);
            ps.setString(3, source);
            ps.setString(4, medium);
            ps.setString( 5,ad_group );
            ps.setInt( 6,impressions );
            ps.setInt(7, clicks);
            ps.setInt(8, visits);
            ps.setDouble(9, ad_cost);
            ps.setInt( 10,bounce );
            ps.setInt( 11,cart );
            
            ps.executeUpdate();
        }
    
        catch (Exception e) {
            e.printStackTrace();
        }
        
    }    

}
