package daily.reports.mundo.google.analytics;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.google.gdata.client.analytics.AnalyticsService;
import com.google.gdata.client.analytics.DataQuery;
import com.google.gdata.data.analytics.DataEntry;
import com.google.gdata.data.analytics.DataFeed;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

import daily.reports.mundo.db.LocalDBConnectionServidor;

//-------------------------------------------------------------------------
// Atributos
//-------------------------------------------------------------------------

/**
 * Clase que se conecta con GA para bajar la información
 */
public class DBGoogleAnalytics {

	//-------------------------------------------------------------------------
	// Atributos
	//-------------------------------------------------------------------------

	
  // Credentials for Client Login Authorization.
  private String user;
  private String password;

  // Table ID constant
  private String tableID;
  private  LocalDBConnectionServidor admin;
  private boolean db;

  
  
	//-------------------------------------------------------------------------
	// Constructor
	//-------------------------------------------------------------------------

  public DBGoogleAnalytics(String user, String password, String tableID) 
  {
      db=true;
	  this.user=user;
	  this.password=password;
	  this.tableID="ga:"+tableID;

  }

	//-------------------------------------------------------------------------
	// Métodos
	//-------------------------------------------------------------------------

  /**
   * Método para cargar toda la información de GA
   */
  public void cargarTablas() 
  {
    try {
    	
    admin= new LocalDBConnectionServidor();
   // admin.restartTables();
      // Service Object to work with the Google Analytics Data Export API.
      AnalyticsService analyticsService = new AnalyticsService("gaExportAPI_acctSample_v2.0");
      
      // Client Login Authorization.
      analyticsService.setUserCredentials(user, password);

      // Get data from the Account Feed.
      //getAccountFeed(analyticsService);

      // Access the Data Feed if the Table Id has been set.
      if (!tableID.isEmpty()) 
      {
    	 
          // Get GA data
          //General
          
          try
           {
              //SEM Cost,Impressions
        	  gettransaction_id( analyticsService );
           }
          catch(Exception e)
          {
              
          }
          
          try
          {
             //SEM Cost,Impressions
        	  getcampaign_ad_group( analyticsService );
          }
         catch(Exception e)
         {
             
         }

         //Random
         //getConvPerHour(analyticsService);
      }
    } 
    catch (AuthenticationException e) {
       
        e.printStackTrace( );
      return;
    } catch (Exception e) {
       
        e.printStackTrace( );
      return;
    } 
    
   
  }

public void gettransaction_id( AnalyticsService analyticsService )
          throws IOException, MalformedURLException, ServiceException 
          {

   
        // Create a query using the DataQuery Object.
        DataQuery query = new DataQuery(new URL("https://www.google.com/analytics/feeds/data"));
        RangoFecha rf= cargarLastUpdate( admin.get_Last_Date_transaction_id( ) );
        query.setStartDate("2013-10-14");
        query.setEndDate("2013-10-14");
        
        query.setDimensions("ga:date,ga:transactionId,ga:campaign,ga:source,ga:medium,ga:adGroup");
        query.setMetrics("ga:totalValue");
        query.setMaxResults(20000);
        query.setIds(tableID);
        
        
        // Make a request to the API.
        DataFeed dataFeed = analyticsService.getFeed(query.getUrl(), DataFeed.class);
        
        
        
        for (DataEntry entry : dataFeed.getEntries()) 
        {
        
            if(db)
            admin.insert_transaction_id_mx(
            		entry.stringValueOf("ga:date"),
                    entry.stringValueOf("ga:transactionId"),  
                    entry.stringValueOf("ga:campaign"),
                    entry.stringValueOf("ga:source"), 
                    entry.stringValueOf("ga:medium"),
                    entry.stringValueOf("ga:adGroup"), 
                    Double.parseDouble(entry.stringValueOf("ga:totalValue")));
        }
        

}

public void getcampaign_ad_group( AnalyticsService analyticsService )
        throws IOException, MalformedURLException, ServiceException 
        {

 
      // Create a query using the DataQuery Object.
      DataQuery query = new DataQuery(new URL("https://www.google.com/analytics/feeds/data"));
      RangoFecha rf= cargarLastUpdate( admin.get_Last_Date_campaign_ad_group( ) );
      query.setStartDate(rf.getFrom());
      query.setEndDate(rf.getFrom());
      
      query.setDimensions("ga:date,ga:campaign,ga:source,ga:medium,ga:adGroup");
      query.setMetrics("ga:impressions,ga:adClicks,ga:visits,ga:adCost,ga:bounces,ga:goal1Starts");
      query.setMaxResults(20000);
      query.setIds(tableID);
      
      
      // Make a request to the API.
      DataFeed dataFeed = analyticsService.getFeed(query.getUrl(), DataFeed.class);
      
      
     
      
      for (DataEntry entry : dataFeed.getEntries()) 
      {
      
          if(db)
          admin.insert_campaign_ad_group_mx(
          		entry.stringValueOf("ga:date"),
                  entry.stringValueOf("ga:campaign"), 
                  entry.stringValueOf("ga:source"), 
                  entry.stringValueOf("ga:medium"), 
                  entry.stringValueOf("ga:adGroup"), 
                  Integer.parseInt(entry.stringValueOf("ga:impressions")), 
                  Integer.parseInt(entry.stringValueOf("ga:adClicks")), 
                  Integer.parseInt(entry.stringValueOf("ga:visits")), 
                  Double.parseDouble(entry.stringValueOf("ga:adCost")),
                  Integer.parseInt(entry.stringValueOf("ga:bounces")), 
                  Integer.parseInt(entry.stringValueOf("ga:goal1Starts")));
      }
      

  
}

/**
 * Método para cargar un archivo de propiedades
 */
private RangoFecha cargarLastUpdate(Date fecha) 
{
    RangoFecha rf= new RangoFecha( );
    try 
    {
        
        Calendar cr = new GregorianCalendar();
        
        cr.setTime( fecha );
        cr.add(Calendar.DAY_OF_MONTH,1);
        
        String anho=""+cr.get(Calendar.YEAR);
        String mes=""+(cr.get(Calendar.MONTH)+1);
        String dia=""+cr.get(Calendar.DAY_OF_MONTH);
        if(mes.length()<2)
            mes="0"+mes;
        if(dia.length()<2)
            dia="0"+dia;
        
        
        rf.setFrom(anho+"-"+mes+"-"+dia);
        
  
        
        cr.setTime(new Date());
        cr.add(Calendar.DAY_OF_MONTH,-1);
        
        anho=""+cr.get(Calendar.YEAR);
        mes=""+(cr.get(Calendar.MONTH)+1);
        dia=""+cr.get(Calendar.DAY_OF_MONTH);
        if(mes.length()<2)
            mes="0"+mes;
        if(dia.length()<2)
            dia="0"+dia;
        rf.setTo(anho+"-"+mes+"-"+dia);
       
        
        

    }
    catch(Exception e)
    {
      
       e.printStackTrace();
        
    }
    return rf;
}


	
	


  
}