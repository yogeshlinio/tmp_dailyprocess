package facebookcostloader.mundo;
import java.io.*;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class MainFacebookLoader
{
    
    private LocalDBConnectionServidor admin;;
    
    public MainFacebookLoader()
    {
        admin= new LocalDBConnectionServidor();
    }
    
    public void loadFBData()
    {
        // Directory path here
        //String path = "./data/";
    	String path = "/home/vftp/gabriel/Subcategory_Visits/";
    	//String path = "C:/Users/Charles/Desktop/Subcategory_Visits/";
        System.out.println(path);
       
        
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles(); 
       
        for (int i = 0; i < listOfFiles.length; i++) 
        {
            if (listOfFiles[i].isFile()) 
            {
                loadFile(path+listOfFiles[i].getName());
                
                File file=new File(path+listOfFiles[i].getName());
                //file.delete( );
                
            }
        }
    }
    

        public void loadFile(String pathFile)
        {
            String linea= "";
           try
           {
            File f = new File(pathFile);
            FileReader fr= new FileReader(f);
            BufferedReader lector = new BufferedReader(fr);
            
            linea= lector.readLine( );
            System.out.println("Load file: "+linea);
            /*for(int i = 0; i < 2; ++i)
            	lector.readLine();*/
            while(linea!=null)
            {
                //Cargar Linea
                if(!linea.contains( "total" ) && !linea.contains( "time" ))
                    {
                        try
                            {
                                procesarLinea(linea);    
                            }
                        catch(Exception e)
                            {
                                e.printStackTrace( );
                            }
                    
                    }
                linea= lector.readLine( );
            }
         
            lector.close( );
            fr.close( );
           }
           catch(Exception e)
           {
              System.out.println(linea);
               e.printStackTrace( );
              
           }
           

        }
     
    private void procesarLinea( String linea ) throws ParseException
        {
        
        try
        {
            String UTF8Str = new String(linea.getBytes(),"UTF-8");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
            String pages="";
            String visits="";
            //int impresiones=0;
            //int adClicks=0;
            //double conversion_rate=0.0;
            
            /*if(!linea.contains( "\"" ) && !(linea.charAt( 0 ) == 44))
            {*/
            	Calendar yesterday = Calendar.getInstance();
            	yesterday.add(Calendar.DATE, -1);
            	String newDate = sdf.format(yesterday.getTime());
                String[]datos= UTF8Str.split( "	" );
                System.out.println( UTF8Str);
                System.out.println("datos[3]=" +datos[1]);

                String datosf[]=datos[1].split( "/" );
                if(datosf[0].length( )<2)
                       datosf[0]="0"+datosf[0];
                if(datosf[1].length( )<2)
                        datosf[1]="0"+datosf[1];
                	
                pages=datos[0];
                visits=datos[2];
                NumberFormat format = NumberFormat.getInstance(Locale.getDefault());
                Number visits2 = format.parse(visits);
                int visits3 = visits2.intValue();
                //admin.insert_GA_FB_Cost(newDate , pages , visits3);
                admin.insert_GA_FB_Cost(datosf[2]+datosf[1]+datosf[0] , pages , visits3);
                
            //}
        
        }
        catch( UnsupportedEncodingException e )
        {
           e.printStackTrace();
        }

       
        
        
        
            
        
        }


    /**
     * @param args
     */
    public static void main( String[] args )
    {
       MainFacebookLoader mfl= new MainFacebookLoader();
       mfl.loadFBData();
        
    }
    


}
