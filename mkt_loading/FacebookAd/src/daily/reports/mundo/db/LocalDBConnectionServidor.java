package daily.reports.mundo.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.Date;

public class LocalDBConnectionServidor {
	// -------------------------------------------------------------------------
	// Atributos
	// -------------------------------------------------------------------------

	/**
	 * Conecci�n con la base de datos local
	 */
	private Connection con;

	// -------------------------------------------------------------------------
	// Constructor
	// -------------------------------------------------------------------------
	/**
	 * Constructor de la clase
	 */
	public LocalDBConnectionServidor() {

		try {

			String dbUrl = "jdbc:mysql://192.168.1.4:3306/dev_marketing";
			String dbClass = "com.mysql.jdbc.Driver";

			Class.forName(dbClass);
			System.out.println("-------Conectando DB Local-------");

			con = DriverManager.getConnection(dbUrl, "charles", "yqKdADKRfqt");
			System.out.println("-------Conectado DB Local-------");

		}

		catch (Exception e) {
			e.printStackTrace();
		}
	}

	// -------------------------------------------------------------------------
	// Google Analytics
	// -------------------------------------------------------------------------
	

 /**
  * Fecha Ultima actualizacion
 */
public Date get_Last_Date_facebookads() 
{

   Date fecha=new Date();
 String query = "select date from ga_facebook_ads_mx order by date desc limit 1";

 try {
     java.sql.PreparedStatement ps = con.prepareStatement(query);

     ResultSet rs= ps.executeQuery( );
     while(rs.next( ))
     {
         fecha=rs.getDate( 1 );
     }

 } catch (Exception e) {
     e.printStackTrace();
 }
 return fecha;
}

public Date get_Last_Date_facebook_transaction_id() 
{

   Date fecha=new Date();
 String query = "select date from ga_facebook_transaction_id_mx order by date desc limit 1";

 try {
     java.sql.PreparedStatement ps = con.prepareStatement(query);

     ResultSet rs= ps.executeQuery( );
     while(rs.next( ))
     {
         fecha=rs.getDate( 1 );
     }

 } catch (Exception e) {
     e.printStackTrace();
 }
 return fecha;
}

  
    public void insert_facebookads(String hour, String date, String campaign, String country, String region, String city, int transactions, int visits, double transactionRevenue, int goal2Starts )
    {
        String query = "insert into ga_facebook_ads_mx(hour,date,campaign,country,region,city,transactions,visits, transaction_revenue, goal2starts) values (?,?,?,?,?,?,?,?,?,?)";
        
        try {
            java.sql.PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, hour);
            ps.setString(2, date.substring(0, 4) + "-" + date.substring(4, 6)+ "-" + date.substring(6));
            ps.setString(3, campaign);
            ps.setString(4, country);
            ps.setString(5, region);
            ps.setString( 6,city );
            ps.setInt( 7,transactions );
            ps.setInt( 8,visits);
            ps.setDouble( 9,transactionRevenue);
            ps.setInt( 10,goal2Starts);
            
            ps.executeUpdate();
        }
    
        catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    public void insert_facebook_transaction_id(String hour, String date, String campaign, String country, String region, String city, String transaction_id, double transactionRevenue)
    {
        String query = "insert into ga_facebook_transaction_id_mx(hour,date,campaign,country,region,city,transaction_id, transaction_revenue) values (?,?,?,?,?,?,?,?)";
        
        try {
            java.sql.PreparedStatement ps = con.prepareStatement(query);
            ps.setString(1, hour);
            ps.setString(2, date.substring(0, 4) + "-" + date.substring(4, 6)+ "-" + date.substring(6));
            ps.setString(3, campaign);
            ps.setString(4, country);
            ps.setString(5, region);
            ps.setString( 6,city );
            ps.setString( 7,transaction_id );
            ps.setDouble( 8,transactionRevenue);
            
            ps.executeUpdate();
        }
    
        catch (Exception e) {
            e.printStackTrace();
        }
        
    }
  

}
