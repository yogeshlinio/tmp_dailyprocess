package daily.reports.mundo.google.analytics;

import java.io.*;
import java.util.Properties;

public class InputLoader
{
	//-------------------------------------------------------------------------
	// Atributos
	//-------------------------------------------------------------------------
	public InputLoader()
	{
		
	}
	
	//-------------------------------------------------------------------------
	// M�todos
	//-------------------------------------------------------------------------

	
	
	/**
	 * M�todo para cargar un archivo de propiedades
	 */
	public void cargarUserInfoGA() 
	{
	    try 
	    {
	    	File archivo= new File("./data/input.txt");
	    	Properties datos = new Properties( );
	        FileInputStream input = new FileInputStream( archivo );
	        datos.load( input );

	        //Obtiene la informaci�n del usuario
	       String user=datos.getProperty( "CLIENT.USERNAME" );
	       String password=datos.getProperty( "CLIENT.PASS" );
	       String tableID=datos.getProperty( "TABLE_ID");
	       
	       daily.reports.mundo.google.analytics.DBGoogleAnalytics dbGA= new daily.reports.mundo.google.analytics.DBGoogleAnalytics(user,password,tableID);
	       dbGA.cargarTablas();
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
		
	}

		/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		InputLoader il= new InputLoader();
		il.cargarUserInfoGA();
	}

}
