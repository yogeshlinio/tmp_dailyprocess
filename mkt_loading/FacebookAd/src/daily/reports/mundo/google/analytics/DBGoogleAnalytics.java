package daily.reports.mundo.google.analytics;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.google.gdata.client.analytics.AnalyticsService;
import com.google.gdata.client.analytics.DataQuery;
import com.google.gdata.data.analytics.DataEntry;
import com.google.gdata.data.analytics.DataFeed;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;

import daily.reports.mundo.db.LocalDBConnectionServidor;

//-------------------------------------------------------------------------
// Atributos
//-------------------------------------------------------------------------

/**
 * Clase que se conecta con GA para bajar la información
 */
public class DBGoogleAnalytics {

	//-------------------------------------------------------------------------
	// Atributos
	//-------------------------------------------------------------------------

	
  // Credentials for Client Login Authorization.
  private String user;
  private String password;

  // Table ID constant
  private String tableID;
  private  LocalDBConnectionServidor admin;
  private boolean db;
  private Logger log;
  
  
	//-------------------------------------------------------------------------
	// Constructor
	//-------------------------------------------------------------------------

  public DBGoogleAnalytics(String user, String password, String tableID) 
  {
      db=true;
	  this.user=user;
	  this.password=password;
	  this.tableID="ga:"+tableID;
	  log= new Logger( );
  }

	//-------------------------------------------------------------------------
	// Métodos
	//-------------------------------------------------------------------------

  /**
   * Método para cargar toda la información de GA
   */
  public void cargarTablas() 
  {
    try {
    	
    admin= new LocalDBConnectionServidor();
   // admin.restartTables();
      // Service Object to work with the Google Analytics Data Export API.
      AnalyticsService analyticsService = new AnalyticsService("gaExportAPI_acctSample_v2.0");
      
      // Client Login Authorization.
      analyticsService.setUserCredentials(user, password);

      // Get data from the Account Feed.
      //getAccountFeed(analyticsService);

      // Access the Data Feed if the Table Id has been set.
      if (!tableID.isEmpty()) 
      {
    	 
          // Get GA data
          //General
          
          try
           {
              //SEM Cost,Impressions
        	  getFacebook_Campaign( analyticsService );
           }
          catch(Exception e)
          {
              log.printLog(e.getMessage( ));
          }
          
          try
          {
             //SEM Cost,Impressions
             getFacebook_transaction_id( analyticsService );
          }
         catch(Exception e)
         {
             log.printLog(e.getMessage( ));
         }

         //Random
         //getConvPerHour(analyticsService);
      }
    } 
    catch (AuthenticationException e) {
        log.printLog( "Authentication failed : " + e.getMessage());
        e.printStackTrace( );
      return;
    } catch (Exception e) {
        log.printLog("Network error trying to retrieve feed: " + e.getMessage());
        e.printStackTrace( );
      return;
    } 
    
    log.closeLog( );
  }

public void getFacebook_Campaign( AnalyticsService analyticsService )
          throws IOException, MalformedURLException, ServiceException 
          {

    log.printLog("get_Last_Date_facebookads");
        // Create a query using the DataQuery Object.
        DataQuery query = new DataQuery(new URL("https://www.google.com/analytics/feeds/data"));
        RangoFecha rf= cargarLastUpdate( admin.get_Last_Date_facebookads( ) );
        query.setStartDate(rf.getFrom());
        query.setEndDate(rf.getFrom());
        
        query.setDimensions("ga:hour,ga:date,ga:campaign,ga:country,ga:region,ga:city");
        query.setMetrics("ga:transactions,ga:visits,ga:transactionRevenue,ga:goal2Starts");
        query.setSegment("gaid::1310677622");
        query.setMaxResults(20000);
        query.setIds(tableID);
        
        
        // Make a request to the API.
        DataFeed dataFeed = analyticsService.getFeed(query.getUrl(), DataFeed.class);
        
        File f = new File("./data/ga_sem_cost.txt");
        PrintWriter pw= new PrintWriter(f);
        // Output data to the screen.
        log.printLog("get_Last_Date_facebookads:\nRows: "+dataFeed.getEntries().size());
        
        for (DataEntry entry : dataFeed.getEntries()) 
        {
        
            if(db)
            admin.insert_facebookads(
            		entry.stringValueOf("ga:hour"),
                    entry.stringValueOf("ga:date"), 
                    entry.stringValueOf("ga:campaign"), 
                    entry.stringValueOf("ga:country"), 
                    entry.stringValueOf("ga:region"), 
                    entry.stringValueOf("ga:city"), 
                    Integer.parseInt(entry.stringValueOf("ga:transactions")),
                    Integer.parseInt(entry.stringValueOf("ga:visits")),
                    Double.parseDouble(entry.stringValueOf("ga:transactionRevenue")),
                    Integer.parseInt(entry.stringValueOf("ga:goal2Starts")));
        }
        
        pw.close( );
    
}


public void getFacebook_transaction_id( AnalyticsService analyticsService )
        throws IOException, MalformedURLException, ServiceException 
        {

  log.printLog("get_Last_Date_facebook_transaction_id");
      // Create a query using the DataQuery Object.
      DataQuery query = new DataQuery(new URL("https://www.google.com/analytics/feeds/data"));
      RangoFecha rf= cargarLastUpdate( admin.get_Last_Date_facebook_transaction_id( ) );
      query.setStartDate(rf.getFrom());
      query.setEndDate(rf.getFrom());
      
      query.setDimensions("ga:hour,ga:date,ga:campaign,ga:country,ga:region,ga:city,ga:transactionId");
      query.setMetrics("ga:transactionRevenue");
      query.setSegment("gaid::1310677622");
      query.setMaxResults(20000);
      query.setIds(tableID);
      
      
      // Make a request to the API.
      DataFeed dataFeed = analyticsService.getFeed(query.getUrl(), DataFeed.class);
      
      File f = new File("./data/ga_sem_cost.txt");
      PrintWriter pw= new PrintWriter(f);
      // Output data to the screen.
      log.printLog("get_Last_Date_facebook_transaction_id:\nRows: "+dataFeed.getEntries().size());
      
      for (DataEntry entry : dataFeed.getEntries()) 
      {
      
          if(db)
          admin.insert_facebook_transaction_id(
        		  entry.stringValueOf("ga:hour"),
                  entry.stringValueOf("ga:date"), 
                  entry.stringValueOf("ga:campaign"), 
                  entry.stringValueOf("ga:country"), 
                  entry.stringValueOf("ga:region"), 
                  entry.stringValueOf("ga:city"), 
                  entry.stringValueOf("ga:transactionId"), 
                  Double.parseDouble(entry.stringValueOf("ga:transactionRevenue")));
                  
      }
      
      pw.close( );
  
}

/**
 * Método para cargar un archivo de propiedades
 */
private RangoFecha cargarLastUpdate(Date fecha) 
{
    RangoFecha rf= new RangoFecha( );
    try 
    {
        
        Calendar cr = new GregorianCalendar();
        
        cr.setTime( fecha );
        cr.add(Calendar.DAY_OF_MONTH,1);
        
        String anho=""+cr.get(Calendar.YEAR);
        String mes=""+(cr.get(Calendar.MONTH)+1);
        String dia=""+cr.get(Calendar.DAY_OF_MONTH);
        if(mes.length()<2)
            mes="0"+mes;
        if(dia.length()<2)
            dia="0"+dia;
        
        
        rf.setFrom(anho+"-"+mes+"-"+dia);
        
        log.printLog(rf.getFrom( ));
        
        cr.setTime(new Date());
        cr.add(Calendar.DAY_OF_MONTH,-1);
        
        anho=""+cr.get(Calendar.YEAR);
        mes=""+(cr.get(Calendar.MONTH)+1);
        dia=""+cr.get(Calendar.DAY_OF_MONTH);
        if(mes.length()<2)
            mes="0"+mes;
        if(dia.length()<2)
            dia="0"+dia;
        rf.setTo(anho+"-"+mes+"-"+dia);
        log.printLog("FROM: "+ rf.getFrom( )+" TO: "+ rf.getFrom( ));
        
        

    }
    catch(Exception e)
    {
      
       log.printLog(e.getMessage( ));
       e.printStackTrace();
        
    }
    return rf;
}


	
	


  
}