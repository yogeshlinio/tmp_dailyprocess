package daily.reports.mundo.google.analytics;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Date;

public class Logger
{
    private PrintWriter pw;
    
    public Logger()
    {
        try
        {
            pw= new PrintWriter(new File("./data/log.txt"));
        }
        catch( FileNotFoundException e )
        {
          e.printStackTrace();
        }
    }
    public void printLog(String mensaje)
    {
        pw.println( "-"+new Date()+" -<"+mensaje+" >");
        System.out.println( "-"+new Date()+" -<"+mensaje+" >");
    }
    public void closeLog( )
    {
        pw.close( );
    }
    
  }
