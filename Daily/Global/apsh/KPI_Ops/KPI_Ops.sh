v_environment=$1
v_country=$2
v_verbose=$3
v_times=1
v_date=`date +"%Y%m%d"`
path=/var/lib/jenkins/git_repository

case $v_country in
      "Mexico") v_countryPrefix="mx";;
	  "Colombia") v_countryPrefix="co";;
  	  "Colombia_Project") v_countryPrefix="co";;
	  "Peru") v_countryPrefix="pe";;	  
	  "Venezuela") v_countryPrefix="ve";;
	  
	  *) echo "Wrong data source";;
esac

case $v_country in
      "Mexico") v_countryPRE="MX";;
	  "Colombia") v_countryPRE="CO";;
  	  "Colombia_Project") v_countryPRE="CO";;
	  "Peru") v_countryPRE="PE";;
	  "Venezuela") v_countryPRE="VE";;
	  
	  *) echo "Wrong data source";;
esac

case $v_country in
      "Mexico") v_wmsSufix="";;
	  "Colombia") v_wmsSufix="_co";;
  	  "Colombia_Project") v_wmsSufix="_co";;
	  "Peru") v_wmsSufix="_pe";;	  
	  "Venezuela") v_wmsSufix="_ve";;
	  
	  *) echo "Wrong data source";;
esac

operations=operations_${v_countryPrefix}
development=development_${v_countryPrefix}

#Waiting for OOT and A_Masters
for i in  Mexico Peru Venezuela Colombia_Project
do
   v_country=$i
   v_event=out_order_tracking
   v_reply=2000  #Wait Eight hours...
   v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
   #. $path/scripts/ToolKit/monitoring.sh
done

for i in  Mexico Peru Venezuela Colombia_Project
do
   v_country=$i
   v_event=A_Master
   v_step=finish
   v_reply=2000  #Wait Eight hours...
   v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
   #. $path/scripts/ToolKit/monitoring.sh
done
echo ""
v_country=$2


pwd

#Get all global configurations
#parse config.ini
. $lib/ToolKit/runEnviroment.sh