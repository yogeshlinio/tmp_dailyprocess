v_environment=$1
v_country=$1
#Get all global configurations

v_db="development_mx"
v_activity="Item_id"
v_date=`date +"%Y%m%d"`


for v_folder in $v_activity
do
    echo "Loading $v_folder ..."

    sqls="./sqls/ODS/${v_folder}"
    for i in $sqls/*.sql
    do
       dateTime=`date`
	   echo $dateTime $i 	   
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b ${v_db} < $i
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		
	   
    done
    dateTime=`date`
    echo " ${dateTime} End Load ..."
done