v_environment=$1
v_country=$2
v_verbose=$3
case $v_country in
      "Mexico") v_countryPrefix="mx";;
	  "Colombia") v_countryPrefix="co";;
  	  "Colombia_Project") v_countryPrefix="co";;
	  "Peru") v_countryPrefix="pe";;	  
	  "Venezuela") v_countryPrefix="ve";;
	  
	  *) echo "Wrong data source";;
esac


case $v_country in
      "Mexico") v_wmsSufix="";;
	  "Colombia") v_wmsSufix="_co";;
  	  "Colombia_Project") v_wmsSufix="_co";;
	  "Peru") v_wmsSufix="_pe";;	  
	  "Venezuela") v_wmsSufix="_ve";;
	  
	  *) echo "Wrong data source";;
esac

case $v_country in
      "Mexico") v_ucountryPrefix="MX";;
	  "Colombia") v_ucountryPrefix="CO";;
  	  "Colombia_Project") v_ucountryPrefix="CO";;
	  "Peru") v_ucountryPrefix="PE";;	  
	  "Venezuela") v_ucountryPrefix="VE";;
	  
	  *) echo "Wrong data source";;
esac

case $v_country in
      "Mexico") v_production="";;
	  "Colombia") v_production="_co";;
  	  "Colombia_Project") v_production="_co";;
	  "Peru") v_production="_pe";;	  
	  "Venezuela") v_production="_ve";;
	  
	  *) echo "Wrong data source";;
esac

pwd

#Get all global configurations
#parse config.ini
. $lib/ToolKit/runEnviroment.sh