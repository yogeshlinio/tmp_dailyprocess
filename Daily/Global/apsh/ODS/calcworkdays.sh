v_environment=$1
v_country=$2
v_verbose=$3
case $v_country in
      "Mexico") v_countryPrefix="mx";;
	  "Colombia") v_countryPrefix="co";;
  	  "Colombia_Project") v_countryPrefix="co";;
	  "Peru") v_countryPrefix="pe";;	  
	  "Venezuela") v_countryPrefix="ve";;
	  
	  *) echo "Wrong data source";;
esac

pwd

#Get all global configurations
#parse config.ini
. $lib/ToolKit/runEnviroment.sh