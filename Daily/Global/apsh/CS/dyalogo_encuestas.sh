#!/bin/bash

#Module to load data from BOB/UMS/OMS into dwh
#usage:
#     load.sh <country> <data_source>
v_system=`pwd`
v_date=`date +"%Y-%m-%d" --date='-1 day'`
v_user=${1}
v_pwd=${2}
v__host=${3}
#echo  "Starting processing ${1}"
v_country=${4}

#absolute path to the linio-git repository
path="../.."
#parse config.ini
. $path/scripts/parser_config.sh
read_ini $path/configChannels.ini

for environment in `cat ./resources/dyalogo_encuestas/dyalogo_encuestas.txt`
do
	echo "--------------------------------------------------------"
		#get the var names according the parameter
		channel_dir="INI__${environment}__DIRECTORY"
		channel_file="INI__${environment}__LOADFILE"
		DB_DATABASE="INI__${environment}__DB_DATABASE"
		DB_TABLE="INI__${environment}__DB_TABLE"
		

		#shell double referencing
		channel_dir=`eval echo \\$\$channel_dir `
		channel_file=`eval echo \\$\$channel_file`
		DB_TABLE=`eval echo \\$\$DB_TABLE`
		DB_DB=`eval echo \\$\$DB_DATABASE`

		channel_dir=`eval echo \$channel_dir `
		channel_file=`eval echo \$channel_file`
		DB_TABLE=`eval echo \$DB_TABLE`
		DB_DB=`eval echo "\$DB_DB"`
		v_folder='/var/lib/jenkins/linio-bi/dyalogo/'
		
		cat $v_system/sqls/CS/dyalogo/dyalogo_encuestas.sql > ./sqls/CS/dyalogo/tmp
		cat $v_system/sqls/CS/dyalogo/tmp  | sed s/@v_file@/${channel_file}/g>  $v_system/sqls/CS/dyalogo/tmp1
		cat $v_system/sqls/CS/dyalogo/tmp1 | sed s/@v_host@/${v__host}/g>       $v_system/sqls/CS/dyalogo/tmp2
		cat $v_system/sqls/CS/dyalogo/tmp2 | sed s/@v_db@/${DB_DB}/g>           $v_system/sqls/CS/dyalogo/tmp3
		cat $v_system/sqls/CS/dyalogo/tmp3 | sed s/@v_tables@/${DB_TABLE}/g>    $v_system/sqls/CS/dyalogo/tmp4
		
		cd $v_folder
		#cat $v_system/sqls/MKT/Bounce_Exchange/tmp4 | sed s/@v_folder@/"${v_folder}"/g>    $v_system/sqls/MKT/Bounce_Exchange/tmp5
		#echo "++++"
		var=$(ls $v_folder/$channel_file) #Donde se guardan los .csv
		for i in $var
		do
			#echo "-------------------"
			mysql -u${v_user}  -p${v_pwd} -h $v__host -b ${DB_DB}  < $v_system/sqls/CS/dyalogo/tmp4
			cat $v_system/sqls/CS/dyalogo/tmp4
			cd ..
		done
		echo "Se eliminan los temporales de SQL"
		cd $v_system
		
		rm $v_system/sqls/CS/dyalogo/tmp* 
done

exit
