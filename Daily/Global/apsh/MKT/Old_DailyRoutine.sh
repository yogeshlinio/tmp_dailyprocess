v_user=$1
v_pwd=$2
v_country=$3
v_date=`date +"%Y%m%d"`

path=/var/lib/jenkins/git_repository

#Waiting for Marketings
for i in  Mexico Colombia Peru Venezuela 
do
   v_country=$i
   v_sufix=""
   case $v_country in
      "Mexico") v_sufix="mx";;
      "Colombia") v_sufix="co";;
      "Peru") v_sufix="pe";;
      "Venezuela") v_sufix="ve";;
      *) echo "Wrong data Source";;
   esac

   v_event="marketing_report.marketing_${v_sufix}"
   v_reply=1000  #Wait Eight hours...
   v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
   . $path/scripts/ToolKit/monitoring.sh

done
echo ""

for i in  Mexico Peru Venezuela 
do
   v_country=$i
   v_event=A_Master
   v_reply=2000  #Wait Eight hours...
   v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
   . $path/scripts/ToolKit/monitoring.sh
done
echo ""


for i in  Colombia_Project
do
   v_country=$i
   #v_event=production_co.tbl_order_detail
   v_event=A_Master
   v_step=finish
   v_reply=2000  #Wait Eight hours...
   v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
   . $path/scripts/ToolKit/monitoring.sh
done
echo ""



v_country=$3

for v_folder in MarketingGlobal 
do
    echo "Loading $v_folder ..."

    sqls="./sqls/${v_folder}"
    for i in $sqls/*.sql
    do
       dateTime=`date`
       echo "|->"  $dateTime $i
	   mysql -u $v_user -p${v_pwd}   -h 172.17.12.191  -b marketing_report < $i
		error=`echo $?`
		if [ $error -gt 0 ]; then
			exit 1
		fi
    done
    dateTime=`date`
    echo " ${dateTime} End Load ..."
done
#cat ./logs/${v_date}_MKT_${v_country}.log  | mail -s "${v_country} MKT Process" jcharles.ruot@linio.com.mx -c carlos.mondragon@linio.com.mx

exit
