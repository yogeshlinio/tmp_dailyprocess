v_environment=$1
v_country=$2
v_verbose=$3
v_date=`date +"%Y%m%d"`

path=/var/lib/jenkins/git_repository

#Waiting for Facebook
v_country=Facebook
v_event=facebook.facebook_campaign
v_reply=8000  #Wait two hours...
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
. $path/scripts/ToolKit/monitoring.sh
v_country=$2
echo ""

#Waiting for A_Master
v_event=A_Master
v_reply=8000  #Wait two hours...
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
. $path/scripts/ToolKit/monitoring.sh

case $v_country in
      "Mexico") v_countryPrefix="mx";;
	  "Colombia") v_countryPrefix="co";;
  	  "Colombia_Project") v_countryPrefix="co";;
	  "Peru") v_countryPrefix="pe";;	  
	  "Venezuela") v_countryPrefix="ve";;
	  
	  *) echo "Wrong data source";;
esac

case $v_country in
      "Mexico") v_wmsSufix="";;
	  "Colombia") v_wmsSufix="_co";;
  	  "Colombia_Project") v_wmsSufix="_co";;
	  "Peru") v_wmsSufix="_pe";;	  
	  "Venezuela") v_wmsSufix="_ve";;
	  
	  *) echo "Wrong data source";;
esac


pwd

#Get all global configurations
#parse config.ini
. $lib/ToolKit/runEnviroment.sh