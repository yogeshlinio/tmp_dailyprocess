v_environment=$1
v_country=$2
v_verbose=$3
v_date=`date +"%Y%m%d"`

path=/var/lib/jenkins/git_repository

#Waiting for A_Master's:
v_country=$v_country
echo    "$v_country Daily Info..."
v_event=A_Master
v_step=finish
v_reply=2000
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
. $lib/ToolKit/monitoring.sh

#Waiting for Facebook Routine
v_country=Regional
v_event=marketing.facebook_campaigns
v_step=
v_reply=360  #Wait two hours...
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
. $path/scripts/ToolKit/monitoring.sh

#Waiting for is_Fashion
v_country=Global
v_event=marketing.is_fashion
v_step=finish
v_reply=360  #Wait two hours...
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
. $path/scripts/ToolKit/monitoring.sh

#Waiting for ga_performance
v_country=Global
v_event=marketing.ga_performance
v_step=finish
v_reply=360  #Wait two hours...
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
. $path/scripts/ToolKit/monitoring.sh

v_country=$2
echo ""

case $v_country in
      "Mexico") v_countryPrefix="mx";;
	  "Colombia") v_countryPrefix="co";;
  	  "Colombia_Project") v_countryPrefix="co";;
	  "Peru") v_countryPrefix="pe";;	  
	  "Venezuela") v_countryPrefix="ve";;
	  
	  *) echo "Wrong data source";;
esac

case $v_country in
      "Mexico") v_wmsSufix="";;
	  "Colombia") v_wmsSufix="_co";;
  	  "Colombia_Project") v_wmsSufix="_co";;
	  "Peru") v_wmsSufix="_pe";;	  
	  "Venezuela") v_wmsSufix="_ve";;
	  
	  *) echo "Wrong data source";;
esac

pwd

#Get all global configurations
#parse config.ini
. $lib/ToolKit/runEnviroment.sh