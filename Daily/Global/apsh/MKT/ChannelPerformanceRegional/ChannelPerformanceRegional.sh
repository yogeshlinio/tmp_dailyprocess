v_environment=$1
v_country=$2
v_verbose=$3
v_date=`date +"%Y%m%d"`

path=/var/lib/jenkins/git_repository


for i in  Mexico Peru Venezuela Colombia
do

	case $i in
		  "Mexico") v_countryPrefix="mx";;
		  "Colombia") v_countryPrefix="co";;
		 # "Colombia_Project") v_countryPrefix="co";;
		  "Peru") v_countryPrefix="pe";;	  
		  "Venezuela") v_countryPrefix="ve";;
		  
		  *) echo "Wrong data source";;
	esac

   v_country=$i
   v_event=marketing_${v_countryPrefix}.channel_performance
   v_step=finish
   v_reply=1000  #Wait n hours...
   v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
   . $path/scripts/ToolKit/monitoring.sh
done
echo ""
v_country=$2

pwd

#Get all global configurations
#parse config.ini
. $lib/ToolKit/runEnviroment.sh