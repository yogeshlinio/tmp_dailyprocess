v_user=$1
v_pwd=$2
v_host=$3
v_country=$4
v_date=`date +"%Y%m%d"`

#Waiting for marketing_ve
v_step=finish
v_event=marketing_report.marketing_ve
v_country=Venezuela
v_reply=2  #Wait two hours...
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
. ../../scripts/ToolKit/monitoring.sh
echo "---"

#Waiting for marketing_pe
v_step=finish
v_event=marketing_report.marketing_pe
v_country=Peru
v_reply=2  #Wait two hours...
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
. ../../scripts/ToolKit/monitoring.sh
echo "---"

#Waiting for marketing_mx
v_step=finish
v_event=marketing_report.marketing_mx
v_country=Mexico
v_reply=2  #Wait two hours...
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
. ../../scripts/ToolKit/monitoring.sh
echo "---"

#Waiting for marketing_co
v_step=finish
v_event=marketing_report.marketing_co
v_country=Colombia
v_reply=2  #Wait two hours...
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
. ../../scripts/ToolKit/monitoring.sh
echo "---"


for v_folder in MKT/Global
do
    echo "Loading $v_folder ..."
    sqls="./sqls/${v_folder}"
    for i in $sqls/*.sql
    do
       dateTime=`date`
	   echo $dateTime $i 
	   #mysql -u ${v_user} -p${v_pwd}   -h ${v_host} -b marketing_report < $i
		error=`echo $?`
		if [ $error -gt 0 ]; then
			exit 1
		fi
    done
    dateTime=`date`
    echo " ${dateTime} End Load ..."
done

