cp -fr Linio_MX_1104_a.sql tmp
for i in `cat TablesNames.txt`
do
   tableOrg=`echo $i | awk -F '@' '{print $1}'`
   tableDes=`echo $i | awk -F '@' '{print $2}'`
   > aux.sql
   cat tmp | sed s/$tableOrg/$tableDes/g >> aux.sql
   mv aux.sql tmp
done
