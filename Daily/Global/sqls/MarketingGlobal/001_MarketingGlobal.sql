call marketing_global;


INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  step,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Marketing',
  'marketing_report.marketing_global',
  'finish',
  NOW(),
  NOW(),
  1,
  1
;


