DROP TABLE IF EXISTS A_Master_Catalog;
CREATE TABLE A_Master_Catalog LIKE development_mx.A_Master_Catalog;
ALTER TABLE A_Master_Catalog  DROP PRIMARY KEY ,ADD COLUMN Country VARCHAR(50) first;
ALTER TABLE A_Master_Catalog  ADD PRIMARY KEY ( Country, Sku_Simple ) ;
INSERT A_Master_Catalog 
SELECT 
   "Mexico" AS Country,
   MAS.* 
FROM
   development_mx.A_Master_Catalog AS MAS;

INSERT A_Master_Catalog
(
Country,
id_catalog_config,
id_catalog_simple,
sku_simple,
sku_config,
sku_name,
id_catalog_attribute_option_global_category,
Cat1,
id_catalog_attribute_option_global_sub_category,
Cat2,
id_catalog_attribute_option_global_sub_sub_category,
Cat3,
Cat_KPI,
Cat_BP,
Product_Weight,
Package_Weight,
Package_Height,
Package_Length,
Package_Width,
Product_Measures,
id_supplier,
Supplier,
delivery_type,
id_catalog_attribute_option_global_buyer,
Head_Buyer,
Buyer,
id_catalog_brand,
Brand,
attribute,
model,
color,
cost,
cost_oms,
delivery_cost_supplier,
eligible_free_shipping,
original_price,
special_price,
special_from_date,
special_to_date,
price,
price_comparison,
special_purchase_price,
fixed_price,
isVisible,
isActive_SKUConfig,
isActive_SKUSimple,
display_if_out_of_stock,
pet_status,
pet_approved,
status_config,
status_simple,
barcode_ean,
stock,
stockout,
warehouse_stock,
supplier_stock,
Reserved_BOB,
repurchase_status,
isMarketPlace,
#MPlace_Fee,
isMarketPlace_Since,
isMarketPlace_Until,
created_at_Config,
created_at_Simple,
id_catalog_tax_class,
tax_percent,
id_catalog_shipment_type,
fulfillment_type,
min_delivery_time,
max_delivery_time,
promise_delivery_badge,
delivery_time_supplier,
tax_class,
boost,
interval_age,
variation
)
SELECT 
   "Peru" AS Country,
id_catalog_config,
id_catalog_simple,
sku_simple,
sku_config,
sku_name,
id_catalog_attribute_option_global_category,
Cat1,
id_catalog_attribute_option_global_sub_category,
Cat2,
id_catalog_attribute_option_global_sub_sub_category,
Cat3,
Cat_KPI,
Cat_BP,
Product_Weight,
Package_Weight,
Package_Height,
Package_Length,
Package_Width,
Product_Measures,
id_supplier,
Supplier,
delivery_type,
id_catalog_attribute_option_global_buyer,
Head_Buyer,
Buyer,
id_catalog_brand,
Brand,
attribute,
model,
color,
cost,
cost_oms,
delivery_cost_supplier,
eligible_free_shipping,
original_price,
special_price,
special_from_date,
special_to_date,
price,
price_comparison,
special_purchase_price,
fixed_price,
isVisible,
isActive_SKUConfig,
isActive_SKUSimple,
display_if_out_of_stock,
pet_status,
pet_approved,
status_config,
status_simple,
barcode_ean,
stock,
stockout,
warehouse_stock,
supplier_stock,
Reserved_BOB,
repurchase_status,
isMarketPlace,
#MPlace_Fee,
isMarketPlace_Since,
isMarketPlace_Until,
created_at_Config,
created_at_Simple,
id_catalog_tax_class,
tax_percent,
id_catalog_shipment_type,
fulfillment_type,
min_delivery_time,
max_delivery_time,
promise_delivery_badge,
delivery_time_supplier,
tax_class,
boost,
interval_age,
variation
FROM
   development_pe.A_Master_Catalog AS MAS;



INSERT A_Master_Catalog
(
Country,
id_catalog_config,
id_catalog_simple,
sku_simple,
sku_config,
sku_name,
id_catalog_attribute_option_global_category,
Cat1,
id_catalog_attribute_option_global_sub_category,
Cat2,
id_catalog_attribute_option_global_sub_sub_category,
Cat3,
Cat_KPI,
Cat_BP,
Product_Weight,
Package_Weight,
Package_Height,
Package_Length,
Package_Width,
Product_Measures,
id_supplier,
Supplier,
delivery_type,
id_catalog_attribute_option_global_buyer,
Head_Buyer,
Buyer,
id_catalog_brand,
Brand,
attribute,
model,
color,
cost,
cost_oms,
delivery_cost_supplier,
eligible_free_shipping,
original_price,
special_price,
special_from_date,
special_to_date,
price,
price_comparison,
special_purchase_price,
fixed_price,
isVisible,
isActive_SKUConfig,
isActive_SKUSimple,
display_if_out_of_stock,
pet_status,
pet_approved,
status_config,
status_simple,
barcode_ean,
stock,
stockout,
warehouse_stock,
supplier_stock,
Reserved_BOB,
repurchase_status,
isMarketPlace,
#MPlace_Fee,
isMarketPlace_Since,
isMarketPlace_Until,
created_at_Config,
created_at_Simple,
id_catalog_tax_class,
tax_percent,
id_catalog_shipment_type,
fulfillment_type,
min_delivery_time,
max_delivery_time,
promise_delivery_badge,
delivery_time_supplier,
tax_class,
boost,
interval_age,
variation
)
SELECT 
   "Venezuela" AS Country,
id_catalog_config,
id_catalog_simple,
sku_simple,
sku_config,
sku_name,
id_catalog_attribute_option_global_category,
Cat1,
id_catalog_attribute_option_global_sub_category,
Cat2,
id_catalog_attribute_option_global_sub_sub_category,
Cat3,
Cat_KPI,
Cat_BP,
Product_Weight,
Package_Weight,
Package_Height,
Package_Length,
Package_Width,
Product_Measures,
id_supplier,
Supplier,
delivery_type,
id_catalog_attribute_option_global_buyer,
Head_Buyer,
Buyer,
id_catalog_brand,
Brand,
attribute,
model,
color,
cost,
cost_oms,
delivery_cost_supplier,
eligible_free_shipping,
original_price,
special_price,
special_from_date,
special_to_date,
price,
price_comparison,
special_purchase_price,
fixed_price,
isVisible,
isActive_SKUConfig,
isActive_SKUSimple,
display_if_out_of_stock,
pet_status,
pet_approved,
status_config,
status_simple,
barcode_ean,
stock,
stockout,
warehouse_stock,
supplier_stock,
Reserved_BOB,
repurchase_status,
isMarketPlace,
#MPlace_Fee,
isMarketPlace_Since,
isMarketPlace_Until,
created_at_Config,
created_at_Simple,
id_catalog_tax_class,
tax_percent,
id_catalog_shipment_type,
fulfillment_type,
min_delivery_time,
max_delivery_time,
promise_delivery_badge,
delivery_time_supplier,
tax_class,
boost,
interval_age,
variation
FROM
   development_ve.A_Master_Catalog AS MAS;



INSERT A_Master_Catalog
(
Country,
id_catalog_config,
id_catalog_simple,
sku_simple,
sku_config,
sku_name,
id_catalog_attribute_option_global_category,
Cat1,
id_catalog_attribute_option_global_sub_category,
Cat2,
id_catalog_attribute_option_global_sub_sub_category,
Cat3,
Cat_KPI,
Cat_BP,
Product_Weight,
Package_Weight,
Package_Height,
Package_Length,
Package_Width,
Product_Measures,
id_supplier,
Supplier,
delivery_type,
id_catalog_attribute_option_global_buyer,
Head_Buyer,
Buyer,
id_catalog_brand,
Brand,
attribute,
model,
color,
cost,
cost_oms,
delivery_cost_supplier,
eligible_free_shipping,
original_price,
special_price,
special_from_date,
special_to_date,
price,
price_comparison,
special_purchase_price,
fixed_price,
isVisible,
isActive_SKUConfig,
isActive_SKUSimple,
display_if_out_of_stock,
pet_status,
pet_approved,
status_config,
status_simple,
barcode_ean,
stock,
stockout,
warehouse_stock,
supplier_stock,
Reserved_BOB,
repurchase_status,
isMarketPlace,
#MPlace_Fee,
isMarketPlace_Since,
isMarketPlace_Until,
created_at_Config,
created_at_Simple,
id_catalog_tax_class,
tax_percent,
id_catalog_shipment_type,
fulfillment_type,
min_delivery_time,
max_delivery_time,
promise_delivery_badge,
delivery_time_supplier,
tax_class,
boost,
interval_age,
variation
)
SELECT 
   "Colombia" AS Country,
id_catalog_config,
id_catalog_simple,
sku_simple,
sku_config,
sku_name,
id_catalog_attribute_option_global_category,
Cat1,
id_catalog_attribute_option_global_sub_category,
Cat2,
id_catalog_attribute_option_global_sub_sub_category,
Cat3,
Cat_KPI,
Cat_BP,
Product_Weight,
Package_Weight,
Package_Height,
Package_Length,
Package_Width,
Product_Measures,
id_supplier,
Supplier,
delivery_type,
id_catalog_attribute_option_global_buyer,
Head_Buyer,
Buyer,
id_catalog_brand,
Brand,
attribute,
model,
color,
cost,
cost_oms,
delivery_cost_supplier,
eligible_free_shipping,
original_price,
special_price,
special_from_date,
special_to_date,
price,
price_comparison,
special_purchase_price,
fixed_price,
isVisible,
isActive_SKUConfig,
isActive_SKUSimple,
display_if_out_of_stock,
pet_status,
pet_approved,
status_config,
status_simple,
barcode_ean,
stock,
stockout,
warehouse_stock,
supplier_stock,
Reserved_BOB,
repurchase_status,
isMarketPlace,
#MPlace_Fee,
isMarketPlace_Since,
isMarketPlace_Until,
created_at_Config,
created_at_Simple,
id_catalog_tax_class,
tax_percent,
id_catalog_shipment_type,
fulfillment_type,
min_delivery_time,
max_delivery_time,
promise_delivery_badge,
delivery_time_supplier,
tax_class,
boost,
interval_age,
variation 
FROM
   development_co_project.A_Master_Catalog AS MAS;
