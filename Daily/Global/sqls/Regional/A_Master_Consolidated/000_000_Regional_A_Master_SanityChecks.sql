INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Regional', 
  'A_Master',
  "start",
  NOW(),
  MAX(date),
  COUNT(*),
  COUNT(*)
FROM
A_Master
;


