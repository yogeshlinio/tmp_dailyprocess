DROP TABLE IF EXISTS @v_db@.@v_tables@;

CREATE TABLE `@v_tables@` (
  `unique_id` varchar(255) DEFAULT NULL,
  `secuencia_encuesta` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


LOAD DATA LOCAL INFILE '@v_file@'
IGNORE INTO TABLE @v_db@.@v_tables@
FIELDS TERMINATED BY ','
;
