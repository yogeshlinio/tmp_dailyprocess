call  production.monitoring_step( "ODS.Sku_simple", "@v_country@", "finish" , @v_times@ );
/* 
*   UPDATE FIELDS ON OSRI
*/
UPDATE            A_Master_Sample
       INNER JOIN ODS.Sku_Simple_@v_countryPrefix@  AS ODS
	             ON ODS.SKU_Simple = A_Master_Sample.SKUSimple
SET
    A_Master_Sample.SKUConfig   = ODS.sku_config,
    A_Master_Sample.SKUName     = ODS.sku_name,	
    A_Master_Sample.IdSupplier  = ODS.id_supplier,
    A_Master_Sample.Supplier    = ODS.Supplier,
    A_Master_Sample.Buyer       = ODS.Buyer,
    A_Master_Sample.HeadBuyer   = ODS.Head_Buyer,
    A_Master_Sample.Brand       = ODS.Brand,

    #A_Master_Sample.isMPlace    = ODS.isMarketPlace,
    #A_Master_Sample.isMPlaceSince = ODS.isMarketPlace_Since,
    #A_Master_Sample.isMPlaceUntil = ODS.isMarketPlace_Until,
    #A_Master_Sample.MPlaceFee     = ODS.MPlace_Fee,

    A_Master_Sample.isVisible           = ODS.isVisible,
    A_Master_Sample.isActiveSKUConfig   = ODS.isActive_SKUConfig,
    A_Master_Sample.isActiveSKUSimple   = ODS.isActive_SKUSimple,

    A_Master_Sample.Cat1       = ODS.Cat1,
    A_Master_Sample.Cat2       = ODS.Cat2,
    A_Master_Sample.Cat3       = ODS.Cat3,
   
    A_Master_Sample.CatKPI     = ODS.Cat_KPI,
    A_Master_Sample.CatBP      = ODS.Cat_BP,
    
    A_Master_Sample.PackageWeight = IF( ODS.Package_Weight = 0, IF( ODS.Product_weight = 0 , 1 , ODS.Product_weight ), ODS.Package_Weight),
    A_Master_Sample.ProductWeight = IF( ODS.Product_weight = 0, IF( ODS.Package_Weight = 0 , 1 , ODS.Package_Weight ) , ODS.Product_weight) ,

    A_Master_Sample.PackageWeight = ODS.Package_Weight,
  
	
    A_Master_Sample.PackageHeight = ODS.Package_Height,
    A_Master_Sample.PackageLength = ODS.Package_Length,
    A_Master_Sample.PackageWidth  = ODS.Package_Width,
	A_Master_Sample.DeliveryType = ODS.delivery_type,
	A_Master_Sample.SupplierType = ODS.Supplier_Type,
	A_Master_Sample.ProductsOrigin = ODS.Products_Origin,	
    A_Master_Sample.DeliveryCostSupplier = IF( A_Master_Sample.DeliveryCostSupplier <= 0 , ODS.Delivery_Cost_Supplier, A_Master_Sample.DeliveryCostSupplier ), 
    A_Master_Sample.VolumeWeight  = ODS.Volumetric_Weight,
    A_Master_Sample.OriginalPrice = IF( ODS.Original_Price <= 0 , ODS.Price , ODS.Original_Price )
;




