/*
*  MX_008_001 Carrier
*/
#Query 1: Se agrega el nombre del Carrier
UPDATE            A_Master_Sample as a
       INNER JOIN @bob_live@.shipment_carrier c
	           ON a.fk_courier = c.id_shipment_carrier
SET
    a.Courier = if( c.`name` !='', c.`name` , fk_courier);


