UPDATE A_Master_Sample
INNER JOIN GlobalConfig.M_PaymentMethodType ON A_Master_Sample.PaymentMethod = M_PaymentMethodType.PaymentMethod
SET A_Master_Sample.PaymentMethodType = M_PaymentMethodType.PaymentMethodType
WHERE
	A_Master_Sample.Country = M_PaymentMethodType.Country;

