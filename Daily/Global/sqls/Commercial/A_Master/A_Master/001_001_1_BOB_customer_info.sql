UPDATE            @development@.A_Master_Sample
       INNER JOIN @bob_live@.sales_order_address
               ON @bob_live@.sales_order_address.id_sales_order_address = @development@.A_Master_Sample.fk_sales_order_address_shipping
SET
   @development@.A_Master_Sample.postcode                   = @bob_live@.sales_order_address.postcode, 
   @development@.A_Master_Sample.city                       = @bob_live@.sales_order_address.city,
   @development@.A_Master_Sample.fk_customer_address_region = @bob_live@.sales_order_address.fk_customer_address_region
;
UPDATE            @development@.A_Master_Sample
       INNER JOIN @bob_live@.customer_address_region
               ON @bob_live@.customer_address_region.id_customer_address_region = @development@.A_Master_Sample.fk_customer_address_region
SET
   @development@.A_Master_Sample.State = @bob_live@.customer_address_region.code 
;