INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'A_Master',
  "start",
  NOW(),
  MAX(date),
  count(*),
  count(*)
FROM
A_Master
;


