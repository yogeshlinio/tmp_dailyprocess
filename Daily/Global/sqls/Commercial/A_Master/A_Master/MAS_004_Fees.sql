/*
*  004 Fees
*/
UPDATE        @development@.A_Master_Sample 
   INNER JOIN @development@.A_E_In_COM_Fees_per_PM 
           ON     @development@.A_Master_Sample.PaymentMethod = @development@.A_E_In_COM_Fees_per_PM.Payment_Method
              AND @development@.A_Master_Sample.Installment   = @development@.A_E_In_COM_Fees_per_PM.Installment
SET
   @development@.A_Master_Sample.Fees        = @development@.A_E_In_COM_Fees_per_PM.Fee,
   @development@.A_Master_Sample.ExtraCharge = @development@.A_E_In_COM_Fees_per_PM.Extra_Charge;
