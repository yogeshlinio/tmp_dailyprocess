DROP TEMPORARY TABLE
IF EXISTS tmp_CreditNotes;

CREATE TEMPORARY TABLE tmp_CreditNotes SELECT
	C.id_CreditNote,
	C.country,
	C.supplier,
	C.Month_Num,
	C.Category_BP,
	C.Reference,
	sum(C.Cost) AS Cost,
	sum(C.Cost_after_tax) AS Cost_after_tax,
	D.contador
FROM
	GlobalConfig.M_CreditNotes C
LEFT JOIN (
	SELECT
		A.Supplier,
		A.MonthNum,
		A.CatBP,
		count(*) contador
	FROM
		A_Master_Sample A
	WHERE
		A.OrderAfterCan = 1
	GROUP BY
		A.Supplier,
		A.MonthNum,
		A.CatBP
) D ON C.Supplier = D.supplier
WHERE
	D.MonthNum = C.Month_Num
AND D.CatBP = C.Category_BP
AND C.Country = "@v_countryShortName"
GROUP BY
	MonthNum,supplier;

UPDATE A_Master_Sample A,
 @development@.tmp_CreditNotes B
SET A.CreditNotes = B.Cost_after_tax / B.contador
WHERE
	A.Supplier = B.supplier
AND A.MonthNum > 201403
AND A.MonthNum = B.Month_Num
AND A.CatBP = B.Category_BP
AND A.OrderAfterCan = 1;

