UPDATE            A_Master_Sample
       INNER JOIN @bob_live@.sales_order_item_status 
               ON A_Master_Sample.fk_sales_order_item_status = sales_order_item_status.id_sales_order_item_status
SET
   A_Master_Sample.status = sales_order_item_status.name;

  
#Query: A 102 U OrderBefore-AfterCan
UPDATE            @development@.A_Master_Sample 
       INNER JOIN @development@.M_Bob_Order_Status_Definition 
              ON     ( A_Master_Sample.PaymentMethod = M_Bob_Order_Status_Definition.Payment_Method) 
                 AND ( A_Master_Sample.Status        = M_Bob_Order_Status_Definition.Status) 
SET 
     A_Master_Sample.OrderBeforeCan = M_Bob_Order_Status_Definition.OrderBeforeCan, 
     A_Master_Sample.OrderAfterCan  = M_Bob_Order_Status_Definition.OrderAfterCan, 
     A_Master_Sample.Cancellations  = M_Bob_Order_Status_Definition.Cancellations, 
     A_Master_Sample.Pending        = M_Bob_Order_Status_Definition.Pending
;

