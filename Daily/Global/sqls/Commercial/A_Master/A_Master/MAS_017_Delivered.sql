call  production.monitoring_step( 'ODS.itens_venda_id' , "@v_country@", 'is_marks' , @v_times@ );
  
/*
* MX_018_001 Delivered
*/
DROP   TEMPORARY TABLE IF EXISTS A_Delivered;
CREATE TEMPORARY TABLE A_Delivered ( INDEX ( fk_sales_order_item  ) )
SELECT
 *
FROM
    @bob_live@.sales_order_item_status_history
WHERE
	#delivered, delivered_electronically
   @bob_live@.sales_order_item_status_history.fk_sales_order_item_status IN ( 52 , 98 )
GROUP BY fk_sales_order_item
;

UPDATE           A_Delivered
      INNER JOIN A_Master_Sample
              ON A_Delivered.fk_sales_order_item = A_Master_Sample.ItemID 
SET
    A_Master_Sample.DateDelivered = A_Delivered.created_at,
    A_Master_Sample.Delivered = 1;

/*
*  MX_018_002	DateDelivered
*/
UPDATE 
               A_Master_Sample  
    INNER JOIN ODS.itens_venda_oot_sample_@v_countryPrefix@ b
            ON A_Master_Sample.ItemID = b.item_id
SET
   A_Master_Sample.DateDelivered =  b.date_delivered,
   A_Master_Sample.Delivered =  b.is_delivered
WHERE 
   b.is_delivered != 0
;



UPDATE A_Master_Sample
SET
    A_Master_Sample.NetDelivered = 1
WHERE
	#closed, delivered, delivered_electronically
    Status in ( select name from @bob_live@.sales_order_item_status
                where id_sales_order_item_status IN ( 6 ,52, 98 ) )
;

UPDATE A_Master_Sample
SET
    A_Master_Sample.DeliveredReturn = 1
WHERE
        Delivered     = 1
    AND NetDelivered  = 0;