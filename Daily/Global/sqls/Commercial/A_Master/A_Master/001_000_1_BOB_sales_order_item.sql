/*
UPDATE A_Master_Sample
INNER JOIN @bob_live@.sales_order
ON A_Master_Sample.idSalesORder = @bob_live@.sales_order.id_sales_order
SET 
`ShippingFee`   	 = @bob_live@.sales_order.shipping_amount  /1.16 
;
*/
SET @since = "2013-01-01";
DROP TABLE IF EXISTS A_Master_Sample; 
CREATE TABLE A_Master_Sample LIKE A_Master_Template;
ALTER TABLE A_Master_Sample
            ADD COLUMN fk_customer_address_region      INT NOT NULL,
            ADD COLUMN fk_sales_order_address_shipping INT NOT NULL,
            ADD COLUMN fk_sales_order_item_status      INT NOT NULL,
            ADD COLUMN fk_catalog_shipment_type        INT NOT NULL,
    

            ADD INDEX  ( couponCode ),
            ADD INDEX  ( fk_customer_address_region ),			
            ADD INDEX  ( fk_customer_address_region ),
            ADD INDEX  ( fk_sales_order_address_shipping ),
            ADD INDEX  ( fk_sales_order_item_status ),
            ADD INDEX  ( fk_catalog_shipment_type  ),

			ADD INDEX  ( city  ),  			
            ADD Index PaymentMethodStatus ( paymentMethod , status )
;

INSERT A_Master_Sample
( 
  `Country`   	,
  `MonthNum`   	,
  `Date`   	,
  `Time`   	,  
  `OrderNum`   	,
   idSalesOrder	,
  `ItemID`   	,
  `StoreId`   	,
  `SKUSimple`   	,
  `SKUName`   	,
  `OriginalPrice`   	,
  `OriginalPriceAfterTax`   	,
  `TaxPercent`   	,
  `Tax`   	,
  `Price`   	,
  `PriceAfterTax`   	,
  `CouponCode`   	,
  `CouponValue`   	,
  `CouponValueAfterTax`   	,
  `PaidPrice`   	,
  `PaidPriceAfterTax`   	,
  `GrandTotal`   	,
  `Cost`   	,
  `CostAfterTax`   	,
  `ShipmentCost`   	,
  `ShippingFee`   	,
  `PaymentMethod`   	,
  `CustomerNum`   	,
  `CustomerEmail`   	,
  `FirstName`   	,
  `LastName`   	,
  `deliveryCostSupplier`,
  `AssistedSalesOperator`, 
  `AboutLinio`, 
  
  `isMPlace`,
  `fk_courier`,
  
  `fk_sales_order_address_shipping` ,
  `fk_sales_order_item_status`,
  `fk_catalog_shipment_type`
)
SELECT
"@v_countryShortName@" AS Country,
date_format( @bob_live@.sales_order.created_at , "%Y%m" )	AS	`MonthNum`   	,
sales_order.created_at	AS	`Date`   	,
date_format(sales_order.created_at , "%T" )	AS	`Time`,    
sales_order.order_nr	AS	`OrderNum`   	,
sales_order.id_sales_order	AS	idSalesOrder	,
sales_order_item.id_sales_order_item	AS	`ItemID`   	,
sales_order.store_id	AS	`StoreId`   	,
sales_order_item.sku	AS	`SKUSimple`   	,
sales_order_item.name	AS	`SKUName`   	,
sales_order_item.original_unit_price	AS	`OriginalPrice`   	,
sales_order_item.original_unit_price/( ( 100 +  sales_order_item.tax_percent )/100 )	AS	`OriginalPriceAfterTax`   	,
sales_order_item.tax_percent	AS	`TaxPercent`   	,
sales_order_item.tax_amount	AS	`Tax`   	,
sales_order_item.unit_price	AS	`Price`   	,
sales_order_item.unit_price /( ( 100 +  sales_order_item.tax_percent )/100 )	AS	`PriceAfterTax`   	,
sales_order.coupon_code	AS	`CouponCode`   	,
sales_order_item.coupon_money_value	AS	`CouponValue`   	,
sales_order_item.coupon_money_value/( ( 100 +  sales_order_item.tax_percent )/100 )	AS	`CouponValueAfterTax`   	,
sales_order_item.paid_price	AS	`PaidPrice`   	,
sales_order_item.paid_price/( ( 100 +  sales_order_item.tax_percent )/100 )	AS	`PaidPriceAfterTax`   	,
sales_order.Grand_Total	AS	`GrandTotal`   	,
sales_order_item.cost	AS	`Cost`   	,
sales_order_item.cost /( ( 100 +  sales_order_item.tax_percent )/100 )	AS	`CostAfterTax`   	,
sales_order_item.shipment_cost	AS	`ShipmentCost`   	,
sales_order_item.shipping_amount  AS	`ShippingFee`   	,

sales_order.payment_method	AS	`PaymentMethod`   	,
sales_order.fk_customer	AS	`CustomerNum`   	,
sales_order.customer_email	AS	`CustomerEmail`   	,
sales_order.customer_first_name	AS	`FirstName`   	,
sales_order.customer_last_name	AS	`LastName`   	,
sales_order_item.delivery_cost_supplier/( ( 100 +  sales_order_item.tax_percent )/100 )	AS	`DeliveryCostSupplier`   	,
sales_order.assisted_sales_operator as `AssistedSalesOperator`,
sales_order.about_linio AS AboutLinio,
sales_order_item.is_option_marketplace as isMPlace,
	
sales_order_item.fk_shipment_carrier as fk_courier,
sales_order.fk_sales_order_address_shipping	AS	`fk_customer_address_shipping` ,
sales_order_item.fk_sales_order_item_status ,
sales_order_item.fk_catalog_shipment_type

FROM 
              @bob_live@.sales_order 
   INNER JOIN @bob_live@.sales_order_item
           ON @bob_live@.sales_order_item.fk_sales_order = @bob_live@.sales_order.id_sales_order
#WHERE
#     @bob_live@.sales_order.created_at Between @since and ( LAST_DAY( @since ) + INTERVAL 1 DAY )
#  OR @bob_live@.sales_order.updated_at Between @since and ( LAST_DAY( @since ) + INTERVAL 1 DAY )
#  OR @bob_live@.sales_order_item.created_at Between @since and ( LAST_DAY( @since ) + INTERVAL 1 DAY )
#  OR @bob_live@.sales_order_item.updated_at Between @since and ( LAST_DAY( @since ) + INTERVAL 1 DAY )
;