call  production.monitoring_step( 'ODS.Id_Procurement_Order_Item' , "@v_country@", 'Cost_Oms' , @v_times@ );

/*
*  Costo de BOB
*/
UPDATE        A_Master_Sample
   INNER JOIN A_Master_Catalog
           ON A_Master_Sample.SKUSimple = A_Master_Catalog.SKU_simple
SET
   A_Master_Sample.Cost         = A_Master_Catalog.cost,
   A_Master_Sample.CostAfterTax = A_Master_Catalog.cost / 
                                                 ( 1 +  ( A_Master_Sample.TaxPercent / 100 ) )          
WHERE
      A_Master_Sample.Cost is null 
   OR A_Master_Sample.Cost = 0
;
   
/*
* Costo Promedio
*/    
UPDATE        A_Master_Sample
   INNER JOIN A_Master_Catalog p
           ON A_Master_Sample.SKUSimple= p.sku_simple 
SET 
   A_Master_Sample.Cost = p.Cost_OMS  ,
   A_Master_Sample.CostAftertax = p.Cost_OMS /( ( 100 +  A_Master_Sample.TaxPercent )/100 )
   
WHERE
 p.Cost_OMS > 0 
  and
  (A_Master_Sample.Cost is null 
	   OR A_Master_Sample.Cost = 0)
;

/*
* VE_005_Cost_OMS
*/
UPDATE             A_Master_Sample  
        INNER JOIN ODS.Id_Procurement_Order_Item_Sample_@v_countrPrefix@ b 
	            ON A_Master_Sample.ItemId = b.item_id
SET 
    A_Master_Sample.Cost           = b.cost_oms,
    A_Master_Sample.CostAftertax   = b.cost_oms_after_tax
;
