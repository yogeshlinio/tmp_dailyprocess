call  production.monitoring_step( "ODS.Sku_simple_Key_Map", "@v_country@", "finish" , @v_times@ );

DROP TABLE IF EXISTS A_Master_Catalog_Sample;

CREATE TABLE  A_Master_Catalog_Sample LIKE A_Master_Catalog_Template;
INSERT  A_Master_Catalog_Sample 
(
   country,
   id_catalog_config,
   id_catalog_simple,
   sku_simple,
   id_catalog_attribute_option_global_category,
   id_catalog_attribute_option_global_sub_category,
   id_catalog_attribute_option_global_sub_sub_category,
   id_supplier,
   id_catalog_attribute_option_global_head_buyer,
   id_catalog_attribute_option_global_buyer,
   id_catalog_brand,
   id_catalog_tax_class,
   id_catalog_shipment_type
)
SELECT
   Country,
   id_catalog_config,
   id_catalog_simple,
   sku_simple,
   id_catalog_attribute_option_global_category,
   id_catalog_attribute_option_global_sub_category,
   id_catalog_attribute_option_global_sub_sub_category,
   id_supplier,
   id_catalog_attribute_option_global_head_buyer,
   id_catalog_attribute_option_global_buyer,
   id_catalog_brand,
   id_catalog_tax_class,
   id_catalog_shipment_type
FROM
   ODS.Sku_Simple_Key_Map_Sample_@v_countryPrefix@
;