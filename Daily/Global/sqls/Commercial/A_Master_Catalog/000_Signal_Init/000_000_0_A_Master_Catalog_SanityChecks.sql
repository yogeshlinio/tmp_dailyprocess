INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date
  )
SELECT 
  '@v_country@',
  'A_Master_Catalog',
  "start",
  NOW(),
  NOW()
;