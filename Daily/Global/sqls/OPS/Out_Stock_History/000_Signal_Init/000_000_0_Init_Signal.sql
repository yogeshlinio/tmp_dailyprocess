﻿INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  UPDATEd_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'out_stock_hist',
  'start',
  NOW(),
  max(date_exit),
  count(*),
  count(item_counter)
FROM
  out_stock_hist;