﻿#Próximos 3 queries chequean si el sku está en stock, cuando hay al menos un item disponible
TRUNCATE TABLE skus_in_stock;

INSERT INTO skus_in_stock
select distinct(sku_simple) from Out_Stock_Hist_Sample
WHERE in_stock=1 and reserved=0;

SET @i = 0 ; 
DROP TEMPORARY TABLE IF EXISTS TMP ; 
CREATE TEMPORARY TABLE TMP 
SELECT
	sku_config,
	Count(*) AS items_in_stock,
	sum(cost_w_o_vat) AS cost_w_o_vat_in_stock
FROM
	out_stock_hist
WHERE
	in_stock = 1
AND reserved = 0
AND (
			fulfillment_type_real <> 'consignment'
			OR fulfillment_type_real IS NOT NULL
		)
GROUP BY
	sku_config;

DROP TABLE IF EXISTS top_skus_sell_list ; 
CREATE TABLE top_skus_sell_list 
SELECT
	sku_config,
	items_in_stock,
	cost_w_o_vat_in_stock,
	@i := @i + 1 AS rkn
FROM
	TMP
ORDER BY
	cost_w_o_vat_in_stock DESC ;

# Sell List
DELETE FROM pro_sell_list_hist_totals where date_reported = curdate();

INSERT INTO pro_sell_list_hist_totals (
	date_reported,
	category_bp,
	cost_w_o_vat_over_two_items,
	cost_w_o_vat_under_two_items) 
SELECT
	J.date_reported,
	J.Categoria,
	J.Value_more_than_two,
	J.Value_less_than_two
FROM
	(	SELECT
			curdate() AS date_reported,
			B.category_bp AS 'Categoria',
			B.cost_w_o_vat AS Value_more_than_two,
			D.cost_w_o_vat AS Value_less_than_two
		FROM
			(	SELECT
					category_bp,
					sum(cost_w_o_vat) AS cost_w_o_vat
				FROM
					(	SELECT
							category_bp,
							sku_config,
							sum(cost_w_o_vat) AS cost_w_o_vat
						FROM
							out_stock_hist
						WHERE
							is_sell_list = 1
						AND category_bp IS NOT NULL
						GROUP BY
							sku_config
						HAVING
							count(sku_config) > 2
					) A
				GROUP BY
					category_bp
			) B
		LEFT JOIN (	SELECT
									category_bp,
									sum(cost_w_o_vat) AS cost_w_o_vat
								FROM
									(	SELECT
											category_bp,
											sum(cost_w_o_vat) AS cost_w_o_vat
										FROM
											out_stock_hist
										WHERE
											is_sell_list = 1
										GROUP BY
											sku_config
										HAVING
											count(sku_config) <= 2
									) C
								GROUP BY
									category_bp
									) D 
		ON B.category_bp = D.category_bp
	) J;



#Calcular PC 1.5
DROP TEMPORARY TABLE IF EXISTS TMP_Reserved;
CREATE TEMPORARY TABLE TMP_Reserved ( PRIMARY KEY ( sku_simple  ) )
SELECT
   sku_simple,
		sum(ifnull(in_stock, 0)) stock_wms
FROM
   out_stock_hist
WHERE
      reserved = 0
  AND in_stock = 1
GROUP BY
sku_simple
;

/*PASO 1 INSERTAMOS VALORES GENERALES*/
DROP TABLE IF EXISTS  tbl_bi_ops_pc_sample;
CREATE TABLE  tbl_bi_ops_pc_sample LIKE  tbl_bi_ops_pc; 
ALTER TABLE  tbl_bi_ops_pc_sample 
   ADD COLUMN tax_percent DOUBLE, 
   ADD COLUMN delivery_cost_supplier DECIMAL(15,2),
   ADD COLUMN eligible_free_shipping INT,
   ADD COLUMN precio_actual DECIMAL(15,2)
    ;
INSERT  tbl_bi_ops_pc_sample (
   sku,
   tax_percent,
   delivery_cost_supplier,
   eligible_free_shipping
   )    
SELECT 
	sku_simple,
    tax_Percent,
    delivery_cost_supplier,
    eligible_free_shipping
FROM
   @development@.A_Master_Catalog
;
/*INSERT REFFERENCES TO SH*/
UPDATE        tbl_bi_ops_pc_sample AS c
  INNER JOIN @development@.tbl_sku_precios_costos sh 
          ON sh.sku_simple = c.sku
SET
   c.pc_15 = sh.precio_actual - COALESCE(sh.costo_actual,0,sh.costo_actual)-
			                        - COALESCE(c.delivery_cost_supplier,0,c.delivery_cost_supplier)
                              - COALESCE(sh.costo_actual IS NULL,0,0.025 * sh.costo_actual),
   c.precio_actual = sh.precio_actual
;

/*UPDATE REFERENCES TO sf*/
UPDATE        tbl_bi_ops_pc_sample AS c
  INNER JOIN @development@.tbl_sku_shipping_fees sf 
          ON sf.sku = c.sku
SET
   c.pc_15 = c.pc_15 + IF(c.eligible_free_shipping = 1,0,IF (sf.shipping_fee IS NULL,0,sf.shipping_fee)) / 
                         (1 +(c.tax_percent / 100));

UPDATE         tbl_bi_ops_pc_sample AS c
   INNER JOIN @development@.tbl_sku_shipping_cost shc 
           ON shc.skusimple = c.sku
SET 
   c.pc_15 = c.pc_15 -IF(shc.shipping_cost IS NULL,0,shc.shipping_cost);


UPDATE         tbl_bi_ops_pc_sample AS c
SET
   c.pc_15_percentage = c.pc_15 / c.precio_actual
;
ALTER TABLE  tbl_bi_ops_pc_sample 
   DROP COLUMN tax_percent , 
   DROP COLUMN delivery_cost_supplier ,
   DROP COLUMN eligible_free_shipping ,
   DROP COLUMN precio_actual
;

INSERT INTO  tbl_bi_ops_pc 
SELECT * FROM  tbl_bi_ops_pc_sample; 

UPDATE  out_stock_hist 
INNER JOIN  tbl_bi_ops_pc 
	ON out_stock_hist.sku_simple = tbl_bi_ops_pc.sku
SET out_stock_hist.pc_15 = tbl_bi_ops_pc.pc_15,
out_stock_hist.pc_15_percentage = tbl_bi_ops_pc.pc_15_percentage;


DROP TEMPORARY TABLE IF EXISTS TMP_Stock;
CREATE TEMPORARY TABLE TMP_Stock ( PRIMARY KEY (Sku_Simple) ) 
SELECT
  Sku_Simple,
  #MAX( Fecha_Event)as Fecha,
  SUM( in_Stock ) AS Stock,
  SUM( ifnull( items_procured_in_transit,0) ) items_pending
FROM
    out_stock_hist
GROUP BY 
  Sku_Simple
;

DROP TABLE IF EXISTS SKU_Inventory_Hist;
CREATE TABLE SKU_Inventory_Hist ( PRIMARY KEY (  sku_simple, Fecha_Event, In_Out )  )
SELECT
	Out_Hist.sku_simple,
	Out_Hist.sku_config,
  Out_Hist.date_entrance as Fecha_Event,

  count(Out_Hist.date_entrance) entries,
	sum( Out_Hist.cost_w_o_vat  )  costo,
	AVG( Out_Hist.cost_w_o_vat  ) * count(date_entrance) costo_entries,
  " In" AS In_Out
FROM
              out_stock_hist AS Out_Hist
  INNER JOIN TMP_Stock
       USING (  SKU_Simple )
GROUP BY
  date_entrance,
  sku_simple
;

REPLACE SKU_Inventory_Hist
SELECT
   Out_Hist.sku_simple,
	 Out_Hist.sku_config,
 	 Out_Hist.date_exit,

   count(Out_Hist.date_exit) as entries,
	sum( Out_Hist.cost_w_o_vat  )  costo,
	AVG( COALESCE( Out_Hist.cost_w_o_vat, 0) ) * count(Out_Hist.date_exit) costo_entries,
 	 'Out' AS In_Out
FROM
              out_stock_hist AS Out_Hist
  INNER JOIN TMP_Stock
       USING (  SKU_Simple )
WHERE date_exit is not null
GROUP BY
   date_exit,
   sku_simple
;


DROP  TABLE IF EXISTS Daily_Stock_Flow;
CREATE  TABLE Daily_Stock_Flow ( PRIMARY KEY ( SKU_Simple, Fecha_Event ) )
SELECT 
   Fecha_Event,
   sku_simple,
	 sku_config,

   0 AS Stock,
   SUM( IF( In_Out = " In"  , Entries , 0 )) AS Entradas,
   SUM( IF( In_Out = "Out" , Entries , 0 )) AS Salidas,
   SUM( IF( In_Out = " In"  , costo   , 0 ))   AS Costo_Entradas,
   SUM( IF( In_Out = " In" , costo_entries , 0 ))   AS Costo_PerDay_Entradas,

   SUM( IF( In_Out = "Out"  , costo   , 0 ))   AS Costo_Salidas,
   SUM( IF( In_Out = "Out" , costo_entries , 0 ))   AS Costo_PerDay_Salidas,
   0   AS Items_Pending
,In_Out
FROM SKU_Inventory_Hist
GROUP BY 
   Fecha_Event, 
   SKU_Simple
;
UPDATE        Daily_Stock_Flow
   INNER JOIN TMP_Stock
       ON     Daily_Stock_Flow.SKU_Simple  = TMP_Stock.SKU_Simple
SET
   Daily_Stock_Flow.Stock         = TMP_Stock.Stock,
   Daily_Stock_Flow.Items_Pending = TMP_Stock.Items_Pending
;  

DROP TABLE IF EXISTS @production@.items_procured_in_transit;
CREATE TABLE @production@.items_procured_in_transit LIKE  items_procured_in_transit;

INSERT INTO @production@.items_procured_in_transit SELECT * FROM  items_procured_in_transit; 
DROP TABLE IF EXISTS @production@.pro_sell_list_hist_totals;

CREATE TABLE @production@.pro_sell_list_hist_totals LIKE  pro_sell_list_hist_totals;
INSERT INTO @production@.pro_sell_list_hist_totals SELECT * FROM  pro_sell_list_hist_totals;

DROP TABLE IF EXISTS @production@.top_skus_sell_list;
CREATE TABLE @production@.top_skus_sell_list LIKE  top_skus_sell_list;

INSERT INTO @production@.top_skus_sell_list SELECT * FROM  top_skus_sell_list;
#INSERT INTO `production`.`table_monitoring_report` VALUES (NULL, 'Daily Stock - CO', NOW());
