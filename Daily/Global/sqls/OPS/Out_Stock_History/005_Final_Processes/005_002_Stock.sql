SET @MonthNum = "000000";
SELECT date_format( (SELECT max(date_entrance) FROM out_stock_hist WHERE date_entrance < curdate()) ,"%Y%m" )  INTO @MonthNum;


CREATE TABLE IF NOT EXISTS A_Stock
(
   MonthNum int,
   stock_item_id int,
   SKU_Simple  varchar(25),
   SKU_Config  varchar(25),
   fulfillment_type_real  varchar(100),
   Days_inStock  int,
   Visibles      int,
   Reserved      int,
   CostAfterTax decimal(15,2),
   UpdatedAt  datetime,
   PRIMARY KEY ( MonthNum, stock_item_id ),
   KEY( SKU_Simple )
)
;



REPLACE A_Stock
SELECT
  @MonthNum   AS MonthNum,
  stock_item_id AS stock_item_id,
  SKU_Simple  AS SKU_Simple,
  SKU_Config  AS SKU_Config,
  fulfillment_type_real,
  Days_in_stock ,
  Visible,
	reserved,
  cost_w_o_vat, 
  now()       AS UpdatedAt
FROM
out_stock_hist 
WHERE
  in_stock  = 1
;