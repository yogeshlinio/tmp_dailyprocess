
UPDATE Out_Stock_Hist_Sample
SET
    sold_last_30_cost_w_o_vat = IF( sold_last_30	= 1, cost_w_o_vat , 0 ),
    sold_last_30_price        = IF( sold_last_30	= 1, price        , 0 ),

    sold_last_30_order_cost_w_o_vat = IF( sold_last_30_order	= 1, cost_w_o_vat , 0 ),
    sold_last_30_order_price        = IF( sold_last_30_order	= 1, price        , 0 ),

    sold_last_7_order_cost_w_o_vat = IF( sold_last_7_order	= 1, cost_w_o_vat , 0 ),

	sold_last_15_order_cost_w_o_vat = IF( sold_last_15_order	= 1, cost_w_o_vat , 0 )
	
;