UPDATE             Out_Stock_Hist_Sample opt
  		INNER JOIN development_mx.A_E_BI_ExchangeRate_USD er 
		        ON date_format( date_entrance , '%Y%m') = er.Month_Num 
set 
	opt.cost_w_o_vat_usd = opt.cost_w_o_vat * er.XR
where
    er.Country = SUBSTR( '@v_country@' FROM 1 FOR  3 ) ;

UPDATE             Out_Stock_Hist_Sample opt
  		INNER JOIN development_mx.A_E_BI_ExchangeRate er 
		        ON date_format( date_entrance , '%Y%m') = er.Month_Num 
set 
	opt.cost_w_o_vat_eur = opt.cost_w_o_vat * er.XR
where
    er.Country = SUBSTR( '@v_country@' FROM 1 FOR  3 ) ;
	
	