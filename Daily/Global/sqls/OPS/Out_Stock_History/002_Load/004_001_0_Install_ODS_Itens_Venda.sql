call  production.monitoring_step( 'ODS.itens_venda_id' ,  "@v_country@", "finish" , @v_times@ );

DROP TEMPORARY TABLE IF EXISTS Itens_Stock_@v_countryPrefix@;
CREATE TEMPORARY TABLE Itens_Stock_@v_countryPrefix@ ( PRIMARY KEY ( item_id ) , KEY ( stock_item_id ) )
ENGINE=InnoDB
SELECT  
   item_id,
   0 as stock_item_id,
   order_number as order_nr
   
FROM
   ODS.itens_venda_oot_sample_@v_countryPrefix@
;

UPDATE           Itens_Stock_@v_countryPrefix@ AS Procurement
      INNER JOIN ODS.itens_venda_oot_key_map_Sample_@v_countryPrefix@ AS Key_Map
	      USING ( item_id )
SET
	Procurement.stock_item_id = Key_Map.estoque_id
;	
DELETE FROM Itens_Stock_@v_countryPrefix@ WHERE stock_item_id = 0;
   
UPDATE            Out_Stock_Hist_Sample                AS Out_Stock_Hist
       INNER JOIN Itens_Stock_@v_countryPrefix@  AS ODS
	        USING ( stock_item_id )
SET
  Out_Stock_Hist.item_id  = ODS.item_id , 
  Out_Stock_Hist.order_nr = ODS.order_nr
;
										   
										   
					