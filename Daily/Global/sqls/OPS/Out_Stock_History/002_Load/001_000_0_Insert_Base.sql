call  production.monitoring_step( 'ODS.Stock_Item_Id' , "@v_country@", "finish" , @v_times@ );

DROP TABLE IF EXISTS Out_Stock_Hist_Sample;
CREATE TABLE  Out_Stock_Hist_Sample LIKE Out_Stock_Hist_Template;
INSERT  Out_Stock_Hist_Sample 
(
    Country	,
    stock_item_id	,
    barcode_wms	,
	barcode_bob_duplicated,
    sku_config	,
    sku_simple	,
    date_entrance	,
	week_entrance,
	
    date_exit	,
	week_exit,
    exit_type	,
    days_in_stock	,
    cost	,
    cost_w_o_vat	,
    in_stock	,
    sold_yesterday	,
	
    sold_last_7	,
    sold_last_30	,
    sold_last_60	,
	
    sold_last_7_order,
    sold_last_15_order,
    sold_last_30_order,	
	
    wh_location	,
    wh_location_before_sold	,
    sub_location,
    fulfillment_type_real,
	
    fulfillment_type_WMS,
    fulfillment_type_bp,  	
	
    reserved	,
    quarantine	,
    quarantine_type	,
    quarantine_date	,
    quarantine_inbound	,
    position_type	,
    position_item_size_type	,
    sold	,
    max_days_in_stock	,
    entrance_last_30	,
    is_sell_list	,
    is_sell_list_30	,
    date_ordered	,
    status_sku	,
    reason_not_visible	,
    status_item	,
    cancelled	,
    sku_simple_blank	,
    sales_rate_config_42	,
    sales_rate_config_30	,
    sales_rate_config_7	,
    pc_15	,
    pc_15_percentage,
    in_stock_cost_w_o_vat,
	item_counter
)
SELECT
    "@v_country@" AS Country	,
    stock_item_id	,
    barcode_wms	,
	barcode_bob_duplicated,
    sku_config	,
    sku_simple	,
    date_entrance	,
	@operations@.week_iso(date_entrance),
    date_exit	,
	@operations@.week_iso(date_entrance	),
    exit_type	,
    days_in_stock	,
    cost	,
    cost_w_o_vat	,
    in_stock	,
    sold_yesterday	,
	
    sold_last_7	,
    sold_last_30	,
    sold_last_60	,
	
    sold_last_7_order,
	
	
    sold_last_15_order,
    sold_last_30_order,	
	
    wh_location	,
    wh_location_before_sold	,
	sub_location,
	IF( fulfillment_type_WMS = "" , "Own Warehouse" , fulfillment_type_WMS ),
	IF( fulfillment_type_WMS = "" , "Own Warehouse" , fulfillment_type_WMS ),
    fulfillment_type_bp,	
    reserved	,
    quarantine	,
    quarantine_type	,
    quarantine_date	,
    quarantine_inbound	,
    position_type	,
    position_item_size_type	,
    sold	,
    max_days_in_stock	,
    entrance_last_30	,
    is_sell_list	,
    is_sell_list_30	,
    date_ordered	,
    status_sku	,
    reason_not_visible	,
    status_item	,
    cancelled	,
    sku_simple_blank	,
	
    sales_rate_config_42	,
    sales_rate_config_30	,
    sales_rate_config_7	,
    pc_15	,
    pc_15_percentage,
	IF(in_stock = 1,cost_w_o_vat, 0   ) AS in_stock_cost_w_o_vat,
	1 as item_counter
FROM
   ODS.Stock_Item_Id_Sample_@v_countryPrefix@
;
