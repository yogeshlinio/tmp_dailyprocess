call  production.monitoring_step( 'ODS.Id_Procurement_Order_Item',  "@v_country@", "finish" , @v_times@ );
DROP TEMPORARY TABLE IF EXISTS Procurement_Stock_@v_countryPrefix@;
CREATE TEMPORARY TABLE Procurement_Stock_@v_countryPrefix@ ( PRIMARY KEY  ( id_procurement_order_item ) , KEY ( stock_item_id ) )
SELECT  
   id_procurement_order_item, 
   0 as stock_item_id,
   purchase_order,
   purchase_order_type
FROM
   ODS.Id_Procurement_Order_Item_Sample_@v_countryPrefix@
;
UPDATE           Procurement_Stock_@v_countryPrefix@ AS Procurement
      INNER JOIN ODS.Id_Procurement_Order_Item_Key_Map_Sample_@v_countryPrefix@ AS Key_Map
	      USING ( id_procurement_order_item )
SET
	Procurement.stock_item_id = Key_Map.estoque_id
;	
DELETE FROM Procurement_Stock_@v_countryPrefix@ WHERE stock_item_id = 0;

UPDATE            Out_Stock_Hist_Sample                AS Out_Stock_Hist
       INNER JOIN Procurement_Stock_@v_countryPrefix@  AS ODS
	        USING ( stock_item_id )
SET
  Out_Stock_Hist.purchase_order      = ODS.purchase_order , 
  Out_Stock_Hist.purchase_order_type = ODS.purchase_order_type
;