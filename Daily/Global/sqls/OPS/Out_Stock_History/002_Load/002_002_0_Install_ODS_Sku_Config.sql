call  production.monitoring_step( "ODS.Sku_simple", "@v_country@", "finish" , @v_times@ );

DROP TEMPORARY TABLE IF EXISTS Sku_Config_@v_countryPrefix@;
CREATE TEMPORARY TABLE Sku_Config_@v_countryPrefix@
( PRIMARY KEY (sku_config  ) )
SELECT
   sku_config,
   avg( average_days_in_stock_WMS )  as  max_days_in_stock
   
FROM   
   ODS.Sku_Simple_Sample_@v_countryPrefix@
WHERE   
stock_wms > 0    
GROUP BY sku_config   
;
UPDATE            Out_Stock_Hist_Sample                   AS Out_Stock_Hist
       INNER JOIN Sku_Config_@v_countryPrefix@ AS ODS
	        USING ( sku_config )
SET
     Out_Stock_Hist.max_days_in_stock = ODS.max_days_in_stock
  
;                                           