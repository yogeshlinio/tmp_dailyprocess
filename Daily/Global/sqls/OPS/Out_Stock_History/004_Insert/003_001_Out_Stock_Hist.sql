DROP TABLE IF EXISTS out_stock_hist;
CREATE TABLE out_stock_hist LIKE Out_Stock_Hist_Template;
REPLACE out_stock_hist SELECT * FROM Out_Stock_Hist_Sample;

#DROP TABLE IF EXISTS A_Master_Catalog_Sample_Backup;
#CREATE TABLE A_Master_Catalog_Sample_Backup LIKE A_Master_Catalog_Template;
#INSERT A_Master_Catalog_Sample_Backup SELECT * FROM A_Master_Catalog;
