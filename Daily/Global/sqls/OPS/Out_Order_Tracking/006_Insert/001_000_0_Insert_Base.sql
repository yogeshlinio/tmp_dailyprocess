call  production.monitoring_step( "ODS.itens_venda_id_key_map", "@v_country@", "finish" , @v_times@ );

DROP TABLE IF EXISTS operations_@v_countryPrefix@.out_order_tracking_sample_tst;

CREATE TABLE operations_@v_countryPrefix@.out_order_tracking_sample_tst LIKE operations_@v_countryPrefix@.out_order_tracking_template;
INSERT  operations_@v_countryPrefix@.out_order_tracking_sample_tst
(
   `item_id`,
   `order_number`,
   `order_id`
)
SELECT
   a.item_id,
   a.numero_order,
   a.order_id
FROM
   ODS.itens_venda_oot_key_map_Sample_@v_countryPrefix@ a
JOIN ODS.itens_venda_oot_sample_@v_countryPrefix@ b
ON a.item_id=b.item_id
;