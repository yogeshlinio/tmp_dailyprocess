INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@',
  'out_procurement_tracking',
  'finish',
  NOW(),
  NOW(),
  count(1),
  count(1)
FROM
operations_@v_countryPrefix@.out_procurement_tracking_sample_tst
;