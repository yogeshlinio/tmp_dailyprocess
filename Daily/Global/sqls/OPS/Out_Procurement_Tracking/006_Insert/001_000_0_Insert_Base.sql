call  production.monitoring_step( "ODS.Id_Procurement_Order_Item_Key_Map", "@v_country@", "finish" , @v_times@ );

DROP TABLE IF EXISTS operations_@v_countryPrefix@.out_procurement_tracking_sample_tst;

CREATE TABLE operations_@v_countryPrefix@.out_procurement_tracking_sample_tst LIKE operations_@v_countryPrefix@.out_procurement_tracking_template;
INSERT  operations_@v_countryPrefix@.out_procurement_tracking_sample_tst
(
   `id_procurement_order_item`,
   `id_procurement_order`,
   `id_catalog_simple`
)
SELECT
   a.id_procurement_order_item,
   a.id_procurement_order,
   a.id_catalog_simple
FROM
   ODS.Id_Procurement_Order_Item_Key_Map_Sample_@v_countryPrefix@ a
JOIN ODS.Id_Procurement_Order_Item_Sample_@v_countryPrefix@ b
USING (id_procurement_order_item)
;