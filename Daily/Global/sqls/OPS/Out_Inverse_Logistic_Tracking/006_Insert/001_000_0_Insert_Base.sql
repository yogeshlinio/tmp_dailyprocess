call  production.monitoring_step( "ODS.itens_venda_id_key_map", "@v_country@", "finish" , @v_times@ );

DROP TABLE IF EXISTS operations_@v_countryPrefix@.out_inverse_logistics_tracking_sample_tst;

CREATE TABLE operations_@v_countryPrefix@.out_inverse_logistics_tracking_sample_tst LIKE operations_@v_countryPrefix@.out_inverse_logistics_tracking_template;
INSERT  operations_@v_countryPrefix@.out_inverse_logistics_tracking_sample_tst
(
   `item_id`,
   `order_number`,
   `sku_simple`
)
SELECT
   item_id,
   numero_order,
   sku
FROM
   ODS.itens_venda_oot_key_map_Sample_@v_countryPrefix@
;