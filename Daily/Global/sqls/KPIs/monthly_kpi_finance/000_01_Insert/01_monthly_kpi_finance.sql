USE @operations@;

SELECT @MonthNum:=date_format( CURRENT_DATE() - INTERVAL 1 month ,"%Y%m" );

DROP TABLE IF EXISTS  monthly_kpi_finance;

CREATE TABLE IF NOT EXISTS  monthly_kpi_finance
(
   MonthNum int,
   stock_item_id int,
   SKU_Simple  varchar(25),
   SKU_Config  varchar(25),   
   CostAfterTax decimal(15,2),
   Fulfillment varchar(65),
   Brand varchar(65),
   UpdatedAt  datetime,
   PRIMARY KEY ( MonthNum, stock_item_id ),
   KEY( SKU_Simple )
)
;

DELETE FROM  monthly_kpi_finance WHERE @MonthNum = date_format( CURRENT_DATE() -INTERVAL 1 month ,"%Y%m" );

INSERT INTO  monthly_kpi_finance
SELECT
  @MonthNum   AS MonthNum,
  stock_item_id AS stock_item_id,
  sku_simple  AS SKU_Simple,
  sku_config  AS SKU_Config,
  cost_w_o_vat,
  fulfillment_type_bp, 
  brand,
  now()       AS UpdatedAt
FROM
  out_stock_hist 
WHERE
  in_stock = 1 and reserved = 0
;