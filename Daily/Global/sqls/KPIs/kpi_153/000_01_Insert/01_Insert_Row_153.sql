USE @development@;

DROP TABLE IF EXISTS TMP_SKUS_DISCOUNT_153;
CREATE TABLE TMP_SKUS_DISCOUNT_153
SELECT
 DISCOUNTS / DISTINCTS     AS Value
FROM(
SELECT 
        count(DISTINCT CONFIG) DISTINCTS
    FROM
        (SELECT 
        `A_Master_Catalog`.`sku_config` AS CONFIG,
            (`A_Master_Catalog`.`isvisible`) VISIBLE,
            `A_Master_Catalog`.`special_from_date`,
            `A_Master_Catalog`.`special_to_date`,
            IF((`A_Master_Catalog`.`special_to_date`) >= CAST(DATE_FORMAT(CURDATE() - INTERVAL 1 MONTH, '%Y-%m-01')
                as DATE), 1, 0)
    FROM
        `A_Master_Catalog`) R
    WHERE
        VISIBLE = 1) as T
        INNER JOIN
    (SELECT 
        count(DISTINCT CONFIG) as DISCOUNTS
    FROM
        (SELECT 
        `A_Master_Catalog`.`sku_config` AS CONFIG,
            (`A_Master_Catalog`.`isvisible`) VISIBLE,
            `A_Master_Catalog`.`special_from_date`,
            `A_Master_Catalog`.`special_to_date`,
            IF((`A_Master_Catalog`.`special_to_date`) >= CAST(DATE_FORMAT(CURDATE() - INTERVAL 1 MONTH, '%Y-%m-01')
                as DATE), 1, 0) DISC
    FROM
        `A_Master_Catalog`) R
    WHERE
        VISIBLE = 1 AND DISC = 1) t