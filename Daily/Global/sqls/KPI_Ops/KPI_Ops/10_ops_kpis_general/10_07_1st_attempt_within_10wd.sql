-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 10_07_1st_attempt_within_10wd
-- Created by: Rafael Guzman
-- Created date: 2014-05-28
-- Updated date: 2014-05-28
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'10. General' AS 'group',
	1007 AS id,
	DATE_FORMAT(date_delivered,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_delivered) AS WeekNum,
	date_delivered date,
	'% 1st Attempt within 10 Workdays' AS kpi,
	'# 1st attempt items <= 10 WD / # items delivered' AS formula,
	fulfillment_type_real AS breakdown_1,
	package_measure_new AS breakdown_2,
	count(*) AS items,
	sum(IF(workdays_total_1st_attempt <=10,1,0)) AS items_sec,
	sum(IF(workdays_total_1st_attempt <=10,1,0))/count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		status_wms IN (
					'Aguardando estoque',
					'Analisando quebra',
					'Estoque reservado',
					'Separando',
					'Aguardando separacao',
					'Faturado',
					'Aguardando expedicao',
					'Expedido',
					'DS estoque reservado',
					'dropshipping notified',
					'backorder',
					'exportado',
					'Waiting dropshipping',
          'handled_by_marketplace',
          'packed_by_marketplace'
	 )
AND		date_delivered BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY date_delivered, fulfillment_type_real, package_measure_new;