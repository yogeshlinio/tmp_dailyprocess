-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 10_04_on_time_delivery
-- Created by: Rafael Guzman
-- Created date: 2014-05-28
-- Updated date: 2014-05-28
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'10. General' AS 'group',
	1004 AS id,
	DATE_FORMAT(date_delivered,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_delivered) AS WeekNum,
	date_delivered_promised date,
	'% On Time Delivery' AS kpi,
	'# Items delivered within promise / Total items delivered promised' AS formula,
	fulfillment_type_real AS breakdown_1,
	'' AS breakdown_2,
	count(*) AS items,
	sum(on_time_total_delivery) AS items_sec,
	sum(on_time_total_delivery)/count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		status_wms IN (
					'Aguardando estoque',
					'Analisando quebra',
					'Estoque reservado',
					'Separando',
					'Aguardando separacao',
					'Faturado',
					'Aguardando expedicao',
					'Expedido',
					'DS estoque reservado',
					'dropshipping notified',
					'backorder',
					'exportado',
					'Waiting dropshipping',
          'handled_by_marketplace',
          'packed_by_marketplace'
	 )
AND		date_delivered_promised BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY date_delivered_promised, fulfillment_type_real;