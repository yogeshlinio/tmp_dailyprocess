-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 10_01_nps_score
-- Created by: Rafael Guzman
-- Created date: 2014-05-28
-- Updated date: 2014-05-28
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'10. General' AS 'group',
	1001 AS id,
	DATE_FORMAT(DateSubmitted,'%Y%m') AS MonthNum,
	@operations@.week_iso(DateSubmitted) AS WeekNum,
	DateSubmitted AS date,
	'NPS Score' AS kpi,
	'( % Promoters - % Detractors ) / Mono item Surveys' AS formula,
	'' AS breakdown_1,
	'' AS breakdown_2,
	Count(DISTINCT ResponseID) AS items,
	(COUNT(DISTINCT (IF(Loyalty = 'promoter',ResponseID,0)))/Count(DISTINCT ResponseID))*100 AS items_sec,
	(COUNT(DISTINCT (IF(Loyalty = 'promoter',ResponseID,0)))/Count(DISTINCT ResponseID) - COUNT(DISTINCT (IF(Loyalty = 'detractor',ResponseID,0)))/Count(DISTINCT ResponseID))*100 AS value,
	Now() AS updated_at
FROM nps.NPSData@v_countryPRE@
WHERE DateSubmitted BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
#AND isMonoItem = 1
GROUP BY DateSubmitted
ORDER BY DateSubmitted DESC;