-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 10_08_conversion_rate_gross_to_delivered
-- Created by: Rafael Guzman
-- Created date: 2014-05-28
-- Updated date: 2014-05-28
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'10. General' AS 'group',
	1008 AS id,
	DATE_FORMAT(Date,'%Y%m') AS MonthNum,
	@operations@.week_iso(Date) AS WeekNum,
	Date AS date,
	'Conversion Rate Gross to Delivered' AS kpi,
	'# Items Delivered not Returned / # Gross Items' AS formula,
	PaymentMethod AS breakdown_1,
	FulfillmentTypeReal AS breakdown_2,
	SUM(OrderBeforeCan) AS items,
	SUM(Delivered-Returns) AS items_sec,
	SUM(Delivered-Returns)/ SUM(OrderBeforeCan) AS value,
	now() AS updated_at
FROM @development@.A_Master
WHERE Date BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY Date, PaymentMethod, FulfillmentTypeReal;