-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 01_05_manual_fraud_check_rejection_rate
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'1. Payments' AS 'group',
	0105 AS id,
	DATE_FORMAT(date_ordered,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_ordered) AS WeekNum,
	date_ordered AS date,
	'Manual Fraud Check Rejection Rate - Credit/Debit Cards' AS kpi,
	'# Refund Needed / Gross Orders' AS formula,
	PaymentMethod AS breakdown_1,
	'' AS breakdown_2,
	sum(placed_orders) AS items,
	sum(manual_fraud_rejected_orders) AS items_secondary,
	sum(manual_fraud_rejected_orders)/SUM(placed_orders) AS value,
	now() AS updated_at
FROM @operations@.order_flow_totals
WHERE date_ordered BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND PaymentMethod IN (	'Amex_Gateway',
						'Banorte_Payworks',
						'Banorte_Payworks_Debit')
GROUP BY date_ordered, PaymentMethod;