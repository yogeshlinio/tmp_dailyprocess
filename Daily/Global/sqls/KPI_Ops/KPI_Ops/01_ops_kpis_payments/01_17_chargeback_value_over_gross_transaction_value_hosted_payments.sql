-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 01_17_chargeback_value_over_gross_transaction_value_hosted_payments
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'1. Payments' AS 'group',
	0117 AS id,
	DATE_FORMAT(DateOrderPlaced + INTERVAL 2 MONTH,'%Y%m') AS MonthNum,
	@operations@.week_iso(DateOrderPlaced + INTERVAL 2 MONTH) AS WeekNum,
	Date(DateOrderPlaced) + INTERVAL 2 MONTH AS date,
	'% Chargeback Value / Gross Transaction Value - Hosted Payment (i.e. Paypal)' AS kpi,
	'# Orders Paid within 24h / # Orders Paid' AS formula,
	PaymentMethod AS breakdown_1,
	'' AS breakdown_2,
	SUM(GrandTotal) AS items,
	SUM(IF(Chargeback=1,GrandTotal,0)) AS items_sec,
	SUM(IF(Chargeback=1,GrandTotal,0))/SUM(GrandTotal) AS rate_resolved_manual_under_4h,
	now() AS updated_at
FROM customer_service.tbl_fraud_check_report
WHERE DateOrderPlaced BETWEEN curdate() - INTERVAL @interval MONTH + INTERVAL 2 MONTH  AND curdate()
AND PaymentMethod IN ('Paypal_Express_Checkout')
GROUP BY Date(DateOrderPlaced), PaymentMethod;