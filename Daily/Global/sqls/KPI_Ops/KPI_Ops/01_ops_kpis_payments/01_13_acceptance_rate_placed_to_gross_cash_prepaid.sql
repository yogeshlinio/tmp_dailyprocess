-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 01_13_acceptance_rate_placed_to_gross
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'1. Payments' AS 'group',
	0113 AS id,
	DATE_FORMAT(date_ordered,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_ordered) AS WeekNum,	
	date_ordered AS date,
	'Acceptance Rate Placed to Gross - Retail / Bank Deposits' AS kpi,
	'# Gross Orders / # Placed' AS formula,
	PaymentMethod AS breakdown_1,
	'' AS breakdown_2,
	sum(placed_orders) AS items,
	sum(gross_orders) AS items_sec,
	sum(gross_orders)/SUM(placed_orders) AS value,
	now() AS updated_at
FROM @operations@.order_flow_totals
WHERE date_ordered BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND PaymentMethod IN ('Banorte_PagoReferenciado',
											'Oxxo_Direct')
GROUP BY date_ordered, PaymentMethod;