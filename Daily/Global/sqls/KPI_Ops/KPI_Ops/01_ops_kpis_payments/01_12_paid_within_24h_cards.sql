-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 01_12_paid_within_24h_cards
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'1. Payments' AS 'group',
	0112 AS id,
	DATE_FORMAT(date_exported,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_exported) AS WeekNum,
	date_exported AS date,
	'% Paid within 24 h - Credit/Debit Cards' AS kpi,
	'# Orders Paid within 24h / # Orders Paid' AS formula,
	payment_method AS breakdown_1,
	'' AS breakdown_2,
	Count(DISTINCT order_number) AS items,
	COUNT(DISTINCT (IF(time_to_export <= 24,order_number,0))),
	COUNT(DISTINCT (IF(time_to_export <= 24,order_number,0)))/Count(DISTINCT order_number),
	Now() AS updated_at
FROM @operations@.out_order_tracking
WHERE date_exported BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND payment_method IN (	'Amex_Gateway',
						'Banorte_Payworks',
						'Banorte_Payworks_Debit')
GROUP BY date_exported, payment_method;