-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 01_09_manual_fraud_check_over_bank_acceptance_transactions
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'1. Payments' AS 'group',
	0109 AS id,
	DATE_FORMAT(date_ordered,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_ordered) AS WeekNum,
	date_ordered AS date,
	'% Manual Fraud Check / Bank Acceptance Transactions - Credit/Debit Cards' AS kpi,
	'# Manual Fraud Check Pending / Payment Pending Orders' AS formula,
	PaymentMethod AS breakdown_1,
	'' AS breakdown_2,
	sum(bank_accepted_orders) AS items,
	sum(manual_fraud_accepted_orders) AS items_sec,
	sum(manual_fraud_accepted_orders)/SUM(bank_accepted_orders) AS value,
	now() AS updated_at
FROM @operations@.order_flow_totals
WHERE date_ordered BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND PaymentMethod IN (	'Amex_Gateway',
						'Banorte_Payworks',
						'Banorte_Payworks_Debit')
GROUP BY date_ordered, PaymentMethod;
