-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 01_01_create table
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

DROP TABLE IF EXISTS @operations@.tbl_order_flow;
CREATE TABLE @operations@.tbl_order_flow (INDEX (OrderNum))
SELECT
	Date AS date_ordered,
	PaymentMethod,
	OrderNum,
	GrandTotal,
	ItemsInOrder,
	1 AS is_new,
	if(OrderBeforeCan = 0,1,0) AS is_invalid,
	if(OrderBeforeCan = 1,1,0) AS is_gross,
	if(OrderAfterCan = 1,1,0) AS is_net#,
#	0 AS is_auto_fraud_check_pending,
#	0 AS is_payment_pending,
#	0 AS is_manual_fraud_check_pending,
#	0 AS is_refund_needed,
#	0 AS is_exportable,
#	0 AS is_system_rejection,
#	0 AS is_system_acceptance,
#	0 AS is_auto_fraud_rejection,
#	0 AS is_auto_fraud_acceptance,
#	0 AS is_bank_rejection,
#	0 AS is_bank_acceptance,
#	0 AS is_manual_fraud_rejection,
#	0 AS is_manual_fraud_acceptance
FROM @development@.A_Master
GROUP BY OrderNum;
