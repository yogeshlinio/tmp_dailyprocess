-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 01_90_rejection_acceptance_flow
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
/*
UPDATE @operations@.tbl_order_flow
SET
	is_system_rejection = if(is_new = 1 AND is_auto_fraud_check_pending = 0 AND is_invalid = 1,1,0),
	is_system_acceptance = if(is_new = 1 AND is_auto_fraud_check_pending = 1 AND is_invalid = 0,1,0),
	is_auto_fraud_rejection = if(is_auto_fraud_check_pending = 1 AND is_payment_pending = 0 AND is_invalid = 1,1,0),
	is_auto_fraud_acceptance = if(is_auto_fraud_check_pending = 1 AND is_payment_pending = 1 AND is_invalid = 0,1,0),
	is_bank_rejection = if(is_payment_pending = 1 AND is_manual_fraud_check_pending = 0 AND is_invalid = 1,1,0),
	is_bank_acceptance = if(is_payment_pending = 1 AND is_manual_fraud_check_pending = 1 AND is_invalid = 0,1,0),
	is_manual_fraud_rejection = if(is_manual_fraud_check_pending = 1 AND is_exportable = 0 AND is_refund_needed = 1,1,0),
	is_manual_fraud_acceptance = if(is_manual_fraud_check_pending = 1 AND is_exportable = 1,1,0)
WHERE PaymentMethod IN ('Banorte_Payworks_Debit','Banorte_Payworks','Amex_Gateway');*/