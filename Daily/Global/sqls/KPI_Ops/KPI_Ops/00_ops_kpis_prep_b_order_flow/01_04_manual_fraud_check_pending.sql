-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 01_04_manual_fraud_check_pending
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

/*
DROP TEMPORARY TABLE IF EXISTS @operations@.tmp_manual_fraud_check_pending;
CREATE TEMPORARY TABLE @operations@.tmp_manual_fraud_check_pending (INDEX (OrderNum))
SELECT
	DATE(a.created_at) AS date_ordered,
	a.order_nr AS OrderNum,
	1 AS is_manual_fraud_check_pending
FROM @bob_live@.sales_order a
INNER JOIN @bob_live@.sales_order_item b
	ON a.id_sales_order = b.fk_sales_order
INNER JOIN @bob_live@.sales_order_item_status_history c
	ON b.id_sales_order_item = c.fk_sales_order_item
WHERE c.fk_sales_order_item_status = 90
GROUP BY a.order_nr;

UPDATE @operations@.tbl_order_flow a
INNER JOIN @operations@.tmp_manual_fraud_check_pending b
	ON a.OrderNum = b.OrderNum
SET a.is_manual_fraud_check_pending = b.is_manual_fraud_check_pending;
*/