/*-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 06_08_abandone_rate_non_operative_schedule_phone
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'6. Customer Service' AS 'group',
	0608 AS id,
	DATE_FORMAT(calldate,'%Y%m') AS MonthNum,
	@operations@.week_iso(calldate) AS WeekNum,
	date(calldate) as date,
	'% Abandone Rate in Non Operating Schedule - phone' AS kpi,
	'# Calls & Chats abandoned / # Calls & Chats received' AS formula,
	'' AS breakdown_1,
	'' AS breakdown_2,
	sum(if(dcontext in ('linio-info','linio-maint'),1,0)) AS items,
	sum(if(HOUR(calldate)not in (08,09,10,11,12,13,14,15,16,17,18,19,20,21,22) AND 
	WEEKDAY(calldate) in (0,1,2,3,4),1,if(HOUR(calldate)not in 
	(09,10,11,12,13,14,15,16,17,18,19) AND 
	WEEKDAY(calldate)=5,1,if(HOUR(calldate)not in 
	(10,11,12,13,14,15,16,17,18) AND 
	WEEKDAY(calldate)=6,1,0)))) AS items_sec,
	sum(if(HOUR(calldate)not in (08,09,10,11,12,13,14,15,16,17,18,19,20,21,22) AND 
	WEEKDAY(calldate) in (0,1,2,3,4),1,if(HOUR(calldate)not in 
	(09,10,11,12,13,14,15,16,17,18,19) AND 
	WEEKDAY(calldate)=5,1,if(HOUR(calldate)not in 
	(10,11,12,13,14,15,16,17,18) AND 
	WEEKDAY(calldate)=6,1,0)))) / sum(if(dcontext in ('linio-info','linio-maint'),1,0)) AS value,
	now() AS updated_at
FROM customer_service.cdr
WHERE date(calldate) BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY date(calldate)
ORDER BY date(calldate) DESC
;*/