-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 06_11_chat_service_level
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'6. Customer Service' AS 'group', 
	0611 AS id,
	DATE_FORMAT(Fecha,'%Y%m') AS MonthNum,
	@operations@.week_iso(Fecha) AS WeekNum,
	Fecha as date,
	'Service Level - Chat answer < 20sec' AS kpi,
	'# Touchpoints answered on target / # Touchpoints answered' AS formula,
	hour(Franja) AS breakdown_1,
	operator_nickname AS breakdown_2,
	sum(Recibido) AS items,
	sum(ChatAtn20) AS items_sec,
	sum(ChatAtn20) / sum(Recibido) AS value,
	now() AS updated_at
FROM customer_service.rep_chats_agt_regional
WHERE 	Fecha BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
and 	pais = UPPER('@v_countryPrefix@')
GROUP BY Fecha, hour(Franja),operator_nickname ;