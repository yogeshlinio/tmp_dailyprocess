-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 06_06_abandone_rate_operative_schedule_phone
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
 UPPER('@v_countryPrefix@') AS country,
 '6. Customer Service' AS 'group',
 0606 AS id,
 DATE_FORMAT(Fecha,'%Y%m') AS MonthNum,
 @operations@.week_iso(Fecha) AS WeekNum,
 Fecha AS date,
 '% Abandone Rate in Operating Schedule - Phone' AS kpi,
 '# Calls & Chats abandoned / # Calls & Chats received' AS formula,
 '' AS breakdown_1,
 '' AS breakdown_2, 
 SUM(Recibidas) AS items,
 SUM(Abandonadas) AS items_sec,
 SUM(Abandonadas) /SUM(Recibidas) AS value,
 now() AS updated_at
FROM customer_service.rep_llamadas_agt_regional
WHERE 
 Fecha BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
and pais=UPPER('@v_countryPrefix@')
GROUP BY Fecha;