-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 06_14_oos_processed_within_24h
-- Created by: Rafael Guzman
-- Created date: 2014-06-03
-- Updated date: 2014-06-03
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'6. Customer Service' AS 'group',
	0614 AS id,
	DATE_FORMAT(date_declared_stockout,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_declared_stockout) AS WeekNum,
	date_declared_stockout as date,
	'% OOS processed within 24 hours' AS kpi,
	'# OOS Processed within target hours / # OOS Declared' AS formula,
	'' AS breakdown_1,
	'' AS breakdown_2,
	Count(*) AS items,
	SUM(IF(time_to_processed_stockout <=24,1,0)) AS items_sec,
	SUM(IF(time_to_processed_stockout <=24,1,0))/Count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_stockout = 1
AND date_declared_stockout BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY date_declared_stockout;