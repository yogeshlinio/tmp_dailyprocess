-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 06_10_phone_service_level
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
 UPPER('@v_countryPrefix@') AS country,
 '6. Customer Service' AS 'group',
 0610 AS id,
 DATE_FORMAT(Fecha,'%Y%m') AS MonthNum,
 @operations@.week_iso(Fecha) AS WeekNum,
 Fecha as date,
 '% Service Level - Phone answer within 20sec' AS kpi,
 '# Touchpoints answered on target / # Touchpoints answered' AS formula,
 DATE_FORMAT(Fecha,'%a')  AS breakdown_1,
 hour(Franja) AS breakdown_2,
 sum(Recibidas) AS items,
 sum(Atendidas20) AS items_sec,
 sum(Atendidas20) / sum(Recibidas) AS value,
 now() AS updated_at
FROM customer_service.rep_llamadas_agt_regional
WHERE Fecha BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
and pais=UPPER('@v_countryPrefix@')
GROUP BY Fecha,DATE_FORMAT(Fecha,'%a'), hour(Franja);