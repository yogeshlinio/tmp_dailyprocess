-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 06_15_bo_processed_within_24h
-- Created by: Rafael Guzman
-- Created date: 2014-06-03
-- Updated date: 2014-06-03
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'6. Customer Service' AS 'group',
	0614 AS id,
	DATE_FORMAT(date_declared_backorder,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_declared_backorder) AS WeekNum,
	date_declared_backorder as date,
	'% BO processed within 24 hours' AS kpi,
	'# BO Processed within target hours / # BO Declared' AS formula,
	'' AS breakdown_1,
	'' AS breakdown_2,
	Count(*) AS items,
	SUM(IF(time_to_processed_backorder <=24,1,0)) AS items_sec,
	SUM(IF(time_to_processed_backorder <=24,1,0))/Count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_backorder = 1
AND date_declared_backorder BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY date_declared_backorder;