-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 06_05_average_handling_time
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
 UPPER('@v_countryPrefix@') AS country,
 '6. Customer Service' AS 'group',
 0605 AS id,
 DATE_FORMAT(Fecha,'%Y%m') AS MonthNum,
 @operations@.week_iso(Fecha) AS WeekNum,
 Fecha AS date,
 'Average Handling Time' AS kpi,
 'Total time to resolve calls / # calls received' AS formula,
 DATE_FORMAT(Fecha,'%a') AS breakdown_1,
 hour(Franja) AS breakdown_2,
 SUM(Atendidas) AS items,
 SUM(Duracion) AS items_sec,
 SUM(Duracion)/SUM(Atendidas)  AS value,
 now() AS updated_at
FROM customer_service.rep_llamadas_agt_regional
where Fecha BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
and pais=UPPER('@v_countryPrefix@')
GROUP BY Fecha, DATE_FORMAT(Fecha,'%a'), hour(Franja);