-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 06_12_email_service_level
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'6. Customer Service' AS 'group',
	0612 AS id,
	DATE_FORMAT(Fecha,'%Y%m') AS MonthNum,
	@operations@.week_iso(Fecha) AS WeekNum,
	Fecha as date,
	'% Service Level - Email answer within 4 working hours' AS kpi,
	'# Touchpoints answered on target / # Touchpoints answered' AS formula,
	hour(Franja) AS breakdown_1,
	Assignee AS breakdown_2,
	sum(solved_tickets) AS items,
	sum(Resuelto_en_4_hrs) AS items_sec,
	sum(Resuelto_en_4_hrs) / sum(solved_tickets) AS value,
	now() AS updated_at
FROM customer_service.test_rep_agt_email_regional
WHERE Pais = UPPER('@v_countryPrefix@')
AND DATE(Fecha) BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY DATE(Fecha), hour(Franja), Assignee
;