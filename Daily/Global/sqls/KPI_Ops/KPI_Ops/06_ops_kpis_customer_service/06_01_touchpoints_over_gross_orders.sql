-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 06_01_touchpoints_over_gross_orders
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

-- -- -- -- Touchpoints Table -- -- -- -- 

DROP TEMPORARY TABLE IF EXISTS @operations@.tmp_contacts;
CREATE TEMPORARY TABLE @operations@.tmp_contacts
AS
SELECT 
	Date,
	channel,
	agent_name,
	SUM(Contact)as Contact
FROM
(
SELECT
 'Phone' AS channel,
 Agente AS agent_name,
 Fecha as Date,
 sum(Atendidas) as Contact
FROM
 customer_service.rep_llamadas_agt_regional
WHERE
 Fecha >= '2014-01-01'
and pais = UPPER('@v_countryPrefix@')
GROUP BY Fecha, Agente
-- UNION ALL
-- SELECT 
-- 	'Facebook' AS channel,
-- 	'' AS agent_name,
-- 	Fecha as Date,
-- 	`comment` as Contact 
-- FROM customer_service.tbl_facebook_comments 
-- GROUP BY Fecha, agent_name
UNION ALL
SELECT 
	'Chat' AS channel,
	operator_nickname AS agent_name,
	Fecha as Date,
	sum(Atendido) as Contact 
FROM customer_service.rep_chats_agt_regional
WHERE pais = UPPER('@v_countryPrefix@')
GROUP BY Fecha, operator_nickname
UNION ALL
SELECT 
 'Email' AS channel,
 Assignee AS agent_name,
 Fecha as Date,
 sum(New_Tickets)  as Contact 
FROM customer_service.test_rep_agt_email_regional 
WHERE 
Pais = UPPER('@v_countryPrefix@')
GROUP BY Fecha, Assignee
 )a
GROUP BY Date, channel, agent_name;

-- -- -- -- Orders Table -- -- -- -- 

DROP TEMPORARY TABLE IF EXISTS @operations@.tmp_gross_orders;
CREATE TEMPORARY TABLE @operations@.tmp_gross_orders (INDEX(Date))
SELECT
	Date,
	COUNT(DISTINCT OrderNum) AS orders
FROM @development@.A_Master
WHERE OrderBeforeCan = 1
GROUP BY Date
ORDER BY Date DESC;

-- -- -- --  KPI -- -- -- -- 

SET @interval = @v_interval@;

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'6. Customer Service' AS 'group',
	0601 AS id,
	DATE_FORMAT(a.date,'%Y%m') AS MonthNum,
	@operations@.week_iso(a.date) AS WeekNum,
	a.date AS date,
	'Touchpoints over Gross Orders' AS kpi,
	'# Touchpoints / Gross Orders' AS formula,
	channel AS breakdown_1,
	agent_name AS breakdown_2,
	AVG(b.orders) AS items,
	SUM(a.contact) AS items_sec,
	SUM(a.contact)/AVG(b.orders)  AS value,
	now() AS updated_at
FROM @operations@.tmp_contacts a
	INNER JOIN @operations@.tmp_gross_orders b
ON a.date = b.date
WHERE a.date BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY a.date, channel, agent_name;