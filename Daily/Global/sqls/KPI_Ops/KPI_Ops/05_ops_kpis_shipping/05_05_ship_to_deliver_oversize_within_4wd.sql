-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 05_05_ship_to_deliver_oversize_within_4wd
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'5. Shipping' AS 'group',
	0505 AS id,
	DATE_FORMAT(date_delivered,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_delivered) AS WeekNum,
	date_delivered date,
	'% ship to deliver - Oversize within 4 days' AS kpi,
	'# Items delivered <= 4 days / # Items Delivered' AS formula,
	shipping_carrier AS breakdown_1,
	ship_to_region AS breakdown_2,
	count(*) AS items,
	sum(IF(workdays_to_deliver<=4,1,0)) AS items_sec,
	sum(IF(workdays_to_deliver<=4,1,0))/count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		status_wms IN (
					'Aguardando estoque',
					'Analisando quebra',
					'Estoque reservado',
					'Separando',
					'Aguardando separacao',
					'Faturado',
					'Aguardando expedicao',
					'Expedido',
					'DS estoque reservado',
					'dropshipping notified',
					'backorder',
					'exportado',
					'Waiting dropshipping',
          'handled_by_marketplace',
          'packed_by_marketplace'
	 )
AND		date_delivered BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND		package_measure_new IN ('oversized')
GROUP BY date_delivered , shipping_carrier, ship_to_region;