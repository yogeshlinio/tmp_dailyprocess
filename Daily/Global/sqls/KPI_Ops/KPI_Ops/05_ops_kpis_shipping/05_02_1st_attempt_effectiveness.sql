-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 05_02_1st_attempt_effectiveness
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'5. Shipping' AS 'group',
	0502 AS id,
	DATE_FORMAT(date_delivered,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_delivered) AS WeekNum,	
	date_delivered AS date,
	'% 1st Attempt effectiveness' AS kpi,
	'# Items delivered on 1st attempt / items delivered' AS formula,
	shipping_carrier AS breakdown_1,
	ship_to_region AS breakdown_2,
	count(*) AS items,
	sum(effective_1st_attempt) AS items_sec,
	sum(effective_1st_attempt)/count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		status_wms IN (
					'Aguardando estoque',
					'Analisando quebra',
					'Estoque reservado',
					'Separando',
					'Aguardando separacao',
					'Faturado',
					'Aguardando expedicao',
					'Expedido',
					'DS estoque reservado',
					'dropshipping notified',
					'backorder',
					'exportado',
					'Waiting dropshipping',
          'handled_by_marketplace',
          'packed_by_marketplace'
	 )
AND		date_delivered BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY date_delivered, shipping_carrier, ship_to_region;