-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 05_03_delivery_effectiveness
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'5. Shipping' AS 'group',
	0503 AS id,
	DATE_FORMAT(date_shipped + INTERVAL 2 WEEK,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_shipped + INTERVAL 2 WEEK) AS WeekNum,
	date_shipped + INTERVAL 2 WEEK AS date,
	'% Delivery effectiveness' AS kpi,
	'# Items delivered / # Items Shipped' AS formula,
	shipping_carrier AS breakdown_1,
	ship_to_region AS breakdown_2,
	count(*) AS items,
	sum(is_delivered) AS items_sec,
	sum(is_delivered)/count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		status_wms IN (
					'Aguardando estoque',
					'Analisando quebra',
					'Estoque reservado',
					'Separando',
					'Aguardando separacao',
					'Faturado',
					'Aguardando expedicao',
					'Expedido',
					'DS estoque reservado',
					'dropshipping notified',
					'backorder',
					'exportado',
					'Waiting dropshipping',
					'handled_by_marketplace',
					'packed_by_marketplace',
					'cancelado'
	 )
AND		date_shipped + INTERVAL 2 WEEK BETWEEN curdate() - INTERVAL @interval MONTH + INTERVAL 2 WEEK AND curdate()
GROUP BY date_shipped + INTERVAL 2 WEEK, shipping_carrier, ship_to_region;