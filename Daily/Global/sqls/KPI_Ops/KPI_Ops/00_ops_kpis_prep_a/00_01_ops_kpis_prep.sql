-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 00_01_ops_kpis_prep
-- Created by: Rafael Guzman
-- Created date: 2014-05-30
-- Updated date: 2014-05-30
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 


-- Build KPI reference table:

-- Build KPI Table:
-- CREATE TABLE `ops_kpi` (
--   `country` varchar(50) NOT NULL DEFAULT '',
--   `group` varchar(50) NOT NULL,
--   `id` int(11) NOT NULL DEFAULT '0',
--   `monthnum` varchar(255) DEFAULT NULL,
--   `week` varchar(255) DEFAULT NULL,
--   `date` date NOT NULL,
--   `kpi` varchar(50) NOT NULL DEFAULT '',
--   `formula` varchar(50) DEFAULT NULL,
--   `items` int(11) DEFAULT NULL,
--   `items_secondary` int(11) DEFAULT NULL,
--   `value` decimal(12,3) DEFAULT NULL,
--   `updated_at` datetime DEFAULT NULL,
--   PRIMARY KEY (`country`,`id`,`date`,`kpi`,`group`)
-- ) ENGINE=MyISAM DEFAULT CHARSET=latin1;


-- CREATE TABLE `ops_kpi_week` (
--   `country` varchar(50) NOT NULL DEFAULT '',
--   `group` varchar(50) NOT NULL,
--   `id` int(11) NOT NULL DEFAULT '0',
--   `monthnum` varchar(255) DEFAULT NULL,
--   `week` varchar(255) DEFAULT NULL,
--   `kpi` varchar(50) NOT NULL DEFAULT '',
--   `formula` varchar(50) DEFAULT NULL,
--   `items` int(11) DEFAULT NULL,
--   `items_secondary` int(11) DEFAULT NULL,
--   `value` decimal(12,3) DEFAULT NULL,
--   `updated_at` datetime DEFAULT NULL,
--   PRIMARY KEY (`country`,`id`,`date`,`kpi`,`group`)
-- ) ENGINE=MyISAM DEFAULT CHARSET=latin1;


-- CREATE TABLE `ops_kpi_week` (
--   `country` varchar(50) NOT NULL DEFAULT '',
--   `group` varchar(50) NOT NULL,
--   `id` int(11) NOT NULL DEFAULT '0',
--   `monthnum` varchar(255) DEFAULT NULL,
--   `kpi` varchar(50) NOT NULL DEFAULT '',
--   `formula` varchar(50) DEFAULT NULL,
--   `items` int(11) DEFAULT NULL,
--   `items_secondary` int(11) DEFAULT NULL,
--   `value` decimal(12,3) DEFAULT NULL,
--   `updated_at` datetime DEFAULT NULL,
--   PRIMARY KEY (`country`,`id`,`date`,`kpi`,`group`)
-- ) ENGINE=MyISAM DEFAULT CHARSET=latin1;