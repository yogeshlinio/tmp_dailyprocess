-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 03_12_NPS_for_crossdocking
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'3. Crossdocking' AS 'group',
	0312 AS id,
	DATE_FORMAT(DateSubmitted,'%Y%m') AS MonthNum,
	@operations@.week_iso(DateSubmitted) AS WeekNum,
	DateSubmitted AS date,
	'NPS for Crossdocking Items' AS kpi,
	'( % Promoters - % Detractors ) / Mono item Surveys' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	Count(DISTINCT ResponseID) AS items,
	(COUNT(DISTINCT (IF(a.Loyalty = 'detractor',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS items_sec,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))/Count(DISTINCT ResponseID) - COUNT(DISTINCT (IF(a.Loyalty = 'detractor',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS value,
	now() AS updated_at
FROM nps.NPSData@v_countryPRE@ a
INNER JOIN @operations@.out_order_tracking b
	ON a.ItemID = b.item_id
WHERE DateSubmitted BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND isMonoItem = 1
AND fulfillment_type_real = 'Crossdocking'
GROUP BY DateSubmitted, procurement_analyst;