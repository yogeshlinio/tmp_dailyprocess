-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 03_14_procurement_backlog
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
/*
# 14. Procurement Backlog
REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'3. Crossdocking' AS 'group',
	0314 AS id,
	DATE_FORMAT(@max_promised,'%Y%m') AS MonthNum,
	@operations@.week_iso(@max_promised) AS WeekNum,
	@max_promised AS date,
	'3. Procurement Backlog' AS kpi,
	count(*) AS items,
	sum(delay_to_procure) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Crossdocking'
AND		status_wms IN (
			'aguardando estoque',
			'analisando quebra');
*/