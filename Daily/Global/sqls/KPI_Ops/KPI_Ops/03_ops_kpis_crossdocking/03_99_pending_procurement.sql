-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 03_13_pending_procurement
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
/*
SET @interval = @v_interval@;
SET @max_promised = curdate();

SELECT 
	max(date_procured_promised) INTO @max_promised 
FROM @operations@.out_order_tracking
WHERE date_procured_promised < curdate();


REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'3. Crossdocking' AS 'group',
	0313 AS id,
	DATE_FORMAT(@max_promised,'%Y%m') AS MonthNum,
	@operations@.week_iso(@max_promised) AS WeekNum,
	@max_promised AS date,
	'Pending Procurement' AS kpi,
	count(*) AS items,
	count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Crossdocking'
AND		status_wms IN (
			'aguardando estoque',
			'analisando quebra');
*/