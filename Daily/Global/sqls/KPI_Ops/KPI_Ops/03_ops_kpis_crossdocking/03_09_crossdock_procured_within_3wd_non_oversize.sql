-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 03_09_crossdock_procured_within_3wd_non_oversize
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'3. Crossdocking' AS 'group',
	0309 AS id,
	DATE_FORMAT(date_procured,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_procured) AS WeekNum,
	date_procured AS date,
	'% Crossdock Procured within 3 workdays - Non Oversize' AS kpi,
	'# Procured on target / Procured items' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	count(*) AS items,
	sum(if(workdays_to_procure <= 3,1,0)) AS items_sec,
	sum(if(workdays_to_procure <= 3,1,0))/count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Crossdocking'
AND		status_wms IN (
			'aguardando estoque',
			'analisando quebra',
			'estoque reservado',
			'aguardando separacao',
			'separando',
			'faturado',
			'aguardando expedicao',
			'expedido')
AND		date_procured BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND		package_measure_new IN ('small','medium','large')
GROUP BY date_procured, procurement_analyst;
