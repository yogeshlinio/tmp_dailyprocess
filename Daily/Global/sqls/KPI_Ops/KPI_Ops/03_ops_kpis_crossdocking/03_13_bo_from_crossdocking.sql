-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 03_13_bo_from_crossdocking
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'3. Crossdocking' AS 'group',
	0303 AS id,
	DATE_FORMAT(date_procured_promised,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_procured_promised) AS WeekNum,
	date_procured_promised AS date,
	'% BO from Crossdocking' AS kpi,
	'' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	count(*) AS items,
	sum(is_backorder) AS items_sec,
	sum(is_backorder)/count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Crossdocking'
AND		status_wms IN (
			'aguardando estoque',
			'analisando quebra',
			'estoque reservado',
			'aguardando separacao',
			'separando',
			'faturado',
			'aguardando expedicao',
			'expedido',
			'backorder',
			'backorder_tratada',
			'cancelado',
			'quebra tratada',
			'quebrado')
AND		date_procured_promised BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY date_procured_promised, procurement_analyst;
