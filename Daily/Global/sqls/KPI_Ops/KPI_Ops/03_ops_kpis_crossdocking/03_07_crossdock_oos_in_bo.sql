-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 03_07_crossdock_oos_in_bo
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'3. Crossdocking' AS 'group',
	0307 AS id,
	DATE_FORMAT(date_declared_backorder,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_declared_backorder) AS WeekNum,
	date_declared_backorder AS date,
	'% Crossdocking OOS in Backorder' AS kpi,
	'#OOS in BO / Total BO Declared' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	count(*) AS items,
	sum(is_stockout),
	sum(is_stockout)/count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Crossdocking'
AND		status_wms IN (
			'aguardando estoque',
			'analisando quebra',
			'estoque reservado',
			'aguardando separacao',
			'separando',
			'faturado',
			'aguardando expedicao',
			'expedido',
			'quebrado',
			'quebra tratada',
			'backorder',
			'backorder_tratada'
)
AND		date_declared_backorder BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND		is_backorder = 1
GROUP BY date_declared_backorder, procurement_analyst;