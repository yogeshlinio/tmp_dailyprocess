-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 05_03_returns_processed_uner_2wd
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'4. Warehouse' AS 'group',
	0403 AS id,
	DATE_FORMAT(date_processed_return_promised,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_processed_return_promised) AS WeekNum,
	date_processed_return_promised AS date,
	'% Returns processed < 2 wd' AS kpi,
	'# Items processed <= 2 wd / Total Items promised processing' AS formula,
	category_bp AS breakdown_1,
	reason_for_entrance_il AS breakdown_2,
	count(*) AS items,
	sum(IF(workdays_to_process_return<=2,1,0)) AS items_sec,
	sum(IF(workdays_to_process_return<=2,1,0))/count(*) AS value,
	now() AS updated_at
FROM @operations@.out_inverse_logistics_tracking
WHERE is_returned = 1
AND		date_processed_return_promised BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY date_processed_return_promised, category_bp, reason_for_entrance_il;