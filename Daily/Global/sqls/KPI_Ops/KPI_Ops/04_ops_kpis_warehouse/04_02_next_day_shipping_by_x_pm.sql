-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 04_02_next_day_shipping_by_x_pm
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'4. Warehouse' AS 'group',
	0402 AS id,
	DATE_FORMAT(date_ready_to_pick_bono,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_ready_to_pick_bono) AS WeekNum,
	date_ready_to_pick_bono AS date,
	'% Next Day Shipping by Xpm - Oversize' AS kpi,
	'# items shipped 23:59 / # Items R2P until X pm previous day' AS formula,
	shipping_carrier AS breakdown_1,
	fulfillment_type_real AS breakdown_2,
	count(*) AS items,
	sum(IF(operations_co.calcworkdays(date_ready_to_pick_bono,date_shipped)<=1,1,0)) AS items_sec,
	sum(IF(operations_co.calcworkdays(date_ready_to_pick_bono,date_shipped)<=1,1,0)) / count(*) AS VALUE,
	now() AS updated_at
FROM
	@operations@.out_order_tracking
WHERE
	is_presale = 0
AND fulfillment_type_real <> 'Dropshipping'
AND status_wms IN (
	'aguardando estoque',
	'analisando quebra',
	'estoque reservado',
	'aguardando separacao',
	'separando',
	'faturado',
	'aguardando expedicao',
	'expedido')
AND date_ready_to_pick_bono BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND package_measure_new IN ('oversized')
AND is_shipped = 1
GROUP BY	date_ready_to_pick_bono, shipping_carrier, fulfillment_type_real;