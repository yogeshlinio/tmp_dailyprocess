-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 02_05_dropship_on_time 1st_attempt
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

-- Total
REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping' AS 'group',
	0205 AS id,
	DATE_FORMAT(date_delivered_promised,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_delivered_promised) AS WeekNum,
	date_delivered_promised AS date,
	'% Dropshipping On Time 1st Attempt' AS kpi,
	'# On Time 1st attempt / Items promised' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	count(*) AS items,
	sum(on_time_total_1st_attempt) AS items_sec,
	sum(on_time_total_1st_attempt)/count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Dropshipping'
AND		status_wms IN (
			'analisando quebra',
			'DS estoque reservado',
			'Waiting Dropshipping',
			'handled_by_marketplace',
			'awaiting_fulfillment',
			'expedido')
AND		date_delivered_promised BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY date_delivered_promised, procurement_analyst;

-- Supplier
REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping - Supplier' AS 'group',
	0205 AS id,
	DATE_FORMAT(date_delivered_promised,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_delivered_promised) AS WeekNum,
	date_delivered_promised AS date,
	'% Dropshipping On Time 1st Attempt' AS kpi,
	'# On Time 1st attempt / Items promised' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	count(*) AS items,
	sum(on_time_total_1st_attempt) AS items_sec,
	sum(on_time_total_1st_attempt)/count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Dropshipping'
AND		status_wms IN (
			'analisando quebra',
			'DS estoque reservado',
			'Waiting Dropshipping',
			'handled_by_marketplace',
			'awaiting_fulfillment',
			'expedido')
AND		date_delivered_promised BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND		supplier_type = 'supplier'
GROUP BY date_delivered_promised, procurement_analyst;

-- Merchant
REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping - Merchant' AS 'group',
	0205 AS id,
	DATE_FORMAT(date_delivered_promised,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_delivered_promised) AS WeekNum,
	date_delivered_promised AS date,
	'% Dropshipping On Time 1st Attempt' AS kpi,
	'# On Time 1st attempt / Items promised' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	count(*) AS items,
	sum(on_time_total_1st_attempt) AS items_sec,
	sum(on_time_total_1st_attempt)/count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Dropshipping'
AND		status_wms IN (
			'analisando quebra',
			'DS estoque reservado',
			'Waiting Dropshipping',
			'handled_by_marketplace',
			'awaiting_fulfillment',
			'expedido')
AND		date_delivered_promised BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND		supplier_type = 'merchant'
GROUP BY date_delivered_promised, procurement_analyst;

-- Imports
/*REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping - Imports' AS 'group',
	0205 AS id,
	DATE_FORMAT(date_delivered_promised,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_delivered_promised) AS WeekNum,
	date_delivered_promised AS date,
	'% Dropshipping On Time 1st Attempt' AS kpi,
	'# On Time 1st attempt / Items promised' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	count(*) AS items,
	sum(on_time_total_1st_attempt) AS items_sec,
	sum(on_time_total_1st_attempt)/count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Dropshipping'
AND		status_wms IN (
			'analisando quebra',
			'DS estoque reservado',
			'Waiting Dropshipping',
			'handled_by_marketplace',
			'awaiting_fulfillment',
			'expedido')
AND		date_delivered_promised BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND		products_origin = 'imported'
GROUP BY date_delivered_promised, procurement_analyst;
*/
