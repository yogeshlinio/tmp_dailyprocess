/*-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 02_09_pending_delivery
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

SELECT 
	max(date_delivered_promised) INTO @max_promised 
FROM @operations@.out_order_tracking
WHERE date_delivered_promised < curdate();

-- Total
REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'4. Dropshipping' AS 'group',
	0209 AS id,
	DATE_FORMAT(@max_promised,'%Y%m') AS MonthNum,
	@operations@.week_iso(@max_promised) AS WeekNum,
	@max_promised AS date,
	'2. Pending Delivery' AS kpi,
	count(*) AS items,
	count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Dropshipping'
AND status_wms IN  ('DS estoque reservado','dropshipping notified','handled_by_marketplace','packed_by_marketplace')
AND		is_delivered = 0
AND 	DATEDIFF(date_delivered_promised,curdate()) >= 0;

-- Supplier
REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'4. Dropshipping - Supplier' AS 'group',
	0209 AS id,
	DATE_FORMAT(@max_promised,'%Y%m') AS MonthNum,
	@operations@.week_iso(@max_promised) AS WeekNum,
	@max_promised AS date,
	'2. Pending Delivery' AS kpi,
	count(*) AS items,
	count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Dropshipping'
AND status_wms IN  ('DS estoque reservado','dropshipping notified','handled_by_marketplace','packed_by_marketplace')
AND		is_delivered = 0
AND 	DATEDIFF(date_delivered_promised,curdate()) >= 0
AND		supplier_type = 'supplier';

-- Merchant
REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'4. Dropshipping - Merchant' AS 'group',
	0209 AS id,
	DATE_FORMAT(@max_promised,'%Y%m') AS MonthNum,
	@operations@.week_iso(@max_promised) AS WeekNum,
	@max_promised AS date,
	'2. Pending Delivery' AS kpi,
	count(*) AS items,
	count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Dropshipping'
AND status_wms IN  ('DS estoque reservado','dropshipping notified','handled_by_marketplace','packed_by_marketplace')
AND		is_delivered = 0
AND 	DATEDIFF(date_delivered_promised,curdate()) >= 0
AND		supplier_type = 'merchant';


-- Imports
REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'4. Dropshipping - Imports' AS 'group',
	0209 AS id,
	DATE_FORMAT(@max_promised,'%Y%m') AS MonthNum,
	@operations@.week_iso(@max_promised) AS WeekNum,
	@max_promised AS date,
	'2. Pending Delivery' AS kpi,
	count(*) AS items,
	count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Dropshipping'
AND status_wms IN  ('DS estoque reservado','dropshipping notified','handled_by_marketplace','packed_by_marketplace')
AND		is_delivered = 0
AND 	DATEDIFF(date_delivered_promised,curdate()) >= 0
AND		products_origin = 'imported';*/