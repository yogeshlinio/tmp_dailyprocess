-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 02_01_shipped_under_2_wd
-- Created by: Rafael Guzman
-- Created date: 2014-05-21
-- Updated date: 2014-05-21
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

-- Total
REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping' AS 'group',
	0201 AS id,
	DATE_FORMAT(date_delivered_promised,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_delivered_promised) AS WeekNum,	
	date_delivered_promised AS date,
	'% Shipped within 2 workdays' AS kpi,
	'# Shipped <= 2 Workdays / Promised Items to ship' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	count(*) AS items,
	sum(if(workdays_total_shipment <= 2,1,0)) AS items_sec,
	sum(if(workdays_total_shipment <= 2,1,0))/count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Dropshipping'
AND		status_wms IN (
			'DS estoque reservado',
			'Waiting Dropshipping',
			'handled_by_marketplace',
			'awaiting_fulfillment',
			'expedido')
AND		date_delivered_promised BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY date_delivered_promised, procurement_analyst;

-- Supplier
REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping - Supplier' AS 'group',
	0201 AS id,
	DATE_FORMAT(date_delivered_promised,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_delivered_promised) AS WeekNum,	
	date_delivered_promised AS date,
	'% Shipped within 2 workdays' AS kpi,
	'# Shipped <= 2 Workdays / Promised Items to ship' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	count(*) AS items,
	sum(if(workdays_total_shipment <= 2,1,0)) AS items_sec,
	sum(if(workdays_total_shipment <= 2,1,0))/count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Dropshipping'
AND		status_wms IN (
			'DS estoque reservado',
			'Waiting Dropshipping',
			'handled_by_marketplace',
			'awaiting_fulfillment',
			'expedido')
AND		date_delivered_promised BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND		supplier_type = 'supplier'
GROUP BY date_delivered_promised, procurement_analyst;

-- Merchant
REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping - Merchant' AS 'group',
	0201 AS id,
	DATE_FORMAT(date_delivered_promised,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_delivered_promised) AS WeekNum,	
	date_delivered_promised AS date,
	'% Shipped within 2 workdays' AS kpi,
	'# Shipped <= 2 Workdays / Promised Items to ship' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	count(*) AS items,
	sum(if(workdays_total_shipment <= 2,1,0)) AS items_sec,
	sum(if(workdays_total_shipment <= 2,1,0))/count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Dropshipping'
AND		status_wms IN (
			'DS estoque reservado',
			'Waiting Dropshipping',
			'handled_by_marketplace',
			'awaiting_fulfillment',
			'expedido')
AND		date_delivered_promised BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND		supplier_type = 'merchant'
GROUP BY date_delivered_promised, procurement_analyst;

/*-- Imports
REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping - Imports' AS 'group',
	0201 AS id,
	DATE_FORMAT(date_delivered_promised,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_delivered_promised) AS WeekNum,	
	date_delivered_promised AS date,
	'% Shipped within 2 workdays' AS kpi,
	'# Shipped <= 2 Workdays / Promised Items to ship' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	count(*) AS items,
	sum(if(workdays_total_shipment <= 2,1,0)) AS items_sec,
	sum(if(workdays_total_shipment <= 2,1,0))/count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Dropshipping'
AND		status_wms IN (
			'DS estoque reservado',
			'Waiting Dropshipping',
			'handled_by_marketplace',
			'awaiting_fulfillment',
			'expedido')
AND		date_delivered_promised BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND		products_origin = 'imported'
GROUP BY date_delivered_promised, procurement_analyst;*/