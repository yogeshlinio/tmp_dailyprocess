-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 02_10_dropshipping_delivery_backlog
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
/*
SET @interval = @v_interval@;
SET @max_promised = curdate();

-- Total
REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping' AS 'group',
	0210 AS id,
	DATE_FORMAT(@max_promised,'%Y%m') AS MonthNum,
	@operations@.week_iso(@max_promised) AS WeekNum,
	@max_promised AS date,
	'Delivery Backlog' AS kpi,
	count(*) AS items,
	count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Dropshipping'
AND 	status_wms IN  ('DS estoque reservado','dropshipping notified','handled_by_marketplace','packed_by_marketplace')
AND		is_delivered = 0
AND 	DATEDIFF(date_delivered_promised,curdate()) < 0;

-- Supplier
REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping - Supplier' AS 'group',
	0210 AS id,
	DATE_FORMAT(@max_promised,'%Y%m') AS MonthNum,
	@operations@.week_iso(@max_promised) AS WeekNum,
	@max_promised AS date,
	'Delivery Backlog' AS kpi,
	count(*) AS items,
	count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Dropshipping'
AND 	status_wms IN  ('DS estoque reservado','dropshipping notified','handled_by_marketplace','packed_by_marketplace')
AND		is_delivered = 0
AND 	DATEDIFF(date_delivered_promised,curdate()) < 0
AND 	supplier_type = 'Supplier';

-- Merchant
REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping - Merchant' AS 'group',
	0210 AS id,
	DATE_FORMAT(@max_promised,'%Y%m') AS MonthNum,
	@operations@.week_iso(@max_promised) AS WeekNum,
	@max_promised AS date,
	'Delivery Backlog' AS kpi,
	count(*) AS items,
	count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Dropshipping'
AND 	status_wms IN  ('DS estoque reservado','dropshipping notified','handled_by_marketplace','packed_by_marketplace')
AND		is_delivered = 0
AND 	DATEDIFF(date_delivered_promised,curdate()) < 0
AND 	supplier_type = 'Merchant';


-- Imports
REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping - Imports' AS 'group',
	0210 AS id,
	DATE_FORMAT(@max_promised,'%Y%m') AS MonthNum,
	@operations@.week_iso(@max_promised) AS WeekNum,
	@max_promised AS date,
	'Delivery Backlog' AS kpi,
	count(*) AS items,
	count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Dropshipping'
AND 	status_wms IN  ('DS estoque reservado','dropshipping notified','handled_by_marketplace','packed_by_marketplace')
AND		is_delivered = 0
AND 	DATEDIFF(date_delivered_promised,curdate()) < 0
AND 	products_origin = 'Imported';*/