-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 02_06_NPS_for_dropshipping_delivered
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

-- Total
REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping' AS 'group',
	0206 AS id,
	DATE_FORMAT(DateSubmitted,'%Y%m') AS MonthNum,
	@operations@.week_iso(DateSubmitted) AS WeekNum,
	DateSubmitted AS date,
	'NPS for Dropshipping Items' AS kpi,
	'( % Promoters - % Detractors ) / Mono item Surveys' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	Count(DISTINCT ResponseID) AS items,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS items_sec,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))/Count(DISTINCT ResponseID) - COUNT(DISTINCT (IF(a.Loyalty = 'detractor',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS value,
	now() AS updated_at
FROM nps.NPSData@v_countryPRE@ a
INNER JOIN @operations@.out_order_tracking b
	ON a.ItemID = b.item_id
WHERE DateSubmitted BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND isMonoItem = 1
AND fulfillment_type_real = 'Dropshipping'
GROUP BY DateSubmitted, procurement_analyst;

-- Supplier
REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping - Supplier' AS 'group',
	0206 AS id,
	DATE_FORMAT(DateSubmitted,'%Y%m') AS MonthNum,
	@operations@.week_iso(DateSubmitted) AS WeekNum,
	DateSubmitted AS date,
	'NPS for Dropshipping Items' AS kpi,
	'( % Promoters - % Detractors ) / Mono item Surveys' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	Count(DISTINCT ResponseID) AS items,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS items_sec,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))/Count(DISTINCT ResponseID) - COUNT(DISTINCT (IF(a.Loyalty = 'detractor',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS value,
	now() AS updated_at
FROM nps.NPSData@v_countryPRE@ a
INNER JOIN @operations@.out_order_tracking b
	ON a.ItemID = b.item_id
WHERE DateSubmitted BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND isMonoItem = 1
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type = 'supplier'
GROUP BY DateSubmitted, procurement_analyst;

-- Merchant
REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping - Merchant' AS 'group',
	0206 AS id,
	DATE_FORMAT(DateSubmitted,'%Y%m') AS MonthNum,
	@operations@.week_iso(DateSubmitted) AS WeekNum,
	DateSubmitted AS date,
	'NPS for Dropshipping Items' AS kpi,
	'( % Promoters - % Detractors ) / Mono item Surveys' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	Count(DISTINCT ResponseID) AS items,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS items_sec,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))/Count(DISTINCT ResponseID) - COUNT(DISTINCT (IF(a.Loyalty = 'detractor',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS value,
	now() AS updated_at
FROM nps.NPSData@v_countryPRE@ a
INNER JOIN @operations@.out_order_tracking b
	ON a.ItemID = b.item_id
WHERE DateSubmitted BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND isMonoItem = 1
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type = 'merchant'
GROUP BY DateSubmitted, procurement_analyst;

/*-- Imports
REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping - Imports' AS 'group',
	0206 AS id,
	DATE_FORMAT(DateSubmitted,'%Y%m') AS MonthNum,
	@operations@.week_iso(DateSubmitted) AS WeekNum,
	DateSubmitted AS date,
	'NPS for Dropshipping Items' AS kpi,
	'( % Promoters - % Detractors ) / Mono item Surveys' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	Count(DISTINCT ResponseID) AS items,
	(COUNT(DISTINCT (IF(a.Loyalty = 'detractor',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS items_sec,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))/Count(DISTINCT ResponseID) - COUNT(DISTINCT (IF(a.Loyalty = 'detractor',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS value,
	now() AS updated_at
FROM nps.NPSData@v_countryPrefix@ a
INNER JOIN @operations@.out_order_tracking b
	ON a.ItemID = b.item_id
WHERE DateSubmitted BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND isMonoItem = 1
AND fulfillment_type_real = 'Dropshipping'
AND products_origin = 'imported'
GROUP BY DateSubmitted, procurement_analyst;
*/