-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 02_03_dropship_oos_declared_within_3wd
-- Created by: Rafael Guzman
-- Created date: 2014-05-21
-- Updated date: 2014-05-21
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

-- Total
REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping' AS 'group',
	0203 AS id,
	DATE_FORMAT(date_declared_stockout,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_declared_stockout) AS WeekNum,
	date_declared_stockout AS date,
	'% Dropshipping OOS declared within 3 workdays' AS kpi,
	'# OOS Declared <= 3 / # OOS Declared' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	count(*) AS items,
	sum(if(workdays_to_stockout_declared <= 3,1,0)) AS items_sec,
	sum(if(workdays_to_stockout_declared <= 3,1,0))/count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Dropshipping'
AND		status_wms IN (
			'cancelado',
			'quebrado',
			'quebra tratada')
AND		date_declared_stockout BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND 	is_stockout = 1
GROUP BY date_declared_stockout, procurement_analyst;

-- Supplier
REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping - Supplier' AS 'group',
	0203 AS id,
	DATE_FORMAT(date_declared_stockout,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_declared_stockout) AS WeekNum,
	date_declared_stockout AS date,
	'% Dropshipping OOS declared within 3 workdays' AS kpi,
	'# OOS Declared <= 3 / # OOS Declared' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	count(*) AS items,
	sum(if(workdays_to_stockout_declared <= 3,1,0)) AS items_sec,
	sum(if(workdays_to_stockout_declared <= 3,1,0))/count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Dropshipping'
AND		status_wms IN (
			'cancelado',
			'quebrado',
			'quebra tratada')
AND		date_declared_stockout BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND 	is_stockout = 1
AND 	supplier_type = 'supplier'
GROUP BY date_declared_stockout, procurement_analyst;

-- Merchant
REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping - Merchant' AS 'group',
	0203 AS id,
	DATE_FORMAT(date_declared_stockout,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_declared_stockout) AS WeekNum,
	date_declared_stockout AS date,
	'% Dropshipping OOS declared within 3 workdays' AS kpi,
	'# OOS Declared <= 3 / # OOS Declared' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	count(*) AS items,
	sum(if(workdays_to_stockout_declared <= 3,1,0)) AS items_sec,
	sum(if(workdays_to_stockout_declared <= 3,1,0))/count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Dropshipping'
AND		status_wms IN (
			'cancelado',
			'quebrado',
			'quebra tratada')
AND		date_declared_stockout BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND 	is_stockout = 1
AND 	supplier_type = 'merchant'
GROUP BY date_declared_stockout, procurement_analyst;

/*

-- Imports
REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping - Imports' AS 'group',
	0203 AS id,
	DATE_FORMAT(date_declared_stockout,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_declared_stockout) AS WeekNum,
	date_declared_stockout AS date,
	'% Dropshipping OOS declared within 3 workdays' AS kpi,
	'# OOS Declared <= 3 / # OOS Declared' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	count(*) AS items,
	sum(if(workdays_to_stockout_declared <= 3,1,0)) AS items_sec,
	sum(if(workdays_to_stockout_declared <= 3,1,0))/count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE is_presale = 0
AND		fulfillment_type_real = 'Dropshipping'
AND		status_wms IN (
			'cancelado',
			'quebrado',
			'quebra tratada')
AND		date_declared_stockout BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND 	is_stockout = 1
AND		products_origin = 'imported'
GROUP BY date_declared_stockout, procurement_analyst;
*/