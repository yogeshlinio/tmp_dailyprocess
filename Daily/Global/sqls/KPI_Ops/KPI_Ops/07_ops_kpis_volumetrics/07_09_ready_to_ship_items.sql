-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 07_09_ready_to_ship_items
-- Created by: Rafael Guzman
-- Created date: 2014-05-29
-- Updated date: 2014-05-29
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'7. Volumetrics' AS 'group',
	0709 AS id,
	DATE_FORMAT(date_ready_to_ship,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_ready_to_ship) AS WeekNum,
	date_ready_to_ship AS date,
	'Ready To Ship Items' AS kpi,
	'' AS formula,
	'' AS breakdown_1,
	'' AS breakdown_2,
	COUNT(*) AS items,
	COUNT(*) AS items_sec,
	COUNT(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE date_ready_to_ship BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY date_ready_to_ship;