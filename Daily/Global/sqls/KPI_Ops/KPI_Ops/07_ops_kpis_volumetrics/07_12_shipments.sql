-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 07_12_shipments
-- Created by: Rafael Guzman
-- Created date: 2014-05-29
-- Updated date: 2014-05-29
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'7. Volumetrics' AS 'group',
	0712 AS id,
	DATE_FORMAT(date_shipped,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_shipped) AS WeekNum,
	date_shipped AS date,
	'Shipments' AS kpi,
	'' AS formula,
	'' AS breakdown_1,
	'' AS breakdown_2,
	count(DISTINCT wms_tracking_code) AS items,
	count(*) AS items_sec,
	COUNT(DISTINCT wms_tracking_code) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE date_shipped BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY date_shipped;