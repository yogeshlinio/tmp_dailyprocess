-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 07_16_returned_items
-- Created by: Rafael Guzman
-- Created date: 2014-05-29
-- Updated date: 2014-05-29
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'7. Volumetrics' AS 'group',
	0716 AS id,
	DATE_FORMAT(DateReturned,'%Y%m') AS MonthNum,
	@operations@.week_iso(DateReturned) AS WeekNum,
	DateReturned AS date,
	'Returned Items' AS kpi,
	'' AS formula,
	'' AS breakdown_1,
	'' AS breakdown_2,
	count(*) AS items,
	count(*) AS items_sec,
	COUNT(*) AS value,
	now() AS updated_at
FROM @development@.A_Master
WHERE DateReturned BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY DateReturned;