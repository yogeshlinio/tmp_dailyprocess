-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 07_11_shipped_items
-- Created by: Rafael Guzman
-- Created date: 2014-05-29
-- Updated date: 2014-05-29
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'7. Volumetrics' AS 'group',
	0711 AS id,
	DATE_FORMAT(date_shipped,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_shipped) AS WeekNum,
	date_shipped AS date,
	'Shipped Items' AS kpi,
	'' AS formula,
	'' AS breakdown_1,
	'' AS breakdown_2,
	count(*) AS items,
	count(*) AS items_sec,
	COUNT(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE date_shipped BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY date_shipped;