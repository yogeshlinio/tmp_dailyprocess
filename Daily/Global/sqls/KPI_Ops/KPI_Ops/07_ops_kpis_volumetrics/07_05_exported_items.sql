-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 07_05_exported_items
-- Created by: Rafael Guzman
-- Created date: 2014-05-29
-- Updated date: 2014-05-29
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'7. Volumetrics' AS 'group',
	0705 AS id,
	DATE_FORMAT(Date,'%Y%m') AS MonthNum,
	@operations@.week_iso(Date) AS WeekNum,
	Date AS date,
	'Exported Items' AS kpi,
	'' AS formula,
	'' AS breakdown_1,
	'' AS breakdown_2,
	COUNT(*) AS items,
	COUNT(*) AS items_sec,
	COUNT(*) AS value,
	now() AS updated_at
FROM @development@.A_Master
WHERE Exported = 1
AND		Date BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY Date;