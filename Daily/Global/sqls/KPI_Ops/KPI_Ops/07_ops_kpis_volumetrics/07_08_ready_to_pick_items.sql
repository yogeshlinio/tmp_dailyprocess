-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 07_08_ready_to_pick_items
-- Created by: Rafael Guzman
-- Created date: 2014-05-29
-- Updated date: 2014-05-29
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'7. Volumetrics' AS 'group',
	0708 AS id,
	DATE_FORMAT(date_ready_to_pick,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_ready_to_pick) AS WeekNum,
	date_ready_to_pick AS date,
	'Ready To Pick Items' AS kpi,
	'' AS formula,
	'' AS breakdown_1,
	'' AS breakdown_2,
	count(*) AS items,
	count(*) AS items,
	COUNT(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE date_ready_to_pick BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY date_ready_to_pick 
ORDER BY date_ready_to_pick DESC;