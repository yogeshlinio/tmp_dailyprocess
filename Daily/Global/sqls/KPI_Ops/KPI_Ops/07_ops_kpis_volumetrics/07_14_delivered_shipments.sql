-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 07_14_delivered_shipments
-- Created by: Rafael Guzman
-- Created date: 2014-05-29
-- Updated date: 2014-05-29
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'7. Volumetrics' AS 'group',
	0714 AS id,
	DATE_FORMAT(date_delivered,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_delivered) AS WeekNum,
	date_delivered AS date,
	'Delivered Shipments' AS kpi,
	'' AS formula,
	'' AS breakdown_1,
	'' AS breakdown_2,
	count(*) AS items,
	COUNT(DISTINCT wms_tracking_code) AS items_sec,
	COUNT(DISTINCT wms_tracking_code) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE date_delivered BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY date_delivered;