-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 07_02_gross_orders
-- Created by: Rafael Guzman
-- Created date: 2014-05-29
-- Updated date: 2014-05-29
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'7. Volumetrics' AS 'group',
	0702 AS id,
	DATE_FORMAT(Date,'%Y%m') AS MonthNum,
	@operations@.week_iso(Date) AS WeekNum,
	Date AS date,
	'Gross Orders' AS kpi,
	'' AS formula,
	'' AS breakdown_1,
	'' AS breakdown_2,
	count(DISTINCT OrderNum) AS items,
	count(*) AS items_sec,
	COUNT(DISTINCT OrderNum) AS value,
	now() AS updated_at
FROM @development@.A_Master
WHERE OrderBeforeCan = 1
AND		Date BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY Date;