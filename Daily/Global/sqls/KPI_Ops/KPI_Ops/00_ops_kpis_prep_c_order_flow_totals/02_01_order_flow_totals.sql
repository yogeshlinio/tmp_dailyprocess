-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 02_01_order_flow_totals
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

DROP TABLE IF EXISTS @operations@.order_flow_totals;
CREATE TABLE @operations@.order_flow_totals
SELECT
	b.date_ordered,
	a.PaymentMethod,
	SUM(b.ItemsInOrder) AS placed_items,
	SUM(OrderCounter) AS placed_orders,
	SUM(a.Invalid) AS invalid_orders,
	SUM(b.is_gross) AS gross_orders,
	SUM(b.is_net) AS net_orders,
	SUM(a.AutoFraudCheckPending) AS auto_fraud_check_pending_orders,
	SUM(a.PaymentPending) AS payment_pending_orders,
	SUM(a.ManualFraudCheckPending) AS manual_fraud_check_pending_orders,
	SUM(a.RefundNeeded) AS refund_needed_orders,
	SUM(a.Exportable) AS exportable_orders,
	SUM(a.RejectNewInvalid) AS system_rejected_orders,
	SUM(IF(a.RejectNewInvalid = 0,1,0)) AS system_accepted_orders,
	SUM(a.RejectAuto) AS auto_fraud_rejected_orders,
	SUM(a.ApproveAuto) AS auto_fraud_accepted_orders,
	SUM(a.RejectBank) AS bank_rejected_orders,
	SUM(IF(a.isBankReviewed = 1 AND a.RejectBank = 0,1,0)) AS bank_accepted_orders,
	SUM(a.RejectManual) AS manual_fraud_rejected_orders,
	SUM(a.ApproveManual) AS manual_fraud_accepted_orders
FROM	customer_service.tbl_fraud_check_report a
INNER JOIN @operations@.tbl_order_flow b
	ON a.OrderNumber = b.OrderNum
GROUP BY
	b.date_ordered,
	a.PaymentMethod
;