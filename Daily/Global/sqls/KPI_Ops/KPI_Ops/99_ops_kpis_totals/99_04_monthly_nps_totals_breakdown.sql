-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 99_02_monthly_nps_totals
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 


SET @interval = @v_interval@;
SET @max_promised = curdate();

-- -- -- -- DROPSHIPPING -- -- -- -- 

-- Total
REPLACE operations_test.ops_kpi_month
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping' AS 'group',
	0206 AS id,
	DATE_FORMAT(DateSubmitted,'%Y%m') AS MonthNum,
	'NPS for Dropshipping Items' AS kpi,
	'( % Promoters - % Detractors ) / Mono item Surveys' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS items,
	(COUNT(DISTINCT (IF(a.Loyalty = 'detractor',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS items_sec,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))/Count(DISTINCT ResponseID) - COUNT(DISTINCT (IF(a.Loyalty = 'detractor',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS value,
	000000000000.000 AS value_monthly,
	now() AS updated_at
FROM nps.NPSData@v_countryPRE@ a
INNER JOIN @operations@.out_order_tracking b
	ON a.ItemID = b.item_id
WHERE DateSubmitted BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND DATE_FORMAT(DateSubmitted,'%Y%m') > DATE_FORMAT(curdate() - INTERVAL @interval MONTH,'%Y%m')
AND isMonoItem = 1
AND fulfillment_type_real = 'Dropshipping'
GROUP BY DATE_FORMAT(DateSubmitted,'%Y%m'), procurement_analyst;

-- Supplier
REPLACE operations_test.ops_kpi_month
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping - Supplier' AS 'group',
	0206 AS id,
	DATE_FORMAT(DateSubmitted,'%Y%m') AS MonthNum,
	'NPS for Dropshipping Items' AS kpi,
	'( % Promoters - % Detractors ) / Mono item Surveys' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS items,
	(COUNT(DISTINCT (IF(a.Loyalty = 'detractor',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS items_sec,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))/Count(DISTINCT ResponseID) - COUNT(DISTINCT (IF(a.Loyalty = 'detractor',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS value,
	000000000000.000 AS value_monthly,
	now() AS updated_at
FROM nps.NPSData@v_countryPRE@ a
INNER JOIN @operations@.out_order_tracking b
	ON a.ItemID = b.item_id
WHERE DateSubmitted BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND DATE_FORMAT(DateSubmitted,'%Y%m') > DATE_FORMAT(curdate() - INTERVAL @interval MONTH,'%Y%m')
AND isMonoItem = 1
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type = 'supplier'
GROUP BY DATE_FORMAT(DateSubmitted,'%Y%m'), procurement_analyst;

-- Merchant
REPLACE operations_test.ops_kpi_month
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping - Merchant' AS 'group',
	0206 AS id,
	DATE_FORMAT(DateSubmitted,'%Y%m') AS MonthNum,
	'NPS for Dropshipping Items' AS kpi,
	'( % Promoters - % Detractors ) / Mono item Surveys' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS items,
	(COUNT(DISTINCT (IF(a.Loyalty = 'detractor',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS items_sec,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))/Count(DISTINCT ResponseID) - COUNT(DISTINCT (IF(a.Loyalty = 'detractor',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS value,
	000000000000.000 AS value_monthly,	
	now() AS updated_at
FROM nps.NPSData@v_countryPRE@ a
INNER JOIN @operations@.out_order_tracking b
	ON a.ItemID = b.item_id
WHERE DateSubmitted BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND DATE_FORMAT(DateSubmitted,'%Y%m') > DATE_FORMAT(curdate() - INTERVAL @interval MONTH,'%Y%m')
AND isMonoItem = 1
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type = 'merchant'
GROUP BY DATE_FORMAT(DateSubmitted,'%Y%m'), procurement_analyst;

/*-- Imports
REPLACE operations_test.ops_kpi_month
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping - Imports' AS 'group',
	0206 AS id,
	DATE_FORMAT(DateSubmitted,'%Y%m') AS MonthNum,
	'NPS for Dropshipping Items' AS kpi,
	'( % Promoters - % Detractors ) / Mono item Surveys' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS items,
	(COUNT(DISTINCT (IF(a.Loyalty = 'detractor',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS items_sec,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))/Count(DISTINCT ResponseID) - COUNT(DISTINCT (IF(a.Loyalty = 'detractor',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS value,
	000000000000.000 AS value_monthly,
	now() AS updated_at
FROM nps.NPSData@v_countryPrefix@ a
INNER JOIN @operations@.out_order_tracking b
	ON a.ItemID = b.item_id
WHERE DateSubmitted BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND DATE_FORMAT(DateSubmitted,'%Y%m') > DATE_FORMAT(curdate() - INTERVAL @interval MONTH,'%Y%m')
AND isMonoItem = 1
AND fulfillment_type_real = 'Dropshipping'
AND products_origin = 'imported'
GROUP BY DATE_FORMAT(DateSubmitted,'%Y%m'), procurement_analyst;
*/

-- -- -- -- CROSSDOCKING -- -- -- -- 

REPLACE operations_test.ops_kpi_month
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'3. Crossdocking' AS 'group',
	0312 AS id,
	DATE_FORMAT(DateSubmitted,'%Y%m') AS MonthNum,
	'NPS for Crossdocking Items' AS kpi,
	'( % Promoters - % Detractors ) / Mono item Surveys' AS formula,
	'' AS breakdown_1,
	procurement_analyst AS breakdown_2,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS items,
	(COUNT(DISTINCT (IF(a.Loyalty = 'detractor',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS items_sec,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))/Count(DISTINCT ResponseID) - COUNT(DISTINCT (IF(a.Loyalty = 'detractor',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS value,
	000000000000.000 AS value_monthly,
	now() AS updated_at
FROM nps.NPSData@v_countryPRE@ a
INNER JOIN @operations@.out_order_tracking b
	ON a.ItemID = b.item_id
WHERE DateSubmitted BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND DATE_FORMAT(DateSubmitted,'%Y%m') > DATE_FORMAT(curdate() - INTERVAL @interval MONTH,'%Y%m')
AND isMonoItem = 1
AND fulfillment_type_real = 'Crossdocking'
GROUP BY DATE_FORMAT(DateSubmitted,'%Y%m'), procurement_analyst;

-- -- -- -- TOTAL -- -- -- --

REPLACE operations_test.ops_kpi_month
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'10. General' AS 'group',
	1001 AS id,
	DATE_FORMAT(DateSubmitted,'%Y%m') AS MonthNum,
	'NPS Score' AS kpi,
	'( % Promoters - % Detractors ) / Mono item Surveys' AS formula,
	'' AS breakdown_1,
	'' AS breakdown_2,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS items,
	(COUNT(DISTINCT (IF(a.Loyalty = 'detractor',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS items_sec,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))/Count(DISTINCT ResponseID) - COUNT(DISTINCT (IF(a.Loyalty = 'detractor',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS value,
	000000000000.000 AS value_monthly,
	Now() AS updated_at
FROM nps.NPSData@v_countryPRE@ a
WHERE DateSubmitted BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND DATE_FORMAT(DateSubmitted,'%Y%m') > DATE_FORMAT(curdate() - INTERVAL @interval MONTH,'%Y%m')
AND isMonoItem = 1
GROUP BY DATE_FORMAT(DateSubmitted,'%Y%m');

