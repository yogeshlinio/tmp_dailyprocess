-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 99_01_daily_totals_table_breakdown
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

DROP TEMPORARY TABLE IF EXISTS operations_test.ops_kpi_linio;
CREATE TEMPORARY TABLE operations_test.ops_kpi_linio
-- ratios
SELECT
	'Linio' AS country,
	`group`,
	id,
	monthnum,
	week,
	date,
	kpi,
	formula,
	breakdown_1,
	breakdown_2,
	sum(items) AS items,
	sum(items_secondary) AS items_secondary,
	sum(items * value)/sum(items) AS value,
	now() AS updated_at
FROM operations_test.ops_kpi
WHERE `group` <> '7. Volumetrics'
OR kpi NOT LIKE '%NPS%'
GROUP BY 
	date,
	`group`,
	id,
	breakdown_1,
	breakdown_2
UNION ALL
-- additions
SELECT
	'Linio' AS country,
	`group`,
	id,
	monthnum,
	`week`,
	date,
	kpi,
	formula,
	breakdown_1,
	breakdown_2,
	sum(items) AS items,
	sum(items_secondary) AS items_secondary,
	sum(value) AS value,
	now() AS updated_at
FROM operations_test.ops_kpi
WHERE `group` = '7. Volumetrics'
GROUP BY 
	date,
	`group`,
	id,
	breakdown_1,
	breakdown_2
ORDER BY 
	date ASC,
	`group` ASC,
	id ASC;

REPLACE operations_test.ops_kpi
SELECT * FROM operations_test.ops_kpi_linio;