-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 99_02_monthly_nps_totals
-- Created by: Rafael Guzman
-- Created date: 2014-06-04
-- Updated date: 2014-06-04
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 


SET @interval = @v_interval@;
SET @max_promised = curdate();

-- -- -- -- CREATE TEMP TABLE -- -- -- -- 
DROP TEMPORARY TABLE IF EXISTS operations_test.ops_kpi_nps_totals;
CREATE TEMPORARY TABLE operations_test.ops_kpi_nps_totals (INDEX (country,monthnum,id))
-- Total Dropshippint
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping' AS 'group',
	0206 AS id,
	DATE_FORMAT(DateSubmitted,'%Y%m') AS MonthNum,
	'NPS for Dropshipping Items' AS kpi,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))/Count(DISTINCT ResponseID) - COUNT(DISTINCT (IF(a.Loyalty = 'detractor',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS value,
	now() AS updated_at
FROM nps.NPSData@v_countryPRE@ a
INNER JOIN @operations@.out_order_tracking b
	ON a.ItemID = b.item_id
WHERE DateSubmitted BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND DATE_FORMAT(DateSubmitted,'%Y%m') > DATE_FORMAT(curdate() - INTERVAL @interval MONTH,'%Y%m')
AND isMonoItem = 1
AND fulfillment_type_real = 'Dropshipping'
GROUP BY DATE_FORMAT(DateSubmitted,'%Y%m')
UNION ALL
-- Supplier Dropshipping
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping - Supplier' AS 'group',
	0206 AS id,
	DATE_FORMAT(DateSubmitted,'%Y%m') AS MonthNum,
	'NPS for Dropshipping Items' AS kpi,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))/Count(DISTINCT ResponseID) - COUNT(DISTINCT (IF(a.Loyalty = 'detractor',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS value,
	now() AS updated_at
FROM nps.NPSData@v_countryPRE@ a
INNER JOIN @operations@.out_order_tracking b
	ON a.ItemID = b.item_id
WHERE DateSubmitted BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND DATE_FORMAT(DateSubmitted,'%Y%m') > DATE_FORMAT(curdate() - INTERVAL @interval MONTH,'%Y%m')
AND isMonoItem = 1
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type = 'supplier'
GROUP BY DATE_FORMAT(DateSubmitted,'%Y%m')
UNION ALL
-- Merchant Dropshipping
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'2. Dropshipping - Merchant' AS 'group',
	0206 AS id,
	DATE_FORMAT(DateSubmitted,'%Y%m') AS MonthNum,
	'NPS for Dropshipping Items' AS kpi,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))/Count(DISTINCT ResponseID) - COUNT(DISTINCT (IF(a.Loyalty = 'detractor',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS value,
	now() AS updated_at
FROM nps.NPSData@v_countryPRE@ a
INNER JOIN @operations@.out_order_tracking b
	ON a.ItemID = b.item_id
WHERE DateSubmitted BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND DATE_FORMAT(DateSubmitted,'%Y%m') > DATE_FORMAT(curdate() - INTERVAL @interval MONTH,'%Y%m')
AND isMonoItem = 1
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type = 'merchant'
GROUP BY DATE_FORMAT(DateSubmitted,'%Y%m')
UNION ALL
-- Imports pending
-- Crossdocking
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'3. Crossdocking' AS 'group',
	0312 AS id,
	DATE_FORMAT(DateSubmitted,'%Y%m') AS MonthNum,
	'NPS for Crossdocking Items' AS kpi,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))/Count(DISTINCT ResponseID) - COUNT(DISTINCT (IF(a.Loyalty = 'detractor',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS value,
	now() AS updated_at
FROM nps.NPSData@v_countryPRE@ a
INNER JOIN @operations@.out_order_tracking b
	ON a.ItemID = b.item_id
WHERE DateSubmitted BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND DATE_FORMAT(DateSubmitted,'%Y%m') > DATE_FORMAT(curdate() - INTERVAL @interval MONTH,'%Y%m')
AND isMonoItem = 1
AND fulfillment_type_real = 'Crossdocking'
GROUP BY DATE_FORMAT(DateSubmitted,'%Y%m')
UNION ALL
-- Total
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'10. General' AS 'group',
	1001 AS id,
	DATE_FORMAT(DateSubmitted,'%Y%m') AS MonthNum,
	'NPS Score' AS kpi,
	(COUNT(DISTINCT (IF(a.Loyalty = 'promoter',ResponseID,0)))/Count(DISTINCT ResponseID) - COUNT(DISTINCT (IF(a.Loyalty = 'detractor',ResponseID,0)))
		/Count(DISTINCT ResponseID))*100 AS value,
	Now() AS updated_at
FROM nps.NPSData@v_countryPRE@ a
WHERE DateSubmitted BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND DATE_FORMAT(DateSubmitted,'%Y%m') > DATE_FORMAT(curdate() - INTERVAL @interval MONTH,'%Y%m')
AND isMonoItem = 1
GROUP BY DATE_FORMAT(DateSubmitted,'%Y%m');

-- -- -- -- UPDATE TABLE -- -- -- -- 
UPDATE operations_test.ops_kpi_month a
INNER JOIN operations_test.ops_kpi_nps_totals b
	ON 	a.country = b.country
	AND a.monthnum = b.monthnum
	AND a.id = b.id
SET 
	a.value_monthly = b.value;