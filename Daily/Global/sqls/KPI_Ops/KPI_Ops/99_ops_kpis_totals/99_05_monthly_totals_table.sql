-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 99_01_monthly_totals_table
-- Created by: Rafael Guzman
-- Created date: 2014-06-04
-- Updated date: 2014-06-04
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 


-- -- -- -- CREATE TEMP TABLE -- -- -- -- 
DROP TEMPORARY TABLE IF EXISTS operations_test.ops_kpi_month_totals;
CREATE TEMPORARY TABLE operations_test.ops_kpi_month_totals (INDEX (country,monthnum,id))
SELECT
 	country,
	`group`,
	id,
	monthnum,
	sum(items) AS items,
	sum(items_secondary) AS items_secondary,
	sum(items * value)/sum(items) AS value
FROM operations_test.ops_kpi_month
WHERE `group` <> '7. Volumetrics'
OR kpi NOT LIKE '%NPS%'
GROUP BY 
	country,
	monthnum,
	`group`,
	id
UNION ALL
SELECT
 	country,
	`group`,
	id,
	monthnum,
	sum(items) AS items,
	sum(items_secondary) AS items_secondary,
	sum(value) AS value
FROM operations_test.ops_kpi_month
WHERE `group` = '7. Volumetrics'
GROUP BY 
	country,
	monthnum,
	`group`,
	id
ORDER BY 
	country ASC,
	monthnum ASC,
	`group` ASC,
	id ASC;

-- -- -- -- UPDATE TABLE -- -- -- -- 
UPDATE operations_test.ops_kpi_month a
INNER JOIN operations_test.ops_kpi_month_totals b
	ON 	a.country = b.country
	AND a.monthnum = b.monthnum
	AND a.id = b.id
SET 
	a.value_monthly = b.value;
