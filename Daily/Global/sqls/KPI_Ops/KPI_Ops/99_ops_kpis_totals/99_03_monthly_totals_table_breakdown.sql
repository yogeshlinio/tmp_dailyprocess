-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 99_01_monthly_totals_table_breakdown
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

TRUNCATE operations_test.ops_kpi_month;

-- ratios
REPLACE operations_test.ops_kpi_month
SELECT
	country,
	`group`,
	id,
	monthnum,
	kpi,
	formula,
	breakdown_1,
	breakdown_2,
	sum(items) AS items,
	sum(items_secondary) AS items_secondary,
	sum(items * value)/sum(items) AS value,
	000000000000.000 AS value_monthly,
	now() AS updated_at
FROM operations_test.ops_kpi
WHERE `group` <> '7. Volumetrics'
OR kpi NOT LIKE '%NPS%'
GROUP BY 
	country,
	monthnum,
	`group`,
	id,
	breakdown_1,
	breakdown_2
ORDER BY 
	country ASC,
	monthnum ASC,
	`group` ASC,
	id ASC;

REPLACE operations_test.ops_kpi_month
SELECT
	'Linio' AS country,
	`group`,
	id,
	monthnum,
	kpi,
	formula,
	breakdown_1,
	breakdown_2,
	sum(items) AS items,
	sum(items_secondary) AS items_secondary,
	sum(items * value)/sum(items) AS value,
	000000000000.000 AS value_monthly,
	now() AS updated_at
FROM operations_test.ops_kpi
WHERE `group` <> '7. Volumetrics'
OR kpi NOT LIKE '%NPS%'
GROUP BY 
	monthnum,
	`group`,
	id,
	breakdown_1,
	breakdown_2
ORDER BY 
	monthnum ASC,
	`group` ASC,
	id ASC;

-- additions
REPLACE operations_test.ops_kpi_month
SELECT
	country,
	`group`,
	id,
	monthnum,
	kpi,
	formula,
	breakdown_1,
	breakdown_2,
	sum(items) AS items,
	sum(items_secondary) AS items_secondary,
	sum(value) AS value,
	000000000000.000 AS value_monthly,
	now() AS updated_at
FROM operations_test.ops_kpi
WHERE `group` = '7. Volumetrics'
GROUP BY 
	country,
	monthnum,
	`group`,
	id,
	breakdown_1,
	breakdown_2
ORDER BY 
	country ASC,
	monthnum ASC,
	`group` ASC,
	id ASC;


REPLACE operations_test.ops_kpi_month
SELECT
	'Linio' AS country,
	`group`,
	id,
	monthnum,
	kpi,
	formula,
	breakdown_1,
	breakdown_2,
	sum(items) AS items,
	sum(items_secondary) AS items_secondary,
	sum(value) AS value,
	000000000000.000 AS value_monthly,
	now() AS updated_at
FROM operations_test.ops_kpi
WHERE `group` = '7. Volumetrics'
GROUP BY 
	monthnum,
	`group`,
	id,
	breakdown_1,
	breakdown_2
ORDER BY 
	monthnum ASC,
	`group` ASC,
	id ASC;