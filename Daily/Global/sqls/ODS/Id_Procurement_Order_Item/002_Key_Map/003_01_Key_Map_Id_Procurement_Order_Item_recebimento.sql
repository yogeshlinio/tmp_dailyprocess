DROP TEMPORARY TABLE IF EXISTS Recebimento_Procurement_@v_countryPrefix@;
CREATE TEMPORARY TABLE Recebimento_Procurement_@v_countryPrefix@ (  
          id int auto_increment,
          PRIMARY KEY (  purchase_order, sku_simple , id ), 
		  KEY (  purchase_order, sku_simple ) )
SELECT 
   inbound_document_identificator AS purchase_order,
   c.sku as sku_simple  , 
   itens_recebimento_id,
   c.recebimento_id   
FROM   
               @v_wmsprod@.recebimento AS b
	INNER JOIN @v_wmsprod@.itens_recebimento AS c 
		    ON b.recebimento_id = c.recebimento_id
ORDER BY purchase_order, sku_simple			 
;   

DROP TEMPORARY TABLE IF EXISTS Procurement_Order_Place_Unique_@v_countryPrefix@;
CREATE TEMPORARY TABLE Procurement_Order_Place_Unique_@v_countryPrefix@
(
    id int auto_increment,
    PRIMARY KEY (  purchase_order, sku_simple , id ), 
	INDEX ( id_procurement_order_item ),
	KEY (  purchase_order, sku_simple ) 
)
SELECT 
   id_procurement_order_item,
   purchase_order,
   sku_simple  , 
   NULL AS id,
   0 AS itens_recebimento_id,
   0 AS recebimento_id   
FROM		 
   Id_Procurement_Order_Item_Key_Map_Sample_@v_countryPrefix@
ORDER BY purchase_order, sku_simple
;
		 
UPDATE         Recebimento_Procurement_@v_countryPrefix@        AS a
    INNER JOIN Procurement_Order_Place_Unique_@v_countryPrefix@ AS c
		 USING ( purchase_order , sku_simple, id ) 
SET
   c.itens_recebimento_id = a.itens_recebimento_id,
   c.recebimento_id       = a.recebimento_id;

   
   UPDATE      Id_Procurement_Order_Item_Key_Map_Sample_@v_countryPrefix@   AS a
    INNER JOIN Procurement_Order_Place_Unique_@v_countryPrefix@ AS c
		 USING ( id_procurement_order_item ) 
SET
   a.itens_recebimento_id = c.itens_recebimento_id,
   a.recebimento_id       = c.recebimento_id;
