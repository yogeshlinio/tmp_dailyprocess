UPDATE            Id_Procurement_Order_Item_Key_Map_Sample_@v_countryPrefix@ AS Id_Procurement_Order_Item_Key_Map_Sample
       INNER JOIN Sku_Simple_Key_Map_Sample_@v_countryPrefix@ AS Sku_Simple_Key_Map
	        USING ( Country, id_catalog_simple )
SET
   Id_Procurement_Order_Item_Key_Map_Sample.Sku_Simple               = Sku_Simple_Key_Map.Sku_Simple
WHERE Id_Procurement_Order_Item_Key_Map_Sample.Country='@v_countryPrefix@'
;