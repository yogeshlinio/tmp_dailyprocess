UPDATE Id_Procurement_Order_Item_Key_Map_Sample_@v_countryPrefix@ AS a
    INNER JOIN @v_wmsprod@.estoque AS b
		ON a.itens_recebimento_id = b.itens_recebimento_id
SET
   a.estoque_id = b.estoque_id;