CREATE TEMPORARY TABLE TMP_Id_Procurement_Order_Item_@v_countryPrefix@ (INDEX (purchase_order))
SELECT purchase_order,
	   COUNT(1) AS num_items
FROM Id_Procurement_Order_Item_Sample_@v_countryPrefix@
WHERE is_deleted = 0
AND	is_cancelled = 0
GROUP BY purchase_order;

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
INNER JOIN TMP_Id_Procurement_Order_Item_@v_countryPrefix@ AS b
	ON a.purchase_order = b.purchase_order
SET 
	a.items_in_po = b.num_items,
	a.perc_items_in_po = 1/b.num_items;
	
UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
SET price_receipt = cost_oms_after_tax
WHERE is_receipt = 1;

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
SET price_not_receipt = cost_oms_after_tax
WHERE is_receipt = 0;

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
SET item_counter = 1;
   