CALL  production.monitoring_step( "operations_@v_countryPrefix@.calcworkdays", "@v_country@", "finish" , 50 );

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON DATE(date_delivery_bob_original_calculated) = calcworkdays.date_last 
    AND date_first = date_po_created
SET workdays_to_receipt_shedule = calcworkdays.workdays;

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON DATE(date_delivery_bob_original_calculated) = calcworkdays.date_last 
    AND workdays = 10
SET date_promised_procurement = calcworkdays.date_first
WHERE workdays_to_receipt_shedule<=5
AND purchase_order_type='Stock';

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON date(date_delivery_bob_original_calculated) = calcworkdays.date_last 
    AND workdays = 5
SET date_promised_procurement = calcworkdays.date_first
WHERE workdays_to_receipt_shedule>5
AND purchase_order_type='Stock';

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON date_po_created = calcworkdays.date_first
		AND date_goods_received = calcworkdays.date_last
SET workdays_to_goods_receipt_since_created = calcworkdays.workdays;

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON date_po_confirmed = calcworkdays.date_first
		AND date_goods_received = calcworkdays.date_last
SET workdays_to_goods_receipt_since_confirmed =	calcworkdays.workdays;

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON date_po_confirmed = calcworkdays.date_first
		AND date_collection_scheduled = calcworkdays.date_last
SET workdays_to_scheduled =	calcworkdays.workdays;