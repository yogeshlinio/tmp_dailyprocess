UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
SET 
 cost_oms_gt_bob = 1
WHERE
 cost_oms > cost_bob;
 
UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
SET  pending_to_receipt=1
WHERE date(date_promised_procurement)<=CURDATE()
AND is_cancelled = 0
AND is_deleted = 0
AND is_receipt = 0
AND date_promised_procurement IS NOT NULL;
	
   