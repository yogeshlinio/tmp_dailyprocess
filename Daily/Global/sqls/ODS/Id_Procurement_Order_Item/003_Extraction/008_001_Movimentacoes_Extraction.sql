UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
INNER JOIN Id_Procurement_Order_Item_Key_Map_Sample_@v_countryPrefix@ AS b
	    USING ( country , Id_Procurement_Order_Item )
INNER JOIN @v_wmsprod@.estoque AS c
	ON b.itens_recebimento_id = c.itens_recebimento_id
INNER JOIN @v_wmsprod@.movimentacoes AS d
ON b.estoque_id = d.estoque_id
SET 
  a.exit_type = 	CASE 
						WHEN ( c.endereco = 'vendidos' AND d.para_endereco = "vendidos" )
										THEN 'sold'
											WHEN ( c.endereco LIKE '%error%' AND d.para_endereco  LIKE '%error%' )
											THEN 'error'
										ELSE NULL
						END;