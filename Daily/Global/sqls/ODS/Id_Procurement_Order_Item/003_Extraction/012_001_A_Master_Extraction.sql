UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
INNER JOIN @bob_live@.sales_order_item AS b 
	ON a.item_id = b.id_sales_order_item
SET 
 a.date_ordered = DATE(b.created_at),
 a.cost_bob = b.cost; 