 /*
CREATE TEMPORARY TABLE operations_mx.tmp_stock_ft (INDEX (stock_item_id))
SELECT
	stock_item_id,
	fulfillment_type_real,
	fulfillment_type_bp
FROM operations_mx.out_stock_hist;

UPDATE operations_mx.out_procurement_tracking a
INNER JOIN operations_mx.tmp_stock_ft b 
	ON a.stock_item_id = b.stock_item_id
SET 
 a.fulfillment_type_real = b.fulfillment_type_real,
 a.fulfillment_type_bp = b.fulfillment_type_bp ;


UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
INNER JOIN @operations@.out_stock_hist AS b 
	ON a.stock_item_id = b.stock_item_id
SET 
 a.fulfillment_type_real = b.fulfillment_type_real,
 a.fulfillment_type_bp = b.fulfillment_type_bp;
 
 */