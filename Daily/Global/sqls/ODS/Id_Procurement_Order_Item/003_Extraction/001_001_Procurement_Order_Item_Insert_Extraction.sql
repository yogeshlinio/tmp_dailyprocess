DROP TABLE IF EXISTS Id_Procurement_Order_Item_Sample_@v_countryPrefix@;
CREATE TABLE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ LIKE Id_Procurement_Order_Item_Template_@v_countryPrefix@;
/*INSERT ALL THE PRIMARY KEYS*/
INSERT Id_Procurement_Order_Item_Sample_@v_countryPrefix@
(
	`Country`,
	`id_procurement_order_item`,
	`id_procurement_order`,
	`sku_simple`
	#`is_receipt`,
	#`cost_oms`,
	#`cost_oms_after_tax`,
	#`inbound_type`
)
SELECT 
	'@v_countryPrefix@' AS Country,
	id_Procurement_order_item,
	id_Procurement_order,
	sku_simple
	#is_receipt,
	#cost_oms,
	#cost_oms_after_tax,
	#inbound_type
FROM 
   Id_Procurement_Order_Item_Key_Map_Sample_@v_countryPrefix@
WHERE 
   Country = '@v_countryPrefix@';
   
   
   