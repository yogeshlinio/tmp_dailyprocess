UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
INNER JOIN Id_Procurement_Order_Item_Key_Map_Sample_@v_countryPrefix@ AS b
	    USING ( country , Id_Procurement_Order_Item )
INNER JOIN @v_wmsprod@.estoque AS c 
	ON b.itens_recebimento_id = c.itens_recebimento_id
SET 
 a.stock_item_id = c.estoque_id,
 a.wh_location = c.endereco;