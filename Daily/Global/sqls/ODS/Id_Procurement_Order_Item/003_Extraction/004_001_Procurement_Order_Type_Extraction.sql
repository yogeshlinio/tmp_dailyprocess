DROP TEMPORARY TABLE IF EXISTS PO_Type_@v_countryPrefix@;
CREATE TEMPORARY TABLE PO_Type_@v_countryPrefix@ 
(
    INDEX ( id_procurement_order_item ),  
	INDEX ( id_procurement_order_type )
 )
SELECT 
   id_procurement_order_item,
   id_procurement_order_type,
   space(45)  AS purchase_order_type 
FROM 
   Id_Procurement_Order_Item_Key_Map_Sample_@v_countryPrefix@
;   

UPDATE            PO_Type_@v_countryPrefix@ AS a
       INNER JOIN @procurement_live@.procurement_order_type AS b
	        USING ( id_procurement_order_type )
SET
   a.purchase_order_type = b.procurement_order_type
;
DELETE FROM PO_Type_@v_countryPrefix@ WHERE id_procurement_order_item = 0;

UPDATE            Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
       INNER JOIN PO_Type_@v_countryPrefix@ AS b
	        USING ( id_procurement_order_item )
SET 		
   a.purchase_order_type = b.purchase_order_type
;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date)
SELECT 
  '@v_country@', 
  'ODS.Id_Procurement_Order_Item',
  "purchase_order_type",
  NOW(),
  NOW()
;