UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS Id_Procurement_Order_Item_Sample
    INNER JOIN Id_Procurement_Order_Item_Key_Map_Sample_@v_countryPrefix@
	    USING ( country , Id_Procurement_Order_Item )
    INNER JOIN @procurement_live@.procurement_order_item
	    USING ( Id_Procurement_Order_Item )
SET
   /*Defaults and Simple Extraction*/
   Id_Procurement_Order_Item_Sample.is_receipt      = procurement_order_item.sku_received,
   Id_Procurement_Order_Item_Sample.is_deleted      = procurement_order_item.is_deleted,
   Id_Procurement_Order_Item_Sample.cost_oms           = procurement_order_item.unit_price,
   Id_Procurement_Order_Item_Sample.cost_oms_after_tax = procurement_order_item.price_before_tax,
   Id_Procurement_Order_Item_Sample.tax             = procurement_order_item.tax;   