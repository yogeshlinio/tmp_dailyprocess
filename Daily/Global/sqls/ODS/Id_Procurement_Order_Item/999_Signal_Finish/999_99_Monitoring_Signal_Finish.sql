INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'ODS.Id_Procurement_Order_Item',
  "finish",
  NOW(),
  NOW(),
  count(*),
  count(*)
FROM
   ODS.Id_Procurement_Order_Item_Sample_@v_countryPrefix@
WHERE 
   Country = '@v_countryPrefix@'   
;


