####eliminar query de ejemplo
call  production.monitoring_step( "ODS.itens_venda_id", "@v_country@", "fulfillment_type_real" , @v_times@ );
DROP TEMPORARY TABLE IF EXISTS Fulfillment_@v_countryPrefix@;
CREATE TEMPORARY TABLE Fulfillment_@v_countryPrefix@ ( INDEX ( item_id ), KEY ( id_procurement_order_item ) )
SELECT 
    item_id,
	0 as id_procurement_order_item,
    fulfillment_type_real
FROM 
    itens_venda_oot_sample_@v_countryPrefix@          AS sample
;

UPDATE            Fulfillment_@v_countryPrefix@                    AS Fulfillment
       INNER JOIN Id_Procurement_Order_Item_Key_Map_Sample_@v_countryPrefix@ AS Key_Map
	        USING ( item_id  )
SET
    Fulfillment.id_procurement_order_item = Key_Map.id_procurement_order_item
;
DELETE FROM  Fulfillment_@v_countryPrefix@ WHERE id_procurement_order_item = 0;

UPDATE            Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
       INNER JOIN Fulfillment_@v_countryPrefix@ AS b
	       USING ( id_procurement_order_item )
SET
	a.fulfillment_type_WMS = b.fulfillment_type_real
;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date)
SELECT 
  '@v_country@', 
  'ODS.Id_Procurement_Order_Item',
  "Fulfiilment_Type_WMS",
  NOW(),
  NOW()
;
