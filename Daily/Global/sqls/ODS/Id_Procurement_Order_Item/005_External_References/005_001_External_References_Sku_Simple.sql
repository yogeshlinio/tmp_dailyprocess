####eliminar query de ejemplo
CALL production.monitoring_step( "ODS.Sku_Simple", "@v_country@", "CatBP" , 60 );

DROP TABLE IF EXISTS TMP_sku_simple_procurement_@v_countryPrefix@;
CREATE TEMPORARY TABLE TMP_sku_simple_procurement_@v_countryPrefix@ (INDEX (country, sku_simple))
SELECT country,
       Sku_simple,
	   fulfillment_type,
	   Cat1,
	   Cat2,
	   Cat3,
	   Cat_BP,
	   Cat_KPI,
	   Head_Buyer,
	   Buyer,
	   sku_config,
	   sku_name,
	   id_supplier,
	   Supplier,
	   Brand
FROM Sku_Simple_Sample_@v_countryPrefix@
WHERE country='@v_countryPrefix@';

UPDATE            Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
       INNER JOIN Id_Procurement_Order_Item_Key_Map_Sample_@v_countryPrefix@ as c
	        ON a.country=c.country AND a.Id_Procurement_Order_Item=c.Id_Procurement_Order_Item
       INNER JOIN TMP_sku_simple_procurement_@v_countryPrefix@ AS b
	        ON b.country=c.country AND b.Sku_Simple=c.Sku_Simple
SET
	a.fulfillment_type_bob = b.fulfillment_type,
	a.cat_1 = b.Cat1,
 	a.cat_2 = b.Cat2,
 	a.cat_3 = b.Cat3,
 	a.category_bp = Cat_BP,
 	a.category_kpi = Cat_KPI,
 	a.head_buyer = b.Head_Buyer,
 	a.buyer = b.Buyer,
 	a.sku_config = b.sku_config,
 	a.sku_name = b.sku_name,
 	a.supplier_id = b.id_supplier,
 	a.supplier_name = b.Supplier,
 	a.brand = b.Brand
;