DROP TEMPORARY TABLE IF EXISTS Recebimento_Procurement_@v_countryPrefix@;
CREATE TEMPORARY TABLE Recebimento_Procurement_@v_countryPrefix@ (  KEY (  purchase_order ) )
SELECT 
   inbound_document_identificator AS purchase_order,
   itens_recebimento_id,
   c.recebimento_id
FROM   
               @v_wmsprod@.recebimento AS b
	INNER JOIN @v_wmsprod@.itens_recebimento AS c 
		    ON b.recebimento_id = c.recebimento_id
;   
   
UPDATE         Recebimento_Procurement_@v_countryPrefix@ AS a
    INNER JOIN Id_Procurement_Order_Item_Key_Map_Sample_@v_countryPrefix@ AS c
		 USING ( purchase_order ) 
SET
   a.itens_recebimento_id = c.itens_recebimento_id,
   a.recebimento_id       = c.recebimento_id;
