DROP TABLE IF EXISTS Id_Procurement_Order_Item_Key_Map_Sample_@v_countryPrefix@;
CREATE TABLE Id_Procurement_Order_Item_Key_Map_Sample_@v_countryPrefix@ LIKE Id_Procurement_Order_Item_Key_Map_Template_@v_countryPrefix@;
/*INSERT ALL THE PRIMARY KEYS*/
INSERT Id_Procurement_Order_Item_Key_Map_Sample_@v_countryPrefix@
(
  `Country`           ,
  `id_procurement_order_item`,
  id_catalog_simple,
  id_procurement_order,
  id_procurement_order_type,
  purchase_order
)
SELECT 
   '@v_countryPrefix@' AS Country,
   a.id_procurement_order_item,
   a.fk_catalog_simple,
   a.fk_procurement_order,
   b.fk_procurement_order_type,
   concat( b.venture_code, lpad(b.id_procurement_order, 7, 0),b.check_digit) AS purchase_order
FROM 
   @procurement_live@.procurement_order_item AS a
   INNER JOIN @procurement_live@.procurement_order AS b
   ON a.fk_procurement_order=b.id_procurement_order;
   