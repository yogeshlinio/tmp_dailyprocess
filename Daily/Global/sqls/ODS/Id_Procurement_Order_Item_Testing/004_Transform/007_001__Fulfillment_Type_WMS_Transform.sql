DROP TEMPORARY TABLE IF EXISTS Consigment_@v_countryPrefix@;
CREATE TEMPORARY TABLE Consigment_@v_countryPrefix@ (  KEY (  recebimento_id ), KEY ( id_procurement_order_item ) )
SELECT 
   recebimento_id,
   0 id_procurement_order_item
FROM   
     @v_wmsprod@.recebimento e
WHERE (e.inbound_document_type_id = 7 OR e.inbound_document_type_id = 11);
;   
UPDATE            Consigment_@v_countryPrefix@ AS Consigment
       INNER JOIN Id_Procurement_Order_Item_Key_Map_Sample_@v_countryPrefix@ AS Key_Map
	      USING ( recebimento_id )
SET 
   Consigment.id_procurement_order_item = Key_Map.id_procurement_order_item
;
DELETE FROM  Consigment_@v_countryPrefix@ WHERE id_procurement_order_item  = 0;

UPDATE            Consigment_@v_countryPrefix@ AS Consigment
       INNER JOIN Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS Sample
	      USING ( id_procurement_order_item )
SET 
   Sample.fulfillment_type_WMS = 'Consignment'
;
