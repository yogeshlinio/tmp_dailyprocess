DROP TABLE IF EXISTS Id_Procurement_Order_Item_Key_Map_Template_@v_countryPrefix@;
CREATE TABLE `Id_Procurement_Order_Item_Key_Map_Template_@v_countryPrefix@` (
  `Country`                   varchar(150) NOT NULL DEFAULT '',
  `id_procurement_order_item` int(10) unsigned DEFAULT NULL,
  `id_procurement_order`      int(10) unsigned DEFAULT NULL,  
  `id_catalog_simple`         int(10) unsigned DEFAULT NULL,
  `sku_simple`                varchar(255) CHARACTER SET utf8 NOT NULL, 
  `id_procurement_order_type` int(11) DEFAULT NULL,
  `purchase_order`            varchar(150) NOT NULL DEFAULT '',
  `itens_recebimento_id`      int(11) DEFAULT NULL,
  `recebimento_id`      int(11) DEFAULT NULL,
  `estoque_id`      int(11) DEFAULT NULL,
  `item_id`                   int(11) DEFAULT NULL,
  PRIMARY KEY (`Country` , id_procurement_order_item ),
  KEY `sku_simple` (`sku_simple`),
  KEY `Country_Sku_simple` (`Country` ,`sku_simple`),  
  KEY `id_catalog_simple` (`id_catalog_simple`),  
  KEY `Country_id_catalog_simple` (`Country` ,`id_catalog_simple`) ,
  KEY `id_procurement_order` (`id_procurement_order`),  
  KEY `Country_id_procurement_order` (`Country` ,`id_procurement_order`),
  KEY `id_procurement_order_type` (`id_procurement_order_type`),
  KEY `Country_purchase_order` (`Country` ,`purchase_order`),
  KEY `purchase_order` (`purchase_order`),
  KEY `itens_recebimento_id` (`itens_recebimento_id`),
  KEY `recebimento_id` (`recebimento_id`),
  KEY `estoque_id` (`estoque_id`),
  KEY `item_id` (`item_id`),
  KEY `Country_item_id` (`Country` ,`item_id`)
  )
  ENGINE=InnoDB DEFAULT CHARSET=latin1;;


