INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'ODS.itens_venda_id_Key_Map',
  "finish",
  NOW(),
  NOW(),
  count(*),
  count(*)
FROM
   ODS.itens_venda_oot_Key_Map
WHERE 
   Country = '@v_countryPrefix@'   
;


