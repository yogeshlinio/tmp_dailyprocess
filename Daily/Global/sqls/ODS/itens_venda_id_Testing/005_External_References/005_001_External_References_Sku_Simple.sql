CALL production.monitoring_step( "ODS.Sku_Simple", "@v_country@", "Fulfillment_Type" , 50 );

UPDATE            itens_venda_oot_sample_@v_countryPrefix@ AS a
       INNER JOIN itens_venda_oot_key_map_Sample_@v_countryPrefix@ 
	        USING ( country , item_id )
       INNER JOIN Sku_Simple_Sample_@v_countryPrefix@ AS b
	        USING ( country , Sku_Simple )
SET
	a.fulfillment_type_bob = b.fulfillment_type
;

DROP TABLE IF EXISTS fulfillment_type_crossdocking;
CREATE TABLE fulfillment_type_crossdocking (INDEX (item_id))
SELECT b.item_id,
       'Crossdocking' as fulfillment_type_real
FROM itens_venda_oot_key_map_Sample_@v_countryPrefix@ b
JOIN @v_wmsprod@.status_itens_venda c
ON b.itens_venda_id = c.itens_venda_id
WHERE c.STATUS IN (	'analisando quebra','aguardando estoque');
			
UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN fulfillment_type_crossdocking b
ON a.item_id = b.item_id
SET 
   a.fulfillment_type_real = IF(a.fulfillment_type_bob = 'Dropshipping_cod', a.fulfillment_type_bob, b.fulfillment_type_real);
   
   
 UPDATE itens_venda_oot_sample_@v_countryPrefix@
SET 
   fulfillment_type_bp = CASE WHEN fulfillment_type_real IN( 'crossdocking', 'consignment')
				                   THEN fulfillment_type_real
                              WHEN fulfillment_type_real = 'Own Warehouse' and fulfillment_type_bob = 'Consignment'
				                   THEN fulfillment_type_bob
				              WHEN fulfillment_type_real = 'Own Warehouse'
				                   THEN 'Outright Buying'
				              WHEN fulfillment_type_real = 'dropshipping' 
				                   THEN 'other'
				              WHEN fulfillment_type_real is null
				                   THEN fulfillment_type_bob
                         END;
