CALL production.monitoring_step ("ODS.id_procurement_order_item", "@v_country@", "dates_po", 50);

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
JOIN itens_venda_oot_key_map_Sample_@v_countryPrefix@ AS b
ON a.item_id=b.item_id
JOIN @v_wmsprod@.estoque AS c
ON b.estoque_id=c.estoque_id
JOIN @v_wmsprod@.itens_recebimento AS d
ON c.itens_recebimento_id=d.itens_recebimento_id
JOIN @procurement_live@.wms_received_item AS e 
ON d.itens_recebimento_id = e.id_wms
JOIN Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS f
ON e.fk_procurement_order_item = f.id_procurement_order_item
SET 
a.date_po_created = date(f.date_po_created),
a.datetime_po_created = f.date_po_created,
a.date_po_updated  = date(f.date_po_created),
a.datetime_po_updated = f.date_po_updated,
a.date_po_issued = date(f.date_po_issued),
a.datetime_po_issued = f.date_po_issued,
a.date_po_confirmed = date(f.date_po_confirmed),
a.datetime_po_confirmed = f.date_po_confirmed;