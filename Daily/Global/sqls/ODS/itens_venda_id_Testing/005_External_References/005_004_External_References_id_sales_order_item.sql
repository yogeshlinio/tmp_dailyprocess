CALL production.monitoring( "A_Master" , "@v_country@",  50 );

#Deberia salir de las nuevas tablas...
UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
JOIN development_@v_countryPrefix@.A_Master AS b
ON a.item_id=b.ItemID
SET
a.tax_percent = b.Tax,
a.cost = b.Cost,
a.cost_w_o_vat = b.CostAfterTax,
a.price = b.Price,
a.price_w_o_vat = b.PriceAfterTax,
a.is_marketplace = b.isMPlace,
a.corporate_sale = if(b.CouponCode LIKE 'VC%',1,0),
a.coupon_value = if(b.CouponCode like 'REP%',b.CouponValue,0), 
a.customer_first_name = b.FirstName,
a.customer_last_name = b.LastName,
a.payment_method = b.PaymentMethod;

/* CAMBIAR POR ESTE CUANDO ESTE LISTO
UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
JOIN Id_Sales_Order_Item_Sample_@v_countryPrefix@ AS b
ON a.item_id=b.Id_Sales_Order_Item
SET
a.tax_percent = b.Tax_Percent,
a.cost = b.Cost,
a.cost_w_o_vat = b.Cost_After_Tax,
a.price = b.Price,
a.price_w_o_vat = b.Price_After_Tax,
a.is_marketplace = b.isMPlace,
a.corporate_sale = if(b.coupon_code LIKE 'VC%',1,0),
a.coupon_value = if(b.coupon_code like 'REP%',b.Coupon_Value,0), 
a.customer_first_name = b.customer_first_name,
a.customer_last_name = b.customer_last_name,
a.payment_method = b.Payment_Method;
*/