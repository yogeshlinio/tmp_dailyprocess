CALL production.monitoring_step( "ODS.itens_venda_id", "@v_country@", "date_delivered" , 50 );

#Actualiza historicamente
UPDATE itens_venda_oot_@v_countryPrefix@ AS a
JOIN TMP_cod_rastreamento_Item_@v_countryPrefix@ AS b
     USING ( item_id )
SET 
    a.date_delivered =  b.date_delivered
WHERE a.date_delivered IS NULL;