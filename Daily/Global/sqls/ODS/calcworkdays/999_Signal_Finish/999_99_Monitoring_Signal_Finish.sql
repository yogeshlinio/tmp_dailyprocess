INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'operations_@v_countryPrefix@.calcworkdays',
  'finish',
  NOW(),
  NOW(),
  0,
  0
;