DROP TABLE IF EXISTS Stock_Item_Id_Sample_@v_countryPrefix@;
CREATE TABLE Stock_Item_Id_Sample_@v_countryPrefix@ LIKE Stock_Item_Id_Template;

INSERT INTO Stock_Item_Id_Sample_@v_countryPrefix@ (
	stock_item_id,
	barcode_wms,
	wh_location,
	sub_location,
	barcode_bob_duplicated,
    reserved
) 
SELECT
	estoque.estoque_id,
	estoque.cod_barras,
	estoque.endereco,
	estoque.sub_endereco,	
	estoque.minucioso,
    IF(estoque.almoxarifado = "separando"
			OR estoque.almoxarifado = "estoque reservado"
			OR estoque.almoxarifado = "aguardando separacao", 1, 0)
FROM	@v_wmsprod@.estoque
;
