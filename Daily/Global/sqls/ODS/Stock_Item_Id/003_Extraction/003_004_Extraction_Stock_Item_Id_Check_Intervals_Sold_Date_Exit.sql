UPDATE            Stock_Item_Id_Sample_@v_countryPrefix@ AS Stock_Item_Id
SET 
  sold_last_60 = CASE
					WHEN datediff(curdate(), date_exit) < 60 
					THEN 1
					ELSE 0
				 END,
  sold_last_30 = CASE
					WHEN datediff(curdate(), date_exit) < 30 
					THEN 1
					ELSE 0
				END,
  sold_last_7 = CASE
				    WHEN datediff(curdate(), date_exit) < 7 
					THEN 1
					ELSE 0
				END,
  sold_yesterday = 	CASE
					WHEN datediff(curdate(),(date_sub(date_exit, INTERVAL 1 DAY))) = 0 
					THEN 1
					ELSE 0
					END,
  entrance_last_30 = CASE
						WHEN datediff(curdate(), date_entrance) < 30 
							THEN 1
							ELSE 0
					END;
