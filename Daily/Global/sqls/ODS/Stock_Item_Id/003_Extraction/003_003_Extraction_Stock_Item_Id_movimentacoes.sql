/*
Fecha y datos de cuando sale vendido un producto
*/
UPDATE            Stock_Item_Id_Sample_@v_countryPrefix@
       INNER JOIN @v_wmsprod@.movimentacoes 
               ON Stock_Item_Id_Sample_@v_countryPrefix@.stock_item_id = movimentacoes.estoque_id 
SET 
	Stock_Item_Id_Sample_@v_countryPrefix@.date_exit = data_criacao,
	Stock_Item_Id_Sample_@v_countryPrefix@.exit_type = "sold",
	Stock_Item_Id_Sample_@v_countryPrefix@.sold = 1
WHERE 
		Stock_Item_Id_Sample_@v_countryPrefix@.wh_location ="vendidos"
	AND movimentacoes.para_endereco="vendidos";

/*
Fecha y datos de cuando sale errado un producto
*/
UPDATE            Stock_Item_Id_Sample_@v_countryPrefix@
       INNER JOIN @v_wmsprod@.movimentacoes 
               ON Stock_Item_Id_Sample_@v_countryPrefix@.stock_item_id = movimentacoes.estoque_id 
SET 
	Stock_Item_Id_Sample_@v_countryPrefix@.date_exit = data_criacao,
	Stock_Item_Id_Sample_@v_countryPrefix@.exit_type = "error"
WHERE 
		Stock_Item_Id_Sample_@v_countryPrefix@.wh_location ="Error_entrada"
	AND movimentacoes.para_endereco="Error_entrada";
	
/*
* Location Before Sold
*/	
UPDATE            Stock_Item_Id_Sample_@v_countryPrefix@ 
       INNER JOIN @v_wmsprod@.movimentacoes 
	           ON Stock_Item_Id_Sample_@v_countryPrefix@ .stock_item_id = movimentacoes.estoque_id 
SET 
    Stock_Item_Id_Sample_@v_countryPrefix@ .wh_location_before_sold = de_endereco
WHERE 
    movimentacoes.para_endereco="vendidos";	
	
/*
*  Intio days_in_stock
*/
UPDATE Stock_Item_Id_Sample_@v_countryPrefix@
SET 
    days_in_stock = 	CASE
							WHEN 	date_exit IS NULL 
						    THEN	datediff(curdate(), date_entrance)
							ELSE	datediff(date_exit, date_entrance)
						END;	
		
/*
*   
*/		
DROP TEMPORARY TABLE IF EXISTS tmp_movimentacoes_@v_countryPrefix@;
CREATE TEMPORARY TABLE tmp_movimentacoes_@v_countryPrefix@ (PRIMARY KEY (estoque_id))
SELECT 
								estoque_id, 
								para_endereco, 
								min(date(data_criacao)) AS data_movimentacao 
							FROM @v_wmsprod@.movimentacoes 
							WHERE 
							        para_endereco LIKE '%DEF%' 
							   OR 	para_endereco LIKE '%REP%' 
							   OR	para_endereco LIKE '%GRT%' 
							   OR 	para_endereco LIKE '%AVE%' 
							   OR 	para_endereco LIKE '%cuerentena%'
							GROUP BY estoque_id;

UPDATE            Stock_Item_Id_Sample_@v_countryPrefix@ AS t
       INNER JOIN tmp_movimentacoes_@v_countryPrefix@ t2
               ON t.stock_item_id = t2.estoque_id
SET 
    t.quarantine_date = t2.data_movimentacao;
	
	
  
/*
*  Insert Monitoring LOG
*/
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date
  )
SELECT 
  '@v_country@', 
  'ODS.Stock_Item_Id',
  "days_in_stock",
  NOW(),
  NOW()
;	