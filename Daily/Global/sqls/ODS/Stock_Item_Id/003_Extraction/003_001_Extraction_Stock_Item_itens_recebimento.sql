/*
* UPDATE Sku_Simple.supplier
*/          
DROP TEMPORARY TABLE IF EXISTS Stock_Itens_Recebemento_@v_countryPrefix@;
CREATE TEMPORARY TABLE Stock_Itens_Recebemento_@v_countryPrefix@ (  PRIMARY KEY   ( Stock_Item_Id  ) )
SELECT 
   Key_Map.Stock_Item_Id,
   data_criacao
FROM   
               Stock_Item_Id_Key_Map_Sample_@v_countryPrefix@ AS Key_Map
    INNER JOIN @v_wmsprod@.itens_recebimento 
        USING ( itens_recebimento_id )
;
	   
UPDATE        Stock_Item_Id_Sample_@v_countryPrefix@    AS Stock_Item_Id
   INNER JOIN Stock_Itens_Recebemento_@v_countryPrefix@ AS Stock_Itens
	    USING ( stock_item_id )   
SET
    Stock_Item_Id.date_entrance = date(Stock_Itens.data_criacao)
;
