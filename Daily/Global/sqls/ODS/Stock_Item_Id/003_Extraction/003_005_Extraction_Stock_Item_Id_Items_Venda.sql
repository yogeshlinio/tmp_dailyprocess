call  production.monitoring_step( "ODS.itens_venda_id", "@v_country@", "status_value_chain" , @v_times@ );

DROP TEMPORARY TABLE IF EXISTS Stock_Itens_Venda_@v_countryPrefix@;
CREATE TEMPORARY TABLE Stock_Itens_Venda_@v_countryPrefix@ (  PRIMARY KEY   ( Stock_Item_Id  ) )
SELECT 
   Key_Map.Stock_Item_Id,
   itens_venda.status_value_chain,
   itens_venda.date_ordered				  
FROM   
               Stock_Item_Id_Key_Map_Sample_@v_countryPrefix@ AS Key_Map
    INNER JOIN itens_venda_oot_@v_countryPrefix@              AS itens_venda
        USING ( item_id )
WHERE 
    item_id != 0
;
	   
UPDATE            Stock_Item_Id_Sample_@v_countryPrefix@ AS Stock_Item_Id
       INNER JOIN Stock_Itens_Venda_@v_countryPrefix@ as itens_venda
	        USING ( Stock_Item_Id )
SET
    Stock_Item_Id.status_item  = itens_venda.status_value_chain,
	Stock_Item_Id.date_ordered = itens_venda.date_ordered				  
;
