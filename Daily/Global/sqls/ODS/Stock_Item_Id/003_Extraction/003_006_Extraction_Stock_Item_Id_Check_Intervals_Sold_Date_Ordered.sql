UPDATE            Stock_Item_Id_Sample_@v_countryPrefix@ AS Stock_Item_Id
SET 
  sold_last_7_order = CASE
					WHEN datediff(curdate(), date_ordered) <= 7
					THEN 1
					ELSE 0
				 END,
  sold_last_15_order = CASE
					WHEN datediff(curdate(), date_ordered) <= 15 
					THEN 1
					ELSE 0
				END,
  sold_last_30_order = CASE
				    WHEN datediff(curdate(), date_ordered) <= 30
					THEN 1
					ELSE 0
				END;


