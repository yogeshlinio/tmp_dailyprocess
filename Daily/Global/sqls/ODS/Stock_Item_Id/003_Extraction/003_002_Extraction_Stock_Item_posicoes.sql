DROP TEMPORARY TABLE IF EXISTS posicoes_@v_countryPrefix@;
CREATE TEMPORARY TABLE  posicoes_@v_countryPrefix@ (  KEY ( stock_item_id  ) )
SELECT 
    Stock_Item_Id_Key_Map.country , 
	Stock_Item_Id_Key_Map.stock_item_id,
    posicoes.participa_estoque,
    posicoes.tipo_posicao,
	CASE
		WHEN posicoes.tipo_posicao = "safe" 
		THEN "small"
		WHEN tipo_posicao = "mezanine" 
		THEN "small"
		WHEN tipo_posicao = "muebles" 
		THEN "large"
	ELSE "tbd"
	END AS position_item_size_type
FROM
                  Stock_Item_Id_Key_Map_Sample_@v_countryPrefix@ AS Stock_Item_Id_Key_Map
       INNER JOIN @v_wmsprod@.posicoes 
	        USING ( posicao )
											
;

UPDATE            Stock_Item_Id_Sample_@v_countryPrefix@ AS Stock_Item_Id
       INNER JOIN posicoes_@v_countryPrefix@ AS posicoes
	        USING ( stock_item_id )
SET
    Stock_Item_Id.in_stock      = posicoes.participa_estoque,
    Stock_Item_Id.position_type = posicoes.tipo_posicao,
	Stock_Item_Id.position_item_size_type = CASE
												WHEN posicoes.tipo_posicao = "safe" 
													THEN "small"
													WHEN tipo_posicao = "mezanine" 
	    											THEN "small"
													WHEN tipo_posicao = "muebles" 
													THEN "large"
		               								ELSE "tbd"
											END;
											
											
											
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date)
SELECT 
  '@v_country@', 
  'ODS.Stock_Item_Id',
  "in_stock",
  NOW(),
  NOW()
;											