DROP TABLE IF EXISTS Stock_Item_Id_Key_Map_Template;
CREATE TABLE `Stock_Item_Id_Key_Map_Template` (
  `country` varchar(10) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `stock_item_id` int(11) unsigned NOT NULL DEFAULT '0',
  `barcode_wms` varchar(200) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `itens_recebimento_id` int(11) unsigned NOT NULL DEFAULT '0',
  `sku_simple` varchar(200) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `posicao` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `item_id` int(11) unsigned NOT NULL DEFAULT '0',
  
  PRIMARY KEY (`Country` , stock_item_id ),
  KEY `Country_barcode_wms` (Country, barcode_wms),
  KEY `barcode_wms` (`barcode_wms`),
  KEY `Country_itens_recebimento_id` ( Country , itens_recebimento_id),
  KEY `itens_recebimento_id` (`itens_recebimento_id`),
  KEY Country_Sku_Simple (`Country` , sku_simple ),
  KEY `Sku_Simple`       (`sku_simple`),
  KEY Country_posicao (`Country` , posicao ),
  KEY `posicao`       (`posicao`),
  KEY Country_item_id (`Country` , item_id ),
  KEY `item_id`       (`item_id`)
  
) Engine=InnoDB;