DROP TABLE IF EXISTS `Stock_Item_Id_Template`;
CREATE TABLE `Stock_Item_Id_Template` (

  `Country`       varchar(50) NOT NULL,
  `stock_item_id` INT NOT NULL,
  `sku_simple` varchar(255) DEFAULT NULL,
  `sku_config` varchar(255) DEFAULT NULL,
 
  `date_entrance` date DEFAULT NULL,
  `date_exit` date DEFAULT NULL,
  `exit_type` varchar(255) DEFAULT NULL,
  `days_in_stock` int(11) DEFAULT NULL,
  `cost` float DEFAULT NULL,
  `cost_w_o_vat` float DEFAULT NULL,
  `in_stock` bit(1) DEFAULT b'0',
  `sold_yesterday` bit(1) DEFAULT b'0',
  `sold_last_7` bit(1) DEFAULT b'0',
  `sold_last_30` bit(1) DEFAULT b'0',
  `sold_last_30_cost_w_o_vat` float DEFAULT NULL,
  `sold_last_30_price` float DEFAULT NULL,
  `sold_last_30_counter` float DEFAULT '0',
  `sold_last_60` bit(1) DEFAULT b'0',
  
  `sold_last_7_order` bit(1) DEFAULT b'0',  
  `sold_last_15_order` bit(1) DEFAULT b'0',  
  `sold_last_30_order` bit(1) DEFAULT b'0',

  `barcode_wms`  varchar(255),
   barcode_bob_duplicated varchar(255),  
  
  `wh_location` varchar(255) DEFAULT NULL,
  `sub_location` int DEFAULT NULL,
  `wh_location_before_sold` varchar(255) NOT NULL,
  `fulfillment_type_wms` varchar(255) NOT NULL,
  `fulfillment_type_bp` varchar(255) NOT NULL,  
  
  `reserved` bit(1) DEFAULT b'0',
  `quarantine` bit(1) DEFAULT b'0',
  `quarantine_type` varchar(255) DEFAULT NULL,
  `quarantine_date` date DEFAULT NULL,
  `quarantine_inbound` bit(1) DEFAULT b'0',
  `position_type` text,
  `position_item_size_type` text,
  `sold` bit(1) DEFAULT b'0',
  `max_days_in_stock` int(11) DEFAULT NULL,
  `entrance_last_30` bit(1) DEFAULT b'0',
  `is_sell_list` bit(1) DEFAULT b'0',
  `is_sell_list_30` bit(1) DEFAULT b'0',
  `date_ordered` datetime DEFAULT NULL,
  `status_item` varchar(255) DEFAULT NULL,
  `content_qc` varchar(255) DEFAULT NULL,
  `status_sku` varchar(255) DEFAULT NULL,
  `reason_not_visible` varchar(255) DEFAULT NULL,

  `cancelled`       bit(1) DEFAULT b'0',
  `times_cancelled` int  DEFAULT '0',
  
  `sku_simple_blank` bit(1) DEFAULT b'1',
  `sku_in_stock` bit(1) DEFAULT b'0',
  `sales_rate_config_42` decimal(21,10) DEFAULT '0.0000000000',
  `sales_rate_config_30` decimal(21,10) DEFAULT '0.0000000000',
  `sales_rate_config_7` decimal(21,10) DEFAULT '0.0000000000',
  
  `pc_15` decimal(21,10) DEFAULT NULL,
  `pc_15_percentage` decimal(21,10) DEFAULT NULL,
  PRIMARY KEY ( Country, `stock_item_id`),
  KEY (`stock_item_id`),
  KEY ( Country, `sku_simple`),
  KEY (`sku_simple`),
  KEY (`sku_config`)  
  
)
ENGINE=InnoDB DEFAULT CHARSET=latin1;