call  production.monitoring_step( "ODS.Sku_Simple", "@v_country@", "cost_oms" , @v_times@ );
DROP TEMPORARY TABLE IF EXISTS Sku_Simple_Stock_@v_countryPrefix@;
CREATE TEMPORARY TABLE Sku_Simple_Stock_@v_countryPrefix@ ( PRIMARY KEY (  Stock_Item_Id ) )
ENGINE=InnoDB
SELECT 
    Stock_Item_Id_Key_Map.Stock_Item_Id,
	Stock_Item_Id_Key_Map.Sku_Simple,
	space(30) as SKU_Config,
	00000.00000 AS Tax_Percent,
	00000.00000 AS Tax_Percent_Factor,
    0000000000000000.00 AS Cost_OMS,
    0000000000000000.00 AS Cost
FROM
                  Stock_Item_Id_Key_Map_Sample_@v_countryPrefix@                  AS Stock_Item_Id_Key_Map
;				  

UPDATE 	           Sku_Simple_Stock_@v_countryPrefix@    AS Sku_Stock
	   INNER JOIN Sku_Simple_Sample_@v_countryPrefix@    AS Sku_Simple
			USING ( Sku_Simple  )
SET 
	Sku_Stock.Sku_Config         = Sku_Simple.Sku_Config,
	Sku_Stock.Tax_Percent        = Sku_Simple.Tax_Percent,
	Sku_Stock.Tax_Percent_Factor = ( 1 + Sku_Simple.Tax_Percent / 100 ),
    Sku_Stock.Cost_OMS = IF (      special_purchase_price IS NULL  
                              OR ( NOT curdate() BETWEEN Sku_Simple.special_from_date AND Sku_Simple.special_to_date ),
		                           Sku_Simple.cost_oms,
	                               LEAST( Sku_Simple.special_purchase_price , Sku_Simple.cost_oms )
		                     ),
    Sku_Stock.cost     = Sku_Simple.cost 							 
;				  

UPDATE            Stock_Item_Id_Sample_@v_countryPrefix@ AS Stock_Item_Id
       INNER JOIN Sku_Simple_Stock_@v_countryPrefix@     AS Sku_Simple
            USING ( stock_item_id )
SET 
    Stock_Item_Id.sku_simple         = Sku_Simple.Sku_Simple  ,
    Stock_Item_Id.Sku_Config         = Sku_Simple.Sku_Config  ,	
	Stock_Item_Id.sku_simple_blank   = 1,	
    Stock_Item_Id.cost               = IF( Sku_Simple.Cost_OMS <= 0 , Sku_Simple.cost , Sku_Simple.Cost_OMS  ) ,
    Stock_Item_Id.cost_w_o_vat       = IF( Sku_Simple.Cost_OMS <= 0 , Sku_Simple.cost , Sku_Simple.Cost_OMS  ) / Tax_Percent_Factor
;

/*
*  Insert Monitoring LOG
*/
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date)
SELECT 
  '@v_country@', 
  'ODS.Stock_Item_Id',
  "sku_simple",
  NOW(),
  NOW()
;

