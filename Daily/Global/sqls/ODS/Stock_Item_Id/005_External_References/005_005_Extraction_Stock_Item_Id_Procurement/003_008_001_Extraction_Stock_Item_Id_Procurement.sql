call  production.monitoring_step( "ODS.Id_Procurement_Order_Item", "@v_country@", "Cost_oms"          , @v_times@);
DROP TEMPORARY TABLE IF EXISTS Procurement_Stock_@v_countryPrefix@;
CREATE TEMPORARY TABLE Procurement_Stock_@v_countryPrefix@ ( KEY ( Id_Procurement_Order_Item  ),  KEY ( stock_item_id  )  )
ENGINE=INNODB
SELECT
    a.Id_Procurement_Order_Item,
    a.estoque_id AS stock_item_id,
	00000000000000000.00 AS cost_oms,
	00000000000000000.00 AS cost_oms_after_tax
FROM	
               Id_Procurement_Order_Item_Key_Map_Sample_@v_countryPrefix@ AS a
    INNER JOIN Stock_Item_Id_Key_Map_Sample_@v_countryPrefix@             AS b
	        ON a.estoque_id = b.stock_item_id
;
 UPDATE            Procurement_Stock_@v_countryPrefix@                        AS a
        INNER JOIN Id_Procurement_Order_Item_Sample_@v_countryPrefix@         AS b
		     USING ( Id_Procurement_Order_Item )
SET 
   a.cost_oms           = b.cost_oms,
   a.cost_oms_after_tax = b.cost_oms_after_tax
;


UPDATE         Stock_Item_Id_Sample_@v_countryPrefix@  AS a 
    INNER JOIN Procurement_Stock_@v_countryPrefix@     AS b
	     USING ( stock_item_id )
SET
	 a.cost          = if(b.cost_oms <= 0 OR b.cost_oms IS NULL,a.cost,        b.cost_oms),
	 a.cost_w_o_vat  = if(b.cost_oms_after_tax <= 0 OR b.cost_oms_after_tax IS NULL,a.cost_w_o_vat,b.cost_oms_after_tax)
	 
;
