CALL production.monitoring_step( "ODS.itens_venda_id", "@v_country@", 'is_marks' , @v_times@ );
DROP TEMPORARY TABLE IF EXISTS TMP_Cancelled_@v_countryPrefix@;
CREATE TEMPORARY TABLE TMP_Cancelled_@v_countryPrefix@ ( PRIMARY KEY ( stock_item_id ) )
SELECT
    estoque_id as stock_item_id,
    sum( is_cancelled ) as times_cancelled
FROM	
                  itens_venda_oot_key_map_Sample_@v_countryPrefix@
       INNER JOIN itens_venda_oot_sample_@v_countryPrefix@ as itens_venda
	        USING ( item_id )
WHERE 			
    estoque_id >= 0
GROUP BY estoque_id;

UPDATE            Stock_Item_Id_Sample_@v_countryPrefix@ AS Stock_Item_Id
       INNER JOIN TMP_Cancelled_@v_countryPrefix@ as itens_venda
	        USING ( stock_item_id )
SET
    Stock_Item_Id.cancelled        = IF( itens_venda.times_cancelled > 0 , 1, 0 ),
    Stock_Item_Id.times_cancelled  = itens_venda.times_cancelled
	
;

