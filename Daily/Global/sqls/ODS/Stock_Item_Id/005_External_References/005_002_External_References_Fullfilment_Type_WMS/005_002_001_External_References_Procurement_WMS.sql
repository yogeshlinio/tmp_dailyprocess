call  production.monitoring_step( 'ODS.Id_Procurement_Order_Item' , "@v_country@", "Fulfiilment_Type_WMS" , @v_times@ );
DROP TEMPORARY TABLE IF EXISTS Fulfillment_@v_countryPrefix@;
CREATE TEMPORARY TABLE Fulfillment_@v_countryPrefix@ ( INDEX ( stock_item_id ) )
SELECT 
	stock_item_id,
    fulfillment_type_WMS
FROM 
    Id_Procurement_Order_Item_Sample_@v_countryPrefix@          AS sample
WHERE 
       stock_item_id != 0 
   AND stock_item_id IS NOT NULL
;

UPDATE            Stock_Item_Id_Sample_@v_countryPrefix@ AS a
       INNER JOIN Fulfillment_@v_countryPrefix@ AS b
	       USING ( stock_item_id )
SET
	a.fulfillment_type_WMS  = b.fulfillment_type_WMS
;


