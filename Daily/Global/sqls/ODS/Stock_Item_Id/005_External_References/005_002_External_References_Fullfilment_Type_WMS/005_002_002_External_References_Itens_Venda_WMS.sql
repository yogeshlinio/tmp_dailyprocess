call  production.monitoring_step( "ODS.itens_venda_id", "@v_country@", "fulfillment_type_real" , @v_times@ );
DROP TEMPORARY TABLE IF EXISTS Fulfillment_@v_countryPrefix@;
CREATE TEMPORARY TABLE Fulfillment_@v_countryPrefix@ ( INDEX ( item_id ), KEY ( stock_item_id ) )
SELECT 
    item_id,
	0 as stock_item_id,
    fulfillment_type_real_stock as fulfillment_type_real
FROM 
    itens_venda_oot_sample_@v_countryPrefix@          AS sample
where 
    fulfillment_type_real_stock != ""
;

UPDATE            Fulfillment_@v_countryPrefix@                    AS Fulfillment
       INNER JOIN Stock_Item_Id_Key_Map_Sample_@v_countryPrefix@ AS Key_Map
	        USING ( item_id  )
SET
    Fulfillment.stock_item_id = Key_Map.stock_item_id
;
DELETE FROM  Fulfillment_@v_countryPrefix@ WHERE stock_item_id = 0;

UPDATE            Stock_Item_Id_Sample_@v_countryPrefix@ AS a
       INNER JOIN Fulfillment_@v_countryPrefix@ AS b
	       USING ( stock_item_id )
SET
	a.fulfillment_type_WMS  = b.fulfillment_type_real
;

UPDATE            Stock_Item_Id_Sample_@v_countryPrefix@ AS a
SET
	a.fulfillment_type_WMS  = "Own Warehouse"
WHERE fulfillment_type_WMS = "" OR fulfillment_type_WMS is null
;


UPDATE            Stock_Item_Id_Sample_@v_countryPrefix@ AS a
SET 
    fulfillment_type_bp = CASE
				WHEN fulfillment_type_WMS IN( 'crossdocking', 'consignment')
				THEN fulfillment_type_WMS
				WHEN fulfillment_type_WMS = 'Own Warehouse'
				THEN 'Outright Buying'
				WHEN fulfillment_type_WMS = 'dropshipping' 
				THEN 'other'
END;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date)
SELECT 
  '@v_country@', 
  'ODS.Stock_Item_Id',
  "Fulfiilment_Type_WMS",
  NOW(),
  NOW()
;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date)
SELECT 
  '@v_country@', 
  'ODS.Stock_Item_Id',
  "fulfillment_type_bp",
  NOW(),
  NOW()
;



