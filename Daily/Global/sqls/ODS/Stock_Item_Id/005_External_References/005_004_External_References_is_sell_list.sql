CALL production.monitoring_step( "ODS.itens_venda_id", "@v_country@", 'fulfillment_type_real' , @v_times@ );
CALL production.monitoring_step( "ODS.Sku_Simple", "@v_country@", 'Cat1' , @v_times@ );
CALL production.monitoring_step( "ODS.Sku_Simple", "@v_country@", "in_stock_WMS", @v_times@ );
CALL production.monitoring_step( "ODS.Stock_Item_Id", "@v_country@", "Fulfiilment_Type_WMS", @v_times@ );

DROP TEMPORARY TABLE IF EXISTS Not_Consignment_Procument_@v_countryPrefix@;
CREATE TEMPORARY TABLE Not_Consignment_Procument_@v_countryPrefix@ 
( 
   key ( sku_config ),
   key ( Stock_Item_Id )   
)
SELECT 
   sample.stock_item_id,
   sample.sku_config as sku_config,
   space(22) AS category_1,
   fulfillment_type_wms,
   0 AS  max_days_in_stock_WMS,
   0 AS  average_remaining_days,
   0 AS sold_42_wms
FROM
   Stock_Item_Id_Sample_@v_countryPrefix@ AS sample
WHERE 		 
       fulfillment_type_wms <> 'Consignment' 
   AND in_stock= 1
   AND reserved= 0
	
;	

DROP TEMPORARY TABLE IF EXISTS Sku_Config_@v_countryPrefix@;
CREATE TEMPORARY TABLE Sku_Config_@v_countryPrefix@ ( PRIMARY KEY ( Sku_Config ) )
SELECT 
    Sku_Config,
	Cat1,
	SUM( stock_WMS - reserved_WMS ) AS items_available_WMS,
	SUM( sold_42_WMS ) AS sold_42_WMS ,
    AVG( average_days_in_stock_WMS ) average_days_in_stock_WMS
FROM
    Sku_Simple_Sample_@v_countryPrefix@   
GROUP BY Sku_Config;	
	
	 
UPDATE            Not_Consignment_Procument_@v_countryPrefix@ AS Not_Consignment
	   INNER JOIN Sku_Config_@v_countryPrefix@         AS Sku_Config
               ON Not_Consignment.Sku_Config = Sku_Config.Sku_Config
SET 
    Not_Consignment.category_1 = Sku_Config.Cat1,
	Not_Consignment.max_days_in_stock_WMS = Sku_Config.average_days_in_stock_WMS,
	Not_Consignment.average_remaining_days = round( Sku_Config.items_available_WMS / round( Sku_Config.sold_42_WMS / 42 ,2 ) , 2 )  ,
	Not_Consignment.sold_42_WMS = Sku_Config.sold_42_WMS
;


UPDATE             Not_Consignment_Procument_@v_countryPrefix@    AS Not_Consignment
        INNER JOIN Stock_Item_Id_Sample_@v_countryPrefix@         AS Stock_Item_Id 
		     USING ( Stock_Item_Id )
SET
   is_sell_list = 1
WHERE 
        max_days_in_stock_WMS > 30
   AND (     ( category_1 = 'Home and Living' AND (   Not_Consignment.average_remaining_days > 150 
                                                   or Not_Consignment.sold_42_WMS = 0))
          OR ( category_1 = 'Fashion'         AND (   Not_Consignment.average_remaining_days > 120 
		                                           or Not_Consignment.sold_42_WMS = 0))
          OR ( category_1 = 'Electrónicos'    AND (   Not_Consignment.average_remaining_days >  60 
		                                           or Not_Consignment.sold_42_WMS = 0))  
          OR ( category_1 not in ('Home and Living','Fashion','Electrónicos')
                                     		  AND (   Not_Consignment.average_remaining_days >  90 
											       or Not_Consignment.sold_42_WMS = 0))
		)		  
;