UPDATE            Stock_Item_Id_Sample_@v_countryPrefix@ AS Stock_Item_Id
       INNER JOIN Stock_Item_Id_Key_Map_Sample_@v_countryPrefix@                  AS Stock_Item_Id_Key_Map
            USING ( stock_item_id )
SET
   Stock_Item_Id.wh_location     = Stock_Item_Id_Key_Map.posicao,
   Stock_Item_Id.quarantine      = 1,
   Stock_Item_Id.quarantine_type = Stock_Item_Id_Key_Map.posicao
WHERE	
        posicao LIKE '%cuarent%'
OR		posicao LIKE '%merma%'
;
