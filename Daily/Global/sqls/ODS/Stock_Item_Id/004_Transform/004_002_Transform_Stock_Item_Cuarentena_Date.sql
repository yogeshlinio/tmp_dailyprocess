DROP TEMPORARY TABLE IF EXISTS tmp_movimentacoes_@v_countryPrefix@;
CREATE TEMPORARY TABLE tmp_movimentacoes_@v_countryPrefix@ (PRIMARY KEY (estoque_id))
SELECT 
								estoque_id, 
								para_endereco, 
								min(date(data_criacao)) AS data_movimentacao 
							FROM @v_wmsprod@.movimentacoes 
							WHERE   para_endereco LIKE '%DEF%' 
							OR 		para_endereco LIKE '%REP%' 
							OR		para_endereco LIKE '%GRT%' 
							OR 		para_endereco LIKE '%AVE%' 
							OR 		para_endereco LIKE '%cuerentena%'
							GROUP BY estoque_id;

UPDATE             Stock_Item_Id_Sample_@v_countryPrefix@ AS Stock_Item_Id
        INNER JOIN tmp_movimentacoes_@v_countryPrefix@ t2
                ON 	Stock_Item_Id.stock_item_id = t2.estoque_id
SET 
    Stock_Item_Id.quarantine_date = t2.data_movimentacao;

UPDATE Stock_Item_Id_Sample_@v_countryPrefix@
SET 
   quarantine_date=date_entrance,
   quarantine_inbound = 1
WHERE 
    quarantine=1 
and quarantine_date IS NULL;

