DROP TABLE IF EXISTS Stock_Item_Id_@v_countryPrefix@;
CREATE TABLE Stock_Item_Id_@v_countryPrefix@ LIKE Stock_Item_Id_Template;
INSERT Stock_Item_Id_@v_countryPrefix@ SELECT * FROM Stock_Item_Id_Sample_@v_countryPrefix@;

CREATE TABLE IF NOT EXISTS Stock_Item_Id LIKE Stock_Item_Id_Template;
REPLACE Stock_Item_Id_@v_countryPrefix@ SELECT * FROM Stock_Item_Id_Sample_@v_countryPrefix@;