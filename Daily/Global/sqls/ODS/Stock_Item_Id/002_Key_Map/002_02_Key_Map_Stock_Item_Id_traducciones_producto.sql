UPDATE            Stock_Item_Id_Key_Map_Sample_@v_countryPrefix@ AS Stock_Item_Id_Key_Map
       INNER JOIN @v_wmsprod@.traducciones_producto 
               ON Stock_Item_Id_Key_Map.barcode_wms = traducciones_producto.identificador
SET 
   Stock_Item_Id_Key_Map.sku_simple = sku;