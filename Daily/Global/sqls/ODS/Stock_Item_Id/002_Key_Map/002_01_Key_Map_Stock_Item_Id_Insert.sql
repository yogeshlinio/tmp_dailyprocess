DROP TABLE IF EXISTS Stock_Item_Id_Key_Map_Sample_@v_countryPrefix@;
CREATE TABLE Stock_Item_Id_Key_Map_Sample_@v_countryPrefix@ LIKE Stock_Item_Id_Key_Map_Template;

/*INSERT ALL THE PRIMARY KEYS*/
INSERT Stock_Item_Id_Key_Map_Sample_@v_countryPrefix@
(
  `country`       ,
  `stock_item_id` ,
  `barcode_wms`   ,
  `itens_recebimento_id` ,
  posicao
)
SELECT
   '@v_countryPrefix@' AS Country,
 	estoque.estoque_id,  
	estoque.cod_barras,
    estoque.itens_recebimento_id,
	estoque.endereco
FROM
    @v_wmsprod@.estoque
;
	