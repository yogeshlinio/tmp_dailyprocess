/*
*/
call  production.monitoring_step( "ODS.itens_venda_id", "@v_country@", "date_exported" , @v_times@ );

DROP TEMPORARY TABLE IF EXISTS tmp_sales_for_stock_@v_countryPrefix@;
CREATE TEMPORARY TABLE tmp_sales_for_stock_@v_countryPrefix@ ( id_sales int auto_increment, PRIMARY KEY ( estoque_id , id_sales  ),  INDEX (estoque_id))
SELECT 
		t.estoque_id,
		t.item_id,
		NULL AS id_sales
	FROM            itens_venda_oot_@v_countryPrefix@
	     INNER JOIN itens_venda_oot_key_map_Sample_@v_countryPrefix@ as t
		      USING ( item_id )
ORDER BY estoque_id, date_exported DESC
;
DELETE FROM tmp_sales_for_stock_@v_countryPrefix@ WHERE id_sales != 1;

UPDATE            Stock_Item_Id_Key_Map_Sample_@v_countryPrefix@ AS Stock_Item_Id_Key_Map
       INNER JOIN tmp_sales_for_stock_@v_countryPrefix@ AS itens_venda 
               ON Stock_Item_Id_Key_Map.stock_item_id = itens_venda.estoque_id
SET 
   Stock_Item_Id_Key_Map.item_id = itens_venda.item_id;
   