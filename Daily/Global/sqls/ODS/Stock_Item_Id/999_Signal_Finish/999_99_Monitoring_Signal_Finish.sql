INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'ODS.Stock_Item_Id',
  "finish",
  NOW(),
  NOW(),
  count(*),
  count(*)
FROM
   ODS.Stock_Item_Id_Sample_@v_countryPrefix@
;



