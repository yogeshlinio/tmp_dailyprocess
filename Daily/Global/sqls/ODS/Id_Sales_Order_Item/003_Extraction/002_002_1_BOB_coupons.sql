DROP TABLE IF EXISTS TMP_Coupons;
CREATE TABLE TMP_Coupons ( INDEX( CouponCode ) )
SELECT
   sales_rule.code AS CouponCode,
   sales_rule_set.code_prefix	AS PrefixCode,
   sales_rule_set.description
FROM
          @bob_live@.sales_rule 
LEFT JOIN @bob_live@.sales_rule_set 
       ON @bob_live@.sales_rule.fk_sales_rule_set = @bob_live@.sales_rule_set.id_sales_rule_set 
;

/* 
*   UPDATE FIELDS ON OSRI
*/
UPDATE            Sales_Order_Item_Id_Sample_@v_countryPrefix@ AS Sales_Order_Item_Id_Sample
       INNER JOIN TMP_Coupons
	          USING ( CouponCode )
SET
    Sales_Order_Item_Id_Sample.PrefixCode            = TMP_Coupons.PrefixCode,
	Sales_Order_Item_Id_Sample.CouponCodeDescription = TMP_Coupons.description
;
