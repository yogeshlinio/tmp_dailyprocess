UPDATE            @development@.Sales_Order_Item_Id_Sample
       INNER JOIN @bob_live@.sales_order_address
               ON @bob_live@.sales_order_address.id_sales_order_address = @development@.Sales_Order_Item_Id_Sample.fk_sales_order_address_shipping
SET
   @development@.Sales_Order_Item_Id_Sample.postcode                   = @bob_live@.sales_order_address.postcode, 
   @development@.Sales_Order_Item_Id_Sample.city                       = @bob_live@.sales_order_address.city,
   @development@.Sales_Order_Item_Id_Sample.fk_customer_address_region = @bob_live@.sales_order_address.fk_customer_address_region
;
UPDATE            @development@.Sales_Order_Item_Id_Sample
       INNER JOIN @bob_live@.customer_address_region
               ON @bob_live@.customer_address_region.id_customer_address_region = @development@.Sales_Order_Item_Id_Sample.fk_customer_address_region
SET
   @development@.Sales_Order_Item_Id_Sample.State = @bob_live@.customer_address_region.code 
;