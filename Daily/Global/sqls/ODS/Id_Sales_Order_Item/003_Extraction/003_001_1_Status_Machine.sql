UPDATE            Sales_Order_Item_Id_Sample_@v_countryPrefix@ AS Sales_Order_Item_Id_Sample
       INNER JOIN @bob_live@.sales_order_item_status 
               ON Sales_Order_Item_Id_Sample.fk_sales_order_item_status = sales_order_item_status.id_sales_order_item_status
SET
   Sales_Order_Item_Id_Sample.status = sales_order_item_status.name;

  
#Query: A 102 U OrderBefore-AfterCan
UPDATE            Sales_Order_Item_Id_Sample_@v_countryPrefix@ AS Sales_Order_Item_Id_Sample
       INNER JOIN @development@.M_Bob_Order_Status_Definition 
              ON     ( Sales_Order_Item_Id_Sample.PaymentMethod = M_Bob_Order_Status_Definition.Payment_Method) 
                 AND ( Sales_Order_Item_Id_Sample.Status        = M_Bob_Order_Status_Definition.Status) 
SET 
     Sales_Order_Item_Id_Sample.OrderBeforeCan = M_Bob_Order_Status_Definition.OrderBeforeCan, 
     Sales_Order_Item_Id_Sample.OrderAfterCan  = M_Bob_Order_Status_Definition.OrderAfterCan, 
     Sales_Order_Item_Id_Sample.Cancellations  = M_Bob_Order_Status_Definition.Cancellations, 
     Sales_Order_Item_Id_Sample.Pending        = M_Bob_Order_Status_Definition.Pending
;

