call  production.monitoring_step( "ODS.Sku_simple", "@v_country@", "finish" , @v_times@ );
/* 
*   UPDATE FIELDS ON OSRI
*/
UPDATE            Sales_Order_Item_Id_Sample_@v_countryPrefix@@ AS Sales_Order_Item_Id_Sample
       INNER JOIN ODS.Sku_Simple_@v_countryPrefix@  AS ODS
	             ON ODS.SKU_Simple = Sales_Order_Item_Id_Sample.SKUSimple
SET
    Sales_Order_Item_Id_Sample.SKUConfig   = ODS.sku_config,
    Sales_Order_Item_Id_Sample.SKUName     = ODS.sku_name,	
    Sales_Order_Item_Id_Sample.IdSupplier  = ODS.id_supplier,
    Sales_Order_Item_Id_Sample.Supplier    = ODS.Supplier,
    Sales_Order_Item_Id_Sample.Buyer       = ODS.Buyer,
    Sales_Order_Item_Id_Sample.HeadBuyer   = ODS.Head_Buyer,
    Sales_Order_Item_Id_Sample.Brand       = ODS.Brand,

    #Sales_Order_Item_Id_Sample.isMPlace    = ODS.isMarketPlace,
    #Sales_Order_Item_Id_Sample.isMPlaceSince = ODS.isMarketPlace_Since,
    #Sales_Order_Item_Id_Sample.isMPlaceUntil = ODS.isMarketPlace_Until,
    #Sales_Order_Item_Id_Sample.MPlaceFee     = ODS.MPlace_Fee,

    Sales_Order_Item_Id_Sample.isVisible           = ODS.isVisible,
    Sales_Order_Item_Id_Sample.isActiveSKUConfig   = ODS.isActive_SKUConfig,
    Sales_Order_Item_Id_Sample.isActiveSKUSimple   = ODS.isActive_SKUSimple,

    Sales_Order_Item_Id_Sample.Cat1       = ODS.Cat1,
    Sales_Order_Item_Id_Sample.Cat2       = ODS.Cat2,
    Sales_Order_Item_Id_Sample.Cat3       = ODS.Cat3,
   
    Sales_Order_Item_Id_Sample.CatKPI     = ODS.Cat_KPI,
    Sales_Order_Item_Id_Sample.CatBP      = ODS.Cat_BP,
    
    Sales_Order_Item_Id_Sample.PackageWeight = IF( ODS.Package_Weight = 0, IF( ODS.Product_weight = 0 , 1 , ODS.Product_weight ), ODS.Package_Weight),
    Sales_Order_Item_Id_Sample.ProductWeight = IF( ODS.Product_weight = 0, IF( ODS.Package_Weight = 0 , 1 , ODS.Package_Weight ) , ODS.Product_weight) ,

    Sales_Order_Item_Id_Sample.PackageWeight = ODS.Package_Weight,
  
	
    Sales_Order_Item_Id_Sample.PackageHeight = ODS.Package_Height,
    Sales_Order_Item_Id_Sample.PackageLength = ODS.Package_Length,
    Sales_Order_Item_Id_Sample.PackageWidth  = ODS.Package_Width,
	Sales_Order_Item_Id_Sample.DeliveryType = ODS.delivery_type,
	Sales_Order_Item_Id_Sample.SupplierType = ODS.Supplier_Type,
	Sales_Order_Item_Id_Sample.ProductsOrigin = ODS.Products_Origin,	
    Sales_Order_Item_Id_Sample.DeliveryCostSupplier = IF( Sales_Order_Item_Id_Sample.DeliveryCostSupplier <= 0 , ODS.Delivery_Cost_Supplier, Sales_Order_Item_Id_Sample.DeliveryCostSupplier ), 
    Sales_Order_Item_Id_Sample.VolumeWeight  = ODS.Volumetric_Weight,
    Sales_Order_Item_Id_Sample.OriginalPrice = IF( ODS.Original_Price <= 0 , ODS.Price , ODS.Original_Price )
;




