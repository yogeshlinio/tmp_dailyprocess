DROP TEMPORARY TABLE IF EXISTS TMP_Sales_Order_Item_Id_Sample_Order_Sample;
CREATE TEMPORARY TABLE TMP_Sales_Order_Item_Id_Sample_Order_Sample ( PRIMARY KEY ( OrderNum ) )
SELECT 
  OrderNum,
  count(*) AS ItemsInOrder,
  SUM( OrderAfterCan  ) AS NetItemsInOrder,
  SUM( OrderBeforeCan ) AS GrossItemsInOrder,
  SUM( PackageWeight  ) AS OrderWeight
FROM 
  Sales_Order_Item_Id_Sample_@v_countryPrefix@
GROUP BY OrderNum;

UPDATE            TMP_Sales_Order_Item_Id_Sample_Order_Sample
       INNER JOIN Sales_Order_Item_Id_Sample_@v_countryPrefix@ AS Sales_Order_Item_Id_Sample
            USING ( OrderNum )
SET
   Sales_Order_Item_Id_Sample.NetItemsInOrder = TMP_Sales_Order_Item_Id_Sample_Order_Sample.NetItemsInOrder,
   Sales_Order_Item_Id_Sample.ItemsInOrder    = TMP_Sales_Order_Item_Id_Sample_Order_Sample.ItemsInOrder,
   Sales_Order_Item_Id_Sample.OrderWeight     = TMP_Sales_Order_Item_Id_Sample_Order_Sample.OrderWeight     
;
