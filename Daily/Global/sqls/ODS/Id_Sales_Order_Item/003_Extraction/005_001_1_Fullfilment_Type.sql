call  production.monitoring_step( 'ODS.Stock_Item_Id' , "@v_country@", "Fulfiilment_Type_WMS" , @v_times@ );

UPDATE            Sales_Order_Item_Id_Sample_@v_countryPrefix@ AS Sales_Order_Item_Id_Sample 
       INNER JOIN @bob_live@.catalog_shipment_type 
               ON Sales_Order_Item_Id_Sample.fk_catalog_shipment_type = catalog_shipment_type.id_catalog_shipment_type
SET
   Sales_Order_Item_Id_Sample.ShipmentType = catalog_shipment_type.name_en;


DROP TEMPORARY TABLE IF EXISTS TMP_Track;
CREATE TEMPORARY TABLE TMP_Track ( PRIMARY KEY ( stock_item_id ), KEY ( item_id ) )
SELECT
  stock_item_id,
  0 AS  item_id,
  fulfillment_type_wms as fulfillment_type_real
FROM ODS.Stock_Item_Id_Sample_@v_countryPrefix@
GROUP BY item_id
;

UPDATE        TMP_Track 
   INNER JOIN ODS.Stock_Item_Id_Key_Map_Sample_@v_countryPrefix@ AS Key_Map
        USING ( Stock_Item_Id )
SET 
   TMP_Track.item_id = Key_Map.item_id;
DELETE FROM TMP_Track WHERE item_id = 0;
   

UPDATE            Sales_Order_Item_Id_Sample_@v_countryPrefix@ AS Sales_Order_Item_Id_Sample
       INNER JOIN TMP_Track
               ON Sales_Order_Item_Id_Sample.ItemId = TMP_Track.item_id
SET
   Sales_Order_Item_Id_Sample.ShipmentType = TMP_Track.fulfillment_type_real,
   Sales_Order_Item_Id_Sample.FulfillmentTypeReal = TMP_Track.fulfillment_type_real
;
