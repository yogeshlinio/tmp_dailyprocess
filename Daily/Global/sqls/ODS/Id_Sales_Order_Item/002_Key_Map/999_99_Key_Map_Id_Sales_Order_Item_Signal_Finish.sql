INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date
  )
SELECT 
  '@v_country@', 
  'ODS.Id_Sales_Order_Item_Key_Map',
  "finish",
  NOW(),
  NOW()
;