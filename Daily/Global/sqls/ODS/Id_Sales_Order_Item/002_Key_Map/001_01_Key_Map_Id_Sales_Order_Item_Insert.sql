DROP TABLE IF EXISTS Id_Sales_Order_Item_Key_Map_Sample_@v_countryPrefix@;
CREATE TABLE Id_Sales_Order_Item_Key_Map_Sample_@v_countryPrefix@ LIKE Id_Sales_Order_Item_Key_Map_Template_@v_countryPrefix@;
/*INSERT ALL THE PRIMARY KEYS*/
INSERT Id_Sales_Order_Item_Key_Map_Sample_@v_countryPrefix@
(
  `Country`           ,
  `Id_Sales_Order_Item`        ,
  `id_sales_order`,
   id_sales_order_item_status,
   id_catalog_shipment_type,
   sku_simple,
   id_shipment_carrier
)
SELECT 
   '@v_countryPrefix@' AS Country,
    id_sales_order_item,
   `fk_sales_order` ,
    fk_sales_order_item_status,
	fk_catalog_shipment_type,
	sku,
	fk_shipment_carrier
FROM 
   @bob_live@.sales_order_item;
   
   
   