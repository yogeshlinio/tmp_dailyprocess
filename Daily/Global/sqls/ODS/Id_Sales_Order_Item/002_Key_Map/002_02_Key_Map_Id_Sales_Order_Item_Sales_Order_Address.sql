UPDATE            Id_Sales_Order_Item_Key_Map_Sample_@v_countryPrefix@ AS Id_Sales_Order_Item_Key_Map_Sample
       INNER JOIN @bob_live@.sales_order_address
	        USING ( id_sales_order_address )
SET
  Id_Sales_Order_Item_Key_Map_Sample.id_customer_address_region = sales_order_address.fk_customer_address_region
;