/*
*  004 Fees
*/
UPDATE        @development@.Sales_Order_Item_Id_Sample 
   INNER JOIN @development@.A_E_In_COM_Fees_per_PM 
           ON     @development@.Sales_Order_Item_Id_Sample.PaymentMethod = @development@.A_E_In_COM_Fees_per_PM.Payment_Method
              AND @development@.Sales_Order_Item_Id_Sample.Installment   = @development@.A_E_In_COM_Fees_per_PM.Installment
SET
   @development@.Sales_Order_Item_Id_Sample.Fees        = @development@.A_E_In_COM_Fees_per_PM.Fee,
   @development@.Sales_Order_Item_Id_Sample.ExtraCharge = @development@.A_E_In_COM_Fees_per_PM.Extra_Charge;
