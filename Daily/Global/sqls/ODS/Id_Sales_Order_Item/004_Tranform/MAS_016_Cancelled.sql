UPDATE Sales_Order_Item_Id_Sample            
SET
   Sales_Order_Item_Id_Sample.Cancelled = 1
WHERE 
       Cancellations = 1 
   AND Rejected      = 0
;
