UPDATE Sales_Order_Item_Id_Sample
SET Sales_Order_Item_Id_Sample.WHCost = 0,
Sales_Order_Item_Id_Sample.FLWHCost = 0,
Sales_Order_Item_Id_Sample.FLCSCost = 0,
Sales_Order_Item_Id_Sample.PackagingCost = 0,
Sales_Order_Item_Id_Sample.CSCost = 0;
#WHERE Sales_Order_Item_Id_Sample.MonthNum >= 201309;

/*
* MX_099_001 Costo de Revistas
*/
#Query: 7_M1_Costo0Revistas
UPDATE Sales_Order_Item_Id_Sample
SET Sales_Order_Item_Id_Sample.WHCost = - 1
WHERE
	(Sales_Order_Item_Id_Sample.ShipmentType = 'Dropshipping' or isMPlace = 1);

/*
* MX_006 FL_Cost
*/
#Query: A 129 U WH/CS cost per Item
DROP TEMPORARY TABLE
IF EXISTS WH_CS_Per_day;

CREATE TEMPORARY TABLE WH_CS_Per_day (
	Month_Num INT,
	TotalDays INT,
	FL_WH_cost_per_day_NC FLOAT,
	FL_CS_cost_per_day_NC FLOAT,
	Pack_cost_per_day_NC FLOAT,
	PRIMARY KEY (Month_Num)
) SELECT
	Month_Num,
	dayofmonth(
		last_day(concat(Month_Num, "01"))
	) AS TotalDays,

	0 AS FL_WH_cost_per_day_NC,
	VALUE AS FL_CS_cost_per_day_NC,
	0 AS Pack_cost_per_day_NC
FROM
	(
		SELECT
			MonthNum AS Month_Num,
		VALUE
		FROM
			(
				SELECT
					*
				FROM
					development_mx.M_Costs
				WHERE
					Country = '@v_countryShortName@'
				AND TypeCost = 'FL_CS_cost'
				ORDER BY
					updatedAt DESC
			) a
		GROUP BY
			MonthNum
	) AS Cost
#WHERE
#	Month_Num >= 201309
GROUP BY
	Month_Num;

/*
*Actualización de Customer Service
*/
UPDATE WH_CS_Per_day
INNER JOIN (
	SELECT
		MonthNum AS Month_Num,
	VALUE
	FROM
		(
			SELECT
				*
			FROM
				development_mx.M_Costs
			WHERE
				Country = '@v_countryShortName@'
			AND TypeCost = 'FL_WH_cost'
			ORDER BY
				updatedAt DESC
		) a
	GROUP BY
		MonthNum
) AS Cost USING (Month_Num)
SET FL_WH_cost_per_day_NC =
VALUE ;
#WHERE
#	Month_Num >= 201309;
	
/*
*Actualizacón de Packaging Cost
*/
UPDATE WH_CS_Per_day
INNER JOIN (
	SELECT
		MonthNum AS Month_Num,
	VALUE
	FROM
		(
			SELECT
				*
			FROM
				development_mx.M_Costs
			WHERE
				Country = '@v_countryShortName@'
			AND TypeCost = 'Packaging'
			ORDER BY
				updatedAt DESC
		) a
	GROUP BY
		MonthNum
) AS Cost USING (Month_Num)
SET Pack_cost_per_day_NC =
VALUE
WHERE
	Month_Num >= 201309;
/*********************************************/
DROP TEMPORARY TABLE
IF EXISTS tmpFLWHCost;

/*********************************************/
CREATE TEMPORARY TABLE tmpFLWHCost  
SELECT
	MonthNum,
	(
		COUNT(Sales_Order_Item_Id_Sample.ItemID) + SUM(Sales_Order_Item_Id_Sample.WHCost)
	) AS item_aux,
	sum(if(PackageWeight <= 0.3, 1, 0) + if(PackageWeight <= 0.3, WHCost, 0)) +
    2*(sum(if(PackageWeight > 0.3 and PackageWeight <= 0.5, 1, 0) + if(PackageWeight > 0.3 and PackageWeight <= 0.5, WHCost, 0))) +
    4*sum(if(PackageWeight > 0.5 and PackageWeight <= 2.14, 1, 0) + if(PackageWeight > 0.5 and PackageWeight <= 2.14, WHCost, 0)) +
    8*sum(if(PackageWeight > 2.14, 1, 0) + if(PackageWeight > 2.14, WHCost, 0)) as "NetItemsGroup"
FROM
	Sales_Order_Item_Id_Sample
WHERE
	Sales_Order_Item_Id_Sample.OrderAfterCan = 1
GROUP BY
	MonthNum 
;

/*********************************************/
UPDATE WH_CS_Per_day
INNER JOIN tmpFLWHCost a on  
WH_CS_Per_day.Month_Num =a.MonthNum
SET
 WH_CS_Per_day.FL_WH_cost_per_day_NC = WH_CS_Per_day.FL_WH_cost_per_day_NC / a.item_aux ,
 WH_CS_Per_day.Pack_cost_per_day_NC  = WH_CS_Per_day.Pack_cost_per_day_NC / a.NetItemsGroup;
/*********************************************/

UPDATE WH_CS_Per_day
INNER JOIN (
	SELECT
		MonthNum AS Month_Num,
		dayofmonth(max(Date)) Days
	FROM
		Sales_Order_Item_Id_Sample
	GROUP BY
		MonthNum
) AS OrdersPerMonth USING (Month_Num)
SET 
WH_CS_Per_day.FL_WH_cost_per_day_NC = (	WH_CS_Per_day.FL_WH_cost_per_day_NC * OrdersPerMonth.Days) / (WH_CS_Per_day.TotalDays),
WH_CS_Per_day.Pack_cost_per_day_NC = (	WH_CS_Per_day.Pack_cost_per_day_NC * OrdersPerMonth.Days) / (WH_CS_Per_day.TotalDays);

/*********************************************/
UPDATE WH_CS_Per_day
INNER JOIN (
	SELECT
		MonthNum AS Month_Num,
		dayofmonth(max(Date)) Days,
		count(ItemId) AS NetItems
	FROM
		Sales_Order_Item_Id_Sample
	WHERE
		OrderAfterCan = 1
	GROUP BY
		MonthNum
) AS OrdersPerMonth USING (Month_Num)
SET WH_CS_Per_day.FL_CS_cost_per_day_NC = (	WH_CS_Per_day.FL_CS_cost_per_day_NC * OrdersPerMonth.Days) / (	OrdersPerMonth.NetItems * WH_CS_Per_day.TotalDays);
/*********************************************/
REPLACE OPS_WH_CS_per_Order_for_PC2 SELECT
	Month_Num,
	WH_CS_Per_day.FL_WH_cost_per_day_NC,
	WH_CS_Per_day.FL_CS_cost_per_day_NC
FROM
	WH_CS_Per_day;

/*********************************************/
UPDATE Sales_Order_Item_Id_Sample
INNER JOIN OPS_WH_CS_per_Order_for_PC2 ON (
	Sales_Order_Item_Id_Sample.MonthNum = OPS_WH_CS_per_Order_for_PC2.Month_Num
	AND Sales_Order_Item_Id_Sample.OrderAfterCan = 1
)
SET Sales_Order_Item_Id_Sample.FLCSCost = OPS_WH_CS_per_Order_for_PC2.CS_Cost_per_Order_@v_countryShortName@;

/*********************************************/
UPDATE Sales_Order_Item_Id_Sample
INNER JOIN OPS_WH_CS_per_Order_for_PC2 ON (
	Sales_Order_Item_Id_Sample.MonthNum = OPS_WH_CS_per_Order_for_PC2.Month_Num
	AND Sales_Order_Item_Id_Sample.OrderAfterCan = 1
	AND WHCost = 0
)
SET Sales_Order_Item_Id_Sample.FLWHCost = OPS_WH_CS_per_Order_for_PC2.WH_Cost_per_Order_@v_countryShortName@;
/*
* update PackagingCost
*/
UPDATE Sales_Order_Item_Id_Sample
INNER JOIN WH_CS_Per_day a ON (
	Sales_Order_Item_Id_Sample.MonthNum = a.Month_Num
	AND Sales_Order_Item_Id_Sample.OrderAfterCan = 1
	AND WHCost = 0
)
SET Sales_Order_Item_Id_Sample.PackagingCost = a.Pack_cost_per_day_NC * (if(PackageWeight <= 0.3, 1, if(PackageWeight > 0.3 and PackageWeight <= 0.5, 2, if(PackageWeight > 0.5 and PackageWeight <= 2.14, 4, if(PackageWeight > 2.14, 8, 0)))))

