call  production.monitoring_step( 'ODS.itens_venda_id' , "@v_country@", 'is_marks' , @v_times@ );
/*
* MX_017_Shipped
*/

UPDATE         Sales_Order_Item_Id_Sample  as a
    INNER JOIN ODS.itens_venda_oot_sample_@v_countryPrefix@ b
            ON a.ItemID = b.item_id
SET
   a.DateShipped = b.date_shipped,
   a.shipped = 1;