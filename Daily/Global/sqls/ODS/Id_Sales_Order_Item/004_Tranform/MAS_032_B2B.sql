/*
* MX_032_B2B
*/
 #Query: M1_CostoCorporativo
UPDATE            Sales_Order_Item_Id_Sample  
       INNER JOIN @development_mx@.A_E_M1_Ordenes_Corporativas 
               ON Sales_Order_Item_Id_Sample.OrderNum = A_E_M1_Ordenes_Corporativas.Order 
SET 
   Sales_Order_Item_Id_Sample.ShippingFee  = IF( useShippingFee  = 1, A_E_M1_Ordenes_Corporativas.ShippingFee,  Sales_Order_Item_Id_Sample.ShippingFee  ) , 
   Sales_Order_Item_Id_Sample.ShippingCost = IF( useShippingCost = 1, A_E_M1_Ordenes_Corporativas.ShippingCost, Sales_Order_Item_Id_Sample.ShippingCost ) , 
   Sales_Order_Item_Id_Sample.PaymentFees  = IF( usePaymentFee   = 1, A_E_M1_Ordenes_Corporativas.PaymentFee  , Sales_Order_Item_Id_Sample.PaymentFees  ) ,
   Sales_Order_Item_Id_Sample.isB2B        = 1
;