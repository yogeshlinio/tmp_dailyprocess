/*
*  MX_018_001 A_OtherRev
*/
DROP   TEMPORARY TABLE IF EXISTS @development@.A_OtherRev;
CREATE TEMPORARY TABLE @development@.A_OtherRev ( INDEX ( MonthNum  ) )
SELECT
 date_format( date , "%Y%m") AS MonthNum,
 sum( value ) AS OtherRev,
 0 as MonthlyNetItems
FROM
  M_Other_Revenue
WHERE
   Country = "@v_countryShortName@"
GROUP BY date_format(  date, "%Y%m" ) 
;

UPDATE       @development@.A_OtherRev
  INNER JOIN ( SELECT MonthNum, count(*) AS Items FROM @development@.Sales_Order_Item_Id_Sample
               WHERE OrderAfterCan = 1 GROUP BY MonthNum ) AS TMP
     USING  ( MonthNum )
SET
   MonthlyNetItems = Items
;

UPDATE           @development@.A_OtherRev
      INNER JOIN @development@.Sales_Order_Item_Id_Sample
           USING ( MonthNum ) 
SET
    @development@.Sales_Order_Item_Id_Sample.OtherRev = ( A_OtherRev.OtherRev * FactorMonth( now() - INTERVAL 1 DAY, MonthNum ) ) / MonthlyNetItems 
WHERE OrderAfterCan = 1
;


