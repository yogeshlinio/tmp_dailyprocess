 #Query: M1_COGS_PC
UPDATE Sales_Order_Item_Id_Sample 
SET 
     Sales_Order_Item_Id_Sample.COGS       =   Sales_Order_Item_Id_Sample.CostAfterTax 
                                     + Sales_Order_Item_Id_Sample.DeliveryCostSupplier
                                     - Sales_Order_Item_Id_Sample.CreditNotes                                     
                                      ,

     Sales_Order_Item_Id_Sample.PCOne     =   Sales_Order_Item_Id_Sample.priceAfterTax
                                     + Sales_Order_Item_Id_Sample.ShippingFee
                                     - Sales_Order_Item_Id_Sample.CouponValueAfterTax
                                     + Sales_Order_Item_Id_Sample.Interest
                                     - Sales_Order_Item_Id_Sample.CostAfterTax
                                     - Sales_Order_Item_Id_Sample.DeliveryCostSupplier
                                     + Sales_Order_Item_Id_Sample.CreditNotes, 

     Sales_Order_Item_Id_Sample.PCOnePFive =   Sales_Order_Item_Id_Sample.priceAfterTax
                                      + Sales_Order_Item_Id_Sample.ShippingFee
                                      + Sales_Order_Item_Id_Sample.Interest
                                      - Sales_Order_Item_Id_Sample.CouponValueAfterTax
                                      - Sales_Order_Item_Id_Sample.CostAfterTax
                                      - Sales_Order_Item_Id_Sample.DeliveryCostSupplier
                                      + Sales_Order_Item_Id_Sample.CreditNotes                                     
                                      - Sales_Order_Item_Id_Sample.ShippingCostEstimated
                                      - Sales_Order_Item_Id_Sample.PaymentFees
									  - Sales_Order_Item_Id_Sample.PackagingCost, 
									  
	 Sales_Order_Item_Id_Sample.PCTwo =   Sales_Order_Item_Id_Sample.PCOnePFive
											- Sales_Order_Item_Id_Sample.FLWHCost
											- Sales_Order_Item_Id_Sample.FLCSCost,
								  
    
     Sales_Order_Item_Id_Sample.Rev =   Sales_Order_Item_Id_Sample.PriceAfterTax  
                              + Sales_Order_Item_Id_Sample.ShippingFee
                              - Sales_Order_Item_Id_Sample.CouponValueAfterTax
                              + Sales_Order_Item_Id_Sample.Interest;

