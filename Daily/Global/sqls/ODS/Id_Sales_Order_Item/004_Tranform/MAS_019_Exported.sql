CALL production.monitoring_step( "ODS.itens_venda_id", "@v_country@", "date_exported"      , @v_times@ );

/*DROP TEMPORARY TABLE IF EXISTS TMP_Exported;
CREATE TEMPORARY TABLE TMP_Exported (  index( ItemID  ) )
SELECT
   fk_sales_order_item AS ItemId,
   max( created_at ) AS DateExported
FROM  bob_live_ve.sales_order_item_status_history
WHERE
	#exported, exported_electronically
   fk_sales_order_item_status in ( 4,115 )
GROUP BY  
fk_sales_order_item;

UPDATE        Sales_Order_Item_Id_Sample  
   INNER JOIN TMP_Exported 
        USING ( ItemId )
SET
   Sales_Order_Item_Id_Sample.Exported     = 1,
   Sales_Order_Item_Id_Sample.DateExported = TMP_Exported.DateExported
;*/

DROP TEMPORARY TABLE IF EXISTS TMP_Exported;
CREATE TEMPORARY TABLE TMP_Exported (  index( ItemID  ) )
SELECT
   item_id AS ItemId,
   date_exported AS DateExported
FROM  ODS.itens_venda_oot_sample_@v_countryPrefix@
;

UPDATE        Sales_Order_Item_Id_Sample  
   INNER JOIN TMP_Exported 
        USING ( ItemId )
SET
   Sales_Order_Item_Id_Sample.Exported     = 1,
   Sales_Order_Item_Id_Sample.DateExported = TMP_Exported.DateExported
;