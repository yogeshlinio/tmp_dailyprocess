
DROP   TEMPORARY TABLE IF EXISTS A_Returns;
CREATE TEMPORARY TABLE A_Returns ( INDEX ( fk_sales_order_item  ) )
SELECT
 fk_sales_order_item,
 created_at as dateReturned
FROM
    @bob_live@.sales_order_item_status_history
WHERE
#automatic return (132), returned (8) no se usa
   fk_sales_order_item_status IN ( 93 )
GROUP BY fk_sales_order_item
;

REPLACE A_Returns
SELECT 
   c.item_id    AS fk_sales_order_item,
   b.changed_at AS created_at
FROM          @v_wmsprod@.inverselogistics_status_history b 
   INNER JOIN @v_wmsprod@.inverselogistics_devolucion c 
           ON b.return_id = c.id
WHERE
   b.status = 6;


UPDATE           A_Returns
      INNER JOIN Sales_Order_Item_Id_Sample
              ON A_Returns.fk_sales_order_item = Sales_Order_Item_Id_Sample.ItemID 
SET
    Sales_Order_Item_Id_Sample.Returns = 1,
	Sales_Order_Item_Id_Sample.DateReturned = A_Returns.dateReturned
	
;

#Query: A 102 U OrderBefore-AfterCan
UPDATE Sales_Order_Item_Id_Sample
SET 
     Sales_Order_Item_Id_Sample.Cancellations  = IF(       Sales_Order_Item_Id_Sample.Cancellations = 1 
                                            AND Sales_Order_Item_Id_Sample.Returns       = 0 , 1 , 0 )
;

/*
* DATE           2013/12/05
* DEVELOPED BY   Carlos Antonio Mondragón Soria
* DESCRIPTION MKT - Status Wrong in Returns 
*/
UPDATE Sales_Order_Item_Id_Sample
SET
   OrderAfterCan = 0,
   Cancellations = 0,
   Pending       = 0
WHERE
   Returns = 1;





