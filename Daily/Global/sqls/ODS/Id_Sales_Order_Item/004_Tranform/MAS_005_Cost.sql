call  production.monitoring_step( 'ODS.Id_Procurement_Order_Item' , "@v_country@", 'Cost_Oms' , @v_times@ );

/*
*  Costo de BOB
*/
UPDATE        Sales_Order_Item_Id_Sample
   INNER JOIN Sales_Order_Item_Id_Sample_Catalog
           ON Sales_Order_Item_Id_Sample.SKUSimple = Sales_Order_Item_Id_Sample_Catalog.SKU_simple
SET
   Sales_Order_Item_Id_Sample.Cost         = Sales_Order_Item_Id_Sample_Catalog.cost,
   Sales_Order_Item_Id_Sample.CostAfterTax = Sales_Order_Item_Id_Sample_Catalog.cost / 
                                                 ( 1 +  ( Sales_Order_Item_Id_Sample.TaxPercent / 100 ) )          
WHERE
      Sales_Order_Item_Id_Sample.Cost is null 
   OR Sales_Order_Item_Id_Sample.Cost = 0
;
   
/*
* Costo Promedio
*/    
UPDATE        Sales_Order_Item_Id_Sample
   INNER JOIN Sales_Order_Item_Id_Sample_Catalog p
           ON Sales_Order_Item_Id_Sample.SKUSimple= p.sku_simple 
SET 
   Sales_Order_Item_Id_Sample.Cost = p.Cost_OMS  ,
   Sales_Order_Item_Id_Sample.CostAftertax = p.Cost_OMS /( ( 100 +  Sales_Order_Item_Id_Sample.TaxPercent )/100 )
   
WHERE
 p.Cost_OMS > 0 
  and
  (Sales_Order_Item_Id_Sample.Cost is null 
	   OR Sales_Order_Item_Id_Sample.Cost = 0)
;

/*
* VE_005_Cost_OMS
*/
UPDATE             Sales_Order_Item_Id_Sample  
        INNER JOIN ODS.Id_Procurement_Order_Item_Sample_@v_countryPrefix@ b 
	            ON Sales_Order_Item_Id_Sample.ItemId = b.item_id
SET 
    Sales_Order_Item_Id_Sample.Cost           = b.cost_oms,
    Sales_Order_Item_Id_Sample.CostAftertax   = b.cost_oms_after_tax
;
