/*
* MX_009_001 Shipping Fee
*/ 
#Query: M1_ShipFee_table
UPDATE Sales_Order_Item_Id_Sample 
SET
   ShippingFeeAfterTax = Sales_Order_Item_Id_Sample.ShippingFee / ( 1 +  ( Sales_Order_Item_Id_Sample.TaxPercent / 100 ) )          
;
