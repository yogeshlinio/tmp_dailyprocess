#Mexico NewReturningGross
USE development_mx;
#Inicializacion de la columna NewReturningGross
UPDATE Sales_Order_Item_Id_Sample
SET
  Sales_Order_Item_Id_Sample.NewReturningGross  = "RETURNING";


#Se crea la tabla que contiene las 
DROP TEMPORARY TABLE IF  EXISTS A_E_BI_OrdersGross;

CREATE TEMPORARY  TABLE A_E_BI_OrdersGross (  PRIMARY KEY ( OrderNum  ) )
SELECT
  OrderNum,
  CustomerNum,
  IF( SUM( OrderBeforeCan ) > 0 , 1 , 0 ) AS OrderBeforeCan,
  IF( SUM( OrderAfterCan ) > 0 , 1 , 0 ) AS OrderAfterCan,
  Date,
  `Time`
FROM  
  Sales_Order_Item_Id_Sample
WHERE  
  OrderBeforeCan = 1
GROUP BY    OrderNum
HAVING OrderBeforeCan = 1
ORDER BY Date,`Time`;

#Se crea la tabla que contiene las primeras ordenes por cliente
DROP TEMPORARY TABLE IF EXISTS A_E_BI_First_OrdersGross;
CREATE TEMPORARY TABLE A_E_BI_First_OrdersGross (  PRIMARY KEY ( CustomerNum , id ), INDEX ( CustomerNum ), id int auto_increment )
SELECT
  CustomerNum,
  OrderNum,
  NULL AS id,
  OrderAfterCan,
  OrderBeforeCan
FROM  
  A_E_BI_OrdersGross
WHERE  
  OrderBeforeCan = 1
ORDER BY 
  CustomerNum , Date asc, Time asc;

#Se eliminan los elementos
DELETE FROM A_E_BI_First_OrdersGross WHERE id > 1 ;

#Se actualiza columna
UPDATE    Sales_Order_Item_Id_Sample       
       INNER JOIN   A_E_BI_First_OrdersGross USING( CustomerNum )
SET
  NewReturningGross  = IF( A_E_BI_First_OrdersGross.OrderNum = Sales_Order_Item_Id_Sample.OrderNum, "NEW", "RETURNING"  );