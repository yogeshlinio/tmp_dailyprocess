CALL production.monitoring_step( 'ODS.Stock_Item_Id' , "@v_country@", "in_stock" , @v_times@ );

/*
*  MX_008_001 Shipping_CostEstimated
*/
#Query 1: Inicializacion de varible ShippingCostEstimated
UPDATE  Sales_Order_Item_Id_Sample 
SET 
     Sales_Order_Item_Id_Sample.ShippingCostEstimated =0;

#Query 2: -1 a todo lo de inventario
UPDATE  Sales_Order_Item_Id_Sample 
SET 
     Sales_Order_Item_Id_Sample.ShippingCostEstimated =-1
WHERE fulfillmentTypeReal != 'dropshipping'
and fulfillmentTypeReal !=''
and fulfillmentTypeReal is not null
;
####Validaciones de Shipping Cost previas
/*
* MX_024 Shipping_COST = 0
*/
 #Query: 2 M1_ShippingCost_0
UPDATE  Sales_Order_Item_Id_Sample 
SET 
    Sales_Order_Item_Id_Sample.ShippingCostEstimated = 0
WHERE 
     Sales_Order_Item_Id_Sample.CouponCode="MKT0xfgVK" 
 Or  Sales_Order_Item_Id_Sample.CouponCode="MKT1eDvI7";

/*
*  MX_008_002 Shipping_Cost Dulces Anahuac
*/
 #Query: A 218 U Dulces Anahuac
UPDATE  Sales_Order_Item_Id_Sample
SET 
    Sales_Order_Item_Id_Sample.ShippingCostEstimated = 0, 
    Sales_Order_Item_Id_Sample.FLCSCost    = 0
WHERE 
   Left(CouponCode,7)="DEPCHIP";

/*
* MX_099_001 Costo de Revistas
*/
 #Query: 7_M1_Costo0Revistas
UPDATE             Sales_Order_Item_Id_Sample
       INNER JOIN @development@.A_E_6_M1_Costos_Revistas 
               ON  Sales_Order_Item_Id_Sample.SKUSimple = A_E_6_M1_Costos_Revistas.SKU_Simple 
SET 
    Sales_Order_Item_Id_Sample.ShippingCostEstimated    = 0;

/*
*  Inventory
*/

UPDATE     Sales_Order_Item_Id_Sample
SET
    Sales_Order_Item_Id_Sample.ShippingCostEstimated = 0
WHERE MonthNum >= date_format( now() - INTERVAL 3 MONTH , "%Y%m" ) 
   and FulfillmentTypeReal = "dropshipping"
;

/*
* MX_099_004 Ordenes coorporativas
*/
 #Query: M1_CostoCorporativo
UPDATE            Sales_Order_Item_Id_Sample  
       INNER JOIN @development@.A_E_M1_Ordenes_Corporativas 
               ON Sales_Order_Item_Id_Sample.OrderNum = A_E_M1_Ordenes_Corporativas.Order 
SET 
   Sales_Order_Item_Id_Sample.ShippingCostEstimated = IF(useShippingCost = 1, A_E_M1_Ordenes_Corporativas.ShippingCost, Sales_Order_Item_Id_Sample.ShippingCostEstimated) 
;

#######
DROP TABLE IF EXISTS No_Stock;
CREATE TEMPORARY TABLE No_Stock ( PRIMARY KEY ( Stock_Item_Id ) , KEY ( Item_Id ) )	
SELECT 
   Stock_Item_Id,
   0 as Item_Id
FROM 
   ODS.Stock_Item_Id_Sample_@v_countryPrefix@    
WHERE 
   in_stock = 0
;

UPDATE            No_Stock
       INNER JOIN ODS.Stock_Item_Id_Key_Map_Sample_@v_countryPrefix@ AS Stock_Item_Id_Key_Map
SET
   No_Stock.Item_Id = Stock_Item_Id_Key_Map.Item_Id;
DELETE FROM No_Stock WHERE Item_Id = 0;
   	
#Query 3: 0 a todo lo -1  que no este en el join
UPDATE       Sales_Order_Item_Id_Sample AS a
        join No_Stock
          ON No_Stock.Item_Id = a.ItemID
SET 
     Sales_Order_Item_Id_Sample.ShippingCostEstimated =0
WHERE ShippingCostEstimated =-1;

#query3_1: 0 a todas las ordenes que tengan solo un item en la Orden
UPDATE  Sales_Order_Item_Id_Sample 
SET 
     Sales_Order_Item_Id_Sample.ShippingCostEstimated =0
WHERE ItemsInOrder=1
and ShippingCostEstimated=-1;


#Query 4: Mas de un item por orden_carrier
DROP TEMPORARY TABLE IF  EXISTS tmp_order_est_ship;
CREATE TEMPORARY  TABLE tmp_order_est_ship (PRIMARY KEY(OrderNum))
SELECT
  OrderNum,
	sum(ShippingCostEstimated) as SumShippingCostEstimated
FROM  
  Sales_Order_Item_Id_Sample
#WHERE  
#  OrderAfterCan = 1
GROUP BY OrderNum;

#Query 5: se pone 0 a todas las ordenes que contengan solo un item con valor -1 
UPDATE  Sales_Order_Item_Id_Sample 
join tmp_order_est_ship on tmp_order_est_ship.OrderNum=Sales_Order_Item_Id_Sample.OrderNum
SET 
    Sales_Order_Item_Id_Sample.ShippingCostEstimated =0
where tmp_order_est_ship.SumShippingCostEstimated =-1;

#
UPDATE  Sales_Order_Item_Id_Sample
join(
SELECT
	OrderNum as Order_Num,
	sum(ShippingCostEstimated) as SumShippingCost
FROM
	 Sales_Order_Item_Id_Sample
#where OrderAfterCan=1
where ShippingCostEstimated=-1
GROUP BY 
	OrderNum,
	Courier) aux
set Sales_Order_Item_Id_Sample.ShippingCostEstimated = if(aux.SumShippingCost = -1,0,Sales_Order_Item_Id_Sample.ShippingCostEstimated)
where Sales_Order_Item_Id_Sample.OrderNum = aux.Order_Num
; 


#Query 6:Se calcula el peso voluumetrico por orden (solo ordenes a estimar)
DROP TABLE IF  EXISTS  tmp_VolumeWeight;
CREATE TABLE  tmp_VolumeWeight (PRIMARY KEY(OrderNum))
SELECT
	OrderNum as OrderNum,
	sum(ShippingCostEstimated) as SumShippingCost,
	sum(VolumeWeight) as SumVolumeWeight,
    count(*) as ItemsInOrder_est,
	PostCode,
	PaymentMethod,
	Courier,
	ShippingCostEstimated
FROM
	 Sales_Order_Item_Id_Sample
where ShippingCostEstimated =-1 
#and OrderAfterCan=1
GROUP BY
	OrderNum;


#query 7: se hace el mach con SRT
DROP TABLE IF  EXISTS  tmp_ShippingCostEstimated;
CREATE TABLE  tmp_ShippingCostEstimated (PRIMARY KEY(OrderNum))
SELECT
	a.PostCode,
	a.OrderNum,
	a.PaymentMethod,
	a.SumVolumeWeight,
	a.Courier,	
	a.ItemsInOrder_est,
	a.ShippingCostEstimated,
	b.method_name,
	b.from_postcode,
	b.to_postcode,
	b.priority,
	b.carrier_name,
	b.shipment_base_cost,
	b.base_weight,
	b.shipment_cost_per_kilogram,
	if(a.SumVolumeWeight <= b.base_weight, b.shipment_base_cost, b.shipment_base_cost+CEIL(a.SumVolumeWeight-b.base_weight)*b.shipment_cost_per_kilogram) as ShippingCostE
FROM
	 tmp_VolumeWeight a
INNER JOIN  SRT b ON a.PostCode BETWEEN b.from_postcode
AND b.to_postcode
AND a.SumVolumeWeight BETWEEN b.from_weight AND b.to_weight
AND b.method_name =
IF (a.PaymentMethod = 'CashOnDelivery_Payment',
	'COD',
IF (
	a.PaymentMethod = 'CreditCardOnDelivery_Payment',
	'COD',
	'PREPAID'
)
)
and a.Courier=b.carrier_name
GROUP BY a.OrderNum;


#Se hace el Update de ShippingCost estimado en A_master
UPDATE  Sales_Order_Item_Id_Sample
JOIN tmp_ShippingCostEstimated b ON b.OrderNum = Sales_Order_Item_Id_Sample.OrderNum
AND b.ShippingCostEstimated = - 1
SET Sales_Order_Item_Id_Sample.ShippingCostEstimated = (
	b.shipment_base_cost / ItemsInOrder_est
) + (
	VolumeWeight / SumVolumeWeight
) * (
	ShippingCostE - b.shipment_base_cost
);

#Se hace el Update de ShippingCost estimado en A_master
UPDATE  Sales_Order_Item_Id_Sample
SET ShippingCostEstimated = ShippingCost
where ShippingCostEstimated <=0;
