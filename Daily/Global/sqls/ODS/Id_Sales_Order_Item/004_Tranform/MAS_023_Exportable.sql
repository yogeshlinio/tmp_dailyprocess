DROP   TEMPORARY TABLE IF EXISTS A_S_BI_Exportable;
CREATE TEMPORARY TABLE A_S_BI_Exportable ( INDEX ( fk_sales_order_item  ) )
SELECT
 *
FROM
    @bob_live@.sales_order_item_status_history
WHERE
   @bob_live@.sales_order_item_status_history.fk_sales_order_item_status IN ( 3 )
GROUP BY fk_sales_order_item
;

UPDATE           A_S_BI_Exportable
      INNER JOIN @development@.Sales_Order_Item_Id_Sample
              ON A_S_BI_Exportable.fk_sales_order_item = @development@.Sales_Order_Item_Id_Sample.ItemID 
SET
    @development@.Sales_Order_Item_Id_Sample.Exportable = 1
;
