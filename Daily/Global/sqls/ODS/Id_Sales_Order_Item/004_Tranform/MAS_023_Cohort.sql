DROP TABLE IF EXISTS @development@.A_E_BI_First_Orders;
CREATE TABLE @development@.A_E_BI_First_Orders (  PRIMARY KEY ( CustomerNum , id ), INDEX( CustomerNum ), id int auto_increment )
SELECT
  CustomerNum,
  OrderNum,
  NULL AS id,
  MonthNum,
  Date,
  OrderAfterCan,
  OrderBeforeCan,
  GrandTotal
FROM  
  @development@.Sales_Order_Item_Id_Sample 
WHERE  
  OrderAfterCan = 1
  AND Date > "2012-05-09"
GROUP BY    OrderNum
ORDER BY 
  CustomerNum , Date asc, `Time`;

DELETE FROM @development@.A_E_BI_First_Orders WHERE id > 1 ;

UPDATE            @development@.A_E_BI_First_Orders
       INNER JOIN @development@.Sales_Order_Item_Id_Sample  USING( CustomerNum )
SET
  @development@.Sales_Order_Item_Id_Sample.FirstOrderNum  = A_E_BI_First_Orders.OrderNum,
  @development@.Sales_Order_Item_Id_Sample.CohortMonthNum = A_E_BI_First_Orders.MonthNum,
  @development@.Sales_Order_Item_Id_Sample.NewReturning  = IF( A_E_BI_First_Orders.OrderNum = @development@.Sales_Order_Item_Id_Sample.OrderNum, "NEW", "RETURNING"  )
;
