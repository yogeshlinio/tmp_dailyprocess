DROP TABLE IF EXISTS Id_Sales_Order_Item_Key_Map_Template_@v_countryPrefix@;
CREATE TABLE `Id_Sales_Order_Item_Key_Map_Template_@v_countryPrefix@` (
  `Country`             varchar(150) NOT NULL DEFAULT '',
  `Id_Sales_Order_Item` INT NOT NULL,
  `Id_Sales_Order`      INT NOT NULL,
  
  `order_num`      INT NOT NULL,
  `sku_simple`          varchar(50)  NOT NULL,
  
  `id_sales_order_address`      INT NOT NULL,
  `id_customer_address_region`      INT NOT NULL,
  `id_sales_order_item_status`      INT NOT NULL,  
  `coupon_code`  varchar(50)  NOT NULL,
  `id_catalog_shipment_type`       INT NOT NULL,
  `id_customer`       INT NOT NULL,
  `id_shipment_carrier`       INT NOT NULL,

  PRIMARY KEY (`Country` , Id_Sales_Order_Item ),
  KEY `Id_Sales_Order_Item` (`Id_Sales_Order_Item`),

  KEY `Country_sku_simple`      ( country , `sku_simple`) ,  
  KEY `sku_simple`      (`sku_simple`) ,

  KEY `Country_Id_Sales_Order`      ( country , `Id_Sales_Order`) ,  
  KEY `Id_Sales_Order`      (`Id_Sales_Order`) ,

  KEY `Country_Order_Num`      ( country , `Order_Num`) ,  
  KEY `Order_Num`      (`Order_Num`) ,
  
  KEY `Country_id_sales_order_address` ( country , id_sales_order_address ),
  KEY `id_sales_order_address` ( id_sales_order_address )  ,

  KEY `Country_id_sales_order_item_status` ( country , id_sales_order_item_status ),
  KEY `id_sales_order_item_status` ( id_sales_order_item_status )  ,
    
  KEY `Country_id_catalog_shipment_type` ( country , id_catalog_shipment_type ),
  KEY `id_catalog_shipment_type` ( id_catalog_shipment_type )  ,

  KEY `Country_coupon_code` ( country , coupon_code ),
  KEY `coupon_code` ( coupon_code )   ,
  
  KEY `Country_id_shipment_carrier` ( country , id_shipment_carrier ),
  KEY `id_shipment_carrier` ( id_shipment_carrier )   
  
);