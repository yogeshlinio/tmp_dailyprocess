/*
*  Monitoring to...
*/
#call  production.monitoring_step(`in_table_name` varchar(255),IN `in_country` varchar(255), `in_step` varchar(50), 5 );
call  production.monitoring_step( "ODS.Stock_Item_Id", "@v_country@", "in_stock"      , @v_times@ );
call  production.monitoring_step( "ODS.Stock_Item_Id", "@v_country@", "days_in_stock" , @v_times@ );

DROP TEMPORARY TABLE IF EXISTS skus_in_stock_@v_countryPrefix@;
CREATE TEMPORARY TABLE skus_in_stock_@v_countryPrefix@ ( PRIMARY KEY ( Sku_Simple ) )
select 
   distinct(sku_simple) as Sku_Simple, 
   sum( in_stock) as stock_WMS,
   sum( if( in_stock = 1 and reserved = 0, 1 , 0 ) ) as items_available_WMS,
   sum( reserved ) as Reserved_WMS,
   avg( if( in_stock = 1 , days_in_stock, null ) ) as average_days_in_stock_WMS,
   max( if( in_stock = 1 , days_in_stock, null ) ) as max_days_in_stock_WMS,
   sum( IF(	exit_type = 'sold' , 1 , 0  ) ) AS sold_WMS,
   sum( IF(	exit_type = 'sold' and date_exit BETWEEN CURDATE() - INTERVAL 42 DAY AND CURDATE(), 1 , 0  ) ) AS sold_42_WMS,   
   sum( IF(	exit_type = 'sold' and date_exit BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE(), 1 , 0  ) ) AS sold_30_WMS,   
   sum( IF(	exit_type = 'sold' and date_exit BETWEEN CURDATE() - INTERVAL  7 DAY AND CURDATE(), 1 , 0  ) ) AS sold_7_WMS      
from 
   Stock_Item_Id_Sample_@v_countryPrefix@
GROUP BY Sku_Simple
;

UPDATE            Sku_Simple_Sample_@v_countryPrefix@ AS Sku_Simple_Sample
       INNER JOIN skus_in_stock_@v_countryPrefix@  AS Tmp
	        USING ( sku_simple )
SET
     Sku_Simple_Sample.max_days_in_stock_WMS     = Tmp.max_days_in_stock_WMS,
     Sku_Simple_Sample.in_stock_WMS              = 1,
     Sku_Simple_Sample.stock_WMS                 = Tmp.stock_WMS,
     Sku_Simple_Sample.Reserved_WMS              = Tmp.Reserved_WMS,
     Sku_Simple_Sample.average_days_in_stock_WMS = Tmp.average_days_in_stock_WMS,
     Sku_Simple_Sample.sold_42_WMS = Tmp.sold_42_WMS,
     Sku_Simple_Sample.sold_30_WMS = Tmp.sold_30_WMS,
     Sku_Simple_Sample.sold_7_WMS  = Tmp.sold_7_WMS,
	 Sku_Simple_Sample.average_remaining_days    = round( Tmp.items_available_WMS / round( Tmp.sold_42_WMS / 42 ,2 ) , 2 )
;

UPDATE            Sku_Simple_Sample_@v_countryPrefix@ AS Sku_Simple_Sample
SET 
    Sku_Simple_Sample.interval_remaining_days  = CASE 
                                                    WHEN  average_remaining_days is null THEN "Infinito"
                                                    WHEN  average_remaining_days <  7  THEN  "< 8" 												   
													WHEN  average_remaining_days < 15  THEN "8-15"
													WHEN  average_remaining_days < 30  THEN "15-29"
                                                    WHEN  average_remaining_days < 45  THEN "30-44"
													WHEN  average_remaining_days < 60  THEN "45-59"
													WHEN  average_remaining_days < 90  THEN "60-89"
													WHEN  average_remaining_days < 120 THEN "90-119"
                                                    WHEN  average_remaining_days < 150 THEN "120-149"
													WHEN  average_remaining_days < 180 THEN "150-179"
                                                    WHEN  average_remaining_days < 210 THEN "180-209"
													WHEN  average_remaining_days < 240 THEN "210-239"
                                                    WHEN  average_remaining_days < 270 THEN "240-269"
													WHEN  average_remaining_days < 300 THEN "270-299"
                                                    WHEN  average_remaining_days < 330 THEN "300-229"
													WHEN  average_remaining_days<=360  THEN "330-360"
													ELSE "> 360"
				 							    END,
    Sku_Simple_Sample.interval_sku_age       =  CASE 
	                                                WHEN  average_days_in_stock_WMS is null   THEN   "Infinito"
                                                    WHEN  average_days_in_stock_WMS<7         THEN   "< 8"
													WHEN  average_days_in_stock_WMS<15        THEN   "8-15"
													WHEN  average_days_in_stock_WMS<30        THEN   "15-29" 
                                                    WHEN  average_days_in_stock_WMS<45        THEN   "30-44"
													WHEN  average_days_in_stock_WMS<60        THEN   "45-59"
                                                    WHEN  average_days_in_stock_WMS<90        THEN   "60-89"
													WHEN  average_days_in_stock_WMS<120       THEN   "90-119"
                                                    WHEN  average_days_in_stock_WMS<150       THEN   "120-149"
													WHEN  average_days_in_stock_WMS<180       THEN   "150-179"
                                                    WHEN  average_days_in_stock_WMS<210       THEN   "180-209"
													WHEN  average_days_in_stock_WMS<240       THEN   "210-239"
                                                    WHEN  average_days_in_stock_WMS<270       THEN   "240-269"
													WHEN  average_days_in_stock_WMS<300       THEN   "270-299"
                                                    WHEN  average_days_in_stock_WMS<330       THEN   "300-229"
													WHEN  average_days_in_stock_WMS<=360      THEN   "330-360"   
													ELSE   "> 360"
												END

;




/*
*  Insert Monitoring LOG
*/
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'ODS.Sku_Simple',
  "in_stock_WMS",
  NOW(),
  NOW(),
  sum(in_stock_WMS),
  sum(in_stock_WMS)
FROM
   Sku_Simple_Sample_@v_countryPrefix@
WHERE 
   Country = '@v_countryPrefix@'   
;