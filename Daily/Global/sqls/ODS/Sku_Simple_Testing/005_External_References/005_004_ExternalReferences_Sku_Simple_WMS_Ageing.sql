/*
*  Monitoring to...
*/
#call  production.monitoring_step(`in_table_name` varchar(255),IN `in_country` varchar(255), `in_step` varchar(50), 5 );
call  production.monitoring_step( "ODS.Stock_Item_Id", "@v_country@", "days_in_stock" , @v_times@ );

DROP TEMPORARY TABLE IF EXISTS Sku_Inventory_Age_@v_countryPrefix@;
CREATE TEMPORARY TABLE Sku_Inventory_Age_@v_countryPrefix@ 
(  PRIMARY KEY ( sku_simple ) )
SELECT
		`sku_simple` AS `sku_simple`,
		avg(`days_in_stock`) AS `avg_age`
FROM
    Stock_Item_Id_Sample_@v_countryPrefix@
WHERE
        `in_stock` = 1 
    AND `reserved` = 0
GROUP BY `sku_simple`;

UPDATE            Sku_Simple_Sample_@v_countryPrefix@    AS Sku_Simple
       INNER JOIN Sku_Inventory_Age_@v_countryPrefix@    AS Sku_Inventory_Age
            USING ( sku_simple )
SET 
	Sku_Simple.sku_age = truncate(Sku_Inventory_Age.avg_age,0);

/*
*  Insert Monitoring LOG
*/
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'ODS.Sku_Simple',
  "sku_age",
  NOW(),
  NOW(),
  sum(in_stock_WMS),
  sum(in_stock_WMS)
FROM
   Sku_Simple_Sample_@v_countryPrefix@
WHERE 
   Country = '@v_countryPrefix@'   
;