DROP TABLE IF EXISTS Sku_Simple_@v_countryPrefix@;
CREATE TABLE Sku_Simple_@v_countryPrefix@ LIKE Sku_Simple_Template_@v_countryPrefix@;
INSERT Sku_Simple_@v_countryPrefix@ SELECT * FROM Sku_Simple_Sample_@v_countryPrefix@;

CREATE TABLE IF NOT EXISTS Sku_Simple LIKE Sku_Simple_Template_@v_countryPrefix@;
REPLACE Sku_Simple SELECT * FROM Sku_Simple_Sample_@v_countryPrefix@;
