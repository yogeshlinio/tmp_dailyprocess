#Actualizar el sku status
DROP TEMPORARY TABLE IF EXISTS Status_Sample_@v_countryPrefix@;
CREATE TEMPORARY TABLE Status_Sample_@v_countryPrefix@ ( PRIMARY KEY ( Sku_Simple ) )
SELECT
   Sku_Simple,
   status_config,
   status_simple,
   pet_approved,
   pet_status,
   space(50 ) AS status_sku,
   space(50 ) AS status_pet,
   space(50 ) AS reason_not_visible
FROM    
   Sku_Simple_Sample_@v_countryPrefix@
WHERE 
   isVisible = 0    
;
   
UPDATE            Status_Sample_@v_countryPrefix@ AS Sku_Simple_Sample
SET 
	status_sku = IF( status_config="deleted"  or status_simple  = "deleted", "Deleted", 
		         IF( status_config="inactive" or status_simple  = "inactive", "Inactive", 
		         IF( status_config="active"   and status_simple = "active", "Active", NULL))),
	status_pet = IF( pet_status IS NULL, 'Pending All', 
                 IF( pet_status = "creation,edited", "Pending Images", 
                 IF( pet_status = "creation,images", "Pending Edited", 
                 IF( pet_status = "creation", "Pending EnI", 
                 IF( pet_status = "creation,edited,images", "Ok", "Error")))));


#Actualizar la razón de no visibilidad
UPDATE Status_Sample_@v_countryPrefix@ AS Sku_Simple_Sample
SET 
   reason_not_visible = 
		IF( status_pet <> "OK",     status_pet, 
		IF( status_sku <> "active", status_sku, 
		IF( pet_approved = 0, "Quality Check Failed", "Stock levels")))
;

UPDATE            Sku_Simple_Sample_@v_countryPrefix@ AS Sku_Sample
       INNER JOIN Status_Sample_@v_countryPrefix@     AS Sku_Status
	        USING ( Sku_Simple )
SET 
    Sku_Sample.reason_not_visible = Sku_Status.reason_not_visible
;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'ODS.Sku_simple',
  "sku_status",
  NOW(),
  NOW(),
  0,
  0
;
