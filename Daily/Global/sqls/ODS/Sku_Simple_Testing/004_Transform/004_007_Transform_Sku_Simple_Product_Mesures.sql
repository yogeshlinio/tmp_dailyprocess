UPDATE Sku_Simple_Sample_@v_countryPrefix@ AS Sku_Simple_Sample
SET 
    Sku_Simple_Sample.Max_VolW_Weight = GREATEST( Volumetric_Weight , Package_Weight )
;

UPDATE Sku_Simple_Sample_@v_countryPrefix@
SET 
    Package_Measure_New = 	CASE
								WHEN Max_VolW_Weight > 35 
								THEN 'oversized'
								ELSE
								    (	CASE
									        WHEN Max_VolW_Weight > 5 
											THEN 'large'
											ELSE
											    (	CASE
												    WHEN Max_VolW_Weight > 2 
													THEN 'medium'
													ELSE 'small'
													END
												)
										END
									)
							END;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'ODS.Sku_Simple',
  'Product_Measures',
  NOW(),
  NOW(),
  0,
  0
;