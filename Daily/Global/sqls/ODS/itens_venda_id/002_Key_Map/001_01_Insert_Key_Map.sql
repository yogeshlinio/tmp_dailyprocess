DROP TABLE IF EXISTS itens_venda_oot_key_map_Sample_@v_countryPrefix@;
CREATE TABLE itens_venda_oot_key_map_Sample_@v_countryPrefix@ LIKE itens_venda_oot_key_map_template_@v_countryPrefix@;
/*INSERT ALL THE PRIMARY KEYS*/
INSERT itens_venda_oot_key_map_Sample_@v_countryPrefix@
(
  `country`,
  `item_id`,
  `itens_venda_id`,
  `estoque_id`,
  `romaneio_id`,
  `sku`,
  `order_id`,  
  `numero_order`,  
  `data_exportable`
)
SELECT DISTINCT
   '@v_countryPrefix@' AS Country,
   `a`.`item_id`,
   `a`.`itens_venda_id` ,
   `a`.`estoque_id` ,
   `a`.`romaneio_id` ,
   `a`.`sku` ,
   `a`.`order_id` ,
   `a`.`numero_order` ,
   `a`.`data_exportable`
FROM 
   @v_wmsprod@.itens_venda AS a;