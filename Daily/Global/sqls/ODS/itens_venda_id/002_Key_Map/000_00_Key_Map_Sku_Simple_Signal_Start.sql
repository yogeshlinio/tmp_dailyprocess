INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date
  )
SELECT 
  '@v_country@', 
  'ODS.itens_venda_id_key_map',
  'start',
  NOW(),
  NOW()
;