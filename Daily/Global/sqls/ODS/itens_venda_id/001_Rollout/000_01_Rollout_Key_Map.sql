DROP TABLE IF EXISTS itens_venda_oot_key_map_template_@v_countryPrefix@;
CREATE TABLE `itens_venda_oot_key_map_template_@v_countryPrefix@` (
  `country` varchar(10) NOT NULL,
  `item_id` int(11) NOT NULL,
  `itens_venda_id` int(11) NOT NULL ,
  `estoque_id` int(11),
  `romaneio_id` int(11),
  `sku` varchar(200),
  `last_status` varchar(200),
  `order_id` int(11),  
  `numero_order` int(11),  
  `data_exportable` datetime DEFAULT NULL,  
  
  PRIMARY KEY (`Country` , item_id ),
  KEY `item_id` (`item_id`),
  KEY `itens_venda_id` (`itens_venda_id`),
  KEY `estoque_id` (`estoque_id`),
  KEY `romaneio_id` (`romaneio_id`),
  KEY `sku` (`sku`),
  KEY `order_id` (`order_id`),
  KEY `numero_order` (`numero_order`)
);