UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
SET a.delivered_last_30_il = CASE WHEN datediff(curdate(), date_delivered) <= 45
                                  AND datediff(curdate(), date_delivered) >= 15 
								    THEN 1
                             ELSE
							      0
						     END;