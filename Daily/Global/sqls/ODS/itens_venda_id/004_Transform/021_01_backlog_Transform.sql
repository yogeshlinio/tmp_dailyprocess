UPDATE itens_venda_oot_sample_@v_countryPrefix@
SET
backlog_delivery = IF((status_wms='Expedido' AND delay_total_delivery = 1 AND date_delivered IS NULL),1,0),
backlog_procurement = IF((date_procured IS NULL AND status_wms IN ('Aguardando estoque','Analisando quebra') AND delay_to_procure=1 ),1,0);