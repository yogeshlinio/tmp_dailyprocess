UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
SET a.procured_promised_last_30 = CASE WHEN DATEDIFF(CURDATE(),date_procured_promised) <= 30
                                       AND DATEDIFF(CURDATE(),date_procured_promised) > 0 
                                         THEN 1
                                  ELSE 0
                                  END,
a.shipped_last_30 = CASE WHEN DATEDIFF(CURDATE(), date_shipped) <= 30
                         AND DATEDIFF(CURDATE(), date_shipped) > 0 
                           THEN 1
                    ELSE 0
                    END,
a.delivered_promised_last_30 = CASE WHEN DATEDIFF(CURDATE(),date_delivered_promised) <= 30
                                    AND DATEDIFF(CURDATE(),date_delivered_promised) > 0 
                                      THEN 1
                               ELSE 0
                               END;