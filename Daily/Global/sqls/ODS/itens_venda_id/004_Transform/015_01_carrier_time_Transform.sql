UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
SET a.carrier_time = max_delivery_time - wh_time - supplier_leadtime
WHERE fulfillment_type_real not like 'Dropshipp%'
AND carrier_time = 0
AND max_delivery_time > (wh_time + supplier_leadtime);