UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
SET a.date_status_value_chain=date_1st_attempt,
status_value_chain='Intentado'
WHERE a.status_wms='Expedido'
AND a.date_1st_attempt IS NOT NULL;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
SET a.date_status_value_chain=date_delivered,
a.status_value_chain='Entregado'
WHERE a.status_wms='Expedido'
AND a.date_delivered IS NOT NULL;

#Debería estar en extracción, pero por la secuencia no es posible de momento
UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
INNER JOIN @v_wmsprod@.inverselogistics_devolucion AS b
ON a.item_id=b.item_id
SET a.date_status_value_chain=b.modified_at,
a.status_value_chain=b.status;


/*
*  Insert Monitoring LOG
*/
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'ODS.itens_venda_id',
  "status_value_chain",
  NOW(),
  NOW(),
  count(status_value_chain),
  count(status_value_chain) 
FROM
   itens_venda_oot_sample_@v_countryPrefix@
;