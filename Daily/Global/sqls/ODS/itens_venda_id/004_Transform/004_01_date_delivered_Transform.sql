DROP TEMPORARY TABLE IF EXISTS TMP_cod_rastreamento_@v_countryPrefix@;
CREATE TEMPORARY TABLE TMP_cod_rastreamento_@v_countryPrefix@ ( INDEX ( cod_rastreamento )  )
	SELECT
	itens.cod_rastreamento,
		CASE
	WHEN max(tms.date) < '2012-05-01' THEN
		NULL
	ELSE
		max(tms.date)
	END as date_delivered
	FROM
		           @v_wmsprod@.vw_itens_venda_entrega AS itens
		INNER JOIN @v_wmsprod@.tms_status_delivery AS tms
	            ON tms.cod_rastreamento = itens.cod_rastreamento
    WHERE 		
	    tms.id_status = 4
GROUP BY itens.cod_rastreamento;

DROP TABLE IF EXISTS TMP_cod_rastreamento_Item_@v_countryPrefix@;
CREATE TABLE TMP_cod_rastreamento_Item_@v_countryPrefix@ ( INDEX ( item_id ) )  
SELECT
   a.cod_rastreamento,
   a.date_delivered,
   b.item_id
FROM
              TMP_cod_rastreamento_@v_countryPrefix@ AS a
   INNER JOIN @v_wmsprod@.vw_itens_venda_entrega AS b
           ON b.cod_rastreamento = a.cod_rastreamento
;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
       INNER JOIN TMP_cod_rastreamento_Item_@v_countryPrefix@ AS b
            USING ( item_id )
SET 
    a.date_delivered =  b.date_delivered;

/* Leido por external references de otros procesos */
INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check
)
SELECT 
  '@v_country@', 
  'ODS.itens_venda_id',
  'date_delivered',
  NOW(),
  NOW(),
  0,
  0
;