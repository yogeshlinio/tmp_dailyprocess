UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN itens_venda_oot_key_map_Sample_@v_countryPrefix@ b
ON a.item_id=b.item_id
JOIN @v_wmsprod@.status_itens_venda c
ON b.itens_venda_id = c.itens_venda_id
SET a.fulfillment_type_real = CASE WHEN c.STATUS = 'Estoque reservado' THEN 'Own Warehouse'
                                   WHEN c.STATUS IN ('analisando quebra','aguardando estoque') THEN 'Crossdocking'
                                   WHEN c.STATUS IN ('DS estoque reservado','Waiting dropshipping','Dropshipping notified',
                                                     'awaiting_fulfillment','Electronic good','handled_by_marketplace',
													 'packed_by_marketplace') THEN 'Dropshipping'
									ELSE "Own Warehouse"				 
							  END
WHERE c.STATUS IN ('Estoque reservado','analisando quebra','aguardando estoque',
                   'DS estoque reservado','Waiting dropshipping','Dropshipping notified',
				   'awaiting_fulfillment','Electronic good','handled_by_marketplace','packed_by_marketplace');
/*
DROP TABLE IF EXISTS fulfillment_type_consignment_@v_countryPrefix@;
CREATE TABLE fulfillment_type_consignment_@v_countryPrefix@ (INDEX (item_id))
SELECT
 b.item_id,
 'Consignment' AS fulfillment_type_real
FROM
           @v_wmsprod@.itens_venda b
INNER JOIN @v_wmsprod@.estoque c
        ON b.estoque_id = c.estoque_id
INNER JOIN @v_wmsprod@.itens_recebimento d
        ON c.itens_recebimento_id = d.itens_recebimento_id
INNER JOIN @v_wmsprod@.recebimento e
        ON d.recebimento_id = e.recebimento_id
INNER JOIN @v_wmsprod@.po_oms f
        ON e.inbound_document_identificator = f.name
INNER JOIN @procurement_live@.procurement_order g
        ON f.po_oms_id = g.id_procurement_order
WHERE     e.inbound_document_type_id = 4
      AND g.fk_procurement_order_type in (7,11);

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
INNER JOIN fulfillment_type_consignment_@v_countryPrefix@ b
 ON a.item_id = b.item_id
SET a.fulfillment_type_real = b.fulfillment_type_real;
*/