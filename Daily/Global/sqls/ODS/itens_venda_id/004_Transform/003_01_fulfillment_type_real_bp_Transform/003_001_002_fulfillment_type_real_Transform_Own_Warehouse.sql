UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN itens_venda_oot_key_map_Sample_@v_countryPrefix@ b
ON a.item_id=b.item_id
JOIN @v_wmsprod@.status_itens_venda c
ON b.itens_venda_id = c.itens_venda_id
SET a.fulfillment_type_real = 'Own Warehouse'
WHERE c.STATUS IN (     'Cancelado', 
						'quebra tratada', 
						'quebrado', 
						'Precancelado', 
						'backorder_tratada');

UPDATE            itens_venda_oot_sample_@v_countryPrefix@ a
             JOIN itens_venda_oot_key_map_Sample_@v_countryPrefix@ b
               ON a.item_id=b.item_id			 
       INNER JOIN @v_wmsprod@.movimentacoes 
	           ON b.estoque_id = movimentacoes.estoque_id
SET 
    a.fulfillment_type_real = 'Own Warehouse'
WHERE
	  movimentacoes.de_endereco = 'retorno de cancelamento'
   OR movimentacoes.de_endereco = 'vendidos'; 

UPDATE itens_venda_oot_sample_@v_countryPrefix@
SET fulfillment_type_real_stock=fulfillment_type_real;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN itens_venda_oot_key_map_Sample_@v_countryPrefix@ b
ON a.item_id=b.item_id
JOIN tmp_sales_for_stock_@v_countryPrefix@ c
ON b.itens_venda_id=c.itens_venda_id
SET 
	a.is_cancelled=1,
	a.fulfillment_type_real_stock='Own Warehouse'
WHERE c.status IN (	'Cancelado', 
					'quebra tratada', 
					'quebrado', 
					'Precancelado', 
					'backorder_tratada');