call  production.monitoring_step( "ODS.Id_Procurement_Order_Item", "@v_country@", "purchase_order_type"          , @v_times@ );

UPDATE            itens_venda_oot_sample_@v_countryPrefix@ a
       INNER JOIN Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS b
             USING ( item_id )
SET 
      a.fulfillment_type_real = 'Consignment'
WHERE b.purchase_order_type = 'Consignment';

