UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
SET
a.tracking_stockout= 
CASE 
   WHEN INSTR(note_tracking,'OOS')!=0 THEN "OOS" 
   WHEN INSTR(note_tracking,'OOT')!=0 THEN "OOT" 
   WHEN INSTR(note_tracking,'PNP')!=0 THEN "PNP" 
   WHEN INSTR(note_tracking,'DSE')!=0 THEN "DSE" 
   WHEN INSTR(note_tracking,'POL')!=0 THEN "POL"  
   WHEN INSTR(note_tracking,'DTE')!=0 THEN "DTE"  
   WHEN INSTR(note_tracking,'SOE')!=0 THEN "SOE"  
   WHEN INSTR(note_tracking,'CBC')!=0 THEN "CBC"  
   WHEN INSTR(note_tracking,'DTL')!=0 THEN "DTL"
   WHEN INSTR(note_tracking,'OSW')!=0 THEN "OSW"
   WHEN INSTR(note_tracking,'UNS')!=0 THEN "UNS"
   WHEN INSTR(note_tracking,'OSM')!=0 THEN "OSM"
   WHEN INSTR(note_tracking,'SWC')!=0 THEN "SWC"
   WHEN INSTR(note_tracking,'OOA')!=0 THEN "OOA"
   WHEN INSTR(note_tracking,'IWL')!=0 THEN "IWL"
   WHEN INSTR(note_tracking,'ETR')!=0 THEN "ETR"
   WHEN INSTR(note_tracking,'CSW')!=0 THEN "CSW"
   WHEN INSTR(note_tracking,'STE')!=0 THEN "STE"
   WHEN INSTR(note_tracking,'SUP')!=0 THEN "BO - SUP"
   WHEN INSTR(note_tracking,'LIN')!=0 THEN "BO - LIN"
   WHEN INSTR(note_tracking,'OOS')!=0 THEN "OOS - OOS"
   WHEN INSTR(note_tracking,'OOT')!=0 THEN "OOS - OOT"
   WHEN INSTR(note_tracking,'PRI')!=0 THEN "OOS - PRI"
   WHEN INSTR(note_tracking,'BUN')!=0 THEN "OOS - BUN"
   WHEN INSTR(note_tracking,'FUL')!=0 THEN "OOS - FUL"
   WHEN INSTR(note_tracking,'ITW')!=0 THEN "OOS - ITW"
   WHEN INSTR(note_tracking,'CAT')!=0 THEN "OOS - CAT"
   ELSE "NA"
END;