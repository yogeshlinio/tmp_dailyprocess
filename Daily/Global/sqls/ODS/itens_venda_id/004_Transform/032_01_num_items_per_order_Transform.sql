TRUNCATE  operations_@v_countryPrefix@.pro_order_tracking_num_items_per_order;
 
INSERT INTO operations_@v_countryPrefix@.pro_order_tracking_num_items_per_order (
order_number,
count_of_order_item_id
) SELECT a.order_number, COUNT(1) AS countoforder_item_id
FROM itens_venda_oot_sample_@v_countryPrefix@ a
GROUP BY a.order_number;
 
UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.pro_order_tracking_num_items_per_order
ON a.order_number = pro_order_tracking_num_items_per_order.order_number
SET a.num_items_per_order = 1 / pro_order_tracking_num_items_per_order.count_of_order_item_id;