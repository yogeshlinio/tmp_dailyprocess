UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
SET 
24hr_shipments_Cust_persp = If(date_ordered is Null,null,If(date_shipped is Null,if( Datediff(CURDATE(),date_ordered)<= 1,1,0), if( Datediff(date_shipped,date_ordered)<=1,1,0))),
24hr_shipmnets_WH_persp = If(date_ready_to_pick is Null,null,If(date_shipped is Null, if( Datediff(CURDATE(),date_ready_to_pick)<= 1,1,0), if( Datediff(date_shipped,date_ready_to_pick)<=1,1,0)));