UPDATE itens_venda_oot_sample_@v_countryPrefix@
SET 
 week_ordered = operations_@v_countryPrefix@.week_iso(date_ordered),
 week_procured_promised = operations_@v_countryPrefix@.week_iso(date_procured_promised),
 week_r2s_promised = operations_@v_countryPrefix@.week_iso(date_ready_to_ship_promised),
 week_delivered_promised = operations_@v_countryPrefix@.week_iso(date_delivered_promised),
 week_shipped = operations_@v_countryPrefix@.week_iso(date_shipped);