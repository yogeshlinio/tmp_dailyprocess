UPDATE itens_venda_oot_sample_@v_countryPrefix@
SET
is_stockout = IF(status_wms IN('Quebrado',
							   'quebra tratada',
							   'backorder_tratada')
                  OR date_declared_stockout IS NOT NULL,1,0),
is_backorder = IF(date_backorder IS NOT NULL,1,0),
is_ready_to_pick = IF(date_ready_to_pick IS NOT NULL,1,0),
is_ready_to_ship = IF(date_ready_to_ship IS NOT NULL,1,0),
is_procured = IF(date_procured IS NOT NULL,1,0),
is_shipped = IF(date_shipped IS NOT NULL,1,0),
is_first_attempt = IF(date_1st_attempt IS NOT NULL,1,0),
is_delivered = IF(date_delivered IS NOT NULL,1,0),
is_cancelled = IF(status_wms in ('Cancelado','quebra tratada','Quebrado','Precancelado','backorder_tratada'),1,0),
is_quebra_tratada = IF((date_declared_stockout IS NOT NULL AND date_processed_stockout IS NOT NULL AND date_declared_stockout < date_processed_stockout),1,0),
is_bono = IF(date_shipped <= date_ready_to_pick_bono,1,0),
is_exits_il = IF(date_exit_il IS NOT NULL,1,0),
is_pending_return_wh_il = IF(status_wms_il IN ('aguardando_agendamiento','aguardando_retorno'),1,0);

UPDATE itens_venda_oot_sample_@v_countryPrefix@ as a 
JOIN @v_wmsprod@.inverselogistics_devolucion as b
ON a.item_id = b.item_id 
SET a.is_returned = 1;

/*
*  Insert Monitoring LOG
*/
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date)
SELECT 
  '@v_country@', 
  'ODS.itens_venda_id',
  'is_marks',
  NOW(),
  NOW()
;