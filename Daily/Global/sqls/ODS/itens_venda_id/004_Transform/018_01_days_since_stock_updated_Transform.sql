UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
SET a.days_since_stock_updated =
IF(a.date_stock_updated IS NULL,NULL,TIMESTAMPDIFF(DAY, date(a.date_stock_updated), CURDATE()));