UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
SET 
a.date_ready_to_pick_bono = CASE WHEN a.datetime_ready_to_pick <= TIMESTAMP(a.date_ready_to_pick)+ INTERVAL 16 HOUR 
                                 AND a.package_measure_new in ('small', 'medium','large')
								   THEN a.date_ready_to_pick
								 WHEN a.datetime_ready_to_pick <= TIMESTAMP(a.date_ready_to_pick)+ INTERVAL 16 HOUR 
                                 AND a.package_measure_new = 'oversized'
                                   THEN a.date_ready_to_pick + INTERVAL 1 DAY 
								 WHEN a.datetime_ready_to_pick > TIMESTAMP(a.date_ready_to_pick)+ INTERVAL 16 HOUR
                                 AND a.package_measure_new in ('small', 'medium','large')
								   THEN a.date_ready_to_pick_bono = date_ready_to_pick + INTERVAL 1 DAY 
								 WHEN a.datetime_ready_to_pick > TIMESTAMP(a.date_ready_to_pick)+ INTERVAL 16 HOUR
                                 AND a.package_measure_new = 'oversized'
								   THEN a.date_ready_to_pick_bono = date_ready_to_pick + INTERVAL 2 DAY
						    ELSE a.date_ready_to_pick_bono
							END;