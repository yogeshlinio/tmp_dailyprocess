CALL production.monitoring_step( "ODS.itens_venda_id", "@v_country@", "status_itens_venda_extraction" , 50 );

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
SET 
a.time_to_procure = If(datetime_exported is Null,null,If(datetime_procured is Null, TIMESTAMPDIFF( SECOND ,datetime_exported,CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_exported,datetime_procured)/3600)),
a.time_to_export = If(datetime_ordered is Null,null,If(datetime_exported is Null, TIMESTAMPDIFF( SECOND ,datetime_ordered, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_ordered, datetime_exported)/3600)),
a.time_to_ready = If(datetime_ready_to_pick is Null,null,If(datetime_ready_to_ship is Null, TIMESTAMPDIFF( SECOND ,datetime_ready_to_pick,CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_ready_to_pick, datetime_ready_to_ship)/3600)),
a.time_to_ship = If(datetime_ready_to_ship is Null,null,If(datetime_shipped is Null, TIMESTAMPDIFF( SECOND ,datetime_ready_to_ship, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_ready_to_ship, datetime_shipped)/3600)),
a.time_to_1st_attempt = If(datetime_shipped is Null,null,If(date_1st_attempt is Null, TIMESTAMPDIFF( SECOND ,datetime_shipped, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_shipped, date_1st_attempt)/3600)),
a.time_to_deliver = If(datetime_shipped is Null,null,If(date_delivered is Null, TIMESTAMPDIFF( SECOND ,datetime_shipped, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,date_delivered, datetime_shipped)/3600)),
a.time_to_request_recollection = If(datetime_shipped is Null,null,If(date_delivered is Null, TIMESTAMPDIFF( SECOND ,datetime_shipped, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_shipped, date_delivered)/3600)),
a.time_to_stockout_declared = If(datetime_ordered is Null,null,If(date_declared_stockout is Null, TIMESTAMPDIFF( SECOND ,datetime_ordered, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_ordered, date_declared_stockout)/3600)),
a.time_total_1st_attempt = If(datetime_ordered is Null,null,If(date_1st_attempt is Null, TIMESTAMPDIFF( SECOND ,datetime_ordered, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_ordered, date_1st_attempt)/3600)),
a.time_total_delivery = If(datetime_ordered is Null,null,If(date_delivered is Null, TIMESTAMPDIFF( SECOND ,datetime_ordered, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_ordered, date_delivered)/3600)),
a.time_to_backorder = If(datetime_exported is Null,null,If(datetime_backorder is Null, TIMESTAMPDIFF(SECOND,datetime_exported, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_exported,datetime_backorder)/3600)),
a.time_to_backorder_tratada = If(datetime_exported is Null,null,If(datetime_backorder_tratada is Null, TIMESTAMPDIFF(SECOND,datetime_exported, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_exported,datetime_backorder_tratada)/3600)),
a.time_to_backorder_accepted = If(datetime_exported is Null,null,If(datetime_backorder_accepted is Null, TIMESTAMPDIFF(SECOND,datetime_exported, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_exported,datetime_backorder_accepted)/3600)),
a.time_to_declared_stockout = If(datetime_exported is Null,null,If(datetime_declared_stockout is Null, 
TIMESTAMPDIFF(SECOND,datetime_exported, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_exported,datetime_declared_stockout)/3600)),
a.time_to_declared_backorder = If(datetime_exported is Null,null,If(datetime_declared_backorder is Null, 
TIMESTAMPDIFF(SECOND,datetime_exported, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_exported,datetime_declared_backorder)/3600)),
a.time_total_shipment = If(datetime_ordered is Null,null,If(datetime_shipped is Null, TIMEDIFF(CURDATE(),datetime_ordered), TIMEDIFF(datetime_shipped,datetime_ordered))),
a.time_to_processed_stockout = If(datetime_declared_stockout is Null,null,If(datetime_processed_stockout is Null, TIMESTAMPDIFF(SECOND,datetime_declared_stockout, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_declared_stockout,datetime_processed_stockout)/3600)),
a.time_to_processed_backorder = If(datetime_backorder is Null,null,If(datetime_processed_backorder is Null, TIMESTAMPDIFF(SECOND,datetime_backorder, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_backorder,datetime_processed_backorder)/3600));