CALL production.monitoring_step( "operations_@v_countryPrefix@.calcworkdays", "@v_country@", "finish" , 50 );
CALL production.monitoring_step( "ODS.itens_venda_id", "@v_country@", "status_itens_venda_extraction" , 50 );

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN @v_wmsprod@.tms_status_delivery b 
ON a.wms_tracking_code = b.cod_rastreamento
SET a.date_shipped = b.date ,
a.datetime_shipped = b.date
WHERE b.id_status = 13
and a.fulfillment_type_real = "Dropshipping";

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
JOIN operations_@v_countryPrefix@.calcworkdays AS c
ON DATE(a.datetime_ready_to_pick ) = c.date_first 
  AND c.workdays = 1 
  AND c.isholiday_first = 1
SET a.date_ready_to_pick = c.date_last,
a.datetime_ready_to_pick = c.date_last;

CALL production.monitoring_step( "ODS.itens_venda_id", "@v_country@", "date_exported" , 50 );

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON DATE(date_exported) = calcworkdays.date_first 
  AND workdays = supplier_leadtime
  AND isweekday = 1 
  AND isholiday = 0
SET date_procured_promised = calcworkdays.date_last,
month_procured_promised = date_format(calcworkdays.date_last,"%Y-%m")
WHERE a.date_exported IS NOT NULL;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON DATE(date_exported ) = calcworkdays.date_first 
  AND workdays = IF((max_delivery_time is null or max_delivery_time =''  or max_delivery_time=0),7,max_delivery_time)
  AND isweekday = 1 
  AND isholiday = 0
SET 
   date_delivered_promised = calcworkdays.date_last,
   month_delivered_promised = date_format(calcworkdays.date_last,"%Y-%m")
WHERE
	a.date_exported IS NOT NULL;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON DATE(date_procured_promised) = calcworkdays.date_first 
AND workdays = wh_time
AND isweekday = 1
AND isholiday = 0
SET 
	date_ready_to_ship_promised = calcworkdays.date_last,
	month_r2s_promised = date_format(calcworkdays.date_last,"%Y-%m")
WHERE
	a.date_procured_promised IS NOT NULL;
	
UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON DATE(date_exported) = calcworkdays.date_first 
AND workdays = 2
AND isweekday = 1
AND isholiday = 0
SET 
	date_ready_to_ship_promised = calcworkdays.date_last,
	month_r2s_promised = date_format(calcworkdays.date_last,"%Y-%m")
WHERE
	a.fulfillment_type_real = 'Dropshipping';

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_exported = calcworkdays.date_first
AND curdate() = calcworkdays.date_last
SET a.workdays_to_po = calcworkdays.workdays
WHERE a.date_exported IS NULL;

#Workdays to export
UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_ordered = calcworkdays.date_first
AND a.date_exported = calcworkdays.date_last
SET a.workdays_to_export = calcworkdays.workdays;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_ordered = calcworkdays.date_first
AND curdate() = calcworkdays.date_last
SET a.workdays_to_export =	calcworkdays.workdays
WHERE date_exported IS NULL;

#Workdays to ready
UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_ready_to_pick	= calcworkdays.date_first
AND a.date_ready_to_ship 	= calcworkdays.date_last
SET a.workdays_to_ready 	=	calcworkdays.workdays;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_ready_to_pick	= calcworkdays.date_first
AND curdate() = calcworkdays.date_last
SET a.workdays_to_ready = calcworkdays.workdays
WHERE date_ready_to_ship IS NULL;

#Workdays to ship
UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_ready_to_ship	= calcworkdays.date_first
AND a.date_shipped = calcworkdays.date_last
SET a.workdays_to_ship = calcworkdays.workdays;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_ready_to_ship	= calcworkdays.date_first
AND curdate() = calcworkdays.date_last
SET a.workdays_to_ship = calcworkdays.workdays
WHERE date_shipped IS NULL;

#workdays_to_1st_attempt
UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_shipped = calcworkdays.date_first
AND a.date_1st_attempt = calcworkdays.date_last
SET a.workdays_to_1st_attempt = calcworkdays.workdays;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_shipped = calcworkdays.date_first
AND curdate() = calcworkdays.date_last
SET a.workdays_to_1st_attempt = calcworkdays.workdays
WHERE date_1st_attempt IS NULL;

#workdays_to_deliver
UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_shipped = calcworkdays.date_first
AND a.date_delivered = calcworkdays.date_last
SET a.workdays_to_deliver = calcworkdays.workdays;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_shipped = calcworkdays.date_first
AND curdate() = calcworkdays.date_last
SET a.workdays_to_deliver = calcworkdays.workdays
WHERE date_delivered IS NULL;

#workdays to stockout declared
UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_ordered = calcworkdays.date_first
AND a.date_declared_stockout  = calcworkdays.date_last
SET a.workdays_to_stockout_declared = calcworkdays.workdays
WHERE date_declared_stockout is not null ;

#workdays_total_1st_attempt
UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_ordered = calcworkdays.date_first
AND a.date_1st_attempt = calcworkdays.date_last
SET a.workdays_total_1st_attempt = calcworkdays.workdays
WHERE date_1st_attempt IS NOT NULL;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_ordered = calcworkdays.date_first
AND curdate() = calcworkdays.date_last
SET a.workdays_total_1st_attempt = calcworkdays.workdays
WHERE date_1st_attempt IS NULL
AND date_ordered IS NOT NULL;

#workdays_total_delivery
UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_ordered = calcworkdays.date_first
AND a.date_delivered = calcworkdays.date_last
SET a.workdays_total_delivery = calcworkdays.workdays
WHERE date_delivered IS NOT NULL;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_ordered = calcworkdays.date_first
AND curdate() = calcworkdays.date_last
SET a.workdays_total_delivery = calcworkdays.workdays
WHERE date_delivered IS NULL
AND date_ordered IS NOT NULL;

#workdays_total_shipment
UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_ordered = calcworkdays.date_first
AND a.date_shipped = calcworkdays.date_last
SET a.workdays_total_shipment = calcworkdays.workdays
WHERE date_shipped IS NOT NULL;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_ordered = calcworkdays.date_first
AND curdate() = calcworkdays.date_last
SET a.workdays_total_shipment 	=	calcworkdays.workdays
WHERE date_shipped IS NULL
AND date_ordered IS NOT NULL;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_ready_to_pick = calcworkdays.date_first
AND calcworkdays.date_last = curdate() 
SET 
   r2s_workday_0  = IF( workdays_to_ready  = 0 AND workdays >= 0, 1 , r2s_workday_0 ),
   r2s_workday_1  = IF( workdays_to_ready <= 1 AND workdays >= 1, 1 , r2s_workday_1 ),
   r2s_workday_2  = IF( workdays_to_ready <= 2 AND workdays >= 2, 1 , r2s_workday_2 ),
   r2s_workday_3  = IF( workdays_to_ready <= 3 AND workdays >= 3, 1 , r2s_workday_3 ),
   r2s_workday_4  = IF( workdays_to_ready <= 4 AND workdays >= 4, 1 , r2s_workday_4 ),
   r2s_workday_5  = IF( workdays_to_ready <= 5 AND workdays >= 5, 1 , r2s_workday_5 ),
   r2s_workday_6  = IF( workdays_to_ready <= 6 AND workdays >= 6, 1 , r2s_workday_6 ), 
   r2s_workday_7  = IF( workdays_to_ready <= 7 AND workdays >= 7, 1 , r2s_workday_7 ), 
   r2s_workday_8  = IF( workdays_to_ready <= 8 AND workdays >= 8, 1 , r2s_workday_8 ),  
   r2s_workday_9  = IF( workdays_to_ready <= 9 AND workdays >= 9, 1 , r2s_workday_9 ),  
   r2s_workday_10 = IF( workdays_to_ready  >10 AND workdays >=10, 1 , r2s_workday_10);

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays b
ON a.date_exported = b.date_first
AND a.date_procured = b.date_last
SET a.workdays_to_procure = b.workdays
WHERE is_stockout = 0 AND fulfillment_type_real not like 'Dropshipping';

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays b
ON a.date_exported = b.date_first AND curdate() = b.date_last
SET a.workdays_to_procure =	b.workdays
WHERE is_stockout = 0 AND a.date_procured IS NULL AND a.fulfillment_type_real <> 'Dropshipping';

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
JOIN operations_@v_countryPrefix@.calcworkdays AS b
ON a.date_exported = b.date_last 
 AND b.workdays = a.min_delivery_time
 AND b.isweekday = 1 
 AND b.isholiday = 0
SET 
 a.min_delivery_date = b.date_first;
 
UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
JOIN operations_@v_countryPrefix@.calcworkdays AS c
ON a.date_ready_to_pick_bono = c.date_first AND c.workdays = 1 AND (c.isholiday_first = 1 OR c.isweekend_first = 1)
SET a.date_ready_to_pick_bono = c.date_last;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
JOIN operations_@v_countryPrefix@.calcworkdays
  ON a.date_exported = calcworkdays.date_first AND date_declared_backorder = calcworkdays.date_last
SET a.workdays_to_backorder_declared = calcworkdays.workdays
WHERE a.date_exported IS NOT NULL
AND a.date_declared_backorder IS NOT NULL;

##### Inverse Logistics #####
UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON date(a.date_last_track_il) = calcworkdays.date_first
		AND date(a.date_inbound_il) = calcworkdays.date_last
SET 
days_to_inbound_il = 
	IF(date_last_track_il IS NULL,
		NULL,
		DATEDIFF(date(date_inbound_il),date(date_last_track_il))),
workdays_to_inbound_il = 
	IF(date_last_track_il IS NULL,
		NULL,calcworkdays.workdays
		)
WHERE a.is_returned=1
AND a.date_inbound_il IS NOT NULL;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON date(a.date_inbound_il) = calcworkdays.date_first
		AND date(a.date_quality_il)= calcworkdays.date_last
SET 
days_to_quality_il = 
	IF(date_inbound_il IS NULL,
		NULL,
		DATEDIFF(date(date_quality_il),date(date_inbound_il))),
workdays_to_quality_il = 
	IF((date_inbound_il IS NULL),
		NULL,calcworkdays.workdays)
WHERE a.is_returned =1
AND date_quality_il is not null;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON date(date_delivered) = calcworkdays.date_first
		AND date(date_customer_request_il) = calcworkdays.date_last
SET 
days_to_customer_request_il = 
IF(date_delivered IS NULL,
		NULL,
	DATEDIFF(date(date_customer_request_il),date(date_delivered))),
workdays_to_customer_request_il  = 
IF(date_customer_request_il IS NULL,
		NULL,calcworkdays.workdays)
WHERE is_returned = 1
AND date_customer_request_il IS NOT NULL;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON date(a.date_customer_request_il) = calcworkdays.date_first
		AND date(a.date_first_track_il) = calcworkdays.date_last
SET 
days_to_track_il = 
	IF(date_customer_request_il IS NULL,
		NULL,
		DATEDIFF(date(date_first_track_il),date(date_customer_request_il))),
workdays_to_track_il = 
	IF(date_customer_request_il IS NULL,
		NULL,calcworkdays.workdays
		)
WHERE is_returned =1
AND date_first_track_il IS NOT NULL;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON date(a.date_quality_il) = calcworkdays.date_first
		AND date(a.date_refunded_il)= calcworkdays.date_last
SET 
days_to_refunded_il = 
	IF((date_quality_il IS NULL),
		NULL,
		DATEDIFF(date(date_refunded_il),date(date_quality_il))),
workdays_to_refunded_il = 
	IF((date_quality_il IS NULL),
		NULL,calcworkdays.workdays)
WHERE is_returned =1
AND date_refunded_il IS NOT NULL;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON date(a.date_customer_request_il) = calcworkdays.date_first
AND 
IF(date(a.date_refunded_il) IS NULL,CURDATE(),date(a.date_refunded_il)) = calcworkdays.date_last
SET 
workdays_total_refunded_il = 
	IF((date_customer_request_il IS NULL),
		NULL,calcworkdays.workdays)
WHERE is_returned =1
AND date_quality_il IS NOT NULL;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON date(a.date_customer_request_il)= calcworkdays.date_first
		AND date(a.date_closed_il)= calcworkdays.date_last
SET 
days_to_closed_il = 
	IF(date_customer_request_il IS NULL,
		NULL,
		DATEDIFF(date(date_closed_il),date(date_customer_request_il))),
workdays_to_closed_il = 
	IF(date_customer_request_il IS NULL,
		NULL,calcworkdays.workdays)
WHERE is_returned =1
AND date_closed_il IS NOT NULL;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
JOIN operations_@v_countryPrefix@.calcworkdays AS b
	ON DATE(a.date_customer_request_il)= b.date_first
		AND DATE(curdate())= b.date_last
SET 
backlog_track_il = IF(b.workdays >2,1,0),
workdays_backlog_track_il = IF(b.workdays >2,(b.workdays-2),0)
WHERE date_first_track_il IS NULL
AND date_inbound_il IS NULL
AND date_quality_il IS NULL
AND date_closed_il IS null
AND date_customer_request_il IS NOT NULL;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays AS b
	ON DATE(a.date_inbound_il)= b.date_first
		AND DATE(curdate())= b.date_last
SET 
backlog_quality_il = IF(b.workdays >3,1,0),
workdays_backlog_quality_il = IF(b.workdays >3,(b.workdays-3),0)
WHERE date_quality_il IS NULL
AND date_closed_il IS NULL
AND is_returned=1;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays AS b
	ON date(a.date_quality_il)= b.date_first
		AND date(curdate())= b.date_last
SET 
backlog_refund_reversion_il = IF(b.workdays>20,1,0),
workdays_backlog_refund_reversion_il = IF(b.workdays>20,(b.workdays-20),0)
WHERE date_cash_refunded_il IS NULL
AND date_voucher_refunded_il IS NULL
AND date_closed_il IS NULL
AND date_quality_il IS NOT NULL
AND is_returned = 1
AND action_il='Reembolso tarjeta o cuenta (10-20 días hábiles)';

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays AS b
	ON date(a.date_quality_il)= b.date_first
		AND date(curdate())= b.date_last
SET 
backlog_refund_voucher_il = IF(b.workdays>2,1,0),
workdays_backlog_refund_voucher_il = IF(b.workdays>2,(b.workdays-2),0)
WHERE date_cash_refunded_il IS NULL
AND date_voucher_refunded_il IS NULL
AND date_closed_il IS NULL
AND date_quality_il IS NOT NULL
AND is_returned =1
AND action_il='Reembolso en crédito Linio (2 días hábiles)';

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
INNER JOIN operations_@v_countryPrefix@.calcworkdays b
 ON a.date_inbound_il = b.date_first
 AND 2 = b.workdays
SET date_processed_return_promised = b.date_last
WHERE is_returned = 1;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON date(a.date_inbound_il) = calcworkdays.date_first
		AND date(a.date_exit_il) = calcworkdays.date_last
SET 
days_to_process_return = 
	IF(date_exit_il IS NULL,
		DATEDIFF(date(CURDATE()),date(date_inbound_il)),
		DATEDIFF(date(date_exit_il),date(date_inbound_il))),
time_to_process_return =
If( date_inbound_il is Null,
TIMESTAMPDIFF( SECOND ,date_inbound_il, CURDATE())/3600,
TIMESTAMPDIFF( SECOND ,date_inbound_il, date_exit_il)/3600),
workdays_to_process_return = 
	IF(date_exit_il IS NULL,
		NULL,calcworkdays.workdays
		)
WHERE a.is_returned=1
AND a.date_inbound_il IS NOT NULL;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON date(a.date_inbound_il) = calcworkdays.date_first
		AND date(CURDATE()) = calcworkdays.date_last
SET 
workdays_to_process_return = calcworkdays.workdays
WHERE a.is_returned=1
AND a.date_inbound_il IS NOT NULL
AND a.date_exit_il IS NULL;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON date(a.date_failed_delivery) = calcworkdays.date_first
		AND date(a.date_cancelled) = calcworkdays.date_last
SET 
days_to_process_fail_delivery = 
	IF(date_failed_delivery < date_cancelled ,
		DATEDIFF(date(date_cancelled),date(date_failed_delivery)),
   IF( date_failed_delivery < date_ready_to_ship ,
		DATEDIFF(date(date_ready_to_ship),date(date_failed_delivery)),DATEDIFF(date(CURDATE()),date(date_failed_delivery)))),
time_to_process_fail_delivery =
IF(date_failed_delivery < date_cancelled ,
		TIMESTAMPDIFF( SECOND ,date_failed_delivery, datetime_cancelled)/3600,
   IF( date_failed_delivery < date_ready_to_ship ,
		TIMESTAMPDIFF( SECOND ,date_failed_delivery, datetime_ready_to_ship)/3600,TIMESTAMPDIFF( SECOND ,date_failed_delivery, CURDATE())/3600)),
workdays_to_process_fail_delivery = 
	IF(date_failed_delivery < date_cancelled , calcworkdays.workdays , NULL)
WHERE a.is_fail_delivery=1
AND a.date_failed_delivery IS NOT NULL;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON date(a.date_failed_delivery) = calcworkdays.date_first
		AND date(a.date_ready_to_ship) = calcworkdays.date_last
SET 
workdays_to_process_fail_delivery = 
	IF(date_failed_delivery < date_ready_to_ship , calcworkdays.workdays , NULL)
WHERE a.is_fail_delivery=1
AND a.date_failed_delivery IS NOT NULL;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON date(a.date_failed_delivery) = calcworkdays.date_first
		AND date(CURDATE()) = calcworkdays.date_last
SET 
workdays_to_process_fail_delivery = calcworkdays.workdays 
WHERE a.is_fail_delivery=1
AND (date_failed_delivery > date_ready_to_ship OR date_failed_delivery > datetime_cancelled )
AND a.date_failed_delivery IS NOT NULL;



