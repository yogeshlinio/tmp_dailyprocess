CALL production.monitoring_step( "ODS.itens_venda_id", "@v_country@", "status_itens_venda_extraction" , 50 );

UPDATE itens_venda_oot_sample_@v_countryPrefix@
SET 
#ontime_to_po = If(date(date_po_created)<=date_po_created_promised,1,0),
#on_time_to_confirm = If(date(date_po_confirmed)<=date_po_confirmed_promised,1,0),
#on_time_to_send_po = If(date(date_po_issued)<=date_po_issued_promised,1,0),
on_time_to_procure = If(date(date_procured)<=date_procured_promised,1,0),
#ontime_to_export = If(date(date_exported)<=date_exported_promised,1,0),
on_time_to_ready = if(date_ready_to_ship<=date_ready_to_ship_promised,1,0),
on_time_to_ship = if(date_shipped<=date_ready_to_ship_promised,1,0),
on_time_to_1st_attempt = if(date_1st_attempt <=date_delivered_promised,1,0),
on_time_to_deliver = if(date_delivered<=date_delivered_promised,1,0),
#on_time_to_request_recollection = If(date_first_attempt<=date_delivered_promised,1,0),
#on_time_to_stockout_declared = If(date_shipped is not null,(if(date_shipped<=date_ready_to_ship_promised,1,0)),(if(date_ready_to_ship_promised<=curdate(),1,0))),
#on_time_to_create_po = If(date_shipped is not null,(if(date_shipped<=date_ready_to_ship_promised,1,0)),(if(date_ready_to_ship_promised<=curdate(),1,0))),
#on_time_to_create_tracking_code = If(date_first_attempt<=date_delivered_promised,1,0),
#on_time_total_1st_attempt = If(date_delivered<=date_delivered_promised,1,0),
on_time_total_delivery = If(date_delivered<=date_delivered_promised,1,0),
on_time_to_procure = CASE WHEN date_procured <= date_procured_promised 
							THEN 1
					 ELSE 0
					 END,
on_time_total_1st_attempt = CASE WHEN date_1st_attempt <= date_delivered_promised 
								   THEN 1
							ELSE 0
							END,
on_time_total_delivery = CASE WHEN date_delivered <= date_delivered_promised 
								THEN 1
							  WHEN date_delivered IS NULL 
								THEN 0
						 ELSE 0
						 END;