UPDATE itens_venda_oot_sample_@v_countryPrefix@
SET sac_backlog = operations_@v_countryPrefix@.dias_habiles_negativos(curdate(), date_declared_stockout)
WHERE date_processed_stockout IS NULL
AND date_declared_stockout IS NOT NULL;