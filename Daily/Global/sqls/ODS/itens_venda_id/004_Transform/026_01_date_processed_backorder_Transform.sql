UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
SET 
a.date_processed_backorder = IF(a.date_backorder_accepted IS NULL, a.date_backorder_tratada, a.date_backorder_accepted),
a.datetime_processed_backorder = IF(a.datetime_backorder_accepted IS NULL, a.datetime_backorder_tratada, a.datetime_backorder_accepted);