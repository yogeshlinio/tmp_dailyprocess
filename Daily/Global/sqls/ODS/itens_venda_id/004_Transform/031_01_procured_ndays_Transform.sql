UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
SET a.procured_3days = IF(a.workdays_to_procure <= 3,1,0),
a.procured_5days = IF(a.workdays_to_procure <= 5,1,0),
a.procured_10days = IF(a.workdays_to_procure <= 10,1,0);