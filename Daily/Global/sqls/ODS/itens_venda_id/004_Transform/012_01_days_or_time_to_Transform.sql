CALL production.monitoring_step( "ODS.itens_venda_id", "@v_country@", "status_itens_venda_extraction" , 50 );

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
SET 
a.days_to_procure = If(date_exported is Null,null,If(date_procured is Null, DATEDIFF(CURDATE(),date_exported), DATEDIFF(date_procured,date_exported))),
a.days_to_export = If(date_ordered is Null,null,If(date_exported is Null, DATEDIFF(CURDATE(),date_ordered), DATEDIFF(date_exported,date_ordered))),
a.days_to_ready = If(date_ready_to_pick is Null,null,If(date_ready_to_ship is Null, DATEDIFF(CURDATE(),date_ready_to_pick), DATEDIFF(date_ready_to_ship,date_ready_to_pick))),
a.days_to_ship = If(date_ready_to_ship is Null,null,If(date_shipped is Null, DATEDIFF(CURDATE(),date_ready_to_ship), DATEDIFF(date_shipped,date_ready_to_ship))),
a.days_to_1st_attempt = If(date_shipped is Null,null,If(date_1st_attempt is Null, DATEDIFF(CURDATE(),date_shipped), DATEDIFF(date_1st_attempt,date_shipped))),
a.days_to_deliver = If(date_shipped is Null,null,If(date_delivered is Null, DATEDIFF(CURDATE(),date_shipped), DATEDIFF(date_delivered,date_shipped))),
a.days_to_request_recollection = If(date_shipped is Null,null,If(date_delivered is Null, DATEDIFF(CURDATE(),date_shipped), DATEDIFF(date_delivered,date_shipped))),
a.days_to_stockout_declared = If(date_ordered is Null,null,If(date_declared_stockout is Null, DATEDIFF(CURDATE(),date_ordered), DATEDIFF(date_declared_stockout,date_ordered))),
a.days_to_backorder_declared = If(date_exported is Null,null,If(date_declared_backorder is Null, null, DATEDIFF(date_declared_backorder,date_exported))),
a.days_total_1st_attempt = If(date_ordered is Null,null,If(date_1st_attempt is Null, DATEDIFF(CURDATE(),date_ordered), DATEDIFF(date_1st_attempt,date_ordered))),
a.days_total_delivery = If(date_ordered is Null,null,If(date_delivered is Null, DATEDIFF(CURDATE(),date_ordered), DATEDIFF(date_delivered,date_ordered))),
a.time_to_backorder = If(datetime_exported is Null,null,If(datetime_backorder is Null, TIMESTAMPDIFF(SECOND,datetime_exported, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_exported,datetime_backorder)/3600)),
a.time_to_backorder_tratada = If(datetime_backorder is Null,null,If(datetime_backorder_tratada is Null, TIMESTAMPDIFF(SECOND,datetime_backorder, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_backorder,datetime_backorder_tratada)/3600)),
a.time_to_backorder_accepted = If(datetime_backorder is Null,null,If(datetime_backorder_accepted is Null, TIMESTAMPDIFF(SECOND,datetime_backorder, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_backorder,datetime_backorder_accepted)/3600)),
a.time_to_declared_stockout = If(datetime_exported is Null,null,If(datetime_declared_stockout is Null, TIMESTAMPDIFF(SECOND,datetime_exported, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_exported,datetime_declared_stockout)/3600)),
a.time_to_declared_backorder = If(datetime_exported is Null,null,If(datetime_declared_backorder is Null, TIMESTAMPDIFF(SECOND,datetime_exported, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_exported,datetime_declared_backorder)/3600)),
a.days_total_shipment = If(date_ordered is Null,null,If(date_shipped is Null, DATEDIFF(CURDATE(),date_ordered), DATEDIFF(date_shipped,date_ordered)));

UPDATE itens_venda_oot_sample_@v_countryPrefix@
SET 
days_to_procure = NULL,
days_to_ready = NULL,
days_to_ship = days_total_shipment-days_to_export,
workdays_to_procure = NULL,
workdays_to_ready = NULL,
workdays_to_ship = workdays_total_shipment-workdays_to_export,
time_to_procure = NULL,
time_to_ready = NULL,
time_to_ship = time_total_shipment-time_to_export
WHERE fulfillment_type_real = 'Dropshipping';
