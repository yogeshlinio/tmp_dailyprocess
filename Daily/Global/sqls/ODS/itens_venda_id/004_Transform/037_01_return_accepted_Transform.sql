UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
SET 
a.return_accepted = CASE WHEN a.status_wms_il = 'retornar_stock'
                         OR a.status_wms_il = 'enviar_cuarentena' 
						   THEN 1
                    ELSE
	                     NULL
                    END;