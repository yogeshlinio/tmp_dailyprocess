UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
SET 
delay_to_procure = CASE WHEN date_procured IS NULL 
                          THEN(CASE WHEN curdate() > date_procured_promised 
							 	    AND fulfillment_type_real NOT IN ('Dropshipping')
							          THEN 1
							   ELSE 0
							   END)
				   ELSE(CASE WHEN date_procured > date_procured_promised 
							   THEN 1
						ELSE 0
						END)
				   END,
delay_to_ready = CASE WHEN workdays_to_ready > 1 
						THEN 1
				 ELSE 0
				 END,
delay_to_ship = CASE WHEN workdays_to_ship > 1 
					   THEN 1
			    ELSE 0
				END,
delay_to_1st_attempt = CASE WHEN workdays_to_1st_attempt > 2 
							  THEN 1
					   ELSE 0
					   END,
delay_to_delivery = CASE WHEN workdays_to_deliver > 2 
						   THEN 1
					ELSE 0
					END,
delay_total_1st_attempt = CASE WHEN date_1st_attempt IS NULL 
                                 THEN (CASE WHEN curdate() > date_delivered_promised 
											  THEN 1
									   ELSE 0
									   END)
						  ELSE (CASE WHEN date_1st_attempt > date_delivered_promised 
									   THEN 1
								ELSE 0
								END)
						  END,
delay_total_delivery = CASE WHEN date_delivered IS NULL 
							  THEN (CASE WHEN curdate() > date_delivered_promised 
										   THEN 1
									ELSE 0
									END)
					   ELSE (CASE WHEN date_delivered > date_delivered_promised 
								    THEN 1
							 ELSE 0
							 END)
					   END,
workdays_delay_to_procure = CASE WHEN workdays_to_procure - supplier_leadtime < 0 
								   THEN operations_@v_countryPrefix@.dias_habiles_negativos(date_procured_promised, CURDATE())
							ELSE workdays_to_procure - supplier_leadtime
							END,
workdays_delay_to_ready = CASE WHEN workdays_to_ready - 1 < 0 
								 THEN 0
						  ELSE workdays_to_ready - 1
						  END,
workdays_delay_to_ship = CASE WHEN workdays_to_ship - 1 < 0 
								THEN 0
						 ELSE workdays_to_ship - 1
						 END,
workdays_delay_to_1st_attempt = CASE WHEN workdays_to_1st_attempt - 3 < 0 
									   THEN 0
								ELSE workdays_to_1st_attempt - 3
								END,
workdays_delay_to_delivery = CASE WHEN workdays_to_deliver - 3 < 0 
									THEN 0
							 ELSE workdays_to_deliver - 3
							 END;