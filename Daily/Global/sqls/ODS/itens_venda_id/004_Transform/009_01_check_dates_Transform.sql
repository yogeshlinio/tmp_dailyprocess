UPDATE itens_venda_oot_sample_@v_countryPrefix@
SET check_dates =
								 IF (date_ordered > date_exported,"Date ordered > date exported",
								 IF (date_exported > date_procured,"Date exported > Date procured",
								 IF (date_procured > date_ready_to_ship,"Date procured > Date ready to ship",
								 IF (date_ready_to_ship > date_shipped,"Date ready to ship > Date shipped",
								 IF (date_shipped > date_1st_attempt,"Date shipped > Date 1st attempt",
								 IF (date_1st_attempt > date_delivered,"Date 1st atteempt> Date delivered",
																 "Correct")))))),
check_date_exported = CASE WHEN date_exported IS NULL
							THEN (CASE WHEN date_procured IS NULL
										AND date_ready_to_ship IS NULL
										AND date_shipped IS NULL
										AND date_1st_attempt IS NULL
										AND date_delivered IS NULL
										THEN 0
								  ELSE 1
								  END )
					  ELSE (CASE WHEN date_exported > date_procured
								  THEN 1
							ELSE 0
							END)
					  END,
check_date_ordered = CASE WHEN date_ordered IS NULL
							THEN (CASE WHEN date_exported IS NULL
										AND date_procured IS NULL
										AND date_ready_to_ship IS NULL
										AND date_shipped IS NULL
										AND date_1st_attempt IS NULL
										AND date_delivered IS NULL
										THEN 0
								  ELSE 1
								  END )
					  ELSE (CASE WHEN date_ordered > date_exported
								  THEN 1
							ELSE 0
							END )
					  END,
check_date_procured = CASE WHEN date_procured IS NULL
							THEN (CASE WHEN date_ready_to_ship IS NULL
										AND date_shipped IS NULL
										AND date_1st_attempt IS NULL
										AND date_delivered IS NULL
										THEN 0
								  ELSE 1
								  END )
					  ELSE (CASE WHEN date_procured > date_ready_to_ship
										THEN 1
								  ELSE 0
								  END )
					  END,
check_date_ready_to_ship = CASE WHEN date_ready_to_ship IS NULL
								 THEN (CASE WHEN date_shipped IS NULL
											 AND date_1st_attempt IS NULL
											 AND date_delivered IS NULL
											 THEN 0
									   ELSE 1
									   END )
						   ELSE (CASE WHEN date_ready_to_ship > date_shipped
									   THEN 1
								 ELSE 0
								 END)
						   END,
check_date_shipped = CASE WHEN date_shipped IS NULL
						   THEN (CASE WHEN date_1st_attempt IS NULL
								       AND date_delivered IS NULL
									   THEN 0
								 ELSE 1
								 END )
					 ELSE (CASE WHEN date_shipped > date_1st_attempt
								 THEN 1
						   ELSE 0
						   END)
					 END,
check_date_1st_attempt = CASE WHEN date_1st_attempt IS NULL
							   THEN (CASE WHEN date_delivered IS NULL
										   THEN 0
									 ELSE 1
									 END )
						 ELSE (CASE WHEN date_1st_attempt > date_delivered
									 THEN 1
							   ELSE 0
							   END )
						 END,
check_date_stockout_over_bo = IF(min_delivery_time>=max_delivery_time,1,0);