UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN @v_production@.calendar AS b
ON DATE(a.date_shipped)=b.dt AND b.isweekend=1
SET a.shipped_is_weekend=1;