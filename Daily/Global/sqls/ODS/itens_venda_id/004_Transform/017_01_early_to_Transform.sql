UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
SET a.early_to_procure = CASE WHEN ( CASE WHEN date_procured IS NULL
                                          THEN datediff(date_procured_promised,curdate())
                                     ELSE datediff(date_procured_promised,date_procured)
                                     END) > 0
                               THEN 1
                         ELSE 0
                         END,
a.early_to_1st_attempt = CASE WHEN ( CASE WHEN date_1st_attempt IS NULL
                                          THEN datediff(date_delivered_promised,curdate())
                                     ELSE datediff(date_delivered_promised,date_1st_attempt)
                                     END) > 0
                              THEN 1
                         ELSE 0
                         END;