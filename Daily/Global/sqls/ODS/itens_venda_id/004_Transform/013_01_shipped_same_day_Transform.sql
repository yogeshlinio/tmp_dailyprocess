UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
SET 
a.shipped_same_day_as_procured = If (date_procured = date_shipped,1,0), 
a.shipped_same_day_as_order = If (date_ordered = date_shipped ,1,0);