CALL production.monitoring_step( "ODS.Sku_Simple", "@v_country@", "CatBP" , 50 );

DROP TABLE IF EXISTS TMP_sku_simple_itens_venda_@v_countryPrefix@;
CREATE TEMPORARY TABLE TMP_sku_simple_itens_venda_@v_countryPrefix@ (INDEX (country, sku_simple))
SELECT country,
       Sku_simple,
	   fulfillment_type,
	   Cat1,
	   Cat2,
	   Cat3,
	   Cat_BP,
	   Cat_KPI,
	   Head_Buyer,
	   Buyer,
	   sku_config,
	   sku_name,
	   id_supplier,
	   Supplier,
	   Supplier_Type,
	   Package_Height,
	   Package_Length,
	   Package_Width,
	   Package_Weight
FROM Sku_Simple_Sample_@v_countryPrefix@
WHERE country='@v_countryPrefix@';

UPDATE            itens_venda_oot_sample_@v_countryPrefix@ AS a
       INNER JOIN itens_venda_oot_key_map_Sample_@v_countryPrefix@ 
	        USING ( country , item_id )
       INNER JOIN TMP_sku_simple_itens_venda_@v_countryPrefix@ AS b
	        USING ( country , Sku_Simple )
SET
	a.fulfillment_type_bob = b.fulfillment_type,
	a.cat_1 = b.Cat1,
 	a.cat_2 = b.Cat2,
 	a.cat_3 = b.Cat3,
 	a.category_bp = Cat_BP,
 	a.category_kpi = Cat_KPI,
 	a.head_buyer = b.Head_Buyer,
 	a.buyer = b.Buyer,
 	a.sku_config = b.sku_config,
 	a.sku_name = b.sku_name,
 	a.supplier_id = b.id_supplier,
 	a.supplier_name = b.Supplier,
 	a.supplier_type = b.Supplier_Type,
 	a.package_height = b.Package_Height,
 	a.package_length = b.Package_Length,
 	a.package_width = b.Package_Width,
 	a.package_weight = b.Package_Weight
;

DROP TABLE IF EXISTS fulfillment_type_crossdocking_@v_countryPrefix@;
CREATE TABLE fulfillment_type_crossdocking_@v_countryPrefix@ (INDEX (item_id))
SELECT b.item_id,
       'Crossdocking' as fulfillment_type_real
FROM itens_venda_oot_key_map_Sample_@v_countryPrefix@ b
JOIN @v_wmsprod@.status_itens_venda c
ON b.itens_venda_id = c.itens_venda_id
WHERE c.STATUS IN (	'analisando quebra','aguardando estoque');
			
UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN fulfillment_type_crossdocking_@v_countryPrefix@ b
ON a.item_id = b.item_id
SET 
   a.fulfillment_type_real = IF(a.fulfillment_type_bob = 'Dropshipping_cod', a.fulfillment_type_bob, b.fulfillment_type_real);

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
SET a.date_ready_to_ship = a.date_shipped
WHERE a.fulfillment_type_real = 'Dropshipping';
   
/* Ahora esta en stock_item_id
UPDATE itens_venda_oot_sample_@v_countryPrefix@
SET 
   fulfillment_type_bp = CASE WHEN fulfillment_type_real IN( 'crossdocking', 'consignment')
				                   THEN fulfillment_type_real
                              #WHEN fulfillment_type_real = 'Own Warehouse' and fulfillment_type_bob = 'Consignment'
				              #     THEN fulfillment_type_bob
				              WHEN fulfillment_type_real = 'Own Warehouse'
				                   THEN 'Outright Buying'
				              WHEN fulfillment_type_real = 'dropshipping' 
				                   THEN 'other'
				              WHEN fulfillment_type_real is null
				                   THEN fulfillment_type_bob
                         END;
*/

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'ODS.itens_venda_id',
  "fulfillment_type_real",
  NOW(),
  NOW(),
  0,
  0
;

UPDATE itens_venda_oot_sample_@v_countryPrefix@
SET
still_to_procure = IF(date_procured IS NULL AND status_wms NOT IN ('quebrado', 'quebra tratada') AND fulfillment_type_real = 'Crossdocking',1,0),
is_gift_card = IF(supplier_name LIKE '%E-Gift Card%',1,0),
is_presale = IF(sku_name LIKE "%preventa%",1,0);

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'ODS.itens_venda_id',
  "is_marks_2",
  NOW(),
  NOW(),
  0,
  0
;

CALL production.monitoring_step( "ODS.Sku_Simple", "@v_country@", "Product_Measures" , 50 );

DROP TABLE IF EXISTS TMP_sku_simple_itens_venda_@v_countryPrefix@;
CREATE TEMPORARY TABLE TMP_sku_simple_itens_venda_@v_countryPrefix@ (INDEX (country, sku_simple))
SELECT country,
       Sku_simple,
	   Volumetric_Weight,
	   Max_VolW_Weight,
	   package_measure_new
FROM Sku_Simple_Sample_@v_countryPrefix@
WHERE country='@v_countryPrefix@';

UPDATE            itens_venda_oot_sample_@v_countryPrefix@ AS a
       INNER JOIN itens_venda_oot_key_map_Sample_@v_countryPrefix@ 
	        USING ( country , item_id )
       INNER JOIN TMP_sku_simple_itens_venda_@v_countryPrefix@ AS b
	        USING ( country , Sku_Simple )
SET
a.vol_weight=b.Volumetric_Weight,
a.max_vol_w_vs_w=b.Max_VolW_Weight,
a.package_measure_new=b.package_measure_new;