UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
SET 
a.24hr_shipments_WH_persp_bono = IF(date_ready_to_pick_bono IS NULL,NULL,
                                    IF(date_shipped IS NULL, IF( DATEDIFF(CURDATE(),date_ready_to_pick_bono)= 0,1,0), 
									IF( DATEDIFF(date_shipped,date_ready_to_pick_bono)=0,1,0)));