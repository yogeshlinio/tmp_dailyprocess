#workdays to create po
UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_exported = calcworkdays.date_first
AND a.date_po_created = calcworkdays.date_last
SET a.workdays_to_po =	calcworkdays.workdays;

#Workdays to confirm
UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_po_created = calcworkdays.date_first
AND a.date_po_confirmed = calcworkdays.date_last
SET a.workdays_to_confirm =	calcworkdays.workdays;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_po_created = calcworkdays.date_first
AND curdate() = calcworkdays.date_last
SET a.workdays_to_confirm =	calcworkdays.workdays
WHERE date_exported IS NULL;

#Workdays to send po
UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_po_confirmed = calcworkdays.date_first
AND a.date_po_issued = calcworkdays.date_last
SET a.workdays_to_send_po =	calcworkdays.workdays;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_po_confirmed = calcworkdays.date_first
AND curdate() = calcworkdays.date_last
SET a.workdays_to_send_po =	calcworkdays.workdays
WHERE date_exported IS NULL;

#workdays to create tracking code
UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.calcworkdays
ON a.date_po_confirmed = calcworkdays.date_first
AND a.date_shipped = calcworkdays.date_last
SET a.workdays_to_create_tracking_code = calcworkdays.workdays
WHERE date_po_confirmed is not null AND date_shipped is not null ;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
SET 
a.days_to_po = If(date_exported is Null,null,If(date_po_created is Null, DATEDIFF(CURDATE(),date_exported), DATEDIFF(date_po_created,date_exported))),
a.days_to_send_po = If(date_po_created is Null,null,If(date_po_issued is Null, DATEDIFF(CURDATE(),date_po_created), DATEDIFF(date_po_issued,date_po_created))),
a.days_to_confirm = If(date_po_issued is Null,null,If(date_po_confirmed is Null, DATEDIFF(CURDATE(),date_po_issued), DATEDIFF(date_po_confirmed,date_po_issued))),
a.days_to_createPO = If(date_exported is Null,null,If(date_po_created is Null, DATEDIFF(CURDATE(),date_exported), DATEDIFF(date_po_created,date_exported))),
a.days_to_create_tracking_code = If(date_po_confirmed is Null,null,If(date_shipped is Null, DATEDIFF(CURDATE(),date_po_confirmed), DATEDIFF(date_shipped,date_po_confirmed))),
a.time_to_po = If(datetime_exported is Null,null,If(datetime_po_created is Null, TIMESTAMPDIFF( SECOND ,datetime_exported, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_exported,datetime_po_created)/3600)),
a.time_to_send_po = If(datetime_po_created is Null,null,If(datetime_po_issued is Null, TIMESTAMPDIFF( SECOND ,datetime_po_created,CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_po_created, datetime_po_issued)/3600)),
a.time_to_confirm = If(datetime_po_issued is Null,null,If(datetime_po_confirmed is Null, TIMESTAMPDIFF( SECOND ,datetime_po_issued,CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_po_issued,datetime_po_confirmed)/3600)),
a.time_to_create_tracking_code = If(datetime_po_confirmed is Null,null,If(datetime_shipped is Null, TIMESTAMPDIFF( SECOND ,datetime_po_confirmed, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_po_confirmed, datetime_shipped)/3600)),
a.time_to_createPO = If(datetime_exported is Null,null,If(datetime_po_created is Null, TIMESTAMPDIFF( SECOND ,datetime_exported, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_exported, datetime_po_created)/3600));