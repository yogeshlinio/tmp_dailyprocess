UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
JOIN @v_wmsprod@.inverselogistics_agendamiento AS b
ON a.item_id=b.item_id
SET a.shipping_carrier_tracking_code_inverse = b.carrier_tracking_code;