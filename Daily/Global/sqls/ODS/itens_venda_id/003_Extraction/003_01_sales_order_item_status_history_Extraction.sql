UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN itens_venda_oot_key_map_Sample_@v_countryPrefix@ b
ON a.item_id=b.item_id
JOIN 
(SELECT
	ib.fk_sales_order_item as item_id,
	MIN(ib.created_at) AS min_of_created_at
FROM
	@bob_live@.sales_order_item_status ia
INNER JOIN @bob_live@.sales_order_item_status_history ib
	ON ia.id_sales_order_item_status = ib.fk_sales_order_item_status
WHERE ia.name IN("exported", "exportable", "exported electronically")
GROUP BY
	ib.fk_sales_order_item) c
ON b.item_id=c.item_id
SET a.date_exported = DATE(c.min_of_created_at),
a.datetime_exported = c.min_of_created_at
WHERE a.date_exported IS NULL;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check
)
SELECT 
  '@v_country@', 
  'ODS.itens_venda_id',
  'date_exported',
  NOW(),
  NOW(),
  0,
  0
;

/*
*  Insert Monitoring LOG
*/
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'ODS.itens_venda_id',
  "date_exported",
  NOW(),
  NOW(),
  count(date_exported),
  count(date_exported) 
FROM
   itens_venda_oot_sample_@v_countryPrefix@
;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ c
JOIN itens_venda_oot_key_map_Sample_@v_countryPrefix@ a
ON c.item_id=a.item_id
JOIN @bob_live@.sales_order_item_status_history AS b
ON a.item_id=b.fk_sales_order_item
SET c.stockout_order_nr = (SUBSTR(b.note,(SELECT INSTR(b.note, '200')),9))
WHERE c.is_stockout=1 AND b.fk_sales_order_item_status=9;