UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN itens_venda_oot_key_map_Sample_@v_countryPrefix@ b
ON a.item_id=b.item_id
JOIN @v_wmsprod@.itens_venda c
ON b.itens_venda_id=c.itens_venda_id
INNER JOIN 
(SELECT itens_venda_id,MIN(date) AS date FROM @v_wmsprod@.comunication_itens_venda
GROUP BY itens_venda_id) AS d
 ON b.itens_venda_id = d.itens_venda_id
SET a.order_id=c.order_id ,
    a.order_number=c.numero_order ,
	a.sku_simple=c.sku ,
	a.status_wms=c.status ,
	a.min_delivery_time=c.tempo_de_entrega_minimo ,
	a.max_delivery_time=c.tempo_de_entrega_maximo ,
	a.date_exported=date(c.data_exportable),
	a.datetime_exported=c.data_exportable,
	a.supplier_leadtime= c.tempo_entrega_fornecedor,
	a.is_linio_promise=if(c.linio_promise IS NULL, 0, c.linio_promise) ,
	a.wh_time= c.tempo_armazem,
	a.carrier_time = c.tempo_expedicao,
	a.note_tracking = c.obs
WHERE DATE_FORMAT(d.date,'%Y%m') >= (DATE_FORMAT(curdate()- INTERVAL 3 MONTH,'%Y%m'));

#Usado para mas adelante obtener el fulfillment_type_real_stock que utiliza el reporte de stock
DROP TABLE IF EXISTS tmp_sales_for_stock_@v_countryPrefix@;
CREATE TABLE tmp_sales_for_stock_@v_countryPrefix@ ( INDEX (itens_venda_id))
SELECT 
		t.itens_venda_id,
		t.data_criacao,
		t.status 
FROM @v_wmsprod@.itens_venda t 
JOIN (SELECT estoque_id, 
			MAX(data_criacao) AS max 
	  FROM @v_wmsprod@.itens_venda 
	  GROUP BY estoque_id) t2 
ON t.data_criacao=t2.max 
AND t.estoque_id=t2.estoque_id;