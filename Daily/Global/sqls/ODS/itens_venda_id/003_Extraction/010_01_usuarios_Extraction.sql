DROP TEMPORARY TABLE IF EXISTS tmp_usuarios_cs_@v_countryPrefix@; 
CREATE TEMPORARY TABLE tmp_usuarios_cs_@v_countryPrefix@(INDEX (item_id))
SELECT
 c.item_id,
 a.`status`,
 a.itens_venda_id,
 b.nome AS UserName
FROM @v_wmsprod@.status_itens_venda a
JOIN @v_wmsprod@.usuarios b
 ON a.usuarios_id = b.usuarios_id
JOIN itens_venda_oot_key_map_Sample_@v_countryPrefix@ c
ON a.itens_venda_id = c.itens_venda_id
where a.`status` in ('backorder_tratada','quebra tratada');

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
JOIN tmp_usuarios_cs_@v_countryPrefix@ AS b
ON b.item_id = a.item_id
SET a.user_cs_bo = CASE WHEN b.status='backorder_tratada' THEN b.UserName ELSE a.user_cs_bo END,
a.user_cs_oos = CASE WHEN b.status='quebra tratada' THEN b.UserName ELSE a.user_cs_oos END;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
JOIN (
      SELECT item_id,nome FROM 
      (SELECT itens_venda_id, MAX(data) data
       FROM @v_wmsprod@.status_itens_venda AS a
       GROUP BY itens_venda_id) AS t1
      JOIN @v_wmsprod@.status_itens_venda AS st
      ON t1.itens_venda_id=st.itens_venda_id AND t1.data=st.data
      JOIN @v_wmsprod@.usuarios AS b
      ON b.usuarios_id=st.usuarios_id
      JOIN @v_wmsprod@.itens_venda AS c
      ON c.itens_venda_id=st.itens_venda_id
      AND c.status=st.status
	  ) t1
ON a.item_id=t1.item_id
SET a.user_status_wms = t1.nome;

DROP TEMPORARY TABLE IF EXISTS TMP3analyst_@v_countryPrefix@ ; 
CREATE TEMPORARY TABLE TMP3analyst_@v_countryPrefix@
SELECT d.*, e.nome
FROM
(SELECT
		b.user_id,
		b.return_id,
		b.STATUS,
		b.changed_at,
		c.item_id
	FROM
		@v_wmsprod@.inverselogistics_status_history b
	JOIN @v_wmsprod@.inverselogistics_devolucion c ON b.return_id = c.id) d 
INNER JOIN @v_wmsprod@.usuarios as e ON d.user_id = e.usuarios_id
WHERE
	d.STATUS = 6;

CREATE INDEX itevm ON TMP3analyst_@v_countryPrefix@(item_id);

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
INNER JOIN TMP3analyst_@v_countryPrefix@ as d ON a.item_id = d.item_id
SET a.analyst_il = d.nome,
a.date_inbound_il  = d.changed_at
WHERE
	d.STATUS = 6;

DROP TEMPORARY TABLE IF EXISTS TMP2analyst_@v_countryPrefix@ ; 
CREATE TEMPORARY TABLE TMP2analyst_@v_countryPrefix@
SELECT
		b.cod_rastreamento,
		b.id_status,
		b.id_user,
		c.nome
	FROM
		@v_wmsprod@.tms_status_delivery b
	JOIN @v_wmsprod@.usuarios c ON b.id_user = c.usuarios_id 
WHERE
	b.id_status = 10 ;

CREATE INDEX cod ON TMP2analyst_@v_countryPrefix@(cod_rastreamento);

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN TMP2analyst_@v_countryPrefix@ AS b 
ON b.cod_rastreamento = a.wms_tracking_code
SET a.analyst_il = b.nome
WHERE
	a.il_type = 'Fail delivery';
	
	
UPDATE itens_venda_oot_sample_@v_countryPrefix@ as a
SET a.date_returned = a.date_inbound_il;