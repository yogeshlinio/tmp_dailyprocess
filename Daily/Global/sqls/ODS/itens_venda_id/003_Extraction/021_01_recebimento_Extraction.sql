UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN itens_venda_oot_key_map_Sample_@v_countryPrefix@ b
ON a.item_id=b.item_id
JOIN @v_wmsprod@.estoque c
ON b.estoque_id=c.estoque_id
JOIN @v_wmsprod@.itens_recebimento d
ON c.itens_recebimento_id=d.itens_recebimento_id
JOIN @v_wmsprod@.recebimento e
ON d.recebimento_id=e.recebimento_id
SET a.purchase_order = e.inbound_document_identificator
WHERE e.inbound_document_type_id = 4;