UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
INNER JOIN @bob_live@.sales_order_item AS b
 ON a.item_id = b.id_sales_order_item
INNER JOIN @bob_live@.sales_order_item_status AS c
 ON b.fk_sales_order_item_status = c.id_sales_order_item_status
SET a.status_bob = c.NAME;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
INNER JOIN @bob_live@.sales_order_item AS b
 ON a.item_id = b.id_sales_order_item
SET 
 a.supplier_leadtime = b.delivery_time_supplier
Where a.supplier_leadtime is null ;