DROP TABLE IF EXISTS itens_venda_oot_sample_@v_countryPrefix@;
CREATE TABLE itens_venda_oot_sample_@v_countryPrefix@ LIKE itens_venda_oot_template;
/*INSERT ALL THE PRIMARY KEYS*/
INSERT itens_venda_oot_sample_@v_countryPrefix@
(
  `Country`           ,
  `item_id`        
)
SELECT 
   '@v_countryPrefix@' AS Country,
   a.item_id
FROM 
   @v_wmsprod@.itens_venda a
JOIN (
SELECT itens_venda_id, MIN(date) AS date FROM @v_wmsprod@.comunication_itens_venda
GROUP BY itens_venda_id
HAVING MIN(date) >= curdate() - INTERVAL 3 MONTH
) b
ON a.itens_venda_id = b.itens_venda_id;
   
   
   