UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN itens_venda_oot_key_map_Sample_@v_countryPrefix@ b
ON a.item_id=b.item_id
JOIN @v_wmsprod@.comunication_itens_venda c
ON b.itens_venda_id=c.itens_venda_id
SET a.date_exported = DATE(c.date),
a.datetime_exported = c.date
WHERE c.comunication = 'Comunicacion completa';