TRUNCATE operations_@v_countryPrefix@.pro_1st_attempt;

INSERT INTO operations_@v_countryPrefix@.pro_1st_attempt (order_item_id, id_status, min_of_date, max_of_date) 
SELECT
	a.item_id,
	tms_status_delivery.id_status,
	MIN(tms_status_delivery.date) AS min_of_date,
	MAX(tms_status_delivery.date) AS max_of_date
FROM itens_venda_oot_key_map_Sample_@v_countryPrefix@ a
JOIN @v_wmsprod@.vw_itens_venda_entrega 
ON a.item_id = vw_itens_venda_entrega.item_id
JOIN @v_wmsprod@.tms_status_delivery 
ON vw_itens_venda_entrega.cod_rastreamento = tms_status_delivery.cod_rastreamento
WHERE tms_status_delivery.id_status IN (5,10)
GROUP BY a.item_id, tms_status_delivery.id_status;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.pro_1st_attempt b
ON a.item_id = b.order_item_id
SET a.date_1st_attempt = CASE WHEN b.id_status=5 THEN b.min_of_date ELSE a.date_1st_attempt END,
a.date_failed_delivery = CASE WHEN b.id_status=10 THEN b.max_of_date ELSE a.date_failed_delivery END;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN @v_wmsprod@.vw_itens_venda_entrega vw 
ON a.item_id = vw.item_id
JOIN @v_wmsprod@.tms_status_delivery del 
ON vw.cod_rastreamento = del.cod_rastreamento
SET a.date_1st_attempt = (
	SELECT
		max(tms.date)
	FROM
		@v_wmsprod@.tms_status_delivery tms,
		@v_wmsprod@.vw_itens_venda_entrega itens
	WHERE
		del.cod_rastreamento = tms.cod_rastreamento
	AND vw.item_id = itens.item_id
	AND tms.id_status = 4
)
WHERE
	a.date_1st_attempt IS NULL
OR a.date_1st_attempt < '2011-05-01';

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN @v_wmsprod@.tms_status_delivery b 
ON a.wms_tracking_code = b.cod_rastreamento
SET a.il_type = 'Fail delivery',
a.is_fail_delivery = 1, 
a.date_inbound_il = b.date 
WHERE b.id_status = 10;

#2.Se usa para las columnas de status_value_chain
DROP TABLE IF EXISTS tmp_tms_status_delivery_sub_@v_countryPrefix@;
CREATE TABLE tmp_tms_status_delivery_sub_@v_countryPrefix@(INDEX (cod_rastreamento))
	SELECT a.cod_rastreamento,a.date AS data,b.nome AS user,c.status
	FROM @v_wmsprod@.tms_status_delivery AS a
	INNER JOIN @v_wmsprod@.usuarios AS b
	ON a.id_user=b.usuarios_id
	INNER JOIN @v_wmsprod@.tms_status AS c
	ON a.id_status=c.id
	ORDER BY a.date DESC;
	
#2.1 Se usa para las columnas de status_value_chain
DROP TABLE IF EXISTS tmp_tms_status_delivery_@v_countryPrefix@;
CREATE TABLE tmp_tms_status_delivery_@v_countryPrefix@(INDEX (item_id))
SELECT t1.item_id,t2.status,t2.data,t2.user
FROM itens_venda_oot_sample_@v_countryPrefix@ AS t1
INNER JOIN tmp_tms_status_delivery_sub_@v_countryPrefix@ AS t2
ON t1.wms_tracking_code=t2.cod_rastreamento;