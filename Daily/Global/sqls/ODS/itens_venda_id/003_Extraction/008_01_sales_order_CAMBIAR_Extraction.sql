UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS b
JOIN itens_venda_oot_key_map_Sample_@v_countryPrefix@ a
ON b.item_id=a.item_id
JOIN @bob_live@.sales_order AS c
ON b.order_number = c.order_nr 
SET 
 b.date_ordered = date(c.created_at),
 b.datetime_ordered = c.created_at
;