UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN @v_wmsprod@.entrega b
ON a.wms_tracking_code = b.cod_rastreamento
SET a.delivery_fullfilment = b.delivery_fulfillment;