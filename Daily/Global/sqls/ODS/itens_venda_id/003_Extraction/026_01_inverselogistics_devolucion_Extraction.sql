DROP TEMPORARY TABLE IF EXISTS TMP2_@v_countryPrefix@ ; 
CREATE TEMPORARY TABLE TMP2_@v_countryPrefix@ 
SELECT c.*, d.description 
FROM @v_wmsprod@.inverselogistics_devolucion as c 
JOIN @v_wmsprod@.inverselogistics_devolucion_razon as d 
ON c.reason_id = d.reason_id;

CREATE INDEX item_id ON TMP2_@v_countryPrefix@(item_id);

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN TMP2_@v_countryPrefix@ b ON a.item_id = b.item_id
SET a.status_wms_il = b.status,
a.id_ilwh = b.id,
a.reason_for_entrance_il = b.description,
date_canceled_il = b.created_at;

DROP TEMPORARY TABLE IF EXISTS TMP3_@v_countryPrefix@; 
CREATE TEMPORARY TABLE TMP3_@v_countryPrefix@
SELECT c.*, d.description 
FROM @v_wmsprod@.inverselogistics_devolucion as c 
JOIN @v_wmsprod@.inverselogistics_devolucion_accion as d 
ON c.action_id = d.action_id;

CREATE INDEX item_id ON TMP3_@v_countryPrefix@(item_id);

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN TMP3_@v_countryPrefix@ b ON a.item_id = b.item_id
SET a.action_il = b.description;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
INNER JOIN @v_wmsprod@.inverselogistics_devolucion as b ON a.item_id = b.item_id
SET a.date_exit_il = CASE WHEN a.status_wms_il = 'retornar_stock'
                          OR a.status_wms_il= 'enviar_cuarentena'
                          OR a.status_wms_il= 'enviar_cuarentena' 
						    THEN modified_at
                     ELSE
	                      NULL
                     END;
					 
#3.Se usa para las columnas de status_value_chain
DROP TABLE IF EXISTS tmp_status_inverselogistics_@v_countryPrefix@;
CREATE TABLE tmp_status_inverselogistics_@v_countryPrefix@(INDEX (item_id))
SELECT t1.item_id,t2.status,t2.data,t2.user
FROM itens_venda_oot_sample_@v_countryPrefix@ AS t1
INNER JOIN
	(SELECT a.item_id,c.status_inverselogistic AS status,a.modified_at AS data,d.nome AS user
	FROM @v_wmsprod@.inverselogistics_devolucion AS a
	INNER JOIN 
		(SELECT return_id,changed_at,user_id
		FROM @v_wmsprod@.inverselogistics_status_history
		ORDER BY changed_at DESC) AS b
	ON a.id=b.return_id
	INNER JOIN @v_wmsprod@.inverselogistics_catalog_status AS c
	ON a.status=c.id_status
	INNER JOIN @v_wmsprod@.usuarios AS d
	ON b.user_id=d.usuarios_id
	GROUP BY a.item_id) AS t2
ON t1.item_id=t2.item_id;