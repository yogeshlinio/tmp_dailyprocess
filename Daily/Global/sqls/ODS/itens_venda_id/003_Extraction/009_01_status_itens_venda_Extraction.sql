DROP TABLE IF EXISTS TMP_status_dates_@v_countryPrefix@;
CREATE TEMPORARY TABLE TMP_status_dates_@v_countryPrefix@ (INDEX (itens_venda_id))
SELECT dates.itens_venda_id
     , MAX(datetime_backorder) AS datetime_backorder
     , MAX(is_backorder) AS is_backorder
	 , MAX(datetime_backorder_tratada) AS datetime_backorder_tratada
     , MAX(datetime_procured) AS datetime_procured
	 , MAX(datetime_assigned_to_stock) AS datetime_assigned_to_stock
     , MAX(datetime_ready_to_pick) AS datetime_ready_to_pick
	 , MAX(datetime_ready_to_ship) AS datetime_ready_to_ship
     , MAX(datetime_shipping_list) AS datetime_shipping_list
	 , MAX(datetime_shipped) AS datetime_shipped
     , MAX(datetime_declared_stockout) AS datetime_declared_stockout
	 , MAX(datetime_processed_stockout) AS datetime_processed_stockout
     , MAX(datetime_cancelled) AS datetime_cancelled
FROM 
(SELECT sv.itens_venda_id, sv.status,
CASE WHEN sv.status='backorder' THEN max(sv.data) ELSE NULL END AS datetime_backorder, 
CASE WHEN sv.status='backorder' THEN 1 ELSE 0 END AS is_backorder,
CASE WHEN sv.status='backorder_tratada' THEN max(sv.data) ELSE NULL END AS datetime_backorder_tratada,
CASE WHEN sv.status='estoque reservado' THEN min(sv.data) ELSE NULL END AS datetime_procured,
CASE WHEN sv.status='estoque reservado' THEN min(sv.data) ELSE NULL END AS datetime_assigned_to_stock,
CASE WHEN sv.status='Aguardando separacao' THEN max(sv.data) ELSE NULL END AS datetime_ready_to_pick,
CASE WHEN sv.status='faturado' THEN max(sv.data) ELSE NULL END datetime_ready_to_ship,
CASE WHEN sv.status='Aguardando expedicao' THEN max(sv.data) ELSE NULL END AS datetime_shipping_list,
CASE WHEN sv.status='expedido' THEN max(sv.data) ELSE NULL END AS datetime_shipped,
CASE WHEN sv.status='Quebrado' THEN max(sv.data) ELSE NULL END AS datetime_declared_stockout,
CASE WHEN sv.status='quebra tratada' THEN max(sv.data) ELSE NULL END AS datetime_processed_stockout,
CASE WHEN sv.status='Cancelado' THEN max(sv.data) ELSE NULL END AS datetime_cancelled
FROM @v_wmsprod@.status_itens_venda sv
JOIN ODS.itens_venda_oot_key_map_Sample_@v_countryPrefix@ km
ON sv.itens_venda_id=km.itens_venda_id
WHERE sv.status IN ('backorder','backorder_tratada','estoque reservado','Aguardando separacao','faturado','Aguardando expedicao','expedido',
				    'Quebrado','quebra tratada','Cancelado')
GROUP BY sv.itens_venda_id,sv.status) dates
GROUP BY dates.itens_venda_id;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN itens_venda_oot_key_map_Sample_@v_countryPrefix@ AS b
ON a.item_id=b.item_id
JOIN TMP_status_dates_@v_countryPrefix@ AS c
ON b.itens_venda_id=c.itens_venda_id
SET a.date_backorder = DATE(c.datetime_backorder),
a.datetime_backorder = c.datetime_backorder,
a.is_backorder = c.is_backorder,
a.date_backorder_tratada = DATE(c.datetime_backorder_tratada),
a.datetime_backorder_tratada = c.datetime_backorder_tratada,
a.date_procured = DATE(c.datetime_procured),
a.datetime_procured = c.datetime_procured,
a.date_assigned_to_stock = DATE(c.datetime_assigned_to_stock),
a.datetime_assigned_to_stock = c.datetime_assigned_to_stock,
a.date_ready_to_pick = DATE(c.datetime_ready_to_pick),
a.datetime_ready_to_pick = c.datetime_ready_to_pick,
a.date_ready_to_ship = DATE(c.datetime_ready_to_ship),
a.datetime_ready_to_ship = c.datetime_ready_to_ship,
a.date_shipping_list = DATE(c.datetime_shipping_list),
a.datetime_shipping_list = c.datetime_shipping_list,
a.date_shipped = DATE(c.datetime_shipped),
a.datetime_shipped = c.datetime_shipped,
a.date_declared_stockout = DATE(c.datetime_declared_stockout),
a.datetime_declared_stockout = c.datetime_declared_stockout,
a.date_processed_stockout = DATE(c.datetime_processed_stockout),
a.datetime_processed_stockout = c.datetime_processed_stockout,
a.date_cancelled = DATE(c.datetime_cancelled),
a.datetime_cancelled = c.datetime_cancelled;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
JOIN itens_venda_oot_key_map_Sample_@v_countryPrefix@ AS b
ON a.item_id=b.item_id
JOIN
	(SELECT usuarios_id, itens_venda_id, max(data) date
	 FROM @v_wmsprod@.status_itens_venda 
	 WHERE status in('Analisando quebra','Waiting dropshipping','dropshipping notified') 
	 GROUP BY itens_venda_id) AS c
ON b.itens_venda_id=c.itens_venda_id
INNER JOIN @v_wmsprod@.usuarios e
 ON c.usuarios_id = e.usuarios_id
AND c.date > a.date_backorder
SET a.date_backorder_accepted = c.date,
a.datetime_backorder_accepted = c.date,
a.user_cs_bo = e.nome;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
INNER JOIN
(SELECT b.item_id, c.status, a.data
FROM (SELECT itens_venda_id, MAX(data) data 
      FROM @v_wmsprod@.status_itens_venda 
	  #JOIN con el KEYMAP cuando este habilitado para periodos
	  GROUP BY itens_venda_id) a
JOIN @v_wmsprod@.status_itens_venda c
ON a.data=c.data AND a.itens_venda_id=c.itens_venda_id
JOIN @v_wmsprod@.itens_venda b
ON a.itens_venda_id=b.itens_venda_id) AS t1
ON a.item_id=t1.item_id
SET a.date_status_value_chain =t1.data,
a.status_value_chain=t1.status;

DROP TEMPORARY TABLE IF EXISTS tmp_stockout_seller_center_@v_countryPrefix@; 
CREATE TEMPORARY TABLE tmp_stockout_seller_center_@v_countryPrefix@ (INDEX (itens_venda_id))
SELECT
                a.itens_venda_id,
                a.numero_order,
                a.item_id,
                b.data
FROM @v_wmsprod@.itens_venda a
JOIN ( SELECT itens_venda_id, MAX(data) data
FROM @v_wmsprod@.status_itens_venda
WHERE detail LIKE '%stock out%'
AND status = 'cancelado'
GROUP BY itens_venda_id) b
ON  a.itens_venda_id = b.itens_venda_id;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ d
INNER JOIN tmp_stockout_seller_center_@v_countryPrefix@ t
                ON d.item_id = t.item_id
SET d.date_declared_stockout = t.data,
d.datetime_declared_stockout = t.data;

#1.Se usa para las columnas de status_value_chain
DROP TABLE IF EXISTS tmp_status_itens_venda_@v_countryPrefix@;
CREATE TABLE tmp_status_itens_venda_@v_countryPrefix@ (INDEX (item_id))
SELECT t2.item_id,t1.status,t1.data,t1.user FROM 
    ((SELECT t0.itens_venda_id,t0.nome AS user,t0.status,max(t0.data) AS data FROM 
     (SELECT itens_venda_id,nome,status,data
     FROM @v_wmsprod@.status_itens_venda AS a
     INNER JOIN @v_wmsprod@.usuarios AS b
     ON a.usuarios_id=b.usuarios_id ORDER BY data DESC) AS t0
    GROUP BY itens_venda_id) AS t1
   INNER JOIN @v_wmsprod@.itens_venda AS t2
   ON (t1.itens_venda_id=t2.itens_venda_id
   AND t1.status=t2.status)
   INNER JOIN itens_venda_oot_sample_@v_countryPrefix@ AS t3
   ON  t2.item_id=t3.item_id);
   
INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check
)
SELECT 
  '@v_country@', 
  'ODS.itens_venda_id',
  'status_itens_venda_extraction',
  NOW(),
  NOW(),
  0,
  0
;