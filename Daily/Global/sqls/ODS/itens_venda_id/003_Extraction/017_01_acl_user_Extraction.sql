DROP TEMPORARY TABLE IF EXISTS tmp_usuarios_bob_@v_countryPrefix@;
CREATE TEMPORARY TABLE tmp_usuarios_bob_@v_countryPrefix@(INDEX (item_id))
SELECT item_id,username FROM (SELECT d.fk_sales_order_item,d.created_at AS date_status_bob,name as status_item,username
			FROM @bob_live@.sales_order_item_status_history AS d
			INNER JOIN @bob_live@.sales_order_item_status AS e
			ON d.fk_sales_order_item_status=e.id_sales_order_item_status
			LEFT JOIN @bob_live@.acl_user AS f
			ON d.fk_acl_user=f.id_acl_user
			ORDER BY d.id_sales_order_item_status_history DESC) AS t2
			INNER JOIN itens_venda_oot_sample_@v_countryPrefix@ AS g
			ON g.item_id=t2.fk_sales_order_item;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
INNER JOIN tmp_usuarios_bob_@v_countryPrefix@ AS b
ON b.item_id = a.item_id
SET a.user_status_bob = b.username;