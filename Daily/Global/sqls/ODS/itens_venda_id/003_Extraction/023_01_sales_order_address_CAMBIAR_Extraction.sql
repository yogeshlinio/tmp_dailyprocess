UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
JOIN @bob_live@.sales_order 
ON a.order_number = sales_order.order_nr
JOIN @bob_live@.sales_order_address 
ON sales_order.fk_sales_order_address_shipping = sales_order_address.id_sales_order_address
SET 
	a.ship_to_zip = postcode,
	a.ship_to_zip2 = mid(postcode, 3, 2),
	a.ship_to_zip3 = RIGHT (postcode, 3),
	a.ship_to_zip4 = LEFT (postcode, 4)
	;