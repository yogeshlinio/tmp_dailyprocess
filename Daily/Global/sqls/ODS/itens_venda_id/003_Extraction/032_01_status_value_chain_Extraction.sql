#4 - union Se usa para las columnas de status_value_chain
DROP TABLE IF EXISTS union_status_@v_countryPrefix@;
CREATE TABLE union_status_@v_countryPrefix@(INDEX (item_id))
(SELECT * FROM tmp_status_itens_venda_@v_countryPrefix@)
UNION
(SELECT * FROM tmp_tms_status_delivery_@v_countryPrefix@)
UNION
(SELECT * FROM tmp_status_inverselogistics_@v_countryPrefix@);

#5. Update1 Se usa para las columnas de status_value_chain
UPDATE union_status_@v_countryPrefix@ AS a
INNER JOIN 
	(SELECT * FROM tmp_status_itens_venda_@v_countryPrefix@ WHERE status='Cancelado') AS b
ON a.item_id=b.item_id
SET a.item_id=a.item_id,
a.status=b.status,
a.data=b.data,
a.user=b.user;

#6. Update2 Se usa para las columnas de status_value_chain
UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
INNER JOIN
	(SELECT t1.* FROM (SELECT * FROM union_status_@v_countryPrefix@
	ORDER BY data DESC) AS t1 GROUP BY item_id) AS b
ON a.item_id = b.item_id
SET a.status_value_chain=b.status,
a.date_status_value_chain =b.data,
a.user_status_value_chain = b.user;