UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN operations_@v_countryPrefix@.status_match_bob_wms b
  ON a.status_wms = b.status_wms AND a.status_bob = b.status_bob
SET 
	a.status_match_bob_wms = b.correct;