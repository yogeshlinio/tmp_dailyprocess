#Debe quedar despues de ship_to_zip, ship_to_city, ship_to_region
DROP TABLE IF EXISTS tmp_ship_oot;
CREATE TEMPORARY TABLE tmp_ship_oot (INDEX(ship_to_zip)) SELECT * FROM itens_venda_oot_sample_@v_countryPrefix@;

DROP TABLE IF EXISTS tmp_ship_zone_mapping;
CREATE TEMPORARY TABLE tmp_ship_zone_mapping (INDEX(area_code)) SELECT * FROM bob_live_@v_countryPrefix@.shipment_zone_mapping;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
INNER JOIN tmp_ship_oot AS b
 ON a.item_id=b.item_id
INNER JOIN tmp_ship_zone_mapping AS c
 ON b.ship_to_zip=c.area_code
SET 
a.ship_to_city=area_2,
a.ship_to_region=area_1;