UPDATE itens_venda_oot_sample_@v_countryPrefix@ ot
INNER JOIN @bob_live@.sales_order_item soi ON ot.item_id = soi.id_sales_order_item
INNER JOIN @bob_live@.shipment_carrier sc ON soi.fk_shipment_carrier = sc.id_shipment_carrier
SET ot.shipping_carrier_srt = sc.NAME;