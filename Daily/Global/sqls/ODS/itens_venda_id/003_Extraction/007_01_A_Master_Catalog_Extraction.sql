/*Ya lo toma de SKU_SIMPLE en external references

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
JOIN itens_venda_oot_key_map_Sample_@v_countryPrefix@ c
ON a.item_id=c.item_id
INNER JOIN development_@v_countryPrefix@.A_Master_Catalog AS b 
 ON c.sku = b.sku_simple
SET
 a.cat_1 = b.Cat1,
 a.cat_2 = b.Cat2,
 a.cat_3 = b.Cat3,
 a.category_bp = Cat_BP,
 a.category_kpi = Cat_KPI,
 a.head_buyer = b.Head_Buyer,
 a.buyer = b.Buyer,
 a.sku_config = b.sku_config,
 a.sku_name = b.sku_name,
 a.supplier_id = b.id_supplier,
 a.supplier_name = b.Supplier,
 a.supplier_type = b.SupplierType,
 a.package_height = b.package_height,
 a.package_length = b.package_length,
 a.package_width = b.package_width,
 a.package_weight = b.package_weight;
 
 */