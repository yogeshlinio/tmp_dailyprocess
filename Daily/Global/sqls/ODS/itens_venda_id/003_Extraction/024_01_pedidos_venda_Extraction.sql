UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
JOIN @v_wmsprod@.pedidos_venda b
ON a.order_number = b.numero_pedido
SET 
a.payment_method = b.metodo_de_pagamento,
a.ship_to_region = b.estado_cliente,
a.ship_to_city = b.cidade_cliente;