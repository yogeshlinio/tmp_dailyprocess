UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN nps.@NPSData@ b
ON a.item_id = b.ItemID
SET
a.loyalty = b.Loyalty,
a.has_nps_score = '1';