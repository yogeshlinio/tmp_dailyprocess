-- Anteriormente esto era la rutina bi_ops_tracking_code
DROP TEMPORARY TABLE IF EXISTS tmp_total_guias_@v_countryPrefix@;

CREATE TEMPORARY TABLE tmp_total_guias_@v_countryPrefix@(INDEX (cod_rastreamento))
SELECT cod_rastreamento, GROUP_CONCAT(DISTINCT track SEPARATOR '-') total_guias 
FROM @v_wmsprod@.tms_tracks
GROUP BY cod_rastreamento;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS b
JOIN tmp_total_guias_@v_countryPrefix@ a
ON b.wms_tracking_code=a.cod_rastreamento
SET b.shipping_carrier_tracking_code=a.total_guias;

#### Insert para A_Master
INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  step,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Colombia_project',
  'out_order_tracking',
  'shipping_carrier_tracking_code',
  NOW(),
  max(datetime_ordered),
  count(*),
  count(item_counter)
FROM
  operations_co.out_order_tracking;