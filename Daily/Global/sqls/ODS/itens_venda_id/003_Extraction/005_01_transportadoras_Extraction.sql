UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
JOIN itens_venda_oot_key_map_Sample_@v_countryPrefix@ b
ON a.item_id=b.item_id
INNER JOIN @v_wmsprod@.romaneio AS c
	ON b.romaneio_id = c.romaneio_id
INNER JOIN @v_wmsprod@.transportadoras AS d
	ON c.transportadora_id = d.transportadoras_id
SET 
 a.shipping_carrier = d.nome_transportadora,
 a.id_shipping_carrier = d.transportadoras_id;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
JOIN @v_wmsprod@.entrega b
ON a.wms_tracking_code = b.cod_rastreamento
JOIN @v_wmsprod@.pedidos_romaneio c
ON b.entrega_id = c.entrega_id
JOIN @v_wmsprod@.romaneio AS d
ON c.romaneio_id = d.romaneio_id
JOIN @v_wmsprod@.transportadoras AS e
ON d.transportadora_id = e.transportadoras_id
SET
	a.shipping_carrier = e.nome_transportadora,
	a.id_shipping_carrier = e.transportadoras_id
WHERE a.shipping_carrier IS NULL;