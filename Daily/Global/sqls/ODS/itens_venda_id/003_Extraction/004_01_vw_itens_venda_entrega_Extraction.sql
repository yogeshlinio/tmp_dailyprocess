UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
INNER JOIN @v_wmsprod@.vw_itens_venda_entrega AS b
ON a.item_id = b.item_id
SET a.wms_tracking_code = b.cod_rastreamento;