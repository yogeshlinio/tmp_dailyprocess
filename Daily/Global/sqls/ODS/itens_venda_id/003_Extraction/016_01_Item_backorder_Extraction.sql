UPDATE itens_venda_oot_sample_@v_countryPrefix@ d
INNER JOIN (
	SELECT
		a.item_id,
		a.tempo_de_entrega,
		a.tempo_entrega_fornecedor,
		a.data_criacao,
		a.estimated_delivery_date,
		a.estimated_delivery_days,
		a.estimated_days_on_wh,
		b.days_back,
		b.date_created
	FROM
		@v_wmsprod@.itens_venda a
	JOIN @v_wmsprod@.item_backorder b 
	ON a.itens_venda_id = b.itens_venda_id
) c ON d.item_id = c.item_id
SET d.date_declared_backorder = c.date_created,
d.datetime_declared_backorder = c.date_created;