INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'ODS.itens_venda_id',
  "finish",
  NOW(),
  NOW(),
  count(1),
  count(1)
FROM
   ODS.itens_venda_oot_key_map_Sample_@v_countryPrefix@
WHERE 
   Country = '@v_countryPrefix@'   
;