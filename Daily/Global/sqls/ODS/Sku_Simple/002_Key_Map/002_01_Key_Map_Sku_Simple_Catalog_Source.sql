UPDATE            Sku_Simple_Key_Map_Sample_@v_countryPrefix@ AS Sku_Simple_Key_Map_Sample
       INNER JOIN @bob_live@.catalog_source
	           ON Sku_Simple_Key_Map_Sample.id_catalog_simple = catalog_source.fk_catalog_simple
SET
   Sku_Simple_Key_Map_Sample.id_catalog_source        = catalog_source.id_catalog_source,
   Sku_Simple_Key_Map_Sample.id_catalog_shipment_type = catalog_source.fk_catalog_shipment_type,
   Sku_Simple_Key_Map_Sample.id_supplier              = catalog_source.fk_supplier
;