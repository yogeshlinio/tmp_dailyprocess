CREATE TABLE IF NOT EXISTS Sku_Simple_Key_Map_@v_countryPrefix@ LIKE Sku_Simple_Key_Map_Template_@v_countryPrefix@;
REPLACE Sku_Simple_Key_Map_@v_countryPrefix@
SELECT * FROM Sku_Simple_Key_Map_Sample_@v_countryPrefix@; 

CREATE TABLE IF NOT EXISTS Sku_Simple_Key_Map LIKE Sku_Simple_Key_Map_Template_@v_countryPrefix@;
REPLACE Sku_Simple_Key_Map
SELECT * FROM Sku_Simple_Key_Map_Sample_@v_countryPrefix@; 

DROP TABLE IF EXISTS Sku_Simple_Key_Map_Backup;
CREATE TABLE Sku_Simple_Key_Map_Backup LIKE Sku_Simple_Key_Map;
INSERT Sku_Simple_Key_Map_Backup
SELECT * FROM Sku_Simple_Key_Map;