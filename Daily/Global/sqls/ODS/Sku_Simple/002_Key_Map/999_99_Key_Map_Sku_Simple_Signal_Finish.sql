INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'ODS.Sku_simple_Key_Map',
  "finish",
  NOW(),
  NOW(),
  count(*),
  count(*)
FROM
   Sku_Simple_Key_Map
WHERE 
   Country = '@v_countryPrefix@'   
;


