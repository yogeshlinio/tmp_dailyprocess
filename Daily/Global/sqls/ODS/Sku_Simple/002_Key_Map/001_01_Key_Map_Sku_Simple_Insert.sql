DROP TABLE IF EXISTS Sku_Simple_Key_Map_Sample_@v_countryPrefix@;
CREATE TABLE Sku_Simple_Key_Map_Sample_@v_countryPrefix@ LIKE Sku_Simple_Key_Map_Template_@v_countryPrefix@;
/*INSERT ALL THE PRIMARY KEYS*/
INSERT Sku_Simple_Key_Map_Sample_@v_countryPrefix@
(
  `Country`           ,
  `sku_simple`        ,
  `id_catalog_simple` ,
  `id_catalog_config` ,
  `id_catalog_tax_class`
)
SELECT 
   '@v_countryPrefix@' AS Country,
   Sku,
   `id_catalog_simple` ,
   `fk_catalog_config` ,
   `fk_catalog_tax_class`
FROM 
   @bob_live@.catalog_simple;
   
   
   