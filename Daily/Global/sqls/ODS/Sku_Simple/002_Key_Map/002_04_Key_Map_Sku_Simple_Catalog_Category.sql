UPDATE            Sku_Simple_Key_Map_Sample_@v_countryPrefix@    AS Sku_Simple_Key_Map_Sample
       INNER JOIN @bob_live@.catalog_config_has_catalog_category  
	           ON     Sku_Simple_Key_Map_Sample.id_catalog_category = catalog_config_has_catalog_category.fk_catalog_category 
			      AND Sku_Simple_Key_Map_Sample.id_catalog_config   = catalog_config_has_catalog_category.fk_catalog_config
SET
  Sku_Simple_Key_Map_Sample.id_catalog_config_has_catalog_category  = catalog_config_has_catalog_category.id_catalog_config_has_catalog_category
;