DROP TEMPORARY TABLE IF EXISTS Data_@v_countryPrefix@;
CREATE TEMPORARY TABLE Data_@v_countryPrefix@ ( PRIMARY KEY ( Sku_Simple ) )
SELECT
   Key_Map.sku_simple,
   Cat.label
FROM   
               Sku_Simple_Key_Map_Sample_@v_countryPrefix@ AS Key_Map
    INNER JOIN @bob_live@.catalog_attribute_set            AS Cat
         USING ( id_catalog_attribute_set )
;

UPDATE              Sku_Simple_Sample_@v_countryPrefix@   AS Sku_Simple
         INNER JOIN Data_@v_countryPrefix@                AS Data
  	          USING ( sku_simple )
SET
    Sku_Simple.attribute = Data.label
;
