DROP TEMPORARY TABLE IF EXISTS Data_@v_countryPrefix@;
CREATE TEMPORARY TABLE Data_@v_countryPrefix@ ( PRIMARY KEY ( Sku_Simple ) )
SELECT
   Key_Map.sku_simple,
   Cat.sku ,
   Cat.name  ,

   Cat.product_weight   ,
   Cat.package_weight   ,
   Cat.package_height   ,
   Cat.package_length   ,
   Cat.package_width    ,
   Cat.product_measures ,
   
      
   Cat.model ,    
   Cat.color ,   
   
   Cat.price_comparison       ,
   
   Cat.display_if_out_of_stock ,
   Cat.pet_status              ,
   Cat.pet_approved            ,
   Cat.status                  ,

   Cat.created_at 
FROM   
               Sku_Simple_Key_Map_Sample_@v_countryPrefix@ AS Key_Map
       INNER JOIN @bob_live@.catalog_config                AS Cat
	        USING ( id_catalog_config )
;
			   
UPDATE              Sku_Simple_Sample_@v_countryPrefix@   AS Sku_Simple_Sample
         INNER JOIN Data_@v_countryPrefix@                AS catalog_config
  	          USING ( sku_simple )
SET
   Sku_Simple_Sample.sku_config = catalog_config.sku ,
   Sku_Simple_Sample.sku_name   = catalog_config.name  ,

   Sku_Simple_Sample.Product_Weight    = catalog_config.product_weight   ,
   Sku_Simple_Sample.Package_Weight    = catalog_config.package_weight   ,
   Sku_Simple_Sample.Package_Height    = catalog_config.package_height   ,
   Sku_Simple_Sample.Package_Length    = catalog_config.package_length   ,
   Sku_Simple_Sample.Package_Width     = catalog_config.package_width    ,
   Sku_Simple_Sample.Product_Measures  = catalog_config.product_measures ,
   Sku_Simple_Sample.Volumetric_Weight = (catalog_config.package_width*catalog_config.package_length*catalog_config.package_height)/5000 ,
      
   Sku_Simple_Sample.model = catalog_config.model ,    
   Sku_Simple_Sample.color = catalog_config.color ,   
   
   Sku_Simple_Sample.price_comparison        = catalog_config.price_comparison       ,
   Sku_Simple_Sample.isActive_SKUConfig      = if( catalog_config.status = "active" , 1 , 0 ) ,
   Sku_Simple_Sample.display_if_out_of_stock = catalog_config.display_if_out_of_stock ,
   Sku_Simple_Sample.pet_status              = catalog_config.pet_status              ,
   Sku_Simple_Sample.pet_approved            = catalog_config.pet_approved            ,
   Sku_Simple_Sample.status_config           = catalog_config.status                  ,

   Sku_Simple_Sample.created_at_config = catalog_config.created_at 
;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'ODS.Sku_simple',
  "catalog_config",
  NOW(),
  NOW(),
  0,
  0
;


