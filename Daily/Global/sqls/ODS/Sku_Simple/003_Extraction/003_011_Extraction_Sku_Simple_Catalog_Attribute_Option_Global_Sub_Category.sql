DROP TEMPORARY TABLE IF EXISTS Sku_Simple_Cat2_@v_countryPrefix@;
CREATE TEMPORARY TABLE Sku_Simple_Cat2_@v_countryPrefix@ ( PRIMARY KEY ( Sku_Simple ) )
SELECT
   Key_Map.sku_simple,
   Cat2.name
FROM   
               Sku_Simple_Key_Map_Sample_@v_countryPrefix@ AS Key_Map
    INNER JOIN @bob_live@.catalog_attribute_option_global_sub_category    AS Cat2
         USING ( id_catalog_attribute_option_global_sub_category )
;

UPDATE              Sku_Simple_Sample_@v_countryPrefix@   AS Sku_Simple
         INNER JOIN Sku_Simple_Cat2_@v_countryPrefix@     AS Cat
  	          USING ( sku_simple )
SET
    Sku_Simple.Cat2 = Cat.name;