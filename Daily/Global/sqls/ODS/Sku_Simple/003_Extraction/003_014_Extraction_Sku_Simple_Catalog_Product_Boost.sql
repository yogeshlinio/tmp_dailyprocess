DROP TEMPORARY TABLE IF EXISTS Sku_Simple_Boost_@v_countryPrefix@;
CREATE TEMPORARY TABLE Sku_Simple_Boost_@v_countryPrefix@ ( PRIMARY KEY ( Sku_Simple ) )
SELECT
   Key_Map.sku_simple,
   catalog_product_boost.boost
FROM   
               Sku_Simple_Key_Map_Sample_@v_countryPrefix@ AS Key_Map
    INNER JOIN @bob_live@.catalog_product_boost
            ON id_catalog_config = fk_catalog_config
;

UPDATE              Sku_Simple_Sample_@v_countryPrefix@    AS Sku_Simple
         INNER JOIN Sku_Simple_Boost_@v_countryPrefix@     AS catalog_product_boost
  	          USING ( sku_simple )
SET
    Sku_Simple.boost = catalog_product_boost.boost;