DROP TABLE IF EXISTS Sku_Simple_Sample_@v_countryPrefix@;
CREATE TABLE Sku_Simple_Sample_@v_countryPrefix@ LIKE Sku_Simple_Template_@v_countryPrefix@;
/*INSERT ALL THE PRIMARY KEYS*/
INSERT Sku_Simple_Sample_@v_countryPrefix@
(
  `Country`           ,
  `sku_simple`        
)
SELECT 
   '@v_countryPrefix@' AS Country,
   sku_simple
FROM 
   Sku_Simple_Key_Map
WHERE 
   Country = '@v_countryPrefix@'    
;
   
   
   