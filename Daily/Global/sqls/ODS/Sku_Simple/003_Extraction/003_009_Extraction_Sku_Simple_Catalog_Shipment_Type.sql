DROP TEMPORARY TABLE IF EXISTS Sku_Simple_FT_@v_countryPrefix@;
CREATE TEMPORARY TABLE Sku_Simple_FT_@v_countryPrefix@ ( PRIMARY KEY ( Sku_Simple ) )
SELECT
   Key_Map.sku_simple,
   FT.name
FROM   
               Sku_Simple_Key_Map_Sample_@v_countryPrefix@ AS Key_Map
    INNER JOIN @bob_live@.catalog_shipment_type    AS FT
         USING ( id_catalog_shipment_type )
;

UPDATE              Sku_Simple_Sample_@v_countryPrefix@   AS Sku_Simple_Sample
         INNER JOIN Sku_Simple_FT_@v_countryPrefix@     AS Cat
  	          USING ( sku_simple )
SET
    Sku_Simple_Sample.fulfillment_type = Cat.name  
;



/* Leido por external references de otros procesos */
INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check
)
SELECT 
  '@v_country@', 
  'ODS.Sku_Simple',
  'Fulfillment_Type',
  NOW(),
  NOW(),
  0,
  0
;