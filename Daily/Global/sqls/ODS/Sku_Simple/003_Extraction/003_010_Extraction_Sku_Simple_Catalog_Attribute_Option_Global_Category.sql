DROP TEMPORARY TABLE IF EXISTS Sku_Simple_Cat1_@v_countryPrefix@;
CREATE TEMPORARY TABLE Sku_Simple_Cat1_@v_countryPrefix@ ( PRIMARY KEY ( Sku_Simple ) )
SELECT
   Key_Map.sku_simple,
   Cat1.name
FROM   
               Sku_Simple_Key_Map_Sample_@v_countryPrefix@ AS Key_Map
    INNER JOIN @bob_live@.catalog_attribute_option_global_category   AS Cat1
         USING ( id_catalog_attribute_option_global_category )
;

UPDATE              Sku_Simple_Sample_@v_countryPrefix@   AS Sku_Simple
         INNER JOIN Sku_Simple_Cat1_@v_countryPrefix@     AS Cat
  	          USING ( sku_simple )
SET
    Sku_Simple.Cat1 = Cat.name;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'ODS.Sku_simple',
  "Cat1",
  NOW(),
  NOW(),
  0,
  0
;
      