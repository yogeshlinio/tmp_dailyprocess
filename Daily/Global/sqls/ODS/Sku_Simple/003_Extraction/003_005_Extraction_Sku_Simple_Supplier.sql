DROP TEMPORARY TABLE IF EXISTS Data_@v_countryPrefix@;
CREATE TEMPORARY TABLE Data_@v_countryPrefix@ ( PRIMARY KEY ( Sku_Simple ) )
SELECT
   Key_Map.sku_simple,
   Cat.name_en
FROM   
                Sku_Simple_Key_Map_Sample_@v_countryPrefix@ AS Key_Map
       INNER JOIN @bob_live@.catalog_supplier               AS Cat
               ON Cat.id_catalog_supplier = Key_Map.id_supplier
;
			   
UPDATE              Sku_Simple_Sample_@v_countryPrefix@   AS Sku_Simple_Sample
         INNER JOIN Data_@v_countryPrefix@                AS Data
  	          USING ( sku_simple )
SET
    Sku_Simple_Sample.Supplier = Data.name_en
;

DROP TEMPORARY TABLE IF EXISTS Data_@v_countryPrefix@;
CREATE TEMPORARY TABLE Data_@v_countryPrefix@ ( PRIMARY KEY ( Sku_Simple ) )
SELECT
   Key_Map.sku_simple,
   supplier.name,
   supplier.type_delivery,
   supplier.type,
   supplier.products_origin
FROM   
                  Sku_Simple_Key_Map_Sample_@v_countryPrefix@ AS Key_Map
       INNER JOIN @bob_live@.supplier                         AS supplier
            USING ( id_supplier )
;
			   
UPDATE              Sku_Simple_Sample_@v_countryPrefix@   AS Sku_Simple_Sample
         INNER JOIN Data_@v_countryPrefix@                AS supplier
  	          USING ( sku_simple )
SET
    Sku_Simple_Sample.Supplier        = supplier.name,
    Sku_Simple_Sample.Delivery_Type   = supplier.type_delivery,
	Sku_Simple_Sample.supplier_type   = supplier.type,
	Sku_Simple_Sample.Products_origin = supplier.products_origin
;