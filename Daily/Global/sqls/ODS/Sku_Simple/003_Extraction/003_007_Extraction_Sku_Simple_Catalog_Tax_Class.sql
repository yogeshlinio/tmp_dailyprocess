DROP TEMPORARY TABLE IF EXISTS Data_@v_countryPrefix@;
CREATE TEMPORARY TABLE Data_@v_countryPrefix@ ( PRIMARY KEY ( Sku_Simple ) )
SELECT
   Key_Map.sku_simple,
   Cat.tax_percent,
   Cat.name   
FROM   
                Sku_Simple_Key_Map_Sample_@v_countryPrefix@ AS Key_Map
     INNER JOIN @bob_live@.`catalog_tax_class` AS Cat
          USING ( id_catalog_tax_class )
;

UPDATE              Sku_Simple_Sample_@v_countryPrefix@   AS Sku_Simple_Sample
         INNER JOIN Data_@v_countryPrefix@                AS Data
  	          USING ( sku_simple )
SET
   Sku_Simple_Sample.`tax_percent` = Data.tax_percent,
   Sku_Simple_Sample.`tax_class`   = Data.name
;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'ODS.Sku_simple',
  "tax_class",
  NOW(),
  NOW(),
  0,
  0
;


