DROP TEMPORARY TABLE IF EXISTS Sku_Simple_Stock_@v_countryPrefix@;
CREATE TEMPORARY TABLE Sku_Simple_Stock_@v_countryPrefix@ ( PRIMARY KEY ( Sku_Simple ) )
SELECT
   Key_Map.sku_simple,
   quantity
FROM   
               Sku_Simple_Key_Map_Sample_@v_countryPrefix@ AS Key_Map
    INNER JOIN @bob_live@.catalog_warehouse_stock  
            ON id_catalog_simple = fk_catalog_simple
;

UPDATE              Sku_Simple_Sample_@v_countryPrefix@    AS Sku_Simple_Sample
         INNER JOIN Sku_Simple_Stock_@v_countryPrefix@     AS catalog_warehouse_stock
  	          USING ( sku_simple )
SET
   Sku_Simple_Sample.warehouse_stock = catalog_warehouse_stock.quantity;