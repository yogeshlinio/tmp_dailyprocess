DROP TEMPORARY TABLE IF EXISTS Data_@v_countryPrefix@;
CREATE TEMPORARY TABLE Data_@v_countryPrefix@ ( PRIMARY KEY ( Sku_Simple ) )
SELECT
   Key_Map.sku_simple,
   /*Defaults and Simple Extraction*/
   catalog_warehouse_stock.quantity
  
FROM   
               Sku_Simple_Key_Map_Sample_@v_countryPrefix@ AS Key_Map
         INNER JOIN @bob_live@.catalog_warehouse_stock  
                 ON id_catalog_simple = fk_catalog_simple
;
			   
UPDATE              Sku_Simple_Sample_@v_countryPrefix@   AS Sku_Simple_Sample
         INNER JOIN Data_@v_countryPrefix@                AS catalog_warehouse_stock
  	          USING ( sku_simple )
SET
   /*Defaults and Simple Extraction*/
   Sku_Simple_Sample.warehouse_stock = catalog_warehouse_stock.quantity
 ;
 
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'ODS.Sku_simple',
  "warehouse_stock",
  NOW(),
  NOW(),
  0,
  0
;