DROP TEMPORARY TABLE IF EXISTS Data_@v_countryPrefix@;
CREATE TEMPORARY TABLE Data_@v_countryPrefix@ ( PRIMARY KEY ( Sku_Simple ) )
SELECT
   Key_Map.sku_simple,
   Cat.barcode_ean,
   Cat.fk_supplier   
FROM   
               Sku_Simple_Key_Map_Sample_@v_countryPrefix@ AS Key_Map
    INNER JOIN @bob_live@.catalog_source AS Cat
	     USING ( id_catalog_source )
;
			   
UPDATE              Sku_Simple_Sample_@v_countryPrefix@   AS Sku_Simple_Sample
         INNER JOIN Data_@v_countryPrefix@                AS Data
  	          USING ( sku_simple )
SET
   Sku_Simple_Sample.barcode_ean = Data.barcode_ean,
   Sku_Simple_Sample.id_supplier = Data.fk_supplier   
;
