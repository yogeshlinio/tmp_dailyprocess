UPDATE Sku_Simple_Sample_@v_countryPrefix@ AS Sku_Simple_Sample
SET 
   isVisible = 1
WHERE
NOT(
			   isnull( Stock_BOB ) OR  Stock_BOB <= 0
            OR `pet_approved` = 0 
			OR `pet_status` not in ( 'creation,edited,images' , "creation, edited" )									 
			OR `status_config` IN ( 'deleted','inactive')
			OR `status_simple` IN ( 'deleted','inactive')
      OR `display_if_out_of_stock` = 1
    )
;
