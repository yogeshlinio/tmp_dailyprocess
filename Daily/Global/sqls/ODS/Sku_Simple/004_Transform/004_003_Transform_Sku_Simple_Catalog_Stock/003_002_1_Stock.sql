/*
*  Fields regarding tbl_catalog_product_stock
*/
UPDATE            Sku_Simple_Sample_@v_countryPrefix@          AS Sku_Simple_Sample
       INNER JOIN tbl_catalog_product_stock_@v_countryPrefix@ AS tbl_catalog_product_stock
            USING ( sku_simple )               
SET
     Sku_Simple_Sample.stock_bob       = tbl_catalog_product_stock.availablebob,
	 Sku_Simple_Sample.Reserved_BOB    = tbl_catalog_product_stock.reservedbob,
	 Sku_Simple_Sample.warehouse_stock = tbl_catalog_product_stock.`ownstock`,
	 Sku_Simple_Sample.supplier_stock  = tbl_catalog_product_stock.`supplierstock`
;