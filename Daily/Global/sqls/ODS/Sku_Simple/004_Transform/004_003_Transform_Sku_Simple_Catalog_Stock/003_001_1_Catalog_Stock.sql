TRUNCATE tbl_catalog_product_stock_@v_countryPrefix@ ;
INSERT INTO tbl_catalog_product_stock_@v_countryPrefix@ (
	fk_catalog_simple,
	sku_simple,
	reservedbob,
	stockbob,
	availablebob
) SELECT
	   id_catalog_simple,
	   sku_simple,
     IF (sum(is_reserved) IS NULL,0,sum(is_reserved)) AS reservedBOB,
     ifnull(catalog_stock.quantity, 0) AS stockbob,
     ifnull(catalog_stock.quantity, 0) - IF (sum(sales_order_item.is_reserved) IS NULL,0, sum(is_reserved)) AS availablebob
FROM
	(
	               Sku_Simple_Key_Map
 	    LEFT  JOIN @bob_live@.catalog_stock 
	            ON     id_catalog_source = catalog_stock.fk_catalog_source
	)
    LEFT JOIN @bob_live@.sales_order_item ON sales_order_item.sku = Sku_Simple_Key_Map.sku_simple
WHERE
    country = '@v_countryPrefix@' 	
GROUP BY sku_simple
ORDER BY
	catalog_stock.quantity DESC;


UPDATE       tbl_catalog_product_stock_@v_countryPrefix@ AS tbl_catalog_product_stock
  INNER JOIN @bob_live@.catalog_warehouse_stock 
          ON tbl_catalog_product_stock.fk_catalog_simple = catalog_warehouse_stock.fk_catalog_simple
SET
tbl_catalog_product_stock.ownstock= catalog_warehouse_stock.quantity;

UPDATE        tbl_catalog_product_stock_@v_countryPrefix@ AS tbl_catalog_product_stock
   INNER JOIN @bob_live@.catalog_supplier_stock 
           ON tbl_catalog_product_stock.fk_catalog_simple = catalog_supplier_stock.fk_catalog_simple
SET 
tbl_catalog_product_stock.supplierstock= catalog_supplier_stock.quantity;

UPDATE tbl_catalog_product_stock_@v_countryPrefix@ set availablebob = 0 where availablebob < 0;

/*
UPDATE operations_co.out_catalog_product_stock a
         INNER JOIN
    operations_co.vw_stock_wms b ON a.sku = b.sku_simple 
SET 
    a.stock_wms = b.stock_wms
WHERE
    b.stock_wms > 0;



UPDATE operations_co.out_catalog_product_stock a
         INNER JOIN
    operations_co.vw_stock_wms_reserved b ON a.sku = b.sku_simple 
SET 
    a.stock_wms_reserved = b.stock_wms_reserved;
*/	
