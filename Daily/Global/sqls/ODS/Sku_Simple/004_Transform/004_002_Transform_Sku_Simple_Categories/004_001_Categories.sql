 UPDATE           Sku_Simple_Sample_@v_countryPrefix@ AS Sku_Simple_Sample
      INNER JOIN GlobalConfig.A_E_M1_New_CategoryBP 
              ON Sku_Simple_Sample.Cat1 = A_E_M1_New_CategoryBP.Cat1 AND Sku_Simple_Sample.Cat2 = A_E_M1_New_CategoryBP.Cat2
SET 
    Sku_Simple_Sample.Cat_BP = A_E_M1_New_CategoryBP.CatBP;

UPDATE           Sku_Simple_Sample_@v_countryPrefix@ AS Sku_Simple_Sample
       INNER JOIN GlobalConfig.A_E_M1_New_CategoryBP 
               ON Sku_Simple_Sample.Cat1 = A_E_M1_New_CategoryBP.Cat1
SET 
    Sku_Simple_Sample.Cat_BP = A_E_M1_New_CategoryBP.CatBP
WHERE 	
    Sku_Simple_Sample.Cat_BP = '9,1 Other'
;

/*   
*   UPDATE Sku_Simple.Cap_KPI
*/
UPDATE              Sku_Simple_Sample_@v_countryPrefix@ AS Sku_Simple_Sample
         INNER JOIN GlobalConfig.M_CategoryKPI 
                 ON Sku_Simple_Sample.Cat_BP =  M_CategoryKPI.CatBP
SET
   Sku_Simple_Sample.Cat_KPI = M_CategoryKPI.CatKPI;
   
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'ODS.Sku_simple',
  "CatBP",
  NOW(),
  NOW(),
  0,
  0
;
   