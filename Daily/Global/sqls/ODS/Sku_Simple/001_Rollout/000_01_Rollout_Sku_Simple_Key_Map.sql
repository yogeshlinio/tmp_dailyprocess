DROP TABLE IF EXISTS Sku_Simple_Key_Map_Template_@v_countryPrefix@;
CREATE TABLE `Sku_Simple_Key_Map_Template_@v_countryPrefix@` (
  `Country`           varchar(150) NOT NULL DEFAULT '',
  `sku_simple` varchar(255) CHARACTER SET utf8 NOT NULL,
  `id_catalog_simple` int(10) unsigned NOT NULL DEFAULT '0',
  `id_catalog_config` int(10) unsigned DEFAULT '0',
  `id_catalog_source` int(10) unsigned DEFAULT '0',  

  `id_catalog_category` int(10) unsigned DEFAULT NULL,
  `id_catalog_config_has_catalog_category` int(10) unsigned DEFAULT NULL,
  
  `id_catalog_attribute_option_global_category` int(10) unsigned DEFAULT NULL,
  `id_catalog_attribute_option_global_sub_category` int(10) unsigned DEFAULT NULL,
  `id_catalog_attribute_option_global_sub_sub_category` int(10) unsigned DEFAULT NULL,
  `id_catalog_attribute_option_global_head_buyer` int(10) unsigned DEFAULT NULL,
  `id_catalog_attribute_option_global_buyer` int(10) unsigned DEFAULT NULL,
  `id_catalog_attribute_option_global_repurchase` int(10) unsigned DEFAULT NULL,
  
  
  `id_catalog_brand` int(10) unsigned DEFAULT NULL,
  `id_catalog_shipment_type` int(255) DEFAULT '3',
  `id_catalog_tax_class` INT NOT NULL,
  `id_supplier` INT NOT NULL,
  `id_catalog_attribute_set` INT NOT NULL,
  
  
  PRIMARY KEY ( `Country` , sku_simple ),
  KEY `sku_simple` (`sku_simple`),
  KEY Country_id_catalog_simple ( `Country` , id_catalog_simple ) ,
  KEY `id_catalog_simple` (`id_catalog_simple`),
  
  KEY Country_id_catalog_source ( `Country` , id_catalog_source ) ,
  KEY `id_catalog_source` (`id_catalog_source`),

  KEY Country_id_catalog_category ( `Country` , id_catalog_category ) ,
  KEY `id_catalog_category` (`id_catalog_category`),
  
  KEY Country_id_catalog_config_has_catalog_category ( `Country` , id_catalog_config_has_catalog_category ) ,
  KEY `id_catalog_config_has_catalog_category` (`id_catalog_config_has_catalog_category`),


  
  KEY `id_catalog_attribute_option_global_category` (`id_catalog_attribute_option_global_category`),
  KEY `id_catalog_attribute_option_global_sub_category` (`id_catalog_attribute_option_global_sub_category`),
  KEY `id_catalog_attribute_option_global_sub_sub_category` (`id_catalog_attribute_option_global_sub_sub_category`),
  KEY `id_catalog_attribute_option_global_head_buyer` (`id_catalog_attribute_option_global_head_buyer`),
  KEY `id_catalog_attribute_option_global_buyer` (`id_catalog_attribute_option_global_buyer`),
  
  KEY `id_catalog_brand` (`id_catalog_brand`),
  KEY `id_catalog_shipment_type` (`id_catalog_shipment_type`),
  KEY `id_catalog_tax_class` (`id_catalog_tax_class`),
  KEY `id_supplier` ( `id_supplier` ),
  KEY `id_catalog_attribute_set` ( `id_catalog_attribute_set` ),
  KEY `id_catalog_attribute_option_global_repurchase` ( `id_catalog_attribute_option_global_repurchase` )
) ENGINE=InnoDB;


