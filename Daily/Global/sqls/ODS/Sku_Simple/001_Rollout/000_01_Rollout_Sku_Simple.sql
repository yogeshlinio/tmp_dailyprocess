DROP TABLE IF EXISTS Sku_Simple_Template_@v_countryPrefix@;
CREATE TABLE `Sku_Simple_Template_@v_countryPrefix@` (
  `Country`           varchar(150) NOT NULL DEFAULT '',
  
  `sku_simple` varchar(255) CHARACTER SET utf8 NOT NULL,
  `sku_config` varchar(255) CHARACTER SET utf8,
  `sku_name` varchar(255) CHARACTER SET utf8,

  `Cat1` varchar(150) NOT NULL DEFAULT '',
  `Cat2` varchar(150) NOT NULL DEFAULT '',
  `Cat3` varchar(150) NOT NULL DEFAULT '',
  `Cat_KPI` varchar(150) NOT NULL DEFAULT '',
  `Cat_BP` varchar(150) NOT NULL DEFAULT '9,1 Other',
  
  `Product_Weight` decimal(11,4) NOT NULL DEFAULT '0.0000',
  `Package_Weight` decimal(11,4) NOT NULL DEFAULT '0.0000',
  `Package_Height` decimal(6,2) DEFAULT NULL COMMENT 'Package height (cm)',
  `Package_Length` decimal(6,2) DEFAULT NULL COMMENT 'Package depth (cm)',
  `Package_Width` decimal(6,2) DEFAULT NULL COMMENT 'Package width (cm)',
  
  `Volumetric_Weight` decimal(6,2) DEFAULT NULL COMMENT 'Package width (cm)',  
  `Max_VolW_Weight` decimal(6,2) DEFAULT NULL COMMENT 'Package width (cm)',  
  
  `Product_Measures` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Package_Measure_New` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  
  `id_supplier` int(10) unsigned DEFAULT NULL,
  `Supplier` varchar(150) NOT NULL DEFAULT '',
  `Delivery_Type` varchar(150) NOT NULL DEFAULT '',      
  `Supplier_Type` varchar(150) NOT NULL DEFAULT '',      
  `Products_origin` varchar(150) NOT NULL DEFAULT '',        
  
  
  `Head_Buyer` varchar(150) NOT NULL DEFAULT '',
  `Buyer` varchar(150) NOT NULL DEFAULT '',
  `Brand` varchar(150) NOT NULL DEFAULT '',
  `attribute` varchar(150) NOT NULL DEFAULT '', 
  `model` varchar(150) NOT NULL DEFAULT '',   
  `color` varchar(150) NOT NULL DEFAULT '',     

  `cost`           decimal(10,2) DEFAULT NULL,
  
  `cost_oms` decimal(10,2) DEFAULT NULL,
  `cost_oms_after_tax` decimal(10,2) DEFAULT NULL,  
  
  `delivery_cost_supplier` decimal(10,2) DEFAULT NULL,
  `eligible_free_shipping` int(1) NOT NULL DEFAULT '0',
  `original_price` decimal(10,2) DEFAULT NULL,
  `special_price`           decimal(10,2) DEFAULT NULL, 
  `special_price_after_tax` decimal(10,2) DEFAULT NULL, 
  
  `special_from_date` date DEFAULT NULL,   
  `special_to_date`  date DEFAULT NULL,   
  
  `price` decimal(10,2) DEFAULT NULL,
  `price_comparison` tinyint(1) unsigned DEFAULT '1',  
  `special_purchase_price` decimal(10,2) DEFAULT NULL,  
  `fixed_price` tinyint(1) unsigned DEFAULT '1',  

  `isVisible` int(1) NOT NULL DEFAULT '0',  
  `isActive_SKUConfig` int(1) NOT NULL DEFAULT '0',
  `isActive_SKUSimple` int(1) NOT NULL DEFAULT '0',
  `display_if_out_of_stock` tinyint(1) DEFAULT '0',
  `reason_not_visible` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '', 
  `pet_status`  varchar(255) CHARACTER SET utf8 DEFAULT NULL,  
  `pet_approved` tinyint(1) NOT NULL DEFAULT '0',

  
  `status_config`  varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `status_simple`  varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `sku_status`  varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  
  `barcode_ean`	varchar(255),  
  `stockout` int(11) DEFAULT '0',    
  `warehouse_stock` int(11) DEFAULT '0',
  `supplier_stock`  int(11) DEFAULT '0',
  `Reserved_BOB`    int(11) DEFAULT '0',  
  `Reserved_WMS`    int(11) DEFAULT '0',  
  
  `in_stock_WMS`     int(11) DEFAULT '0',   

  
  `stock_BOB`    int(11) DEFAULT '0',  
  `stock_WMS`     int(11) DEFAULT '0',   
  `average_days_in_stock_WMS`     int(11) DEFAULT '0',     
  `max_days_in_stock_WMS`     int(11) DEFAULT '0',     
  
  `in_reserved_WMS`     int(11) DEFAULT '0',   
  `reserved_stock_WMS`     int(11) DEFAULT '0',   
  `share_reserved_bob`     int(11) DEFAULT '0',   
  `share_stock_bob`     int(11) DEFAULT '0',   
  
  `stock_in_transit_OMS`   int(11) DEFAULT '0',   
  `cost_in_transit_OMS`   decimal(10,2) DEFAULT '0',   
  
  `sold_42_WMS`   int(11) DEFAULT '0',   
  `sold_30_WMS`   int(11) DEFAULT '0',   
  `sold_7_WMS`   int(11) DEFAULT '0',   
  
  `sku_age` int(11) DEFAULT NULL,
  `average_remaining_days`  int(11) DEFAULT NULL,
  `interval_sku_age` varchar(45) DEFAULT 'Infinito',
  `interval_remaining_days` varchar(45) DEFAULT 'Infinito',
  `repurchase_status` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT 'Not classified',

  `isMarketPlace` int(1) NOT NULL DEFAULT '0',
  `isMarketPlace_Since` varchar(10) NOT NULL DEFAULT '',
  `isMarketPlace_Until` varchar(10) NOT NULL DEFAULT '',
  
  `is_Outlet` bit NOT NULL,
  

  `created_at_Config` datetime DEFAULT NULL,
  `created_at_Simple` datetime DEFAULT NULL,

  `tax_percent` decimal(5,2) DEFAULT '0.00',

  `fulfillment_type` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT "Own Warehouse",

  `min_delivery_time` int(255) DEFAULT '3',
  `max_delivery_time` int(255) DEFAULT '7',
  `promise_delivery_badge` tinyint(1) unsigned DEFAULT NULL,
  `delivery_time_supplier` int(255) DEFAULT NULL,
  
  `tax_class` varchar(30) NOT NULL,
  `boost` tinyint NOT NULL DEFAULT '0',
  `interval_age` varchar(15) DEFAULT 'Infinito',
  `variation` varchar(100),
#  `sell_list` tinyint(4) DEFAULT '0',
#  `tracker` varchar(156) DEFAULT NULL,
#  `interval_age` varchar(15) DEFAULT NULL,

  PRIMARY KEY ( `Country`, `sku_simple`),
  KEY `sku_simple` (`sku_simple`),
  KEY `sku_config` (`sku_config`),
  KEY `Cat1`   ( `Cat1`  ),
  KEY `Cat2`   ( `Cat2`  ), 
  KEY `Cat3`   ( `Cat3`  ),
  KEY `Cat_BP` ( `Cat_BP`)
) 
ENGINE=InnoDB DEFAULT CHARSET=latin1;


