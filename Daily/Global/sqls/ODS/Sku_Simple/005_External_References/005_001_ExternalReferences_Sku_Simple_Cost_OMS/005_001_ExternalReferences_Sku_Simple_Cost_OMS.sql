/*
*  Monitoring to...
*/
#call  production.monitoring_step(`in_table_name` varchar(255),IN `in_country` varchar(255), `in_step` varchar(50), 5 );
call  production.monitoring_step( "ODS.Id_Procurement_Order_Item", "@v_country@", "Cost_oms" , @v_times@ );

/*
*  Continue...
*/
DROP  TABLE IF EXISTS TMP_CostPerSKU_Cost_@v_countryPrefix@;
CREATE TEMPORARY TABLE TMP_CostPerSKU_Cost_@v_countryPrefix@
(
   PRIMARY KEY( sku_simple, cost_oms, cost_oms_after_tax) 
)

SELECT
   KeyMap.sku_simple,
   Id_Procurement_Order_Item.cost_oms AS Cost_oms,
   Id_Procurement_Order_Item.tax,
   Id_Procurement_Order_Item.cost_oms_after_tax AS cost_oms_after_tax,
   count(*) AS Unidades,
   00000000000000000000000.00  AS costo_total,
   00000000000000000000000.00  AS costo_total_after_tax
FROM
               Id_Procurement_Order_Item_Key_Map_Sample_@v_countryPrefix@ AS KeyMap
    INNER JOIN Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS Id_Procurement_Order_Item
	     USING ( Id_Procurement_Order_Item )
WHERE    is_deleted   = 0
     AND is_cancelled = 0
GROUP BY sku_simple, cost_oms
;
 
UPDATE TMP_CostPerSKU_Cost_@v_countryPrefix@
SET
   costo_total           = cost_oms * unidades,
   costo_total_after_tax = cost_oms_after_tax * unidades
;

DROP TABLE IF EXISTS TMP_CostPerSKU_@v_countryPrefix@;
CREATE TABLE TMP_CostPerSKU_@v_countryPrefix@
(
   PRIMARY KEY( sku_simple) 
)
SELECT
   sku_simple,
   SUM( costo_total ) AS Cost_oms,
   SUM( costo_total_after_tax ) AS cost_oms_after_tax,
   SUM( Unidades ) AS Unidades,
   SUM( costo_total ) / SUM( Unidades )  AS costo_ponderado,
   SUM( costo_total_after_tax ) / SUM( Unidades )  AS costo_ponderado_after_tax
FROM TMP_CostPerSKU_Cost_@v_countryPrefix@
GROUP BY sku_simple
;


/*
* Cost_OMS in Catalog
*/
UPDATE            Sku_Simple_Sample_@v_countryPrefix@ AS Sku_Simple_Sample
       INNER JOIN TMP_CostPerSKU_@v_countryPrefix@ AS p
            USING ( Sku_Simple )
SET 
   Sku_Simple_Sample.Cost_Oms           = p.costo_ponderado    ,
   Sku_Simple_Sample.Cost_Oms_After_Tax = p.costo_ponderado_after_tax    
   
WHERE
   p.costo_ponderado > 0 
;   

/*
*  Insert Monitoring LOG
*/
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'ODS.Sku_Simple',
  "Cost_OMS",
  NOW(),
  NOW(),
  sum(Cost_OMS),
  sum(Cost_OMS)
FROM
   Sku_Simple_Sample_@v_countryPrefix@
WHERE 
   Country = '@v_countryPrefix@'   
;