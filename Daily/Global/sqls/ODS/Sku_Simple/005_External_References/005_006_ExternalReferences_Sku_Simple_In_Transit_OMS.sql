call  production.monitoring_step( "ODS.Id_Procurement_Order_Item", "@v_country@", "Cost_oms" , @v_times@ );
call  production.monitoring_step( "ODS.Id_Procurement_Order_Item", "@v_country@", "in_transit" , @v_times@ );

DROP TEMPORARY TABLE IF EXISTS items_procured_in_transit_@v_countryPrefix@;
CREATE TEMPORARY TABLE items_procured_in_transit_@v_countryPrefix@ ( PRIMARY KEY ( Sku_Simple ) )
 SELECT
	sku_simple,
	count(*) AS items,
	avg(cost_oms) AS cost_oms
FROM
    Id_Procurement_Order_Item_Sample_@v_countryPrefix@
WHERE in_transit = 1 GROUP BY sku_simple;


UPDATE            Sku_Simple_Sample_@v_countryPrefix@ AS Sku_Simple_Sample
       INNER JOIN items_procured_in_transit_@v_countryPrefix@ AS p
            USING ( Sku_Simple )
SET 
   Sku_Simple_Sample.stock_in_transit_OMS = p.items    ,
   Sku_Simple_Sample.cost_in_transit_OMS  = p.cost_oms    
;   
  
   /*
*  Insert Monitoring LOG
*/
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'ODS.Sku_Simple',
  "In_transit",
  NOW(),
  NOW(),
  sum(stock_in_transit_OMS),
  sum(stock_in_transit_OMS)
FROM
   Sku_Simple_Sample_@v_countryPrefix@
WHERE 
   Country = '@v_countryPrefix@'   
;