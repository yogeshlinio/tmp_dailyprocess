call  production.monitoring_step( "ODS.Stock_Item_Id", "@v_country@", "in_stock"      , @v_times@ );
# What is on the outlet
DROP TEMPORARY TABLE IF EXISTS Is_Outlet_@v_countryPrefix@;
CREATE TEMPORARY TABLE Is_Outlet_@v_countryPrefix@ ( PRIMARY KEY ( Sku_Config ) )
SELECT
   Sku_Config
FROM
                  Sku_Simple_Sample_@v_countryPrefix@    AS Sku_Simple
	   INNER JOIN Sku_Simple_Key_Map			  
	        USING ( country , sku_simple )
	   INNER JOIN @bob_live@.catalog_category  
	        USING ( id_catalog_category )
   
WHERE 
       catalog_category.id_catalog_category > 1   
   AND NAME LIKE '%liquida%'
GROUP BY Sku_Config
;

UPDATE            
	             Sku_Simple_Sample_@v_countryPrefix@    AS Sku_Simple
       INNER JOIN Is_Outlet_@v_countryPrefix@ as IN_Oulet
               ON Sku_Simple.Sku_Config = IN_Oulet.Sku_Config
SET			
  is_Outlet = 1
WHERE in_stock_WMS = 1;

/*
*  Insert Monitoring LOG
*/
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'ODS.Sku_Simple',
  "In_Oulet",
  NOW(),
  NOW(),
  sum(is_Outlet),
  sum(is_Outlet)
FROM
   Sku_Simple_Sample_@v_countryPrefix@
WHERE 
   Country = '@v_countryPrefix@'   
;