/*
*  Monitoring to...
*/
#call  production.monitoring_step(`in_table_name` varchar(255),IN `in_country` varchar(255), `in_step` varchar(50), 5 );
call  production.monitoring_step( "ODS.Stock_Item_Id", "@v_country@", "in_stock"      , @v_times@ );
call  production.monitoring_step( "ODS.Stock_Item_Id", "@v_country@", "days_in_stock" , @v_times@ );

DROP TEMPORARY TABLE IF EXISTS skus_reserved_@v_countryPrefix@;
CREATE TEMPORARY TABLE skus_reserved_@v_countryPrefix@ ( PRIMARY KEY ( Sku_Simple ) )
select distinct(sku_simple) as Sku_Simple, count(*) as stock_Reserved from Stock_Item_Id_Sample_@v_countryPrefix@
WHERE reserved=1
GROUP BY Sku_Simple
;
UPDATE            Sku_Simple_Sample_@v_countryPrefix@ AS Sku_Simple_Sample
       INNER JOIN skus_reserved_@v_countryPrefix@  AS Tmp
	        USING ( sku_simple )
SET
     Sku_Simple_Sample.in_reserved_WMS    = 1,
     Sku_Simple_Sample.reserved_stock_WMS = Tmp.stock_Reserved
;  

# Indicadores de Stock para consulta
UPDATE Sku_Simple_Sample_@v_countryPrefix@
SET 
    share_reserved_bob = 	IF (	reserved_bob = 0,0,
													IF (	(reserved_bob - reserved_stock_WMS) / stock_wms > 1,
													          1,(reserved_bob - reserved_stock_WMS) / stock_wms)
						        ), 
    share_stock_bob    = stock_bob / stock_wms;
	
 
/*
*  Insert Monitoring LOG
*/
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'ODS.Sku_Simple',
  "reserved_stock_WMS",
  NOW(),
  NOW(),
  sum(reserved_stock_WMS),
  sum(reserved_stock_WMS)
FROM
   Sku_Simple_Sample_@v_countryPrefix@
WHERE 
   Country = '@v_countryPrefix@'   
;