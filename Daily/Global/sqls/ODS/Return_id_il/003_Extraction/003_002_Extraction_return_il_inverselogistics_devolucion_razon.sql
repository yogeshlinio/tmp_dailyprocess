UPDATE            return_il_Sample_@v_countryPrefix@ as return_il_Sample
       INNER JOIN return_il_Key_Map USING ( country , item_id)
       INNER JOIN @v_wmsprod@.inverselogistics_devolucion_razon ON return_il_Key_Map.reason_id = inverselogistics_devolucion_razon.reason_id
SET
   /*Defaults and Simple Extraction*/
   Sku_Simple_Sample.reason_for_entrance_il                   = inverselogistics_devolucion_razon.descriptioon;                   