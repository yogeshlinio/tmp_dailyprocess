UPDATE            return_il_Sample_@v_countryPrefix@ as return_il_Sample
       INNER JOIN @v_wmsprod@.inverselogistics_agendamiento ON return_il_Sample.item_id = @v_wmsprod@.inverselogistics_agendamiento.item_id
SET
   /*Defaults and Simple Extraction*/
   Sku_Simple_Sample.shipping_carrier_tracking_code_inverse                  = inverselogistics_catalog_money_status.carrier_tracking_code; 