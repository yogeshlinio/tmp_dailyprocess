UPDATE            return_il_Sample_@v_countryPrefix@ as return_il_Sample
       INNER JOIN return_il_Key_Map USING ( country , item_id)
       INNER JOIN @v_wmsprod@.inverselogistics_catalog_money_status ON return_il_Key_Map.money_status = inverselogistics_catalog_money_status.id_status
SET
   /*Defaults and Simple Extraction*/
   Sku_Simple_Sample.money_status                  = inverselogistics_catalog_money_status.money_status; 