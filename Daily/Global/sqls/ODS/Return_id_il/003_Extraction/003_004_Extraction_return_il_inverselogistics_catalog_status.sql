UPDATE            return_il_Sample_@v_countryPrefix@ as return_il_Sample
       INNER JOIN return_il_Key_Map USING ( country , item_id)
       INNER JOIN @v_wmsprod@.inverselogistics_catalog_status ON return_il_Key_Map.status = inverselogistics_catalog_status.id_status
SET
   /*Defaults and Simple Extraction*/
   Sku_Simple_Sample.status_wms_il                  = inverselogistics_catalog_status.status_inverselogistic;  