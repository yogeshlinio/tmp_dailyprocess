      UPDATE            return_il_Sample_@v_countryPrefix@  as a
	  INNER JOIN return_il_Key_Map USING ( country , item_id)
	  INNER JOIN @v_wmsprod@.tms_status_delivery  ON return_il_Key_Map.wms_tracking_code = tms_status_delivery.cod_rastreamento
SET
a.il_type = 'Fail delivery',
a.is_fail_delivery = 1, 
a.date_inbound_il = tms_status_delivery.date 
WHERE tms_status_delivery.id_status = 10;


UPDATE return_il_Sample_@v_countryPrefix@ as  a
INNER JOIN return_il_Key_Map USING ( country , item_id)
	  INNER JOIN @v_wmsprod@.ws_log_lista_embarque b ON b.cod_rastreamento = return_il_Key_Map.wms_tracking_code
SET a.reason_for_entrance_il = b.observaciones
WHERE
 a.is_fail_delivery = 1;
 
 
 UPDATE return_il_Sample_@v_countryPrefix@ as a
SET a.il_type = 'Return'
WHERE a.is_returned = 1;


UPDATE return_il_Sample_@v_countryPrefix@ a
SET a.return_accepted = CASE
WHEN a.status_wms_il = 'retornar_stock'
OR a.status_wms_il = 'enviar_cuarentena' THEN
	1
ELSE
	NULL
END;

UPDATE return_il_Sample_@v_countryPrefix@ as a 
SET a.is_exits_il = 1 
WHERE date_exit_il is not null;

UPDATE return_il_Sample_@v_countryPrefix@ as a 
SET a.is_pending_return_wh_il = 1 
WHERE status_wms_il in ('aguardando_agendamiento','aguardando_retorno');

DROP TEMPORARY TABLE IF EXISTS operations_@v_countryPrefix@.TMP2analyst ; 
CREATE TEMPORARY TABLE operations_@v_countryPrefix@.TMP2analyst
SELECT
		b.cod_rastreamento,
		b.id_status,
		b.id_user,
		c.nome
	FROM
		@v_wmsprod@.tms_status_delivery b
	JOIN @v_wmsprod@.usuarios c ON b.id_user = c.usuarios_id 
WHERE
	b.id_status = 10 ;

create index cod on operations_@v_countryPrefix@.TMP2analyst(cod_rastreamento);

UPDATE return_il_Sample_@v_countryPrefix@ a
INNER JOIN return_il_Key_Map USING ( country , item_id)
INNER JOIN operations_@v_countryPrefix@.TMP2analyst as  b ON b.cod_rastreamento = return_il_Key_Map.wms_tracking_code
SET a.analyst_il = b.nome
WHERE
	a.il_type = 'Fail delivery';

UPDATE return_il_Sample_@v_countryPrefix@ as a
SET a.delivered_last_30_il = CASE
WHEN datediff(curdate(), date_delivered) <= 45
AND datediff(curdate(), date_delivered) >= 15 THEN
	1
ELSE
	0
END;