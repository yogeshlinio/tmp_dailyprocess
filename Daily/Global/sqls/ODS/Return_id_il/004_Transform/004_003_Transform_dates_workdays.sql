  
	  UPDATE return_il_Sample_@v_countryPrefix@
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON date(return_il_Sample_@v_countryPrefix@.date_last_track_il) = calcworkdays.date_first
		AND date(return_il_Sample_@v_countryPrefix@.date_inbound_il) = calcworkdays.date_last
SET 
days_to_inbound_il = 
	IF(date_last_track_il IS NULL,
		NULL,
		DATEDIFF(date(date_inbound_il),date(date_last_track_il))),
workdays_to_inbound_il = 
	IF(date_last_track_il IS NULL,
		NULL,calcworkdays.workdays
		)
WHERE return_il_Sample_@v_countryPrefix@.is_returned=1
AND return_il_Sample_@v_countryPrefix@.date_inbound_il IS NOT NULL;


UPDATE return_il_Sample_@v_countryPrefix@
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON date(return_il_Sample_@v_countryPrefix@.date_inbound_il) = calcworkdays.date_first
		AND date(return_il_Sample_@v_countryPrefix@.date_quality_il)= calcworkdays.date_last
SET 
days_to_quality_il = 
	IF(date_inbound_il IS NULL,
		NULL,
		DATEDIFF(date(date_quality_il),date(date_inbound_il))),
workdays_to_quality_il = 
	IF((date_inbound_il IS NULL),
		NULL,calcworkdays.workdays)
WHERE return_il_Sample_@v_countryPrefix@.is_returned =1
AND date_quality_il is not null;



UPDATE return_il_Sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON date(date_delivered) = calcworkdays.date_first
		AND date(date_customer_request_il) = calcworkdays.date_last
SET 
days_to_customer_request_il = 
IF(date_delivered IS NULL,
		NULL,
	DATEDIFF(date(date_customer_request_il),date(date_delivered))),
workdays_to_customer_request_il  = 
IF(date_customer_request_il IS NULL,
		NULL,calcworkdays.workdays)
WHERE is_returned = 1
AND date_customer_request_il IS NOT NULL;



UPDATE return_il_Sample_@v_countryPrefix@
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON date(return_il_Sample_@v_countryPrefix@.date_customer_request_il) = calcworkdays.date_first
		AND date(return_il_Sample_@v_countryPrefix@.date_first_track_il) = calcworkdays.date_last
SET 
days_to_track_il = 
	IF(date_customer_request_il IS NULL,
		NULL,
		DATEDIFF(date(date_first_track_il),date(date_customer_request_il))),
workdays_to_track_il = 
	IF(date_customer_request_il IS NULL,
		NULL,calcworkdays.workdays
		)
WHERE is_returned =1
AND date_first_track_il IS NOT NULL;


UPDATE return_il_Sample_@v_countryPrefix@
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON date(return_il_Sample_@v_countryPrefix@.date_quality_il) = calcworkdays.date_first
		AND date(return_il_Sample_@v_countryPrefix@.date_refunded_il)= calcworkdays.date_last
SET 
days_to_refunded_il = 
	IF((date_quality_il IS NULL),
		NULL,
		DATEDIFF(date(date_refunded_il),date(date_quality_il))),
workdays_to_refunded_il = 
	IF((date_quality_il IS NULL),
		NULL,calcworkdays.workdays)
WHERE is_returned =1
AND date_refunded_il IS NOT NULL;

UPDATE return_il_Sample_@v_countryPrefix@
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON date(return_il_Sample_@v_countryPrefix@.date_customer_request_il) = calcworkdays.date_first
AND 
IF(date(return_il_Sample_@v_countryPrefix@.date_refunded_il) IS NULL,CURDATE(),date(return_il_Sample_@v_countryPrefix@.date_refunded_il)) = calcworkdays.date_last
SET 
workdays_total_refunded_il = 
	IF((date_customer_request_il IS NULL),
		NULL,calcworkdays.workdays)
WHERE is_returned =1
AND date_quality_il IS NOT NULL;


UPDATE return_il_Sample_@v_countryPrefix@ 
INNER JOIN operations_@v_countryPrefix@.calcworkdays
	ON date(return_il_Sample_@v_countryPrefix@.date_customer_request_il)= calcworkdays.date_first
		AND date(return_il_Sample_@v_countryPrefix@.date_closed_il)= calcworkdays.date_last
SET 
days_to_closed_il = 
	IF(date_customer_request_il IS NULL,
		NULL,
		DATEDIFF(date(date_closed_il),date(date_customer_request_il))),
workdays_to_closed_il = 
	IF(date_customer_request_il IS NULL,
		NULL,calcworkdays.workdays)
WHERE is_returned =1
AND date_closed_il IS NOT NULL;



UPDATE return_il_Sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays AS b
	ON date(a.datetime_status_wms_il) = b.date_first
		AND date(a.date_refunded_il) = b.date_last
SET 
days_status_il_to_refund_il = 
	IF(date_refunded_il IS NULL,
		NULL,
		DATEDIFF(date(date_refunded_il),date(datetime_status_wms_il))),
workdays_status_il_to_refund_il = 
	IF(date_refunded_il IS NULL,
		NULL,b.workdays
		)
WHERE a.is_returned=1;

UPDATE return_il_Sample_@v_countryPrefix@ AS a
INNER JOIN
(SELECT item_id,TIMESTAMPDIFF( SECOND ,datetime_status_wms_il , date_refunded_il)/3600 AS time
FROM return_il_Sample_@v_countryPrefix@) AS b
ON a.item_id=b.item_id
SET a.times_status_il_to_refund_il=b.time
WHERE a.is_returned=1
AND date_refunded_il IS NOT NULL;





