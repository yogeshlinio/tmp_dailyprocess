DROP TABLE IF EXISTS return_il_Key_Map_Template_@v_countryPrefix@;
CREATE TABLE `return_il_Key_Map_Template_@v_countryPrefix@` (
  `country` varchar(10) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `item_id` int(11) unsigned NOT NULL DEFAULT '0',
  `return_id` int(11) unsigned NOT NULL DEFAULT '0',
  `item_venda_id` int(11) unsigned NOT NULL DEFAULT '0',
  `pedido_venda_id` int(11) unsigned NOT NULL DEFAULT '0',
  `reason_id` int(11) unsigned NOT NULL DEFAULT '0',
  `status` int(11) unsigned NOT NULL DEFAULT '0',
  `action_id` int(11) unsigned NOT NULL DEFAULT '0',
  `money_status` int(11) unsigned NOT NULL DEFAULT '0',
  `sku_simple` varchar(200) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `estoque_id` varchar(200) CHARACTER SET utf8 NOT NULL DEFAULT '', 
  `order_number` int(11) unsigned NOT NULL DEFAULT '0', 
  `wms_tracking_code` varchar(200) CHARACTER SET utf8 NOT NULL DEFAULT '', 
  
  
  
  KEY (`Country` , item_id ),
  KEY `item_id` (`item_id`),
  KEY `return_id` (`return_id`),
  KEY `item_venda_id` (`item_venda_id`),
  KEY `estoque_id` (`estoque_id`),
  KEY `pedido_venda_id` (`pedido_venda_id`),
  KEY `reason_id` (`pedido_venda_id`),
  KEY `status` (`pedido_venda_id`),
  KEY `action_id` (`pedido_venda_id`),
  KEY `money_status` (`pedido_venda_id`),
  KEY `sku_simple` (`sku_simple`),
  KEY `order_number` (`order_number`),
  KEY `wms_tracking_code` (`wms_tracking_code`)
);