DROP TABLE IF EXISTS `return_il_template`;
CREATE TABLE `return_il_template` (
  `item_id` varchar(45) NOT NULL DEFAULT '',
  `Country` text,
  `comments_il` varchar(255) DEFAULT NULL,
  `shipping_carrier_tracking_code_inverse` varchar(100) DEFAULT NULL,
  `status_wms_il` text,
  `id_ilwh` varchar(255) DEFAULT NULL,
  `il_type` varchar(45) DEFAULT NULL,
  `reason_for_entrance_il` varchar(255) DEFAULT NULL,
  `action_il` varchar(255) DEFAULT NULL,
  `damaged_item` varchar(255) DEFAULT NULL,
  `damaged_package` varchar(255) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `return_accepted` int(255) DEFAULT '0',
  `position_il` varchar(255) DEFAULT NULL,
  `analyst_il` varchar(255) DEFAULT NULL,
  `delivered_last_30_il` int(1) DEFAULT '0',
  `is_exits_il` int(1) DEFAULT '0',
  `is_pending_return_wh_il` int(1) DEFAULT '0',
  `is_returned` int(1) DEFAULT '0',
  `is_fail_delivery` int(1) DEFAULT '0',
  `date_status_wms_il` date DEFAULT NULL,
  `date_returned` date DEFAULT NULL,
  `date_canceled_il` date DEFAULT NULL,
  `date_exit_il` date DEFAULT NULL,
  `return_receivedserv` int(11) DEFAULT NULL,
  `date_inbound_il` date DEFAULT NULL,
  `date_customer_request_il` date DEFAULT NULL,
  `date_first_track_il` date DEFAULT NULL,
  `date_last_track_il` date DEFAULT NULL,
  `date_inbound_carrier_il` date DEFAULT NULL,
  `date_quality_il` date DEFAULT NULL,
  `date_cash_refunded_il` date DEFAULT NULL,
  `date_voucher_refunded_il` date DEFAULT NULL,
  `date_refunded_il` date DEFAULT NULL,
  `date_closed_il` date DEFAULT NULL,
  `datetime_status_wms_il` datetime DEFAULT NULL,
  `money_status` text,
  `days_to_customer_request_il` int(11) DEFAULT NULL,
  `days_to_track_il` int(11) DEFAULT NULL,
  `days_to_inbound_il` int(11) DEFAULT NULL,
  `days_to_quality_il` int(11) DEFAULT NULL,
  `days_to_refunded_il` int(11) DEFAULT NULL,
  `days_status_il_to_refund_il` int(11) DEFAULT NULL,
  `days_to_closed_il` int(11) DEFAULT NULL,
  `workdays_to_customer_request_il` int(11) DEFAULT NULL,
  `workdays_to_track_il` int(11) DEFAULT NULL,
  `workdays_to_inbound_il` int(11) DEFAULT NULL,
  `workdays_to_quality_il` int(11) DEFAULT NULL,
  `workdays_to_refunded_il` int(11) DEFAULT NULL,
  `workdays_total_refunded_il` int(11) DEFAULT NULL,
  `workdays_to_closed_il` int(11) DEFAULT NULL,
  `workdays_status_il_to_refund_il` int(11) DEFAULT NULL,
  `backlog_track_il` int(1) DEFAULT '0',
  `backlog_currier_il` int(1) DEFAULT '0',
  `backlog_quality_il` int(1) DEFAULT '0',
  `backlog_refund_consignment_il` int(1) DEFAULT '0',
  `backlog_refund_effecty_il` int(1) DEFAULT '0',
  `backlog_refund_reversion_il` int(1) DEFAULT '0',
  `user_name_status_il` text,
  `times_status_il_to_refund_il` float(255,0) DEFAULT NULL,
  `backlog_refund_voucher_il` int(1) DEFAULT '0',
  `workdays_backlog_track_il` int(11) DEFAULT NULL,
  `workdays_backlog_currier_il` int(11) DEFAULT NULL,
  `workdays_backlog_quality_il` int(11) DEFAULT NULL,
  `workdays_backlog_refund_consignment_il` int(11) DEFAULT NULL,
  `workdays_backlog_refund_effecty_il` int(11) DEFAULT NULL,
  `workdays_backlog_refund_reversion_il` int(11) DEFAULT NULL,
  `workdays_backlog_refund_voucher_il` int(11) DEFAULT NULL,
  `on_time_track_il` int(1) DEFAULT NULL,
  `on_time_currier_il` int(1) DEFAULT NULL,
  `on_time_quality_il` int(1) DEFAULT NULL,
  `on_time_refund_consignment_il` int(1) DEFAULT NULL,
  `on_time_refund_effecty_il` int(1) DEFAULT NULL,
  `on_time_refund_reversion_il` int(1) DEFAULT NULL,
  `on_time_refund_voucher_il` int(1) DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
