DROP TABLE IF EXISTS return_il_Key_Map_Sample_@v_countryPrefix@;
CREATE TABLE return_il_Key_Map_Sample_@v_countryPrefix@ LIKE return_il_Key_Map_Template_@v_countryPrefix@;
/*INSERT ALL THE PRIMARY KEYS*/
INSERT return_il_Key_Map_Sample_@v_countryPrefix@
(
  `Country`,
  `item_id`,
  `sku_simple`,
  `order_number`,
  `item_venda_id`
)
SELECT DISTINCT
   '@v_countryPrefix@' AS Country,
   `item_id`,
   `sku` ,
   `numero_order`,
   `itens_venda_id`  
FROM 
   @v_wmsprod@.itens_venda;