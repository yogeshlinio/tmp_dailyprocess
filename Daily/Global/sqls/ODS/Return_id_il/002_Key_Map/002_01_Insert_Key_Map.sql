UPDATE            return_il_Key_Map_Sample_@v_countryPrefix@ AS return_il_Key_Map_Sample
       INNER JOIN @v_wmsprod@.inverselogistics_devolucion
	           ON return_il_Key_Map_Sample.item_id = inverselogistics_devolucion.item_id
SET
   return_il_Key_Map_Sample.return_id        = inverselogistics_devolucion.id,
   return_il_Key_Map_Sample.estoque_id              = inverselogistics_devolucion.estoque_id,
   return_il_Key_Map_Sample.pedido_venda_id        = inverselogistics_devolucion.pedido_venda_id,
   return_il_Key_Map_Sample.reason_id = inverselogistics_devolucion.reason_id,
   return_il_Key_Map_Sample.action_id              = inverselogistics_devolucion.action_id,
   return_il_Key_Map_Sample.status              = inverselogistics_devolucion.status,
   return_il_Key_Map_Sample.money_status              = inverselogistics_devolucion.money_status
;

UPDATE            return_il_Key_Map_Sample_@v_countryPrefix@ AS return_il_Key_Map_Sample
       INNER JOIN @v_wmsprod@.vw_itens_venda_entrega
	           ON return_il_Key_Map_Sample.item_id = vw_itens_venda_entrega.item_id
SET
   return_il_Key_Map_Sample.wms_tracking_code        = vw_itens_venda_entrega.cod_rastreamento ;