/*
Nombre:
Fecha:
Descripción:
Version:
*/
DROP TABLE
 IF EXISTS test_linio.WeeklyReport_mx;

CREATE TABLE test_linio.WeeklyReport_mx (
	`Date Placed` date NOT NULL,
	`Weekday` VARCHAR (9) NOT NULL,
	`Month` INT NOT NULL,
	`Exchange Rate` DECIMAL (15, 4) NOT NULL,
	`Gross Price` DECIMAL (15, 4) NOT NULL,
	`Gross Paid Price AT` DECIMAL (15, 4) NOT NULL,
	`Gross Price AT` DECIMAL (15, 4) NOT NULL,
	`# Orders` INT NOT NULL,
	`# Items` INT NOT NULL,
	`Cost` DECIMAL (15, 4) NOT NULL,
	`Inbound Cost` DECIMAL (15, 4) NOT NULL,
	`Coupon value` DECIMAL (15, 4) NOT NULL,
	`Original Price` DECIMAL (15, 4) NOT NULL,
	`MKT Spend` DECIMAL (15, 4) NOT NULL,
	`Visits` INT NOT NULL,
	`Branded Visits` DECIMAL (15, 4) NOT NULL,
	`Total Customers` INT NOT NULL,
	`New Customers` INT NOT NULL,
	`Unique Visitors` INT NOT NULL,
	`Pageviews` INT NOT NULL,
	`Canceled Orders` DECIMAL (15, 4) NOT NULL,
	`Rejected Orders` DECIMAL (15, 4) NOT NULL,
	`Returned Items` INT NOT NULL,
	`Returned Value` DECIMAL (15, 4) NOT NULL,
	`CC %` DECIMAL (15, 4) NOT NULL,
	`OD %` DECIMAL (15, 4) NOT NULL,
	`Paypal %` DECIMAL (15, 4) NOT NULL,
	`Oxxo %` DECIMAL (15, 4) NOT NULL,
	`Electronics Sales` DECIMAL (15, 4) NOT NULL,
	`Fashion Sales` DECIMAL (15, 4) NOT NULL,
	`Others Sales` DECIMAL (15, 4) NOT NULL,
	`Inventory Value` DECIMAL (15, 4) NOT NULL,
	`Inventory Online` DECIMAL (15, 4) NOT NULL,
	`SKUs visible` INT NOT NULL
);

INSERT INTO test_linio.WeeklyReport_mx (
	`Date Placed`,
	`Weekday`,
	`Month`
) SELECT
	a.Date AS `Date Placed`,
	DATE_FORMAT(a.Date, '%W') AS 'Weekday',
	a.MonthNum AS 'Month'
FROM
	development_mx.A_Master a
GROUP BY
	a.Date;

UPDATE test_linio.WeeklyReport_mx a
INNER JOIN (
	SELECT DISTINCT
		Month_Num,
		XR
	FROM
		development_mx.A_E_BI_ExchangeRate
	WHERE
		Country = 'MEX'
) b ON a. MONTH = b.Month_Num
SET a.`Exchange Rate` = b.XR;

UPDATE test_linio.WeeklyReport_mx a
INNER JOIN (
	SELECT
		Date,
		sum(Price) AS `Gross Price`,
		sum(PaidPriceAfterTax) AS `Gross Paid Price AT`,
		sum(PriceAfterTax) AS `Gross Price AT`,
		count(DISTINCT OrderNum) AS `# Orders`,
		count(ItemID) AS `# Items`,
		sum(CostAfterTax) AS `Cost`,
		sum(DeliveryCostSupplier) AS `Inbound Cost`,
		sum(CouponValueAfterTax) AS `Coupon value`,
		sum(OriginalPrice) AS `Original Price`,
		count(DISTINCT CustomerNum) AS `Total Customers`
	FROM
		development_mx.A_Master
	WHERE
		OrderBeforeCan = 1
	GROUP BY
		Date
) c ON a.`Date Placed` = c.Date
SET a.`Gross Price` = c.`Gross Price`,
 a.`Gross Paid Price AT` = c.`Gross Paid Price AT`,
 a.`Gross Price AT` = c.`Gross Price AT`,
 a.`# Orders` = c.`# Orders`,
 a.`# Items` = c.`# Items`,
 a.`Cost` = c.`Cost`,
 a.`Inbound Cost` = c.`Inbound Cost`,
 a.`Coupon value` = c.`Coupon value`,
 a.`Original Price` = c.`Original Price`,
 a.`Total Customers` = c.`Total Customers`;

UPDATE test_linio.WeeklyReport_mx a
INNER JOIN (
	SELECT
		date,
		sum(cost) AS `MKT Spend`
	FROM
		marketing_report.global_report
	WHERE
		country = 'Mexico'
	GROUP BY
		date
) d ON a.`Date Placed` = d.date
SET a.`MKT Spend` = d.`MKT Spend`;

UPDATE test_linio.WeeklyReport_mx a
INNER JOIN (
	SELECT
		date,
		sum(visits) AS `Visits`
	FROM
		SEM.campaign_ad_group_mx
	GROUP BY
		Date
) e ON a.`Date Placed` = e.date
SET a.`Visits` = e.`Visits`;

#*** Branded Visits;

UPDATE test_linio.WeeklyReport_mx a
INNER JOIN (
	SELECT
		Date,
		sum(

			IF (
				First_Gross_Order = OrderNum,
				1,
				0
			)
		) AS `New Customers`,
		sum(Cancelled) AS `Canceled Orders`,
		sum(Rejected) AS `Rejected Orders`
	FROM
		development_mx.Out_SalesReportOrder
	GROUP BY
		Date
) f ON a.`Date Placed` = f.Date
SET a.`New Customers` = f.`New Customers`,
 a.`Canceled Orders` = f.`Canceled Orders`,
 a.`Rejected Orders` = f.`Rejected Orders`;

#*** Unique Visitors;

#*** Pageviews;

UPDATE test_linio.WeeklyReport_mx a
INNER JOIN (
	SELECT
		Date,
		count(ItemID) AS `Returned Items`,
		sum(Price) AS `Returned Value`
	FROM
		development_mx.A_Master
	WHERE
		Returned = 1
	GROUP BY
		Date
) g ON a.`Date Placed` = g.Date
SET a.`Returned Items` = g.`Returned Items`,
 a.`Returned Value` = g.`Returned Value`;

UPDATE test_linio.WeeklyReport_mx a
INNER JOIN (
	SELECT
		Date,
		count(ItemID) AS `CC %`
	FROM
		development_mx.A_Master
	WHERE
		OrderBeforeCan = 1
	AND PaymentMethod IN (
		'Amex_Gateway',
		'Banorte_Payworks'
	)
	GROUP BY
		Date
) h ON a.`Date Placed` = h.Date
SET a.`CC %` = h.`CC %`;

UPDATE test_linio.WeeklyReport_mx a
INNER JOIN (
	SELECT
		Date,
		count(ItemID) AS `OD %`
	FROM
		development_mx.A_Master
	WHERE
		OrderBeforeCan = 1
	AND PaymentMethod IN (
		'CashOnDelivery_Payment',
		'CreditCardOnDelivery_Payment'
	)
	GROUP BY
		Date
) i ON a.`Date Placed` = i.Date
SET a.`OD %` = i.`OD %`;

UPDATE test_linio.WeeklyReport_mx a
INNER JOIN (
	SELECT
		Date,
		count(ItemID) AS `Paypal %`
	FROM
		development_mx.A_Master
	WHERE
		OrderBeforeCan = 1
	AND PaymentMethod IN ('Paypal_Express_Checkout')
	GROUP BY
		Date
) j ON a.`Date Placed` = j.Date
SET a.`Paypal %` = j.`Paypal %`;

UPDATE test_linio.WeeklyReport_mx a
INNER JOIN (
	SELECT
		Date,
		count(ItemID) AS `Oxxo %`
	FROM
		development_mx.A_Master
	WHERE
		OrderBeforeCan = 1
	AND PaymentMethod IN ('Oxxo_Direct')
	GROUP BY
		Date
) k ON a.`Date Placed` = k.Date
SET a.`Oxxo %` = k.`Oxxo %`;

UPDATE test_linio.WeeklyReport_mx a
INNER JOIN (
	SELECT
		Date,
		sum(Price) AS `Electronics Sales`
	FROM
		development_mx.A_Master
	WHERE
		OrderBeforeCan = 1
	AND Cat1 IN (
		'Electrónicos',
		'TV, Audio y Video',
		'Videojuegos'
	)
	GROUP BY
		Date
) l ON a.`Date Placed` = l.Date
SET a.`Electronics Sales` = l.`Electronics Sales`;

UPDATE test_linio.WeeklyReport_mx a
INNER JOIN (
	SELECT
		Date,
		sum(Price) AS `Fashion Sales`
	FROM
		development_mx.A_Master
	WHERE
		OrderBeforeCan = 1
	AND Cat1 IN ('Fashion')
	GROUP BY
		Date
) m ON a.`Date Placed` = m.Date
SET a.`Fashion Sales` = m.`Fashion Sales`;

UPDATE test_linio.WeeklyReport_mx a
INNER JOIN (
	SELECT
		Date,
		sum(Price) AS `Others Sales`
	FROM
		development_mx.A_Master
	WHERE
		OrderBeforeCan = 1
	AND Cat1 IN (
		'Home and Living',
		'Belleza'
	)
	GROUP BY
		Date
) n ON a.`Date Placed` = n.Date
SET a.`Others Sales` = n.`Others Sales`;

#*** Inventory Value;

#*** Inventory OnLine;

UPDATE test_linio.WeeklyReport_mx a
INNER JOIN (
	SELECT
		`Day`,
		count AS `SKUs visible`
	FROM
		development_mx.A_E_M1_SKUs_Visible
) o ON a.`Date Placed` = o.`Day`
SET a.`SKUs visible` = o.`SKUs visible`

/*Se crea la tabla WeeklyReport en development_mx*/
DROP TABLE IF EXISTS development_mx.WeeklyReport;
CREATE TABLE development_mx.WeeklyReport LIKE test_linio.WeeklyReport_mx;

REPLACE development_mx.WeeklyReport
SELECT
   WeeklyReport.*
FROM test_linio.WeeklyReport_mx;






