USE @marketing@;

drop temporary table if exists total;
create  temporary table total (index(date, campaign, fk_channel, country))
select date, fk_channel, campaign,country, sum(gross_revenue) rev, count(1) uds
from channel_performance cp 
where source = 'facebook'
group by date, fk_channel,campaign, country;

drop temporary table if exists costos;
create  temporary table costos (index(date, campaign_name, fk_channel, country))
select date,fk_channel,country,campaign_name, sum(spend)*XR as spent 
from marketing.facebook_campaign f
inner join development_mx.A_E_BI_ExchangeRate_USD usd
on f.campaign = usd.campaign
and f.country  = usd.country
group by date, fk_channel, country, campaign_name;

update channel_performance cp
inner join 
total t
on cp.date = t.date
and cp.fk_channel = t.fk_channel
and cp.campaign = t.campaign
and cp.country = t.country
inner join
costos t2
on cp.date = t2.date
and cp.fk_channel = t2.fk_channel
and cp.campaign = t2.campaign_name
and cp.country = t2.country
set cp.marketing_cost =if(t.rev= 0, spent*gross_revenue/t.uds,spent*gross_revenue/t.rev);
