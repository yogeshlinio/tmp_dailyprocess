USE @marketing@;

update channel_performance as t1
    left join (select date, channel,sum(gross_revenue) as sum_gr,count(channel) as count_channel 
      from channel_performance
      where medium = 'affiliates' or source = 'affiliates'
      group by channel,date) as t2 
      on t1.channel = t2.channel and t1.date = t2.date
    inner join marketing_report.affiliates_@v_countryPrefix@ as t3 on t3.network=t1.channel and t1.date=t3.date
    inner join development_mx.A_E_BI_ExchangeRate_USD as dollar on t1.country = dollar.Country and t1.yrmonth = dollar.Month_Num
    inner join development_mx.A_E_BI_ExchangeRate as euro on t1.country = euro.Country and t1.yrmonth = euro.Month_Num

set marketing_cost = case t1.channel
          when 'PampaNetwork' then if(t2.sum_gr>0,(gross_revenue/sum_gr)*commission_total,commission_total/count_channel)*dollar.XR 
          when 'GlobalLeadsGroup' then if(t2.sum_gr>0,(gross_revenue/sum_gr)*commission_total,commission_total/count_channel)*euro.XR
          when 'TradeTracker' then if(t2.sum_gr>0,(gross_revenue/sum_gr)*commission_total,commission_total/count_channel)*euro.XR
          else if(t2.sum_gr>0,(gross_revenue/sum_gr)*commission_total,commission_total/count_channel)
       end 
where medium = 'affiliates' or t1.source = 'affiliates'
;