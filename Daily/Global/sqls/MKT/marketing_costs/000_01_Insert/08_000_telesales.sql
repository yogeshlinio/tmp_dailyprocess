USE @marketing@;

drop temporary table if exists total;
create  temporary table total (index(date,  fk_channel_group, country))
select date, fk_channel_group, country, sum(gross_revenue) rev, count(1) uds
from channel_performance
where channel_group = 'TeleSales'
group by date, fk_channel_group, country;


drop temporary table if exists costos;
create  temporary table costos (index(monthnum, id_channel_group, country))
select country, monthnum, id_channel_group,sum(value)/
day(last_day(cast(concat(SUBSTR(cast(monthnum as char),1,4),'-',
substr(cast(monthnum as char),-2),'-','10') as date))) spent 
from development_mx.M_Costs 
inner join marketing.channel_group
where typeCost='SalesForce'
and channel_group = 'TeleSales'
group by country, monthnum;

update channel_performance cp
inner join 
total t
on cp.date = t.date
and cp.fk_channel_group = t.fk_channel_group
and cp.country = t.country
inner join
costos t2
on cp.yrmonth = t2.monthnum
and cp.fk_channel_group = t2.id_channel_group
and cp.country = t2.country
set cp.marketing_cost =if(t.rev= 0, spent/t.uds,spent*gross_revenue/t.rev);
