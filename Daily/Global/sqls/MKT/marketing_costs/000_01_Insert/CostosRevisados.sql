

-- Mexico
-- Cost
 
 set @country := 'MEX';

update marketing_mx.channel_performance g inner join 
(select @country country, date, sum(commission_total)*er.XR cost from marketing_report.affiliates_mx r 
inner join development_mx.A_E_BI_ExchangeRate er on date_format(date,'%Y%m')=er.month_num where network='TradeTracker' group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'TradeTracker';

update marketing_report.global_report g set cost = (select sum(commission_total) from marketing_report.affiliates_mx r where g.date=r.date and network='TradeTracker') where channel='Trade Tracker' and country='Mexico' and datediff(curdate(), g.date)<@days; 

update marketing_mx.channel_performance g inner join 
(select @country country, date, sum(commission_total)*er.XR cost from marketing_report.affiliates_mx r 
inner join development_mx.A_E_BI_ExchangeRate_USD er on date_format(date,'%Y%m')=er.month_num where network='PampaNetwork' group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'Pampa Network';

update marketing_report.global_report g set cost = (select sum(commission_total) from marketing_report.affiliates_mx r where g.date=r.date and network='PampaNetwork') where channel='Pampa Network' and country='Mexico' and datediff(curdate(), g.date)<@days; 

update marketing_mx.channel_performance g inner join 
(select @country country, date, sum(commission_total)*er.XR cost from marketing_report.affiliates_mx r inner join development_mx.A_E_BI_ExchangeRate er on date_format(date,'%Y%m')=er.month_num where network='GlobalLeadsGroup' group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'Glg';

update marketing_report.global_report g set cost = (select sum(commission_total) from marketing_report.affiliates_mx r where g.date=r.date and network='GlobalLeadsGroup') where channel='Glg' and country='Mexico' and datediff(curdate(), g.date)<@days; 

update marketing_mx.channel_performance g inner join 
(select @country country, date, sum(commission_total) cost from marketing_report.affiliates_mx r where network='Netslave' group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'Netslave';

update marketing_report.global_report g set cost = (select sum(commission_total) from marketing_report.affiliates_mx r where g.date=r.date and network='Netslave') where channel='Netslave' and country='Mexico' and datediff(curdate(), g.date)<@days;

update marketing_mx.channel_performance g inner join 
(select @country country, date, sum(cost) cost from marketing_report.sociomantic r group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'Sociomantic';

update marketing_report.global_report g set cost = (select sum(cost) from marketing_report.sociomantic r where g.date=r.date and country='MEX') where channel='Sociomantic' and country='Mexico' and datediff(curdate(), g.date)<@days;

update marketing_mx.channel_performance g inner join 
(select @country country, date, sum(clicks)*.26 cost from marketing_report.triggit_mx r where network='Netslave' group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'Triggit';

update marketing_report.global_report g set cost = (select sum(clicks)*0.26 from marketing_report.triggit_mx r where g.date=r.report_start_date) where channel='Triggit' and country='Mexico' and datediff(curdate(), g.date)<@days;

update marketing_mx.channel_performance g inner join 
(select @country country, date, sum(priceaftertax-couponvalueaftertax)*0.056*5.69*er.XR from development_mx.A_Master r inner join development_mx.A_E_BI_ExchangeRate_USD er on date_format(date,'%Y%m')=er.month_num where r.orderbeforecan=1 and r.channel='Vizury' group by country,date) t 
on g.date = t.date and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'Vizury';+

update marketing_report.global_report g set cost = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0))*0.056*5.69 from production.tbl_order_detail r where g.date=r.date and r.obc=1 and r.channel='Vizury') where channel='Vizury' and country='Mexico' and datediff(curdate(), g.date)<@days;



#######################################################pendiente
update marketing_mx.channel_performance g inner join 
(select @country country, date, sum(cost)/count cost from marketing_report.offline_mx r where network='Netslave' group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'Triggit';


update marketing_report.global_report g set cost = (select sum(cost)/count from marketing_report.offline_mx t, marketing_report.temporary_offline r where g.date=r.date and t.date=r.date) where channel_group='Offline Marketing' and country='Mexico' and datediff(curdate(), g.date)<@days;

TeleSales, sales de development_mx.M_Costs where typeCost = 'Sales Force';

update marketing_mx.channel_performance g inner join 
(select @country country, date, sum(value)/
day(last_day(cast(concat(SUBSTR(cast(monthnum as char),1,4),'-',substr(cast(monthnum as char),-2),'-','10') as date))) cost 
from development_mx.M_Costs where typeCost='Sales Force') t 
on g.yrmonth = t.monthnum
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel_group = 'TeleSales';

update marketing_report.global_report g set cost = (select sum(cost)/count from marketing_report.tele_sales_mx t, marketing_report.temporary_tele_sales r where g.date=r.date and t.date=r.date) where channel_group='Tele Sales' and datediff(curdate(), g.date)<@days and country='Mexico';








-- Colombia
-- Cost
 set @country := 'COL';

 update marketing_co.channel_performance g inner join 
(select @country country, date, sum(cost)*er.XR cost from marketing_report.criteo r inner join development_mx.A_E_BI_ExchangeRate_USD er on date_format(date,'%Y%m')=er.month_num group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'Criteo';

update marketing_report.global_report g set cost = (select sum(cost) from marketing_report.criteo r where country='COL' and g.date=r.date) where channel='Criteo' and country='Colombia' and datediff(curdate(), g.date)<@days; 


update marketing_co.channel_performance g inner join 
(select @country country, date, sum(cost)*er.XR cost from marketing_report.exoclick r inner join development_mx.A_E_BI_ExchangeRate_USD er on date_format(date,'%Y%m')=er.month_num group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'Exo';

update marketing_report.global_report g set cost = (select sum(cost) from marketing_report.exoclick r where country='Colombia' and g.date=r.date) where channel='Exo' and country='Colombia' and datediff(curdate(), g.date)<@days;

update marketing_co.channel_performance g inner join 
(select @country country, date, sum(clicks)*250 cost from marketing_report.icck r group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'ICCK';

update marketing_report.global_report g set cost = (select sum(clicks)*250 from marketing_report.icck r where country='Colombia' and g.date=r.date) where channel='ICCK' and country='Colombia' and datediff(curdate(), g.date)<@days;

update marketing_co.channel_performance g inner join 
(select @country country, date, sum(commission_total)*er.XR cost from marketing_report.affiliates_co r inner join development_mx.A_E_BI_ExchangeRate er on date_format(date,'%Y%m')=er.month_num where network='TradeTracker' group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'TradeTracker';

update marketing_report.global_report g set cost = (select sum(commission_total) from marketing_report.affiliates_co r where g.date=r.date and network='TradeTracker') where channel='Trade Tracker' and country='Colombia' and datediff(curdate(), g.date)<@days; 

update marketing_co.channel_performance g inner join 
(select @country country, date, sum(commission_total)*er.XR cost from marketing_report.affiliates_co r inner join development_mx.A_E_BI_ExchangeRate_USD er on date_format(date,'%Y%m')=er.month_num where network='PampaNetwork' group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'PampaNetwork';

update marketing_report.global_report g set cost = (select sum(commission_total) from marketing_report.affiliates_co r where g.date=r.date and network='PampaNetwork') where channel='Pampa Network' and country='Colombia' and datediff(curdate(), g.date)<@days; 

update marketing_co.channel_performance g inner join 
(select @country country, date, sum(commission_total) cost from marketing_report.affiliates_co r where network='Netslave' group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'Netslave';

update marketing_report.global_report g set cost = (select sum(commission_total) from marketing_report.affiliates_co r where g.date=r.date and network='Netslave') where channel='Netslave' and country='Colombia' and datediff(curdate(), g.date)<@days;

update marketing_co.channel_performance g inner join 
(select @country country, date, sum(cost) cost from marketing_report.sociomantic r group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'Sociomatic';

update marketing_report.global_report g set cost = (select sum(cost) from marketing_report.sociomantic r where g.date=r.date and country='COL') where channel='Sociomantic' and country='Colombia' and datediff(curdate(), g.date)<@days;


update marketing_co.channel_performance g inner join 
(select @country country, date, sum(clicks)*.26 cost from marketing_report.triggit_co r where network='Netslave' group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'Triggit';

update marketing_report.global_report g set cost = (select sum(clicks)*0.2 from marketing_report.triggit_co r where g.date=r.report_start_date) where channel='Triggit' and country='Colombia' and datediff(curdate(), g.date)<@days;

update marketing_co.channel_performance g inner join 
(select @country country, date, sum(priceaftertax-couponvalueaftertax)*0.056*6.64*er.XR from development_co_project.A_Master r inner join development_mx.A_E_BI_ExchangeRate_USD er on date_format(date,'%Y%m')=er.month_num where r.orderbeforecan=1 and r.channel='Vizury' group by country,date) t 
on g.date = t.date and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'Vizury';

update marketing_report.global_report g set cost = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0))*0.056*6.64 from production_co.tbl_order_detail r where g.date=r.date and r.obc=1 and r.channel='Vizury') where channel='Vizury' and country='Colombia' and datediff(curdate(), g.date)<@days;

##################################################pendiente
update marketing_co.channel_performance g inner join 
(select @country country, date, sum(cost)/count cost from marketing_report.offline_co r where network='Netslave' group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel_group = 'OfflineMarketing';

update marketing_report.global_report g set cost = (select sum(cost)/count from marketing_report.offline_co t, marketing_report.temporary_offline r where g.date=r.date and t.date=r.date) where channel_group='Offline Marketing' and country='Colombia' and datediff(curdate(), date)<@days;

update marketing_co.channel_performance g inner join 
(select @country country, date, sum(value)/
day(last_day(cast(concat(SUBSTR(cast(monthnum as char),1,4),'-',substr(cast(monthnum as char),-2),'-','10') as date))) cost 
from development_co_project.M_Costs where typeCost='Sales Force') t 
on g.yrmonth = t.monthnum
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel_group = 'TeleSales';

update marketing_report.global_report g set cost = (select sum(cost)/count from marketing_report.tele_sales_co t, marketing_report.temporary_tele_sales r where g.date=r.date and t.date=r.date) where channel_group='Tele Sales' and country='Colombia' and datediff(curdate(), date)<@days;




-- Peru
 set @country := 'PER';


-- Cost

 update marketing_pe.channel_performance g inner join 
(select @country country, date, sum(commission_total)*er.XR cost from marketing_report.affiliates_pe r inner join development_pe.A_E_BI_ExchangeRate er on date_format(date,'%Y%m')=er.month_num where network='TradeTracker' group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'TradeTracker';

update marketing_report.global_report g set cost = (select sum(commission_total) from marketing_report.affiliates_pe r where g.date=r.date and network='TradeTracker') where channel='Trade Tracker' and country='Peru' and datediff(curdate(), g.date)<@days; 

 update marketing_pe.channel_performance g inner join 
(select @country country, date, sum(commission_total)*er.XR cost from marketing_report.affiliates_pe r inner join development_pe.A_E_BI_ExchangeRate_USD er on date_format(date,'%Y%m')=er.month_num where network='PampaNetwork' group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'PampaNetwork';

update marketing_report.global_report g set cost = (select sum(commission_total) from marketing_report.affiliates_pe r where g.date=r.date and network='PampaNetwork') where channel='Pampa Network' and country='Peru' and datediff(curdate(), g.date)<@days; 

 update marketing_pe.channel_performance g inner join 
(select @country country, date, sum(commission_total) cost from marketing_report.affiliates_pe r where network='Netslave' group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'Netslave';

update marketing_report.global_report g set cost = (select sum(commission_total) from marketing_report.affiliates_pe r where g.date=r.date and network='Netslave') where channel='Netslave' and country='Peru' and datediff(curdate(), g.date)<@days;

 update marketing_pe.channel_performance g inner join 
(select @country country, date, sum(cost) cost from marketing_report.affiliates_pe r  group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'Sociomantic';

update marketing_report.global_report g set cost = (select sum(cost) from marketing_report.sociomantic r where g.date=r.date and country='PER') where channel='Sociomantic' and country='Peru' and datediff(curdate(), g.date)<@days;

 update marketing_pe.channel_performance g inner join 
(select @country country, date, sum(clicks)*.18 cost from marketing_report.affiliates_pe r  group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'Triggit';

update marketing_report.global_report g set cost =(select sum(clicks)*0.18 from marketing_report.triggit_pe r where g.date=r.report_start_date) where channel='Triggit' and country='Peru' and datediff(curdate(), g.date)<@days;

update marketing_co.channel_performance g inner join 
(select @country country, date, sum(priceaftertax-couponvalueaftertax)*0.056*4.70*er.XR from development_pe.A_Master r inner join development_mx.A_E_BI_ExchangeRate_USD er on date_format(date,'%Y%m')=er.month_num where r.orderbeforecan=1 and r.channel='Vizury' group by country,date) t 
on g.date = t.date and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'Vizury';

update marketing_report.global_report g set cost = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0))*0.056*4.70 from production_pe.tbl_order_detail r where g.date=r.date and r.obc=1 and r.channel='Vizury') where channel='Vizury' and country='Peru' and datediff(curdate(), g.date)<@days;

###########################################33pendiente

update marketing_report.global_report g set cost = (select sum(cost)/count from marketing_report.offline_pe t, marketing_report.temporary_offline r where g.date=r.date and t.date=r.date) where channel_group='Offline Marketing' and country='Peru' and datediff(curdate(), date)<@days;



 update marketing_pe.channel_performance g inner join 
(select @country country, date, sum(value)/
day(last_day(cast(concat(SUBSTR(cast(monthnum as char),1,4),'-',substr(cast(monthnum as char),-2),'-','10') as date))) cost 
from development_pe.M_Costs where typeCost='Sales Force') t 
on g.yrmonth = t.monthnum
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel_group = 'TeleSales';
 

update marketing_report.global_report g set cost = (select sum(cost)/count from marketing_report.tele_sales_pe t, marketing_report.temporary_tele_sales r where g.date=r.date and t.date=r.date) where channel_group='Tele Sales' and country='Peru' and datediff(curdate(), g.date)<@days;






-- Venezuela
 set @country := 'VEN';

-- Cost
 update marketing_ve.channel_performance g inner join 
(select @country country, date, sum(commission_total)*er.XR cost from marketing_report.affiliates_ve r inner join development_pe.A_E_BI_ExchangeRate_USD er on date_format(date,'%Y%m')=er.month_num where network='PampaNetwork' group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'PampaNetwork';


update marketing_report.global_report g set cost = (select sum(commission_total) from marketing_report.affiliates_ve r where g.date=r.date and network='PampaNetwork') where channel='Pampa Network' and country='Venezuela' and datediff(curdate(), g.date)<@days; 

 update marketing_ve.channel_performance g inner join 
(select @country country, date, sum(commission_total) cost from marketing_report.affiliates_ve r  where network='Netslave' group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'Netslave';

update marketing_report.global_report g set cost = (select sum(commission_total) from marketing_report.affiliates_ve r where g.date=r.date and network='Netslave') where channel='Netslave' and country='Venezuela' and datediff(curdate(), g.date)<@days;

 update marketing_ve.channel_performance g inner join 
(select @country country, date, sum(cost)*er.XR cost from marketing_report.affiliates_ve r inner join development_pe.A_E_BI_ExchangeRate er on date_format(date,'%Y%m')=er.month_num where network='TradeTracker' group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'TradeTracker';

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_ve r where channel='Trade Tracker' and g.date=r.date) where channel='Trade Tracker' and country='Venezuela' and datediff(curdate(), g.date)<@days; 

 update marketing_ve.channel_performance g inner join 
(select @country country, date, sum(cost) cost from marketing_report.sociomantic r group by country,date) t 
on g.date = t.date
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'Sociomantic';

update marketing_report.global_report g set cost = (select sum(cost) from marketing_report.sociomantic r where g.date=r.date and country='VEN') where channel='Sociomantic' and country='Venezuela' and datediff(curdate(), g.date)<@days;


update marketing_ve.channel_performance g inner join 
(select @country country, date, sum(priceaftertax-couponvalueaftertax)*0.024*er.XR from development_ve.A_Master r inner join development_mx.A_E_BI_ExchangeRate_USD er on date_format(date,'%Y%m')=er.month_num where r.orderbeforecan=1 and r.channel='Vizury' group by country,date) t 
on g.date = t.date and g.country = t.country
set g.marketing_cost = t.cost
where g.channel = 'Vizury';

update marketing_report.global_report g set cost = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0))*0.024 from production_ve.tbl_order_detail r where g.date=r.date and r.obc=1 and r.channel='Vizury') where channel='Vizury' and country='Venezuela' and datediff(curdate(), g.date)<@days;


###################################3pendiente
update marketing_report.global_report g set cost = (select sum(cost)/count from marketing_report.offline_ve t, marketing_report.temporary_offline r where g.date=r.date and t.date=r.date) where channel_group='Offline Marketing' and country='Venezuela' and datediff(curdate(), date)<@days;



 update marketing_ve.channel_performance g inner join 
(select @country country, date, sum(value)/
day(last_day(cast(concat(SUBSTR(cast(monthnum as char),1,4),'-',substr(cast(monthnum as char),-2),'-','10') as date))) cost 
from development_ve.M_Costs where typeCost='Sales Force') t 
on g.yrmonth = t.monthnum
and g.country = t.country
set g.marketing_cost = t.cost
where g.channel_group = 'TeleSales';
