-- SQL Insertar Crecimiento Mobile Diario
#CREATE  TABLE development_co.daily_growth_mobile
DROP TABLE IF EXISTS marketing.tmp_Date_Cat1_Possibility;

CREATE TABLE `marketing`.`tmp_Date_Cat1_Possibility` 
Select t.country, t.dt, t.cat1
FROM(SELECT distinct 'MEX' as country, c.dt, m.cat1 FROM operations_mx.calendar c, (Select distinct cat1 From development_mx.A_Master_Catalog) m
 union all SELECT distinct 'COL', c.dt, m.cat1 FROM operations_mx.calendar c, (Select distinct cat1 From development_co_project.A_Master_Catalog) m 
 union all SELECT distinct 'PER', c.dt, m.cat1 FROM operations_mx.calendar c, (Select distinct cat1 From development_pe.A_Master_Catalog) m 
 union all SELECT distinct 'VEN', c.dt, m.cat1 FROM operations_mx.calendar c, (Select distinct cat1 From development_ve.A_Master_Catalog) m) t Where t.dt <= CURDATE() - INTERVAL 1 DAY;


TRUNCATE TABLE `marketing`.`daily_growth_mobile`;

Call `marketing`.`Daily_Growth_Mobile`();

DROP TABLE IF EXISTS marketing.tmp_Date_Cat1_Possibility;