use marketing;


#FB FP
update facebook_channel c
join facebook_campaign f
set f.fk_channel = c.fk_channel
where f.campaign_name like c.campaign_query escape '!'
and c.fk_channel = 92;

#FBF FP
update facebook_channel c
join facebook_campaign f
set f.fk_channel = c.fk_channel
where f.campaign_name like c.campaign_query escape '!'
and c.fk_channel = 59;

#FB NF CAC
update facebook_channel c
join facebook_campaign f
set f.fk_channel = c.fk_channel
where f.campaign_name like c.campaign_query escape '!'
and c.fk_channel = 203;

#FB CAC
update facebook_channel c
join facebook_campaign f
set f.fk_channel = c.fk_channel
where f.campaign_name like c.campaign_query escape '!'
and c.fk_channel = 64;

#FBF CAC
update facebook_channel c
join facebook_campaign f
set f.fk_channel = c.fk_channel
where f.campaign_name like c.campaign_query escape '!'
and c.fk_channel = 67;

#FB NF
update facebook_channel c
join facebook_campaign f
set f.fk_channel = c.fk_channel
where f.campaign_name like c.campaign_query escape '!'
and c.fk_channel = 62;

#FBF NF
update facebook_channel c
join facebook_campaign f
set f.fk_channel = c.fk_channel
where f.campaign_name like c.campaign_query escape '!'
and c.fk_channel = 68;

#FB RHS
update facebook_channel c
join facebook_campaign f
set f.fk_channel = c.fk_channel
where f.campaign_name like c.campaign_query escape '!'
and c.fk_channel = 60;

#FB MP
update facebook_channel c
join facebook_campaign f
set f.fk_channel = c.fk_channel
where f.campaign_name like c.campaign_query escape '!'
and c.fk_channel = 66;

#FBF MP
update facebook_channel c
join facebook_campaign f
set f.fk_channel = c.fk_channel
where f.campaign_name like c.campaign_query escape '!'
and c.fk_channel = 69;

#Mobile App
update facebook_channel c
join facebook_campaign f
set f.fk_channel = c.fk_channel
where f.campaign_name like c.campaign_query escape '!'
and c.fk_channel = 105;

#FB EX
update facebook_channel c
join facebook_campaign f
set f.fk_channel = c.fk_channel
where f.campaign_name like c.campaign_query escape '!'
and c.fk_channel = 198;

#FB AFF
update facebook_channel c
join facebook_campaign f
set f.fk_channel = c.fk_channel
where f.campaign_name like c.campaign_query escape '!'
and c.fk_channel = 212;

#FB RHS
update facebook_channel c
join facebook_campaign f
set f.fk_channel = c.fk_channel
where f.fk_channel is null
and c.fk_channel = 60;

update facebook_campaign f
inner join channel c
on fk_channel = id_channel
set f.channel = c.channel;


