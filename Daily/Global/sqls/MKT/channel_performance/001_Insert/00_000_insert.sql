USE @marketing@;

SELECT @v_maxDate:= MAX(date)  - interval 30 day
FROM channel_performance;

delete from channel_performance 
where yrmonth >= date_format(@v_maxDate, '%Y%m');

replace channel_performance
		(country,
		date,
		yrmonth,
		channel_group,
		channel,
		fk_channel,
		fk_channel_group,
		source,
		medium,
		is_fashion,
		campaign,
		ad_group,
		keyword,
		is_mobile,
		profile,
		net_revenue,
		gross_revenue)
select 	
		a.country,
		a.date, 
		a.monthnum, 
		c.channel_group, 
		c.channel, 
		c.fk_channel,
		c.fk_channel_group,
		c.source, 
		c.medium, 
		c.is_fashion,
		c.campaign,
		c.ad_group,
		c.keyword,
		c.is_mobile,
		c.profile,
		sum(a.rev*a.orderAfterCan) as net_revenue,
		sum(a.rev*a.orderBeforeCan) as gross_revenue
from @development@.A_Master a
inner join channel_report c
on orderID = idSalesOrder
where c.fk_channel is not null and a.monthnum >= date_format(@v_maxDate, '%Y%m')
and c.fk_channel_group is not null
group by 	
		a.country,
		a.date, 
		c.channel_group, 
		c.channel,
		c.source, 
		c.medium, 
		c.is_fashion,
		c.campaign,
		c.ad_group,
		c.keyword,
		c.is_mobile,
		c.profile;
	
/*
replace channel_performance
		(country,
		date,
		yrmonth,
		fk_channel,
		fk_channel_group,
		source,
		medium,
		is_fashion,
		campaign,
		ad_group,
		keyword,
		is_mobile,
		profile)
	select 
		c.country,
		c.date, 
		c.yrmonth,  
		c.fk_channel,
		c.fk_channel_group,
		c.source, 
		c.medium, 
		c.is_fashion,
		c.campaign,
		c.ad_group,
		c.keyword,
		c.is_mobile,
		c.profile
from  marketing.ga_performance c
where c.fk_channel is not null and c.date > @v_maxDate
and c.fk_channel_group is not null
and c.country=substring('@v_country@',1,3)
group by 	
		c.country,
		c.date, 
		c.fk_channel,
		c.fk_channel_group,
		c.source, 
		c.medium, 
		c.is_fashion,
		c.campaign,
		c.ad_group,
		c.keyword,
		c.is_mobile,
		c.profile;
	*/	

update channel_performance c 
inner join marketing.channel
on c.fk_channel = id_channel
inner join marketing.channel_group
on c.fk_channel_group = id_channel_group
set c.channel = channel.channel,
	c.channel_group = channel_group.channel_group;
	
update channel_performance
set gross_revenue = 0,
	gross_orders = 0,
	net_revenue = 0,
	net_orders = 0,
	pcone = 0,
	pconepfive = 0,
	pctwo = 0,
	canceled_revenue = 0,
	pending_revenue = 0,
	marketplace_revenue = 0,
	estimated_orders = 0,
	estimated_new_customers = 0,
	estimated_revenue = 0,
	avg_basket_size = 0,
	new_customers = 0,
	new_customers_gross = 0,
	stock_out = 0,
	marketing_cost = 0
where yrmonth  >= date_format(@v_maxDate, '%Y%m');
