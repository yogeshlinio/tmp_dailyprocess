	USE @marketing@;

	drop table if exists tmp_customers;
	create table tmp_customers (UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `fk_channel`,
  `fk_channel_group`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`,
  `is_fashion`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
	select a.date,
			c.country,
			c.fk_channel,
			c.fk_channel_group,
			c.is_fashion,
			c.source,
			c.medium,
			c.campaign,
			c.ad_group,
			c.is_mobile,
			c.keyword,
			c.profile,
			0 as gross_customers,
			0 as net_customers,
			0 as cancelled_customers,
			0 as returned_customers,
			0 as rejected_customers
			from @development@.A_Master a
	inner join channel_report c
	on orderID = idSalesOrder
	where orderbeforecan = 1
	group by	a.date, 
				c.fk_channel, 
				c.fk_channel_group, 
				c.source, 
				c.medium, 
				c.is_fashion,
				c.campaign,
				c.ad_group,
				c.is_mobile,
				c.keyword,
				c.profile;

	update tmp_customers t
	inner join (
	select a.date,
			c.country,
			c.fk_channel,
			c.fk_channel_group,
			c.is_fashion,
			c.source,
			c.medium,
			c.campaign,
			c.ad_group,
			c.is_mobile,
			c.keyword,
			c.profile,
			count(distinct CustomerNum) as gross_customers
			from @development@.A_Master a
	inner join channel_report c
	on orderID = idSalesOrder
	where orderbeforecan = 1 and NewReturningGross = 'NEW'
	group by	a.date, 
				c.fk_channel, 
				c.fk_channel_group, 
				c.source, 
				c.medium, 
				c.is_fashion,
				c.campaign,
				c.ad_group,
				c.is_mobile,
				c.keyword,
				c.profile) r
					USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`)
	set t.gross_customers = r.gross_customers
	;

	update tmp_customers t
	join (select a.date,
			c.country,
			c.fk_channel,
			c.fk_channel_group,
			c.is_fashion,
			c.source,
			c.medium,
			c.campaign,
			c.ad_group,
			c.is_mobile,
			c.keyword,
			c.profile,
			count(distinct CustomerNum) as net_customers
			from @development@.A_Master a
	inner join channel_report c
	on orderID = idSalesOrder
	where orderaftercan = 1 and NewReturningGross = 'NEW'
	group by	a.date, 
				c.fk_channel, 
				c.fk_channel_group, 
				c.source, 
				c.medium, 
				c.is_fashion,
				c.campaign,
				c.ad_group,
				c.is_mobile,
				c.keyword,
				c.profile) r
					USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`)
	set t.net_customers = r.net_customers
	;

	update tmp_customers t
	join (select a.date,
			c.country,
			c.fk_channel,
			c.fk_channel_group,
			c.is_fashion,
			c.source,
			c.medium,
			c.campaign,
			c.ad_group,
			c.is_mobile,
			c.keyword,
			c.profile,
			count(distinct CustomerNum) as cancelled_customers
			from @development@.A_Master a
	inner join channel_report c
	on orderID = idSalesOrder
	where cancelled = 1 and NewReturningGross = 'NEW'
	group by	a.date, 
				c.fk_channel, 
				c.fk_channel_group, 
				c.source, 
				c.medium, 
				c.is_fashion,
				c.campaign,
				c.ad_group,
				c.is_mobile,
				c.keyword,
				c.profile) r
					USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`)
	set t.cancelled_customers = r.cancelled_customers
	;

	update tmp_customers t
	join (select a.date,
			c.country,
			c.fk_channel,
			c.fk_channel_group,
			c.is_fashion,
			c.source,
			c.medium,
			c.campaign,
			c.ad_group,
			c.is_mobile,
			c.keyword,
			c.profile,
			count(distinct CustomerNum) as rejected_customers
			from @development@.A_Master a
	inner join channel_report c
	on orderID = idSalesOrder
	where rejected = 1 and NewReturningGross = 'NEW'
	group by	a.date, 
				c.fk_channel, 
				c.fk_channel_group, 
				c.source, 
				c.medium, 
				c.is_fashion,
				c.campaign,
				c.ad_group,
				c.is_mobile,
				c.keyword,
				c.profile) r
					USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`)
	set t.rejected_customers = r.rejected_customers
	;

	update tmp_customers t
	join (select a.date,
			c.country,
			c.fk_channel,
			c.fk_channel_group,
			c.is_fashion,
			c.source,
			c.medium,
			c.campaign,
			c.ad_group,
			c.is_mobile,
			c.keyword,
			c.profile,
			count(distinct CustomerNum) as returned_customers
			from @development@.A_Master a
	inner join channel_report c
	on orderID = idSalesOrder
	where returns = 1
	group by	a.date, 
				c.fk_channel, 
				c.fk_channel_group, 
				c.source, 
				c.medium, 
				c.is_fashion,
				c.campaign,
				c.ad_group,
				c.is_mobile,
				c.keyword,
				c.profile) r
					USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`)
	set t.returned_customers = r.returned_customers
	;

	drop temporary table if exists tmp_net_gross_customers_rate;
	create temporary table tmp_net_gross_customers_rate (index(dt))
	select  dt, 
			avg(gross_net) net_gross
	from
	(select date,
			sum(net_customers)/sum(gross_customers) gross_net
	from tmp_customers a
	group by date) t
	join
	operations_mx.calendar
	where t.date between dt - interval 42 day and dt - interval 15 day
	and dt < curdate()
	group by dt;

	drop temporary table if exists tmp_estimated_customers_avg;
	create temporary table tmp_estimated_customers_avg (UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `fk_channel`,
  `fk_channel_group`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`,
  `is_fashion`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
	select A_Master.date, c.country, c.channel_group, 
			c.channel, 
			c.fk_channel,
			c.fk_channel_group,
			c.source, 
			c.medium, 
			c.is_fashion,
			c.campaign,
			c.ad_group,
			c.is_mobile,
			c.keyword,
			c.profile,
			((count(distinct CustomerNum)) * net_gross) as gross_customers
	from @development@.A_Master
	inner join tmp_net_gross_customers_rate gr on dt = date
	inner join channel_report c
	on orderID = idSalesOrder
	where orderbeforecan = 1
	group by A_Master.date,
			c.fk_channel,
			c.fk_channel_group,
			c.source, 
			c.medium, 
			c.is_fashion,
			c.campaign,
			c.ad_group,
			c.is_mobile,
			c.keyword,
			c.profile;

	drop temporary table if exists tmp_max_customers;
	create temporary table tmp_max_customers (UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `fk_channel`,
  `fk_channel_group`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`,
  `is_fashion`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
	 select tn.date, 
			tn.country,
			tn.fk_channel,
			tn.fk_channel_group,
			tn.source, 
			tn.medium, 
			tn.is_fashion,
			tn.campaign,
			tn.ad_group,
			tn.is_mobile,
			tn.keyword,
			tn.profile,
			if(tg.gross_customers > new_customers, tg.gross_customers,new_customers) as max_customers
	from channel_performance tn
	inner join tmp_estimated_customers_avg tg
	USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`);


	drop temporary table if exists tmp_estimated_customers_pending;
	create temporary table tmp_estimated_customers_pending (UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `fk_channel`,
  `fk_channel_group`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`,
  `is_fashion`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
	select date, 
			country,
			fk_channel,
			fk_channel_group,
			source, 
			medium, 
			is_fashion,
			campaign,
			ad_group,
			is_mobile,
			keyword,
			profile,
			gross_customers*(1-cancelled_customers/gross_customers - rejected_customers/gross_customers - returned_customers/gross_customers) estimated_customers
	from tmp_customers
	order by date desc
	;

	drop  table if exists tmp_estimated_customers;
	create  table tmp_estimated_customers (UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `fk_channel`,
  `fk_channel_group`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`,
  `is_fashion`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
	select t1.date, 
			t1.country,
			t1.fk_channel,
			t1.fk_channel_group,
			t1.source, 
			t1.medium, 
			t1.is_fashion,
			t1.campaign,
			t1.ad_group,
			t1.is_mobile,
			t1.keyword,
			t1.profile,
			if(estimated_customers < max_customers, estimated_customers, max_customers) estimated_customers
	from tmp_estimated_customers_pending t1
	inner join tmp_max_customers t2
	USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`)
	order by date desc;


	update channel_performance cp
	inner join tmp_estimated_customers t
	USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`)
	set cp.estimated_new_customers = t.estimated_customers;
