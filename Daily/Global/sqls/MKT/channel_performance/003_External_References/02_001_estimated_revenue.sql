USE @marketing@;

#average Net/Gross rate from_28 days to_14 days_before today
drop temporary table if exists tmp_net_gross_rate;
create temporary table tmp_net_gross_rate (index(dt))
select dt, 
		avg(gross_net) net_gross
from
(select a.date,
		sum(rev*orderaftercan)/sum(rev*orderbeforecan) gross_net
from @development@.A_Master a
inner join channel_report c
on orderID = idSalesOrder
group by a.date) t
join
operations_mx.calendar
where t.date between dt - interval 42 day and dt - interval 15 day
and dt < curdate()
group by dt;

#Net revenue
drop temporary table if exists tmp_net_revenue;
create temporary table tmp_net_revenue ( UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `fk_channel`,
  `fk_channel_group`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`,
  `is_fashion`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
select a.country,a.date, fk_channel_group, fk_channel, c.source, medium, is_fashion, c.campaign, ad_group, keyword, is_mobile, profile,
	count(distinct idSalesOrder) net_orders, sum(rev) net_revenue
	from @development@.A_Master a
	inner join channel_report c
	on orderID = idSalesOrder
	where OrderAfterCan = 1
	group by a.country,a.date, fk_channel_group, fk_channel, source, medium, is_fashion, c.campaign, ad_group, keyword, is_mobile, profile
;

		
#Revenue estimado usando el promedio
drop temporary table if exists tmp_estimated_revenue_avg;
create temporary table tmp_estimated_revenue_avg( UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `fk_channel`,
  `fk_channel_group`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`,
  `is_fashion`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
select A_Master.country,A_Master.date, c.channel_group, 
		c.channel, 
		c.fk_channel,
		c.fk_channel_group,
		c.source, 
		c.medium, 
		c.is_fashion,
		c.campaign,
		c.ad_group,
		c.is_mobile,
		c.keyword,
		c.profile,
		(sum(rev) * net_gross) as gross_revenue
from @development@.A_Master
inner join tmp_net_gross_rate gr on dt = date
inner join channel_report c
on orderID = idSalesOrder
where orderbeforecan = 1
group by A_Master.date,
		c.fk_channel,
		c.fk_channel_group,
		c.source, 
		c.medium, 
		c.is_fashion,
		c.campaign,
		c.ad_group,
		c.is_mobile,
		c.keyword,
		c.profile;

		
#Hace el máximos entre el net estimado y el actual net
drop temporary table if exists tmp_max_revenue;
create temporary table tmp_max_revenue ( UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `fk_channel`,
  `fk_channel_group`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`,
  `is_fashion`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
 select tn.country,tn.date, 
		tn.channel_group, 
		tn.channel, 
		tn.fk_channel,
		tn.fk_channel_group,
		tn.source, 
		tn.medium, 
		tn.is_fashion,
		tn.campaign,
		tn.ad_group,
		tn.is_mobile,
		tn.keyword,
		tn.profile,
		if(tg.gross_revenue > net_revenue, tg.gross_revenue,net_revenue) max_revenue,
		net_revenue
from channel_performance tn
inner join tmp_estimated_revenue_avg tg
USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`) ;


#(%Canceled - %Rejected - %Returned - %Vouchers )
drop temporary table if exists tmp_gross_revenue_split;
create temporary table tmp_gross_revenue_split ( UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `fk_channel`,
  `fk_channel_group`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`,
  `is_fashion`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
select A_Master.country,A_Master.date, c.channel_group, 
		c.channel, 
		c.fk_channel,
		c.fk_channel_group,
		c.source, 
		c.medium, 
		c.is_fashion,
		c.campaign,
		c.ad_group,
		c.is_mobile,
		c.keyword,
		c.profile,
		sum(priceaftertax) gross_revenue, sum(priceaftertax*cancelled) as cancelled_revenue,
sum(priceaftertax*rejected) as rejected_revenue, sum(priceaftertax*returns) as returned_revenue,
sum(couponvalueaftertax*orderaftercan) as voucher_value,
sum(priceaftertax*orderaftercan) net_revenue, sum(priceaftertax*pending) as pending_revenue
from @development@.A_Master
inner join channel_report c
on orderID = idSalesOrder
where orderbeforecan = 1
group by A_Master.date,c.channel_group, 
		c.channel, 
		c.fk_channel,
		c.fk_channel_group,
		c.source, 
		c.medium, 
		c.is_fashion,
		c.campaign,
		c.ad_group,
		c.is_mobile,
		c.keyword,
		c.profile;

		
#Gross * ( 1 - %Canceled - %Rejected - %Returned - %Vouchers )		
drop temporary table if exists tmp_estimated_revenue_pending;
create temporary table tmp_estimated_revenue_pending ( UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `fk_channel`,
  `fk_channel_group`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`,
  `is_fashion`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
select country,date, channel_group, 
		channel, 
		fk_channel,
		fk_channel_group,
		source, 
		medium, 
		is_fashion,
		campaign,
		ad_group,
		is_mobile,
		keyword,
		profile,
		gross_revenue*(1-cancelled_revenue/gross_revenue - rejected_revenue/gross_revenue - returned_revenue/gross_revenue - voucher_value/gross_revenue) estimated_revenue
from tmp_gross_revenue_split
order by date desc
;

#Calcula el mínimo de las dos estimaciones
drop  table if exists tmp_estimated_revenue;
create  table tmp_estimated_revenue ( UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `fk_channel`,
  `fk_channel_group`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`,
  `is_fashion`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
select t1.country,t1.date, t1.channel_group, 
		t1.channel, 
		t1.fk_channel,
		t1.fk_channel_group,
		t1.source, 
		t1.medium, 
		t1.is_fashion, 
		t1.campaign,
		t1.ad_group,
		t1.is_mobile,
		t1.keyword,
		t1.profile,
		if(estimated_revenue < max_revenue, estimated_revenue, max_revenue) estimated_revenue
from tmp_estimated_revenue_pending t1
inner join tmp_max_revenue t2
USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`)
order by date desc;


#calcula el estimated revenue
update channel_performance cp
inner join tmp_estimated_revenue t
USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`)
set cp.estimated_revenue = t.estimated_revenue;

