USE @marketing@;

drop table if exists temporary_oos ;
create table temporary_oos (
  UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `fk_channel`,
  `fk_channel_group`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`,
  `is_fashion`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 
 select t.country,
    t.date,
    fk_channel_group,
    fk_channel,
    is_fashion,
	c.campaign,
	ad_group,
	is_mobile,
	keyword,
	profile,
    count(*) as stock_out from
    @development@.A_Master t
        inner join
    @v_wmsprod@.itens_venda i
        inner join
    @v_wmsprod@.status_itens_venda si
        inner join
    channel_report c
where
    t.itemid = i.item_id
        and t.orderbeforecan = 1
        and i.itens_venda_id = si.itens_venda_id
        and OrderId = idSalesOrder
        and si.status in ('Quebrado')
group by t.country , date , fk_channel_group , fk_channel, c.campaign, ad_group, is_mobile, keyword, is_fashion, profile;

update channel_performance g
        inner join
    temporary_oos o 	
	USING (       `country`,
                   `date`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`)
set 
    g.stock_out = o.stock_out;