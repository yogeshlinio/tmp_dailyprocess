	USE @marketing@;

	drop table if exists tmp_customers;
	create table tmp_customers ( UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `fk_channel`,
  `fk_channel_group`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`,
  `is_fashion`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
	select a.date,
			c.country,
			c.fk_channel,
			c.fk_channel_group,
			c.is_fashion,
			c.source,
			c.medium,
			c.campaign,
			c.ad_group,
			c.is_mobile,
			c.keyword,
			c.profile,
			count(distinct CustomerNum) as gross_customers
			from @development@.A_Master a
	inner join channel_report c
	on orderID = idSalesOrder
	where orderbeforecan = 1 and NewReturningGross = 'NEW'
	group by	a.date, 
				c.country,
				c.fk_channel, 
				c.fk_channel_group, 
				c.source, 
				c.medium, 
				c.is_fashion,
				c.campaign,
				c.ad_group,
				c.is_mobile,
				c.keyword,
				c.profile;

				
	drop table if exists tmp_customers_net;
	create table tmp_customers_net ( UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `fk_channel`,
  `fk_channel_group`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`,
  `is_fashion`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
	select a.date,
			c.country,
			c.fk_channel,
			c.fk_channel_group,
			c.is_fashion,
			c.source,
			c.medium,
			c.campaign,
			c.ad_group,
			c.is_mobile,
			c.keyword,
			c.profile,
			count(distinct CustomerNum) as net_customers
			from @development@.A_Master a
	inner join channel_report c
	on orderID = idSalesOrder
	where orderaftercan = 1 and NewReturning = 'NEW'
	group by	a.date, 
				c.country,
				c.fk_channel, 
				c.fk_channel_group, 
				c.source, 
				c.medium, 
				c.is_fashion,
				c.campaign,
				c.ad_group,
				c.is_mobile,
				c.keyword,
				c.profile;

	update channel_performance cp
	inner join tmp_customers t  USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`)
	set cp.new_customers_gross = t.gross_customers;


	update channel_performance cp
	set cp.new_customers = 0;
	
	update channel_performance cp
	inner join tmp_customers_net t USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`)
	
	set cp.new_customers = t.net_customers;

