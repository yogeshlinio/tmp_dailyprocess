drop temporary table if exists temporary_basket ;
create temporary table temporary_basket ( UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `fk_channel`,
  `fk_channel_group`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`,
  `is_fashion`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 
select country,date, fk_channel_group, fk_channel, campaign, ad_group, keyword, is_mobile, source, medium, is_fashion, profile, avg(basket) as avg_basket_size from
    (select 
       a.country, a.date, fk_channel_group, fk_channel, a.OrderNum, c.campaign, ad_group, keyword, is_mobile, c.source, medium, is_fashion, profile, count(*) as basket
    from
        @development@.A_Master a
	inner join
		channel_report c
	on
		OrderID = idSalesOrder
    where
        OrderAfterCan = 1
    group by date , fk_channel_group , fk_channel , a.OrderNum,campaign, ad_group, keyword, is_mobile, c.source, medium, is_fashion, profile) a
group by date, fk_channel_group , fk_channel,campaign, ad_group, keyword, is_mobile, source, medium, is_fashion, profile;


update  channel_performance g
        inner join
		temporary_basket t USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`)
set 
    g.avg_basket_size = t.avg_basket_size;