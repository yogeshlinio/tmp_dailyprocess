USE @marketing@;


drop table if exists tmp_orders;
create table tmp_orders ( UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `fk_channel`,
  `fk_channel_group`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`,
  `is_fashion`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
select 
		a.country,
		a.date,
		c.fk_channel,
		c.fk_channel_group,
		c.is_fashion,
		c.source,
		c.medium,
		c.campaign,
		c.ad_group,
		c.is_mobile,
		c.keyword,
		c.profile,
		count(distinct idsalesorder) as gross_orders,
		0 as net_orders,
		0 as cancelled_orders,
		0 as returned_orders,
		0 as rejected_orders
		from @development@.A_Master a
inner join channel_report c
on orderID = idSalesOrder
where orderbeforecan = 1
group by	
			a.date, 
			c.fk_channel, 
			c.fk_channel_group, 
			c.source, 
			c.medium, 
			c.is_fashion,
			c.campaign,
			c.ad_group,
			c.is_mobile,
			c.keyword,
			c.profile;

update tmp_orders t
join (select a.country,a.date,
		c.fk_channel,
		c.fk_channel_group,
		c.is_fashion,
		c.source,
		c.medium,
		c.campaign,
		c.ad_group,
		c.is_mobile,
		c.keyword,
		c.profile,
		count(distinct idsalesorder) as net_orders
		from @development@.A_Master a
inner join channel_report c
on orderID = idSalesOrder
where orderaftercan = 1
group by	a.date, 
			c.fk_channel, 
			c.fk_channel_group, 
			c.source, 
			c.medium, 
			c.is_fashion,
			c.campaign,
			c.ad_group,
			c.is_mobile,
			c.keyword,
			c.profile) r
			USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`)
set t.net_orders = r.net_orders
;

update tmp_orders t
join (select a.country,a.date,
		c.fk_channel,
		c.fk_channel_group,
		c.is_fashion,
		c.source,
		c.medium,
		c.campaign,
		c.ad_group,
		c.is_mobile,
		c.keyword,
		c.profile,
		count(distinct idsalesorder) as cancelled_orders
		from @development@.A_Master a
inner join channel_report c
on orderID = idSalesOrder
where cancelled = 1
group by	a.date, 
			c.fk_channel, 
			c.fk_channel_group, 
			c.source, 
			c.medium, 
			c.is_fashion,
			c.campaign,
			c.ad_group,
			c.is_mobile,
			c.keyword,
			c.profile) r
				USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`)
set t.cancelled_orders = r.cancelled_orders
;

update tmp_orders t
join (select a.country,a.date,
		c.fk_channel,
		c.fk_channel_group,
		c.is_fashion,
		c.source,
		c.medium,
		c.campaign,
		c.ad_group,
		c.is_mobile,
		c.keyword,
		c.profile,
		count(distinct idsalesorder) as rejected_orders
		from @development@.A_Master a
inner join channel_report c
on orderID = idSalesOrder
where rejected = 1
group by	a.date, 
			c.fk_channel, 
			c.fk_channel_group, 
			c.source, 
			c.medium, 
			c.is_fashion,
			c.campaign,
			c.ad_group,
			c.is_mobile,
			c.keyword,
			c.profile) r
				USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`)
set t.rejected_orders = r.rejected_orders
;

update tmp_orders t
join (select a.country, a.date,
		c.fk_channel,
		c.fk_channel_group,
		c.is_fashion,
		c.source,
		c.medium,
		c.campaign,
		c.ad_group,
		c.is_mobile,
		c.keyword,
		c.profile,
		count(distinct idsalesorder) as returned_orders
		from @development@.A_Master a
inner join channel_report c
on orderID = idSalesOrder
where returns = 1
group by	a.date, 
			c.fk_channel, 
			c.fk_channel_group, 
			c.source, 
			c.medium, 
			c.is_fashion,
			c.campaign,
			c.ad_group,
			c.is_mobile,
			c.keyword,
			c.profile) r
				USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`)
set t.returned_orders = r.returned_orders
;

drop temporary table if exists tmp_net_gross_orders_rate;
create temporary table tmp_net_gross_orders_rate (index(dt))
select  dt, 
		avg(gross_net) net_gross
from
(select date,
		sum(net_orders)/sum(gross_orders) gross_net
from tmp_orders a
group by date) t
join
operations_mx.calendar
where t.date between dt - interval 42 day and dt - interval 15 day
and dt < curdate()
group by dt;

drop temporary table if exists tmp_estimated_orders_avg;
create temporary table tmp_estimated_orders_avg ( UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `fk_channel`,
  `fk_channel_group`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`,
  `is_fashion`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
select A_Master.country,A_Master.date, c.channel_group, 
		c.channel, 
		c.fk_channel,
		c.fk_channel_group,
		c.source, 
		c.medium, 
		c.is_fashion,
		c.campaign,
		c.ad_group,
		c.is_mobile,
		c.keyword,
		c.profile,
		((count(distinct idsalesorder)) * net_gross) as gross_orders
from @development@.A_Master
inner join tmp_net_gross_orders_rate gr on dt = date
inner join channel_report c
on orderID = idSalesOrder
where orderbeforecan = 1
group by A_Master.date,
		c.fk_channel,
		c.fk_channel_group,
		c.source, 
		c.medium, 
		c.is_fashion,
		c.campaign,
		c.ad_group,
		c.is_mobile,
		c.keyword,
		c.profile;

drop temporary table if exists tmp_max_orders;
create temporary table tmp_max_orders( UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `fk_channel`,
  `fk_channel_group`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`,
  `is_fashion`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
 select 
		tn.country,
		tn.date, 
		tn.fk_channel,
		tn.fk_channel_group,
		tn.source, 
		tn.medium, 
		tn.is_fashion,
		tn.campaign,
		tn.ad_group,
		tn.is_mobile,
		tn.keyword,
		tn.profile,
		if(tg.gross_orders > net_orders, tg.gross_orders,net_orders) as max_orders
from channel_performance tn
inner join tmp_estimated_orders_avg tg
USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`);


drop temporary table if exists tmp_estimated_orders_pending;
create temporary table tmp_estimated_orders_pending ( UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `fk_channel`,
  `fk_channel_group`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`,
  `is_fashion`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
select country,
		date, 
		fk_channel,
		fk_channel_group,
		source, 
		medium, 
		is_fashion,
		campaign,
		ad_group,
		is_mobile,
		keyword,
		profile,
		gross_orders*(1-cancelled_orders/gross_orders - rejected_orders/gross_orders - returned_orders/gross_orders) estimated_orders
from tmp_orders
order by date desc
;

drop  table if exists tmp_estimated_orders;
create  table tmp_estimated_orders ( UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `fk_channel`,
  `fk_channel_group`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`,
  `is_fashion`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
select 
		t1.country,
		t1.date, 
		t1.fk_channel,
		t1.fk_channel_group,
		t1.source, 
		t1.medium, 
		t1.is_fashion, 
		t1.campaign,
		t1.ad_group,
		t1.is_mobile,
		t1.keyword,
		t1.profile,
		if(estimated_orders < max_orders, estimated_orders, max_orders) estimated_orders
from tmp_estimated_orders_pending t1
inner join tmp_max_orders t2
USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`)
order by date desc;


update channel_performance cp
inner join tmp_estimated_orders t
USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`)
set cp.estimated_orders = t.estimated_orders;
