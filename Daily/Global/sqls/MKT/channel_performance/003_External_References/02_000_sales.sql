USE @marketing@;

#Net Values
drop temporary table if exists aux_cp_nv;
create temporary table aux_cp_nv ( UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `fk_channel`,
  `fk_channel_group`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`,
  `is_fashion`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
select a.country,a.date, fk_channel_group, fk_channel, c.source, medium, is_fashion, c.campaign, ad_group, keyword, is_mobile, profile,
	count(distinct idSalesOrder) net_orders, sum(rev) net_revenue, sum(pcone) pcone,sum(pconepfive) pconepfive, sum(pctwo) pctwo,
	sum(rev*isMplace) mp_rev
	from @development@.A_Master a
	inner join channel_report c
	on orderID = idSalesOrder
	where OrderAfterCan = 1
	group by a.country,a.date, fk_channel_group, fk_channel, source, medium, is_fashion, c.campaign, ad_group, keyword, is_mobile, profile
;

update channel_performance cp
inner join 
	aux_cp_nv t
	 USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`)
set cp.net_orders = t.net_orders,
	cp.net_revenue = t.net_revenue,
	cp.pcone=t.pcone,
	cp.pconepfive=t.pconepfive,
	cp.pctwo=t.pctwo,
	cp.marketplace_revenue = t.mp_rev;

#Gross Values
drop temporary table if exists aux_cp_nv;
create temporary table aux_cp_nv ( UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `fk_channel`,
  `fk_channel_group`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`,
  `is_fashion`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
select a.country,a.date, fk_channel_group, fk_channel, c.source, medium, is_fashion, c.campaign, ad_group, keyword, is_mobile, profile,
	count(distinct idSalesOrder) gross_orders, sum(rev) gross_revenue, sum(rev*Cancelled) canceled_revenue,sum(rev*pending) pending_revenue
	from @development@.A_Master a
	inner join channel_report c
	on orderID = idSalesOrder
	where OrderBeforeCan = 1
	group by a.country,a.date, fk_channel_group, fk_channel, source, medium, is_fashion, c.campaign, ad_group, keyword, is_mobile, profile
;

update channel_performance cp
inner join 
	aux_cp_nv t
	 USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`)
set cp.gross_revenue = t.gross_revenue,
	cp.canceled_revenue = t.canceled_revenue,
	cp.pending_revenue=t.pending_revenue,
	cp.gross_orders = t.gross_orders;
