USE @marketing@;
drop temporary table if exists aux_visit;
create temporary table aux_visit (
UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`,
  `is_fashion`,
 fk_channel,
 fk_channel_group
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
select country,
          date, 
		  is_fashion, 
		  source, 
		  medium, 
		  campaign, 
		  ad_group, 
		  is_mobile, 
		  keyword, 
		  profile, 
		  fk_channel,
 fk_channel_group,
		  sum(visits) visits
from 
   marketing.ga_performance c 
group by 
   date, 
   country, 
   is_fashion, 
   source, 
   medium, 
   campaign, 
   ad_group, 
   is_mobile, 
   keyword, 
   profile,
   fk_channel,
 fk_channel_group
;


UPDATE            channel_performance g 
       inner join aux_visit t
            USING ( 
                   `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`,
				   fk_channel,
 fk_channel_group)
set 
    g.visits = t.visits

;
