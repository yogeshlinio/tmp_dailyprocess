USE @marketing@;

drop temporary table if exists total;
create  temporary table total (index(date,  fk_channel_group, country))
select date, fk_channel_group, country, sum(gross_revenue) rev, count(1) uds
from channel_performance
where channel_group = 'Offline Marketing'
group by date, fk_channel_group, country;


drop temporary table if exists costos;
create  temporary table costos (index(date, id_channel_group, country))
select country, date,id_channel_group, sum(Value) spent
from development_mx.M_Offline
inner join marketing.channel_group
where channel_group = 'Offline Marketing'
group by country, date;

update channel_performance cp
inner join 
total t
on cp.date = t.date
and cp.fk_channel_group = t.fk_channel_group
and cp.country = t.country
inner join
costos t2
on cp.date = t2.date
and cp.fk_channel_group = t2.id_channel_group
and cp.country = t2.country
set cp.marketing_cost =if(t.rev= 0, spent/t.uds,spent*gross_revenue/t.rev);
