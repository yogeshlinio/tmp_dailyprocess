
USE @marketing@;

set @country := substring('@v_country',1,3);

insert ignore into channel_performance
		(country,
		date,
		yrmonth,
		fk_channel,
		fk_channel_group,
		source,
		medium,
		is_fashion,
		campaign,
		ad_group,
		keyword,
		is_mobile,
		profile,
		visits)
	select 
		c.country,
		c.date, 
		c.yrmonth,  
		c.fk_channel,
		c.fk_channel_group,
		c.source, 
		c.medium, 
		c.is_fashion,
		c.campaign,
		c.ad_group,
		c.keyword,
		c.is_mobile,
		c.profile,
		sum(c.visits) visits
from  marketing.ga_performance c
left join channel_performance cp
USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `fk_channel`,
                   `fk_channel_group`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`,
                   `is_fashion`)
where c.fk_channel is not null 
and c.fk_channel_group is not null
and c.yrmonth >= date_format(curdate() - interval 1 month, '%Y%m')
and c.country = @country
and cp.date is null
group by 	
		c.country,
		c.date, 
		c.fk_channel,
		c.fk_channel_group,
		c.source, 
		c.medium, 
		c.is_fashion,
		c.campaign,
		c.ad_group,
		c.keyword,
		c.is_mobile,
		c.profile;
		
#FB 
insert ignore into channel_performance
	(country, date, yrmonth, source, medium, is_fashion, campaign, fk_channel, 
		fk_channel_group, channel, channel_group,ad_group, keyword, is_mobile, profile)
select f.country,
		f.date,
		date_format(f.date, '%Y%m') yrmonth,
		f.source,
		f.medium,
		f.is_fashion,
		f.campaign_name,
		f.fk_channel,
		f.id_channel_group,
		f.channel,
		f.channel_group,
		'' ad_group,
		'' keyword,
		f.is_mobile,
		f.profile
 from (select country,date, campaign, sum(marketing_cost) marketing_cost
 from channel_performance
 where (source = 'facebook' or source = 'fb')
 group by country,campaign, date) c 
 right join marketing.ga_performance g 
 on g.country = c.country and g.campaign = c.campaign and g.date = c.date 
 right join (select country,date,campaign_name,source,fa.fk_channel,
id_channel_group, channel.channel, channel_group.channel_group, medium, f.is_fashion, profile,is_mobile,sum(spend) spend
 from marketing.facebook_campaign f
 inner join marketing.facebook_account fa
 on f.account_name = fa.account_name
inner join marketing.channel
on fa.fk_channel = id_channel
inner join marketing.channel_group
on channel.fk_channel_group = id_channel_group
 group by country, campaign_name, date) f 
 on f.country = c.country 
and c.campaign = campaign_name 
and c.date = f.date
 where f.country in (@country) 
and date_format(f.date, '%Y%m') >= 201405
 and g.campaign is null;
 
update channel_performance cp
inner join marketing.channel c
on id_channel = fk_channel
set cp.channel =c.channel
where cp.channel is null;

update channel_performance cp
inner join marketing.channel_group c
on id_channel_group = fk_channel_group
set cp.channel_group =c.channel_group
where cp.channel_group is null;