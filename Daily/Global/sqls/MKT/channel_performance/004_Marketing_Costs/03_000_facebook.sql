USE @marketing@;

drop temporary table if exists total;
create  temporary table total (index(date, campaign, country))
select date, campaign,country, sum(gross_revenue) rev, count(1) uds
from channel_performance
where (source = 'facebook' or source = 'fb')
group by date,campaign;

drop temporary table if exists costos;
create  temporary table costos (index(date, campaign_name, country))
select date,usd.country,campaign_name, sum(spend)*XR as spent 
from marketing.facebook_campaign
inner join development_mx.A_E_BI_ExchangeRate_USD usd
on usd.country = facebook_campaign.country
and usd.month_num = date_format(date, '%Y%m')
group by date, campaign_name;

update channel_performance cp
inner join 
total t
on cp.date = t.date
and cp.campaign = t.campaign
and cp.country = t.country
inner join
costos t2
on cp.date = t2.date
and cp.campaign = t2.campaign_name
and cp.country = t2.country
set cp.marketing_cost =if(t.rev= 0, spent/t.uds,spent*gross_revenue/t.rev);
