#newsletter costs
USE @marketing@;
       
update channel_performance as t1
    left join (select date, fk_channel_group,sum(gross_revenue) as sum_gr,count(1) as count_channel 
    from channel_performance
    where channel_group = 'Newsletter'
    group by date
    ) as t2 on t1.fk_channel_group = t2.fk_channel_group and t1.date = t2.date
    inner join (select * from development_mx.M_Other_Cost where type='experian') as t3 
      on t3.country=t1.country and t1.date=t3.date
    inner join development_mx.A_E_BI_ExchangeRate_USD as dollar on t1.country = dollar.Country and t1.yrmonth = dollar.Month_Num
    inner join development_mx.A_E_BI_ExchangeRate as euro on t1.country = euro.Country and t1.yrmonth = euro.Month_Num

set t1.marketing_cost = if(t2.sum_gr>0,(gross_revenue/sum_gr)*value,value/count_channel)*dollar.XR
where channel_group = 'Newsletter'
;