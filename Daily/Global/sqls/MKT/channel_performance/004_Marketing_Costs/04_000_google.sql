USE @marketing@;

set @country := substring('@v_country@',1,3);

#No Linio Fashion
drop temporary table if exists tmp_google;
create temporary table tmp_google
(UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
select ga_performance.country, date,  source, medium, campaign, is_mobile, keyword, ad_group, profile, sum(ad_cost) cost
from marketing.ga_performance
where ad_cost>0  
and profile<>'liniofashion' and campaign not like 'fb.%' and ad_group not like '%|f|%' and campaign not like 'brandf.'
and ga_performance.country = @country
group by date,ga_performance.country,source, medium, campaign, is_mobile, keyword,ad_group,  profile;

drop temporary table if exists tmp_google_rev;
create temporary table tmp_google_rev
(UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
select country, date,  source, medium, campaign,  is_mobile, keyword,ad_group, profile, sum(gross_revenue) rev
, count(1) uds
from channel_performance
where source = 'google'
and profile<>'liniofashion' and campaign not like 'fb.%' and ad_group not like '%|f|%' and campaign not like 'brandf.'
group by date,country,source, medium, campaign, is_mobile,keyword,ad_group,profile;


drop temporary table if exists tmp_cost_rev;
create temporary table tmp_cost_rev
(UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
select t.*, rev, uds
from tmp_google t inner join tmp_google_rev t2
USING (       `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`);

UPDATE channel_performance cp
inner join
tmp_cost_rev as t USING (  `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`)
set cp.marketing_cost=if(t.rev = 0, cost/uds, cost*gross_revenue/t.rev)
;

#Linio Fashion
drop temporary table if exists tmp_google;
create temporary table tmp_google
(UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
select country, date,  source, medium, campaign, is_mobile, keyword,ad_group,  profile, sum(ad_cost) cost
from marketing.ga_performance
where ad_cost>0 
and profile='liniofashion' 
and ga_performance.country = @country
and (campaign like 'fb.%' or ad_group like '%|f|%' or campaign like 'brandf.%')
group by date,country,source, medium, campaign, is_mobile, keyword,ad_group, profile;

drop temporary table if exists tmp_google_rev;
create temporary table tmp_google_rev
(UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
select country, date,  source, medium, campaign,  is_mobile, ad_group,keyword,  profile, sum(gross_revenue) rev
, count(1) uds
from channel_performance
where source = 'google'
and profile='liniofashion'
and (campaign like 'fb.%' or ad_group like '%|f|%' or campaign like 'brandf.%')
group by date,country,source, medium, campaign, is_mobile,ad_group,keyword,  profile;


drop temporary table if exists tmp_cost_rev;
create temporary table tmp_cost_rev
(UNIQUE KEY Unico ( 
  `country`,
  `date`,
  `source`,
  `medium`,
  `is_mobile`,
  `ad_group`,
  `keyword`,
  `campaign`,
  `profile`
) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
select t.*, rev, uds
from tmp_google t inner join tmp_google_rev t2
USING (  `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`);

UPDATE channel_performance cp
inner join
tmp_cost_rev as t USING (  `country`,
                   `date`,
                   `source`,
                   `medium`,
                   `is_mobile`,
                   `ad_group`,
                   `keyword`,
                   `campaign`,
                   `profile`)
set cp.marketing_cost=if(t.rev = 0, cost/uds, cost*gross_revenue/t.rev)
;




