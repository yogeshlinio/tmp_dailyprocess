use @marketing@;

drop temporary table if exists t2;
create temporary table t2 (index(date,channel)) 
select date, channel,sum(gross_revenue) as sum_gr,count(channel) as count_channel 
from channel_performance
where medium = 'affiliates' or source = 'affiliates'
group by channel, date;

drop temporary table if exists t3;
create temporary table t3 (index(date,network))
select date, network, sum(commission_total) commission_total 
from marketing_report.affiliates_@v_countryPrefix@ 
group by date, network;


update channel_performance as t1
inner join t2 
  on t1.channel = t2.channel 
  and t1.date = t2.date
inner join t3 
  on t3.network=t1.channel and t1.date=t3.date
inner join development_mx.A_E_BI_ExchangeRate_USD as dollar 
  on t1.country = dollar.Country  
  and t1.yrmonth = dollar.Month_Num
inner join development_mx.A_E_BI_ExchangeRate as euro 
  on t1.country = euro.Country 
  and t1.yrmonth = euro.Month_Num
set marketing_cost = case t1.channel
          when 'PampaNetwork' then if(t2.sum_gr>0,(gross_revenue/sum_gr)*commission_total,commission_total/count_channel)*dollar.XR 
          when 'GlobalLeadsGroup' then if(t2.sum_gr>0,(gross_revenue/sum_gr)*commission_total,commission_total/count_channel)*euro.XR
          when 'TradeTracker' then if(t2.sum_gr>0,(gross_revenue/sum_gr)*commission_total,commission_total/count_channel)*euro.XR
          else if(t2.sum_gr>0,(gross_revenue/sum_gr)*commission_total,commission_total/count_channel)
       end 
where (medium = 'affiliates' or t1.source = 'affiliates')
;

# Affiliate: Ingenious

drop temporary table if exists t4;
create temporary table t4 (index(date,country))
select date,country, sum(commissions) commission 
from marketing.affiliates_export 
group by date, country;

drop temporary table if exists t5;
create temporary table t5 (index(date)) 
select date, country ,sum(gross_revenue) as sum_gr,count(1) as count_channel 
from channel_performance
where source = 'Ingenious' and medium = 'affiliates' -- and channel = 'Ingenious'
group by date;

update channel_performance as t6
inner join t5  
  on t6.date = t5.date
  and t6.country = t5.country
inner join t4 
  on t4.country=t6.country 
  and t4.date=t6.date
inner join development_mx.A_E_BI_ExchangeRate_USD as dollar 
  on t6.country = dollar.Country  
  and t6.yrmonth = dollar.Month_Num
set marketing_cost = if(t5.sum_gr>0,(t6.gross_revenue/t5.sum_gr)*t4.commission*dollar.XR,t4.commission/t5.count_channel*dollar.XR)
where (source = 'Ingenious' and medium = 'affiliates')
;





