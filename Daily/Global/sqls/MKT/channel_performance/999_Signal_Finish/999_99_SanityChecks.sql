USE @marketing@;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  '@marketing@.channel_performance',
  'finish',
  NOW(),
  MAX(date),
  count(*),
  count(*)
FROM
channel_performance
;
