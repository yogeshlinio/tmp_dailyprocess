USE @marketing@;

UPDATE channel_performance
SET week = date_format(date, '%Y%v'),
yrquarter = concat(year(date), 0,quarter(date))
WHERE week is null;