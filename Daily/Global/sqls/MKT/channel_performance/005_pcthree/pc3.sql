USE @marketing@;

#pc3 (pc2-marketing_costs)
update channel_performance
set pcthree=pctwo-marketing_cost;

delete from channel_performance
where marketing_cost = 0
or visits = 0
or gross_revenue = 0;
