call marketing_@v_countryPrefix@;

INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  '@v_country@',
  'marketing_report.marketing_@v_countryPrefix@',
  NOW(),
  NOW(),
  1,
  1
;
