use marketing;
-- *************** Facebook **************
drop table if exists marketing_costs;
create table marketing_costs as
Select c.country
	, DATE_FORMAT(c.date, '%Y%m') as "yrmonth"
	, cast(c.date as date) date
	, g.channel_group as "channel_group"
	, p.channel
	, sum(c.spend)*XR as "marketing_cost"
from marketing.facebook_campaign c
Inner Join marketing.channel p
On c.fk_channel = p.id_channel
Inner Join marketing.channel_group g
On p.fk_channel_group = g.id_channel_group
inner join development_mx.A_E_BI_ExchangeRate_USD usd
on usd.Month_Num = DATE_FORMAT(c.date, '%Y%m') 
and usd.country = c.country
Where DATE_FORMAT(c.date, '%Y%m') >= 201401
group by c.country, c.date, g.channel_group, p.channel union all



-- *************** Google ************** Ok


Select t.country
	, t.yrmonth
	, cast(t.date as date)
	, t.channel_group
	, t.channel 
	, Sum(marketing_cost) as "marketing_cost"
From 
(
Select  c.country country
	, DATE_FORMAT(c.date, '%Y%m') as "yrmonth"
	, cast(c.date as date) date
	, g.channel_group as "channel_group"
	, p.channel
	, sum(c.ad_cost) as "marketing_cost"
-- Select *
from marketing.ga_performance c 
Inner Join marketing.channel p
On c.fk_channel = p.id_channel
Inner Join marketing.channel_group g
On c.fk_channel_group = g.id_channel_group
where c.ad_cost > 0  
and c.profile <> 'liniofashion'
and DATE_FORMAT(c.date, '%Y%m') >= 201401
group by c.country, c.date, g.channel_group, p.channel
 
Union all

Select c.country country
	, DATE_FORMAT(c.date, '%Y%m') as "yrmonth"
	, cast(c.date as date) date
	, g.channel_group as "channel_group"
	, p.channel
	, sum(c.ad_cost) as "marketing_cost"
-- Select *
from marketing.ga_performance c 
Inner Join marketing.channel p
On c.fk_channel = p.id_channel
Inner Join marketing.channel_group g
On c.fk_channel_group = g.id_channel_group
where c.ad_cost > 0  
and c.profile = 'liniofashion'
and DATE_FORMAT(c.date, '%Y%m') >= 201401
and (c.campaign like 'fb.%' or c.ad_group like '%|f|%' or c.campaign like '%fashion%')
group by c.country, c.date, g.channel_group, p.channel
) t
Where t.country not in('PER', 'VEN')
Group by t.country, t.yrmonth, t.date, t.channel_group, t.channel

UNION ALL

Select t.country
	, t.yrmonth
	, cast(t.date as date) date
	, t.channel_group
	, t.channel 
	, Sum(marketing_cost) as "marketing_cost"
From 
(
Select  c.country country
	, DATE_FORMAT(c.date, '%Y%m') as "yrmonth"
	, c.date
	, g.channel_group as "channel_group"
	, p.channel
	, sum(c.ad_cost) * usd.xr as "marketing_cost"
from marketing.ga_performance c 
inner join development_mx.A_E_BI_ExchangeRate usd
on c.yrmonth = usd.month_num
and c.country = usd.country
Inner Join marketing.channel p
On c.fk_channel = p.id_channel
Inner Join marketing.channel_group g
On c.fk_channel_group = g.id_channel_group
where c.ad_cost > 0 
and c.country in('VEN', 'PER')
and c.profile <> 'liniofashion'
and DATE_FORMAT(c.date, '%Y%m') >= 201401
group by c.country, c.date, g.channel_group, p.channel

Union all

Select  c.country  country
	, DATE_FORMAT(c.date, '%Y%m') as "yrmonth"
	, cast(c.date as date) date
	, g.channel_group as "channel_group"
	, p.channel
	, sum(c.ad_cost) * usd.xr as "marketing_cost"
from marketing.ga_performance c 
inner join development_mx.A_E_BI_ExchangeRate usd
on c.yrmonth = usd.month_num
and c.country = usd.country
Inner Join marketing.channel p
On c.fk_channel = p.id_channel
Inner Join marketing.channel_group g
On c.fk_channel_group = g.id_channel_group
where c.ad_cost > 0 
and c.country in('VEN', 'PER')
and c.profile = 'liniofashion'
and (c.campaign like 'fb.%' or c.ad_group like '%|f|%' or c.campaign like '%fashion%')
and DATE_FORMAT(c.date, '%Y%m') >= 201401
group by c.country, c.date, g.channel_group, p.channel
) t
Group by t.country, t.yrmonth, t.date, t.channel_group, t.channel union all



-- *************** Display **************  Ok

select 'COL'
	, DATE_FORMAT(date, '%Y%m') as "yrmonth"
	, cast(date as date) date
	, 'Display' as "channel_group"
	, 'Exoclick' as "channel"
	, sum(cost) as "marketing_cost"
from marketing_report.exoclick t
Where DATE_FORMAT(t.date, '%Y%m') >= 201401
group by t.country, t.date
Union all
select 'COL'
	, DATE_FORMAT(date, '%Y%m') as "yrmonth"
	, date
	, 'Display' as "channel_group"
	, 'ICCK' as "channel"
	, sum(clicks)*250 as "marketing_cost"
from marketing_report.icck t
Where country= 'Colombia'
and DATE_FORMAT(t.date, '%Y%m') >= 201401
Group  by t.country, t.date union all

-- Display Mx Vacio
-- Ven
select t.country
	, DATE_FORMAT(t.date, '%Y%m') as "yrmonth"
	, t.date
	, 'Display' as "channel_group"
	, type as "channel"
	, sum(Value) * usd.xr as "marketing_cost"
from development_mx.M_Other_Cost t
inner join marketing.channel
on type = channel
inner join development_mx.A_E_BI_ExchangeRate_USD usd
on usd.country = t.country
and usd.Month_Num = date_format(date, '%Y%m')
and DATE_FORMAT(t.date, '%Y%m') >= 201401
group by t.country, date, type union all




-- *************** Newsletter ************** Full


Select moc.country
	, DATE_FORMAT(moc.date, '%Y%m') as "yrmonth"
	, moc.date
	, 'Newsletter' as "channel_group"
	, 'Newsletter' as "channel"
	, sum(value)*dollar.xr as "marketing_cost"
from development_mx.M_Other_Cost moc
inner join development_mx.A_E_BI_ExchangeRate_USD as dollar
on moc.country = dollar.Country and DATE_FORMAT(moc.date,'%Y%m%') = dollar.Month_Num
where type='experian' and DATE_FORMAT(moc.date, '%Y%m') >= 201401
Group by moc.country, date union all





-- *************** OffLine ************** Ok


select country
	, DATE_FORMAT(date, '%Y%m') as "yrmonth"
	, date
	, 'Offline Marketing' as "channel_group"
	, 'Offline Marketing' as "channel"
	, sum(Value) as "marketing_cost"
from development_mx.M_Offline
Where DATE_FORMAT(date, '%Y%m') >= 201401
group by country, date union all




-- *************** Telesales ************** Ok

select country
	, DATE_FORMAT(dt, '%Y%m') as "yrmonth"
	, dt
	, 'TeleSales' as "channel_group"
	, 'TeleSales' as "channel"
	,sum(value)/day(last_day(cast(concat(SUBSTR(cast(monthnum as char),1,4),'-', substr(cast(monthnum as char),-2),'-','10') as date))) as "marketing_cost"
from development_mx.M_Costs 
inner join  operations_mx.calendar
on date_format(dt, '%Y%m')  = monthnum
where typeCost='SalesForce'
and monthnum >= 201401
group by country,dt
union all




-- *************** Affiliates ************** 

Select 'COL'
	, DATE_FORMAT(t1.date, '%Y%m') as "yrmonth"
	, t1.date
	,  'Affiliates' as "channel_group"
	, t1.network as "channel"
	, case t1.network
          when 'PampaNetwork' then commission_total * dollar.XR 
          when 'GlobalLeadsGroup' then commission_total * euro.XR
          when 'TradeTracker' then commission_total * euro.XR
          else commission_total
       end as "marketing_cost"
-- Select * 
From (Select date, network, Sum(commission_total) as commission_total 
			From marketing_report.affiliates_co
			Group by date, network) as t1
    inner join development_mx.A_E_BI_ExchangeRate_USD as dollar on DATE_FORMAT(t1.date, '%Y%m') = dollar.Month_Num and 'COL' = dollar.Country
    inner join development_mx.A_E_BI_ExchangeRate as euro on 'COL' = euro.Country and DATE_FORMAT(t1.date, '%Y%m') = euro.Month_Num
where DATE_FORMAT(t1.date, '%Y%m') >= 201401
Group by t1.date, t1.network

Union all

Select 'MEX'
	, DATE_FORMAT(t1.date, '%Y%m') as "yrmonth"
	, t1.date
	,'Affiliates' as "channel_group"
	, t1.network as "channel"
	, case t1.network
          when 'PampaNetwork' then commission_total * dollar.XR 
          when 'GlobalLeadsGroup' then commission_total * euro.XR
          when 'TradeTracker' then commission_total * euro.XR
          else commission_total
       end as "marketing_cost"
-- Select * 
From (Select date, network, Sum(commission_total) as commission_total 
			From marketing_report.affiliates_mx
			Group by date, network) as t1
    inner join development_mx.A_E_BI_ExchangeRate_USD as dollar on DATE_FORMAT(t1.date, '%Y%m') = dollar.Month_Num and 'MEX' = dollar.Country
    inner join development_mx.A_E_BI_ExchangeRate as euro on 'MEX' = euro.Country and DATE_FORMAT(t1.date, '%Y%m') = euro.Month_Num
where DATE_FORMAT(t1.date, '%Y%m') >= 201401
Group by t1.date, t1.network

Union all

Select 'PER'
	, DATE_FORMAT(t1.date, '%Y%m') as "yrmonth"
	, t1.date
	,'Affiliates' as "channel_group"
	, t1.network as "channel"
	, case t1.network
          when 'PampaNetwork' then commission_total * dollar.XR 
          when 'GlobalLeadsGroup' then commission_total * euro.XR
          when 'TradeTracker' then commission_total * euro.XR
          else commission_total
       end as "marketing_cost"
-- Select * 
From (Select date, network, Sum(commission_total) as commission_total 
			From marketing_report.affiliates_pe
			Group by date, network) as t1
    inner join development_mx.A_E_BI_ExchangeRate_USD as dollar on DATE_FORMAT(t1.date, '%Y%m') = dollar.Month_Num and 'PER' = dollar.Country
    inner join development_mx.A_E_BI_ExchangeRate as euro on 'PER' = euro.Country and DATE_FORMAT(t1.date, '%Y%m') = euro.Month_Num
where DATE_FORMAT(t1.date, '%Y%m') >= 201401
Group by t1.date, t1.network

Union all

Select 'VEN'
	, DATE_FORMAT(t1.date, '%Y%m') as "yrmonth"
	, t1.date
	, 'Affiliates' as "channel_group"
	, t1.network as "channel"
	, case t1.network
          when 'PampaNetwork' then commission_total * dollar.XR 
          when 'GlobalLeadsGroup' then commission_total * euro.XR
          when 'TradeTracker' then commission_total * euro.XR
          else commission_total
       end as "marketing_cost"
-- Select * 
From (Select date, network, Sum(commission_total) as commission_total 
			From marketing_report.affiliates_ve
			Group by date, network) as t1
    inner join development_mx.A_E_BI_ExchangeRate_USD as dollar on DATE_FORMAT(t1.date, '%Y%m') = dollar.Month_Num and 'VEN' = dollar.Country
    inner join development_mx.A_E_BI_ExchangeRate as euro on 'VEN' = euro.Country and DATE_FORMAT(t1.date, '%Y%m') = euro.Month_Num
where DATE_FORMAT(t1.date, '%Y%m') >= 201401
Group by t1.date, t1.network 

UNION ALL


--                   ***************                  Retargeting                   ************** 

-- COL
select a.country
	, DATE_FORMAT(a.date, '%Y%m') as "yrmonth"
	, a.date
	, 'Retargeting' as "channel_group"
	, 'Vizury' as "channel"
	,sum(rev*orderbeforecan-rev*cancelled-ShippingCost)*.08 as "marketing_cost"
from development_co_project.A_Master a
inner join marketing_report.vizury_report b
on a.ordernum=b.order_id
inner join development_mx.A_E_BI_ExchangeRate_USD er 
on date_format(a.date,'%Y%m')=er.month_num and a.country=er.country
where DATE_FORMAT(a.date, '%Y%m') >= 201401
group by a.date
-- EN este query llama  exchange rate pero los valores pienso que estan en moneda local

Union all

select a.country
	, DATE_FORMAT(a.date, '%Y%m') as "yrmonth"
	, a.date
	, 'Retargeting' as "channel_group"
	, 'Sociomantic' as "channel"
	, sum(a.cost) * er.XR as "marketing_cost"
from marketing_report.sociomantic a
inner join marketing.retargeting_campaigns b
on a.campaign=b.campaign_plataforma and a.country=b.country 
inner join development_mx.A_E_BI_ExchangeRate_USD er 
on date_format(a.date,'%Y%m')=er.month_num and a.country=er.country
where fk_channel=170 and a.country='COL' and DATE_FORMAT(a.date, '%Y%m') >= 201401
group by a.date

Union all

select er.country as "country"
	, DATE_FORMAT(report_start_date, '%Y%m') as "yrmonth"
	, report_start_date as "date"
	, 'Retargeting' as "channel_group"
	, 'Triggit' as "channel"
	, (sum(clicks) * cct.cost_click) * er.Xr  as "marketing_cost"
From marketing_report.triggit_co
inner join marketing_report.costo_click_triggit cct
on cct.country= 'COL'
inner join development_mx.A_E_BI_ExchangeRate_USD er 
on date_format(report_start_date,'%Y%m') = er.month_num and 'COL' = er.country
Where DATE_FORMAT(report_start_date, '%Y%m') >= 201401
group by report_start_date

Union all


-- MEX

select a.country
	, DATE_FORMAT(a.date, '%Y%m') as "yrmonth"
	, a.date
	, 'Retargeting' as "channel_group"
	, 'Vizury' as "channel"
	, sum(rev*orderbeforecan-rev*cancelled-ShippingCost)*.08 as "marketing_cost"
from development_mx.A_Master a
inner join marketing_report.vizury_report b
on a.ordernum=b.order_id
inner join development_mx.A_E_BI_ExchangeRate_USD er 
on date_format(a.date,'%Y%m')=er.month_num and a.country=er.country
where DATE_FORMAT(a.date, '%Y%m') >= 201401
group by a.date
-- EN este query llama  exchange rate pero los valores pienso que estan en moneda local

Union all

select a.country
	, DATE_FORMAT(a.date, '%Y%m') as "yrmonth"
	, a.date
	, 'Retargeting' as "channel_group"
	, 'Sociomantic' as "channel"
	, sum(a.cost) * er.XR as "marketing_cost"
from marketing_report.sociomantic a
inner join marketing.retargeting_campaigns b
on a.campaign=b.campaign_plataforma and a.country=b.country 
inner join development_mx.A_E_BI_ExchangeRate_USD er 
on date_format(a.date,'%Y%m')=er.month_num and a.country=er.country
where fk_channel=170 and a.country='MEX' and DATE_FORMAT(a.date, '%Y%m') >= 201401
group by a.date

Union all

select er.country as "country"
	, DATE_FORMAT(report_start_date, '%Y%m') as "yrmonth"
	, report_start_date as "date"
	, 'Retargeting' as "channel_group"
	, 'Triggit' as "channel"
	, (sum(clicks) * cct.cost_click) * er.Xr  as "marketing_cost"
From marketing_report.triggit_mx
inner join marketing_report.costo_click_triggit cct
on cct.country= 'MEX'
inner join development_mx.A_E_BI_ExchangeRate_USD er 
on date_format(report_start_date,'%Y%m') = er.month_num and 'MEX' = er.country
Where DATE_FORMAT(report_start_date, '%Y%m') >= 201401
group by report_start_date

Union all

-- PER

select a.country
	, DATE_FORMAT(a.date, '%Y%m') as "yrmonth"
	, a.date
	, 'Retargeting' as "channel_group"
	, 'Vizury' as "channel"
	,sum(rev*orderbeforecan-rev*cancelled-ShippingCost)*.08 as "marketing_cost"
from development_pe.A_Master a
inner join marketing_report.vizury_report b
on a.ordernum=b.order_id
inner join development_mx.A_E_BI_ExchangeRate_USD er 
on date_format(a.date,'%Y%m')=er.month_num and a.country=er.country
where DATE_FORMAT(a.date, '%Y%m') >= 201401
group by a.date

Union all

select a.country
	, DATE_FORMAT(a.date, '%Y%m') as "yrmonth"
	, a.date
	, 'Retargeting' as "channel_group"
	, 'Sociomantic' as "channel"
	, sum(a.cost) * er.XR as "marketing_cost"
from marketing_report.sociomantic a
inner join marketing.retargeting_campaigns b
on a.campaign=b.campaign_plataforma and a.country=b.country 
inner join development_mx.A_E_BI_ExchangeRate_USD er 
on date_format(a.date,'%Y%m')=er.month_num and a.country=er.country
where fk_channel=170 and a.country='PER' and DATE_FORMAT(a.date, '%Y%m') >= 201401
group by a.date

Union all

select er.country as "country"
	, DATE_FORMAT(report_start_date, '%Y%m') as "yrmonth"
	, report_start_date as "date"
	, 'Retargeting' as "channel_group"
	, 'Triggit' as "channel"
	, (sum(clicks) * cct.cost_click) * er.Xr  as "marketing_cost"
From marketing_report.triggit_pe
inner join marketing_report.costo_click_triggit cct
on cct.country= 'PER'
inner join development_mx.A_E_BI_ExchangeRate_USD er 
on date_format(report_start_date,'%Y%m') = er.month_num and 'PER' = er.country
Where DATE_FORMAT(report_start_date, '%Y%m') >= 201401
group by report_start_date;





