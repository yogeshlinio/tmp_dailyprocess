use marketing;

update channel_performance
SET ecosystem = 'Mobile App'
WHERE is_mobile = 1
AND profile  = 'mobileapp';

update channel_performance
SET ecosystem = 'Mobile Web'
WHERE is_mobile = 1
AND profile  not in ('mobileapp');


update channel_performance
SET ecosystem = 'Desktop Web'
WHERE is_mobile = 0
AND profile  not in ('mobileapp');
