use marketing;

update channel_performance c
inner join development_mx.A_E_BI_ExchangeRate_USD usd
on c.country = usd.country
and c.yrmonth = usd.month_num
set c.gross_revenue = c.gross_revenue/usd.XR,
	c.net_revenue = c.net_revenue/usd.XR,
	c.estimated_revenue = c.estimated_revenue/usd.XR,
	c.attributed_revenue = c.attributed_revenue/usd.XR,
	c.marketing_cost = c.marketing_cost/usd.XR,
	c.pcone = c.pcone/usd.XR,
	c.pconepfive = c.pconepfive/usd.XR,
	c.pctwo = c.pctwo/usd.XR,
	c.pcthree = c.pcthree/usd.XR,
	c.canceled_revenue = c.canceled_revenue/usd.XR,
	c.pending_revenue = c.pending_revenue/usd.XR,
	c.marketplace_revenue = c.marketplace_revenue/usd.XR,
	c.new_customers_PC2 = c.new_customers_PC2/usd.XR,
	c.avg_ticket_size = c.avg_ticket_size/usd.XR;
