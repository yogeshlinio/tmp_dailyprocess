USE marketing;

#Actualiza las transacciones de GA
update ga_transaction gt
inner join marketing.is_fashion ifa
set
	gt.is_fashion = 1
where
	ifa.field = 'campaign'
and	gt.campaign like ifa.value_query;

update ga_transaction gt
inner join marketing.is_fashion ifa
set
	gt.is_fashion = 1
where
	ifa.field = 'source'
and	gt.source like ifa.value_query;

update ga_transaction gt
inner join marketing.is_fashion ifa
set
	gt.is_fashion = 1
where
	ifa.field = 'landing_page'
and	gt.landing_page like ifa.value_query;

#Actualiza las campañas de Facebook
update facebook_campaign gt
inner join marketing.is_fashion ifa
set
	gt.is_fashion = 1
where
	ifa.field = 'campaign'
and	gt.campaign_name like ifa.value_query;

update facebook_campaign gt
inner join marketing.is_fashion ifa
set
	gt.is_fashion = 1
where
	ifa.field = 'account'
and	gt.account_name like ifa.value_query;

#actualiza is_fashion en ga_performance
update ga_performance gt
inner join marketing.is_fashion ifa
set
	gt.is_fashion = 1
where
	ifa.field = 'campaign'
and	gt.campaign like ifa.value_query;

update ga_performance gt
inner join marketing.is_fashion ifa
set
	gt.is_fashion = 1
where
	ifa.field = 'source'
and	gt.source like ifa.value_query;

