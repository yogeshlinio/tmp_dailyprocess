USE marketing;

update ga_performance c
join channel
join channel_group
set c.fk_channel_group = channel.fk_channel_group
where c.fk_channel = channel.id_channel
and channel_group.id_channel_group = channel.fk_channel_group
and channel.id_channel = channel.id_channel;