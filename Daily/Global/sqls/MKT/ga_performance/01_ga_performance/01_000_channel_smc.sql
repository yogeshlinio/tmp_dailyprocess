USE marketing;

update ga_performance c
join smc
join channel 
set c.fk_channel = channel.id_channel
where smc.fk_channel = channel.id_channel
and c.source like smc.source_query escape '!'
and smc.medium is null
and smc.campaign is null;

update ga_performance c
join smc
join channel 
set 
		c.fk_channel = channel.id_channel
where smc.fk_channel = channel.id_channel
and c.medium like smc.medium_query escape '!'
and smc.source is null
and smc.campaign is null;

update ga_performance c
join smc
join channel 
set 
		c.fk_channel = channel.id_channel
where smc.fk_channel = channel.id_channel
and c.campaign like smc.campaign_query escape '!'
and smc.source is null
and smc.medium is null;

update ga_performance c
join smc
join channel 
set 
		c.fk_channel = channel.id_channel
where smc.fk_channel = channel.id_channel
and c.source like smc.source_query escape '!'
and c.medium like smc.medium_query escape '!'
and smc.campaign is null;

update ga_performance c
join smc
join channel 
set 
		c.fk_channel = channel.id_channel
where smc.fk_channel = channel.id_channel
and c.source like smc.source_query escape '!'
and c.campaign like smc.campaign_query escape '!'
and smc.medium is null;

update ga_performance c
join smc
join channel 
set 
		c.fk_channel = channel.id_channel
where smc.fk_channel = channel.id_channel
and c.campaign like smc.campaign_query escape '!'
and c.medium like smc.medium_query escape '!'
and smc.source is null;

update ga_performance c
join smc
join channel 
set 
		c.fk_channel = channel.id_channel
where smc.fk_channel = channel.id_channel
and c.source like smc.source_query escape '!'
and c.medium like smc.medium_query escape '!'
and c.campaign like smc.campaign_query escape '!';

update ga_performance c
set
	fk_channel = 185
where	
	(source is not null 
	OR medium is not null 
	OR campaign is not null)
	and fk_channel is null;
	
update ga_performance c
set
	fk_channel = 182
where	
	(source='' 
	OR medium='' 
	OR campaign='')
	and fk_channel is null;
	
