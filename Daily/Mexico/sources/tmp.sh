#!/bin/bash
echo $1
#Module to load data from BOB/UMS/OMS into dwh
#usage:
#     load.sh <country> <data_source>
v_user=${1}
v_pwd=${2}
v__host=${3}
#echo  "Starting processing ${1}"
v_country=${4}


#absolute path to the linio-git repository
path="../.."
#parse config.ini
. $path/scripts/parser_config.sh
read_ini $path/configChannels.ini

for environment in `cat ./resources/Environments_CRM.txt`
do
echo
echo "--------------------------------------------------------"
	#get the var names according the parameter
	channel_dir="INI__${environment}__DIRECTORY"
	channel_file="INI__${environment}__LOADFILE"
	DB_DATABASE="INI__${environment}__DB_DATABASE"
	DB_TABLE="INI__${environment}__DB_TABLE"
	name="INI__${environment}__NAME"
	type="INI__${environment}__TYPE"

	#shell double referencing
	channel_dir=`eval echo \\$\$channel_dir `
	channel_file=`eval echo \\$\$channel_file`
	DB_TABLE=`eval echo \\$\$DB_TABLE`
	name=`eval echo \\$\$name`
	type=`eval echo \\$\$type`
	DB_DB=`eval echo \\$\$DB_DATABASE`

	channel_dir=`eval echo \$channel_dir `
	channel_file=`eval echo \$channel_file`
	DB_TABLE=`eval echo \$DB_TABLE`
	name=`eval echo \$name`
	type=`eval echo \$type`
	DB_DB=`eval echo \$DB_DB`

	echo "channel_dir	"--$channel_dir
	echo "channel_file	"--$channel_file
	echo "DB_TABLE		"--$DB_TABLE
	echo "v__host		"--$v__host
	echo "DB_DB			"--$DB_DB
	echo "name			"--$name
	echo "type			"--$type
	pwd
	cat ./sqls/CRM/loadData.sql | sed s%@v_dir@%"${channel_dir}"%g>./sqls/CRM/tmp
	cat ./sqls/CRM/tmp  | sed s/@v_file@/${channel_file}/g>./sqls/CRM/tmp1
	cat ./sqls/CRM/tmp1 | sed s/@v_host@/${v__host}/g>./sqls/CRM/tmp2
	cat ./sqls/CRM/tmp2 | sed s/@v_db@/${DB_DB}/g>./sqls/CRM/tmp3
	cat ./sqls/CRM/tmp3 | sed s/@v_tables@/${DB_TABLE}/g>./sqls/CRM/tmp4
	echo "++++"
	var=$(ls $channel_dir/*${type}.${name}*)
	for i in $var
	do
		echo "-------------------"
		echo "Se copia: " $i
		nombre=` echo $i |	awk 'BEGIN { FS = "/" } ; { print $6 }'`
		#awk -F'/' '{print $4}'
		cp $i ./sources/
		echo "El nombre del archivo es::" 
		ls 
		cd sources
		gunzip ${nombre}
		#mysql -u${v_user}  -p${v_pwd} -h $v__host -b $DB_DB <$path/sql/tmp4
		echo "Se ejecuta ---mysql -u"${v_user}" -p"${v_pwd}" -h "${v__host}" -b "${DB_DB}" <" 
		cat ./sqls/CRM/tmp4
		echo "Se Comprime:: " $i
		gzip ${i}
	done
	rm ./sqls/CRM/tmp* 
done
exit
