/*
* MX_032_B2B
*/
 #Query: M1_CostoCorporativo
UPDATE            A_Master_Sample  
       INNER JOIN A_E_M1_Ordenes_Corporativas 
               ON A_Master_Sample.OrderNum = A_E_M1_Ordenes_Corporativas.Order 
SET 
   A_Master_Sample.ShippingFee  = IF( useShippingFee  = 1, A_E_M1_Ordenes_Corporativas.ShippingFee,  A_Master_Sample.ShippingFee  ) , 
   A_Master_Sample.ShippingCost = IF( useShippingCost = 1, A_E_M1_Ordenes_Corporativas.ShippingCost, A_Master_Sample.ShippingCost ) , 
   A_Master_Sample.PaymentFees  = IF( usePaymentFee   = 1, A_E_M1_Ordenes_Corporativas.PaymentFee  , A_Master_Sample.PaymentFees  ) ,
   A_Master_Sample.isB2B        = 1
;