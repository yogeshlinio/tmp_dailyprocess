 #Query: A 103 U Payment Type/Installment
UPDATE               development_mx.A_Master_Sample  
          INNER JOIN bob_live_mx.payment_sales_amex 
                  ON development_mx.A_Master_Sample.idSalesOrder = bob_live_mx.payment_sales_amex.fk_sales_order
SET
   #Out_SalesReportItem.Payment_Type =  bob_live_mx.payment_sales_amex.payment_type,
   development_mx.A_Master_Sample.Installment =   bob_live_mx.payment_sales_amex.installment_months
;


UPDATE              development_mx.A_Master_Sample 
         INNER JOIN bob_live_mx.payment_sales_banortepayworks 
                 ON development_mx.A_Master_Sample.idSalesOrder = bob_live_mx.payment_sales_banortepayworks.fk_sales_order 
SET 
   #Out_SalesReportItem.Payment_Type = bob_live_mx.payment_sales_banorte.payment_type,
   development_mx.A_Master_Sample.Installment  = bob_live_mx.payment_sales_banortepayworks.installment_months;

