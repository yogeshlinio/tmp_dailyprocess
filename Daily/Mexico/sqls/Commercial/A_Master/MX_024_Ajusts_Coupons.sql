/*
*   MX_001_001_Coupons_Visa Subsidios
*/
#Query: A 127 U Actual_Paid_Price/Coupon (Visa Subsidios)
UPDATE (           development_mx.A_Master_Sample
        INNER JOIN development_mx.COM_Visa_Promotion_17_Products 
                ON     (development_mx.A_Master_Sample.SKUConfig  = COM_Visa_Promotion_17_Products.SKU_Config) 
                   AND (development_mx.A_Master_Sample.CouponCode = COM_Visa_Promotion_17_Products.Coupon_Code)) 
SET 
   development_mx.A_Master_Sample.ShippingFee           = development_mx.A_Master_Sample.ShippingFee         + (COM_Visa_Promotion_17_Products.Shipping_Fee_Charged_to_VISA_after_VAT/development_mx.A_Master_Sample.ItemsInOrder)
;

/*
* MX_024 Shipping_COST = 0
*/
 #Query: 2 M1_ShippingCost_0
UPDATE development_mx.A_Master_Sample 
SET 
   development_mx.A_Master_Sample.ShippingCost = 0
WHERE 
    development_mx.A_Master_Sample.CouponCode="MKT0xfgVK" 
 Or development_mx.A_Master_Sample.CouponCode="MKT1eDvI7";

/*
*  MX_008_002 Shipping_Cost Dulces Anahuac
*/
 #Query: A 218 U Dulces Anahuac
UPDATE development_mx.A_Master_Sample
SET 
   development_mx.A_Master_Sample.ShippingCost = 0, 
   development_mx.A_Master_Sample.FLCSCost    = 0
WHERE 
   Left(CouponCode,7)="DEPCHIP";

