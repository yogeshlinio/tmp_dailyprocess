USE development_mx;
UPDATE   A_Master_Sample SET  isMPlace = 0;
DROP TABLE IF EXISTS TMP_MPCat;
CREATE TABLE TMP_MPCat ( INDEX ( MonthNum, ItemId ), INDEX( OrderNum ) )
SELECT
    Date,
    DateDelivered,
    DateReturned,
    MonthNum,
    OrderNum,
    ItemID,
    SKUSimple,
    SKUConfig,
    SKUName,
      A_Master_Sample.Supplier as Supplier,
    HeadBuyer,
    Buyer,
    CatBP,
    CatKPI,
    Cat1,
    Cat2,
    Cat3,
    A_E_BI_Marketplace_Commission.Category as Category_MPlace,
    A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
    A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
    A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
    A_E_BI_Marketplace_Commission.Since,
    A_E_BI_Marketplace_Commission.Fee,
    OriginalPrice,
    PriceAfterTax, 
    CouponValueAfterTax, 
    CostAfterTax, 
    ShippingFee, 
    PCOne,
    Interest,        
    TransactionFeeAfterTax,
    OrderBeforeCan,
    Returns,
    Cancellations,
    Rejected,
    OrderAfterCan,
    PCOnePFive,
    PCOnePFive- FLWHCost- FLCSCost AS PCTwo 

FROM
                   A_Master_Sample 
    INNER JOIN   A_E_BI_Marketplace_Commission 
			ON      idSupplier =   A_E_BI_Marketplace_Commission.id_supplier
WHERE
#    OrderBeforeCan = 1
    IF(  A_E_BI_Marketplace_Commission.Since IS NULL, 1,
       IF( Date >=   A_E_BI_Marketplace_Commission.Since,1,0)    
    )
AND IF(  A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( Date <=   A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND   A_E_BI_Marketplace_Commission.Category = "" 
AND   A_E_BI_Marketplace_Commission.SubCategory = ""
AND   A_E_BI_Marketplace_Commission.SubSubCategory = ""
AND   A_E_BI_Marketplace_Commission.SubSubSubCategory = ""
AND   A_E_BI_Marketplace_Commission.perSKU = ""
Order by supplier
;

REPLACE TMP_MPCat 
SELECT
    Date,
    DateDelivered,
    DateReturned,
    MonthNum,
    OrderNum,
    ItemID,
    SKUSimple,
    SKUConfig,
    SKUName,
      A_Master_Sample.Supplier as Supplier,
    HeadBuyer,
    Buyer,
    CatBP,
    CatKPI,
    Cat1,
    Cat2,
    Cat3,
    A_E_BI_Marketplace_Commission.Category as Category_MPlace,
    A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
    A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
    A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
    A_E_BI_Marketplace_Commission.Since,
    A_E_BI_Marketplace_Commission.Fee,
    OriginalPrice,
    PriceAfterTax, 
    CouponValueAfterTax, 
    CostAfterTax, 
    ShippingFee, 
    PCOne,
    Interest,        
    TransactionFeeAfterTax,
    OrderBeforeCan,
    Returns,
    Cancellations,
    Rejected,
    OrderAfterCan,
    PCOnePFive,
    PCOnePFive- FLWHCost- FLCSCost AS PCTwo 
FROM
                   A_Master_Sample 
    INNER JOIN   A_E_BI_Marketplace_Commission 
            ON      idSupplier =   A_E_BI_Marketplace_Commission.id_supplier
WHERE
#     OrderBeforeCan = 1
    IF(  A_E_BI_Marketplace_Commission.Since IS NULL, 1,
       IF( Date >=   A_E_BI_Marketplace_Commission.Since,1,0)    
    )
AND IF(  A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( Date <=   A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND   A_E_BI_Marketplace_Commission.Category =  CatBP 
AND   A_E_BI_Marketplace_Commission.SubCategory = ""
AND   A_E_BI_Marketplace_Commission.SubSubCategory = ""
AND   A_E_BI_Marketplace_Commission.SubSubSubCategory = ""
AND   A_E_BI_Marketplace_Commission.perSKU = ""
;

REPLACE TMP_MPCat 
SELECT
    Date,
    DateDelivered,
    DateReturned,
    MonthNum,
    OrderNum,
    ItemID,
    SKUSimple,
    SKUConfig,
    SKUName,
      A_Master_Sample.Supplier as Supplier,
    HeadBuyer,
    Buyer,
    CatBP,
    CatKPI,
    Cat1,
    Cat2,
    Cat3,
    A_E_BI_Marketplace_Commission.Category as Category_MPlace,
    A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
    A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
    A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
    A_E_BI_Marketplace_Commission.Since,
    A_E_BI_Marketplace_Commission.Fee,
    OriginalPrice,
    PriceAfterTax, 
    CouponValueAfterTax, 
    CostAfterTax, 
    ShippingFee, 
    PCOne,
    Interest,        
    TransactionFeeAfterTax,
    OrderBeforeCan,
    Returns,
    Cancellations,
    Rejected,
    OrderAfterCan,
    PCOnePFive,
    PCOnePFive- FLWHCost- FLCSCost AS PCTwo 
FROM
                   A_Master_Sample 
    INNER JOIN   A_E_BI_Marketplace_Commission 
            ON      idSupplier =   A_E_BI_Marketplace_Commission.id_supplier
WHERE
#    OrderBeforeCan = 1
    IF(  A_E_BI_Marketplace_Commission.Since IS NULL, 1,
       IF( Date >=   A_E_BI_Marketplace_Commission.Since,1,0)    
    )
AND IF(  A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( Date <=   A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND   A_E_BI_Marketplace_Commission.Category =  CatBP 
AND (     Cat1 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR  Cat2 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR  Cat3 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
     )
AND   A_E_BI_Marketplace_Commission.SubCategory != ""
AND   A_E_BI_Marketplace_Commission.perSKU = ""
;

REPLACE TMP_MPCat 
SELECT
    Date,
    DateDelivered,
    DateReturned,
    MonthNum,
    OrderNum,
    ItemID,
    SKUSimple,
    SKUConfig,
    SKUName,
      A_Master_Sample.Supplier as Supplier,
    HeadBuyer,
    Buyer,
    CatBP,
    CatKPI,
    Cat1,
    Cat2,
    Cat3,
    A_E_BI_Marketplace_Commission.Category as Category_MPlace,
    A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
    A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
    A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
    A_E_BI_Marketplace_Commission.Since,
    A_E_BI_Marketplace_Commission.Fee,
    OriginalPrice,
    PriceAfterTax, 
    CouponValueAfterTax, 
    CostAfterTax, 
    ShippingFee, 
    PCOne,
    Interest,        
    TransactionFeeAfterTax,
    OrderBeforeCan,
    Returns,
    Cancellations,
    Rejected,
    OrderAfterCan,
    PCOnePFive,
    PCOnePFive- FLWHCost- FLCSCost AS PCTwo 
FROM
                   A_Master_Sample 
    INNER JOIN   A_E_BI_Marketplace_Commission 
            ON      idSupplier =   A_E_BI_Marketplace_Commission.id_supplier
WHERE
#    OrderBeforeCan = 1
    IF(  A_E_BI_Marketplace_Commission.Since IS NULL, 1,
       IF( Date >=   A_E_BI_Marketplace_Commission.Since,1,0)    
    )
AND IF(  A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( Date <=   A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND   A_E_BI_Marketplace_Commission.Category =  CatBP 
AND (     Cat1 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR  Cat2 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
OR  Cat3 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
     )
AND (     Cat1 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
      OR  Cat2 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
OR  Cat3 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
     )
AND   A_E_BI_Marketplace_Commission.SubSubCategory != ""
AND   A_E_BI_Marketplace_Commission.perSKU = ""
;

REPLACE TMP_MPCat 
SELECT
    Date,
    DateDelivered,
    DateReturned,
    MonthNum,
    OrderNum,
    ItemID,
    SKUSimple,
    SKUConfig,
    SKUName,
      A_Master_Sample.Supplier as Supplier,
    HeadBuyer,
    Buyer,
    CatBP,
    CatKPI,
    Cat1,
    Cat2,
    Cat3,
    A_E_BI_Marketplace_Commission.Category as Category_MPlace,
    A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
    A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
    A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
    A_E_BI_Marketplace_Commission.Since,
    A_E_BI_Marketplace_Commission.Fee,
    OriginalPrice,
    PriceAfterTax, 
    CouponValueAfterTax, 
    CostAfterTax, 
    ShippingFee, 
    PCOne,
    Interest,        
    TransactionFeeAfterTax,
    OrderBeforeCan,
    Returns,
    Cancellations,
    Rejected,
    OrderAfterCan,
    PCOnePFive,
    PCOnePFive- FLWHCost- FLCSCost AS PCTwo 
FROM
                   A_Master_Sample 
    INNER JOIN   A_E_BI_Marketplace_Commission 
            ON      idSupplier =   A_E_BI_Marketplace_Commission.id_supplier
WHERE
#    OrderBeforeCan = 1
    IF(  A_E_BI_Marketplace_Commission.Since IS NULL, 1,
       IF( Date >=   A_E_BI_Marketplace_Commission.Since,1,0)    
    )
AND IF(  A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( Date <=   A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND   A_E_BI_Marketplace_Commission.Category =  CatBP 
AND (     Cat1 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR  Cat2 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
OR  Cat3 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
     )
AND (     Cat1 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
      OR  Cat2 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
OR  Cat3 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
     )
AND (     Cat1 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubSubSubCategory, "%" ) 
      OR  Cat2 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubSubSubCategory, "%" ) 
OR  Cat3 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubSubSubCategory, "%" ) 
     )
AND   A_E_BI_Marketplace_Commission.SubSubSubCategory != ""
AND   A_E_BI_Marketplace_Commission.perSKU = ""
;

/*
*  Per SKU
*/
REPLACE TMP_MPCat 
SELECT
    Date,
    DateDelivered,
    DateReturned,
    MonthNum,
    OrderNum,
    ItemID,
    SKUSimple,
    SKUConfig,
    SKUName,
      A_Master_Sample.Supplier as Supplier,
    HeadBuyer,
    Buyer,
    CatBP,
    CatKPI,
    Cat1,
    Cat2,
    Cat3,
    A_E_BI_Marketplace_Commission.Category as Category_MPlace,
    A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
    A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
    A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
    A_E_BI_Marketplace_Commission.Since,
    A_E_BI_Marketplace_Commission.Fee,
    OriginalPrice,
    PriceAfterTax, 
    CouponValueAfterTax, 
    CostAfterTax, 
    ShippingFee, 
    PCOne,
    Interest,        
    TransactionFeeAfterTax,
    OrderBeforeCan,
    Returns,
    Cancellations,
    Rejected,
    OrderAfterCan,
    PCOnePFive,
    PCOnePFive- FLWHCost- FLCSCost AS PCTwo 
FROM
                   A_Master_Sample 
    INNER JOIN   A_E_BI_Marketplace_Commission 
            ON      idSupplier =   A_E_BI_Marketplace_Commission.id_supplier
WHERE
#     OrderBeforeCan = 1
    IF(  A_E_BI_Marketplace_Commission.Since IS NULL, 1,
       IF( Date >=   A_E_BI_Marketplace_Commission.Since,1,0)    
    )
AND IF(  A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( Date <=   A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND   A_E_BI_Marketplace_Commission.Category = ""
AND   A_E_BI_Marketplace_Commission.SubCategory = ""
AND   A_E_BI_Marketplace_Commission.SubSubCategory = ""
AND   A_E_BI_Marketplace_Commission.SubSubSubCategory = ""
AND   A_E_BI_Marketplace_Commission.perSKU != ""
AND  SKUName like concat( "%" , replace( perSKU , " " , "%" ) , "%" )
;

REPLACE TMP_MPCat 
SELECT
    Date,
    DateDelivered,
    DateReturned,
    MonthNum,
    OrderNum,
    ItemID,
    SKUSimple,
    SKUConfig,
    SKUName,
      A_Master_Sample.Supplier as Supplier,
    HeadBuyer,
    Buyer,
    CatBP,
    CatKPI,
    Cat1,
    Cat2,
    Cat3,
    A_E_BI_Marketplace_Commission.Category as Category_MPlace,
    A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
    A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
    A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
    A_E_BI_Marketplace_Commission.Since,
    A_E_BI_Marketplace_Commission.Fee,
    OriginalPrice,
    PriceAfterTax, 
    CouponValueAfterTax, 
    CostAfterTax, 
    ShippingFee, 
    PCOne,
    Interest,        
    TransactionFeeAfterTax,
    OrderBeforeCan,
    Returns,
    Cancellations,
    Rejected,
    OrderAfterCan,
    PCOnePFive,
    PCOnePFive- FLWHCost- FLCSCost AS PCTwo 
FROM
                   A_Master_Sample 
    INNER JOIN   A_E_BI_Marketplace_Commission 
            ON      idSupplier =   A_E_BI_Marketplace_Commission.id_supplier
WHERE
#     OrderBeforeCan = 1
    IF(  A_E_BI_Marketplace_Commission.Since IS NULL, 1,
       IF( Date >=   A_E_BI_Marketplace_Commission.Since,1,0)    
    )
AND IF(  A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( Date <=   A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND   A_E_BI_Marketplace_Commission.Category =  CatBP 
AND   A_E_BI_Marketplace_Commission.SubCategory = ""
AND   A_E_BI_Marketplace_Commission.SubSubCategory = ""
AND   A_E_BI_Marketplace_Commission.SubSubSubCategory = ""
AND   A_E_BI_Marketplace_Commission.perSKU != ""
AND  SKUName like concat( "%" , replace( perSKU , " " , "%" ) , "%" )
;

REPLACE TMP_MPCat 
SELECT
    Date,
    DateDelivered,
    DateReturned,
    MonthNum,
    OrderNum,
    ItemID,
    SKUSimple,
    SKUConfig,
    SKUName,
      A_Master_Sample.Supplier as Supplier,
    HeadBuyer,
    Buyer,
    CatBP,
    CatKPI,
    Cat1,
    Cat2,
    Cat3,
    A_E_BI_Marketplace_Commission.Category as Category_MPlace,
    A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
    A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
    A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
    A_E_BI_Marketplace_Commission.Since,
    A_E_BI_Marketplace_Commission.Fee,
    OriginalPrice,
    PriceAfterTax, 
    CouponValueAfterTax, 
    CostAfterTax, 
    ShippingFee, 
    PCOne,
    Interest,        
    TransactionFeeAfterTax,
    OrderBeforeCan,
    Returns,
    Cancellations,
    Rejected,
    OrderAfterCan,
    PCOnePFive,
    PCOnePFive- FLWHCost- FLCSCost AS PCTwo 
FROM
                   A_Master_Sample 
    INNER JOIN   A_E_BI_Marketplace_Commission 
            ON      idSupplier =   A_E_BI_Marketplace_Commission.id_supplier
WHERE
#    OrderBeforeCan = 1
    IF(  A_E_BI_Marketplace_Commission.Since IS NULL, 1,
       IF( Date >=   A_E_BI_Marketplace_Commission.Since,1,0)    
    )
AND IF(  A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( Date <=   A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND   A_E_BI_Marketplace_Commission.Category =  CatBP 
AND (     Cat1 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR  Cat2 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
OR  Cat3 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
     )
AND   A_E_BI_Marketplace_Commission.SubCategory != ""
AND   A_E_BI_Marketplace_Commission.perSKU != ""
AND  SKUName like concat( "%" , replace( perSKU , " " , "%" ) , "%" )
;

REPLACE TMP_MPCat 
SELECT
    Date,
    DateDelivered,
    DateReturned,
    MonthNum,
    OrderNum,
    ItemID,
    SKUSimple,
    SKUConfig,
    SKUName,
      A_Master_Sample.Supplier as Supplier,
    HeadBuyer,
    Buyer,
    CatBP,
    CatKPI,
    Cat1,
    Cat2,
    Cat3,
    A_E_BI_Marketplace_Commission.Category as Category_MPlace,
    A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
    A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
    A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
    A_E_BI_Marketplace_Commission.Since,
    A_E_BI_Marketplace_Commission.Fee,
    OriginalPrice,
    PriceAfterTax, 
    CouponValueAfterTax, 
    CostAfterTax, 
    ShippingFee, 
    PCOne,
    Interest,        
    TransactionFeeAfterTax,
    OrderBeforeCan,
    Returns,
    Cancellations,
    Rejected,
    OrderAfterCan,
    PCOnePFive,
    PCOnePFive- FLWHCost- FLCSCost AS PCTwo 
FROM
                   A_Master_Sample 
    INNER JOIN   A_E_BI_Marketplace_Commission 
            ON      idSupplier =   A_E_BI_Marketplace_Commission.id_supplier
WHERE
#    OrderBeforeCan = 1
    IF(  A_E_BI_Marketplace_Commission.Since IS NULL, 1,
       IF( Date >=   A_E_BI_Marketplace_Commission.Since,1,0)    
    )
AND IF(  A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( Date <=   A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND   A_E_BI_Marketplace_Commission.Category =  CatBP 
AND (     Cat1 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR  Cat2 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
OR  Cat3 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
     )
AND (     Cat1 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
      OR  Cat2 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
OR  Cat3 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
     )
AND   A_E_BI_Marketplace_Commission.SubSubCategory != ""
AND   A_E_BI_Marketplace_Commission.perSKU != ""
AND  SKUName like concat( "%" , replace( perSKU , " " , "%" ) , "%" )
;

REPLACE TMP_MPCat 
SELECT
    Date,
    DateDelivered,
    DateReturned,
    MonthNum,
    OrderNum,
    ItemID,
    SKUSimple,
    SKUConfig,
    SKUName,
      A_Master_Sample.Supplier as Supplier,
    HeadBuyer,
    Buyer,
    CatBP,
    CatKPI,
    Cat1,
    Cat2,
    Cat3,
    A_E_BI_Marketplace_Commission.Category as Category_MPlace,
    A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
    A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
    A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
    A_E_BI_Marketplace_Commission.Since,
    A_E_BI_Marketplace_Commission.Fee,
    OriginalPrice,
    PriceAfterTax, 
    CouponValueAfterTax, 
    CostAfterTax, 
    ShippingFee, 
    PCOne,
    Interest,        
    TransactionFeeAfterTax,
    OrderBeforeCan,
    Returns,
    Cancellations,
    Rejected,
    OrderAfterCan,
    PCOnePFive,
    PCOnePFive- FLWHCost- FLCSCost AS PCTwo 
FROM
                   A_Master_Sample 
    INNER JOIN   A_E_BI_Marketplace_Commission 
            ON      idSupplier =   A_E_BI_Marketplace_Commission.id_supplier
WHERE
    #OrderBeforeCan = 1
    IF(  A_E_BI_Marketplace_Commission.Since IS NULL, 1,
       IF( Date >=   A_E_BI_Marketplace_Commission.Since,1,0)    
    )
AND IF(  A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( Date <=   A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND   A_E_BI_Marketplace_Commission.Category =  CatBP 
AND (     Cat1 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR  Cat2 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
OR  Cat3 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
     )
AND (     Cat1 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
      OR  Cat2 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
OR  Cat3 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
     )
AND (     Cat1 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubSubSubCategory, "%" ) 
      OR  Cat2 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubSubSubCategory, "%" ) 
OR  Cat3 like CONCAT( "%",  A_E_BI_Marketplace_Commission.SubSubSubCategory, "%" ) 
     )
AND   A_E_BI_Marketplace_Commission.SubSubSubCategory != ""
AND   A_E_BI_Marketplace_Commission.perSKU != ""
AND  SKUName like concat( "%" , replace( perSKU , " " , "%" ) , "%" )
;

/*
*  Inventory
*/
DROP TEMPORARY TABLE IF EXISTS   TMP_out_order_tracking;
CREATE TEMPORARY TABLE   TMP_out_order_tracking ( KEY ( item_id ) )
SELECT
    CAST(  item_id AS DECIMAL ) AS item_id 
FROM
   operations_mx.out_order_tracking
WHERE
   fulfillment_type_real = "inventory"
;

/*
DELETE FROM TMP_MPCat 
WHERE 
    ItemId in ( 
                SELECT *  FROM   TMP_out_order_tracking
              )
AND Supplier not in ( select Bob_Supplier_Name from   A_E_BI_Marketplace_Commission WHERE ownWarehouse = 1 )
;
*/


DROP TEMPORARY TABLE IF EXISTS TMP_Track;
CREATE TEMPORARY TABLE TMP_Track ( PRIMARY KEY ( item_id ) )
SELECT  
  CAST( item_id as DECIMAL) AS  item_id,
  fulfillment_type_real  
FROM operations_mx.out_order_tracking
GROUP BY item_id

;
   

/*
*
*
*
*/
DROP TABLE IF EXISTS M1_Market_Place;
CREATE TABLE M1_Market_Place  ( PRIMARY KEY ( ItemId ) )
SELECT 
      TMP_MPCat.MonthNum   AS Month_Num,
      TMP_MPCat.ItemId AS ItemID, 
      TMP_MPCat.Date, 
      TMP_MPCat.DateDelivered,
      TMP_MPCat.DateReturned,
      TMP_MPCat.OrderNum  as OrderNum, 
      TMP_MPCat.CatBP AS Category_BP, 
      TMP_MPCat.CatKPI,
      TMP_MPCat.Supplier,
      TMP_MPCat.HeadBuyer,
      TMP_MPCat.Buyer,
      TMP_MPCat.SKUSimple AS SKU_Simple, 
      TMP_MPCat.SKUName AS SKU_Name, 
      TMP_MPCat.OriginalPrice,
      TMP_MPCat.PriceAfterTax as Price_After_Tax, 
      TMP_MPCat.CouponValueAfterTax as after_vat_coupon, 
      TMP_MPCat.CostAfterTax as cost_after_tax, 
      COALESCE( TMP_MPCat.ShippingFee , 0 ) AS Shipping_Fee_Charged, 
      TMP_MPCat.PCOne,
        TMP_MPCat.PriceAfterTax 
      - TMP_MPCat.CouponValueAfterTax 
      + COALESCE( TMP_MPCat.ShippingFee , 0 )
      + TMP_MPCat.Interest
       as Rev,
      TMP_MPCat.PriceAfterTax * TMP_MPCat.Fee as Commission,
      TMP_MPCat.Fee,
      A_E_BI_ExchangeRate.XR,
      TMP_MPCat.TransactionFeeAfterTax as Transaction_fee_after_tax,

      TMP_MPCat.PCOnePFive,
      TMP_MPCat.PCTwo ,
      space(15) AS fulfillment_type_real ,
      TMP_MPCat.OrderBeforeCan,
      TMP_MPCat.Returns,
      TMP_MPCat.Cancellations,
      TMP_MPCat.Rejected,
      TMP_MPCat.OrderAfterCan

FROM            TMP_MPCat
     INNER JOIN A_E_BI_ExchangeRate
             ON TMP_MPCat.MonthNum= A_E_BI_ExchangeRate.Month_Num 
 
WHERE 
#       TMP_MPCat.OrderBeforeCan=1 
       TMP_MPCat.Date>="2013/5/31" 
   AND IF( TMP_MPCat.Since IS NULL , 1, IF( TMP_MPCat.Date >=  TMP_MPCat.Since , 1, 0 ) )
   AND A_E_BI_ExchangeRate.Country = "MEX"
GROUP BY 
    TMP_MPCat.ItemID, 
    TMP_MPCat.Date, 
    TMP_MPCat.OrderNum, 
    TMP_MPCat.CatBP, 
    TMP_MPCat.SKUSimple, 
    TMP_MPCat.SKUName, 
    TMP_MPCat.PriceAfterTax, 
    TMP_MPCat.CouponValueAfterTax, 
    TMP_MPCat.CostAfterTax, 
    TMP_MPCat.ShippingFee, 
    TMP_MPCat.PCOne;

UPDATE        M1_Market_Place
#   INNER JOIN production.out_order_tracking
    INNER JOIN TMP_Track 
           ON M1_Market_Place.ItemId = TMP_Track.item_id
SET
   M1_Market_Place.fulfillment_type_real = TMP_Track.fulfillment_type_real
;


UPDATE         M1_Market_Place
    INNER JOIN A_Master_Sample
	     USING ( ItemId )
SET 
    isMPlace      = 1,
    MPlaceFee     = Fee,
	A_Master_Sample.CostAfterTax = A_Master_Sample.PriceAfterTax - Commission,
	A_Master_Sample.Cost         = ( A_Master_Sample.PriceAfterTax - Commission ) * ( 1 + ( taxPercent / 100 ) )
;