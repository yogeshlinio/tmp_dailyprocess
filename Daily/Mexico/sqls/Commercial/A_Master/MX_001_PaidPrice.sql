/*
* MX_001_001 Paid Price CiberSanta
*/
 #Query: A 217 U Ciber Santa
UPDATE            development_mx.A_Master_Sample
       INNER JOIN A_E_Ciber_Santa 
               ON      development_mx.A_Master_Sample.SKUConfig = development_mx.A_E_Ciber_Santa.SKU
                  AND  date_format( development_mx.A_Master_Sample.Date , "%Y-%m-%d" ) = development_mx.A_E_Ciber_Santa.Date

SET 
   development_mx.A_Master_Sample.PaidPrice            =   development_mx.A_Master_Sample.Price - A_E_Ciber_Santa.Subsidio,
   development_mx.A_Master_Sample.PaidPriceAfterTax    = ( development_mx.A_Master_Sample.Price - A_E_Ciber_Santa.Subsidio) * 0.86
;