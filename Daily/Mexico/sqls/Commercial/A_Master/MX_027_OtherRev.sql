/*
*  MX_018_001 A_OtherRev
*/
DROP   TEMPORARY TABLE IF EXISTS development_mx.A_OtherRev;
CREATE TEMPORARY TABLE development_mx.A_OtherRev ( INDEX ( MonthNum  ) )
SELECT
 date_format( date , "%Y%m") AS MonthNum,
 sum( value ) AS OtherRev,
 0 as MonthlyNetItems
FROM
  M_Other_Revenue
WHERE
   Country = "MEX"
GROUP BY date_format(  date, "%Y%m" ) 
;

UPDATE       development_mx.A_OtherRev
  INNER JOIN ( SELECT MonthNum, count(*) AS Items FROM development_mx.A_Master_Sample
               WHERE OrderAfterCan = 1 GROUP BY MonthNum ) AS TMP
     USING  ( MonthNum )
SET
   MonthlyNetItems = Items
;

UPDATE           development_mx.A_OtherRev
      INNER JOIN development_mx.A_Master_Sample
           USING ( MonthNum ) 
SET
    development_mx.A_Master_Sample.OtherRev = ( A_OtherRev.OtherRev * FactorMonth( now() - INTERVAL 1 DAY, MonthNum ) ) / MonthlyNetItems 
WHERE OrderAfterCan = 1
;


