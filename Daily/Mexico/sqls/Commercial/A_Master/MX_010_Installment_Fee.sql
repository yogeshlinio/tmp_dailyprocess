-- F110 U
DROP TEMPORARY TABLE IF EXISTS A_Installments_Fees_TMP;
CREATE TEMPORARY TABLE A_Installments_Fees_TMP ( KEY ( OrderNum ) , KEY( id_sales_order_item ) )
SELECT
    development_mx.A_Master_Sample.OrderNum,
	development_mx.A_Master_Sample.ItemID as id_sales_order_item,
	ExtractValue( soal.message,'/transaction/paymentInformation/cardNumber') AS card_number ,
		0 as bin,
		space(50) as issuing_bank,
    Installment,
    0000.000000 AS Fee,
    GrandTotal,
    PriceAfterTax
FROM
               development_mx.A_Master_Sample
    INNER JOIN bob_live_mx.sales_order_item soi on development_mx.A_Master_Sample.ItemID = soi.id_sales_order_item
    INNER JOIN bob_live_mx.sales_order_fraudcheck_log soal on soi.fk_sales_order = soal.fk_sales_order
WHERE title = 'transaction sent'
;

UPDATE A_Installments_Fees_TMP 
SET
    issuing_bank  = "PROSA"
;

-- F116 U
UPDATE A_Installments_Fees_TMP
   SET bin = left(card_number,6);
 
-- F117 U
UPDATE            A_Installments_Fees_TMP 
       INNER JOIN operations_mx.Pro_catalog_card_bins ccb 
               ON A_Installments_Fees_TMP.bin = ccb.bin
SET issuing_bank = ccb.bank;

UPDATE           A_Installments_Fees_TMP 
      INNER JOIN A_E_BI_Bank_MSI 
              ON     A_Installments_Fees_TMP.Installment  = A_E_BI_Bank_MSI.MSI 
                 AND A_Installments_Fees_TMP.issuing_bank = A_E_BI_Bank_MSI.issuing_bank
SET
    A_Installments_Fees_TMP.Fee = A_E_BI_Bank_MSI.FEE;


UPDATE           A_Installments_Fees_TMP 
      INNER JOIN A_E_BI_Bank_MSI 
              ON     A_Installments_Fees_TMP.Installment  = A_E_BI_Bank_MSI.MSI 
                 AND A_E_BI_Bank_MSI.issuing_bank = "PROSA"
SET
    A_Installments_Fees_TMP.Fee = A_E_BI_Bank_MSI.FEE
WHERE 
    A_Installments_Fees_TMP.Fee = 0
;

DROP  TABLE IF EXISTS A_Installments_Fees;
CREATE TABLE A_Installments_Fees ( PRIMARY KEY ( OrderNum ) )
SELECT
    OrderNum,
    issuing_bank,
    bin,
    AVG( GrandTotal )* AVG( Fee ) / 100 AS Installment_Fee,
    SUM( PriceAfterTax ) AS Order_PriceAfterTax
FROM
    A_Installments_Fees_TMP
GROUP BY OrderNum
;


UPDATE            A_Installments_Fees 
       INNER JOIN development_mx.A_Master_Sample
            USING ( OrderNum )
SET
       development_mx.A_Master_Sample.IssuingBank    = A_Installments_Fees.issuing_bank,
       development_mx.A_Master_Sample.Bin            = A_Installments_Fees.bin,

       development_mx.A_Master_Sample.InstallmentFee =
                                               ( A_Installments_Fees.Installment_Fee * 
                                               ( development_mx.A_Master_Sample.PriceAfterTax / 
                                                 A_Installments_Fees.Order_PriceAfterTax ) 
                                              ),
       development_mx.A_Master_Sample.InstallmentFeeAfterTax = 
                                               ( A_Installments_Fees.Installment_Fee * 
                                               ( development_mx.A_Master_Sample.PriceAfterTax / 
                                                 A_Installments_Fees.Order_PriceAfterTax ) 
                                              ) / 
                                              ( 1 + TaxPercent / 100 )
;

