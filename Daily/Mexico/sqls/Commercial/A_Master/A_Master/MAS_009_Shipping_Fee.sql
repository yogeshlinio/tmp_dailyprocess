/*
* MX_009_001 Shipping Fee
*/
/* 
#Query: M1_ShipFee_table
UPDATE @development@.A_Master_Sample 
SET
   ShippingFee = round(((A_Master_Sample.PackageWeight/ @development@.A_Master_Sample.OrderWeight )*A_Master_Sample.ShippingFee),3) 
;*/
/*
* MX_009_001 Shipping Fee
*/ 
#Query: M1_ShipFee_table
UPDATE A_Master_Sample 
SET
   ShippingFeeAfterTax = A_Master_Sample.ShippingFee / ( 1 +  ( A_Master_Sample.TaxPercent / 100 ) )          
;


/*
* MX_009_002 
*/
 #Query: M1_ShipFee_CAC
UPDATE @development@.A_Master_Sample 
SET 
   @development@.A_Master_Sample.ShippingFee = 55
WHERE 
   @development@.A_Master_Sample.CouponCode In ( "CAC0xY3Iz","CACs4tyXP");

