UPDATE @development@.A_Master_Sample
SET
   PaymentFees =  @development@.A_Master_Sample.PaidPrice /( 1 + @development@.A_Master_Sample.TaxPercent / 100 )  * ( @development@.A_Master_Sample.Fees / 100 )
                 +@development@.A_Master_Sample.ExtraCharge
;

/*
*   MX_001_006_Paypal_Vouchers
*/
UPDATE @development@.A_Master_Sample
 SET 
    @development@.A_Master_Sample.CouponValue           = 0, 
    @development@.A_Master_Sample.CouponValueAfterTax   = 0, 
    @development@.A_Master_Sample.PaymentFees           = 0
    #A_Master_Sample.Commercial_price_after_tax = A_Master_Sample.price_after_tax
WHERE 
     @development@.A_Master_Sample.CouponCode in ( "COM1Tw3Bz" ,
                                                    "COM1QuGJR" , 
                                                    "COMq517VW" ,
                                                    "COMc4jiTb"  ,  
                                                    "COM57OMUy" )
  Or @development@.A_Master_Sample.CouponCode like "%MKTpaypal%"
;
