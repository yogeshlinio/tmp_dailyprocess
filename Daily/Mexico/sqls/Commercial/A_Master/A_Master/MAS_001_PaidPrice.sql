/*
* MX_001_001 Paid Price CiberSanta
*/
 #Query: A 217 U Ciber Santa
UPDATE            @development@.A_Master_Sample
       INNER JOIN A_E_Ciber_Santa 
               ON      @development@.A_Master_Sample.SKUConfig = @development@.A_E_Ciber_Santa.SKU
                  AND  date_format( @development@.A_Master_Sample.Date , "%Y-%m-%d" ) = @development@.A_E_Ciber_Santa.Date

SET 
   @development@.A_Master_Sample.PaidPrice            =   @development@.A_Master_Sample.Price - A_E_Ciber_Santa.Subsidio,
   @development@.A_Master_Sample.PaidPriceAfterTax    = ( @development@.A_Master_Sample.Price - A_E_Ciber_Santa.Subsidio) * 0.86
;