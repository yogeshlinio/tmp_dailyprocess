 #Query: M1_ShippingCost_Junio
UPDATE @development@.A_Master_Sample SET 
@development@.A_Master_Sample.ShippingCost = 104
WHERE @development@.A_Master_Sample.Itemid=267502;

/*
* MX_099_001 Costo de Revistas
*/
 #Query: 7_M1_Costo0Revistas
UPDATE            @development@.A_Master_Sample 
       INNER JOIN A_E_6_M1_Costos_Revistas 
               ON @development@.A_Master_Sample.SKUSimple = A_E_6_M1_Costos_Revistas.SKU_Simple 
SET 
   @development@.A_Master_Sample.ShippingCost    = 0, 
   @development@.A_Master_Sample.DeliveryCostSupplier = 0;

/*
* MX_099 Payment_Fee / Transaction_Fee
*/
UPDATE @development@.A_Master_Sample
SET
    PaymentFees = TransactionFeeAfterTax
WHERE Date >= "2013-07-01"
;
/*
* MX_099 HeadPhones Dree
*/
UPDATE            @development@.A_Master_Sample
       INNER JOIN @development@.A_E_BI_PatchHeadphonesDree
            USING ( ItemId )
       INNER JOIN @development@.Out_SalesReportItem
            USING ( ItemId )
SET
   @development@.A_Master_Sample.Cost         = @development@.Out_SalesReportItem.Cost_OMS_BOB,
   @development@.A_Master_Sample.CostAfterTax = @development@.Out_SalesReportItem.Cost_after_tax_OMS_BOB
;

UPDATE @development@.A_Master_Sample
SET
   @development@.A_Master_Sample.Cost         = 0,
   @development@.A_Master_Sample.CostAfterTax = 0
WHERE
   @development@.A_Master_Sample.ItemId = 210928
;
/*
Descripcion: Se actualiza Cost y CostAfterTax, debido a un error al capturar dichos campos
Solicitud: Jorge Nieto
Fecha: 15-01-2014
*/
UPDATE @development@.A_Master_Sample
SET
   @development@.A_Master_Sample.Cost         = 2882,
   @development@.A_Master_Sample.CostAfterTax = 2882/1.16
WHERE
   @development@.A_Master_Sample.ItemID=813472;
;
