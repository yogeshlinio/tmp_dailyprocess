USE development_mx;
DROP TEMPORARY TABLE IF EXISTS A_NetWithFee;
CREATE TEMPORARY TABLE A_NetWithFee ( PRIMARY KEY ( Date, PaymentMethod ) )
SELECT
  Date,
  PaymentMethod,
  count(*) AS Items,
  count( DISTINCT OrderNum ) AS Orders,
  SUM( PaidPrice )           AS PaidPrice
FROM
  development_mx.A_Master_Sample
WHERE
       Date >= "2013-07-01"
   AND OrderAfterCan = 1
   AND PaymentMethod not in ( "CashOnDelivery_Payment", "Banorte_PagoReferenciado" )
GROUP BY Date, PaymentMethod;

DROP TEMPORARY TABLE IF EXISTS A_NetCollected;
CREATE TEMPORARY TABLE A_NetCollected ( PRIMARY KEY ( DateCollected, PaymentMethod ) )
SELECT
  DateCollected,
  PaymentMethod,
  count(*) AS Items,
  count( DISTINCT OrderNum ) AS Orders,
  SUM( PaidPrice )           AS PaidPrice,
  0000000000000.00           AS Net_PaidPrice,
  0000000.00000              AS Fee,
  0000000000000.00           AS TransactionFee
FROM
  development_mx.A_Master_Sample
WHERE
       Date >= "2013-07-01"
   AND Collected = 1
   AND IF( PaymentMethod not  in ( "Oxxo_Direct", "CreditCardOnDelivery_Payment" ) ,
           IF( OrderAfterCan = 1 AND Refunded = 0 , 1, 0  ),
           1
         )
GROUP BY DateCollected, PaymentMethod;

UPDATE        A_NetWithFee
   INNER JOIN A_NetCollected
           ON     A_NetCollected.DateCollected = A_NetWithFee.Date
              AND A_NetCollected.PaymentMethod = A_NetWithFee.PaymentMethod
SET
   A_NetCollected.Net_PaidPrice = A_NetWithFee.PaidPrice;

UPDATE development_mx.A_NetCollected
SET
    Fee = 4.05
WHERE 
    PaymentMethod in ( "Amex_Gateway" );

UPDATE development_mx.A_NetCollected
SET
    Fee = 1.93
WHERE 
    PaymentMethod in ( "Banorte_Payworks_Debit" );

UPDATE development_mx.A_NetCollected
SET
    Fee = 1.4
WHERE 
    PaymentMethod in ( "Banorte_Payworks" );

UPDATE development_mx.A_NetCollected
SET
    Fee = 2
WHERE 
    PaymentMethod in ( "Oxxo_Direct" );

UPDATE development_mx.A_NetCollected
SET
    Fee = 3
WHERE 
    PaymentMethod in ( "Paypal_Express_Checkout" );

UPDATE development_mx.A_NetCollected
SET
    Fee = 1.76
WHERE 
    PaymentMethod in ( "CreditCardOnDelivery_Payment" );

UPDATE development_mx.A_NetCollected
SET
   TransactionFee = PaidPrice * ( Fee / 100 );

UPDATE development_mx.A_NetCollected
SET
   TransactionFee = TransactionFee + ( 4 * Orders )
WHERE
   PaymentMethod in ( "Paypal_Express_Checkout" )
;

UPDATE            development_mx.A_Master_Sample
       INNER JOIN A_NetCollected 
           ON     A_NetCollected.DateCollected = A_Master_Sample.Date
              AND A_NetCollected.PaymentMethod = A_Master_Sample.PaymentMethod
SET

   A_Master_Sample.TransactionFee   = ( A_NetCollected.TransactionFee *
                                                     (A_Master_Sample.PaidPrice /
                                                      A_NetCollected.Net_PaidPrice) 
                                      ),
                                                     
   A_Master_Sample.TransactionFeeAfterTax = ( A_NetCollected.TransactionFee *
                                                     (A_Master_Sample.PaidPrice /
                                                      A_NetCollected.Net_PaidPrice) 
                                             ) /
                                             ( 1 + A_Master_Sample.TaxPercent / 100 )  
WHERE
       A_Master_Sample.Date >= "2013-07-01"
   AND A_Master_Sample.OrderAfterCan = 1
   AND A_Master_Sample.PaymentMethod not in ( "CashOnDelivery_Payment", "Banorte_PagoReferenciado" )
;
