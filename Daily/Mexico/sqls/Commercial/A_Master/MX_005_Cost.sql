/*
* MX_005_001 Adjust Wrong Cost form Daily Report
*/

 #Query: A 114 U Adjust Wrong Costs
UPDATE            development_mx.A_Master_Sample
       INNER JOIN development_mx.COM_Adjust_Wrong_Costs_on_Daily_Report 
               ON     (development_mx.A_Master_Sample.OrderNum   = COM_Adjust_Wrong_Costs_on_Daily_Report.Order_Num) 
                  AND (development_mx.A_Master_Sample.SKUSimple  = COM_Adjust_Wrong_Costs_on_Daily_Report.SKU_Simple) 

SET 
    development_mx.A_Master_Sample.Cost           = development_mx.COM_Adjust_Wrong_Costs_on_Daily_Report.Cost, 
    development_mx.A_Master_Sample.CostAftertax   = development_mx.COM_Adjust_Wrong_Costs_on_Daily_Report.Cost_after_tax;

/*
*  MX_005_002 Adjust null cost
*/
UPDATE        development_mx.A_Master_Sample
   INNER JOIN development_mx.A_Master_Catalog
           ON development_mx.A_Master_Sample.SKUSimple = development_mx.A_Master_Catalog.SKU_simple
SET
   development_mx.A_Master_Sample.Cost         = development_mx.A_Master_Catalog.cost,
   development_mx.A_Master_Sample.CostAfterTax = development_mx.A_Master_Catalog.cost / 
                                                 ( 1 +  ( development_mx.A_Master_Sample.TaxPercent / 100 ) )          
WHERE
      development_mx.A_Master_Sample.Cost is null 
   OR development_mx.A_Master_Sample.Cost = 0
;

UPDATE        A_Master_Sample
   INNER JOIN A_Master_Catalog p
           ON A_Master_Sample.SKUSimple= p.sku_simple 
SET 
   A_Master_Sample.Cost = p.Cost_OMS  ,
   A_Master_Sample.CostAftertax = p.Cost_OMS /( ( 100 +  A_Master_Sample.TaxPercent )/100 )
   
WHERE
 p.Cost_OMS > 0 
  and
  (A_Master_Sample.Cost is null 
	   OR A_Master_Sample.Cost = 0)
;   


/*
* MX_005_003 Cost_OMS
*/
UPDATE             development_mx.A_Master_Sample  
        INNER JOIN wmsprod.itens_venda b 
                ON b.item_id = development_mx.A_Master_Sample.ItemID
        INNER JOIN wmsprod.estoque c 
                ON c.estoque_id = b.estoque_id 
        INNER JOIN wmsprod.itens_recebimento d
                ON c.itens_recebimento_id = d.itens_recebimento_id 
        INNER JOIN procurement_live_mx.wms_received_item e 
                ON c.itens_recebimento_id=e.id_wms 
        INNER JOIN procurement_live_mx.procurement_order_item f
                ON e.fk_procurement_order_item=f.id_procurement_order_item 
SET 
    development_mx.A_Master_Sample.Cost           = f.unit_price ,
    development_mx.A_Master_Sample.CostAftertax   = f.unit_price / ( ( 100 +  development_mx.A_Master_Sample.TaxPercent )/100 )
WHERE 
       f.is_deleted = 0
   AND (    development_mx.A_Master_Sample.Date >= "2013-07-01" 
         OR development_mx.A_Master_Sample.Cost is null 
         OR development_mx.A_Master_Sample.Cost <= 0 )
   AND f.unit_price is not null 
   AND f.unit_price > 0
       ;
 
/* 
UPDATE        development_mx.A_Master_Sample
   INNER JOIN wmsprod.itens_venda
           ON development_mx.A_Master_Sample.ItemID = itens_venda.item_id
SET
   development_mx.A_Master_Sample.Cost         = itens_venda.cost_item,
   development_mx.A_Master_Sample.CostAfterTax = itens_venda.cost_item / 
                                                 ( 1 +  ( development_mx.A_Master_Sample.TaxPercent / 100 ) )
where A_Master_Sample.Cost <=0        
;
*/ 
/*
UPDATE        development_mx.A_Master_Sample
   INNER JOIN development_mx.A_Master_Catalog 
           ON development_mx.A_Master_Sample.SKUSimple = development_mx.A_Master_Catalog.sku_simple
SET 
   development_mx.A_Master_Sample.Cost         = development_mx.A_Master_Catalog.cost,
   development_mx.A_Master_Sample.CostAfterTax = development_mx.A_Master_Catalog.cost /( 1 + development_mx.A_Master_Sample.TaxPercent/ 100 )
WHERE 
(      
      development_mx.A_Master_Sample.Cost is null  
   OR development_mx.A_Master_Sample.Cost <= 0 
)
AND development_mx.A_Master_Catalog.cost is not null 
AND development_mx.A_Master_Catalog.cost > 0
;
*/
