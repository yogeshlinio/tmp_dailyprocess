/*
*  MX_001_002_Coupons_EV_VISA
*/
UPDATE development_mx.A_Master_Sample
SET
   development_mx.A_Master_Sample.CouponValue         = 0,
   development_mx.A_Master_Sample.CouponValueAfterTax = 0
WHERE
   development_mx.A_Master_Sample.PrefixCode IN ("CCE",
                                                 "PRCcre",
                                                 "OPScre",
                                                 "DEP",
                                                 "VISA",
                                                 "VISA3"
                                                );


/*
*  MX_001_003_Adjust_Wrong_Coupons_by_CC
*/
UPDATE            development_mx.A_Master_Sample
       INNER JOIN COM_Adjust_Wrong_Vouchers_Used_by_CC 
               ON     (development_mx.A_Master_Sample.OrderNum  = COM_Adjust_Wrong_Vouchers_Used_by_CC.Order_Nr) 
                  AND (development_mx.A_Master_Sample.SKUSimple = COM_Adjust_Wrong_Vouchers_Used_by_CC.SKU_Simple) 
SET 
   development_mx.A_Master_Sample.CouponValue         = COM_Adjust_Wrong_Vouchers_Used_by_CC.Coupon_money_value, 
   development_mx.A_Master_Sample.CouponValueAfterTax = COM_Adjust_Wrong_Vouchers_Used_by_CC.After_vat_coupon;

/*
*  MX_001_004_Non_MKT_Voucher_CCemp
*/
UPDATE development_mx.A_Master_Sample
SET
   development_mx.A_Master_Sample.CouponValue             = 0,
   development_mx.A_Master_Sample.CouponValueAfterTax     = 0
WHERE
   development_mx.A_Master_Sample.PrefixCode = "CCemp"
;

/*
*  MX_001_005_Adjust_Nomina_Sales
*/
 #Query: A 131 U Adjust Nomina Sales
UPDATE development_mx.A_Master_Sample
       INNER JOIN COM_Adjust_Price_Coupon_for_Nomina_Sales 
               ON     ( development_mx.A_Master_Sample.CouponCode = COM_Adjust_Price_Coupon_for_Nomina_Sales.Voucher_Code) 
                  AND ( development_mx.A_Master_Sample.SKUConfig  = COM_Adjust_Price_Coupon_for_Nomina_Sales.SKU_Config) 
SET 
    development_mx.A_Master_Sample.CouponValue           = COM_Adjust_Price_Coupon_for_Nomina_Sales.Coupon_Money_Value, 
    development_mx.A_Master_Sample.CouponValueAfterTax   = COM_Adjust_Price_Coupon_for_Nomina_Sales.After_VAT_Coupon;

/*
*   MX_001_006_Paypal_Vouchers
*/
UPDATE development_mx.A_Master_Sample
 SET 
    development_mx.A_Master_Sample.CouponValue           = 0, 
    development_mx.A_Master_Sample.CouponValueAfterTax   = 0
    #A_Master_Sample.Commercial_price_after_tax = A_Master_Sample.price_after_tax
WHERE 
     development_mx.A_Master_Sample.CouponCode in ( "COM1Tw3Bz" ,
                                                    "COM1QuGJR" ,  "COMq517VW" ,
                                                    "COMc4jiTb"  ,  "COM57OMUy" )
  Or development_mx.A_Master_Sample.CouponCode like "%paypal%"
;
/*
*   MX_001_007_Wrong_OPS_Voucher ???
*/

 #Query: A 226 U Wrong OPS Voucher
UPDATE (           A_Master_Sample 
         LEFT JOIN bob_live_mx.sales_rule 
                ON A_Master_Sample.CouponCode = bob_live_mx.sales_rule.code) 
         LEFT JOIN bob_live_mx.sales_rule_set 
                ON bob_live_mx.sales_rule.fk_sales_rule_set = bob_live_mx.sales_rule_set.id_sales_rule_set 
SET 
     A_Master_Sample.PrefixCode = bob_live_mx.sales_rule_set.code_prefix
WHERE 
    (((A_Master_Sample.PrefixCode)="OPScred") 
AND ((A_Master_Sample.Date)>="2012/1/12"));

/*
*   MX_001_009_CAC_Campaign_Enero
*/
 #Query: 1_M1_CouponMoneyValue+ShippingCost
UPDATE development_mx.A_Master_Sample
SET 
    development_mx.A_Master_Sample.CouponValue = development_mx.A_Master_Sample.CouponValue + development_mx.A_Master_Sample.ShippingCost
WHERE 
    development_mx.A_Master_Sample.Couponcode in ( "MKT0xfgVK" , "MKT1eDvI7");

/*
*  MX_001_010
*/
 #Query: M1_U_voucherMKT
UPDATE            development_mx.A_Master_Sample
       INNER JOIN development_mx.A_E_M1_VoucherMKT 
               ON A_Master_Sample.CouponCode = A_E_M1_VoucherMKT.Voucher
SET 
   development_mx.A_Master_Sample.PrefixCode = "MKT";

/*
* DATE           2013/11/28
* DEVELOPED BY   Carlos Antonio Mondragón Soria
* DESCRIPTION MKT - CAC COUPON CODE FIX 
*/
UPDATE        A_Master_Sample
   INNER JOIN test_linio.M_MKT_Vouchers
       USING  ( CouponCode )
SET
   PrefixCode = test_linio.M_MKT_Vouchers.Campaign
;

