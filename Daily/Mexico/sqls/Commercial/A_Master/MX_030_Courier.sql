/*
*  MX_008_001 Carrier
*/
#Query 1: Se agrega el nombre del Carrier
UPDATE            development_mx.A_Master_Sample
       INNER JOIN bob_live_mx.shipment_carrier c
	           ON A_Master_Sample.fk_courier = c.id_shipment_carrier
SET
    A_Master_Sample.Courier = if( c.`name` !='', c.`name` , fk_courier);


