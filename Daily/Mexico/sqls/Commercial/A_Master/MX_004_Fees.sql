/*
*  004 Fees
*/
UPDATE        development_mx.A_Master_Sample 
   INNER JOIN development_mx.A_E_In_COM_Fees_per_PM 
           ON     development_mx.A_Master_Sample.PaymentMethod = development_mx.A_E_In_COM_Fees_per_PM.Payment_Method
              AND development_mx.A_Master_Sample.Installment   = development_mx.A_E_In_COM_Fees_per_PM.Installment
SET
   development_mx.A_Master_Sample.Fees        = development_mx.A_E_In_COM_Fees_per_PM.Fee,
   development_mx.A_Master_Sample.ExtraCharge = development_mx.A_E_In_COM_Fees_per_PM.Extra_Charge;
