/*
*   UPDATE A_Master_Catalog.Cat1 A_Master_Catalog.Cat2 A_Master_Catalog.Cat3
*/
UPDATE              A_Master_Catalog_Sample         	
         INNER JOIN bob_live_mx.catalog_attribute_option_global_category  
                USING ( id_catalog_attribute_option_global_category )
SET
   A_Master_Catalog_Sample.Cat1 =catalog_attribute_option_global_category.name;

UPDATE              A_Master_Catalog_Sample         
         INNER JOIN bob_live_mx.catalog_attribute_option_global_sub_category  
                USING ( id_catalog_attribute_option_global_sub_category )
SET
   A_Master_Catalog_Sample.Cat2 = catalog_attribute_option_global_sub_category.name;

UPDATE              A_Master_Catalog_Sample
         INNER JOIN bob_live_mx.catalog_attribute_option_global_sub_sub_category  
                USING ( id_catalog_attribute_option_global_sub_sub_category )
SET
   A_Master_Catalog_Sample.Cat3 = catalog_attribute_option_global_sub_sub_category.name;

/*
*   UPDATE A_Master_Catalog.Cat_BP
*/
 #Query: M1_CategoryBP2
UPDATE            A_Master_Catalog_Sample 
       INNER JOIN GlobalConfig.A_E_M1_New_CategoryBP 
               ON A_Master_Catalog_Sample.Cat2 = A_E_M1_New_CategoryBP.Cat2 and A_Master_Catalog_Sample.Cat1 = A_E_M1_New_CategoryBP.Cat1
SET 
    A_Master_Catalog_Sample.Cat_BP = A_E_M1_New_CategoryBP.CatBP
;

UPDATE            A_Master_Catalog_Sample 
       INNER JOIN GlobalConfig.A_E_M1_New_CategoryBP 
               ON A_Master_Catalog_Sample.Cat1 = A_E_M1_New_CategoryBP.Cat1
SET 
    A_Master_Catalog_Sample.Cat_BP = A_E_M1_New_CategoryBP.CatBP
WHERE 
   	A_Master_Catalog_Sample.Cat_BP = '9,1 Other'
;

/*
 #Query: M1_CategoryBP2
UPDATE           A_Master_Catalog_Sample 
      INNER JOIN GlobalConfig.A_E_M1_New_CategoryBP 
              ON A_Master_Catalog_Sample.Cat1 = A_E_M1_New_CategoryBP.Cat1 
SET 
    A_Master_Catalog_Sample.Cat_BP = A_E_M1_New_CategoryBP.CatBP;

UPDATE            A_Master_Catalog_Sample 
       INNER JOIN GlobalConfig.A_E_M1_New_CategoryBP 
               ON A_Master_Catalog_Sample.Cat2 = A_E_M1_New_CategoryBP.Cat2 
SET 
    A_Master_Catalog_Sample.Cat_BP = A_E_M1_New_CategoryBP.CatBP
;

UPDATE            A_Master_Catalog_Sample 
       INNER JOIN GlobalConfig.A_E_M1_New_CategoryBP 
               ON A_Master_Catalog_Sample.Cat2 = A_E_M1_New_CategoryBP.Cat2 and A_Master_Catalog_Sample.Cat1 = A_E_M1_New_CategoryBP.Cat1
SET 
    A_Master_Catalog_Sample.Cat_BP = A_E_M1_New_CategoryBP.CatBP
;
*/


/* Colombia's Way...
 UPDATE bazayaco.tbl_catalog_product_v2 a
INNER JOIN development_mx.A_E_M1_New_CategoryBP b ON a.new_cat2 = b.cat2
SET a.category_bp = b.CatBP;


UPDATE bazayaco.tbl_catalog_product_v2 a
INNER JOIN development_mx.A_E_M1_New_CategoryBP b ON a.new_cat1 = b.cat1
SET a.category_bp = b.CatBP
WHERE a.category_bp IS NULL;
*/
 
/*   
*   UPDATE A_Master_Catalog.Cap_KPI
*/
UPDATE              A_Master_Catalog_Sample         
         INNER JOIN GlobalConfig.M_CategoryKPI 
                 ON A_Master_Catalog_Sample.Cat_BP =  M_CategoryKPI.CatBP
SET
   A_Master_Catalog_Sample.Cat_KPI = M_CategoryKPI.CatKPI;
 