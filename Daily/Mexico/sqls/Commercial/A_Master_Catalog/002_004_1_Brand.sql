/*
*   UPDATE A_Master_Catalog.Brand

*/         
UPDATE           A_Master_Catalog_Sample         
      INNER JOIN bob_live_mx.catalog_brand
           USING ( id_catalog_brand )
SET
    A_Master_Catalog_Sample.brand =  catalog_brand.`name` 
;

