UPDATE            A_Master_Catalog_Sample
       INNER JOIN bob_live_mx.catalog_config
            USING ( id_catalog_config )
       INNER JOIN bob_live_mx.catalog_attribute_set
               ON fk_catalog_attribute_set = id_catalog_attribute_set
SET
		A_Master_Catalog_Sample.attribute = catalog_attribute_set.label
;
