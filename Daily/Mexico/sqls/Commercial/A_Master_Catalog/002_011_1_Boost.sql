UPDATE            A_Master_Catalog_Sample
       INNER JOIN bob_live_mx.catalog_config
            USING ( id_catalog_config )
       INNER JOIN bob_live_mx.catalog_product_boost
               ON fk_catalog_config = id_catalog_config
SET
		A_Master_Catalog_Sample.boost = catalog_product_boost.boost
;
