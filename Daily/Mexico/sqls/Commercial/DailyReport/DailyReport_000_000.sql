drop table if exists development_mx.DailyReport	
;	
create table development_mx.DailyReport	
(`Date Placed` date not null, `Weekday` varchar(9) not null, `Month` int not null, `Exchange Rate` decimal (15,4) not null,
`Visits` int not null, `Placed Sales` decimal (15,4) not null, `Placed Items` int not null, `Placed Unique Items` int not null,
`Placed Orders` int not null, `Placed Tax Amount` decimal (15,4) not null, `Invalid Sales` decimal (15,4), `Invalid Items` int not null,	
`Invalid Tax Amount` decimal (15,4) not null, `Invalid Orders` int not null, `Gross Sales with VAT` decimal (15,4) not null,	
`Gross Items` int not null, `Gross Tax Amount` decimal (15,4) not null, `Gross Unique Items` int not null, `Gross Orders` int not null,	
`Gross Sales` decimal (15,4) not null, `Gross COGS` decimal (15,4) not null, `Gross Cost` decimal (15,4) not null,	
`Gross Inbound Cost` decimal (15,4) not null, `Canceled Sales` decimal (15,4) not null, `Canceled Items` int not null,	
`Canceled Orders` int not null, `Rejected Sales` decimal(15,4) not null, `Rejected Items` int not null, `Rejected Orders` int not null,	
`Pending Sales` decimal (15,4) not null, `Pending Items` int not null, `Pending Orders` int not null,	
`Gross Sales Post` decimal (15,4) not null, `Gross Items Post` int not null, `Gross COGS Post` decimal (15,4) not null,	
`Gross Cost Post` decimal (15,4) not null, `Gross Inbound Cost Post` decimal (15,4) not null, `Gross Unique Items Post` int not null,	
`Gross Orders Post` int not null, `Returned Sales` decimal (15,4) not null, `Returned Items` int not null,	
`Returned COGS` decimal (15,4) not null, `Returned Orders` int not null, `Net Sales Pre-Voucher` decimal (15,4) not null,	
`Voucher Value` decimal (15,4) not null, `Product Net Sales` decimal (15,4) not null,	`Net Interest` decimal (15,4) not null,	
`Shipping Fee` decimal (15,4) not null, `Other Revenue` decimal (15,4) not null, `Net Revenue` decimal (15,4) not null,
`Net Items` int not null, `Net COGS` decimal (15,4) not null, `Shipping Cost` decimal (15,4) not null,
`Net Cost` decimal (15, 4) not null, `Net Inbound Cost` decimal (15,4) not null, `Net Unique Items` int not null,
`Net Orders` int not null, `Delivered Revenue` decimal (15,4) not null, `Delivered Items` int not null,
`Delivered COGS` decimal (15,4) not null, `Delivered Shipping Cost` decimal (15,4) not null,
`Returned Date Revenue` decimal (15,4) not null, `Returned Date Items` int not null, `Returned Date COGS` decimal (15,4) not null,
`Returned Date Shipping Cost` decimal (15,4) not null, `Retention Cost` decimal (15,4) not null,
`Acquisition Cost` decimal (15,4) not null, `Other MKT Spend` decimal (15,4) not null, `Total Transaction Fee` decimal (15,4) not null,
`Installment Fee` decimal (15,4) not null, `FL WH Cost` decimal (15,4) not null, `FL CS Cost` decimal (15,4) not null,
`FL Total Cost` decimal (15,4) not null, `Total Customers` int not null, `New Customers` int not null,
`1,1 Product Sales` decimal	(15,4) not null, `1,1 Shipping Fee` decimal (15,4) not null, `1,1 Cost` decimal (15,4) not null,
`1,1 Inbound Cost` decimal (15,4) not null, `1,1 Shipping Cost`decimal (15,4) not null, `1,1 Payment Fee` decimal (15,4) not null,
`1,1 WH Cost` decimal (15,4) not null, `1,1 CS Cost` decimal (15,4) not null,
`1,2 Product Sales` decimal	(15,4) not null, `1,2 Shipping Fee` decimal (15,4) not null, `1,2 Cost` decimal (15,4) not null,
`1,2 Inbound Cost` decimal (15,4) not null, `1,2 Shipping Cost`decimal (15,4) not null, `1,2 Payment Fee` decimal (15,4) not null,
`1,2 WH Cost` decimal (15,4) not null, `1,2 CS Cost` decimal (15,4) not null,
`1,3 Product Sales` decimal	(15,4) not null, `1,3 Shipping Fee` decimal (15,4) not null, `1,3 Cost` decimal (15,4) not null,
`1,3 Inbound Cost` decimal (15,4) not null, `1,3 Shipping Cost`decimal (15,4) not null, `1,3 Payment Fee` decimal (15,4) not null,
`1,3 WH Cost` decimal (15,4) not null, `1,3 CS Cost` decimal (15,4) not null,
`2,1 Product Sales` decimal	(15,4) not null, `2,1 Shipping Fee` decimal (15,4) not null, `2,1 Cost` decimal (15,4) not null,
`2,1 Inbound Cost` decimal (15,4) not null, `2,1 Shipping Cost`decimal (15,4) not null, `2,1 Payment Fee` decimal (15,4) not null,
`2,1 WH Cost` decimal (15,4) not null, `2,1 CS Cost` decimal (15,4) not null,
`2,2 Product Sales` decimal	(15,4) not null, `2,2 Shipping Fee` decimal (15,4) not null, `2,2 Cost` decimal (15,4) not null,
`2,2 Inbound Cost` decimal (15,4) not null, `2,2 Shipping Cost`decimal (15,4) not null, `2,2 Payment Fee` decimal (15,4) not null,
`2,2 WH Cost` decimal (15,4) not null, `2,2 CS Cost` decimal (15,4) not null,
`3,1 Product Sales` decimal	(15,4) not null, `3,1 Shipping Fee` decimal (15,4) not null, `3,1 Cost` decimal (15,4) not null,
`3,1 Inbound Cost` decimal (15,4) not null, `3,1 Shipping Cost`decimal (15,4) not null, `3,1 Payment Fee` decimal (15,4) not null,
`3,1 WH Cost` decimal (15,4) not null, `3,1 CS Cost` decimal (15,4) not null,
`4,1 Product Sales` decimal	(15,4) not null, `4,1 Shipping Fee` decimal (15,4) not null, `4,1 Cost` decimal (15,4) not null,
`4,1 Inbound Cost` decimal (15,4) not null, `4,1 Shipping Cost`decimal (15,4) not null, `4,1 Payment Fee` decimal (15,4) not null,
`4,1 WH Cost` decimal (15,4) not null, `4,1 CS Cost` decimal (15,4) not null,
`5,1 Product Sales` decimal	(15,4) not null, `5,1 Shipping Fee` decimal (15,4) not null, `5,1 Cost` decimal (15,4) not null,
`5,1 Inbound Cost` decimal (15,4) not null, `5,1 Shipping Cost`decimal (15,4) not null, `5,1 Payment Fee` decimal (15,4) not null,
`5,1 WH Cost` decimal (15,4) not null, `5,1 CS Cost` decimal (15,4) not null,
`5,2 Product Sales` decimal	(15,4) not null, `5,2 Shipping Fee` decimal (15,4) not null, `5,2 Cost` decimal (15,4) not null,
`5,2 Inbound Cost` decimal (15,4) not null, `5,2 Shipping Cost`decimal (15,4) not null, `5,2 Payment Fee` decimal (15,4) not null,
`5,2 WH Cost` decimal (15,4) not null, `5,2 CS Cost` decimal (15,4) not null,
`6,1 Product Sales` decimal	(15,4) not null, `6,1 Shipping Fee` decimal (15,4) not null, `6,1 Cost` decimal (15,4) not null,
`6,1 Inbound Cost` decimal (15,4) not null, `6,1 Shipping Cost`decimal (15,4) not null, `6,1 Payment Fee` decimal (15,4) not null,
`6,1 WH Cost` decimal (15,4) not null, `6,1 CS Cost` decimal (15,4) not null,
`7,1 Product Sales` decimal	(15,4) not null, `7,1 Shipping Fee` decimal (15,4) not null, `7,1 Cost` decimal (15,4) not null,
`7,1 Inbound Cost` decimal (15,4) not null, `7,1 Shipping Cost`decimal (15,4) not null, `7,1 Payment Fee` decimal (15,4) not null,
`7,1 WH Cost` decimal (15,4) not null, `7,1 CS Cost` decimal (15,4) not null,
`8,1 Product Sales` decimal	(15,4) not null, `8,1 Shipping Fee` decimal (15,4) not null, `8,1 Cost` decimal (15,4) not null,
`8,1 Inbound Cost` decimal (15,4) not null, `8,1 Shipping Cost`decimal (15,4) not null, `8,1 Payment Fee` decimal (15,4) not null,
`8,1 WH Cost` decimal (15,4) not null, `8,1 CS Cost` decimal (15,4) not null,
`9,1 Product Sales` decimal	(15,4) not null, `9,1 Shipping Fee` decimal (15,4) not null, `9,1 Cost` decimal (15,4) not null,
`9,1 Inbound Cost` decimal (15,4) not null, `9,1 Shipping Cost`decimal (15,4) not null, `9,1 Payment Fee` decimal (15,4) not null,
`9,1 WH Cost` decimal (15,4) not null, `9,1 CS Cost` decimal (15,4) not null,


`MKT cost` decimal (15,4) not null, `MP Unique SKUs` int not null, `MP Items` int not null, `MP Revenue` decimal (15,4) not null,
`MP Commission` decimal (15,4) not null, `MP CommRev` decimal (15,4) not null, `MP Suppliers` int not null,
`MP SKUs from Suppliers` int not null, 

`Payment Fee` decimal (15,4) not null, 
`Gross Customers` int not null,

        `Gross Cancelled Customers` int not null,
        `Gross Rejected Customers` int not null,
        `Gross Returned Customers` int not null,

`MP Cost` decimal(15,4) not null ,

`9,2 Product Sales` decimal	(15,4) not null, `9,2 Shipping Fee` decimal (15,4) not null, `9,2 Cost` decimal (15,4) not null,
`9,2 Inbound Cost` decimal (15,4) not null, `9,2 Shipping Cost`decimal (15,4) not null, `9,2 Payment Fee` decimal (15,4) not null,
`9,2 WH Cost` decimal (15,4) not null, `9,2 CS Cost` decimal (15,4) not null,

`Sales Force Cost` decimal (15,4) not null,
`Chargebacks` decimal (15,4) not null,

`IL Shipping Cost` decimal (15,4) not null,
`IL Quarantine Cost` decimal (15,4) not null,
`DeliveredRev` decimal(15,4) not null,
`Net+Ret_Revenue` decimal(15,4) not null,
`Gross Revenue (for Estimation)` decimal(15,4) not null,
`% Positive PC 1.5 Orders` decimal(15,4) not null
)
;	

insert into development_mx.DailyReport (`Date Placed`, `Weekday`, `Month`, `Placed Sales`, `Placed Items`, `Placed Unique Items`,
`Placed Orders`, `Placed Tax Amount`)
select a.Date as 'Date Placed', DATE_FORMAT(a.Date,'%W') as 'Weekday', a.MonthNum as 'Month', sum(a.Price) as 'Placed Sales',	
count(a.ItemId) as 'Placed Items', count(distinct a.SKUSimple) as 'Placed Unique Items', count(distinct a.OrderNum) as 'Placed Orders',	
sum(Tax) as 'Placed Tax Amount'
from development_mx.A_Master a
group by a.Date
;
update development_mx.DailyReport a
inner join (	
#XR	
select Month_Num, XR	
from development_mx.A_E_BI_ExchangeRate	
where Country = 'MEX'	
) b	
on a.Month = b.Month_Num
set a.`Exchange Rate` = b.XR
;
update development_mx.DailyReport a
inner join (	
#Visits
select date, sum(visits) as Visits	
from SEM.campaign_ad_group_mx	
group by Date	
) c	
on a.`Date Placed` = c.date	
set a.Visits = c.Visits
;
update development_mx.DailyReport a
inner join (	
#Invalid Items	
select Date, sum(Price) as 'Invalid Sales', count(ItemID) as 'Invalid Items', Sum(Tax) as 'Invalid Tax Amount'	
from development_mx.A_Master	
where OrderBeforeCan = 0	
group by Date	
) d	
on a.`Date Placed` = d.Date
set a.`Invalid Sales` = d.`Invalid Sales`, a.`Invalid Items` = d.`Invalid Items`, a.`Invalid Tax Amount` = d.`Invalid Tax Amount`	
;
update development_mx.DailyReport a
inner join (	
#Invalid Orders	
select Date, count(OrderNum) as 'Invalid Orders'	
from development_mx.Out_SalesReportOrder	
where OrderBeforeCan = 0	
group by Date	
) e	
on a.`Date Placed` = e.Date	
set a.`Invalid Orders` = e.`Invalid Orders`
;
update development_mx.DailyReport a
inner join (	
#Gross items with VAT	
select Date, sum(Price) as 'Gross Sales with VAT', count(ItemID) as 'Gross Items', Sum(Tax) as 'Gross Tax Amount',	
count(distinct SKUSimple) as 'Gross Unique Items'	
from development_mx.A_Master	
where OrderBeforeCan = 1	
group by Date	
) f
on a.`Date Placed` = f.Date	
set a.`Gross Sales with VAT` = f.`Gross Sales with VAT`, a.`Gross Items` = f.`Gross Items`, a.`Gross Tax Amount` = f.`Gross Tax Amount`,
a.`Gross Unique Items` = f.`Gross Unique Items`
;
update development_mx.DailyReport a
inner join (	
#Gross orders	
select Date, count(OrderNum) as 'Gross Orders'	
from development_mx.Out_SalesReportOrder	
where OrderBeforeCan = 1	
group by Date	
) g
on a.`Date Placed` = g.Date	
set a.`Gross Orders` = g.`Gross Orders`
;
update development_mx.DailyReport a
inner join (	
#Gross items w/o VAT	
select Date, sum(PriceAfterTax) as 'Gross Sales', sum(COGS) as 'Gross COGS', sum(CostAfterTax) as 'Gross Cost',	
sum(DeliveryCostSupplier) as 'Gross Inbound Cost'	
from development_mx.A_Master	
where OrderBeforeCan = 1	
group by Date	
) h	
on a.`Date Placed` = h.Date	
set a.`Gross Sales` = h.`Gross Sales`, a.`Gross COGS` = h.`Gross COGS`, a.`Gross Cost` = h.`Gross Cost`,
a.`Gross Inbound Cost` = h.`Gross Inbound Cost`
;
update development_mx.DailyReport a
inner join (
#Canceled Items	
select Date, sum(PriceAfterTax) as 'Canceled Sales', count(ItemID) as 'Canceled Items'	
from development_mx.A_Master	
where Cancelled = 1	
group by Date	
) i
on a.`Date Placed` = i.Date	
set a.`Canceled Sales` = i.`Canceled Sales`, a.`Canceled Items` = i.`Canceled Items`
;
update development_mx.DailyReport a
inner join (	
#Canceled Orders	
select Date, count(OrderNum) as 'Canceled Orders'	
from development_mx.Out_SalesReportOrder	
where Cancelled = 1	
group by Date	
) j	
on a.`Date Placed` = j.Date	
set a.`Canceled Orders` = j.`Canceled Orders`
;
update development_mx.DailyReport a
inner join (
#Rejected Items	
select Date, sum(PriceAfterTax) as 'Rejected Sales', count(ItemID) as 'Rejected Items'	
from development_mx.A_Master	
where Rejected = 1	
group by Date	
) k
on a.`Date Placed` = k.Date	
set a.`Rejected Sales` = k.`Rejected Sales`, a.`Rejected Items` = k.`Rejected Items`
;
update development_mx.DailyReport a
inner join (	
#Rejected Orders	
select Date, count(OrderNum) as 'Rejected Orders'	
from development_mx.Out_SalesReportOrder
where Rejected = 1	
group by Date	
) l	
on a.`Date Placed` = l.Date	
set a.`Rejected Orders` = l.`Rejected Orders`
;
update development_mx.DailyReport a
inner join (	
#Pending Items	
select Date, sum(PriceAfterTax) as 'Pending Sales', count(ItemID) as 'Pending Itmes'	
from development_mx.A_Master	
where Pending = 1	
group by Date	
) m
on a.`Date Placed` = m.Date
set a.`Pending Sales` = m.`Pending Sales`, a.`Pending Items` = m.`Pending Itmes`
;
update development_mx.DailyReport a
inner join (	
#Pending Orders	
select Date, count(OrderNum) as 'Pending Orders'	
from development_mx.Out_SalesReportOrder	
where Pending = 1	
group by Date	
) n	
on a.`Date Placed` = n.Date
set a.`Pending Orders` = n.`Pending Orders`
;
update development_mx.DailyReport a
inner join (	
#Post-Can & Pend Gross Items	
select Date, sum(PriceAfterTax) as 'Gross Sales Post', count(ItemID) as 'Gross Items Post', sum(COGS) as 'Gross COGS Post',	
sum(CostAfterTax) as 'Gross Cost Post', sum(DeliveryCostSupplier) as 'Gross Inbound Cost Post',	
count(distinct SKUSimple) as 'Gross Unique Items Post'	
from development_mx.A_Master	
where OrderBeforeCan = 1	
and Cancellations = 0	
and Pending = 0	
group by Date	
) o	
on a.`Date Placed` = o.Date	
set a.`Gross Sales Post` = o.`Gross Sales Post`, a.`Gross Items Post` = o.`Gross Items Post`, a.`Gross COGS Post` = o.`Gross COGS Post`,
a.`Gross Cost Post` = o.`Gross Cost Post`, a.`Gross Inbound Cost Post` = o.`Gross Inbound Cost Post`,
a.`Gross Unique Items Post` = o.`Gross Unique Items Post`
;
update development_mx.DailyReport a
inner join (
#Post-Can & Pend Gross Orders	
select Date, count(OrderNum) as 'Gross Orders Post'	
from development_mx.Out_SalesReportOrder	
where OrderBeforeCan = 1	
and Cancelled = 0	
and Rejected = 0	
and Pending = 0	
group by Date	
) p
on a.`Date Placed` = p.Date	
set a.`Gross Orders Post` = p.`Gross Orders Post`
;
update development_mx.DailyReport a
inner join (
#Returned Items	
select Date, sum(PriceAfterTax) as 'Returned Sales', count(ItemID) as 'Returned Items', sum(COGS) as 'Returned COGS'	
from development_mx.A_Master	
where `Returns` = 1	
group by Date	
) q
on a.`Date Placed` = q.Date	
set a.`Returned Sales` = q.`Returned Sales`, a.`Returned Items` = q.`Returned Items`, a.`Returned COGS` = q.`Returned COGS`
;
update development_mx.DailyReport a
inner join (
#Returned Orders	
select Date, count(OrderNum) as 'Returned Orders'	
from development_mx.Out_SalesReportOrder	
where `Returns` = 1	
group by Date	
) r	
on a.`Date Placed` = r.Date	
set a.`Returned Orders` = r.`Returned Orders`
;
update development_mx.DailyReport a
inner join (
#Net Sales Pre-Voucher	
select Date, sum(PriceAfterTax) as 'Net Sales Pre-Voucher'	
from development_mx.A_Master	
where OrderAfterCan = 1	
group by Date	
) s
on a.`Date Placed` = s.Date	
set a.`Net Sales Pre-Voucher` = s.`Net Sales Pre-Voucher`
;
update development_mx.DailyReport a
inner join (
#Voucher	
select Date, sum(CouponValueAfterTax) as 'Voucher Value', sum(Interest) as 'Net Interest'	
from development_mx.A_Master	
where OrderAfterCan = 1	
group by Date	
) t
on a.`Date Placed` = t.Date	
set a.`Voucher Value` = t.`Voucher Value`, a.`Net Interest` = t.`Net Interest`
;
update development_mx.DailyReport a
inner join (
#Product Net Sales & Net Revenue	
select Date, sum(PriceAfterTax - CouponValueAfterTax) as 'Product Net Sales', sum(ShippingFee) as 'Shipping Fee',	
sum(Rev) as 'Net Revenue', count(ItemID) as 'Net Items', sum(COGS) as 'Net COGS',
sum(ShippingCost) as 'Shipping Cost',	sum(CostAfterTax) as 'Net Cost',
sum(DeliveryCostSupplier) as 'Net Inbound Cost', count(distinct SKUSimple) as 'Net Unique Items',
SUM( OtherRev) AS `Other Rev`	
from development_mx.A_Master
where OrderAfterCan = 1	
group by Date
) u
on a.`Date Placed` = u.Date
set a.`Product Net Sales` = u.`Product Net Sales`, a.`Shipping Fee` = u.`Shipping Fee`, a.`Net Revenue` = u.`Net Revenue`,
a.`Net Items` = u.`Net Items`, a.`Net COGS` = u.`Net COGS`, a.`Shipping Cost` = u.`Shipping Cost`, a.`Net Cost` = u.`Net Cost`,
a.`Net Inbound Cost` = u.`Net Inbound Cost`, a.`Net Unique Items` = u.`Net Unique Items`, a.`Other Revenue` = u.`Other Rev`
;
update development_mx.DailyReport a
inner join (
#Net Orders	
select Date, count(OrderNum) as 'Net Orders'	
from development_mx.Out_SalesReportOrder	
where OrderAfterCan = 1	
group by Date	
) v	
on a.`Date Placed` = v.Date	
set a.`Net Orders` = v.`Net Orders`
;
update development_mx.DailyReport a
inner join (
#Delivered Revenue	
select DateDelivered, sum(Rev) as 'Delivered Revenue', count(ItemID) as 'Delivered Items', sum(COGS) as 'Delivered COGS',	
sum(ShippingCost) as 'Delivered Shipping Cost'	
from development_mx.A_Master	
group by DateDelivered	
) w
on a.`Date Placed` =	w.DateDelivered	
set a.`Delivered Revenue` = w.`Delivered Revenue`, a.`Delivered Items` = w.`Delivered Items`, a.`Delivered COGS` = w.`Delivered COGS`,
a.`Delivered Shipping Cost` = w.`Delivered Shipping Cost`
;
update development_mx.DailyReport a
inner join (
#Returned Revenue	
select DateReturned, sum(Rev) as 'Returned Revenue', count(ItemID) as 'Returned Items', sum(COGS) as 'Returned COGS',	
sum(ShippingCost) as 'Returned Shipping Cost'	
from development_mx.A_Master	
#where `Returns` = 1	
group by DateReturned	
) x
on a.`Date Placed` = x.DateReturned
set a.`Returned Date Revenue` = x.`Returned Revenue`, a.`Returned Date Items` = x.`Returned Items`,
a.`Returned Date COGS` = x.`Returned COGS`, a.`Returned Date Shipping Cost` = x.`Returned Shipping Cost`
;
update development_mx.DailyReport a
inner join (
#Acquisition Cost	
select a.date, sum(a.cost) - b.cost - c.cost as "Retention Cost", b.cost + c.cost as "Acquisition Cost"	
from production.retargeting_affiliates_mx a	
inner join marketing_report.sociomantic_fbx_loyalty_mx b	
inner join marketing_report.sociomantic_loyalty_mx c	
on a.date = b.date and b.date = c.date	
group by a.date	
order by a.date asc	
) y
on a.`Date Placed` = y.date	
set a.`Acquisition Cost` = y.`Acquisition Cost`	
;
update development_mx.DailyReport a
inner join (
#CBIT Vouchers for MKT Spend	
select Date, sum(CouponValueAfterTax) as 'CBIT Voucher'	
from development_mx.A_Master	
where CouponCode like '%cbit%'	
and OrderAfterCan = 1	
group by Date	
) z	
on a.`Date Placed` = z.Date	
set a.`Other MKT Spend` = z.`CBIT Voucher`
;
update development_mx.DailyReport a
inner join (
#Transaction Fee	
select x.Date, sum(coalesce(x.CollectedFee,0) - coalesce(y.RefundedFee,0)) as 'Total Transaction Fee'	
from (select Date, OrderNum, sum(TransactionFeeAfterTax) as 'CollectedFee'	
from development_mx.A_Master	
where Collected = 1	
and PaymentMethod in ('Amex_Gateway', 'Banorte_Payworks', 'Banorte_Payworks_Debit', 'Oxxo_Direct',
'Paypal_Express_Checkout', 'CreditCardOnDelivery_Payment')
and Date >= 20130701
group by Date, OrderNum) x	
left join (select Date, OrderNum, sum(TransactionFeeAfterTax) as 'RefundedFee'	
from development_mx.A_Master	
where `Status` = 'Refunded'	
and PaymentMethod in ('Amex_Gateway', 'Banorte_Payworks', 'Banorte_Payworks_Debit', 'Paypal_Express_Checkout')
and Date >= 20130701
group by Date, OrderNum) y	
using (Date, OrderNum)	
group by Date	
) a1	
on a.`Date Placed` = a1.Date	
set a.`Total Transaction Fee` = a1.`Total Transaction Fee`
;
update development_mx.DailyReport a
inner join (
#Installment Fee	
select Date, sum(InstallmentFeeAfterTax) as 'Installment Fee'	
from development_mx.A_Master	
where Collected = 1	
and (PaymentMethod in ('Amex_Gateway', 'Banorte_Payworks')
or (PaymentMethod = 'CreditCardOnDelivery_Payment' and Issuingbank <> 'AMEXBANK'))	
and Date >= 20130701	
group by Date	
) b1	
on a.`Date Placed` = b1.Date	
set a.`Installment Fee` = b1.`Installment Fee`
;
#update development_mx.DailyReport a
#inner join (
#FL costs	
#select x.Month_Num, (x.FL_WH_cost/y.Days) * x.exchange_rate as 'FL WH Cost', (x.FL_CS_cost/y.Days) * x.exchange_rate as 'FL CS Cost',
#(x.FL_total_cost/y.Days) * x.exchange_rate  as 'FL Total Cost'	
#from development_mx.A_E_BI_FLCosts x
#inner join test_linio.M1_MonthAux y
#on x.Month_Num = y.`Month`
#where Country = 'MEX'	
#) c1	
#on a.`Month`= c1.Month_Num	
#set a.`FL WH Cost` = c1.`FL WH Cost`, a.`FL CS Cost` = c1.`FL CS Cost`, a.`FL Total Cost` = c1.`FL Total Cost`	
#;
update development_mx.DailyReport a
inner join (
#Customers day	
select Date, count(distinct CustomerNum) as 'Total Customers', sum(if(OrderNum = First_Net_Order, 1, 0)) as 'New Customers'	
from development_mx.Out_SalesReportOrder	
where OrderAfterCan = 1	
group by Date	
) d1	
on a.`Date Placed` = d1.Date	
set a.`Total Customers` = d1.`Total Customers`, a.`New Customers` = d1.`New Customers`
;
update development_mx.DailyReport a
inner join (
#Per Category 1,1
select Date, sum(PriceAfterTax - CouponValueAfterTax + Interest) as 'Product Sales', sum(ShippingFee) as 'Shipping Fee',
sum(CostAfterTax -CreditNotes) as 'Cost', sum(DeliveryCostSupplier) as 'Inbound Cost', sum(ShippingCost) as 'Shipping Cost',
sum(PaymentFees) as 'Payment Fees', sum(FLWHCost) as 'WH Cost', sum(FLCSCost) as 'CS Cost'	
from development_mx.A_Master
where OrderAfterCan = 1
and CatBP like '1,1%'
group by Date
) e1
on a.`Date Placed` = e1.Date
set a.`1,1 Product Sales` = e1.`Product Sales`, a.`1,1 Shipping Fee` = e1.`Shipping Fee`, a.`1,1 Cost` = e1.Cost,
a.`1,1 Inbound Cost` = e1.`Inbound Cost`, a.`1,1 Shipping Cost` = e1.`Shipping Cost`, a.`1,1 Payment Fee` = e1.`Payment Fees`,
a.`1,1 WH Cost` = e1.`WH Cost`, a.`1,1 CS Cost` = e1.`CS Cost`
;
update development_mx.DailyReport a
inner join (
#Per Category 1,2
select Date, sum(PriceAfterTax - CouponValueAfterTax + Interest) as 'Product Sales', sum(ShippingFee) as 'Shipping Fee',
sum(CostAfterTax -CreditNotes) as 'Cost', sum(DeliveryCostSupplier) as 'Inbound Cost', sum(ShippingCost) as 'Shipping Cost',
sum(PaymentFees) as 'Payment Fees', sum(FLWHCost) as 'WH Cost', sum(FLCSCost) as 'CS Cost'	
from development_mx.A_Master
where OrderAfterCan = 1
and CatBP like '1,2%'
group by Date
) e1
on a.`Date Placed` = e1.Date
set a.`1,2 Product Sales` = e1.`Product Sales`, a.`1,2 Shipping Fee` = e1.`Shipping Fee`, a.`1,2 Cost` = e1.Cost,
a.`1,2 Inbound Cost` = e1.`Inbound Cost`, a.`1,2 Shipping Cost` = e1.`Shipping Cost`, a.`1,2 Payment Fee` = e1.`Payment Fees`,
a.`1,2 WH Cost` = e1.`WH Cost`, a.`1,2 CS Cost` = e1.`CS Cost`
;
update development_mx.DailyReport a
inner join (
#Per Category 1,3
select Date, sum(PriceAfterTax - CouponValueAfterTax + Interest) as 'Product Sales', sum(ShippingFee) as 'Shipping Fee',
sum(CostAfterTax -CreditNotes) as 'Cost', sum(DeliveryCostSupplier) as 'Inbound Cost', sum(ShippingCost) as 'Shipping Cost',
sum(PaymentFees) as 'Payment Fees', sum(FLWHCost) as 'WH Cost', sum(FLCSCost) as 'CS Cost'	
from development_mx.A_Master
where OrderAfterCan = 1
and CatBP like '1,3%'
group by Date
) e1
on a.`Date Placed` = e1.Date
set a.`1,3 Product Sales` = e1.`Product Sales`, a.`1,3 Shipping Fee` = e1.`Shipping Fee`, a.`1,3 Cost` = e1.Cost,
a.`1,3 Inbound Cost` = e1.`Inbound Cost`, a.`1,3 Shipping Cost` = e1.`Shipping Cost`, a.`1,3 Payment Fee` = e1.`Payment Fees`,
a.`1,3 WH Cost` = e1.`WH Cost`, a.`1,3 CS Cost` = e1.`CS Cost`
;
update development_mx.DailyReport a
inner join (
#Per Category 2,1
select Date, sum(PriceAfterTax - CouponValueAfterTax + Interest) as 'Product Sales', sum(ShippingFee) as 'Shipping Fee',
sum(CostAfterTax -CreditNotes) as 'Cost', sum(DeliveryCostSupplier) as 'Inbound Cost', sum(ShippingCost) as 'Shipping Cost',
sum(PaymentFees) as 'Payment Fees', sum(FLWHCost) as 'WH Cost', sum(FLCSCost) as 'CS Cost'	
from development_mx.A_Master
where OrderAfterCan = 1
and CatBP like '2,1%'
group by Date
) e1
on a.`Date Placed` = e1.Date
set a.`2,1 Product Sales` = e1.`Product Sales`, a.`2,1 Shipping Fee` = e1.`Shipping Fee`, a.`2,1 Cost` = e1.Cost,
a.`2,1 Inbound Cost` = e1.`Inbound Cost`, a.`2,1 Shipping Cost` = e1.`Shipping Cost`, a.`2,1 Payment Fee` = e1.`Payment Fees`,
a.`2,1 WH Cost` = e1.`WH Cost`, a.`2,1 CS Cost` = e1.`CS Cost`
;
update development_mx.DailyReport a
inner join (
#Per Category 2,2
select Date, sum(PriceAfterTax - CouponValueAfterTax + Interest) as 'Product Sales', sum(ShippingFee) as 'Shipping Fee',
sum(CostAfterTax -CreditNotes) as 'Cost', sum(DeliveryCostSupplier) as 'Inbound Cost', sum(ShippingCost) as 'Shipping Cost',
sum(PaymentFees) as 'Payment Fees', sum(FLWHCost) as 'WH Cost', sum(FLCSCost) as 'CS Cost'	
from development_mx.A_Master
where OrderAfterCan = 1
and CatBP like '2,2%'
group by Date
) e1
on a.`Date Placed` = e1.Date
set a.`2,2 Product Sales` = e1.`Product Sales`, a.`2,2 Shipping Fee` = e1.`Shipping Fee`, a.`2,2 Cost` = e1.Cost,
a.`2,2 Inbound Cost` = e1.`Inbound Cost`, a.`2,2 Shipping Cost` = e1.`Shipping Cost`, a.`2,2 Payment Fee` = e1.`Payment Fees`,
a.`2,2 WH Cost` = e1.`WH Cost`, a.`2,2 CS Cost` = e1.`CS Cost`
;
update development_mx.DailyReport a
inner join (
#Per Category 3,1
select Date, sum(PriceAfterTax - CouponValueAfterTax + Interest) as 'Product Sales', sum(ShippingFee) as 'Shipping Fee',
sum(CostAfterTax -CreditNotes) as 'Cost', sum(DeliveryCostSupplier) as 'Inbound Cost', sum(ShippingCost) as 'Shipping Cost',
sum(PaymentFees) as 'Payment Fees', sum(FLWHCost) as 'WH Cost', sum(FLCSCost) as 'CS Cost'	
from development_mx.A_Master
where OrderAfterCan = 1
and CatBP like '3,1%'
group by Date
) e1
on a.`Date Placed` = e1.Date
set a.`3,1 Product Sales` = e1.`Product Sales`, a.`3,1 Shipping Fee` = e1.`Shipping Fee`, a.`3,1 Cost` = e1.Cost,
a.`3,1 Inbound Cost` = e1.`Inbound Cost`, a.`3,1 Shipping Cost` = e1.`Shipping Cost`, a.`3,1 Payment Fee` = e1.`Payment Fees`,
a.`3,1 WH Cost` = e1.`WH Cost`, a.`3,1 CS Cost` = e1.`CS Cost`
;
update development_mx.DailyReport a
inner join (
#Per Category 4,1
select Date, sum(PriceAfterTax - CouponValueAfterTax + Interest) as 'Product Sales', sum(ShippingFee) as 'Shipping Fee',
sum(CostAfterTax -CreditNotes) as 'Cost', sum(DeliveryCostSupplier) as 'Inbound Cost', sum(ShippingCost) as 'Shipping Cost',
sum(PaymentFees) as 'Payment Fees', sum(FLWHCost) as 'WH Cost', sum(FLCSCost) as 'CS Cost'	
from development_mx.A_Master
where OrderAfterCan = 1
and CatBP like '4,1%'
group by Date
) e1
on a.`Date Placed` = e1.Date
set a.`4,1 Product Sales` = e1.`Product Sales`, a.`4,1 Shipping Fee` = e1.`Shipping Fee`, a.`4,1 Cost` = e1.Cost,
a.`4,1 Inbound Cost` = e1.`Inbound Cost`, a.`4,1 Shipping Cost` = e1.`Shipping Cost`, a.`4,1 Payment Fee` = e1.`Payment Fees`,
a.`4,1 WH Cost` = e1.`WH Cost`, a.`4,1 CS Cost` = e1.`CS Cost`
;
update development_mx.DailyReport a
inner join (
#Per Category 5,1
select Date, sum(PriceAfterTax - CouponValueAfterTax + Interest) as 'Product Sales', sum(ShippingFee) as 'Shipping Fee',
sum(CostAfterTax -CreditNotes) as 'Cost', sum(DeliveryCostSupplier) as 'Inbound Cost', sum(ShippingCost) as 'Shipping Cost',
sum(PaymentFees) as 'Payment Fees', sum(FLWHCost) as 'WH Cost', sum(FLCSCost) as 'CS Cost'	
from development_mx.A_Master
where OrderAfterCan = 1
and CatBP like '5,1%'
group by Date
) e1
on a.`Date Placed` = e1.Date
set a.`5,1 Product Sales` = e1.`Product Sales`, a.`5,1 Shipping Fee` = e1.`Shipping Fee`, a.`5,1 Cost` = e1.Cost,
a.`5,1 Inbound Cost` = e1.`Inbound Cost`, a.`5,1 Shipping Cost` = e1.`Shipping Cost`, a.`5,1 Payment Fee` = e1.`Payment Fees`,
a.`5,1 WH Cost` = e1.`WH Cost`, a.`5,1 CS Cost` = e1.`CS Cost`
;
update development_mx.DailyReport a
inner join (
#Per Category 5,2
select Date, sum(PriceAfterTax - CouponValueAfterTax + Interest) as 'Product Sales', sum(ShippingFee) as 'Shipping Fee',
sum(CostAfterTax -CreditNotes) as 'Cost', sum(DeliveryCostSupplier) as 'Inbound Cost', sum(ShippingCost) as 'Shipping Cost',
sum(PaymentFees) as 'Payment Fees', sum(FLWHCost) as 'WH Cost', sum(FLCSCost) as 'CS Cost'	
from development_mx.A_Master
where OrderAfterCan = 1
and CatBP like '5,2%'
group by Date
) e1
on a.`Date Placed` = e1.Date
set a.`5,2 Product Sales` = e1.`Product Sales`, a.`5,2 Shipping Fee` = e1.`Shipping Fee`, a.`5,2 Cost` = e1.Cost,
a.`5,2 Inbound Cost` = e1.`Inbound Cost`, a.`5,2 Shipping Cost` = e1.`Shipping Cost`, a.`5,2 Payment Fee` = e1.`Payment Fees`,
a.`5,2 WH Cost` = e1.`WH Cost`, a.`5,2 CS Cost` = e1.`CS Cost`
;
update development_mx.DailyReport a
inner join (
#Per Category 6,1
select Date, sum(PriceAfterTax - CouponValueAfterTax + Interest) as 'Product Sales', sum(ShippingFee) as 'Shipping Fee',
sum(CostAfterTax -CreditNotes) as 'Cost', sum(DeliveryCostSupplier) as 'Inbound Cost', sum(ShippingCost) as 'Shipping Cost',
sum(PaymentFees) as 'Payment Fees', sum(FLWHCost) as 'WH Cost', sum(FLCSCost) as 'CS Cost'	
from development_mx.A_Master
where OrderAfterCan = 1
and CatBP like '6,1%'
group by Date
) e1
on a.`Date Placed` = e1.Date
set a.`6,1 Product Sales` = e1.`Product Sales`, a.`6,1 Shipping Fee` = e1.`Shipping Fee`, a.`6,1 Cost` = e1.Cost,
a.`6,1 Inbound Cost` = e1.`Inbound Cost`, a.`6,1 Shipping Cost` = e1.`Shipping Cost`, a.`6,1 Payment Fee` = e1.`Payment Fees`,
a.`6,1 WH Cost` = e1.`WH Cost`, a.`6,1 CS Cost` = e1.`CS Cost`
;
update development_mx.DailyReport a
inner join (
#Per Category 7,1
select Date, sum(PriceAfterTax - CouponValueAfterTax + Interest) as 'Product Sales', sum(ShippingFee) as 'Shipping Fee',
sum(CostAfterTax -CreditNotes) as 'Cost', sum(DeliveryCostSupplier) as 'Inbound Cost', sum(ShippingCost) as 'Shipping Cost',
sum(PaymentFees) as 'Payment Fees', sum(FLWHCost) as 'WH Cost', sum(FLCSCost) as 'CS Cost'	
from development_mx.A_Master
where OrderAfterCan = 1
and CatBP like '7,1%'
group by Date
) e1
on a.`Date Placed` = e1.Date
set a.`7,1 Product Sales` = e1.`Product Sales`, a.`7,1 Shipping Fee` = e1.`Shipping Fee`, a.`7,1 Cost` = e1.Cost,
a.`7,1 Inbound Cost` = e1.`Inbound Cost`, a.`7,1 Shipping Cost` = e1.`Shipping Cost`, a.`7,1 Payment Fee` = e1.`Payment Fees`,
a.`7,1 WH Cost` = e1.`WH Cost`, a.`7,1 CS Cost` = e1.`CS Cost`
;
update development_mx.DailyReport a
inner join (
#Per Category 8,1
select Date, sum(PriceAfterTax - CouponValueAfterTax + Interest) as 'Product Sales', sum(ShippingFee) as 'Shipping Fee',
sum(CostAfterTax -CreditNotes) as 'Cost', sum(DeliveryCostSupplier) as 'Inbound Cost', sum(ShippingCost) as 'Shipping Cost',
sum(PaymentFees) as 'Payment Fees', sum(FLWHCost) as 'WH Cost', sum(FLCSCost) as 'CS Cost'	
from development_mx.A_Master
where OrderAfterCan = 1
and CatBP like '8,1%'
group by Date
) e1
on a.`Date Placed` = e1.Date
set a.`8,1 Product Sales` = e1.`Product Sales`, a.`8,1 Shipping Fee` = e1.`Shipping Fee`, a.`8,1 Cost` = e1.Cost,
a.`8,1 Inbound Cost` = e1.`Inbound Cost`, a.`8,1 Shipping Cost` = e1.`Shipping Cost`, a.`8,1 Payment Fee` = e1.`Payment Fees`,
a.`8,1 WH Cost` = e1.`WH Cost`, a.`8,1 CS Cost` = e1.`CS Cost`
;
update development_mx.DailyReport a
inner join (
#Per Category 9,1
select Date, sum(PriceAfterTax - CouponValueAfterTax + Interest) as 'Product Sales', sum(ShippingFee) as 'Shipping Fee',
sum(CostAfterTax -CreditNotes) as 'Cost', sum(DeliveryCostSupplier) as 'Inbound Cost', sum(ShippingCost) as 'Shipping Cost',
sum(PaymentFees) as 'Payment Fees', sum(FLWHCost) as 'WH Cost', sum(FLCSCost) as 'CS Cost'	
from development_mx.A_Master
where OrderAfterCan = 1
and CatBP like '9,1%'
group by Date
) e1
on a.`Date Placed` = e1.Date
set a.`9,1 Product Sales` = e1.`Product Sales`, a.`9,1 Shipping Fee` = e1.`Shipping Fee`, a.`9,1 Cost` = e1.Cost,
a.`9,1 Inbound Cost` = e1.`Inbound Cost`, a.`9,1 Shipping Cost` = e1.`Shipping Cost`, a.`9,1 Payment Fee` = e1.`Payment Fees`,
a.`9,1 WH Cost` = e1.`WH Cost`, a.`9,1 CS Cost` = e1.`CS Cost`
;


update development_mx.DailyReport a
inner join (
#Per Category 9,2
select Date, sum(PriceAfterTax - CouponValueAfterTax + Interest) as 'Product Sales', sum(ShippingFee) as 'Shipping Fee',
sum(CostAfterTax -CreditNotes) as 'Cost', sum(DeliveryCostSupplier) as 'Inbound Cost', sum(ShippingCost) as 'Shipping Cost',
sum(PaymentFees) as 'Payment Fees', sum(FLWHCost) as 'WH Cost', sum(FLCSCost) as 'CS Cost'	
from development_mx.A_Master
where OrderAfterCan = 1
and (
   Cat1 LIKE "%INTANGIBLES%" OR
   Cat2 LIKE "%INTANGIBLES%" OR
   Cat3 LIKE "%INTANGIBLES%" OR

   Cat1 LIKE "%GARANT%" OR
   Cat2 LIKE "%GARANT%" OR
   Cat3 LIKE "%GARANT%" OR

   Cat1 LIKE "%TIEMPO%AIRE%" OR
   Cat2 LIKE "%TIEMPO%AIRE%" OR
   Cat3 LIKE "%TIEMPO%AIRE%" OR

   Cat1 LIKE "%TIEMPO%AIRE%" OR
   Cat2 LIKE "%TIEMPO%AIRE%" OR
   Cat3 LIKE "%TIEMPO%AIRE%" OR

   Cat1 LIKE "%Tarjetas%regalo%" OR
   Cat2 LIKE "%Tarjetas%regalo%" OR
   Cat3 LIKE "%Tarjetas%regalo%"
    )
group by Date
) e1
on a.`Date Placed` = e1.Date
set a.`9,2 Product Sales` = e1.`Product Sales`, a.`9,2 Shipping Fee` = e1.`Shipping Fee`, a.`9,2 Cost` = e1.Cost,
a.`9,2 Inbound Cost` = e1.`Inbound Cost`, a.`9,2 Shipping Cost` = e1.`Shipping Cost`, a.`9,2 Payment Fee` = e1.`Payment Fees`,
a.`9,2 WH Cost` = e1.`WH Cost`, a.`9,2 CS Cost` = e1.`CS Cost`
;

update development_mx.DailyReport a
inner join (
select date, sum(cost) as 'MKTcost'
from marketing_report.global_report
where country = 'Mexico'
group by date
) f1
on a.`Date Placed` = f1.date
set a.`MKT Cost` = f1.MKTcost
;
update development_mx.DailyReport a
inner join (
# `MP Unique SKUs`
select Date, count(distinct sku_simple) as `MP Unique SKUs`, count(sku_simple) as `MP Items`, sum(Rev) as `MP Revenue`,
    sum(Commission) as `MP CommRev`, count(distinct Supplier) as `MP Suppliers`,
    sum( Cost_after_tax) AS `MP Cost`
from development_mx.M1_Market_Place
group by Date
) g1
on a.`Date Placed` = g1.Date
set a.`MP Unique SKUs` = g1.`MP Unique SKUs`, a.`MP Items` = g1.`MP Items`, a.`MP Revenue` = g1.`MP Revenue`,
  a.`MP CommRev` = g1.`MP CommRev`, a.`MP Suppliers` = g1.`MP Suppliers`, a.`MP Cost` = g1.`MP Cost`
;

update development_mx.DailyReport a
inner join (
select Date, sum(PaymentFees) as 'Payment Fee'
from development_mx.A_Master
where OrderAfterCan = 1
group by Date
) h1
on a.`Date Placed` = h1.Date
set a.`Payment Fee` = h1.`Payment Fee`
;
update development_mx.DailyReport a
inner join (
select Date, sum(if(OrderNum = First_Gross_Order, 1, 0)) as 'Gross Customers'
from development_mx.Out_SalesReportOrder
where OrderBeforeCan = 1
group by Date
) i1
on a.`Date Placed` = i1.Date
set a.`Gross Customers` = i1.`Gross Customers`
;

update development_mx.DailyReport a
inner join (
      select Date, sum(if(OrderNum = First_Gross_Order, 1, 0)) as 'Gross Cancelled Customers'
      from development_mx.Out_SalesReportOrder
      where OrderBeforeCan = 1
       and Cancelled = 1
      group by Date
      ) j1
on a.`Date Placed` = j1.Date
set a.`Gross Cancelled Customers` = j1.`Gross Cancelled Customers`
;
update development_mx.DailyReport a
inner join (
      select Date, sum(if(OrderNum = First_Gross_Order, 1, 0)) as 'Gross Rejected Customers'
      from development_mx.Out_SalesReportOrder
      where OrderBeforeCan = 1
       and Rejected = 1
      group by Date
      ) k1
on a.`Date Placed` = k1.Date
set a.`Gross Rejected Customers` = k1.`Gross Rejected Customers`
;
update development_mx.DailyReport a
inner join (
      select Date, sum(if(OrderNum = First_Gross_Order, 1, 0)) as 'Gross Returned Customers'
      from development_mx.Out_SalesReportOrder
      where OrderBeforeCan = 1
       and `Returns` = 1
      group by Date
      ) l1
on a.`Date Placed` = l1.Date
set a.`Gross Returned Customers` = l1.`Gross Returned Customers`
;

update development_mx.DailyReport a
inner join (
      select Date, sum(if(OrderNum = First_Gross_Order, 1, 0)) as 'Gross Cancelled Customers'
      from development_mx.Out_SalesReportOrder
      where OrderBeforeCan = 1
       and Cancelled = 1
      group by Date
      ) j1
on a.`Date Placed` = j1.Date
set a.`Gross Cancelled Customers` = j1.`Gross Cancelled Customers`
;
update development_mx.DailyReport a
inner join (
      select Date, sum(if(OrderNum = First_Gross_Order, 1, 0)) as 'Gross Rejected Customers'
      from development_mx.Out_SalesReportOrder
      where OrderBeforeCan = 1
       and Rejected = 1
      group by Date
      ) k1
on a.`Date Placed` = k1.Date
set a.`Gross Rejected Customers` = k1.`Gross Rejected Customers`
;
update development_mx.DailyReport a
inner join (
      select Date, sum(if(OrderNum = First_Gross_Order, 1, 0)) as 'Gross Returned Customers'
      from development_mx.Out_SalesReportOrder
      where OrderBeforeCan = 1
       and `Returns` = 1
      group by Date
      ) l1
on a.`Date Placed` = l1.Date
set a.`Gross Returned Customers` = l1.`Gross Returned Customers`
;

Update development_mx.DailyReport a
inner join (
         select MonthNum , Cost / date_format( last_day( date_format( CONCAT( MonthNum , "01" )  , "%Y%m%d" ) ) , "%d"  ) as 'Daily Sales Force'
      from development_mx.M_Mkt_AdditionalCost
      where Country = "MEX"
     
           ) m1
ON a.Month = m1.MonthNum
SET
   a.`Sales Force Cost` = m1.`Daily Sales Force` 
;

update development_mx.DailyReport a
inner join (
      select Date,  Mex as 'Chargebacks'
      from test_linio.M_Chargebacks
         ) n1
on a.`Date Placed` = n1.Date
set a.`Chargebacks` = n1.`Chargebacks`
;

update development_mx.DailyReport a
inner join (
#FL_WH cost
select x.MonthNum, (x.value/y.Days) as 'FL WH Cost' 
from 
(select MonthNum, Country, TypeCost, value, max(updatedAt)
from development_mx.M_Costs
group by MonthNum, Country, TypeCost) x
inner join test_linio.M1_MonthAux y
on x.MonthNum = y.`Month`
where x.Country = 'MEX'
and x.TypeCost =  'FL_WH_Cost'
) n1 
on a.`Month`= n1.MonthNum 
set a.`FL WH Cost` = n1.`FL WH Cost` 
;

update development_mx.DailyReport a
inner join (
#FL_CS cost
select x.MonthNum, (x.value/y.Days) as 'FL CS Cost' 
from 
(select MonthNum, Country, TypeCost, value, max(updatedAt)
from development_mx.M_Costs
group by MonthNum, Country, TypeCost) x
inner join test_linio.M1_MonthAux y
on x.MonthNum = y.`Month`
where x.Country = 'MEX'
and x.TypeCost =  'FL_CS_Cost'
) o1 
on a.`Month`= o1.MonthNum 
set a.`FL CS Cost` = o1.`FL CS Cost` 
;
update development_mx.DailyReport a
#FL Total cost
set a.`FL Total Cost` = a.`FL WH Cost` + a.`FL CS Cost`
;

update development_mx.DailyReport a
inner join (
      select Date, Mex
      from test_linio.M_IL_ShipCost
) p1
on a.`Date Placed` = p1.Date
set a.`IL Shipping Cost` = p1.`Mex`
;

update development_mx.DailyReport a
inner join (
      select Date, Mex
      from test_linio.M_IL_Cost
) q1
on a.`Date Placed` = q1.Date
set a.`IL Quarantine Cost` = q1.`Mex`
;


update development_mx.DailyReport c
inner join (
select a.MonthNum, a.date, a.DeliveredRev as 'DeliveredRev', b.Revenue as 'Net+Ret_Revenue'
from
(
select MonthNum, date, sum(if(DateDelivered > 0, Rev,0)) as 'DeliveredRev'
from development_mx.A_Master
group by Date
) a
inner join
(
select MonthNum, date, sum(Rev) as 'Revenue'
from development_mx.A_Master
where OrderAfterCan = 1
or `Returns` = 1
group by Date
) b
on a.date = b.date
) as r1
on c.`Date Placed` = r1.Date
set 
  c.`DeliveredRev`    = r1.`DeliveredRev`,
  c.`Net+Ret_Revenue` = r1.`Net+Ret_Revenue`
;

update development_mx.DailyReport c
inner join (
select Date, sum(Rev) as Rev
from development_mx.A_Master
where OrderBeforeCan = 1
group by Date
) as s1
on c.`Date Placed` = s1.date
SET
  c.`Gross Revenue (for Estimation)` = s1.Rev
;

update development_mx.DailyReport c
inner join (
select a.Date, count(a.OrderNum) As Orders
from
(select distinct Date, OrderNum, sum(PCOnePFive) as 'PC1-5'
from development_mx.A_Master
where OrderAfterCan = 1
group by Date, OrderNum) a
where a.`PC1-5` > 0
group by a.Date
) as t1
on c.`Date Placed` = t1.date
SET
  c.`% Positive PC 1.5 Orders` = t1.Orders
;



DELETE FROM development_mx.DailyReport WHERE `Date Placed` <= "2012-05-08";

