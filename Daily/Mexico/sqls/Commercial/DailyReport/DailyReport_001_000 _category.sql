drop table if exists development_mx.DailyReport_USD_Category	
;	
create table development_mx.DailyReport_USD_Category	
(`CatBP` varchar(50) not null, `Month` int not null,
`Visits` int not null, `Placed Sales` decimal (15,4) not null, `Placed Items` int not null, `Placed Unique Items` int not null,`Placed Tax Amount` decimal (15,4) not null,
`Placed Orders` int not null, `Invalid Sales` decimal (15,4), `Invalid Items` int not null,	
`Invalid Tax Amount` decimal (15,4) not null, `Invalid Orders` int not null, `Gross Sales with VAT` decimal (15,4) not null,	
`Gross Items` int not null, `Gross Tax Amount` decimal (15,4) not null, `Gross Unique Items` int not null, `Gross Orders` int not null,	
`Gross Sales` decimal (15,4) not null, `Gross COGS` decimal (15,4) not null, `Gross Cost` decimal (15,4) not null,	
`Gross Inbound Cost` decimal (15,4) not null, `Canceled Sales` decimal (15,4) not null, `Canceled Items` int not null,	
`Canceled Orders` int not null, `Rejected Sales` decimal(15,4) not null, `Rejected Items` int not null, `Rejected Orders` int not null,	
`Pending Sales` decimal (15,4) not null, `Pending Items` int not null, `Pending Orders` int not null,	
`Gross Sales Post` decimal (15,4) not null, `Gross Items Post` int not null, `Gross COGS Post` decimal (15,4) not null,	
`Gross Cost Post` decimal (15,4) not null, `Gross Inbound Cost Post` decimal (15,4) not null, `Gross Unique Items Post` int not null,	
`Gross Orders Post` int not null, `Returned Sales` decimal (15,4) not null, `Returned Items` int not null,	
`Returned COGS` decimal (15,4) not null, `Returned Orders` int not null, `Net Sales Pre-Voucher` decimal (15,4) not null,	
`Voucher Value` decimal (15,4) not null, `Product Net Sales` decimal (15,4) not null,	`Net Interest` decimal (15,4) not null,	
`Shipping Fee` decimal (15,4) not null, `Other Revenue` decimal (15,4) not null, `MP Revenue` decimal (15,4) not null,
`MP Commission` decimal (15,4) not null,`Net Revenue` decimal (15,4) not null,`Net Items` int not null, `Net COGS` decimal (15,4) not null,
`MP Cost` decimal(15,4) not null,`Shipping Cost` decimal (15,4) not null,
`Net Cost` decimal (15, 4) not null, `Net Inbound Cost` decimal (15,4) not null, `Net Unique Items` int not null,
`Net Orders` int not null, `Delivered Revenue` decimal (15,4) not null, `Delivered Items` int not null,
`Delivered COGS` decimal (15,4) not null, `Delivered Shipping Cost` decimal (15,4) not null,
`Returned Date Revenue` decimal (15,4) not null, `Returned Date Items` int not null, `Returned Date COGS` decimal (15,4) not null,
`Returned Date Shipping Cost` decimal (15,4) not null, `Retention Cost` decimal (15,4) not null,
`Acquisition Cost` decimal (15,4) not null, `Other MKT Spend` decimal (15,4) not null, `Total Transaction Fee` decimal (15,4) not null,
`Payment Fee` decimal (15,4) not null,`Installment Fee` decimal (15,4) not null, `FL WH Cost` decimal (15,4) not null,`FL CS Cost` decimal (15,4) not null,
`FL Total Cost` decimal (15,4) not null, `Total Customers` int not null, `New Customers` int not null,
`MKT cost` decimal (15,4) not null, `MP Unique SKUs` int not null, `MP Items` int not null,  `MP CommRev` decimal (15,4) not null, `MP Suppliers` int not null,
`MP SKUs from Suppliers` int not null, `Gross Customers` int not null,`Gross Cancelled Customers` int not null,
`Gross Rejected Customers` int not null,`Gross Returned Customers` int not null
)
;	
#-------------------------------------------------------------------------------------------------
INSERT INTO development_mx.DailyReport_USD_Category (
	`CatBP`,
	`Month`
)
SELECT
	catBP,
	MonthNum
FROM
	development_mx.M_CategoryKPI,
	development_mx.M_Month
WHERE
	MonthNum BETWEEN date_format(
		date(now()) - INTERVAL 13 MONTH,
		'%Y%m'
	)
AND date_format(
	date(now()) - INTERVAL 1 DAY,
	'%Y%m'
)
ORDER BY
	CatBP,
	MonthNum;
#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	SELECT
		CatBP,
		MonthNum,
		sum(Price) AS 'Placed Sales',
		count(ItemId) AS 'Placed Items',
		count(DISTINCT SKUSimple) AS 'Placed Unique Items',
		count(DISTINCT OrderNum) AS 'Placed Orders',
		sum(Tax) AS 'Placed Tax Amount'
	FROM
		regional.A_Master
	GROUP BY
		CatBP,
		MonthNum
) d ON a.`Month` = d.MonthNum
AND a.CatBP = d.CatBP
SET a.`Placed Sales` = d.`Placed Sales`,
 a.`Placed Items` = d.`Placed Items`,
 a.`Placed Unique Items` = d.`Placed Unique Items`,
 a.`Placed Orders` = d.`Placed Orders`,
 a.`Placed Tax Amount` = d.`Placed Tax Amount`;
	
#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	#Invalid Items	
	SELECT
		CatBP,
		MonthNum,
		sum(Price) AS 'Invalid Sales',
		count(ItemID) AS 'Invalid Items',
		Sum(Tax) AS 'Invalid Tax Amount'
	FROM
		regional.A_Master
	WHERE
		OrderBeforeCan = 0
	GROUP BY
		CatBP,MonthNum
) d ON a.`Month` = d.MonthNum AND a.CatBP = d.CatBP
SET a.`Invalid Sales` = d.`Invalid Sales`,
 a.`Invalid Items` = d.`Invalid Items`,
 a.`Invalid Tax Amount` = d.`Invalid Tax Amount`;
#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	#Invalid Orders	
	SELECT
		CatBP,
		MonthNum,
		count(DISTINCT(OrderNum)) AS 'Invalid Orders'
	FROM
		regional.A_Master
	WHERE
		OrderBeforeCan = 0
	GROUP BY
		CatBP,MonthNum
) e ON a.`Month` = e.MonthNum and a.CatBP = e.CatBP
SET a.`Invalid Orders` = e.`Invalid Orders`;
#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	#Gross items with VAT	
	SELECT
		CatBP,
		MonthNum,
		sum(Price) AS 'Gross Sales with VAT',
		count(ItemID) AS 'Gross Items',
		Sum(Tax) AS 'Gross Tax Amount',
		count(DISTINCT SKUSimple) AS 'Gross Unique Items'
	FROM
		regional.A_Master
	WHERE
		OrderBeforeCan = 1
	GROUP BY
		CatBP,
		MonthNum
) f ON a.`Month` = f.MonthNum and a.CatBP = f.CatBP
SET a.`Gross Sales with VAT` = f.`Gross Sales with VAT`,
 a.`Gross Items` = f.`Gross Items`,
 a.`Gross Tax Amount` = f.`Gross Tax Amount`,
 a.`Gross Unique Items` = f.`Gross Unique Items`;
#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	#Gross orders	
	SELECT
		CatBP,
		MonthNum,
		count(DISTINCT(OrderNum)) AS 'Gross Orders'
	FROM
		regional.A_Master
	WHERE
		OrderBeforeCan = 1
	GROUP BY
		CatBP,MonthNum
) g ON a.`Month` = g.MonthNum and a.CatBP = g.CatBP
SET a.`Gross Orders` = g.`Gross Orders`;
#-------------------------------------------------------------------------------------------------

UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	#Gross items w/o VAT	
	SELECT
		CatBP,
		MonthNum,
		sum(PriceAfterTax) AS 'Gross Sales',
		sum(COGS) AS 'Gross COGS',
		sum(CostAfterTax) AS 'Gross Cost',
		sum(DeliveryCostSupplier) AS 'Gross Inbound Cost'
	FROM
		regional.A_Master
	WHERE
		OrderBeforeCan = 1
	GROUP BY
		CatBP,
		MonthNum
) h ON a.`Month` = h.MonthNum and a.CatBP = h.CatBP
SET a.`Gross Sales` = h.`Gross Sales`,
 a.`Gross COGS` = h.`Gross COGS`,
 a.`Gross Cost` = h.`Gross Cost`,
 a.`Gross Inbound Cost` = h.`Gross Inbound Cost`;
#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	#Canceled Items	
	SELECT
		CatBP,
		MonthNum,
		sum(PriceAfterTax) AS 'Canceled Sales',
		count(ItemID) AS 'Canceled Items'
	FROM
		regional.A_Master
	WHERE
		Cancelled = 1
	GROUP BY
		CatBP,
		MonthNum
) i ON a.`Month` = i.MonthNum and a.CatBP = i.CatBP
SET a.`Canceled Sales` = i.`Canceled Sales`,
 a.`Canceled Items` = i.`Canceled Items`;
#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	#Canceled Orders	
	SELECT
		CatBP,
		MonthNum,
		count(DISTINCT(OrderNum)) AS 'Canceled Orders'
	FROM
		regional.A_Master
	WHERE
		Cancelled = 1
	GROUP BY
		CatBP,MonthNum
) j ON a.`Month` = j.MonthNum and a.CatBP = j.CatBP
SET a.`Canceled Orders` = j.`Canceled Orders`;
#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	#Rejected Items	
	SELECT
		CatBP,
		MonthNum,
		sum(PriceAfterTax) AS 'Rejected Sales',
		count(ItemID) AS 'Rejected Items'
	FROM
		regional.A_Master
	WHERE
		Rejected = 1
	GROUP BY
		CatBP,
		MonthNum
) k ON a.`Month` = k.MonthNum and a.CatBP = k.CatBP
SET a.`Rejected Sales` = k.`Rejected Sales`,
 a.`Rejected Items` = k.`Rejected Items`;
#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	#Rejected Orders	
	SELECT
		CatBP,
		MonthNum,
		count( DISTINCT(OrderNum)) AS 'Rejected Orders'
	FROM
		regional.A_Master
	WHERE
		Rejected = 1
	GROUP BY
		CatBP,MonthNum
) l ON a.`Month` = l.MonthNum and a.CatBP= l.CatBP
SET a.`Rejected Orders` = l.`Rejected Orders`;
#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	SELECT
		CatBP,
		MonthNum,
		sum(PriceAfterTax) AS 'Pending Sales',
		count(ItemID) AS 'Pending Itmes'
	FROM
		regional.A_Master
	WHERE
		Pending = 1
	GROUP BY
		CatBP,
		MonthNum
) m ON a.`Month` = m.MonthNum and a.CatBP = m.CatBP
SET a.`Pending Sales` = m.`Pending Sales`,
 a.`Pending Items` = m.`Pending Itmes`;
 
#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	#Pending Orders	
	SELECT
		CatBP,
		MonthNum,
		count(DISTINCT(OrderNum)) AS 'Pending Orders'
	FROM
		regional.A_Master
	WHERE
		Pending = 1
	GROUP BY
		CatBP,MonthNum
) n ON a.`Month` = n.MonthNum and a.CatBP = n.CatBP
SET a.`Pending Orders` = n.`Pending Orders`;
#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	#Post-Can & Pend Gross Items	
	SELECT
		CatBP,
		MonthNum,
		sum(PriceAfterTax) AS 'Gross Sales Post',
		count(ItemID) AS 'Gross Items Post',
		sum(COGS) AS 'Gross COGS Post',
		sum(CostAfterTax) AS 'Gross Cost Post',
		sum(DeliveryCostSupplier) AS 'Gross Inbound Cost Post',
		count(DISTINCT SKUSimple) AS 'Gross Unique Items Post'
	FROM
		regional.A_Master
	WHERE
		OrderBeforeCan = 1
	AND Cancellations = 0
	AND Pending = 0
	GROUP BY
		CatBP,MonthNum
) o ON a.`Month` = o.MonthNum and a.CatBP = o.CatBP
SET a.`Gross Sales Post` = o.`Gross Sales Post`,
 a.`Gross Items Post` = o.`Gross Items Post`,
 a.`Gross COGS Post` = o.`Gross COGS Post`,
 a.`Gross Cost Post` = o.`Gross Cost Post`,
 a.`Gross Inbound Cost Post` = o.`Gross Inbound Cost Post`,
 a.`Gross Unique Items Post` = o.`Gross Unique Items Post`;
#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	#Post-Can & Pend Gross Orders	
	SELECT
		CatBP,
		MonthNum,
		count(DISTINCT(OrderNum)) AS 'Gross Orders Post'
	FROM
		regional.A_Master
	WHERE
		OrderBeforeCan = 1
	AND Cancelled = 0
	AND Rejected = 0
	AND Pending = 0
	GROUP BY
		CatBP,MonthNum
) p ON a.`Month` = p.MonthNum and a.CatBP = p.CatBP
SET a.`Gross Orders Post` = p.`Gross Orders Post`;
#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	#Returned Items	
	SELECT
		CatBP,
		MonthNum,
		sum(PriceAfterTax) AS 'Returned Sales',
		count(ItemID) AS 'Returned Items',
		sum(COGS) AS 'Returned COGS'
	FROM
		regional.A_Master
	WHERE
		`Returns` = 1
	GROUP BY
		CatBP,
		MonthNum
) q ON a.`Month` = q.MonthNum and a.CatBP = q.CatBP
SET a.`Returned Sales` = q.`Returned Sales`,
 a.`Returned Items` = q.`Returned Items`,
 a.`Returned COGS` = q.`Returned COGS`;
#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	#Returned Orders	
	SELECT
		CatBP,
		MonthNum,
		count(DISTINCT(OrderNum)) AS 'Returned Orders'
	FROM
		regional.A_Master
	WHERE
		`Returns` = 1
	GROUP BY
		CatBP,MonthNum
) r ON a.`Month` = r.MonthNum and a.CatBP = r.CatBP
SET a.`Returned Orders` = r.`Returned Orders`;
#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	#Net Sales Pre-Voucher	
	SELECT
		CatBP,
		MonthNum,
		sum(PriceAfterTax) AS 'Net Sales Pre-Voucher'
	FROM
		regional.A_Master
	WHERE
		OrderAfterCan = 1
	GROUP BY
		CatBP,
		MonthNum
) s ON a.`Month` = s.MonthNum and a.CatBP = s.CatBP
SET a.`Net Sales Pre-Voucher` = s.`Net Sales Pre-Voucher`;
#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	#Voucher	
	SELECT
		CatBP,
		MonthNum,
		sum(CouponValueAfterTax) AS 'Voucher Value',
		sum(Interest) AS 'Net Interest'
	FROM
		regional.A_Master
	WHERE
		OrderAfterCan = 1
	GROUP BY
		CatBP,
		MonthNum
) t ON a.Month = t.MonthNum and a.CatBP = t.CatBP
SET a.`Voucher Value` = t.`Voucher Value`,
 a.`Net Interest` = t.`Net Interest`;
#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	#Product Net Sales & Net Revenue	
	SELECT
		CatBP,
		MonthNum,
		sum(
			PriceAfterTax - CouponValueAfterTax
		) AS 'Product Net Sales',
		sum(ShippingFee) AS 'Shipping Fee',
		sum(Rev) AS 'Net Revenue',
		count(ItemID) AS 'Net Items',
		sum(COGS) AS 'Net COGS',
		sum(ShippingCost) AS 'Shipping Cost',
		#Aqui va ShippingCostEstimated
		sum(CostAfterTax) AS 'Net Cost',
		sum(DeliveryCostSupplier) AS 'Net Inbound Cost',
		count(DISTINCT SKUSimple) AS 'Net Unique Items',
		SUM(OtherRev) AS `Other Rev`
	FROM
		regional.A_Master
	WHERE
		OrderAfterCan = 1
	GROUP BY
		CatBP,
		MonthNum
) u ON a.`Month` = u.MonthNum and a.CatBP = u.CatBP
SET a.`Product Net Sales` = u.`Product Net Sales`,
 a.`Shipping Fee` = u.`Shipping Fee`,
 a.`Net Revenue` = u.`Net Revenue`,
 a.`Net Items` = u.`Net Items`,
 a.`Net COGS` = u.`Net COGS`,
 a.`Shipping Cost` = u.`Shipping Cost`,
 a.`Net Cost` = u.`Net Cost`,
 a.`Net Inbound Cost` = u.`Net Inbound Cost`,
 a.`Net Unique Items` = u.`Net Unique Items`,
 a.`Other Revenue` = u.`Other Rev`;
#-------------------------------------------------------------------------------------------------

#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	# `MP Unique SKUs`
	SELECT
		Month_Num,
		Category_BP,
		count(DISTINCT sku_simple) AS `MP Unique SKUs`,
		count(sku_simple) AS `MP Items`,
		sum(Rev) AS `MP Revenue`,
		sum(Commission) AS `MP CommRev`,
		count(DISTINCT Supplier) AS `MP Suppliers`,
		sum(Cost_after_tax) AS `MP Cost`
	FROM
		development_mx.M1_Market_Place
	GROUP BY
		Category_BP,Month_Num
) g1 ON a.`Month` = g1.Month_Num and a.CatBP = g1.Category_BP
SET a.`MP Unique SKUs` = g1.`MP Unique SKUs`,
 a.`MP Items` = g1.`MP Items`,
 a.`MP Revenue` = g1.`MP Revenue`,
 a.`MP CommRev` = g1.`MP CommRev`,
 a.`MP Suppliers` = g1.`MP Suppliers`,
 a.`MP Cost` = g1.`MP Cost`;
  
#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	#Net Orders	
	SELECT
		CatBP,
		MonthNum,
		count(DISTINCT(OrderNum)) AS 'Net Orders'
	FROM
		regional.A_Master
	WHERE
		OrderAfterCan = 1
	GROUP BY
		CatBP,MonthNum
) v ON a.`Month` = v.MonthNum and a.CatBP = v.CatBP
SET a.`Net Orders` = v.`Net Orders`;
#-------------------------------------------------------------------------------------------------

UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	#Delivered Revenue	
	SELECT
		CatBP,
		MonthNum,
		sum(Rev) AS 'Delivered Revenue',
		count(ItemID) AS 'Delivered Items',
		sum(COGS) AS 'Delivered COGS',
		sum(ShippingCost) AS 'Delivered Shipping Cost'
	FROM
		regional.A_Master
	GROUP BY
		CatBP,MonthNum
) w ON a.`Month` = w.MonthNum and a.CatBP = w.CatBP
SET a.`Delivered Revenue` = w.`Delivered Revenue`,
 a.`Delivered Items` = w.`Delivered Items`,
 a.`Delivered COGS` = w.`Delivered COGS`,
 a.`Delivered Shipping Cost` = w.`Delivered Shipping Cost`;
 
#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	#Returned Revenue	
	SELECT
		CatBP,
		MonthNum,
		sum(Rev) AS 'Returned Revenue',
		count(ItemID) AS 'Returned Items',
		sum(COGS) AS 'Returned COGS',
		sum(ShippingCost) AS 'Returned Shipping Cost',
		DATE_FORMAT(DateReturned,'%Y%m') as 'MonthNumReturned'
	FROM
		regional.A_Master #where `Returns` = 1	
	GROUP BY
		CatBP,MonthNumReturned
) x ON a.`Month` = x.MonthNumReturned and a.CatBP = x.catBP
SET a.`Returned Date Revenue` = x.`Returned Revenue`,
 a.`Returned Date Items` = x.`Returned Items`,
 a.`Returned Date COGS` = x.`Returned COGS`,
 a.`Returned Date Shipping Cost` = x.`Returned Shipping Cost`;
#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	SELECT
		CatBP,
		MonthNum,
		sum(PaymentFees) AS 'Payment Fee'
	FROM
		regional.A_Master
	WHERE
		OrderAfterCan = 1
	GROUP BY
		CatBP,MonthNum
) h1 ON a.`Month` = h1.MonthNum and a.CatBP = h1.CatBP
SET a.`Payment Fee` = h1.`Payment Fee`;
#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category a
INNER JOIN (
	#Installment Fee	
	SELECT
		CatBP,
		MonthNum,
		sum(InstallmentFeeAfterTax) AS 'Installment Fee'
	FROM
		regional.A_Master
	WHERE
		Collected = 1
	AND (
		PaymentMethod IN (
			'Amex_Gateway',
			'Banorte_Payworks'
		)
		OR (
			PaymentMethod = 'CreditCardOnDelivery_Payment'
			AND Issuingbank <> 'AMEXBANK'
		)
	)
	AND Date >= 20130701
	GROUP BY
		CatBP, MonthNum
) b1 ON a.`Month` = b1.MonthNum and a.CatBP = b1.CatBP
SET a.`Installment Fee` = b1.`Installment Fee`;
#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category c
INNER JOIN (
	SELECT
		CatBP,
		MonthNum,
		sum(FLWHCost) AS 'FLWHCost'
	FROM
		regional.A_Master
	GROUP BY
		CatBP,MonthNum
) AS v1 ON c.`Month` = v1.MonthNum and c.CatBP = v1.CatBP
SET c.`FL WH Cost` = v1.`FLWHCost`;
#-------------------------------------------------------------------------------------------------
UPDATE development_mx.DailyReport_USD_Category c
INNER JOIN (
	SELECT
		CatBP,
		MonthNum,
		sum(FLCSCost) AS 'FLCSCost'
	FROM
		regional.A_Master
	GROUP BY
		Date
) AS v1 ON c.`Month` = v1.MonthNum and c.CatBP = v1.CatBP
SET c.`FL CS Cost` = v1.`FLCSCost`;
#-------------------------------------------------------------------------------------------------
update development_mx.DailyReport_USD_Category a
#FL Total cost
set a.`FL Total Cost` = a.`FL WH Cost` + a.`FL CS Cost`
;

#-------------------------------------------------------------------------------------------------

#-------------------------------------------------------------------------------------------------

#-------------------------------------------------------------------------------------------------
#
##
##