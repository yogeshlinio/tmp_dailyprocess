use development_mx;
DROP TABLE IF EXISTS TMP_MPCat;
CREATE TABLE TMP_MPCat ( INDEX ( MonthNum, ItemId ), INDEX( OrderNum ) )
SELECT
   development_mx.A_Master.Date,
   development_mx.A_Master.DateDelivered,
   development_mx.A_Master.DateReturned,
   development_mx.A_Master.MonthNum,
   development_mx.A_Master.OrderNum,
   development_mx.A_Master.ItemID,
   development_mx.A_Master.SKUSimple,
   development_mx.A_Master.SKUConfig,
   development_mx.A_Master.SKUName,
   development_mx.A_Master.idSupplier as idSupplier,   
   development_mx.A_Master.Supplier as Supplier,
   development_mx.A_Master.HeadBuyer,
   development_mx.A_Master.Buyer,
   development_mx.A_Master.CatBP,
   development_mx.A_Master.CatKPI,
   development_mx.A_Master.Cat1,
   development_mx.A_Master.Cat2,
   development_mx.A_Master.Cat3,
   development_mx.A_E_BI_Marketplace_Commission.Category as Category_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.Since,
   development_mx.A_E_BI_Marketplace_Commission.Fee,
   development_mx.A_Master.OriginalPrice,
   development_mx.A_Master.PriceAfterTax, 
   development_mx.A_Master.CouponValueAfterTax, 
   development_mx.A_Master.CostAfterTax, 
   development_mx.A_Master.ShippingFee, 
   development_mx.A_Master.PCOne,
   development_mx.A_Master.Interest,        
   development_mx.A_Master.TransactionFeeAfterTax,
   development_mx.A_Master.OrderBeforeCan,
   development_mx.A_Master.Returns,
   development_mx.A_Master.Cancellations,
   development_mx.A_Master.Rejected,
   development_mx.A_Master.OrderAfterCan,
   development_mx.A_Master.PCOnePFive,
   development_mx.A_Master.PCOnePFive-development_mx.A_Master.FLWHCost-development_mx.A_Master.FLCSCost AS PCTwo,
   development_mx.A_Master.ShipmentCost,
   development_mx.A_Master.FLCSCost,
   development_mx.A_Master.FLWHCost

FROM
               development_mx.A_Master 
    INNER JOIN development_mx.A_E_BI_Marketplace_Commission 
			ON     development_mx.A_Master.idSupplier = development_mx.A_E_BI_Marketplace_Commission.id_supplier
WHERE
#    OrderBeforeCan = 1
    IF(  A_E_BI_Marketplace_Commission.Since IS NULL, 1,
       IF( Date >=   A_E_BI_Marketplace_Commission.Since,1,0)    
    )
AND IF(development_mx.A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF(development_mx.A_Master.Date <= development_mx.A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND development_mx.A_E_BI_Marketplace_Commission.Category = "" 
AND development_mx.A_E_BI_Marketplace_Commission.SubCategory = ""
AND development_mx.A_E_BI_Marketplace_Commission.SubSubCategory = ""
AND development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory = ""
AND development_mx.A_E_BI_Marketplace_Commission.perSKU = ""
Order by supplier
;

REPLACE TMP_MPCat 
SELECT
   development_mx.A_Master.Date,
   development_mx.A_Master.DateDelivered,
   development_mx.A_Master.DateReturned,
   development_mx.A_Master.MonthNum,
   development_mx.A_Master.OrderNum,
   development_mx.A_Master.ItemID,
   development_mx.A_Master.SKUSimple,
   development_mx.A_Master.SKUConfig,
   development_mx.A_Master.SKUName,
   development_mx.A_Master.idSupplier as idSupplier,   
   development_mx.A_Master.Supplier as Supplier,
   development_mx.A_Master.HeadBuyer,
   development_mx.A_Master.Buyer,
   development_mx.A_Master.CatBP,
   development_mx.A_Master.CatKPI,
   development_mx.A_Master.Cat1,
   development_mx.A_Master.Cat2,
   development_mx.A_Master.Cat3,
   development_mx.A_E_BI_Marketplace_Commission.Category as Category_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.Since,
   development_mx.A_E_BI_Marketplace_Commission.Fee,
   development_mx.A_Master.OriginalPrice,
   development_mx.A_Master.PriceAfterTax, 
   development_mx.A_Master.CouponValueAfterTax, 
   development_mx.A_Master.CostAfterTax, 
   development_mx.A_Master.ShippingFee, 
   development_mx.A_Master.PCOne,
   development_mx.A_Master.Interest,        
   development_mx.A_Master.TransactionFeeAfterTax,
   development_mx.A_Master.OrderBeforeCan,
   development_mx.A_Master.Returns,
   development_mx.A_Master.Cancellations,
   development_mx.A_Master.Rejected,
   development_mx.A_Master.OrderAfterCan,
   development_mx.A_Master.PCOnePFive,
   development_mx.A_Master.PCOnePFive-development_mx.A_Master.FLWHCost-development_mx.A_Master.FLCSCost AS PCTwo,
   development_mx.A_Master.ShipmentCost,
   development_mx.A_Master.FLCSCost,
   development_mx.A_Master.FLWHCost
FROM
               development_mx.A_Master 
    INNER JOIN development_mx.A_E_BI_Marketplace_Commission
			ON     development_mx.A_Master.idSupplier = development_mx.A_E_BI_Marketplace_Commission.id_supplier
WHERE
#    development_mx.A_Master.OrderBeforeCan = 1
    IF(  A_E_BI_Marketplace_Commission.Since IS NULL, 1,
       IF( Date >=   A_E_BI_Marketplace_Commission.Since,1,0)    
    )
AND IF(development_mx.A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF(development_mx.A_Master.Date <= development_mx.A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND development_mx.A_E_BI_Marketplace_Commission.Category = development_mx.A_Master.CatBP 
AND development_mx.A_E_BI_Marketplace_Commission.SubCategory = ""
AND development_mx.A_E_BI_Marketplace_Commission.SubSubCategory = ""
AND development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory = ""
AND development_mx.A_E_BI_Marketplace_Commission.perSKU = ""
;

REPLACE TMP_MPCat 
SELECT
   development_mx.A_Master.Date,
   development_mx.A_Master.DateDelivered,
   development_mx.A_Master.DateReturned,
   development_mx.A_Master.MonthNum,
   development_mx.A_Master.OrderNum,
   development_mx.A_Master.ItemID,
   development_mx.A_Master.SKUSimple,
   development_mx.A_Master.SKUConfig,
   development_mx.A_Master.SKUName,
   development_mx.A_Master.idSupplier as idSupplier,   
   development_mx.A_Master.Supplier as Supplier,
   development_mx.A_Master.HeadBuyer,
   development_mx.A_Master.Buyer,
   development_mx.A_Master.CatBP,
   development_mx.A_Master.CatKPI,
   development_mx.A_Master.Cat1,
   development_mx.A_Master.Cat2,
   development_mx.A_Master.Cat3,
   development_mx.A_E_BI_Marketplace_Commission.Category as Category_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.Since,
   development_mx.A_E_BI_Marketplace_Commission.Fee,
   development_mx.A_Master.OriginalPrice,
   development_mx.A_Master.PriceAfterTax, 
   development_mx.A_Master.CouponValueAfterTax, 
   development_mx.A_Master.CostAfterTax, 
   development_mx.A_Master.ShippingFee, 
   development_mx.A_Master.PCOne,
   development_mx.A_Master.Interest,        
   development_mx.A_Master.TransactionFeeAfterTax,
   development_mx.A_Master.OrderBeforeCan,
   development_mx.A_Master.Returns,
   development_mx.A_Master.Cancellations,
   development_mx.A_Master.Rejected,
   development_mx.A_Master.OrderAfterCan,
   development_mx.A_Master.PCOnePFive,
   development_mx.A_Master.PCOnePFive-development_mx.A_Master.FLWHCost-development_mx.A_Master.FLCSCost AS PCTwo,
   development_mx.A_Master.ShipmentCost,
   development_mx.A_Master.FLCSCost,
   development_mx.A_Master.FLWHCost 
FROM
               development_mx.A_Master 
    INNER JOIN development_mx.A_E_BI_Marketplace_Commission
			ON     development_mx.A_Master.idSupplier = development_mx.A_E_BI_Marketplace_Commission.id_supplier
WHERE
#    OrderBeforeCan = 1
    IF(  A_E_BI_Marketplace_Commission.Since IS NULL, 1,
       IF( Date >=   A_E_BI_Marketplace_Commission.Since,1,0)    
    )
AND IF(development_mx.A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF(development_mx.A_Master.Date <= development_mx.A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND development_mx.A_E_BI_Marketplace_Commission.Category = development_mx.A_Master.CatBP 
AND (    development_mx.A_Master.Cat1 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR development_mx.A_Master.Cat2 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
OR development_mx.A_Master.Cat3 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
     )
AND development_mx.A_E_BI_Marketplace_Commission.SubCategory != ""
AND development_mx.A_E_BI_Marketplace_Commission.perSKU = ""
;

REPLACE TMP_MPCat 
SELECT
   development_mx.A_Master.Date,
   development_mx.A_Master.DateDelivered,
   development_mx.A_Master.DateReturned,
   development_mx.A_Master.MonthNum,
   development_mx.A_Master.OrderNum,
   development_mx.A_Master.ItemID,
   development_mx.A_Master.SKUSimple,
   development_mx.A_Master.SKUConfig,
   development_mx.A_Master.SKUName,
   development_mx.A_Master.idSupplier as idSupplier,   
   development_mx.A_Master.Supplier as Supplier,
   development_mx.A_Master.HeadBuyer,
   development_mx.A_Master.Buyer,
   development_mx.A_Master.CatBP,
   development_mx.A_Master.CatKPI,
   development_mx.A_Master.Cat1,
   development_mx.A_Master.Cat2,
   development_mx.A_Master.Cat3,
   development_mx.A_E_BI_Marketplace_Commission.Category as Category_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.Since,
   development_mx.A_E_BI_Marketplace_Commission.Fee,
   development_mx.A_Master.OriginalPrice,
   development_mx.A_Master.PriceAfterTax, 
   development_mx.A_Master.CouponValueAfterTax, 
   development_mx.A_Master.CostAfterTax, 
   development_mx.A_Master.ShippingFee, 
   development_mx.A_Master.PCOne,
   development_mx.A_Master.Interest,        
   development_mx.A_Master.TransactionFeeAfterTax,
   development_mx.A_Master.OrderBeforeCan,
   development_mx.A_Master.Returns,
   development_mx.A_Master.Cancellations,
   development_mx.A_Master.Rejected,
   development_mx.A_Master.OrderAfterCan,
   development_mx.A_Master.PCOnePFive,
   development_mx.A_Master.PCOnePFive-development_mx.A_Master.FLWHCost-development_mx.A_Master.FLCSCost AS PCTwo,
   development_mx.A_Master.ShipmentCost,
   development_mx.A_Master.FLCSCost,
   development_mx.A_Master.FLWHCost 
FROM
               development_mx.A_Master 
    INNER JOIN development_mx.A_E_BI_Marketplace_Commission 
			ON     development_mx.A_Master.idSupplier = development_mx.A_E_BI_Marketplace_Commission.id_supplier
WHERE
#    OrderBeforeCan = 1
    IF(  A_E_BI_Marketplace_Commission.Since IS NULL, 1,
       IF( Date >=   A_E_BI_Marketplace_Commission.Since,1,0)    
    )
AND IF(development_mx.A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF(development_mx.A_Master.Date <= development_mx.A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND development_mx.A_E_BI_Marketplace_Commission.Category = development_mx.A_Master.CatBP 
AND (    development_mx.A_Master.Cat1 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR development_mx.A_Master.Cat2 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
OR development_mx.A_Master.Cat3 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
     )
AND (    development_mx.A_Master.Cat1 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
      OR development_mx.A_Master.Cat2 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
OR development_mx.A_Master.Cat3 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
     )
AND development_mx.A_E_BI_Marketplace_Commission.SubSubCategory != ""
AND development_mx.A_E_BI_Marketplace_Commission.perSKU = ""
;

REPLACE TMP_MPCat 
SELECT
   development_mx.A_Master.Date,
   development_mx.A_Master.DateDelivered,
   development_mx.A_Master.DateReturned,
   development_mx.A_Master.MonthNum,
   development_mx.A_Master.OrderNum,
   development_mx.A_Master.ItemID,
   development_mx.A_Master.SKUSimple,
   development_mx.A_Master.SKUConfig,
   development_mx.A_Master.SKUName,
   development_mx.A_Master.idSupplier as idSupplier,   
   development_mx.A_Master.Supplier as Supplier,
   development_mx.A_Master.HeadBuyer,
   development_mx.A_Master.Buyer,
   development_mx.A_Master.CatBP,
   development_mx.A_Master.CatKPI,
   development_mx.A_Master.Cat1,
   development_mx.A_Master.Cat2,
   development_mx.A_Master.Cat3,
   development_mx.A_E_BI_Marketplace_Commission.Category as Category_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.Since,
   development_mx.A_E_BI_Marketplace_Commission.Fee,
   development_mx.A_Master.OriginalPrice,
   development_mx.A_Master.PriceAfterTax, 
   development_mx.A_Master.CouponValueAfterTax, 
   development_mx.A_Master.CostAfterTax, 
   development_mx.A_Master.ShippingFee, 
   development_mx.A_Master.PCOne,
   development_mx.A_Master.Interest,        
   development_mx.A_Master.TransactionFeeAfterTax,
   development_mx.A_Master.OrderBeforeCan,
   development_mx.A_Master.Returns,
   development_mx.A_Master.Cancellations,
   development_mx.A_Master.Rejected,
   development_mx.A_Master.OrderAfterCan,
   development_mx.A_Master.PCOnePFive,
   development_mx.A_Master.PCOnePFive-development_mx.A_Master.FLWHCost-development_mx.A_Master.FLCSCost AS PCTwo,
   development_mx.A_Master.ShipmentCost,
   development_mx.A_Master.FLCSCost,
   development_mx.A_Master.FLWHCost 
FROM
               development_mx.A_Master 
    INNER JOIN development_mx.A_E_BI_Marketplace_Commission 
			ON     development_mx.A_Master.idSupplier = development_mx.A_E_BI_Marketplace_Commission.id_supplier
WHERE
#    OrderBeforeCan = 1
    IF(  A_E_BI_Marketplace_Commission.Since IS NULL, 1,
       IF( Date >=   A_E_BI_Marketplace_Commission.Since,1,0)    
    )
AND IF(development_mx.A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF(development_mx.A_Master.Date <= development_mx.A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND development_mx.A_E_BI_Marketplace_Commission.Category = development_mx.A_Master.CatBP 
AND (    development_mx.A_Master.Cat1 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR development_mx.A_Master.Cat2 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
OR development_mx.A_Master.Cat3 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
     )
AND (    development_mx.A_Master.Cat1 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
      OR development_mx.A_Master.Cat2 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
OR development_mx.A_Master.Cat3 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
     )
AND (    development_mx.A_Master.Cat1 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory, "%" ) 
      OR development_mx.A_Master.Cat2 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory, "%" ) 
OR development_mx.A_Master.Cat3 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory, "%" ) 
     )
AND development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory != ""
AND development_mx.A_E_BI_Marketplace_Commission.perSKU = ""
;

/*
*  Per SKU
*/
REPLACE TMP_MPCat 
SELECT
   development_mx.A_Master.Date,
   development_mx.A_Master.DateDelivered,
   development_mx.A_Master.DateReturned,
   development_mx.A_Master.MonthNum,
   development_mx.A_Master.OrderNum,
   development_mx.A_Master.ItemID,
   development_mx.A_Master.SKUSimple,
   development_mx.A_Master.SKUConfig,
   development_mx.A_Master.SKUName,
   development_mx.A_Master.idSupplier as idSupplier,   
   development_mx.A_Master.Supplier as Supplier,
   development_mx.A_Master.HeadBuyer,
   development_mx.A_Master.Buyer,
   development_mx.A_Master.CatBP,
   development_mx.A_Master.CatKPI,
   development_mx.A_Master.Cat1,
   development_mx.A_Master.Cat2,
   development_mx.A_Master.Cat3,
   development_mx.A_E_BI_Marketplace_Commission.Category as Category_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.Since,
   development_mx.A_E_BI_Marketplace_Commission.Fee,
   development_mx.A_Master.OriginalPrice,
   development_mx.A_Master.PriceAfterTax, 
   development_mx.A_Master.CouponValueAfterTax, 
   development_mx.A_Master.CostAfterTax, 
   development_mx.A_Master.ShippingFee, 
   development_mx.A_Master.PCOne,
   development_mx.A_Master.Interest,        
   development_mx.A_Master.TransactionFeeAfterTax,
   development_mx.A_Master.OrderBeforeCan,
   development_mx.A_Master.Returns,
   development_mx.A_Master.Cancellations,
   development_mx.A_Master.Rejected,
   development_mx.A_Master.OrderAfterCan,
   development_mx.A_Master.PCOnePFive,
   development_mx.A_Master.PCOnePFive-development_mx.A_Master.FLWHCost-development_mx.A_Master.FLCSCost AS PCTwo,
   development_mx.A_Master.ShipmentCost,
   development_mx.A_Master.FLCSCost,
   development_mx.A_Master.FLWHCost
FROM
               development_mx.A_Master 
    INNER JOIN development_mx.A_E_BI_Marketplace_Commission
			ON     development_mx.A_Master.idSupplier = development_mx.A_E_BI_Marketplace_Commission.id_supplier
WHERE
#    development_mx.A_Master.OrderBeforeCan = 1
    IF(  A_E_BI_Marketplace_Commission.Since IS NULL, 1,
       IF( Date >=   A_E_BI_Marketplace_Commission.Since,1,0)    
    )
AND IF(development_mx.A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF(development_mx.A_Master.Date <= development_mx.A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND development_mx.A_E_BI_Marketplace_Commission.Category = ""
AND development_mx.A_E_BI_Marketplace_Commission.SubCategory = ""
AND development_mx.A_E_BI_Marketplace_Commission.SubSubCategory = ""
AND development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory = ""
AND development_mx.A_E_BI_Marketplace_Commission.perSKU != ""
AND development_mx.A_Master.SKUName like concat( "%" , replace( perSKU , " " , "%" ) , "%" )
;

REPLACE TMP_MPCat 
SELECT
   development_mx.A_Master.Date,
   development_mx.A_Master.DateDelivered,
   development_mx.A_Master.DateReturned,
   development_mx.A_Master.MonthNum,
   development_mx.A_Master.OrderNum,
   development_mx.A_Master.ItemID,
   development_mx.A_Master.SKUSimple,
   development_mx.A_Master.SKUConfig,
   development_mx.A_Master.SKUName,
   development_mx.A_Master.idSupplier as idSupplier,   
   development_mx.A_Master.Supplier as Supplier,
   development_mx.A_Master.HeadBuyer,
   development_mx.A_Master.Buyer,
   development_mx.A_Master.CatBP,
   development_mx.A_Master.CatKPI,
   development_mx.A_Master.Cat1,
   development_mx.A_Master.Cat2,
   development_mx.A_Master.Cat3,
   development_mx.A_E_BI_Marketplace_Commission.Category as Category_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.Since,
   development_mx.A_E_BI_Marketplace_Commission.Fee,
   development_mx.A_Master.OriginalPrice,
   development_mx.A_Master.PriceAfterTax, 
   development_mx.A_Master.CouponValueAfterTax, 
   development_mx.A_Master.CostAfterTax, 
   development_mx.A_Master.ShippingFee, 
   development_mx.A_Master.PCOne,
   development_mx.A_Master.Interest,        
   development_mx.A_Master.TransactionFeeAfterTax,
   development_mx.A_Master.OrderBeforeCan,
   development_mx.A_Master.Returns,
   development_mx.A_Master.Cancellations,
   development_mx.A_Master.Rejected,
   development_mx.A_Master.OrderAfterCan,
   development_mx.A_Master.PCOnePFive,
   development_mx.A_Master.PCOnePFive-development_mx.A_Master.FLWHCost-development_mx.A_Master.FLCSCost AS PCTwo,
   development_mx.A_Master.ShipmentCost,
   development_mx.A_Master.FLCSCost,
   development_mx.A_Master.FLWHCost
FROM
               development_mx.A_Master 
    INNER JOIN development_mx.A_E_BI_Marketplace_Commission
			ON     development_mx.A_Master.idSupplier = development_mx.A_E_BI_Marketplace_Commission.id_supplier
WHERE
#    development_mx.A_Master.OrderBeforeCan = 1
    IF(  A_E_BI_Marketplace_Commission.Since IS NULL, 1,
       IF( Date >=   A_E_BI_Marketplace_Commission.Since,1,0)    
    )
AND IF(development_mx.A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF(development_mx.A_Master.Date <= development_mx.A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND development_mx.A_E_BI_Marketplace_Commission.Category = development_mx.A_Master.CatBP 
AND development_mx.A_E_BI_Marketplace_Commission.SubCategory = ""
AND development_mx.A_E_BI_Marketplace_Commission.SubSubCategory = ""
AND development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory = ""
AND development_mx.A_E_BI_Marketplace_Commission.perSKU != ""
AND development_mx.A_Master.SKUName like concat( "%" , replace( perSKU , " " , "%" ) , "%" )
;

REPLACE TMP_MPCat 
SELECT
   development_mx.A_Master.Date,
   development_mx.A_Master.DateDelivered,
   development_mx.A_Master.DateReturned,
   development_mx.A_Master.MonthNum,
   development_mx.A_Master.OrderNum,
   development_mx.A_Master.ItemID,
   development_mx.A_Master.SKUSimple,
   development_mx.A_Master.SKUConfig,
   development_mx.A_Master.SKUName,
   development_mx.A_Master.idSupplier as idSupplier,   
   development_mx.A_Master.Supplier as Supplier,
   development_mx.A_Master.HeadBuyer,
   development_mx.A_Master.Buyer,
   development_mx.A_Master.CatBP,
   development_mx.A_Master.CatKPI,
   development_mx.A_Master.Cat1,
   development_mx.A_Master.Cat2,
   development_mx.A_Master.Cat3,
   development_mx.A_E_BI_Marketplace_Commission.Category as Category_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.Since,
   development_mx.A_E_BI_Marketplace_Commission.Fee,
   development_mx.A_Master.OriginalPrice,
   development_mx.A_Master.PriceAfterTax, 
   development_mx.A_Master.CouponValueAfterTax, 
   development_mx.A_Master.CostAfterTax, 
   development_mx.A_Master.ShippingFee, 
   development_mx.A_Master.PCOne,
   development_mx.A_Master.Interest,        
   development_mx.A_Master.TransactionFeeAfterTax,
   development_mx.A_Master.OrderBeforeCan,
   development_mx.A_Master.Returns,
   development_mx.A_Master.Cancellations,
   development_mx.A_Master.Rejected,
   development_mx.A_Master.OrderAfterCan,
   development_mx.A_Master.PCOnePFive,
   development_mx.A_Master.PCOnePFive-development_mx.A_Master.FLWHCost-development_mx.A_Master.FLCSCost AS PCTwo,
   development_mx.A_Master.ShipmentCost,
   development_mx.A_Master.FLCSCost,
   development_mx.A_Master.FLWHCost
FROM
               development_mx.A_Master 
    INNER JOIN development_mx.A_E_BI_Marketplace_Commission
			ON     development_mx.A_Master.idSupplier = development_mx.A_E_BI_Marketplace_Commission.id_supplier
WHERE
#    OrderBeforeCan = 1
    IF(  A_E_BI_Marketplace_Commission.Since IS NULL, 1,
       IF( Date >=   A_E_BI_Marketplace_Commission.Since,1,0)    
    )
AND IF(development_mx.A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF(development_mx.A_Master.Date <= development_mx.A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND development_mx.A_E_BI_Marketplace_Commission.Category = development_mx.A_Master.CatBP 
AND (    development_mx.A_Master.Cat1 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR development_mx.A_Master.Cat2 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
OR development_mx.A_Master.Cat3 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
     )
AND development_mx.A_E_BI_Marketplace_Commission.SubCategory != ""
AND development_mx.A_E_BI_Marketplace_Commission.perSKU != ""
AND development_mx.A_Master.SKUName like concat( "%" , replace( perSKU , " " , "%" ) , "%" )
;

REPLACE TMP_MPCat 
SELECT
   development_mx.A_Master.Date,
   development_mx.A_Master.DateDelivered,
   development_mx.A_Master.DateReturned,
   development_mx.A_Master.MonthNum,
   development_mx.A_Master.OrderNum,
   development_mx.A_Master.ItemID,
   development_mx.A_Master.SKUSimple,
   development_mx.A_Master.SKUConfig,
   development_mx.A_Master.SKUName,
   development_mx.A_Master.idSupplier as idSupplier,   
   development_mx.A_Master.Supplier as Supplier,
   development_mx.A_Master.HeadBuyer,
   development_mx.A_Master.Buyer,
   development_mx.A_Master.CatBP,
   development_mx.A_Master.CatKPI,
   development_mx.A_Master.Cat1,
   development_mx.A_Master.Cat2,
   development_mx.A_Master.Cat3,
   development_mx.A_E_BI_Marketplace_Commission.Category as Category_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.Since,
   development_mx.A_E_BI_Marketplace_Commission.Fee,
   development_mx.A_Master.OriginalPrice,
   development_mx.A_Master.PriceAfterTax, 
   development_mx.A_Master.CouponValueAfterTax, 
   development_mx.A_Master.CostAfterTax, 
   development_mx.A_Master.ShippingFee, 
   development_mx.A_Master.PCOne,
   development_mx.A_Master.Interest,        
   development_mx.A_Master.TransactionFeeAfterTax,
   development_mx.A_Master.OrderBeforeCan,
   development_mx.A_Master.Returns,
   development_mx.A_Master.Cancellations,
   development_mx.A_Master.Rejected,
   development_mx.A_Master.OrderAfterCan,
   development_mx.A_Master.PCOnePFive,
   development_mx.A_Master.PCOnePFive-development_mx.A_Master.FLWHCost-development_mx.A_Master.FLCSCost AS PCTwo,
   development_mx.A_Master.ShipmentCost,
   development_mx.A_Master.FLCSCost,
   development_mx.A_Master.FLWHCost 
FROM
               development_mx.A_Master 
    INNER JOIN development_mx.A_E_BI_Marketplace_Commission
			ON     development_mx.A_Master.idSupplier = development_mx.A_E_BI_Marketplace_Commission.id_supplier
WHERE
#    OrderBeforeCan = 1
    IF(  A_E_BI_Marketplace_Commission.Since IS NULL, 1,
       IF( Date >=   A_E_BI_Marketplace_Commission.Since,1,0)    
    )
AND IF(development_mx.A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF(development_mx.A_Master.Date <= development_mx.A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND development_mx.A_E_BI_Marketplace_Commission.Category = development_mx.A_Master.CatBP 
AND (    development_mx.A_Master.Cat1 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR development_mx.A_Master.Cat2 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
OR development_mx.A_Master.Cat3 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
     )
AND (    development_mx.A_Master.Cat1 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
      OR development_mx.A_Master.Cat2 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
OR development_mx.A_Master.Cat3 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
     )
AND development_mx.A_E_BI_Marketplace_Commission.SubSubCategory != ""
AND development_mx.A_E_BI_Marketplace_Commission.perSKU != ""
AND development_mx.A_Master.SKUName like concat( "%" , replace( perSKU , " " , "%" ) , "%" )
;

REPLACE TMP_MPCat 
SELECT
   development_mx.A_Master.Date,
   development_mx.A_Master.DateDelivered,
   development_mx.A_Master.DateReturned,
   development_mx.A_Master.MonthNum,
   development_mx.A_Master.OrderNum,
   development_mx.A_Master.ItemID,
   development_mx.A_Master.SKUSimple,
   development_mx.A_Master.SKUConfig,
   development_mx.A_Master.SKUName,
   development_mx.A_Master.idSupplier as idSupplier,   
   development_mx.A_Master.Supplier as Supplier,
   development_mx.A_Master.HeadBuyer,
   development_mx.A_Master.Buyer,
   development_mx.A_Master.CatBP,
   development_mx.A_Master.CatKPI,
   development_mx.A_Master.Cat1,
   development_mx.A_Master.Cat2,
   development_mx.A_Master.Cat3,
   development_mx.A_E_BI_Marketplace_Commission.Category as Category_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.Since,
   development_mx.A_E_BI_Marketplace_Commission.Fee,
   development_mx.A_Master.OriginalPrice,
   development_mx.A_Master.PriceAfterTax, 
   development_mx.A_Master.CouponValueAfterTax, 
   development_mx.A_Master.CostAfterTax, 
   development_mx.A_Master.ShippingFee, 
   development_mx.A_Master.PCOne,
   development_mx.A_Master.Interest,        
   development_mx.A_Master.TransactionFeeAfterTax,
   development_mx.A_Master.OrderBeforeCan,
   development_mx.A_Master.Returns,
   development_mx.A_Master.Cancellations,
   development_mx.A_Master.Rejected,
   development_mx.A_Master.OrderAfterCan,
   development_mx.A_Master.PCOnePFive,
   development_mx.A_Master.PCOnePFive-development_mx.A_Master.FLWHCost-development_mx.A_Master.FLCSCost AS PCTwo,
   development_mx.A_Master.ShipmentCost,
   development_mx.A_Master.FLCSCost,
   development_mx.A_Master.FLWHCost 
FROM
               development_mx.A_Master 
    INNER JOIN development_mx.A_E_BI_Marketplace_Commission
			ON     development_mx.A_Master.idSupplier = development_mx.A_E_BI_Marketplace_Commission.id_supplier
WHERE
    #OrderBeforeCan = 1
    IF(  A_E_BI_Marketplace_Commission.Since IS NULL, 1,
       IF( Date >=   A_E_BI_Marketplace_Commission.Since,1,0)    
    )
AND IF(development_mx.A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF(development_mx.A_Master.Date <= development_mx.A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND development_mx.A_E_BI_Marketplace_Commission.Category = development_mx.A_Master.CatBP 
AND (    development_mx.A_Master.Cat1 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR development_mx.A_Master.Cat2 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
OR development_mx.A_Master.Cat3 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
     )
AND (    development_mx.A_Master.Cat1 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
      OR development_mx.A_Master.Cat2 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
OR development_mx.A_Master.Cat3 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
     )
AND (    development_mx.A_Master.Cat1 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory, "%" ) 
      OR development_mx.A_Master.Cat2 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory, "%" ) 
OR development_mx.A_Master.Cat3 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory, "%" ) 
     )
AND development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory != ""
AND development_mx.A_E_BI_Marketplace_Commission.perSKU != ""
AND development_mx.A_Master.SKUName like concat( "%" , replace( perSKU , " " , "%" ) , "%" )
;

/*
*  Inventory
*/
DROP TEMPORARY TABLE IF EXISTS development_mx.TMP_out_order_tracking;
CREATE TEMPORARY TABLE development_mx.TMP_out_order_tracking ( KEY ( item_id ) )
SELECT
    CAST(  item_id AS DECIMAL ) AS item_id 
FROM
   operations_mx.out_order_tracking
WHERE
   fulfillment_type_real = "inventory"
;
/*
DELETE FROM TMP_MPCat 
WHERE 
    ItemId in ( 
                SELECT *  FROM development_mx.TMP_out_order_tracking
              )
AND Supplier not in ( select Bob_Supplier_Name from development_mx.A_E_BI_Marketplace_Commission WHERE ownWarehouse = 1 )
;
*/
DROP TEMPORARY TABLE IF EXISTS TMP_Track;
CREATE TEMPORARY TABLE TMP_Track ( PRIMARY KEY ( item_id ) )
SELECT  
  CAST( item_id as DECIMAL) AS  item_id,
  fulfillment_type_real  
FROM operations_mx.out_order_tracking
GROUP BY item_id

;
   

/*
*
*
*
*/
DROP TABLE IF EXISTS M1_Market_Place;
CREATE TABLE M1_Market_Place  ( PRIMARY KEY ( ItemId ) )
SELECT 
      TMP_MPCat.MonthNum   AS Month_Num,
      TMP_MPCat.ItemId AS ItemID, 
      TMP_MPCat.Date, 
      TMP_MPCat.DateDelivered,
      TMP_MPCat.DateReturned,
      TMP_MPCat.OrderNum  as OrderNum, 
      TMP_MPCat.CatBP AS Category_BP, 
      TMP_MPCat.CatKPI,
      TMP_MPCat.idSupplier,	  
      TMP_MPCat.Supplier,
      TMP_MPCat.HeadBuyer,
      TMP_MPCat.Buyer,
      TMP_MPCat.SKUSimple AS SKU_Simple, 
      TMP_MPCat.SKUName AS SKU_Name, 
      TMP_MPCat.OriginalPrice,
      TMP_MPCat.PriceAfterTax as Price_After_Tax, 
      TMP_MPCat.CouponValueAfterTax as after_vat_coupon, 
      #TMP_MPCat.CostAfterTax as cost_after_tax, 
	  TMP_MPCat.PriceAfterTax - TMP_MPCat.PriceAfterTax * TMP_MPCat.Fee as cost_after_tax,
	  
	  
      COALESCE( TMP_MPCat.ShippingFee , 0 ) AS Shipping_Fee_Charged, 
      TMP_MPCat.PCOne AS PCOne ,
        TMP_MPCat.PriceAfterTax 
      - TMP_MPCat.CouponValueAfterTax 
      + COALESCE( TMP_MPCat.ShippingFee , 0 )
      + TMP_MPCat.Interest
       as Rev,
      TMP_MPCat.PriceAfterTax * TMP_MPCat.Fee as Commission,
      TMP_MPCat.Fee,
      A_E_BI_ExchangeRate.XR,
      TMP_MPCat.TransactionFeeAfterTax as Transaction_fee_after_tax,

      TMP_MPCat.PCOnePFive  AS PCOnePFive,
      TMP_MPCat.PCTwo  as PCTwo,
      space(15) AS fulfillment_type_real ,
      TMP_MPCat.OrderBeforeCan,
      TMP_MPCat.Returns,
      TMP_MPCat.Cancellations,
      TMP_MPCat.Rejected,
      TMP_MPCat.OrderAfterCan,
	  TMP_MPCat.ShipmentCost,
      TMP_MPCat.FLCSCost,
      TMP_MPCat.FLWHCost

FROM            TMP_MPCat
     INNER JOIN A_E_BI_ExchangeRate
             ON TMP_MPCat.MonthNum= A_E_BI_ExchangeRate.Month_Num 
 
WHERE 
#       TMP_MPCat.OrderBeforeCan=1 
       TMP_MPCat.Date>="2013/5/31" 
   AND IF( TMP_MPCat.Since IS NULL , 1, IF( TMP_MPCat.Date >=  TMP_MPCat.Since , 1, 0 ) )   
   AND A_E_BI_ExchangeRate.Country = "MEX"
GROUP BY 
    TMP_MPCat.ItemID, 
    TMP_MPCat.Date, 
    TMP_MPCat.OrderNum, 
    TMP_MPCat.CatBP, 
    TMP_MPCat.SKUSimple, 
    TMP_MPCat.SKUName, 
    TMP_MPCat.PriceAfterTax, 
    TMP_MPCat.CouponValueAfterTax, 
    TMP_MPCat.CostAfterTax, 
    TMP_MPCat.ShippingFee, 
    TMP_MPCat.PCOne;

UPDATE        M1_Market_Place
#   INNER JOIN production.out_order_tracking
    INNER JOIN TMP_Track 
           ON M1_Market_Place.ItemId = TMP_Track.item_id
SET
   M1_Market_Place.fulfillment_type_real = TMP_Track.fulfillment_type_real
;


DROP TABLE IF EXISTS M1_MarketPlace_Out;
CREATE TABLE M1_MarketPlace_Out
SELECT
   Date,
   COUNT( distinct SKU_Simple ) AS SKUs,
   COUNT(*) AS Items,
   SUM( Rev / XR ) as RevE,
   SUM( Commission / XR ) as CommissionE,
   SUM( Commission ) / SUM( Rev  )  as RevCom,
   ( 
    SELECT COUNT(*) AS Count_MKSupplier  FROM A_E_BI_Marketplace_Commission WHERE M1_Market_Place.Date >= A_E_BI_Marketplace_Commission.Since  
   ) AS Count_MKSupplier 
FROM
M1_Market_Place
GROUP BY Date;    

INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  step,
  updated_at,
  
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Mexico',
  'M1_Market_Place',
  'finish',
  NOW(),  
  MAX(date),
  count(*),
  count(*)
FROM
development_mx.M1_Market_Place
;