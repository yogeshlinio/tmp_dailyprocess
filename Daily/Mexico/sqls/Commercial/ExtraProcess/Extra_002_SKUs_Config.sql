USE development_mx;
SET @MonthNum = "000000";
SELECT date_format( CURRENT_DATE() -INTERVAL 1 Day ,"%Y%m" )  INTO @MonthNum;
#DROP TABLE IF EXISTS A_SKUsVisible;
CREATE TABLE IF NOT EXISTS A_SKUsVisible
(
   MonthNum int,
   Cat_KPI varchar(25),
   SKU_Simple varchar(25),
   SKU_Config varchar(25),
   SKU_Name   varchar(50),
   isMPlace   int,
   UpdatedAt  datetime,
   PRIMARY KEY ( MonthNum, SKU_Simple  ),
   KEY( SKU_Simple )
)
;

REPLACE development_mx.A_SKUsVisible
SELECT
  @MonthNum   AS MonthNum,
  
  Cat_KPI     AS Cat_KPI, 
  SKU_Simple  AS SKU_Simple,
  SKU_Config  AS SKU_Config,
  SKU_Name    AS SKU_Name,
  isMarketPlace    AS isMPlace,
  now()       AS UpdatedAt
FROM
  development_mx.A_Master_Catalog
WHERE
  isVisible = 1
;


REPLACE A_E_M1_SKUs_Visible
SELECT
   Date( now() )- INTERVAL  1 DAY AS Day,
   Count(DISTINCT SKU_Config),
   0
FROM A_Master_Catalog
WHERE isVisible = 1
;

