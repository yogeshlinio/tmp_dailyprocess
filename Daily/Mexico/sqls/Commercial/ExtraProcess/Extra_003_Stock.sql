USE development_mx;
SET @MonthNum = "000000";
SELECT date_format( CURRENT_DATE() -INTERVAL 1 Day ,"%Y%m" )  INTO @MonthNum;

CREATE TABLE IF NOT EXISTS development_mx.A_Stock
(
   MonthNum int,
   stock_item_id int,
   SKU_Simple  varchar(25),
   SKU_Config  varchar(25),

   Days_inStock  int,
   Visibles      int,
   
   CostAfterTax decimal(15,2),
   UpdatedAt  datetime,
   PRIMARY KEY ( MonthNum, stock_item_id ),
   KEY( SKU_Simple )
)
;

REPLACE development_mx.A_Stock
SELECT
  @MonthNum   AS MonthNum,
  stock_item_id AS stock_item_id,
  SKU_Simple  AS SKU_Simple,
  SKU_Config  AS SKU_Config,
  Days_in_stock ,
  Visible,
  cost_w_o_vat, 
  now()       AS UpdatedAt
FROM
  operations_mx.out_stock_hist 
WHERE
      in_stock  = 1
  AND fulfillment_type_real not like '%Consignment%'
;

