USE development_mx;

SELECT @c_date:=CURDATE() - INTERVAL 1 DAY
FROM dual;

SELECT @v_maxdate:=MAX(date)
FROM catalog_history;


SELECT 'Borrando ultimo dia de catalog_history', now();
DELETE FROM catalog_history WHERE date=@v_maxDate and @v_maxdate=@c_date;


#Catalog Visible
Select 'Truncando tabla catalog_visible', now();
TRUNCATE TABLE catalog_visible;

Select 'Insertando en catalog_visible', now();
INSERT INTO catalog_visible ( sku_config
                                          , sku_simple
                                          , pet_status
                                          , pet_approved
                                          , status_config
                                          , status_simple
                                          , name
                                          , display_if_out_of_stock
                                          , quantity
                                          , updated_at
                                          , activated_at
                                          , price) 
SELECT catalog_config.sku as sku_config
     , catalog_simple.sku as sku_simple
     , catalog_config.pet_status
     , catalog_config.pet_approved
     , catalog_config.status
     , catalog_simple.status
     , catalog_config.name
     , catalog_config.display_if_out_of_stock
     , catalog_stock.quantity
     , catalog_config.updated_at
     , catalog_config.activated_at
     , catalog_simple.price
-- Select count(*)
FROM bob_live_mx.catalog_source 
INNER JOIN bob_live_mx.catalog_simple 
ON catalog_source.fk_catalog_simple = catalog_simple.id_catalog_simple
INNER JOIN  bob_live_mx.catalog_config
ON  id_catalog_config = fk_catalog_config
INNER JOIN bob_live_mx.catalog_stock 
ON catalog_source.id_catalog_source = catalog_stock.fk_catalog_source
WHERE (
	   (
        (catalog_config.pet_status="creation,edited" OR catalog_config.pet_status="creation,edited,images")
       ) 
       AND 
       (
        (catalog_config.pet_approved)=1
       ) 
       AND 
       (
        (catalog_config.status)="active"
       ) 
       AND 
       (
        (catalog_simple.status)="active"
       ) 
       AND 
       (
        (catalog_config.display_if_out_of_stock)=0
       ) 
       AND 
       (
        (catalog_stock.quantity)>0
       ) 
       AND 
       (
        (catalog_simple.price)>0
       )
      )
      OR 
      (
       (
        (catalog_config.pet_status="creation,edited" OR catalog_config.pet_status="creation,edited,images")
       ) 
       AND 
	   (
        (catalog_config.pet_approved)=1
       ) 
       AND 
       (
        (catalog_config.status)="active"
       ) 
       AND 
	   (
        (catalog_simple.status)="active"
       ) 
       AND 
       (
        (catalog_config.display_if_out_of_stock)=1
	   ) 
       AND 
       (
        (catalog_simple.price)>0
       )
);

Select 'Insertando ultimo dia en catalog_history', now();
#Catalog History
INSERT INTO catalog_history ( yrmonth, date
                                          , sku_config
                                          , sku_simple
                                          , product_name
                                          , status_config
                                          , status_simple
                                          , quantity, price) 
SELECT DATE_FORMAT(curdate() - INTERVAL 1 DAY,'%Y%m')
     , curdate() - INTERVAL 1 DAY
     , catalog_config.sku as sku_config
     , catalog_simple.sku as sku_simple
     , catalog_config.name
     , catalog_config.status
     , catalog_simple.status
     , catalog_stock.quantity
     , catalog_simple.price
-- Select count(*)
FROM (bob_live_mx.catalog_config 
INNER JOIN bob_live_mx.catalog_simple 
ON catalog_config.id_catalog_config = catalog_simple.fk_catalog_config) 
INNER JOIN bob_live_mx.catalog_source
ON catalog_simple.id_catalog_simple = catalog_source.fk_catalog_simple
LEFT JOIN bob_live_mx.catalog_stock 
ON catalog_source.id_catalog_source = catalog_stock.fk_catalog_source;

UPDATE catalog_history 
SET quantity = 0  
WHERE quantity is null 
AND date = curdate() - INTERVAL 1 DAY;

UPDATE catalog_history c 
INNER JOIN catalog_visible v 
ON c.sku_simple=v.sku_simple 
SET visible = 1 
WHERE date = curdate() - INTERVAL 1 DAY;

UPDATE catalog_history SET visible = 0 
WHERE visible IS NULL 
AND date = curdate() - INTERVAL 1 DAY;

Select 'End Rutina Catalog History', now();