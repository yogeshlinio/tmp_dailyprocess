/*
*   UPDATE A_Master_Catalog.buyer 
*   UPDATE A_Master_Catalog.head_buyer 
*/         
UPDATE           A_Master_Catalog_Sample         
      INNER JOIN bob_live_mx.catalog_attribute_option_global_buyer_name
           ON A_Master_Catalog_Sample.id_catalog_attribute_option_global_buyer=catalog_attribute_option_global_buyer_name.id_catalog_attribute_option_global_buyer_name
		   #USING ( id_catalog_attribute_option_global_buyer_name )
SET
    A_Master_Catalog_Sample.Buyer      = catalog_attribute_option_global_buyer_name.name;            

UPDATE           A_Master_Catalog_Sample         
      INNER JOIN bob_live_mx.catalog_attribute_option_global_head_buyer_name
           ON A_Master_Catalog_Sample.id_catalog_attribute_option_global_head_buyer=catalog_attribute_option_global_head_buyer_name.id_catalog_attribute_option_global_head_buyer_name
		   #USING ( id_catalog_attribute_option_global_buyer_name )
SET
    A_Master_Catalog_Sample.Head_Buyer = catalog_attribute_option_global_head_buyer_name.name            
;	