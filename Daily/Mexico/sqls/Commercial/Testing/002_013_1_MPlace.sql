DROP TABLE IF EXISTS TMP_MPCat;
CREATE TABLE TMP_MPCat ( INDEX ( SKU_Simple ) )
SELECT
   A_Master_Catalog.SKU_Simple,
   A_Master_Catalog.SKU_Config ,
   A_Master_Catalog.SKU_Name,
   A_Master_Catalog.Supplier as Supplier,
   A_Master_Catalog.Cat_BP,
   A_Master_Catalog.Cat_KPI,
   A_Master_Catalog.Cat1,
   A_Master_Catalog.Cat2,
   A_Master_Catalog.Cat3,
   development_mx.A_E_BI_Marketplace_Commission.Category as Category_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.Since,
   development_mx.A_E_BI_Marketplace_Commission.Fee,
   fulfillment_type
FROM
               A_Master_Catalog 
    INNER JOIN development_mx.A_E_BI_Marketplace_Commission 
            ON A_Master_Catalog.id_Supplier = development_mx.A_E_BI_Marketplace_Commission.id_supplier
WHERE
    IF(   development_mx.A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( curdate() <= development_mx.A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND development_mx.A_E_BI_Marketplace_Commission.Category = "" 
AND development_mx.A_E_BI_Marketplace_Commission.SubCategory = ""
AND development_mx.A_E_BI_Marketplace_Commission.SubSubCategory = ""
AND development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory = ""
AND development_mx.A_E_BI_Marketplace_Commission.perSKU = ""
GROUP BY SKU_Simple
;

REPLACE TMP_MPCat 
SELECT
   A_Master_Catalog.SKU_Simple,
   A_Master_Catalog.SKU_Config ,
   A_Master_Catalog.SKU_Name,
   A_Master_Catalog.Supplier as Supplier,
   A_Master_Catalog.Cat_BP,
   A_Master_Catalog.Cat_KPI,
   A_Master_Catalog.Cat1,
   A_Master_Catalog.Cat2,
   A_Master_Catalog.Cat3,
   development_mx.A_E_BI_Marketplace_Commission.Category as Category_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.Since,
   development_mx.A_E_BI_Marketplace_Commission.Fee,
   fulfillment_type
FROM
               A_Master_Catalog 
    INNER JOIN development_mx.A_E_BI_Marketplace_Commission 
            ON A_Master_Catalog.id_Supplier = development_mx.A_E_BI_Marketplace_Commission.id_supplier
WHERE
    IF(   development_mx.A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( curdate() <= development_mx.A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND development_mx.A_E_BI_Marketplace_Commission.Category = A_Master_Catalog.Cat_BP
AND development_mx.A_E_BI_Marketplace_Commission.SubCategory = ""
AND development_mx.A_E_BI_Marketplace_Commission.SubSubCategory = ""
AND development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory = ""
AND development_mx.A_E_BI_Marketplace_Commission.perSKU = ""
;

REPLACE TMP_MPCat 
SELECT
   A_Master_Catalog.SKU_Simple,
   A_Master_Catalog.SKU_Config ,
   A_Master_Catalog.SKU_Name,
   A_Master_Catalog.Supplier as Supplier,
   A_Master_Catalog.Cat_BP,
   A_Master_Catalog.Cat_KPI,
   A_Master_Catalog.Cat1,
   A_Master_Catalog.Cat2,
   A_Master_Catalog.Cat3,
   development_mx.A_E_BI_Marketplace_Commission.Category as Category_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.Since,
   development_mx.A_E_BI_Marketplace_Commission.Fee,
   fulfillment_type
FROM
               A_Master_Catalog 
    INNER JOIN development_mx.A_E_BI_Marketplace_Commission 
            ON A_Master_Catalog.id_Supplier = development_mx.A_E_BI_Marketplace_Commission.id_supplier
WHERE
    IF(   development_mx.A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( curdate() <= development_mx.A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND development_mx.A_E_BI_Marketplace_Commission.Category = A_Master_Catalog.Cat_BP
AND (    A_Master_Catalog.Cat1 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR A_Master_Catalog.Cat2 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
OR A_Master_Catalog.Cat3 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
     )
AND development_mx.A_E_BI_Marketplace_Commission.SubCategory != ""
AND development_mx.A_E_BI_Marketplace_Commission.perSKU = ""
;

REPLACE TMP_MPCat 
SELECT
   A_Master_Catalog.SKU_Simple,
   A_Master_Catalog.SKU_Config ,
   A_Master_Catalog.SKU_Name,
   A_Master_Catalog.Supplier as Supplier,
   A_Master_Catalog.Cat_BP,
   A_Master_Catalog.Cat_KPI,
   A_Master_Catalog.Cat1,
   A_Master_Catalog.Cat2,
   A_Master_Catalog.Cat3,
   development_mx.A_E_BI_Marketplace_Commission.Category as Category_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.Since,
   development_mx.A_E_BI_Marketplace_Commission.Fee,
   fulfillment_type
FROM
               A_Master_Catalog 
    INNER JOIN development_mx.A_E_BI_Marketplace_Commission 
            ON A_Master_Catalog.id_Supplier = development_mx.A_E_BI_Marketplace_Commission.id_supplier
WHERE
    IF(   development_mx.A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( curdate() <= development_mx.A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND development_mx.A_E_BI_Marketplace_Commission.Category = A_Master_Catalog.Cat_BP
AND (    A_Master_Catalog.Cat1 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR A_Master_Catalog.Cat2 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
OR A_Master_Catalog.Cat3 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
     )
AND (    A_Master_Catalog.Cat1 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
      OR A_Master_Catalog.Cat2 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
OR A_Master_Catalog.Cat3 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
     )
AND development_mx.A_E_BI_Marketplace_Commission.SubSubCategory != ""
AND development_mx.A_E_BI_Marketplace_Commission.perSKU = ""
;

REPLACE TMP_MPCat 
SELECT
   A_Master_Catalog.SKU_Simple,
   A_Master_Catalog.SKU_Config ,
   A_Master_Catalog.SKU_Name,
   A_Master_Catalog.Supplier as Supplier,
   A_Master_Catalog.Cat_BP,
   A_Master_Catalog.Cat_KPI,
   A_Master_Catalog.Cat1,
   A_Master_Catalog.Cat2,
   A_Master_Catalog.Cat3,
   development_mx.A_E_BI_Marketplace_Commission.Category as Category_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.Since,
   development_mx.A_E_BI_Marketplace_Commission.Fee,
   fulfillment_type
FROM
               A_Master_Catalog 
    INNER JOIN development_mx.A_E_BI_Marketplace_Commission 
            ON A_Master_Catalog.id_Supplier = development_mx.A_E_BI_Marketplace_Commission.id_supplier
WHERE
    IF(   development_mx.A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( curdate() <= development_mx.A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND development_mx.A_E_BI_Marketplace_Commission.Category = A_Master_Catalog.Cat_BP
AND (    A_Master_Catalog.Cat1 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR A_Master_Catalog.Cat2 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
OR A_Master_Catalog.Cat3 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
     )
AND (    A_Master_Catalog.Cat1 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
      OR A_Master_Catalog.Cat2 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
OR A_Master_Catalog.Cat3 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
     )
AND (    A_Master_Catalog.Cat1 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory, "%" ) 
      OR A_Master_Catalog.Cat2 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory, "%" ) 
OR A_Master_Catalog.Cat3 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory, "%" ) 
     )
AND development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory != ""
AND development_mx.A_E_BI_Marketplace_Commission.perSKU = ""
;

/*
*  Per SKU
*/
REPLACE TMP_MPCat 
SELECT
   A_Master_Catalog.SKU_Simple,
   A_Master_Catalog.SKU_Config ,
   A_Master_Catalog.SKU_Name,
   A_Master_Catalog.Supplier as Supplier,
   A_Master_Catalog.Cat_BP,
   A_Master_Catalog.Cat_KPI,
   A_Master_Catalog.Cat1,
   A_Master_Catalog.Cat2,
   A_Master_Catalog.Cat3,
   development_mx.A_E_BI_Marketplace_Commission.Category as Category_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.Since,
   development_mx.A_E_BI_Marketplace_Commission.Fee,
   fulfillment_type
FROM
               A_Master_Catalog 
    INNER JOIN development_mx.A_E_BI_Marketplace_Commission 
            ON A_Master_Catalog.id_Supplier = development_mx.A_E_BI_Marketplace_Commission.id_supplier
WHERE
    IF(   development_mx.A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( curdate() <= development_mx.A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND development_mx.A_E_BI_Marketplace_Commission.Category = ""
AND development_mx.A_E_BI_Marketplace_Commission.SubCategory = ""
AND development_mx.A_E_BI_Marketplace_Commission.SubSubCategory = ""
AND development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory = ""
AND development_mx.A_E_BI_Marketplace_Commission.perSKU != ""
AND A_Master_Catalog.SKU_Name like concat( "%" , replace( perSKU , " " , "%" ) , "%" )
;

REPLACE TMP_MPCat 
SELECT
   A_Master_Catalog.SKU_Simple,
   A_Master_Catalog.SKU_Config ,
   A_Master_Catalog.SKU_Name,
   A_Master_Catalog.Supplier as Supplier,
   A_Master_Catalog.Cat_BP,
   A_Master_Catalog.Cat_KPI,
   A_Master_Catalog.Cat1,
   A_Master_Catalog.Cat2,
   A_Master_Catalog.Cat3,
   development_mx.A_E_BI_Marketplace_Commission.Category as Category_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.Since,
   development_mx.A_E_BI_Marketplace_Commission.Fee,
   fulfillment_type
FROM
               A_Master_Catalog 
    INNER JOIN development_mx.A_E_BI_Marketplace_Commission 
            ON     A_Master_Catalog.Supplier = development_mx.A_E_BI_Marketplace_Commission.Bob_Supplier_Name
WHERE
    IF(   development_mx.A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( curdate() <= development_mx.A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND development_mx.A_E_BI_Marketplace_Commission.Category = A_Master_Catalog.Cat_BP
AND development_mx.A_E_BI_Marketplace_Commission.SubCategory = ""
AND development_mx.A_E_BI_Marketplace_Commission.SubSubCategory = ""
AND development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory = ""
AND development_mx.A_E_BI_Marketplace_Commission.perSKU != ""
AND A_Master_Catalog.SKU_Name like concat( "%" , replace( perSKU , " " , "%" ) , "%" )
;

REPLACE TMP_MPCat 
SELECT
   A_Master_Catalog.SKU_Simple,
   A_Master_Catalog.SKU_Config ,
   A_Master_Catalog.SKU_Name,
   A_Master_Catalog.Supplier as Supplier,
   A_Master_Catalog.Cat_BP,
   A_Master_Catalog.Cat_KPI,
   A_Master_Catalog.Cat1,
   A_Master_Catalog.Cat2,
   A_Master_Catalog.Cat3,
   development_mx.A_E_BI_Marketplace_Commission.Category as Category_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.Since,
   development_mx.A_E_BI_Marketplace_Commission.Fee,
   fulfillment_type
FROM
               A_Master_Catalog 
    INNER JOIN development_mx.A_E_BI_Marketplace_Commission 
            ON A_Master_Catalog.id_Supplier = development_mx.A_E_BI_Marketplace_Commission.id_supplier
WHERE
    IF(   development_mx.A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( curdate() <= development_mx.A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND development_mx.A_E_BI_Marketplace_Commission.Category = A_Master_Catalog.Cat_BP
AND (    A_Master_Catalog.Cat1 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR A_Master_Catalog.Cat2 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
OR A_Master_Catalog.Cat3 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
     )
AND development_mx.A_E_BI_Marketplace_Commission.SubCategory != ""
AND development_mx.A_E_BI_Marketplace_Commission.perSKU != ""
AND A_Master_Catalog.SKU_Name like concat( "%" , replace( perSKU , " " , "%" ) , "%" )
;

REPLACE TMP_MPCat 
SELECT
   A_Master_Catalog.SKU_Simple,
   A_Master_Catalog.SKU_Config ,
   A_Master_Catalog.SKU_Name,
   A_Master_Catalog.Supplier as Supplier,
   A_Master_Catalog.Cat_BP,
   A_Master_Catalog.Cat_KPI,
   A_Master_Catalog.Cat1,
   A_Master_Catalog.Cat2,
   A_Master_Catalog.Cat3,
   development_mx.A_E_BI_Marketplace_Commission.Category as Category_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.Since,
   development_mx.A_E_BI_Marketplace_Commission.Fee,
   fulfillment_type
FROM
               A_Master_Catalog 
    INNER JOIN development_mx.A_E_BI_Marketplace_Commission 
            ON A_Master_Catalog.id_Supplier = development_mx.A_E_BI_Marketplace_Commission.id_supplier
WHERE
    IF(   development_mx.A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( curdate() <= development_mx.A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND development_mx.A_E_BI_Marketplace_Commission.Category = A_Master_Catalog.Cat_BP
AND (    A_Master_Catalog.Cat1 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR A_Master_Catalog.Cat2 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
OR A_Master_Catalog.Cat3 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
     )
AND (    A_Master_Catalog.Cat1 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
      OR A_Master_Catalog.Cat2 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
OR A_Master_Catalog.Cat3 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
     )
AND development_mx.A_E_BI_Marketplace_Commission.SubSubCategory != ""
AND development_mx.A_E_BI_Marketplace_Commission.perSKU != ""
AND A_Master_Catalog.SKU_Name like concat( "%" , replace( perSKU , " " , "%" ) , "%" )
;

REPLACE TMP_MPCat 
SELECT
   A_Master_Catalog.SKU_Simple,
   A_Master_Catalog.SKU_Config ,
   A_Master_Catalog.SKU_Name,
   A_Master_Catalog.Supplier as Supplier,
   A_Master_Catalog.Cat_BP,
   A_Master_Catalog.Cat_KPI,
   A_Master_Catalog.Cat1,
   A_Master_Catalog.Cat2,
   A_Master_Catalog.Cat3,
   development_mx.A_E_BI_Marketplace_Commission.Category as Category_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubCategory as SubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubCategory as SubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory as SubSubSubCategory_MPlace,
   development_mx.A_E_BI_Marketplace_Commission.Since,
   development_mx.A_E_BI_Marketplace_Commission.Fee,
   fulfillment_type
FROM
               A_Master_Catalog 
    INNER JOIN development_mx.A_E_BI_Marketplace_Commission 
            ON A_Master_Catalog.id_Supplier = development_mx.A_E_BI_Marketplace_Commission.id_supplier
WHERE
    IF(   development_mx.A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( curdate() <= development_mx.A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND development_mx.A_E_BI_Marketplace_Commission.Category = A_Master_Catalog.Cat_BP
AND (    A_Master_Catalog.Cat1 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR A_Master_Catalog.Cat2 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
OR A_Master_Catalog.Cat3 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
     )
AND (    A_Master_Catalog.Cat1 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
      OR A_Master_Catalog.Cat2 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
OR A_Master_Catalog.Cat3 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
     )
AND (    A_Master_Catalog.Cat1 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory, "%" ) 
      OR A_Master_Catalog.Cat2 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory, "%" ) 
OR A_Master_Catalog.Cat3 like CONCAT( "%",development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory, "%" ) 
     )
AND development_mx.A_E_BI_Marketplace_Commission.SubSubSubCategory != ""
AND development_mx.A_E_BI_Marketplace_Commission.perSKU != ""
AND A_Master_Catalog.SKU_Name like concat( "%" , replace( perSKU , " " , "%" ) , "%" )
;
/*
DELETE FROM TMP_MPCat 
WHERE 
    fulfillment_type in ( "Own Warehouse" )
AND Supplier not in ( select id_supplier from development_mx.A_E_BI_Marketplace_Commission WHERE ownWarehouse = 1 )
;
*/
UPDATE TMP_MPCat 
   INNER JOIN A_Master_Catalog_Sample
      USING ( SKU_Simple )
SET
   isMarketPlace       = 1,
   isMarketPlace_Since = Since
;