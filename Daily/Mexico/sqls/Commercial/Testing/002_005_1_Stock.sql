/*
*  Fields regarding tbl_catalog_product_stock
*/
UPDATE        tbl_catalog_product_stock
   INNER JOIN A_Master_Catalog_Sample 
           ON id_catalog_simple = fk_catalog_simple
SET
   A_Master_Catalog_Sample.stock           = tbl_catalog_product_stock.availablebob,
	 A_Master_Catalog_Sample.Reserved_BOB    = tbl_catalog_product_stock.reservedbob,
	 A_Master_Catalog_Sample.warehouse_stock = tbl_catalog_product_stock.`ownstock`,
	 A_Master_Catalog_Sample.supplier_stock  = tbl_catalog_product_stock.`supplierstock`
;