USE development_mx;
DROP TEMPORARY TABLE IF EXISTS TMP_A_Master_Order_Sample;
CREATE TEMPORARY TABLE TMP_A_Master_Order_Sample ( PRIMARY KEY ( OrderNum ) )
SELECT 
  OrderNum,
  count(*) AS ItemsInOrder,
  SUM( OrderAfterCan  ) AS NetItemsInOrder,
  SUM( OrderBeforeCan ) AS GrossItemsInOrder,
  SUM( PackageWeight  ) AS OrderWeight
FROM 
  development_mx.A_Master_Sample
GROUP BY OrderNum;

UPDATE            development_mx.TMP_A_Master_Order_Sample
       INNER JOIN development_mx.A_Master_Sample
            USING ( OrderNum )
SET
   A_Master_Sample.NetItemsInOrder = development_mx.TMP_A_Master_Order_Sample.NetItemsInOrder,
   A_Master_Sample.ItemsInOrder    = development_mx.TMP_A_Master_Order_Sample.ItemsInOrder,
   A_Master_Sample.OrderWeight     = development_mx.TMP_A_Master_Order_Sample.OrderWeight     
;
