drop table if exists development_mx.WeeklyReport	
;	
create table development_mx.WeeklyReport
(`Date Placed` date not null, `Weekday` varchar(9) not null, `Month` int not null, `Exchange Rate` decimal (15,4) not null,
`Gross Price` decimal (15,4) not null, `Gross Paid Price AT` decimal (15,4) not null, `Gross Price AT` decimal (15,4) not null,
`# Orders` int not null, `# Items` int not null, `Cost` decimal (15,4) not null, `Inbound Cost` decimal (15,4) not null,
`Coupon value` decimal (15,4) not null, `Original Price` decimal (15,4) not null, `MKT Spend` decimal (15,4) not null,
`Visits` int not null, `Branded Visits` decimal (15,4) not null, `Total Customers` int not null, `New Customers` int not null,
`Unique Visitors` int not null, `Pageviews` int not null, `Canceled Orders` decimal (15,4) not null,
`Rejected Orders` decimal (15,4) not null, `Returned Items` int not null, `Returned Value` decimal (15,4) not null,
`CC %` decimal (15,4) not null, `OD %` decimal (15,4) not null, `Paypal %` decimal (15,4) not null,
`Oxxo %` decimal (15,4) not null, `Electronics Sales` decimal (15,4) not null, `Fashion Sales` decimal (15,4) not null,
`Others Sales` decimal (15,4) not null, `Inventory Value` decimal (15,4) not null, `Inventory Online` decimal (15,4) not null,
`SKUs visible` int not null)
;
insert into development_mx.WeeklyReport (`Date Placed`, `Weekday`, `Month`)
select a.Date as `Date Placed`, DATE_FORMAT(a.Date,'%W') as 'Weekday', a.MonthNum as 'Month'
from development_mx.A_Master a
group by a.Date
;
update development_mx.WeeklyReport a
inner join(
select distinct Month_Num, XR	
from development_mx.A_E_BI_ExchangeRate	
where Country = 'MEX'	
) b	
on a.Month = b.Month_Num
set a.`Exchange Rate` = b.XR
;
update development_mx.WeeklyReport a
inner join(
select Date, sum(Price) as `Gross Price`, sum(PaidPriceAfterTax) as `Gross Paid Price AT`, sum(PriceAfterTax) as `Gross Price AT`,
count(distinct OrderNum) as `# Orders`, count(ItemID) as `# Items`, sum(CostAfterTax) as `Cost`,
sum(DeliveryCostSupplier) as `Inbound Cost`, sum(CouponValueAfterTax) as `Coupon value`,
sum(OriginalPrice) as `Original Price`, count(distinct CustomerNum) as `Total Customers`
from development_mx.A_Master
where OrderBeforeCan = 1
group by Date
) c
on a.`Date Placed` = c.Date
set a.`Gross Price` = c.`Gross Price`, a.`Gross Paid Price AT` = c.`Gross Paid Price AT`, a.`Gross Price AT` = c.`Gross Price AT`,
a.`# Orders` = c.`# Orders`, a.`# Items` = c.`# Items`, a.`Cost` = c.`Cost`, a.`Inbound Cost` = c.`Inbound Cost`,
a.`Coupon value` = c.`Coupon value`, a.`Original Price` = c.`Original Price`, a.`Total Customers` = c.`Total Customers`
;
update development_mx.WeeklyReport a
inner join(
select date, sum(cost) as `MKT Spend`
from marketing_report.global_report
where country = 'Mexico'
group by date
) d
on a.`Date Placed` = d.date
set a.`MKT Spend` = d.`MKT Spend`
;
update development_mx.WeeklyReport a
inner join(
select date, sum(visits) as `Visits`
from SEM.campaign_ad_group_mx	
group by Date
) e
on a.`Date Placed` = e.date
set a.`Visits` = e.`Visits`
;
#*** Branded Visits
;
update development_mx.WeeklyReport a
inner join(
select Date, sum(if(First_Gross_Order=OrderNum,1,0)) as `New Customers`, sum(Cancelled) as `Canceled Orders`,
sum(Rejected) as `Rejected Orders`
from development_mx.Out_SalesReportOrder
group by Date
) f
on a.`Date Placed` = f.Date
set a.`New Customers` = f.`New Customers`, a.`Canceled Orders` = f.`Canceled Orders`, a.`Rejected Orders` = f.`Rejected Orders`
;
#*** Unique Visitors
;
#*** Pageviews
;
update development_mx.WeeklyReport a
inner join(
select Date, count(ItemID) as `Returned Items`, sum(Price) as `Returned Value`
from development_mx.A_Master
where Returned = 1
group by Date
) g
on a.`Date Placed` = g.Date
set a.`Returned Items` = g.`Returned Items`, a.`Returned Value` = g.`Returned Value`
;
update development_mx.WeeklyReport a
inner join(
select Date, count(ItemID) as `CC %`
from development_mx.A_Master
where OrderBeforeCan = 1
and PaymentMethod in ('Amex_Gateway', 'Banorte_Payworks')
group by Date
) h
on a.`Date Placed` = h.Date
set a.`CC %` = h.`CC %`
;
update development_mx.WeeklyReport a
inner join(
select Date, count(ItemID) as `OD %`
from development_mx.A_Master
where OrderBeforeCan = 1
and PaymentMethod in ('CashOnDelivery_Payment', 'CreditCardOnDelivery_Payment')
group by Date
) i
on a.`Date Placed` = i.Date
set a.`OD %` = i.`OD %`
;
update development_mx.WeeklyReport a
inner join(
select Date, count(ItemID) as `Paypal %`
from development_mx.A_Master
where OrderBeforeCan = 1
and PaymentMethod in ('Paypal_Express_Checkout')
group by Date
) j
on a.`Date Placed` = j.Date
set a.`Paypal %` = j.`Paypal %`
;
update development_mx.WeeklyReport a
inner join(
select Date, count(ItemID) as `Oxxo %`
from development_mx.A_Master
where OrderBeforeCan = 1
and PaymentMethod in ('Oxxo_Direct')
group by Date
) k
on a.`Date Placed` = k.Date
set a.`Oxxo %` = k.`Oxxo %`
;
update development_mx.WeeklyReport a
inner join(
select Date, sum(Price) as `Electronics Sales`
from development_mx.A_Master
where OrderBeforeCan = 1
and Cat1 in ('Electrónicos', 'TV, Audio y Video', 'Videojuegos')
group by Date
) l
on a.`Date Placed` = l.Date
set a.`Electronics Sales` = l.`Electronics Sales`
;
update development_mx.WeeklyReport a
inner join(
select Date, sum(Price) as `Fashion Sales`
from development_mx.A_Master
where OrderBeforeCan = 1
and Cat1 in ('Fashion')
group by Date
) m
on a.`Date Placed` = m.Date
set a.`Fashion Sales` = m.`Fashion Sales`
;
update development_mx.WeeklyReport a
inner join(
select Date, sum(Price) as `Others Sales`
from development_mx.A_Master
where OrderBeforeCan = 1
and Cat1 in ('Home and Living', 'Belleza')
group by Date
) n
on a.`Date Placed` = n.Date
set a.`Others Sales` = n.`Others Sales`
;
#*** Inventory Value
;
#*** Inventory OnLine
;
update development_mx.WeeklyReport a
inner join(
select `Day`, count as `SKUs visible`
from development_mx.A_E_M1_SKUs_Visible
) o
on a.`Date Placed` = o.`Day`
set a.`SKUs visible` = o.`SKUs visible`
;
