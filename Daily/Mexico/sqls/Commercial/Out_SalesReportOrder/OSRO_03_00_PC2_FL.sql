UPDATE Out_SalesReportOrder SET   PC2_FL = PC_OnePFive;
UPDATE            Out_SalesReportOrder   
       INNER JOIN A_E_BI_FLCosts 
            USING ( Month_Num )
SET
   PC2_FL = PC_OnePFive - FL_total_cost;
