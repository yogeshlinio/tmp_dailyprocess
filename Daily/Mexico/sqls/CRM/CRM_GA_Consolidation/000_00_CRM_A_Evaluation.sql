DROP TABLE IF EXISTS A_Evaluation;
CREATE TABLE A_Evaluation ( PRIMARY KEY ( Campaign ) )
SELECT  
   "Mexico" as Country,
   date_format(Date, '%Y%m') `Month`,  
   production.week_iso( date ) AS `Week`, 
   Date,   
   `Mailing-Name` AS Campaign, 
   SUM(MessageCount) AS Sent,  
   SUM(BounceCount)  AS Bounces, 
   SUM(OpenCount)  AS Openings,   
   SUM(ClickCount) AS Clicks, 
   SUM(BounceCount) / SUM(MessageCount)  AS `%Bounce`,    
   SUM(OpenCount)  / SUM(MessageCount) AS `%Open`,  
   SUM(ClickCount) / SUM(OpenCount) AS `%Click` 
FROM   
         A_CRM_CAMPAIGN_EMAIL 
WHERE DATE_FORMAT(DATE,'%Y%m') >= 201312
GROUP BY   `Mailing-Name`;

INSERT  production.table_monitoring_log
SELECT 
   null,
   "Mexico" AS country,
   "CRM_MX.A_Evaluation" AS table_name,
   "finish"    AS step,
   now()       AS updated_at,
   MAX( Date ) AS key_date,
   COUNT(*)    AS total_rows,
   COUNT(*)    AS total_rows_check
FROM
   A_CRM_CAMPAIGN_EMAIL 
;