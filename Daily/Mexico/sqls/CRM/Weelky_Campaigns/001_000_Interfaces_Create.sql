INSERT IGNORE interface_bounces    SELECT * FROM interface_bounces_sample;
INSERT IGNORE interface_clicks     SELECT * FROM interface_clicks_sample;
INSERT IGNORE interface_messages   SELECT * FROM interface_messages_sample;
INSERT IGNORE interface_openings   SELECT * FROM interface_openings_sample;

INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  step,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Mexico',
  'CRM.A_CRM_INTERFACE_EMAIL',
  'start',
  now(),
  now(),
  0,
  0
;

DROP TABLE IF EXISTS A_CRM_INTERFACE_EMAIL;
CREATE TABLE A_CRM_INTERFACE_EMAIL (
  `Interface-ID` VARCHAR (20) NOT NULL,
  Alias VARCHAR (255) DEFAULT NULL,
  Description VARCHAR (50) NOT NULL,
  Email VARCHAR (50) NOT NULL,
  `Dispatch-Start` date NOT NULL,
  `Link-URL` varchar(50) NOT NULL,
  `Message-Timestamp` date NOT NULL,
  Message bit NOT NULL,
  MessageCount int NOT NULL,

  dateOpen date NOT NULL,
  OPEN bit NOT NULL,
  OpenCount int NOT NULL,

  dateClick date NOT NULL,
  click bit NOT NULL,
  ClickCount int NOT NULL,

  dateBounce date NOT NULL,
  bounce bit NOT NULL,
  bounce_reason text NOT NULL,
  BounceCount int NOT NULL,

  created_at datetime,
  updated_at datetime,
  PRIMARY KEY (`Interface-ID`,   email)
);

INSERT A_CRM_INTERFACE_EMAIL (

  `Interface-ID`,
  Alias,
  email,
  `dateBounce`,
  created_at,
  updated_at
) SELECT
  `Interface-ID`,
  Alias,
  UniqueKey,
  date(`Timestamp`) AS Date,
  now(),
  now()
FROM
  interface_bounces
GROUP BY
  `Interface-ID`, UniqueKey;

REPLACE A_CRM_INTERFACE_EMAIL (
  `Interface-ID`,
  Alias,
  email,
  `dateClick`,
  created_at,
  updated_at
) SELECT
  `Profil-ID`,
  `Interface-ID`,
  Alias
  UniqueKey,
  `Timestamp`,
  now(),
  now()
FROM
  interface_clicks
GROUP BY
  `Interface-ID`, UniqueKey;

REPLACE A_CRM_INTERFACE_EMAIL (
  `Interface-ID`,
  Alias,
  email,
  `dateOpen`,
  created_at,
  updated_at
) SELECT
  `Interface-ID`,
  Alias,
  UniqueKey,
  `Timestamp`,
  now(),
  now()
FROM
  interface_openings
GROUP BY
  `Interface-ID`, UniqueKey;

REPLACE A_CRM_INTERFACE_EMAIL (
  `Interface-ID`,
  Alias,
  email,
  `dateOpen`,
  created_at,
  updated_at
) 
SELECT
  `Interface-ID`,
  Alias,
  UniqueKey,
  DATE(`Timestamp`),
  now() ,
  now()
FROM
  interface_messages
GROUP BY
  `Interface-ID`, UniqueKey;
  
