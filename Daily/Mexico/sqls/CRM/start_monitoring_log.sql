INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Mexico', 
  'CRM.loading_channels',
  'start',
  now(),
  now(),
  0,
  0
;
