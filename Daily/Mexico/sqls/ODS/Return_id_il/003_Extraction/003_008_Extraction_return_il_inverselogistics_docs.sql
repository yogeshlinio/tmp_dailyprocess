UPDATE            return_il_Sample_@v_countryPrefix@ as return_il_Sample
       INNER JOIN opertaions_@v_countryPrefix@.pro_inverse_logistics_docs  ON return_il_Sample.item_id = pro_inverse_logistics_docs.item_id
SET
   /*Defaults and Simple Extraction*/
    return_il_Sample.date_inbound_il = pro_inverse_logistics_docs.date_entrance_ilwh,
return_il_Sample.shipping_carrier_tracking_code_inverse = pro_inverse_logistics_docs.dhl_tracking_code,
return_il_Sample.il_type = pro_inverse_logistics_docs.status,
return_il_Sample.reason_for_entrance_il = pro_inverse_logistics_docs.reason,
return_il_Sample.action_il = pro_inverse_logistics_docs.ilwh_1st_step,
return_il_Sample.damaged_item = pro_inverse_logistics_docs.damaged_item,
return_il_Sample.damaged_package = pro_inverse_logistics_docs.damaged_package,
return_il_Sample.comments_il = pro_inverse_logistics_docs.comments,
return_il_Sample.return_accepted = pro_inverse_logistics_docs.return_accepted,
return_il_Sample.position_il = pro_inverse_logistics_docs.ilwh_position,
return_il_Sample.date_exit_il = pro_inverse_logistics_docs.date_exit_ilwh,
return_il_Sample.date_canceled_il = pro_inverse_logistics_docs.date_cancelled,
return_il_Sample.analyst_il  = pro_inverse_logistics_docs.entered_to_ilwh_by,
return_il_Sample.comments = pro_inverse_logistics_docs.comments_main;
 
   
   
   
   
