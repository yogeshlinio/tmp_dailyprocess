DROP TEMPORARY TABLE IF EXISTS TMP_Returned;
CREATE TEMPORARY TABLE TMP_Returned (  index( fk_sales_order_item  ) )
SELECT
fk_sales_order_item   
FROM  bob_live_mx.sales_order_item_status_history
WHERE
#returned
fk_sales_order_item_status  = 8
GROUP BY  
fk_sales_order_item;

DROP TEMPORARY TABLE IF EXISTS TMP_Deliver;
CREATE TEMPORARY TABLE TMP_Deliver (  index( fk_sales_order_item  ) )
SELECT
fk_sales_order_item   
FROM  bob_live_mx.sales_order_item_status_history
WHERE
#delivered, delivered_electronically
fk_sales_order_item_status  in (52,98)
GROUP BY  
fk_sales_order_item;


DROP TEMPORARY TABLE IF EXISTS TMP_RefundedNeeded;
CREATE TEMPORARY TABLE TMP_RefundedNeeded (  index( fk_sales_order_item  ) )
SELECT
fk_sales_order_item   
FROM  bob_live_mx.sales_order_item_status_history
WHERE
   fk_sales_order_item_status  in ( 63, 87 )
GROUP BY  
fk_sales_order_item;

DROP TEMPORARY TABLE IF EXISTS TMP;
CREATE TEMPORARY TABLE TMP ( KEY ( fk_sales_order_item ) )
SELECT fk_sales_order_item ,
#refund_needed
 MAX( IF( fk_sales_order_item_status = 87 , created_at  , null  ) ) AS fecha FROM  
bob_live_mx.sales_order_item_status_history
WHERE
    fk_sales_order_item IN     ( SELECT * FROM TMP_Deliver ) 
AND fk_sales_order_item IN     ( SELECT * FROM TMP_RefundedNeeded ) 
AND fk_sales_order_item NOT IN ( SELECT * FROM TMP_Returned ) 
GROUP BY fk_sales_order_item ;

REPLACE TMP
#returned
SELECT fk_sales_order_item , MAX( IF( fk_sales_order_item_status = 8 , created_at  , null  ) ) FROM  
bob_live_mx.sales_order_item_status_history
WHERE
   
fk_sales_order_item IN ( SELECT * FROM TMP_Returned ) 
GROUP BY fk_sales_order_item ;

UPDATE            Id_Sales_Order_Item_Sample_@v_countryPrefix@
       INNER JOIN TMP  ON Id_Sales_Order_Item_Sample_@v_countryPrefix@.ItemId = TMP.fk_sales_order_item 
SET
 Id_Sales_Order_Item_Sample_@v_countryPrefix@.Refunded = 1,
 Id_Sales_Order_Item_Sample_@v_countryPrefix@.`DateRefunded` = TMP.Fecha
;
/*
*
*  MX_018_001	DateDelivered
*/
UPDATE 
               Id_Sales_Order_Item_Sample_@v_countryPrefix@  
    INNER JOIN operations_mx.out_order_tracking b
            ON Id_Sales_Order_Item_Sample_@v_countryPrefix@.ItemID = b.item_id
SET
   Id_Sales_Order_Item_Sample_@v_countryPrefix@.DateDelivered =  b.date_delivered,
   Id_Sales_Order_Item_Sample_@v_countryPrefix@.Delivered =  1
   
WHERE 
   b.date_delivered is not null
;

DROP TEMPORARY TABLE IF EXISTS TMP_Returned;
CREATE TEMPORARY TABLE TMP_Returned (  index( fk_sales_order_item  ) )
SELECT
fk_sales_order_item   
FROM  bob_live_mx.sales_order_item_status_history
WHERE
#returned
fk_sales_order_item_status  = 8
GROUP BY  
fk_sales_order_item;

DROP TEMPORARY TABLE IF EXISTS TMP_Deliver;
CREATE TEMPORARY TABLE TMP_Deliver (  index( fk_sales_order_item  ) )
SELECT
fk_sales_order_item   
FROM  bob_live_mx.sales_order_item_status_history
WHERE
#delivered, delivered_electronically
fk_sales_order_item_status  in (52,98)
GROUP BY  
fk_sales_order_item;

/*
* MX_018_002 Delivered
*/
DROP   TEMPORARY TABLE IF EXISTS A_Delivered;
CREATE TEMPORARY TABLE A_Delivered ( INDEX ( fk_sales_order_item  ) )
SELECT
 *
FROM
    bob_live_mx.sales_order_item_status_history
WHERE
   bob_live_mx.sales_order_item_status_history.fk_sales_order_item_status IN ( 52 , 98 )
GROUP BY fk_sales_order_item
;

UPDATE           A_Delivered
      INNER JOIN Id_Sales_Order_Item_Sample_@v_countryPrefix@
              ON A_Delivered.fk_sales_order_item = Id_Sales_Order_Item_Sample_@v_countryPrefix@.ItemID 
SET
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.DateDelivered = A_Delivered.created_at,
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.Delivered = 1;

UPDATE Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.NetDelivered = 1
WHERE	
fk_sales_order_item_status  =87
;

DROP TEMPORARY TABLE IF EXISTS TMP;
CREATE TEMPORARY TABLE TMP ( KEY ( fk_sales_order_item ) )
SELECT fk_sales_order_item ,
#refund_needed
 MAX( IF( fk_sales_order_item_status = 87 , created_at  , null  ) ) AS fecha FROM  
bob_live_mx.sales_order_item_status_history
WHERE
    fk_sales_order_item IN     ( SELECT * FROM TMP_Deliver ) 
AND fk_sales_order_item IN     ( SELECT * FROM TMP_RefundedNeeded ) 
AND fk_sales_order_item NOT IN ( SELECT * FROM TMP_Returned ) 
GROUP BY fk_sales_order_item ;

REPLACE TMP
#returned
SELECT fk_sales_order_item , MAX( IF( fk_sales_order_item_status = 8 , created_at  , null  ) ) FROM  
bob_live_mx.sales_order_item_status_history
WHERE
    fk_sales_order_item_status  in ( select id_sales_order_item_status from bob_live_mx.sales_order_item_status
                where id_sales_order_item_status IN ( 6 /*, 52 No existe en MX*/ ) )
;

UPDATE Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.DeliveredReturn = 1
WHERE
        Delivered     = 1
    AND NetDelivered  = 0;
