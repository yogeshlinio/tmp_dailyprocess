/*
* MX_001_001 Paid Price CiberSanta
*/
 #Query: A 217 U Ciber Santa
call production.monitoring_step( "ODS.Sku_simple" , "@v_country@" , "catalog_config" , 8000  ); 
UPDATE            Id_Sales_Order_Item_Sample_@v_countryPrefix@ AS Id_Sales_Order_Item_Sample
       INNER JOIN Id_Sales_Order_Item_Key_Map
	        USING ( Country , Id_Sales_Order_Item )
       INNER JOIN Sku_Simple_Sample_@v_countryPrefix@ AS Sku_Simple
	        USING ( Country , Sku_Simple )
       INNER JOIN @development@.A_E_Ciber_Santa 
               ON      Sku_Simple.SKU_Config = development_mx.A_E_Ciber_Santa.SKU
                  AND  date_format( Id_Sales_Order_Item_Sample.date_order_placed , "%Y-%m-%d" ) = development_mx.A_E_Ciber_Santa.Date

SET 
   Id_Sales_Order_Item_Sample.Paid_Price           =   Id_Sales_Order_Item_Sample.Price - A_E_Ciber_Santa.Subsidio,
   Id_Sales_Order_Item_Sample.Paid_Price_After_Tax = ( Id_Sales_Order_Item_Sample.Price - A_E_Ciber_Santa.Subsidio) * 0.86
;