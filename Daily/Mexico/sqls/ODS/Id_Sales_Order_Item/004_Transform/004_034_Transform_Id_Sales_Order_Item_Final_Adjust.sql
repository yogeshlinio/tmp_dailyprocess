 #Query: M1_ShippingCost_Junio
UPDATE development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@ SET 
development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCost = 104
WHERE development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.Itemid=267502;

/*
* MX_099_001 Costo de Revistas
*/
 #Query: 7_M1_Costo0Revistas
UPDATE            development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@ 
       INNER JOIN A_E_6_M1_Costos_Revistas 
               ON development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.SKUSimple = A_E_6_M1_Costos_Revistas.SKU_Simple 
SET 
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCost    = 0, 
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.DeliveryCostSupplier = 0;
	
	
/*
* MX_099_004 Ordenes coorporativas
*/
 #Query: M1_CostoCorporativo
UPDATE            Id_Sales_Order_Item_Sample_@v_countryPrefix@  
       INNER JOIN A_E_M1_Ordenes_Corporativas 
               ON Id_Sales_Order_Item_Sample_@v_countryPrefix@.OrderNum = A_E_M1_Ordenes_Corporativas.Order 
SET 
   Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingFee  = IF( useShippingFee  = 1, A_E_M1_Ordenes_Corporativas.ShippingFee,  Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingFee  ) , 
   Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCost = IF( useShippingCost = 1, A_E_M1_Ordenes_Corporativas.ShippingCost, Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCost ) , 
   Id_Sales_Order_Item_Sample_@v_countryPrefix@.PaymentFees  = IF( usePaymentFee   = 1, A_E_M1_Ordenes_Corporativas.PaymentFee  , Id_Sales_Order_Item_Sample_@v_countryPrefix@.PaymentFees  ) 
;

/*
* MX_099 Payment_Fee / Transaction_Fee
*/
UPDATE development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET
    PaymentFees = TransactionFeeAfterTax
WHERE Date >= "2013-07-01"
;
/*
* MX_099 HeadPhones Dree
*/
UPDATE            development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@
       INNER JOIN development_mx.A_E_BI_PatchHeadphonesDree
            USING ( ItemId )
       INNER JOIN development_mx.Out_SalesReportItem
            USING ( ItemId )
SET
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.Cost         = development_mx.Out_SalesReportItem.Cost_OMS_BOB,
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.CostAfterTax = development_mx.Out_SalesReportItem.Cost_after_tax_OMS_BOB
;

UPDATE development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.Cost         = 0,
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.CostAfterTax = 0
WHERE
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ItemId = 210928
;
/*
Descripcion: Se actualiza Cost y CostAfterTax, debido a un error al capturar dichos campos
Solicitud: Jorge Nieto
Fecha: 15-01-2014
*/
UPDATE development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.Cost         = 2882,
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.CostAfterTax = 2882/1.16
WHERE
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ItemID=813472;
;
