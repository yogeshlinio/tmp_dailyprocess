/*
*   MX_001_001_Coupons_Visa Subsidios
*/
#Query: A 127 U Actual_Paid_Price/Coupon (Visa Subsidios)
UPDATE (           development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@
        INNER JOIN development_mx.COM_Visa_Promotion_17_Products 
                ON     (development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.SKUConfig  = COM_Visa_Promotion_17_Products.SKU_Config) 
                   AND (development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.CouponCode = COM_Visa_Promotion_17_Products.Coupon_Code)) 
SET 
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingFee           = development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingFee         + (COM_Visa_Promotion_17_Products.Shipping_Fee_Charged_to_VISA_after_VAT/development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ItemsInOrder)
;

/*
* MX_024 Shipping_COST = 0
*/
 #Query: 2 M1_ShippingCost_0
UPDATE development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@ 
SET 
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCost = 0
WHERE 
    development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.CouponCode="MKT0xfgVK" 
 Or development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.CouponCode="MKT1eDvI7";

/*
*  MX_008_002 Shipping_Cost Dulces Anahuac
*/
 #Query: A 218 U Dulces Anahuac
UPDATE development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET 
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCost = 0, 
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.FLCSCost    = 0
WHERE 
   Left(CouponCode,7)="DEPCHIP";

