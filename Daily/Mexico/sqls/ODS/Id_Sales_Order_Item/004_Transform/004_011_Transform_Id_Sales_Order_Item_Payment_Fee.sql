UPDATE development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET
   PaymentFees =  development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.PaidPrice /( 1 + development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.TaxPercent / 100 )  * ( development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.Fees / 100 )
                 +development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ExtraCharge
;

/*
*   MX_001_006_Paypal_Vouchers
*/
UPDATE development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@
 SET 
    development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.CouponValue           = 0, 
    development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.CouponValueAfterTax   = 0, 
    development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.PaymentFees           = 0
    #Id_Sales_Order_Item_Sample_@v_countryPrefix@.Commercial_price_after_tax = Id_Sales_Order_Item_Sample_@v_countryPrefix@.price_after_tax
WHERE 
     development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.CouponCode in ( "COM1Tw3Bz" ,
                                                    "COM1QuGJR" , 
                                                    "COMq517VW" ,
                                                    "COMc4jiTb"  ,  
                                                    "COM57OMUy" )
  Or development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.CouponCode like "%MKTpaypal%"
;
