/*
*  MX_012_001 Interest
*/
DROP TEMPORARY TABLE IF EXISTS development_mx.A_FactorInterest;
CREATE TEMPORARY TABLE development_mx.A_FactorInterest (
    OrderNum int,
    id_sales_order int,
    Total_Interests DECIMAL(15,5),
    Order_PriceAfterTax decimal(15,5),
    Order_Items int,

    OrderNet_PriceAfterTax decimal(15,5),
    OrderNet_Items int,

    PRIMARY KEY ( OrderNum ),
    INDEX ( id_sales_order )
);

INSERT A_FactorInterest
SELECT
  OrderNum,
  IdSalesOrder AS id_sales_order,
  0 AS Total_Interests,
  SUM( PriceAfterTax ) as Order_PriceAfterTax,
  COUNT(*) AS Order_Items,
  SUM( IF( OrderAfterCan = 1 , PriceAfterTax , 0 ) ) as OrderNet_Price_after_tax,
  SUM( IF( OrderAfterCan = 1 , 1, 0 ) ) AS OrderNet_Items
FROM
  development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@
GROUP BY OrderNum;

UPDATE            development_mx.A_FactorInterest
       INNER JOIN bob_live_mx.sales_order 
               ON  A_FactorInterest.id_sales_order = bob_live_mx.sales_order.id_sales_order
SET
     A_FactorInterest.Total_Interests = bob_live_mx.sales_order.total_interests;

UPDATE            development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@
       INNER JOIN A_FactorInterest USING ( OrderNum )
SET
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.Interest   = ( A_FactorInterest.Total_Interests *
                                                     (development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.PriceAfterTax /
                                                      A_FactorInterest.Order_PriceAfterTax) 
                                                   ) /
                                                   ( 1 + taxPercent / 100 )  ,
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.NetInterest = IF( OrderAfterCan = 1,
                                                        ( A_FactorInterest.Total_Interests *
                                                          (development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.PriceAfterTax /
                                                           A_FactorInterest.OrderNet_PriceAfterTax)
                                                        )/
                                                        ( 1 + taxPercent / 100 )  ,
                                                        0
                                                      )
;

UPDATE            development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.MCI            = IF( Interest        > 0 , 1, 0) ,
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.NetMCI         = IF( NetInterest     > 0 , 1, 0 ),
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.MSI            = IF(    Interest = 0
                                                      AND Installment > 0, 1 , 0),
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.InstallmentFee = IF( Interest  > 0 , Interest , InstallmentFee )   
;
