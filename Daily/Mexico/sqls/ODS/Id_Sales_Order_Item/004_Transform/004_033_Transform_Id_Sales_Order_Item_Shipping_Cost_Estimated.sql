/*
*  MX_008_001 Shipping_CostEstimated
*/
#Query 1: Inicializacion de varible ShippingCostEstimated
UPDATE development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@ 
SET 
    development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCostEstimated =0;

#Query 2: -1 a todo lo de inventario
UPDATE development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@ 
SET 
    development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCostEstimated =-1
WHERE fulfillmentTypeReal != 'dropshipping'
and fulfillmentTypeReal !=''
and fulfillmentTypeReal is not null
;
####Validaciones de Shipping Cost previas
/*
* MX_024 Shipping_COST = 0
*/
 #Query: 2 M1_ShippingCost_0
UPDATE development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@ 
SET 
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCostEstimated = 0
WHERE 
    development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.CouponCode="MKT0xfgVK" 
 Or development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.CouponCode="MKT1eDvI7";

/*
*  MX_008_002 Shipping_Cost Dulces Anahuac
*/
 #Query: A 218 U Dulces Anahuac
UPDATE development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET 
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCostEstimated = 0, 
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.FLCSCost    = 0
WHERE 
   Left(CouponCode,7)="DEPCHIP";

/*
* MX_099_001 Costo de Revistas
*/
 #Query: 7_M1_Costo0Revistas
UPDATE            development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@
       INNER JOIN A_E_6_M1_Costos_Revistas 
               ON development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.SKUSimple = A_E_6_M1_Costos_Revistas.SKU_Simple 
SET 
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCostEstimated    = 0;

/*
*  Inventory
*/
DROP TEMPORARY TABLE IF EXISTS development_mx.TMP_out_order_tracking;
CREATE TEMPORARY TABLE development_mx.TMP_out_order_tracking ( KEY ( item_id ) )
SELECT
    CAST(  item_id AS DECIMAL ) AS item_id
FROM
   operations_mx.out_order_tracking
WHERE
   fulfillment_type_real = "dropshipping"
;

UPDATE        development_mx.TMP_out_order_tracking
   INNER JOIN development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@
           ON item_id = ItemId
SET
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCostEstimated = 0
WHERE MonthNum >= date_format( now() - INTERVAL 3 MONTH , "%Y%m" ) 
;

/*
* MX_099_004 Ordenes coorporativas
*/
 #Query: M1_CostoCorporativo
UPDATE            Id_Sales_Order_Item_Sample_@v_countryPrefix@  
       INNER JOIN A_E_M1_Ordenes_Corporativas 
               ON Id_Sales_Order_Item_Sample_@v_countryPrefix@.OrderNum = A_E_M1_Ordenes_Corporativas.Order 
SET 
   Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCostEstimated = IF(useShippingCost = 1, A_E_M1_Ordenes_Corporativas.ShippingCost, Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCostEstimated) 
;

#######
	
#Query 3: 0 a todo lo -1  que no este en el join
UPDATE development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@ 
join operations_mx.out_stock_hist 
  on operations_mx.out_stock_hist.item_id
= development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ItemID
SET 
    development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCostEstimated =0
WHERE ShippingCostEstimated =-1 and in_stock=0;

#query3_1: 0 a todas las ordenes que tengan solo un item en la Orden
UPDATE development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@ 
SET 
    development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCostEstimated =0
WHERE ItemsInOrder=1
and ShippingCostEstimated=-1;


#Query 4: Mas de un item por orden_carrier
DROP TEMPORARY TABLE IF  EXISTS tmp_order_est_ship;
CREATE TEMPORARY  TABLE tmp_order_est_ship (PRIMARY KEY(OrderNum))
SELECT
  OrderNum,
	sum(ShippingCostEstimated) as SumShippingCostEstimated
FROM  
  Id_Sales_Order_Item_Sample_@v_countryPrefix@
#WHERE  
#  OrderAfterCan = 1
GROUP BY OrderNum;

#Query 5: se pone 0 a todas las ordenes que contengan solo un item con valor -1 
UPDATE development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@ 
join tmp_order_est_ship on tmp_order_est_ship.OrderNum=Id_Sales_Order_Item_Sample_@v_countryPrefix@.OrderNum
SET 
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCostEstimated =0
where tmp_order_est_ship.SumShippingCostEstimated =-1;

#
UPDATE development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@
join(
SELECT
	OrderNum as Order_Num,
	sum(ShippingCostEstimated) as SumShippingCost
FROM
	development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@
#where OrderAfterCan=1
where ShippingCostEstimated=-1
GROUP BY 
	OrderNum,
	Courier) aux
set Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCostEstimated = if(aux.SumShippingCost = -1,0,Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCostEstimated)
where Id_Sales_Order_Item_Sample_@v_countryPrefix@.OrderNum = aux.Order_Num
; 


#Query 6:Se calcula el peso voluumetrico por orden (solo ordenes a estimar)
DROP TABLE IF  EXISTS development_mx.tmp_VolumeWeight;
CREATE TABLE development_mx.tmp_VolumeWeight (PRIMARY KEY(OrderNum))
SELECT
	OrderNum as OrderNum,
	sum(ShippingCostEstimated) as SumShippingCost,
	sum(VolumeWeight) as SumVolumeWeight,
    count(*) as ItemsInOrder_est,
	PostCode,
	PaymentMethod,
	Courier,
	ShippingCostEstimated
FROM
	development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@
where ShippingCostEstimated =-1 
#and OrderAfterCan=1
GROUP BY
	OrderNum;


#query 7: se hace el mach con SRT
DROP TABLE IF  EXISTS development_mx.tmp_ShippingCostEstimated;
CREATE TABLE development_mx.tmp_ShippingCostEstimated (PRIMARY KEY(OrderNum))
SELECT
	a.PostCode,
	a.OrderNum,
	a.PaymentMethod,
	a.SumVolumeWeight,
	a.Courier,	
	a.ItemsInOrder_est,
	a.ShippingCostEstimated,
	b.method_name,
	b.from_postcode,
	b.to_postcode,
	b.priority,
	b.carrier_name,
	b.shipment_base_cost,
	b.base_weight,
	b.shipment_cost_per_kilogram,
	if(a.SumVolumeWeight <= b.base_weight, b.shipment_base_cost, b.shipment_base_cost+CEIL(a.SumVolumeWeight-b.base_weight)*b.shipment_cost_per_kilogram) as ShippingCostE
FROM
	development_mx.tmp_VolumeWeight a
INNER JOIN development_mx.SRT b ON a.PostCode BETWEEN b.from_postcode
AND b.to_postcode
AND a.SumVolumeWeight BETWEEN b.from_weight AND b.to_weight
AND b.method_name =
IF (a.PaymentMethod = 'CashOnDelivery_Payment',
	'COD',
IF (
	a.PaymentMethod = 'CreditCardOnDelivery_Payment',
	'COD',
	'PREPAID'
)
)
and a.Courier=b.carrier_name
GROUP BY a.OrderNum;


#Se hace el Update de ShippingCost estimado en A_master
UPDATE development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@
JOIN tmp_ShippingCostEstimated b ON b.OrderNum = Id_Sales_Order_Item_Sample_@v_countryPrefix@.OrderNum
AND b.ShippingCostEstimated = - 1
SET Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCostEstimated = (
	b.shipment_base_cost / ItemsInOrder_est
) + (
	VolumeWeight / SumVolumeWeight
) * (
	ShippingCostE - b.shipment_base_cost
);

#Se hace el Update de ShippingCost estimado en A_master
UPDATE development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET ShippingCostEstimated = ShippingCost
where ShippingCostEstimated <=0;
