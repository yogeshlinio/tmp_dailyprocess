/*
* MX_009_001 Shipping Fee
*/
/* 
#Query: M1_ShipFee_table
UPDATE development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@ 
SET
   ShippingFee = round(((Id_Sales_Order_Item_Sample_@v_countryPrefix@.PackageWeight/ development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.OrderWeight )*Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingFee),3) 
;*/
/*
* MX_009_001 Shipping Fee
*/ 
#Query: M1_ShipFee_table
UPDATE Id_Sales_Order_Item_Sample_@v_countryPrefix@ 
SET
   ShippingFeeAfterTax = Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingFee / ( 1 +  ( Id_Sales_Order_Item_Sample_@v_countryPrefix@.TaxPercent / 100 ) )          
;


/*
* MX_009_002 
*/
 #Query: M1_ShipFee_CAC
UPDATE development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@ 
SET 
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingFee = 55
WHERE 
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.CouponCode In ( "CAC0xY3Iz","CACs4tyXP")
;