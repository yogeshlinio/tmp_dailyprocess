/*
*  MX_008_001 Shipping_Cost 201210 201211
*/
/*
* MX_008_003 Shipping_Cost from June
*/
 #Query: M1_ShipmentCostFromJune13
UPDATE development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@ 
SET 
    development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCost = ( development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShipmentCost/( 1.2 * 0.9 ) ) * 1.05
WHERE (((development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.Date)>="2013/06/01"));

/*
*  MX_008_000 Package_Weight
*/
UPDATE        development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@ 
   INNER JOIN development_mx.Out_SalesReportItem 
        USING ( ItemID )
SET
   #development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.PackageWeight = development_mx.Out_SalesReportItem.Package_weight,
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCost  = development_mx.Out_SalesReportItem.Shipping_Cost
;

/*
*  Inventory
*/
DROP TEMPORARY TABLE IF EXISTS development_mx.TMP_out_order_tracking;
CREATE TEMPORARY TABLE development_mx.TMP_out_order_tracking ( KEY ( item_id ) )
SELECT
    CAST(  item_id AS DECIMAL ) AS item_id
FROM
   operations_mx.out_order_tracking
WHERE
   fulfillment_type_real = "dropshipping"
;

UPDATE        development_mx.TMP_out_order_tracking
   INNER JOIN development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@
           ON item_id = ItemId
SET
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCost = 0
WHERE MonthNum >= date_format( now() - INTERVAL 3 MONTH , "%Y%m" ) 
;
