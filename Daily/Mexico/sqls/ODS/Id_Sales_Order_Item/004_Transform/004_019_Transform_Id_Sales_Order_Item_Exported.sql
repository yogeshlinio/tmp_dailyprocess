DROP TEMPORARY TABLE IF EXISTS TMP_Exported;
CREATE TEMPORARY TABLE TMP_Exported (  index( ItemID  ) )
SELECT
   fk_sales_order_item AS ItemId,
   max( created_at ) AS DateExported
FROM  bob_live_mx.sales_order_item_status_history
WHERE
	#exported, exported_electronically
   fk_sales_order_item_status in ( 4,97 )
GROUP BY  
fk_sales_order_item;

UPDATE        Id_Sales_Order_Item_Sample_@v_countryPrefix@  
   INNER JOIN TMP_Exported 
        USING ( ItemId )
SET
   Id_Sales_Order_Item_Sample_@v_countryPrefix@.Exported     = 1,
   Id_Sales_Order_Item_Sample_@v_countryPrefix@.DateExported = TMP_Exported.DateExported
;