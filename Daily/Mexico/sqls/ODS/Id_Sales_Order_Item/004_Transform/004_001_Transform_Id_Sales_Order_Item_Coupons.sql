/*
*  MX_001_002_Coupons_EV_VISA
*/
UPDATE  Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET
    CouponValue         = 0,
    CouponValueAfterTax = 0
WHERE
    PrefixCode IN ("CCE",
                   "PRCcre",
                   "OPScre",
                   "DEP",
                   "VISA",
                   "VISA3"
                  );

/*
*  MX_001_003_Adjust_Wrong_Coupons_by_CC
*  monitoring
*/
UPDATE            Id_Sales_Order_Item_Sample_@v_countryPrefix@ AS Id_Sales_Order_Item_Sample
       INNER JOIN Id_Sales_Order_Item_Key_Map
	        USING ( Country , Id_Sales_Order_Item )
       INNER JOIN @development@.COM_Adjust_Wrong_Vouchers_Used_by_CC 
               ON     ( Id_Sales_Order_Item_Sample.Order_Num  = COM_Adjust_Wrong_Vouchers_Used_by_CC.Order_Nr   ) 
                  AND ( Id_Sales_Order_Item_Sample.SKU_Simple = COM_Adjust_Wrong_Vouchers_Used_by_CC.SKU_Simple ) 
SET 
    Id_Sales_Order_Item_Sample.Coupon_Value           = COM_Adjust_Wrong_Vouchers_Used_by_CC.Coupon_money_value, 
    Id_Sales_Order_Item_Sample.Coupon_Value_After_Tax = COM_Adjust_Wrong_Vouchers_Used_by_CC.After_vat_coupon;

/*
*  MX_001_004_Non_MKT_Voucher_CCemp
*/
UPDATE  Id_Sales_Order_Item_Sample_@v_countryPrefix@ AS Id_Sales_Order_Item_Sample
SET
    Id_Sales_Order_Item_Sample.CouponValue             = 0,
    Id_Sales_Order_Item_Sample.CouponValueAfterTax     = 0
WHERE
    Id_Sales_Order_Item_Sample.PrefixCode = "CCemp"
;

/*
*  MX_001_005_Adjust_Nomina_Sales
*/
call production.monitoring_step( "ODS.Sku_simple" , "@v_country@" , "catalog_config" , 8000  );
UPDATE            Id_Sales_Order_Item_Sample_@v_countryPrefix@ AS Id_Sales_Order_Item_Sample
       INNER JOIN Id_Sales_Order_Item_Key_Map
	        USING ( Country , Id_Sales_Order_Item )
       INNER JOIN Sku_Simple_Sample_@v_countryPrefix@ AS Sku_Simple
	        USING ( Country , Sku_Simple )
	   INNER JOIN COM_Adjust_Price_Coupon_for_Nomina_Sales 
               ON     (  Id_Sales_Order_Item_Sample.Coupon_Code = COM_Adjust_Price_Coupon_for_Nomina_Sales.Voucher_Code) 
                  AND (  Sku_Simple.SKU_Config                  = COM_Adjust_Price_Coupon_for_Nomina_Sales.SKU_Config) 
SET 
     Id_Sales_Order_Item_Sample.Coupon_Value             = COM_Adjust_Price_Coupon_for_Nomina_Sales.Coupon_Money_Value, 
     Id_Sales_Order_Item_Sample.Coupon_Value_After_Tax   = COM_Adjust_Price_Coupon_for_Nomina_Sales.After_VAT_Coupon;

/*
*   MX_001_006_Paypal_Vouchers
*/
UPDATE  Id_Sales_Order_Item_Sample_@v_countryPrefix@ AS Id_Sales_Order_Item_Sample
 SET 
     Id_Sales_Order_Item_Sample.Coupon_Value           = 0, 
     Id_Sales_Order_Item_Sample.Coupon_Value_After_Tax = 0

WHERE 
      Coupon_Code in ( "COM1Tw3Bz" ,
                      "COM1QuGJR" ,  "COMq517VW" ,
                      "COMc4jiTb"  ,  "COM57OMUy" )
  Or  Coupon_Code like "%paypal%"
;

/*
*   MX_001_009_CAC_Campaign_Enero
*/
 #Query: 1_M1_CouponMoneyValue+ShippingCost
UPDATE  Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET 
     Coupon_Value =  Coupon_Value + Shipping_Cost
WHERE 
     Coupon_code in ( "MKT0xfgVK" , "MKT1eDvI7");

/*
*  MX_001_010
*/
 #Query: M1_U_voucherMKT
UPDATE            Id_Sales_Order_Item_Sample_@v_countryPrefix@ AS Id_Sales_Order_Item_Sample
       INNER JOIN Id_Sales_Order_Item_Key_Map
	        USING ( Country , Id_Sales_Order_Item )
       INNER JOIN @development@.A_E_M1_VoucherMKT 
               ON Id_Sales_Order_Item_Key_Map.Coupon_Code = A_E_M1_VoucherMKT.Voucher
SET 
    Id_Sales_Order_Item_Sample.PrefixCode = "MKT";

/*
* DATE           2013/11/28
* DEVELOPED BY   Carlos Antonio Mondragón Soria
* DESCRIPTION MKT - CAC COUPON CODE FIX 
*/
UPDATE            Id_Sales_Order_Item_Sample_@v_countryPrefix@ AS Id_Sales_Order_Item_Sample
       INNER JOIN Id_Sales_Order_Item_Key_Map
	        USING ( Country , Id_Sales_Order_Item )
       INNER JOIN test_linio.M_MKT_Vouchers
               ON  Id_Sales_Order_Item_Key_Map.Coupon_Code = M_MKT_Vouchers.CouponCode
SET
   Prefix_Code = test_linio.M_MKT_Vouchers.Campaign
;

