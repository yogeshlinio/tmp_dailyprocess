/*
*  MX_018_001	DateDelivered
*/
UPDATE 
               Id_Sales_Order_Item_Sample_@v_countryPrefix@  
    INNER JOIN operations_mx.out_order_tracking b
            ON Id_Sales_Order_Item_Sample_@v_countryPrefix@.ItemID = b.item_id
SET
   Id_Sales_Order_Item_Sample_@v_countryPrefix@.DateDelivered =  b.date_delivered,
   Id_Sales_Order_Item_Sample_@v_countryPrefix@.Delivered =  1
   
WHERE 
   b.date_delivered is not null
;

/*
* MX_018_002 Delivered
*/
DROP   TEMPORARY TABLE IF EXISTS A_Delivered;
CREATE TEMPORARY TABLE A_Delivered ( INDEX ( fk_sales_order_item  ) )
SELECT
 *
FROM
    bob_live_mx.sales_order_item_status_history
WHERE
	#delivered, delivered_electronically
   bob_live_mx.sales_order_item_status_history.fk_sales_order_item_status IN ( 52 , 98 )
GROUP BY fk_sales_order_item
;

UPDATE           A_Delivered
      INNER JOIN Id_Sales_Order_Item_Sample_@v_countryPrefix@
              ON A_Delivered.fk_sales_order_item = Id_Sales_Order_Item_Sample_@v_countryPrefix@.ItemID 
SET
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.DateDelivered = A_Delivered.created_at,
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.Delivered = 1;

UPDATE Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.NetDelivered = 1
WHERE
	#closed, delivered, delivered_electronically
    Status in ( select name from bob_live_mx.sales_order_item_status
                where id_sales_order_item_status IN ( 6 ,52, 98 ) )
;

UPDATE Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.DeliveredReturn = 1
WHERE
        Delivered     = 1
    AND NetDelivered  = 0;
