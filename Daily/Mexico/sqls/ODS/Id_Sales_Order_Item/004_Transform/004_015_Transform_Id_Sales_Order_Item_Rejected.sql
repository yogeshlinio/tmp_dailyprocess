DROP   TEMPORARY TABLE IF EXISTS A_Rejections_Shipped;
CREATE TEMPORARY TABLE A_Rejections_Shipped ( INDEX ( ItemID ) )
SELECT
   fk_sales_order_item as ItemID
FROM
   bob_live_mx.sales_order_item_status_history 
WHERE 
	#Shipped
   fk_sales_order_item_status  = 5
GROUP BY ItemID
;

DROP   TEMPORARY TABLE IF EXISTS A_Rejections_Delivered;
CREATE TEMPORARY TABLE A_Rejections_Delivered ( INDEX ( ItemID ) )
SELECT
   fk_sales_order_item as ItemID
FROM
   bob_live_mx.sales_order_item_status_history 
WHERE 
	#delivered, delivered_electronically
   fk_sales_order_item_status  in( 52 , 98 )
GROUP BY ItemID
;

DROP   TEMPORARY TABLE IF EXISTS A_Rejections_Shipped_NoDelivered;
CREATE TEMPORARY TABLE A_Rejections_Shipped_NoDelivered ( INDEX ( ItemID ) )
SELECT
   ItemID
FROM
   A_Rejections_Shipped 
WHERE 
   ItemId  not IN ( SELECT * FROM A_Rejections_Delivered )
;


UPDATE            Id_Sales_Order_Item_Sample_@v_countryPrefix@ 
       INNER JOIN A_Rejections_Shipped_NoDelivered
            USING ( ItemID )
SET
   Id_Sales_Order_Item_Sample_@v_countryPrefix@.Rejected = 1

WHERE
#       Out_SalesReportItem.Cancellations = 1
#   AND 
       Id_Sales_Order_Item_Sample_@v_countryPrefix@.Status in (
                                       "canceled" ,
                                       "cancelled" ,
                                       "refund_needed" ,
                                       "store_credit_issued" ,
                                       "clarify_refund_not_processed",
                                       "store_credit_needed",
                                       "store_credit_issued",
                                       "clarify_store_credit_not_issued" ,
                                       "refunded"
                                     )

;
