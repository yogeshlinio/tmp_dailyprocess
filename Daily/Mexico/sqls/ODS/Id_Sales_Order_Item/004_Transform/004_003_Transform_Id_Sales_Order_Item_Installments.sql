 #Query: A 103 U Payment Type/Installment
UPDATE            Id_Sales_Order_Item_Sample_@v_countryPrefix@ AS Id_Sales_Order_Item_Sample
       INNER JOIN Id_Sales_Order_Item_Key_Map
	        USING ( Country , Id_Sales_Order_Item )
       INNER JOIN @bob_live@.payment_sales_amex 
               ON Id_Sales_Order_Item_Key_Map.idSalesOrder = bob_live_mx.payment_sales_amex.fk_sales_order
SET
   #Out_SalesReportItem.Payment_Type =  bob_live_mx.payment_sales_amex.payment_type,
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.Installment =   bob_live_mx.payment_sales_amex.installment_months
;


UPDATE              development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@ 
         INNER JOIN bob_live_mx.payment_sales_banortepayworks 
                 ON development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.idSalesOrder = bob_live_mx.payment_sales_banortepayworks.fk_sales_order 
SET 
   #Out_SalesReportItem.Payment_Type = bob_live_mx.payment_sales_banorte.payment_type,
   development_mx.Id_Sales_Order_Item_Sample_@v_countryPrefix@.Installment  = bob_live_mx.payment_sales_banortepayworks.installment_months;

