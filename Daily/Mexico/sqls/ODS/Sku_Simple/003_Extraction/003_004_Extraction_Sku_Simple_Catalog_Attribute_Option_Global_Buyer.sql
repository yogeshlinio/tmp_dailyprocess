DROP TEMPORARY TABLE IF EXISTS Data_@v_countryPrefix@;
CREATE TEMPORARY TABLE Data_@v_countryPrefix@ ( PRIMARY KEY ( Sku_Simple ) )
SELECT
   Key_Map.sku_simple,
   catalog_attribute_option_global_buyer_name.name
FROM   
                  Sku_Simple_Key_Map_Sample_@v_countryPrefix@ AS Key_Map
       INNER JOIN @bob_live@.catalog_attribute_option_global_buyer_name
	           ON id_catalog_attribute_option_global_buyer = id_catalog_attribute_option_global_buyer_name
;
			   
UPDATE              Sku_Simple_Sample_@v_countryPrefix@   AS Sku_Simple_Sample
         INNER JOIN Data_@v_countryPrefix@                AS Data
  	          USING ( sku_simple )
SET
    Sku_Simple_Sample.Buyer      = Data.name;
;


DROP TEMPORARY TABLE IF EXISTS Data_@v_countryPrefix@;
CREATE TEMPORARY TABLE Data_@v_countryPrefix@ ( PRIMARY KEY ( Sku_Simple ) )
SELECT
   Key_Map.sku_simple,
   catalog_attribute_option_global_head_buyer_name.name           
FROM   
                  Sku_Simple_Key_Map_Sample_@v_countryPrefix@ AS Key_Map
       INNER JOIN @bob_live@.catalog_attribute_option_global_head_buyer_name
               ON id_catalog_attribute_option_global_head_buyer= id_catalog_attribute_option_global_head_buyer_name
;
			   
UPDATE              Sku_Simple_Sample_@v_countryPrefix@   AS Sku_Simple_Sample
         INNER JOIN Data_@v_countryPrefix@                AS Data
  	          USING ( sku_simple )
SET
    Sku_Simple_Sample.Head_Buyer      = Data.name           
;