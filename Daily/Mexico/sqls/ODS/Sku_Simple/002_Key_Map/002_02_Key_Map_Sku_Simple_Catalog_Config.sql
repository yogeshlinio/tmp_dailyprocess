UPDATE            Sku_Simple_Key_Map_Sample_@v_countryPrefix@ AS Sku_Simple_Key_Map_Sample
       INNER JOIN @bob_live@.catalog_config
	        USING ( id_catalog_config )
SET
  Sku_Simple_Key_Map_Sample.id_catalog_brand                                    = catalog_config.fk_catalog_brand,
  Sku_Simple_Key_Map_Sample.id_catalog_attribute_option_global_buyer            = catalog_config.fk_catalog_attribute_option_global_buyer_name,
  Sku_Simple_Key_Map_Sample.id_catalog_attribute_option_global_head_buyer       = catalog_config.fk_catalog_attribute_option_global_head_buyer_name,
  Sku_Simple_Key_Map_Sample.id_catalog_attribute_option_global_category         = catalog_config.fk_catalog_attribute_option_global_category,
  Sku_Simple_Key_Map_Sample.id_catalog_attribute_option_global_sub_category     = catalog_config.fk_catalog_attribute_option_global_sub_category,
  Sku_Simple_Key_Map_Sample.id_catalog_attribute_option_global_sub_sub_category = catalog_config.fk_catalog_attribute_option_global_sub_sub_category,
  Sku_Simple_Key_Map_Sample.id_catalog_attribute_set                            = catalog_config.fk_catalog_attribute_set

  

;