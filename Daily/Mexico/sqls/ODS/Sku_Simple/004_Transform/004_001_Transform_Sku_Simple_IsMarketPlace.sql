DROP TABLE IF EXISTS TMP_MPCat;
CREATE TABLE TMP_MPCat ( INDEX ( SKU_Simple ) )
SELECT
   ODS.SKU_Simple,
   A_E_BI_Marketplace_Commission.Since,
   A_E_BI_Marketplace_Commission.Until,
   A_E_BI_Marketplace_Commission.Fee
FROM
               Sku_Simple_Sample_@v_countryPrefix@ AS ODS
    INNER JOIN Sku_Simple_Key_Map AS KeyMap
	     USING ( country , sku_simple )    
	INNER JOIN development_mx.A_E_BI_Marketplace_Commission 
            ON KeyMap.id_Supplier = A_E_BI_Marketplace_Commission.id_supplier
WHERE
    A_E_BI_Marketplace_Commission.Category = "" 
AND A_E_BI_Marketplace_Commission.SubCategory = ""
AND A_E_BI_Marketplace_Commission.SubSubCategory = ""
AND A_E_BI_Marketplace_Commission.SubSubSubCategory = ""
AND A_E_BI_Marketplace_Commission.perSKU = ""
GROUP BY SKU_Simple
;

REPLACE TMP_MPCat 
SELECT
   ODS.SKU_Simple,
   A_E_BI_Marketplace_Commission.Since,
   A_E_BI_Marketplace_Commission.Until,
   A_E_BI_Marketplace_Commission.Fee
FROM
               Sku_Simple_Sample_@v_countryPrefix@ AS ODS
    INNER JOIN Sku_Simple_Key_Map AS KeyMap
	     USING ( country , sku_simple )    
	INNER JOIN development_mx.A_E_BI_Marketplace_Commission 
            ON KeyMap.id_Supplier = A_E_BI_Marketplace_Commission.id_supplier
WHERE
    IF(   A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( curdate() <= A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND A_E_BI_Marketplace_Commission.Category = ODS.Cat_BP
AND A_E_BI_Marketplace_Commission.SubCategory = ""
AND A_E_BI_Marketplace_Commission.SubSubCategory = ""
AND A_E_BI_Marketplace_Commission.SubSubSubCategory = ""
AND A_E_BI_Marketplace_Commission.perSKU = ""
;

REPLACE TMP_MPCat 
SELECT
   ODS.SKU_Simple,
   A_E_BI_Marketplace_Commission.Since,
   A_E_BI_Marketplace_Commission.Until,
   A_E_BI_Marketplace_Commission.Fee
FROM
               Sku_Simple_Sample_@v_countryPrefix@ AS ODS
    INNER JOIN Sku_Simple_Key_Map AS KeyMap
	     USING ( country , sku_simple )    
	INNER JOIN development_mx.A_E_BI_Marketplace_Commission 
            ON KeyMap.id_Supplier = A_E_BI_Marketplace_Commission.id_supplier
WHERE
    IF(   A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( curdate() <= A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND A_E_BI_Marketplace_Commission.Category = ODS.Cat_BP
AND (    ODS.Cat1 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR ODS.Cat2 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
OR ODS.Cat3 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
     )
AND A_E_BI_Marketplace_Commission.SubCategory != ""
AND A_E_BI_Marketplace_Commission.perSKU = ""
;

REPLACE TMP_MPCat 
SELECT
   ODS.SKU_Simple,
   A_E_BI_Marketplace_Commission.Since,
   A_E_BI_Marketplace_Commission.Until,
   A_E_BI_Marketplace_Commission.Fee
FROM
               Sku_Simple_Sample_@v_countryPrefix@ AS ODS
    INNER JOIN Sku_Simple_Key_Map AS KeyMap
	     USING ( country , sku_simple )    
	INNER JOIN development_mx.A_E_BI_Marketplace_Commission 
            ON KeyMap.id_Supplier = A_E_BI_Marketplace_Commission.id_supplier
WHERE
    IF(   A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( curdate() <= A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND A_E_BI_Marketplace_Commission.Category = ODS.Cat_BP
AND (    ODS.Cat1 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR ODS.Cat2 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
OR ODS.Cat3 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
     )
AND (    ODS.Cat1 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
      OR ODS.Cat2 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
OR ODS.Cat3 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
     )
AND A_E_BI_Marketplace_Commission.SubSubCategory != ""
AND A_E_BI_Marketplace_Commission.perSKU = ""
;

REPLACE TMP_MPCat 
SELECT
   ODS.SKU_Simple,
   A_E_BI_Marketplace_Commission.Since,
   A_E_BI_Marketplace_Commission.Until,
   A_E_BI_Marketplace_Commission.Fee
FROM
               Sku_Simple_Sample_@v_countryPrefix@ AS ODS
    INNER JOIN Sku_Simple_Key_Map AS KeyMap
	     USING ( country , sku_simple )    
	INNER JOIN development_mx.A_E_BI_Marketplace_Commission 
            ON KeyMap.id_Supplier = A_E_BI_Marketplace_Commission.id_supplier
WHERE
    IF(   A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( curdate() <= A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND A_E_BI_Marketplace_Commission.Category = ODS.Cat_BP
AND (    ODS.Cat1 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR ODS.Cat2 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
OR ODS.Cat3 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
     )
AND (    ODS.Cat1 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
      OR ODS.Cat2 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
OR ODS.Cat3 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
     )
AND (    ODS.Cat1 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubSubSubCategory, "%" ) 
      OR ODS.Cat2 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubSubSubCategory, "%" ) 
OR ODS.Cat3 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubSubSubCategory, "%" ) 
     )
AND A_E_BI_Marketplace_Commission.SubSubSubCategory != ""
AND A_E_BI_Marketplace_Commission.perSKU = ""
;

/*
*  Per SKU
*/
REPLACE TMP_MPCat 
SELECT
   ODS.SKU_Simple,
   A_E_BI_Marketplace_Commission.Since,
   A_E_BI_Marketplace_Commission.Until,
   A_E_BI_Marketplace_Commission.Fee
FROM
               Sku_Simple_Sample_@v_countryPrefix@ AS ODS
    INNER JOIN Sku_Simple_Key_Map AS KeyMap
	     USING ( country , sku_simple )    
	INNER JOIN development_mx.A_E_BI_Marketplace_Commission 
            ON KeyMap.id_Supplier = A_E_BI_Marketplace_Commission.id_supplier
WHERE
    IF(   A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( curdate() <= A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND A_E_BI_Marketplace_Commission.Category = ""
AND A_E_BI_Marketplace_Commission.SubCategory = ""
AND A_E_BI_Marketplace_Commission.SubSubCategory = ""
AND A_E_BI_Marketplace_Commission.SubSubSubCategory = ""
AND A_E_BI_Marketplace_Commission.perSKU != ""
AND ODS.SKU_Name like concat( "%" , replace( perSKU , " " , "%" ) , "%" )
;

REPLACE TMP_MPCat 
SELECT
   ODS.SKU_Simple,
   A_E_BI_Marketplace_Commission.Since,
   A_E_BI_Marketplace_Commission.Until,
   A_E_BI_Marketplace_Commission.Fee
FROM
               Sku_Simple_Sample_@v_countryPrefix@ AS ODS
    INNER JOIN Sku_Simple_Key_Map AS KeyMap
	     USING ( country , sku_simple )    
	INNER JOIN development_mx.A_E_BI_Marketplace_Commission 
            ON KeyMap.id_Supplier = A_E_BI_Marketplace_Commission.id_supplier
WHERE
    IF(   A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( curdate() <= A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND A_E_BI_Marketplace_Commission.Category = ODS.Cat_BP
AND A_E_BI_Marketplace_Commission.SubCategory = ""
AND A_E_BI_Marketplace_Commission.SubSubCategory = ""
AND A_E_BI_Marketplace_Commission.SubSubSubCategory = ""
AND A_E_BI_Marketplace_Commission.perSKU != ""
AND ODS.SKU_Name like concat( "%" , replace( perSKU , " " , "%" ) , "%" )
;

REPLACE TMP_MPCat 
SELECT
   ODS.SKU_Simple,
   A_E_BI_Marketplace_Commission.Since,
   A_E_BI_Marketplace_Commission.Until,
   A_E_BI_Marketplace_Commission.Fee
FROM
               Sku_Simple_Sample_@v_countryPrefix@ AS ODS
    INNER JOIN Sku_Simple_Key_Map AS KeyMap
	     USING ( country , sku_simple )    
	INNER JOIN development_mx.A_E_BI_Marketplace_Commission 
            ON KeyMap.id_Supplier = A_E_BI_Marketplace_Commission.id_supplier
WHERE
    IF(   A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( curdate() <= A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND A_E_BI_Marketplace_Commission.Category = ODS.Cat_BP
AND (    ODS.Cat1 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR ODS.Cat2 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
OR ODS.Cat3 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
     )
AND A_E_BI_Marketplace_Commission.SubCategory != ""
AND A_E_BI_Marketplace_Commission.perSKU != ""
AND ODS.SKU_Name like concat( "%" , replace( perSKU , " " , "%" ) , "%" )
;

REPLACE TMP_MPCat 
SELECT
   ODS.SKU_Simple,
   A_E_BI_Marketplace_Commission.Since,
   A_E_BI_Marketplace_Commission.Until,
   A_E_BI_Marketplace_Commission.Fee
FROM
               Sku_Simple_Sample_@v_countryPrefix@ AS ODS
    INNER JOIN Sku_Simple_Key_Map AS KeyMap
	     USING ( country , sku_simple )    
	INNER JOIN development_mx.A_E_BI_Marketplace_Commission 
            ON KeyMap.id_Supplier = A_E_BI_Marketplace_Commission.id_supplier
WHERE
    IF(   A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( curdate() <= A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND A_E_BI_Marketplace_Commission.Category = ODS.Cat_BP
AND (    ODS.Cat1 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR ODS.Cat2 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
OR ODS.Cat3 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
     )
AND (    ODS.Cat1 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
      OR ODS.Cat2 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
OR ODS.Cat3 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
     )
AND A_E_BI_Marketplace_Commission.SubSubCategory != ""
AND A_E_BI_Marketplace_Commission.perSKU != ""
AND ODS.SKU_Name like concat( "%" , replace( perSKU , " " , "%" ) , "%" )
;

REPLACE TMP_MPCat 
SELECT
   ODS.SKU_Simple,
   A_E_BI_Marketplace_Commission.Since,
   A_E_BI_Marketplace_Commission.Until,
   A_E_BI_Marketplace_Commission.Fee
FROM
               Sku_Simple_Sample_@v_countryPrefix@ AS ODS
    INNER JOIN Sku_Simple_Key_Map AS KeyMap
	     USING ( country , sku_simple )    
	INNER JOIN development_mx.A_E_BI_Marketplace_Commission 
            ON KeyMap.id_Supplier = A_E_BI_Marketplace_Commission.id_supplier
WHERE
    IF(   A_E_BI_Marketplace_Commission.Until IS NULL, 1,
       IF( curdate() <= A_E_BI_Marketplace_Commission.Until,1,0)    
    )
AND A_E_BI_Marketplace_Commission.Category = ODS.Cat_BP
AND (    ODS.Cat1 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
      OR ODS.Cat2 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
OR ODS.Cat3 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubCategory, "%" ) 
     )
AND (    ODS.Cat1 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
      OR ODS.Cat2 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
OR ODS.Cat3 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubSubCategory, "%" ) 
     )
AND (    ODS.Cat1 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubSubSubCategory, "%" ) 
      OR ODS.Cat2 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubSubSubCategory, "%" ) 
OR ODS.Cat3 like CONCAT( "%",A_E_BI_Marketplace_Commission.SubSubSubCategory, "%" ) 
     )
AND A_E_BI_Marketplace_Commission.SubSubSubCategory != ""
AND A_E_BI_Marketplace_Commission.perSKU != ""
AND ODS.SKU_Name like concat( "%" , replace( perSKU , " " , "%" ) , "%" )
;


UPDATE            TMP_MPCat 
       INNER JOIN Sku_Simple_Sample_@v_countryPrefix@
            USING ( SKU_Simple )
SET
   isMarketPlace        = 1,
   isMarketPlace_Since  = TMP_MPCat.Since,
   isMarketPlace_Until  = TMP_MPCat.Until
;