DROP TEMPORARY TABLE IF EXISTS TMP_Variations;
CREATE TEMPORARY TABLE TMP_Variations ( PRIMARY KEY ( id_catalog_simple ) )
select 
        `@bob_live@`.`catalog_simple_electronics`.`fk_catalog_simple` AS `id_catalog_simple`,
        `@bob_live@`.`catalog_simple_electronics`.`variation` AS `variation`
         from
        `@bob_live@`.`catalog_simple_electronics`
         where
        ((`@bob_live@`.`catalog_simple_electronics`.`variation` is not null)
            and (`@bob_live@`.`catalog_simple_electronics`.`variation` <> '...')
            and (`@bob_live@`.`catalog_simple_electronics`.`variation` <> '…')
            and (`@bob_live@`.`catalog_simple_electronics`.`variation` <> '?')
            and (`@bob_live@`.`catalog_simple_electronics`.`variation` <> ',,,')) 
union 
select 
        `@bob_live@`.`catalog_simple_toys_baby`.`fk_catalog_simple` AS `fk_catalog_simple`,
        `@bob_live@`.`catalog_simple_toys_baby`.`variation` AS `variation`
    from
        `@bob_live@`.`catalog_simple_toys_baby`
    where
        ((`@bob_live@`.`catalog_simple_toys_baby`.`variation` is not null)
            and (`@bob_live@`.`catalog_simple_toys_baby`.`variation` <> '...')
            and (`@bob_live@`.`catalog_simple_toys_baby`.`variation` <> '…')
            and (`@bob_live@`.`catalog_simple_toys_baby`.`variation` <> '?')
            and (`@bob_live@`.`catalog_simple_toys_baby`.`variation` <> ',,,')) 
union 
select 
        `@bob_live@`.`catalog_simple_health_beauty`.`fk_catalog_simple` AS `fk_catalog_simple`,
        `@bob_live@`.`catalog_simple_health_beauty`.`variation` AS `variation`
    from
        `@bob_live@`.`catalog_simple_health_beauty`
    where
        ((`@bob_live@`.`catalog_simple_health_beauty`.`variation` is not null)
            and (`@bob_live@`.`catalog_simple_health_beauty`.`variation` <> '...')
            and (`@bob_live@`.`catalog_simple_health_beauty`.`variation` <> '…')
            and (`@bob_live@`.`catalog_simple_health_beauty`.`variation` <> '?')
            and (`@bob_live@`.`catalog_simple_health_beauty`.`variation` <> ',,,'))
union 
select 
        `@bob_live@`.`catalog_simple_home_living`.`fk_catalog_simple` AS `fk_catalog_simple`,
        `@bob_live@`.`catalog_simple_home_living`.`variation` AS `variation`
    from
        `@bob_live@`.`catalog_simple_home_living`
    where
        ((`@bob_live@`.`catalog_simple_home_living`.`variation` is not null)
            and (`@bob_live@`.`catalog_simple_home_living`.`variation` <> '…')
            and (`@bob_live@`.`catalog_simple_home_living`.`variation` <> '?')
            and (`@bob_live@`.`catalog_simple_home_living`.`variation` <> ',,,')) 
union 
select 
        `@bob_live@`.`catalog_simple_media`.`fk_catalog_simple` AS `fk_catalog_simple`,
        `@bob_live@`.`catalog_simple_media`.`variation` AS `variation`
    from
        `@bob_live@`.`catalog_simple_media`
    where
        ((`@bob_live@`.`catalog_simple_media`.`variation` is not null)
            and (`@bob_live@`.`catalog_simple_media`.`variation` <> '...')
            and (`@bob_live@`.`catalog_simple_media`.`variation` <> '…')
            and (`@bob_live@`.`catalog_simple_media`.`variation` <> '?')
            and (`@bob_live@`.`catalog_simple_media`.`variation` <> ',,,')) 
union 
select 
        `@bob_live@`.`catalog_simple_other`.`fk_catalog_simple` AS `fk_catalog_simple`,
        `@bob_live@`.`catalog_simple_other`.`variation` AS `variation`
    from
        `@bob_live@`.`catalog_simple_other`
    where
        ((`@bob_live@`.`catalog_simple_other`.`variation` is not null)
            and (`@bob_live@`.`catalog_simple_other`.`variation` <> '...')
            and (`@bob_live@`.`catalog_simple_other`.`variation` <> '…')
            and (`@bob_live@`.`catalog_simple_other`.`variation` <> '?')
            and (`@bob_live@`.`catalog_simple_other`.`variation` <> ',,,')) 
union 
select 
        `@bob_live@`.`catalog_simple_sports`.`fk_catalog_simple` AS `fk_catalog_simple`,
        `@bob_live@`.`catalog_simple_sports`.`variation` AS `variation`
    from
        `@bob_live@`.`catalog_simple_sports`
    where
        ((`@bob_live@`.`catalog_simple_sports`.`variation` is not null)
            and (`@bob_live@`.`catalog_simple_sports`.`variation` <> '...')
            and (`@bob_live@`.`catalog_simple_sports`.`variation` <> '…')
            and (`@bob_live@`.`catalog_simple_sports`.`variation` <> '?')
            and (`@bob_live@`.`catalog_simple_sports`.`variation` <> ',,,')) 
union 
select 
        `@bob_live@`.`catalog_simple_fashion`.`fk_catalog_simple` AS `fk_catalog_simple`,
        `@bob_live@`.`catalog_attribute_option_fashion_size`.`name` AS `name`
    from
        (`@bob_live@`.`catalog_simple_fashion`
        join `@bob_live@`.`catalog_attribute_option_fashion_size`)
    where
        ((`@bob_live@`.`catalog_simple_fashion`.`fk_catalog_attribute_option_fashion_size` = `@bob_live@`.`catalog_attribute_option_fashion_size`.`id_catalog_attribute_option_fashion_size`)
            and (`@bob_live@`.`catalog_attribute_option_fashion_size`.`name` is not null))
;

UPDATE            Sku_Simple_Sample_@v_countryPrefix@ AS Sku_Simple_Sample
       INNER JOIN Sku_Simple_Key_Map
	        USING ( country , sku_simple )
       INNER JOIN TMP_Variations
	        USING ( id_catalog_simple )
SET
   Sku_Simple_Sample.variation = TMP_Variations.variation

;