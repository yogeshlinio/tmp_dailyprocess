/*UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
INNER JOIN @procurement_live@.catalog_supplier_attributes AS b 
	ON a.supplier_id = b.fk_catalog_supplier
SET 
 a.catalog_payment_terms = b.payment_terms,
 a.catalog_payment_type = b.payment_type,
 a.procurement_analyst = b.buyer_name,
 a.credit_limit = b.credit_limit,
 a.pick_at_region = b.zone;

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
INNER JOIN @procurement_live@.catalog_supplier_attributes AS b 
	ON a.supplier_id = b.fk_catalog_supplier
SET 
 a.pick_at_zip = b.post_code,
 a.pick_at_zip2 = MID(b.post_code, 3, 2),
 a.pick_at_zip3 = RIGHT (b.post_code, 3);*/
 
 
UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
JOIN @procurement_live@.catalog_supplier_attributes b 
ON a.supplier_id = b.fk_catalog_supplier
SET 
 #a.supplier_name = b.name,
 #a.supplier_tax_id = b.nit,
 #a.catalog_payment_type = b.payment_type,
 a.catalog_payment_terms = b.payment_terms,
 #a.procurement_analyst = b.buyer_name,
 a.pick_at_zip = b.post_code,
 a.pick_at_zip2 = mid(b.post_code, 3, 2),
 a.pick_at_zip3 = RIGHT (b.post_code, 3),
 #a.pick_at_city = b.,
 a.pick_at_region = b.supplier_zone;
 #a.credit_limit = b.credit_limit;

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
JOIN @procurement_live@.catalog_supplier b 
ON a.supplier_id = b.id_catalog_supplier
SET 
a.supplier_name = b.name;

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
JOIN @procurement_live@.catalog_supplier_attributes
ON catalog_supplier_attributes.fk_catalog_supplier = a.supplier_id
JOIN @procurement_live@.fos_user
ON catalog_supplier_attributes.fk_user = fos_user.id
SET 
 a.procurement_analyst = fos_user.username;

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
JOIN @procurement_live@.catalog_supplier_attributes
ON catalog_supplier_attributes.fk_catalog_supplier = a.supplier_id
JOIN @procurement_live@.catalog_payment_type
ON catalog_supplier_attributes.fk_payment_type = catalog_payment_type.id_payment_type
SET 
a.catalog_payment_type = catalog_payment_type.payment_type;