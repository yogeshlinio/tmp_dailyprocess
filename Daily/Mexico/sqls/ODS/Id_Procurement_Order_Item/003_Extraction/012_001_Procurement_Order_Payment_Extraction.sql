UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
INNER JOIN @procurement_live@.procurement_order_payment_items AS b 
	ON a.id_procurement_order_item = b.fk_procurement_order_item
INNER JOIN @procurement_live@.procurement_order_payment AS c 
	ON b.fk_procurement_order_payment = c.id_procurement_order_payment
SET 
 a.procurement_payment_type = c.payment_type,
 a.date_paid = DATE(c.operation_date); 