UPDATE  Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
    INNER JOIN Id_Procurement_Order_Item_Key_Map_Sample_@v_countryPrefix@ AS b
	    USING ( country , Id_Procurement_Order_Item )
    INNER JOIN @procurement_live@.procurement_order AS c
	    ON b.Id_Procurement_Order=c.Id_Procurement_Order
SET
   /*Defaults and Simple Extraction*/
	a.purchase_order = CONCAT(c.venture_code,LPAD(c.id_procurement_order, 7, 0),c.check_digit),
	a.fk_procurement_order_type = c.fk_procurement_order_type,
	a.procurement_payment_status = c.payment_status,
	a.procurement_payment_terms = c.procurement_payment_terms,
	a.procurement_payment_event = c.procurement_payment_event,
	a.is_cancelled = c.is_cancelled,
	a.date_po_created = DATE(c.created_at),
	a.date_po_updated = DATE(c.updated_at),
	a.date_po_issued = DATE(c.sent_at),
	a.in_transit = IF( c.is_cancelled = 0 and a.is_receipt = 0 and a.is_deleted = 0 ,1 , 0  )
	;


   /*
*  Insert Monitoring LOG
*/
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date)
SELECT 
  '@v_country@', 
  'ODS.Id_Procurement_Order_Item',
  "In_transit",
  NOW(),
  NOW() 
;		