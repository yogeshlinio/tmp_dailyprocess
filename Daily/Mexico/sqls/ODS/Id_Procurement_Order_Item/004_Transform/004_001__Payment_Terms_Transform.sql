UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
SET 
payment_terms = 			CASE
								WHEN date_paid IS NULL 
									THEN NULL
									ELSE
									(CASE
										WHEN date_goods_received IS NULL 
											THEN DATEDIFF(date_paid, curdate())
											ELSE DATEDIFF(date_paid, date_goods_received)
									END)
								END; 
UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
SET payment_terms = payment_terms_scheduled
WHERE
	payment_terms IS NULL ; 

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
SET payment_terms = payment_terms_scheduled
WHERE
	payment_terms IS NULL ; 

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
SET payment_terms = payment_terms_expected
WHERE
	payment_terms IS NULL ;

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
SET 
 paid_last_15 = IF(DATEDIFF(curdate(),date_paid) <= 15,1,0),
 paid_last_30 = IF(DATEDIFF(curdate(),date_paid) <= 30,1,0),
 paid_last_45 = IF(DATEDIFF(curdate(),date_paid) <= 45,1,0);


UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
SET 
payment_terms_scheduled = 	CASE
								WHEN date_payment_scheduled IS NULL 
									THEN NULL
									ELSE 
									(CASE
										WHEN date_goods_received IS NULL 
											THEN DATEDIFF(date_payment_scheduled, curdate())
											ELSE DATEDIFF(date_payment_scheduled,	date_goods_received)
									END)
								END;

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
SET 
payment_terms_expected = 	CASE
								WHEN date_goods_received IS NULL 
									THEN DATEDIFF(date_payment_estimation,curdate())
									ELSE 
									(CASE
										WHEN date_payment_promised IS NULL 
											THEN DATEDIFF(date_payment_estimation,date_goods_received)
											ELSE DATEDIFF(date_payment_promised,date_goods_received)
									END)
								END,
payment_terms_programmed = DATEDIFF(date_payment_programmed,date_goods_received);

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
SET payment_terms = payment_terms_programmed
WHERE
	payment_terms IS NULL;