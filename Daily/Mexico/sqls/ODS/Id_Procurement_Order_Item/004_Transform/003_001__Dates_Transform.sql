UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
SET 
	month_payment = DATE_FORMAT(date_paid, "%x-%m"),
	week_payment = operations_@v_countryPrefix@.week_iso (date_paid);

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
SET a.date_payment_programmed = CASE
									WHEN a.is_confirmed = 1 
										THEN date_payment_scheduled
										ELSE
											(CASE
												WHEN a.is_receipt = 1 
													THEN date_payment_promised
													ELSE date_payment_estimation
											END)
									END
WHERE
	procurement_payment_event = "pedido";