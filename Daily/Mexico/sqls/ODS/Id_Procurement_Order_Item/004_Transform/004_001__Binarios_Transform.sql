UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
SET 
 is_paid = 1
WHERE
	date_paid IS NOT NULL;

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
SET 
 is_invoiced = 1
WHERE
 date_invoice_created IS NOT NULL; 