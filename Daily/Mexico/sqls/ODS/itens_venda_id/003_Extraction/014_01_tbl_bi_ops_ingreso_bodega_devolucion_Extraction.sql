

UPDATE            itens_venda_oot_sample_@v_countryPrefix@ as a
       INNER JOIN operations_@v_countryPrefix@.pro_inverse_logistics_docs  ON a.item_id = pro_inverse_logistics_docs.item_id
SET
   /*Defaults and Simple Extraction*/
    a.date_inbound_il = pro_inverse_logistics_docs.date_entrance_ilwh,
a.shipping_carrier_tracking_code_inverse = pro_inverse_logistics_docs.dhl_tracking_code,
a.il_type = pro_inverse_logistics_docs.status,
a.reason_for_entrance_il = pro_inverse_logistics_docs.reason,
a.action_il = pro_inverse_logistics_docs.ilwh_1st_step,
a.damaged_item = pro_inverse_logistics_docs.damaged_item,
a.damaged_package = pro_inverse_logistics_docs.damaged_package,
a.comments_il = pro_inverse_logistics_docs.comments,
a.return_accepted = pro_inverse_logistics_docs.return_accepted,
a.position_il = pro_inverse_logistics_docs.ilwh_position,
a.date_exit_il = pro_inverse_logistics_docs.date_exit_ilwh,
a.date_canceled_il = pro_inverse_logistics_docs.date_cancelled,
a.analyst_il  = pro_inverse_logistics_docs.entered_to_ilwh_by,
a.comments = pro_inverse_logistics_docs.comments_main;