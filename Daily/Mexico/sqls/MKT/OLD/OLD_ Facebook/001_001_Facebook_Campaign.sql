delete from facebook.facebook_campaign;

insert into facebook.facebook_campaign select * from facebook.facebook_campaign_mx 
union 
select * from facebook.facebook_campaign_co 
union 
select * from facebook.facebook_campaign_pe 
union 
select * from facebook.facebook_campaign_ve 
union 
select * from facebook.facebook_campaign_fashion_co;  


update facebook.facebook_campaign set campaign = replace(campaign, '_', '.');
INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Facebook',
  'facebook.facebook_campaign',
  NOW(),
  NOW(),
  1,
  1
;

