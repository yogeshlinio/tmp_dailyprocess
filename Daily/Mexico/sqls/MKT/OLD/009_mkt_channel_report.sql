/*
Nombre: channel_report
Autor: Jean-Charles Rout Machado
Fecha: 20-01-2014
Descripcion:
*/
#Channel Report MX---

update production.tbl_order_detail t inner join SEM.transaction_id_mx c on t.order_nr=c.transaction_id set t.source_medium= concat(c.source,' / ',c.medium),t.campaign=c.campaign where source_medium is null;

#Define channel---
##UPDATE tbl_order_detail SET source='',channel='', channel_group='';
#Voucher is priority statemnt
UPDATE tbl_order_detail SET source='' where source is null;
UPDATE tbl_order_detail SET campaign='' where campaign is null;
UPDATE tbl_order_detail SET source=coupon_code WHERE coupon_code is not null;
UPDATE tbl_order_detail SET source=source_medium WHERE coupon_code is null;
UPDATE tbl_order_detail SET source=source_medium WHERE
coupon_code like 'NL%' OR coupon_code like 'NR%' OR coupon_code like
'COM%' OR coupon_code like 'BNR%';
UPDATE tbl_order_detail SET source=source_medium where channel_group='Non Identified';
#If voucher is empty use GA source


#NEW REGISTERS
UPDATE tbl_order_detail SET channel='New Register' 
WHERE (source like 'NL%' OR source like 'NR%' OR source like
'COM%' OR source like 'BNR%') AND source_medium is null;



#BLOG
-- UPDATE tbl_order_detail SET channel='Blog Linio' WHERE source like 'blog%';

#REFERRAL
UPDATE tbl_order_detail SET channel='Referral Sites' WHERE source like '%referral' 
AND source<>'facebook.com / referral'
AND source<>'m.facebook.com / referral'
AND source<>'linio.com.co / referral'
AND source<>'linio.com / referral'
AND source<>'linio.com.mx / referral'
AND source<>'google.co.ve / referral'
AND source<>'linio.com.ve / referral'
AND source<>'linio.com.pe / referral'
AND source<>'www-staging.linio.com.ve / referral'
AND source<>'google.com / referral'
AND source<>'.com.ve / referral'
AND source<>'blog.linio.com.ve / referral' AND source<>'t.co / referral'
AND source not like '%google%' AND source not like '%blog%' and source not like '%pampa%'
and source not like '%buscape%';


UPDATE tbl_order_detail SET channel='Buscape' 
WHERE source='Buscape / Price Comparison' OR source like 'buscape%' or source='tracker.buscape.com.mx / referral';

#FB ADS
UPDATE tbl_order_detail SET channel='Facebook Ads' WHERE source='facebook / socialmediaads' OR
source like '%book%ads%' or source='facebook / (not set)';

UPDATE tbl_order_detail set channel= 'Dark Post' where campaign like '%darkpost%';

#SEM 
UPDATE tbl_order_detail SET channel='SEM' WHERE source='google / cpc' AND (campaign not like '%linio%' AND 
campaign not like 'er.%' AND campaign not like 'brandb%');
UPDATE tbl_order_detail SET channel='Bing' where source like '%bing%';
UPDATE tbl_order_detail SET channel='SEM Branded' WHERE (source='google / cpc' or source like '%bing%') AND campaign like 'brandb%';
UPDATE tbl_order_detail SET channel='Other Branded' WHERE source = '201.151.86.164' or source ='10.0.0.9' or source = 'www-staging.linio.com.mx' or source='blog.linio.com.mx' or source='213.229.186.15';

#GDN
UPDATE tbl_order_detail SET channel='Google Display Network' WHERE source='google / cpc' AND 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%');

#RETARGETING
UPDATE tbl_order_detail set channel='Other Retargeting' where source like '%retargeting%';
UPDATE tbl_order_detail SET channel='Sociomantic' WHERE source='sociomantic / (not set)' 
OR source='sociomantic / retargeting' OR source 
like '%socioman%' or source='facebook / retargeting';
UPDATE tbl_order_detail SET channel='Cart Recovery' WHERE source='Cart_Recovery / CRM';
UPDATE tbl_order_detail SET channel='Vizury' WHERE source='vizury / retargeting' OR source like '%vizury%';
#UPDATE tbl_order_detail SET channel='Facebook R.' WHERE source='facebook / retargeting';
UPDATE tbl_order_detail SET channel='VEInteractive' WHERE source like '%VEInteractive%';
UPDATE tbl_order_detail SET channel='Main ADV' where source = 'retargeting / mainadv' or source like '%mainadv%' or source like '%solocpm%';
UPDATE tbl_order_detail SET channel='AdRoll' where source like '%adroll%';
UPDATE tbl_order_detail SET channel='My Things' where source like '%mythings%';
UPDATE tbl_order_detail SET channel='Avazu' where source like '%avazu%';


#Criteo
UPDATE tbl_order_detail set channel='Criteo' where source like '%criteo%';

#SOCIAL MEDIA
UPDATE tbl_order_detail SET channel='Facebook Referral' WHERE 
(source='facebook.com / referral' OR source='m.facebook.com / referral' OR source='Banamex-deb / socialmedia' OR 
source='SocialMedia / FacebookVoucher') AND 
(source<>'facebook / socialmediaads' AND source<>'facebook / retargeting');
UPDATE tbl_order_detail SET channel='Twitter Referral' WHERE source='t.co / referral';
UPDATE tbl_order_detail SET channel='FB Posts' WHERE source='facebook / socialmedia' or source='twitter / socialmedia';
UPDATE tbl_order_detail SET channel='March 10 FB Video' WHERE source='MKT1SWKug';
UPDATE tbl_order_detail SET channel='March 22 FB Video' WHERE source='MKT1IIuzu' OR SOURCE='MKT1WBY1T';
UPDATE tbl_order_detail SET channel='Youtube' where source='youtube / socialmedia';

#SERVICIO AL CLIENTE
UPDATE tbl_order_detail set channel='Other Tele Sales' where source = 'CS /%' or source = '%/ CS' or source like 'telesales /%' or source like '%/ telesales'; 
UPDATE tbl_order_detail SET channel='Inbound' WHERE source like 'Tele%' or source like '%inbound%';
UPDATE tbl_order_detail SET channel='OutBound' WHERE source like 'REC%' or source like '%outbound%';
UPDATE tbl_order_detail SET channel='CCEMP Vouchers' WHERE source like 'CCEMP%';

#SEO
UPDATE tbl_order_detail SET channel='Search Organic' WHERE source like '%organic' OR source='google.com.mx / referral';

UPDATE tbl_order_detail SET channel='Search Organic' where source like 'google.%' and source like '%referral';

UPDATE tbl_order_detail SET channel='Yahoo Answers' where source like '%mx.answers.yahoo.com%';

#NEWSLETTER
UPDATE tbl_order_detail set Channel='Newsletter' WHERE 
(source like '%CRM' OR source like '%email' OR source like 'Email Marketing%' or source like 'postal%') AND 
(source<>'TeleSales / CRM' AND source<>'CDEAL / email' AND source<>'Cart_Recovery / CRM');
UPDATE tbl_order_detail set Channel='CAC Deals' WHERE
source='MKTNfue3n' OR
source='MKT0i1Gq2' OR
source='MKT0xfgVK' OR
source='MKT1eDvI7' OR
source='MKTB0TZfz' OR
source='MKTxmsjvs' OR
source='MKTB0TZfz' OR
source='MKT3TcOf8' OR
source='MKT063HRE' OR
source='MKTqqU8KR' OR
source='MKT7lSkUd' OR
source='MKT1pFyS6' OR
source='MKT7tavt8' OR
source='MKT0uJXni' OR
source='MKT0pTUbn' OR
source='MKT1ORfvU' OR
source='MKT1Z54Lh' OR
source='MKTf4A6Sk' OR
source='MKT0xfgVK' OR
source='MKT1eDvI7' OR
source='MKTxmsjvs' OR
source='MKTB0TZfz' OR
source='MKT3TcOf8' OR
source='MKT063HRE' OR
source='MKTqqU8KR' OR
source='MKTaudifonosfb1' OR
source='MKTfDc7gLi' OR
source='MKTJ97' OR
source='MKTM36' OR
source='MKTM50' OR
source='MKTM90' OR
source like 'CAC%';


#OTHER
UPDATE tbl_order_detail SET channel='Exchange' WHERE source like 'CCE%';
UPDATE tbl_order_detail SET channel='Content Mistake' WHERE source like 'CON%';
UPDATE tbl_order_detail SET channel='Procurement Mistake' WHERE source like 'PRC%';
UPDATE tbl_order_detail SET channel='Devoluciones' WHERE source like 'OPS%';
UPDATE tbl_order_detail SET channel='IT Vouchers' WHERE source like 'IT%';
UPDATE tbl_order_detail SET channel='Employee Vouchers' WHERE (source like 'TE%' AND source<>'TeleSales / CRM')  
OR source like 'EA%' OR source like 'LIN%';

#PARTNERSHIPS

UPDATE tbl_order_detail SET channel='Partnerships' where source like '%Partnership%' or coupon_code like 'PAR%';

UPDATE tbl_order_detail SET channel='Other Partnerships' where source like '%PARTNERSHIP%' or coupon_code like '%PARTNERSHIP%';

UPDATE tbl_order_detail SET channel='Buen Fin 2013' where campaign like '%BuenFin2013%';

UPDATE tbl_order_detail SET channel='Banorte' WHERE source like '%banorte%' or (description_voucher like '%banorte%'
and coupon_code=source) or source like '%Banorteixe%' or coupon_code in ('COM1gn4kh', 
'COMOeznfM', 
'COM1aJa4q', 
'COM0E6Dlp', 
'COMqSU7M5',
'COM0t9PWA',
'COM1ssnpB', 
'COM0A22dO',
'COM4FkI2i');
UPDATE tbl_order_detail SET channel='Santander' WHERE source='MKTExFiiv' OR source like '%Santander-deb%' OR
source='MKTsaHAD2' OR
source='MKT6bucMW' OR
source='MKT0v8y8X' OR
source='MKTulPG5T' OR
source='MKTx6OGcF' OR
source='MKT0bhnLc' OR
source='MKT0iZjZS' OR
source='MKT0Akg6y' OR
source='MKTn6RcCs' OR
source='MKT1ft96n' OR
source='MKTnwM00A' OR
source='MKTph4cQn' OR
source='MKTHs9mG1' OR
source='MKTL38fvT' OR
source='MKT111GV9' OR
source='MKT59kWnR' OR
source='MKTXukY1p' OR source like '%Santander%' OR source='MKT1H8uB8' OR
source='MKT1A3rIA' OR 
source='MKT1RwDN6' OR 
source='MKTJ5YgZz' OR 
source='MKTyJLtQA' OR 
source='MKT0LcYyg' OR 
source='MKT15sqy8' OR 
source='MKT1H8uB8' OR 
source='MKTHyUt98' OR 
source='MKT1VK5uv' OR 
source='MKTsCydnL' OR 
source='MKT1aTNQc' OR 
source='MKT1aTCNt' OR 
source='MKT17v5KB' OR 
source='MKT0StMG8' OR 
source='MKTxQGoWi'
OR (description_voucher like '%santander%' and coupon_code=source);
UPDATE tbl_order_detail set Channel='AMEX' WHERE source like 'AMEX%';

UPDATE tbl_order_detail SET channel='PAR Voucher' WHERE source like 'PAR%';
UPDATE tbl_order_detail SET channel='Banamex' WHERE 
source='Banamex / Partnership' OR source='MKTfXPuRa' OR source='MKT1KzeNi' OR source='MKT0plcnf' OR
source='MKT0YW4bI' OR
source='MKT1kgmm6' OR
source='MKTTLQ8QP' OR
source='MKTpgBEQv' OR
source='MKTevO4UN' OR
source='MKT0yvpr6' OR
source='MKTgUOaDm' OR
source='MKT1KzeNi' OR
source='MKT0plcnf' OR
source='MKTfXPuRa' OR source like '%banamex%'
 or (description_voucher like '%banamex%' and coupon_code=source) or coupon_code in ('COMWAnHqk',
'COMj6VdXm',
'COMleM6yw',
'COMvacYUl',
'COM0quw9T',
'COM1od8oi',
'COM03A4rf');
UPDATE tbl_order_detail SET channel='Dafiti' WHERE source='Dafiti / Partnership' OR
(description_voucher like '%dafiti%' and coupon_code=source);
UPDATE tbl_order_detail SET channel='Bancomer' WHERE 
source='MKT0qnqj3' OR
source='MKT1mR3we' OR
source='MKTHGWq1T' OR
source='MKT1LPgtD' OR
source='MKTQzGnNQ' OR
source='MKT1mlElQ' OR source='MKT0qnqj3' OR source like '%bancomer%' or 
(description_voucher like '%bancomer%' and coupon_code=source) or source like '%Banamex-deb%' or coupon_code in ('COMbB6AKv',
'COMQV1fvj',
'COMk26IZN',
'COMl3tBhE',
'COM0bKAkK',
'COMwmfJAH',
'COMyIDag2',
'COMKXInFy',
'COMpfewAF',
'COM9ESnZw',
'COM0SrC75',
'COMQTbq9s',
'COMDaXNVy',
'COMq62YER',
'COM1HTYw1',
'COM1tqcJ4',
'COMKwqYxd',
'COMBqk10S',
'COMVRMweB',
'COMsd1D5W',
'COM08NaXX',
'COMTHhcfh',
'COMF5vDPA',
'COM09ykHd',
'COMtgSSOc',
'COMeOoRqS',
'COMo8FaTg',
'COMDfv9ii',
'COM0reEj7',
'COMMrwW8l',
'COMekuAx9',
'COM1ipcME',
'COMmmJv5p',
'COMYIIvGh');

UPDATE tbl_order_detail SET channel='American Express' WHERE 
source='AMEX1cqUNp' OR
source='AMEX0JmHIq' OR
source='AMEX4xvQ53' OR
source='AMEX7lwlWV' OR
source='AMEXSoybiO' or 
(description_voucher like '%american%express' and coupon_code=source);

UPDATE tbl_order_detail SET channel='PayPal' WHERE source like '%paypal%' or 
(description_voucher like '%paypal%' and coupon_code=source);



UPDATE tbl_order_detail set channel='Corporate Deal' where coupon_code in ('Intercam','CDEALBW','CDEALGS','CDEALcrecer','CDEALMV','LYTtTtn8Q','CDEALFUNKY','CDEALTW','CDEALGH','CDEALSMG', 'CDEALP001');

UPDATE tbl_order_detail set channel='Corporate Deal' where coupon_code LIKE 'CDEAL%' or source_medium like '%CORPORATESALES%';

UPDATE tbl_order_detail set channel='Bank' where coupon_code in('MKT0wnkzE',
'MKTkkyQnk',
'MKT1BY8UY',
'MKTMblVuX',
'MKT0tnUuL',
'MKT1mFB5t',
'MKTM84jXQ',
'MKT0iQEbB',
'MKTOTOgvc',
'MKTOggLi5',
'MKTYMZaRd',
'MKT0N6Uy6',
'MKT0ThhQd',
'MKTiuuHiD',
'MKTUb9lgI',
'MKTzNa59o',
'MKT0aF3I6',
'MKTxmmIGM',
'MKTpaypal6',
'MKT010yoY',
'MKTFQsEjD',
'MKTUbO0ZH',
'MKTxYYAFX',
'MKT0UC4gA',
'MKT6MveDC',
'MKT154hDI',
'MKTeCmqxS',
'COMtNBgtx',
'COM1XeXow',
'COM1yS69N',
'COM12vTGP',
'COM0Szfz1',
'COMsq3XPZ',
'COMOhGprT',
'COMAtSA4A',
'COMTacDRh',
'COMoW24Lj',
'COM02XwgL',
'MKTFM9Ojf',
'MKT078Io6',
'MKTE4UpIP',
'MKT1uaPuh',
'MKT1KOf1T',
'MKT1FtOWs',
'MKTpiP20I',
'MKTNX01Vo',
'MKT5A0vss',
'COMq2yXkS',
'COMKImnUu',
'COMrwMzLv',
'COMLbrM8B',
'COMe6sShP',
'COM1PQNCd',
'MKTFQReMz',
'MKTCPDLKa',
'MKTxmfcd7',
'MKT0CMpgL',
'MKT1R2DZ0',
'MKT00fpoM',
'COMtNBgtx',
'COM1XeXow',
'COM1yS69N',
'COM12vTGP',
'COM0Szfz1',
'COMsq3XPZ',
'COMOhGprT',
'COMAtSA4A',
'COMTacDRh',
'COMoW24Lj',
'COM02XwgL',
'COM0u62rt',
'COM0jwWC4',
'COMEtA71y',
'COMvIVok2',
'COM0iGdhI',
'COMrrk8t6',
'COMa1U08u',
'COM0FQm0F',
'COM12PSUn',
'COMIONFTE',
'COM0u3tio',
'COM1nejSI',
'COM1LQ2c9');

#DIRECT
UPDATE tbl_order_detail SET channel='Linio.com Referral' WHERE  
source='linio.com.co / referral' OR
source='linio.com / referral' OR
source='linio.com.mx / referral' OR
source='linio.com.ve / referral' OR
source='linio.com.pe / referral' OR
source='www-staging.linio.com.ve / referral';

UPDATE tbl_order_detail SET channel='Directo / Typed' WHERE source='(direct) / (none)' OR 
source like '%direct%none%';

#OFFLINE
UPDATE tbl_order_detail SET channel='PR vouchers' WHERE source like 'PR%';
UPDATE tbl_order_detail SET channel='Flyers' WHERE source='regalolinio200' OR
source='regalo200' OR source='descuento20';
UPDATE tbl_order_detail SET channel='SMS' WHERE source='madre' or source like '%Partnership/sms%';
UPDATE tbl_order_detail SET channel='Other Offline' Where
source='CATALOGO' or source='descuento20' or source like '%metro%' 
or source like '%sms%';
UPDATE tbl_order_detail set channel='Linio Flyers' where coupon_code in ('descuento20','regalo200');
UPDATE tbl_order_detail set channel='Bank Flyers' where coupon_code in ('BANIXE01','PAYPAL150');
UPDATE tbl_order_detail set channel='SMS' where coupon_code in ('PADRE','REGRESA','REGRESO','REGRESA1');
UPDATE tbl_order_detail set channel='Partnership Flyers' where coupon_code in ('taxi','regalolinio100','regalolinio150');

UPDATE tbl_order_detail set channel='NUEVO' where coupon_code='NUEVO';
UPDATE tbl_order_detail set channel='EXCLUSIVIDAD' where coupon_code='EXCLUSIVIDAD';
UPDATE tbl_order_detail set channel='regalolinio100' where coupon_code='regalolinio100';
UPDATE tbl_order_detail set channel='regalolinio150' where coupon_code='regalolinio150';
UPDATE tbl_order_detail set channel='regreso' where coupon_code='regreso';
UPDATE tbl_order_detail set channel='regresa' where coupon_code='regresa';
UPDATE tbl_order_detail set channel='regresa1' where coupon_code='regresa1';
UPDATE tbl_order_detail set channel='taxi' where coupon_code='taxi';
UPDATE tbl_order_detail set channel='BANIXE01' where coupon_code='BANIXE01';
UPDATE tbl_order_detail set channel='BANIXE02' where coupon_code='BANIXE02';
UPDATE tbl_order_detail set channel='BANIXE03' where coupon_code='BANIXE03';
UPDATE tbl_order_detail set channel='BANIXE04' where coupon_code='BANIXE04';
UPDATE tbl_order_detail set channel='PAYPAL150' where coupon_code='PAYPAL150';
UPDATE tbl_order_detail set channel='SAN200' where coupon_code='SAN200';
UPDATE tbl_order_detail set channel='LIN120' where coupon_code='LIN120';
UPDATE tbl_order_detail set channel='regalo150' where coupon_code='regalo150';
UPDATE tbl_order_detail set channel='linio150' where coupon_code='linio150';
UPDATE tbl_order_detail set channel='linio5' where coupon_code='linio5';
UPDATE tbl_order_detail set channel='linio220' where coupon_code='linio220';
UPDATE tbl_order_detail set channel='linio10' where coupon_code='linio10';
UPDATE tbl_order_detail set channel='Bridgestone150' where coupon_code='Bridgestone150';
UPDATE tbl_order_detail set channel='N80' where coupon_code='N80';
UPDATE tbl_order_detail set channel='N120' where coupon_code='N120';
UPDATE tbl_order_detail set channel='N200' where coupon_code='N200';
UPDATE tbl_order_detail set channel='soriana120' where coupon_code='soriana120';
UPDATE tbl_order_detail set channel='soriana100' where coupon_code='soriana100';
UPDATE tbl_order_detail set channel='BMX100' where coupon_code='BMX100';
UPDATE tbl_order_detail set channel='BMX150' where coupon_code='BMX150';
UPDATE tbl_order_detail set channel='BANIXE04' where coupon_code='BANIXE04';
UPDATE tbl_order_detail set channel='BBVA150' where coupon_code='BBVA150';
UPDATE tbl_order_detail set channel='FINDE' where coupon_code='FINDE';
UPDATE tbl_order_detail set channel='VUELA' where coupon_code='VUELA';
UPDATE tbl_order_detail set channel='OFERTA150' where coupon_code='OFERTA150';
UPDATE tbl_order_detail set channel='DIC150' where coupon_code='DIC150';
UPDATE tbl_order_detail set channel='DEPORTE150' where coupon_code='DEPORTE150';


#AFFILIATES
UPDATE tbl_order_detail SET channel='Other Affiliates' where source like '%affiliate%' or source like '%afiliate%';
UPDATE tbl_order_detail SET channel='Descuentomx' WHERE source like 'dscuentomx / affiliates' or source like 'dscuento.com.mx%';
UPDATE tbl_order_detail SET channel='Mercado Libre Affliates' WHERE source like 'Mercadolibre / Affiliates';
UPDATE tbl_order_detail SET channel='Glg' WHERE source='glg / Affiliates';
UPDATE tbl_order_detail SET channel='Clickmagic' where source='Clickmagic / Affiliates';
UPDATE tbl_order_detail SET channel='Trade Tracker' where source='tradetracker / Affiliates';
UPDATE tbl_order_detail SET channel='Dscuento' where source='MKTDscuento' or source='dscuento.com.mx / referral';
UPDATE tbl_order_detail SET channel='Cupones Magicos' where source='cuponesmagicos.com.mx / referral';
UPDATE tbl_order_detail set channel='Pampa Network' where source like '%pampa%';
UPDATE tbl_order_detail set channel='SoloCPM' where source like '%solocpm%';
UPDATE tbl_order_detail set channel='Promodescuentos' where source like '%promodescuentos%';

#Pago Movil
UPDATE tbl_order_detail SET channel='Pago Movil' where source like '%Pagomovil/partnership%' or campaign like '%pagomovil2013%';

#El Universal
UPDATE tbl_order_detail SET channel='El Universal' where source like '%el-universal%';

#CORPORATE SALES
UPDATE tbl_order_detail SET channel='Corporate Sales' WHERE source like 'CDEAL%';

#N/A
UPDATE 
tbl_order_detail SET channel='Unknown-No voucher' 
WHERE source_medium='' or source_medium is null;
UPDATE tbl_order_detail SET 
channel='Unknown-Voucher' WHERE channel='' AND source<>'';

#Channel Report Extra Stuff to define Channel---
#Venta corporativa 



#Partnerships Channel Extra


#Mercado libre Orders 

UPDATE tbl_order_detail SET channel='Mercado Libre Voucher' WHERE channel like 'MKTMeLi%';
#OFFLINE
UPDATE tbl_order_detail SET channel='PR vouchers' WHERE channel like 'PR%';

#CAC Vouchers
UPDATE tbl_order_detail set channel = 'CAC Deals' where (coupon_code like 'CAC%' or campaign like '%CAC%') and campaign not in ('MEX_fanpage_m_21-50_saludbelleza_20130606NauticaClassic_23_48', 'BlancaChida', '20130115.BibliotecaCool', '20130103.VacacionesEnLasMontañas');

#Curebit
UPDATE tbl_order_detail set channel = 'Curebit' where coupon_code like '%cbit%' or source_medium like '%curebit%';

UPDATE tbl_order_detail t inner join mobileapp_transaction_id m on t.order_nr=m.transaction_id set channel = 'Mobile App';

#DEFINE CHANNEL GROUP

#Mobile App
UPDATE tbl_order_detail SET channel_group='Mobile App' WHERE channel='Mobile App';

#Facebook Ads Group
UPDATE tbl_order_detail SET channel_group='Facebook Ads' WHERE channel='Facebook Ads' or channel = 'Dark Post' or channel='Facebook Referral';

#Referal Platform
UPDATE tbl_order_detail set channel_group = 'Referal Platform' where channel='Curebit';

#SEM Group
UPDATE tbl_order_detail SET channel_group='Search Engine Marketing' WHERE channel='SEM' or channel='Bing' or channel='Other Branded';

#Social Media Group
UPDATE tbl_order_detail SET 
channel_group='Social Media' WHERE channel='Twitter Referral' or channel='FB Posts' or channel='March 10 FB Video' OR channel='March 22 FB Video' or channel='Youtube';

#Retargeting Group
UPDATE tbl_order_detail SET channel_group='Retargeting' WHERE channel='Sociomantic' or source like '%retargeting' OR 
channel='Cart Recovery' OR channel='Vizury' OR 'Facebook R.' OR channel = 'VEInteractive' OR channel = 'Criteo' or channel='Main ADV' or channel='AdRoll' or channel='Other Retargeting' or channel='My Things' or channel ='Avazu' or channel='Promosdescuentos';

#GDN group
UPDATE tbl_order_detail SET channel_group='Google Display Network' WHERE channel like 'Google Display Network'; 

#NewsLetter Group
UPDATE tbl_order_detail SET channel_group='NewsLetter' WHERE channel='Newsletter';

#CAC Deals Group
UPDATE tbl_order_detail set channel_group='CAC Deals' where channel='CAC Deals';

#SEO Group
UPDATE tbl_order_detail SET channel_group='Search Engine Optimization' WHERE channel='Search Organic' or channel='Yahoo Answers';

#Partnerships Group

UPDATE tbl_order_detail SET channel_group='Partnerships' WHERE channel='Santander' OR channel='PAR Voucher' OR 
channel='AMEX' or channel='Banamex' OR channel='Dafiti' OR channel='Bancomer' OR channel='American Express' OR
channel='Banorte' or channel='PayPal' or channel='Bank' or channel='Corporate Deal' or channel='Other Partnerships' or channel='Pago Movil' or channel='El Universal' or channel='Buen Fin 2013' or channel='Partnerships';


#Corporate Sales Group
UPDATE tbl_order_detail SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';

#Offline Group
UPDATE tbl_order_detail SET channel_group='Offline Marketing' WHERE channel='PR vouchers' or channel='Flyers' 
or channel='SMS' or channel='Other Offline' or channel='Linio Flyers' or channel='Bank Flyers' or channel='Partnership Flyers' or channel='NUEVO' or channel='EXCLUSIVIDAD' or channel='regalolinio100' or channel='regalolinio150' or channel='regreso' or channel='regresa'  or channel='regresa1' or channel='taxi' or channel='BANIXE01' or channel='BANIXE02' or channel='BANIXE03' or channel='PAYPAL150' or channel='SAN200' or channel='LIN120' or channel='regalo150' or channel='linio150' or channel='linio5' or channel='linio220' or channel='linio10' or channel='Bridgestone150' or channel='N80' or channel='N120' or channel='N200' or channel='soriana120' or channel='soriana100' or channel='BMX100' or channel='BMX150' or channel='BANIXE04' or channel='BBVA150' or channel='FINDE' or channel='VUELA' or channel='OFERTA150' or channel='DIC150' or channel='DEPORTE150';

#Tele Sales Group
UPDATE tbl_order_detail SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales' OR channel='CCEMP Vouchers' or channel='Other Tele Sales';

#Branded Group
UPDATE tbl_order_detail SET channel_group='Branded' 
WHERE channel='Linio.com Referral' OR channel='Directo / Typed' OR
channel='SEM Branded';

#ML Group
UPDATE tbl_order_detail SET channel_group='Mercado Libre' WHERE channel='Mercado Libre Referral' OR channel='Mercado Libre Voucher' OR channel='Otros - Mercado Libre';

#Blog Group
-- UPDATE tbl_order_detail SET channel_group='Blogs' WHERE channel='Blog Linio';

#Affiliates Group
UPDATE tbl_order_detail SET channel_group='Affiliates' WHERE channel='Buscape' OR channel='Descuentomx' 
OR channel='Mercado Libre Affliates' OR channel='Glg' or channel='Pampa Network' OR channel='Clickmagic' OR 
channel='Tradetracker' OR channel='Dscuento' OR channel='Referral P.' OR channel='Cupones Magicos' or channel='Other Affiliates' or channel='SoloCPM' or channel='PromoDescuentos';

#Other (identified) Group
UPDATE tbl_order_detail SET channel_group='Other (identified)' 
WHERE channel='Referral Sites' OR channel='Reactivation Letters' 
OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' OR
channel='Exchange' OR
channel='Content Mistake' OR
channel='Procurement Mistake' OR
channel='Employee Voucher' OR channel='IT Vouchers';

#Extra Channel Group

update tbl_order_detail set channel_group='Social Media' where source_medium = 'FB / FB';
update tbl_order_detail set channel_group='Social Media' where source_medium = 'Facebook / social media';
update tbl_order_detail set channel_group='Google Display Network' where source_medium = 'google / Display';
update tbl_order_detail set channel_group='Newsletter' where source_medium = 'Postal / (not set)';
update tbl_order_detail set channel_group='Other (identified)' where source_medium = 'sclaberinto.blogspot.mx / referral';
update tbl_order_detail set channel_group='Other (identified)' where source_medium = 'plus.url.google.com / referral';
update tbl_order_detail set channel_group='Social Media' where source_medium = 'SocialMedia / FacebookVoucher';
update tbl_order_detail set channel_group='Partnerships' where source_medium = 'Publimetro / Partnership';
update tbl_order_detail set channel_group='Google Display Network' where source_medium = 'MetrosCubicos / Retargeting';
update tbl_order_detail set channel_group='Other (identified)' where source_medium = 'mail.google.com / referral';
update tbl_order_detail set channel_group='Affiliates' where source_medium = 'Affiliates / Mercadolibre';
update tbl_order_detail set channel_group='Affiliates' where source_medium = 'Cuponzote / Affiliates';
update tbl_order_detail set channel_group='Affiliates' where source_medium = 'Google_Shopping / Price Comparison';
update tbl_order_detail set channel_group='Tele Sales' where source_medium = 'CS / Inbound';
update tbl_order_detail set channel_group='Tele Sales' where source_medium = 'CS / Outbound';
update tbl_order_detail set channel_group='Other (identified)' where source_medium = 'pagead2.googlesyndication.com / referral';
update tbl_order_detail set channel_group='Affiliates' where source_medium = 'tradetracker / Affiliates';
update tbl_order_detail set channel_group='Affiliates' where source_medium = 'Clickmagic / Affiliates';


#Unknown Group
UPDATE tbl_order_detail SET channel_group='Non identified' WHERE channel='Unknown-No voucher' OR channel='Unknown-Voucher';

#Define Channel Type---
#TO BE DONE

#Ownership

UPDATE tbl_order_detail set Ownership='' where channel_group='Social Media';
UPDATE tbl_order_detail set Ownership='' where channel_group='NewsLetter';
UPDATE tbl_order_detail set Ownership='' where channel_group='Search Engine Optimization';
UPDATE tbl_order_detail set Ownership='' where channel_group='Blogs';
UPDATE tbl_order_detail set Ownership='' where channel_group='Offline Marketing';
UPDATE tbl_order_detail set Ownership='' where channel_group='Partnerships';
UPDATE tbl_order_detail set Ownership='' where channel_group='Mercado Libre';
UPDATE tbl_order_detail set Ownership='' where channel_group='Tele Sales';


-- SELECT 'Start: channel_report_no_voucher ', now();
-- call channel_report_no_voucher();

-- SELECT 'Start: visits_costs_channel ', now();
-- call visits_costs_channel();

-- SELECT 'Start: daily markting ', now();
-- call daily_marketing();

-- SELECT 'Start: monthly marketing ', now();
-- CALL monthly_marketing_2();
-- SELECT 'End: channel_report', now();
