call SEM.google_optimization_ad_group_mx;
call SEM.google_optimization_transaction_id_mx;
call SEM.google_optimization_keyword_mx;
call SEM.google_optimization_transaction_id_keyword_mx;

call facebook.facebook_optimization_mx;
call facebook.fanpage_optimization_mx;
call facebook.facebook_optimization_keyword_mx;
call production.channel_report;
call production.channel_report;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Mexico',  #Country
  'marketing_report.marketing_mx',
  NOW(),
  NOW(),
  1,
  1;

INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Mexico',
  'marketing_report.marketing_mx',
  NOW(),
  NOW(),
  1,
  1
;

