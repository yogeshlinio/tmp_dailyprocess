USE marketing_mx;

update channel_report c
join marketing.smc
join marketing.channel 
set c.channel = channel.channel,
		c.fk_channel = channel.id_channel
where smc.fk_channel = channel.id_channel
and c.source like concat('%', smc.source, '%')
and smc.medium is null
and smc.campaign is null;

update channel_report c
join marketing.smc
join marketing.channel 
set c.channel = channel.channel,
		c.fk_channel = channel.id_channel
where smc.fk_channel = channel.id_channel
and c.medium like concat('%', smc.medium, '%')
and smc.source is null
and smc.campaign is null;

update channel_report c
join marketing.smc
join marketing.channel 
set c.channel = channel.channel,
		c.fk_channel = channel.id_channel
where smc.fk_channel = channel.id_channel
and c.campaign like smc.campaign_query escape '!'
and smc.source is null
and smc.medium is null;

update channel_report c
join marketing.smc
join marketing.channel 
set c.channel = channel.channel,
		c.fk_channel = channel.id_channel
where smc.fk_channel = channel.id_channel
and c.source like concat('%', smc.source, '%')
and c.medium like concat ('%', smc.medium, '%')
and smc.campaign is null;

update channel_report c
join marketing.smc
join marketing.channel 
set c.channel = channel.channel,
		c.fk_channel = channel.id_channel
where smc.fk_channel = channel.id_channel
and c.source like concat('%', smc.source, '%')
and c.campaign like smc.campaign_query escape '!'
and smc.medium is null;

update channel_report c
join marketing.smc
join marketing.channel 
set c.channel = channel.channel,
		c.fk_channel = channel.id_channel
where smc.fk_channel = channel.id_channel
and c.campaign like smc.campaign_query escape '!'
and c.medium like concat ('%', smc.medium, '%')
and smc.source is null;

update channel_report c
join marketing.smc
join marketing.channel 
set c.channel = channel.channel,
		c.fk_channel = channel.id_channel
where smc.fk_channel = channel.id_channel
and c.source like concat('%', smc.source, '%')
and c.medium like concat('%', smc.medium, '%')
and c.campaign like smc.campaign_query escape '!';

update channel_report c
set
	fk_channel = 185
where	
	(source is not null 
	OR medium is not null 
	OR campaign is not null)
	and (fk_channel is null or fk_channel = 0);
	
update channel_report c
set
	fk_channel = 182
where	
	(source='' 
	OR medium='' 
	OR campaign='')
	and (fk_channel is null or fk_channel = 0);
	
update channel_report c
join marketing.channel
set	
	c.channel = channel.channel
where c.fk_channel = channel.id_channel
