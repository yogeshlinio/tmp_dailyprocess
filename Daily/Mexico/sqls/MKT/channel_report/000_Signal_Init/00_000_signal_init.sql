USE marketing_mx;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Mexico', 
  'marketing_mx.channel_report',
  'start',
  NOW(),
  MAX(date),
  count(*),
  count(*)
FROM
channel_report
;