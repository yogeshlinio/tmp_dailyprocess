
USE @marketing@;

SET @country :='@v_country@';


#Vizury
drop temporary table if exists aux_vizury;
create temporary table aux_vizury (index(date),index(country))
select sum(rev*orderbeforecan-rev*cancelled)*er.xr cost, a.date, a.country 
from @development@.A_Master a
inner join
marketing_report.vizury_report b
on a.ordernum=b.order_id
inner join
@development@.A_E_BI_ExchangeRate_USD er 
on date_format(a.date,'%Y%m')=er.month_num and a.country=er.country
where a.country='Mex'
group by a.date;

drop temporary table if exists aux_cp;
create temporary table aux_cp (index(date),index(country))
select sum(gross_revenue) as total_gr, date ,country, count(1) cuantos
from channel_performance 
where country='MEX' and channel='Vizury' group by date;

update channel_performance g	
inner join
aux_cp gr
on gr.date=g.date and gr.country=g.country 
inner join  
aux_vizury t
on gr.date=t.date 
set g.marketing_cost=t.cost*if(gr.total_gr=0,1,g.gross_revenue)/if(gr.total_gr=0,gr.cuantos,gr.total_gr)
where g.channel='Vizury'
;



	
	
#sociomantic
drop temporary table if exists aux_socio;
create temporary table aux_socio (index(date),index(country))
select sum(cost) cost, a.date, a.country, a.campaign plat_camp ,b.campaign_ga ga_camp 
from marketing_report.sociomantic a
inner join
marketing.retargeting_campaigns b
on a.campaign=b.campaign_plataforma and a.country=b.country 
where fk_channel=170 and a.country='MEX'
group by a.date, country, ga_camp;

drop temporary table if exists aux_cp;
create temporary table aux_cp (index(date),index(country))
select sum(gross_revenue) as total_gr, country, date, count(1) cuantos
from channel_performance 
where fk_channel=170 
group by date, country;


update channel_performance g	
inner join
aux_socio t
on g.date=t.date and g.country=t.country and g.campaign=t.ga_camp
inner join 
aux_cp gr
on gr.date=t.date and gr.country=t.country and g.campaign=t.ga_camp
inner join
@development@.A_E_BI_ExchangeRate er 
on date_format(t.date,'%Y%m')=er.month_num
set g.marketing_cost=t.cost*if(gr.total_gr=0,1,g.gross_revenue)/if(gr.total_gr=0,gr.cuantos,gr.total_gr)*er.xr
where g.fk_channel=170
and er.country=t.country ;

	

	
#Triggit	
drop temporary table if exists aux_triggit;
create temporary table aux_triggit (index(date))
select sum(clicks)*cost_click cost, report_start_date as date
from marketing_report.triggit_@v_countryPrefix@
inner join 
marketing_report.costo_click_triggit cct
on cct.country=@country
group by date;

drop temporary table if exists aux_cp;
create temporary table aux_cp (index(date),index(country))
select sum(gross_revenue) as total_gr, country, date, count(1) cuantos
from channel_performance where fk_channel=177 group by date, country;

update channel_performance g	
inner join
aux_cp gr
on gr.date=g.date and gr.country=g.country 
inner join  
aux_triggit t
on gr.date=t.date 
inner join
@development@.A_E_BI_ExchangeRate_USD er 
on date_format(t.date,'%Y%m')=er.month_num
set g.marketing_cost=t.cost*if(gr.total_gr=0,1,g.gross_revenue)/if(gr.total_gr=0,gr.cuantos,gr.total_gr)*er.xr
where g.fk_channel=177
and er.country=gr.country 
;


