call operations_mx.calcworkdays_inventory;

SELECT date_first INTO @dia_habil_anterior FROM operations_mx.calcworkdays_inventory
WHERE date_last = curdate() - interval 1 day
     AND workdays = 1 
     AND isholiday_first = 0
     AND isweekend_first = 0
     AND isweekday_first = 1;

set @max_day=cast((select max(date) from operations_mx.tbl_days_of_inventory_per_day) as date);

#delete from operations_mx.tbl_days_of_inventory_per_day 
#where date=@dia_habil_anterior;

INSERT INTO operations_mx.tbl_days_of_inventory_per_day 
			(date,buyer,new_cat1,new_cat2,new_cat3,sku_simple,sku_config,
				items_in_stock, items_in_stock_consignment, costo_after_vat_in_stock, costo_after_vat_Consignation, 
					category_bp, pc_15_percentage, visible)
SELECT * FROM 
			(SELECT @dia_habil_anterior AS date,buyer,
					category_1,category_2,category_3,sku_simple,sku_config,
					SUM(IF(fulfillment_type_real like '%Own Warehouse%' OR fulfillment_type_real like '%Crossdock%',
					in_stock,0)) AS 'items_in_stock',					
					SUM(IF(fulfillment_type_real like '%Consignment%',
					in_stock,0)) AS 'items_in_stock_consignment',
					SUM(IF(fulfillment_type_real like '%Own Warehouse%' OR fulfillment_type_real like '%Crossdock%',
					cost_w_o_vat*in_stock,0)) AS 'Inventory_cost',
					SUM(IF(fulfillment_type_real like '%Consignment%',
					cost_w_o_vat*in_stock,0)) AS 'Consignation_cost',
					category_bp AS 'Category_BP',
					pc_15_percentage as 'PC_1_5',
					visible as 'Is_Visible'
				FROM operations_mx.out_stock_hist WHERE in_stock=1 AND reserved = 0
GROUP BY sku_simple) AS a
where date<>@max_day;

UPDATE operations_mx.tbl_days_of_inventory_per_day d
INNER JOIN 
		(SELECT sku_simple, 
		SUM(sold_last_30_order) sold_last_30_order,
		SUM(sold_last_30_order_cost_w_o_vat) sold_last_30_order_cost_w_o_vat,
		SUM(sold_last_15_order) sold_last_15_order,
		SUM(sold_last_15_order_cost_w_o_vat) sold_last_15_order_cost_w_o_vat,		
		SUM(sold_last_7_order) sold_last_7_order,
		SUM(sold_last_7_order_cost_w_o_vat) sold_last_7_order_cost_w_o_vat
		FROM operations_mx.out_stock_hist 
		WHERE 
			sku_in_stock = 1 GROUP BY sku_simple) s 
ON 
	d.sku_simple = s.sku_simple
SET 
	d.items_sold_last_30_days = s.sold_last_30_order,
	d.costo_after_vat_sold_last_30_days = s.sold_last_30_order_cost_w_o_vat,	
	d.items_sold_last_15_days = s.sold_last_15_order,
	d.costo_after_vat_sold_last_15_days = s.sold_last_15_order_cost_w_o_vat,
	d.items_sold_last_7_days = s.sold_last_7_order,
	d.costo_after_vat_sold_last_7_days = s.sold_last_7_order_cost_w_o_vat
where date<>@max_day ;

SET @yrmonth := CONCAT(year(@dia_habil_anterior),IF(MONTH(@dia_habil_anterior)<10,CONCAT(0,MONTH(@dia_habil_anterior)),MONTH(@dia_habil_anterior)));

UPDATE operations_mx.tbl_days_of_inventory_per_day 
SET
	yrmonth=@yrmonth,
	week=weekofyear(@dia_habil_anterior)
WHERE yrmonth IS NULL;

UPDATE operations_mx.tbl_days_of_inventory_per_day 
SET
	inv_days_by_items=
					IF(items_sold_last_30_days=0,NULL,(items_in_stock+items_in_stock_consignment)/(items_sold_last_30_days/30)),
	inv_days_by_value=
					IF(costo_after_vat_sold_last_30_days=0,NULL,(costo_after_vat_in_stock+costo_after_vat_consignation)/(costo_after_vat_sold_last_30_days/30))
where date<>@max_day
;

SELECT  'Actualizar sell list',now();

UPDATE operations_mx.tbl_days_of_inventory_per_day b 
INNER JOIN
		(SELECT sku_simple,SUM(is_sell_list) AS 'items_sell_list',
			SUM(is_sell_list*cost_w_o_vat) AS 'value_sell_list'
			FROM operations_mx.out_stock_hist
		WHERE in_stock=1 AND reserved=0
						AND fulfillment_type_real not IN ('Consignment')
		GROUP BY sku_simple) AS a
ON 
	b.sku_simple=a.sku_simple
SET 
	b.items_sell_list=a.items_sell_list,
	b.value_sell_list=a.value_sell_list
WHERE date=@dia_habil_anterior
AND date<>@max_day;

UPDATE operations_mx.tbl_days_of_inventory_per_day b 
INNER JOIN
		(SELECT sku_simple,SUM(is_sell_list_30) AS 'items_sell_list_30',
				SUM(is_sell_list_30*cost_w_o_vat) AS 'value_sell_list_30'
			FROM operations_mx.out_stock_hist
			WHERE in_stock=1 AND reserved=0
						AND fulfillment_type_real not IN ('Consignment')
			GROUP BY sku_simple) AS a
ON 
	b.sku_simple=a.sku_simple
SET 
	b.items_sell_list_30=a.items_sell_list_30,
	b.value_sell_list_30=a.value_sell_list_30
WHERE date=@dia_habil_anterior
AND date<>@max_day;


INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  UPDATEd_at, 
  key_date)
SELECT 
  'Mexico', 
  'ExtraProcess.Inventory_PerDay',
  'finish',
  NOW(),
  NOW();


update operations_mx.tbl_days_of_inventory_per_day set buyer = trim(both ' ' from replace(replace(replace(buyer, '\t', ''), '\n',''),'\r',''));

