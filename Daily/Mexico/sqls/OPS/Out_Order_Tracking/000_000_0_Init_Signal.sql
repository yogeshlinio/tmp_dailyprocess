-- --------------------------------------------------------------------------------
-- Nombre: 1000_ops_out_order_tracking.sql
-- Pais: Colombia
-- Autor: BI Ops Team
-- Fecha creaci�n: 2014-02-19
-- Version: 1.0
-- Fecha ultima modificacion: 2014-02-19
-- --------------------------------------------------------------------------------
SELECT  'Monitoring_log',now();
INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Mexico', 
  'out_order_tracking',
  'start',
  NOW(),
  max(datetime_ordered),
  count(*),
  count(item_counter)
FROM
  operations_mx.out_order_tracking;