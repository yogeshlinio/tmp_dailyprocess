REPLACE operations_mx.out_order_tracking SELECT * FROM operations_mx.out_order_tracking_sample;

#INSERT INTO `production`.`table_monitoring_report` VALUES (NULL, 'Daily Procurement - MX Project', NOW());
#INSERT INTO `production`.`table_monitoring_report` VALUES (NULL, 'Daily Warehouse - MX Project', NOW());
#INSERT INTO `production`.`table_monitoring_report` VALUES (NULL, 'Daily Deliveries - MX Project', NOW());
#INSERT INTO `production`.`table_monitoring_report` VALUES (NULL, 'Daily Warehouse - MX Project', NOW());

DROP TABLE IF EXISTS production.out_order_tracking;
CREATE TABLE production.out_order_tracking LIKE operations_mx.out_order_tracking; 
INSERT INTO production.out_order_tracking SELECT * FROM operations_mx.out_order_tracking; 
