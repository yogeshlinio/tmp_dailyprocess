INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Mexico', 
  'out_procurement_tracking',
  'finish',
  NOW(),
  max(date_po_created),
  count(*),
  count(item_counter)
FROM
  operations_mx.out_procurement_tracking;
