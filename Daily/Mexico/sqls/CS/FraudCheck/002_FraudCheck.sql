#1 B 001 D Create Table  MC  OK
DROP TABLE IF EXISTS customer_service.tbl_fraud_check_report_sample;
CREATE TABLE customer_service.tbl_fraud_check_report_sample LIKE customer_service.tbl_fraud_check_report;

#2 B 101 A +tbl-sales_order  MC  ok
INSERT INTO customer_service.tbl_fraud_check_report_sample (
	id_sales_order,
	OrderNumber,
	DateOrderPlaced,
	DayOrderPlaced,
	MonthOrderPlaced,
	YearOrderPlaced,
	WeekOrderPlaced,
	PaymentMethod,
	GrandTotal
) SELECT
	id_sales_order,
	order_nr,
	created_at,
	DATE_FORMAT(created_at,'%Y-%m-%d'),
	MONTH(created_at),
	YEAR(created_at),
	operations_mx.week_iso (created_at),
	payment_method,
	grand_total
FROM
	bob_live_mx.sales_order
WHERE bob_live_mx.sales_order.payment_method IN ("Banorte_Payworks_Debit",
"Banorte_Payworks",
"Amex_Gateway",
"Adquira_Interredes",
"Adyen_HostedPaymentPage",
"DineroMail_HostedPaymentPage",
"DineroMail_Api")
AND DATEDIFF(curdate(),sales_order.created_at) <= 90
;

UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN bob_live_mx.sales_order_item b
ON a.id_sales_order = b.fk_sales_order
SET a.isElectronic_good = 1
WHERE b.fk_catalog_electronic_good_type is not NULL
;

#5 B 203 U AccertifyScore  MC  unable / AccertifyRecommendation
UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN  bob_live_mx.sales_order_fraudcheck_log b
ON a.id_sales_order = b.fk_sales_order
SET 
	a.AccertifyScore = ExtractValue (b.message,'/transaction-results/total-score'),
	a.AccertifyRecommendation = ExtractValue (b.message,'/transaction-results/recommendation-code'),
	a.isScored = 1
WHERE	title = 'accertify response';


#####S0 B 209 Status binaries

# PaymentConfirmationPending 
DROP TEMPORARY TABLE IF EXISTS customer_service.tmp_PaymentConfirmationPending;
CREATE TEMPORARY TABLE customer_service.tmp_PaymentConfirmationPending (INDEX(fk_sales_order))
SELECT
	a.fk_sales_order,
	b.fk_sales_order_item_status
FROM	bob_live_mx.sales_order_item a 
INNER JOIN bob_live_mx.sales_order_item_status_history b 
	ON a.id_sales_order_item = b.fk_sales_order_item
WHERE b.fk_sales_order_item_status = 23
;

UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN customer_service.tmp_PaymentConfirmationPending b
	ON a.id_sales_order = b.fk_sales_order
SET a.PaymentConfirmationPending = 1;

DROP TEMPORARY TABLE IF EXISTS customer_service.tmp_payment_pending_orders;
CREATE TEMPORARY TABLE customer_service.tmp_payment_pending_orders (INDEX(fk_sales_order))
SELECT
 fk_sales_order
FROM bob_live_mx.payment_sales_amex
GROUP BY fk_sales_order
UNION
SELECT
 fk_sales_order
FROM bob_live_mx.payment_sales_banortepayworks
GROUP BY fk_sales_order
UNION
SELECT
 fk_sales_order
FROM bob_live_mx.payment_sales_dineromail
GROUP BY fk_sales_order;

UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN customer_service.tmp_payment_pending_orders b
	ON a.id_sales_order = b.fk_sales_order
SET a.PaymentConfirmationPending = 1;

# clarify_payment_pending_timeout 
DROP TEMPORARY TABLE IF EXISTS customer_service.tmp_clarify_payment_pending_timeout;
CREATE TEMPORARY TABLE customer_service.tmp_clarify_payment_pending_timeout (INDEX(fk_sales_order))
SELECT
	a.fk_sales_order,
	b.fk_sales_order_item_status
FROM	bob_live_mx.sales_order_item a 
INNER JOIN bob_live_mx.sales_order_item_status_history b 
	ON a.id_sales_order_item = b.fk_sales_order_item
WHERE b.fk_sales_order_item_status = 39
;

UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN customer_service.tmp_clarify_payment_pending_timeout b
	ON a.id_sales_order = b.fk_sales_order
SET a.clarify_payment_pending_timeout = 1;

# Invalid 
DROP TEMPORARY TABLE IF EXISTS customer_service.tmp_Invalid;
CREATE TEMPORARY TABLE customer_service.tmp_Invalid (INDEX(fk_sales_order))
SELECT
	a.fk_sales_order,
	b.fk_sales_order_item_status
FROM	bob_live_mx.sales_order_item a 
INNER JOIN bob_live_mx.sales_order_item_status_history b 
	ON a.id_sales_order_item = b.fk_sales_order_item
WHERE b.fk_sales_order_item_status = 10
;

UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN customer_service.tmp_Invalid b
	ON a.id_sales_order = b.fk_sales_order
SET a.Invalid = 1;


# AutoFraudCheckPending 
DROP TEMPORARY TABLE IF EXISTS customer_service.tmp_AutoFraudCheckPending;
CREATE TEMPORARY TABLE customer_service.tmp_AutoFraudCheckPending (INDEX(fk_sales_order))
SELECT
	a.fk_sales_order,
	b.fk_sales_order_item_status
FROM	bob_live_mx.sales_order_item a 
INNER JOIN bob_live_mx.sales_order_item_status_history b 
	ON a.id_sales_order_item = b.fk_sales_order_item
WHERE b.fk_sales_order_item_status = 88
;

UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN customer_service.tmp_AutoFraudCheckPending b
	ON a.id_sales_order = b.fk_sales_order
SET a.AutoFraudCheckPending = 1;

# ClarifyAutoFraudCheckError 
DROP TEMPORARY TABLE IF EXISTS customer_service.tmp_ClarifyAutoFraudCheckError;
CREATE TEMPORARY TABLE customer_service.tmp_ClarifyAutoFraudCheckError (INDEX(fk_sales_order))
SELECT
	a.fk_sales_order,
	b.fk_sales_order_item_status
FROM	bob_live_mx.sales_order_item a 
INNER JOIN bob_live_mx.sales_order_item_status_history b 
	ON a.id_sales_order_item = b.fk_sales_order_item
WHERE b.fk_sales_order_item_status = 89
;

UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN customer_service.tmp_ClarifyAutoFraudCheckError b
	ON a.id_sales_order = b.fk_sales_order
SET a.ClarifyAutoFraudCheckError = 1;

# PaymentPending 
DROP TEMPORARY TABLE IF EXISTS customer_service.tmp_PaymentPending;
CREATE TEMPORARY TABLE customer_service.tmp_PaymentPending (INDEX(fk_sales_order))
SELECT
	a.fk_sales_order,
	b.fk_sales_order_item_status
FROM	bob_live_mx.sales_order_item a 
INNER JOIN bob_live_mx.sales_order_item_status_history b 
	ON a.id_sales_order_item = b.fk_sales_order_item
WHERE b.fk_sales_order_item_status = 2
;

UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN customer_service.tmp_PaymentPending b
	ON a.id_sales_order = b.fk_sales_order
SET a.PaymentPending = 1;


# ClarifyPaymentError 
DROP TEMPORARY TABLE IF EXISTS customer_service.tmp_ClarifyPaymentError;
CREATE TEMPORARY TABLE customer_service.tmp_ClarifyPaymentError (INDEX(fk_sales_order))
SELECT
	a.fk_sales_order,
	b.fk_sales_order_item_status
FROM	bob_live_mx.sales_order_item a 
INNER JOIN bob_live_mx.sales_order_item_status_history b 
	ON a.id_sales_order_item = b.fk_sales_order_item
WHERE b.fk_sales_order_item_status = 15
;

UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN customer_service.tmp_ClarifyPaymentError b
	ON a.id_sales_order = b.fk_sales_order
SET a.ClarifyPaymentError = 1;

# ManualFraudCheckPending 
DROP TEMPORARY TABLE IF EXISTS customer_service.tmp_ManualFraudCheckPending;
CREATE TEMPORARY TABLE customer_service.tmp_ManualFraudCheckPending (INDEX(fk_sales_order))
SELECT
	a.fk_sales_order,
	b.fk_sales_order_item_status
FROM	bob_live_mx.sales_order_item a 
INNER JOIN bob_live_mx.sales_order_item_status_history b 
	ON a.id_sales_order_item = b.fk_sales_order_item
WHERE b.fk_sales_order_item_status = 90
;

UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN customer_service.tmp_ManualFraudCheckPending b
	ON a.id_sales_order = b.fk_sales_order
SET a.ManualFraudCheckPending = 1;

# ClarifyManualFraudCheck 
DROP TEMPORARY TABLE IF EXISTS customer_service.tmp_ClarifyManualFraudCheck;
CREATE TEMPORARY TABLE customer_service.tmp_ClarifyManualFraudCheck (INDEX(fk_sales_order))
SELECT
	a.fk_sales_order,
	b.fk_sales_order_item_status
FROM	bob_live_mx.sales_order_item a 
INNER JOIN bob_live_mx.sales_order_item_status_history b 
	ON a.id_sales_order_item = b.fk_sales_order_item
WHERE b.fk_sales_order_item_status = 91
;

UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN customer_service.tmp_ClarifyManualFraudCheck b
	ON a.id_sales_order = b.fk_sales_order
SET a.ClarifyManualFraudCheck = 1;

# RefundNeeded
DROP TEMPORARY TABLE IF EXISTS customer_service.tmp_RefundNeeded;
CREATE TEMPORARY TABLE customer_service.tmp_RefundNeeded (INDEX(fk_sales_order))
SELECT
	a.fk_sales_order,
	b.fk_sales_order_item_status
FROM	bob_live_mx.sales_order_item a 
INNER JOIN bob_live_mx.sales_order_item_status_history b 
	ON a.id_sales_order_item = b.fk_sales_order_item
WHERE b.fk_sales_order_item_status = 87
;

UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN customer_service.tmp_RefundNeeded b
	ON a.id_sales_order = b.fk_sales_order
SET a.RefundNeeded = 1;

# Exportable 
DROP TEMPORARY TABLE IF EXISTS customer_service.tmp_Exportable;
CREATE TEMPORARY TABLE customer_service.tmp_Exportable (INDEX(fk_sales_order))
SELECT
	a.fk_sales_order,
	b.fk_sales_order_item_status
FROM	bob_live_mx.sales_order_item a 
INNER JOIN bob_live_mx.sales_order_item_status_history b 
	ON a.id_sales_order_item = b.fk_sales_order_item
WHERE b.fk_sales_order_item_status = 3
;

UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN customer_service.tmp_Exportable b
	ON a.id_sales_order = b.fk_sales_order
SET a.Exportable = 1;

# Canceled 
DROP TEMPORARY TABLE IF EXISTS customer_service.tmp_Canceled;
CREATE TEMPORARY TABLE customer_service.tmp_Canceled (INDEX(fk_sales_order))
SELECT
	a.fk_sales_order,
	b.fk_sales_order_item_status
FROM	bob_live_mx.sales_order_item a 
INNER JOIN bob_live_mx.sales_order_item_status_history b 
	ON a.id_sales_order_item = b.fk_sales_order_item
WHERE b.fk_sales_order_item_status = 9
;

UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN customer_service.tmp_Canceled b
	ON a.id_sales_order = b.fk_sales_order
SET a.Canceled = 1;

# ManualRefundNeeded 
DROP TEMPORARY TABLE IF EXISTS customer_service.tmp_ManualRefundNeeded;
CREATE TEMPORARY TABLE customer_service.tmp_ManualRefundNeeded (INDEX(fk_sales_order))
SELECT
	a.fk_sales_order,
	b.fk_sales_order_item_status
FROM	bob_live_mx.sales_order_item a 
INNER JOIN bob_live_mx.sales_order_item_status_history b 
	ON a.id_sales_order_item = b.fk_sales_order_item
WHERE b.fk_sales_order_item_status = 53
;

UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN customer_service.tmp_ManualRefundNeeded b
	ON a.id_sales_order = b.fk_sales_order
SET a.ManualRefundNeeded = 1;

# FraudCheckPending 
DROP TEMPORARY TABLE IF EXISTS customer_service.tmp_FraudCheckPending;
CREATE TEMPORARY TABLE customer_service.tmp_FraudCheckPending (INDEX(fk_sales_order))
SELECT
	a.fk_sales_order,
	b.fk_sales_order_item_status
FROM	bob_live_mx.sales_order_item a 
INNER JOIN bob_live_mx.sales_order_item_status_history b 
	ON a.id_sales_order_item = b.fk_sales_order_item
WHERE b.fk_sales_order_item_status = 55
;

UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN customer_service.tmp_FraudCheckPending b
	ON a.id_sales_order = b.fk_sales_order
SET a.FraudCheckPending = 1;



#17 B 218 U isAmexReviewed MC  ok
DROP TEMPORARY TABLE IF EXISTS tmp_payment_sales_amex;
CREATE TEMPORARY TABLE tmp_payment_sales_amex(fk_sales_order FLOAT , KEY(fk_sales_order));
INSERT INTO tmp_payment_sales_amex
SELECT 
	fk_sales_order 
FROM bob_live_mx.payment_sales_amex;

UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN tmp_payment_sales_amex b
ON a.id_sales_order = b.fk_sales_order
SET a.isAmexReviewed = 1;

#18 B 219 U isBanorteReviewed  MC
DROP TABLE IF EXISTS tmp_payment_sales_banortepayworks;
CREATE TEMPORARY TABLE tmp_payment_sales_banortepayworks(fk_sales_order FLOAT , KEY(fk_sales_order));
INSERT INTO tmp_payment_sales_banortepayworks
SELECT 
	fk_sales_order 
FROM bob_live_mx.payment_sales_banortepayworks;

UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN tmp_payment_sales_banortepayworks  b
	ON a.id_sales_order = b.fk_sales_order
SET a.isBanorteReviewed = 1;

# 19 B 220 U isDineromailReviewed  MC
DROP TEMPORARY TABLE IF EXISTS customer_service.tmp_payment_sales_dineromail;
CREATE TEMPORARY TABLE customer_service.tmp_payment_sales_dineromail (INDEX(fk_sales_order))
SELECT
 *
FROM bob_live_mx.payment_sales_dineromail;

UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN customer_service.tmp_payment_sales_dineromail b 
ON a.id_sales_order = b.fk_sales_order
SET a.isDineromailReviewed = 1;

#20 B 221 U isBankReviewed  MC
UPDATE customer_service.tbl_fraud_check_report_sample
SET isBankReviewed = 1
WHERE isAmexReviewed = 1
OR isBanorteReviewed = 1
OR isDineromailReviewed = 1
;


#T0 B 222 U RejectNewInvalid MC   ok 0  <<
UPDATE customer_service.tbl_fraud_check_report_sample
SET NewError = 1
WHERE PaymentPending=0
AND 	Exportable=0
AND 	Canceled=0
AND 	Invalid=0
AND 	ClarifyPaymentError=0
AND 	ManualRefundNeeded=0
AND 	FraudCheckPending=0
AND 	RefundNeeded=0
AND 	PaymentConfirmationPending=0;

#T1 B 222 U RejectNewInvalid MC   ok 0  <<
UPDATE customer_service.tbl_fraud_check_report_sample
SET RejectNewInvalid = 1
WHERE Invalid = 1
AND isScored = 0
AND isBankReviewed = 0;

#T2 PendingScore  <<
UPDATE customer_service.tbl_fraud_check_report_sample
SET PendingScore = 1
WHERE	IsScored = 1
AND 	Invalid = 0 
AND 	PaymentPending = 0 
AND 	RejectNewInvalid = 0
AND 	isBankReviewed = 0
;

#T3 B 223 U RejectAuto MC  <<
UPDATE customer_service.tbl_fraud_check_report_sample
SET RejectAuto = 1
WHERE	isScored = 1 
AND 	Invalid = 1 
AND 	isBankReviewed=0;

#T4 B 225 U PendingBank MC  0<<
UPDATE customer_service.tbl_fraud_check_report
SET PendingBank = 1
WHERE	 (		PaymentPending = 1 OR PaymentConfirmationPending=1)
				AND Invalid = 0
				AND ManualFraudCheckPending = 0
				AND Exportable=0
				AND Canceled=0
				AND RefundNeeded = 0
;

#T9 B 224 U RejectBank MC
UPDATE customer_service.tbl_fraud_check_report_sample
SET RejectBank = 1
WHERE RejectNewInvalid = 0
AND 	RejectAuto = 0
AND 	PendingBank = 0
AND (			ManualFraudCheckPending = 0
			OR 	FraudCheckPending = 0)
AND (			Canceled = 1
			OR 	Invalid = 1)
AND 	Exportable=0 
;

#T5 B 226 U ApproveAuto  MC
UPDATE customer_service.tbl_fraud_check_report_sample
SET ApproveAuto = 1
WHERE	Exportable=1
AND 	ManualFraudCheckPending= 0
AND 	RejectNewInvalid = 0
AND 	RejectBank = 0
AND 	FraudCheckPending=0
;

#T6 B 229 U PendingManual  MC
UPDATE customer_service.tbl_fraud_check_report_sample
SET PendingManual = 1
WHERE	NewError = 0
AND 	RejectNewInvalid = 0
AND 	RejectAuto=0 
AND		RejectBank=0
AND 	PendingBank=0
AND 	ApproveAuto=0
AND 	Exportable=0
AND 	FraudCheckPending=1
AND 	ManualRefundNeeded=0
AND 	RefundNeeded=0
OR (	NewError = 0
	AND RejectNewInvalid = 0
	AND RejectAuto=0 
	AND	RejectBank=0
	AND PendingBank=0
	AND ApproveAuto=0
	AND Exportable=0
	AND ManualFraudCheckPending=1
	AND RefundNeeded=0)
;

#T7 B 228 U RejectManual  MC
UPDATE customer_service.tbl_fraud_check_report_sample
SET RejectManual = 1
WHERE	NewError = 0
AND 	RejectNewInvalid = 0
AND 	RejectAuto=0 
AND		RejectBank=0
AND 	PendingBank=0
AND 	ApproveAuto=0
AND 	Exportable=0
AND 	PendingManual=0
AND (			ManualRefundNeeded=1
			OR 	RefundNeeded=1)
;

#T8 B 227 U ApproveManual  MC
UPDATE customer_service.tbl_fraud_check_report_sample
SET ApproveManual = 1
WHERE	NewError = 0
AND 	RejectNewInvalid = 0
AND 	RejectAuto=0 
AND		RejectBank=0
AND 	PendingBank=0
AND 	ApproveAuto=0
AND 	PendingManual=0
AND 	RejectManual=0
OR (		Exportable=1
		AND FraudCheckPending=1)
;


#29 B 230 U Analyst  EC
UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN bob_live_mx.sales_order_item b
	ON a.id_sales_order = b.fk_sales_order
INNER JOIN bob_live_mx.sales_order_item_status_history c
	ON b.id_sales_order_item = c.fk_sales_order_item
INNER JOIN 	bob_live_mx.acl_user d
	ON c.fk_acl_user = 	d.id_acl_user
SET a.Analyst = If (d.username = "ignacio.diaz","Ignacio Diaz",
								If (d.username = "eva.gerardo","Eva Gerardo",
								If (d.username = "osiel.espinoza","Osiel Espinoza",
								If (d.username = "grimkarl11","Carlos Manrique",
										d.username))))
WHERE a.RejectManual = 1
AND a.ApproveManual = 1
AND c.fk_sales_order_item_status IN ( 53, 87, 3)
;

#30 B 231 U DateApproveAuto MC
UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN bob_live_mx.sales_order_item b
	ON a.id_sales_order = b.fk_sales_order
INNER JOIN bob_live_mx.sales_order_item_status_history c
	ON b.id_sales_order_item = c.fk_sales_order_item
SET a.DateApproveAuto = c.updated_at
WHERE c.fk_sales_order_item_status = 3
AND 	a.ApproveAuto = 1
AND 	a.ApproveManual = 0
;

#31 B 232 U DateApproveManual MC
UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN bob_live_mx.sales_order_item b
	ON a.id_sales_order = b.fk_sales_order
INNER JOIN bob_live_mx.sales_order_item_status_history c 
ON b.id_sales_order_item = c.fk_sales_order_item
SET 
	a.DateApproveManual = c.updated_at
WHERE c.fk_sales_order_item_status = 3
AND 	a.ApproveAuto = 0
AND 	a.ApproveManual = 1
;

#32 B 233 U DateRejectAuto MC  >>
UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN bob_live_mx.sales_order_item b
	ON a.id_sales_order = b.fk_sales_order
INNER JOIN bob_live_mx.sales_order_item_status_history c
	ON b.id_sales_order_item = c.fk_sales_order_item
SET a.DateRejectAuto = c.updated_at
WHERE a.RejectAuto = 1
AND 	c.fk_sales_order_item_status = 10
;

#33 B 234 U DateRejectManual MC
UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN bob_live_mx.sales_order_item b
	ON a.id_sales_order = b.fk_sales_order
INNER JOIN bob_live_mx.sales_order_item_status_history c
	ON b.id_sales_order_item = c.fk_sales_order_item
SET a.DateRejectManual = c.updated_at
WHERE a.RejectManual = 1
AND 	c.fk_sales_order_item_status = 53
OR 		c.fk_sales_order_item_status = 87
AND 	a.ApproveAuto = 0
AND 	a.ApproveManual = 0
;
#34 B 235 U TimeToApproveManual MC
UPDATE customer_service.tbl_fraud_check_report_sample 
SET TimeToApproveManual = timediff(DateApproveManual,DateOrderPlaced)
WHERE DateApproveManual IS NOT NULL
;
####################################################################
#35 B 236 U TimeApproveTotal  MC
UPDATE customer_service.tbl_fraud_check_report_sample
SET TimeToApproveTotal = CASE 
													WHEN DateApproveManual IS NOT NULL 
													THEN TIMEDIFF(DateApproveManual,DateOrderPlaced)
													WHEN DateApproveAuto IS NOT NULL
													THEN TIMEDIFF(DateApproveAuto,DateOrderPlaced)
													ELSE 
													NULL 
 												 END;

#36 B 237 U TimeToRejectManual  MC  OK
UPDATE customer_service.tbl_fraud_check_report_sample
SET TimeToRejectManual = TIMEDIFF(DateRejectManual,DateOrderPlaced)
WHERE DateRejectManual IS NOT NULL
;

#37 B 238 U TimeToRejectTotal  MC   ¿?
UPDATE customer_service.tbl_fraud_check_report_sample
SET TimeToRejectTotal = CASE 
													WHEN DateRejectManual IS NOT NULL 
													THEN timeDIFF(DateRejectManual,DateOrderPlaced)
													WHEN DateRejectAuto IS NOT NULL 
													THEN TIMEDIFF(DateRejectAuto,DateOrderPlaced) 
													ELSE NULL 
												END;


#38 B 239 U TimeToResolveManual  MC  ok
UPDATE customer_service.tbl_fraud_check_report_sample
SET TimeToResolveManual = IFNULL(TimeToApproveManual,0) 
												+ IFNULL(TimeToRejectManual,0)
WHERE TimeToApproveManual IS NOT NULL
OR TimeToRejectManual IS NOT NULL
;

#39 B 240 U TimeToResolveTotal  MC  OK
UPDATE customer_service.tbl_fraud_check_report_sample
SET TimeToResolveTotal = 	CASE  
														WHEN TimeToApproveTotal IS NOT NULL
														THEN TimeToApproveTotal  
														WHEN TimeToRejectTotal IS NOT NULL
														THEN TimeToRejectTotal
														ELSE NULL
													END;

#40 B 241 U TotalInvalidOrders MC
UPDATE customer_service.tbl_fraud_check_report_sample
SET TotalInvalidOrders = 1
WHERE RejectNewInvalid = 1
OR 		PendingBank = 1
OR 		RejectBank = 1
;

#41 B 242 U TotalAnalyzedOrders MC  OK
UPDATE customer_service.tbl_fraud_check_report_sample 
SET TotalAnalyzedOrders = 1
WHERE RejectAuto = 1
OR 		ApproveAuto = 1
OR 		ApproveManual = 1
OR 		RejectManual = 1
OR 		PendingManual = 1
;

DROP TEMPORARY TABLE IF EXISTS customer_service.tmp_creditcard;
CREATE TEMPORARY TABLE customer_service.tmp_creditcard (INDEX(order_nr,auth_code,date))
SELECT 
	c.order_nr,
	b.auth_code,
	DATE(b.created_at) AS date
FROM bob_live_mx.payment_sales_banortepayworks b
LEFT JOIN bob_live_mx.sales_order c
	ON b.fk_sales_order = c.id_sales_order;

DROP TABLE IF EXISTS customer_service.tbl_chargeback_orders;
CREATE TABLE customer_service.tbl_chargeback_orders (INDEX(order_nr))
SELECT
	b.order_nr,
	b.auth_code,
	b.date
FROM customer_service.chargeback a
LEFT JOIN customer_service.tmp_creditcard b
	ON a.`CODIGO DE AUTORIZACION`=b.auth_code 
	AND a.`FECHA VENTA`=b.date;

UPDATE customer_service.tbl_fraud_check_report_sample a
INNER JOIN customer_service.tbl_chargeback_orders b
	ON a.OrderNumber = b.order_nr
SET a.Chargeback = 1;


-- UPDATE customer_service.tbl_fraud_check_report_sample 
-- INNER JOIN 
-- (select order_nr FROM
-- 
-- (select * from customer_service.chargeback)a
-- left join 
-- (select * from bob_live_mx.payment_sales_banortepayworks)b
-- on a.`CODIGO DE AUTORIZACION`=b.auth_code and a.`FECHA VENTA`=date(b.created_at)
-- left join 
-- bob_live_mx.sales_order c
-- on b.fk_sales_order=c.id_sales_order 
-- )A ON customer_service.tbl_fraud_check_report_sample.OrderNumber=A.order_nr
-- SET customer_service.tbl_fraud_check_report_sample.Chargeback = 1;
-- 

-- -- -- ADJUSTMENTS -- -- -- 

UPDATE customer_service.tbl_fraud_check_report_sample
SET 
	RejectBank=0,
	RejectNewInvalid=0,
	PendingBank=0,
	ApproveManual=0,
	ApproveAuto=1
WHERE OrderNumber=200002138;

UPDATE customer_service.tbl_fraud_check_report_sample
SET 
	RejectBank=1,
	PendingBank =0,
	RejectNewInvalid=0
WHERE OrderNumber 
IN(200001179,
200001579,
200002179,
200002659,
200006659,
200002229,
200002529,
200002749,
200002869,
200003129,
200003349,
200004769,
200005269,
200005929,
200007529,
200007829,
200002626,
200004346,
200004996,
200005626,
200006726,
200008626
);

DROP TABLE IF EXISTS tmp_payment_sales_banortepayworks;
create TEMPORARY table tmp_payment_sales_banortepayworks
(auth_code DOUBLE,created_at date,fk_sales_order FLOAT , KEY(fk_sales_order));

insert INTO tmp_payment_sales_banortepayworks
select auth_code,created_at,fk_sales_order from bob_live_mx.payment_sales_banortepayworks;

DROP TABLE IF EXISTS tmp_sales_order;
create TEMPORARY table tmp_sales_order
(id_sales_order DOUBLE,order_nr DOUBLE, KEY(id_sales_order));
insert INTO tmp_sales_order
select id_sales_order,order_nr from bob_live_mx.sales_order;



-- UPDATE customer_service.tbl_fraud_check_report_sample INNER JOIN 
-- 
-- (select order_nr FROM
-- 
-- (select * from customer_service.chargeback)a
-- left join 
-- 
-- (select * from tmp_payment_sales_banortepayworks)b
-- on a.`CODIGO DE AUTORIZACION`=b.auth_code and a.`FECHA VENTA`=date(b.created_at)
-- left join 
-- tmp_sales_order c
-- on b.fk_sales_order=c.id_sales_order 
-- )A ON customer_service.tbl_fraud_check_report_sample.OrderNumber=A.order_nr
-- SET customer_service.tbl_fraud_check_report_sample.Chargeback = 1
-- ;

REPLACE customer_service.tbl_fraud_check_report SELECT * FROM customer_service.tbl_fraud_check_report_sample;