INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  step,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Mexico',
  'FraudCheck',
  'finish',
  NOW(),
  now(),
  count(*),
  count(*)
FROM
  customer_service.tbl_fraud_check_report;


