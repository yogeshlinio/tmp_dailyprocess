use customer_service;

SELECT 'Truncate rep_llamadas_agt_regional';
TRUNCATE rep_llamadas_agt_regional;

SELECT 'Truncate rep_llamadas_agt_regional';
TRUNCATE rep_llamadas_agt_regional;
SELECT 'Insert rep_llamadas_agt_regional';
INSERT INTO rep_llamadas_agt_regional
(Fecha,
Franja,
Pais,
Agente,
Recibidas,
Atendidas,
Abandonadas,
Duracion,
Espera,
EsperaAgente,
Atendidas20,
Transferencia,
Satisfaccion,
Respuestas_Satisfaccion,
FCR,
Peticiones_FCR,
AbandonadasMenor5)

select 
    event_date As Fecha,
	Franja,
	'CO' as Pais,
	agent_name as Agente,
	sum(net_event) Recibidas,	
	sum(answered_in_wh) Atendidas,
	sum(unanswered_in_wh) Abandonadas,
	sum(call_duration_seg) Duracion,
    sum(hold_duration_seg) Espera,
	sum(agent_hold_time_seg) EsperaAgente,
    sum(answered_in_wh_20) Atendidas20,
    sum(transfered_to_queue) Transferencia,
	sum(good)+sum(regular) Satisfaccion,
	sum(answered_3) Respuestas_Satisfaccion,
	sum(if(first_contact=1 and resolution=1,1,0)) FCR,
	sum(answ_1_2) Peticiones_FCR,
	sum(unanswered_in_wh_cust) Abandonadas_menor5
from
    customer_service_co.bi_ops_cc_dyalogo
where
    pos = 1 and net_event = 1 
group by event_date,agent_name,Franja  

union ALL


SELECT
event_date as `Fecha`,
`Franja`,
'MX' as `Pais`,
agent_name as `Agente`,
SUM(net_event) as `Recibidas`,
SUM(answered_in_wh) as `Atendidas`,
SUM(unanswered_in_wh) as `Abandonadas`,
SUM(call_duration_seg) as `Duracion`,
SUM(hold_duration_seg) as `Espera`,
SUM(agent_hold_time_seg) as EsperaAgente,
SUM(answered_in_wh_20) as `Atendidas20`,
SUM(transfered_to_queue) as `Transferencia`,
SUM(good)+sum(regular) Satisfaccion,
SUM(answered_3) Respuestas_Satisfaccion,
SUM(if(first_contact=1 and resolution=1,1,0)) FCR,
SUM(answ_1_2) Peticiones_FCR,
SUM(if(hold_duration_seg<5,unanswered_in_wh,0)) as `Abandonadas_menor5`
FROM customer_service.bi_ops_cc_mx
WHERE pos=1 and net_event=1
GROUP BY event_date,agent_ext,Franja  


UNION ALL


SELECT
event_date as `Fecha`,
`Franja`,
'PE' as `Pais`,
agent_ext as `Agente`,
SUM(net_event) as `Recibidas`,
SUM(answered_in_wh) as `Atendidas`,
SUM(unanswered_in_wh) as `Abandonadas`,
SUM(call_duration_seg) as `Duracion`,
SUM(hold_duration_seg) as `Espera`,
SUM(agent_hold_time_seg) as EsperaAgente,
SUM(answered_in_wh_20) as `Atendidas20`,
SUM(transfered_to_queue) as `Transferencia`,
SUM(good)+sum(regular) Satisfaccion,
SUM(answered_3) Respuestas_Satisfaccion,
SUM(if(first_contact=1 and resolution=1,1,0)) FCR,
SUM(answ_1_2) Peticiones_FCR,
SUM(if(hold_duration_seg<5,unanswered_in_wh,0)) as `Abandonadas_menor5`
FROM customer_service_pe.bi_ops_cc_pe
WHERE pos=1 
GROUP BY event_date,agent_ext,Franja;


#Agregar datos llamadas no en horas no laborales

insert into rep_llamadas_agt_regional
(Fecha, Franja, AbandonadasNoLaborales, pais)

select a.date, b.Franja, a.calls, a.pais from
(select date, shift, sum(non_working_hour) calls, 'MX' pais from bi_ops_cc_non_operating_hours
where dcontext like '%linio%'
and non_working_hour=1
group by date, shift
union all
select date, shift, sum(non_working_hour) calls,  'CO' pais from customer_service_co.bi_ops_cc_non_operating_hours
where non_working_hour=1
group by date, shift
union all
select date, shift, sum(non_working_hour) calls,  'PE' pais from customer_service_pe.bi_ops_cc_non_operating_hours
where dcontext in 
('app-blackhole','app-announcement-2')
and non_working_hour=1
group by date, shift) a
inner join 
customer_service.tbl_48_franjas b
on a.shift=b.Id
;

SELECT 'Actualiza Semana';
UPDATE  rep_llamadas_agt_regional SET Semana=date_format(fecha,'%Y-%u');