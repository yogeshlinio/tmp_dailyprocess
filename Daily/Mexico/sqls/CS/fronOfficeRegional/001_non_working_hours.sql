#PERU

use customer_service_pe;

set @max_date=(select max(date) from bi_ops_cc_non_operating_hours);

delete from bi_ops_cc_non_operating_hours
where date>=@max_date;

#truncate table bi_ops_cc_non_operating_hours;

insert into bi_ops_cc_non_operating_hours
(date,
hour,
shift,
weekday,
calldate,
uniqueid,
src,
dst,
dcontext,
disposition,
duration,
calls)
select date(calldate) date, hour(calldate) hour, IF((date_format(calldate,'%i') < 30),concat(date_format(calldate,'%H'),'00'),
					concat(date_format(calldate,'%H'),'30')) shift, weekday(calldate) weekday,
calldate,uniqueid, src,dst,dcontext,disposition,duration,1 calls from customer_service_pe.cdr where 
date(calldate)>=@max_date;

update bi_ops_cc_non_operating_hours a 
inner join operations_pe.calendar b 
on a.date=b.dt
set holiday=isholiday
where date>=@max_date;


update bi_ops_cc_non_operating_hours
set
non_working_hour=
if((weekday=6 or holiday=1)
and hour not between 9 and 13,1,
if(weekday=5
and hour not between 9 and 17,1,
if(hour not between 8 and 22,1,0)))
where date>=@max_date;

delete from bi_ops_cc_non_operating_hours
where non_working_hour=0
and date>=@max_date;


update bi_ops_cc_non_operating_hours a
inner join bi_ops_cc_pe b on a.uniqueid=b.queue_stats_id
set
non_working_hour=0;

#M�XICO

use customer_service;

set @max_date=(select max(date) from bi_ops_cc_non_operating_hours);

delete from bi_ops_cc_non_operating_hours
where date>=@max_date;

#truncate table bi_ops_cc_non_operating_hours;

insert into bi_ops_cc_non_operating_hours
(date,
hour,
shift,
weekday,
calldate,
uniqueid,
src,
dst,
dcontext,
disposition,
duration,
calls)
select date(calldate) date, hour(calldate) hour, IF((date_format(calldate,'%i') < 30),concat(date_format(calldate,'%H'),'00'),
					concat(date_format(calldate,'%H'),'30')) shift, weekday(calldate) weekday,
calldate,uniqueid, src,dst,dcontext,disposition,duration,1 calls from customer_service.cdr where 
date(calldate)>=
@max_date;

update bi_ops_cc_non_operating_hours a 
inner join operations_mx.calendar b 
on a.date=b.dt
set holiday=isholiday
where date>=@max_date;


update bi_ops_cc_non_operating_hours
set
non_working_hour=
if((weekday=6 or holiday=1)
and hour not between 10 and 18,1,
if(weekday=5
and hour not between 9 and 19,1,
if(hour not between 8 and 22,1,0)))
where date>=@max_date;

delete from bi_ops_cc_non_operating_hours
where non_working_hour=0
and date>=@max_date;


update bi_ops_cc_non_operating_hours a
inner join bi_ops_cc_mx b on a.uniqueid=b.queue_stats_id
set
non_working_hour=0;

#COLOMBIA

use customer_service_co;

set @max_date=(select max(date) from bi_ops_cc_non_operating_hours);

delete from bi_ops_cc_non_operating_hours
where date>=@max_date;

#truncate table bi_ops_cc_non_operating_hours;

insert into bi_ops_cc_non_operating_hours
(date,
hour,
shift,
weekday,
calldate,
uniqueid,
src,
dst,
dcontext,
disposition,
duration,
calls)
SELECT date(fecha_hora) date, hour(fecha_hora), IF((date_format(fecha_hora,'%i') < 30),concat(date_format(fecha_hora,'%H'),'00'),
					concat(date_format(fecha_hora,'%H'),'30')) shift, weekday(fecha_hora) weekday,
fecha_hora,unique_id,numero_telefonico,sentido,tipo_llamada,resultado,duracion_total,1 calls FROM CS_dyalogo.dy_llamadas_espejo
where date(fecha_hora)>=@max_date
and sentido='Entrante'
;

update bi_ops_cc_non_operating_hours a 
inner join operations_co.calendar b 
on a.date=b.dt
set holiday=isholiday
where date>=@max_date;


update bi_ops_cc_non_operating_hours
set
non_working_hour=
if((weekday=6 or holiday=1)
and hour not between 10 and 17,1,
if(weekday=5
and hour not between 9 and 20,1,
if(hour not between 8 and 20,1,0)))
where date>=@max_date;


update bi_ops_cc_non_operating_hours
set
non_working_hour=0
where weekday <=4 and
shift='0730'
and holiday=0; 

delete from bi_ops_cc_non_operating_hours
where non_working_hour=0
and date>=@max_date;


update bi_ops_cc_non_operating_hours a
inner join bi_ops_cc_dyalogo b on a.uniqueid=b.uniqueid
set
non_working_hour=0;