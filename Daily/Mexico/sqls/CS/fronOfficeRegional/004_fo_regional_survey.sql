
SELECT 'Truncate rep_llamadas_agt_regional_survey';
TRUNCATE rep_llamadas_agt_regional_survey;
SELECT 'Insert rep_llamadas_agt_regional_survey';
insert into rep_llamadas_agt_regional_survey
(Fecha,
Pais,
Franja,
Agente,
Atendidas,
Answer1Option1,
Answered1,
Answer2Option1,
Answered2,
Answer3Option1,
Answer3Option2,Answered3)

select 
    event_date As Fecha,
	'CO' as Pais,
	Franja,
	agent_name as Agente,
	sum(answered_in_wh) Atendidas,
	sum(resolution) as Answer1Option1,
	sum(answered_1) as Answered1,
	sum(first_contact) as Answer2Option1,
	sum(answered_2) as Answered2,
	sum(good) Answer3Option1,
	sum(regular) Answer3Option2,
	sum(answered_3) Answered3
from
    customer_service_co.bi_ops_cc_dyalogo
where
    pos = 1 and answered_in_wh = 1 
group by event_date , event_shift,agent_name

union ALL

select 
    event_date As Fecha,
	'MX' as Pais,
	Franja,
	agent_name as Agente,
	sum(answered_in_wh) Atendidas,
	sum(resolution) as Answer1Option1,
	sum(answered_1) as Answered1,
	sum(first_contact) as Answer2Option1,
	sum(answered_2) as Answered2,
	sum(good) Answer3Option1,
	sum(regular) Answer3Option2,
	sum(answered_3) Answered3
from
    customer_service.bi_ops_cc_mx
where
    pos = 1 and answered_in_wh = 1 
GROUP BY event_date,agent_ext,Franja  

UNION ALL


select 
    event_date As Fecha,
	'PE' as Pais,
	Franja,
	agent_name as Agente,
	sum(answered_in_wh) Atendidas,
	sum(resolution) as Answer1Option1,
	sum(answered_1) as Answered1,
	sum(first_contact) as Answer2Option1,
	sum(answered_2) as Answered2,
	sum(good) Answer3Option1,
	sum(regular) Answer3Option2,
	sum(answered_3) Answered3
from
    customer_service_pe.bi_ops_cc_pe
where
    pos = 1 and answered_in_wh = 1 
GROUP BY event_date,agent_ext,Franja
;

SELECT 'Actualiza Datos faltantes';
update rep_llamadas_agt_regional_survey
set
Answer1Option2=Answered1-Answer1Option1,
Answer2Option2=Answered2-Answer2Option1,
Answer3Option3=Answered3-Answer3Option1-Answer3Option2;

SELECT 'Actualiza Semana';
UPDATE  rep_llamadas_agt_regional_survey SET Semana=date_format(fecha,'%Y-%u');
