INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  step,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Regional',
  'CC_Rep_Front_Office',
  'start',
  NOW(),
  max(Fecha),
  '0',
  '0'
FROM
  customer_service.rep_llamadas_agt_regional

