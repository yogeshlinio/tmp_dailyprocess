INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Peru', 
  'bi_ops_cc_pe',
  'finish',
  NOW(),
  MAX(event_date),
  count(*),
  count(*)
FROM
  customer_service_pe.bi_ops_cc_pe;