use customer_service_pe;

set time_zone='-5:00';
	SET @MAXDATE =(SELECT MAX(event_date) FROM customer_service_pe.bi_ops_cc_pe);

	DELETE FROM customer_service_pe.bi_ops_cc_pe WHERE event_date >=@MAXDATE;


#TRUNCATE TABLE bi_ops_cc_pe;
INSERT INTO bi_ops_cc_pe (queue_stats_id,agent)
SELECT	
	callid,agent as queue_stats_id
FROM
(SELECT DISTINCT callid, agent FROM queuelog
WHERE FROM_UNIXTIME(timestamp,'%Y-%m-%d')>=@MAXDATE AND action in 
('CONNECT','ABANDON'))a;

UPDATE bi_ops_cc_pe a
INNER JOIN
(SELECT * from queuelog WHERE action IN 
('COMPLETEAGENT','COMPLETECALLER') AND FROM_UNIXTIME(timestamp,'%Y-%m-%d')>=@MAXDATE )b
ON a.queue_stats_id=b.callid
SET call_duration_seg=info2,
agent_ext=b.agent,answered_in_wh=1
;

UPDATE bi_ops_cc_pe a
INNER JOIN
(SELECT * from queuelog WHERE action IN ('ENTERQUEUE') 
AND FROM_UNIXTIME(timestamp,'%Y-%m-%d')>=@MAXDATE )b
ON a.queue_stats_id=b.callid
SET number=info2,
event_date=FROM_UNIXTIME(timestamp,'%Y-%m-%d'),
event_datetime=FROM_UNIXTIME(timestamp,'%Y-%m-%d %H:%i:%s'),
a.queue=qname
#CASE 1 WHEN
#THEN 'linio Post/Pre' WHEN b.queue='54646' THEN 'Pre-Venta' WHEN b.queue='54646-2'
#THEN 
#'Post-Venta' WHEN b.queue='43556' THEN 'HelloFood' END
;

UPDATE customer_service_pe.bi_ops_cc_pe  a
INNER JOIN
(
SELECT * from queuelog WHERE action IN ('CONNECT') AND FROM_UNIXTIME(timestamp,'%Y-%m-%d')>=@MAXDATE 
)b
ON      a.queue_stats_id=b.callid
SET     hold_duration_seg=info1,agent_hold_time_seg
=info3
;


UPDATE bi_ops_cc_pe a
INNER JOIN
(SELECT * from queuelog WHERE action IN ('ABANDON')
AND FROM_UNIXTIME(timestamp,'%Y-%m-%d')>=@MAXDATE )b
ON      a.queue_stats_id=b.callid
SET     hold_duration_seg=info3,unanswered_in_wh=1
;


UPDATE bi_ops_cc_pe a
INNER JOIN
(SELECT * from queuelog WHERE action IN ('TRANSFER') 
AND FROM_UNIXTIME(timestamp,'%Y-%m-%d')>=@MAXDATE )b
ON  a.queue_stats_id=b.callid
SET call_duration_seg=info4,
transfered_to_queue=1,answered_in_wh=1,
agent_ext=b.agent
;

UPDATE bi_ops_cc_pe
SET 
	event_hour=HOUR(event_datetime),
	event_minute=MINUTE(event_datetime),
	event_shift=IF(MINUTE(event_datetime)<30,
	concat(date_format(event_datetime,'%H'),'00'),
	concat(date_format(event_datetime,'%H'),'30')),
	event_weekday=WEEKDAY(event_datetime),
	event_week=WEEK(event_datetime,0),
	event_month=MONTH(event_datetime),
	event_year=YEAR(event_datetime),
	pre=if(queue=1001,1,0),
	pre_answ=if(queue=1001 AND answered_in_wh=1,1,0),
	pos=if(queue=1002,1,0),
	pos_answ=if(answered_in_wh=1 AND queue=1002 ,1,0),
	net_event=1,
	answered_in_wh_20=if(answered_in_wh=1 AND hold_duration_seg<20,1,0),
	unanswered_in_wh_cust=if(unanswered_in_wh=1 AND hold_duration_seg<5,1,0)
where event_date>=@MAXDATE
	;


UPDATE
customer_service_pe.bi_ops_cc_pe a
left join
customer_service.tbl_48_franjas b
on a.event_shift=b.Id
 set a.Franja=b.Franja
#where a.Franja is not null
;

#Agregar nombre de los agentes
update
customer_service_pe.bi_ops_cc_pe a 
inner join tbl_staff_CC_PE b on right(a.agent,3)=b.Extension
set
agent_name=Nombre
where event_date>=@MAXDATE;

UPDATE 
customer_service_pe.bi_ops_cc_pe a
INNER JOIN 
(
SELECT
	uniqueid,
	DATE(TIMESTAMP)Date,
	`status`,
	id_quest_1,
	sel_opt_1,
	id_quest_2,
	sel_opt_2,
	id_quest_3,
	sel_opt_3
FROM
	customer_service_pe.reports
WHERE
DATE(TIMESTAMP)>='2014-04-01'#@MAXDATE
)b
ON a.uniqueid=b.uniqueid
SET transfered_survey=1,
		answ_1_2=if(id_quest_1=1 and id_quest_2=2,1,0),
		resolution=if(status='COMPLETADO',sel_opt_1,0),
		first_contact=if(status='COMPLETADO',sel_opt_2,0),
		answered_3=if(id_quest_3=3,1,0),
		good=IF(sel_opt_3=1,1,0),
		regular=IF(sel_opt_3=2,1,0)
		;