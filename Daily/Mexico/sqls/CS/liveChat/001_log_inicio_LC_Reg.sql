INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  step,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Regional',
  'CC_LiveChat_Ajustado',
  'start',
  NOW(),
  max(Date),
  '0',
  '0'
FROM
  customer_service_co.tbl_bi_ops_cc_chats_performance

