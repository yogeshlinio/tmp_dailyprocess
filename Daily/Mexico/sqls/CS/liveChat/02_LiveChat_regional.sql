USE customer_service_co;

SELECT  'Inicio rutina de chats',now();

#Zona horaria de Colombia

set time_zone='-5:00';

set @last_day=(SELECT max(date) FROM tbl_bi_ops_cc_chats_performance);

SELECT  'Insertar datos a la tabla de chats',now();

insert into tbl_bi_ops_cc_chats_performance (date,chats)
SELECT date, sum(invitations) FROM customer_service.tbl_invitations_live_chat
where date>@last_day
group by date;


/*SELECT  'Insertar tiempo promedio',now();

update tbl_bi_ops_cc_chats_performance a
inner join tbl_performance_live_chat b
on a.date=b.date
set act=time
where mail='Total Average'
and date>@last_day;*/


SELECT  'Insertar datos del performance de visitantes',now();
update tbl_bi_ops_cc_chats_performance a
inner join customer_service.tbl_queue_live_chat b
on a.date=b.date
set 
wait_in_queue=waiting_visitors,
proceed_to_chat=attended_visitors,
abandon_queue=abandon
where a.date>@last_day;

SELECT  'Insertar datos de tiempos de espera del visitante',now();
update tbl_bi_ops_cc_chats_performance a
inner join customer_service.tbl_timebefore_proceed b
on a.date=b.date
set 
waiting_time_max=mwt_before_proceed,
waiting_time_avg=awt_before_proceed,
waiting_time_min=iwt_before_proceed
where a.date>@last_day;

SELECT  'Insertar datos de tiempos de espera antes de abandono',now();
update tbl_bi_ops_cc_chats_performance a
inner join customer_service.tbl_timebefore_abandonment b
on a.date=b.date
set 
time_to_abandon_max=mwt_before_abandonment,
time_to_abandon_avg=awt_before_abandonment,
time_to_abandon_min=iwt_before_abandonment
where a.date>@last_day;

SELECT  'Insertar datos de satisfacción',now();
update tbl_bi_ops_cc_chats_performance a
inner join (select date, sum(chats_rated) chats_rated, sum(rated_good) rated_good,
sum(rated_bad) rated_bad from customer_service.tbl_rating_live_chat where name<>'Total Average' group by date) b
on a.date=b.date
set 
a.chats_rated=b.chats_rated,
a.rated_good=b.rated_good,
a.rated_bad=b.rated_bad
where 
a.date>@last_day;

update tbl_bi_ops_cc_chats_performance
set
queued=chats+abandon_queue;

SELECT  'Insertar datos por agente',now();

insert into customer_service_co.tbl_bi_ops_cc_chats_performance_agents
(date,operator_name, chats, total_chatting_sec,
chatting_sec, available_sec, away_sec )
SELECT * FROM customer_service.tbl_availability_live_chat
where date>@last_day
and number_of_chats<>'-0';

SELECT  'Insertar act por agente',now();

update customer_service_co.tbl_bi_ops_cc_chats_performance_agents
set
act_sec=total_chatting_sec/chats 
where act_sec is null;

#Actualizar calificaciones por agente
update
customer_service_co.tbl_bi_ops_cc_chats_performance_agents a
inner join customer_service.tbl_rating_live_chat b
on a.date=b.date and a.operator_name=b.name
set 
a.rated=b.chats_rated,
a.rated_good=b.rated_good,
a.rated_bad=b.rated_bad
where rated is null;

#Actualizar act general (Valor aproximado)
update
customer_service_co.tbl_bi_ops_cc_chats_performance a
inner join (SELECT date, sum(chats*act_sec)/sum(chats) act 
FROM tbl_bi_ops_cc_chats_performance_agents
group by date) b
on a.date=b.date
set a.act_sec=b.act
where a.act_sec is null;

#Agregar tiempo promedio de espera
update
customer_service_co.tbl_bi_ops_cc_chats_performance 
set waiting_time_sec=(time_to_sec(waiting_time_avg)*chats+time_to_sec(time_to_abandon_avg)*abandon_queue)/(chats+abandon_queue)
where waiting_time_sec  is null;

#Agregar nombre de los agentes
update tbl_bi_ops_cc_chats_performance_agents a 
inner join bi_ops_matrix_sac b
on a.operator_name=b.user_chat
set 
name_matrix=name,
agent_identification=identification
where name_matrix is null;


SELECT  'Fin rutina de chats',now();
