INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  step,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Regional',
  'CC_2R_routines',
  'finish',
  NOW(),
  max(Fecha),
  count(*),
  count(Fecha)
FROM
  customer_service.rep_telesales_agt_regional;