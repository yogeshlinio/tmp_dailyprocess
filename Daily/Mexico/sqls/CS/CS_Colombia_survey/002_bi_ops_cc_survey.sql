SELECT  'Inicio rutina CC Survey',now();

use customer_service_co;

#Horario Colombia

set time_zone='-5:00';

#Datos ultimo d�a
set @max_day=cast((select max(call_date) from bi_ops_cc_survey_dyalogo_test) as date);

delete from bi_ops_cc_survey_dyalogo_test
where call_date>=@max_day;

#truncate table bi_ops_cc_survey;

SELECT  'Insertar datos a la tabla inicial',now();
insert into bi_ops_cc_survey_dyalogo_test(uniqueid,call_datetime,call_date,call_time,agent_name,agent_ext) 
select unique_id,fecha_hora,date(fecha_hora), time(fecha_hora),
replace(left(right(nombre_agente,length(nombre_agente)-instr(nombre_agente,'|')),instr(right(nombre_agente,length(nombre_agente)-instr(nombre_agente,'|')),'|')-1),'_',' '),
right(nombre_agente,if(instr(right(nombre_agente,length(nombre_agente)-instr(nombre_agente,'|')),'|')=0,null,length(right(nombre_agente,length(nombre_agente)-instr(nombre_agente,'|'))) -instr(right(nombre_agente,length(nombre_agente)-instr(nombre_agente,'|')),'|')))
 from CS_dyalogo.dy_encuestas_resultados
where campana <>""
and date(fecha_hora)>=@max_day
group by unique_id;

SELECT  'Agregar informaci�n de los asesores', now();
update bi_ops_cc_survey_dyalogo_test a
inner join bi_ops_cc_dyalogo b 
on a.uniqueid=b.uniqueid
set
a.agent_identification=b.agent_identification,
a.callerid=b.number,
a.queue=b.queue
where transfered_to_queue<>1;

SELECT  'Pregunta 1', now();
update bi_ops_cc_survey_dyalogo_test a 
inner join CS_dyalogo.dy_encuestas_resultados b
on a.uniqueid=b.unique_id
set
answer_1=if(respuesta in(1,2),1,0),
answer1_option_1=if(respuesta=1,1,0),
answer1_option_2=if(respuesta=2,1,0)
where id_pregunta=1;

SELECT  'Pregunta 2', now();
update bi_ops_cc_survey_dyalogo_test a 
inner join CS_dyalogo.dy_encuestas_resultados b
on a.uniqueid=b.unique_id
set
answer_2=if(respuesta in(1,2),1,0),
answer2_option_1=if(respuesta=1,1,0),
answer2_option_2=if(respuesta=2,1,0)
where id_pregunta=2;

SELECT  'Pregunta 3', now();
update bi_ops_cc_survey_dyalogo_test a 
inner join CS_dyalogo.dy_encuestas_resultados b
on a.uniqueid=b.unique_id
set
answer_3=if(respuesta in(1,2,3),1,0),
answer3_option_1=if(respuesta=1,1,0),
answer3_option_2=if(respuesta=2,1,0),
answer3_option_3=if(respuesta=3,1,0)
where id_pregunta=3;

SELECT  'Actualizar respuestas', now();
update bi_ops_cc_survey_dyalogo_test
set
respuestas=answer_1+answer_2+answer_3;

#actualizar respuestas
update bi_ops_cc_survey_dyalogo_test
set 
respuestas=0
where respuestas is null ;

update bi_ops_cc_survey_dyalogo_test
set 
abandon_in=1
where respuestas=0 ;

SELECT  'Abandonados', now();
update bi_ops_cc_survey_dyalogo_test
set 
abandon_in=if(respuestas=0,1,0),
answer1_abandon=if(answer_1=0,1,0),
answer2_abandon=if(answer_2=0,1,0),
answer3_abandon=if(answer_3=0,1,0);

SELECT  'Ingresar datos en la tabla principal', now();

#Cambio de la encuesta de satisfacci�n/First contact resolution
update bi_ops_cc_dyalogo a inner join bi_ops_cc_survey_dyalogo_test b
on a.uniqueid=b.uniqueid
set
a.transfered_survey=1,
a.answered_3=answer_3,
a.good=answer3_option_1,
a.regular=answer3_option_2,
a.answ_1_2=if(answer_1=1 and answer_2=1,1,0),
a.first_contact=answer2_option_1,
a.resolution=answer1_option_1,
a.answered_1=answer_1,
a.answered_2=answer_2
where a.transfered_survey =0;

SELECT  'Rutina finalizada Survey',now();