INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'customer_service_co.bi_ops_cc_survey',
  'fin',
  NOW(),
  MAX(call_datetime),
  count(*),
  count(*)
FROM
  customer_service_co.bi_ops_cc_survey;
