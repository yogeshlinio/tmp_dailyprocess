use customer_service_co;
#####Historio de 90 dias Colombia para llamadas posventa desfase de 2 semanas
########################################################


DROP TEMPORARY TABLE IF EXISTS tmp_amaster;
CREATE TEMPORARY TABLE tmp_amaster (key(date))
SELECT DATE_ADD(Date, interval 14 DAY )Date,COUNT(DISTINCT OrderNum)Ordenes
FROM development_co_project.A_Master
WHERE Date>='2013-10-01' AND OrderBeforeCan=1
GROUP BY date;

DROP TEMPORARY TABLE IF EXISTS tmp_llamadas;
CREATE TEMPORARY TABLE tmp_llamadas (key(fecha))
SELECT event_date as Fecha,COUNT(1)Llamadas
FROM customer_service_co.bi_ops_cc
WHERE event_date>='2013-10-01' AND net_event=1 AND pos=1
GROUP BY Fecha

UNION ALL

SELECT event_date as Fecha,COUNT(1)Llamadas
FROM customer_service_co.bi_ops_cc_dyalogo
WHERE event_date>='2013-10-01' AND net_event=1 AND pos=1
GROUP BY Fecha
;

DROP TABLE IF EXISTS DM_CS_hist_90_2weeks_co;
CREATE TABLE DM_CS_hist_90_2weeks_co 
SELECT date,llamadas,ordenes
 FROM 
tmp_amaster a
LEFT JOIN
tmp_llamadas b
ON a.Date=b.Fecha 
WHERE Date>=DATE_SUB(CURDATE(), INTERVAL 90 day) 
and Date <CURDATE()
GROUP BY date;

#####Historio de 90 dias Colombia para llamadas preventa
########################################################
DROP TEMPORARY TABLE IF EXISTS tmp_amaster;
CREATE TEMPORARY TABLE tmp_amaster (key(date))
SELECT Date,COUNT(DISTINCT OrderNum)Ordenes
FROM development_co_project.A_Master
WHERE Date>='2013-10-01' AND OrderBeforeCan=1
GROUP BY date;

DROP TEMPORARY TABLE IF EXISTS tmp_llamadas;
CREATE TEMPORARY TABLE tmp_llamadas (key(fecha))
SELECT event_date as Fecha,SUM(if(queue='ivr_cola1',1,0))Llamadas
FROM customer_service_co.bi_ops_cc
WHERE event_date>='2013-10-01' AND net_event=1
GROUP BY Fecha

UNION ALL

SELECT event_date as Fecha,COUNT(1)Llamadas
FROM customer_service_co.bi_ops_cc_dyalogo
WHERE event_date>='2013-10-01' AND net_event=1 AND pre=1
GROUP BY Fecha


;

DROP TABLE IF EXISTS DM_CS_TS_hist_90_CO;
CREATE TABLE DM_CS_TS_hist_90_CO 
SELECT date,llamadas,ordenes
 FROM 
tmp_amaster a
LEFT JOIN
tmp_llamadas b
ON a.Date=b.Fecha 
WHERE Date>=DATE_SUB(CURDATE(), INTERVAL 90 day) 
and Date <CURDATE()
GROUP BY date;

#####Historio de 90 dias de chats para Colombia
########################################################

########################################################

DROP TEMPORARY TABLE IF EXISTS tmp_amaster;
CREATE TEMPORARY TABLE tmp_amaster (key(date))
SELECT Date,COUNT(DISTINCT OrderNum)Ordenes
FROM development_co_project.A_Master
WHERE Date>='2013-10-01' AND OrderBeforeCan=1
GROUP BY date;

DROP TEMPORARY TABLE IF EXISTS tmp_chats;
CREATE TEMPORARY TABLE tmp_chats (key(chat_start_time))
SELECT date as chat_start_time,chats 
FROM 
customer_service_co.tbl_bi_ops_cc_chats_performance
UNION ALL
SELECT date(chat_creation_time)chat_start_time,COUNT(chat_creation_time)chats  FROM customer_service_co.tbl_live_chat_co
GROUP BY date(chat_creation_time);


DROP TABLE IF EXISTS DM_CHAT_hist_90_co;
CREATE TABLE DM_CHAT_hist_90_co
SELECT date,Chats as llamadas,ordenes
 FROM 
tmp_amaster a
LEFT JOIN
tmp_chats b
ON a.Date=b.chat_start_time
WHERE Date>=DATE_SUB(CURDATE(), INTERVAL 90 day) 
and Date <CURDATE()
GROUP BY date;
