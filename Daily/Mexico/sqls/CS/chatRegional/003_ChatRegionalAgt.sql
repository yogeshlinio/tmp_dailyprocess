use customer_service;

SET @max_day = (SELECT max(Fecha) FROM rep_chats_agt_regional);

select date(@max_day);

DELETE FROM rep_chats_agt_regional WHERE Fecha>=@max_day;

insert into rep_chats_agt_regional 
(Fecha,Franja,operator_nickname,Pais,Recibido,Atendido, TEspera, TEsperaAgente, TEsperaAbandono,Abandono, TDuracion,ChatAtn20)

#Reporte Chats General x Agt
select date, franja, operator_nickname, pais, sum(net_event) Recibido, sum(answered_in_wh) Atendido, sum(queue_time) TEspera, sum(operator_first_response_delay) TEsperaAgente,
sum(if(unanswered_in_wh=1,chat_duration,0)) TEsperaAbandono, sum(unanswered_in_wh) Abandono, sum(duration_time) TDuracion, sum(answered_in_wh_20) ChatAtn20
from tbl_olark_chat_general
where date>=@max_day
group by date, shift, operator_nickname, pais;


insert into rep_chats_agt_regional 
(Fecha,Franja,operator_nickname,Pais,Atendido,TEsperaAgente,TDuracion,ChatAtn20)

#Reporte Chats Co x Agt
SELECT chat_date Fecha, franja, operator_nickname, 'CO' Pais, count(1) Atendido, sum(queue_duration) TEsperaAgente, sum(chat_duration) TDuracion,
sum(if(queue_duration<20,1,0)) ChatAtn20 FROM customer_service_co.tbl_live_chat_co
where chat_date>=@max_day
group by  chat_date, chat_shift, operator_nickname ;


insert into rep_chats_agt_regional 
(Fecha,Franja,Pais,TEsperaAbandono,Abandono)

#Reporte Chats Co Abandonos
select Fecha, Franja, 'CO', TEsperaAbandono,  Abandono
 from 
customer_service.tbl_base_48 BDS
left join
(select a.date, cast(if(length(hour(a.hour))=1,concat('0',hour(a.hour), '00') ,concat(hour(a.hour), '00')) as char) shift, 
abandon Abandono, abandon_time*abandon TEsperaAbandono from customer_service_co.tbl_queue_visitors_live_chat a
inner join customer_service_co.tbl_queue_time_live_chat b
on a.date=b.date and a.hour=b.hour) ChatAbandon
on BDS.Fecha=ChatAbandon.date and BDS.id=cast(ChatAbandon.shift as char)
where 
BDS.Fecha>=@max_day and
BDS.Fecha<date(CURDATE());

#Agregar datos generales
update rep_chats_agt_regional 
set 
Recibido=Atendido+Abandono,
TEspera=TEsperaAgente+TEsperaAbandono
where pais='CO';

UPDATE rep_chats_agt_regional 
SET 
    Semana = date_format(fecha, '%Y-%u');