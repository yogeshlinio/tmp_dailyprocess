#Chats M�xico, Per� y Venezuela

use customer_service;

#Ingresar informaci�n tabla general de Olark
truncate table tbl_olark_chat_general;
insert into tbl_olark_chat_general 
(pais,chat_start_time, chat_end_time, chat_duration, operator_nickname, visitor_nickname, visitor_location, visitor_email, visitor_organization, chat_word_count,
operator_messages_sent, visitor_messages_sent, operator_first_response_delay, visitor_ipaddress, transcript_type)
select 'MX' as pais,chat_start_time, chat_end_time, chat_duration, operator_nickname, visitor_nickname, visitor_location, visitor_email, visitor_organization, chat_word_count,
operator_messages_sent, visitor_messages_sent, operator_first_response_delay, visitor_ipaddress, transcript_type from tbl_olark_chat
union all
select 'PE' as pais, chat_start_time, chat_end_time, chat_duration, operator_nickname, visitor_nickname, visitor_location, visitor_email, visitor_organization, chat_word_count,
operator_messages_sent, visitor_messages_sent, operator_first_response_delay, visitor_ipaddress, transcript_type from tbl_olark_chat_pe
union all
select 'VE' as pais,chat_start_time, chat_end_time, chat_duration, operator_nickname, visitor_nickname, visitor_location, visitor_email, visitor_organization, chat_word_count,
operator_messages_sent, visitor_messages_sent, operator_first_response_delay, visitor_ipaddress, transcript_type
 from tbl_olark_chat_ve;



#Actualizar chats recibidos
update tbl_olark_chat_general
set
inc_chat=1
WHERE  
visitor_nickname not like '%#%';

#Actualizar filtros binarios
update tbl_olark_chat_general
set
answered_in_wh=if(operator_first_response_delay <>'None',1,0),
answered_in_wh_20=if(operator_first_response_delay <>'None' and operator_first_response_delay<=20,1,0),
unanswered_in_wh=if(operator_first_response_delay ='None' AND visitor_messages_sent>0,1,0)
where inc_chat=1
;

#Actualizar campos de fecha, mes y d�a de la semana
update tbl_olark_chat_general
set
date=date(chat_start_time),
monthnum=date_format(chat_start_time,"%Y%m"),
weekday=weekday(chat_start_time),
shift=IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
);

#Agregar franja horaria
update tbl_olark_chat_general a inner join tbl_base_48 b
on a.shift=b.Id
set a.franja=b.Franja;

#Holidays
update tbl_olark_chat_general a inner join operations_mx.calendar b on a.date=b.dt
set holiday=b.isholiday
where pais='MX';

update tbl_olark_chat_general a inner join operations_pe.calendar b on a.date=b.dt
set holiday=b.isholiday
where pais='PE';

update tbl_olark_chat_general a inner join operations_ve.calendar b on a.date=b.dt
set holiday=b.isholiday
where pais='VE';

#Horas laborales chat
update tbl_olark_chat_general
set workinghour=if(weekday=6,
	if(hour(chat_start_time)>=10 and hour(chat_start_time)<18,
		1,0),
		if(weekday=5,
			if(hour(chat_start_time)>=9 and hour(chat_start_time)<19,
			1,0),
			if(hour(chat_start_time)>=8 and hour(chat_start_time)<23,
				1,0)))
where pais='MX';

update tbl_olark_chat_general
set workinghour=if(weekday=6,
	if(hour(chat_start_time)>=9 and hour(chat_start_time)<20,
		1,0),
		if(weekday=5,
			if(hour(chat_start_time)>=9 and hour(chat_start_time)<20,
			1,0),
			if(hour(chat_start_time)>=8 and hour(chat_start_time)<23,
				1,0)))
where pais='PE';

update tbl_olark_chat_general
set workinghour=if(weekday=6,0,
		if(weekday=5,
			if(hour(chat_start_time)>=9 and hour(chat_start_time)<17,1,0),
		if(weekday=4,
			if(hour(chat_start_time)<20,1,0),
		if(weekday=0,
			if(hour(chat_start_time)>=8,1,0),
		1))))
where pais='VE';

#Actualizar eventos netos
update tbl_olark_chat_general
set
net_event=1
where answered_in_wh+unanswered_in_wh=1
and workinghour=1
;

update tbl_olark_chat_general
set
queue_time=if(answered_in_wh=1,operator_first_response_delay,chat_duration),
duration_time=if(answered_in_wh=1,chat_duration,0)
where net_event=1;

#Colombia

use customer_service_co;

#Actualizar campos de fecha, mes y d�a de la semana
update tbl_live_chat_co
set
chat_date=date(chat_start_time),
chat_shift=IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
);

#Actualizar Franjas horarias
update tbl_live_chat_co a inner join customer_service.tbl_base_48 b
on a.chat_shift=b.Id
set a.franja=b.Franja;

#Agregar cantidad de agentes
update tbl_live_chat_co
set qagents=
if(operator_nickname="" or operator_nickname is null,0,1)+
if(operator_nickname2="" or operator_nickname2 is null,0,1)+
if(operator_nickname3="" or operator_nickname3 is null,0,1)+
if(operator_nickname4="" or operator_nickname4 is null,0,1)+
if(operator_nickname5="" or operator_nickname5 is null,0,1)+
if(operator_nickname6="" or operator_nickname6 is null,0,1)+
if(operator_nickname7="" or operator_nickname7 is null,0,1),
chat_date=date(chat_start_time),
chat_shift=IF (	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
)
where qagents is null
;

#Agregar last_operator_nickname
update tbl_live_chat_co
set
last_operator_nickname=
if(qagents=1,operator_nickname,
if(qagents=2,operator_nickname2,
if(qagents=3,operator_nickname3,
if(qagents=4,operator_nickname4,
if(qagents=5,operator_nickname5,
if(qagents=6,operator_nickname6,operator_nickname7))))))
where last_operator_nickname is null;


#####################################################################
#Datos de chats por agente Colombia

use customer_service_co;

SET @max_day = (SELECT max(date) FROM tbl_bi_ops_cc_chats_performance_agents);

DELETE FROM tbl_bi_ops_cc_chats_performance_agents WHERE date>=@max_day;

#Agregar cantidad principal de chats
insert into customer_service_co.tbl_bi_ops_cc_chats_performance_agents(date, operator_name, chats_first_operator, act_sec)
SELECT chat_date, operator_nickname, count(1) cant_chats, avg(chat_duration)  FROM customer_service_co.tbl_live_chat_co
where chat_date>=@max_day
group by chat_date, operator_nickname;

#Agregar otros chats en los que participan
update customer_service_co.tbl_bi_ops_cc_chats_performance_agents a inner join
(SELECT chat_date, operator_nickname2, count(1) cant_chats  FROM customer_service_co.tbl_live_chat_co
where chat_date>=@max_day
and operator_nickname2<>""
group by chat_date, operator_nickname) b
on a.date=b.chat_date
and a.operator_name=b.operator_nickname2
set
a.other_chats=cant_chats;

update customer_service_co.tbl_bi_ops_cc_chats_performance_agents a inner join
(SELECT chat_date, operator_nickname3, count(1) cant_chats  FROM customer_service_co.tbl_live_chat_co
where chat_date>=@max_day
and operator_nickname3<>""
group by chat_date, operator_nickname) b
on a.date=b.chat_date
and a.operator_name=b.operator_nickname3
set
other_chats=other_chats+cant_chats;


update customer_service_co.tbl_bi_ops_cc_chats_performance_agents a inner join
(SELECT chat_date, operator_nickname4, count(1) cant_chats  FROM customer_service_co.tbl_live_chat_co
where chat_date>=@max_day
and operator_nickname4<>""
group by chat_date, operator_nickname) b
on a.date=b.chat_date
and a.operator_name=b.operator_nickname4
set
other_chats=other_chats+cant_chats;

update customer_service_co.tbl_bi_ops_cc_chats_performance_agents a inner join
(SELECT chat_date, operator_nickname5, count(1) cant_chats  FROM customer_service_co.tbl_live_chat_co
where chat_date>=@max_day
and operator_nickname5<>""
group by chat_date, operator_nickname) b
on a.date=b.chat_date
and a.operator_name=b.operator_nickname5
set
other_chats=other_chats+cant_chats;


update customer_service_co.tbl_bi_ops_cc_chats_performance_agents a inner join
(SELECT chat_date, operator_nickname6, count(1) cant_chats  FROM customer_service_co.tbl_live_chat_co
where chat_date>=@max_day
and operator_nickname6<>""
group by chat_date, operator_nickname) b
on a.date=b.chat_date
and a.operator_name=b.operator_nickname6
set
other_chats=other_chats+cant_chats;


update customer_service_co.tbl_bi_ops_cc_chats_performance_agents a inner join
(SELECT chat_date, operator_nickname7, count(1) cant_chats  FROM customer_service_co.tbl_live_chat_co
where chat_date>=@max_day
and operator_nickname7<>""
group by chat_date, operator_nickname) b
on a.date=b.chat_date
and a.operator_name=b.operator_nickname7
set
other_chats=other_chats+cant_chats;

#Agregar nivel de satisfacci�n de acuerdo al �ltimo asesor

update customer_service_co.tbl_bi_ops_cc_chats_performance_agents a inner join
(SELECT chat_date, last_operator_nickname,  sum(if(nivel_de_servicio in ('Muy Satisfecho','Satisfecho'),1,0)) satisfaccion,
sum(if(nivel_de_servicio <>"",1,0)) calificados
 FROM customer_service_co.tbl_live_chat_co
where chat_date>=@max_day
group by chat_date, last_operator_nickname) b
on a.date=b.chat_date
and a.operator_name=b.last_operator_nickname
set
rated=calificados,
rated_good=satisfaccion,
rated_bad=calificados-satisfaccion;

#Suma cantidad total de chats
update customer_service_co.tbl_bi_ops_cc_chats_performance_agents 
set
chats=chats_first_operator+other_chats
where date>=@max_day;

#Agregar nombre de los agentes
update tbl_bi_ops_cc_chats_performance_agents a 
inner join bi_ops_matrix_sac b
on a.operator_name=b.user_chat
set 
name_matrix=name,
agent_identification=identification
where name_matrix is null
and date>=@max_day;

#Agregar tiempo total de chat

update tbl_bi_ops_cc_chats_performance_agents 
set 
total_chatting_sec=act_sec*chats
where total_chatting_sec is null;
