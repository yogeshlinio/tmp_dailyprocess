INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  step,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Colombia',
  'Sales_Report',
  'finish',
  NOW(),
  max(Date),
  '0',
  '0'
FROM
  customer_service.tbl_telesales_co

