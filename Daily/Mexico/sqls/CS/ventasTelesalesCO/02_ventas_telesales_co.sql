SELECT  'Insertar datos de las ventas',now();

DROP TEMPORARY TABLE IF EXISTS A_Master_ev_co;
CREATE TEMPORARY TABLE A_Master_ev_co (key(OrderNum))
SELECT 
	Master.OrderNum,
	Master.ItemID,
  Master.OriginalPrice,
	Master.Rev,
	Master.Price,
  Master.CouponCode,
  Master.CouponValue,
  Master.PaidPrice,
  Master.TaxPercent,
  Master.PaidPriceAfterTax,

  Master.Cancellations,
	Master.OrderAfterCan,
	Master.OrderBeforeCan,
  Master.Returns,
  
	Master.Status,
	Master.PaymentMethod,
  Master.Installment,
	Master.ChannelGroup,
	Master.DateCollected,
	Master.DateDelivered,
	Master.Date,
	Master.Time,
	Master.AssistedSalesOperator
FROM 
	           development_co_project.A_Master as Master
WHERE AssistedSalesOperator<>''
 ;

DROP TABLE IF EXISTS tbl_sales_hist_co;
CREATE TABLE tbl_sales_hist_co ( 
                                 PRIMARY KEY ( Date, telesales,agent_Name, OrderNum, ItemID ) , 
                                         KEY ( telesales) , 
                                         KEY ( OrderNum ),
                                         KEY ( ItemID )
 )
SELECT
	Date,
	Master.Time,
	'CO' as Pais,
	Master.assistedsalesoperator AS telesales,
	`name` AS agent_Name,
	identification,
	Master.OrderNum,
	Master.ItemID,
    Master.OriginalPrice,
	Master.Rev,
	Master.Price,
    Master.CouponCode,
    Master.CouponValue,
    Master.PaidPrice,
    Master.TaxPercent,
    Master.PaidPriceAfterTax,

    Master.Cancellations,
	Master.OrderAfterCan,
	Master.OrderBeforeCan,
    Master.Returns,
  
	Master.Status,
	Master.PaymentMethod,
    Master.Installment,
	Master.ChannelGroup,

	Master.DateCollected,
	Master.DateDelivered
FROM
	        customer_service_co.bi_ops_matrix_ventas a
LEFT JOIN A_Master_ev_co AS Master 
       ON a.sales_operator = Master.assistedsalesoperator AND a.sales_operator is not null
WHERE date IS NOT NULL
ORDER BY Date,OrderNum, ItemId
;