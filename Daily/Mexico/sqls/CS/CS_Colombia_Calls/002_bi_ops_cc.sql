use customer_service_co;

SELECT  'Inicio rutina bi_ops_cc',now();

set @max_day=cast((select max(event_date) from bi_ops_cc_dyalogo)  as date);
set time_zone='-5:00';



delete from bi_ops_cc_dyalogo
where event_date>=@max_day;
#truncate table bi_ops_cc_dyalogo;

#Agregar matriz historica sac
delete from cs_performance_co.bi_ops_matrix_sac_history

where date>=DATE_SUB(CURDATE(), INTERVAL 1 day);


insert into cs_performance_co.bi_ops_matrix_sac_history
select a.*, date_sub(curdate(), interval 1 day) from bi_ops_matrix_sac a;


#Agregar matriz historica ventas
delete from cs_performance_co.bi_ops_matrix_ventas_history

where date>=DATE_SUB(CURDATE(), INTERVAL 1 day);


insert into cs_performance_co.bi_ops_matrix_ventas_history
select a.*, DATE_SUB(CURDATE(), INTERVAL 1 day) from bi_ops_matrix_ventas a;



SELECT  'Ingresar datos de dyalogo Colombia',now();

insert into bi_ops_cc_dyalogo (queue_stats_id, queue, agent_name,agent_identification, agent_ext, event, call_duration_seg,
hold_duration_seg, agent_hold_time_seg, uniqueid, number, record)
SELECT a.id, campana, agente,  b.identificacion, extension, resultado,
 duracion_al_aire, tiempo_espera, tiempo_timbrando, unique_id, numero_telefonico,grabacion
  FROM CS_dyalogo.dy_llamadas_espejo a left join
CS_dyalogo.dy_agentes b on a.id_agente_espejo=b.id
where date(fecha_hora)>=@max_day
and sentido='Entrante'
and campana is not null;

SELECT  'Llamadas transferidas',now();

#Ingresar llamadas transferidas y cantidad de veces
DROP  TEMPORARY TABLE IF EXISTS calls_transfered_to_queue;
CREATE TEMPORARY TABLE calls_transfered_to_queue

#Llenar los datos de las llamadas transferidas 
select unique_id,count(1) cant, max(fecha_hora) max_fecha_hora, null as cola
 from CS_dyalogo.v_queue_log where evento in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON') group by unique_id having count(1)>1;

#Ingresar base general de las llamadas transferidas
truncate table calls_transfered;
insert into calls_transfered

select b.*,a.cant, replace(left(right(agente,length(agente)-instr(agente,'|')),instr(right(agente,length(agente)-instr(agente,'|')),'|')-1),'_',' ') name,
right(agente,if(instr(right(agente,length(agente)-instr(agente,'|')),'|')=0,null,length(right(agente,length(agente)-instr(agente,'|'))) -instr(right(agente,length(agente)-instr(agente,'|')),'|'))) ext, 
left(datos,instr(datos,'|')-1) info1,left(right(datos,length(datos)-instr(datos,'|')),instr(right(datos,length(datos)-instr(datos,'|')),'|')-1) info2,
right(datos,if(instr(right(datos,length(datos)-instr(datos,'|')),'|')=0,null,length(right(datos,length(datos)-instr(datos,'|'))) -instr(right(datos,length(datos)-instr(datos,'|')),'|')))
  info3, null as info4, null as info5, null as info6, null as queue1, null as queue2, null as queue3, null as sequence
from  CS_dyalogo.v_queue_log b
inner join calls_transfered_to_queue a
on a.unique_id=b.unique_id
where evento in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON');

update calls_transfered
set
info4=left(info3,instr(info3,'|')-1), 
info5=left(right(info3,length(info3)-instr(info3,'|')),instr(right(info3,length(info3)-instr(info3,'|')),'|')-1),
info6=right(info3,if(instr(right(info3,length(info3)-instr(info3,'|')),'|')=0,null,length(right(info3,length(info3)-instr(info3,'|'))) -instr(right(info3,length(info3)-instr(info3,'|')),'|')))
;

#Ingresar secuencias de transferencia
drop temporary table if exists sequence_transfered;
create temporary table sequence_transfered
select unique_id, group_concat( distinct cola order by fecha_hora asc ) sequence from calls_transfered group by unique_id;

update calls_transfered a
inner join sequence_transfered b
on a.unique_id=b.unique_id
set a.sequence=b.sequence;

update calls_transfered
set
queue1=left(sequence,instr(sequence,',')-1), 
queue2=if(cant=2,right(sequence,length(sequence)-instr(sequence,',')),left(right(sequence,length(sequence)-instr(sequence,',')),instr(right(sequence,length(sequence)-instr(sequence,',')),',')-1)),
queue3=right(sequence,if(instr(right(sequence,length(sequence)-instr(sequence,',')),',')=0,null,length(right(sequence,length(sequence)-instr(sequence,','))) -instr(right(sequence,length(sequence)-instr(sequence,',')),',')))
;

#Identificar llamadas transferidas en la tabla general y borrarlas
update bi_ops_cc_dyalogo a 
inner join calls_transfered b
on a.uniqueid=b.unique_id
set transfered_from_queue=1
where date(fecha_hora)>=@max_day;

delete from bi_ops_cc_dyalogo
where transfered_from_queue=1
and event_date is null;

SELECT  'Ingresar llamadas transferidas a bi_ops_cc',now();
#Agregar llamadas transferidas a bi_ops_cc
insert into bi_ops_cc_dyalogo (queue_stats_id, queue, agent_name, agent_ext, event, call_duration_seg,
hold_duration_seg, uniqueid)
SELECT id,  cola, name,  ext, if(evento='ABANDON','Abandonada','Contestada'),
if(evento='ABANDON',info3,if(evento='TRANSFER',info4,info1)) , if(evento='ABANDON',0,if(evento='TRANSFER',info5,info2)),  unique_id
  FROM calls_transfered
where date(fecha_hora)>=@max_day;

update bi_ops_cc_dyalogo a 
inner join calls_transfered b
on a.uniqueid=b.unique_id
set
initial_queue=queue1,
second_queue=queue2,
third_queue=queue3,
sequence_queue=sequence,
transfered_to_queue=if(queue=queue1,1,if(queue=queue2 and cant>=3,1,0)),
transfered_from_queue=if(queue<>queue1,1,0);

SELECT  'Fecha hora de conexi�n',now();

drop table if exists fecha_time_connected;

create temporary table fecha_time_connected

select unique_id, fecha_hora, cola, right(datos,if(instr(right(datos,length(datos)-instr(datos,'|')),'|')=0,null,length(right(datos,length(datos)-instr(datos,'|'))) -instr(right(datos,length(datos)-instr(datos,'|')),'|'))) agent_hold_time
from CS_dyalogo.v_queue_log where evento='ENTERQUEUE';

create index unique_id on  fecha_time_connected (unique_id);

update bi_ops_cc_dyalogo
set
queue=replace(queue," ","");

update bi_ops_cc_dyalogo a
inner join fecha_time_connected b
on a.uniqueid=b.unique_id
set 
event_datetime=fecha_hora,
event_date=date(fecha_hora),
event_hour=hour(fecha_hora), 
event_minute=minute(fecha_hora),
event_weekday= weekday(fecha_hora),
event_week=weekofyear(fecha_hora),
event_month=monthname(fecha_hora),
event_year=year(fecha_hora),
a.agent_hold_time_seg=if(a.agent_hold_time_seg is null,b.agent_hold_time,a.agent_hold_time_seg)
where a.queue=b.cola;



SELECT  'Actualizar shifts',now();

update bi_ops_cc_dyalogo 
set event_shift=IF(MINUTE(event_datetime)<30,
	concat(date_format(event_datetime,'%H'),'00'),
	concat(date_format(event_datetime,'%H'),'30'));

SELECT  'Actualizar holidays',now();
update bi_ops_cc_dyalogo a inner join operations_co.calendar b on a.event_date=b.dt
set holiday=b.isholiday
where holiday is null;

SELECT  'Actualizar horas laborales',now();
update bi_ops_cc_dyalogo
set workinghour=if(event_weekday=6 or holiday=1,
	if(event_hour>=10 and event_hour<18,
		1,0),
		if(event_weekday=5,
			if(event_hour>=9 and event_hour<21,
			1,0),
			if(event_hour>=8 and event_hour<21,
				1,0)))
where event_date>=@max_day
;

update bi_ops_cc_dyalogo
set workinghour=1
where holiday=0 and event_weekday<5
and event_shift='0730'
and event_date>=@max_day
;

SELECT  'Actualizar net_events',now();
update bi_ops_cc_dyalogo
set net_event=1
where event in ('Contestada','Abandonada') 
#and workinghour=1
and event_date>=@max_day
;

SELECT  'Actualizar binarios de tiempos',now();
update bi_ops_cc_dyalogo
set 
unanswered_in_wh=if(event in ('Abandonada'),1,0),
unanswered_in_wh_cust=if(event in ('Abandonada') and hold_duration_seg<=5,1,0),
answered_in_wh=if(event in ('Contestada'),1,0),
answered_in_wh_20=IF(event in ('Contestada') and hold_duration_seg<=20,1,0),
no_answer=if(event='No Contestada',1,0),
agent_q=if(agent_name is not null,1,0)
where net_event=1
and event_date>=@max_day
;

#Llamadas pos
update bi_ops_cc_dyalogo
set 
pos=1,
pos_answ=if(answered_in_wh=1,1,0)
where net_event=1
and queue in ('Devolucionesytramites','Reclamos','Estadodepedido','Cancelaciones')
and event_date>=@max_day
;

#Llamadas pre
update bi_ops_cc_dyalogo
set 
pre=1,
pre_answ=if(answered_in_wh=1,1,0)
where net_event=1
and queue ='Ventaspersonasnaturales'
and event_date>=@max_day
;

SELECT  'Actualizar cedulas faltantes',now();
update bi_ops_cc_dyalogo a
inner join CS_dyalogo.dy_agentes b on a.agent_name=b.nombre
set agent_identification=identificacion
where agent_identification is null and event='Contestada';

#Franjas
UPDATE
customer_service_co.bi_ops_cc_dyalogo a
left join
customer_service.tbl_48_franjas b
on a.event_shift=b.Id
 set a.Franja=b.Franja
;

SELECT  'Fin rutina bi_ops_cc',now();