INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'bi_ops_cc',
  'finish',
  NOW(),
  MAX(event_date),
  count(*),
  count(*)
FROM
  customer_service_co.bi_ops_cc_dyalogo;