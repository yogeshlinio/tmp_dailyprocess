DROP TABLE  IF EXISTS tbl_telesales_co;
CREATE TABLE `tbl_telesales_co` (
  `Date` date NOT NULL,
  `hour` int(2) DEFAULT NULL,
  `minute` int(2) DEFAULT NULL,
  `Campaign` binary(0) DEFAULT NULL,
  `AssistedSalesOperator` varchar(255) NOT NULL,
  `ChannelGroup` varchar(45) DEFAULT NULL,
  `channel` varchar(45) DEFAULT NULL,
  `OrderNum` varchar(45) NOT NULL,
  `ItemID` int(11) NOT NULL,
  `PaidPrice` decimal(15,5) NOT NULL,
  `Rev` double NOT NULL,
  `OrderAfterCan` bit(1) NOT NULL,
  `OrderBeforeCan` bit(1) NOT NULL,
  `OriginalPrice` decimal(15,5) NOT NULL,
  `Status` varchar(255) NOT NULL,
  `agent_ext` binary(0) DEFAULT NULL,
  `Identification` varchar(255) DEFAULT NULL,
  `agent_Name` varchar(255) DEFAULT NULL,
  `coordinator` varchar(255) DEFAULT NULL,
  `process` varchar(255) DEFAULT NULL,
  `source_medium` varchar(512) DEFAULT NULL,
  `Telesales` varchar(1) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `other_channels` varchar(1) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `NewReturning` varchar(255) NOT NULL,
  `PCOne` double NOT NULL,
  `PCOnePFive` double NOT NULL,
  `PCTwo` double NOT NULL,
  `DateCollected` date NOT NULL,
  `shift_date_order` binary(0) DEFAULT NULL,
  `DateDelivered` date NOT NULL,
  `PaymentMethod` varchar(50) NOT NULL,
  `Installment` int(11) NOT NULL,
  `Pais` varchar(2) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `XR` decimal(8,4) DEFAULT NULL,
  `SKUName` varchar(255) NOT NULL,
  `Cat1` varchar(255) NOT NULL,
  `Cat2` varchar(255) NOT NULL,
  `Cat3` varchar(255) NOT NULL,
		CouponCode varchar(255) NOT NULL,
	CouponValue double NOT NULL
)
SELECT 
	Master.Date,	
	HOUR(Master.Time) as hour,
	MINUTE(Master.Time) as minute,
	NULL as Campaign,
	Master.AssistedSalesOperator,
	channel.channel_group as ChannelGroup,
  channel.channel,
	Master.OrderNum,
	Master.ItemID,
	Master.PaidPrice,
	Master.Rev,
	Master.OrderAfterCan,
	Master.OrderBeforeCan,
  Master.OriginalPrice,
	Master.Status,	
	NULL AS agent_ext,
	NULL AS Identification,
	null as agent_Name,
	NULL AS coordinator,
	NULL AS process,
	channel.source_medium,
	'1' AS Telesales,
	'0' AS other_channels,
	NewReturning,
	PCOne,
	PCOnePFive,
	PCTwo,
	Master.DateCollected,
	NULL as shift_date_order,
	Master.DateDelivered,
  Master.PaymentMethod,
  Master.Installment,
	'CO' as Pais,  
	XR,
	SKUName,
	Cat1,
	Cat2,
	Cat3,
	CouponCode,
	CouponValue
	
FROM 
	    development_co_project.A_Master as Master
LEFT JOIN
			marketing_co.channel_report channel
ON `Master`.OrderNum=channel.OrderNum
LEFT JOIN 
			(SELECT * FROM development_mx.A_E_BI_ExchangeRate_USD WHERE Country='Col') Ex
ON DATE_FORMAT(`Master`.date,'%Y%m')=Ex.Month_Num
WHERE 
		channel.channel_group	='Telesales' 
 ;

UPDATE tbl_telesales_co a
LEFT JOIN customer_service_co.bi_ops_matrix_ventas b
ON a.AssistedSalesOperator=b.sales_operator
SET a.Identification=b.identification,a.agent_Name=b.`name`,a.coordinator=b.coordinator
WHERE Pais='CO';