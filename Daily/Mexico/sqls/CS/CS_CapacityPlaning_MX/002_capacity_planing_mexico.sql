use customer_service;
#####Historio de 90 dias M�xico para llamadas posventa con desfase de 2 semanas
########################################################


DROP TEMPORARY TABLE IF EXISTS tmp_amaster;
CREATE TEMPORARY TABLE tmp_amaster (key(date))
SELECT DATE_ADD(Date, interval 14 DAY )Date,count(DISTINCT OrderNum)Ordenes
FROM development_mx.A_Master
WHERE Date>='2013-10-01' AND OrderBeforeCan=1
GROUP BY date;

DROP TEMPORARY TABLE IF EXISTS tmp_llamadas;
CREATE TEMPORARY TABLE tmp_llamadas (key(fecha))
SELECT event_date as Fecha,SUM(pos)Llamadas
FROM customer_service.bi_ops_cc_mx
WHERE event_date >='2013-10-01'
GROUP BY event_date ;

DROP TABLE IF EXISTS DM_CS_hist_90_2weeks_mx;
CREATE TABLE DM_CS_hist_90_2weeks_mx 
SELECT date,llamadas,ordenes
 FROM 
tmp_amaster a
LEFT JOIN
tmp_llamadas b
ON a.Date=b.Fecha 
WHERE Date>=DATE_SUB(CURDATE(), INTERVAL 90 day) 
and Date <CURDATE()
GROUP BY date;



#####Historio de 90 dias MX para llamadas preventa
########################################################


DROP TEMPORARY TABLE IF EXISTS tmp_amaster;
CREATE TEMPORARY TABLE tmp_amaster (key(date))
SELECT Date,COUNT(DISTINCT OrderNum)Ordenes
FROM development_mx.A_Master
WHERE Date>='2013-10-01' AND OrderBeforeCan=1
GROUP BY date;

DROP TEMPORARY TABLE IF EXISTS tmp_llamadas;
CREATE TEMPORARY TABLE tmp_llamadas (key(fecha))
SELECT Fecha,SUM(if(Cola='Pre-Venta'=1,1,0))Llamadas
FROM customer_service.tbl_queueMetricsAuto
WHERE Fecha>='2013-10-01'
GROUP BY Fecha;

DROP TABLE IF EXISTS DM_CS_TS_hist_90;
CREATE TABLE DM_CS_TS_hist_90 
SELECT date,llamadas,ordenes
 FROM 
tmp_amaster a
LEFT JOIN
tmp_llamadas b
ON a.Date=b.Fecha 
WHERE Date>=DATE_SUB(CURDATE(), INTERVAL 90 day) 
and Date <CURDATE()
GROUP BY date;

#####Historio de 90 dias de chats para Colombia
########################################################


DROP TEMPORARY TABLE IF EXISTS tmp_amaster;
CREATE TEMPORARY TABLE tmp_amaster (key(date))
SELECT Date,SUM(if(OrderBeforeCan=1,1,0))Ordenes
FROM development_mx.A_Master
WHERE Date>='2013-10-01'
GROUP BY date;

DROP TEMPORARY TABLE IF EXISTS tmp_chats;
CREATE TEMPORARY TABLE tmp_chats (key(chat_start_time))
SELECT Date(chat_start_time)chat_start_time,COUNT(*)Chats
FROM customer_service.tbl_olark_chat
WHERE chat_start_time>='2013-10-01'
AND visitor_nickname not like 'Mexico (%' 
AND visitor_nickname not like 'Mexico #%' AND visitor_nickname not like 'unknown #%'
AND visitor_nickname not like 'USA (%'
AND visitor_nickname not like 'USA #%'
AND visitor_nickname not like 'Spain (%'
AND visitor_nickname not like 'Colombia (%'
AND visitor_nickname not like 'Panama (%'
AND visitor_nickname not like 'Chile (%'
AND visitor_nickname not like 'Peru (%'
AND visitor_nickname not like 'Peru #%'
AND visitor_location like  'Mexico%'

GROUP BY Date(chat_start_time);

DROP TABLE IF EXISTS DM_CHAT_hist_90_mx;
CREATE TABLE DM_CHAT_hist_90_mx
SELECT date,Chats,ordenes
 FROM 
tmp_amaster a
LEFT JOIN
tmp_chats b
ON a.Date=b.chat_start_time
WHERE Date>=DATE_SUB(CURDATE(), INTERVAL 90 day) 
and Date <CURDATE()
GROUP BY date;