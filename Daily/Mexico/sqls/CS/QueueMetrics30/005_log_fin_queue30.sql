INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  step,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Mexico',
  'tbl_queueMetrics30',
  'finish',
  NOW(),
  max(Fecha),
  count(*),
  count(Fecha)
FROM
  customer_service.tbl_queueMetrics30;


