update
customer_service.tbl_queueMetrics30
set Atendida=if(Transferencia=1,1,0)
where Atendida=0;

update
customer_service.tbl_queueMetrics30 a
left join
customer_service.tbl_48_franjas b
on a.intervalo=b.Id
 set a.Franja=b.Franja
where a.Franja is not null;


UPDATE customer_service.tbl_queueMetrics30 a
left JOIN
(
select id,answer1 from customer_service.questions where question=1 and date_format(
cast(
concat(
LEFT (substring(`date`, 7, 7), 4),
substring(`date`, 3, 3),
'-',
LEFT (`date`, 2),
' ',
REPLACE (RIGHT(`date`, 8), '.', ':')
) AS datetime
),
'%Y-%m-%d'
)>='2013-12-01'  #date_sub(date(curdate()), INTERVAL 1 DAY) and `date`<date(curdate())
)b
on a.id = b.id
SET question1=answer1;
UPDATE customer_service.tbl_queueMetrics30 a
left JOIN
(
select id,answer1 from customer_service.questions where question=2 and date_format(
cast(
concat(
LEFT (substring(`date`, 7, 7), 4),
substring(`date`, 3, 3),
'-',
LEFT (`date`, 2),
' ',
REPLACE (RIGHT(`date`, 8), '.', ':')
) AS datetime
),
'%Y-%m-%d'
)>='2013-12-01' #date_sub(date(curdate()), INTERVAL 1 DAY) and `date`<date(curdate())
)b
on a.id = b.id
SET question2=answer1;

UPDATE customer_service.tbl_queueMetrics30 SET FCR = 
if(question1=1 and question2=1,1,null);

UPDATE customer_service.tbl_queueMetrics30 SET FCR = 0
where question1>=0 and FCR IS NULL;

update customer_service.tbl_queueMetrics30 a
left join
customer_service.tbl_staff_ev b
on RIGHT(a.Administrada,4)=b.Extension
SET Agente=Nombre
WHERE Agente ='NULL'
;

/*update customer_service.tbl_queueMetrics30 a
left join
customer_service.staff b
on a.Administrada=b.nome_agente
SET Agente=descr_agente
WHERE Agente is null
AND Fecha>='2014-02-09'
;*/