USE test;
DROP  TABLE if EXISTS sampleQueue;
CREATE  TABLE sampleQueue
select * from queuemetrics.queue_log  where FROM_UNIXTIME(time_id,'%Y-%m-%d')
#in ('2014-02-15')
>=
 date_sub(date_format(curdate(),'%Y-%m-%d'),interval 1 day)
and 
FROM_UNIXTIME(time_id,'%Y-%m-%d')<
 date_format(curdate(),'%Y-%m-%d')
;

SELECT 
	Fecha,
	Time,
	Franja,
	Intervalo,
	Llamante,
	Cola,
	Espera,
	Duracion,
	Administrada,
	Contador,
	Atendida,
	Abandonada,
	Transferencia,
	Id,
	TiempoEnCola,
	NULL as question1,
	NULL as question2,
	NULL as FCR,
	b.descr_agente as Agente
FROM

(SELECT FROM_UNIXTIME(time_id,'%Y-%m-%d')AS Fecha
,FROM_UNIXTIME(time_id,'%H:%i:%s')AS Time,null as Franja,
IF(FROM_UNIXTIME(time_id,'%i')<30,
concat(FROM_UNIXTIME(time_id,'%H'),'00'),
concat(FROM_UNIXTIME(time_id,'%H'),'30'))AS Intervalo,
Llamante,Cola,Espera,Duracion,Administrada,Contador,Atendida,Abandonada,Transferencia,Id,TiempoEnCola
FROM
(SELECT VERB,DATA2 AS Duracion,call_id,agent as
Administrada,'1' as Atendida,'0' as Abandonada,'0' as Transferencia
,'1' as Contador,call_id as id
 FROM sampleQueue WHERE VERB IN ('COMPLETEAGENT','COMPLETECALLER')
 AND queue in ('54646','54646-2','43556')
)A
LEFT JOIN
(SELECT VERB,DATA2 AS Llamante,call_id,time_id,CASE 1 WHEN
FROM_UNIXTIME(time_id,'%Y-%m-%d')<'2013-09-09' 
THEN 'linio Post/Pre' WHEN queue='54646' THEN 'Pre-Venta' WHEN queue='54646-2'
THEN 
'Post-Venta' WHEN queue='43556' THEN 'HelloFood' END as Cola FROM sampleQueue
WHERE VERB IN ('ENTERQUEUE')
 AND queue in ('54646','54646-2','43556')
)B
ON A.call_id=B.call_id
LEFT JOIN
(SELECT VERB,DATA3 AS Espera,DATA1 AS TiempoEnCola,call_id FROM sampleQueue WHERE VERB IN
('CONNECT') AND queue in ('54646','54646-2','43556'))C
ON A.call_id=C.call_id


UNION ALL


SELECT FROM_UNIXTIME(time_id,'%Y-%m-%d')AS Fecha
,FROM_UNIXTIME(time_id,'%H:%i:%s')AS Time,null as Franja
,IF(FROM_UNIXTIME(time_id,'%i')<30,
concat(FROM_UNIXTIME(time_id,'%H'),'00'),
concat(FROM_UNIXTIME(time_id,'%H'),'30'))AS Intervalo,
Llamante,Cola,Espera,Duracion,Administrada,Contador,Atendida,Abandonada,Transferencia,id,TiempoEnCola
FROM
(SELECT VERB,DATA3 AS Espera,null AS Duracion,call_id,null as Administrada,'0'
as Atendida,'1' as Abandonada,'0' as Transferencia,NULL as TiempoEnCola
,'1' as Contador,call_id as id
 FROM sampleQueue WHERE VERB IN ('ABANDON') AND queue in
('54646','54646-2','43556'))A
LEFT JOIN
(SELECT VERB,DATA2 AS Llamante,call_id,time_id,CASE 1 WHEN
FROM_UNIXTIME(time_id,'%Y-%m-%d')<'2013-09-09' THEN 'linio Post/Pre' WHEN
queue='54646' THEN 'Pre-Venta' WHEN queue='54646-2' THEN 'Post-Venta' WHEN
queue='43556'   THEN 'HelloFood' END as Cola FROM sampleQueue WHERE VERB IN
('ENTERQUEUE') AND queue in ('54646','54646-2','43556'))B
ON A.call_id=B.call_id


UNION ALL


SELECT FROM_UNIXTIME(time_id,'%Y-%m-%d')AS Fecha
,FROM_UNIXTIME(time_id,'%H:%i:%s')AS Time,null as Franja
,IF(FROM_UNIXTIME(time_id,'%i')<30,
concat(FROM_UNIXTIME(time_id,'%H'),'00'),
concat(FROM_UNIXTIME(time_id,'%H'),'30'))AS Intervalo,
Llamante,Cola,Espera,Duracion,Administrada,Contador,Atendida,Abandonada,Transferencia,id,TiempoEnCola
FROM
(SELECT VERB,DATA4 AS Duracion,call_id,agent as Administrada,'0' as
Atendida,'0' as Abandonada,'1' as Transferencia
,'1' as Contador,call_id as id
 FROM sampleQueue WHERE VERB IN ('TRANSFER') AND queue in
('54646','54646-2','43556'))A
LEFT JOIN
(SELECT VERB,DATA2 AS Llamante,call_id,time_id,CASE 1 WHEN
FROM_UNIXTIME(time_id,'%Y-%m-%d')<'2013-09-09' THEN 'linio Post/Pre' WHEN
queue='54646' THEN 'Pre-Venta' WHEN queue='54646-2' THEN 'Post-Venta' WHEN
queue='43556'   THEN 'HelloFood' END as Cola FROM sampleQueue WHERE VERB IN
('ENTERQUEUE') AND queue in ('54646','54646-2','43556'))B
ON A.call_id=B.call_id
LEFT JOIN
(SELECT VERB,DATA3 AS Espera,DATA1 as TiempoEnCola,call_id FROM sampleQueue WHERE VERB IN
('CONNECT') AND queue in ('54646','54646-2','43556'))C
ON A.call_id=C.call_id)a

LEFT JOIN
queuemetrics.agenti_noti b


ON a.Administrada=b.nome_agente
;