USE test;
DROP  TABLE if EXISTS sampleQueue;
CREATE  TABLE sampleQueue
select * from queuemetrics.queue_log  where FROM_UNIXTIME(time_id,'%Y-%m-%d')
#in ('2014-02-15')
>=
 date_sub(date_format(curdate(),'%Y-%m-%d'),interval 1 day)
and 
FROM_UNIXTIME(time_id,'%Y-%m-%d')<
date_format(curdate(),'%Y-%m-%d')
;

DROP TEMPORARY TABLE IF EXISTS tbl_in;
CREATE TEMPORARY TABLE tbl_in
SELECT 	queue,
				agent,
				FROM_UNIXTIME(time_id,'%Y-%m-%d') Fecha,
				MIN(FROM_UNIXTIME(time_id,'%Y-%m-%d %H:%i:%s')) Hora_entrada
FROM 
				sampleQueue
WHERE 
				VERB IN ('ADDMEMBER')
GROUP BY
				queue,
				agent,
				FROM_UNIXTIME(time_id,'%Y-%m-%d');


DROP TEMPORARY TABLE IF EXISTS tbl_out;
CREATE TEMPORARY TABLE tbl_out
SELECT 	queue,
				agent,
				FROM_UNIXTIME(time_id,'%Y-%m-%d') Fecha,
				MAX(FROM_UNIXTIME(time_id,'%Y-%m-%d %H:%i:%s')) Hora_salida
FROM 
				sampleQueue
WHERE 
				VERB IN ('REMOVEMEMBER')
GROUP BY
				queue,
				agent,
				FROM_UNIXTIME(time_id,'%Y-%m-%d');

SELECT 
				a.queue,
				a.agent,
				a.Fecha,
				Hora_entrada,
				Hora_salida,
((UNIX_TIMESTAMP(Hora_salida)-UNIX_TIMESTAMP(Hora_entrada))/60)/60/24 AS Tiempo_en_oficina
FROM
	tbl_in a
LEFT JOIN
	tbl_out b
ON 	a.queue=b.queue 
		AND 
		a.agent=b.agent
		AND
		a.Fecha=b.Fecha;