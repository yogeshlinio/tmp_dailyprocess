use customer_service;

SELECT  'Inicio pausas de los agentes',now();

#truncate table auxiliary_tbl_adjusted;

set @max_day=cast((select max(date) from auxiliary_tbl_adjusted)  as date);

delete from auxiliary_tbl_adjusted
where date>=@max_day;

#Ingresar datos de tiempos ajustando fecha y ordenados por hora y agente
insert into auxiliary_tbl_adjusted (id, datetime, date, hour, extension, event, queue)
select id, concat(date,' ',hour) as datetime, date, hour, extension, event, queue from
(select id,date(date) date, hour, extension, event, queue
 from customer_service.auxiliary_tbl) t
where date>=@max_day
order by extension asc, concat(date,' ',hour) asc ;

truncate table bi_ops_cc_pauses_mx;

#Ingresar diferencias de tiempos
insert into bi_ops_cc_pauses_mx(id_tbl,datetime1, datetime2, dif, extension, queue, event)
SELECT f1.id_tbl,f1.datetime datetime1, f2.datetime datetime2, SEC_TO_TIME( UNIX_TIMESTAMP( f2.datetime ) - UNIX_TIMESTAMP( f1.datetime ) ) AS dif,
f1.extension, f1.queue,concat(f1.event,"-",f2.event) event
FROM auxiliary_tbl_adjusted f1
inner JOIN auxiliary_tbl_adjusted f2 ON f1.id_tbl +1= f2.id_tbl
and f1.datetime<f2.datetime and date(f1.datetime)=date(f2.datetime)
and f1.extension=f2.extension
GROUP BY f1.datetime,f1.extension
ORDER BY f1.id_tbl;

#Actualizar pausas de zendesk y breaks
update bi_ops_cc_pauses_mx
set
type='Zendesk'
where event ='start_zendesk-finish_zendesk';

update bi_ops_cc_pauses_mx
set
type='Break'
where event ='start_break-finish_break';

#Actualizar nombre de los agentes
update bi_ops_cc_pauses_mx a inner join
		tbl_staff_customer_service b ON a.Extension = b.Extension
set a.nombre=b.agente;

#Actualizar tiempo en segundos
update bi_ops_cc_pauses_mx 
set
tiempo_segundos=time_to_sec(dif);

SELECT  'Fin pausas de los agentes',now();