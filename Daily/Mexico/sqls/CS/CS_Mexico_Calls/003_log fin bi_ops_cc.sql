INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Mexico', 
  'bi_ops_general_cc',
  'finish',
  NOW(),
  MAX(event_date),
  count(*),
  count(*)
FROM
  customer_service.bi_ops_cc_mx;