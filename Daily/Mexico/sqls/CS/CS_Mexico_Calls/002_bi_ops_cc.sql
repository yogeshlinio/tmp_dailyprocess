
SET @MAXDATE =(SELECT MAX(event_date) FROM customer_service.bi_ops_cc_mx);

DELETE FROM customer_service.bi_ops_cc_mx WHERE event_date >=@MAXDATE;

INSERT INTO bi_ops_cc_mx (queue_stats_id,uniqueid)
SELECT	
	call_id as queue_stats_id,
	call_id as uniqueid
FROM
(SELECT DISTINCT call_id FROM queue_log
WHERE FROM_UNIXTIME(time_id,'%Y-%m-%d')>=@MAXDATE AND verb in 
('CONNECT','ABANDON'))a;

UPDATE bi_ops_cc_mx a
INNER JOIN
(SELECT * from queue_log WHERE VERB IN 
('COMPLETEAGENT','COMPLETECALLER') AND queue in ('54646','54646-2','43556') AND FROM_UNIXTIME(time_id,'%Y-%m-%d')>=@MAXDATE)b
ON a.queue_stats_id=b.call_id
SET call_duration_seg=DATA2,
agent_ext=b.agent,answered_in_wh=1
;

UPDATE bi_ops_cc_mx a
INNER JOIN
(SELECT * from queue_log WHERE VERB IN ('ENTERQUEUE') 
AND queue in ('54646','54646-2','43556')AND FROM_UNIXTIME(time_id,'%Y-%m-%d')>=@MAXDATE)b
ON a.queue_stats_id=b.call_id
SET number=DATA2,
event_date=FROM_UNIXTIME(time_id,'%Y-%m-%d'),
event_datetime=FROM_UNIXTIME(time_id,'%Y-%m-%d %H:%i:%s'),
a.queue=CASE 1 WHEN
FROM_UNIXTIME(time_id,'%Y-%m-%d')<'2013-09-09' 
THEN 'linio Post/Pre' WHEN b.queue='54646' THEN 'Pre-Venta' WHEN b.queue='54646-2'
THEN 
'Post-Venta' WHEN b.queue='43556' THEN 'HelloFood' END
;

UPDATE bi_ops_cc_mx a
INNER JOIN
(SELECT * from queue_log WHERE VERB IN ('CONNECT') 
AND     queue in ('54646','54646-2','43556') AND FROM_UNIXTIME(time_id,'%Y-%m-%d')>=@MAXDATE)b
ON      a.queue_stats_id=b.call_id
SET     hold_duration_seg=DATA3,agent_hold_time_seg
=DATA1
;


UPDATE bi_ops_cc_mx a
INNER JOIN
(SELECT * from queue_log WHERE VERB IN ('ABANDON')
AND FROM_UNIXTIME(time_id,'%Y-%m-%d')>=@MAXDATE)b
ON      a.queue_stats_id=b.call_id
SET     hold_duration_seg=DATA3,unanswered_in_wh=1
;


UPDATE bi_ops_cc_mx a
INNER JOIN
(SELECT * from queue_log WHERE VERB IN ('TRANSFER') 
AND FROM_UNIXTIME(time_id,'%Y-%m-%d')>=@MAXDATE)b
ON  a.queue_stats_id=b.call_id
SET call_duration_seg=DATA4,
transfered_to_queue=1,answered_in_wh=1,answered_in_wh=1,
agent_ext=b.agent
;
#No disponible
/*update bi_ops_cc_mx a 
inner join 
customer_service.cdr b 
on a.uniqueid=b.uniqueid
set 
a.record=b.record;*/

UPDATE bi_ops_cc_mx
SET 
	event_hour=HOUR(event_datetime),
	event_minute=MINUTE(event_datetime),
	event_shift=IF(MINUTE(event_datetime)<30,
	concat(date_format(event_datetime,'%H'),'00'),
	concat(date_format(event_datetime,'%H'),'30')),
	event_weekday=WEEKDAY(event_datetime),
	event_week=WEEK(event_datetime,0),
	event_month=MONTH(event_datetime),
	event_year=YEAR(event_datetime),
	pre=if(queue='Pre-Venta',1,0),
	pre_answ=if(queue='Pre-Venta' AND (answered_in_wh=1 or transfered_to_queue=1),1,0),
	pos=if(queue='Post-Venta',1,0),
	pos_answ=if(queue='Post-Venta' AND (answered_in_wh=1 or transfered_to_queue=1),1,0),
	net_event=1,
	answered_in_wh_20=if((answered_in_wh=1 OR transfered_to_queue=1) AND hold_duration_seg<20,1,0),
	unanswered_in_wh_cust=if(unanswered_in_wh=1 AND hold_duration_seg<5,1,0),
	transfered_from_queue=if(LENGTH(CAST(number AS CHAR))=4,1,0);

#update bi_ops_cc_mx 
#SET answered_in_wh=1
#WHERE transfered_to_queue=1;

UPDATE
customer_service.bi_ops_cc_mx a
left join
customer_service.tbl_48_franjas b
on a.event_shift=b.Id
 set a.Franja=b.Franja
#where a.Franja is not null
;

update customer_service.bi_ops_cc_mx a
left join
customer_service.tbl_staff_ev b
on RIGHT(a.agent_ext,4)=b.Extension
SET agent_name=Nombre
WHERE agent_name is NULL
;

update customer_service.bi_ops_cc_mx a
left join
customer_service.agenti_noti b
on RIGHT(a.agent_ext,4)=RIGHT(b.nome_agente,4)
SET agent_name=descr_agente
WHERE agent_name is NULL
;


/*
UPDATE customer_service.bi_ops_cc_mx SET resolution =1
if(question1=1 and question2=1,1,null);

UPDATE customer_service.bi_ops_cc_mx SET resolution = 0
where question1>=0 and FCR IS NULL;*/
