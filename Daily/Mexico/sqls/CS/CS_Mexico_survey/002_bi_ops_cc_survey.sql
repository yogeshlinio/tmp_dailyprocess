use customer_service;


#Datos ultimo d�a
set @max_day=cast((select max(call_date) from bi_ops_cc_survey_mx) as date);


delete from bi_ops_cc_survey_mx
where call_date>=@max_day;

#truncate table bi_ops_cc_survey;

SELECT  'Insertar datos a la tabla inicial',now();
insert into bi_ops_cc_survey_mx(uniqueid, callerid, call_datetime, call_date, call_time, agent_name, agent_ext, queue) 
select a.id, a.callerid, b.event_datetime, b.event_date, time(b.event_datetime), agent_name, agent_ext, queue  from questions a 
inner join bi_ops_cc_mx b
on a.id=b.queue_stats_id
where b.event_date>=@max_day
group by a.id;


SELECT  'Pregunta 1', now();
update bi_ops_cc_survey_mx a 
inner join questions b
on a.uniqueid=b.id
set
answer_1=if(answer1=1 or answer2=2,1,0),
answer1_option_1=if(answer1=1,1,0),
answer1_option_2=if(answer2=2,1,0)
where question=1;

SELECT  'Pregunta 2', now();
update bi_ops_cc_survey_mx a 
inner join questions b
on a.uniqueid=b.id
set
answer_2=if(answer1=1 or answer2=2,1,0),
answer2_option_1=if(answer1=1,1,0),
answer2_option_2=if(answer2=2,1,0)
where question=2;

SELECT  'Pregunta 3', now();
update bi_ops_cc_survey_mx a 
inner join questions b
on a.uniqueid=b.id
set
answer_3=if(answer1=1 or answer2=2 or answer3=3,1,0),
answer3_option_1=if(answer1=1,1,0),
answer3_option_2=if(answer2=2,1,0),
answer3_option_3=if(answer3=3,1,0)
where question=3;

SELECT  'Actualizar respuestas', now();
update bi_ops_cc_survey_mx
set
respuestas=answer_1+answer_2+answer_3;


update bi_ops_cc_survey_mx
set 
abandon_in=1
where respuestas=0 ;

SELECT  'Abandonados', now();
update bi_ops_cc_survey_mx
set 
abandon_in=if(respuestas=0,1,0),
answer1_abandon=if(answer_1=0,1,0),
answer2_abandon=if(answer_2=0,1,0),
answer3_abandon=if(answer_3=0,1,0);

SELECT  'Ingresar datos en la tabla principal', now();

#Cambio de la encuesta de satisfacci�n/First contact resolution
update bi_ops_cc_mx a inner join bi_ops_cc_survey_mx b
on a.uniqueid=b.uniqueid
set
a.transfered_survey=1,
a.answered_3=answer_3,
a.good=answer3_option_1,
a.regular=answer3_option_2,
a.answ_1_2=if(answer_1=1 and answer_2=1,1,0),
a.first_contact=answer2_option_1,
a.resolution=answer1_option_1,
a.answered_1=answer_1,
a.answered_2=answer_2
where a.transfered_survey =0;



SELECT  'Rutina finalizada Survey mx',now();