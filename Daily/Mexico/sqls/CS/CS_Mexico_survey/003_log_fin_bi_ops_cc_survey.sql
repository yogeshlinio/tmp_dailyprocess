INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Mexico', 
  'bi_ops_cc_survey_mx',
  'fin',
  NOW(),
  MAX(call_datetime),
  count(*),
  count(*)
FROM
  customer_service.bi_ops_cc_survey_mx;
