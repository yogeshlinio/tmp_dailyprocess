INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  step,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Mexico',
  'B2B',
  'start',
  NOW(),
  max(date),
  '0',
  '0'
FROM
  customer_service.rep_b2b_sales ;


