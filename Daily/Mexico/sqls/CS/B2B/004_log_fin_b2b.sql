INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  step,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Mexico',
  'B2B',
  'finish',
  NOW(),
  max(date),
  count(*),
  count(date)
FROM
customer_service.rep_b2b_sales ;

