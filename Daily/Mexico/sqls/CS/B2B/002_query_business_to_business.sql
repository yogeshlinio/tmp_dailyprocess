SELECT 'Start B2B Report';
SELECT 'SECTION NET_SALES';
DROP TEMPORARY TABLE IF EXISTS b2b_net_sales;
CREATE TEMPORARY TABLE b2b_net_sales (KEY(yearM),KEY(CouponCode))
AS
SELECT yearM,CouponCode,COUNT(OrderNUm)New_transactions_MTD,SUM(netRev)Net_revenue_MTD,
SUM(if(NewReturning='NEW',1,0))NewCustomers FROM 
(
SELECT
	date_format
(if(Installment>1,`development_mx`.`A_Master`.`Date`,`development_mx`.`A_Master`.`DateCollected`),'%Y%m') AS `yearM`,
NewReturning,
		`development_mx`.`A_Master`.`OrderNum` AS `OrderNUm`,
	`development_mx`.`A_Master`.`CouponCode` AS `CouponCode`,
	sum(
		`development_mx`.`A_Master`.`Rev`
	) AS `netRev`
FROM
	`development_mx`.`A_Master`
WHERE
	(
		(
			`development_mx`.`A_Master`.`OrderAfterCan` = 1
		)
		AND (
			date_format
(if(Installment>1,`development_mx`.`A_Master`.`Date`,`development_mx`.`A_Master`.`DateCollected`),'%Y%m') >= '201309'
		)
		AND (
			`development_mx`.`A_Master`.`CouponCode` <> ''
		)
	)
GROUP BY
	date_format(if(Installment>1,`development_mx`.`A_Master`.`Date`,`development_mx`.`A_Master`.`DateCollected`),'%Y%m') ,
	`development_mx`.`A_Master`.`OrderNum`,
	`development_mx`.`A_Master`.`CouponCode`,
	NewReturning
)a
GROUP BY yearM,CouponCode;
SELECT 'GROSS SALES';


DROP TEMPORARY TABLE IF EXISTS b2b_gross_sales;
CREATE TEMPORARY TABLE b2b_gross_sales (KEY(yearM),KEY(CouponCode))
AS
SELECT yearM,CouponCode,COUNT(OrderNUm)New_transactions_MTD_Gross,SUM(grossRev)Gross_revenue_MTD FROM 
(SELECT
	date_format(`development_mx`.`A_Master`.`Date`,'%Y%m'	) AS `yearM`,
	`development_mx`.`A_Master`.`OrderNum` AS `OrderNUm`,
	`development_mx`.`A_Master`.`CouponCode` AS `CouponCode`,
	sum(`development_mx`.`A_Master`.`Rev`) AS `grossRev`
FROM
	`development_mx`.`A_Master`
WHERE
	(
		(
			`development_mx`.`A_Master`.`OrderBeforeCan` = 1
		)
		AND (
			date_format(
				`development_mx`.`A_Master`.`Date`,
				'%Y%m'
			) >= '201309'
		)
		AND (
			`development_mx`.`A_Master`.`CouponCode` <> ''
		)
	)
GROUP BY
	date_format(`development_mx`.`A_Master`.`Date`,'%Y%m'),
	`development_mx`.`A_Master`.`CouponCode`,	
	`development_mx`.`A_Master`.`OrderNum`
)a
GROUP BY yearM,CouponCode;

DROP TEMPORARY TABLE IF EXISTS coupon_list;
CREATE TEMPORARY TABLE coupon_list 
AS

SELECT * FROM tbl_coupon_ev
UNION ALL 
SELECT proyecto as person,Fecha as Date,Voucher as coupon_code,'corporate' as type
FROM tbl_coupon_coporate_sales
;

SELECT 'INSERT REP B2B';
TRUNCATE customer_service.rep_b2b_sales;
INSERT INTO  customer_service.rep_b2b_sales 

SELECT
	person,
	date_format(a.date, '%Y%m')date,
	type,
	SUM(d.New_transactions_MTD_Gross) New_transactions_MTD_Gross,
	SUM(d.Gross_revenue_MTD) Gross_revenue_MTD,
	sum(b.New_transactions_MTD) New_transactions_MTD,
	SUM(b.Net_revenue_MTD) Net_revenue_MTD,
	sum(b.NewCustomers) NewCustomers
FROM
	coupon_list a
INNER JOIN b2b_net_sales b ON a.coupon_code LIKE b.CouponCode
AND date_format(a.date, '%Y%m') = b.yearM

INNER JOIN b2b_gross_sales d ON a.coupon_code LIKE d.CouponCode
AND date_format(a.date, '%Y%m') = d.yearM
#WHERE date='201404'
	#type = 'corporate'

GROUP BY
	person,
	date_format(a.date, '%Y%m'),type

UNION ALL

SELECT 
		agent_name as person,
		date_format(Date, '%Y%m') as date,
		'telesales' as type,
		sum(grossOrder) as New_transactions_MTD_Gross,
		sum(grossRev)as Gross_revenue_MTD,
		sum(netOrder)New_transactions_MTD,
		sum(netRev)Net_revenue_MTD,
		0 as NewCustomers
FROM 
	rep_detalle_ventas_dia
WHERE date is NOT NULL
GROUP BY
	person,
	date_format(Date, '%Y%m')
;



SELECT 'Ranking Telesales';

DROP TABLE rep_rank_telesales;
CREATE TABLE rep_rank_telesales (Ranking int auto_increment ,PRIMARY KEY (Ranking))
SELECT 
		NULL as Ranking,
		agent_name as Agent,
		sum(grossOrder) as New_transactions_MTD_Gross,
		sum(netOrder)New_transactions_MTD,		
		sum(grossRev)as Gross_revenue_MTD,
		sum(netRev)Net_revenue_MTD,
		SUM(pendingOrder)Pending_Orders,
		SUM(pendingRev)Pending_Revenue

FROM 
	rep_detalle_ventas_dia
WHERE date is NOT NULL and date_format(Date, '%Y%m') ='201405'
GROUP BY
	agent_name,
	date_format(Date, '%Y%m')
ORDER BY Net_revenue_MTD DESC
;



SELECT 'Ranking DirectSales';
DROP TABLE rep_rank_direct_sales;
CREATE TABLE rep_rank_direct_sales (Ranking int auto_increment ,PRIMARY KEY (Ranking))
SELECT null as Ranking,person,Net_revenue_MTD,New_transactions_MTD FROM `rep_b2b_sales`
WHERE type='individual' and date ='201405'
ORDER BY Net_revenue_MTD DESC;