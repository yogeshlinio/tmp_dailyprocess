/*
Reemplaza a call sp_sales_Report;
*/
#----------------------------------------------------------------------
#   Insert into table_monitoring_log START
#----------------------------------------------------------------------
#########Sales TS MX

INSERT INTO production.table_monitoring_log
SELECT
null,
"Mexico",
"tbl_sales_hist_ev",
"Start",
now(),
now(),
0,
0
;
DROP TEMPORARY TABLE IF EXISTS A_Master_ev;
CREATE TEMPORARY TABLE A_Master_ev (key(OrderNum))
SELECT 
	Master.OrderNum,
	Master.ItemID,
  Master.OriginalPrice,
	Master.Rev,
	Master.Price,
  Master.CouponCode,
  Master.CouponValue,
  Master.PaidPrice,
  Master.TaxPercent,
  Master.PaidPriceAfterTax,

  Master.Cancellations,
	Master.OrderAfterCan,
	Master.OrderBeforeCan,
  Master.Returns,
  
	Master.Status,
	Master.PaymentMethod,
  Master.Installment,
	channel.channel_group as ChannelGroup,
	Master.DateCollected,
	Master.DateDelivered,
	Master.Date,
	Master.Time,
	Master.AssistedSalesOperator
FROM 
	    development_mx.A_Master as Master
LEFT JOIN
			marketing_report.channel_report_mx channel
ON `Master`.OrderNum=channel.OrderNum
WHERE 
				Master.AssistedSalesOperator	<>''
 ;
#INSERT INTO tbl_sales_hist_ev
DROP TABLE IF EXISTS tbl_sales_hist_ev;
CREATE TABLE tbl_sales_hist_ev ( 
                                 PRIMARY KEY ( Date, telesales,agent_Name, OrderNum, ItemID ) , 
                                         KEY ( telesales) , 
                                         KEY ( OrderNum ),
                                         KEY ( ItemID )
 )
SELECT
	Date,
	Master.Time,
	'MX' as Pais,
	Master.assistedsalesoperator AS telesales,
	cast( Nombre as char(50) ) AS agent_Name,
	NULL AS identification,
	Master.OrderNum,
	Master.ItemID,
    Master.OriginalPrice,
	Master.Rev,
	Master.Price,
    Master.CouponCode,
    Master.CouponValue,
    Master.PaidPrice,
    Master.TaxPercent,
    Master.PaidPriceAfterTax,

    Master.Cancellations,
	Master.OrderAfterCan,
	Master.OrderBeforeCan,
    Master.Returns,
  
	Master.Status,
	Master.PaymentMethod,
    Master.Installment,
	Master.ChannelGroup,
	Master.DateCollected,
	Master.DateDelivered
FROM
	        tbl_staff_ev a
LEFT JOIN A_Master_ev AS Master 
       ON a.telesales = Master.assistedsalesoperator AND a.telesales is not null
WHERE date IS NOT NULL
ORDER BY Date,OrderNum, ItemId
;

TRUNCATE rep_detalle_ventas_dia;
INSERT INTO rep_detalle_ventas_dia (
Date,
agent_Name
)
SELECT DISTINCT Date,agent_Name FROM(
/*SELECT DISTINCT
	case when DateCollected = '0000-00-00' then date else Datecollected end  As Date,
	agent_Name
FROM
	tbl_sales_hist_ev
UNION ALL*/
SELECT DISTINCT
	Date,
	agent_Name
FROM
	tbl_sales_hist_ev
)a
ORDER BY Date
;


DROP TEMPORARY TABLE IF EXISTS grossTABLE;
CREATE TEMPORARY TABLE grossTABLE (key(Date,agent_Name))

SELECT 
	Date,
	agent_Name,
	COUNT(OrderNum) AS gross_Ord,
	SUM(grossRev) AS gross_Rev 

FROM 
(	SELECT
		Date,
		agent_Name,
		OrderNum,
		sum(Rev)grossRev 
	FROM 
		tbl_sales_hist_ev
	
	
	
	GROUP BY
		Date,
		agent_Name,
		OrderNum
)A
GROUP BY
	date,
	agent_Name
;

UPDATE customer_service.rep_detalle_ventas_dia A
LEFT JOIN
grossTABLE B
ON A.agent_name=B.agent_Name and A.Date=B.Date
SET grossOrder=gross_Ord;


UPDATE customer_service.rep_detalle_ventas_dia A
LEFT JOIN
grossTABLE B
ON A.agent_name=B.agent_Name and A.Date=B.Date
SET grossRev=gross_rev;


DROP TEMPORARY TABLE IF EXISTS netTABLE;
CREATE TEMPORARY TABLE netTABLE (key(Date,agent_Name))
SELECT 
	Date,
	agent_Name,
	COUNT(OrderNum) AS net_Ord,
	SUM(netRev) AS net_Rev 

FROM 
(	SELECT
Datecollected  As Date,		


		agent_Name,
		OrderNum,
		SUM(Rev)netRev 
	FROM 
		tbl_sales_hist_ev
	WHERE
		 
OrderAfterCan=1
		
	
	GROUP BY
		 Datecollected ,
		agent_Name,
		OrderNum
)A
GROUP BY
	Date,
	agent_Name
;

UPDATE customer_service.rep_detalle_ventas_dia A
LEFT JOIN
netTABLE B
ON A.agent_name=B.agent_Name and A.Date=B.Date
SET netOrder=net_Ord;


UPDATE customer_service.rep_detalle_ventas_dia A
LEFT JOIN
netTABLE B
ON A.agent_name=B.agent_Name and A.Date=B.Date
SET netRev=net_Rev;



DROP TEMPORARY TABLE IF EXISTS pendingTABLE;
CREATE TEMPORARY TABLE pendingTABLE (key(Date,agent_Name))

SELECT 
	Date,
	agent_Name,
	COUNT(OrderNum) AS peding_Ord,
	SUM(grossRev) AS pending_Rev 

FROM 
(	SELECT
		Date,
		agent_Name,
		OrderNum,
		sum(Rev)grossRev 
	FROM 
		tbl_sales_hist_ev
where `Status` in ('payment_confirmation_pending_reminded_2','manual_fraud_check_pending','payment_pending','payment_confirmation_pending_reminded_1')
	
	
	
	GROUP BY
		Date,
		agent_Name,
		OrderNum
)A
GROUP BY
	Date,
	agent_Name
;


UPDATE customer_service.rep_detalle_ventas_dia A
LEFT JOIN
pendingTABLE B
ON A.agent_name=B.agent_Name and A.Date=B.Date
SET pendingOrder=peding_Ord;


UPDATE customer_service.rep_detalle_ventas_dia A
LEFT JOIN
pendingTABLE B
ON A.agent_name=B.agent_Name and A.Date=B.Date
SET pendingRev=pending_Rev;

#----------------------Llamadas----------------------------------------

UPDATE customer_service.rep_detalle_ventas_dia A
LEFT JOIN
VW_LlamadasxAsesorPreVenta B
ON A.agent_name=B.Agente and A.Date=B.Fecha
SET Llamadas=Atn;

TRUNCATE rep_detalle_ventas_mes;
INSERT INTO rep_detalle_ventas_mes 
SELECT 
DATE_FORMAT(date,'%Y%m')Date,
agent_name,
sum(grossOrder)grossOrder,
sum(grossRev)grossRev,
SUM(netOrder)netOrder,
SUM(netRev)netRev,
null as targetOrdAch,
null as targetRevAch,
sum(pendingOrder)pendingOrder,
SUM(pendingRev)pendingRev

FROM customer_service.rep_detalle_ventas_dia
GROUP BY DATE_FORMAT(date,'%Y%m'),agent_name
;

#----------------------------------------------------------------------
#   Insert into table_monitoring_log finish
#----------------------------------------------------------------------
INSERT INTO production.table_monitoring_log
SELECT
   null,
   "Mexico",
   "tbl_sales_hist_ev",
   "Finish",
   now(),
   now(),
   COUNT(*),
   COUNT(*)
FROM 
   tbl_sales_hist_ev
;