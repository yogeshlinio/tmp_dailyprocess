use customer_service_co;

#Zona horaria de Colombia

set time_zone='-5:00';

set @max_day=cast((select max(event_date) from bi_ops_cc_outbound_dyalogo)  as date);

select @max_day;

delete from bi_ops_cc_outbound_dyalogo
where date(event_date)>=@max_day;
#truncate table bi_ops_cc_outbound_dyalogo;

SELECT  'Insertar datos bi_ops_cc_outbound',now();
insert into bi_ops_cc_outbound_dyalogo 
(uniqueid, event_datetime, event_date, agent_name, agent_ext, identification, event, total_duration_seg, call_duration_seg,
hold_duration_seg, event_hour, event_minute, event_weekday, event_week, event_month, event_year, campana,type, number, record)
SELECT 
unique_id, fecha_hora, date(fecha_hora), agente, extension, identificacion, resultado, duracion_total, duracion_al_aire, 
tiempo_espera, hour(fecha_hora), minute(fecha_hora), weekday(fecha_hora),weekofyear(fecha_hora),monthname(fecha_hora),year(fecha_hora), campana,tipo_llamada,numero_telefonico, grabacion
FROM CS_dyalogo.dy_llamadas_espejo a left join
CS_dyalogo.dy_agentes b on a.id_agente_espejo=b.id
where sentido='Saliente'
and date(fecha_hora)>=@max_day;

SELECT  'Agregar proceso',now();

update bi_ops_cc_outbound_dyalogo a inner join 
bi_ops_matrix_ventas b on cast(a.identification as unsigned)=cast(b.identification as unsigned) 
set process_ventas=b.process
where process_sac is null and process_ventas is null;

update bi_ops_cc_outbound_dyalogo a inner join 
bi_ops_matrix_sac b on cast(a.identification as unsigned)=cast(b.identification as unsigned) 
set process_sac=b.process
where process_sac is null and process_ventas is null;

update bi_ops_cc_outbound_dyalogo
set event_shift=if(event_minute<30,concat(event_hour,':','00'),concat(event_hour,':','30'))
where event_date>=@max_day;

SELECT  'Actualizar binarios de tiempos',now();
update bi_ops_cc_outbound_dyalogo
set 
answered=if(event ='Contestada',1,0),
busy=if(event ='Ocupado',1,0),
unanswered=if(event ='No Contestada',1,0),
calls=if(event in ('Fallida','Ocupado'),0,1)
where event_date>=@max_day;

UPDATE bi_ops_cc_outbound_dyalogo
SET outbound=1 where 
process='OUTBOUND'
and event_date>=@max_day;

#Actualizar tipos de procesos
update customer_service_co.bi_ops_cc_outbound_dyalogo 
set process=process_ventas
where process is null;

update customer_service_co.bi_ops_cc_outbound_dyalogo 
set process=process_sac
where process is null;

update customer_service_co.bi_ops_cc_outbound_dyalogo 
set 
outbound=if(process like '%OUTBOUND%',1,0)
where outbound is null;

update customer_service_co.bi_ops_cc_outbound_dyalogo 
set 
inb_ventas=if(process like '%VENTAS%',1,0)
where inb_ventas is null;

update customer_service_co.bi_ops_cc_outbound_dyalogo 
set 
inb_ventas=if(process like '%VENTAS%',1,0)
where inb_ventas is null;

update customer_service_co.bi_ops_cc_outbound_dyalogo 
set 
inb_sac=if(process like '%SAC%',1,0)
where inb_sac is null;

update customer_service_co.bi_ops_cc_outbound_dyalogo 
set 
back_office=if(process like '%BACK OFFICE%',1,0)
where back_office is null;

update customer_service_co.bi_ops_cc_outbound_dyalogo 
set 
chat=if(process like '%CHAT%',1,0)
where chat is null;

update customer_service_co.bi_ops_cc_outbound_dyalogo 
set 
facebook=if(process like '%Facebook%',1,0)
where facebook is null;

update customer_service_co.bi_ops_cc_outbound_dyalogo 
set 
otros=if(outbound+inb_ventas+inb_sac+back_office+chat+facebook=0,1,0)
where otros is null;