INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'customer_service_co.bi_ops_cc_outbound',
  'finish',
  NOW(),
  MAX(event_datetime),
  count(*),
  count(*)
FROM
  customer_service_co.bi_ops_cc_outbound;