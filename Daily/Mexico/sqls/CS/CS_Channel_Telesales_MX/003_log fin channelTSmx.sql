INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Mexico', 
  'tbl_telesales_mx',
  'finish',
  NOW(),
  MAX(date),
  count(*),
  count(*)
FROM
  customer_service.tbl_telesales_mx;