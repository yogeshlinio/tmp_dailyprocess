DROP TABLE  IF EXISTS tbl_telesales_mx;
CREATE TABLE `tbl_telesales_mx` (
  `Date` date NOT NULL,
  `hour` int(2) DEFAULT NULL,
  `minute` int(2) DEFAULT NULL,
  `Campaign` binary(0) DEFAULT NULL,
  `AssistedSalesOperator` varchar(255) NOT NULL,
  `ChannelGroup` varchar(255) DEFAULT '',
  `channel` varchar(255) DEFAULT '',
  `OrderNum` varchar(45) NOT NULL,
  `ItemID` int(11) NOT NULL,
  `PaidPrice` decimal(15,5) NOT NULL,
  `Rev` double NOT NULL,
  `OrderAfterCan` bit(1) NOT NULL,
  `OrderBeforeCan` bit(1) NOT NULL,
  `OriginalPrice` decimal(15,5) NOT NULL,
  `Status` varchar(255) NOT NULL,
  `agent_ext` varchar(59) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `Identification` varchar(50) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `agent_Name` varchar(50) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `coordinator` varchar(50) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `process` varchar(50) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `source_medium` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Telesales` varchar(1) NOT NULL DEFAULT '',
  `other_channels` varchar(1) NOT NULL DEFAULT '',
  `NewReturning` varchar(255) NOT NULL,
  `PCOne` double NOT NULL,
  `PCOnePFive` double NOT NULL,
  `PCTwo` double NOT NULL,
  `DateCollected` date NOT NULL,
  `shift_date_order` binary(0) DEFAULT NULL,
  `DateDelivered` date NOT NULL,
  `PaymentMethod` varchar(50) NOT NULL,
  `Installment` int(11) NOT NULL,
  `Pais` varchar(2) NOT NULL DEFAULT '',
  `XR` decimal(8,4) DEFAULT NULL,
  `SKUName` varchar(255) NOT NULL,
  `Cat1` varchar(255) NOT NULL,
  `Cat2` varchar(255) NOT NULL,
  `Cat3` varchar(255) NOT NULL
)

SELECT 
	Master.Date,	
	HOUR(Master.Time) as hour,
	MINUTE(Master.Time) as minute,
	NULL as Campaign,
	Master.AssistedSalesOperator,
	channel.channel_group as ChannelGroup,
  channel.channel,
	Master.OrderNum,
	Master.ItemID,
	Master.PaidPrice,
	Master.Rev,
	Master.OrderAfterCan,
	Master.OrderBeforeCan,
  Master.OriginalPrice,
	Master.Status,	
	NULL AS agent_ext,
	NULL AS Identification,
	null as agent_Name,
	NULL AS coordinator,
	NULL AS process,
	channel.source_medium,
	'1' AS Telesales,
	'0' AS other_channels,
	NewReturning,
	PCOne,
	PCOnePFive,
	PCTwo,
	Master.DateCollected,
	NULL as shift_date_order,
	Master.DateDelivered,
  Master.PaymentMethod,
  Master.Installment,
	'MX' as Pais,  
	XR,
	SKUName,
	Cat1,
	Cat2,
	Cat3
	
FROM 
	    (SELECT * FROM development_mx.A_Master)as Master
LEFT JOIN
			marketing_mx.channel_report channel
ON `Master`.OrderNum=channel.OrderNum
LEFT JOIN 
			(SELECT * FROM development_mx.A_E_BI_ExchangeRate_USD WHERE Country='Mex') Ex
ON DATE_FORMAT(`Master`.date,'%Y%m')=Ex.Month_Num
WHERE 
		channel.channel_group	='Telesales' 
 ;

UPDATE customer_service.tbl_telesales_mx a
INNER JOIN 
			customer_service.tbl_staff_ev b
ON a.AssistedSalesOperator=b.TELESALES
set a.agent_ext=b.Extension;