SELECT *  FROM questions 
WHERE 
date_format(
cast(
concat(
LEFT (substring(`date`, 7, 7), 4),
substring(`date`, 3, 3),
'-',
LEFT (`date`, 2),
' ',
REPLACE (RIGHT(`date`, 8), '.', ':')
) AS datetime
),
'%Y-%m-%d'
)
# between '2013-12-30' and '2013-12-31';
 >=date_format(DATE_SUB(curdate(),INTERVAL 1 DAY),'%Y-%m-%d') and 
 `date`<DATE_FORMAT(curdate(),'%d-%m-%Y')
;


