INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  step,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Regional',
  'CC_Rep_Preventa_Telesales',
  'start',
  NOW(),
  max(Fecha),
  '0',
  '0'
FROM
  customer_service.test_rep_telesales_agt_regional


