DROP TABLE IF EXISTS tbl_sales_Hist_Consolidated;
CREATE TABLE tbl_sales_Hist_Consolidated
SELECT * FROM
tbl_sales_hist_ev
UNION ALL
SELECT * FROM
tbl_sales_hist_co;

SET @max_day = (	SELECT		max(Fecha)	FROM		customer_service.test_rep_telesales_agt_regional
WHERE Pais='CO');
SELECT 'Inicio';
DELETE FROM 	customer_service.test_rep_telesales_agt_regional WHERE	Fecha >=@max_day AND Pais='CO';

DROP TEMPORARY TABLE
IF EXISTS llamadasCO;

CREATE TEMPORARY TABLE llamadasCO (KEY(Fecha), KEY(Intervalo)) SELECT
	event_date AS Fecha,
	b.Franja AS Intervalo,
	agent_name AS Nombre,
	agent_identification,
	null AS Contador,
	null as Contador2,
	sum(net_event) Recibidas,
	sum(answered_in_wh) Atendidas,
	sum(unanswered_in_wh) Abandonadas,
	sum(call_duration_seg) Duracion,
	sum(hold_duration_seg) Espera,
	sum(agent_hold_time_seg) EsperaAgente,
	sum(answered_in_wh_20) Atendidas20,
	sum(transfered_to_queue) Transferencia,
	sum(good) Satisfaccion,
	sum(answered_3) Respuestas_Satisfaccion
FROM
	customer_service_co.bi_ops_cc a
LEFT JOIN tbl_48_franjas b ON a.event_shift = b.Id_Alt
WHERE
	pre = 1
AND net_event = 1
AND event_date >= '2013-05-01'
GROUP BY
	event_date,
	event_shift,
	agent_name
HAVING
	Fecha >=@max_day;

INSERT INTO test_rep_telesales_agt_regional 
SELECT
	a.Fecha,
	a.Franja,
	'CO' AS Pais,
	Nombre,
	agent_identification,
	Contador,
	Contador2,
	Recibidas,
	Atendidas,
	Abandonadas,
	Duracion,
	Espera,
	EsperaAgente,
	Atendidas20,
	Transferencia,
	Satisfaccion,
	Respuestas_Satisfaccion,
	NULL AS gross_Ord,
	NULL AS gross_Rev,
	NULL AS Items,
	NULL AS net_Ord,
	NULL AS net_Rev,
	NULL AS Tgross_orders,
	NULL AS Tnetorders,
	NULL AS TvalueGross,
	NULL AS TvalueNet,
	NULL as Semana
FROM
	customer_service.tbl_base_48 a
LEFT JOIN llamadasCO b ON a.Fecha = b.Fecha
AND a.Franja = b.Intervalo
WHERE
	a.Fecha < DATE(CURDATE())
AND a.Fecha >=@max_day;
SELECT 'Orders_CO';

#--------------------------Update Gross Orders co
#--------Tabla temporal gross co
DROP TEMPORARY TABLE IF EXISTS grosSalesCO;
CREATE TEMPORARY TABLE grosSalesCO(KEY(Date),KEY(identification),KEY(Intervalo))
SELECT 
	Date,
	identification,
	agent_Name,
	Intervalo,
	Franja,
	COUNT(OrderNum) AS gross_Ord,
	SUM(grossRev) AS gross_Rev,
	SUM(Items)Items

FROM 
(	SELECT
		Date,
		identification,
		agent_Name,
		if (
		minute(Time)		<30
		,CONCAT(hour(Time),'00'),CONCAT(hour(Time),'30')) as Intervalo,
		OrderNum,
		sum(Rev)grossRev,
		count(ItemID)As Items
	FROM 
		customer_service.tbl_sales_Hist_Consolidated
		WHERE
		OrderBeforeCan=1 AND Pais='CO'#and telesales=1
	GROUP BY
		Date,
		identification,
		agent_Name,
		if (minute(Time)<30		,CONCAT(hour(Time),'00'),CONCAT(hour(Time),'30')),
		OrderNum
)A
LEFT JOIN
		customer_service.tbl_48_franjas b
	ON A.Intervalo=b.Id
	
WHERE Date>='2013-05-01'
GROUP BY
	date,
	Intervalo,
	agent_Name
;


#-------------Update gross
#SELECT * FROM
UPDATE customer_service.test_rep_telesales_agt_regional a
LEFT JOIN 
grosSalesCO b
ON a.Fecha=b.date AND a.Franja=b.Franja AND a.agent_identification=b.identification
SET a.gross_Ord=b.gross_Ord , a.gross_Rev=b.gross_Rev , a.Items=b.Items
WHERE Pais='CO';

#-------------------------------Update Net CO 
#----------------Temporal NEt

DROP TEMPORARY TABLE IF EXISTS netSalesCO;
CREATE TEMPORARY TABLE netSalesCO(KEY(Date),KEY(identification),KEY(Intervalo))
SELECT 
	Date,
	identification,
	agent_Name,
	Franja,
	Intervalo,
	COUNT(OrderNum) AS net_Ord,
	SUM(netRev) AS net_Rev 

FROM 
(	
SELECT
 case when DateCollected = '0000-00-00' then date else Datecollected end as Date,		
 identification,

if (
		minute(Time)		<30
		,CONCAT(hour(Time),'00'),CONCAT(hour(Time),'30')) as Intervalo,
		agent_Name,
		OrderNum,
		SUM(Rev)netRev 
	FROM 
		customer_service.tbl_sales_Hist_Consolidated
	WHERE
		 
OrderAfterCan=1 AND Pais='CO'
		#AND telesales=1
	
	GROUP BY
		Date,
if (
		minute(Time)		<30
		,CONCAT(hour(Time),'00'),CONCAT(hour(Time),'30')),		
agent_Name,
identification,
		OrderNum
)A
LEFT JOIN
		customer_service.tbl_48_franjas b
	ON A.Intervalo=b.Id


GROUP BY
	Date,
	Intervalo,
agent_Name
;

#---------Update CO
UPDATE customer_service.test_rep_telesales_agt_regional a
LEFT JOIN 
netSalesCO b
ON a.Fecha=b.date AND a.Franja=b.Franja AND a.agent_identification=b.identification
SET a.net_Ord=b.net_Ord , a.net_Rev=b.net_Rev 
WHERE Pais='CO';


#---------------Total Ords CO
DROP TEMPORARY TABLE IF EXISTS totalOrders;
CREATE TEMPORARY TABLE totalOrders(KEY(Date))
SELECT
	date,'1' as Contador,
	count(DISTINCT(OrderNum)) Tgross_orders, sum(Rev) Value
FROM
	development_co_project.A_Master
WHERE
	OrderBeforeCan = 1
AND date >= '2014-01-01'
GROUP BY
	date/*,
	cast(
		(

			IF (
				MINUTE (HOUR) < 30,
				concat(HOUR(HOUR), ':', '00'),
				concat(HOUR(HOUR), ':', '30')
			)
		) AS time
	)*/
;

#---Update Total orders co
UPDATE customer_service.test_rep_telesales_agt_regional a
LEFT JOIN 
totalOrders b
ON a.Fecha=b.date #AND a.Franja=b.Franja 
SET a.Tgross_orders=b.Tgross_orders , a.Contador=b.Contador,
a.TvalueGross=b.Value
WHERE Pais='CO';

#---------------Total Ords Net CO
DROP TEMPORARY TABLE IF EXISTS totalOrders;
CREATE TEMPORARY TABLE totalOrders(KEY(Date))
SELECT
	DateCollected date,
	'1' as Contador,
	count(DISTINCT(OrderNum)) Tnet_orders, sum(Rev) ValueNet
FROM
	development_co_project.A_Master
WHERE
	OrderAfterCan = 1
AND DateCollected >= '2014-01-01'
GROUP BY
	date/*,
	cast(
		(

			IF (
				MINUTE (HOUR) < 30,
				concat(HOUR(HOUR), ':', '00'),
				concat(HOUR(HOUR), ':', '30')
			)
		) AS time
	)*/
;
#---Update Total orders co
UPDATE customer_service.test_rep_telesales_agt_regional a
LEFT JOIN 
totalOrders b
ON a.Fecha=b.date #AND a.Franja=b.Franja 
SET a.Tnetorders=b.Tnet_orders , a.Contador2=b.Contador,
a.TvalueNet=b.ValueNet
WHERE Pais='CO';


###################################################
#################------M�xico-----#################
#################-----------------#################
###################################################

SET @max_dayMX = (	SELECT		max(Fecha)	FROM		customer_service.test_rep_telesales_agt_regional
WHERE Pais='MX');
SELECT 'Orders_MX';
DELETE FROM 	customer_service.test_rep_telesales_agt_regional WHERE	Fecha >=@max_dayMX
AND Pais='MX';


DROP TEMPORARY TABLE IF EXISTS llamadasMx;
CREATE TEMPORARY TABLE llamadasMx (KEY(Fecha), KEY(Franja))
SELECT
		a.Fecha,
	a.Franja,
	Agente AS Nombre,
	NULL as agent_identification,
	null AS Contador,
	null as Contador2,
	Recibidas,
	Atendidas,
	Abandonadas,
	Duracion,
	Espera,
	EsperaAgente,
	Transferencia,
	Atendidas20,
	FCR as Satisfaccion,
	Peticiones as Respuestas_Satisfaccion
FROM
	(
select Fecha,Franja,Agente,
sum(if (cola='Pre-Venta',Contador,0)) Recibidas,
			sum(if (cola='Pre-Venta',Atendida,0))+sum(if (cola='Pre-Venta',Transferencia,0)) Atendidas,
			sum(if (cola='Pre-Venta',Abandonada,0)) Abandonadas,
			sum(if (cola='Pre-Venta',Duracion,0)) Duracion,
			sum(if (cola='Pre-Venta',Transferencia,0)) Transferencia,
			sum(if (cola='Post-Venta',TiempoEnCola,0)) Espera,
			sum(if (cola='Post-Venta',Espera,0)) EsperaAgente,
			sum(if (cola='Pre-Venta' AND Espera<=20,Atendida,0))+sum(if (cola='Pre-Venta' AND Espera<=20,Transferencia,0)) Atendidas20,
			SUM(if (cola='Pre-Venta' ,FCR,0))FCR,
			COUNT(if (cola='Pre-Venta' ,question2,0))Peticiones
FROM
customer_service.tbl_queueMetrics30
GROUP BY Fecha,Franja,Agente)a  
HAVING Fecha >=@max_dayMX
;

INSERT INTO test_rep_telesales_agt_regional 
SELECT
	a.Fecha,
	a.Franja,
	'MX' AS Pais,
	Nombre,
	agent_identification,
	Contador,
	Contador2,
	Recibidas,
	Atendidas,
	Abandonadas,
	Duracion,
	Espera,
	EsperaAgente,
	Atendidas20,
	Transferencia,
	Satisfaccion,
	Respuestas_Satisfaccion,
	NULL AS gross_Ord,
	NULL AS gross_Rev,
	NULL AS Items,
	NULL AS net_Ord,
	NULL AS net_Rev,
	NULL AS Tgross_orders,
	NULL AS Tnetorders,
	NULL AS TvalueGross,
	NULL AS TvalueNet,
	NULL AS Semana
FROM
	tbl_base_48 a
LEFT JOIN llamadasMx b ON a.Fecha = b.Fecha
AND a.Franja = b.Franja
WHERE
	a.Fecha < DATE(CURDATE())
AND a.Fecha >=@max_dayMX
;

##GRoss Orders MX
DROP TEMPORARY TABLE IF EXISTS grosSalesMX;
CREATE TEMPORARY TABLE grosSalesMX(KEY(Date),KEY(agent_Name),KEY(Franja))
SELECT 
	Date,
	agent_Name,
	Franja,
	COUNT(OrderNUm) AS gross_Ord,
	SUM(grossRev) AS gross_Rev,
	SUM(Items)Items

FROM 
(	SELECT
		Date,
		agent_Name,
		if (
		DATE_FORMAT(Time,'%i')		<30
		,CONCAT(DATE_FORMAT(Time,'%H'),'00'),CONCAT(DATE_FORMAT(Time,'%H'),'30')) as Intervalo,
		OrderNum,
		sum(Rev)grossRev,
		count(ItemID)As Items
	FROM 
		customer_service.tbl_sales_Hist_Consolidated
	WHERE OrderBeforeCan=1 AND Pais='MX'
	GROUP BY
		Date,
		agent_Name,
		if (DATE_FORMAT(Time,'%i')<30,CONCAT(DATE_FORMAT(Time,'%H'),'00'),CONCAT(DATE_FORMAT(Time,'%H'),'30')),
		OrderNum
 
)A
LEFT JOIN
customer_service.tbl_48_franjas B
ON cast(A.Intervalo as CHAR)=B.Id
GROUP BY
	date,
	Intervalo
,agent_Name
;

#-------------Update gross
#SELECT * FROM
UPDATE customer_service.test_rep_telesales_agt_regional a
LEFT JOIN 
grosSalesMX b
ON a.Fecha=b.date AND a.Franja=b.Franja AND a.Nombre=b.agent_Name
SET a.gross_Ord=b.gross_Ord , a.gross_Rev=b.gross_Rev , a.Items=b.Items
WHERE Pais='MX';

#3############Net Sales mx

DROP TEMPORARY TABLE IF EXISTS netSalesMX;
CREATE TEMPORARY TABLE netSalesMX(KEY(Date),KEY(agent_Name),KEY(Franja))
SELECT 
	Date,
	agent_Name,
	Franja,
	COUNT(OrderNum) AS net_Ord,
	SUM(netRev) AS net_Rev 

FROM 
(	SELECT
case when DateCollected = '0000-00-00' then date else Datecollected end  As Date,		

if (DATE_FORMAT(Time,'%i')<30,CONCAT(DATE_FORMAT(Time,'%H'),'00'),CONCAT(DATE_FORMAT(Time,'%H'),'30')) as Intervalo,
		agent_Name,
		OrderNum,
		SUM(Rev)netRev 
	FROM 
		tbl_sales_Hist_Consolidated
	WHERE
		 OrderAfterCan=1 AND Pais='MX'
	GROUP BY
		date,
if (DATE_FORMAT(Time,'%i')<30,CONCAT(DATE_FORMAT(Time,'%H'),'00'),
CONCAT(DATE_FORMAT(Time,'%H'),'30')),		
agent_Name,
		OrderNum
)A
LEFT JOIN
customer_service.tbl_48_franjas B
ON cast(A.Intervalo as CHAR)=B.Id

GROUP BY
	Date,
	Intervalo,
agent_Name;

#---------Update CO
UPDATE 
#SELECT * FROM
customer_service.test_rep_telesales_agt_regional a
LEFT JOIN 
netSalesMX b
ON a.Fecha=b.date AND a.Franja=b.Franja AND a.Nombre=b.agent_Name
SET a.net_Ord=b.net_Ord , a.net_Rev=b.net_Rev 
WHERE Pais='MX';

#########
#Gross Orders Mx
DROP TEMPORARY TABLE IF EXISTS totalOrdersMX;
CREATE TEMPORARY TABLE totalOrdersMX(KEY(Date))
SELECT
	date,'1' as Contador,
	count(DISTINCT(OrderNum)) Tgross_orders,
	sum(Rev) Value
FROM
	development_mx.A_Master
WHERE
	OrderBeforeCan = 1
AND date >= '2014-01-01'
GROUP BY
	date
/*,
	cast(
		(

			IF (
				MINUTE (HOUR) < 30,
				concat(HOUR(HOUR), ':', '00'),
				concat(HOUR(HOUR), ':', '30')
			)
		) AS time
	)*/
;

SELECT 'Inter';
UPDATE
#SELECT * FROM 
customer_service.test_rep_telesales_agt_regional a
LEFT JOIN 
totalOrdersMX b
ON a.Fecha=b.date #AND a.Franja=b.Franja 
SET a.Tgross_orders=b.Tgross_orders , a.Contador=b.Contador,
a.TvalueGross=b.Value
WHERE Pais='MX';

#########
#Net Orders Mx
DROP TEMPORARY TABLE IF EXISTS totalOrdersMX;
CREATE TEMPORARY TABLE totalOrdersMX(KEY(Date))
SELECT
	DateCollected date,'1' as Contador,
	count(DISTINCT(OrderNum)) Tnet_orders,
	sum(Rev) ValueNet
FROM
	development_mx.A_Master
WHERE
	OrderAfterCan = 1
AND DateCollected >= '2014-01-01'
GROUP BY
	DateCollected
/*,
	cast(
		(

			IF (
				MINUTE (HOUR) < 30,
				concat(HOUR(HOUR), ':', '00'),
				concat(HOUR(HOUR), ':', '30')
			)
		) AS time
	)*/
;


UPDATE
#SELECT * FROM 
customer_service.test_rep_telesales_agt_regional a
LEFT JOIN 
totalOrdersMX b
ON a.Fecha=b.date #AND a.Franja=b.Franja 
SET a.Tnetorders=b.Tnet_orders , a.Contador2=b.Contador,
a.TvalueNet=b.ValueNet
WHERE Pais='MX';

UPDATE test_rep_telesales_agt_regional  SET Semana=date_format(fecha,'%Y-%u');

SELECT 'Fin';