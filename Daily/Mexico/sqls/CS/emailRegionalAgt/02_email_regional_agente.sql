#############unificacion de datos
truncate table customer_service.tbl_zendesk_general;

#Insertar datos de M�xico
insert into customer_service.tbl_zendesk_general
(summation_column,
Id,
Requester,
Requester_id,
Requester_external_id,
Requester_email,
Requester_domain,
Submitter,
Assignee,
Group_,
Subject_,
Tags,
Status_,
Priority,
Via,
Ticket_type,
Created_at,
Updated_at,
Assigned_at,
Organization,
Due_date,
Initially_assigned_at,
Solved_at,
Resolution_Time,
Satisfaction_Score,
Group_stations,
Assignee_stations,
Reopens,
Replies,
First_reply_time_in_minutes,
First_reply_time_in_minutes_within_business_hours,
First_resolution_time_in_minutes,
First_resolution_time_in_minutes_within_business_hours,
Full_resolution_time_in_minutes,
Full_resolution_time_in_minutes_within_business_hours,
Agent_wait_time_in_minutes,
Agent_wait_time_in_minutes_within_business_hours,
Requester_wait_time_in_minutes,
Requester_wait_time_in_minutes_within_businesshours,
On_hold_time_in_minutes,
On_hold_time_in_minutes_within_business_hours,
Canal_de_contacto,
Genera_Venta,
Numero_del_pedido,
Telefono,
SKU_1,
SKU_2,
Clasificacion,
pais)
select * from
(select a.*,'MX' from customer_service.tbl_zendesk a
group by Id) t;

#Insertar datos de Colombia
insert into customer_service.tbl_zendesk_general
(summation_column,
Id,
Requester,
Requester_id,
Requester_external_id,
Requester_email,
Requester_domain,
Submitter,
Assignee,
Group_,
Subject_,
Tags,
Status_,
Priority,
Via,
Ticket_type,
Created_at,
Updated_at,
Assigned_at,
Organization,
Due_date,
Initially_assigned_at,
Solved_at,
Resolution_Time,
Satisfaction_Score,
Group_stations,
Assignee_stations,
Reopens,
Replies,
First_reply_time_in_minutes,
First_reply_time_in_minutes_within_business_hours,
First_resolution_time_in_minutes,
First_resolution_time_in_minutes_within_business_hours,
Full_resolution_time_in_minutes,
Full_resolution_time_in_minutes_within_business_hours,
Agent_wait_time_in_minutes,
Agent_wait_time_in_minutes_within_business_hours,
Requester_wait_time_in_minutes,
Requester_wait_time_in_minutes_within_businesshours,
On_hold_time_in_minutes,
On_hold_time_in_minutes_within_business_hours,
Canal_de_contacto,
Clasificacion,
Numero_del_pedido,
Motivo_devolucion,
Retorno_del_producto,
Respuesta_quality,
Escalamiento_por_piezas_faltantes,
pais)
select * from
(select 
`Summation column`,
`Id`,
`Requester`,
`Requester id`,
`Requester external id`,
`Requester email`,
`Requester domain`,
`Submitter`,
`Assignee`,
`Group`,
`Subject`,
`Tags`,
`Status`,
`Priority`,
`Via`,
`Ticket type`,
`created_at`,
`Updated at`,
`Assigned at`,
`Organization`,
`Due date`,
`Initially assigned at`,
`Solved at`,
`Resolution time`,
`Satisfaction Score`,
`Group stations`,
`Assignee stations`,
`Reopens`,
`Replies`,
`First reply time in minutes`,
`First reply time in minutes within business hours`,
`First resolution time in minutes`,
`First resolution time in minutes within business hours`,
`Full resolution time in minutes`,
`Full resolution time in minutes within business hours`,
`Agent wait time in minutes`,
`Agent wait time in minutes within business hours`,
`Requester wait time in minutes`,
`Requester wait time in minutes within business hours`,
`On hold time in minutes`,
`On hold time in minutes within business hours`,
`Contacto Inicial [list]`,
`Tipo de solicitud [list]`,
`Order ID (BOB) [int]`,
`MOTIVO DE DEVOLUCI�N O CANCELACION [list]`,
`RETORNO DEL PRODUCTO [list]`,
`RESPUESTA QUALITY [list]`,
`ESCALAMIENTO POR PIEZAS FALTANTES [list]`,
'CO' from customer_service.tbl_zendesk_co a
group by Id) t;

#Insertar datos Per�

insert into customer_service.tbl_zendesk_general
(summation_column,
Id,
Requester,
Requester_id,
Requester_external_id,
Requester_email,
Requester_domain,
Submitter,
Subgrupo,
Group_,
Subject_,
Tags,
Status_,
Priority,
Via,
Ticket_type,
Created_at,
Updated_at,
Assigned_at,
Organization,
Due_date,
Initially_assigned_at,
Solved_at,
Resolution_Time,
Satisfaction_Score,
Group_stations,
Assignee_stations,
Reopens,
Replies,
First_reply_time_in_minutes,
First_reply_time_in_minutes_within_business_hours,
First_resolution_time_in_minutes,
First_resolution_time_in_minutes_within_business_hours,
Full_resolution_time_in_minutes,
Full_resolution_time_in_minutes_within_business_hours,
Agent_wait_time_in_minutes,
Agent_wait_time_in_minutes_within_business_hours,
Requester_wait_time_in_minutes,
Requester_wait_time_in_minutes_within_businesshours,
On_hold_time_in_minutes,
On_hold_time_in_minutes_within_business_hours,
Canal_de_contacto,
Assignee,
Clasificacion,
pais)
select * from
(select 
`Summation column`,
`Id`,
`Requester`,
`Requester id`,
`Requester external id`,
`Requester email`,
`Requester domain`,
`Submitter`,
`Assignee`,
`Group`,
`Subject`,
`Tags`,
`Status`,
`Priority`,
`Via`,
`Ticket type`,
`created_at`,
`Updated at`,
`Assigned at`,
`Organization`,
`Due date`,
`Initially assigned at`,
`Solved at`,
`Resolution time`,
`Satisfaction Score`,
`Group stations`,
`Assignee stations`,
`Reopens`,
`Replies`,
`First reply time in minutes`,
`First reply time in minutes within business hours`,
`First resolution time in minutes`,
`First resolution time in minutes within business hours`,
`Full resolution time in minutes`,
`Full resolution time in minutes within business hours`,
`Agent wait time in minutes`,
`Agent wait time in minutes within business hours`,
`Requester wait time in minutes`,
`Requester wait time in minutes within business hours`,
`On hold time in minutes`,
`On hold time in minutes within business hours`,
`Canal [list]`,
`Assignee [list]`,
`Tipo de solicitud [list]`
,
'PE' from customer_service.tbl_zendesk_pe a
group by Id) t;

#Insertar datos Venezuela

insert into customer_service.tbl_zendesk_general
(summation_column,
Id,
Requester,
Requester_id,
Requester_external_id,
Requester_email,
Requester_domain,
Submitter,
Assignee,
Group_,
Subject_,
Tags,
Status_,
Priority,
Via,
Ticket_type,
Created_at,
Updated_at,
Assigned_at,
Organization,
Due_date,
Initially_assigned_at,
Solved_at,
Resolution_Time,
Satisfaction_Score,
Group_stations,
Assignee_stations,
Reopens,
Replies,
First_reply_time_in_minutes,
First_reply_time_in_minutes_within_business_hours,
First_resolution_time_in_minutes,
First_resolution_time_in_minutes_within_business_hours,
Full_resolution_time_in_minutes,
Full_resolution_time_in_minutes_within_business_hours,
Agent_wait_time_in_minutes,
Agent_wait_time_in_minutes_within_business_hours,
Requester_wait_time_in_minutes,
Requester_wait_time_in_minutes_within_businesshours,
On_hold_time_in_minutes,
On_hold_time_in_minutes_within_business_hours,
Clasificacion,
Canal_de_contacto,
Numero_del_pedido,
SKU_1,
SKU_2,
Genera_Venta,
pais)
select * from
(select 
`Summation column`,
`Id`,
`Requester`,
`Requester id`,
`Requester external id`,
`Requester email`,
`Requester domain`,
`Submitter`,
`Assignee`,
`Group`,
`Subject`,
`Tags`,
`Status`,
`Priority`,
`Via`,
`Ticket type`,
`created_at`,
`Updated at`,
`Assigned at`,
`Organization`,
`Due date`,
`Initially assigned at`,
`Solved at`,
`Resolution time`,
`Satisfaction Score`,
`Group stations`,
`Assignee stations`,
`Reopens`,
`Replies`,
`First reply time in minutes`,
`First reply time in minutes within business hours`,
`First resolution time in minutes`,
`First resolution time in minutes within business hours`,
`Full resolution time in minutes`,
`Full resolution time in minutes within business hours`,
`Agent wait time in minutes`,
`Agent wait time in minutes within business hours`,
`Requester wait time in minutes`,
`Requester wait time in minutes within business hours`,
`On hold time in minutes`,
`On hold time in minutes within business hours`,
`Tipo [list]`,
`Canal de contacto [list]`,
`N�mero de pedido [txt]`,
`SKU 1 [txt]`,
`SKU 2 [txt]`,
`Ventas [list]`
,
'VE' from customer_service.tbl_zendesk_ve a
group by Id) t;

#-------------------------------------------------------------------
#Definir base de correos entrantes de Front Office para los 4 pa�ses

#Front Office M�xico
update customer_service.tbl_zendesk_general
set es_inc_front_office=1
where Pais='MX'
and `Group_` IN ('Front Office Email','Front Office','Front Office - CHAT','','-')
and via in ('Mail','email');

#Front Office Colombia
update customer_service.tbl_zendesk_general
set es_inc_front_office=1
where Pais='CO'
and `Group_` IN ('Front Office','','-')
and via in ('Mail','email');

#Front Office Per�
update customer_service.tbl_zendesk_general
set es_inc_front_office=1
where Pais='PE'
and (`Assignee`='-' or 
`Assignee` IN (SELECT `tbl_staff_zendesk_pe`.`Assignee [list]` FROM 
tbl_staff_zendesk_pe WHERE 
`tbl_staff_zendesk_pe`.`Direccion` = 'Front Office'))
and via in ('Mail','email');

#Front Office Venezuela
update customer_service.tbl_zendesk_general
set es_inc_front_office=1
where Pais='VE'
and Via in ('Mail','email') and `Group_` IN ('Front Office', '-');

#Definir base de Back Office para los 4 pa�ses

#Back Office M�xico
update customer_service.tbl_zendesk_general
set es_backoffice=1
where Pais='MX'
and (`Group_` like '%BO%'
or `Group_` like '%B.O.%')
and via in ('Mail','email');

#Back Office Colombia
update customer_service.tbl_zendesk_general
set es_backoffice=1
where Pais='CO'
and `Group_` not IN ('FRONT OFFICE','','-','LOG�STICA');

#Back Office Per�
update customer_service.tbl_zendesk_general
set es_backoffice=1
where Pais ='PE'
and es_inc_front_office=0;

#Back Office Venezuela
update customer_service.tbl_zendesk_general
set es_backoffice=1
where Pais='VE'
and `Group_` ='Back Office';

#################################################################
#Ajustar tiempo de primera respuesta

#update tbl_zendesk_general
#set
#First_reply_time_in_minutes=First_resolution_time_in_minutes
#where 
#(First_reply_time_in_minutes is null
#or First_reply_time_in_minutes="");


#FILTRO DE INGRESOS EN HORAS NO LABORALES

#M�xico
#LUN-VIE
update customer_service.tbl_zendesk_general
set despues_de_horario=1
where dayofweek(date(Created_at)) in (2,3,4,5,6)
and (hour(Created_at)>=23 or 
hour(Created_at)<8)
and pais='MX';

#S�BADO
update customer_service.tbl_zendesk_general
set despues_de_horario=1
where dayofweek(date(Created_at)) =7
and (hour(Created_at)>=19 or 
hour(Created_at)<9)
and pais='MX';

#DOMINGO
update customer_service.tbl_zendesk_general
set despues_de_horario=1
where dayofweek(date(Created_at)) =1
and (hour(Created_at)>=18 or 
hour(Created_at)<10)
and pais='MX';

#Colombia
#LUN-VIE
update customer_service.tbl_zendesk_general
set despues_de_horario=1
where dayofweek(date(Created_at)) in (2,3,4,5,6)
and (hour(Created_at)>=21 or 
hour(Created_at)<8)
and pais='CO';

#S�BADO
update customer_service.tbl_zendesk_general
set despues_de_horario=1
where dayofweek(date(Created_at)) =7
and (hour(Created_at)>=21 or 
hour(Created_at)<9)
and pais='CO';

#DOMINGO
update customer_service.tbl_zendesk_general
set despues_de_horario=1
where dayofweek(date(Created_at)) =1
and (hour(Created_at)>=18 or 
hour(Created_at)<10)
and pais='CO';

#Per�
#LUN-VIE
update customer_service.tbl_zendesk_general
set despues_de_horario=1
where dayofweek(date(Created_at)) in (2,3,4,5,6)
and (hour(Created_at)>=23 or 
hour(Created_at)<8)
and pais='PE';

#S�BADO
update customer_service.tbl_zendesk_general
set despues_de_horario=1
where dayofweek(date(Created_at)) =7
and (hour(Created_at)>=20 or 
hour(Created_at)<9)
and pais='PE';

#DOMINGO
update customer_service.tbl_zendesk_general
set despues_de_horario=1
where dayofweek(date(Created_at)) =1
and (hour(Created_at)>=20 or 
hour(Created_at)<9)
and pais='PE';

#Venezuela
#LUN-VIE
update customer_service.tbl_zendesk_general
set despues_de_horario=1
where dayofweek(date(Created_at)) in (2,3,4,5,6)
and (hour(Created_at)>=20 or 
hour(Created_at)<8)
and pais='VE';

#S�BADO
update customer_service.tbl_zendesk_general
set despues_de_horario=1
where dayofweek(date(Created_at)) =7
and (hour(Created_at)>=17 or 
hour(Created_at)<9)
and pais='VE';

#DOMINGO
update customer_service.tbl_zendesk_general
set despues_de_horario=1
where dayofweek(date(Created_at)) =1
and pais='VE';

#Tiempos ajustados de primera respuesta sin tener en cuenta horarios no laborales

#M�xico
update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date(created_at), interval 8 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='MX'
and dayofweek(date(Created_at)) in (2,3,4,5,6)
and hour(Created_at)<8;

update tbl_zendesk_general
set
created_at_ajustado= if(dayofweek(date(Created_at))=6,
DATE_ADD(date_add(date(created_at), interval 1 day), interval 9 hour),
DATE_ADD(date_add(date(created_at), interval 1 day), interval 8 hour)),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='MX'
and dayofweek(date(Created_at)) in (2,3,4,5,6)
and hour(Created_at)>=23;


update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date(created_at), interval 9 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='MX'
and dayofweek(date(Created_at))=7
and hour(Created_at)<9;

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date_add(date(created_at), interval 1 day), interval 10 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='MX'
and dayofweek(date(Created_at))=7
and hour(Created_at)>=19;

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date(created_at), interval 10 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='MX'
and dayofweek(date(Created_at))=1
and hour(Created_at)<10;

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date_add(date(created_at), interval 1 day), interval 8 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='MX'
and dayofweek(date(Created_at))=7
and hour(Created_at)>=18;

#Colombia

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date(created_at), interval 8 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='CO'
and dayofweek(date(Created_at)) in (2,3,4,5,6)
and hour(Created_at)<8;

update tbl_zendesk_general
set
created_at_ajustado= if(dayofweek(date(Created_at))=6,
DATE_ADD(date_add(date(created_at), interval 1 day), interval 9 hour),
DATE_ADD(date_add(date(created_at), interval 1 day), interval 8 hour)),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='CO'
and dayofweek(date(Created_at)) in (2,3,4,5,6)
and hour(Created_at)>=21;


update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date(created_at), interval 9 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='CO'
and dayofweek(date(Created_at))=7
and hour(Created_at)<9;

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date_add(date(created_at), interval 1 day), interval 10 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='CO'
and dayofweek(date(Created_at))=7
and hour(Created_at)>=21;

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date(created_at), interval 10 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='CO'
and dayofweek(date(Created_at))=1
and hour(Created_at)<10;

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date_add(date(created_at), interval 1 day), interval 8 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='CO'
and dayofweek(date(Created_at))=7
and hour(Created_at)>=18;

#Per�

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date(created_at), interval 8 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='PE'
and dayofweek(date(Created_at)) in (2,3,4,5,6)
and hour(Created_at)<8;

update tbl_zendesk_general
set
created_at_ajustado= if(dayofweek(date(Created_at))=6,
DATE_ADD(date_add(date(created_at), interval 1 day), interval 9 hour),
DATE_ADD(date_add(date(created_at), interval 1 day), interval 8 hour)),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='PE'
and dayofweek(date(Created_at)) in (2,3,4,5,6)
and hour(Created_at)>=23;


update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date(created_at), interval 9 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='PE'
and dayofweek(date(Created_at))=7
and hour(Created_at)<9;

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date_add(date(created_at), interval 1 day), interval 9 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='PE'
and dayofweek(date(Created_at))=7
and hour(Created_at)>=20;

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date(created_at), interval 9 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='PE'
and dayofweek(date(Created_at))=1
and hour(Created_at)<9;

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date_add(date(created_at), interval 1 day), interval 8 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='PE'
and dayofweek(date(Created_at))=7
and hour(Created_at)>=20;

#Venezuela

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date(created_at), interval 8 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='VE'
and dayofweek(date(Created_at)) in (2,3,4,5,6)
and hour(Created_at)<8;

update tbl_zendesk_general
set
created_at_ajustado= if(dayofweek(date(Created_at))=6,
DATE_ADD(date_add(date(created_at), interval 1 day), interval 9 hour),
DATE_ADD(date_add(date(created_at), interval 1 day), interval 8 hour)),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='VE'
and dayofweek(date(Created_at)) in (2,3,4,5,6)
and hour(Created_at)>=20;


update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date(created_at), interval 9 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='VE'
and dayofweek(date(Created_at))=7
and hour(Created_at)<9;

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date_add(date(created_at), interval 2 day), interval 8 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='VE'
and dayofweek(date(Created_at))=7
and hour(Created_at)>=17;

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date_add(date(created_at), interval 1 day), interval 8 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='VE'
and dayofweek(date(Created_at))=1;

#Ingresar nuevos tiempos de primera respuesta

update tbl_zendesk_general
set
first_reply_time_ajustado_time=if(created_at_ajustado>date_first_reply_time_ajustado,0,
TIMEDIFF(date_first_reply_time_ajustado,created_at_ajustado))
where despues_de_horario=1;

update tbl_zendesk_general
set
first_reply_time_ajustado=time_to_sec(first_reply_time_ajustado_time)/60
where despues_de_horario=1;

#Completar tiempos de primera respuesta
update tbl_zendesk_general
set first_reply_time_ajustado=First_reply_time_in_minutes
where first_reply_time_ajustado is null;

#Filtros generales
update tbl_zendesk_general
set 
solved=if (`Solved_at` <> '0000-00-00 00:00:00',1,0),
Fecha_creacion=cast(`Created_at` AS date),
Intervalo_creacion=IF((date_format(`Created_at`,'%i') < 30),concat(date_format(`Created_at`,'%H'),'00'),
					concat(date_format(`Created_at`,'%H'),'30')),
Fecha_solucion=cast(`Solved_at` AS date),
Intervalo_solucion=IF((date_format(`Solved_at`,'%i') < 30),concat(date_format(`Solved_at`,'%H'),'00'),
					concat(date_format(`Solved_at`,'%H'),'30'));

#Otros filtros (solucion el mismo d�a, soluci�n dentro de 4 horas)

update tbl_zendesk_general
set 
same_day=if(Fecha_creacion=Fecha_solucion,1,0),
4_horas_de_respuesta=if(first_reply_time_ajustado/60<4,1,0),
calificados=if(satisfaction_score in ('Bad','Good'),1,0),
calificados_bueno=if(satisfaction_score ='Good',1,0);

#Tiempos de soluci�n back office Colombia
update customer_service.tbl_zendesk_general a 
inner join customer_service_co.zendesk_tipos_de_solicitud_tiempos b
on a.Clasificacion=b.tipo_solicitud
set
time_to_reply_bo_hours=tiempo_primera_respuesta,
time_to_solve_bo_hours=tiempo_solucion
where pais='CO';

#Solucionados a tiempo colombia
update customer_service.tbl_zendesk_general a 
set 
reply_on_time_bo=if(First_reply_time_in_minutes<=time_to_reply_bo_hours*60,1,0),
solved_on_time_bo=if(Full_resolution_time_in_minutes<=time_to_solve_bo_hours*60,1,0)
where pais='CO'
and time_to_reply_bo_hours is not null
and solved=1;

#########################
#Fin unificacion de datos
#########################




SET @max_day = (SELECT max(Fecha) FROM customer_service.test_rep_agt_email_regional);

DELETE FROM customer_service.test_rep_agt_email_regional WHERE Fecha>=@max_day;

#Insertar intervalos Mexico
INSERT INTO test_rep_agt_email_regional 
(Fecha, Id, Franja, Pais, Assignee)

SELECT a.Fecha, a.Id, a.Franja, a.Pais, b.Assignee FROM
(SELECT *,'MX' as Pais FROM customer_service.tbl_base_48) a

LEFT JOIN

(SELECT DISTINCT Fecha,Assignee,Intervalo
FROM
(
SELECT DISTINCT	Fecha_creacion as Fecha, Assignee,Pais,Intervalo_creacion Intervalo
FROM
	tbl_zendesk_general
WHERE
	es_inc_front_office=1
	and pais='MX'
UNION ALL
SELECT DISTINCT	
	Fecha_solucion as Fecha1,
	Assignee,
	Pais,
	Intervalo_solucion
FROM
	`tbl_zendesk_general`
WHERE
	es_inc_front_office=1
	and pais='MX'
)a)b
on a.Fecha=b.Fecha AND a.Id=cast(b.Intervalo as CHAR)
WHERE a.Fecha>=@max_day AND a.Fecha<DATE(CURDATE());

#Insertar intervalos Colombia
INSERT INTO test_rep_agt_email_regional 
(Fecha, Id, Franja, Pais, Assignee)

SELECT a.Fecha, a.Id, a.Franja, a.Pais, b.Assignee FROM
(SELECT *,'CO' as Pais FROM customer_service.tbl_base_48) a

LEFT JOIN

(SELECT DISTINCT Fecha,Assignee,Intervalo
FROM
(
SELECT DISTINCT	Fecha_creacion as Fecha, Assignee,Pais,Intervalo_creacion Intervalo
FROM
	tbl_zendesk_general
WHERE
	es_inc_front_office=1
	and pais='CO'
UNION ALL
SELECT DISTINCT	
	Fecha_solucion as Fecha1,
	Assignee,
	Pais,
	Intervalo_solucion
FROM
	`tbl_zendesk_general`
WHERE
	es_inc_front_office=1
	and pais='CO'
)a)b
on a.Fecha=b.Fecha AND a.Id=cast(b.Intervalo as CHAR)
WHERE a.Fecha>=@max_day AND a.Fecha<DATE(CURDATE());

#Insertar intervalos Peru
INSERT INTO test_rep_agt_email_regional 
(Fecha, Id, Franja, Pais, Assignee)

SELECT a.Fecha, a.Id, a.Franja, a.Pais, b.Assignee FROM
(SELECT *,'PE' as Pais FROM customer_service.tbl_base_48) a

LEFT JOIN

(SELECT DISTINCT Fecha,Assignee,Intervalo
FROM
(
SELECT DISTINCT	Fecha_creacion as Fecha, Assignee,Pais,Intervalo_creacion Intervalo
FROM
	tbl_zendesk_general
WHERE
	es_inc_front_office=1
	and pais='PE'
UNION ALL
SELECT DISTINCT	
	Fecha_solucion as Fecha1,
	Assignee,
	Pais,
	Intervalo_solucion
FROM
	`tbl_zendesk_general`
WHERE
	es_inc_front_office=1
	and pais='PE'
)a)b
on a.Fecha=b.Fecha AND a.Id=cast(b.Intervalo as CHAR)
WHERE a.Fecha>=@max_day AND a.Fecha<DATE(CURDATE());

#Insertar intervalos Venezuela
INSERT INTO test_rep_agt_email_regional 
(Fecha, Id, Franja, Pais, Assignee)

SELECT a.Fecha, a.Id, a.Franja, a.Pais, b.Assignee FROM
(SELECT *,'VE' as Pais FROM customer_service.tbl_base_48) a

LEFT JOIN

(SELECT DISTINCT Fecha,Assignee,Intervalo
FROM
(
SELECT DISTINCT	Fecha_creacion as Fecha, Assignee,Pais,Intervalo_creacion Intervalo
FROM
	tbl_zendesk_general
WHERE
	es_inc_front_office=1
	and pais='VE'
UNION ALL
SELECT DISTINCT	
	Fecha_solucion as Fecha1,
	Assignee,
	Pais,
	Intervalo_solucion
FROM
	`tbl_zendesk_general`
WHERE
	es_inc_front_office=1
	and pais='VE'
)a)b
on a.Fecha=b.Fecha AND a.Id=cast(b.Intervalo as CHAR)
WHERE a.Fecha>=@max_day AND a.Fecha<DATE(CURDATE());

#Datos en base a la fecha de creaci�n
update test_rep_agt_email_regional a
inner join 
(select Pais, Fecha_creacion, Intervalo_creacion, Assignee, sum(summation_column) New_tickets,
sum(solved) solved_tickets,
sum(Full_resolution_time_in_minutes/60) Tiempo_Resolucion_Creacion,
sum(first_reply_time_ajustado/60) Tiempo_primera_respuesta_creacion,
sum(same_day) resuelto_mismo_dia
from tbl_zendesk_general
where es_inc_front_office=1
Group by Pais, Fecha_creacion, Intervalo_creacion, Assignee) b
on a.Fecha=b.Fecha_creacion
and a.Id=b.Intervalo_creacion
and a.Pais=b.Pais
and a.Assignee=b.Assignee
set
a.New_Tickets=b.New_tickets,
a.solved_tickets=b.solved_tickets,
a.Tiempo_Resolucion_Creacion=b.Tiempo_Resolucion_Creacion,
a.Tiempo_primera_respuesta_creacion=b.Tiempo_primera_respuesta_creacion,
a.resuelto_mismo_dia=b.resuelto_mismo_dia
WHERE a.Fecha>=@max_day AND a.Fecha<DATE(CURDATE());

#Datos en base a la fecha de solucion
update test_rep_agt_email_regional a
inner join 
(select Pais, Fecha_solucion, Intervalo_solucion, Assignee, 
sum(solved) solved_ticketsT,
sum(Full_resolution_time_in_minutes/60) Tiempo_Resolucion,
sum(first_reply_time_ajustado/60) Tiempo_primera_respuesta,
sum(calificados) Tickets_calificados,
sum(calificados_bueno) Tickets_calificados_bueno,
sum(4_horas_de_respuesta) Resuelto_en_4_hrs,
sum(First_resolution_time_in_minutes/60) Tiempo_Primera_Solucion,
avg(Reopens)*sum(solved) Reopens
from tbl_zendesk_general
where solved=1
and es_inc_front_office=1
Group by Pais, Fecha_solucion, Intervalo_solucion, Assignee) b
on a.Fecha=b.Fecha_solucion
and a.Id=b.Intervalo_solucion
and a.Pais=b.Pais
and a.Assignee=b.Assignee
set
a.solved_ticketsT=b.solved_ticketsT,
a.Tiempo_Resolucion=b.Tiempo_Resolucion,
a.Tiempo_primera_respuesta=b.Tiempo_primera_respuesta,
a.Tickets_calificados=b.Tickets_calificados,
a.Tickets_calificados_bueno=b.Tickets_calificados_bueno,
a.Resuelto_en_4_hrs=b.Resuelto_en_4_hrs,
a.Tiempo_Primera_Solucion=b.Tiempo_Primera_Solucion,
a.Reopens=b.Reopens
where Fecha>=@max_day AND Fecha<DATE(CURDATE());

#############BACKLOG#############################

#Llama la rutina que llena el backlog de correos entrantes
call bi_ops_cc_backlog_inc_fo;

#Actualiza los datos de backlog general
update test_rep_agt_email_regional a inner join 
backlog_incoming_tickets_by_country b
on a.Fecha=b.date
and a.Pais=b.pais
set 
Backlog_by_day=backlog,
backlog_3d=backlog_72h,
backlog_15d=`backlog<15d`,
backlog_30d=`backlog<1m`,
backlog_mayor_30d=`backlog>1m`;

#Actualizar numero de semana
UPDATE  test_rep_agt_email_regional SET Semana=date_format(Fecha,'%Y-%u');

