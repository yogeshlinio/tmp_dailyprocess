use customer_service;

truncate table rep_llamadas_agt_regional_day;

insert into rep_llamadas_agt_regional_day (Fecha, Pais, Agente, Identificacion, Atendidas, Duracion, EsperaAgente, Atendidas20, Transferencia, Satisfaccion,
Respuestas_Satisfaccion,FCR,Peticiones_FCR)

select 
    event_date As Fecha,
	'CO' as Pais,
	agent_name as Agente,
	agent_identification as Identificacion,
	sum(answered_in_wh) Atendidas,
	sum(call_duration_seg) Duracion,
	sum(agent_hold_time_seg) EsperaAgente,
    sum(answered_in_wh_20) Atendidas20,
    sum(transfered_to_queue) Transferencia,
	sum(good) Satisfaccion,
	sum(answered_3) Respuestas_Satisfaccion,
	sum(if(first_contact=1 and resolution=1,1,0)) FCR,
	sum(answ_1_2) Peticiones_FCR
from
    customer_service_co.bi_ops_cc_dyalogo
where
    pos = 1 and net_event = 1 
	and unanswered_in_wh=0
	and event_date>='2014-04-01'
group by event_date , agent_name

UNION all

select 
    event_date As Fecha,
	'MX' as Pais,
	agent_name as Agente,
	agent_identification as Identificacion,
	sum(answered_in_wh) Atendidas,
	sum(call_duration_seg) Duracion,
	sum(agent_hold_time_seg) EsperaAgente,
    sum(answered_in_wh_20) Atendidas20,
    sum(transfered_to_queue) Transferencia,
	sum(good) Satisfaccion,
	sum(answered_3) Respuestas_Satisfaccion,
	sum(if(first_contact=1 and resolution=1,1,0)) FCR,
	sum(answ_1_2) Peticiones_FCR
from
    customer_service.bi_ops_cc_mx
where
    pos = 1 and net_event = 1 
	and unanswered_in_wh=0
	and event_date>='2014-04-01'
group by event_date , agent_name

UNION ALL

select 
    event_date As Fecha,
	'PE' as Pais,
	agent_name as Agente,
	agent_identification as Identificacion,
	sum(answered_in_wh) Atendidas,
	sum(call_duration_seg) Duracion,
	sum(agent_hold_time_seg) EsperaAgente,
    sum(answered_in_wh_20) Atendidas20,
    sum(transfered_to_queue) Transferencia,
	sum(good) Satisfaccion,
	sum(answered_3) Respuestas_Satisfaccion,
	sum(if(first_contact=1 and resolution=1,1,0)) FCR,
	sum(answ_1_2) Peticiones_FCR
from
    customer_service_pe.bi_ops_cc_pe
where
    pos = 1 and net_event = 1 
	and unanswered_in_wh=0
	and event_date>='2014-04-01'
group by event_date , agent_name;

#Actualizar Holidays

update rep_llamadas_agt_regional_day a 
inner join operations_co.calendar b 
on a.Fecha=b.dt
set is_holiday=isholiday
where pais='CO';

update rep_llamadas_agt_regional_day a 
inner join operations_mx.calendar b 
on a.Fecha=b.dt
set is_holiday=isholiday
where pais='MX';


update rep_llamadas_agt_regional_day a 
inner join operations_pe.calendar b 
on a.Fecha=b.dt
set is_holiday=isholiday
where pais='PE';

update rep_llamadas_agt_regional_day a 
inner join operations_ve.calendar b 
on a.Fecha=b.dt
set is_holiday=isholiday
where pais='VE';

#Horas de llamada
update rep_llamadas_agt_regional_day 
set
HorasEnLlamada=Duracion/3600;

#Tiempos Colombia
update rep_llamadas_agt_regional_day a
inner join 
(select 
fecha, cedula, campana_dyalogo, sum(tiempo_conexion_segundos)/3600 horas_conexion, 
(sum(tiempo_conexion_segundos)-sum(total_tiempo_efectivo_segundos))/3600 horas_pausas
from customer_service_co.bi_ops_cc_adherencia_sac
where hora_conexion is not null
and fecha>='2014-04-01'
group by fecha, cedula) b
on cast(a.identificacion as unsigned)=cast(b.cedula as unsigned)
and a.Fecha=b.fecha
set
HorasConexion=horas_conexion,
HorasPausas=horas_pausas
where pais='CO';

#Agregar tiempos de conexi�n
update  rep_llamadas_agt_regional_day a 
inner join customer_service_co.bi_ops_matrix_sac b
on cast(a.identificacion as unsigned)=cast(b.identification as unsigned)
set
HorasProgramadas=if(duracion_turno='6.5',6.5,if(is_holiday=1 or weekday(fecha)>=5,5, 9)),
proceso=process;


#Tiempos de pausas M�xico
update rep_llamadas_agt_regional_day a
inner join
(select date(datetime1) date,
nombre,  sum(if(type='Break',tiempo_segundos,0))/3600 horas_break,
sum(if(type='Zendesk',tiempo_segundos,0))/3600 horas_zendesk
 from bi_ops_cc_pauses_mx
where type is not null
group by date(datetime1),
nombre) b
on a.agente=b.nombre
and a.fecha=b.date
set
HorasACW=horas_zendesk,
HorasPausas=horas_break
where pais='MX';

#Tiempos conexion y programaci�n M�xico
update rep_llamadas_agt_regional_day a
inner join
(select Fecha, AGENTE, Hora_entrada, Hora_salida, time_to_sec(TIMEDIFF(Hora_salida, Hora_entrada))/3600 horas_conexion,
JORNADA,PUESTO  from tbl_time_in_office a inner join
tbl_staff_customer_service b ON right(a.agent, 4) = b.Extension
where a.Fecha >= '2014-04-01') b
on a.agente=b.AGENTE
and a.fecha=b.Fecha
set
HorasConexion=horas_conexion,
HorasProgramadas=if(weekday(a.fecha)=6,6,if(weekday(a.fecha)=5,5, JORNADA)),
proceso=PUESTO
where pais='MX';

#Tiempos de conexi�n y programaci�n Per�
update  rep_llamadas_agt_regional_day t1
inner join 
(select date, Nombre, Puesto,sum(tiempo_conexion)/3600 conexion,
`HORAS BRUTAS LV` horario from customer_service_pe.time_in_office a 
inner join customer_service_pe.tbl_staff_CC_PE b on right(a.agent,3)=b.Extension
group by date, agent) t2
on t1.Agente=t2.Nombre
and t1.Fecha=t2.date
set
HorasConexion=conexion,
HorasProgramadas=if(weekday(t1.fecha)=6 or is_holiday=1,5,if(weekday(t1.fecha)=5,3, horario)),
proceso=Puesto
where pais='PE';

#Tiempos de pausas Per�
update rep_llamadas_agt_regional_day a
inner join
(select date(datetime1) date,
nombre,  sum(if(type is not null,tiempo_segundos,0))/3600 horas_break
 from customer_service_pe.bi_ops_cc_pauses_pe
where type is not null
group by date(datetime1),
nombre) b
on a.agente=b.nombre
and a.fecha=b.date
set
HorasPausas=horas_break
where pais='PE';

#Ajuste Horas Programadas
update rep_llamadas_agt_regional_day
set
HorasConexion=HorasProgramadas
where HorasConexion>12;

#Ajuste Pausas
update rep_llamadas_agt_regional_day
set HorasPausas=0
where HorasPausas>HorasConexion;

#Filtro de agentes de postventa
update rep_llamadas_agt_regional_day
set
postsales_agent=if(pais='CO' and proceso like '%SAC%',1,if(pais='MX' and proceso like '%FRONT OFFICE%',1,if(pais='PE' and proceso like '%Inbound Calls Agent%',1,0)));

UPDATE  rep_llamadas_agt_regional_day SET Semana=date_format(fecha,'%Y-%u');