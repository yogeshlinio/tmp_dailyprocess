
TRUNCATE rep_mkt_telesales;
INSERT INTO  rep_mkt_telesales ( Fecha,Assisted_Operator,Pais,Identification,XR,channel)
SELECT
DISTINCT 
		date as Fecha , 
		AssistedSalesOperator as Assisted_Operator,
		'CO' as Pais,
		null as Identification,
		XR,
		channel
FROM 
		tbl_telesales_co
WHERE Date>='2014-01-01'
;

UPDATE rep_mkt_telesales a
LEFT JOIN customer_service_co.bi_ops_matrix_ventas b
ON a.Assisted_Operator=b.sales_operator
SET a.Identification=b.identification,a.Nombre=b.`name`
,a.Coordinador=b.coordinator
,a.Schedule=b.shift_pattern
WHERE Pais='CO';

DROP TEMPORARY TABLE IF EXISTS llamadas;
CREATE TEMPORARY TABLE llamadas

select 
    event_date As Fecha,
		agent_name as Agente,
		agent_identification,
		'Inbound' as channel,
	sum(net_event) Recibidas,	
	sum(answered_in_wh) Atendidas,
	sum(unanswered_in_wh) Abandonadas,
	sum(call_duration_seg) Duracion,
    sum(hold_duration_seg) Espera,
	sum(agent_hold_time_seg) EsperaAgente,
    sum(answered_in_wh_20) Atendidas20,
    sum(transfered_to_queue) Transferencia,
	sum(good) Satisfaccion,
	sum(answered_3) Respuestas_Satisfaccion,
	sum(if(first_contact=1 and resolution=1,1,0)) FCR,
	sum(answ_1_2) Peticiones_FCR,
	sum(unanswered_in_wh_cust) Abandonadas_menor5
from
    customer_service_co.bi_ops_cc_dyalogo
where
    pre = 1 and net_event = 1 
group by event_date ,agent_identification
;

UPDATE rep_mkt_telesales a
LEFT JOIN llamadas b
ON a.Fecha =b.Fecha and a.Identification=b.agent_identification AND a.channel=b.channel
SET a.Recibidas=b.Recibidas,
a.Atendidas=b.Atendidas,
a.Abandonadas=b.Abandonadas,
a.Duracion=b.Duracion,
a.EsperaAgente=b.EsperaAgente,
a.Espera=b.Espera,
a.Transferencia=b.Transferencia,
a.Atendidas20=b.Atendidas20,
a.Respuestas_Satisfaccion=b.Respuestas_Satisfaccion,
a.Satisfaccion=b.Satisfaccion

WHERE a.Pais='CO';
USE customer_service;
DROP TEMPORARY TABLE IF EXISTS llamadasT;
CREATE TEMPORARY TABLE llamadasT
select 
    event_date As Fecha,
		'Inbound' as channel,
		agent_identification,
	sum(net_event) RecibidasT,	
	sum(answered_in_wh) Atendidas,
	sum(unanswered_in_wh) Abandonadas,
	sum(call_duration_seg) Duracion,
    sum(hold_duration_seg) Espera,
	sum(agent_hold_time_seg) EsperaAgente,
    sum(answered_in_wh_20) Atendidas20,
    sum(transfered_to_queue) Transferencia,
	sum(good) Satisfaccion,
	sum(answered_3) Respuestas_Satisfaccion,
	sum(if(first_contact=1 and resolution=1,1,0)) FCR,
	sum(answ_1_2) Peticiones_FCR,
	sum(unanswered_in_wh_cust) Abandonadas_menor5
from
    customer_service_co.bi_ops_cc_dyalogo
where
    pre = 1 and net_event = 1 
group by event_date,agent_identification
;


UPDATE rep_mkt_telesales a
LEFT JOIN llamadasT b
ON a.Fecha=b.Fecha AND a.channel=b.channel AND a.Identification=b.agent_identification
SET a.RecibidasT=b.RecibidasT
, Contador2=1
WHERE a.Pais='CO';


DROP TEMPORARY TABLE IF EXISTS grossTABLE;
CREATE TEMPORARY TABLE grossTABLE (key(Date))

SELECT 
	Date,
	agent_Name,
	AssistedSalesOperator,
	channel,
	COUNT(OrderNum) AS gross_Ord,
	SUM(grossRev) AS gross_Rev ,
		SUM(PCOne)PCOne,
		SUM(PCOnePFive)PCOnePFive,
		SUM(PCTwo)PCTwo

FROM 
(	SELECT
		Date,
		agent_Name,
		AssistedSalesOperator,
		OrderNum,
		channel,
		sum(Rev)grossRev,
		SUM(PCOne)PCOne,
		SUM(PCOnePFive)PCOnePFive,
		SUM(PCTwo)PCTwo
	FROM 
		tbl_telesales_co
	WHERE OrderBeforeCan=1
	GROUP BY
		Date,
		AssistedSalesOperator,
		OrderNum,channel
)A
GROUP BY
	date,
	AssistedSalesOperator,channel
;

UPDATE customer_service.rep_mkt_telesales A
LEFT JOIN
grossTABLE B
ON A.Assisted_Operator=B.AssistedSalesOperator and A.Fecha=B.Date and A.channel=B.channel 
SET A.gross_Ord=B.gross_Ord,
A.gross_Rev=B.gross_rev,
A.PCOne=B.PCOne,
A.PCOnePFive=B.PCOnePFive,
A.PCTwo=B.PCTwo
WHERE Pais='CO';


DROP TEMPORARY TABLE IF EXISTS netTABLE;
CREATE TEMPORARY TABLE netTABLE (key(Date))
SELECT 
	Date,
	agent_Name,
	AssistedSalesOperator,
	channel,
	COUNT(OrderNum) AS net_Ord,
	SUM(netRev) AS net_Rev,
	SUM(Items)Items

FROM 
(	SELECT
		Date,
		agent_Name,
		AssistedSalesOperator,
		channel,
		OrderNum,
		SUM(Rev)netRev,
		COUNT(OrderNum)Items
	FROM 
		tbl_telesales_co
	WHERE
		 
OrderAfterCan=1
		
	
	GROUP BY
			Date,
			AssistedSalesOperator,
			OrderNum,channel
)A
GROUP BY
	Date,
	AssistedSalesOperator,channel
;

UPDATE customer_service.rep_mkt_telesales A
LEFT JOIN
netTABLE B
ON A.Assisted_Operator=B.AssistedSalesOperator and A.Fecha=B.Date and A.channel=B.channel
SET A.net_Ord=B.net_Ord,A.net_Rev=B.net_Rev,
A.Items=B.Items
WHERE Pais='CO';


DROP TEMPORARY TABLE IF EXISTS newCustTable;
CREATE TEMPORARY TABLE newCustTable (key(Date))
SELECT 
	Date,
	AssistedSalesOperator,channel,
	SUM(IF(NewReturning='NEW',1,0))NewCust
FROM 
(	SELECT
		Date,
		AssistedSalesOperator,
		OrderNum,
		NewReturning,
		channel,
		sum(Rev)grossRev
		FROM 
		tbl_telesales_co
	WHERE OrderBeforeCan=1
	GROUP BY
		Date,
		AssistedSalesOperator,
		OrderNum,
		NewReturning,channel
)A
GROUP BY
	date,
	AssistedSalesOperator,channel
;

UPDATE customer_service.rep_mkt_telesales A
LEFT JOIN
newCustTable B
ON A.Assisted_Operator=B.AssistedSalesOperator and A.Fecha=B.Date and A.channel=B.channel
SET A.NewCust=B.NewCust
WHERE Pais='CO';

UPDATE customer_service.rep_mkt_telesales a
LEFT JOIN
(SELECT yrmonth,SUM(marketing_cost)cost FROM marketing.marketing_costs
WHERE country='COL' AND channel_group='Telesales'
GROUP BY yrmonth)b
ON DATE_FORMAT(a.Fecha,'%Y%m')=b.yrmonth
SET a.Cost=b.cost
WHERE a.Pais='CO'
;
##########################
#####MEXICO###############
##########################

INSERT INTO  rep_mkt_telesales ( Fecha,Assisted_Operator,Pais,Identification,XR,channel)
SELECT
DISTINCT 
		date as Fecha , 
		AssistedSalesOperator as Assisted_Operator,
		'MX' AS Pais,
		agent_ext AS Identification,
		XR,
		channel
FROM 
		tbl_telesales_mx
WHERE Date>='2014-01-01';

UPDATE rep_mkt_telesales a
LEFT JOIN tbl_staff_ev b
ON a.Assisted_Operator=b.TELESALES
SET a.Nombre=b.Nombre,
a.Coordinador=b.Coordinador,
a.Schedule=b.Horario
WHERE Pais='MX';

DROP TEMPORARY TABLE IF EXISTS llamadas;
CREATE TEMPORARY TABLE llamadas
SELECT
event_date as `Fecha`,
'MX' as `Pais`,
agent_name as `Nombre`,
agent_ext,
'Inbound' as channel,
SUM(net_event) as `Recibidas`,
SUM(answered_in_wh) as `Atendidas`,
SUM(unanswered_in_wh) as `Abandonadas`,
SUM(call_duration_seg) as `Duracion`,
SUM(hold_duration_seg) as `Espera`,
SUM(agent_hold_time_seg) as EsperaAgente,
SUM(transfered_to_queue) as `Transferencia`,
SUM(answered_in_wh_20) as `Atendidas20`,
SUM(good) Satisfaccion,
SUM(answered_3) Respuestas_Satisfaccion,
SUM(if(first_contact=1 and resolution=1,1,0)) FCR,
SUM(answ_1_2) Peticiones_FCR,
SUM(if(hold_duration_seg<5,unanswered_in_wh,0)) as `Abandonadas_menor5`,
NULL as `Semana`

FROM customer_service.bi_ops_cc_mx
WHERE pre=1
GROUP BY event_date,agent_ext
;

UPDATE rep_mkt_telesales a
LEFT JOIN llamadas b
ON a.Fecha =b.Fecha and a.Identification=RIGHT(b.agent_ext,4) AND a.channel=b.channel
SET a.Recibidas=b.Recibidas,
a.Atendidas=b.Atendidas,
a.Abandonadas=b.Abandonadas,
a.Duracion=b.Duracion,
a.EsperaAgente=b.EsperaAgente,
a.Espera=b.Espera,
a.Transferencia=b.Transferencia,
a.Atendidas20=b.Atendidas20,
a.Respuestas_Satisfaccion=b.Respuestas_Satisfaccion,
a.Satisfaccion=b.Satisfaccion
WHERE a.Pais='MX';


DROP TEMPORARY TABLE IF EXISTS llamadasT;
CREATE TEMPORARY TABLE llamadasT
SELECT
event_date as `Fecha`,
agent_ext,
'Inbound' as channel,
SUM(net_event) as `RecibidasT`
FROM customer_service.bi_ops_cc_mx
WHERE pre=1
GROUP BY event_date,agent_ext
;


UPDATE rep_mkt_telesales a
LEFT JOIN llamadasT b
ON a.Fecha =b.Fecha and a.Identification=RIGHT(b.agent_ext,4) AND a.channel=b.channel
SET a.RecibidasT=b.RecibidasT
,Contador2=1
WHERE a.Pais='MX';


DROP TEMPORARY TABLE IF EXISTS grossTABLE;
CREATE TEMPORARY TABLE grossTABLE (key(Date))

SELECT 
	Date,
	agent_Name,
	channel,
	AssistedSalesOperator,
	COUNT(OrderNum) AS gross_Ord,
	SUM(grossRev) AS gross_Rev ,
		SUM(PCOne)PCOne,
		SUM(PCOnePFive)PCOnePFive,
		SUM(PCTwo)PCTwo

FROM 
(	SELECT
		Date,
		agent_Name,
		AssistedSalesOperator,
		OrderNum,
		channel,
		sum(Rev)grossRev ,
		SUM(PCOne)PCOne,
		SUM(PCOnePFive)PCOnePFive,
		SUM(PCTwo)PCTwo
	FROM 
		tbl_telesales_mx
	WHERE OrderBeforeCan=1
	GROUP BY
		Date,
		AssistedSalesOperator,
		OrderNum,channel
)A
GROUP BY
	date,
	AssistedSalesOperator,channel
;

UPDATE customer_service.rep_mkt_telesales A
LEFT JOIN
grossTABLE B
ON A.Assisted_Operator=B.AssistedSalesOperator and A.Fecha=B.Date and A.channel=B.channel
SET A.gross_Ord=B.gross_Ord,
A.gross_Rev=B.gross_rev,
A.PCOne=B.PCOne,
A.PCOnePFive=B.PCOnePFive,
A.PCTwo=B.PCTwo
WHERE Pais='MX';


DROP TEMPORARY TABLE IF EXISTS netTABLE;
CREATE TEMPORARY TABLE netTABLE (key(Date))
SELECT 
	Date,
	agent_Name,
	AssistedSalesOperator,
	channel,
	COUNT(OrderNum) AS net_Ord,
	SUM(netRev) AS net_Rev ,
	SUM(Items)Items

FROM 
(	SELECT
		Date,
		agent_Name,
		AssistedSalesOperator,
		OrderNum,
		channel,
		SUM(Rev)netRev ,
		COUNT(OrderNum)Items
	FROM 
		tbl_telesales_mx
	WHERE
		 
OrderAfterCan=1
		
	
	GROUP BY
			Date,
			AssistedSalesOperator,
			OrderNum,channel
)A
GROUP BY
	Date,
	AssistedSalesOperator,channel
;

UPDATE customer_service.rep_mkt_telesales A
LEFT JOIN
netTABLE B
ON A.Assisted_Operator=B.AssistedSalesOperator and A.Fecha=B.Date and A.channel=B.channel
SET A.net_Ord=B.net_Ord,A.net_Rev=B.net_Rev,A.Items=B.Items
WHERE Pais='MX';


DROP TEMPORARY TABLE IF EXISTS newCustTable;
CREATE TEMPORARY TABLE newCustTable (key(Date))
SELECT 
	Date,
	AssistedSalesOperator,
	channel,
	SUM(IF(NewReturning='NEW',1,0))NewCust
FROM 
(	SELECT
		Date,
		AssistedSalesOperator,
		OrderNum,
		NewReturning,
		channel,
		sum(Rev)grossRev
		FROM 
		tbl_telesales_mx
	WHERE OrderBeforeCan=1
	GROUP BY
		Date,
		AssistedSalesOperator,
		OrderNum,
		NewReturning,channel
)A
GROUP BY
	date,
	AssistedSalesOperator,channel
;

UPDATE customer_service.rep_mkt_telesales A
LEFT JOIN
newCustTable B
ON A.Assisted_Operator=B.AssistedSalesOperator and A.Fecha=B.Date and A.channel=B.channel
SET A.NewCust=B.NewCust
WHERE Pais='MX';

UPDATE customer_service.rep_mkt_telesales a
LEFT JOIN
(SELECT yrmonth,SUM(marketing_cost)cost FROM marketing.marketing_costs
WHERE country='MEX' AND channel_group='Telesales'
GROUP BY yrmonth)b
ON DATE_FORMAT(a.Fecha,'%Y%m')=b.yrmonth
SET a.Cost=b.cost
WHERE a.Pais='MX'
;
UPDATE customer_service.rep_mkt_telesales
SET channel_aux=IF(Pais='CO' AND channel in ('Inbound','New Register','Unknown-CS'),'Inbound',channel)
WHERE Pais='CO';

UPDATE customer_service.rep_mkt_telesales
SET channel_aux=IF(Pais='MX' AND 
channel in (
'Inbound',
'New Register',
'Unknown-CS',
'Devoluciones',
'Exchange',
'Corporate Deal',
'NL CAC',
'PR vouchers',
'IT Vouchers',
'Curebit',
'Corporate Sales',
'PAR Voucher',
'Employee Vouchers',
'Dscuento',
'BANIXE02'
),'Inbound',channel)
WHERE Pais='MX';

UPDATE customer_service.rep_mkt_telesales
SET yrmonth=DATE_FORMAT(Fecha,'%Y%m');

UPDATE customer_service.rep_mkt_telesales
SET Semana=DATE_FORMAT(Fecha,'%Y-%u');

INSERT INTO customer_service.rep_mkt_telesales (Fecha,RecibidasT,channel,Pais,channel_aux,yrmonth)
SELECT event_date as Fecha,SUM(unanswered_in_wh) as RecibidasT,'Inbound' as channel ,'CO' as Pais,'Inbound' as channel_aux
,DATE_FORMAT(event_date,'%Y%m') as yrmonth
FROM customer_service_co.bi_ops_cc_dyalogo 
WHERE event_date>='2014-01-01' and pre=1
GROUP BY event_date;

INSERT INTO customer_service.rep_mkt_telesales (Fecha,RecibidasT,channel,Pais,channel_aux,yrmonth)
SELECT event_date as Fecha,SUM(unanswered_in_wh) as RecibidasT,'Inbound' as channel ,'MX' as Pais,'Inbound' as channel_aux,DATE_FORMAT(event_date,'%Y%m') as yrmonth
FROM customer_service.bi_ops_cc_mx
WHERE event_date>='2014-01-01' and pre=1
GROUP BY event_date;