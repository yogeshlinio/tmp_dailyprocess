INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Regional', 
  'TS/MKT_Report',
  'start',
  NOW(),
  MAX(Fecha),
  count(*),
  count(*)
FROM
  customer_service.rep_mkt_telesales;