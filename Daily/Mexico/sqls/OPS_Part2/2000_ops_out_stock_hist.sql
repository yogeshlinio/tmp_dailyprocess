/*
 Nombre: 2000_ops_out_stock_hist.sql
 Autor: Luis Ochoa, Rafael Guzman
 Fecha Creación:28/01/2014
 Descripción:
 Version: 1.0
 */
INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Mexico', 
  'out_stock_hist',
  'start',
  NOW(),
  max(date_exit),
  count(*),
  count(item_counter)
FROM
  production.out_stock_hist;


TRUNCATE	rafael.out_stock_hist ; 


INSERT INTO rafael.out_stock_hist (
		stock_item_id,
		barcode_wms,
		date_entrance,
		barcode_bob_duplicated,
		in_stock,
		wh_location,
		cost) 
SELECT
		estoque.estoque_id,
		estoque.cod_barras,
		CASE 	WHEN data_criacao IS NULL 
					THEN NULL
					ELSE date(data_criacao)
					END AS expr1,
	estoque.minucioso,
	posicoes.participa_estoque,
	estoque.endereco,
	estoque.unit_price
FROM
	(
		wmsprod.estoque
		LEFT JOIN wmsprod.itens_recebimento ON estoque.itens_recebimento_id = itens_recebimento.itens_recebimento_id
	)
LEFT JOIN wmsprod.posicoes ON estoque.endereco = posicoes.posicao ; 


UPDATE rafael.out_stock_hist
INNER JOIN wmsprod.movimentacoes ON out_stock_hist.stock_item_id = movimentacoes.estoque_id
SET out_stock_hist.date_entrance = date(movimentacoes.data_criacao)
WHERE
	movimentacoes.de_endereco = 'retorno de cancelamento'
OR movimentacoes.de_endereco = 'vendidos';


DELETE rafael.pro_unique_ean_bob.*
FROM
	rafael.pro_unique_ean_bob ; 


	INSERT INTO rafael.pro_unique_ean_bob (ean) SELECT
		produtos.ean AS ean
	FROM
		bob_live_mx.catalog_simple
	INNER JOIN wmsprod.produtos ON catalog_simple.sku = produtos.sku
	WHERE
		catalog_simple. STATUS NOT LIKE "deleted"
	GROUP BY
		produtos.ean
	HAVING
		count(produtos.ean = 1) ; 


		UPDATE rafael.out_stock_hist
	INNER JOIN wmsprod.traducciones_producto ON out_stock_hist.barcode_wms = traducciones_producto.identificador
	SET out_stock_hist.sku_simple = sku ; 


	UPDATE rafael.out_stock_hist a
	INNER JOIN wmsprod.movimentacoes b ON a.stock_item_id = b.estoque_id
	SET a.date_exit = date(b.data_criacao),
	a.exit_type = "sold"
WHERE
	b.para_endereco = "vendidos" ; 


	UPDATE rafael.out_stock_hist a
INNER JOIN wmsprod.movimentacoes b ON a.stock_item_id = b.estoque_id
SET a.date_exit = date(b.data_criacao),
 a.exit_type = "error"
WHERE
	b.para_endereco = "error" ; 


UPDATE (
		(
			(
				rafael.out_stock_hist
				INNER JOIN bob_live_mx.catalog_simple ON out_stock_hist.sku_simple = catalog_simple.sku
			)
			INNER JOIN bob_live_mx.catalog_config ON catalog_simple.fk_catalog_config = catalog_config.id_catalog_config
		)
		INNER JOIN bob_live_mx.catalog_shipment_type ON catalog_simple.fk_catalog_shipment_type = catalog_shipment_type.id_catalog_shipment_type
	)
INNER JOIN bob_live_mx.supplier ON catalog_config.fk_supplier = supplier.id_supplier
SET out_stock_hist.barcode_bob = barcode_ean,
 out_stock_hist.sku_config = catalog_config.sku,
 out_stock_hist.sku_name = catalog_config. NAME,
 out_stock_hist.sku_supplier_config = catalog_config.sku_supplier_config,
 out_stock_hist.model = catalog_config.model,
 out_stock_hist.supplier_name = supplier. NAME,
 out_stock_hist.supplier_id = supplier.id_supplier,
 out_stock_hist.fullfilment_type_bob = catalog_shipment_type. NAME,
 out_stock_hist.price = catalog_simple.price ; 

UPDATE rafael.out_stock_hist
INNER JOIN bob_live_mx.catalog_config 
	ON out_stock_hist.sku_config = catalog_config.sku
INNER JOIN bob_live_mx.catalog_brand 
	ON catalog_config.fk_catalog_brand = catalog_brand.id_catalog_brand
SET out_stock_hist.brand = catalog_brand.name;

UPDATE rafael.out_stock_hist a
INNER JOIN wmsprod.estoque b ON a.stock_item_id = b.estoque_id
INNER JOIN procurement_live_mx.wms_received_item c ON b.itens_recebimento_id = c.id_wms
INNER JOIN procurement_live_mx.procurement_order_item d ON c.fk_procurement_order_item = d.id_procurement_order_item
SET a.cost = d.unit_price
WHERE
	a.cost IS NULL ; 


	UPDATE (
		(
			(
				rafael.out_stock_hist
				INNER JOIN bob_live_mx.catalog_simple ON out_stock_hist.sku_simple = catalog_simple.sku
			)
			INNER JOIN bob_live_mx.catalog_config ON catalog_simple.fk_catalog_config = catalog_config.id_catalog_config
		)
		INNER JOIN bob_live_mx.catalog_shipment_type ON catalog_simple.fk_catalog_shipment_type = catalog_shipment_type.id_catalog_shipment_type
	)
INNER JOIN bob_live_mx.supplier ON catalog_config.fk_supplier = supplier.id_supplier
SET out_stock_hist.cost = catalog_simple.cost
WHERE
	out_stock_hist.cost IS NULL ; 


	UPDATE rafael.out_stock_hist
INNER JOIN bob_live_mx.catalog_config ON out_stock_hist.sku_config = catalog_config.sku
INNER JOIN bob_live_mx.catalog_attribute_option_global_buyer_name ON catalog_attribute_option_global_buyer_name.id_catalog_attribute_option_global_buyer_name = catalog_config.fk_catalog_attribute_option_global_buyer_name
INNER JOIN bob_live_mx.catalog_attribute_option_global_head_buyer_name ON catalog_attribute_option_global_head_buyer_name.id_catalog_attribute_option_global_head_buyer_name = catalog_config.fk_catalog_attribute_option_global_head_buyer_name
SET out_stock_hist.buyer = catalog_attribute_option_global_buyer_name. NAME,
 out_stock_hist.head_buyer = catalog_attribute_option_global_head_buyer_name. NAME ; 


UPDATE rafael.out_stock_hist
SET out_stock_hist.sold_last_30 = CASE
WHEN datediff(curdate(), date_exit) <= 30 THEN
	1
ELSE
	0
END,
 out_stock_hist.sold_yesterday = CASE
WHEN datediff(
	curdate(),
	(
		date_sub(date_exit, INTERVAL 1 DAY)
	)
) = 0 THEN
	1
ELSE
	0
END,
 out_stock_hist.sold_last_10 = CASE
WHEN datediff(curdate(), date_exit) < 10 THEN
	1
ELSE
	0
END,
 out_stock_hist.sold_last_7 = CASE
WHEN datediff(curdate(), date_exit) < 7 THEN
	1
ELSE
	0
END
WHERE
	out_stock_hist.exit_type = "sold" ; 


	UPDATE rafael.out_stock_hist
SET out_stock_hist.entrance_last_30 = CASE
WHEN datediff(curdate(), date_entrance) < 30 THEN
	1
ELSE
	0
END,
 out_stock_hist.oms_po_last_30 = CASE
WHEN datediff(
	curdate(),
	date_procurement_order
) < 30 THEN
	1
ELSE
	0
END ; 


UPDATE rafael.out_stock_hist
SET out_stock_hist.days_in_stock = CASE
WHEN date_exit IS NULL THEN
	datediff(curdate(), date_entrance)
ELSE
	datediff(date_exit, date_entrance)
END ; 


UPDATE rafael.out_stock_hist
SET out_stock_hist.fullfilment_type_real = "inventory" ; 


UPDATE rafael.out_stock_hist
INNER JOIN (
	wmsprod.itens_venda
	INNER JOIN wmsprod.status_itens_venda ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
) ON out_stock_hist.stock_item_id = itens_venda.estoque_id
SET out_stock_hist.fullfilment_type_real = "crossdock"
WHERE
	status_itens_venda. STATUS = "analisando quebra"
OR status_itens_venda. STATUS = "aguardando estoque" ; 


UPDATE (
	rafael.out_stock_hist
	INNER JOIN wmsprod.itens_venda ON out_stock_hist.stock_item_id = itens_venda.estoque_id
)
INNER JOIN wmsprod.status_itens_venda ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
SET out_stock_hist.fullfilment_type_real = "dropshipping"
WHERE
	(
		(
			(status_itens_venda. STATUS) = "ds estoque reservado"
		)
	) ; 


UPDATE rafael.out_stock_hist
INNER JOIN wmsprod.movimentacoes ON out_stock_hist.stock_item_id = movimentacoes.estoque_id
SET out_stock_hist.fullfilment_type_real = 'inventory'
WHERE
	movimentacoes.de_endereco = 'retorno de cancelamento'
OR movimentacoes.de_endereco = 'vendidos' ; 


UPDATE rafael.out_stock_hist
INNER JOIN bob_live_mx.catalog_simple ON out_stock_hist.sku_simple = catalog_simple.sku
INNER JOIN bob_live_mx.catalog_tax_class ON catalog_simple.fk_catalog_tax_class = catalog_tax_class.id_catalog_tax_class
SET out_stock_hist.tax_percentage = tax_percent,
 out_stock_hist.cost_w_o_vat = out_stock_hist.cost / (1 + tax_percent / 100) ; 


UPDATE rafael.out_stock_hist
SET out_stock_hist.in_stock_cost_w_o_vat = out_stock_hist.cost_w_o_vat
WHERE
	out_stock_hist.in_stock = 1 ; 


UPDATE rafael.out_stock_hist
SET out_stock_hist.sold_last_30_cost_w_o_vat = out_stock_hist.cost_w_o_vat,
 out_stock_hist.sold_last_30_price = out_stock_hist.price
WHERE
	out_stock_hist.sold_last_30 = 1 ; 


DELETE rafael.pro_stock_hist_sku_count.*
FROM
	rafael.pro_stock_hist_sku_count ; 


INSERT INTO rafael.pro_stock_hist_sku_count (sku_simple, sku_count) 
SELECT
		out_stock_hist.sku_simple,
		count(*) AS sumofitem_counter
FROM
		rafael.out_stock_hist
GROUP BY
		out_stock_hist.sku_simple,
		out_stock_hist.in_stock
HAVING
		out_stock_hist.in_stock = 1 ; 


UPDATE rafael.out_stock_hist
INNER JOIN rafael.pro_stock_hist_sku_count ON out_stock_hist.sku_simple = pro_stock_hist_sku_count.sku_simple
SET out_stock_hist.sku_counter = 1 / pro_stock_hist_sku_count.sku_count ; 


UPDATE rafael.out_stock_hist
SET out_stock_hist.reserved = '0' ; 


UPDATE rafael.out_stock_hist
INNER JOIN wmsprod.estoque ON out_stock_hist.stock_item_id = estoque.estoque_id
SET out_stock_hist.reserved = "1"
WHERE
		(
			(
				(estoque.almoxarifado) = "separando"
				OR (estoque.almoxarifado) = "estoque reservado"
				OR (estoque.almoxarifado) = "aguardando separacao"
			)
		) ; 


UPDATE rafael.out_stock_hist
INNER JOIN bob_live_mx.catalog_simple ON out_stock_hist.sku_simple = catalog_simple.sku
SET out_stock_hist.supplier_leadtime = catalog_simple.delivery_time_supplier ; 


UPDATE rafael.out_stock_hist
INNER JOIN (
		(
			bob_live_mx.catalog_config
			INNER JOIN bob_live_mx.catalog_simple ON catalog_config.id_catalog_config = catalog_simple.fk_catalog_config
		)
		INNER JOIN bob_live_mx.catalog_stock ON catalog_simple.id_catalog_simple = catalog_stock.fk_catalog_simple
	) ON out_stock_hist.sku_config = catalog_config.sku
SET out_stock_hist.visible = 1
WHERE
		(
			(
				(catalog_config.pet_status) = "creation,edited,images"
			)
			AND (
				(
					catalog_config.pet_approved
				) = 1
			)
			AND (
				(catalog_config. STATUS) = "active"
			)
			AND (
				(catalog_simple. STATUS) = "active"
			)
			AND ((catalog_simple.price) > 0)
			AND (
				(
					catalog_config.display_if_out_of_stock
				) = 0
			)
			AND ((catalog_stock.quantity) > 0)
			AND ((catalog_simple.cost) > 0)
		)
	OR (
		(
			(catalog_config.pet_status) = "creation,edited,images"
		)
		AND (
			(
				catalog_config.pet_approved
			) = 1
		)
		AND (
			(catalog_config. STATUS) = "active"
		)
		AND (
			(catalog_simple. STATUS) = "active"
		)
		AND ((catalog_simple.price) > 0)
		AND (
			(
				catalog_config.display_if_out_of_stock
			) = 1
		)
		AND ((catalog_stock.quantity) > 0)
		AND ((catalog_simple.cost) > 0)
	) ; 


UPDATE rafael.out_stock_hist
SET 
  out_stock_hist.week_exit = rafael.week_iso (date_exit),
  out_stock_hist.week_entrance = rafael.week_iso (date_entrance) ; 


DELETE rafael.pro_stock_hist_sold_last_30_count.*
FROM
		rafael.pro_stock_hist_sold_last_30_count ; 


INSERT INTO rafael.pro_stock_hist_sold_last_30_count (
	sold_last_30,
	sku_simple,
	count_of_item_counter
) 
SELECT
	out_stock_hist.sold_last_30,
	out_stock_hist.sku_simple,
	count(*) AS count_of_item_counter
FROM
	rafael.out_stock_hist
GROUP BY
	out_stock_hist.sold_last_30,
	out_stock_hist.sku_simple
HAVING
	out_stock_hist.sold_last_30 = 1 ; 


UPDATE rafael.out_stock_hist
INNER JOIN rafael.pro_stock_hist_sold_last_30_count ON out_stock_hist.sku_simple = pro_stock_hist_sold_last_30_count.sku_simple
SET out_stock_hist.sold_last_30_counter = 1 / pro_stock_hist_sold_last_30_count.count_of_item_counter ; 


UPDATE rafael.out_stock_hist
SET out_stock_hist.sku_simple_blank = 1
WHERE
	out_stock_hist.sku_simple IS NULL ; 


UPDATE (
				wmsprod.itens_recebimento
				INNER JOIN wmsprod.estoque ON itens_recebimento.itens_recebimento_id = estoque.itens_recebimento_id
			)
		INNER JOIN rafael.out_stock_hist ON estoque.estoque_id = out_stock_hist.stock_item_id
		SET out_stock_hist.quarantine = 1
		WHERE
			itens_recebimento.endereco = "cuarentena"
		OR itens_recebimento.endereco = "cuarenta2" ; 


DELETE rafael.pro_sum_last_5_out_of_6_wks.*
FROM
			rafael.pro_sum_last_5_out_of_6_wks ; 


INSERT INTO rafael.pro_sum_last_5_out_of_6_wks (sku_simple, expr1) SELECT
		max_last_6_wks.sku_simple,
		sumofcountofdate_exit - maxofcountofdate_exit AS expr1
FROM
		rafael.max_last_6_wks
GROUP BY
		max_last_6_wks.sku_simple,
		sumofcountofdate_exit - maxofcountofdate_exit ; 


UPDATE rafael.out_stock_hist
INNER JOIN rafael.pro_sum_last_5_out_of_6_wks ON out_stock_hist.sku_simple = pro_sum_last_5_out_of_6_wks.sku_simple
SET out_stock_hist.sum_sold_last_5_out_of_6_wks = pro_sum_last_5_out_of_6_wks.expr1 ; 


UPDATE rafael.out_stock_hist
INNER JOIN bob_live_mx.catalog_config ON out_stock_hist.sku_config = catalog_config.sku
SET out_stock_hist.package_height = catalog_config.package_height,
	out_stock_hist.package_length = catalog_config.package_length,
	out_stock_hist.package_width = catalog_config.package_width ; 


UPDATE rafael.out_stock_hist
SET out_stock_hist.vol_weight = package_height * package_length * package_width / 5000 ; 


UPDATE rafael.out_stock_hist
INNER JOIN bob_live_mx.catalog_config ON out_stock_hist.sku_config = catalog_config.sku
SET out_stock_hist.package_weight = catalog_config.package_weight ; 


UPDATE rafael.out_stock_hist
SET out_stock_hist.max_vol_w_vs_w = CASE
WHEN vol_weight > package_weight THEN
	vol_weight
ELSE
	package_weight
END ; 


UPDATE rafael.out_stock_hist
SET out_stock_hist.package_measure = CASE
WHEN max_vol_w_vs_w > 45.486 THEN
	"large"
ELSE
			(
				CASE
				WHEN max_vol_w_vs_w > 2.277 THEN
					"medium"
				ELSE
					"small"
				END
			)
		END ; 

UPDATE rafael.out_stock_hist
SET out_stock_hist.package_measure_new = CASE
WHEN max_vol_w_vs_w > 35 THEN
	'oversized'
ELSE
			(
				CASE
				WHEN max_vol_w_vs_w > 5 THEN
					'large'
				ELSE
					(
						CASE
						WHEN max_vol_w_vs_w > 2 THEN
							'medium'
						ELSE
							'small'
						END
					)
				END
			)
END ; 


UPDATE rafael.out_stock_hist
INNER JOIN wmsprod.itens_inventario ON out_stock_hist.barcode_wms = itens_inventario.cod_barras
SET out_stock_hist.sub_position = sub_endereco ; 


UPDATE wmsprod.posicoes
INNER JOIN rafael.out_stock_hist ON posicoes.posicao = out_stock_hist.wh_location
SET out_stock_hist.position_type = posicoes.tipo_posicao ; 


		UPDATE rafael.out_stock_hist
		SET out_stock_hist.position_item_size_type = CASE
		WHEN position_type = "safe" THEN
			"small"
		ELSE
			(
				CASE
				WHEN position_type = "mezanine" THEN
					"small"
				ELSE
					(
						CASE
						WHEN position_type = "muebles" THEN
							"large"
						ELSE
							"tbd"
						END
					)
				END
			)
		END ; 


DELETE
FROM
			rafael.items_procured_in_transit ; 


			INSERT INTO rafael.items_procured_in_transit (
				sku_simple,
				number_ordered,
				unit_price
			) SELECT
				catalog_simple.sku,
				count(
					procurement_order_item.id_procurement_order_item
				) AS countofid_procurement_order_item,
				avg(
					procurement_order_item.unit_price
				) AS avgofunit_price
			FROM
				bob_live_mx.catalog_simple
			INNER JOIN (
				procurement_live_mx.procurement_order_item
				INNER JOIN procurement_live_mx.procurement_order ON procurement_order_item.fk_procurement_order = procurement_order.id_procurement_order
			) ON catalog_simple.id_catalog_simple = procurement_order_item.fk_catalog_simple
			WHERE
				(
					(
						(
							procurement_order_item.is_deleted
						) = 0
					)
					AND (
						(
							procurement_order.is_cancelled
						) = 0
					)
					AND (
						(
							procurement_order_item.sku_received
						) = 0
					)
				)
			GROUP BY
				catalog_simple.sku ; 


UPDATE rafael.out_stock_hist
INNER JOIN rafael.items_procured_in_transit ON out_stock_hist.sku_simple = items_procured_in_transit.sku_simple
SET out_stock_hist.items_procured_in_transit = number_ordered,
	out_stock_hist.procurement_price = unit_price ; 


UPDATE (
				procurement_live_mx.procurement_order
				INNER JOIN (
					(
						bob_live_mx.catalog_simple
						INNER JOIN rafael.out_stock_hist ON catalog_simple.sku = out_stock_hist.sku_simple
					)
					INNER JOIN procurement_live_mx.procurement_order_item ON catalog_simple.id_catalog_simple = procurement_order_item.fk_catalog_simple
				) ON procurement_order.id_procurement_order = procurement_order_item.fk_procurement_order
			)
SET out_stock_hist.date_procurement_order = procurement_order.created_at ; 


UPDATE rafael.out_stock_hist
INNER JOIN procurement_live_mx.catalog_supplier_attributes ON out_stock_hist.supplier_id = catalog_supplier_attributes.fk_catalog_supplier
SET out_stock_hist.oms_payment_terms = catalog_supplier_attributes.payment_terms,
		out_stock_hist.oms_payment_event = catalog_supplier_attributes.payment_event ; 


UPDATE rafael.out_stock_hist
SET out_stock_hist.date_payable_promised = adddate(
		date_procurement_order,
		INTERVAL oms_payment_terms DAY
	)
	WHERE
		out_stock_hist.oms_payment_event IN ("factura", "pedido") ; 


		UPDATE rafael.out_stock_hist
	SET out_stock_hist.date_payable_promised = adddate(
		date_procurement_order,
		INTERVAL oms_payment_terms + supplier_leadtime DAY
	)
	WHERE
		out_stock_hist.oms_payment_event = "entrega" ; 


		UPDATE rafael.out_stock_hist
	SET out_stock_hist.sold = 1
	WHERE
		out_stock_hist.exit_type = "sold" ; 


		DELETE
	FROM
		rafael.sell_rate_simple ; 


		INSERT INTO rafael.sell_rate_simple (
			sku_simple,
			num_items,
			num_sales,
			average_days_in_stock
		) SELECT
			sku_simple,
			count(*),
			sum(sold),
			avg(days_in_stock)
		FROM
			rafael.out_stock_hist
		GROUP BY
			sku_simple ; 


			UPDATE rafael.sell_rate_simple srs
		SET srs.num_items_available = (
			SELECT
				sum(in_stock)
			FROM
				rafael.out_stock_hist osh
			WHERE
				reserved = '0'
			AND srs.sku_simple = osh.sku_simple
			GROUP BY
				sku_simple
		) ; 


		UPDATE rafael.sell_rate_simple
		SET sell_rate_simple.average_sell_rate = CASE
		WHEN average_days_in_stock = 0 THEN
			0
		ELSE
			num_sales / average_days_in_stock
		END ; 


		UPDATE rafael.sell_rate_simple
		SET sell_rate_simple.remaining_days = CASE
		WHEN average_sell_rate = 0 THEN
			0
		ELSE
			num_items_available / average_sell_rate
		END ;


		TRUNCATE rafael.sell_rate_config ; 


			INSERT INTO rafael.sell_rate_config (
				sku_config,
				num_items,
				num_sales
			) SELECT
				sku_config,
				count(*),
				sum(sold)
			FROM
				rafael.out_stock_hist
			WHERE
				datediff(curdate(), date_exit) <= 42
			GROUP BY
				sku_config ; 


				UPDATE rafael.sell_rate_config a
			INNER JOIN (
				SELECT
					sku_config,
					avg(days_in_stock) avg_days
				FROM
					rafael.out_stock_hist
				WHERE
					in_stock = 1
				AND reserved = 0
				GROUP BY
					sku_config
			) b ON a.sku_config = b.sku_config
			SET a.average_days_in_stock = CASE
			WHEN b.avg_days >= 42 THEN
				42
			ELSE
				b.avg_days
			END ; 


			UPDATE rafael.sell_rate_config a
			SET a.num_items_available = (
				SELECT
					sum(b.in_stock)
				FROM
					rafael.out_stock_hist b
				WHERE
					b.reserved = '0'
				AND a.sku_config = b.sku_config
				GROUP BY
					sku_config
			) ; 


			UPDATE rafael.sell_rate_config a
			SET a.average_sell_rate = CASE
			WHEN a.average_days_in_stock = 0 THEN
				0
			ELSE
				a.num_sales / a.average_days_in_stock
			END ; 


			UPDATE rafael.sell_rate_config
			SET sell_rate_config.remaining_days = CASE
			WHEN average_sell_rate = 0 THEN
				0
			ELSE
				num_items_available / average_sell_rate
			END ; 


			UPDATE rafael.out_stock_hist a
			INNER JOIN rafael.sell_rate_config b ON a.sku_config = b.sku_config
			SET a.average_remaining_days = b.remaining_days ; 


			DELETE
			FROM
				rafael.pro_max_days_in_stock ; 


				INSERT INTO rafael.pro_max_days_in_stock (
					sku_config,
					max_days_in_stock,
					min_days_in_stock
				) SELECT
					sku_config,
					max(days_in_stock),
					min(days_in_stock)
				FROM
					rafael.out_stock_hist
				WHERE
					in_stock = 1
				AND reserved = 0
				GROUP BY
					sku_config ; 


					UPDATE rafael.out_stock_hist
				INNER JOIN rafael.pro_max_days_in_stock ON out_stock_hist.sku_config = pro_max_days_in_stock.sku_config
				SET out_stock_hist.max_days_in_stock = pro_max_days_in_stock.max_days_in_stock,
				out_stock_hist.min_days_in_stock = pro_max_days_in_stock.min_days_in_stock ; 


				UPDATE rafael.out_stock_hist
			SET out_stock_hist.week_payable_promised = rafael.week_iso (date_payable_promised) ; 


			UPDATE rafael.out_stock_hist
			SET out_stock_hist.week_procurement_order = rafael.week_iso (date_procurement_order) ; 


			UPDATE rafael.out_stock_hist
			INNER JOIN bob_live_mx.catalog_simple ON out_stock_hist.sku_simple = catalog_simple.sku
			SET out_stock_hist.delivery_cost_supplier = catalog_simple.delivery_cost_supplier ; 


			UPDATE rafael.out_stock_hist
			SET cogs = cost_w_o_vat + delivery_cost_supplier ; 


			UPDATE rafael.out_stock_hist
			SET out_stock_hist.days_payable_since_entrance = CASE
			WHEN date_entrance IS NULL THEN
				NULL
			ELSE
				(
					CASE
					WHEN oms_payment_event IN ('factura', 'pedido') THEN
						oms_payment_terms - supplier_leadtime
					ELSE
						(
							CASE
							WHEN oms_payment_event = 'entrega' THEN
								oms_payment_terms
							END
						)
					END
				)
			END ; 


			UPDATE rafael.out_stock_hist a
			INNER JOIN bob_live_mx.catalog_config b ON a.sku_config = b.sku
			INNER JOIN bob_live_mx.catalog_attribute_option_global_category c ON b.fk_catalog_attribute_option_global_category = c.id_catalog_attribute_option_global_category
			INNER JOIN bob_live_mx.catalog_attribute_option_global_sub_category d ON b.fk_catalog_attribute_option_global_sub_category = d.id_catalog_attribute_option_global_sub_category
			INNER JOIN bob_live_mx.catalog_attribute_option_global_sub_sub_category e ON b.fk_catalog_attribute_option_global_sub_sub_category = e.id_catalog_attribute_option_global_sub_sub_category
			SET a.category_com_main = c. NAME,
			a.category_com_sub = d. NAME,
			a.category_com_sub_sub = e. NAME ; 


			UPDATE rafael.out_stock_hist a
		INNER JOIN development_mx.A_E_M1_New_CategoryBP b ON a.category_com_sub = b.cat2
		SET a.category_bp = b.CatBP ; 


		UPDATE rafael.out_stock_hist a
		INNER JOIN development_mx.A_E_M1_New_CategoryBP b ON a.category_com_main = b.cat1
		SET a.category_bp = b.CatBP
		WHERE
			a.category_bp IS NULL ; 


		UPDATE rafael.out_stock_hist
		SET fullfilment_type_bp = CASE
		WHEN fullfilment_type_real = 'crossdock' THEN
			'crossdocking'
		WHEN fullfilment_type_real = 'inventory'
		AND fullfilment_type_bob = 'consignment' THEN
			'consignment'
		WHEN fullfilment_type_real = 'inventory'
		AND fullfilment_type_bob <> 'consignment' THEN
			'outright buying'
		WHEN fullfilment_type_real = 'dropshipping' THEN
			'other'
		END ; 



UPDATE rafael.out_stock_hist set is_sell_list = 1
WHERE max_days_in_stock > 30
and in_stock= 1
and reserved= 0
and fullfilment_type_bob <> 'Consignment' 
and fullfilment_type_bob is not null
and category_com_main = 'Home and Living' 
and (average_remaining_days > 150
or average_remaining_days is null);

UPDATE rafael.out_stock_hist set is_sell_list = 1
where max_days_in_stock > 30
and in_stock= 1
and reserved= 0
and fullfilment_type_bob <> 'Consignment' 
and fullfilment_type_bob is not null
and category_com_main = 'Fashion' 
and (average_remaining_days > 120
or average_remaining_days is null);

UPDATE rafael.out_stock_hist set is_sell_list = 1
where max_days_in_stock > 30
and in_stock= 1
and reserved= 0
and fullfilment_type_bob <> 'Consignment' 
and fullfilment_type_bob is not null
and category_com_main = 'Electrónicos' 
and (average_remaining_days > 60
or average_remaining_days is null);

UPDATE rafael.out_stock_hist set is_sell_list = 1
where max_days_in_stock > 30
and in_stock= 1
and reserved= 0
and fullfilment_type_bob <> 'Consignment' 
and fullfilment_type_bob is not null
and category_com_main not in ('Home and Living','Fashion','Electrónicos') 
and (average_remaining_days > 90
or average_remaining_days is null);


SET @i = 0 ; 
DROP TEMPORARY TABLE IF EXISTS TMP ; 
CREATE TEMPORARY TABLE TMP 
SELECT
	sku_config,
	Count(*) AS items_in_stock,
	sum(cost_w_o_vat) AS cost_w_o_vat_in_stock
FROM
	rafael.out_stock_hist
WHERE
	in_stock = 1
AND reserved = 0
AND (
			fullfilment_type_bob <> 'consignment'
			OR fullfilment_type_bob IS NOT NULL
		)
GROUP BY
	sku_config;

DROP TABLE IF EXISTS rafael.top_skus_sell_list ; 
CREATE TABLE rafael.top_skus_sell_list 
SELECT
	sku_config,
	items_in_stock,
	cost_w_o_vat_in_stock,
	@i := @i + 1 AS rkn
FROM
	TMP
ORDER BY
	cost_w_o_vat_in_stock DESC ;


DROP TEMPORARY TABLE IF EXISTS tmp_sales_for_stock;
CREATE TEMPORARY TABLE tmp_sales_for_stock ( INDEX (estoque_id))
SELECT 
		t.itens_venda_id,
		t.item_id,
		t.order_id,
		t.numero_order,
		t.data_pedido,
		t.estoque_id,
		t.data_criacao,
		t.status 
	FROM wmsprod.itens_venda t 
JOIN (SELECT 
				estoque_id, 
				max(data_criacao) AS max 
			FROM wmsprod.itens_venda 
			GROUP BY estoque_id) t2 
ON t.data_criacao=t2.max 
AND t.estoque_id=t2.estoque_id 
;

UPDATE	rafael.out_stock_hist a 
 INNER JOIN tmp_sales_for_stock b
	ON a.stock_item_id = b.estoque_id
SET 
	a.item_id = b.item_id;



# Sell List

INSERT INTO rafael.pro_sell_list_hist_totals (
	date_reported,
	category_bp,
	cost_w_o_vat_over_two_items,
	cost_w_o_vat_under_two_items) 
SELECT
	J.date_reported,
	J.Categoria,
	J.Value_more_than_two,
	J.Value_less_than_two
FROM
	(	SELECT
			curdate() AS date_reported,
			B.category_bp AS 'Categoria',
			B.cost_w_o_vat AS Value_more_than_two,
			D.cost_w_o_vat AS Value_less_than_two
		FROM
			(	SELECT
					category_bp,
					sum(cost_w_o_vat) AS cost_w_o_vat
				FROM
					(	SELECT
							category_bp,
							sku_config,
							sum(cost_w_o_vat) AS cost_w_o_vat
						FROM
							production.out_stock_hist
						WHERE
							is_sell_list = 1
						AND category_bp IS NOT NULL
						GROUP BY
							sku_config
						HAVING
							count(sku_config) > 2
					) A
				GROUP BY
					category_bp
			) B
		LEFT JOIN (	SELECT
									category_bp,
									sum(cost_w_o_vat) AS cost_w_o_vat
								FROM
									(	SELECT
											category_bp,
											sum(cost_w_o_vat) AS cost_w_o_vat
										FROM
											production.out_stock_hist
										WHERE
											is_sell_list = 1
										GROUP BY
											sku_config
										HAVING
											count(sku_config) <= 2
									) C
								GROUP BY
									category_bp
									) D 
		ON B.category_bp = D.category_bp
	) J;


UPDATE rafael.out_stock_hist
SET item_counter = 1;

DROP TABLE IF EXISTS production.out_stock_hist;

CREATE TABLE production.out_stock_hist LIKE rafael.out_stock_hist; 

INSERT INTO production.out_stock_hist SELECT * FROM rafael.out_stock_hist; 

INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Mexico', 
  'out_stock_hist',
  'finish',
  NOW(),
  max(date_exit),
  count(*),
  count(item_counter)
FROM
  production.out_stock_hist;


DROP TABLE IF EXISTS production.items_procured_in_transit;

CREATE TABLE production.items_procured_in_transit LIKE rafael.items_procured_in_transit;

INSERT INTO production.items_procured_in_transit SELECT * FROM rafael.items_procured_in_transit;


DROP TABLE IF EXISTS production.top_skus_sell_list;

CREATE TABLE production.top_skus_sell_list LIKE rafael.top_skus_sell_list;

INSERT INTO production.top_skus_sell_list SELECT * FROM rafael.top_skus_sell_list;

DROP TABLE IF EXISTS production.pro_sell_list_hist_totals;

CREATE TABLE production.pro_sell_list_hist_totals LIKE rafael.pro_sell_list_hist_totals;

INSERT INTO production.pro_sell_list_hist_totals SELECT * FROM rafael.pro_sell_list_hist_totals;

