/*
 Nombre: 2300_ops_out_safety_stock.sql
 Autor: Luis Ochoa, Rafael Guzman
 Fecha Creación:28/01/2014
 Descripción:
 Version: 1.0
 */
select  'Safety Stock: start',now();

delete  from production.out_safety_stock;

insert into production.out_safety_stock(date, item_id, order_num, product_name, sku_simple, supplier, obc, oac, status) select date, item, order_nr, product_name, sku, proveedor, obc, oac, status_item from production.tbl_order_detail where status_item in ('confirm_call_1', 'manual_fraud_check_pending', 'payment_pending');

update production.out_safety_stock s set s.in_stock = (select sum(h.in_stock) from production.out_stock_hist h where s.sku_simple=h.sku_simple and reserved=0 group by h.sku_simple); 

/*update production.out_safety_stock s inner join production.out_stock_hist h on s.sku_simple=h.sku_simple set s.items_procured_in_transit=h.items_procured_in_transit, s.fullfilment_type_bob=h.fullfilment_type_bob, s.fullfilment_type_real=h.fullfilment_type_real;*/

update production.out_safety_stock s inner join production.out_stock_hist h on s.sku_simple=h.sku_simple set s.fullfilment_type_bob=h.fullfilment_type_bob, s.fullfilment_type_real=h.fullfilment_type_real; 

update production.out_safety_stock s inner join production.items_procured_in_transit ipit on s.sku_simple=ipit.sku_simple set s.items_procured_in_transit=ipit.number_ordered;

update production.out_safety_stock inner join bob_live_mx.sales_order_item on out_safety_stock.sku_simple = sales_order_item.sku inner join bob_live_mx.catalog_shipment_type on sales_order_item.fk_catalog_shipment_type = catalog_shipment_type.id_catalog_shipment_type set out_safety_stock.fullfilment_type_bob = catalog_shipment_type.name where out_safety_stock.fullfilment_type_bob is null;

update production.out_safety_stock set in_stock=0 where in_stock is null;

update production.out_safety_stock set items_procured_in_transit=0 where items_procured_in_transit is null;

delete from production.temporary_safety_stock;

insert into production.temporary_safety_stock select order_num, sku_simple from production.out_safety_stock;

update production.out_safety_stock s set count_order= (select count(distinct order_num)/count(order_num) from production.temporary_safety_stock t where s.order_num=t.order_num group by t.order_num);

update production.out_safety_stock s set count_sku= (select count(distinct sku_simple)/count(sku_simple) from production.temporary_safety_stock t where s.sku_simple=t.sku_simple and s.order_num=t.order_num group by t.sku_simple, t.order_num);

/*update wmsprod.itens_venda inner join production.out_safety_stock on itens_venda.sku = out_safety_stock.sku_simple set out_safety_stock.items_pending_procurement = 1 where itens_venda.status in ('aguardando estoque', 'analisando quebra');*/

update production.out_safety_stock set items_pending_procurement = (select count(itens_venda_id) from wmsprod.itens_venda where itens_venda.sku = out_safety_stock.sku_simple and itens_venda.status in ('aguardando estoque', 'analisando quebra') group by itens_venda.sku);

update production.out_safety_stock set items_pending_procurement = 0 where items_pending_procurement is null;

select  'Safety Stock: stop',now();
