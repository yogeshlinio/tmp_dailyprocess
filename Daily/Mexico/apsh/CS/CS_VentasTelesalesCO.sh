v_user=$1
v_pwd=$2
v_host=$3
v_date=`date +"%Y%m%d"`
v_country=Colombia_Project

echo    "$v_country Ventas Telesales Colombia..."
v_event=A_Master
v_reply=480
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
v_step=finish
. $lib/ToolKit/monitoring.sh 
      
 

       echo "Loading start monitoring log ..."
       sqls="./sqls/CS/ventasTelesalesCO"
       sources="./sources"
       dateTime=`date`
       echo $dateTime $i 	   
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b customer_service < $sqls/000_log_inicio_Sales_Report_CO.sql
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		

       echo "Update tbl_telesales_co ..."
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b customer_service < $sqls/02_ventas_telesales_co.sql
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		


       echo "End Log"
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b customer_service < $sqls/003_log_Fin_Sales_Report_CO.sql
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		

       dateTime=`date`
       echo " ${dateTime} End Load ..."
exit;
