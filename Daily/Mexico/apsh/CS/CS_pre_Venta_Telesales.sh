v_user=$1
v_pwd=$2
v_host=$3
v_date=`date +"%Y%m%d"`
v_country=Mexico

echo    "$v_country Reporte Pre Venta Telesales Regional..."
v_event=Sales_Report
v_reply=480
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
	. $lib/ToolKit/monitoring.sh       

v_country=Colombia
v_event=Sales_Report
	. $lib/ToolKit/monitoring.sh       

v_country=Mexico
v_event=tbl_queueMetrics30
	. $lib/ToolKit/monitoring.sh       
v_event=customer_service_co.bi_ops_general_cc

v_country=Colombia
v_event=bi_ops_cc
	. $lib/ToolKit/monitoring.sh       

v_country=Colombia
v_event=customer_service_co.bi_ops_cc_survey
	. $lib/ToolKit/monitoring.sh       


       echo "Loading start monitoring log ..."
       sqls="./sqls/CS/preVentaTelesales"
       sources="./sources"
       dateTime=`date`
       echo $dateTime $i 	   
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b customer_service < $sqls/001_log_inicio_preventa_telesales.sql
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		

       echo "Update Tabla pre Venta telesales Agente ..."
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b customer_service < $sqls/02_pre_venta_telesales_agente.sql
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		

       
       echo "End Log"
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b customer_service < $sqls/003_log_fin_preventa_telesales.sql
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		

       dateTime=`date`
       echo " ${dateTime} End Load ..."
exit;
