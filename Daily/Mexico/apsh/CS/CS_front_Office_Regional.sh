v_user=$1
v_pwd=$2
v_host=$3
v_date=`date +"%Y%m%d"`
v_country=Mexico

echo    "$v_country Reporte Front Office Regional..."
v_event=bi_ops_general_cc
v_reply=1200
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
. $lib/ToolKit/monitoring.sh 

v_event=bi_ops_cc_survey_mx
. $lib/ToolKit/monitoring.sh

v_country=Colombia      
v_event=bi_ops_cc
. $lib/ToolKit/monitoring.sh
    
v_event=customer_service_co.bi_ops_cc_survey
. $lib/ToolKit/monitoring.sh


v_country=Peru
v_event=bi_ops_cc_pe
. $lib/ToolKit/monitoring.sh
v_event=customer_service_pe.bi_ops_cc_survey
. $lib/ToolKit/monitoring.sh

       echo "Datos horas no laborales ..."
       sqls="./sqls/CS/fronOfficeRegional"
       sources="./sources"
       dateTime=`date`
       echo $dateTime $i 	   
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b customer_service < $sqls/001_non_working_hours.sql
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi	

       echo "Loading start monitoring log ..."
       sqls="./sqls/CS/fronOfficeRegional"
       sources="./sources"
       dateTime=`date`
       echo $dateTime $i 	   
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b customer_service < $sqls/002_log_inicio_FO_Reg.sql
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		

       echo "Update Tabla FO Agente ..."
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b customer_service < $sqls/003_fo_regional_agente.sql
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		

       echo "Update Tabla FO Agente Survey..."
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b customer_service < $sqls/004_fo_regional_survey.sql
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		
       

       echo "End Log"
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b customer_service < $sqls/005_log_fin_FO_Reg.sql
       if [ $error -gt 0 ]; then
           exit 1
       fi		

       dateTime=`date`
       echo " ${dateTime} End Load ..."
exit;
