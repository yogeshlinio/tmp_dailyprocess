v_user=$1
v_pwd=$2
v_host=$3
v_date=`date +"%Y%m%d"`
v_country=Mexico

echo    "$v_country Reporte telesales con mkt report..."
v_event=tbl_telesales_mx
v_reply=1920
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
v_step=finish
. $lib/ToolKit/monitoring.sh
v_country=Colombia
v_event=tbl_telesales_co
. $lib/ToolKit/monitoring.sh
v_event=marketing_co.channel_report
. $lib/ToolKit/monitoring.sh
v_country=Marketing
v_event=marketing_report.global_report
. $lib/ToolKit/monitoring.sh

for v_folder in CS_Telesales_w_Mkt_Reg
do
    echo "Loading $v_folder ..."
    sqls="./sqls/CS/${v_folder}"
    for i in $sqls/*.sql
    do
       dateTime=`date`
	   echo $dateTime $i 	   
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b customer_service < $i
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		
	   
    done
    dateTime=`date`
    echo " ${dateTime} End Load ..."
done
exit;
