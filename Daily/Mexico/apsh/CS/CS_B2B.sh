v_user=$1
v_pwd=$2
v_host=$3
v_date=`date +"%Y%m%d"`
v_country=Mexico

echo    "$v_country Reporte Business to business Regional..."
v_event=A_Master
v_reply=1920
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
v_step=finish
	. $lib/ToolKit/monitoring.sh       
v_event=Sales_Report
. $lib/ToolKit/monitoring.sh

       echo "Loading start monitoring log ..."
       sqls="./sqls/CS/B2B"
       sources="./sources"
       dateTime=`date`
       echo $dateTime $i 	   
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b customer_service < $sqls/001_log_inicio_b2b.sql
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		

       echo "Update Tabla b2b ..."
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b customer_service < $sqls/002_query_business_to_business.sql
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		


       echo "End Log"
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b customer_service < $sqls/004_log_fin_b2b.sql
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		

       dateTime=`date`
       echo " ${dateTime} End Load ..."
exit;
