v_user=$1
v_pwd=$2
v_host=$3
v_date=`date +"%Y%m%d"`
v_country=Regional

echo    "$v_country Reporte Chat Regional..."
v_event=CC_olark
v_reply=480
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
	. $lib/ToolKit/monitoring.sh       

v_event=livechat
v_reply=480
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
	. $lib/ToolKit/monitoring.sh       
  
       echo "Loading start monitoring log ..."
       sqls="./sqls/CS/chatRegional"
       sources="./sources"
       dateTime=`date`
       echo $dateTime $i 	   
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b customer_service < $sqls/001_log_inicio_chat_Reg.sql
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		

       echo "Integration Tabla Chat Regional ..."
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b customer_service < $sqls/002_ChatRegionalIntegration.sql
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi

       echo "Update Tabla Chat Regional ..."
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b customer_service < $sqls/003_ChatRegionalAgt.sql
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		

       echo "End Log"
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b customer_service < $sqls/004_log_fin_chat_Reg.sql
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		

       dateTime=`date`
       echo " ${dateTime} End Load ..."
exit;
