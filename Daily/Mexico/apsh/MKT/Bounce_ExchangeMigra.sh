#!/bin/bash

#Module to load data from BOB/UMS/OMS into dwh
#usage:
#     load.sh <country> <data_source>
v_system=`pwd`

v_user=${1}
v_pwd=${2}
v__host=${3}
#echo  "Starting processing ${1}"
v_country=${4}

v_date=${5}

if [ $v_country=Mexico ]
	then pais=mexico
else [ $v_country=Colombia ]
	pais=colombia
fi

echo "fecha:"$v_date

#absolute path to the linio-git repository
path="../.."
#parse config.ini
. $path/scripts/parser_config.sh
read_ini $path/configChannels.ini

for environment in `cat ./resources/Bounce_Exchange.txt`
do
	echo "--------------------------------------------------------"
		#get the var names according the parameter
		channel_dir="INI__${environment}__DIRECTORY"
		channel_file="INI__${environment}__LOADFILE"
		DB_DATABASE="INI__${environment}__DB_DATABASE"
		DB_TABLE="INI__${environment}__DB_TABLE"
		

		#shell double referencing
		channel_dir=`eval echo \\$\$channel_dir `
		channel_file=`eval echo \\$\$channel_file`
		DB_TABLE=`eval echo \\$\$DB_TABLE`
		DB_DB=`eval echo \\$\$DB_DATABASE`

		channel_dir=`eval echo \$channel_dir `
		channel_file=`eval echo \$channel_file`
		DB_TABLE=`eval echo \$DB_TABLE`
		DB_DB=`eval echo "\$DB_DB"`
		v_folder='/var/lib/jenkins/linio-bi/Bounce_Exchange/'
		
		#echo "channel_dir	"--$channel_dir
		#echo "channel_file	"--$channel_file
		#echo "DB_TABLE		"--$DB_TABLE
		#echo "v__host		"--$v__host
		#echo "DB_DB			"--$DB_DB
		#echo "name			"--$name
		#echo "type			"--$type
		#pwd
		
		cat $v_system/sqls/MKT/Bounce_Exchange/Bounce_Exchange.sql > ./sqls/MKT/Bounce_Exchange/tmp
		cat $v_system/sqls/MKT/Bounce_Exchange/tmp  | sed s/@v_file@/${channel_file}/g>  $v_system/sqls/MKT/Bounce_Exchange/tmp1
		cat $v_system/sqls/MKT/Bounce_Exchange/tmp1 | sed s/@v_host@/${v__host}/g>       $v_system/sqls/MKT/Bounce_Exchange/tmp2
		cat $v_system/sqls/MKT/Bounce_Exchange/tmp2 | sed s/@v_db@/${DB_DB}/g>           $v_system/sqls/MKT/Bounce_Exchange/tmp3
		cat $v_system/sqls/MKT/Bounce_Exchange/tmp3 | sed s/@v_tables@/${DB_TABLE}/g>    $v_system/sqls/MKT/Bounce_Exchange/tmp4
		cat $v_system/sqls/MKT/Bounce_Exchange/tmp4 | sed s/@v_country@/${v_country}/g>    $v_system/sqls/MKT/Bounce_Exchange/tmp5
		cat $v_system/sqls/MKT/Bounce_Exchange/tmp5 | sed s/@v_date@/${v_date}/g>    $v_system/sqls/MKT/Bounce_Exchange/tmp6
		
		cd $v_folder
		#cat $v_system/sqls/MKT/Bounce_Exchange/tmp4 | sed s/@v_folder@/"${v_folder}"/g>    $v_system/sqls/MKT/Bounce_Exchange/tmp5
		#echo "++++"
		var=$(ls $v_folder/$channel_file) #Donde se guardan los .csv
		for i in $var
		do
			#echo "-------------------"
			mysql -u${v_user}  -p${v_pwd} -h $v__host -b ${DB_DB}  < $v_system/sqls/MKT/Bounce_Exchange/tmp6
			cat $v_system/sqls/MKT/Bounce_Exchange/tmp6
			cd ..
		done
		echo "Se eliminan los temporales de SQL"
		cd $v_system
		
		rm $v_system/sqls/MKT/Bounce_Exchange/tmp* 
done

exit
