v_user=$1
v_pwd=$2
v_country=$3
v_date=`date +"%Y%m%d"`

path=/var/lib/jenkins/git_repository

for v_folder in MKT/DailyRoutine
do
    echo "Loading $v_folder ..."
    sqls="./sqls/${v_folder}"
    for i in $sqls/*.sql
    do
       dateTime=`date`
       echo "|->" $dateTime $i
	   echo "mysql -u $v_user -p${v_pwd}   -h 172.17.12.191  -b production < $i"
       if [ $error -gt 0 ]; then
           exit 1
       fi		
	   
    done
    dateTime=`date`
    echo " ${dateTime} End Load ..."
done

echo ""
#Waiting for Saul's scrapped
v_event=marketing.scrapper
v_reply=360  #Wait two hours...
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
. $path/scripts/ToolKit/monitoring.sh

echo ""
#Facebook
for v_folder in MKT/Facebook
do
    echo "Loading $v_folder ..."
    sqls="./sqls/${v_folder}"
    for i in $sqls/*.sql
    do
       dateTime=`date`
       echo "|-> " $dateTime $i
	   echo "mysql -u $v_user -p${v_pwd}  -h 172.17.12.191  -b facebook < $i"
       if [ $error -gt 0 ]; then
           exit 1
       fi		
	   
    done
    dateTime=`date`
    echo " ${dateTime} End Load ..."
done
echo ""

#Waiting for Facebook
v_country=Regional
v_event=facebook_campaigns
v_reply=2  #Wait two hours...
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
. $path/scripts/ToolKit/monitoring.sh
v_country=$3
echo ""

for v_folder in MKT/Marketing 
do
    echo "Loading $v_folder ..."

    sqls="./sqls/${v_folder}"
    for i in $sqls/*.sql
    do
       dateTime=`date`
       echo "|->"  $dateTime $i
	   echo "mysql -u $v_user -p${v_pwd}  -h 172.17.12.191  -b marketing_report < $i"
       if [ $error -gt 0 ]; then
           exit 1
       fi		
	   
    done
    dateTime=`date`
    echo " ${dateTime} End Load ..."
done
#cat ./logs/${v_date}_MKT_${v_country}.log  | mail -s "${country} MKT Process" jcharles.ruot@linio.com.mx -c carlos.mondragon@linio.com.mx
