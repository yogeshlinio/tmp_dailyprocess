v_user=$1
v_pwd=$2
v_host=$3
v_activity="A_Master"
v_date=`date +"%Y%m%d"`
v_country=Mexico
echo    "$v_country Daily Info..."

    echo "Loading $v_folder ..."

    sqls="./sqls/OPS/Out_Inverse_Logistic_Tracking/"
    for i in $sqls/*.sql
    do
       dateTime=`date`
	   echo $dateTime $i 	   
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b operations_mx < $i
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		
	   
    done
    dateTime=`date`
    echo " ${dateTime} End Load ..."
