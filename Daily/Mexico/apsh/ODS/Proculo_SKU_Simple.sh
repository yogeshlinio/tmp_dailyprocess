v_user=$1
v_pwd=$2
v_host=$3
v_db="development_mx"
v_activity="Item_id"
v_date=`date +"%Y%m%d"`

v_country=Mexico

#echo    "$v_country Daily Info..."
#v_event=out_order_tracking
#v_step=shipping_carrier_tracking_code
#v_reply=8000
#v_dependecy=0 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
#. $lib/ToolKit/monitoring.sh

for v_folder in $v_activity
do
    echo "Loading $v_folder ..."

    sqls="./sqls/ODS/${v_folder}"
    for i in $sqls/*.sql
    do
       dateTime=`date`
	   echo $dateTime $i 	   
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b ${v_db} < $i
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		
	   
    done
    dateTime=`date`
    echo " ${dateTime} End Load ..."
done