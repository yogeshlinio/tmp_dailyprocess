v_user=$1
v_pwd=$2
v_host=$3
v_country=$4
v_date=`date +"%Y%m%d"`

echo ""
v_country=Mexico
#Waiting for Saul's scrapped
v_event=wmsprod.itens_venda
v_reply=1200  #Wait two hours...
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
. ../../scripts/ToolKit/monitoring.sh
echo "---"

for v_folder in OPS
do
    echo "Loading $v_folder ..."
    sqls="./sqls/${v_folder}"
    for i in $sqls/*.sql
    do
		dateTime=`date`
		echo "$dateTime---> $i" 
		mysql -u ${v_user} -p${v_pwd}   -h ${v_host} -b production < $i
        error=`echo $?`
        if [ $error -gt 0 ]; then
           exit 1
        fi		
	done
	dateTime=`date`
		echo "($dateTime)::End Load ..."
done
