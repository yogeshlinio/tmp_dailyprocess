v_user=$1
v_pwd=$2
v_host=$3
v_country=$4
v_date=`date +"%Y%m%d"`


echo    "$v_country Daily Info..."
v_event=CRM.loading_channels
v_reply=1000
v_step=finish
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
#. /home/cmondragon/production/ToolKit/monitoring.sh
. $lib/ToolKit/monitoring.sh

for v_folder in Daily_Campaigns Daily_Interfaces
do
    echo "Loading $v_folder ..."

    sqls="./sqls/CRM/${v_folder}"
    for i in $sqls/*.sql
    do
       dateTime=`date`
	   echo $dateTime $i 	   
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b CRM_MX < $i
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		
	   
    done
    dateTime=`date`
    echo " ${dateTime} End Load ..."
done

#Instalations on the final table
echo    "$v_country Daily Info..."
v_event=CRM.A_CRM_CAMPAIGN_EMAIL_SAMPLE
v_reply=120
v_step=finish
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
#. /home/cmondragon/production/ToolKit/monitoring.sh
. $lib/ToolKit/monitoring.sh

for v_folder in Daily_Instalation
do
    echo "Loading $v_folder ..."

    sqls="./sqls/CRM/${v_folder}"
    for i in $sqls/*.sql
    do
       dateTime=`date`
	   echo $dateTime $i 	   
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b CRM_MX < $i
       if [ $error -gt 0 ]; then
           exit 1
       fi		
	   
    done
    dateTime=`date`
    echo " ${dateTime} End Load ..."
done




exit;
