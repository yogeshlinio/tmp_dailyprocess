v_user=$1
v_pwd=$2
v_host=$3
v_country=$4
v_folder=$5
v_date=`date +"%Y%m%d"`

pwd
cd sqls/OPS
pwd
		dateTime=`date`
		echo "Start--($dateTime):: mysql -u ${v_user} -p${v_pwd}   -h ${v_host} -b production < ${v_folder}.sql" 
		mysql -u ${v_user} -p${v_pwd}   -h ${v_host} -b production < ${v_folder}.sql
        error=`echo $?`
        if [ $error -gt 0 ]; then
           exit 1
        fi		
		
		dateTime=`date`
		echo "($dateTime)::End Load ..."
