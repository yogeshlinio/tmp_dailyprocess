v_environment=$1
v_country=$2
v_verbose=$3
v_times=1

PATH=$PATH:/usr/bin/mysql
export PATH
#absolute path to the linio-git repository
lib=`pwd`
lib=$lib/scripts
export lib

home=`pwd`
export home

global="./Daily/Global"
export global


#parse config.ini
. $lib/parser_config.sh
read_ini ./config.ini 

#get the var names according the parameter
v_dateExc=`date`
echo "Execution..." ${v_dateExc}  ${v_country}
pwd

. $lib/ToolKit/Testing.sh ${v_environment} ${v_country} ${v_verbose}
error=`echo $?`
if [ $error -gt 0 ]; then
    exit 1
fi