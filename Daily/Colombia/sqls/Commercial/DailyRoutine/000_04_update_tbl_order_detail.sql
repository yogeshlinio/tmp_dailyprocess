
SELECT  'Daily routine: tbl_order_detail START',now();

SET @itemID=:0 ;
# Variable para actualizacion de advertising
DECLARE dummy_count INT DEFAULT 0;

SELECT  'update development_co.tbl_order_detail START',now();

SELECT @last_date:=max(date) from development_co.tbl_order_detail;


update development_co.tbl_order_detail join bob_live_co.sales_order_item
ON id_sales_order_item=Item 
join bob_live_co.sales_order_item_status 
on fk_sales_order_item_status=id_sales_order_item_status
set status_item= sales_order_item_status.name;


update development_co.tbl_order_detail join development_co.status_bob 
on tbl_order_detail.payment_method=status_bob.payment_method 
and tbl_order_detail.status_item=status_bob.status_bob
SET tbl_order_detail.OBC=status_bob.OBC, 
tbl_order_detail.OAC=status_bob.OAC,
tbl_order_detail.PENDING=status_bob.PENDING,
tbl_order_detail.CANCEL=status_bob.CANCEL,
tbl_order_detail.RETURNED=status_bob.RETURNED;


update development_co.tbl_order_detail join development_co.tbl_catalog_product_v2 
on tbl_order_detail.sku=tbl_catalog_product_v2.sku 
SET n1=cat1,n2=cat2,n3=cat3,n4=cat4, tbl_order_detail.Buyer = tbl_catalog_product_v2.Buyer,
tbl_order_detail.Brand = tbl_catalog_product_v2.Brand,
tbl_order_detail.Proveedor = tbl_catalog_product_v2.Supplier,
tbl_order_detail.product_name = tbl_catalog_product_v2.product_name,
tbl_order_detail.variation = tbl_catalog_product_v2.variation,
tbl_order_detail.color = tbl_catalog_product_v2.color,
tbl_order_detail.attribute = tbl_catalog_product_v2.attribute,
tbl_order_detail.new_cat1 = tbl_catalog_product_v2.new_cat1,
tbl_order_detail.new_cat2 = tbl_catalog_product_v2.new_cat2,
tbl_order_detail.new_cat3 = tbl_catalog_product_v2.new_cat3;

#Actualiza todo lo de Advertising
SELECT CASE WHEN (CURDATE() + INTERVAL MIN(ct) DAY) = (LAST_DAY(CURDATE()) + INTERVAL 1 DAY) THEN
                 1
			ELSE 0
	   END 
INTO dummy_count
FROM (
SELECT orderID, COUNT(1) AS ct
FROM development_co.tbl_order_detail
WHERE custID=0 
  AND DAY(date)=0 
GROUP BY orderID) a;

IF dummy_count = 1 THEN
   update development_co.tbl_order_detail t inner join 
   ((SELECT MIN(item) item                
   FROM development_co.tbl_order_detail       
   WHERE custID=0                
   AND DAY(date)=0                
   GROUP BY orderID)) t1 on t.item = t1.item 
   SET date=CURDATE()-INTERVAL 1 DAY,     
   year=YEAR(CURDATE()-INTERVAL 1 DAY),    
   month=MONTH(CURDATE()-INTERVAL 1 DAY),    
   yrmonth_ordered=(YEAR(CURDATE()-INTERVAL 1 DAY)*100)+(MONTH(CURDATE()-INTERVAL 1 DAY)),    
   yrmonth_delivered=(YEAR(CURDATE()-INTERVAL 1 DAY)*100)+(MONTH(CURDATE()-INTERVAL 1 DAY)),    
   yrmonth=(YEAR(CURDATE()-INTERVAL 1 DAY)*100)+(MONTH(CURDATE()-INTERVAL 1 DAY));
END IF;



SELECT 'INSERT NEW DATA',NOW();

set @itemID = (select item from development_co.tbl_order_detail 
where  CustID  > 0 order by item desc limit 1);

insert into development_co.tbl_order_detail (CustID,OrderID,order_nr,payment_method,unit_price,paid_price,
coupon_money_value,coupon_code,date,hour,sku,Item,
status_item,OBC,PENDING,CANCEL,OAC,RETURNED,n1,n2,n3,n4,
tax_percent,unit_price_after_vat,
paid_price_after_vat,coupon_money_after_vat,cost_pet,costo_oferta,costo_after_vat,
delivery_cost_supplier,
buyer,brand,product_name,peso,proveedor,sku_config,ciudad,region,shipping_fee,orderShippingFee, about_linio,
assisted_sales_operator)
(select `sales_order`.`fk_customer` AS `CustID`,`sales_order`.`id_sales_order` AS `OrderID`,
`sales_order`.`order_nr` AS `order_nr`,`sales_order`.`payment_method` AS `payment_method`,
`sales_order_item`.`unit_price` AS `unit_price`,
`sales_order_item`.`paid_price` AS `paid_price`,
`sales_order_item`.`coupon_money_value` AS `coupon_money_value`,`sales_order`.`coupon_code` AS `coupon_code`,
cast(`sales_order`.`created_at` as date) AS `date`,
concat(hour(`sales_order`.`created_at`),':',minute(`sales_order`.`created_at`),':',
second(`sales_order`.`created_at`)) AS `hour`,
`sales_order_item`.`sku` AS `sku`,
`sales_order_item`.`id_sales_order_item` AS `Item`,
`sales_order_item_status`.`name` AS `status_item`,
(select `status_bob`.`OBC` from development_co.`status_bob` 
where ((`status_bob`.`payment_method` = `sales_order`.`payment_method`) and (`sales_order_item_status`.`name` = `status_bob`.`status_bob`)) limit 1) AS `OBC`,
(select `status_bob`.`PENDING` from `status_bob` where ((`status_bob`.`payment_method` = `sales_order`.`payment_method`) and 
(`sales_order_item_status`.`name` = `status_bob`.`status_bob`)) limit 1) AS `PENDING`,
(select `status_bob`.`CANCEL` from `status_bob` where ((`status_bob`.`payment_method` = `sales_order`.`payment_method`) and (`sales_order_item_status`.`name` = `status_bob`.`status_bob`)) limit 1) AS `CANCEL`,
(select `status_bob`.`OAC` from `status_bob` where ((`status_bob`.`payment_method` = `sales_order`.`payment_method`) and (`sales_order_item_status`.`name` = `status_bob`.`status_bob`)) limit 1) AS `OAC`,
(select `status_bob`.`RETURNED` from `status_bob` where ((`status_bob`.`payment_method` = `sales_order`.`payment_method`)
and (`sales_order_item_status`.`name` = `status_bob`.`status_bob`)) limit 1) AS `RETURNED`,
`tbl_catalog_product_v2`.`cat1` AS `n1`,
`tbl_catalog_product_v2`.`cat2` AS `n2`,
`tbl_catalog_product_v2`.`cat3` AS `n3`,
`tbl_catalog_product_v2`.`cat4` AS `n4`,
`tbl_catalog_product_v2`.`tax_percent` AS `tax_percent`,
(`sales_order_item`.`unit_price` / (1 + (`tbl_catalog_product_v2`.`tax_percent` / 100))) AS `unit_price_after_vat`,
(`sales_order_item`.`paid_price` / (1 + (`tbl_catalog_product_v2`.`tax_percent` / 100))) AS `paid_price_after_vat`,
(`sales_order_item`.`coupon_money_value` / (1 + (`tbl_catalog_product_v2`.`tax_percent` / 100))) AS `coupon_money_after_vat`,
`tbl_catalog_product_v2`.`cost` AS `cost_pet`,
IF(`tbl_catalog_product_v2`.`cost`<`sales_order_item`.`cost`,`sales_order_item`.`cost`,0) AS `costo_oferta`,
`sales_order_item`.`cost` AS `costo_after_vat`,
if(isnull(`sales_order_item`.`delivery_cost_supplier`),`tbl_catalog_product_v2`.`inbound`,
`sales_order_item`.`delivery_cost_supplier`) AS `delivery_cost_supplier`,
`tbl_catalog_product_v2`.`Buyer` AS `buyer`,
`tbl_catalog_product_v2`.`Brand` AS `brand`,
`tbl_catalog_product_v2`.`product_name` AS `Product_name`,
ifnull(`tbl_catalog_product_v2`.`package_weight`,ifnull(`tbl_catalog_product_v2`.`product_weight`,1)) AS `peso`,
`tbl_catalog_product_v2`.`Supplier` AS `proveedor`,
`tbl_catalog_product_v2`.`sku_config` AS `sku_config`,
if(isnull(sales_order_address.municipality),
sales_order_address.city,sales_order_address.municipality) AS ciudad,
sales_order_address.region AS region,
sales_order_item.shipping_amount as shipping_fee,
sales_order.shipping_amount as ordershippingFee,
sales_order.about_linio as about_linio,
sales_order.assisted_sales_operator
from ((((bob_live_co.`sales_order_item` join bob_live_co.`sales_order_item_status`) join bob_live_co.`sales_order`) 
join development_co.`tbl_catalog_product_v2`) join bob_live_co.`sales_order_address`) 
where
((`sales_order_item`.`fk_sales_order_item_status` = `sales_order_item_status`.`id_sales_order_item_status`) 
and (`sales_order_item`.`id_sales_order_item` > itemID) and (`sales_order`.`id_sales_order` = `sales_order_item`.`fk_sales_order`) 
and (`sales_order`.`fk_sales_order_address_shipping` = `sales_order_address`.`id_sales_order_address`)
and (`sales_order_item`.`sku` = `tbl_catalog_product_v2`.`sku`)
));

update development_co.tbl_order_detail
set year = year(date) , month = month(date) , yrmonth = date_format(date, '%Y%m')
where (date >=@last_date or yrmonth = 0 or yrmonth is null) and day(date) > 0;


/*call daily_extra_queries(@last_date);*/

update development_co.tbl_order_detail set buyer = trim(both '	' from buyer);

update development_co.tbl_order_detail join development_co.status_bob 
on tbl_order_detail.payment_method=status_bob.payment_method 
and tbl_order_detail.status_item=status_bob.status_bob
SET tbl_order_detail.OBC=status_bob.OBC, 
tbl_order_detail.OAC=status_bob.OAC,
tbl_order_detail.PENDING=status_bob.PENDING,
tbl_order_detail.CANCEL=status_bob.CANCEL,
tbl_order_detail.RETURNED=status_bob.RETURNED;

update development_co.tbl_order_detail join development_co.tbl_catalog_product_v2 
on tbl_order_detail.sku=tbl_catalog_product_v2.sku 
SET n1=cat1,n2=cat2,n3=cat3,n4=cat4, tbl_order_detail.Buyer = tbl_catalog_product_v2.Buyer,
tbl_order_detail.Brand = tbl_catalog_product_v2.Brand,
tbl_order_detail.Proveedor = tbl_catalog_product_v2.Supplier,
tbl_order_detail.product_name = tbl_catalog_product_v2.product_name,
tbl_order_detail.variation = tbl_catalog_product_v2.variation,
tbl_order_detail.color = tbl_catalog_product_v2.color,
tbl_order_detail.attribute = tbl_catalog_product_v2.attribute,
tbl_order_detail.new_cat1 = tbl_catalog_product_v2.new_cat1,
tbl_order_detail.new_cat2 = tbl_catalog_product_v2.new_cat2,
tbl_order_detail.new_cat3 = tbl_catalog_product_v2.new_cat3,
tbl_order_detail.repurchase_status = tbl_catalog_product_v2.repurchase_status,
tbl_order_detail.model = tbl_catalog_product_v2.model;

#Is marketplace
update development_co.tbl_order_detail
INNER JOIN bob_live_co.sales_order_item 
ON tbl_order_detail.item=sales_order_item.id_sales_order_item
SET tbl_order_detail.is_marketplace=sales_order_item.is_option_marketplace;

update development_co.tbl_order_detail inner join development_co.tbl_exc_marketplace
on tbl_order_detail.sku_config = tbl_exc_marketplace.sku_config
set is_marketplace = 1;

update development_co.tbl_order_detail set is_marketplace = 0 where is_marketplace is null;


SELECT 'START PC2',NOW();
TRUNCATE development_co.tbl_group_order_detail;
INSERT INTO development_co.tbl_group_order_detail (orderid, items, pesoTotal, grandTotal)
(SELECT orderid, count(order_nr) as items,
SUM(greatest(cast(replace(IF(isnull(peso),1,peso),',','.') as DECIMAL(21,6)),1.0))as pesoTotal,
SUM(paid_price) as grandTotal
FROM development_co.tbl_order_detail WHERE  OAC=1 and RETURNED=0 and rejected = 0 group by order_nr);	


TRUNCATE development_co.tbl_group_order_detail_gross;
INSERT INTO development_co.tbl_group_order_detail_gross (orderid, items, pesoTotal, grandTotal)
(SELECT orderid, count(order_nr) as items,
SUM(greatest(cast(replace(IF(isnull(peso),1,peso),',','.') as DECIMAL(21,6)),1.0))as pesoTotal,
SUM(paid_price) as grandTotal
FROM development_co.tbl_order_detail WHERE  OBC=1 group by order_nr);	



update development_co.tbl_order_detail SET orderPeso = 0, nr_items = 0, orderTotal = 0, net_orders = 0, gross_items = 0;

UPDATE  development_co.tbl_order_detail AS t 
inner JOIN development_co.tbl_group_order_detail as e 
ON e.orderid = t.orderID 
SET t.orderPeso = e.pesoTotal , t.nr_items=e.items, t.orderTotal=e.grandTotal
where t.OAC=1 and t.RETURNED=0 and t.rejected = 0;

UPDATE development_co.tbl_order_detail AS t 
inner JOIN development_co.tbl_group_order_detail_gross as e 
ON e.orderid = t.orderID 
SET t.orderPeso_gross = e.pesoTotal , t.gross_items=e.items , t.orderTotal_gross=e.grandTotal
where t.OBC=1;


update development_co.tbl_order_detail as t
SET gross_orders = if(gross_items is null,0,1/gross_items) where OBC = 1;


update development_co.tbl_order_detail as t
SET net_orders = if(nr_items is null, 0,1/nr_items) where OAC = 1 and RETURNED = 0 and rejected = 0;

/*l_order_detail inner join (select yrmonth, orderID, count(item) items
from tbl_bi_ops_delivery inner join tbl_order_detail on item = item_id
where date_shipped is not null
group by yrmonth, orderID) t on tbl_order_detail.orderID = t.orderID
 set shipped_items = t.items;
*/

update development_co.tbl_order_detail set orderShippingFee=0.0 where orderShippingFee is null;


#San Andrés, Providencia y Santa Catalina
update development_co.tbl_order_detail set tax_percent = 0 where region like 'Archipielago De San Andres, Providencia Y Santa Catalina%' and date >= '2013-07-23';
update development_co.tbl_order_detail set unit_price_after_vat = unit_price/(1+tax_percent/100), paid_price_after_vat = paid_price/(1+tax_percent/100)
where region like 'Archipielago De San Andres, Providencia Y Santa Catalina%' and date >= '2013-07-23';


update development_co.tbl_order_detail 
set shipping_fee_after_vat=if(shipping_fee is null, 0, shipping_fee/(1+tax_percent/100));

update development_co.tbl_order_detail set gross_grand_total_after_vat=shipping_fee_after_vat+paid_price_after_vat WHERE OBC = 1;
update development_co.tbl_order_detail set net_grand_total_after_vat=shipping_fee_after_vat+paid_price_after_vat 
WHERE (OAC = 1 and RETURNED = 0 and rejected = 0);


update development_co.tbl_order_detail set ciudad = 'Maicao' , region = 'La Guajira'  where ciudad = 'LA GUAJIRA' and region = 'MAICAO';
update development_co.tbl_order_detail set ciudad = 'Bogota' , region = 'Bogota D.C'  where ciudad = 'Bogota' and region = 'Bogota';
update development_co.tbl_order_detail set ciudad = 'Bogota' , region = 'Cundinamarca'  where ciudad = '​BOGOTÁ D.C' and region = 'CUNDINAMARCA';
update development_co.tbl_order_detail set ciudad = 'Armenia' , region = 'Quindio'  where ciudad = '​​ARMENIA (QUI)' and region = 'QUINDIO';
update development_co.tbl_order_detail set ciudad = 'Piedecuesta' , region = 'Santander'  where ciudad = 'pie de cuesta' and region = 'Santander';
update development_co.tbl_order_detail set ciudad = 'Orito' , region = 'Putumayo'  where ciudad = 'Putumayo' and region = 'Orito';
update development_co.tbl_order_detail set ciudad = 'Ibague' , region = 'Tolima'  where ciudad = 'Tolima' and region = 'Ibague';

update development_co.tbl_order_detail t 
inner join bob_live_co.shipment_zone_mapping t2 
on t.ciudad = t2.area_2 and t.region=t2.area_1
set t.fk_shipment_zone_mapping = t2.id_shipment_zone_mapping 
where yrmonth>=201305;


update development_co.tbl_order_detail 
set fk_courier=1 where courier like '%deprisa%';

update development_co.tbl_order_detail 
set fk_courier=2 where courier like '%servientrega%';

update development_co.tbl_order_detail 
set fk_courier=3 where courier like '%thomas%';


update development_co.tbl_order_detail set grand_total_after_vat=shipping_fee_after_vat+paid_price_after_vat;
update development_co.tbl_order_detail set paid_price_after_vat_sin_cac =  paid_price_after_vat;

update development_co.tbl_order_detail SET paid_price_after_vat = paid_price/(1+tax_percent/100) 
WHERE ( channel like '%CAC%')
AND obc = 1 ;


update development_co.tbl_order_detail set marketingCost = coupon_money_value/(1+tax_percent/100), 
voucherMktCost = marketingCost = coupon_money_value/(1+tax_percent/100),
paid_price_after_vat_sin_cac = paid_price_after_vat + 
if(coupon_money_value/(1+tax_percent/100) is null,0,coupon_money_value/(1+tax_percent/100))
where  ( (channel like '%CAC%' or coupon_code like '%CAC%') 
and (channel_group='Newsletter' OR channel_group like '%Fan%Page%' or channel_group like '%Facebook%') );

/*

update development_co.tbl_order_detail set CS=6930.0/CAST(nr_items AS DECIMAL),WH=0.0 
where date>='2012-05-01' and date<'2012-07-01'  
and OAC = 1 and RETURNED = 0 and rejected  = 0;
update development_co.tbl_order_detail set CS=6930.0/CAST(nr_items AS DECIMAL),WH=3520.0/CAST(nr_items AS DECIMAL) 
where date>='2012-07-01' 
and date<'2012-08-01' and OAC = 1 and RETURNED = 0 and rejected  = 0;
update development_co.tbl_order_detail 
set CS=6.6*2200/CAST(nr_items AS DECIMAL),WH=12.14*2200/CAST(nr_items AS DECIMAL) 
where date>='2012-08-01' 
and date<'2012-09-01' and OAC = 1 and RETURNED = 0 and rejected  = 0;
update development_co.tbl_order_detail 
set CS=7.53*2200/CAST(nr_items AS DECIMAL),WH=12.43*2200/CAST(nr_items AS DECIMAL) 
where date>='2012-09-01' 
and date<'2012-10-01' and OAC = 1 and RETURNED = 0 and rejected  = 0;
update development_co.tbl_order_detail 
set CS=5.18*2200/CAST(nr_items AS DECIMAL),WH=8.98*2200/CAST(nr_items AS DECIMAL) 
where date>='2012-10-01' 
and date<'2012-11-01' and OAC = 1 and RETURNED = 0 and rejected  = 0;
update development_co.tbl_order_detail 
set CS=5.06*2200/CAST(nr_items AS DECIMAL),WH=6.27*2200/CAST(nr_items AS DECIMAL) 
where date>='2012-11-01' 
and date<'2012-12-01' and OAC = 1 and RETURNED = 0 and rejected  = 0;
update development_co.tbl_order_detail 
set CS=6.74*2200/CAST(nr_items AS DECIMAL),WH=8.54*2200/CAST(nr_items AS DECIMAL) 
where date>='2012-12-01' 
and date<'2013-01-01' and OAC = 1 and RETURNED = 0 and rejected  = 0;
update development_co.tbl_order_detail 
set CS=5.78*2200/CAST(nr_items AS DECIMAL),WH=5.93*2200/CAST(nr_items AS DECIMAL) 
where date>='2013-01-01' 
and date<'2013-02-01' and OAC = 1 and RETURNED = 0 and rejected  = 0;
update development_co.tbl_order_detail 
set CS=6.45*2200/CAST(nr_items AS DECIMAL),WH=5.28*2200/CAST(nr_items AS DECIMAL) 
where date>='2013-02-01' 
and date<'2013-03-01' and OAC = 1 and RETURNED = 0 and rejected  = 0;
update development_co.tbl_order_detail 
set CS=6.06*2200/CAST(nr_items AS DECIMAL),WH=4.98*2200/CAST(nr_items AS DECIMAL) 
where date>='2013-03-01' 
and date<'2013-04-01' and OAC = 1 and RETURNED = 0 and rejected  = 0;
update development_co.tbl_order_detail 
set CS=6.73*2200/CAST(nr_items AS DECIMAL),WH=6.31*2200/CAST(nr_items AS DECIMAL) 
where date>='2013-04-01' 
and date<'2013-05-01' and OAC = 1 and RETURNED = 0 and rejected  = 0;

#Mayo CC
update development_co.tbl_order_detail inner join 
(select date,count(distinct orderID) net_orders from development_co.tbl_order_detail  
where oac = 1 and returned = 0 and yrmonth = 201305 group by date) orders
 on tbl_order_detail.date = orders.date
set CS=(61217/31/orders.net_orders/nr_items)*2350
where tbl_order_detail.yrmonth = 201305 and OAC = 1 and RETURNED = 0 and rejected  = 0;

#WH 
update development_co.tbl_order_detail inner join 
(select date,count(distinct orderID) net_orders from development_co.tbl_order_detail  
where oac = 1 and returned = 0 and yrmonth = 201305 group by date) orders
 on tbl_order_detail.date = orders.date
set WH=(65068/31/orders.net_orders/nr_items)*2350
where tbl_order_detail.yrmonth = 201305 and OAC = 1 and RETURNED = 0 and rejected  = 0;

#Junio CC
update development_co.tbl_order_detail inner join 
(select date,count(distinct orderID) net_orders from development_co.tbl_order_detail  
where oac = 1 and returned = 0 and yrmonth = 201306 group by date) orders
 on tbl_order_detail.date = orders.date
set CS=(63626/30/orders.net_orders/nr_items)*2350
where tbl_order_detail.yrmonth = 201306 and (OAC = 1 and RETURNED = 0 and rejected  = 0);

#wh
update development_co.tbl_order_detail inner join 
(select date,count(distinct orderID) net_orders from development_co.tbl_order_detail  
where oac = 1 and returned = 0 and yrmonth = 201306 group by date) orders
 on tbl_order_detail.date = orders.date
set WH=(63221/30/orders.net_orders/nr_items)*2350
where tbl_order_detail.yrmonth = 201306 and (OAC = 1 and RETURNED = 0 and rejected  = 0);

#Julio CC
update development_co.tbl_order_detail inner join 
(select date,count(distinct orderID) net_orders from development_co.tbl_order_detail  
where oac = 1 and returned = 0 and yrmonth = 201307 
and (channel_group not like '%Corporate%Sales%'  or channel_group is null) group by date) orders
 on tbl_order_detail.date = orders.date
set CS=(60422/31/orders.net_orders/nr_items)*2350
where tbl_order_detail.yrmonth = 201307 and (OAC = 1 and RETURNED = 0 and rejected  = 0)
 and (channel_group not like '%Corporate%Sales%'  or channel_group is null);

#wh
update development_co.tbl_order_detail inner join 
(select date,count(distinct orderID) net_orders from development_co.tbl_order_detail  
where oac = 1 and returned = 0 and yrmonth = 201307 
and (channel_group not like '%Corporate%Sales%'  or channel_group is null) group by date) orders
 on tbl_order_detail.date = orders.date
set WH=(65512/31/orders.net_orders/nr_items)*2350
where tbl_order_detail.yrmonth = 201307 and (OAC = 1 and RETURNED = 0 and rejected  = 0)
 and (channel_group not like '%Corporate%Sales%'  or channel_group is null);

#Agosto
update development_co.tbl_order_detail inner join 
(select date,count(distinct orderID) net_orders 
from development_co.tbl_order_detail 
where oac = 1 and returned = 0 and yrmonth = 201308 and custID > 0
and (channel_group not like '%Corporate%Sales%'  or channel_group is null) group by date) orders
 on tbl_order_detail.date = orders.date
set CS=(33277/31/orders.net_orders/nr_items)*2350
where tbl_order_detail.yrmonth = 201308 and (OAC = 1 and RETURNED = 0 and rejected  = 0)
 and (channel_group not like '%Corporate%Sales%'  or channel_group is null) and custID > 0;

#Septiembre
update development_co.tbl_order_detail inner join 
(select date,count(distinct orderID) net_orders 
from development_co.tbl_order_detail 
where oac = 1 and returned = 0 and yrmonth = 201309 and custID > 0
and (channel_group not like '%Corporate%Sales%'  or channel_group is null) group by date) orders
 on tbl_order_detail.date = orders.date
set CS=(35973/30/orders.net_orders/nr_items)*2350
where tbl_order_detail.yrmonth = 201309 and (OAC = 1 and RETURNED = 0 and rejected  = 0)
 and (channel_group not like '%Corporate%Sales%'  or channel_group is null) and custID > 0;
*/
#wh
#No aplica para las órdenes de dropshipping
/*update development_co.tbl_order_detail 
INNER JOIN
   (SELECT date,count(distinct orderID) net_orders 
    from development_co.tbl_order_detail  
    INNER JOIN tbl_bi_ops_delivery b
    on item =item_id
    WHERE oac = 1 and returned = 0 and yrmonth = 201309 and custID > 0
    and IFNULL(tbl_order_detail.is_marketplace,0) = 0
    and (channel_group not like '%Corporate%Sales%'  or channel_group is null)
    and  not(fulfilment_wms = 'Dropshipping'  
    or (fulfilment_wms is null and fulfilment_bob = 'Dropshipping')) group by date
   ) orders
ON tbl_order_detail.date = orders.date
SET WH=(56596/30/orders.net_orders/nr_items)*2350
WHERE tbl_order_detail.yrmonth = 201309 and (OAC = 1 and RETURNED = 0 and rejected  = 0 and CustID > 0 and IFNULL(tbl_order_detail.is_marketplace,0) = 0)
 and (channel_group not like '%Corporate%Sales%'  or channel_group is null);*/


#Noviembre
update development_co.tbl_order_detail inner join 
(select date,count(distinct orderID) net_orders 
from development_co.tbl_order_detail 
where oac = 1 and returned = 0 and yrmonth = 201311 and custID > 0
and (channel_group not like '%Corporate%Sales%'  or channel_group is null) group by date) orders
 on tbl_order_detail.date = orders.date
set CS=(43071.65820/30/orders.net_orders/nr_items)*2450
where tbl_order_detail.yrmonth = 201311 and (OAC = 1 and RETURNED = 0 and rejected  = 0)
 and (channel_group not like '%Corporate%Sales%'  or channel_group is null) and custID > 0;


#Diciembre
update development_co.tbl_order_detail inner join 
(select date,count(distinct orderID) net_orders 
from development_co.tbl_order_detail 
where oac = 1 and returned = 0 and yrmonth = 201312 and custID > 0
group by date) orders
 on tbl_order_detail.date = orders.date
set CS=(133086592.41/31/orders.net_orders/nr_items)
where tbl_order_detail.yrmonth = 201312 and (OAC = 1 and RETURNED = 0 and rejected  = 0)
and custID > 0;

#Enero 2014
update development_co.tbl_order_detail inner join 
(select date,count(distinct orderID) net_orders 
from development_co.tbl_order_detail 
where oac = 1 and returned = 0 and yrmonth = 201401 and custID > 0
group by date) orders
 on tbl_order_detail.date = orders.date
set CS=(123661708/31/orders.net_orders/nr_items)
where tbl_order_detail.yrmonth = 201401 and (OAC = 1 and RETURNED = 0 and rejected  = 0)
and custID > 0;


update development_co.tbl_order_detail set WH=0.0,shipping_cost = 0 
where custID = 0 or  channel_group  like '%Corporate%Sales%' ;

update development_co.tbl_order_detail set shipping_cost=
(SELECT  bogota from shipping_cost where peso=Least(ceil(orderPeso),200))/CAST(nr_items AS DECIMAL) 
where ciudad like '%bogota%' and OAC = 1 and RETURNED = 0 and rejected = 0 and date<='2012-12-11';
update development_co.tbl_order_detail set shipping_cost=
(SELECT  resto_pais from shipping_cost where peso=Least(ceil(orderPeso),200))/CAST(nr_items AS DECIMAL) 
where ciudad not like '%bogota%' and OAC = 1 and RETURNED = 0 and rejected = 0 and date<='2012-12-11';
update development_co.tbl_order_detail set shipping_cost= orderShippingFee/CAST(nr_items AS DECIMAL)
 where OAC = 1 and RETURNED = 0 and date>'2012-12-11' and date < '2013-05-01'; 



update development_co.tbl_order_detail set payment_cost=greatest(0.029*(orderTotal+orderShippingFee)+400,2500)/CAST(nr_items AS DECIMAL)
 where  payment_method ='Pagosonline_Creditcard' and (OAC = 1 and RETURNED = 0 and rejected  = 0);
update development_co.tbl_order_detail set payment_cost=greatest(0.019*(orderTotal+orderShippingFee)+400,2500)/CAST(nr_items AS DECIMAL)
 where  payment_method ='Pagosonline_Pse' and (OAC = 1 and RETURNED = 0 and rejected  = 0);
update development_co.tbl_order_detail set payment_cost=(orderTotal+orderShippingFee)*0.01/CAST(nr_items AS DECIMAL)
 where ciudad like '%bogota%' and  payment_method ='CashOnDelivery_Payment' and (OAC = 1 and RETURNED = 0 and rejected  = 0);
update development_co.tbl_order_detail set payment_cost=(orderTotal+orderShippingFee)*0.015/CAST(nr_items AS DECIMAL) 
where ciudad not like '%bogota%' and  payment_method ='CashOnDelivery_Payment' and (OAC = 1 and RETURNED = 0 and rejected  = 0);
update development_co.tbl_order_detail set payment_cost=2000/CAST(nr_items AS DECIMAL) 
where payment_method ='Consignacion_Payment' and (OAC = 1 and RETURNED = 0 and rejected  = 0); 
update development_co.tbl_order_detail set payment_cost=(0.01664*(orderTotal+orderShippingFee))/CAST(nr_items AS DECIMAL)
 where  payment_method ='Efecty_Payment' and (OAC = 1 and RETURNED = 0 and rejected  = 0);

update development_co.tbl_order_detail t
JOIN development_co.tbl_paypal_trm_diaria d
ON t.date=d.fecha
set t.payment_cost=0.038*(t.orderTotal+t.orderShippingFee)+(0.3*d.trm)/CAST(t.nr_items AS DECIMAL)
 where t.payment_method ='Paypal_Express_Checkout' and (t.OAC = 1 and t.RETURNED = 0 and t.rejected  = 0);

#####################################################################################
#   +++++++++++++++++++++++++++++++PENDIENTE+++++++++++++++++++++++++++++++++++++   #
#####################################################################################
/* AGREGAR PAYPAL 
¿Qué pasaría si en la tabla diaria queda un día sin tasa de cambio?, MAX o MIN trm value?
*/
#####################################################################################
#####################################################################################



update development_co.tbl_order_detail set delivery_cost_supplier=0.0 where proveedor='IZC MAYORISTA S.A.S';



update development_co.tbl_order_detail set shipping_fee=0.0 where shipping_fee is null;
update development_co.tbl_order_detail set delivery_cost_supplier=0.0 where delivery_cost_supplier is null;
update development_co.tbl_order_detail set WH=0.0 where WH is null;
update development_co.tbl_order_detail set CS=0.0 where CS is null;
update development_co.tbl_order_detail set payment_cost=0.0 where payment_cost is null;
update development_co.tbl_order_detail set shipping_cost=0.0 where shipping_cost is null;
