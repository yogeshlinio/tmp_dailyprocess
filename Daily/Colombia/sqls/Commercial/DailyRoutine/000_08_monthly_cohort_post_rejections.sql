
Select 'Start: tbl_monthly_cohort ', now();

truncate development_co.tbl_monthly_cohort_post_rejections;
insert into development_co.tbl_monthly_cohort_post_rejections(CustID,cohort,idFirstOrder,idLastOrder,firstOrder, lastOrder,rangoDias)
select CustId,
cast(concat(year(firstOrder),if(length(month(firstOrder)) < 2,concat('0', month(firstOrder)),month(firstOrder)))as signed) as cohort,
idFirstOrder,idLastOrder,firstOrder, lastOrder, DATEDIFF(cast(now() as date), lastOrder) rangoDias from development_co.view_cohort_post_rejections;

update development_co.tbl_monthly_cohort_post_rejections set rangoDias_label =30 where rangoDias >= 30 and rangoDias <= 60;
update development_co.tbl_monthly_cohort_post_rejections set rangoDias_label =60 where rangoDias >= 61 and rangoDias <= 90;
update development_co.tbl_monthly_cohort_post_rejections set rangoDias_label =90 where rangoDias >= 91 and rangoDias <= 120;
update development_co.tbl_monthly_cohort_post_rejections set rangoDias_label =120 where rangoDias >= 121 and rangoDias <= 150;
update development_co.tbl_monthly_cohort_post_rejections set rangoDias_label =150 where rangoDias >= 151 and rangoDias <= 180;
update development_co.tbl_monthly_cohort_post_rejections set rangoDias_label =180 where rangoDias >= 181 and rangoDias <= 210;
update development_co.tbl_monthly_cohort_post_rejections set rangoDias_label =210 where rangoDias >= 210 and rangoDias <= 240;
update development_co.tbl_monthly_cohort_post_rejections set rangoDias_label =240 where rangoDias >= 241 and rangoDias <= 270;
update development_co.tbl_monthly_cohort_post_rejections set rangoDias_label =270 where rangoDias >= 271 and rangoDias <= 300;
update development_co.tbl_monthly_cohort_post_rejections set rangoDias_label =300 where rangoDias >= 301 and rangoDias <= 330;
update development_co.tbl_monthly_cohort_post_rejections set rangoDias_label =330 where rangoDias >= 331 and rangoDias <= 360;
update development_co.tbl_monthly_cohort_post_rejections set rangoDias_label =360 where rangoDias >= 361 and rangoDias <= 390;


update development_co.tbl_monthly_cohort_post_rejections inner join development_co.tbl_order_detail on idFirstOrder = orderID 
and tbl_monthly_cohort_post_rejections.CustID = tbl_order_detail.CustID
set tbl_monthly_cohort_post_rejections.coupon_code = tbl_order_detail.coupon_code;


update development_co.tbl_monthly_cohort_post_rejections inner join bob_live_co.sales_rule
on sales_rule.code=tbl_monthly_cohort_post_rejections.coupon_code 
set tbl_monthly_cohort_post_rejections.fk_sales_rule_set=sales_rule.fk_sales_rule_set;


update development_co.tbl_monthly_cohort_post_rejections inner join bob_live_co.sales_rule_set
on sales_rule_set.id_sales_rule_set=tbl_monthly_cohort_post_rejections.fk_sales_rule_set
set tbl_monthly_cohort_post_rejections.coupon_code_desc=sales_rule_set.description;


update development_co.tbl_monthly_cohort_post_rejections inner join development_co.tbl_order_detail on idLastOrder = orderID
 and tbl_monthly_cohort_post_rejections.CustID = tbl_order_detail.CustID
set tbl_monthly_cohort_post_rejections.coupon_code_last = tbl_order_detail.coupon_code;

update development_co.tbl_monthly_cohort_post_rejections inner join development_co.tbl_order_detail on idLastOrder = orderID 
and tbl_monthly_cohort_post_rejections.CustID = tbl_order_detail.CustID
set tbl_monthly_cohort_post_rejections.coupon_code_last = tbl_order_detail.coupon_code;

update development_co.tbl_monthly_cohort_post_rejections inner join development_co.tbl_order_detail on idLastOrder = orderID and tbl_monthly_cohort_post_rejections.CustID = tbl_order_detail.CustID
set tbl_monthly_cohort_post_rejections.channel_last = tbl_order_detail.channel;

update development_co.tbl_monthly_cohort_post_rejections inner join development_co.tbl_order_detail on idFirstOrder = orderID and tbl_monthly_cohort_post_rejections.CustID = tbl_order_detail.CustID
set tbl_monthly_cohort_post_rejections.channel_first = tbl_order_detail.channel;


update development_co.tbl_monthly_cohort_post_rejections inner join development_co.tbl_order_detail on idFirstOrder = orderID and tbl_monthly_cohort_post_rejections.CustID = tbl_order_detail.CustID
set tbl_monthly_cohort_post_rejections.about_linio = tbl_order_detail.about_linio;

update development_co.tbl_monthly_cohort_post_rejections set coupon_code = '' where coupon_code is null;

update development_co.tbl_monthly_cohort_post_rejections set groupon_check = 'GR' where coupon_code like 'GR%' AND coupon_code_desc like '%groupon%';

update development_co.tbl_monthly_cohort_post_rejections set groupon_check = 'NOTGR' 
where coupon_code_desc not like '%groupon%';

update development_co.tbl_monthly_cohort_post_rejections set groupon_check = 'NOTGR' 
where coupon_code_desc is null ;

update development_co.tbl_order_detail t inner join development_co.CAC_campaigns c on t.sku like concat(c.sku,'%') and t.paid_price = c.paid_price
inner join development_co.tbl_monthly_cohort_post_rejections m on m.custID = t.CustID and m.idFirstOrder = t.orderID
set m.channel_first = c.CAC
where t.date >= fecha_campanha and t.date <= fecha_fin_campanha and oac = 1 and returned = 0 and rejected = 0;

update development_co.tbl_order_detail t inner join development_co.CAC_campaigns c on t.sku like concat(c.sku,'%') and t.paid_price = c.paid_price
inner join development_co.tbl_monthly_cohort_post_rejections m on m.custID = t.CustID and m.idLastOrder = t.orderID
set m.channel_last = c.CAC
where t.date >= fecha_campanha and t.date <= fecha_fin_campanha and oac = 1 and returned = 0 and rejected = 0;

update development_co.tbl_monthly_cohort_post_rejections m inner join 
(select date, t.custID, orderID, sum(t.coupon_money_value/(1+tax_percent/100)) valor from development_co.tbl_order_detail t
 inner join development_co.tbl_monthly_cohort_post_rejections m on t.custID = m.custID
and idFirstOrder = orderID where oac = 1 and returned = 0 and rejected = 0 and t.coupon_code is not null
group by date desc, t.custID, orderID) total on m.custID = total.custID and m.idFirstOrder = total.orderID
set m.coupon_money_value_after_vat = total.valor;


update development_co.tbl_monthly_cohort_post_rejections inner join (select distinct channel, channel_group from development_co.tbl_order_detail ) t on channel_first = channel 
set channel_group_first = channel_group;

update development_co.tbl_monthly_cohort_post_rejections inner join (select distinct channel, channel_group from development_co.tbl_order_detail ) t on channel_last = channel 
set channel_group_last = channel_group;

Select 'End: tbl_monthly_cohort_post_rejections ', now();
