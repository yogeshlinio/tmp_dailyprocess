SELECT  'Daily routine: Start',now();

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'development_co.tbl_order_detail parte 1',
  'start',
  NOW(),
  MAX(date),
  count(*),
  count(*)
FROM
  development_co.tbl_order_detail
;
