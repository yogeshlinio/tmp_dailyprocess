
SELECT 'NEW CUSTOMERS',NOW();
update development_co.tbl_order_detail set  new_customers = 0, new_customers_gross = 0;
update development_co.tbl_order_detail inner join  
(select firstOrder as date,custID as newCustomer, idFirstOrder from development_co.view_cohort ) as tbl_nc on tbl_nc.date=tbl_order_detail.date 
and tbl_nc.newCustomer = tbl_order_detail.custID and tbl_nc.idFirstOrder = orderID
set tbl_order_detail.new_customers=1/nr_items;

update development_co.tbl_order_detail inner join  
(select firstOrder as date,custID as newCustomer, idFirstOrder from development_co.view_cohort_gross ) as tbl_nc on tbl_nc.date=tbl_order_detail.date 
and tbl_nc.newCustomer = tbl_order_detail.custID and tbl_nc.idFirstOrder = orderID
set tbl_order_detail.new_customers_gross=1/gross_items;

update development_co.tbl_order_detail
set year = year(date) , month = month(date) , yrmonth = date_format(date, '%Y%m')
where date >=@last_date and day(date) > 0;


update development_co.tbl_order_detail SET unit_price = 19900, unit_price_after_vat = 19900/1.16, 
paid_price  = 9900, paid_price_after_vat = 9900/1.16, paid_price_after_vat_sin_cac = 9900/1.16
WHERE coupon_code = 'CAC0403' and sku = 'CR610EL90NARLACOL-99496';

update development_co.tbl_order_detail SET voucherMktCost = paid_price_after_vat - unit_price_after_vat, marketingCost = paid_price_after_vat - unit_price_after_vat,
coupon_money_value = unit_price - paid_price
WHERE coupon_code = 'CAC0403' and sku = 'CR610EL90NARLACOL-99496';

update development_co.tbl_order_detail SET paid_price_after_vat = paid_price/(1+tax_percent/100) 
WHERE ( channel like '%CAC%')
AND obc = 1 ;

update development_co.tbl_order_detail set marketingCost = coupon_money_value/(1+tax_percent/100),
paid_price_after_vat_sin_cac = paid_price_after_vat + 
if(coupon_money_value/(1+tax_percent/100) is null,0,coupon_money_value/(1+tax_percent/100))
where  ( channel like '%CAC%') ;

update development_co.tbl_order_detail inner join 
(select date,count(distinct orderID) net_orders 
from development_co.tbl_order_detail 
where oac = 1 and returned = 0 and yrmonth = 201309 and custID > 0
and (channel_group like '%Tele%Sales%'  or channel_group is null) group by date) orders
 on tbl_order_detail.date = orders.date
inner join development_co.tbl_pc_costs on tbl_order_detail.yrmonth  = tbl_pc_costs.yrmonth
set acquisition_costs=(tbl_pc_costs.cost/31/orders.net_orders/nr_items)
where tbl_order_detail.yrmonth = 201309 and (OAC = 1 and RETURNED = 0 and rejected  = 0)
 and (channel_group not like '%Tele%Sales%') and custID > 0
and tbl_pc_costs.cost_type = 'CS Sales';

update development_co.tbl_order_detail inner join 
(select date,count(distinct orderID) net_orders 
from development_co.tbl_order_detail 
where oac = 1 and returned = 0 and yrmonth = 201310 and custID > 0
and (channel_group like '%Tele%Sales%'  or channel_group is null) group by date) orders
 on tbl_order_detail.date = orders.date
inner join development_co.tbl_pc_costs on tbl_order_detail.yrmonth  = tbl_pc_costs.yrmonth
set acquisition_costs=(tbl_pc_costs.cost/31/orders.net_orders/nr_items)
where tbl_order_detail.yrmonth = 201310 and (OAC = 1 and RETURNED = 0 and rejected  = 0)
 and (channel_group not like '%Tele%Sales%') and custID > 0
and tbl_pc_costs.cost_type = 'CS Sales';



#Órdenes viejas

update development_co.tbl_order_detail t inner join (
SELECT distinct yrmonth, t.custID, v.order_nr as order_vieja ,  note comentario_order_vieja,
nuevas.order_nr order_nueva, 
nuevas.channel_group channel_group_nueva
, nuevas.channel channel_nueva
, nuevas.`source/medium` source_medium_nueva
, nuevas.campaign  campaign_nueva
FROM development_co.view_telesales v inner join development_co.tbl_order_detail t
on v.order_nr = t.order_nr
right join (select distinct date,custID, order_nr,orderID, channel_group,
channel,`source/medium`, campaign  from development_co.tbl_order_detail  
where (`source/medium` like '%cross%' or `source/medium` like '%up%' ) and `source/medium` not like '%supp%'
and oac = 1 and returned = 0 and rejected = 0 ) nuevas on t.custID = nuevas.custID) p on t.order_nr = p.order_nueva set t.orden_vieja = p.order_vieja
where p.order_vieja <> p.order_nueva;

insert into production.table_monitoring_log
(country, table_name, updated_at, key_date, total_rows, total_rows_check)
values ('Colombia', 'tbl_order_detail - orden_anterior',now(),now(), row_count(), row_count());

-- Category BP

update development_co.tbl_order_detail t inner join development_mx.A_E_M1_New_CategoryBP m on m.Cat1=t.new_cat1 set t.category_bp=m.catbp where t.item>=itemid;

update development_co.tbl_order_detail t inner join development_mx.A_E_M1_New_CategoryBP m on m.Cat2=t.new_cat2 set t.category_bp=m.catbp where (t.new_cat1   =  "Entretenimiento" or t.new_cat1 like "Electr%nicos") and t.item>=itemid;

#Original price
update development_co.tbl_order_detail t inner join bob_live_co.sales_order_item c on t.item = c.id_sales_order_item
set original_price = c.original_unit_price
where original_price is null;

insert into production.table_monitoring_log
(country, table_name, updated_at, key_date, total_rows, total_rows_check)
values ('Colombia', 'tbl_order_detail - original_unit_price',now(),now(), row_count(), row_count());
