 
select @start_current_month:=cast(date_add(now(), interval - day(now())+1 day) as date);

select @end_current_month:=cast(date_add(now(), interval - 1 day) as date);

select @start_previous_month:=date_add(cast(date_add(now(), interval - day(now())+1 day) as date), interval - 1 month);

select @end_previous_month:=date_add(cast(date_add(now(), interval - 1 day) as date), interval - 1 month);

delete from development_co.tbl_mom_net_sales_per_buyer where date = @end_current_month ;
insert into development_co.tbl_mom_net_sales_per_buyer(date, buyer, netSalesPreviousMonth, netSalesCurrentMonth,month_over_month)
select @end_current_month, currentMonth.buyer, sum(NetSalesPreviousMonth) NetSalesPreviousMonth, sum(NetSalesCurrentMonth) NetSalesCurrentMonth, 
if((sum(NetSalesCurrentMonth)/sum(NetSalesPreviousMonth))-1  is null, 0,1-(sum(NetSalesPreviousMonth)/sum(NetSalesCurrentMonth)))
from
(select buyer, sum(paid_price_after_vat) +sum(if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat)) as NetSalesCurrentMonth
from development_co.tbl_order_detail
where oac = 1 and returned = 0 and date >= @start_current_month and date <= @end_current_month
group by buyer) currentMonth inner join
(select buyer, sum(paid_price_after_vat) +sum(if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat)) as NetSalesPreviousMonth
from development_co.tbl_order_detail
where oac = 1 and returned = 0 and date >= @start_previous_month and date <= @end_previous_month
group by buyer) previousMonth on
currentMonth.buyer = previousMonth.buyer
group by month(@start_current_month), currentMonth.buyer;

insert into development_co.tbl_mom_net_sales_per_buyer(date, buyer, netSalesPreviousMonth, netSalesCurrentMonth,month_over_month)
select date, 'MTD',sum(NetSalesPreviousMonth), sum(NetSalesCurrentMonth), (sum(NetSalesCurrentMonth)/sum(NetSalesPreviousMonth))-1
from (
select @end_current_month date, sum(NetSalesPreviousMonth) NetSalesPreviousMonth, sum(NetSalesCurrentMonth) NetSalesCurrentMonth, 
1-(sum(NetSalesPreviousMonth)/sum(NetSalesCurrentMonth))
from
(select buyer, sum(paid_price_after_vat) +sum(if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat)) as NetSalesCurrentMonth
from development_co.tbl_order_detail
where oac = 1 and returned = 0 and date >= @start_current_month and date <= @end_current_month
group by buyer) currentMonth inner join
(select buyer, sum(paid_price_after_vat) +sum(if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat)) as NetSalesPreviousMonth
from development_co.tbl_order_detail
where oac = 1 and returned = 0 and date >= @start_previous_month and date <= @end_previous_month
group by buyer) previousMonth on
currentMonth.buyer = previousMonth.buyer
group by month(@start_current_month), currentMonth.buyer) p group by date;

update development_co.tbl_mom_net_sales_per_buyer
set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));
