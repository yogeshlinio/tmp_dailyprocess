SELECT  'Daily routine: Tbl_catalog_product START',now();

Select 'Start: catalog_product_v2', now();

DROP TABLE IF EXISTS simple_variation;
CREATE TABLE simple_variation as (select * from simple_variation_view);
ALTER TABLE `development_co`.`simple_variation` ADD PRIMARY KEY (`fk_catalog_simple`) ;


truncate production_co.tbl_catalog_product_stock;
insert into production_co.tbl_catalog_product_stock(fk_catalog_simple,sku,reservedbob,stockbob,availablebob)
select catalog_simple.id_catalog_simple,catalog_simple.sku,
IF(sum(is_reserved) is null, 0, sum(is_reserved))  as reservedBOB 
,ifnull(catalog_stock.quantity,0) as stockbob,
ifnull(catalog_stock.quantity,0)-IF(sum(sales_order_item.is_reserved) is null, 0, sum(is_reserved)) as availablebob
from 
(bob_live_co.catalog_simple left join bob_live_co.catalog_stock on catalog_simple.id_catalog_simple=catalog_stock.fk_catalog_simple) 
left join bob_live_co.sales_order_item
 on sales_order_item.sku=catalog_simple.sku
group by catalog_simple.sku
order by catalog_stock.quantity desc;


UPDATE ((production_co.tbl_catalog_product_stock 
inner join bob_live_co.catalog_warehouse_stock 
on production_co.tbl_catalog_product_stock.fk_catalog_simple = catalog_warehouse_stock.fk_catalog_simple) 
inner join bob_live_co.catalog_supplier_stock 
on production_co.tbl_catalog_product_stock.fk_catalog_simple = catalog_supplier_stock.fk_catalog_simple)
SET
production_co.tbl_catalog_product_stock.ownstock= catalog_warehouse_stock.quantity, 
production_co.tbl_catalog_product_stock.supplierstock= catalog_supplier_stock.quantity;



DROP TABLE IF EXISTS tbl_catalog_product_v3;
CREATE TABLE tbl_catalog_product_v3 as (select * from production_co.catalog_product);
update tbl_catalog_product_v3  set visible='NO' where cat1 is null;

ALTER TABLE `development_co`.`tbl_catalog_product_v3` ADD PRIMARY KEY (`sku`) ;

DROP TABLE IF EXISTS tbl_catalog_product_v2;
ALTER TABLE `development_co`.`tbl_catalog_product_v3` RENAME TO  `development_co`.`tbl_catalog_product_v2` ;

ALTER TABLE `development_co`.`tbl_catalog_product_v2` 
ADD INDEX (`sku_config` ) ;

ALTER TABLE `development_co`.`tbl_catalog_product_v2` ADD COLUMN `sell_list`
 TINYINT NULL DEFAULT 0  AFTER `marketplace_badge_name` ;

ALTER TABLE `development_co`.`tbl_catalog_product_v2` 
ADD INDEX (`sell_list` ) ;

ALTER TABLE `development_co`.`tbl_catalog_product_v2` ADD COLUMN `tracker` VARCHAR(156) NULL  AFTER `sell_list` ;

ALTER TABLE `development_co`.`tbl_catalog_product_v2` ADD COLUMN `interval_age` VARCHAR(15) NULL  AFTER `tracker` ;

/*UPDATE tbl_catalog_product_v2 c inner join tbl_bi_ops_procurement p on c.sku = p.sku_simple 
SET c.tracker = ifnull(p.tracker,''), c.fulfillment_type =  if(fk_procurement_order_type = 7, 'Consignación',c.fulfillment_type);*/

ALTER TABLE `development_co`.`tbl_catalog_product_v2` ADD COLUMN `cost_oms` decimal(21,6)  AFTER `cost` ;

#update tbl_catalog_product_v2 c  inner join tbl_sku_fixed_prices s on c.sku = s.sku set c.fixed_price = 1;

Select 'Start: catalog_product_v2 - categorias', now();
ALTER TABLE `development_co`.`tbl_catalog_product_v2` ADD COLUMN `categorias` VARCHAR(2048) NULL  AFTER `price_comparison` ;

update tbl_catalog_product_v2 inner join (
select catalog_config.name product_name, catalog_config.sku sku_config, catalog_simple.sku, group_concat(catalog_category.name) categorias from
               bob_live_co.catalog_config_has_catalog_category 
inner join bob_live_co.catalog_category on id_catalog_category = fk_catalog_category
inner join bob_live_co.catalog_config on id_catalog_config = catalog_config_has_catalog_category.fk_catalog_config
inner join bob_live_co.catalog_simple on id_catalog_config = catalog_simple.fk_catalog_config
            where
                (`catalog_config_has_catalog_category`.`fk_catalog_category` > 1)
                    and (`catalog_config_has_catalog_category`.`fk_catalog_category` <> 1762)
                    and (`catalog_config_has_catalog_category`.`fk_catalog_category` <> 1779)
                    and (`catalog_config_has_catalog_category`.`fk_catalog_category` <> 1797)
                    and (`catalog_config_has_catalog_category`.`fk_catalog_category` <> 1799)
group by catalog_config.name, catalog_config.sku, catalog_simple.sku) cat
on cat.sku = tbl_catalog_product_v2.sku
set tbl_catalog_product_v2.categorias = cat.categorias; 

ALTER TABLE `development_co`.`tbl_catalog_product_v2` ADD COLUMN `fk_categorias` VARCHAR(2048) NULL  AFTER `categorias` ;

update tbl_catalog_product_v2 inner join (
select catalog_config.name product_name, catalog_config.sku sku_config, catalog_simple.sku, group_concat(catalog_category.name) categorias, 
group_concat(fk_catalog_category) fk_categorias
 from
               bob_live_co.catalog_config_has_catalog_category 
inner join bob_live_co.catalog_category on id_catalog_category = fk_catalog_category
inner join bob_live_co.catalog_config on id_catalog_config = catalog_config_has_catalog_category.fk_catalog_config
inner join bob_live_co.catalog_simple on id_catalog_config = catalog_simple.fk_catalog_config
            where
                (`catalog_config_has_catalog_category`.`fk_catalog_category` > 1)
                    and (`catalog_config_has_catalog_category`.`fk_catalog_category` <> 1762)
                    and (`catalog_config_has_catalog_category`.`fk_catalog_category` <> 1779)
                    and (`catalog_config_has_catalog_category`.`fk_catalog_category` <> 1797)
                    and (`catalog_config_has_catalog_category`.`fk_catalog_category` <> 1799)
group by catalog_config.name, catalog_config.sku, catalog_simple.sku) cat
on cat.sku = tbl_catalog_product_v2.sku
set tbl_catalog_product_v2.fk_categorias = cat.fk_categorias; 


ALTER TABLE `development_co`.`tbl_catalog_product_v2` 
ADD COLUMN `category_bp` VARCHAR(45) NULL AFTER `product_measures`;

UPDATE development_co.tbl_catalog_product_v2 a
INNER JOIN development_mx.A_E_M1_New_CategoryBP b ON a.new_cat2 = b.cat2
SET a.category_bp = b.CatBP;


UPDATE development_co.tbl_catalog_product_v2 a
INNER JOIN development_mx.A_E_M1_New_CategoryBP b ON a.new_cat1 = b.cat1
SET a.category_bp = b.CatBP
WHERE a.category_bp IS NULL;
/*
update tbl_bi_ops_stock 
set buyer = trim(both '	' from replace(replace(replace(buyer, '\t', ''), '\n',''),'\r',''));*/

Select 'End: catalog_product_v2', now();
commit;
SELECT  'Daily routine: Tbl_catalog_product_v2 OK',now();