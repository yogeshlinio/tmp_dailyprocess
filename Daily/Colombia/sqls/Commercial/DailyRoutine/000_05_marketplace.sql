#Marketplace

update tbl_order_detail t inner join production_co.tbl_marketplace mp on t.proveedor = mp.supplier
	set delivery_cost_supplier = 0, wh = 0, shipping_cost = 0 ,
	costo_after_vat = if(comision = 0, costo_after_vat,(unit_price_after_vat + shipping_fee_after_vat)*(1-comision));
