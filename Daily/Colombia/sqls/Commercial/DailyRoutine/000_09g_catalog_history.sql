
SET @c_date=DATE(NOW() - INTERVAL 1 DAY);
SET @v_maxDate='0000-00-00';

SELECT MAX(date) INTO @v_maxDate
FROM development_co.catalog_history;

IF (@v_maxDate=@c_date) THEN
   SELECT 'Borrando ultimo dia de catalog_history', now();
   DELETE FROM development_co.catalog_history WHERE date=@v_maxDate;
END IF;

#Catalog Visible
Select 'Truncando tabla catalog_visible', now();
TRUNCATE TABLE development_co.catalog_visible;

Select 'Insertando en catalog_visible', now();
INSERT INTO development_co.catalog_visible ( sku_config
                                          , sku_simple
                                          , pet_status
                                          , pet_approved
                                          , status_config
                                          , status_simple
                                          , name
                                          , display_if_out_of_stock
                                          , quantity
                                          , updated_at
                                          , activated_at
                                          , price) 
SELECT catalog_config.sku as sku_config
     , catalog_simple.sku as sku_simple
     , catalog_config.pet_status
     , catalog_config.pet_approved
     , catalog_config.status
     , catalog_simple.status
     , catalog_config.name
     , catalog_config.display_if_out_of_stock
     , catalog_stock.quantity
     , catalog_config.updated_at
     , catalog_config.activated_at
     , catalog_simple.price
FROM (bob_live_co.catalog_config 
      INNER JOIN bob_live_co.catalog_simple 
      ON catalog_config.id_catalog_config = catalog_simple.fk_catalog_config) 
INNER JOIN bob_live_co.catalog_stock 
ON catalog_simple.id_catalog_simple = catalog_stock.fk_catalog_simple
WHERE (
	   (
        (catalog_config.pet_status)="creation,edited,images"
       ) 
       AND 
       (
        (catalog_config.pet_approved)=1
       ) 
       AND 
       (
        (catalog_config.status)="active"
       ) 
       AND 
       (
        (catalog_simple.status)="active"
       ) 
       AND 
       (
        (catalog_config.display_if_out_of_stock)=0
       ) 
       AND 
       (
        (catalog_stock.quantity)>0
       ) 
       AND 
       (
        (catalog_simple.price)>0
       )
      )
      OR 
      (
       (
        (catalog_config.pet_status)="creation,edited,images"
       ) 
       AND 
	   (
        (catalog_config.pet_approved)=1
       ) 
       AND 
       (
        (catalog_config.status)="active"
       ) 
       AND 
	   (
        (catalog_simple.status)="active"
       ) 
       AND 
       (
        (catalog_config.display_if_out_of_stock)=1
	   ) 
       AND 
       (
        (catalog_simple.price)>0
       )
);

Select 'Insertando ultimo dia en catalog_history', now();
#Catalog History
INSERT INTO development_co.catalog_history ( date
                                          , sku_config
                                          , sku_simple
                                          , product_name
                                          , status_config
                                          , status_simple
                                          , quantity, price) 
SELECT curdate() - INTERVAL 1 DAY
     , catalog_config.sku as sku_config
     , catalog_simple.sku as sku_simple
     , catalog_config.name
     , catalog_config.status
     , catalog_simple.status
     , catalog_stock.quantity
     , catalog_simple.price
FROM (bob_live_co.catalog_config 
INNER JOIN bob_live_co.catalog_simple 
ON catalog_config.id_catalog_config = catalog_simple.fk_catalog_config) 
LEFT JOIN bob_live_co.catalog_stock 
ON catalog_simple.id_catalog_simple = catalog_stock.fk_catalog_simple;

UPDATE development_co.catalog_history 
SET quantity = 0  
WHERE quantity is null 
AND date = curdate() - INTERVAL 1 DAY;

UPDATE development_co.catalog_history c 
INNER JOIN development_co.catalog_visible v 
ON c.sku_simple=v.sku_simple 
SET visible = 1 
WHERE date = curdate() - INTERVAL 1 DAY;

UPDATE development_co.catalog_history SET visible = 0 
WHERE visible IS NULL 
AND date = curdate() - INTERVAL 1 DAY;

Select 'End Rutina Catalog History - Recuerdos Lilibeth', now();
