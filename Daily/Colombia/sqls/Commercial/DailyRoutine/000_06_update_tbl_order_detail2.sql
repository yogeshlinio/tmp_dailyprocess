
update development_co.tbl_order_detail t inner join development_co.tbl_shipment_zone t2 on t.ciudad = t2.ciudad
set t.fk_shipment_zone = t2.fk_zone where fk_shipment_zone is null;


update development_co.tbl_order_detail inner join (
select date, orderID, item, n1, product_name,t.payment_method, peso, t.shipping_cost viejo ,
 s.shipping_cost nuevo, s.shipping_cost - t.shipping_cost dif
from development_co.tbl_order_detail  t inner join development_co.tbl_ciudad_zone_type c on t.ciudad = c.ciudad
inner join development_co.tbl_payment_method_groups p on t.payment_method = p.payment_method 
inner join development_co.tbl_shipping_costs_zone_types s on s.fk_zone_type = c.fk_zone_type and s.payment_method_group = p.payment_method_group
where t.peso >= peso_min and t.peso <= peso_max
and oac = 1 and yrmonth >= 201401) shipping_costs on tbl_order_detail.item = shipping_costs.item
set tbl_order_detail.shipping_cost = shipping_costs.nuevo where tbl_order_detail.date >=  '2014-01-01';

#Shipping costs de las órdenes pendientes
update development_co.tbl_order_detail inner join (
select date, orderID, item, n1, product_name,t.payment_method, peso, t.shipping_cost viejo ,
 s.shipping_cost nuevo, s.shipping_cost - t.shipping_cost dif
from development_co.tbl_order_detail  t inner join development_co.tbl_ciudad_zone_type c on t.ciudad = c.ciudad
inner join development_co.tbl_payment_method_groups p on t.payment_method = p.payment_method 
inner join development_co.tbl_shipping_costs_zone_types s on s.fk_zone_type = c.fk_zone_type and s.payment_method_group = p.payment_method_group
where t.peso >= peso_min and t.peso <= peso_max
and obc = 1 and pending = 1 and yrmonth >= 201401) shipping_costs on tbl_order_detail.item = shipping_costs.item
set tbl_order_detail.shipping_cost = shipping_costs.nuevo where tbl_order_detail.date >=  '2014-01-01';

update development_co.tbl_order_detail
SET shipping_cost=4000
WHERE sku_config IN ('CI878OT80HFBLACOL','CI878OT79HFCLACOL')
AND yrmonth>=201309
AND oac=1 AND returned=0 AND rejected=0
AND payment_method='CashOnDelivery_Payment';
/*
update development_co.tbl_order_detail t inner join tbl_bi_ops_delivery b 
on item = item_id
set t.date_delivered = b.date_delivered, t.yrmonth_delivered = date_format(b.date_delivered, '%Y%m')
where t.date_delivered is null;*/

update development_co.tbl_order_detail inner join bob_live_co.sales_rule on code = coupon_code
inner join bob_live_co.sales_rule_set on id_sales_rule_set = fk_sales_rule_set
set coupon_code_description = description ,coupon_code_prefix = code_prefix
where coupon_code is not null;
