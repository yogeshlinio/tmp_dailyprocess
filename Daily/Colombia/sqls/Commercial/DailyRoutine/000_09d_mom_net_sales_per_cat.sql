
select @start_current_month:=cast(date_add(now(), interval - day(now())+1 day) as date);


select @end_current_month:=cast(date_add(now(), interval - 1 day) as date);


select @start_previous_month:=date_add(cast(date_add(now(), interval - day(now())+1 day) as date), interval - 1 month);


select @end_previous_month:=date_add(cast(date_add(now(), interval - 1 day) as date), interval - 1 month);

select @last_date:=max(date) from tbl_order_detail;

delete from development_co.tbl_mom_net_sales_per_cat where date = @end_current_month;
insert into development_co.tbl_mom_net_sales_per_cat(date, n1, netSalesPreviousMonth, netSalesCurrentMonth,month_over_month, cogsCurrentMonth, pc15costsCurrentMonth,
pc2CostsCurrentMonth, cogsPreviousMonth, pc15costsPreviousMonth,
pc2CostsPreviousMonth)
select @end_current_month, currentMonth.new_cat1, sum(NetSalesPreviousMonth) NetSalesPreviousMonth, sum(NetSalesCurrentMonth) NetSalesCurrentMonth, 
if((sum(NetSalesCurrentMonth)/sum(NetSalesPreviousMonth))-1  is null, 0,1-(sum(NetSalesPreviousMonth)/sum(NetSalesCurrentMonth))),
currentMonth.cogs, currentMonth.pc15_costs, currentMonth.pc2_costs,
previousMonth.cogs, previousMonth.pc15_costs, previousMonth.pc2_costs
from
(select new_cat1, sum(paid_price_after_vat) +sum(if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat)) as NetSalesCurrentMonth,
(sum(costo_after_vat) + sum(ifnull(delivery_cost_supplier,0))) cogs, (sum(shipping_cost) + sum(payment_cost)) pc15_costs,
(sum(shipping_cost) + sum(payment_cost) + sum(cs) + sum(wh)) pc2_costs
from tbl_order_detail
where oac = 1 and returned = 0 and rejected=0 and date >= @start_current_month and date <= @end_current_month
group by new_cat1) currentMonth left join
(select new_cat1, sum(paid_price_after_vat) +sum(if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat)) as NetSalesPreviousMonth,
sum(costo_after_vat) + sum(ifnull(delivery_cost_supplier,0)) as cogs, sum(shipping_cost) + sum(payment_cost) pc15_costs,
sum(shipping_cost) + sum(payment_cost) + sum(cs) + sum(wh) pc2_costs
from tbl_order_detail
where oac = 1 and returned = 0 and rejected=0 and date >= @start_previous_month and date <= @end_previous_month
group by new_cat1) previousMonth on
currentMonth.new_cat1 = previousMonth.new_cat1
group by month(@start_current_month), currentMonth.new_cat1;

insert into development_co.tbl_mom_net_sales_per_cat(date, n1, netSalesPreviousMonth, netSalesCurrentMonth,month_over_month, cogsCurrentMonth, pc15costsCurrentMonth,
pc2CostsCurrentMonth, cogsPreviousMonth, pc15costsPreviousMonth,
pc2CostsPreviousMonth)
select date, 'MTD',sum(NetSalesPreviousMonth), sum(NetSalesCurrentMonth), (sum(NetSalesCurrentMonth)/sum(NetSalesPreviousMonth))-1,
sum(cogsCM), sum(pc15_costsCM), sum(pc2_costsCM),
sum(cogsPM), sum(pc15_costsPM), sum(pc2_costsPM)
from (
select @end_current_month date, sum(NetSalesPreviousMonth) NetSalesPreviousMonth, sum(NetSalesCurrentMonth) NetSalesCurrentMonth, 
1-(sum(NetSalesPreviousMonth)/sum(NetSalesCurrentMonth)),
sum(cogsCM) cogsCM, sum(currentMonth.pc15_costs) pc15_costsCM, sum(currentMonth.pc2_costs) pc2_costsCM,
sum(cogsPM) cogsPM, sum(previousMonth.pc15_costs) pc15_costsPM, sum(previousMonth.pc2_costs) pc2_costsPM
from
(select new_cat1, sum(paid_price_after_vat) +sum(if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat)) as NetSalesCurrentMonth,
(sum(costo_after_vat) + sum(ifnull(delivery_cost_supplier,0))) cogsCM, (sum(shipping_cost) + sum(payment_cost)) pc15_costs,
(sum(shipping_cost) + sum(payment_cost) + sum(cs) + sum(wh)) pc2_costs
from tbl_order_detail
where oac = 1 and returned = 0 and rejected=0 and date >= @start_current_month and date <= @end_current_month
group by new_cat1) currentMonth left join
(select new_cat1, sum(paid_price_after_vat) +sum(if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat)) as NetSalesPreviousMonth,
(sum(costo_after_vat) + sum(ifnull(delivery_cost_supplier,0))) as cogsPM, (sum(shipping_cost) + sum(payment_cost) ) pc15_costs,
(sum(shipping_cost) + sum(payment_cost) + sum(cs) + sum(wh)) pc2_costs
from tbl_order_detail
where oac = 1 and returned = 0 and rejected=0 and date >= @start_previous_month and date <= @end_previous_month
group by new_cat1) previousMonth on
currentMonth.new_cat1 = previousMonth.new_cat1
group by month(@start_current_month), currentMonth.new_cat1) p group by date;

update development_co.tbl_mom_net_sales_per_cat
set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));
