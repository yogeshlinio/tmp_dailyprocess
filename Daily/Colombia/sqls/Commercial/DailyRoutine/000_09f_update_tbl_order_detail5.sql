
update development_co.tbl_order_detail join development_co.tbl_catalog_product_v2 
on tbl_order_detail.sku=tbl_catalog_product_v2.sku 
SET
tbl_order_detail.new_cat1 = tbl_catalog_product_v2.new_cat1,
tbl_order_detail.new_cat2 = tbl_catalog_product_v2.new_cat2,
tbl_order_detail.new_cat3 = tbl_catalog_product_v2.new_cat3;

SELECT @last_date:=max(date) from development_co.tbl_order_detail;

SELECT  'Update catalog_config',now();
UPDATE ((development_co.tbl_order_detail INNER JOIN bob_live_co.catalog_simple ON tbl_order_detail.sku = catalog_simple.sku) 
INNER JOIN bob_live_co.catalog_config ON catalog_simple.fk_catalog_config = catalog_config.id_catalog_config)
SET 
tbl_order_detail.package_width = greatest(cast(replace(IF(isnull(catalog_config.package_width),1,catalog_config.package_width),',','.') as DECIMAL(6,2)),1.0), 
tbl_order_detail.package_length = greatest(cast(replace(IF(isnull(catalog_config.package_length),1,catalog_config.package_length),',','.') as DECIMAL(6,2)),1.0), 
tbl_order_detail.package_height = greatest(cast(replace(IF(isnull(catalog_config.package_height),1,catalog_config.package_height),',','.') as DECIMAL(6,2)),1.0);

SELECT  'Calculo Size',now();
UPDATE development_co.tbl_order_detail 
SET Vol_Weight=(tbl_order_detail.package_width*tbl_order_detail.package_length*tbl_order_detail.package_height)/5000,
Size=if(Vol_Weight<2.277,"Small",(if(Vol_Weight>45.486,"Large","Medium")));

UPDATE development_co.tbl_order_detail set WH=0 where channel_group='Corporate Sales';
#UPDATE development_co.tbl_order_detail set CS=0 where channel_group='Corporate Sales';

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'development_co.tbl_order_detail parte 1',
  'finish',
  NOW(),
  MAX(date),
  count(*),
  count(*)
FROM
  development_co.tbl_order_detail
;

SELECT  'daily_routine: OK',now();