
select 'Start: Voucher costs ', now();
update development_co.tbl_voucher_costs v inner join development_co.tbl_order_detail t on voucher = coupon_code
set v.channel_group = t.channel_group, v.channel = t.channel;

update development_co.tbl_voucher_costs v set dailySpend = cost/datediff(toDate, fromDate);

update development_co.tbl_order_detail t inner join 
(select coupon_code, count(distinct orderID) ordenes, count(item) items, dailySpend,
 dailySpend/count(distinct orderID) cpo, 
if(curdate()  > toDate, datediff(toDate , fromDate),datediff(curdate() , fromDate))*dailySpend/count(item) spend, 
if(curdate() > toDate, datediff(toDate , fromDate),datediff(curdate()  , fromDate))
from development_co.tbl_order_detail t inner join tbl_voucher_costs v on t.coupon_code = v.voucher
where oac = 1 and returned = 0 and rejected = 0
group by coupon_code) t2 on t.coupon_code = t2.coupon_code
set t.marketingCost = t2.spend where t.oac = 1 and t.returned = 0 and t.rejected = 0;

#Para que el costo de julio no quede acumulado
update development_co.tbl_order_detail t inner join 
(select fromDate, coupon_code, count(distinct orderID) ordenes, count(item) items, dailySpend,
 dailySpend/count(distinct orderID) cpo, 
if(curdate()  > toDate, datediff(toDate , fromDate),datediff(curdate() , fromDate))*dailySpend/count(item), 
if(curdate() > toDate, datediff(toDate , fromDate),datediff(curdate()  , fromDate)), datediff( curdate(),
 if(fromDate <'2013-07-01','2013-07-01', fromDate))*dailySpend/count(item) spend
from tbl_order_detail t inner join tbl_voucher_costs v on t.coupon_code = v.voucher
where oac = 1 and returned = 0 and rejected = 0 and toDate >= '2013-07-01' and yrmonth = 201307
group by coupon_code) t2 on t.coupon_code = t2.coupon_code
set t.marketingCost = t2.spend where t.oac = 1 and t.returned = 0 and t.rejected = 0;

#Para que el costo de agosto no quede acumulado
update development_co.tbl_order_detail t inner join 
(select fromDate, coupon_code, count(distinct orderID) ordenes, count(item) items, dailySpend,
 dailySpend/count(distinct orderID) cpo, 
if(curdate()  > toDate, datediff(toDate , fromDate),datediff(curdate() , fromDate))*dailySpend/count(item), 
if(curdate() > toDate, datediff(toDate , fromDate),datediff(curdate()  , fromDate)), datediff( curdate(), 
if(fromDate <'2013-08-01','2013-08-01', fromDate))*dailySpend/count(item) spend
from development_co.tbl_order_detail t inner join development_co.tbl_voucher_costs v on t.coupon_code = v.voucher
where oac = 1 and returned = 0 and rejected = 0 and toDate >= '2013-08-01' and yrmonth = 201308
group by coupon_code) t2 on t.coupon_code = t2.coupon_code
set t.marketingCost = t2.spend where t.oac = 1 and t.returned = 0 and t.rejected = 0;

#Para que el costo de septiembre no quede acumulado
update development_co.tbl_order_detail t inner join 
(select fromDate, coupon_code, count(distinct orderID) ordenes, count(item) items, dailySpend,
 dailySpend/count(distinct orderID) cpo, 
if(curdate()  > toDate, datediff(toDate , fromDate),datediff(curdate() , fromDate))*dailySpend/count(item), 
if(curdate() > toDate, datediff(toDate , fromDate),datediff(curdate()  , fromDate)), datediff( curdate(), 
if(fromDate <'2013-09-01','2013-09-01', fromDate))*dailySpend/count(item) spend
from development_co.tbl_order_detail t inner join development_co.tbl_voucher_costs v on t.coupon_code = v.voucher
where oac = 1 and returned = 0 and rejected = 0 and toDate >= '2013-09-01' and yrmonth = 201309
group by coupon_code) t2 on t.coupon_code = t2.coupon_code
set t.marketingCost = t2.spend where t.oac = 1 and t.returned = 0 and t.rejected =0;


#Para que el costo de septiembre no quede acumulado
update development_co.tbl_order_detail t inner join 
(select fromDate, coupon_code, count(distinct orderID) ordenes, count(item) items, dailySpend,
 dailySpend/count(distinct orderID) cpo, 
if(curdate()  > toDate, datediff(toDate , fromDate),datediff(curdate() , fromDate))*dailySpend/count(item), 
if(curdate() > toDate, datediff(toDate , fromDate),datediff(curdate()  , fromDate)), datediff( curdate(), 
if(fromDate <'2013-10-01','2013-10-31', fromDate))*dailySpend/count(item) spend
from development_co.tbl_order_detail t inner join development_co.tbl_voucher_costs v on t.coupon_code = v.voucher
where oac = 1 and returned = 0 and rejected = 0 and toDate >= '2013-10-01' and yrmonth = 201310
group by coupon_code) t2 on t.coupon_code = t2.coupon_code
set t.marketingCost = t2.spend where t.oac = 1 and t.returned = 0 and t.rejected =0;

#Para que el costo de septiembre no quede acumulado
update development_co.tbl_order_detail t inner join 
(select fromDate, coupon_code, count(distinct orderID) ordenes, count(item) items, dailySpend,
 dailySpend/count(distinct orderID) cpo, 
if(curdate()  > toDate, datediff(toDate , fromDate),datediff(curdate() , fromDate))*dailySpend/count(item), 
if(curdate() > toDate, datediff(toDate , fromDate),datediff(curdate()  , fromDate)), datediff( curdate(), 
if(fromDate <'2013-11-01','2013-11-30', fromDate))*dailySpend/count(item) spend
from development_co.tbl_order_detail t inner join development_co.tbl_voucher_costs v on t.coupon_code = v.voucher
where oac = 1 and returned = 0 and rejected = 0 and toDate >= '2013-11-30' and yrmonth = 201311
group by coupon_code) t2 on t.coupon_code = t2.coupon_code
set t.marketingCost = t2.spend where t.oac = 1 and t.returned = 0 and t.rejected =0;
select 'End: Voucher costs ', now();
