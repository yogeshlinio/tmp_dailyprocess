
update development_co.tbl_order_detail set costo_after_vat=135000
WHERE sku='TI235EL39YEELACOL-83806' and date>='2013-05-08' and date<='2013-05-31';

update development_co.tbl_order_detail set gross_grand_total_after_vat=shipping_fee_after_vat+paid_price_after_vat WHERE OBC = 1;
update development_co.tbl_order_detail set net_grand_total_after_vat=shipping_fee_after_vat+paid_price_after_vat 
WHERE (OAC = 1 and RETURNED = 0 and rejected  = 0);

update development_co.tbl_order_detail t inner join development_co.tbl_ventas_extraordinarias v 
on t.coupon_code = v.coupon_code
set t.paid_price = (v.paid_price/31),
t.unit_price = (v.unit_price/31),
t.shipping_fee = (v.shipping_fee/31)
where  v.coupon_code not like 'VC%' and yrmonth = 201308;

update development_co.tbl_order_detail t 
set paid_price_after_vat = paid_price/(1 + tax_percent/100), shipping_fee_after_vat = shipping_fee/(1 + tax_percent/100), 
unit_price_after_vat = paid_price/(1 + tax_percent/100)
where (coupon_code like '0000%') 
and oac = 1 and returned = 0 and rejected = 0;

#Actualizacion para la contribucion de marketing de XBOX, se asigna como comprador Maritza
update development_co.tbl_order_detail
SET buyer='Maritza'
WHERE custID=0
AND orderID=6;

#call pricing_report();

#Recalculo del paid_price de las ordenes con coupon_code LIKE 'ER%'
/*update development_co.tbl_order_detail
SET paid_price=coupon_money_value + paid_price,
    coupon_money_value=unit_price-paid_price
WHERE coupon_code_prefix IN (SELECT Prefijo FROM tbl_prefijos_vouchers_sac WHERE Prefijo LIKE'ER%')
AND oac=1 AND rejected=0 AND returned=0
AND unit_price<=(coupon_money_value + paid_price);*/

update development_co.tbl_order_detail
SET paid_price=coupon_money_value + paid_price,
    coupon_money_value=unit_price-paid_price,
    paid_price_after_vat=paid_price/(1 + tax_percent/100),
    coupon_money_after_vat=coupon_money_value/(1 + tax_percent/100)
WHERE coupon_code_prefix IN (SELECT Prefijo FROM tbl_prefijos_vouchers_sac WHERE Prefijo LIKE'ER%')
AND oac=1 AND rejected=0 AND returned=0
AND unit_price<=(coupon_money_value + paid_price);

update tbl_order_detail
set paid_price = coupon_money_value, coupon_money_value = 0
where paid_price = 0 and unit_price = coupon_money_value
and obc = 1 and (coupon_code like 'REO%' or coupon_code like 'REP%' or coupon_code like 'INV%' or coupon_code like 'GAR%' or coupon_code like 'CAN%' or coupon_code like 'DEV%')
order by date desc;

update tbl_order_detail
set paid_price_after_vat = paid_price/(1+tax_percent/100), coupon_money_after_vat = coupon_money_value/(1+tax_percent/100)
where (coupon_code like 'REO%' or coupon_code like 'REP%' or coupon_code like 'INV%' or coupon_code like 'GAR%');

update development_co.tbl_order_detail t inner join bob_live_co.sales_order_item  b on t.item = b.id_sales_order_item 
set t.date_ordered = b.created_at, 
yrmonth_ordered = date_format(b.created_at, '%Y%m') where t.date_ordered is null;

insert into production.table_monitoring_log
(country, table_name, updated_at, key_date, total_rows, total_rows_check)
values ('Colombia', 'tbl_order_detail - date_ordered',now(),now(), row_count(), row_count());

update development_co.tbl_order_detail t set t.is_visible_now = 0;
update development_co.tbl_order_detail t inner join tbl_catalog_product_v2 c on t.sku = c.sku
set t.is_visible_now = if(c.visible = 'SI', 1, 0);

update development_co.tbl_order_detail t inner join bob_live_co.catalog_config c on t.sku_config = c.sku
inner join bob_live_co.supplier s on s.id_supplier = c.fk_supplier
set t.fk_supplier = s.id_supplier;

insert into production.table_monitoring_log
(country, table_name, updated_at, key_date, total_rows, total_rows_check)
values ('Colombia', 'tbl_order_detail - supplier',now(),now(), row_count(), row_count());

update development_co.tbl_order_detail t inner join development_co.tbl_catalog_product_v2 c
on t.sku = c.sku
set t.category_bp = c.category_bp;

insert into production.table_monitoring_log
(country, table_name, updated_at, key_date, total_rows, total_rows_check)
values ('Colombia', 'tbl_order_detail - category_bp',now(),now(), row_count(), row_count());


SELECT  'update tbl_order_detail END',now();

commit;
SELECT  'Daily routine: tbl_order_detail END',now();