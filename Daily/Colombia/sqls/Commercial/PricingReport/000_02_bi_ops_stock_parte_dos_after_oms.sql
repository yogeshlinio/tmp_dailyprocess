
select 'Start: bi_ops_stock_parte_dos_after_oms', now();
/*
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'production_co.tbl_bi_ops_stock parte 2',
  'start',
  NOW(),
  MAX(date_entrance),
  count(*),
  count(*)
FROM
  production_co.tbl_bi_ops_stock
;

update tbl_bi_ops_stock a inner join tbl_bi_ops_procurement b on a.op=b.OP and a.sku_simple=b.sku_simple 
set
a.cost_oms_wo_vat=b.price_before_tax,
a.type_oms=b.type,
a.supplier_negotiation=b.negotiation_type
where b.is_cancelled=0 and b.is_deleted=0;


SELECT  'Actualizar costos reales',now();
update 
tbl_bi_ops_stock
set cost_stock_wms=if(cost_oms_wo_vat is null, cost_wo_vat,cost_oms_wo_vat);

update 
tbl_bi_ops_stock
set cost_stock_wms= cost_wo_vat
where cost_stock_wms is null;


SELECT  'Sold last 30 segun la orden',now();
update tbl_bi_ops_stock 
set tbl_bi_ops_stock.sold_last_30_order_cost_w_o_vat = tbl_bi_ops_stock. cost_stock_wms, 
tbl_bi_ops_stock.sold_last_30_order_price = tbl_bi_ops_stock.product_price
where tbl_bi_ops_stock.sold_last_30_order=1;	

truncate table tbl_bi_ops_stock_sold_last_30_count;

SELECT  'Insertar datos tabla sold_last_30_count',now();
insert into tbl_bi_ops_stock_sold_last_30_count ( sold_last_30, sku_simple, count_of_item_counter )
select tbl_bi_ops_stock.sold_last_30_order, tbl_bi_ops_stock.sku_simple, count(tbl_bi_ops_stock.item_counter) as count_of_item_counter
from tbl_bi_ops_stock group by tbl_bi_ops_stock.sold_last_30_order, tbl_bi_ops_stock.sku_simple having tbl_bi_ops_stock.sold_last_30_order=1;

update tbl_bi_ops_stock inner join tbl_bi_ops_stock_sold_last_30_count on tbl_bi_ops_stock.sku_simple = tbl_bi_ops_stock_sold_last_30_count.sku_simple 
set tbl_bi_ops_stock.sold_last_30_order_counter = 1/ tbl_bi_ops_stock_sold_last_30_count.count_of_item_counter;



#Se actualizan costos de oms de acuerdo a las notas a credito
UPDATE production_co.tbl_bi_ops_stock od
JOIN tbl_cambios_costos cc
ON od.sku_simple=cc.sku
SET od.cost_oms_wo_vat=cc.cost
WHERE od.order_date BETWEEN cc.from_date AND cc.to_date;


update 
tbl_bi_ops_stock a inner join tbl_purchase_order_stock b on a.op=b.oc and a.sku_simple=b.sku
set
a.cost_oms_wo_vat=b.costo,
a.type_oms=tipo_orden,
a.supplier_negotiation=b.tipo_pago;

update 
tbl_bi_ops_stock a inner join tbl_purchase_order b on a.op=b.oc and a.sku_simple=b.sku
set
a.cost_oms_wo_vat=b.costo,
a.type_oms='Crossdock',
a.supplier_negotiation=b.tipo_pago;

update 
tbl_bi_ops_stock
set
type_oms='Stock'
where type_oms = 'Inventario';


update 
production_co.tbl_bi_ops_stock
set
type_oms = if(wh_location like '%CONSI%', 'Consignment', 'Ingreso')
where type_oms IS NULL;

#Actualiza el sell list por 60 días de ventas, 30 y 7

update production_co.tbl_bi_ops_stock 
set 
average_remaining_days = 9999
where average_remaining_days is null;

update 
tbl_bi_ops_stock
set
sell_list_7=if(average_remaining_days_sim_7 >= 90,1,0),
sell_list_30=if(average_remaining_days_30 >= 90,1,0),
maxdaysstock=if(max_days_in_stock>30,1,0);

update 
tbl_bi_ops_stock
set
sell_list = 1
where category_com_main = 'Home and Living' and
average_remaining_days >= 150;

update 
tbl_bi_ops_stock
set
sell_list = 1
where category_com_main = 'Fashion' and
average_remaining_days >= 120;

update 
tbl_bi_ops_stock
set
sell_list = 1
where category_com_main = 'Electrónicos' and
average_remaining_days >= 60;

update 
tbl_bi_ops_stock
set
sell_list = 1
where category_com_main not in ('Electrónicos', 'Home and Living', 'Fashion') and
average_remaining_days >= 90;


update 
tbl_bi_ops_stock
set
sell_list=if(maxdaysstock=1,1,0)
where sell_list=1;

update 
tbl_bi_ops_stock
set
sell_list_7=if(maxdaysstock=1,1,0)
where sell_list_7=1;


update 
tbl_bi_ops_stock
set
sell_list_30=if(maxdaysstock=1,1,0)
where sell_list_30=1;



update tbl_bi_ops_stock 
set in_stock_not_reserved=1
where stock_wms>0;

UPDATE tbl_bi_ops_stock
set buyer='B5 - Katherine'
where buyer ='B11 - Katherine';

update tbl_bi_ops_stock a inner join tbl_bi_ops_categories b
on a.buyer=b.buyer and a.category_1=b.cat_1 and a.category_2=b.cat_2
set 
a.buyer_cat=b.buyer_cat,
a.planner=b.planner;

update tbl_bi_ops_stock a inner join production_co.tbl_catalog_product_v2 b on a.sku_simple=b.sku
set a.attribute=b.attribute;

update tbl_bi_ops_stock set stock_wms =0
where stock_wms is null;

update production_co.tbl_bi_ops_stock a inner join tbl_bi_ops_new_categories b 
on a.buyer=b.Buyer and a.category_1=b.New_cat1 and a.category_2=b.New_cat2
set
a.buyer_cat=b.CatBP
where b.New_cat2<>"";

update production_co.tbl_bi_ops_stock a inner join tbl_bi_ops_new_categories b 
on a.buyer=b.Buyer and a.category_1=b.New_cat1 
set
a.buyer_cat=b.CatBP
where b.New_cat2="";
*/

#call days_inventory_per_day();


SELECT  'Catalog_product stock edad promedio del inventario',now();
update production_co.tbl_catalog_product_stock a inner join vw_inv_avg_ageing b
on a.sku=b.sku_simple
set
a.inv_age=b.avg_age;

SELECT  'Extra query fechas de cuarentena',now();
update tbl_bi_ops_stock t
inner join (select estoque_id,para_endereco,min(data_movimentacao) data_movimentacao from
(select estoque_id,para_endereco,data_criacao as data_movimentacao 
from wmsprod_co.movimentacoes 
where para_endereco liKe '%DEF%' or para_endereco like '%REP%' OR 
para_endereco like '%GRT%' OR para_endereco like '%AVE%' 
or para_endereco like '%cuerentena%') as a
group by estoque_id)
as t2
on t.stock_item_id=t2.estoque_id
set t.quarantine_date=t2.data_movimentacao;

update tbl_bi_ops_stock
set quarantine_date=date_entrance
where quarantine=1 
and quarantine_date is null;



#Saber categorías en liquidación
truncate table tbl_skus_liquidation;
insert into tbl_skus_liquidation
select sku,name from (SELECT cc.*,lft, rgt, rgt - lft,c2.sku,c.name, c.status  FROM bob_live_co.catalog_config_has_catalog_category cc 
inner join bob_live_co.catalog_category c on id_catalog_category = fk_catalog_category
inner join bob_live_co.catalog_config c2 on cc.fk_catalog_config = c2.id_catalog_config
where id_catalog_category > 1) as a 
where a.name like '%liquida%';

update production_co.tbl_bi_ops_stock a
inner join tbl_skus_liquidation b
ON a.sku_config = b.sku
set liquidation=1
where a.in_stock=1 and reserved=0;

#Actualizar datos para visible or no visible
SELECT  'Actualizar qc, status tbl_catalog_product_v2',now();
UPDATE production_co.tbl_bi_ops_stock
INNER JOIN production_co.tbl_catalog_product_v2 
ON tbl_bi_ops_stock.sku_simple = tbl_catalog_product_v2.sku
SET tbl_bi_ops_stock.qc = tbl_catalog_product_v2.QC,
tbl_bi_ops_stock.status_config = tbl_catalog_product_v2.status_config,
tbl_bi_ops_stock.status_s = tbl_catalog_product_v2.status_simple,
tbl_bi_ops_stock.pet_status = tbl_catalog_product_v2.pet_status;

#Actualizar datos para pet status como DV wants
UPDATE production_co.tbl_bi_ops_stock
INNER JOIN production_co.tbl_catalog_product_v2 
ON tbl_bi_ops_stock.sku_simple = tbl_catalog_product_v2.sku
SET tbl_bi_ops_stock.qc = tbl_catalog_product_v2.QC,
tbl_bi_ops_stock.status_config = tbl_catalog_product_v2.status_config,
tbl_bi_ops_stock.status_s = tbl_catalog_product_v2.status_simple,
tbl_bi_ops_stock.pet_status = if(tbl_catalog_product_v2.pet_status IS NULL, 'FALTA TODO', 
if(tbl_catalog_product_v2.pet_status = "creation,edited", "FALTA IMAGES", 
if(tbl_catalog_product_v2.pet_status = "creation,images", "FALTA EDITED", 
if(tbl_catalog_product_v2.pet_status = "creation", "FALTA EYI", 
if(tbl_catalog_product_v2.pet_status = "creation,edited,images", "OK", "ERROR")))));

insert into production.table_monitoring_log
(country, table_name, updated_at, key_date, total_rows, total_rows_check)
values ('Colombia', 'tbl_bi_ops_stock - qc, status sku',now(),now(), row_count(), row_count());

#Actualizar el sku status
UPDATE production_co.tbl_bi_ops_stock
SET tbl_bi_ops_stock.sku_status = if(tbl_bi_ops_stock.status_config="deleted" or tbl_bi_ops_stock.status_s="deleted", "deleted", 
if(tbl_bi_ops_stock.status_config="inactive" or tbl_bi_ops_stock.status_s="inactive", "inactive", 
if(tbl_bi_ops_stock.status_config="active" and tbl_bi_ops_stock.status_s="active", "active", NULL)));

#Actualizar la razón de no visibilidad
UPDATE production_co.tbl_bi_ops_stock
SET tbl_bi_ops_stock.reason_not_visible = 
if(tbl_bi_ops_stock.pet_status<>"OK", tbl_bi_ops_stock.pet_status, 
if(tbl_bi_ops_stock.sku_status<>"active", tbl_bi_ops_stock.sku_status, 
if(tbl_bi_ops_stock.qc=0, "Quality Check Failed", "STOCK")))
WHERE visible_in = 0;

/*
#Inserta PC 1.5 en la table pc después de haber corrido la rutina que creó Lore
TRUNCATE TABLE production_co.tbl_bi_ops_pc;

INSERT INTO tbl_bi_ops_pc(
select c.sku, ((sh.precio_actual + if(c.eligible_free_shipping = 1, 0, if(sf.shipping_fee IS NULL, 0, sf.shipping_fee)))/(1+(c.tax_percent/100)) - if(sh.costo_actual IS NULL, 0, sh.costo_actual) -
if(c.delivery_cost_supplier IS NULL, 0, c.delivery_cost_supplier) - if(shc.shipping_cost IS NULL, 0, shc.shipping_cost) - if(sh.costo_actual IS NULL, 0, 0.025*sh.costo_actual)) as pc,
(((sh.precio_actual + if(c.eligible_free_shipping = 1, 0, if(sf.shipping_fee IS NULL, 0, sf.shipping_fee)))/(1+(c.tax_percent/100)) - if(sh.costo_actual IS NULL, 0, sh.costo_actual) -
if(c.delivery_cost_supplier IS NULL, 0, c.delivery_cost_supplier) - if(shc.shipping_cost IS NULL, 0, shc.shipping_cost) - if(sh.costo_actual IS NULL, 0, 0.025*sh.costo_actual))/sh.precio_actual) as pc_percentage
from tbl_catalog_product_v2 c
inner join (select sku_simple, sum(ifnull(in_stock,0)) stock_wms
 from tbl_bi_ops_stock 
 where reserved = 0 and in_stock_real = 1 group by sku_simple) i 
on i.sku_simple = c.sku
inner join tbl_sku_precios_costos sh on sh.sku = c.sku
inner join tbl_sku_shipping_cost shc on shc.sku = c.sku
inner join tbl_sku_shipping_fees sf on sf.sku = c.sku);

#Después de tener los datos en la tabla de pc se hace el cruce con stock
UPDATE production_co.tbl_bi_ops_stock INNER JOIN production_co.tbl_bi_ops_pc 
ON tbl_bi_ops_stock.sku_simple = tbl_bi_ops_pc.sku
SET production_co.tbl_bi_ops_stock.pc_15 = production_co.tbl_bi_ops_pc.pc_15,
production_co.tbl_bi_ops_stock.pc_15_percentage = production_co.tbl_bi_ops_pc.pc_15_percentage;

*/

#Actualizar reason_stats_bob
UPDATE tbl_bi_ops_stock a 
inner join (
            select sku, s.name as stat, is_reserved 
            from bob_live_co.sales_order_item i
            inner join bob_live_co.sales_order_item_status s
            on s.id_sales_order_item_status = i.fk_sales_order_item_status
            where s.id_sales_order_item_status IN (9,10,103)
            and i.is_reserved = 1 
group by sku) b 
ON a.sku_simple = b.sku
set a.reason_status_bob = b.stat
where a.reason_not_visible = 'STOCK';

#Actualizar skus sin ventas
update production_co.tbl_bi_ops_stock 
set sku_without_sales = 1,
average_remaining_days = 9999
where average_remaining_days IS NULL AND sell_list = 1;

update production_co.tbl_bi_ops_stock
set 
tbl_bi_ops_stock.interval_age=if(max_days_in_stock<7,"< 8",if(max_days_in_stock<15,"8-15", if(max_days_in_stock<30,"15-29", 
if(max_days_in_stock<45,"30-44",if(max_days_in_stock<60,"45-59",
if(max_days_in_stock<90,"60-89",if(max_days_in_stock<120,"90-119",
if(max_days_in_stock<150,"120-149",if(max_days_in_stock<180,"150-179",
if(max_days_in_stock<210,"180-209",if(max_days_in_stock<240,"210-239",
if(max_days_in_stock<270,"240-269",if(max_days_in_stock<300,"270-299",
if(max_days_in_stock<330,"230-259",if(max_days_in_stock<=360,"330-360","> 360")))))))))))))));

UPDATE production_co.tbl_bi_ops_stock
SET 
  tbl_bi_ops_stock.week_exit = production_co.week_iso (date_exit)
		where date_exit is not null; 
UPDATE production_co.tbl_bi_ops_stock
SET 
  tbl_bi_ops_stock.week_entrance = production_co.week_iso (date_entrance) 
	where date_entrance is not null; 


update tbl_bi_ops_stock s inner join tbl_catalog_product_v2 c on s.sku_simple = c.sku
set c.interval_age = s.interval_age;

update production_co.tbl_bi_ops_stock
set planner = if(tbl_bi_ops_stock.buyer IN ("B0 - Jessica", "B3 - Maria Fernanda", "B4 - Freddy",
"B5 - Katherine", "B6 - Ximena", "B8 - Guillermo", "B8 - Cristina"), "HOGAR", 
if(tbl_bi_ops_stock.buyer IN ("B2 - Alexander", "B1 - Maritza"), "ELECTRONICA","FASHION"));

SELECT  'Actualizar is marketplace',now();
update tbl_bi_ops_stock s inner join tbl_catalog_product_v2 c
on s.sku_simple = c.sku
set s.is_marketplace = c.is_marketplace;


#Actualizar sell list tbl_catalog_product_v2
SELECT  'Actualizar sell list tbl_catalog_product_v2',now();
update tbl_catalog_product_v2 c inner join
(select sku_simple, sum(sell_list) s, count(sell_list) c from tbl_bi_ops_stock group by sku_simple) t
on c.sku = t.sku_simple
set c.sell_list = floor(t.s/t.c);
