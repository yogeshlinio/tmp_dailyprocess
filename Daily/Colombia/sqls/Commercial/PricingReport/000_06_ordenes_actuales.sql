
#Rutina Creada y Actualizada por: 	Natali Serrano Guerrero
#Fecha de última Actualización: 	04 de Diciembre 2013
#Funcion: 							Genera el listado de ordenes para utilizar en la macro de Ordenes de Compra
#									de Dropshipping y Marketplace.
SELECT  'Truncate',now();
TRUNCATE TABLE production_co.tbl_bi_ops_ordenes_duplicadas;

SELECT  'Insert',now();
INSERT production_co.tbl_bi_ops_ordenes_duplicadas(item_id)
SELECT production_co.tbl_purchase_order.itemid
FROM production_co.tbl_purchase_order 
GROUP BY production_co.tbl_purchase_order.orderid, production_co.tbl_purchase_order.itemid, production_co.tbl_purchase_order.sku 
HAVING 
Count(production_co.tbl_purchase_order.itemid) > 1;

SELECT  'a',now();
UPDATE production_co.tbl_bi_ops_ordenes_duplicadas
SET max_bi_id = oc_mas_nueva(item_id);

SELECT  'b',now();
UPDATE production_co.tbl_bi_ops_ordenes_duplicadas
INNER JOIN production_co.tbl_purchase_order
ON production_co.tbl_bi_ops_ordenes_duplicadas.max_bi_id = tbl_purchase_order.id
SET production_co.tbl_bi_ops_ordenes_duplicadas.fecha_creacion = production_co.tbl_purchase_order.fecha_creacion;

SELECT  'c',now();
TRUNCATE TABLE production_co.tbl_bi_ops_ordenes_unicas;

SELECT  'd',now();
INSERT production_co.tbl_bi_ops_ordenes_unicas(item_id, bi_id, fecha_creacion)
SELECT production_co.tbl_purchase_order.itemid, production_co.tbl_purchase_order.id, production_co.tbl_purchase_order.fecha_creacion
FROM production_co.tbl_purchase_order 
GROUP BY production_co.tbl_purchase_order.itemid
HAVING 
Count(production_co.tbl_purchase_order.itemid) = 1;

SELECT  'e',now();
TRUNCATE TABLE production_co.tbl_bi_ops_ordenes_totales;

SELECT  'f',now();
INSERT production_co.tbl_bi_ops_ordenes_totales(item_id, bi_id, fecha_creacion)
SELECT * FROM production_co.tbl_bi_ops_ordenes_unicas;

INSERT production_co.tbl_bi_ops_ordenes_totales(item_id, bi_id, fecha_creacion)
SELECT * FROM production_co.tbl_bi_ops_ordenes_duplicadas;

SELECT  'g',now();
UPDATE production_co.tbl_bi_ops_ordenes_totales
SET recibido = actualizar_recibido(item_id, fecha_creacion);

SELECT  'h',now();
UPDATE production_co.tbl_bi_ops_ordenes_totales
INNER JOIN wmsprod_co.itens_venda
ON production_co.tbl_bi_ops_ordenes_totales.item_id = itens_venda.item_id
SET production_co.tbl_bi_ops_ordenes_totales.status_wms = itens_venda.status;

SELECT  'i',now();
UPDATE production_co.tbl_bi_ops_ordenes_totales
SET recibido = IF(status_wms='Expedido',1,0);

SELECT  'j',now();
TRUNCATE TABLE production_co.tbl_bi_ops_items_wms;
INSERT production_co.tbl_bi_ops_items_wms(item_id)
SELECT DISTINCT(item_id)
FROM wmsprod_co.itens_venda
WHERE status='Analisando quebra' or status='Aguardando estoque' or status='Waiting dropshipping';

SELECT  'k',now();
TRUNCATE TABLE production_co.tbl_bi_ops_to_purchase;

SELECT  'l',now();
INSERT production_co.tbl_bi_ops_to_purchase(item_id)
SELECT item_id
FROM production_co.tbl_bi_ops_items_wms
WHERE production_co.tbl_bi_ops_items_wms.item_id NOT IN(select item_id from production_co.tbl_bi_ops_ordenes_totales);

SELECT  'm',now();
INSERT production_co.tbl_bi_ops_to_purchase(item_id)
SELECT item_id
FROM production_co.tbl_bi_ops_items_wms
WHERE production_co.tbl_bi_ops_items_wms.item_id  IN(select item_id from production_co.tbl_bi_ops_ordenes_totales where recibido=1);

SELECT  'n',now();
UPDATE production_co.tbl_bi_ops_to_purchase
INNER JOIN wmsprod_co.itens_venda ON production_co.tbl_bi_ops_to_purchase.item_id = wmsprod_co.itens_venda.item_id 
INNER JOIN wmsprod_co.pedidos_venda ON wmsprod_co.itens_venda.order_id = wmsprod_co.pedidos_venda.order_id
SET
production_co.tbl_bi_ops_to_purchase.order_id=itens_venda.order_id,
production_co.tbl_bi_ops_to_purchase.order_number=itens_venda.numero_order,
production_co.tbl_bi_ops_to_purchase.payment_method=pedidos_venda.metodo_de_pagamento,
production_co.tbl_bi_ops_to_purchase.unit_price=itens_venda.preco_unitario,
production_co.tbl_bi_ops_to_purchase.order_date=itens_venda.data_pedido,
production_co.tbl_bi_ops_to_purchase.sku=itens_venda.sku,
production_co.tbl_bi_ops_to_purchase.sku_supplier=itens_venda.supplier_sku,
production_co.tbl_bi_ops_to_purchase.item_id=itens_venda.item_id,
production_co.tbl_bi_ops_to_purchase.item_name=itens_venda.nome_produto,
production_co.tbl_bi_ops_to_purchase.brand=itens_venda.item_brand,
production_co.tbl_bi_ops_to_purchase.billing_first_name= pedidos_venda.nome_cliente,
production_co.tbl_bi_ops_to_purchase.billing_address=pedidos_venda.rua_cliente,
production_co.tbl_bi_ops_to_purchase.shipping_first_name=pedidos_venda.nome_cliente,
production_co.tbl_bi_ops_to_purchase.shipping_address=pedidos_venda.rua_cliente,
production_co.tbl_bi_ops_to_purchase.shipping_city=pedidos_venda.cidade_cliente,
production_co.tbl_bi_ops_to_purchase.shipping_region=pedidos_venda.estado_cliente,
production_co.tbl_bi_ops_to_purchase.shipping_phone=pedidos_venda.cob_telefone_cliente,
production_co.tbl_bi_ops_to_purchase.billing_phone=pedidos_venda.cob_mobile_client,
production_co.tbl_bi_ops_to_purchase.supplier_id=itens_venda.supplier_id,
production_co.tbl_bi_ops_to_purchase.supplier_name=itens_venda.supplier_name,
production_co.tbl_bi_ops_to_purchase.nit=pedidos_venda.cpf_cliente,
production_co.tbl_bi_ops_to_purchase.status_date=itens_venda.data_ultima_alteracao,
production_co.tbl_bi_ops_to_purchase.status_wms=itens_venda.status;

SELECT  'o',now();
UPDATE production_co.tbl_bi_ops_to_purchase
INNER JOIN bob_live_co.catalog_simple
ON production_co.tbl_bi_ops_to_purchase.sku = catalog_simple.sku 
SET
production_co.tbl_bi_ops_to_purchase.model=catalog_simple.oc_model;

SELECT  'p',now();
UPDATE production_co.tbl_purchase_order
INNER JOIN production_co.tbl_bi_ops_tracking_suppliers
ON production_co.tbl_bi_ops_tracking_suppliers.supplier_name = production_co.tbl_purchase_order.nombre_proveedor
INNER JOIN production_co.tbl_tracker
ON production_co.tbl_bi_ops_tracking_suppliers.tracker_name = production_co.tbl_tracker.nombre_tracker
SET production_co.tbl_purchase_order.tracker =  production_co.tbl_tracker.id_tracker;

SELECT  'q',now();
UPDATE production_co.tbl_purchase_order
INNER JOIN wmsprod_co.itens_venda
ON production_co.tbl_purchase_order.itemid = wmsprod_co.itens_venda.item_id
SET production_co.tbl_purchase_order.status_wms = itens_venda.status;

SELECT  'actualizar price',now();
UPDATE production_co.tbl_bi_ops_to_purchase
INNER JOIN development_co.tbl_order_detail ON production_co.tbl_bi_ops_to_purchase.item_id = development_co.tbl_order_detail.Item
SET production_co.tbl_bi_ops_to_purchase.unite_price_after_vat = tbl_order_detail.unit_price_after_vat;

SELECT  'actualizar shipping fee',now();
UPDATE production_co.tbl_bi_ops_to_purchase
INNER JOIN development_co.tbl_order_detail ON production_co.tbl_bi_ops_to_purchase.item_id = development_co.tbl_order_detail.Item
SET production_co.tbl_bi_ops_to_purchase.shipping_fee_after_vat = development_co.tbl_order_detail.shipping_fee_after_vat;

#actualiza iva desde tbl_order_detail
#SELECT  'actualizar tax_amounth',now();
#UPDATE production_co.tbl_bi_ops_to_purchase
#INNER JOIN tbl_order_detail ON production_co.tbl_bi_ops_to_purchase.item_id = tbl_order_detail.Item
#SET production_co.tbl_bi_ops_to_purchase.tax_amounth = tbl_order_detail.tax_percent;

#actualiza iva desde catalog_producto_v2
SELECT  'actualizar tax_amounth',now();
UPDATE production_co.tbl_bi_ops_to_purchase AS a
INNER JOIN production_co.tbl_catalog_product_v2 AS b
ON a.sku = b.sku
SET a.tax_amounth = b.tax_percent;

SELECT  'actualizar shippin fee (costo de envio)',now();
UPDATE production_co.tbl_bi_ops_to_purchase
INNER JOIN production_co.tbl_catalog_product_v2 ON production_co.tbl_bi_ops_to_purchase.sku = production_co.tbl_catalog_product_v2.sku
SET production_co.tbl_bi_ops_to_purchase.shipping_fee = IF(eligible_free_shipping=1,0,tbl_catalog_product_v2.delivery_cost_supplier);

SELECT  'actualizar recibido',now();
UPDATE production_co.tbl_purchase_order
INNER JOIN production_co.tbl_bi_ops_ordenes_totales
ON production_co.tbl_purchase_order.id = production_co.tbl_bi_ops_ordenes_totales.bi_id
SET production_co.tbl_purchase_order.recibido = production_co.tbl_bi_ops_ordenes_totales.recibido;

UPDATE production_co.tbl_bi_ops_to_purchase
INNER JOIN production_co.tbl_bi_ops_delivery
ON production_co.tbl_bi_ops_to_purchase.item_id = production_co.tbl_bi_ops_delivery.item_id
SET production_co.tbl_bi_ops_to_purchase.coupon_code = production_co.tbl_bi_ops_delivery.supplier_procurement_time;

SELECT  'Rutina Finalizada',now();
