
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'production_co.tbl_days_of_inventory_per_day',
  'finish',
  NOW(),
  MAX(date),
  count(*),
  count(*)
FROM
  production_co.tbl_days_of_inventory_per_day
;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'production_co.tbl_bi_ops_stock parte 2',
  'finish',
  NOW(),
  MAX(date_entrance),
  count(*),
  count(*)
FROM
  production_co.tbl_bi_ops_stock
;


INSERT INTO `production`.`table_monitoring_report` VALUES (NULL, 'Daily Stock - CO', NOW());

SELECT  'End: bi_ops_stock_parte_dos_after_oms',now();
