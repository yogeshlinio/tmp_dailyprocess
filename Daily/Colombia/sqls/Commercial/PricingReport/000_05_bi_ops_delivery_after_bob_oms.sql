
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'production_co.tbl_bi_ops_delvery parte 2',
  'start',
  NOW(),
  MAX(date_exportable),
  count(*),
  count(*)
FROM
  production_co.tbl_bi_ops_delivery
;

SELECT 'Update presale TABLA tbl_order_detail',now();
UPDATE production_co.tbl_bi_ops_delivery 
INNER JOIN development_co.tbl_order_detail
ON production_co.tbl_bi_ops_delivery.item_id = tbl_order_detail.item
SET production_co.tbl_bi_ops_delivery.presale = 1
WHERE (((tbl_order_detail.product_name) like '%preventa%'));

SELECT  'Update OOS recovered tbl_order_detail',now();
UPDATE production_co.tbl_bi_ops_delivery
INNER JOIN development_co.tbl_order_detail
ON tbl_order_detail.order_nr=tbl_bi_ops_delivery.nueva_orden_cc
SET
tbl_bi_ops_delivery.OOS_recovered = 1
WHERE nueva_orden_cc<>''
AND status_wms IN ('Quebrado','quebra tratada')
AND tbl_order_detail.oac=1
AND tbl_order_detail.returned=0;

SELECT  'Update OOS recovered from google docs CC TABLA tbl_order_detail',now();
UPDATE production_co.tbl_bi_ops_delivery
INNER JOIN development_co.tbl_order_detail
ON tbl_order_detail.Item=tbl_bi_ops_delivery.item_id_reorder_replace
SET
production_co.tbl_bi_ops_delivery.OOS_recovered = 1
WHERE production_co.tbl_bi_ops_delivery.status_wms in ('Quebrado','quebra tratada')
AND development_co.tbl_order_detail.oac=1
AND development_co.tbl_order_detail.returned=0;

SELECT  'Update Ventas corporativas TABLA tbl_order_detail',now();
UPDATE production_co.tbl_bi_ops_delivery
INNER JOIN development_co.tbl_order_detail
ON production_co.tbl_bi_ops_delivery.item_id=tbl_order_detail.Item
SET
production_co.tbl_bi_ops_delivery.corporate_sales = 1
WHERE coupon_code like 'VC%';

###############################


Select 'Start: bi_ops_delivery_after_bob_oms', now();
SELECT  'Update OOS recovered',now();
UPDATE production_co.tbl_bi_ops_delivery
INNER JOIN development_co.tbl_order_detail
ON tbl_order_detail.order_nr=tbl_bi_ops_delivery.nueva_orden_cc
SET
tbl_bi_ops_delivery.OOS_recovered = 1
WHERE nueva_orden_cc<>''
AND status_wms IN ('Quebrado','quebra tratada')
AND tbl_order_detail.oac=1
AND tbl_order_detail.returned=0;

UPDATE production_co.tbl_bi_ops_delivery
INNER JOIN development_co.tbl_order_detail
ON tbl_order_detail.Item=tbl_bi_ops_delivery.item_id_reorder_replace
SET
tbl_bi_ops_delivery.OOS_recovered = 1
WHERE tbl_bi_ops_delivery.status_wms in ('Quebrado','quebra tratada')
AND tbl_order_detail.oac=1
AND tbl_order_detail.returned=0;

SELECT  'Update Ventas corporativas',now();
UPDATE production_co.tbl_bi_ops_delivery
INNER JOIN development_co.tbl_order_detail
ON tbl_bi_ops_delivery.item_id=tbl_order_detail.Item
SET
tbl_bi_ops_delivery.corporate_sales = 1
WHERE coupon_code like 'VC%';

######################### PAYMENT TERMS #########################
#################################################################
SELECT  'Payment terms 1',now();
UPDATE tbl_bi_ops_delivery AS a
INNER JOIN
	(SELECT OP,sku_simple,negotiation_type
	FROM tbl_bi_ops_procurement
	WHERE  tbl_bi_ops_procurement.is_cancelled=0
	AND tbl_bi_ops_procurement.is_deleted=0
	GROUP BY OP,sku_simple) AS b
ON (a.op=b.OP AND a.sku_simple=b.sku_simple)
SET a.payment_terms = b.negotiation_type;

SELECT  'Payment terms2',now();
UPDATE production_co.tbl_bi_ops_delivery AS a
INNER JOIN procurement_live_co.catalog_supplier_attributes AS b
ON a.supplier_id=b.fk_catalog_supplier
SET
a.payment_terms=negotiation_type
WHERE a.payment_terms IS NULL;

SELECT  'Payment terms',now();
UPDATE tbl_bi_ops_delivery as a
INNER JOIN tbl_bi_ops_procurement as b
ON a.sku_simple=b.sku_simple
SET
a.payment_terms=b.negotiation_type
WHERE a.payment_terms IS NULL;

SELECT  'Payment terms numero',now();
UPDATE production_co.tbl_bi_ops_delivery
SET
payment_term=udf_cleanCreditNum(payment_terms);

##### Trae Tracker historico
SELECT  'Traer Tracker historico',now();
UPDATE production_co.tbl_bi_ops_delivery as a
INNER JOIN production_co.tbl_bi_ops_trackers_history as b
ON a.item_id=b.item_id
SET a.tracker = b.tracker;

SELECT  'Traer Tracker de OMS',now();
UPDATE tbl_bi_ops_delivery AS a
INNER JOIN
	(SELECT OP,sku_simple,tracker
	FROM tbl_bi_ops_procurement
	WHERE  tbl_bi_ops_procurement.is_cancelled=0
	AND tbl_bi_ops_procurement.is_deleted=0
	AND tracker not in ('','INACTIVO','LINIO','0')
	GROUP BY OP,sku_simple) AS b
ON (a.op=b.OP AND a.sku_simple=b.sku_simple)
SET a.tracker = b.tracker
WHERE b.tracker IS NOT NULL
AND a.tracker IS NULL
AND b.tracker not in ('','INACTIVO','LINIO','0');

SELECT  'Tracker de oms',now();
UPDATE tbl_bi_ops_delivery AS a
INNER JOIN procurement_co.catalog_supplier_attributes AS b
ON a.supplier_id=b.fk_catalog_supplier
SET a.tracker=b.tracker
WHERE a.tracker IS NULL
AND b.tracker not in ('','INACTIVO','LINIO','0');

#SELECT  'Tracker de oms nombre',now();
#UPDATE production_co.tbl_bi_ops_delivery AS a
#INNER JOIN procurement_live_co.catalog_supplier_attributes AS b
#ON a.supplier_name=b.company_name
#SET a.tracker=b.tracker
#WHERE a.tracker IS NULL;

#### Actualización de los otros trackers Tabla trackers and supplier
SELECT  'Traer tracker tabla de trackers and supplier',now();
UPDATE production_co.tbl_bi_ops_delivery as a
INNER JOIN production_co.tbl_bi_ops_tracking_suppliers as b
ON b.supplier_name = a.supplier_name
SET a.tracker = b.tracker_name
WHERE a.tracker IS NULL;

#### Guarda tracker
SELECT  'Guarda Tracker historico',now();
INSERT production_co.tbl_bi_ops_trackers_history (item_id,tracker,date_update)
SELECT item_id,tracker,curdate()
FROM production_co.tbl_bi_ops_delivery
WHERE production_co.tbl_bi_ops_delivery.tracker IS NOT NULL;


INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'production_co.tbl_bi_ops_delivery parte 2',
  'finish',
  NOW(),
  MAX(date_exportable),
  count(*),
  count(*)
FROM
  production_co.tbl_bi_ops_delivery
;

SELECT  'CALL bi_ops_tracking_code',now();