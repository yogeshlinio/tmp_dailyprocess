
#Llamar la rutina para poner pc 1.5 en tbl_bi_ops_stock después de haber corrido pricing_report

SELECT  'Pc 1.5 stock routine	',now();

#Inserta PC 1.5 en la table pc después de haber corrido la rutina que creó Lore
TRUNCATE TABLE production_co.tbl_bi_ops_pc;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'production_co.tbl_bi_ops_pc',
  'start',
  NOW(),
  NOW(),
  count(*),
  count(*)
FROM
  production_co.tbl_bi_ops_pc
;

INSERT INTO production_co.tbl_bi_ops_pc(
select c.sku, ((sh.precio_actual + if(c.eligible_free_shipping = 1, 0, if(sf.shipping_fee IS NULL, 0, sf.shipping_fee)))/(1+(c.tax_percent/100)) - if(sh.costo_actual IS NULL, 0, sh.costo_actual) -
if(c.delivery_cost_supplier IS NULL, 0, c.delivery_cost_supplier) - if(shc.shipping_cost IS NULL, 0, shc.shipping_cost) - if(sh.costo_actual IS NULL, 0, 0.025*sh.costo_actual)) as pc,
(((sh.precio_actual + if(c.eligible_free_shipping = 1, 0, if(sf.shipping_fee IS NULL, 0, sf.shipping_fee)))/(1+(c.tax_percent/100)) - if(sh.costo_actual IS NULL, 0, sh.costo_actual) -
if(c.delivery_cost_supplier IS NULL, 0, c.delivery_cost_supplier) - if(shc.shipping_cost IS NULL, 0, shc.shipping_cost) - if(sh.costo_actual IS NULL, 0, 0.025*sh.costo_actual))/sh.precio_actual) as pc_percentage
from tbl_catalog_product_v2 c
left join (select sku_simple, sum(ifnull(in_stock,0)) stock_wms
 from tbl_bi_ops_stock 
 where reserved = 0 and in_stock_real = 1 group by sku_simple) i 
on i.sku_simple = c.sku
left join tbl_sku_precios_costos sh on sh.sku = c.sku
left join tbl_sku_shipping_cost shc on shc.sku = c.sku
left join tbl_sku_shipping_fees sf on sf.sku = c.sku);

#Después de tener los datos en la tabla de pc se hace el cruce con stock
UPDATE production_co.tbl_bi_ops_stock INNER JOIN production_co.tbl_bi_ops_pc 
ON tbl_bi_ops_stock.sku_simple = tbl_bi_ops_pc.sku
SET production_co.tbl_bi_ops_stock.pc_15 = production_co.tbl_bi_ops_pc.pc_15,
production_co.tbl_bi_ops_stock.pc_15_percentage = production_co.tbl_bi_ops_pc.pc_15_percentage;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'production_co.tbl_bi_ops_pc',
  'finish',
  NOW(),
  NOW(),
  count(*),
  count(*)
FROM
  production_co.tbl_bi_ops_pc
;

SELECT  'Pc 1.5 stock routine FIN',now();
