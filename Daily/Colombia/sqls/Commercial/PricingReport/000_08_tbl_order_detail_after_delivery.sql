
Select 'Start: tbl_order_detail_after_delivery', now();

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'development_co.tbl_order_detail parte 2',
  'start',
  NOW(),
  MAX(date),
  count(*),
  count(*)
FROM
  development_co.tbl_order_detail
;

#Stock outs
update tbl_order_detail set stockout = 0;
update tbl_order_detail inner join tbl_bi_ops_delivery on item = item_id
set tbl_order_detail.stockout = 1 
where tbl_bi_ops_delivery.stockout_real = 1 and oac = 0 and cancel = 1;

insert into production.table_monitoring_log
(country, table_name, updated_at, key_date, total_rows, total_rows_check)
values ('Colombia', 'tbl_order_detail - stockout',now(),now(), row_count(), row_count());

#Rejections
update tbl_order_detail t inner join (
select itens_venda.item_id
from wmsprod_co.status_itens_venda 
inner join wmsprod_co.itens_venda on status_itens_venda.itens_venda_id = itens_venda.itens_venda_id
inner join tbl_bi_ops_delivery on tbl_bi_ops_delivery.item_id  = itens_venda.item_id
inner join tbl_order_detail on itens_venda.item_id = item
 where 
 status_itens_venda.status in ('Expedido') and status_wms = 'Cancelado' 
and 
tbl_order_detail.payment_method = 'CashOnDelivery_Payment' and returned = 0 and cancel = 1) wms on item = item_id
set rejected = 1 , cancel = 0 , oac = 1;

insert into production.table_monitoring_log
(country, table_name, updated_at, key_date, total_rows, total_rows_check)
values ('Colombia', 'tbl_order_detail - rejections',now(),now(), row_count(), row_count());

update tbl_order_detail inner join (select yrmonth, orderID, count(item) items
from tbl_bi_ops_delivery inner join tbl_order_detail on item = item_id
where date_shipped is not null
group by yrmonth, orderID) t on tbl_order_detail.orderID = t.orderID
 set shipped_items = t.items;

insert into production.table_monitoring_log
(country, table_name, updated_at, key_date, total_rows, total_rows_check)
values ('Colombia', 'tbl_order_detail - shipped_items',now(),now(), row_count(), row_count());

update development_co.tbl_order_detail t 
inner join tbl_bi_ops_delivery t2 
on t.item = t2.item_id 
set t.courier = t2.shipping_courier
where yrmonth>=201305;

insert into production.table_monitoring_log
(country, table_name, updated_at, key_date, total_rows, total_rows_check)
values ('Colombia', 'tbl_order_detail - courier',now(),now(), row_count(), row_count());

#wh
#No aplica para las órdenes de dropshipping
update development_co.tbl_order_detail inner join 
(select date,count(distinct orderID) net_orders 
from development_co.tbl_order_detail  inner join tbl_bi_ops_delivery b
on item =item_id
where oac = 1 and returned = 0 and yrmonth = 201308 and custID > 0
and (channel_group not like '%Corporate%Sales%'  or channel_group is null)
and  not(fulfilment_real = 'Dropshipping'  
or (fulfilment_real is null and fulfilment_bob = 'Dropshipping')) group by date) orders
 on tbl_order_detail.date = orders.date
set WH=(56596/31/orders.net_orders/nr_items)*2350
where tbl_order_detail.yrmonth = 201308 and (OAC = 1 and RETURNED = 0 and rejected  = 0 and CustID > 0)
 and (channel_group not like '%Corporate%Sales%'  or channel_group is null);

#wh
#No aplica para las órdenes de dropshipping
UPDATE tbl_order_detail 
INNER JOIN
   (SELECT date,count(distinct orderID) net_orders 
    FROM tbl_order_detail 
    INNER JOIN tbl_bi_ops_delivery b
    on item =item_id
    INNER JOIN tbl_catalog_product_v2 p
    ON tbl_order_detail.sku=p.sku
    WHERE oac = 1 and returned = 0 and yrmonth = 201311 and custID > 0
    and (IFNULL(tbl_order_detail.is_marketplace,0) = 0 OR (IFNULL(tbl_order_detail.is_marketplace,0)=1 AND fulfilment_real='Crossdocking'))
    AND (IFNULL(p.config_type,1)<>2  OR p.sku_config NOT IN ('CI878OT80HFBLACOL','CI878OT79HFCLACOL'))
    and (channel_group not like '%Corporate%Sales%'  or channel_group is null)
    and  not(fulfilment_wms = 'Dropshipping'  
    or (fulfilment_wms is null and fulfilment_bob = 'Dropshipping')) group by date
   ) orders
ON tbl_order_detail.date = orders.date
INNER JOIN tbl_bi_ops_delivery b
ON tbl_order_detail.item = b.item_id
INNER JOIN tbl_catalog_product_v2 p
ON tbl_order_detail.sku=p.sku
SET tbl_order_detail.WH=(77181.47273/30/orders.net_orders/nr_items)*2450
WHERE tbl_order_detail.yrmonth = 201311 
 and (tbl_order_detail.OAC = 1 and tbl_order_detail.RETURNED = 0 and tbl_order_detail.rejected  = 0 and tbl_order_detail.CustID > 0 
 AND (IFNULL(tbl_order_detail.is_marketplace,0) = 0) OR (IFNULL(tbl_order_detail.is_marketplace,0)=1 AND b.fulfilment_real='Crossdocking'))
 AND (IFNULL(p.config_type,1)<>2  OR p.sku_config NOT IN ('CI878OT80HFBLACOL','CI878OT79HFCLACOL')) 
 AND (channel_group not like '%Corporate%Sales%'  or channel_group is null);


#wh
#No aplica para las órdenes de dropshipping
update development_co.tbl_order_detail 
INNER JOIN
   (SELECT date,count(distinct orderID) net_orders 
    from development_co.tbl_order_detail  
    INNER JOIN production_co.tbl_bi_ops_delivery b
    on item =item_id
    INNER JOIN production_co.tbl_catalog_product_v2 p
    ON tbl_order_detail.sku=p.sku
    WHERE oac = 1 and returned = 0 and yrmonth = 201312 and custID > 0
    and (IFNULL(tbl_order_detail.is_marketplace,0) = 0 OR (IFNULL(tbl_order_detail.is_marketplace,0)=1 AND fulfilment_real='Crossdocking'))
    AND (IFNULL(p.config_type,1)<>2  OR p.sku_config NOT IN ('CI878OT80HFBLACOL','CI878OT79HFCLACOL'))
    and (channel_group not like '%Corporate%Sales%'  or channel_group is null)
    and  not(fulfilment_wms = 'Dropshipping'  
    or (fulfilment_wms is null and fulfilment_bob = 'Dropshipping')) group by date
   ) orders
ON tbl_order_detail.date = orders.date
INNER JOIN production_co.tbl_bi_ops_delivery b
ON tbl_order_detail.item = b.item_id
INNER JOIN production_co.tbl_catalog_product_v2 p
ON tbl_order_detail.sku=p.sku
SET tbl_order_detail.WH=(245310327/31/orders.net_orders/nr_items)
WHERE tbl_order_detail.yrmonth = 201312
 and (tbl_order_detail.OAC = 1 and tbl_order_detail.RETURNED = 0 and tbl_order_detail.rejected  = 0 and tbl_order_detail.CustID > 0 
 AND (IFNULL(tbl_order_detail.is_marketplace,0) = 0) OR (IFNULL(tbl_order_detail.is_marketplace,0)=1 AND b.fulfilment_real='Crossdocking'))
 AND (IFNULL(p.config_type,1)<>2  OR p.sku_config NOT IN ('CI878OT80HFBLACOL','CI878OT79HFCLACOL')) 
 AND (channel_group not like '%Corporate%Sales%'  or channel_group is null);

#wh Enero 2014
#No aplica para las órdenes de dropshipping
update development_co.tbl_order_detail 
INNER JOIN
   (SELECT date,count(distinct orderID) net_orders 
    from development_co.tbl_order_detail  
    INNER JOIN production_co.tbl_bi_ops_delivery b
    on item =item_id
    INNER JOIN production_co.tbl_catalog_product_v2 p
    ON tbl_order_detail.sku=p.sku
    WHERE oac = 1 and returned = 0 and yrmonth = 201401 and custID > 0
    and (IFNULL(tbl_order_detail.is_marketplace,0) = 0 OR (IFNULL(tbl_order_detail.is_marketplace,0)=1 AND fulfilment_real='Crossdocking'))
    AND (IFNULL(p.config_type,1)<>2  OR p.sku_config NOT IN ('CI878OT80HFBLACOL','CI878OT79HFCLACOL'))
    and (channel_group not like '%Corporate%Sales%'  or channel_group is null)
    and  not(fulfilment_wms = 'Dropshipping'  
    or (fulfilment_wms is null and fulfilment_bob = 'Dropshipping')) group by date
   ) orders
ON tbl_order_detail.date = orders.date
INNER JOIN production_co.tbl_bi_ops_delivery b
ON tbl_order_detail.item = b.item_id
INNER JOIN production_co.tbl_catalog_product_v2 p
ON tbl_order_detail.sku=p.sku
SET tbl_order_detail.WH=(77181.47273/31/orders.net_orders/nr_items)*2450
WHERE tbl_order_detail.yrmonth = 201401
 and (tbl_order_detail.OAC = 1 and tbl_order_detail.RETURNED = 0 and tbl_order_detail.rejected  = 0 and tbl_order_detail.CustID > 0 
 AND (IFNULL(tbl_order_detail.is_marketplace,0) = 0) OR (IFNULL(tbl_order_detail.is_marketplace,0)=1 AND b.fulfilment_real='Crossdocking'))
 AND (IFNULL(p.config_type,1)<>2  OR p.sku_config NOT IN ('CI878OT80HFBLACOL','CI878OT79HFCLACOL')) 
 AND (channel_group not like '%Corporate%Sales%'  or channel_group is null);

insert into production.table_monitoring_log
(country, table_name, updated_at, key_date, total_rows, total_rows_check)
values ('Colombia', 'tbl_order_detail - wh dec',now(),now(), row_count(), row_count());


update tbl_order_detail t
inner join tbl_bi_ops_delivery b
on item = item_id
set t.wh = 0,
t.delivery_cost_supplier = 0,
t.shipping_cost = 0
where fulfilment_wms = 'Dropshipping'  
or (fulfilment_wms is null and fulfilment_bob = 'Dropshipping');


insert into production.table_monitoring_log
(country, table_name, updated_at, key_date, total_rows, total_rows_check)
values ('Colombia', 'tbl_order_detail - wh dropshipping',now(),now(), row_count(), row_count());

update tbl_order_detail t inner join tbl_bi_ops_delivery b 
on item = item_id
set t.date_delivered = b.date_delivered, t.yrmonth_delivered = date_format(b.date_delivered, '%Y%m')
where t.date_delivered is null;


insert into production.table_monitoring_log
(country, table_name, updated_at, key_date, total_rows, total_rows_check)
values ('Colombia', 'tbl_order_detail - date_delivered',now(),now(), row_count(), row_count());
-- Sanity CHECK



INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'development_co.tbl_order_detail parte 2',
  'finish',
  NOW(),
  MAX(date),
  count(*),
  count(*)
FROM
  development_co.tbl_order_detail
;

Select 'End: tbl_order_detail_after_delivery', now();
