
DECLARE v_costOMS INT DEFAULT 0;
DECLARE v_time INT DEFAULT -1;

select count(cost_oms) into v_costOMS from tbl_catalog_product_v2;

IF v_costOMS>40000 THEN

select 'Start: Pricing report', now();

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'development_co.tbl_pricing',
  'start',
  NOW(),
  NOW(),
  count(*),
  count(*)
FROM
  development_co.tbl_pricing
;

	Drop Table If Exists tbl_sku_monthly_sales;
	Create Table tbl_sku_monthly_sales ( index( sku) )
	SELECT sku, sku_config,
new_cat1, new_cat2, new_cat3, buyer, proveedor, brand,
	SUM(if(oac = 1 and returned = 0 and rejected = 0,if(datediff(curdate(),r.date)<=30,1,0),0)) AS num_sales_1_month, 
	SUM(if(oac = 1 and returned = 0 and rejected = 0,if(datediff(curdate(),r.Date)<=30,paid_price,0),0)) AS net_sales_1_month, 
	SUM(if(oac = 1 and returned = 0 and rejected = 0,if(datediff(curdate(),r.date)<=60,1,0),0)) AS num_sales_2_month, 
	SUM(if(oac = 1 and returned = 0 and rejected = 0,if(datediff(curdate(),r.Date)<=60,paid_price,0),0)) AS net_sales2_month, 
SUM(if(oac = 1 and returned = 0 and rejected = 0,if(datediff(curdate(),r.date)<=90,1,0),0)) AS num_sales_3_month, 
	SUM(if(oac = 1 and returned = 0 and rejected = 0,if(datediff(curdate(),r.Date)<=90,paid_price,0),0)) AS net_sales_3_month 
	FROM
	tbl_order_detail r
	WHERE
	datediff(curdate(),r.date)<=90
	group by sku;

   drop table if exists tmp_copy_pricing_tier;
     create table tmp_copy_pricing_tier as
    select sku_config,sku,pricing_comparison_tier from copy_pricing_tier;

    -- drop table if exists copy_pricing_tier;
    -- create table copy_pricing_tier as
   -- select sku_config,sku,pricing_comparison_tier from tbl_pricing;



drop table if exists tbl_pricing;
CREATE TABLE `development_co`.`tbl_pricing`
(
	`id_tbl_pricing` Integer auto_increment primary key,
	`new_cat1` Varchar(24) null,
	`new_cat2` Varchar(24) null,
	`new_cat3` Varchar(24) null,
	`supplier` Varchar(24) null,
	`brand` Varchar(64) null,
	`product_name` Varchar(64) null,
	`model` Varchar(64) null,
	`sku_config` Varchar(24) null,
	`sku` Varchar(24) null unique key,
tax_percent decimal(21,6),
	`price` decimal(21,6) null,
	`special_price` decimal(21,6) null,
special_to_date date null,
special_from_date date null,
	#`cost` decimal(21,6) null,
`cost_today` decimal(21,6) null,
	`cost_oms` decimal(21,6) null,
	`delivery_cost_supplier` decimal(21,6) null,
	`supplier_controled_price` tinyint(4) null,
	`fixed_price` tinyint(4) null,
	`sell_list` tinyint(4) null,
	`price_today` decimal(21,6) null,
	`price_after_tax` decimal(21,6) null,
	`voucher_value_after_tax`  decimal(21,6) null,
	`payment_fee` decimal(21,6) null,
	`shipping_fee`  decimal(21,6) null,
	`shipping_cost`  decimal(21,6) null,
	`shipping_fee_over_shipping_cost`  decimal(50,10) null,
	`PC1` decimal(50,10) null,
	`PC1_5` decimal(50,10)null,
	`pricing_tier` tinyint(4) null,
	`num_sales_last_7_days` decimal(50,10) null,
	`actual_paid_price_last_7_days`decimal(50,10) null,
	`actual_shipping_fee_last_7_days` decimal(50,10) null,
	`actual_shipping_cost_last_7_days` decimal(50,10) null,
	`actual_revenue_last_7_days` decimal(50,10) null,
	`actual_PC1_last_7_days` decimal(50,10) null,
	`actual_PC1_5_last_7_days` decimal(50,10) null,
	`num_sales_last_30_days` decimal(50,10) null,
	`actual_paid_price_last_30_days` decimal(50,10) null,
	`actual_shipping_fee_last_30_days` decimal(50,10) null,
	`actual_shipping_cost_last_30_days` decimal(50,10) null,
	`actual_revenue_last_30_days` decimal(50,10)null,
	`actual_PC1_last_30_days` decimal(50,10)null,
	`actual_PC1_5_last_30_days` decimal(50,10) null,
	`total_stock` int(11)  null,
	own_stock int (11) null,
	`shipping_fee_over_product_price` decimal(50,10) null,
eligible_for_free_shipping tinyint(4),
is_marketplace tinyint(4),
price_comparison tinyint(4),
attribute varchar(45),
repurchase_status varchar(45),
pricing_comparison_tier tinyint(4),
last_pricing_comparison_tier tinyint(4),
lab_pricing_tier varchar(255),
lab_pricing_comparison_tier varchar(255),
lab_last_pricing_comparison_tier  varchar(255),
lab_CR_PVS  varchar(255),
comparison_tier varchar(255),
config_counter  TINYINT NULL DEFAULT 0,
complete_catalog varchar(255),
top_50_sales varchar(255),
top_100_visits varchar(255),
visible varchar(4),
product_views int(32),
items int(32),
conversion_rate decimal(50,10),
buyer varchar(255),
	INDEX `sku_config_idx` (`sku_config`)
);

select 'Start: Truncate tbl pricing', now();
	truncate table tbl_pricing;

	Insert into `tbl_pricing`(new_cat1, new_cat2, new_cat3, supplier, brand,
	 product_name, model, sku_config, sku, price, special_price,  delivery_cost_supplier, sell_list,
	 cost_oms, tax_percent, fixed_price,special_to_date, special_from_date, price_comparison, attribute, repurchase_status,visible) 
	SELECT
	new_cat1, new_cat2, new_cat3, Supplier, brand, product_name, 
	model, sku_config, sku, price, special_price,  delivery_cost_supplier, sell_list, 
	 cost_oms,tax_percent,
	fixed_price, special_to_date, special_from_date, price_comparison, attribute, repurchase_status, visible
	FROM
	tbl_catalog_product_v2
	WHERE status_config = 'active' -- and pet_status = 'creation,edited,images' -- and qc = 1 
    and price is not null and price > 0 and cost_oms is not null ;

	update tbl_pricing t inner join tbl_catalog_product_v2 c on t.sku = c.sku
	set t.eligible_for_free_shipping = c.eligible_free_shipping, t.is_marketplace = c.is_marketplace;

update tbl_pricing t  
	set t.is_marketplace = 0 where t.is_marketplace is null;

update tbl_pricing t  
	set t.price_comparison = 0 where t.price_comparison is null;

select 'Start: Precios costos', now();

drop table if exists tbl_sku_precios_costos;
	create table tbl_sku_precios_costos ( index( sku) ) as
	SELECT tbl_catalog_product_v2.sku,
	 ifnull(IF (tbl_catalog_product_v2.special_price is null, tbl_catalog_product_v2.price,
	if(now() > tbl_catalog_product_v2.special_to_date, tbl_catalog_product_v2.price,
	 if(now() < tbl_catalog_product_v2.special_from_date, tbl_catalog_product_v2.price,
	 tbl_catalog_product_v2.special_price))),0) as 'precio_actual', 
	ifnull(IF (tbl_catalog_product_v2.specialpurchaseprice is null, tbl_catalog_product_v2.cost_oms, 
	if(now() > tbl_catalog_product_v2.special_to_date, tbl_catalog_product_v2.cost_oms, 
	if(now() < tbl_catalog_product_v2.special_from_date, tbl_catalog_product_v2.cost_oms,
	 tbl_catalog_product_v2.specialpurchaseprice))),0) as 'costo_actual'
 FROM 
	 tbl_catalog_product_v2  
group by tbl_catalog_product_v2.sku;

	update tbl_pricing t inner join 
	tbl_sku_precios_costos p on t.sku = p.sku
	set cost_today = costo_actual, 
	price_today = precio_actual;

update tbl_pricing set price_after_tax = price_today/(1+tax_percent/100);

select 'Start: Shipping fees', now();

drop table if exists tbl_sku_shipping_fees;
	create table tbl_sku_shipping_fees ( index( sku) ) as
select tab1.sku, sum(handling_cost*items)/sum(items)+ sum(freight_cost*items)/sum(items) shipping_fee,
sum(handling_cost*items)/sum(items) handling_cost, sum(freight_cost*items)/sum(items) freight_cost
from (
	select sku, name,fk_shipment_zone,
	#Se elige el mínimo handling cost de todos los posibles
	min(handling_cost) handling_cost,
	ifnull(min(freight_cost_ex), min(freight_cost)) freight_cost
	from (
	select cs.sku, cc.name,c.fk_catalog_category, handling_cost, s.cost_structure, s.fk_shipment_zone,
	if(ss.cost_structure = 'i',ss.freight_cost, ifnull(ifnull(cc.package_weight, cc.product_weight*ss.freight_cost),ss.freight_cost)) freight_cost_ex,
	if(s.cost_structure = 'i',s.freight_cost, ifnull(ifnull(cc.package_weight, cc.product_weight*s.freight_cost),s.freight_cost)) freight_cost
	 from bob_live_co.shipment_freight_cost s 
	inner join bob_live_co.catalog_config_has_catalog_category c 
	on c.fk_catalog_category = s.fk_catalog_category
	inner join bob_live_co.catalog_config cc 
	on id_catalog_config = c.fk_catalog_config
	inner join bob_live_co.catalog_simple cs 
	on id_catalog_config = cs.fk_catalog_config
	inner join bob_live_co.shipment_handling_cost sh 
	on sh.fk_catalog_category = c.fk_catalog_category
	left join bob_live_co.shipment_freight_cost_sku ss 
	on ss.fk_shipment_zone = s.fk_shipment_zone
	 and ss.fk_catalog_simple = cs.id_catalog_simple
	group by cs.sku, c.fk_catalog_category, s.fk_shipment_zone
	order by cs.sku,s.fk_shipment_zone, s.cost_structure ) t

	group by sku, fk_shipment_zone) tab1
inner join 
	(select tbl_order_detail.yrmonth, 
	tbl_order_detail.fk_shipment_zone,
	count(item)/c.items r, count(item) items
	from tbl_order_detail 
	inner join (select yrmonth, 
	count(distinct orderID) orders,
	 count(item) items
	from tbl_order_detail 
	where obc = 1
	and yrmonth = 201308
	group by yrmonth ) c 
	on tbl_order_detail.yrmonth =c.yrmonth
	where obc = 1
	and tbl_order_detail.yrmonth = 201308
	group by tbl_order_detail.yrmonth, 
	fk_shipment_zone
	order by r desc
	limit 6) distribucion_ventas 
on tab1.fk_shipment_zone = distribucion_ventas.fk_shipment_zone
where freight_cost > 0 and freight_cost is not null
group by sku;

	update tbl_pricing t inner join 
	tbl_sku_shipping_fees p on t.sku = p.sku
	set t.shipping_fee = p.shipping_fee;

	update tbl_pricing set shipping_fee = 0
    where eligible_for_free_shipping = 1;


select 'Start: Tbl coupon values', now();
drop table if exists tbl_coupon_values;
create table tbl_coupon_values ( index( precio_desde), index(precio_hasta), index(id_rango) ) as
select id_rango, precio_desde, precio_hasta
, sum(unit_price) unit_price
, sum(coupon_money_value) coupon_money_value
, sum(coupon_money_value)/sum(unit_price) proporcion
, label
from tbl_order_detail
inner join tbl_rangos_de_precios
on unit_price >= precio_desde and unit_price < precio_hasta
where oac = 1;

select 'Start: Shipping cost', now();
	drop table if exists tbl_sku_shipping_cost;
	create table tbl_sku_shipping_cost ( index( sku) ) as
	select sku, sum(shipping_cost_ponderado*items)/sum(items) shipping_cost 
from tbl_catalog_product_v2 t 
inner join tbl_shipping_costs_ponderados s 
on  product_weight >= peso_min
 and product_weight <= peso_max
inner join 
	(select tbl_order_detail.yrmonth, 
	tbl_order_detail.fk_shipment_zone_mapping,
	count(item)/c.items r, count(item) items
	from tbl_order_detail 
	inner join (select yrmonth, 
	count(distinct orderID) orders,
	 count(item) items
	from tbl_order_detail 
	where obc = 1
	and yrmonth = 201308
	group by yrmonth ) c 
on tbl_order_detail.yrmonth =c.yrmonth
where obc = 1
and tbl_order_detail.yrmonth = 201308
group by tbl_order_detail.yrmonth, region,
fk_shipment_zone_mapping
order by r desc
limit 6) distribucion_ventas 
on distribucion_ventas.fk_shipment_zone_mapping = s.fk_shipment_zone_mapping
where  payment_method_group = 'COD'
group by sku;

	update tbl_pricing t inner join 
	tbl_sku_shipping_cost p on t.sku = p.sku
	set t.shipping_cost = p.shipping_cost;


	#tbl_catalog_product_stock.ownstock (x.quantity), tbl_catalog_product_stock.supplierstock (z.quantity)
	UPDATE tbl_pricing t 
	INNER JOIN tbl_catalog_product_stock s ON s.sku=t.sku
	set t.total_stock= s.ownstock+s.supplierstock, t.own_stock = s.ownstock;

	
select 'Start: Coupon values', now();
drop table if exists tbl_coupon_values;
create table tbl_coupon_values ( index( precio_desde), index(precio_hasta), index(id_rango) ) as
select id_rango, precio_desde, precio_hasta
, sum(unit_price) unit_price
, sum(coupon_money_value) coupon_money_value
, sum(coupon_money_value)/sum(unit_price) proporcion
, label
from tbl_order_detail
inner join tbl_rangos_de_precios
on unit_price >= precio_desde and unit_price < precio_hasta
where oac = 1
and returned = 0
and rejected = 0
and yrmonth >= 201303
and ((coupon_code_prefix not in
(select Prefijo from tbl_prefijos_vouchers_sac)
and coupon_code not like 'VC%'
and coupon_code not like 'REO%'
and coupon_code not like 'REP%'
and coupon_code not like 'ER%'
and coupon_code not like 'CAN%'
and coupon_code not like 'DEV%'
and coupon_code_prefix not like 'CAC%'
and coupon_code not like '0000%'
and coupon_code_description not like 'Partner%'
and coupon_code_description not like '%CAC%'
and coupon_code not like 'CASHBACK%'
and coupon_code not like '%COMBO%'
and coupon_code_prefix <> ''
and coupon_code_prefix not like 'OUT%'
and coupon_code not like 'FBCAC%'
and coupon_code_prefix not like 'BANDEJA%'
and coupon_code_prefix not like 'ESTUFA%'
and coupon_code_prefix not like 'LM2X'
and coupon_code_prefix not like 'GC%'
and coupon_code_prefix not like 'KER%'
and coupon_code_prefix not like 'DOBLEOUT%'
and coupon_code_prefix not like 'PERFUME%'
AND coupon_code_prefix not like 'INVCAC%'
and coupon_code_description not like 'PR%'
and coupon_code_prefix not like 'NPS%'
and coupon_code_prefix not like 'INV%'
and coupon_code not like 'TAPETES%'
and coupon_code not like 'AUDIFONOS%'
and coupon_code not like 'SARTENES%'
and coupon_code not like 'alamaula%'
and coupon_code not like 'ML%'
and coupon_code not like 'cbit%'
and coupon_code not like 'Curebit%'
and coupon_code not like 'HVE%'
and coupon_code not like 'BLOG%'
and coupon_code not like 'CEROIVA%'
and coupon_code not like 'CS%'
and coupon_code not like 'Fabian%'
and coupon_code not like 'ANDRES%'
and coupon_code not like 'fbtrtost%'
and coupon_code not like 'HAV%'
and coupon_code not like 'TAPETE%'
and coupon_code not like 'CITI%'
and coupon_code not like 'PAGNOIVA%'
and coupon_code not like 'NOIVA%'
and coupon_code not like 'PAG%'
and coupon_code not like 'V-%'
and coupon_code not like 'VUELVEDINERO%'
and coupon_code not like 'DINEROVUELVE%'
and coupon_code not like 'VOUCHER%'
and coupon_code not like 'ONE80%'
and coupon_code not like 'CINTU50%'
and coupon_code not like 'PAN2X1%'
and coupon_code not like 'BO2X1%'
and coupon_code not like 'LE-MINUIT-%'
and coupon_code not like '2X1%'
and coupon_code not like 'oakley70%'
and coupon_code not like 'FELIZ%'
and coupon_code not like 'AV332HL77AJILACOL-119599%'
and coupon_code not like 'AV332HL77AJILACOL-119599%'
and coupon_code not like 'BCWAFLERAQZY%'
and coupon_code not like 'FBHOR%'
and coupon_code not like 'FBCITI282%'
and coupon_code not like 'AV332HL77AJILACOL-119599%'
and coupon_code not like 'FBTITANBX%'
and coupon_code not like 'FBBTA%'
and coupon_code not like 'DOBLEDINB0PqUO%'
and coupon_code not like 'FBTITANB%'
and coupon_code not like 'FBNIKONB%'
and coupon_code not like 'FBCCAM%'
and coupon_code not like 'FBBB%'
and coupon_code not like 'LUZMARINARIANO%'
and coupon_code not like 'FBO1%'
and coupon_code not like 'AV332HL77AJILACOL-119599%'
and coupon_code not like 'PRECIOBAJO%'
and coupon_code not like 'NADIEPUEDE%'
and coupon_code not like 'SHILHER%'
and coupon_code not like 'JULIANJIMENEZ1m6Zy%'
and coupon_code not like 'PRECIOBAJO%'
and coupon_code not like 'REBLU10jU6l%'
and coupon_code not like 'MUYFELIZ%'
and coupon_code not like 'NADIEPUEDE%'
and coupon_code not like 'AV332HL77AJILACOL-119599%'
and coupon_code not like 'PRECIOBAJO%'
and coupon_code not like 'NADIEPUEDE%'
and coupon_code not like 'SIMBLU09Rhr%'
and coupon_code not like 'PRECIOBAJO%'
and coupon_code not like 'JUAN.CORREDOR%'
and coupon_code not like 'NADIEPUEDE%'
and coupon_code not like 'IVANVELEZ%'
and coupon_code not like 'NADIEPUEDE%'
and coupon_code not like 'PRECIOBAJO%'
and coupon_code not like 'NADIEPUEDE%'
and coupon_code not like 'BCWAFLERAQZY%'
and coupon_code not like 'PRECIOBAJO%'
and coupon_code not like 'LOGJULIANESCANDONtdcO7%'
and coupon_code not like 'Karen%'
and coupon_code not like 'FBHOR%'
and coupon_code not like 'FBCITI282%'
and coupon_code not like 'AV332HL77AJILACOL-119599%'
and coupon_code not like 'NADIEPUEDE%'
and coupon_code not like 'FBTITANBX%'
and coupon_code not like 'FBBTA%'
and coupon_code not like 'NADIEPUEDE%'
and coupon_code not like 'Sergio%'
and coupon_code not like 'Jair%'
and coupon_code not like 'PRECIOBAJO%'
and coupon_code not like 'INS05uyv%'
and coupon_code not like 'DOBLEDINB0PqUO%'
and coupon_code not like 'FBTITANB%'
and coupon_code not like 'PRECIOBAJO%'
and coupon_code not like 'FBF70000%'
and coupon_code not like 'NADIEPUEDE%'
and coupon_code not like 'LUISALARCON%'
and coupon_code not like 'NADIEPUEDE%'
and coupon_code not like 'PRECIOBAJO%'
and coupon_code not like 'NADIEPUEDE%'
and coupon_code not like 'PRECIOBAJO%'
and coupon_code not like 'FBNIKONB%'
and coupon_code not like 'FBCCAM%'
and coupon_code not like 'FBBB%'
and coupon_code not like 'NADIEPUEDE%'
and coupon_code not like 'LENNDOQIb%'
and coupon_code not like 'PRECIOBAJO%'
and coupon_code in (select coupon_code from tbl_marketing_vouchers) )
or coupon_code is null)
and unit_price > 1000
and coupon_money_value >= 0
group by id_rango
order by precio_desde asc
;

set  @payment_cost_rate:= 0;
select sum(if(payment_method_group = 'COD', cod_rate, 0.029)*r) into @payment_cost_rate
from
(select sum(if(region like 'Bogot%', 0.01*p.r, 0.015*p.r)) cod_rate
from (
select tbl_order_detail.yrmonth,region,
count(item)/c.items r
from tbl_order_detail 
inner join (select yrmonth, 
count(distinct orderID) orders,
count(item) items
from tbl_order_detail 
where obc = 1
and yrmonth = 201308
group by yrmonth ) c 
on tbl_order_detail.yrmonth =c.yrmonth
where obc = 1
and tbl_order_detail.yrmonth = 201308
and fk_shipment_zone_mapping = 2392
union all
select tbl_order_detail.yrmonth,'Resto del país', 
count(item)/c.items r
from tbl_order_detail 
inner join (select yrmonth, 
count(distinct orderID) orders,
count(item) items
from tbl_order_detail 
where obc = 1
and yrmonth = 201308
group by yrmonth ) c 
on tbl_order_detail.yrmonth =c.yrmonth
where obc = 1
and tbl_order_detail.yrmonth = 201308
and fk_shipment_zone_mapping <> 2392
group by tbl_order_detail.yrmonth) p) ciudades
join(
select tbl_order_detail.yrmonth,payment_method_group,
count(item)/c.items r
from tbl_order_detail 
inner join (select yrmonth, 
count(distinct orderID) orders,
count(item) items
from tbl_order_detail 
where oac = 1 and returned = 0 and rejected = 0
and yrmonth = 201308
group by yrmonth ) c 
on tbl_order_detail.yrmonth =c.yrmonth
inner join tbl_payment_method_groups pm 
on pm.payment_method = tbl_order_detail.payment_method
where oac = 1 and returned = 0 and rejected = 0
and tbl_order_detail.yrmonth = 201308
group by yrmonth, payment_method_group) pm;

select 'Start: Sku payment costs', now();
drop table if exists tbl_sku_payment_costs;
create table tbl_sku_payment_costs ( index( sku) ) as
select pc.sku, precio_actual, shipping_fee,@payment_cost_rate,(sum(precio_actual) + sum(ifnull(shipping_fee,0)))*@payment_cost_rate payment_cost
from tbl_sku_precios_costos pc left join
tbl_sku_shipping_fees sf on
pc.sku = sf.sku
group by pc.sku;

	update tbl_pricing t 
	INNER JOIN tbl_catalog_product_v2 s on s.sku=t.sku 
	inner join tbl_sku_payment_costs pc on pc.sku = t.sku
	inner join tbl_coupon_values on t.price_today >= precio_desde and t.price_today < precio_hasta
	#INNER JOIN tbl_catalog_product_v2 d on s.fk_catalog_shipment_type=d.id_catalog_shipment_type 
	set t.PC1=
	#Precio vigente
    ifnull(t.price_today/(1+t.tax_percent/100),0) 
	#Shipping Fee
	+ ifnull(t.shipping_fee/(1+t.tax_percent/100),0)
	#Costo vigente
	- ifnull(t.cost_oms,0) 
	#Inbound cost
    - ifnull(t.delivery_cost_supplier,0) - 
	#Voucher amount
	tbl_coupon_values.proporcion*ifnull(t.price_today/(1+t.tax_percent/100),0) ,
	t.PC1_5=
	 #Price
	 ifnull(t.price_today/(1+t.tax_percent/100),0)
	 #Shipping Fee
	 + ifnull(t.shipping_fee/(1+t.tax_percent/100),0) 
	 #Costo vigente
     - ifnull(t.cost_oms,0) 
     - ifnull(t.delivery_cost_supplier,0) 
      #Payment cost
	 - pc.payment_cost 
	 #Shipping cost
    - if(s.`fulfillment_type` = 'Dropshipping', 0, ifnull(t.shipping_cost,0)),
	 t.shipping_fee_over_product_price=t.shipping_fee/t.price;


	update tbl_pricing t 
	set
	shipping_fee_over_shipping_cost=shipping_fee/shipping_cost; 

	update tbl_pricing p inner join 
	tbl_sku_payment_costs pc on p.sku = pc.sku
	set p.payment_fee = ifnull(pc.payment_cost,0);

	update tbl_pricing p inner join
	tbl_coupon_values c on p.price_today >= precio_desde and p.price_today < precio_hasta
	set voucher_value_after_tax = proporcion*ifnull(price_today,0);

	update tbl_pricing t 
	set t.pricing_tier = case 
    -- when t.fixed_price = 1 then 7
    when t.PC1_5 between 0 and -7500 then 4
    when t.PC1_5 <=-7500 and t.PC1_5/t.price_after_tax >-0.1  then 5
	when t.PC1_5/t.price_after_tax <= 0 then 6
	when t.PC1_5/t.price_after_tax between 0 and .1 then 3 
	when t.PC1_5/t.price_after_tax between .1 and .2 then 2 
	when t.PC1_5/t.price_after_tax >= .2 then 1 
    else 8
   END;

select 'Start: Sku actual numbers', now();
	Drop Table If Exists sku_actual_numbers;
	Create Table sku_actual_numbers ( index( sku) )
	SELECT sku, 
	SUM(if(oac = 1 and returned = 0 and rejected = 0,if(datediff(curdate(),r.date)<=7,1,0),0)) AS num_sales_7, 
	SUM(if(oac = 1 and returned = 0 and rejected = 0,if(datediff(curdate(),r.Date)<=7,paid_price_after_vat,0),0)) AS price_paid_7, 
	SUM(if(oac = 1 and returned = 0 and rejected = 0,if(datediff(curdate(),r.Date)<=7,shipping_fee,0),0)) AS shipping_fee_7,
	SUM(if(oac = 1 and returned = 0 and rejected = 0,if(datediff(curdate(),r.Date)<=7,shipping_cost,0),0)) AS shipping_cost_7,
	SUM(if(oac = 1 and returned = 0 and rejected = 0,if(datediff(curdate(),r.Date)<=7,paid_price_after_vat + shipping_fee_after_vat,0),0)) AS net_rev_7,
	SUM(if(oac = 1 and returned = 0 and rejected = 0,if(datediff(curdate(),r.Date)<=7,paid_price_after_vat + shipping_fee_after_vat - cost_oms - 
	delivery_cost_supplier,0),0)) AS PC1_7,
	SUM(if(oac = 1 and returned = 0 and rejected = 0,if(datediff(curdate(),r.Date)<=7,paid_price_after_vat + shipping_fee_after_vat - cost_oms - 
	delivery_cost_supplier - shipping_cost - payment_cost,0),0)) AS PC1_5_7,
	SUM(if(oac = 1 and returned = 0 and rejected = 0,if(datediff(curdate(),r.Date)<=30,1,0),0)) AS num_sales_30, 
	SUM(if(oac = 1 and returned = 0 and rejected = 0,if(datediff(curdate(),r.Date)<=30,paid_price_after_vat,0),0)) AS price_paid_30, 
	SUM(if(oac = 1 and returned = 0 and rejected = 0,if(datediff(curdate(),r.Date)<=30,shipping_fee_after_vat,0),0)) AS shipping_fee_30,
	SUM(if(oac = 1 and returned = 0 and rejected = 0,if(datediff(curdate(),r.Date)<=30,shipping_cost,0),0)) AS shipping_cost_30,
	SUM(if(oac = 1 and returned = 0 and rejected = 0,if(datediff(curdate(),r.Date)<=30,paid_price_after_vat + shipping_fee_after_vat,0),0)) AS net_rev_30,
	SUM(if(oac = 1 and returned = 0 and rejected = 0,if(datediff(curdate(),r.Date)<=30,paid_price_after_vat + shipping_fee_after_vat - cost_oms - 
	delivery_cost_supplier ,0),0)) AS PC1_30,
	SUM(if(oac = 1 and returned = 0 and rejected = 0,if(datediff(curdate(),r.Date)<=30,paid_price + shipping_fee - cost_oms - 
	delivery_cost_supplier - shipping_cost - payment_cost,0),0)) AS PC1_5_30 
	FROM
	tbl_order_detail r
	WHERE
	datediff(curdate(),r.date)<=30
	group by sku;

	Update  tbl_pricing t 
	INNER JOIN sku_actual_numbers a on a.sku=t.sku
	set 
	t.num_sales_last_7_days= a.num_sales_7,
	t.actual_paid_price_last_7_days=a.price_paid_7,
	t.actual_shipping_fee_last_7_days=a.shipping_fee_7,
	t.actual_shipping_cost_last_7_days=a.shipping_cost_7,
	t.actual_revenue_last_7_days=a.net_rev_7,
	t.actual_PC1_last_7_days=a.PC1_7, 
	t.actual_PC1_5_last_7_days=a.PC1_5_7,
	t.num_sales_last_30_days= a.num_sales_30,
	t.actual_paid_price_last_30_days=a.price_paid_30,
	t.actual_shipping_fee_last_30_days=a.shipping_fee_30,
	t.actual_shipping_cost_last_30_days=a.shipping_cost_30,
	t.actual_revenue_last_30_days=a.net_rev_30, 
	t.actual_PC1_last_30_days=a.PC1_30,
	t.actual_PC1_5_last_30_days=a.PC1_5_30;

UPDATE tbl_pricing t inner join price_comparison 
on t.sku_config = price_comparison.sku_config
set t.pricing_comparison_tier = 
if(checked = 0, 0,
if(benchmark = 0, 0,
if(checked = 1 and price_today <= min*0.9, 1,
if(checked = 1 and price_today between min*0.9 and min, 2,
if(checked = 1 and price_today between min and promedio, 3,
if(checked = 1 and price_today = promedio, 4,
if(checked = 1 and price_today between promedio and max, 5,
if(checked = 1 and price_today between max and max*1.1, 6,
if(checked = 1 and price_today > max*1.1, 7,-1)))))))));

update tbl_pricing set pricing_comparison_tier = 0 where pricing_comparison_tier is null;

update tbl_pricing t inner join price_comparison 
on t.sku_config = price_comparison.sku_config
set comparison_tier = 
if(pricing_comparison_tier <> 0, 'Comparable',
if(checked=null,'TBD',
if(benchmark = 0, 'No Comparisson Found','TBD')));

update tbl_pricing set comparison_tier = 'TBD' where comparison_tier is null;

update tbl_pricing t inner join (SELECT sku_config, max(sku) sku from tbl_pricing
group by sku_config) p on t.sku_config = p.sku_config and t.sku = p.sku
set config_counter = 1;

update tbl_pricing set complete_catalog = 'Complete Catalog';
select 'Start: Top 50 y top 100', now();
update tbl_pricing t inner join (select todo.rownum - min + 1 row_num,todo.yrmonth, todo.new_cat1, todo.sku_config,todo.net_sales from
(SELECT @rownum:=@rownum+1 AS rownum, t.*
from
 (select yrmonth, new_cat1, sku_config,sum(paid_price_after_vat) + sum(shipping_fee_after_vat) net_sales
from tbl_order_detail
where oac = 1 
and returned = 0
and rejected = 0
and yrmonth = 201309
group by yrmonth, new_cat1, sku_config
order by new_cat1 asc,net_sales desc) t , (SELECT @rownum:=0) r )  todo
inner join
(select new_cat1, min(rownum) min from (
SELECT @rownum:=@rownum+1 AS rownum, t.*
from
 (select yrmonth, new_cat1, sku_config,sum(paid_price_after_vat) + sum(shipping_fee_after_vat) net_sales
from tbl_order_detail
where oac = 1 
and returned = 0
and rejected = 0
and yrmonth = 201309
group by yrmonth, new_cat1, sku_config
order by new_cat1 asc,net_sales desc) t , (SELECT @rownum:=0) r ) t
group by new_cat1) top on todo.new_cat1 = top.new_cat1
where todo.rownum<= top.min+99) s 
on t.sku_config = s.sku_config 
set top_50_sales = 'Products from top 100 sales per category'
where config_counter = 1;

update tbl_pricing t inner join (select todo.rownum - min + 1 row_num,todo.yr_mt, todo.new_cat1, todo.sku_config,todo.product_views from
(SELECT @rownum:=@rownum+1 AS rownum, t.*
from
 (select yr_mt, c.new_cat1, v.sku_config,sum(product_views) product_views
from tbl_monthly_visits v inner join tbl_catalog_product_v2 c on v.sku_config = c.sku_config
where yr_mt = 201309
group by yr_mt, c.new_cat1, v.sku_config
order by new_cat1 asc,product_views desc) t , (SELECT @rownum:=0) r )  todo
inner join
(select new_cat1, min(rownum) min from (
SELECT @rownum:=@rownum+1 AS rownum, t.*
from
 (select yr_mt, c.new_cat1, v.sku_config,sum(product_views) product_views
from tbl_monthly_visits v inner join tbl_catalog_product_v2 c on v.sku_config = c.sku_config
where yr_mt = 201309
group by yr_mt, c.new_cat1, v.sku_config
order by new_cat1 asc,product_views desc) t , (SELECT @rownum:=0) r ) t
group by new_cat1) top on todo.new_cat1 = top.new_cat1
where todo.rownum<= top.min+2000) s 
on t.sku_config = s.sku_config
set top_100_visits = 'Products from top 2000 visits per category'
where config_counter = 1;


set @cs:= 0;
set @wh:=0;
select avg(cs), avg(wh) into @cs, @wh
from tbl_order_detail
where oac = 1 and returned = 0 and rejected = 0
and date between date_add(curdate(), interval -60 day)
and curdate();

select 'Start: Last pricing tier', now();
update tbl_pricing t inner join copy_pricing_tier
on t.sku = copy_pricing_tier.sku
	set t.last_pricing_comparison_tier = copy_pricing_tier.pricing_comparison_tier;

#Last pricing tier
select count(pricing_comparison_tier) into v_costOMS from tbl_pricing;

IF v_costOMS<40000 THEN
     drop table if exists copy_pricing_tier;
     create table copy_pricing_tier as
    select sku_config,sku,pricing_comparison_tier from tmp_copy_pricing_tier;

update tbl_pricing t inner join copy_pricing_tier
on t.sku = copy_pricing_tier.sku
	set t.last_pricing_comparison_tier = copy_pricing_tier.pricing_comparison_tier;
 END IF;


select 'Start: Conversion rates', now();
Drop Table If Exists tmpVisitsDay;
create table tmpVisitsDay ( index( sku,date) )
	select sku,sum(product_views) product_views,date from tbl_daily_visits x force index (date_idx)
	where x.date >=CURDATE() - INTERVAL 4 MONTH 
	group by date,x.sku;


Drop Table If Exists tmpBoughtsDay;
create table tmpBoughtsDay ( index( sku_config,date) )
select date,sku_config,count(sku) items
	from tbl_order_detail z 
	where z.date >=CURDATE() - INTERVAL 4 MONTH 
	and OAC=1 and returned = 0 group by date,z.sku_config;

drop table if exists tbl_conversion_rates_sku;
	create table tbl_conversion_rates_sku ( index( sku) ) as
select tmpVisitsDay.sku, sum(product_views) product_views,
sum(items) items,sum(items)/sum(product_views) conversion_rate
 from ( 
	tmpVisitsDay
  inner join 
	tmpBoughtsDay
on tmpVisitsDay.sku=tmpBoughtsDay.sku_config 
and tmpVisitsDay.date=tmpBoughtsDay.date) group by tmpVisitsDay.sku;

select 'Start: Updating', now();
update tbl_pricing t inner join 
	tbl_conversion_rates_sku p on t.sku_config = p.sku
	set t.product_views = p.product_views,t.conversion_rate = p.conversion_rate,
	t.items = p.items;
select 'End: Conversion rates', now();


 update tbl_pricing t inner join tbl_pricing_tiers pt on t.pricing_tier = pt.pricing_tier
 set lab_pricing_tier = label;

 update tbl_pricing t inner join tbl_pricing_tiers pt on t.last_pricing_comparison_tier = pt.pricing_comparison_tier
 set lab_last_pricing_comparison_tier = label;

 update tbl_pricing t inner join tbl_pricing_tiers pt on t.pricing_comparison_tier = pt.pricing_comparison_tier
 set lab_pricing_comparison_tier = label;

UPDATE tbl_pricing t 
set t.lab_CR_PVS = 
if(product_views >= 100 and conversion_rate > 0.02, '>2% CR & >=100 PVs',
if(product_views >= 100 and conversion_rate between 0.01 and 0.02, '1%-2% CR & >=100 PVs',
if(product_views >= 100 and conversion_rate between 0 and 0.01, '0%-1% CR & >=100 PVs',
if(product_views >= 100 and conversion_rate =0, '0% CR & >=100 PVs',
if(product_views < 100 and conversion_rate > 0.02, '>2% CR & <100 PVs',
if(product_views < 100 and conversion_rate between 0.01 and 0.02, '1%-2% CR & <100 PVs',
if(product_views < 100 and conversion_rate between 0 and 0.01, '0%-1% CR & <100 PVs',
if(product_views < 100 and conversion_rate =0, '0% CR & <100 PVs',
' =0% CR & <100 PVs'))))))));


select 'Update Buyer', now();
update tbl_pricing t inner join tbl_catalog_product_v2 c on t.sku = c.sku
	set t.buyer = c.buyer;

select 'TempPricingTable: Start', now();
drop table If Exists tempPricingTable;
create table tempPricingTable
SELECT @rownum:=@rownum+1 AS posicionVentas,t.*, curdate()-INTERVAL 1 DAY  diaActual
from
 (SELECT a.*,b.net_sales_1_month+b.net_sales2_month total2Months,d.checked,d.min,
d.max,d.promedio,d.benchmark FROM (development_co.tbl_pricing a 
left join tbl_sku_monthly_sales b on a.sku = b.sku 
) left join development_co.price_comparison d 
on a.sku_config = d.sku_config 
order by total2Months desc ) t , 
(SELECT @rownum:=0) r;

select 'FinalPricingTable: Start', now();
drop table If Exists finalPricingTable;
create table finalPricingTable
select * from tempPricingTable group by sku;

select count(sku_config) into v_time from catalog_history_all_prices where date=curdate();
IF v_time<50 THEN
 
select 'Actualizar pricing history',now();
insert into development_co.catalog_history_all_prices 
(yrmonth,date,sku_config,sku_simple,pvp,spvp,costo,shipping_fee_item) 
select concat(year(now()),if(month(now())<10,concat('0',month(now())),month(now()))) yrmonth,
 concat(year(now()),'-',if(month(now())<10,concat('0',month(now())),month(now())),'-',day(now())) date,
t.sku_config,t.sku,price,x.precio_actual,x.costo_actual,y.shipping_fee
from (tbl_catalog_product_v2 t left join tbl_sku_precios_costos x on t.sku=x.sku)
left join tbl_sku_shipping_fees y on t.sku=y.sku;


 END IF;

select 'End: Pricing report', now();

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'development_co.tbl_pricing',
  'finish',
  NOW(),
  NOW(),
  count(*),
  count(*)
FROM
  development_co.tbl_pricing
;

ELSE

select 'End: No costs ready', now();
 END IF;
