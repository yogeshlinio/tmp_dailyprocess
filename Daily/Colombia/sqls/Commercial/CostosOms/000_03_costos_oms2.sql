call monitoring("Colombia","production_co.tbl_bi_ops_stock parte 1");

#Notas crédito + Cambios costos por errores en PET
UPDATE development_co.tbl_order_detail od
JOIN development_co.tbl_cambios_costos cc
  ON od.sku=cc.sku
SET od.cost_oms=cc.cost
WHERE od.date BETWEEN cc.from_date AND cc.to_date;

UPDATE development_co.tbl_order_detail od
JOIN development_co.tbl_cambios_costos cc
  ON od.sku_config=cc.sku
SET od.cost_oms=cc.cost
WHERE od.date BETWEEN cc.from_date AND cc.to_date;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'development_co.tbl_sku_costos_oms',
  'finish',
  NOW(),
  NOW(),
  count(*),
  count(*)
FROM
  development_co.tbl_sku_costos_oms
;

Select 'Costos OMS: END', now();
