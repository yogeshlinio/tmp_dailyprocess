call monitoring("Colombia","production_co.tbl_bi_ops_stock parte 1");

#Órdenes de marketplace

select 'Start: costos_marketplace ', now();
update development_co.tbl_order_detail t 
inner join development_co.tbl_marketplace_sku m
on t.fk_supplier = m.fk_supplier and t.sku = m.sku
set cost_oms = (unit_price_after_vat*over_paid_price+
shipping_fee_after_vat*over_shipping_fee)*(1-commission),
costo_after_vat = (unit_price_after_vat*over_paid_price+
shipping_fee_after_vat*over_shipping_fee)*(1-commission),
delivery_cost_supplier = 0, wh = 0, shipping_cost = 0
where is_marketplace = 1;

update development_co.tbl_order_detail t 
inner join development_co.tbl_marketplace_sku m
on t.fk_supplier = m.fk_supplier and t.sku = m.sku
set cost_oms = (unit_price_after_vat*over_paid_price+
shipping_fee_after_vat*over_shipping_fee)*(1-commission),
costo_after_vat = (unit_price_after_vat*over_paid_price+
shipping_fee_after_vat*over_shipping_fee)*(1-commission),
delivery_cost_supplier = 0, wh = 0, shipping_cost = 0
where yrmonth >= 201311;

update tbl_order_detail od inner join 
(select sku, product_name,is_marketplace, proveedor, c.Over__paid_price, c.over_shipping_fee,fk_supplier, min(date), avg(Fee) commission
from tbl_order_detail t inner join tbl_marketplace_cat_com_v5 c 
on t.fk_supplier = c.id_supplier 
where  cost_oms = 0 and date >= '2013-12-01'
and oac = 1 and returned = 0 and rejected = 0
group by sku, fk_supplier) t on od.sku = t.sku
set cost_oms = (unit_price_after_vat*Over__paid_price+
shipping_fee_after_vat*over_shipping_fee)*(1-commission),
costo_after_vat = (unit_price_after_vat*Over__paid_price+
shipping_fee_after_vat*over_shipping_fee)*(1-commission),
delivery_cost_supplier = 0, wh = 0, shipping_cost = 0 ;


select 'End: costos_marketplace ', now();
