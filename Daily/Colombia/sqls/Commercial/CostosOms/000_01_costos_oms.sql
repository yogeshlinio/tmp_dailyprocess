call monitoring("Colombia","production_co.tbl_bi_ops_stock parte 1");

Select 'Costos OMS: START', now();

Truncate table development_co.tbl_sku_costos_oms;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'development_co.tbl_sku_costos_oms',
  'start',
  NOW(),
  NOW(),
  count(*),
  count(*)
FROM
  development_co.tbl_sku_costos_oms
;

INSERT INTO development_co.tbl_sku_costos_oms(sku, price_before_tax, unidades)
(select p.sku_simple, p.price_before_tax, count(p.op) unidades
from production_co.tbl_bi_ops_procurement p inner join production_co.tbl_bi_ops_stock s
 on p.sku_simple = s.sku_simple and p.op = s.op
WHERE p.is_cancelled=0 and in_stock = 1
AND p.is_deleted=0 
group by p.sku_simple desc, p.price_before_tax);

update development_co.tbl_sku_costos_oms set price_before_tax = price_before_tax*1000
where sku in (
'VA287FA35XIELACOL-116259',
'VA287FA35XIELACOL-116260',
'VA287FA35XIELACOL-116261',
'VA287FA35XIELACOL-116262',
'VA287FA35XIELACOL-116264',
'VA287FA36XIDLACOL-116251',
'VA287FA36XIDLACOL-116254',
'VA287FA36XIDLACOL-116258',
'VA287FA37XICLACOL-116243',
'VA287FA37XICLACOL-116244',
'VA287FA37XICLACOL-116245',
'VA287FA37XICLACOL-116246',
'VA287FA37XICLACOL-116247',
'VA287FA37XICLACOL-116248',
'VA287FA37XICLACOL-116249',
'VA287FA37XICLACOL-116250',
'VA287FA38XIBLACOL-116238',
'VA287FA38XIBLACOL-116239');

update development_co.tbl_sku_costos_oms set costo_total = price_before_tax * unidades;

#update tbl_catalog_product_v2 set cost_oms = 0 where cost_oms is null;

Select 'Updating OMS catalog_product_v2', now();

update development_co.tbl_catalog_product_v2 c inner join (
select sku, costo_total/unidades_totales as costo_ponderado from (
select sku, sum(costo_total) costo_total, sum(unidades) unidades_totales 
from development_co.tbl_sku_costos_oms group by sku desc) t) p on c.sku = p.sku set cost_oms = p.costo_ponderado;

#Cuando no encuentra costo en OMS, pone el costo vigente de PET
update development_co.tbl_catalog_product_v2  set cost_oms = 
ifnull(IF (tbl_catalog_product_v2.specialpurchaseprice is null, 
tbl_catalog_product_v2.cost, 
 if(curdate() > tbl_catalog_product_v2.special_to_date, tbl_catalog_product_v2.cost, 
 if(curdate() < tbl_catalog_product_v2.special_from_date, tbl_catalog_product_v2.cost,
  tbl_catalog_product_v2.specialpurchaseprice))),0) where cost_oms is null;

Select 'Updating OMS tbl_order_detail', now();

#Costos OMS
UPDATE development_co.tbl_order_detail  inner JOIN wmsprod_co.itens_venda ON tbl_order_detail.Item=itens_venda.item_id 
inner JOIN wmsprod_co.estoque ON itens_venda.estoque_id=estoque.estoque_id 
inner JOIN wmsprod_co.itens_recebimento ON estoque.itens_recebimento_id=itens_recebimento.itens_recebimento_id 
inner JOIN wmsprod_co.recebimento ON recebimento.recebimento_id=itens_recebimento.recebimento_id 
inner JOIN (
SELECT DISTINCT op, sku_simple, price_before_tax, is_cancelled, is_deleted 
FROM production_co.tbl_bi_ops_procurement 
where  tbl_bi_ops_procurement.is_cancelled=0 AND tbl_bi_ops_procurement.is_deleted=0
) tbl_bi_ops_procurement
ON recebimento.inbound_document_identificator=tbl_bi_ops_procurement.OP 
AND tbl_order_detail.sku=tbl_bi_ops_procurement.sku_simple 
set tbl_order_detail.cost_oms = tbl_bi_ops_procurement.price_before_tax;

#Cuando no lo encuentra en OMS, pone el ponderado de OMS
update development_co.tbl_order_detail c inner join (
select sku, costo_total/unidades_totales as costo_ponderado from (
select sku, sum(costo_total) costo_total, sum(unidades) unidades_totales 
from development_co.tbl_sku_costos_oms group by sku desc) t) p on c.sku = p.sku set cost_oms = p.costo_ponderado
where c.cost_oms is null;

#Si no lo encontró por ningún lado en OMS, pone el costo vigente en PET para la fecha de la orden 
/*update development_co.tbl_order_detail inner join development_co.tbl_catalog_product_v2 on tbl_order_detail.sku = tbl_catalog_product_v2.sku
set tbl_order_detail.cost_oms = 
ifnull(IF (tbl_catalog_product_v2.specialpurchaseprice is null, 
tbl_catalog_product_v2.cost, 
 if(date >= tbl_catalog_product_v2.special_to_date, tbl_catalog_product_v2.cost, 
 if(date <= tbl_catalog_product_v2.special_from_date, tbl_catalog_product_v2.cost,
  tbl_catalog_product_v2.specialpurchaseprice))),0) where tbl_order_detail.cost_oms is null;*/

update development_co.tbl_order_detail set tbl_order_detail.cost_oms =  costo_after_vat 
where (tbl_order_detail.cost_oms is null or tbl_order_detail.cost_oms = 0 and costo_after_vat > 0) and (is_marketplace = 0 or is_marketplace is null);


update development_co.tbl_order_detail
set cost_oms = cost_oms*1000
where sku in (
'VA287FA35XIELACOL-116259',
'VA287FA35XIELACOL-116260',
'VA287FA35XIELACOL-116261',
'VA287FA35XIELACOL-116262',
'VA287FA35XIELACOL-116264',
'VA287FA36XIDLACOL-116251',
'VA287FA36XIDLACOL-116254',
'VA287FA36XIDLACOL-116258',
'VA287FA37XICLACOL-116243',
'VA287FA37XICLACOL-116244',
'VA287FA37XICLACOL-116245',
'VA287FA37XICLACOL-116246',
'VA287FA37XICLACOL-116247',
'VA287FA37XICLACOL-116248',
'VA287FA37XICLACOL-116249',
'VA287FA37XICLACOL-116250',
'VA287FA38XIBLACOL-116238',
'VA287FA38XIBLACOL-116239') and cost_oms > 1 and cost_oms < 100;
