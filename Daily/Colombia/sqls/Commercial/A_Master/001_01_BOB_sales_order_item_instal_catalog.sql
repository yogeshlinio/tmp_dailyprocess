USE development_co;
/* 
*   UPDATE FIELDS ON OSRI
*/
UPDATE            development_co.A_Master_Sample
       INNER JOIN development_co.A_Master_Catalog
	             ON development_co.A_Master_Catalog.SKU_Simple = development_co.A_Master_Sample.SKUSimple
SET
    A_Master_Sample.IdSupplier   = A_Master_Catalog.id_catalog_Supplier,
    A_Master_Sample.Supplier   = A_Master_Catalog.Supplier,
    A_Master_Sample.Buyer      = A_Master_Catalog.Buyer,
#    A_Master_Sample.HeadBuyer  = A_Master_Catalog.Head_Buyer,
    A_Master_Sample.Brand      = A_Master_Catalog.Brand,
    A_Master_Sample.isMPlace   = A_Master_Catalog.isMarketPlace,
    A_Master_Sample.isMPlaceSince       = A_Master_Catalog.isMarketPlace_Since,
    A_Master_Sample.isActiveSKUConfig   = A_Master_Catalog.isActive_SKUConfig,
    A_Master_Sample.isActiveSKUSimple   = A_Master_Catalog.isActive_SKUSimple,

    A_Master_Sample.Cat1       = A_Master_Catalog.Cat1,
    A_Master_Sample.Cat2       = A_Master_Catalog.Cat2,
    A_Master_Sample.Cat3       = A_Master_Catalog.Cat3,
   
    A_Master_Sample.OldCat1    = A_Master_Catalog.OldCat1, 
    A_Master_Sample.OldCat2    = A_Master_Catalog.OldCat2, 
    A_Master_Sample.OldCat3    = A_Master_Catalog.OldCat3, 
    A_Master_Sample.CatKPI     = A_Master_Catalog.Cat_KPI,
    A_Master_Sample.CatBP      = A_Master_Catalog.Cat_BP,
    
    A_Master_Sample.PackageWeight = A_Master_Catalog.Package_Weight,
    A_Master_Sample.ProductWeight = A_Master_Catalog.Product_weight,
	A_Master_Sample.PackageHeight = A_Master_Catalog.Package_height,
	A_Master_Sample.PackageLength = A_Master_Catalog.Package_length,
	A_Master_Sample.PackageWidth	 = A_Master_Catalog.Package_width, 

    A_Master_Sample.OriginalPrice        = A_Master_Catalog.Original_Price  ,
    A_Master_Sample.DeliveryCostSupplier = IF( DeliveryCostSupplier <= 0 OR ISNULL( DeliveryCostSupplier ),
                                                           	  A_Master_Catalog.Delivery_Cost_Supplier, 
                                                              DeliveryCostSupplier )
;

/*
*	 001_999_001 Adjusts 
*/
UPDATE development_co.A_Master_Sample
SET
    A_Master_Sample.OriginalPrice = A_Master_Sample.Price
WHERE A_Master_Sample.OriginalPrice <= 0;



