ALTER TABLE development_co.A_Master_Sample
            DROP INDEX  fk_customer_address_region ,
            DROP INDEX  fk_sales_order_address_shipping ,
            DROP INDEX  fk_sales_order_item_status,
 	    DROP INDEX  PaymentMethodStatus ,
            DROP INDEX  fk_catalog_shipment_type  ,
            DROP COLUMN fk_customer_address_region      ,
            DROP COLUMN fk_sales_order_address_shipping ,
            DROP COLUMN fk_sales_order_item_status,
            DROP COLUMN fk_catalog_shipment_type  
;

DROP TABLE IF EXISTS development_co.A_Master;
CREATE TABLE development_co.A_Master LIKE development_co.A_Master_Template;

REPLACE development_co.A_Master
SELECT
   A_Master_Sample.*
FROM development_co.A_Master_Sample;

DROP TABLE IF EXISTS development_co.A_Master_Backup;
CREATE TABLE development_co.A_Master_Backup LIKE development_co.A_Master;
INSERT INTO development_co.A_Master_Backup
SELECT * FROM development_co.A_Master;

