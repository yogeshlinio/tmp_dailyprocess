 #Query: M1_COGS_PC
UPDATE development_co.A_Master_Sample 
SET 
    development_co.A_Master_Sample.COGS       =  development_co.A_Master_Sample.CostAfterTax 
                                     +development_co.A_Master_Sample.DeliveryCostSupplier,

    development_co.A_Master_Sample.PCOne     =  development_co.A_Master_Sample.priceAfterTax
                                     +development_co.A_Master_Sample.ShippingFee
                                     -development_co.A_Master_Sample.CouponValueAfterTax
                                     -development_co.A_Master_Sample.CostAfterTax
                                     -development_co.A_Master_Sample.DeliveryCostSupplier, 

    development_co.A_Master_Sample.PCOnePFive =  development_co.A_Master_Sample.priceAfterTax
                                      +development_co.A_Master_Sample.ShippingFee
                                      +development_co.A_Master_Sample.Interest
                                      -development_co.A_Master_Sample.CouponValueAfterTax
                                      -development_co.A_Master_Sample.CostAfterTax
                                      -development_co.A_Master_Sample.DeliveryCostSupplier
                                      -development_co.A_Master_Sample.ShippingCost
                                      -development_co.A_Master_Sample.PaymentFees
									  -development_co.A_Master_Sample.PackagingCost, 
									  
    development_co.A_Master_Sample.PCTwo =  development_co.A_Master_Sample.PCOnePFive
											-development_co.A_Master_Sample.FLWHCost
											-development_co.A_Master_Sample.FLCSCost,
											
    development_co.A_Master_Sample.Rev =  development_co.A_Master_Sample.PriceAfterTax  
                              +development_co.A_Master_Sample.ShippingFee
                              -development_co.A_Master_Sample.CouponValueAfterTax
                              +development_co.A_Master_Sample.Interest
;
    
