DROP TABLE IF EXISTS development_co.A_E_BI_First_OrdersGross;
CREATE TABLE development_co.A_E_BI_First_OrdersGross (  PRIMARY KEY ( CustomerNum , id ), INDEX( CustomerNum ), id int auto_increment )
SELECT
  CustomerNum,
  OrderNum,
  NULL AS id,
  OrderAfterCan,
  OrderBeforeCan
FROM  
  development_co.A_Master_Sample
WHERE  
  OrderBeforeCan = 1
ORDER BY 
  CustomerNum , Date asc;

DELETE FROM A_E_BI_First_OrdersGross WHERE id > 1 ;

UPDATE            A_E_BI_First_OrdersGross
       INNER JOIN development_co.A_Master_Sample  USING( CustomerNum )
SET
  A_Master_Sample.NewReturningGross  = IF( A_E_BI_First_OrdersGross.OrderNum = development_co.A_Master_Sample.OrderNum, "NEW", "RETURNING"  );



