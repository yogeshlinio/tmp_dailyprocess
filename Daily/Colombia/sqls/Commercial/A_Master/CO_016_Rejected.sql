DROP   TEMPORARY TABLE IF EXISTS development_co.A_Rejections_Shipped;
CREATE TEMPORARY TABLE development_co.A_Rejections_Shipped ( INDEX ( ItemID ) )
SELECT
   fk_sales_order_item as ItemID
FROM
   bob_live_co.sales_order_item_status_history 
WHERE 
   fk_sales_order_item_status  = 5
GROUP BY ItemID
;


DROP   TEMPORARY TABLE IF EXISTS development_co.A_Rejections_Delivered;
CREATE TEMPORARY TABLE development_co.A_Rejections_Delivered ( INDEX ( ItemID ) )
SELECT
   fk_sales_order_item as ItemID
FROM
   bob_live_co.sales_order_item_status_history 
WHERE 
   fk_sales_order_item_status  = 68
GROUP BY ItemID
;

DROP   TEMPORARY TABLE IF EXISTS development_co.A_Rejections_Shipped_NoDelivered;
CREATE TEMPORARY TABLE development_co.A_Rejections_Shipped_NoDelivered ( INDEX ( ItemID ) )
SELECT
   ItemID
FROM
   development_co.A_Rejections_Shipped 
WHERE 
   ItemId  not IN ( SELECT * FROM development_co.A_Rejections_Delivered )
;


UPDATE            development_co.A_Master_Sample 
       INNER JOIN development_co.A_Rejections_Shipped_NoDelivered
            USING ( ItemID )
SET
   development_co.A_Master_Sample.Rejected = 1

WHERE
#       Out_SalesReportItem.Cancellations = 1
#   AND 
       development_co.A_Master_Sample.Status in (
                                       "canceled" ,
                                       "cancelled" ,
                                       "refund_needed" ,
                                       "store_credit_issued" ,
                                       "clarify_refund_not_processed",
                                       "store_credit_needed",
                                       "store_credit_issued",
                                       "clarify_store_credit_not_issued" ,
                                       "refunded"
                                     )

;
