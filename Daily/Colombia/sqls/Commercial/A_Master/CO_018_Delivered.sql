UPDATE           development_co.A_Master_Sample
SET
    development_co.A_Master_Sample.Delivered = 1
WHERE DateDelivered != "0000-00-00"
;

UPDATE development_co.A_Master_Sample
SET
    development_co.A_Master_Sample.NetDelivered = 1
WHERE
    Delivered = 1 
AND OrderAfterCan = 1
;

UPDATE development_co.A_Master_Sample
SET
    development_co.A_Master_Sample.DeliveredReturn = 1
WHERE
        Delivered     = 1
    AND NetDelivered  = 0;
