USE development_co;
DROP TABLE IF EXISTS TMP_Coupons;
CREATE TABLE TMP_Coupons ( INDEX( CouponCode ) )
SELECT
   bob_live_co.sales_rule.code AS CouponCode,
   bob_live_co.sales_rule_set.code_prefix	AS PrefixCode,
   bob_live_co.sales_rule_set.description AS description
FROM
        	bob_live_co.sales_rule 
LEFT JOIN bob_live_co.sales_rule_set 
       ON bob_live_co.sales_rule.fk_sales_rule_set = bob_live_co.sales_rule_set.id_sales_rule_set 
;

/* 
*   UPDATE FIELDS ON OSRI
*/
UPDATE            development_co.A_Master_Sample
       INNER JOIN development_co.TMP_Coupons
	          USING ( CouponCode )
SET
    development_co.A_Master_Sample.PrefixCode = development_co.TMP_Coupons.PrefixCode,
	development_co.A_Master_Sample.CouponCodeDescription = TMP_Coupons.description
;
