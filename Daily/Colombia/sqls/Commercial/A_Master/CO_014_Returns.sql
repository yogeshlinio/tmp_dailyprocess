DROP   TEMPORARY TABLE IF EXISTS development_co.A_Returns;
CREATE TEMPORARY TABLE development_co.A_Returns ( INDEX ( fk_sales_order_item  ) )
SELECT
 fk_sales_order_item
FROM
    bob_live_co.sales_order_item_status_history
WHERE
   bob_live_co.sales_order_item_status_history.fk_sales_order_item_status IN ( 8 )
GROUP BY fk_sales_order_item
;

REPLACE development_co.A_Returns
SELECT 
   c.item_id as fk_sales_order_item
FROM          wmsprod.inverselogistics_status_history b 
   INNER JOIN wmsprod.inverselogistics_devolucion c 
           ON b.return_id = c.id
WHERE
   b.status = 'retornado';


UPDATE           development_co.A_Returns
      INNER JOIN development_co.A_Master_Sample
              ON development_co.A_Returns.fk_sales_order_item = development_co.A_Master_Sample.ItemID 
SET
    development_co.A_Master_Sample.Returns = 1
;

#Query: A 102 U OrderBefore-AfterCan
UPDATE development_co.A_Master_Sample
SET 
     development_co.A_Master_Sample.Cancellations  = IF(    development_co.A_Master_Sample.Cancellations = 1 
                                                         AND development_co.A_Master_Sample.Returns       = 0 , 1 , 0 )
;


/*
* DATE           2013/12/05
* DEVELOPED BY   Carlos Antonio Mondragón Soria
* DESCRIPTION MKT - Status Wrong in Returns 
*/
UPDATE development_co.A_Master_Sample
SET
   OrderAfterCan = 0,
   Cancellations = 0,
   Pending       = 0
WHERE
   Returns = 1;

