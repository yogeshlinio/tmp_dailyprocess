USE development_co;
/*
* CREATION TABLE A_Master_Catalog
* filled with information from
* catalog_simple and catalog_config
*/ 
DROP TABLE IF EXISTS development_co.A_Master_Catalog;
CREATE TABLE development_co.A_Master_Catalog ( 
                                          INDEX ( id_catalog_config ),
                                          INDEX ( id_catalog_simple ),
                                          INDEX ( sku_simple ), 
                                          INDEX ( sku_config ), 
										                      INDEX ( id_catalog_supplier ),
#										                      INDEX ( id_catalog_attribute_option_global_head_buyer_name ),
                                      	  INDEX ( id_catalog_attribute_option_global_buyer ),
                                          INDEX ( id_catalog_brand ),
                                          INDEX ( id_catalog_attribute_option_global_category ),
                                          INDEX ( id_catalog_attribute_option_global_sub_category ),
									                        INDEX ( id_catalog_attribute_option_global_sub_sub_category ),

                                          INDEX (	OldCat1 )
 										                     )
SELECT 
   bob_live_co.catalog_config.id_catalog_config,
   bob_live_co.catalog_simple.id_catalog_simple,
   bob_live_co.catalog_simple.sku AS sku_simple, 
   bob_live_co.catalog_config.sku AS sku_config,
   bob_live_co.catalog_config.name as sku_name,

   bob_live_co.catalog_config.fk_catalog_attribute_option_global_category AS id_catalog_attribute_option_global_category,
   SPACE(150) AS Cat1,
   bob_live_co.catalog_config.fk_catalog_attribute_option_global_sub_category AS id_catalog_attribute_option_global_sub_category,
   SPACE(150) AS Cat2,
   bob_live_co.catalog_config.fk_catalog_attribute_option_global_sub_sub_category AS id_catalog_attribute_option_global_sub_sub_category,
   SPACE(150) AS Cat3,

   SPACE(150) AS Cat_KPI,
   SPACE(150) AS Cat_BP,
   SPACE(150) AS OldCat1, 
   SPACE(150) AS OldCat2,
   SPACE(150) AS OldCat3, 
   bob_live_co.catalog_config.product_weight         AS Product_weight, 
   bob_live_co.catalog_config.package_weight          AS Package_Weight,
   bob_live_co.catalog_config.package_height AS Package_height,
   bob_live_co.catalog_config.package_length AS Package_length,
   bob_live_co.catalog_config.package_width AS Package_width,

   bob_live_co.catalog_config.fk_supplier AS id_catalog_supplier,
   SPACE(150) AS Supplier,
 #  bob_live_co.catalog_config.fk_catalog_attribute_option_global_head_buyer_name AS id_catalog_attribute_option_global_head_buyer_name   ,
#   SPACE(150) AS Head_Buyer,
   bob_live_co.catalog_config.fk_catalog_attribute_option_global_buyer AS id_catalog_attribute_option_global_buyer,
   SPACE(150) AS Buyer,   

   bob_live_co.catalog_config.fk_catalog_brand  AS id_catalog_brand,
   SPACE(150) AS Brand,
   bob_live_co.catalog_simple.cost, 
   bob_live_co.catalog_simple.delivery_cost_supplier, 
   bob_live_co.catalog_simple.eligible_free_shipping,
   bob_live_co.catalog_simple.original_price,
   if( bob_live_co.catalog_config.status = "active" , 1 , 0 ) as isActive_SKUConfig,
   if( bob_live_co.catalog_simple.status = "active" , 1 , 0 ) as isActive_SKUSimple,
   0    AS isVisible,
   0    AS isMarketPlace,
   "0000-00-00" AS isMarketPlace_Since,
   "0000-00-00" AS isMarketPlace_Until,
   bob_live_co.catalog_config.created_at as created_at_Config,
   bob_live_co.catalog_simple.created_at as created_at_Simple

FROM 
            bob_live_co.catalog_simple 
	LEFT JOIN bob_live_co.catalog_config 
	       ON bob_live_co.catalog_simple.fk_catalog_config = bob_live_co.catalog_config.id_catalog_config;


/*
* SKU Visible
*/
use bob_live_co;
DROP TEMPORARY TABLE IF EXISTS development_co.TMP_STOCK_AVAILABLE;
CREATE TEMPORARY TABLE development_co.TMP_STOCK_AVAILABLE ( INDEX ( sku ) )
SELECT 
   `catalog_simple`.`sku`, 
   IFNULL(( SELECT CAST(quantity AS SIGNED INT) FROM catalog_warehouse_stock WHERE catalog_warehouse_stock.fk_catalog_simple = catalog_simple.id_catalog_simple),0) AS `catalog_warehouse_stock`, 
   IFNULL(( SELECT CAST(quantity AS SIGNED INT) FROM catalog_supplier_stock  WHERE catalog_supplier_stock.fk_catalog_simple = catalog_simple.id_catalog_simple),0)  AS `catalog_supplier_stock`, 
   (( SELECT COUNT(*) FROM sales_order_item JOIN sales_order ON fk_sales_order = id_sales_order WHERE     fk_sales_order_process IN (33,5,27,26,31,8,23,28,32,34,29,9,30) AND is_reserved = 1               AND sales_order_item.sku = catalog_simple.sku) + IFNULL((SELECT SUM(catalog_stock_shared.reserved) FROM catalog_stock_shared WHERE catalog_stock_shared.sku = catalog_simple.sku GROUP BY catalog_stock_shared.sku), 0)) AS `item_reserved`, 
   GREATEST((IFNULL((SELECT CAST(quantity AS SIGNED INT) FROM catalog_stock WHERE catalog_stock.fk_catalog_simple = catalog_simple.id_catalog_simple),0) - ( (SELECT COUNT(*) FROM sales_order_item JOIN sales_order ON fk_sales_order = id_sales_order WHERE fk_sales_order_process IN (33,5,27,26,31,8,23,28,32,34,29,9,30) AND is_reserved = 1 AND sales_order_item.sku = catalog_simple.sku) + IFNULL((SELECT SUM(catalog_stock_shared.reserved) FROM catalog_stock_shared WHERE catalog_stock_shared.sku = catalog_simple.sku GROUP BY catalog_stock_shared.sku), 0))),0) AS `stock_available` 
FROM 
   bob_live_co.`catalog_simple` 
ORDER BY `catalog_simple`.`id_catalog_simple` DESC
;
DROP TABLE IF EXISTS development_co.A_E_BI_SkuVisibles;
CREATE TABLE  development_co.A_E_BI_SkuVisibles ( KEY ( sku_config ) )
SELECT
   bob_live_co.catalog_config.sku AS sku_config,
   bob_live_co.catalog_simple.sku AS sku_simple,
   bob_live_co.catalog_config.`status` AS status_config,
   bob_live_co.catalog_simple.`status` AS status_simple,
   space(50) as Category_BP,
   space(50) as Cat1,
   space(50) as Cat2,
   space(50) as Cat3,
   bob_live_co.catalog_config.pet_status,
   development_co.TMP_STOCK_AVAILABLE.stock_available
  
FROM
           bob_live_co.catalog_config
INNER JOIN bob_live_co.catalog_simple 
        ON bob_live_co.catalog_simple.fk_catalog_config = bob_live_co.catalog_config.id_catalog_config
 LEFT JOIN development_co.TMP_STOCK_AVAILABLE
        ON bob_live_co.catalog_simple.sku = development_co.TMP_STOCK_AVAILABLE.sku
WHERE
   bob_live_co.catalog_config.`status` LIKE 'active' AND
   bob_live_co.catalog_simple.`status` LIKE 'active' AND
   bob_live_co.catalog_config.pet_approved = 1 AND
   bob_live_co.catalog_config.pet_status LIKE 'creation,edited,images' AND
   (
       development_co.TMP_STOCK_AVAILABLE.stock_available > 0 
    OR bob_live_co.catalog_config.display_if_out_of_stock = 1
   )
;
use development_co;
UPDATE           development_co.A_Master_Catalog  
      INNER JOIN development_co.A_E_BI_SkuVisibles
           USING ( sku_simple )
SET
   development_co.A_Master_Catalog.isVisible = 1
;

/*
*  UPDATE A_Master_Catalog.OldCat1
*  UPDATE A_Master_Catalog.OldCat2
*  UPDATE A_Master_Catalog.OldCat3
*  UPDATE A_Master_Catalog.Product_Weight
*   UPDATE A_Master_Catalog.Packaget_Weight
*/
UPDATE            development_co.A_Master_Catalog
       INNER JOIN development_mx.A_E_Pro_CategoryTree_No_Duplicates 
               ON SKU_Config = A_E_Pro_CategoryTree_No_Duplicates.sku 
SET 
    development_co.A_Master_Catalog.OldCat1 = A_E_Pro_CategoryTree_No_Duplicates.FirstOfCat1, 
    development_co.A_Master_Catalog.OldCat2 = A_E_Pro_CategoryTree_No_Duplicates.FirstOfCat2, 
    development_co.A_Master_Catalog.OldCat3 = A_E_Pro_CategoryTree_No_Duplicates.FirstOfCat3; 
   
 #Query: A 104 U Category 1-2-3
UPDATE           development_co.A_Master_Catalog
      INNER JOIN development_mx.A_E_Pro_CategoryTree_No_Duplicates 
              ON development_co.A_Master_Catalog.SKU_Config = A_E_Pro_CategoryTree_No_Duplicates.sku
SET
    A_Master_Catalog.Cat1  = A_E_Pro_CategoryTree_No_Duplicates.FirstOfCat1, 
    A_Master_Catalog.Cat2  = A_E_Pro_CategoryTree_No_Duplicates.FirstOfCat2, 
    A_Master_Catalog.Cat3  = A_E_Pro_CategoryTree_No_Duplicates.FirstOfCat3,
    A_Master_Catalog.Product_weight = A_E_Pro_CategoryTree_No_Duplicates.Product_Weight,
    A_Master_Catalog.Package_Weight = A_E_Pro_CategoryTree_No_Duplicates.Package_Weight

; 
UPDATE           development_co.A_Master_Catalog
      INNER JOIN development_mx.A_E_Category_BoB_BP 
              ON development_co.A_Master_Catalog.Cat1 = A_E_Category_BoB_BP.Cat_1_BoB 
SET 
    A_Master_Catalog.Cat_BP = A_E_Category_BoB_BP.Category_BP
;

UPDATE             development_mx.A_U_Package_Weight_No_Nulls 
        INNER JOIN development_co.A_Master_Catalog
		            ON A_U_Package_Weight_No_Nulls.SKU_Config = development_co.A_Master_Catalog.SKU_Config 
SET 
   development_co.A_Master_Catalog.Package_Weight = A_U_Package_Weight_No_Nulls.Package_Weight;



/*
*   UPDATE A_Master_Catalog.Cat1 A_Master_Catalog.Cat2 A_Master_Catalog.Cat3
*/
UPDATE           development_co.A_Master_Catalog		   
			INNER JOIN bob_live_co.catalog_attribute_option_global_category  
					 USING ( id_catalog_attribute_option_global_category )
SET
   development_co.A_Master_Catalog.Cat1 = bob_live_co.catalog_attribute_option_global_category.name;

UPDATE           development_co.A_Master_Catalog		   
			INNER JOIN bob_live_co.catalog_attribute_option_global_sub_category  
					 USING ( id_catalog_attribute_option_global_sub_category )
SET
   development_co.A_Master_Catalog.Cat2 = bob_live_co.catalog_attribute_option_global_sub_category.name;

UPDATE           development_co.A_Master_Catalog		   
			INNER JOIN bob_live_co.catalog_attribute_option_global_sub_sub_category  
					 USING ( id_catalog_attribute_option_global_sub_sub_category )
SET
   development_co.A_Master_Catalog.Cat3 = bob_live_co.catalog_attribute_option_global_sub_sub_category.name;

/*
*   UPDATE A_Master_Catalog.Cat_BP
*/
UPDATE           development_co.A_Master_Catalog 
      INNER JOIN development_mx.A_E_M1_New_CategoryBP 
              ON development_co.A_Master_Catalog.Cat1 = A_E_M1_New_CategoryBP.Cat1 
SET 
    development_co.A_Master_Catalog.Cat_BP = A_E_M1_New_CategoryBP.CatBP;

 #Query: M1_CategoryBP2
UPDATE           development_co.A_Master_Catalog 
       INNER JOIN development_mx.A_E_M1_New_CategoryBP 
               ON development_co.A_Master_Catalog.Cat2 = A_E_M1_New_CategoryBP.Cat2 
SET 
    development_co.A_Master_Catalog.Cat_BP = A_E_M1_New_CategoryBP.CatBP
WHERE 
    development_co.A_Master_Catalog.Cat1="Entretenimiento" 
 Or development_co.A_Master_Catalog.Cat1 LIKE "Electr%nicos";




/*	
*   UPDATE A_Master_Catalog.Cap_KPI
*/
UPDATE           development_co.A_Master_Catalog		   
			INNER JOIN development_mx.M_CategoryKPI 
					    ON development_co.A_Master_Catalog.Cat_BP =  development_mx.M_CategoryKPI.CatBP
SET
   development_co.A_Master_Catalog.Cat_KPI = development_mx.M_CategoryKPI.CatKPI;





/*
*   UPDATE A_Master_Catalog.Brand
*/
UPDATE          development_co.A_Master_Catalog		   
	   INNER JOIN bob_live_co.catalog_brand  
	        USING ( id_catalog_brand )
SET
    development_co.A_Master_Catalog.Brand = bob_live_co.catalog_brand.name_en;   			


/*
*   UPDATE A_Master_Catalog.buyer 
*/		   
UPDATE          development_co.A_Master_Catalog		   
	   INNER JOIN bob_live_co.catalog_attribute_option_global_buyer
	        USING ( id_catalog_attribute_option_global_buyer )
SET
    development_co.A_Master_Catalog.Buyer = bob_live_co.catalog_attribute_option_global_buyer.name;   			

/*
*   UPDATE A_Master_Catalog.head_buyer 
*		   
UPDATE          development_co.A_Master_Catalog		   
	   INNER JOIN bob_live_co.catalog_attribute_option_global_head_buyer_name
	        USING ( id_catalog_attribute_option_global_head_buyer_name )
SET
    development_co.A_Master_Catalog.Head_Buyer = bob_live_co.catalog_attribute_option_global_head_buyer_name.name;   			
*/	   
/*
* UPDATE A_Master_Catalog.supplier
*/ 		   
UPDATE            development_co.A_Master_Catalog
       INNER JOIN bob_live_co.catalog_supplier 
	          USING ( id_catalog_supplier )
SET
    development_co.A_Master_Catalog.Supplier = bob_live_co.catalog_supplier.name_en
;
UPDATE            development_co.A_Master_Catalog
       INNER JOIN bob_live_co.supplier 
	             ON development_co.A_Master_Catalog.id_catalog_supplier = bob_live_co.supplier.id_supplier
SET
    development_co.A_Master_Catalog.Supplier = bob_live_co.supplier.name
;


