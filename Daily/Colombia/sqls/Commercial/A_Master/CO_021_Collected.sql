DROP   TEMPORARY TABLE IF EXISTS A_S_BI_Delivered;
CREATE TEMPORARY TABLE A_S_BI_Delivered ( INDEX ( fk_sales_order_item  ) )
SELECT
 ItemId AS fk_sales_order_item,
 DateDelivered as created_at
FROM
   development_co.A_Master_Sample
WHERE
    development_co.A_Master_Sample.DateDelivered <> "0000-00-00" 
AND development_co.A_Master_Sample.DateDelivered is not null
GROUP BY ItemId 
;
UPDATE           A_S_BI_Delivered
      INNER JOIN development_co.A_Master_Sample
              ON A_S_BI_Delivered.fk_sales_order_item = development_co.A_Master_Sample.ItemID 
SET
    development_co.A_Master_Sample.DateCollected = A_S_BI_Delivered.created_at,
    development_co.A_Master_Sample.Collected = 1
;
DROP   TEMPORARY TABLE IF EXISTS A_S_BI_Delivered;
CREATE TEMPORARY TABLE A_S_BI_Delivered ( INDEX ( fk_sales_order_item  ) )
SELECT
 fk_sales_order_item,
 created_at
FROM
    bob_live_co.sales_order_item_status_history
WHERE
   bob_live_co.sales_order_item_status_history.fk_sales_order_item_status IN ( 68 )
GROUP BY fk_sales_order_item
;

UPDATE           A_S_BI_Delivered
      INNER JOIN development_co.A_Master_Sample
              ON A_S_BI_Delivered.fk_sales_order_item = development_co.A_Master_Sample.ItemID 
SET
    development_co.A_Master_Sample.DateCollected = A_S_BI_Delivered.created_at,
    development_co.A_Master_Sample.Collected = 1
WHERE
    PaymentMethod in ( "CreditCardOnDelivery_Payment" ,
                       "CashOnDelivery_Payment"  )
;

DROP   TEMPORARY TABLE IF EXISTS A_S_BI_Exportable;
CREATE TEMPORARY TABLE A_S_BI_Exportable ( INDEX ( fk_sales_order_item  ) )
SELECT
  fk_sales_order_item,
  created_at
FROM
    bob_live_co.sales_order_item_status_history
WHERE
   bob_live_co.sales_order_item_status_history.fk_sales_order_item_status IN ( 3 )
GROUP BY fk_sales_order_item
;


UPDATE           A_S_BI_Exportable
      INNER JOIN development_co.A_Master_Sample
              ON A_S_BI_Exportable.fk_sales_order_item = development_co.A_Master_Sample.ItemID 
SET

    development_co.A_Master_Sample.DateCollected = A_S_BI_Exportable.created_at,
    development_co.A_Master_Sample.Collected = 1
WHERE
    PaymentMethod in ( "Amex_Gateway" ,
                        "Banorte_Payworks",
                        "Banorte_Payworks_Debit",
                        "Paypal_Express_Checkout",
                        "Oxxo_Direct",
                        "Banorte_PagoReferenciado",
                        "Bancomer_PagoReferenciado",
                        /*Old Collected*/
                        'Adquira_Interredes', 
                        'Amex_Gateway', 
                        'Bancomer_PagoReferenciado', 
                        'Banorte_PagoReferenciado', 
                        'Banorte_Payworks', 
                        'Banorte_Payworks_Debit', 
                        'DineroMail_Api', 
                        'DineroMail_HostedPaymentPage', 
                        'Oxxo_Direct', 
                        'Paypal_Express_Checkout'
                       )
;

DROP   TEMPORARY TABLE IF EXISTS A_S_BI_Manual_Fraud_Check_Pending;
CREATE TEMPORARY TABLE A_S_BI_Manual_Fraud_Check_Pending ( INDEX ( fk_sales_order_item  ) )
SELECT
 *
FROM
    bob_live_co.sales_order_item_status_history
WHERE
   bob_live_co.sales_order_item_status_history.fk_sales_order_item_status IN ( 122 )
GROUP BY fk_sales_order_item
;

UPDATE           A_S_BI_Manual_Fraud_Check_Pending
      INNER JOIN development_co.A_Master_Sample
              ON A_S_BI_Manual_Fraud_Check_Pending.fk_sales_order_item = development_co.A_Master_Sample.ItemID 
SET
    development_co.A_Master_Sample.DateCollected = A_S_BI_Manual_Fraud_Check_Pending.created_at,
    development_co.A_Master_Sample.Collected = 1
WHERE
    PaymentMethod in (   "Amex_Gateway" 
                        , "Banorte_Payworks"
#                       , "Banorte_Payworks_Debit"
                        'Amex_Gateway', 
                        'Banorte_Payworks', 
                        'Banorte_Payworks_Debit', 
                        'DineroMail_Api', 
                        'DineroMail_HostedPaymentPage'
                       )
;


UPDATE           A_S_BI_Manual_Fraud_Check_Pending
      INNER JOIN development_co.A_Master_Sample
              ON A_S_BI_Manual_Fraud_Check_Pending.fk_sales_order_item = development_co.A_Master_Sample.ItemID 
SET
    development_co.A_Master_Sample.DateCollected = A_S_BI_Manual_Fraud_Check_Pending.created_at,
    development_co.A_Master_Sample.Collected = 1
WHERE
    PaymentMethod in (   "Amex_Gateway" ,
                         "Banorte_Payworks"
#                       , "Banorte_Payworks_Debit"
                         'Amex_Gateway', 
                         'Banorte_Payworks', 
                         'Banorte_Payworks_Debit', 
                         'DineroMail_Api', 
                         'DineroMail_HostedPaymentPage'
                         'DineroMail_HostedPaymentPage'
                       )
;



DROP   TEMPORARY TABLE IF EXISTS A_S_BI_Auto_Fraud_Check_Pending;
CREATE TEMPORARY TABLE A_S_BI_Auto_Fraud_Check_Pending ( INDEX ( fk_sales_order_item  ) )
SELECT
 *
FROM
    bob_live_co.sales_order_item_status_history
WHERE
   bob_live_co.sales_order_item_status_history.fk_sales_order_item_status IN ( 120 )
GROUP BY fk_sales_order_item
;

UPDATE           A_S_BI_Auto_Fraud_Check_Pending
      INNER JOIN development_co.A_Master_Sample
              ON A_S_BI_Auto_Fraud_Check_Pending.fk_sales_order_item = development_co.A_Master_Sample.ItemID 
SET
    development_co.A_Master_Sample.DateCollected = A_S_BI_Auto_Fraud_Check_Pending.created_at,
    development_co.A_Master_Sample.Collected = 1
WHERE
    PaymentMethod in (   "Amex_Gateway" 
                        , "Banorte_Payworks"
#                       , "Banorte_Payworks_Debit"
                        'Amex_Gateway', 
                        'Banorte_Payworks', 
                        'Banorte_Payworks_Debit', 
                        'DineroMail_Api', 
                        'DineroMail_HostedPaymentPage'
                       )
;



DROP   TEMPORARY TABLE IF EXISTS A_S_BI_Fraud_Check_Pending;
CREATE TEMPORARY TABLE A_S_BI_Fraud_Check_Pending ( INDEX ( fk_sales_order_item  ) )
SELECT
 *
FROM
    bob_live_co.sales_order_item_status_history
WHERE
   bob_live_co.sales_order_item_status_history.fk_sales_order_item_status IN ( 71 )
GROUP BY fk_sales_order_item
;

UPDATE           A_S_BI_Fraud_Check_Pending
      INNER JOIN development_co.A_Master_Sample
              ON A_S_BI_Fraud_Check_Pending.fk_sales_order_item = development_co.A_Master_Sample.ItemID 
SET
    development_co.A_Master_Sample.DateCollected = A_S_BI_Fraud_Check_Pending.created_at,
    development_co.A_Master_Sample.Collected = 1
WHERE
    PaymentMethod in (   "Amex_Gateway" 
                        , "Banorte_Payworks"
#                       , "Banorte_Payworks_Debit"
                        'Amex_Gateway', 
                        'Banorte_Payworks', 
                        'Banorte_Payworks_Debit', 
                        'DineroMail_Api', 
                        'DineroMail_HostedPaymentPage'
                       )
;


