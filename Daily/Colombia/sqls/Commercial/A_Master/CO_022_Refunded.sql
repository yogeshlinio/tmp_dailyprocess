DROP   TEMPORARY TABLE IF EXISTS A_S_BI_Refunded;
CREATE TEMPORARY TABLE A_S_BI_Refunded ( INDEX ( fk_sales_order_item  ) )
SELECT
 *
FROM
    bob_live_co.sales_order_item_status_history
WHERE
   bob_live_co.sales_order_item_status_history.fk_sales_order_item_status IN ( 127 )
GROUP BY fk_sales_order_item
;

UPDATE development_co.A_Master_Sample SET Refunded = 1 where Status = "refunded";


UPDATE           A_S_BI_Refunded
      INNER JOIN development_co.A_Master_Sample
              ON A_S_BI_Refunded.fk_sales_order_item = development_co.A_Master_Sample.ItemID 
SET
    development_co.A_Master_Sample.DateRefunded = A_S_BI_Refunded.created_at
WHERE
    development_co.A_Master_Sample.Refunded = 1
;
