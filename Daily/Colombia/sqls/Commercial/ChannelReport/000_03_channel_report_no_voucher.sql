
update development_co.ga_cost_campaign set type = 'liniofashion' where source like 'face%fashion' and type is null;
update development_co.ga_cost_campaign set type = 'linio' where source = 'facebook' and type is null;

update development_co.ga_visits_cost_source_medium set source_medium = concat( source, ' / ', medium) where source_medium is null;
update development_co.ga_cost_campaign set source_medium = concat( source, ' / ', medium) where source_medium is null;


update development_co.ga_visits_cost_source_medium SET channel='Referral Sites' WHERE source_medium like '%referral'
AND (source_medium not like 'google%' OR source_medium not like 'linio%' OR source_medium not like 'facebook%' OR source_medium<>'linio.com.co / referral' 
OR source_medium<>'facebook / retargeting');

update development_co.ga_visits_cost_source_medium SET channel='Buscape' WHERE source_medium like '%buscape%';


update development_co.ga_visits_cost_source_medium SET channel='Pampa Network' WHERE source_medium like 'pampa%';

update development_co.ga_visits_cost_source_medium SET channel='SEM Branded' WHERE source_medium='google / cpc' AND campaign like '%linio%';
update development_co.ga_visits_cost_source_medium SET channel='SEM (unbranded)' WHERE source_medium='google / cpc' AND campaign not like '%linio%';


update development_co.ga_visits_cost_source_medium SET channel='Google Display Network' WHERE source_medium='google / cpc' AND 
campaign like 'r.%' or campaign like '[D%';

update development_co.ga_visits_cost_source_medium SET channel='Facebook R.' WHERE source_medium='facebook / retargeting';

update development_co.ga_visits_cost_source_medium SET channel='Sociomantic' WHERE source_medium like 'socioma%';
update development_co.ga_visits_cost_source_medium SET channel='Vizury' WHERE source_medium like '%vizury%';
update development_co.ga_visits_cost_source_medium SET channel='VEInteractive' WHERE source_medium like '%VEInteractive%';

update development_co.ga_visits_cost_source_medium SET channel='Mercado Libre Referral' WHERE source_medium='mercado%referral';
update development_co.ga_visits_cost_source_medium SET channel='Mercado Libre Voucher' WHERE source_medium like 'ML%';

update development_co.ga_visits_cost_source_medium SET channel='Facebook Referral' WHERE 
(source_medium like '%book%referral' OR source_medium='facebook.com / referral') AND 
(source_medium<>'facebook / socialmediaads' and source_medium<>'facebook / retargeting');
update development_co.ga_visits_cost_source_medium SET channel='Muy Feliz' WHERE source_medium like 'MUYFELIZ%';

update development_co.ga_visits_cost_source_medium SET channel='Twitter' 
WHERE source_medium='t.co / referral' OR source_medium='socialmedia / smtweet' OR source_medium like 'Twitter%' OR source_medium like 'TW%';
update development_co.ga_visits_cost_source_medium SET channel='FB Posts' WHERE (source_medium like '%facebook%' OR (source_medium like 'FB%' AND source_medium not like 'FBCAC%')
OR source_medium='faebook / socialmedia') AND (source_medium<>'facebook / socialmediaads' and source_medium<>'facebook / retargeting'); 
update development_co.ga_visits_cost_source_medium SET channel='FB CAC' 
WHERE source_medium like 'FBCAC%';

update development_co.ga_visits_cost_source_medium SET channel='Facebook Ads' WHERE source_medium ='facebook / socialmediaads' and source_medium<>'facebook / retargeting';
update development_co.ga_visits_cost_source_medium SET channel='Alejandra Arce Voucher' WHERE source_medium like 'voucher5%' OR source_medium like 'voucher2%';

update development_co.ga_visits_cost_source_medium SET channel='Facebook Retargeting' WHERE source_medium ='facebook / retargeting';

update development_co.ga_visits_cost_source_medium SET channel='UpSell' WHERE  source_medium like 'UP%' OR source_medium like '%UP%' OR 
(source_medium='CS / call' AND campaign like 'ups%' );
update development_co.ga_visits_cost_source_medium SET channel='Invalids' WHERE (source_medium='CS / call' AND campaign like 'INVALID%') or source_medium='CS / invalid';
update development_co.ga_visits_cost_source_medium SET channel='Reactivation' WHERE (source_medium='CS / call' AND campaign like 'reactivation%') OR source_medium like 'ACTIVA%' 
OR source_medium='CS / Reactivation';
update development_co.ga_visits_cost_source_medium SET channel='Inbound' WHERE (source_medium='CS / call' AND campaign like 'inbou%' ) OR source_medium='CS / Inbound' OR 
source_medium='out%' OR source_medium like 'Telesales%';
update development_co.ga_visits_cost_source_medium SET channel='OutBound' WHERE source_medium='CS / OutBound' OR source_medium='CS / out' OR source_medium like 'CV%' OR (source_medium='CS / call' AND campaign like 'outbou%');
update development_co.ga_visits_cost_source_medium SET channel='CrossSales' WHERE source_medium='CS / CrossSale' OR source_medium='CS / cross_sell';
update development_co.ga_visits_cost_source_medium SET channel='Replace' WHERE source_medium like '%replace%';
update development_co.ga_visits_cost_source_medium SET channel='ReOrder' WHERE source_medium='CS / reorder';
update development_co.ga_visits_cost_source_medium SET channel='Invalid-Crossell' WHERE source_medium='CS / invalidcrossell';


update development_co.ga_visits_cost_source_medium SET channel='Search Organic' WHERE source_medium like '%organic' OR source_medium='google.com.co / referral';

update development_co.ga_visits_cost_source_medium set channel='Newsletter' WHERE (source_medium='Postal / CRM' 
OR source_medium='Email Marketing / CRM' OR source_medium='EMM / CRM' 
OR source_medium='Postal / (not set)') or (source_medium like '%CRM%'and source_medium not like 'VEInteractive%') 
OR (source_medium like 'LOCO%' and source_medium is not null and source_medium not like '%blog%');


update development_co.ga_visits_cost_source_medium set channel='Voucher Navidad' WHERE source_medium like 'NAVIDAD%' 
OR source_medium like 'LINIONAVIDAD%'; 
update development_co.ga_visits_cost_source_medium set channel='Voucher Fin de Año' where source_medium='FINDEAÑO31';
update development_co.ga_visits_cost_source_medium set channel='Bandeja CAC' where source_medium='BANDEJA0203';
update development_co.ga_visits_cost_source_medium set channel='Tapete CAC' where source_medium like 'TAPETE%';
update development_co.ga_visits_cost_source_medium set channel = 'Perfumes CAC' where source_medium like 'PERFUME%';
update development_co.ga_visits_cost_source_medium set channel = 'VoucherBebes-Feb' where source_medium like 'BEBES%';

update development_co.ga_visits_cost_source_medium SET channel='Blog Linio' WHERE source_medium like 'blog%' OR source_medium='blog.linio.com.co / referral' OR source_medium='BLS12C';
update development_co.ga_visits_cost_source_medium SET channel='Blog Mujer' WHERE source_medium='mujer.linio.com.co / referral';

update development_co.ga_visits_cost_source_medium SET channel='Reactivation Letters' WHERE (source_medium like 'REACT%' OR source_medium like 'CLIENTVIP%' OR source_medium like 'LINIOVIP%' OR source_medium='VUELVE' OR source_medium like 'CLIENTEVIP%') AND source_medium<>'CLIENTEVIP1';
update development_co.ga_visits_cost_source_medium SET channel='Customer Adquisition Letters' WHERE  source_medium like 'CONOCE%' OR source_medium='CLIENTEVIP1';
update development_co.ga_visits_cost_source_medium SET channel='Internal Sells' WHERE source_medium like 'INTER%';
update development_co.ga_visits_cost_source_medium SET channel='Employee Vouchers' WHERE source_medium like 'OCT%' OR source_medium like 'SEP%';
update development_co.ga_visits_cost_source_medium SET channel='Parcel Vouchers' WHERE  source_medium like 'DESCUENTO%' OR source_medium='LINIO0KO' OR source_medium='LINIOiju';
update development_co.ga_visits_cost_source_medium SET channel='New Register' WHERE  source_medium like 'NL%' OR source_medium like 'NR%';
update development_co.ga_visits_cost_source_medium SET channel='Disculpas' WHERE source_medium like 'H1yJvf' OR source_medium like 'H87h7C' OR source_medium='KI025EL78LLXLACOL-25324'
OR source_medium='KI025EL85GRQLACOL-4516';
update development_co.ga_visits_cost_source_medium SET channel='Suppliers Email' WHERE source_medium='suppliers_mail / email';
update development_co.ga_visits_cost_source_medium SET channel='Out Of Stock' WHERE source_medium like 'NST%' OR source_medium='N0Ctor';
update development_co.ga_visits_cost_source_medium SET channel='Devoluciones' WHERE source_medium like 'DEV%';
update development_co.ga_visits_cost_source_medium SET channel='Broken Vouchers' WHERE source_medium like 'HVE%';
update development_co.ga_visits_cost_source_medium SET channel='Broken Discount' WHERE source_medium  like  'KER%' OR source_medium='D1UqWU';
update development_co.ga_visits_cost_source_medium SET channel='Voucher Empleados' WHERE source_medium like 'EMPLEADO%';
update development_co.ga_visits_cost_source_medium SET channel='Gift Cards' WHERE source_medium like 'GC%';
update development_co.ga_visits_cost_source_medium SET channel='Friends and Family' WHERE source_medium like 'FF%';
update development_co.ga_visits_cost_source_medium SET channel='Leads' WHERE source_medium like 'LC%';


update development_co.ga_visits_cost_source_medium SET channel='Banco Helm' WHERE  source_medium like  'HELM%';
update development_co.ga_visits_cost_source_medium SET channel='Banco de Bogotá' WHERE  source_medium like 'CLIENTEBOG%' OR source_medium like 'MOVISTAR%' 
OR source_medium like 'BOG%' OR source_medium like 'BDB%' OR source_medium='BB42' OR source_medium='BB46' OR source_medium='BBLAV' OR source_medium='BBNEV' OR
source_medium='PA953EL96NHFLACOL' OR
source_medium='PA953EL96NHFLACOL' OR
source_medium='BH859EL51WBWLACOL' OR
source_medium='CU054HL58AXPLACOL' OR
source_medium='SA860TB24WCXLACOL' OR
source_medium='HA997TB17DLQLACOL' OR
source_medium='SA860TB35WCMLACOL' OR
source_medium='SA860TB37WCKLACOL' OR
source_medium='BO944TB20JEPLACOL' OR
source_medium='MG614TB23HTWLACOL' OR
source_medium='BA186TB36HTJLACOL' OR
source_medium='PR506TB63BFCLACOL' OR
source_medium='MI392TB32VRBLACOL' OR
source_medium='MI392TB33VRALACOL' OR
source_medium='BO944TB79JCILACOL' OR
source_medium='BO944TB24JELLACOL' OR
source_medium='HA997TB23DLKLACOL';

update development_co.ga_visits_cost_source_medium SET channel='Banco Davivienda' WHERE  source_medium like 'DAV%' OR source_medium='DV_AG' OR source_medium='DVPLANCHA46';
update development_co.ga_visits_cost_source_medium SET channel='Banco BBVA' WHERE  source_medium like 'BBVA%' OR
source_medium='SA860TB32WCPLACOL' OR
source_medium='SA860TB43WCELACOL' OR
source_medium='BA186TB35HTKLACOL' OR
source_medium='PR506TB55BFKLACOL' OR
source_medium='PR506TB62BFDLACOL' OR
source_medium='PA953EL97NHELACOL' OR
source_medium='PO916EL07ETSLACOL' OR
source_medium='PO916EL04ETVLACOL' OR
source_medium='LU939HL23XSILACOL' OR
source_medium='LU939HL32XRZLACOL' OR
source_medium='HA997TB15DLSLACOL' OR
source_medium='SA860TB35WCMLACOLACOL';

update development_co.ga_visits_cost_source_medium SET channel='Campus Party' WHERE source_medium like 'CP%';
update development_co.ga_visits_cost_source_medium SET channel='Publimetro' WHERE source_medium='PUBLIMETRO';
update development_co.ga_visits_cost_source_medium SET channel='El Espectador' WHERE source_medium='ESPECT0yx' OR source_medium like 'FIXED%';
update development_co.ga_visits_cost_source_medium SET channel='Grupon' WHERE source_medium like 'GR%';
update development_co.ga_visits_cost_source_medium SET channel='Dafiti' WHERE source_medium='AMIGODAFITI';
update development_co.ga_visits_cost_source_medium SET channel='Sin IVA 16' WHERE source_medium='SINIVA16';
update development_co.ga_visits_cost_source_medium SET channel='En Medio' WHERE source_medium='ENMEDIO';
update development_co.ga_visits_cost_source_medium SET channel='Bancolombia' WHERE source_medium like 'AMEX%' OR
source_medium='ABNSbkpSmM' OR
source_medium='ATR08i1X6D' OR
source_medium='TABAMEX1DA2h' OR source_medium='ATR08i1X6D' OR
source_medium='DEV1sd0c' OR
source_medium='DEVfjFW6' OR
source_medium='DEVbCM2o' OR
source_medium='DEVN6TJN' OR
source_medium='DEVFvwqn' OR
source_medium='DEV0QUfY' OR
source_medium='DEVmhwdB' OR
source_medium='DEV0rCuq' OR
source_medium='DEVlLjUZ' OR
source_medium='DEVzKPuf' OR
source_medium='DEV6IJP6' OR
source_medium='DEV08L8A' OR
source_medium='DEV0Fvg' OR
source_medium='DEVcTttT' OR
source_medium='DEVywRNF' OR
source_medium='DEV0umCc' OR
source_medium='DEVFcXZN' OR
source_medium='AMEXTAB8BSoc';
update development_co.ga_visits_cost_source_medium SET channel='Citi Bank' WHERE  source_medium like 'CITI%';


update development_co.ga_visits_cost_source_medium SET channel='Linio.com Referral' WHERE  source_medium='linio.com / referral' OR source_medium='info.linio.com.co / referral';
update development_co.ga_visits_cost_source_medium SET channel='Directo / Typed' WHERE  source_medium='(direct) / (none)';



update development_co.ga_visits_cost_source_medium SET channel='Referral Program' WHERE source_medium like 'REF%';

update development_co.ga_visits_cost_source_medium set channel='Corporate Sales' WHERE source_medium like'VC%' OR source_medium like 'VCJ%' OR source_medium like 'VCH%'OR source_medium like 'CVE%' 
OR source_medium like 'VCA%' OR source_medium='corporatesales / corporatesales' OR source_medium='BIENVENIDO10';

update development_co.ga_visits_cost_source_medium SET channel='Journalist Vouchers' WHERE source_medium='CAMILOCARM50' OR
source_medium='JUANPABVAR50' OR
source_medium='LAURACHARR50' OR
source_medium='MONICAPARA50' OR
source_medium='DIEGONARVA50' OR
source_medium='LUCYBUENO50' OR
source_medium='DIANALEON50' OR
source_medium='SILVIAPARR50' OR
source_medium='LAURAAYALA50' OR
source_medium='LEONARDONI50' OR
source_medium='CYNTHIARUIZ50' OR
source_medium='JAVIERLEAESC50' OR
source_medium='LORENAPULEC50' OR
source_medium='MARIAISABE50' OR
source_medium='PAOLACALLE50' OR
source_medium='ISABELSALAZ50' OR
source_medium='VICTORSOLA50' OR
source_medium='FABIANRAM50' OR
source_medium='ANDRESROD50' OR
source_medium='HENRYGON50' OR
source_medium='JOHANMA50' OR
source_medium='ALFONSOAYA50' OR
source_medium='VICTORSOLANAV';
update development_co.ga_visits_cost_source_medium SET channel='ADN' WHERE source_medium like'ADN%';
update development_co.ga_visits_cost_source_medium SET channel='Metro' WHERE source_medium like'METRO%';
update development_co.ga_visits_cost_source_medium SET channel='TRANSMILENIO' WHERE source_medium like'TRANSMILENIO%';
update development_co.ga_visits_cost_source_medium SET channel='SMS SanValentin' WHERE source_medium like'SANVALENTIN%';
update development_co.ga_visits_cost_source_medium SET channel='SuperDescuentos' WHERE source_medium like'SUPERDESCUENTOS%';
update development_co.ga_visits_cost_source_medium SET channel='Folletos' WHERE source_medium like'REGALO%';




update development_co.ga_visits_cost_source_medium SET channel='Unknown-No voucher' WHERE source_medium='';
update development_co.ga_visits_cost_source_medium SET channel='Unknown-Voucher' WHERE channel='' AND source_medium<>'';





update development_co.ga_visits_cost_source_medium SET channel_group='Facebook Ads' WHERE channel='Facebook Ads';


update development_co.ga_visits_cost_source_medium SET channel_group='Search Engine Marketing' WHERE channel='SEM (unbranded)';

#Social Media
update development_co.ga_visits_cost_source_medium SET channel_group='Linio Fan Page' WHERE (channel='Facebook Referral' OR channel='Twitter Referral'
OR channel='FB Posts' OR channel='FB-Diana Call') and type = 'linio';

update development_co.ga_visits_cost_source_medium SET channel_group='Fashion Fan Page' WHERE (channel='Facebook Referral' OR channel='Twitter Referral'
OR channel='FB Posts' OR channel='FB-Diana Call') and type = 'liniofashion';


update development_co.ga_visits_cost_source_medium SET channel_group='Retargeting' WHERE channel='Sociomantic' or channel = 'Vizury'
OR channel = 'VEInteractive' or channel = 'GDN Retargeting';


update development_co.ga_visits_cost_source_medium SET channel_group='Google Display Network' WHERE channel like 'GDN'; 


update development_co.ga_visits_cost_source_medium SET channel_group='NewsLetter' WHERE channel='Newsletter'
OR channel='Voucher Navidad' OR channel='Voucher Fin de Año' 
OR channel='Bandeja CAC' OR channel='Tapete CAC' OR channel='Perfumes CAC'
OR channel='VoucherBebes-Feb' OR channel='Sartenes CAC' OR channel='Estufa CAC';


update development_co.ga_visits_cost_source_medium SET channel_group='Search Engine Optimization' WHERE channel='Search Organic';


update development_co.ga_visits_cost_source_medium SET channel_group='Partnerships' WHERE
channel='Banco Helm' OR channel='Banco de Bogotá' OR channel='Banco Davivienda'
OR channel='Banco Davivienda' OR channel='Banco BBVA'
OR channel='Campus Party' OR channel='Publimetro'
OR channel='El Espectador' OR channel='Grupon'
OR channel='Dafiti' OR channel='Sin IVA 16' OR channel='En Medio' OR channel='Bancolombia' OR channel='AMEX'
OR channel='Citi Bank';


update development_co.ga_visits_cost_source_medium SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';


update development_co.ga_visits_cost_source_medium SET channel_group='Offline Marketing' WHERE 
channel='ADN' OR channel='Journalist Vouchers' OR channel='Metro' OR
channel='TRANSMILENIO' OR channel='SMS SanValentin' OR channel='SuperDescuentos' OR
channel='Folletos';


update development_co.ga_visits_cost_source_medium SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales' OR channel='Replace';


update development_co.ga_visits_cost_source_medium SET channel_group='Branded' WHERE channel='Linio.com Referral' 
OR channel='Directo / Typed' OR
channel='SEM Branded';


update development_co.ga_visits_cost_source_medium SET channel_group='Mercado Libre' WHERE channel='Mercado Libre Referral' OR channel='Mercado Libre Voucher' OR channel='Otros - Mercado Libre';


update development_co.ga_visits_cost_source_medium SET channel_group='Blogs' WHERE 
channel='Blog Linio' OR channel='Blog Mujer';


update development_co.ga_visits_cost_source_medium SET channel_group='Affiliates' WHERE channel='Buscape';



update development_co.ga_visits_cost_source_medium SET channel_group='Other (identified)' WHERE channel='Referral Sites' 
OR channel='Reactivation Letters' OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' 
OR channel='Referral Program';

update development_co.ga_visits_cost_source_medium set channel_group = 'Linio Fan Page', channel = 'FB Posts' 
where source_medium = 'facebook' and medium = 'socialmedia';

update development_co.ga_visits_cost_source_medium set channel_group = 'Fashion Fan Page', channel = 'FB Posts' 
where source_medium = 'facebookfashion' and medium = 'socialmedia';

update development_co.ga_visits_cost_source_medium SET channel='Unknown-No voucher' WHERE channel is null;
update development_co.ga_visits_cost_source_medium SET channel_group='Non identified' WHERE channel='Unknown-No voucher' OR channel='Unknown-Voucher' Or channel is null;







update development_co.ga_visits_cost_source_medium SET channel_group='Facebook Ads' WHERE channel='Facebook Ads' OR channel='Alejandra Arce Voucher';


update development_co.ga_visits_cost_source_medium SET channel_group='Search Engine Marketing' WHERE channel='SEM (unbranded)';

#Social Media
update development_co.ga_visits_cost_source_medium SET channel_group='Linio Fan Page' WHERE (channel='Facebook Referral' OR channel='Twitter'
OR channel='FB Posts' OR channel='FB-Diana Call' OR channel='Muy Feliz' or channel='FB CAC') and type = 'linio';

update development_co.ga_visits_cost_source_medium SET channel_group='Fashion Fan Page' WHERE (channel='Facebook Referral' OR channel='Twitter'
OR channel='FB Posts' OR channel='FB-Diana Call' OR channel='Muy Feliz' or channel='FB CAC') and type = 'liniofashion';


update development_co.ga_visits_cost_source_medium SET channel_group='Retargeting' WHERE channel='Sociomantic' OR
channel='Vizury' OR channel = 'VEInteractive' or channel = 'GDN Retargeting' OR channel='Facebook Retargeting';


update development_co.ga_visits_cost_source_medium SET channel_group='Google Display Network' WHERE channel='Google Display Network'; 


update development_co.ga_visits_cost_source_medium SET channel_group='NewsLetter' WHERE channel='Newsletter'
OR channel='Voucher Navidad' OR channel='Voucher Fin de Año' 
OR channel='Bandeja CAC' OR channel='Tapete CAC' OR channel='Perfumes CAC'
OR channel='VoucherBebes-Feb' OR channel='Sartenes CAC' OR channel='Estufa CAC' OR channel='Vuelve CAC' OR
channel='Memoria CAC' OR
channel='Audifonos CAC' OR channel='Venir Reactivation' OR 
channel='Regresa Reactivation' OR 
channel='Bienvenido CAC' OR channel='Amigo CAC' OR channel='Aqui Reactivation' OR
channel='Tiramos la Casa por la ventana' OR channel='Creating Happiness' OR channel='Grita Reactivation' OR
channel='CACs Marzo' OR channel='CACs Abril' OR channel='CACs Mayo' OR channel='CACs Junio' OR channel='CACs Julio'
OR channel='Reactivación Mayo' OR channel='Fashion';





update development_co.ga_visits_cost_source_medium SET channel_group='Search Engine Optimization' WHERE channel='Search Organic';


update development_co.ga_visits_cost_source_medium SET channel_group='Partnerships' WHERE
channel='Banco Helm' OR channel='Banco de Bogotá' OR channel='Banco Davivienda'
OR channel='Banco Davivienda' OR channel='Banco BBVA'
OR channel='Campus Party' OR 
 channel='El Espectador' OR channel='Grupon'
OR channel='Dafiti' OR channel='Sin IVA 16' OR channel='En Medio' OR channel='Bancolombia' OR channel='AMEX'
OR channel='Citi Bank' OR channel='Lenddo' OR channel='Unknown Partnership March' OR 
channel='Post BDB';


update development_co.ga_visits_cost_source_medium SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';


update development_co.ga_visits_cost_source_medium SET channel_group='Offline Marketing' WHERE 
channel='ADN' OR channel='Journalist Vouchers' OR channeL='Metro' OR
channel='TRANSMILENIO' OR channel='SMS SanValentin' OR channel='SuperDescuentos' OR
channel='Folletos' OR channel='MUJER' OR channel='DiaEspecial' OR channel='Hora Loca SMS' OR 
channel='Calle Vouchers' OR channel='SMS' OR channel='BBVADP' OR channel='Other PR' OR channel='Valla Abril'
OR channel='SMS Nueva pagina NC' OR channel='SMS Nueva pagina Costumers' OR channel='SMS Más temprano más barato' OR 
channel='Barranquilla' OR channel='Regresa (React.)' OR channel='Te Esperamos (React.)' OR channel='Gracias (Repurchase)'
OR channel='Falabella Customers' OR channel='Cali' OR channel='CC Avenida Chile' OR channel='Correo Bogota' 
OR channel='Feria Libro' OR channel='Correo directo bogota' OR channel='CEROIVA' OR channel='SMS Madres' or
channel='Circulo de la Moda' OR channel='SMS Mas Barato' or channel='Mailing 2' or channel='Publimetro';



update development_co.ga_visits_cost_source_medium SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales' OR channel='Replace' OR channel='ReOrder' OR channel='Invalid-Crossell';


update development_co.ga_visits_cost_source_medium SET channel_group='Branded' WHERE channel='Linio.com Referral' 
OR channel='Directo / Typed' OR
channel='SEM Branded';


update development_co.ga_visits_cost_source_medium SET channel_group='Mercado Libre' WHERE channel='Mercado Libre Referral' OR channel='Mercado Libre Voucher' OR channel='Otros - Mercado Libre';


update development_co.ga_visits_cost_source_medium SET channel_group='Blogs' WHERE 
channel='Blog Linio' OR channel='Blog Mujer' OR channel='Lector';


update development_co.ga_visits_cost_source_medium SET channel_group='Affiliates' WHERE channel='Buscape';



update development_co.ga_visits_cost_source_medium SET channel_group='Other (identified)' WHERE channel='Referral Sites' 
OR channel='Reactivation Letters' OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' 
OR channel='Referral Program' OR channel='Nueva Página' OR channel='Promesa Linio' OR channel='NO IVA Abril' OR
channel='Invitados VIP Círculo de la moda';



update development_co.ga_visits_cost_source_medium SET channel_group='Affiliates' where channel='Pampa Network';


update development_co.ga_cost_campaign SET channel='Referral Sites' WHERE source_medium like '%referral'
AND (source_medium not like 'google%' OR source_medium not like 'linio%' OR source_medium not like 'facebook%' OR source_medium<>'linio.com.co / referral' 
OR source_medium<>'facebook / retargeting');



update development_co.ga_cost_campaign SET channel='Buscape' WHERE source_medium like '%buscape%';



update development_co.ga_cost_campaign SET channel='Pampa Network' WHERE source_medium like 'pampa%';


update development_co.ga_cost_campaign SET channel='SEM Branded' WHERE source_medium='google / cpc' AND campaign like '%linio%';
update development_co.ga_cost_campaign SET channel='SEM (unbranded)' WHERE source_medium='google / cpc' AND campaign not like '%linio%';



update development_co.ga_cost_campaign SET channel='Google Display Network' WHERE source_medium='google / cpc' AND 
campaign like 'r.%' or campaign like '[D%';


update development_co.ga_cost_campaign SET channel='Facebook R.' WHERE source_medium='facebook / retargeting';


update development_co.ga_cost_campaign SET channel='Sociomantic' WHERE source_medium like 'socioma%';
update development_co.ga_cost_campaign SET channel='Vizury' WHERE source_medium like '%vizury%';
update development_co.ga_cost_campaign SET channel='VEInteractive' WHERE source_medium like '%VEInteractive%';

update development_co.ga_cost_campaign SET channel='Mercado Libre Referral' WHERE source_medium='mercado%referral';
update development_co.ga_cost_campaign SET channel='Mercado Libre Voucher' WHERE source_medium like 'ML%';

update development_co.ga_cost_campaign SET channel='Facebook Referral' WHERE 
(source_medium like '%book%referral' OR source_medium='facebook.com / referral') AND 
(source_medium<>'facebook / socialmediaads' and source_medium<>'facebook / retargeting');
update development_co.ga_cost_campaign SET channel='Muy Feliz' WHERE source_medium like 'MUYFELIZ%';

update development_co.ga_cost_campaign SET channel='Twitter' 
WHERE source_medium='t.co / referral' OR source_medium='socialmedia / smtweet' OR source_medium like 'Twitter%' OR source_medium like 'TW%';
update development_co.ga_cost_campaign SET channel='FB Posts' WHERE (source_medium like '%facebook%' OR (source_medium like 'FB%' AND source_medium not like 'FBCAC%')
OR source_medium='faebook / socialmedia') AND (source_medium<>'facebook / socialmediaads' and source_medium<>'facebook / retargeting'); 
update development_co.ga_cost_campaign SET channel='FB CAC' 
WHERE source_medium like 'FBCAC%';

update development_co.ga_cost_campaign SET channel='Facebook Ads' WHERE source_medium ='facebook / socialmediaads' and source_medium<>'facebook / retargeting';
update development_co.ga_cost_campaign SET channel='Alejandra Arce Voucher' WHERE source_medium like 'voucher5%' OR source_medium like 'voucher2%';

update development_co.ga_cost_campaign SET channel='Facebook Retargeting' WHERE source_medium ='facebook / retargeting';

update development_co.ga_cost_campaign SET channel='UpSell' WHERE  source_medium like 'UP%' OR source_medium like '%UP%' OR 
(source_medium='CS / call' AND campaign like 'ups%' );
update development_co.ga_cost_campaign SET channel='Invalids' WHERE (source_medium='CS / call' AND campaign like 'INVALID%') or source_medium='CS / invalid';
update development_co.ga_cost_campaign SET channel='Reactivation' WHERE (source_medium='CS / call' AND campaign like 'reactivation%') OR source_medium like 'ACTIVA%' 
OR source_medium='CS / Reactivation';
update development_co.ga_cost_campaign SET channel='Inbound' WHERE (source_medium='CS / call' AND campaign like 'inbou%' ) OR source_medium='CS / Inbound' OR 
source_medium='out%' OR source_medium like 'Telesales%';
update development_co.ga_cost_campaign SET channel='OutBound' WHERE source_medium='CS / OutBound' OR source_medium='CS / out' OR source_medium like 'CV%' OR (source_medium='CS / call' AND campaign like 'outbou%');
update development_co.ga_cost_campaign SET channel='CrossSales' WHERE source_medium='CS / CrossSale' OR source_medium='CS / cross_sell';
update development_co.ga_cost_campaign SET channel='Replace' WHERE source_medium like '%replace%';
update development_co.ga_cost_campaign SET channel='ReOrder' WHERE source_medium='CS / reorder';
update development_co.ga_cost_campaign SET channel='Invalid-Crossell' WHERE source_medium='CS / invalidcrossell';


update development_co.ga_cost_campaign SET channel='Search Organic' WHERE source_medium like '%organic' OR source_medium='google.com.co / referral';

update development_co.ga_cost_campaign set channel='Newsletter' WHERE (source_medium='Postal / CRM' 
OR source_medium='Email Marketing / CRM' OR source_medium='EMM / CRM' 
OR source_medium='Postal / (not set)') or (source_medium like '%CRM%'and source_medium not like 'VEInteractive%') 
OR (source_medium like 'LOCO%' and source_medium is not null and source_medium not like '%blog%');

update development_co.ga_cost_campaign SET channel='Blog Linio' WHERE source_medium like 'blog%' 
OR source_medium='BL%' OR source_medium like 'madres%';
update development_co.ga_cost_campaign SET channel='Blog Mujer' WHERE source_medium='mujer.linio.com.co / referral';
update development_co.ga_cost_campaign SET channel='Lector' WHERE source_medium like 'lector%';

update development_co.ga_cost_campaign SET channel='Linio.com Referral' WHERE source_medium='linio.com / referral' 
OR source_medium='info.linio.com.co / referral' OR source_medium='linio.com.co / referral';
update development_co.ga_cost_campaign SET channel='Directo / Typed' WHERE  source_medium='(direct) / (none)';



update development_co.ga_cost_campaign SET channel='Referral Program' WHERE source_medium like 'REF%';

update development_co.ga_cost_campaign set channel='Corporate Sales' WHERE source_medium like'VC%' OR source_medium like 'VCJ%' OR source_medium like 'VCH%'OR source_medium like 'CVE%' 
OR source_medium like 'VCA%' OR source_medium='corporatesales / corporatesales' OR source_medium='BIENVENIDO10';




update development_co.ga_cost_campaign SET channel_group='Facebook Ads' WHERE channel='Facebook Ads' OR channel='Alejandra Arce Voucher';


update development_co.ga_cost_campaign SET channel_group='Search Engine Marketing' WHERE channel='SEM (unbranded)';

#Social Media
update development_co.ga_cost_campaign SET channel_group='Linio Fan Page' WHERE (channel='Facebook Referral' OR channel='Twitter'
OR channel='FB Posts' OR channel='FB-Diana Call' OR channel='Muy Feliz' or channel='FB CAC') and type = 'linio';

update development_co.ga_cost_campaign SET channel_group='Fashion Fan Page' WHERE (channel='Facebook Referral' OR channel='Twitter'
OR channel='FB Posts' OR channel='FB-Diana Call' OR channel='Muy Feliz' or channel='FB CAC') and type = 'liniofashion';


update development_co.ga_cost_campaign SET channel_group='Retargeting' WHERE channel='Sociomantic' OR
channel='Vizury' OR channel = 'VEInteractive' or channel = 'GDN Retargeting' OR channel='Facebook Retargeting';


update development_co.ga_cost_campaign SET channel_group='Google Display Network' WHERE channel='Google Display Network'; 


update development_co.ga_cost_campaign SET channel_group='NewsLetter' WHERE channel='Newsletter'
OR channel='Voucher Navidad' OR channel='Voucher Fin de Año' 
OR channel='Bandeja CAC' OR channel='Tapete CAC' OR channel='Perfumes CAC'
OR channel='VoucherBebes-Feb' OR channel='Sartenes CAC' OR channel='Estufa CAC' OR channel='Vuelve CAC' OR
channel='Memoria CAC' OR
channel='Audifonos CAC' OR channel='Venir Reactivation' OR 
channel='Regresa Reactivation' OR 
channel='Bienvenido CAC' OR channel='Amigo CAC' OR channel='Aqui Reactivation' OR
channel='Tiramos la Casa por la ventana' OR channel='Creating Happiness' OR channel='Grita Reactivation' OR
channel='CACs Marzo' OR channel='CACs Abril' OR channel='CACs Mayo' OR channel='CACs Junio' OR channel='CACs Julio'
OR channel='Reactivación Mayo' OR channel='Fashion';





update development_co.ga_cost_campaign SET channel_group='Search Engine Optimization' WHERE channel='Search Organic';


update development_co.ga_cost_campaign SET channel_group='Partnerships' WHERE
channel='Banco Helm' OR channel='Banco de Bogotá' OR channel='Banco Davivienda'
OR channel='Banco Davivienda' OR channel='Banco BBVA'
OR channel='Campus Party' OR 
 channel='El Espectador' OR channel='Grupon'
OR channel='Dafiti' OR channel='Sin IVA 16' OR channel='En Medio' OR channel='Bancolombia' OR channel='AMEX'
OR channel='Citi Bank' OR channel='Lenddo' OR channel='Unknown Partnership March' OR 
channel='Post BDB';


update development_co.ga_cost_campaign SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';


update development_co.ga_cost_campaign SET channel_group='Offline Marketing' WHERE 
channel='ADN' OR channel='Journalist Vouchers' OR channeL='Metro' OR
channel='TRANSMILENIO' OR channel='SMS SanValentin' OR channel='SuperDescuentos' OR
channel='Folletos' OR channel='MUJER' OR channel='DiaEspecial' OR channel='Hora Loca SMS' OR 
channel='Calle Vouchers' OR channel='SMS' OR channel='BBVADP' OR channel='Other PR' OR channel='Valla Abril'
OR channel='SMS Nueva pagina NC' OR channel='SMS Nueva pagina Costumers' OR channel='SMS Más temprano más barato' OR 
channel='Barranquilla' OR channel='Regresa (React.)' OR channel='Te Esperamos (React.)' OR channel='Gracias (Repurchase)'
OR channel='Falabella Customers' OR channel='Cali' OR channel='CC Avenida Chile' OR channel='Correo Bogota' 
OR channel='Feria Libro' OR channel='Correo directo bogota' OR channel='CEROIVA' OR channel='SMS Madres' or
channel='Circulo de la Moda' OR channel='SMS Mas Barato' or channel='Mailing 2' or channel='Publimetro';



update development_co.ga_cost_campaign SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales' OR channel='Replace' OR channel='ReOrder' OR channel='Invalid-Crossell';


update development_co.ga_cost_campaign SET channel_group='Branded' WHERE channel='Linio.com Referral' 
OR channel='Directo / Typed' OR
channel='SEM Branded';


update development_co.ga_cost_campaign SET channel_group='Mercado Libre' WHERE channel='Mercado Libre Referral' OR channel='Mercado Libre Voucher' OR channel='Otros - Mercado Libre';


update development_co.ga_cost_campaign SET channel_group='Blogs' WHERE 
channel='Blog Linio' OR channel='Blog Mujer' OR channel='Lector';


update development_co.ga_cost_campaign SET channel_group='Affiliates' WHERE channel='Buscape';



update development_co.ga_cost_campaign SET channel_group='Other (identified)' WHERE channel='Referral Sites' 
OR channel='Reactivation Letters' OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' 
OR channel='Referral Program' OR channel='Nueva Página' OR channel='Promesa Linio' OR channel='NO IVA Abril' OR
channel='Invitados VIP Círculo de la moda';


update development_co.ga_cost_campaign SET 
channel_group='Non identified' WHERE channel='Unknown' OR channel='Unknown-Voucher' OR channel='Unknown-GA Tracking';



update development_co.ga_cost_campaign SET channel_group='Affiliates' where channel='Pampa Network';

