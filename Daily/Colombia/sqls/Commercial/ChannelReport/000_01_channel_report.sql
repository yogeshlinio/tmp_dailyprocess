
SELECT 'Start: Channel report', now();
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'development_co.channel report',
  'start',
  NOW(),
  MAX(date),
  count(*),
  count(*)
FROM
  development_co.tbl_order_detail
;

set SQL_SAFE_UPDATES=0;

update tbl_order_detail inner join ga_order on tbl_order_detail.order_nr=ga_order.order_nr 
set `source/medium`= concat(ga_order.source,' / ',ga_order.medium),
tbl_order_detail.campaign=ga_order.campaign where `source/medium` is null;

#Se pone el source medium original para los reorders, replaces, invalids, etc.
update   tbl_order_detail t inner join production_co.tbl_orders_reo_rep_inv o
 on t.coupon_code = o.coupon_code
inner join tbl_order_detail t2 
on t2.order_nr = order_nr_original
set t.`source/medium` =  t2.`source/medium`
where  t.yrmonth >= 201308 and t2.`source/medium` is not null;

update tbl_order_detail set source='', channel='', channel_group='';

update tbl_order_detail set source=coupon_code where coupon_code is not null;

update tbl_order_detail set source=`source/medium` where coupon_code is null;

update tbl_order_detail set channel='New Register' 
where (source like 'NL%' or source like 'NR%') and `source/medium` is null;


update tbl_order_detail set source=`source/medium` 
where
(coupon_code like 'NL%' or 
coupon_code like 'NR%' or 
coupon_code='SINIVA' or 
coupon_code like 'COM%' 
or coupon_code like 'NUEVA%' 
or coupon_code like 'CASA%' 
or coupon_code like 'PAG%' 
or coupon_code='100COMBO-AR238EL90VPTLACOL-15x' 
or coupon_code like 'CERO%' 
or coupon_code like 'MADRID%' 
or coupon_code like '%COMBO-%' 
or coupon_code like 'shilher-esmaltescombo%' 
or coupon_code like 'dev%'
or coupon_code_description like 'Partnerships%'
OR coupon_code like 'VUELVEDINERO%'
or coupon_code like 'DINEROVUELVE%'
or coupon_code like 'HAVA01'
or coupon_code like 'CASHBACK%'
) 
and `source/medium` is not null;

update tbl_order_detail set source=`source/medium` 
where
(coupon_code in (select distinct coupon_code from production_co.tbl_orders_reo_rep_inv ) 
or coupon_code like 'CAC%')
and yrmonth >= 201308
and `source/medium` is not null;



update tbl_order_detail set source=`source/medium` where coupon_code='CAC0726111';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC07262';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC07263';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC07264';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC07265';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC07266';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC07267';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC07268';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC07269';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC072610';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC072611';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC072612';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC072613';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC072614';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC072615';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC072616';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC072617';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC072618';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC072619';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC072620';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC072621';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC072622';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC072623';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC072624';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC072625';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC0726261';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC07301';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC07302';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC07303';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC07304';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC07305';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC07306';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC07307';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC073013';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC07308';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC07309';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC073010';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC073014';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC073015';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC073016';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC073011';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC073017';
update tbl_order_detail set source=`source/medium` where coupon_code='CAC073012';
update tbl_order_detail set source=`source/medium` where coupon_code like 'CAC0809%';


update tbl_order_detail set channel='Referral Sites' where source like '%referral'
and (source not like 'google%' and source not like 'linio%' and source not like 'facebook%' or source<>'linio.com.co / referral' and
 source<>'facebook / retargeting');



update tbl_order_detail set channel='Buscape' where source like '%buscape%';

update tbl_order_detail set channel='Tradetracker' where source like '%tradetracker / Affiliates%';

update tbl_order_detail set channel='Pampa Network' where source like 'pampa%';
update tbl_order_detail set channel='Affiliate Program' where source like 'cbit%';

update tbl_order_detail set channel='SEM Branded' where source='google / cpc' and campaign like '%linio%';
update tbl_order_detail set channel='SEM (unbranded)' where source='google / cpc' and campaign not like '%linio%'
and (campaign not like 'er.%' and campaign not like '[D%' and campaign not like 'r.%');

update tbl_order_detail set channel='Google Display Network' where source='google / cpc' and 
(campaign like 'er.%' or campaign like '[D%' or campaign like 'r.%');

update tbl_order_detail set channel = 'Bing' where source = 'bing / cpc';

update tbl_order_detail set channel='Facebook R.' where source='facebook / retargeting';


update tbl_order_detail set channel='Sociomantic' where source like 'socioma%';
update tbl_order_detail set channel='Vizury' where source like '%vizury%';
update tbl_order_detail set channel='VEInteractive' where source like '%VEInteractive%';

update tbl_order_detail set channel='Mercado Libre' where source='%mercado%referral%' or source like 'ML%';
update tbl_order_detail set channel='Alamaula' where source like 'alamaula%' or source like 'ALA%';



update tbl_order_detail set channel='Twitter' 
where source='t.co / referral' or source='socialmedia / smtweet' or source like 'Twitter%' or source like 'TW%';

update tbl_order_detail 
set channel='Linio Fan Page' 
where 
(source='FASHION25' 
or source like '%facebook%' 
or (source like 'FB%' and source not like 'FBCAC%')
or source='faebook / socialmedia') 
and (source<>'facebook / socialmediaads' and source<>'facebook / retargeting'); 

update tbl_order_detail set channel='FB CAC' 
where source like 'FBCAC%' or source like 'CACFB%';

update tbl_order_detail set channel='FBF CAC' 
where source like 'CACFF%';


update tbl_order_detail set channel='Fashion Fan Page' 
where source like 'FBF%';

update tbl_order_detail set channel = 'Fashion Fan Page' 
where coupon_code like 'HAVA01' and yrmonth = 201307;

update tbl_order_detail set channel='Linio Fan Page' 
where source='facebook / socialmedia' and ((campaign not like '%fashion%' and yrmonth=201306) and campaign is not null);

update tbl_order_detail set channel='FB Dark posts' 
where source='facebook / socialmediaads' and ((campaign like '%darkpost%') and campaign is not null);

update tbl_order_detail set channel='Linio Fan Page' 
where order_nr='200113276';

update tbl_order_detail set channel='Fashion Fan Page' 
where source='facebook / socialmedia' and campaign like '%fashion%' and yrmonth <= 201307;

update tbl_order_detail set channel='Linio Fan Page' 
where source='facebook / socialmedia' and yrmonth >= 201308;


update tbl_order_detail set channel='Fashion Fan Page' 
where source='facebook / socialmedia_fashion';

update tbl_order_detail set channel='Fashion Fan Page' 
where source='facebook.com / referral' and n1='linio_fashion';



update tbl_order_detail set channel='Facebook Ads' where 
source ='facebook / socialmediaads' and source<>'facebook / retargeting';
update tbl_order_detail set channel='Alejandra Arce Voucher' where source like 'voucher5%' or source like 'voucher2%';
update tbl_order_detail set channel='FB Ads CAC Mayo' where source like 'CACFA04%';
update tbl_order_detail set channel='FB Ads CAC Junio' where source like 'CACFA05%';

update tbl_order_detail set  channel = 'FB Ads CAC' where source like 'CACADS%';

update tbl_order_detail set channel='Facebook Retargeting' where source ='facebook / retargeting';
update tbl_order_detail set channel='Criteo Retargeting' where source ='criteo / retargeting';
update tbl_order_detail set channel='My Things' where source ='mythings / retargeting';
update tbl_order_detail set channel='MainADV' where source like '%mainadv%';


#SEO
update tbl_order_detail set channel='Search Organic' where source like '%organic' or source='google.com.co / referral';

#NEWSLETTER
update tbl_order_detail set channel='Newsletter' where (source='Postal / CRM' 
or source='Email Marketing / CRM' or source='EMM / CRM' 
or source='Postal / (not set)') or (source like '%CRM%'and source not like 'VEInteractive%') 
or (source like 'LOCO%' and (source is not null and
 source not like '%blog%')
);
update tbl_order_detail set Channel='Voucher Navidad' where source like 'NAVIDAD%' 
or source like 'LINIONAVIDAD%'; 
update tbl_order_detail set Channel='Voucher Fin de Año' where source='FINDEAÑO31';
update tbl_order_detail set Channel='Bandeja CAC' where source='BANDEJA0203';
update tbl_order_detail set Channel='Tapete CAC' where source like 'TAPETE%';
update tbl_order_detail set Channel='Perfumes CAC' where source like 'PERFUME%';
update tbl_order_detail set Channel='VoucherBebes-Feb' where source like 'BEBES%';
update tbl_order_detail set channel='Sartenes CAC' where source like 'SARTENES%';
update tbl_order_detail set channel='Estufa CAC' where source like 'ESTUFA%';
update tbl_order_detail set channel='Vuelve CAC' where source like 'VUELVE%' and (coupon_code not like 'VUELVEDINERO');
update tbl_order_detail set channel='Memoria CAC'  where source like 'MEMORIA%';
update tbl_order_detail set channel='Audifonos CAC'  where source like 'AUDIFONO%';
update tbl_order_detail set channel='CACs Mayo' where source like 'CAC05%' or source='CAC0420' 
or (source='Postal / CRM' and campaign like '%cac%' and yrmonth=201305);
update tbl_order_detail set channel='CACs Abril' where (source like 'CAC04%' and source<>'CAC0420') 
or (source='Postal / CRM' and campaign like '%cac%' and yrmonth=201304);
update tbl_order_detail set channel='CACs Marzo' where source like 'CAC03%' 
or (source='Postal / CRM' and campaign like '%cac%' and yrmonth=201303);
update tbl_order_detail set channel='CACs Junio' where source like 'CAC06%'
 or (source='Postal / CRM' and campaign like '%cac%' and yrmonth=201306);
update tbl_order_detail set channel='CACs Julio' where source like 'CAC07%' or 
(source='Postal / CRM' and campaign like '%cac%' and yrmonth=201307);
update tbl_order_detail set channel='Reactivación Mayo' where source='Regalo50' or source='Regalo40' 
or source='Regalo60' or source like 'GRITA%' or Source='regalo30' or source LIKE 'aqui%';
update tbl_order_detail set channel='Fashion' where source like 'MODA%' or source like 'ESTILO%';

update tbl_order_detail set channel='Venir Reactivation' where source like 'VENIR%';
update tbl_order_detail set channel='Regresa Reactivation' where source like 'REGRESA%' and (source<>'Regresas' 
and source<>'Regresas49' and source<>'Regresa49');
update tbl_order_detail set channel='Bienvenido CAC'  where source like 'BIENVENIDO%';
update tbl_order_detail set channel='Amigo CAC'  where source like 'AMIGO%';
update tbl_order_detail set channel='Tiramos la Casa por la ventana' where source like 'CASA%' 
and (channel='' or channel is null);
update tbl_order_detail set channel='Reactivación Junio' where source='LOCURA50' or source='LOCURA040' 
or source='LOCURA40' 
or source='LOCURA60' or 
(source='Postal / CRM' and campaign like '%reactivation%' and yrmonth=201307) or 
(coupon_code_description like 'CRM%' and coupon_code_description not like'%CAC%' and yrmonth=201307 and
coupon_code=source);

update tbl_order_detail set channel='Reactivación Julio' where 
(source='Postal / CRM' and campaign like '%reactivation%' and yrmonth=201307) or 
(coupon_code_description like 'CRM%' and coupon_code_description not like'%CAC%' and yrmonth=201308 and
coupon_code=source);

update tbl_order_detail 
set channel='Happy Birthday' 
where coupon_code like 'HB%';

update tbl_order_detail
set channel='Newsletter'
where coupon_code in ('CAC08100', 'CAC0850', 'CAC0820');

update tbl_order_detail
set channel = 'NL CAC'
where source like 'CAC%' and (source not like 'CACFB%' and source not like 'CACFF%' and source not like 'CACBLOG%' and source not like 'CACADS%'); 


update tbl_order_detail set channel='Blog Linio' where source like 'blog%' ;
update tbl_order_detail set channel='Blog Linio' where order_nr='200133986' or order_nr='200882196'
or source='BL%' or source like 'madres%';
update tbl_order_detail set channel='Blog Mujer' where source='mujer.linio.com.co / referral';

update tbl_order_detail set channel = 'Blog CAC'where source like 'CACBLOG%';

update tbl_order_detail set channel='Lector' where source like 'lector%';

update tbl_order_detail set channel='Reactivation Letters' where (source like 'REACT%' or source like 'CLIENTVIP%' or source like 'LINIOVIP%' or source='VUELVE' or source like 'CLIENTEVIP%') and source<>'CLIENTEVIP1';
update tbl_order_detail set channel='Customer Adquisition Letters' where  source like 'CONOCE%' or source='CLIENTEVIP1';
update tbl_order_detail set channel='Internal Sells' where source like 'INTER%';
update tbl_order_detail set Channel='Employee Vouchers' 
where source like 'OCT%' or source like 'SEP%' or source like 'EMP%' or (source like 'CS%' and (channel is null or channel=''));
update tbl_order_detail set channel='Parcel Vouchers' where  source like 'DESCUENTO%' or source='LINIO0KO' or source='LINIOiju';
update tbl_order_detail set channel='New Register' where  source like 'NL%' or source like 'NR%';
update tbl_order_detail set channel='Disculpas' where source like 'H1yJvf' or source like 'H87h7C' or source='KI025EL78LLXLACOL-25324'
or source='KI025EL85GRQLACOL-4516';
update tbl_order_detail set channel='Suppliers Email' where source='suppliers_mail / email';
update tbl_order_detail set channel='Out Of Stock' where source like 'NST%' or source='N0Ctor';
update tbl_order_detail set channel='Devoluciones' where source like 'DEV%';
update tbl_order_detail set channel='Broken Vouchers' where source like 'HVE%';
update tbl_order_detail set channel='Broken Discount' where source  like  'KER%' or source='D1UqWU';
update tbl_order_detail set channel='Voucher Empleados' where source like 'EMPLEADO%';
update tbl_order_detail set channel='Gift Cards' where source like 'GC%';
update tbl_order_detail set channel='Friends and Family' where source like 'FF%';
update tbl_order_detail set channel='Leads' where source like 'LC%';
update tbl_order_detail set channel='Nueva Página' where source like 'NUEVA%' and (channel='' or channel is null);
update tbl_order_detail set channel='Promesa Linio' where source like 'PROMESA%';
update tbl_order_detail set channel='Creating Happiness' where source like 'FELIZ%' or
source like 'MUFELIZ%';
update tbl_order_detail set channel='NO IVA Abril' where source like 'PAG%' or SOURCE like
'CERO%';
update tbl_order_detail set channel='Invitados VIP Círculo de la moda' where source like 'CIRCULOREG%';
update tbl_order_detail set channel='CS-REORDER' where source like 'REO%';
update tbl_order_detail set channel='CS-CANCEL' where source like 'CAN%';



update tbl_order_detail set channel='Banco Helm' where  source like  'HELM%';
update tbl_order_detail set channel='Banco de Bogotá' where  source like 'CLIENTEBOG%' or source like 'MOVISTAR%' 
or source like 'BOG%' or source like 'BDB%' or source='BB42' or source='BB46' or source='BBLAV' or source='BBNEV' or
source='PA953EL96NHFLACOL' or
source='PA953EL96NHFLACOL' or
source='BH859EL51WBWLACOL' or
source='CU054HL58AXPLACOL' or
source='SA860TB24WCXLACOL' or
source='HA997TB17DLQLACOL' or
source='SA860TB35WCMLACOL' or
source='SA860TB37WCKLACOL' or
source='BO944TB20JEPLACOL' or
source='MG614TB23HTWLACOL' or
source='BA186TB36HTJLACOL' or
source='PR506TB63BFCLACOL' or
source='MI392TB32VRBLACOL' or
source='MI392TB33VRALACOL' or
source='BO944TB79JCILACOL' or
source='BO944TB24JELLACOL' or
source='HA997TB23DLKLACOL';
update tbl_order_detail set channel='Post BDB' where source like 'POSTBD%';

update tbl_order_detail set channel='Unknown Partnership March' where source='LG082EL56ECLLACOL-20343' or
SOURCE='SO017EL14ZQRLACOL48' or
SOURCE='LG082EL97RQSLACOL48' or
SOURCE='NI088EL26CJLLACOL48' or
SOURCE='GE008EL90BEZLACOL48' or
SOURCE='BDE862EL18WGZLACOL48' or
SOURCE='LU939HL19XSMLACOL48' or
SOURCE='LU939HL11XSULACOL48' or
SOURCE='LU939HL03XTCLACOL48' or
SOURCE='AR842EL87LLOLACOL48' or
SOURCE='HO107EL04MOPLACOL48' or
SOURCE='TC124EL64NBRLACOL48' or
SOURCE='CO052EL34DGFLACOL48' or
SOURCE='MI392EL71GRCLACOL48' or
SOURCE='GU689FA67MQALACOL48' or
SOURCE='TO692FA65MQCLACOL48' or
SOURCE='GU689FA66MQBLACOL48' or
SOURCE='FR119EL73NBILACOL48' or
SOURCE='HA135EL15KYALACOL48' or
SOURCE='CU054EL66KZXLACOL48' or
SOURCE='IN122TB66NBPLACOL48';

update tbl_order_detail set channel='Banco Davivienda' where  source like 'DAV%' or source='DV_AG' or source='DVPLANCHA46';
update tbl_order_detail set channel='Banco BBVA' where  (source like 'BBVA%' and source<>'BBVADP') or
source='SA860TB32WCPLACOL' or
source='SA860TB43WCELACOL' or
source='BA186TB35HTKLACOL' or
source='PR506TB55BFKLACOL' or
source='PR506TB62BFDLACOL' or
source='PA953EL97NHELACOL' or
source='PO916EL07ETSLACOL' or
source='PO916EL04ETVLACOL' or
source='LU939HL23XSILACOL' or
source='LU939HL32XRZLACOL' or
source='HA997TB15DLSLACOL' or
source='SA860TB35WCMLACOLACOL' or (source like '%BBVA%' and source<>'FBBBVAS');

update tbl_order_detail set channel='Campus Party' where source like 'CP%';
update tbl_order_detail set channel='Publimetro' where source like 'PUBLIMETRO%';
update tbl_order_detail set channel='El Espectador' where source='ESPECT0yx' or source like 'FIXED%';
update tbl_order_detail set channel='Groupon' where coupon_code_description like '%Groupon%' and 
(source not like 'gracias%' and source not like 'GRI%' and source is not null);
update tbl_order_detail set channel='Dafiti' where source='AMIGODAFITI';
update tbl_order_detail set channel='Sin IVA 16' where source='SINIVA16';
update tbl_order_detail set channel='En Medio' where source='ENMEDIO';
update tbl_order_detail set channel='Bancolombia' where source like 'AMEX%' or
source='ABNSbkpSmM' or
source='ATR08i1X6D' or
source='TABAMEX1DA2h' or source='ATR08i1X6D' or
source='DEV1sd0c' or
source='DEVfjFW6' or
source='DEVbCM2o' or
source='DEVN6TJN' or
source='DEVFvwqn' or
source='DEV0QUfY' or
source='DEVmhwdB' or
source='DEV0rCuq' or
source='DEVlLjUZ' or
source='DEVzKPuf' or
source='DEV6IJP6' or
source='DEV08L8A' or
source='DEV0Fvg' or
source='DEVcTttT' or
source='DEVywRNF' or
source='DEV0umCc' or
source='DEVFcXZN' or
source='AMEXTAB8BSoc';
update tbl_order_detail set channel='Citi Bank' where  source like 'CITI%' or source like 'POSTCITI%';

update tbl_order_detail set channel='Lenddo' where order_nr='200547692' or
order_nr='200461292' or
order_nr='200427692' or
order_nr='200111692' or
order_nr='200547692' or source Like 'LEND%' or source like 'LENNDO%';
update tbl_order_detail set channel='VISA' where source like 'VISA%';

update tbl_order_detail set 
channel=substr(substr(coupon_code_description, locate('-',coupon_code_description)+1), 1, 
locate('-', substr(coupon_code_description, locate('-',coupon_code_description)+1))-1)
where coupon_code=source and yrmonth>=201305 and 
coupon_code_description like 'Partnerships-%';

update tbl_order_detail set channel='Reorders CITI' where  source like 'INVCITI%';
update tbl_order_detail set channel='Replace CITI' where  source like 'REPCITI%';

update tbl_order_detail set channel='Referral Sites' where source like '%referral'
and (source not like 'google%' and source not like 'linio%' and source not like 'facebook%' or source<>'linio.com.co / referral' and
 source<>'facebook / retargeting');

update tbl_order_detail set channel_group='Partnerships' where
(coupon_code_description like 'Partnerships%') and coupon_code=source and yrmonth>=201305;


update tbl_order_detail set channel='Linio.com Referral' where source='linio.com / referral' 
or source='info.linio.com.co / referral' or source='linio.com.co / referral' or source='r.linio.com.co / referral';
update tbl_order_detail set channel='Directo / Typed' where  source='(direct) / (none)';


update tbl_order_detail set channel='Liniofashion.com.co Referral' where source='liniofashion.com.co / referral';




update tbl_order_detail set channel='Referral Program' where source like 'REF%';

update tbl_order_detail set channel='Corporate Sales' where source like'VC%' or source like 'VCJ%' or source like 'VCH%'or source like 'CVE%' 
or source like 'VCA%' or source='BIENVENIDO10' or coupon_code like 'VCLiq%';

update tbl_order_detail set channel='Corporate Sales' where order_nr = '200729359' or order_nr = '200655719' or order_nr = '200935119'
or order_nr = '200636439' or order_nr = '200691959' or order_nr = '200461959';

update tbl_order_detail set channel='Journalist Vouchers' where source='CAMILOCARM50' or
source='JUANPABVAR50' or
source='LAURACHARR50' or
source='MONICAPARA50' or
source='DIEGONARVA50' or
source='LUCYBUENO50' or
source='DIANALEON50' or
source='SILVIAPARR50' or
source='LAURAAYALA50' or
source='LEONARDONI50' or
source='CYNTHIARUIZ50' or
source='JAVIERLEAESC50' or
source='LORENAPULEC50' or
source='MARIAISABE50' or
source='PAOLACALLE50' or
source='ISABELSALAZ50' or
source='VICTORSOLA50' or
source='FABIANRAM50' or
source='ANDRESROD50' or
source='HENRYGON50' or
source='JOHANMA50' or
source='ALFONSOAYA50' or
source='VICTORSOLANAV' or SOURCE='LUISFERNANDOBOTERO' or
source='PILARBOLIVAR' or
source='MARCELAESTRADA' or
source='LUXLANCHEROS';

update tbl_order_detail set channel='ADN' where source like'ADN%';
update tbl_order_detail set channel='Metro' where source like'METRO%';
update tbl_order_detail set channel='TRANSMILENIO' where source like 'TRANSMILENIO%' or
SOURCE='Calle76' or
SOURCE='Calle100' or
SOURCE='Calle63' or
SOURCE='Calle45' or
SOURCE='Marly' or source='HEROES' or SOURCE='ESCUELAMILITAR';
update tbl_order_detail set channel='SMS SanValentin' where source like'SANVALENTIN%';
update tbl_order_detail set channel='SuperDescuentos' where source like'SUPERDESCUENTOS%';
update tbl_order_detail set channel='Folletos' where source='Regalo';
update tbl_order_detail set channel='Mujer' where source like 'MUJER%' and source<>'mujer.linio.com.co / referral';
update tbl_order_detail set channel='DiaEspecial' where source like 'DiaEspecial%';
update tbl_order_detail set channel='Hora Loca SMS' where source like 'HORALOCA%';
update tbl_order_detail set channel='Calle Vouchers' where source like 'call%';
update tbl_order_detail set channel='SMS' where source like 'SMS%' and source<>'SMSNueva';
update tbl_order_detail set channel='BBVADP' where source='BBVADP';
update tbl_order_detail set channel='Other PR' where source='LG082EL97RQSLACOLDP' or
source='SO017EL14ZQRLACOLDP';
update tbl_order_detail set channel='Valla Abril' where source='Regalo50';
update tbl_order_detail set channel='SMS Nueva pagina NC' where source='RegaloSMS';
update tbl_order_detail set channel='SMS Nueva pagina Costumers' where source='SMSNueva';
update tbl_order_detail set channel='SMS Más temprano más barato' where source='BaratoSMS';
update tbl_order_detail set channel='Barranquilla' where source like '%quilla%' or source='Karen' or source='Deiner' or
source='Jair' or source='Sergio' or
order_nr='200973772' or
order_nr='200254572' or
order_nr='200674572' or
order_nr='200633772' or
order_nr='200312572' or
order_nr='200978572' or
order_nr='200217572' or
order_nr='200625572' or
order_nr='200583572' or
order_nr='200973572' or
order_nr='200333572' or
order_nr='200544172' or
order_nr='200486172' or
order_nr='200316172';
update tbl_order_detail set channel='Regresa (React.)'  where source='Regresa' 
or source='Regresas49' or source='Regresa49';
update tbl_order_detail set channel='Te Esperamos (React.)' where source like 'teesperamos%';
update tbl_order_detail set channel='Gracias (Repurchase)' where source like 'gracias%';
update tbl_order_detail set channel='Falabella Customers' where source='50Bienvenido' or source like '50bienvenido%';
update tbl_order_detail set channel='Cali' where source='Fabian' or
source='Gloria' or
source='Miguel' or source like 'Cali%';
update tbl_order_detail set channel='CC Avenida Chile' where source like 'PARATI%';
update tbl_order_detail set channel='Correo Bogota' where source='100Regalo';
update tbl_order_detail set channel='Feria Libro' where source like 'Feria%';
update tbl_order_detail set channel='Correo directo bogota' where source='50Regalo' or source='100Regalo' or 
source='200Regalo';
update tbl_order_detail set channel='SMS Madres' where source='MADRE30';
update tbl_order_detail set channel='CEROIVA' where source like 'CEROIVA%';
update tbl_order_detail set channel='Circulo de la Moda' where source like 'CIRCULOMODA%';
update tbl_order_detail set channel='SMS Mas Barato' where source like 'MASBARATO%';
update tbl_order_detail set channel='Mailing 2' where source='REGALO1'
or source='REGALO2'
or source='REGALO3'
or source='REGALO050'
or source='REGALO100'
or source='REGALO200';
update tbl_order_detail set channel='Reactivation60 dias' where source ='Vuelve50';
update tbl_order_detail set channel='Reactivation120 dias' where source='vuelve050';
update tbl_order_detail set channel='Reactivation180 dias' where source='50Vuelve';
update tbl_order_detail set channel='Valla 2nd stage' where source like 'valla%';
update tbl_order_detail set channel='Repurchase50' where source='050Gracias' or source like '%050gracias%' 
or source like '%50gra%';
update tbl_order_detail set channel='Repurchase20' where source='020Gracias';
update tbl_order_detail set channel='Catalogo Junio' where source='CATALOGO50';

update tbl_order_detail set coupon_code_description='Offline-Envios600000casas-20' where source='TODO20';

commit;
select 'Offline Marketing', now();
update tbl_order_detail set 
channel=substr(substr(coupon_code_description, locate('-',coupon_code_description)+1), 1, 
locate('-', substr(coupon_code_description, locate('-',coupon_code_description)+1))-1)
where coupon_code=source and yrmonth>=201305 and 
coupon_code_description like 'offline%';

update tbl_order_detail
set channel = coupon_code
where channel = '' and coupon_code_description like 'offline%';

update tbl_order_detail set channel_group='Offline Marketing' where
(coupon_code_description like 'offline%') and coupon_code=source and yrmonth>=201305;

commit;

#PR OTHER
update tbl_order_detail set 
channel=substr(substr(coupon_code_description, locate('-',coupon_code_description)+1), 1, 
locate('-', substr(coupon_code_description, locate('-',coupon_code_description)+1))-1)
where coupon_code=source and yrmonth>=201305 and 
coupon_code_description like 'PR-%';

update tbl_order_detail set channel_group='Other (identified)' where
(coupon_code_description like 'PR-%') and coupon_code=source and yrmonth>=201305;

UPDATE tbl_order_detail set channel='Subastas Linio', channel_group='Other (identified)' , ChannelType = 'Marketing CO'
where custID=0 AND orderID IN (5,8,11);

update tbl_order_detail set channel='Corporate Sales' where order_nr ='200091496' 
or order_nr ='200078186' or order_nr ='200066886' or order_nr ='200088876' or order_nr ='200091629'
or order_nr ='200061629' or order_nr ='200021629' or order_nr ='200041629' or order_nr ='200035629'
or order_nr ='200015629' or order_nr ='200075629' or order_nr ='200085629' or order_nr ='200065629'
or order_nr ='200018629' or order_nr ='200078629' or order_nr ='200098629' or order_nr ='200037636'
or order_nr ='200027356' or order_nr ='200078656' or order_nr ='200058656' or order_nr ='200032829'
or order_nr ='200051569' or order_nr ='200097636' or order_nr ='200023129' or order_nr ='200025729'
or order_nr ='200037969' or order_nr ='200094566' or order_nr ='200064566' or order_nr ='200017946'
or order_nr ='200056446' or order_nr ='200091732' or order_nr ='200061732' or order_nr ='200021732'
or order_nr ='200067932' or order_nr ='200012232' or order_nr ='200062112' or order_nr ='200027812'
or order_nr ='200047812' or order_nr ='200018812' or order_nr ='200021972' or order_nr ='200035972'
or order_nr ='200055972' or order_nr ='200027672' or order_nr ='200058672' or order_nr ='200095272'
or order_nr ='200096272' or order_nr ='200066272' or order_nr ='200046272' or order_nr ='200012272'
or order_nr ='200072272' or order_nr ='200078186' or order_nr ='200091496' or order_nr ='200034532'
or order_nr ='200014532' or order_nr ='200045272' or order_nr ='200075862' or order_nr ='200046796'
or order_nr ='200013572' or order_nr ='200066886' or order_nr ='200088876' or order_nr ='200027356'
or order_nr ='200078656' or order_nr ='200051569' or order_nr ='200032829' or order_nr ='200097636'
or order_nr ='200023129' or order_nr ='200025729' or order_nr ='200043322' or order_nr ='200083356'
or order_nr ='200031416' or order_nr ='200014936' or order_nr ='200047812' or order_nr ='200027812'
or order_nr='200026765' or order_nr='200065495' or order_nr='200094437' or
order_nr='200021561' or
order_nr='200012651' or
order_nr='200044861' or
order_nr='200021791' or
order_nr='200057991' or
order_nr='200083781' or
order_nr='200064581' or
order_nr='200011781' or
order_nr='200073781' or
order_nr='200023781' or
order_nr='200036831' or
order_nr='200029831' or
order_nr='200099831' or
order_nr='200089831' or
order_nr='200019831' or
order_nr='200089683' or
order_nr='200046911' or
order_nr='200041933' or
order_nr='200085933' or
order_nr='200045933' or
order_nr='200037933' or
order_nr='200017933' or
order_nr='200041933' or
order_nr='200085933' or
order_nr='200045933' or
order_nr='200037933' or
order_nr='200017933' or
order_nr='200781642' or
order_nr='200677842' or
order_nr='200991742' or
order_nr='200271142' or
order_nr='200245142' or
order_nr='200325142' or
order_nr='200365142' or
order_nr='200895142' or
order_nr='200555142' or
order_nr='200271142' or
order_nr='200423142' or
order_nr='200841142' or
order_nr='200195962' or
order_nr='200829492' or
order_nr='200168292' or
order_nr='200918692' or order_nr='200853382'
or order_nr='200532252' or
order_nr='200529252' or
order_nr='200884952' or
order_nr='200828952' or
order_nr='200359852' or
order_nr='200297852' or
order_nr='200452752' or
order_nr='200411152' or
order_nr='200914352' 
or order_nr='200685116';



update tbl_order_detail set channel='FB-Diana Call'
where (order_nr='200059657' or
order_nr='200017957' or
order_nr='200076157' or
order_nr='200059157' or
order_nr='200064357' or
order_nr='200021357' or
order_nr='200016217' or
order_nr='200095217' or
order_nr='200082617' or
order_nr='200059617' or
order_nr='200074917' or
order_nr='200086917' or
order_nr='200036917' or
order_nr='200032517' or
order_nr='200074117' or
order_nr='200028317' or
order_nr='200046437' or
order_nr='200088437' or
order_nr='200044837' or
order_nr='200017837' or
order_nr='200037445' or
order_nr='200086725' or
order_nr='200097725' or
order_nr='200093725' or
order_nr='200079525' or
order_nr='200062325' or
order_nr='200095325' or
order_nr='200042665' or
order_nr='200093965' or
order_nr='200054865' or
order_nr='200082765' or
order_nr='200025765' or
order_nr='200035765' or
order_nr='200053765' or
order_nr='200012365' or
order_nr='200056495' or
order_nr='200061495' or
order_nr='200024295' or
order_nr='200066695' or
order_nr='200053695' or
order_nr='200026395' or
order_nr='200061395' or
order_nr='200067485' or
order_nr='200062275' or
order_nr='200014455' or
order_nr='200042455' or
order_nr='200037735' or
order_nr='200026335' or
order_nr='200079335' or
order_nr='200078335' or
order_nr='200091641' or
order_nr='200053641' or
order_nr='200068541' or
order_nr='200052141' or
order_nr='200024341' or
order_nr='200061821' or
order_nr='200052821' or
order_nr='200038961' or
order_nr='200041521' or
order_nr='200058321' or
order_nr='200052261' or
order_nr='200025661' or
order_nr='200059961' or
order_nr='200048961' or
order_nr='200093961' or
order_nr='200062861' or
order_nr='200069561' or
order_nr='200055561' or
order_nr='200013561' or
order_nr='200033561' or
order_nr='200053161' or
order_nr='200011161' or
order_nr='200035361' or
order_nr='200019491' or
order_nr='200033891' or
order_nr='200055681' or
order_nr='200045981' or
order_nr='200059781' or
order_nr='200078781' or
order_nr='200058581' or
order_nr='200038371' or
order_nr='200053371' or
order_nr='200083451' or
order_nr='200044251' or
order_nr='200042251' or
order_nr='200014651' or
order_nr='200036851' or
order_nr='200026951' or
order_nr='200085851' or
order_nr='200039751' or
order_nr='200091751' or
order_nr='200022551' or
order_nr='200091151' or
order_nr='200069351' or
order_nr='200049411' or
order_nr='200034211' or
order_nr='200049211' or
order_nr='200039611' or
order_nr='200045811' or
order_nr='200095811' or
order_nr='200091811' or
order_nr='200022711' or
order_nr='200023111' or
order_nr='200021711' or
order_nr='200096231' or
order_nr='200069931' or
order_nr='200056731' or
order_nr='200043731' or
order_nr='200044631' or
order_nr='200023443' or
order_nr='200062943' or
order_nr='200087843' or
order_nr='200017963' or
order_nr='200097863' or
order_nr='200084763' or
order_nr='200024563' or
order_nr='200082693' or
order_nr='200081693' or
order_nr='200019993' or
order_nr='200055993' or
order_nr='200081993' or
order_nr='200039893' or
order_nr='200049793' or
order_nr='200044593' or
order_nr='200062593' or
order_nr='200087683' or
order_nr='200023683' or
order_nr='200086783' or
order_nr='200056783' or
order_nr='200069783' or
order_nr='200039783' or
order_nr='200071783' or
order_nr='200066583' or
order_nr='200062583' or
order_nr='200057583' or
order_nr='200099883' or
order_nr='200092473' or
order_nr='200068673' or
order_nr='200067673' or
order_nr='200043673' or
order_nr='200049973' or
order_nr='200064873' or
order_nr='200027923' or
order_nr='200053373' or
order_nr='200016453' or
order_nr='200023333' or
order_nr='200584442' or
order_nr='200382442' or
order_nr='200465442' or
order_nr='200776442' or
order_nr='200831442' or
order_nr='200614242' or
order_nr='200244242' or
order_nr='200127242' or
order_nr='200937242' or
order_nr='200617242' or
order_nr='200965242' or
order_nr='200663242' or
order_nr='200255242' or
order_nr='200226642' or
order_nr='200287642' or
order_nr='200326642' or
order_nr='2000649642' or
order_nr='200937642' or
order_nr='200559842' or
order_nr='200857842' or
order_nr='200614742' or
order_nr='200514742' or
order_nr='200742742' or
order_nr='200233742' or
order_nr='200143542' or
order_nr='200993542' or
order_nr='200719542' or
order_nr='200339142' or
order_nr='200352342' or
order_nr='200659422' or
order_nr='200968462' or
order_nr='200384962' or
order_nr='200546662' or
order_nr='200313362' or
order_nr='200921292' or
order_nr='200891292' or
order_nr='200743292' or
order_nr='200699692' or
order_nr='200635692' or
order_nr='200781692' or
order_nr='200195592' or
order_nr='200185592' or
order_nr='200787192' or 
order_nr='200225192' or 
order_nr='200666472' or 
order_nr='200197472' or 
order_nr='200277472' or 
order_nr='200296872' or 
order_nr='200367872' or 
order_nr='200941872' or
order_nr='200219772' or
order_nr='200548772' or
order_nr='200273772' or
order_nr='200739172' or
order_nr='200855172' or
order_nr='200915372' or
order_nr='200841372' or
order_nr='200734952') and (channel is null or channel='');




update development_co.tbl_order_detail set channel='Banco de Bogotá' where sku like 'PA953EL96NHFLACOL%' and  
date>='2012-11-26' and date<='2012-12-17' and paid_price='299900' and coupon_code is null;
update development_co.tbl_order_detail set channel='Banco de Bogotá' where sku like 'BH859EL51WBWLACOL%' and  date>='2012-11-26' and date<='2012-12-17' and paid_price='989900' and coupon_code is null;
update development_co.tbl_order_detail set channel='Banco de Bogotá' where sku like 'CU054HL58AXPLACOL%' and  date>='2012-11-26' and date<='2012-12-17' and paid_price='299900' and coupon_code is null;
update development_co.tbl_order_detail set channel='Banco de Bogotá' where sku='SA860TB24WCXLACOL-80971' and  date>='2012-11-26' and date<='2012-12-17' and paid_price='104900' and coupon_code is null;
update development_co.tbl_order_detail set channel='Banco de Bogotá' where sku='HA997TB17DLQLACOL-37681' and  date>='2012-11-26' and date<='2012-12-17' and paid_price='94900' and coupon_code is null;
update development_co.tbl_order_detail set channel='Banco de Bogotá' where sku='SA860TB35WCMLACOL-80960' and  date>='2012-11-26' and date<='2012-12-17' and paid_price='139900' and coupon_code is null;
update development_co.tbl_order_detail set channel='Banco de Bogotá' where sku='SA860TB37WCKLACOL-80958' and  date>='2012-11-26' and date<='2012-12-17' and paid_price='69900' and coupon_code is null;
update development_co.tbl_order_detail set channel='Banco de Bogotá' where sku like 'BO944TB20JEPLACOL%' and  date>='2012-11-26' and date<='2012-12-17' and paid_price='97900' and coupon_code is null;
update development_co.tbl_order_detail set channel='Banco de Bogotá' where sku='MG614TB23HTWLACOL-61488' and  date>='2012-11-26' and date<='2012-12-17' and paid_price='79900' and coupon_code is null;
update development_co.tbl_order_detail set channel='Banco de Bogotá' where sku like 'BA186TB36HTJLACOL%' and  date>='2012-11-26' and date<='2012-12-17' and paid_price='179900' and coupon_code is null;
update development_co.tbl_order_detail set channel='Banco de Bogotá' where sku like 'PR506TB63BFCLACOL%' and  date>='2012-11-26' and date<='2012-12-17' and paid_price='549900' and coupon_code is null;
update development_co.tbl_order_detail set channel='Banco de Bogotá' where sku like 'MI392TB32VRBLACOL%' and  date>='2012-11-26' and date<='2012-12-17' and paid_price='149900' and coupon_code is null;
update development_co.tbl_order_detail set channel='Banco de Bogotá' where sku like 'MI392TB33VRALACOL%' and  date>='2012-11-26' and date<='2012-12-17' and paid_price='139900' and coupon_code is null;
update development_co.tbl_order_detail set channel='Banco de Bogotá' where sku like 'BO944TB79JCILACOL%' and  date>='2012-11-26' and date<='2012-12-17' and paid_price='89900' and coupon_code is null;
update development_co.tbl_order_detail set channel='Banco de Bogotá' where sku='BO944TB24JELLACOL-23774' and  date>='2012-11-26' and date<='2012-12-17' and paid_price='149900' and coupon_code is null;
update development_co.tbl_order_detail set channel='Banco de Bogotá' where sku='HA997TB23DLKLACOL-37675' and  date>='2012-11-26' and date<='2012-12-17' and paid_price='94900' and coupon_code is null;
update development_co.tbl_order_detail set channel='Banco de Bogotá' where order_nr='200995142' or order_nr='200971142' 
or order_nr='200895142' or order_nr='200781142' or order_nr='200695142' or order_nr='200555142' or order_nr='200453142'
 or order_nr='200271142' or order_nr='200245142';

#Recuperacion partnership


update tbl_order_detail set channel='Recuperación - Partnership' where
order_nr='200387876' or 
order_nr='200626686' or
order_nr='200387876' or
order_nr='200754196' or
order_nr='200938396' or
order_nr='200915166' or
order_nr='200281996' or
order_nr='200242486' or source like 'REODOCK%';




update tbl_order_detail set channel='Otros - Mercado Libre' where order_nr='200016188' 
or order_nr='200046759' or order_nr='200046759' or order_nr='200076448' or 
order_nr='200077719' or order_nr='200046519' or order_nr='200016497' or order_nr='200082525' or order_nr='200068765' or order_nr='200072317' or
order_nr='200057225' or order_nr='200017967' or order_nr='200056697' or order_nr='200047697'
or order_nr='200081195' or order_nr='200051195' or order_nr='200088578' or order_nr='200078317'
or order_nr='200053298' or 
order_nr='200017717' or
order_nr='200016757' or order_nr='200053625' or order_nr='200089185' or
order_nr='200087561' or
order_nr='200056561' or
order_nr='200019561' or
order_nr='200038561' or
order_nr='200554462' or
order_nr='200511792' or
order_nr= '200424292' or
order_nr= '200846792' or
order_nr= '200971292' or order_nr='200283182' or order_nr='200165272' or order_nr='200177812' or 
order_nr='200264526' or order_nr='200296146' or order_nr='200578432'
or order_nr='200578432'
or order_nr='200257166'
or order_nr='200811276'
or order_nr='200534676'
;



update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CO224EL19ZJSLACOL%' and paid_price='249900' and date>='2013-02-01' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CH964EL45KJILACOL%' and paid_price='299900' and date>='2013-02-01' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'HY223EL31VLKLACOL%' and paid_price='599900' and date>='2013-02-03' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PA094EL36NLNLACOL%' and paid_price='199900' and date>='2013-02-03' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PE976OS21FWWLACOL%' and paid_price='69900' and date>='2013-02-03' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'TR606HL42POZLACOL%' and paid_price='9900' and date>='2013-02-03' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'EP007EL02GQZLACOL%' and paid_price='299900' and date>='2013-02-04' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'TO018EL79AEMLACOL%' and paid_price='899900' and date>='2013-02-04' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'NI089EL22EOFLACOL%' and paid_price='999000' and date>='2013-02-04' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'NI089EL22EOFLACOL%' and paid_price='899900' and date>='2013-02-04' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'XB125EL42ZORLACOL%' and paid_price='109900' and date>='2013-02-04' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PL098EL61WJELACOL%' and paid_price='899000' and date>='2013-02-04' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CA259EL40JSDLACOL%' and paid_price='159500' and date>='2013-02-04' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'NA528EL66JUZLACOL%' and paid_price='159500' and date>='2013-02-04' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CO224EL19ZJSLACOL%' and paid_price='249900' and date>='2013-02-05' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA197EL88ZKXLACOL%' and paid_price='549900' and date>='2013-02-05' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'JE859EL74IFLLACOL%' and paid_price='79900' and date>='2013-02-05' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'XB125EL61ANYLACOL%' and paid_price='449900' and date>='2013-02-05' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'KA778EL89PFMLACOL%' and paid_price='33000' and date>='2013-02-05' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'ZA876EL32XWTLACOL%' and paid_price='52500' and date>='2013-02-05' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'EP007EL02GQZLACOL%' and paid_price='199900' and date>='2013-02-05' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'RT613HL03TEULACOL%' and paid_price='69900' and date>='2013-02-05' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'MA390TB01PTMLACOL%' and paid_price='179900' and date>='2013-02-05' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'DR937TB27IDKLACOL%' and paid_price='109900' and date>='2013-02-05' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'TI235EL67EKQLACOL%' and paid_price='229900' and date>='2013-02-05' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'MI085EL06ISNLACOL%' and paid_price='99900' and date>='2013-02-05' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'IN940TB76IJFLACOL%' and paid_price='329900' and date>='2013-02-05' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SO621HL91VSQLACOL%' and paid_price='299900' and date>='2013-02-05' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'TI235EL39YEELACOL%' and paid_price='189900' and date>='2013-02-05' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'TO018EL79AEMLACOL%' and paid_price='699900' and date>='2013-02-05' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PA953EL35JLSLACOL%' and paid_price='69900' and date>='2013-02-05' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'VI271EL78AMFLACOL%' and paid_price='99900' and date>='2013-02-05' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CO224EL19ZJSLACOL%' and paid_price='249900' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'ZA876EL32XWTLACOL%' and paid_price='52500' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'VI271EL78AMFLACOL%' and paid_price='99900' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'EL780HL01ISSLACOL%' and paid_price='599900' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'DR937TB27IDKLACOL%' and paid_price='109900' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'KA778EL89PFMLACOL%' and paid_price='33000' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'ZA876EL32XWTLACOL%' and paid_price='52500' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SO621HL91VSQLACOL%' and paid_price='299900' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'TI235EL39YEELACOL%' and paid_price='189900' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'RT613HL03TEULACOL%' and paid_price='69900' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BE996EL29XPELACOL%' and paid_price='149900' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'EP007EL02GQZLACOL%' and paid_price='199900' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'TO018EL79AEMLACOL%' and paid_price='699900' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'XB125EL61ANYLACOL%' and paid_price='489900' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'ME248EL35VWULACOL%' and paid_price='9900' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'ME248EL35VWULACOL%' and paid_price='9900' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PR862EL69BGWLACOL%' and paid_price='249900' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'TI235EL67EKQLACOL%' and paid_price='229900' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'EL780HL01ISSLACOL%' and paid_price='599900' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BE996EL29XPELACOL%' and paid_price='149900' and date>='2013-02-06' and date<='2013-02-31';
SELECT '50% COMPLEADO', now();
SELECT 'EMPIEZA EL 50% RESTANTE', now();

update tbl_order_detail set channel='Newsletter' where source='' and sku like 'MA390TB01PTMLACOL%' and paid_price='179900' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'DR937TB27IDKLACOL%' and paid_price='109900' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'IN940TB76IJFLACOL%' and paid_price='329900' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'DR922EL51HQYLACOL%' and paid_price='25900' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BE508TB65JYWLACOL%' and paid_price='399900' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BE508TB64JYXLACOL%' and paid_price='399900' and date>='2013-02-06' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'MI392EL73GRALACOL%' and paid_price='399900' and date>='2013-02-07' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'ZO801TB29AGKLACOL%' and paid_price='9990000' and date>='2013-02-07' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'ZO801TB34AGFLACOL%' and paid_price='2990000' and date>='2013-02-07' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BE996EL29XPELACOL%' and paid_price='14990000' and date>='2013-02-07' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'VI271EL78AMFLACOL%' and paid_price='9990000' and date>='2013-02-07' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'HY223EL31VLKLACOL%' and paid_price='59990000' and date>='2013-02-07' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PR862EL69BGWLACOL%' and paid_price='24990000' and date>='2013-02-07' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'TI235EL39YEELACOL%' and paid_price='18990000' and date>='2013-02-07' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'GE138EL75AMILACOL%' and paid_price='9990000' and date>='2013-02-07' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CO052EL47DFSLACOL%' and paid_price='5990000' and date>='2013-02-07' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'IM053HL78ITPLACOL%' and paid_price='9990000' and date>='2013-02-07' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'DR937TB27IDKLACOL%' and paid_price='10990000' and date>='2013-02-07' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CO224EL19ZJSLACOL%' and paid_price='24990000' and date>='2013-02-07' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'RT613HL03TEULACOL%' and paid_price='69900' and date>='2013-02-07' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'EL780HL01ISSLACOL%' and paid_price='599900' and date>='2013-02-07' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA197EL88ZKXLACOL%' and paid_price='549900' and date>='2013-02-07' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PR862EL69BGWLACOL%' and paid_price='249900' and date>='2013-02-08' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'EP007EL02GQZLACOL%' and paid_price='2499000' and date>='2013-02-08' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA197EL88ZKXLACOL%' and paid_price='1999000' and date>='2013-02-08' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'VI271EL78AMFLACOL%' and paid_price='5499000' and date>='2013-02-08' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PR862EL69BGWLACOL%' and paid_price='999000' and date>='2013-02-08' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'HY223EL31VLKLACOL%' and paid_price='2499000' and date>='2013-02-08' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'TI235EL39YEELACOL%' and paid_price='5999000' and date>='2013-02-08' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'XB125EL61ANYLACOL%' and paid_price='1899000' and date>='2013-02-08' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PA094EL36NLNLACOL%' and paid_price='4899000' and date>='2013-02-08' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CO052EL47DFSLACOL%' and paid_price='1999000' and date>='2013-02-08' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'LG082EL26NQRLACOL%' and paid_price='599000' and date>='2013-02-08' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'RT613HL03TEULACOL%' and paid_price='7699000' and date>='2013-02-08' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PA094EL36NLNLACOL%' and paid_price='699000' and date>='2013-02-08' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'XB125EL42ZORLACOL%' and paid_price='1999000' and date>='2013-02-08' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'QB234EL07WHKLACOL%' and paid_price='1099000' and date>='2013-02-08' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SO017EL93VKWLACOL%' and paid_price='1899000' and date>='2013-02-08' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PL098EL61WJELACOL%' and paid_price='789000' and date>='2013-02-08' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'LG082EL26NQRLACOL%' and paid_price='769900' and date>='2013-02-08' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PR862EL69BGWLACOL%' and paid_price='249900' and date>='2013-02-08' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA197EL88ZKXLACOL%' and paid_price='549900' and date>='2013-02-08' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'QB234EL07WHKLACOL%' and paid_price='189000' and date>='2013-02-09' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'MI392EL73GRALACOL%' and paid_price='399900' and date>='2013-02-09' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'EP007EL02GQZLACOL%' and paid_price='199000' and date>='2013-02-09' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA197EL88ZKXLACOL%' and paid_price='549900' and date>='2013-02-09' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PR862EL69BGWLACOL%' and paid_price='249900' and date>='2013-02-09' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'EL051HL74KKBLACOL%' and paid_price='599900' and date>='2013-02-09' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'HY223EL31VLKLACOL%' and paid_price='599900' and date>='2013-02-09' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'TI235EL39YEELACOL%' and paid_price='189900' and date>='2013-02-09' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'XB125EL61ANYLACOL%' and paid_price='489900' and date>='2013-02-09' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PA094EL36NLNLACOL%' and paid_price='199900' and date>='2013-02-09' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CO052EL47DFSLACOL%' and paid_price='59900' and date>='2013-02-09' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'LG082EL26NQRLACOL%' and paid_price='769900' and date>='2013-02-09' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'RT613HL03TEULACOL%' and paid_price='69900' and date>='2013-02-09' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'VI271EL78AMFLACOL%' and paid_price='99900' and date>='2013-02-09' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'MI085EL06ISNLACOL%' and paid_price='139900' and date>='2013-02-09' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'XB125EL42ZORLACOL%' and paid_price='109900' and date>='2013-02-09' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SO017EL93VKWLACOL%' and paid_price='789000' and date>='2013-02-09' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PL098EL61WJELACOL%' and paid_price='789000' and date>='2013-02-09' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'OD666FA38RQZLACOL%' and paid_price='153000' and date>='2013-02-10' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BR683FA76RLRLACOL%' and paid_price='66400' and date>='2013-02-10' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CA996FA38FGVLACOL%' and paid_price='285900' and date>='2013-02-10' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'JE714HB84OMLLACOL%' and paid_price='89900' and date>='2013-02-10' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'QB234EL07WHKLACOL%' and paid_price='189900' and date>='2013-02-10' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA015EL82UYVLACOL%' and paid_price='899900' and date>='2013-02-10' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BR683FA48RMTLACOL%' and paid_price='36800' and date>='2013-02-10' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CA996FA74FFLLACOL%' and paid_price='278900' and date>='2013-02-10' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CA748HB00TYDLACOL%' and paid_price='129900' and date>='2013-02-10' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'VI271EL78AMFLACOL%' and paid_price='99900' and date>='2013-02-10' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'EL051HL74KKBLACOL%' and paid_price='599900' and date>='2013-02-10' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'OD666FA62RQBLACOL%' and paid_price='153000' and date>='2013-02-10' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'DA669HL23NGELACOL%' and paid_price='4900' and date>='2013-02-10' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CO052EL74AMJLACOL%' and paid_price='59900' and date>='2013-02-11' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BL041EL11SJKLACOL%' and paid_price='359900' and date>='2013-02-11' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'OD666FA33NBYLACOL%' and paid_price='194900' and date>='2013-02-11' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA015EL71GVWLACOL%' and paid_price='1099900' and date>='2013-02-11' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BL041EL11SJKLACOL%' and paid_price='359900' and date>='2013-02-11' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA197EL88ZKXLACOL%' and paid_price='549900' and date>='2013-02-11' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'TI235EL39YEELACOL%' and paid_price='189900' and date>='2013-02-11' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'XB125EL61ANYLACOL%' and paid_price='489900' and date>='2013-02-11' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'AR842EL87LLOLACOL%' and paid_price='399900' and date>='2013-02-11' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PH097EL36DUPLACOL%' and paid_price='89900' and date>='2013-02-11' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'HO098EL22BBXLACOL%' and paid_price='127000' and date>='2013-02-11' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'LG082EL26NQRLACOL%' and paid_price='769900' and date>='2013-02-11' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'RT613HL03TEULACOL%' and paid_price='69900' and date>='2013-02-11' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'VI271EL78AMFLACOL%' and paid_price='99900' and date>='2013-02-11' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'NI089EL41ENMLACOL%' and paid_price='359900' and date>='2013-02-11' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'MI085EL06ISNLACOL%' and paid_price='139900' and date>='2013-02-11' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'LG082EL97RQSLACOL%' and paid_price='1199900' and date>='2013-02-11' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SO621HL91VSQLACOL%' and paid_price='299900' and date>='2013-02-11' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'EL051HL74KKBLACOL%' and paid_price='599900' and date>='2013-02-11' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BL041EL11SJKLACOL%' and paid_price='359900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA015EL71GVWLACOL%' and paid_price='1099900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'TI235EL67EKQLACOL%' and paid_price='229900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'TI235EL39YEELACOL%' and paid_price='189900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PR862EL69BGWLACOL%' and paid_price='249900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'XB125EL61ANYLACOL%' and paid_price='489900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'VI271EL53KOSLACOL%' and paid_price='99900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'AR842EL87LLOLACOL%' and paid_price='399900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PH097EL36DUPLACOL%' and paid_price='89900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'HO098EL68IQDLACOL%' and paid_price='149900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'LG082EL26NQRLACOL%' and paid_price='769900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'RT613HL03TEULACOL%' and paid_price='69900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SO017EL49NAKLACOL%' and paid_price='329000' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'NI089EL41ENMLACOL%' and paid_price='359900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'MI085EL06ISNLACOL%' and paid_price='139900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'LG082EL97RQSLACOL%' and paid_price='1199900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SO621HL91VSQLACOL%' and paid_price='299900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'EL051HL74KKBLACOL%' and paid_price='599900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BL041EL11SJKLACOL%' and paid_price='359900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'OD666FA33NBYLACOL%' and paid_price='194900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA015EL71GVWLACOL%' and paid_price='1099900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'AR842EL87LLOLACOL%' and paid_price='399900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'XB125EL61ANYLACOL%' and paid_price='489900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BE996EL29XPELACOL%' and paid_price='149900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SO017EL14ZQRLACOL%' and paid_price='639900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CA705HB43OKELACOL%' and paid_price='129900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'JE714HB84OMLLACOL%' and paid_price='89900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'OD666FA62RQBLACOL%' and paid_price='153000' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'OD666FA38RQZLACOL%' and paid_price='153000' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CA996FA74FFLLACOL%' and paid_price='278900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CA996FA38FGVLACOL%' and paid_price='285900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BR683FA48RMTLACOL%' and paid_price='36800' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BR683FA76RLRLACOL%' and paid_price='66400' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SO017EL49NAKLACOL%' and paid_price='329000' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA015EL82UYVLACOL%' and paid_price='899900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'EL051HL74KKBLACOL%' and paid_price='599900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'ME248EL35VWULACOL%' and paid_price='9900' and date>='2013-02-12' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'AR842EL87LLOLACOL%' and paid_price='399900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BE996EL29XPELACOL%' and paid_price='149900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SO621HL86VSVLACOL%' and paid_price='299900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BR683FA48RMTLACOL%' and paid_price='36800' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'EL051HL74KKBLACOL%' and paid_price='599900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CA705HB43OKELACOL%' and paid_price='129900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'OL092EL06BFLLACOL%' and paid_price='199000' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'EL780HL52PSLLACOL%' and paid_price='499900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'VI271EL53KOSLACOL%' and paid_price='99900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CU707HB51OJWLACOL%' and paid_price='24900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'EL052HL82ITLLACOL%' and paid_price='799900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BL041EL11SJKLACOL%' and paid_price='359900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA015EL71GVWLACOL%' and paid_price='1099900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CO224EL19ZJSLACOL%' and paid_price='249900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'HE009EL05ILYLACOL%' and paid_price='119900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BR683FA76RLRLACOL%' and paid_price='66400' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'QB234EL07WHKLACOL%' and paid_price='189900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA015EL71GVWLACOL%' and paid_price='1099900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PR862EL69BGWLACOL%' and paid_price='249900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'NI089EL41ENMLACOL%' and paid_price='359900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'TI235EL67EKQLACOL%' and paid_price='229900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'NI088EL08CKDLACOL%' and paid_price='1279900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'LG082EL64ECDLACOL%' and paid_price='599900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'LG082EL97RQSLACOL%' and paid_price='1199900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CH964EL45KJILACOL%' and paid_price='299900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'EL780HL52PSLLACOL%' and paid_price='499900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BE996EL05ISOLACOL%' and paid_price='599000' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'RT613HL87HVGLACOL%' and paid_price='99900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'HO098EL69IQCLACOL%' and paid_price='259900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CE048EL37FMSLACOL%' and paid_price='733000' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PL098EL61WJELACOL%' and paid_price='899000' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA015EL71GVWLACOL%' and paid_price='1099900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'QB234EL07WHKLACOL%' and paid_price='189900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BL041EL11SJKLACOL%' and paid_price='359900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'EL052HL84ITJLACOL%' and paid_price='699900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BE996EL29XPELACOL%' and paid_price='149900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CO224EL19ZJSLACOL%' and paid_price='249900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'LG082EL64ECDLACOL%' and paid_price='599900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'JE859EL29YASLACOL%' and paid_price='24900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'GE138EL03HVSLACOL%' and paid_price='59900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'HO098EL21BBYLACOL%' and paid_price='54900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA015EL32FQTLACOL%' and paid_price='1199900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'RT613HL87HVGLACOL%' and paid_price='99900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'HA235EL58ZOBLACOL%' and paid_price='59900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'OL092EL06BFLLACOL%' and paid_price='199900' and date>='2013-02-13' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'LG082EL64ECDLACOL%' and paid_price='599900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'EL052HL84ITJLACOL%' and paid_price='699900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'VI271EL53KOSLACOL%' and paid_price='99900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BL041EL11SJKLACOL%' and paid_price='359900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA015EL71GVWLACOL%' and paid_price='1099900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'LG082EL64ECDLACOL%' and paid_price='599900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'EL052HL84ITJLACOL%' and paid_price='699900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'VI271EL53KOSLACOL%' and paid_price='99900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BL041EL11SJKLACOL%' and paid_price='359900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA015EL71GVWLACOL%' and paid_price='1099900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'DA669HL23NGELACOL%' and paid_price='1900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'DR921TB97LLELACOL%' and paid_price='19900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'MU935TB62HYFLACOL%' and paid_price='49900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'OF556TB34YTTLACOL%' and paid_price='34900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'OF556TB30YTXLACOL%' and paid_price='29900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BE508TB65JYWLACOL%' and paid_price='399900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'DR937TB33IDELACOL%' and paid_price='199900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'EV923TB60YGHLACOL%' and paid_price='199900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PR506TB42CSJLACOL%' and paid_price='129900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'TI235EL39YEELACOL%' and paid_price='189900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BE996EL29XPELACOL%' and paid_price='149900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PA953EL96NHFLACOL%' and paid_price='259900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PR862EL69BGWLACOL%' and paid_price='249900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'LG082EL26NQRLACOL%' and paid_price='769900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'LG082EL26NQRLACOL%' and paid_price='769900' and date>='2013-02-14' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PR862EL69BGWLACOL%' and paid_price='249900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA015EL71GVWLACOL%' and paid_price='1099900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'XB125EL61ANYLACOL%' and paid_price='489900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'LG082EL81KVILACOL%' and paid_price='999900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BL041EL10SJLLACOL%' and paid_price='479900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'NI089EL41ENMLACOL%' and paid_price='359900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'HE009EL05ILYLACOL%' and paid_price='119900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'LG082EL97RQSLACOL%' and paid_price='1099900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'VT210EL17ATMLACOL%' and paid_price='79900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PA953EL35JLSLACOL%' and paid_price='69900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA015EL26KEFLACOL%' and paid_price='909900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'LG082EL26NQRLACOL%' and paid_price='769900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'HO098EL68IQDLACOL%' and paid_price='149900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'MA668FA88IPJLACOL%' and paid_price='51800' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'VI271EL96HNFLACOL%' and paid_price='89900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SP844EL42XZHLACOL%' and paid_price='129900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'LG082EL56ECLLACOL%' and paid_price='2598000' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'XB125EL61ANYLACOL%' and paid_price='489900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BL041EL10SJLLACOL%' and paid_price='479900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'EL052HL84ITJLACOL%' and paid_price='699900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA015EL26KEFLACOL%' and paid_price='909000' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'JE859EL29YASLACOL%' and paid_price='24900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'DR937TB33IDELACOL%' and paid_price='199900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'AL038EL44ASLLACOL%' and paid_price='89900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'HO098EL21BBYLACOL%' and paid_price='54900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SO621HL07OPKLACOL%' and paid_price='499900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BR032EL49RDELACOL%' and paid_price='218000' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CH964EL46KJHLACOL%' and paid_price='349900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'MA668FA88IPJLACOL%' and paid_price='51800' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'DR921TB97LLELACOL%' and paid_price='19900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'DR937TB33IDELACOL%' and paid_price='199900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA015EL71GVWLACOL%' and paid_price='1099900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'QB234EL07WHKLACOL%' and paid_price='189900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BL041EL11SJKLACOL%' and paid_price='359900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'EL052HL84ITJLACOL%' and paid_price='699900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BE996EL29XPELACOL%' and paid_price='149900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CO224EL19ZJSLACOL%' and paid_price='249900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'AR842EL87LLOLACOL%' and paid_price='249900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'GE138EL03HVSLACOL%' and paid_price='59900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'HO098EL21BBYLACOL%' and paid_price='54900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA015EL32FQTLACOL%' and paid_price='1199900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'RT613HL87HVGLACOL%' and paid_price='99900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'HA235EL58ZOBLACOL%' and paid_price='59900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'OL092EL06BFLLACOL%' and paid_price='199900' and date>='2013-02-15' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'TI235EL39YEELACOL%' and paid_price='189900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PR862EL69BGWLACOL%' and paid_price='249900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA015EL71GVWLACOL%' and paid_price='1099900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BL041EL11SJKLACOL%' and paid_price='359900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'AL284EL62BWNLACOL%' and paid_price='69900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'LG082EL26NQRLACOL%' and paid_price='769900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'NI088EL28CJJLACOL%' and paid_price='449000' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'JE859EL29YASLACOL%' and paid_price='24900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'GE138EL03HVSLACOL%' and paid_price='59900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BE508TB65JYWLACOL%' and paid_price='399900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BR931TB28ACPLACOL%' and paid_price='79900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA106EL35VQYLACOL%' and paid_price='104900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'DR921TB97LLELACOL%' and paid_price='19900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'VI662HL34MUFLACOL%' and paid_price='234000' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'IN216EL71IAQLACOL%' and paid_price='32900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'TI235EL39YEELACOL%' and paid_price='189900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PR862EL69BGWLACOL%' and paid_price='249900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'VI271EL53KOSLACOL%' and paid_price='99900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SO621HL86VSVLACOL%' and paid_price='299900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'LG082EL64ECDLACOL%' and paid_price='639900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'AL284EL62BWNLACOL%' and paid_price='69900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'RT613HL03TEULACOL%' and paid_price='69900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'NI088EL28CJJLACOL%' and paid_price='449000' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'AR842EL87LLOLACOL%' and paid_price='379900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CH964EL45KJILACOL%' and paid_price='299900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BE996EL29XPELACOL%' and paid_price='149900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'DR937TB33IDELACOL%' and paid_price='199900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PA094EL36NLNLACOL%' and paid_price='199900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'LA016HL92GNJLACOL%' and paid_price='46900' and date>='2013-02-16' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'TI235EL39YEELACOL%' and paid_price='189900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA015EL71GVWLACOL%' and paid_price='1099900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'JE859EL29YASLACOL%' and paid_price='24900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'GE138EL26ATDLACOL%' and paid_price='79900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'VI271EL53KOSLACOL%' and paid_price='99900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CO052EL74AMJLACOL%' and paid_price='59900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SO621HL91VSQLACOL%' and paid_price='369900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'LG082EL26NQRLACOL%' and paid_price='769900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BR931TB14KUBLACOL%' and paid_price='79900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BE508TB65JYWLACOL%' and paid_price='399900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BR931TB28ACPLACOL%' and paid_price='79900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA106EL35VQYLACOL%' and paid_price='104900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'DR921TB97LLELACOL%' and paid_price='19900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'OF556TB33YTULACOL%' and paid_price='34900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'OF556TB25YUCLACOL%' and paid_price='29900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'TI235EL39YEELACOL%' and paid_price='189900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SA015EL71GVWLACOL%' and paid_price='1099900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'VI271EL53KOSLACOL%' and paid_price='99900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'SO621HL86VSVLACOL%' and paid_price='299900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'LG082EL64ECDLACOL%' and paid_price='639900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'AL284EL62BWNLACOL%' and paid_price='69900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'RT613HL03TEULACOL%' and paid_price='69900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'RT613HL03TEULACOL%' and paid_price='69900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'AR842EL87LLOLACOL%' and paid_price='379900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CH964EL45KJILACOL%' and paid_price='299900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BE996EL29XPELACOL%' and paid_price='149900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BR931TB14KUBLACOL%' and paid_price='79900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BR931TB28ACPLACOL%' and paid_price='79900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'DR921TB97LLELACOL%' and paid_price='19900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CU707HB51OJWLACOL%' and paid_price='9900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'CU707HB50OJXLACOL%' and paid_price='9900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'TI235EL39YEELACOL%' and paid_price='189900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'BR032EL49RDELACOL%' and paid_price='218000' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'PA094EL36NLNLACOL%' and paid_price='199900' and date>='2013-02-17' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'MU492EL27XPGLACOL%' and paid_price='249900' and date>='2013-02-18' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'LG082EL81KVILACOL%' and paid_price='999900' and date>='2013-02-18' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'MI392EL71GRCLACOL%' and paid_price='179900' and date>='2013-02-18' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'MO849TB51VAYLACOL%' and paid_price='169900' and date>='2013-02-18' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'MO849TB56VATLACOL%' and paid_price='99900' and date>='2013-02-18' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'MO849TB64VALLACOL%' and paid_price='69900' and date>='2013-02-18' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'MO849TB14UYNLACOL%' and paid_price='49900' and date>='2013-02-18' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'MO849TB63VEILACOL%' and paid_price='69900' and date>='2013-02-18' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'MO849TB44VFBLACOL%' and paid_price='69900' and date>='2013-02-18' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'MO849TB45VFALACOL%' and paid_price='79900' and date>='2013-02-18' and date<='2013-02-31';
update tbl_order_detail set channel='Newsletter' where source='' and sku like 'MO849TB96VGXLACOL%' and paid_price='79900' and date>='2013-02-18' and date<='2013-02-31';

update tbl_order_detail set channel='Newsletter' where item='126154'
or item='127088'
or item='127089'
or item='127272'
or item='127654'
or item='127833'
or item='128630'
or item='128631'
or item='129392'
or item='130515'
or item='130946'
or item='132629'
or item='133038'
or item='133155'
or item='133195'
or item='133196'
or item='133197'
or item='133227'
or item='133228'
or item='133229'
or item='135149'
or item='135326'
or item='135387'
or item='135496'
or item='135594'
or item='135625'
or item='136078'
or item='136141'
or item='136494'
or item='136549'
or item='136766'
or item='136776'
or item='137301'
or item='137517'
or item='137590'
or item='137591'
or item='138163'
or item='138822'
or item='139091'
or item='139127'
or item='139291'
or item='139292'
or item='139568'
or item='139805'
or item='139835'
or item='140711'
or item='141184'
or item='141346'
or item='141350'
or item='142255'
or item='142664'
or item='143030'
or item='143121'
or item='143844'
or item='144016'
or item='144033'
or item='144631'
or item='144648'
or item='144649'
or item='144778'
or item='144789'
or item='144811'
or item='144815'
or item='144817'
or item='144986'
or item='145038'
or item='145140'
or item='145256'
or item='145265'
or item='145270'
or item='145272'
or item='145274'
or item='145762'
or item='145828'
or item='145920'
or item='145999'
or item='146050'
or item='146085'
or item='147028'
or item='147539'
or item='147688'
or item='147839'
or item='147926'
or item='147989'
or item='148359'
or item='148587';



update tbl_order_detail set channel='FB Posts' where item='126256'
or item='126466'
or item='127080'
or item='127113'
or item='127269'
or item='127940'
or item='127956'
or item='128021'
or item='128087'
or item='128424'
or item='128498'
or item='128538'
or item='129124'
or item='129473'
or item='129778'
or item='129894'
or item='130695'
or item='131174'
or item='131770'
or item='131845'
or item='132250'
or item='132426'
or item='132682'
or item='133091'
or item='133104'
or item='133691'
or item='133721'
or item='133788'
or item='134409'
or item='134540'
or item='134559'
or item='134611'
or item='134996'
or item='135697'
or item='135975'
or item='136671'
or item='137365'
or item='137505'
or item='137516'
or item='137876'
or item='138122'
or item='138267'
or item='138824'
or item='139481'
or item='139783'
or item='139838'
or item='139968'
or item='140384'
or item='140609'
or item='140663'
or item='141208'
or item='141212'
or item='141930'
or item='142461'
or item='142577'
or item='143624'
or item='143915'
or item='144017'
or item='144111'
or item='144634'
or item='144635'
or item='144638'
or item='144641'
or item='144645'
or item='144732'
or item='144772'
or item='144776'
or item='144820'
or item='144821'
or item='145030'
or item='145031'
or item='145040'
or item='145127'
or item='145195'
or item='145269'
or item='145728'
or item='145794'
or item='145935'
or item='145946'
or item='146002'
or item='146454'
or item='146918'
or item='146919'
or item='147080'
or item='147100'
or item='147445'
or item='147500'
or item='147822'
or item='147838';

#offline l exta
update tbl_order_detail set channel_group='Offline Marketing' where
order_nr='200752236' or
order_nr='200673316' or
order_nr='200328436' or
order_nr='200959236';

#TELE-SALES
update tbl_order_detail set channel='Inbound' 
where (source='CS / call' and campaign like 'inbou%' ) or (source='CS / Inbound' or 
source='out%' or source like 'Telesales%');

#TELESALES-UP-SELL
update tbl_order_detail set channel='Up-sell'
where  source like 'UP%' or source like '%UP%' or 
(source='CS / call' and campaign like 'ups%' );

#TELESALES-INVALIDS
update tbl_order_detail set channel='Invalids' where 
(source='CS / call' and campaign like 'INVALID%') or SOURCE='CS / invalid' or source='OUT1mTG7'
or (source like 'INV%' and (channel is null or channel=''));

#TELESALES-REACTICIÓN
update tbl_order_detail set channel='Reactivation'
 where (source='CS / call' and campaign like 'reactivation%') or source like 'ACTIVA%' 
or source='CS / Reactivation';

#TELESALES-OUTBOUND
update tbl_order_detail set channel='Outbound' 
where source='CS / OutBound' 
or source='CS / out' or source like 'CV%' or (source='CS / call' and campaign like 'outbou%');

#TELESALES -CROSS-SALE
update tbl_order_detail set channel='Cross-sale' where source='CS / CrossSale' or source='CS / cross_sell';

#TELESALES- REPLACE
update tbl_order_detail set channel='Replace' where source like '%replace%' 
or (source like 'REP%' and channel is null);

#TELESALES-REORDER
update tbl_order_detail set channel='Reorder' where source='CS / reorder';

#TELESALES-INVALID-CROSSELL
update tbl_order_detail set channel='Invalid-crossell' where source='CS / invalidcrossell';

#TELESALES-INCENTIVOS
update tbl_order_detail set channel='CS-Incentivos' where source='CS / Incentivos' or 
source like 'CSIN%';




update tbl_order_detail set channel='Unknown' where source='';
update tbl_order_detail set channel='Unknown-Voucher' where channel='' and (source<>''and coupon_code is not null);
update tbl_order_detail set channel='Unknown-GA Tracking' where channel='' 
and source<>''and coupon_code is null and `source/medium` is not null;





update tbl_order_detail inner join production_co.tbl_orders_reo_rep_inv on order_nr = order_nr_nueva 
set channel = channel_original where (tbl_order_detail.source like 'REO%' and tbl_order_detail.coupon_code like 'reo%');

update tbl_order_detail inner join production_co.tbl_orders_reo_rep_inv on order_nr = order_nr_nueva
set channel = channel_original where (tbl_order_detail.source like 'REO%' and tbl_order_detail.coupon_code like 'reo%');

update tbl_order_detail inner join production_co.tbl_orders_reo_rep_inv on order_nr = order_nr_nueva
set channel = channel_original where (tbl_order_detail.source like 'REO%' and tbl_order_detail.coupon_code like 'reo%');

UPDATE tbl_order_detail set channel_group='Advertising' where order_nr like '50000%' and coupon_code not like 'VC%';

update tbl_order_detail set channel='Unknown-CS' where (source is null or source='') and
(assisted_sales_operator<>'' or assisted_sales_operator is not null);

#FIX OLD PARTNERHIPS TO OTHERS

update tbl_order_detail set channel_group='Other (identified)' where channel='Libro Bogota';
SELECT 'Start: CHANNEL GROUPING', now();
update tbl_order_detail set channel_group='Facebook Ads' where channel='Facebook Ads' or channel='Alejandra Arce Voucher'
or channel='FB Ads CAC Junio' or channel='FB Ads CAC Mayo' or channel = 'FB Ads CAC';


update tbl_order_detail set channel_group='Search Engine Marketing' where channel='SEM (unbranded)';


update tbl_order_detail set channel_group='Linio Fan Page' where channel='Facebook Referral' or channel='Twitter'
or channel='FB Posts' or channel='FB-Diana Call' or channel='Muy Feliz' or channel='FB CAC' or 
channel='Linio Fan Page' or channel = 'FB Dark posts';

update tbl_order_detail set channel_group ='Fashion Fan Page' where channel='Fashion Fan Page'  or  channel = 'FBF CAC';


update tbl_order_detail set channel_group='Retargeting' where channel='Sociomantic' or
channel='Vizury'  or channel = 'GDN Retargeting'
or channel='Facebook Retargeting' or channel='Criteo Retargeting' or channel = 'My Things' or channel = 'MainADV';


update tbl_order_detail set channel_group='Google Display Network' where channel='Google Display Network'; 


update tbl_order_detail set channel_group='NewsLetter' where channel='Newsletter'
or channel='Voucher Navidad' or channel='Voucher Fin de Año' 
or channel='Bandeja CAC' or channel='Tapete CAC' or channel='Perfumes CAC'
or channel='VoucherBebes-Feb' or channel='Sartenes CAC' or channel='Estufa CAC' or channel='Vuelve CAC' or
channel='Memoria CAC' or
channel='Audifonos CAC' or channel='Venir Reactivation' or 
channel='Audifonos CAC' or channel='Venir Reactivation' or 
channel='Regresa Reactivation' or 
channel='Bienvenido CAC' or channel='Amigo CAC' or channel='Aqui Reactivation' or
channel='Tiramos la Casa por la ventana' or channel='Creating Happiness' or channel='Grita Reactivation' or
channel='CACs Marzo' or channel='CACs Abril' or channel='CACs Mayo' or channel='CACs Junio' or channel='CACs Julio'
or channel='Reactivación Mayo' or channel='Fashion' or channel='Reactivación Junio' or 
channel='Reactivación Julio' or channel = 'VEInteractive' or channel = 'Happy Birthday' or channel = 'NL CAC';

/*
update tbl_order_detail set channel_group='NewsLetter' where
(coupon_code_description like 'CRM%') and coupon_code=source and yrmonth>=201305;*/

update tbl_order_detail set channel_group = 'NewsLetter'
where  channel='New Register' and assisted_sales_operator is null;

update tbl_order_detail set channel_group='Search Engine Optimization' where channel='Search Organic';


update tbl_order_detail set channel_group='Partnerships' where
channel='Banco Helm' or channel='Banco de Bogotá' or channel='Banco Davivienda'
or channel='Banco Davivienda' or channel='Banco BBVA'
or channel='Campus Party' or 
 channel='El Espectador' or channel='Groupon'
or channel='Dafiti' or channel='Sin IVA 16' or channel='En Medio' or channel='Bancolombia' or channel='AMEX'
or channel='Citi Bank' or channel='Lenddo' or channel='Unknown Partnership March' or 
channel='Post BDB' or channel='VISA' or channel='Coomservi' or channel='Recuperación - Partnership'
or channel='Reorders CITI' or channel='Replace CITI' or channel='BDBJULIO'
 or
channel='Bancolombia E'
or channel='BDB JUNIO';


update tbl_order_detail set channel_group='Corporate Sales' where channel='Corporate Sales';


update tbl_order_detail set channel_group='Offline Marketing' where 
channel='ADN' or channel='Journalist Vouchers' or channeL='Metro' or
channel='TRANSMILENIO' or channel='SMS SanValentin' or channel='SuperDescuentos' or
channel='Folletos' or channel='MUJER' or channel='DiaEspecial' or channel='Hora Loca SMS' or 
channel='Calle Vouchers' or channel='SMS' or channel='BBVADP' or channel='Other PR' or channel='Valla Abril'
or channel='SMS Nueva pagina NC' or channel='SMS Nueva pagina Costumers' or channel='SMS Más temprano más barato' or 
channel='Barranquilla' or channel='Regresa (React.)' or channel='Te Esperamos (React.)' or channel='Gracias (Repurchase)'
or channel='Falabella Customers' or channel='Cali' or channel='CC Avenida Chile' or channel='Correo Bogota' 
or channel='Feria Libro' or channel='Correo directo bogota' or channel='CEROIVA' or channel='SMS Madres' or
channel='Circulo de la Moda' or channel='SMS Mas Barato' or channel='Mailing 2' or channel='Publimetro' or
channel='Reactivation60 dias' or
channel='Reactivation120 dias' or
channel='Reactivation180 dias' or channel='Valla 2nd stage' or channel='Repurchase50' or
channel='Repurchase20' or channel='Catalogo Junio' or channel='Repurchase 50'
or channel='SMS Mas barato nadie puede'
or channel='Repurchase  50'
or channel='SMS padres2'
or channel='SMS Independencia'
or channel='SMS aniversario base interna'
or channel='Repurchase2' or channel='Periodista' or channel='Envios 600.000 casas'
;
 

update tbl_order_detail set channel='Invalids' where source like 'INV%' 
and(channel is null or channel='' or channel_group='Non identified');


update tbl_order_detail set channel='Reorder' where source like 'REO%' 
and (channel is null or channel='' or channel_group='Non identified');

update tbl_order_detail set channel='Replace' where source like 'REP%' 
and (channel is null or channel='' or channel_group='Non identified');


update tbl_order_detail set channel_group='Tele Sales' 
where channel='Up-sell' or channel='Outbound' or channel='Inbound' or channel='Reactivation' 
or channel='Invalids' or channel='Cross-sales' or channel='Replace' or channel='Reorder' 
or channel='Invalid-crossell' or channel='Unknown-CS' or 
channel='CS-Incentivos' or channel='CS-REORDER' or channel='CS-CANCEL' or channel='cross-sale';

update tbl_order_detail set channel_group = 'Tele Sales'
where  channel='New Register' and assisted_sales_operator is not null;


update tbl_order_detail set channel_group='Branded' where channel='Linio.com Referral' 
or channel='Directo / Typed' or
channel='SEM Branded' or channel='Liniofashion.com.co Referral' or channel = 'Bing';


update tbl_order_detail set channel_group='Mercado Libre - Alamaula' 
where channel='Mercado Libre' or channel='Otros - Mercado Libre' or channel='Alamaula';


update tbl_order_detail set channel_group='Blogs' where 
channel='Blog Linio' or channel='Blog Mujer' or channel='Lector' or channel = 'Blog CAC';


update tbl_order_detail set channel_group='Affiliates' where channel='Buscape' or 
channel='Affiliate Program' or channel = 'Tradetracker';

update tbl_order_detail set channel = 'Cash Back' WHERE `source/medium` is null and coupon_code in ('DINEROVUELVE' ,'VUELVEDINERO');


update tbl_order_detail set channel_group='Other (identified)' where channel='Referral Sites' 
or channel='Reactivation Letters' or channel like 'Customer Adquisition%' or channel='Internal Sells' or
channel='Employee Vouchers' or channel='Parcel Vouchers' or channel='Disculpas' 
or channel='Suppliers Email' or channel='Out Of Stock' 
or channel='Broken Discount' or channel='Broken Vouchers' or channel='Devoluciones' or
channel='Voucher Empleados' or channel='Gift Cards' or channel='Friends and Family' or channel='Leads' 
or channel='Referral Program' or channel='Nueva Página' or channel='Promesa Linio' or channel='NO IVA Abril' or
channel='Invitados VIP Círculo de la moda' or  channel = 'Cash Back';



update tbl_order_detail set 
channel_group='Non identified' where channel='Unknown' or channel='Unknown-Voucher' or channel='Unknown-GA Tracking';






update tbl_order_detail set channel_group='NewsLetter', channel='Tablets 12.12 NL' where sku='TI235EL39YEELACOL-83806' and date='2012-12-12' and OAC='1' and RETURNED='0' and channel_group='Unknown' limit 169;
update tbl_order_detail set channel_group='Search Engine Marketing', channel='Tablets 12.12 SEO' where sku='TI235EL39YEELACOL-83806' and date='2012-12-12' and OAC='1' and RETURNED='0' and channel_group='Unknown' limit 8;
update tbl_order_detail set channel_group='Linio Fan Page', channel='Tablets 12.12 SM' where sku='TI235EL39YEELACOL-83806' and date='2012-12-12' and OAC='1' and RETURNED='0' and channel_group='Unknown' limit 8;
update tbl_order_detail set channel_group='Mercado Libre', channel='Tablets 12.12 ML' where sku='TI235EL39YEELACOL-83806' and date='2012-12-12' and OAC='1' and RETURNED='0' and channel_group='Unknown' limit 8;
update tbl_order_detail set channel_group='Tele Sales', channel='Tablets 12.12 CS' where sku='TI235EL39YEELACOL-83806' and date='2012-12-12' and OAC='1' and RETURNED='0' and channel_group='Unknown' limit 8;
update tbl_order_detail set channel_group='Blogs', channel='Tablets 12.12 Blog' where sku='TI235EL39YEELACOL-83806' and date='2012-12-12' and OAC='1' and RETURNED='0' and channel_group='Unknown' limit 8;


update tbl_order_detail set channel_group='Affiliates' where channel='Pampa Network';


update tbl_order_detail set channeltype=0 where channel_group='Non identified';



update tbl_order_detail set ChannelType='Marketing CO' 
where (channel_group='NewsLetter' or
channel_group='Linio Fan Page' or
channel_group='Fashion Fan Page' or
channel_group='Partnerships' or
channel_group='Search Engine Optimization' or
channel_group='Corporate Sales' or
channel_group='Mercado Libre - Alamaula' or
channel_group='Blogs' or
channel_group='Branded' or
channel_group='Other (identified)'
or channel_group='Tele Sales' or channel_group='Offline Marketing')
and channel_group<>'Non identified';


update tbl_order_detail set ChannelType='Marketing Regional'
where (channel_group='Facebook Ads' or channel_group='Search Engine Marketing'
or channel_group='Retargeting' or
channel_group='Affiliates' or channel='Google Display Network') and channel_group<>'Non identified';

update tbl_order_detail set channeltype=0 where channel_group='Non identified';



update tbl_order_detail set Ownership='Vanessa Socha' where channel_group='Linio Fan Page' and channel<>'Fashion Fan Page';
update tbl_order_detail set Ownership='Andrés Ponce' where channel='Fashion Fan Page';
update tbl_order_detail set Ownership='Jose Sequera' where channel_group='NewsLetter';
update tbl_order_detail set Ownership='Paola' where channel_group='Blogs';
update tbl_order_detail set Ownership='Diana Pantoja' where channel_group='Offline Marketing';
update tbl_order_detail set Ownership='Jonathan Lizarazo' where channel_group='Partnerships';
update tbl_order_detail set Ownership='Sindy' where channel_group='Mercado Libre';
update tbl_order_detail set Ownership='Tahani' where channel_group='Tele Sales';
update tbl_order_detail set Ownership='Fabián/Camilo/Jair' where channel_group='Corporate Sales';


update tbl_order_detail set costo_after_vat=12900 where sku_config='CO052EL27VAYLACOL' and channel like '%CAC%';
update tbl_order_detail set costo_after_vat=10000 where sku_config='SO017EL23VSILACOL' and channel like '%CAC%';
update tbl_order_detail set costo_after_vat=11200 where sku_config='IN031HL52HXNLACOL' and channel like '%CAC%';
update tbl_order_detail set costo_after_vat=16500 where sku_config='CO224EL91ERKLACOL' and channel like '%CAC%';
update tbl_order_detail set costo_after_vat=5167 where sku_config='DA669HL23NGELACOL' and channel like '%CAC%';
update tbl_order_detail set costo_after_vat=9470 where sku_config='EK495HL72AXBLACOL' and channel like '%CAC%';
update tbl_order_detail set costo_after_vat=11797 where sku_config='CU707HB51OJWLACOL' and channel like '%CAC%';
update tbl_order_detail set costo_after_vat=12850 where sku_config='RO004OS89SYOLACOL' and channel like '%CAC%';
update tbl_order_detail set costo_after_vat=12000 where sku_config='TR606HL42POZLACOL' and channel like '%CAC%';
update tbl_order_detail set costo_after_vat=10000 where sku_config='SO017EL23VSILACOL' and channel like '%CAC%';
update tbl_order_detail set costo_after_vat=12900 where sku_config='CO052EL27VAYLACOL' and channel like '%CAC%';
update tbl_order_detail set costo_after_vat=14900 where sku_config='HA235EL74ZNLLACOL' and channel like '%CAC%';
update tbl_order_detail set costo_after_vat=9000 where sku_config='CR610EL90NARLACOL' and channel like '%CAC%';
update tbl_order_detail set costo_after_vat=18710 where sku_config='TR606HL45VQOLACOL' and channel like '%CAC%';



update tbl_order_detail set costo_after_vat=560344 where sku_config='SO017EL14ZQRLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=3193925 where sku_config='LG082EL50ECRLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=187425 where sku_config='CH049EL57IFALACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=689000 where sku_config='TO018EL78KVLLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=1600000 where sku_config='SA015EL56IBFLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=1245110 where sku_config='SA015EL57IBELACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=515000 where sku_config='SA197EL17TXMLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=255550 where sku_config='SP844EL88VLBLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=190900 where sku_config='PR862EL69BGWLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=395890 where sku_config='PR862EL53MIWLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=190000 where sku_config='TI235EL77XYWLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=839000 where sku_config='SA015EL92HZVLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=305000 where sku_config='IV097EL84LKPLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=212000 where sku_config='TI235EL07EOULACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=150862 where sku_config='FU059EL44LIHLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=54000 where sku_config='VI271EL96HNFLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=235759 where sku_config='CA006EL06BBPLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=271552 where sku_config='AI003EL78HMZLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=814776 where sku_config='CA006EL09BQWLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=387853 where sku_config='NI088EL98EWZLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=162931 where sku_config='SA015EL62NNHLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=283207 where sku_config='CA006EL58GHDLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=106000 where sku_config='BE996EL29XPELACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=142000 where sku_config='OL092EL07BFKLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=200000 where sku_config='SA015EL58LHTLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=440343 where sku_config='XB125EL92SQTLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=440343 where sku_config='MI085EL78PVHLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=891301 where sku_config='XB125EL44ZOPLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
update tbl_order_detail set costo_after_vat=398000 where sku_config='XB125EL61ANYLACOL' and channel_group='Mercado Libre' and yrmonth=201305;

update tbl_order_detail set WH=0 where channel_group='Corporate Sales';
update tbl_order_detail set CS=0 where channel_group='Corporate Sales';


update tbl_order_detail set costo_after_vat='1145000' where 
(sku='SA015EL52XDRLACOL-116112' or sku='SA015EL51XDSLACOL-116113') and channel_group='Partnerships';
update tbl_order_detail set costo_after_vat='1390000' 
where sku='HP071EL30YYRLACOL-118003'  and channel_group='Partnerships';


UPDATE tbl_order_detail set channel_group='Advertising' where order_nr like '50000%' and (coupon_code not like 'VC%' AND
coupon_code not like 'VISA%');
UPDATE tbl_order_detail set channel_group='Advertising' where order_nr like '50000%' and coupon_code like '%VISA%';


update tbl_order_detail set 
channel=substr(substr(coupon_code_description, locate('-',coupon_code_description)+1), 1, 
locate('-', substr(coupon_code_description, locate('-',coupon_code_description)+1))-1)
where coupon_code=source and yrmonth>=201305 and 
coupon_code_description like 'offline%';

update tbl_order_detail set channel_group='Offline Marketing' where
(coupon_code_description like 'offline%') and coupon_code=source and yrmonth>=201305;

/*call top_ventas_por_canal();*/
Select 'Channel Report No Voucher Start: ', now();

