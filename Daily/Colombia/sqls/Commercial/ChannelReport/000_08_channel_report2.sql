select 'End: channel_report_no_voucher', now();

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'development_co.channel report',
  'finish',
  NOW(),
  MAX(date),
  count(*),
  count(*)
FROM
  development_co.tbl_order_detail
;
SELECT 'End: Channel report', now();

/*call newsletter_cust_freq();*/

/*call super_distribucion_CACs();*/
