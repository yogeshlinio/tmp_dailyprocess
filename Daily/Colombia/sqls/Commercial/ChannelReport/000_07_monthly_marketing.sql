
Select 'Iniciando monthly marketing routine', now();


select @yrmonth := date_format(curdate()-1,'%Y%m');

DELETE FROM development_co.tbl_monthly_marketing WHERE yrmonth=@yrmonth;

INSERT IGNORE INTO  development_co.tbl_monthly_marketing (month,channel) 
SELECT DISTINCT month(now()), channel_group FROM  development_co.tbl_order_detail WHERE channel_group is not null and yrmonth=@yrmonth;

update  development_co.tbl_monthly_marketing
set yrmonth = date_format(curdate()-1,'%Y%m')
where month = month (curdate()-1);



UPDATE  development_co.tbl_monthly_marketing  left JOIN (SELECT yrmonth yrmonth, reporting_channel, sum(visits) visits
FROM  development_co.daily_costs_visits_per_channel where daily_costs_visits_per_channel.date < cast(curdate() as date)
GROUP BY yrmonth, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.visits=daily_costs_visits_per_channel.visits 
WHERE tbl_monthly_marketing.yrmonth = @yrmonth
;



UPDATE  development_co.tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, ceil(sum(gross_orders)) gross_orders
FROM  development_co.tbl_order_detail WHERE OBC = 1 and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=tbl_order_detail.channel_group
SET tbl_monthly_marketing.gross_orders=tbl_order_detail.gross_orders
WHERE tbl_monthly_marketing.yrmonth = @yrmonth
;


UPDATE  development_co.tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, ceil(sum(net_orders)) net_orders
FROM  development_co.tbl_order_detail WHERE oac = 1 and returned = 0 and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=tbl_order_detail.channel_group
SET tbl_monthly_marketing.net_orders=tbl_order_detail.net_orders
WHERE tbl_monthly_marketing.yrmonth = @yrmonth
;



UPDATE  development_co.tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, reporting_channel, sum(net_orders_e) net_orders_e
FROM  development_co.daily_costs_visits_per_channel where daily_costs_visits_per_channel.date < cast(curdate() as date)
GROUP BY yrmonth, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.orders_net_expected=daily_costs_visits_per_channel.net_orders_e
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;


UPDATE  development_co.tbl_monthly_marketing SET conversion_rate=IF(gross_orders/visits IS NULL, 0, gross_orders/visits);


UPDATE  development_co.tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, 
(SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat))/2350 pending_sales
FROM  development_co.tbl_order_detail WHERE tbl_order_detail.pending=1  and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
SET tbl_monthly_marketing.pending_revenue=tbl_order_detail.pending_sales
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group and tbl_monthly_marketing.yrmonth = @yrmonth;


UPDATE  development_co.tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, 
(SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat))/2350 gross_sales
FROM  development_co.tbl_order_detail WHERE tbl_order_detail.obc=1  and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
SET tbl_monthly_marketing.gross_revenue=tbl_order_detail.gross_sales
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group and tbl_monthly_marketing.yrmonth = @yrmonth;


UPDATE  development_co.tbl_monthly_marketing  INNER JOIN (SELECT yrmonth, channel_group, 
(SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat))/2350 net_sales
FROM  development_co.tbl_order_detail WHERE tbl_order_detail.oac=1 AND tbl_order_detail.returned=0  and tbl_order_detail.date < cast(curdate() as date) 
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
SET tbl_monthly_marketing.net_revenue=tbl_order_detail.net_sales
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group and tbl_monthly_marketing.yrmonth = @yrmonth
;



UPDATE  development_co.tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, 
reporting_channel, sum(net_rev_e)/2350 net_rev_e
FROM  development_co.daily_costs_visits_per_channel where daily_costs_visits_per_channel.date < cast(curdate() as date)
GROUP BY yrmonth, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.net_revenue_expected=daily_costs_visits_per_channel.net_rev_e and tbl_monthly_marketing.yrmonth = @yrmonth
;

UPDATE development_co.tbl_monthly_marketing SET net_revenue_expected = net_revenue WHERE net_revenue_expected=0;
UPDATE development_co.tbl_monthly_marketing SET net_revenue_expected = net_revenue WHERE net_revenue_expected<net_revenue;



UPDATE development_co.tbl_monthly_marketing  
INNER JOIN (SELECT yrmonth yrmonth, channel_group, sum(target_net_sales) monthly_target
FROM development_co.daily_targets_per_channel
GROUP BY yrmonth, channel_group) daily_targets_per_channel
ON daily_targets_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_targets_per_channel.channel_group
SET tbl_monthly_marketing.monthly_target=daily_targets_per_channel.monthly_target and tbl_monthly_marketing.yrmonth = @yrmonth;


UPDATE development_co.tbl_monthly_marketing 
SET 
deviation=if((net_revenue_expected/(monthly_target/(SELECT DAYOFMONTH(LAST_DAY(NOW())))*(SELECT DAY(NOW())-1))-1) IS NULL,
0,(net_revenue_expected/(monthly_target/(SELECT DAYOFMONTH(LAST_DAY(NOW())))*(SELECT DAY(NOW())-1))-1))
where tbl_monthly_marketing.yrmonth = @yrmonth;


UPDATE development_co.tbl_monthly_marketing SET avg_ticket=net_revenue/net_orders;



UPDATE development_co.tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, 
channel_group, sum(adCost)/2350 cost
FROM development_co.ga_cost_campaign where ga_cost_campaign.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) ga_cost_campaign
ON ga_cost_campaign.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=ga_cost_campaign.channel_group
SET tbl_monthly_marketing.marketing_cost=ga_cost_campaign.cost
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;



UPDATE development_co.tbl_monthly_marketing SET cost_rev=IF(marketing_cost/net_revenue_expected IS NULL, 0, marketing_cost/net_revenue_expected)
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;


UPDATE development_co.tbl_monthly_marketing SET cost_per_order=IF(marketing_cost/net_orders IS NULL, 0, marketing_cost/net_orders)
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;



UPDATE development_co.tbl_monthly_marketing  
INNER JOIN (SELECT yrmonth yrmonth, channel_group, MAX(target_cpo) target_cpo
FROM development_co.daily_targets_per_channel
GROUP BY yrmonth, channel_group) daily_targets_per_channel
ON daily_targets_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_targets_per_channel.channel_group
SET tbl_monthly_marketing.cost_per_order_target=daily_targets_per_channel.target_cpo
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;


UPDATE development_co.tbl_monthly_marketing SET cpo_vs_target_cpo=(cost_per_order/cost_per_order_target)-1 WHERE tbl_monthly_marketing.yrmonth = @yrmonth;


UPDATE development_co.tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, ceil(sum(new_customers)) nc
FROM development_co.tbl_order_detail where oac = 1 and returned = 0 and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=tbl_order_detail.channel_group
SET tbl_monthly_marketing.new_clients=tbl_order_detail.nc WHERE tbl_monthly_marketing.yrmonth = @yrmonth;


UPDATE development_co.tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, 
reporting_channel, sum(new_customers_e) nc_e
FROM development_co.daily_costs_visits_per_channel 
where daily_costs_visits_per_channel.date < cast(curdate() as date)
GROUP BY yrmonth, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.new_clientes_expected=daily_costs_visits_per_channel.nc_e WHERE tbl_monthly_marketing.yrmonth = @yrmonth;



UPDATE development_co.tbl_monthly_marketing SET new_clientes_expected=new_clients WHERE new_clientes_expected=0 AND tbl_monthly_marketing.yrmonth = @yrmonth;


UPDATE development_co.tbl_monthly_marketing SET client_acquisition_cost=marketing_cost/new_clientes_expected WHERE tbl_monthly_marketing.yrmonth = @yrmonth;



UPDATE development_co.tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, 
(SUM(costo_after_vat)+SUM(delivery_cost_supplier)+SUM(wh)+SUM(cs)+SUM(payment_cost)+SUM(shipping_cost))/2350 pc2_costs
FROM development_co.tbl_order_detail WHERE tbl_order_detail.oac=1 AND tbl_order_detail.returned=0  and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
SET tbl_monthly_marketing.pc2_costs=tbl_order_detail.pc2_costs
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group AND tbl_monthly_marketing.yrmonth = @yrmonth;


UPDATE development_co.tbl_monthly_marketing SET profit_contribution_2=1-(pc2_costs/net_revenue) WHERE tbl_monthly_marketing.yrmonth = @yrmonth;



UPDATE development_co.tbl_monthly_marketing INNER JOIN tbl_mom_net_sales_per_channel 
ON tbl_monthly_marketing.channel = tbl_mom_net_sales_per_channel.channel_group
AND tbl_monthly_marketing.yrmonth = tbl_mom_net_sales_per_channel.yrmonth
SET tbl_monthly_marketing.month_over_month = tbl_mom_net_sales_per_channel.month_over_month WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

INSERT IGNORE INTO development_co.tbl_monthly_marketing (month, channel) 
VALUES(month(date_add(now(), interval -1 day)), 'MTD');

update development_co.tbl_monthly_marketing
set yrmonth = concat(year(curdate()-1),if(month(curdate()-1)<10,concat(0,month(curdate()-1)),month(curdate()-1)))
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

UPDATE development_co.tbl_monthly_marketing INNER JOIN development_co.tbl_mom_net_sales_per_channel 
ON tbl_monthly_marketing.channel = tbl_mom_net_sales_per_channel.channel_group
AND tbl_monthly_marketing.yrmonth = tbl_mom_net_sales_per_channel.yrmonth
SET tbl_monthly_marketing.month_over_month = tbl_mom_net_sales_per_channel.month_over_month
where channel_group = 'MTD' AND tbl_monthly_marketing.yrmonth = @yrmonth;



update development_co.tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion,
 sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from development_co.tbl_order_detail t left join development_co.tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from development_co.tbl_order_detail t left join development_co.tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from development_co.tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_net_revenue = ifnull(m.net_revenue + t.adjusted_net_revenue/2350, m.net_revenue)
where m.channel <> 'Tele Sales' AND m.yrmonth = @yrmonth;

update development_co.tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from development_co.tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from development_co.tbl_order_detail t left join development_co.tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from development_co.tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_net_revenue = ifnull( t.adjusted_net_revenue/2350,0)
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth ;


update development_co.tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from development_co.tbl_order_detail t left join development_co.tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from development_co.tbl_order_detail t left join development_co.tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from development_co.tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and obc = 1 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_gross_revenue = ifnull(m.gross_revenue + t.adjusted_net_revenue/2350, m.gross_revenue)
where m.channel <> 'Tele Sales'  AND m.yrmonth = @yrmonth;

update development_co.tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from development_co.tbl_order_detail t left join development_co.tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from development_co.tbl_order_detail t left join development_co.tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from development_co.tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and obc = 1 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_gross_revenue = ifnull( t.adjusted_net_revenue/2350,0)
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth;


update development_co.tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from development_co.tbl_order_detail t left join development_co.tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from development_co.tbl_order_detail t left join development_co.tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from development_co.tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and pending = 1 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_pending_revenue = ifnull(m.pending_revenue + t.adjusted_net_revenue/2350, m.pending_revenue)
where m.channel <> 'Tele Sales' AND m.yrmonth = @yrmonth;

update development_co.tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from development_co.tbl_order_detail t left join development_co.tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from development_co.tbl_order_detail t left join development_co.tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from development_co.tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and pending = 1 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_pending_revenue = ifnull( t.adjusted_net_revenue/2350,0)
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth;


update development_co.tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, 
sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  count(distinct orderID) as totalChannel
from development_co.tbl_order_detail t left join development_co.tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, count(distinct orderID) as totalAbout 
from development_co.tbl_order_detail t left join development_co.tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, count(distinct orderID) netSales
from development_co.tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_net_orders = ifnull(m.net_orders + t.adjusted_net_revenue, m.net_orders)
where m.channel <> 'Tele Sales' AND m.yrmonth = @yrmonth;

update development_co.tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, 
sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  count(distinct orderID) as totalChannel
from development_co.tbl_order_detail t left join development_co.tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, count(distinct orderID) as totalAbout 
from development_co.tbl_order_detail t left join development_co.tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, count(distinct orderID) netSales
from development_co.tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_net_orders = t.adjusted_net_revenue
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth;


update development_co.tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, 
sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(new_customers) as totalChannel
from development_co.tbl_order_detail t left join development_co.tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio,  sum(new_customers) as totalAbout 
from development_co.tbl_order_detail t left join development_co.tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(new_customers) netSales
from development_co.tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_new_clients = ifnull(m.new_clients + t.adjusted_net_revenue, m.new_clients)
where m.channel <> 'Tele Sales' AND m.yrmonth = @yrmonth;

update development_co.tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, 
sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group, sum(new_customers) as totalChannel
from development_co.tbl_order_detail t left join development_co.tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio,  sum(new_customers) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(new_customers) netSales
from development_co.tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_new_clients = t.adjusted_net_revenue
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth;


Select 'Finalizando monthly marketing routine', now();
