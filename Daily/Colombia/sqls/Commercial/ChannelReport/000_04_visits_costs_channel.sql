
delete from development_co.tbl_visits_cost_channel;
delete from development_co.daily_costs_visits_per_channel;

insert ignore into development_co.tbl_visits_cost_channel (date, channel_group, visits, adcost) 
select v.*, if(c.adCost is null, 0, c.adCost) adCost from (
select date, channel_group, sum(if(adCost is null, 0, adCost)) adCost from development_co.ga_cost_campaign
group by date , channel_group) c right join (
select date, channel_group, sum(visits) visits from development_co.ga_visits_cost_source_medium
group by date , channel_group) v on c.channel_group = v.channel_group and c.date = v.date
order by v.date desc;

insert ignore into development_co.daily_costs_visits_per_channel (date, cost_local, reporting_channel, visits) 
select date, adcost, channel_group, visits from development_co.tbl_visits_cost_channel;

update development_co.daily_costs_visits_per_channel set month = month(date);

update development_co.daily_costs_visits_per_channel
set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));

update development_co.daily_costs_visits_per_channel set reporting_channel = 'Non identified' where reporting_channel is null;

delete from development_co.media_rev_orders;

insert ignore into development_co.media_rev_orders(date, channel_group, net_rev, gross_rev, net_orders, gross_orders, new_customers_gross, new_customers)
 select date, channel_group, sum(net_grand_total_after_vat), sum(gross_grand_total_after_vat), sum(net_orders), sum(gross_orders), 
sum(new_customers_gross), sum(new_customers) from development_co.tbl_order_detail group by date, channel_group;

update development_co.daily_costs_visits_per_channel a inner join development_co.media_rev_orders b on a.reporting_channel=b.channel_group
 and a.date = b.date set a.gross_rev = b.gross_rev;

update development_co.daily_costs_visits_per_channel a inner join development_co.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.gross_orders = b.gross_orders;

update development_co.daily_costs_visits_per_channel a inner join development_co.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.new_customers_gross = b.new_customers_gross;

update development_co.daily_costs_visits_per_channel a set net_rev_e = (select (avg(net_rev/gross_rev)) from media_rev_orders b where b.date between date_sub(a.date, interval 21 day) and date_sub(a.date,interval 7 day) and a.reporting_channel=b.channel_group group by reporting_channel);

update development_co.daily_costs_visits_per_channel a set net_orders_e = (select (avg(net_orders/gross_orders)) from media_rev_orders b where b.date between date_sub(a.date, interval 21 day) and date_sub(a.date,interval 7 day) and a.reporting_channel=b.channel_group group by reporting_channel);

update development_co.daily_costs_visits_per_channel a set new_customers_e = (select (avg(new_customers/new_customers_gross)) from media_rev_orders b where b.date between date_sub(a.date, interval 21 day) and date_sub(a.date,interval 7 day) and a.reporting_channel=b.channel_group group by reporting_channel);

update development_co.daily_costs_visits_per_channel set net_rev_e = net_rev_e*gross_rev, net_orders_e = net_orders_e*gross_orders, new_customers_e = new_customers_e*new_customers_gross;

update development_co.daily_costs_visits_per_channel a inner join development_co.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.net_rev_e = b.net_rev, a.net_orders_e = b.net_orders, a.new_customers_e = b.new_customers where a.date < date_sub(curdate(), interval 14 day);

update development_co.daily_costs_visits_per_channel a inner join development_co.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.net_rev_e = case when b.net_rev>a.net_rev_e then b.net_rev else a.net_rev_e end, a.net_orders_e = case when b.net_orders>a.net_orders_e then b.net_orders else a.net_orders_e end, a.new_customers_e = case when b.new_customers>a.new_customers_e then b.new_customers else a.new_customers_e end;

update development_co.daily_costs_visits_per_channel a inner join development_co.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.net_rev_e = case when a.net_rev_e is null then b.net_rev else a.net_rev_e end, a.net_orders_e = case when a.net_orders_e is null then b.net_orders else a.net_orders_e end, a.new_customers_e = case when a.new_customers_e is null then b.new_customers else a.new_customers_e end;

update development_co.daily_costs_visits_per_channel a inner join development_co.daily_targets_per_channel b 
on a.reporting_channel=b.channel_group and a.date = b.date set a.target_net_sales = b.target_net_sales, a.target_cpo = b.target_cpo;
