
#call daily_marketing_spend ();
truncate development_co.tbl_daily_marketing_report;

select 'DAILY MARKETING STARTS:', now();
# INSET DATE + CHANNEL
INSERT 
INTO development_co.tbl_daily_marketing_report (date, channel_group) 
select date, tbl_channel_groups.channel_group from
(SELECT DISTINCT 
date 
FROM development_co.ga_visits_cost_source_medium 
WHERE channel_group is not null) p join tbl_channel_groups 
 order by date desc, channel_group desc;

# MARKETING COST
UPDATE development_co.tbl_daily_marketing_report  INNER JOIN (SELECT date, 
reporting_channel, sum(cost_local) cost
FROM development_co.daily_costs_visits_per_channel
GROUP BY date, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.date=tbl_daily_marketing_report.date 
AND tbl_daily_marketing_report.channel_group=daily_costs_visits_per_channel.reporting_channel
SET tbl_daily_marketing_report.marketing_cost=daily_costs_visits_per_channel.cost;
#WHERE month(tbl_daily_marketing_report.date) >= month(cast(curdate()as date))-1

#VOUCHER COST

#CAC cost
update development_co.tbl_daily_marketing_report d inner join
 (select date, channel_group, sum(marketingCost) marketingCost from development_co.tbl_order_detail
where  ( (channel like '%CAC%' or coupon_code like '%CAC%') and 
(channel_group='Newsletter' OR channel_group like '%Fan%Page%' OR channel_group like '%Facebook%Ads%') ) 
and oac = 1 and returned = 0 and rejected = 0
group by date desc, channel_group) t on d.date = t.date and t.channel_group = d.channel_group
set d.cac_cost = marketingCost;

select 'COSTS STARTS:', now();

#REtention cost
update development_co.tbl_daily_marketing_report d inner join 
(select date, channel_group, sum(adCost) marketingCost from development_co.tbl_daily_marketing_spend
 where source = 'sociomantic' 
and (medium = 'retargeting-loyalty' or medium = 'retargeting-fbx-loyalty')
group by date desc,channel_group) t on d.date = t.date and t.channel_group = d.channel_group
set d.retention_cost = marketingCost;

update development_co.tbl_daily_marketing_report d inner join (select date, channel_group, sum(marketingCost) marketingCost from development_co.tbl_order_detail 
where channel_group like 'Offline%Marketing%'
and oac = 1 and returned = 0 and rejected = 0
group by date desc, channel_group) t on d.date = t.date and t.channel_group = d.channel_group
set d.retention_cost = marketingCost;

update development_co.tbl_daily_marketing_report d inner join 
(select date, channel_group, sum(adCost) marketingCost from development_co.tbl_daily_marketing_spend
 where source = 'mercadoLibre' 
group by date desc,channel_group) t on d.date = t.date and t.channel_group = d.channel_group
set d.retention_cost = marketingCost;

#Acquisition cost
update development_co.tbl_daily_marketing_report t inner join (
select date,channel_group, channel, sum(adCost) marketing_cost
from development_co.tbl_daily_marketing_spend where source like '%vizury%'
group by channel_group, channel,date) tab on t.channel_group = tab.channel_group
and t.date = tab.date
set acquisition_cost =  tab.marketing_cost;

update development_co.tbl_daily_marketing_report t inner join (
select date,channel_group, channel, sum(adCost) marketing_cost
from development_co.tbl_daily_marketing_spend where source like '%veint%'
group by channel_group, channel, date) tab on t.channel_group = tab.channel_group
and t.date = tab.date
set acquisition_cost =  tab.marketing_cost;

update development_co.tbl_daily_marketing_report t inner join (
select date,channel_group, channel, sum(adCost) marketing_cost
from development_co.tbl_daily_marketing_spend where source = 'sociomantic' and (medium = 'retargeting' or medium = 'retargeting-fbx')
group by channel_group, channel, date) tab on t.channel_group = tab.channel_group
and t.date = tab.date
set acquisition_cost =  tab.marketing_cost;


update development_co.tbl_daily_marketing_report t inner join (
select date,channel_group,  sum(adCost) marketing_cost
from development_co.ga_cost_campaign where source like '%google%'
group by channel_group, date) tab on t.channel_group = tab.channel_group
and t.date = tab.date
set acquisition_cost = tab.marketing_cost;

update development_co.tbl_daily_marketing_report t inner join (
select date,channel_group,  sum(adCost) marketing_cost
from development_co.tbl_daily_marketing_spend where source = 'facebook'
group by channel_group,  date) tab on t.channel_group = tab.channel_group
and t.date = tab.date
set acquisition_cost = tab.marketing_cost;


update development_co.tbl_daily_marketing_report t inner join (
select date,channel_group, channel, sum(adCost) marketing_cost
from development_co.tbl_daily_marketing_spend where source like '%pampa%'
group by channel_group, channel,date) tab on t.channel_group = tab.channel_group
and t.date = tab.date
set acquisition_cost =  tab.marketing_cost;

update development_co.tbl_daily_marketing_report set acquisition_cost=((24539+13132)*2350)/31
where channel_group='Tele Sales' and month(date)=8 and year(date)=2013
;

update development_co.tbl_daily_marketing_report set acquisition_cost=((32825)*2350)/30
where channel_group='Tele Sales' and month(date)=9 and year(date)=2013;

update development_co.tbl_daily_marketing_report set acquisition_cost=((90256413.67)/2450)/31
where channel_group='Tele Sales' and month(date)=10 and year(date)=2013;

update development_co.tbl_daily_marketing_report set acquisition_cost=((38952.36)*2450)/31
where channel_group='Tele Sales' and month(date)=11 and year(date)=2013;

update development_co.tbl_daily_marketing_report set acquisition_cost = 240000/31
where channel_group = 'Corporate Sales' and month(date) = 10 and year(date) = 2013;

update development_co.tbl_daily_marketing_report t inner join (
select date,channel_group, channel, sum(adCost) marketing_cost
from tbl_daily_marketing_spend where  channel_group = 'Corporate Sales'
group by channel_group, channel,date) tab on t.channel_group = tab.channel_group
and t.date = tab.date
set acquisition_cost =  tab.marketing_cost;
# VISITS

UPDATE development_co.tbl_daily_marketing_report  INNER JOIN (SELECT date, reporting_channel, sum(visits) visits
FROM development_co.daily_costs_visits_per_channel
GROUP BY date, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.date=tbl_daily_marketing_report.date 
AND tbl_daily_marketing_report.channel_group=daily_costs_visits_per_channel.reporting_channel
SET tbl_daily_marketing_report.visits=daily_costs_visits_per_channel.visits;

#ORDERS
select 'ORDERS AND REVENUES STARTS', now();

UPDATE development_co.tbl_daily_marketing_report  
INNER JOIN (SELECT date, channel_group, 
count(distinct orderID, obc, cancel, pending, rejected,returned, oac) orders
FROM development_co.tbl_order_detail
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing_report.date 
AND tbl_daily_marketing_report.channel_group=tbl_order_detail.channel_group
SET tbl_daily_marketing_report.orders=tbl_order_detail.orders;
#WHERE month(tbl_daily_marketing_report.date) >= month(cast(curdate()as date))-1;

#REVENUE
UPDATE development_co.tbl_daily_marketing_report  INNER JOIN (SELECT date, channel_group, 
ifnull((SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat)),0) revenue
FROM development_co.tbl_order_detail GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing_report.date 
SET tbl_daily_marketing_report.revenue=tbl_order_detail.revenue
WHERE tbl_daily_marketing_report.channel_group=tbl_order_detail.channel_group;
#AND month(tbl_daily_marketing_report.date) >= month(cast(curdate()as date))-1 ;

#GROSS ORDERS
UPDATE development_co.tbl_daily_marketing_report  
INNER JOIN (SELECT date, channel_group, 
count(distinct orderID, obc, cancel, pending, rejected,returned, oac)  gross_orders
FROM development_co.tbl_order_detail where obc = 1
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing_report.date 
AND tbl_daily_marketing_report.channel_group=tbl_order_detail.channel_group
SET tbl_daily_marketing_report.gross_orders=tbl_order_detail.gross_orders;
#WHERE month(tbl_daily_marketing_report.date) >= month(cast(curdate()as date))-1;

#GROSS REVENUE

UPDATE development_co.tbl_daily_marketing_report  INNER JOIN (SELECT date, channel_group, 
ifnull((SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat)),0) gross_revenue
FROM development_co.tbl_order_detail WHERE tbl_order_detail.obc=1 
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing_report.date 
SET tbl_daily_marketing_report.gross_revenue=tbl_order_detail.gross_revenue
WHERE tbl_daily_marketing_report.channel_group=tbl_order_detail.channel_group ;
#AND month(tbl_daily_marketing_report.date) >= month(cast(curdate()as date))-1;

#CANCELLED ORDERS
UPDATE development_co.tbl_daily_marketing_report  
INNER JOIN (SELECT date, channel_group, 
count(distinct orderID, obc, cancel, pending, rejected,returned, oac) cancelled_orders
FROM development_co.tbl_order_detail WHERE (tbl_order_detail.cancel=1 and stockout=0) or rejected=1
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing_report.date 
AND tbl_daily_marketing_report.channel_group=tbl_order_detail.channel_group
SET tbl_daily_marketing_report.cancelled_orders=tbl_order_detail.cancelled_orders;
#WHERE month(tbl_daily_marketing_report.date) >= month(cast(curdate()as date))-1;

# CANCELLED REVENUE
UPDATE tbl_daily_marketing_report  INNER JOIN (SELECT date, channel_group, 
ifnull((SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat)),0) cancelled_revenue
FROM development_co.tbl_order_detail WHERE (tbl_order_detail.cancel=1 and stockout=0) or rejected=1
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing_report.date 
SET tbl_daily_marketing_report.cancelled_revenue=tbl_order_detail.cancelled_revenue
WHERE tbl_daily_marketing_report.channel_group=tbl_order_detail.channel_group;
#AND month(tbl_daily_marketing_report.date) >= month(cast(curdate()as date))-1;



#OSS ORDERS
UPDATE tbl_daily_marketing_report  
INNER JOIN (SELECT date, channel_group, 
count(distinct orderID, obc, cancel, pending, rejected,returned, oac) oss_orders
FROM development_co.tbl_order_detail WHERE tbl_order_detail.cancel=1 and stockout=1
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing_report.date 
AND tbl_daily_marketing_report.channel_group=tbl_order_detail.channel_group
SET tbl_daily_marketing_report.oss_orders=tbl_order_detail.oss_orders;
#WHERE month(tbl_daily_marketing_report.date) >= month(cast(curdate()as date))-1;

# OSS REVENUE
UPDATE tbl_daily_marketing_report  INNER JOIN (SELECT date, channel_group, 
ifnull((SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat)),0) oss_revenue
FROM development_co.tbl_order_detail WHERE tbl_order_detail.cancel=1 and stockout=1
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing_report.date 
SET tbl_daily_marketing_report.oss_revenue=tbl_order_detail.oss_revenue
WHERE tbl_daily_marketing_report.channel_group=tbl_order_detail.channel_group;
#AND month(tbl_daily_marketing_report.date) >= month(cast(curdate()as date))-1;


#PENDING ORDERS
UPDATE tbl_daily_marketing_report  
INNER JOIN (SELECT date, channel_group, 
count(distinct orderID, obc, cancel, pending, rejected,returned, oac) pending_orders
FROM development_co.tbl_order_detail WHERE tbl_order_detail.pending=1
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing_report.date 
AND tbl_daily_marketing_report.channel_group=tbl_order_detail.channel_group
SET tbl_daily_marketing_report.pending_orders=tbl_order_detail.pending_orders;
#WHERE month(tbl_daily_marketing_report.date) >= month(cast(curdate()as date))-1;

#PENDING REVENUE
UPDATE development_co.tbl_daily_marketing_report  INNER JOIN (SELECT date, channel_group, 
ifnull((SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat)),0) pending_revenue
FROM development_co.tbl_order_detail WHERE tbl_order_detail.pending=1
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing_report.date 
SET tbl_daily_marketing_report.pending_revenue=tbl_order_detail.pending_revenue
WHERE tbl_daily_marketing_report.channel_group=tbl_order_detail.channel_group; 
#AND month(tbl_daily_marketing_report.date) >= month(cast(curdate()as date))-1;

#RETURNED ORDERS
UPDATE development_co.tbl_daily_marketing_report  
INNER JOIN (SELECT date, channel_group, 
count(distinct orderID, obc, cancel, pending, rejected,returned, oac) returned_orders
FROM development_co.tbl_order_detail WHERE tbl_order_detail.returned=1
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing_report.date 
AND tbl_daily_marketing_report.channel_group=tbl_order_detail.channel_group
SET tbl_daily_marketing_report.returned_orders=tbl_order_detail.returned_orders;
#WHERE month(tbl_daily_marketing_report.date) >= month(cast(curdate()as date))-1;

#RETURNED REVENUE
UPDATE development_co.tbl_daily_marketing_report  INNER JOIN (SELECT date, channel_group, 
ifnull((SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat)),0) returned_revenue
FROM development_co.tbl_order_detail WHERE tbl_order_detail.returned=1
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing_report.date 
SET tbl_daily_marketing_report.returned_revenue=tbl_order_detail.returned_revenue
WHERE tbl_daily_marketing_report.channel_group=tbl_order_detail.channel_group ;
#AND month(tbl_daily_marketing_report.date) >= month(cast(curdate()as date))-1;

#Visits
update development_co.tbl_daily_marketing_report
set visits = gross_orders - cancelled_orders - oss_orders - returned_orders - pending_orders
where channel_group in ('Offline Marketing', 'Corporate Sales', 'Tele Sales', 'Mercado Libre - Alamaula');

select 'NEW CUSTOMERS STARTS', now();
#NEW_CUSTOMERS
UPDATE development_co.tbl_daily_marketing_report  
INNER JOIN (SELECT date, channel_group, count(distinct t.custID) new_customers
FROM development_co.tbl_order_detail t inner join development_co.tbl_monthly_cohort m on t.custID = m.custID WHERE t.oac=1 and t.returned=0 and t.rejected = 0
and t.new_customers>0
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing_report.date 
AND tbl_daily_marketing_report.channel_group=tbl_order_detail.channel_group
SET tbl_daily_marketing_report.new_customers=tbl_order_detail.new_customers;

UPDATE development_co.tbl_daily_marketing_report  
INNER JOIN (SELECT date, channel_group, count(distinct custID) customers
FROM development_co.tbl_order_detail WHERE tbl_order_detail.oac=1 and tbl_order_detail.returned=0 and tbl_order_detail.rejected = 0
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing_report.date 
AND tbl_daily_marketing_report.channel_group=tbl_order_detail.channel_group
SET tbl_daily_marketing_report.customers=tbl_order_detail.customers;
#WHERE month(tbl_daily_marketing_report.date) >= month(cast(curdate()as date))-1;

#PRODUCT COST
UPDATE development_co.tbl_daily_marketing_report  INNER JOIN (SELECT date, channel_group, 
(SUM(ifnull(cost_oms,0))) product_cost
FROM development_co.tbl_order_detail WHERE tbl_order_detail.oac=1 and tbl_order_detail.returned=0 and tbl_order_detail.rejected = 0
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing_report.date 
SET tbl_daily_marketing_report.product_cost=tbl_order_detail.product_cost
WHERE tbl_daily_marketing_report.channel_group=tbl_order_detail.channel_group; 
#AND month(tbl_daily_marketing_report.date) >= month(cast(curdate()as date))-1;


#PC1 COST
UPDATE development_co.tbl_daily_marketing_report  INNER JOIN (SELECT date, channel_group, 
(SUM(cost_oms)+sum(ifnull(delivery_cost_supplier,0))) pc1_cost
FROM development_co.tbl_order_detail WHERE tbl_order_detail.oac=1 and tbl_order_detail.returned=0 and tbl_order_detail.rejected = 0
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing_report.date 
SET tbl_daily_marketing_report.pc1_cost=tbl_order_detail.pc1_cost
WHERE tbl_daily_marketing_report.channel_group=tbl_order_detail.channel_group; 
#AND month(tbl_daily_marketing_report.date) >= month(cast(curdate()as date))-1;


#PC 1.5 COST
UPDATE development_co.tbl_daily_marketing_report  INNER JOIN (SELECT date, channel_group, 
(SUM(cost_oms)+sum(ifnull(delivery_cost_supplier,0))+SUM(ifnull(shipping_cost,0))+SUM(ifnull(payment_cost,0))) pc1_5_cost
FROM development_co.tbl_order_detail WHERE tbl_order_detail.oac=1 and tbl_order_detail.returned=0 and tbl_order_detail.rejected = 0
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing_report.date 
SET tbl_daily_marketing_report.pc1_5_cost=tbl_order_detail.pc1_5_cost
WHERE tbl_daily_marketing_report.channel_group=tbl_order_detail.channel_group; 
#AND month(tbl_daily_marketing_report.date) >= month(cast(curdate()as date))-1;

#PC 2 COST
UPDATE development_co.tbl_daily_marketing_report  INNER JOIN (SELECT date, channel_group, 
(SUM(cost_oms)+sum(ifnull(delivery_cost_supplier,0))+SUM(ifnull(shipping_cost,0))+SUM(ifnull(payment_cost,0))+SUM(ifnull(WH,0))
+SUM(ifnull(CS,0))) pc2_cost
FROM development_co.tbl_order_detail WHERE tbl_order_detail.oac=1 and tbl_order_detail.returned=0 and tbl_order_detail.rejected = 0
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing_report.date 
SET tbl_daily_marketing_report.pc2_cost=tbl_order_detail.pc2_cost
WHERE tbl_daily_marketing_report.channel_group=tbl_order_detail.channel_group;
#AND month(tbl_daily_marketing_report.date) >= month(cast(curdate()as date))-1;


select 'CAT. REVENUES STARTS', now();
UPDATE development_co.tbl_daily_marketing_report d INNER JOIN (select date, channel_group,
 sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as net_revenue
from development_co.tbl_order_detail where oac = 1 and returned = 0 and tbl_order_detail.rejected = 0 and 
(new_cat2 = 'Electrodomésticos' or new_cat2 = 'Línea Blanca' ) and buyer like '%Maria%Fernanda%' group by date, channel_group)t 
on d.date = t.date and d.channel_group = t.channel_group set Appliances_net_revenue = net_revenue;

UPDATE development_co.tbl_daily_marketing_report d INNER JOIN (select date, channel_group, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as net_revenue
from development_co.tbl_order_detail where oac = 1 and returned = 0 and tbl_order_detail.rejected = 0 and (new_cat1 = 'Deportes' )  and buyer like '%Maria%Fernanda%' group by date, channel_group)t 
on d.date = t.date and d.channel_group = t.channel_group set Sports_net_revenue = net_revenue;

UPDATE development_co.tbl_daily_marketing_report d INNER JOIN (select date, channel_group, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as net_revenue
from development_co.tbl_order_detail where oac = 1 and returned = 0  and tbl_order_detail.rejected = 0 and (new_cat1 = 'Salud' or new_cat1 = 'Belleza' ) and buyer like '%Maria%Fernanda%' group by date, channel_group)t 
on d.date = t.date and d.channel_group = t.channel_group set Health_Beauty_net_revenue = net_revenue;

UPDATE development_co.tbl_daily_marketing_report d INNER JOIN (select date, channel_group, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as net_revenue
from development_co.tbl_order_detail where oac = 1 and returned = 0 and tbl_order_detail.rejected = 0 and (new_cat1 = 'Home and Living') and buyer like '%Freddy%' group by date, channel_group)t 
on d.date = t.date and d.channel_group = t.channel_group set Home_net_revenue = net_revenue;

UPDATE development_co.tbl_daily_marketing_report d INNER JOIN (select date, channel_group, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as net_revenue
from development_co.tbl_order_detail where oac = 1 and returned = 0 and tbl_order_detail.rejected = 0 and buyer like '%Katherine%' group by date, channel_group)t 
on d.date = t.date and d.channel_group = t.channel_group set Books_Music_net_revenue = net_revenue;

UPDATE development_co.tbl_daily_marketing_report d INNER JOIN (select date, channel_group, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as net_revenue
from development_co.tbl_order_detail where oac = 1 and returned = 0 and tbl_order_detail.rejected = 0 and buyer like '%Cristina%' group by date, channel_group)t 
on d.date = t.date and d.channel_group = t.channel_group set Bazar_net_revenue = net_revenue;

UPDATE development_co.tbl_daily_marketing_report d INNER JOIN (select date, channel_group, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as net_revenue
from development_co.tbl_order_detail where oac = 1 and returned = 0 and tbl_order_detail.rejected = 0 and buyer like '%Ximena%' group by date, channel_group)t 
on d.date = t.date and d.channel_group = t.channel_group set Kids_Babies_net_revenue = net_revenue;


UPDATE development_co.tbl_daily_marketing_report d INNER JOIN (select date, channel_group, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as net_revenue
from development_co.tbl_order_detail where oac = 1 and returned = 0 and tbl_order_detail.rejected = 0 and buyer like '%Jessica%' group by date, channel_group)t 
on d.date = t.date and d.channel_group = t.channel_group set Sexual_and_Cosmetics_net_revenue = net_revenue;

UPDATE development_co.tbl_daily_marketing_report d INNER JOIN (select date, channel_group, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as net_revenue
from development_co.tbl_order_detail where oac = 1 and returned = 0 and tbl_order_detail.rejected = 0 and (new_cat2 = 'Cámaras') and buyer like '%Maritza%' group by date, channel_group)t 
on d.date = t.date and d.channel_group = t.channel_group set Photography_net_revenue = net_revenue;

UPDATE development_co.tbl_daily_marketing_report d INNER JOIN (select date, channel_group, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as net_revenue
from development_co.tbl_order_detail where oac = 1 and returned = 0 and tbl_order_detail.rejected = 0 and (new_cat2 = 'Video Juegos') and buyer like '%Maritza%' group by date, channel_group)t 
on d.date = t.date and d.channel_group = t.channel_group set Videogames_net_revenue = net_revenue;

UPDATE development_co.tbl_daily_marketing_report d INNER JOIN (select date, channel_group, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as net_revenue
from development_co.tbl_order_detail where oac = 1 and returned = 0 and tbl_order_detail.rejected = 0 and (new_cat2 = 'Teléfonos y GPS') and buyer like '%Alexander%' group by date, channel_group)t 
on d.date = t.date and d.channel_group = t.channel_group set Cellphones_net_revenue = net_revenue;

UPDATE development_co.tbl_daily_marketing_report d INNER JOIN (select date, channel_group, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as net_revenue
from development_co.tbl_order_detail where oac = 1 and returned = 0 and tbl_order_detail.rejected = 0 and (new_cat2 = 'Cómputo') and buyer like '%Alexander%' group by date, channel_group)t 
on d.date = t.date and d.channel_group = t.channel_group set Computing_net_revenue = net_revenue;

UPDATE development_co.tbl_daily_marketing_report d INNER JOIN (select date, channel_group, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as net_revenue
from development_co.tbl_order_detail where oac = 1 and returned = 0 and tbl_order_detail.rejected = 0 and (new_cat2 = 'Audio' or new_cat2 = 'TV y Video') and buyer like '%Alexander%' group by date, channel_group)t 
on d.date = t.date and d.channel_group = t.channel_group set TV_Audio_Video_net_revenue = net_revenue;

UPDATE development_co.tbl_daily_marketing_report d INNER JOIN (select date, channel_group, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as net_revenue
from development_co.tbl_order_detail where oac = 1 and returned = 0 and tbl_order_detail.rejected = 0 and (new_cat1 = 'Fashion') group by date, channel_group)t 
on d.date = t.date and d.channel_group = t.channel_group set Fashion_net_revenue = net_revenue;

select 'LAST PART STARTS', now();
#NET SALES NC

UPDATE development_co.tbl_daily_marketing_report  INNER JOIN (SELECT date, channel_group, 
ifnull((SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat)),0) net_sales_nc
FROM development_co.tbl_order_detail WHERE tbl_order_detail.oac=1 and rejected=0 and returned=0 and new_customers>0
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing_report.date 
SET tbl_daily_marketing_report.net_sales_nc=tbl_order_detail.net_sales_nc
WHERE tbl_daily_marketing_report.channel_group=tbl_order_detail.channel_group ;

#NET_SALES RETURNING


UPDATE development_co.tbl_daily_marketing_report  INNER JOIN (SELECT date, channel_group, 
ifnull((SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat)),0) net_sales_rc
FROM development_co.tbl_order_detail WHERE tbl_order_detail.oac=1 and rejected=0 and returned=0 and 
(new_customers<=0 or new_customers is null or new_customers='')
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing_report.date 
SET tbl_daily_marketing_report.net_sales_rc=tbl_order_detail.net_sales_rc
WHERE tbl_daily_marketing_report.channel_group=tbl_order_detail.channel_group ;

#PC 2 COST NC
UPDATE development_co.tbl_daily_marketing_report  INNER JOIN (SELECT date, channel_group, 
ifnull((SUM(cost_oms)+sum(delivery_cost_supplier)+SUM(shipping_cost)+SUM(payment_cost)+SUM(WH)+SUM(CS)),0) pc2_nc
FROM development_co.tbl_order_detail WHERE tbl_order_detail.oac=1 and rejected=0 and returned=0 and new_customers>0
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing_report.date 
SET tbl_daily_marketing_report.pc2_nc=tbl_order_detail.pc2_nc
WHERE tbl_daily_marketing_report.channel_group=tbl_order_detail.channel_group;


#PC 2 COST RC
UPDATE development_co.tbl_daily_marketing_report  INNER JOIN (SELECT date, channel_group, 
ifnull((SUM(cost_oms)+sum(delivery_cost_supplier)+SUM(shipping_cost)+SUM(payment_cost)+SUM(WH)+SUM(CS)),0) pc2_rc
FROM development_co.tbl_order_detail WHERE tbl_order_detail.oac=1 and rejected=0 and returned=0 
and (new_customers<=0 or new_customers is null or new_customers='')
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing_report.date 
SET tbl_daily_marketing_report.pc2_rc=tbl_order_detail.pc2_rc
WHERE tbl_daily_marketing_report.channel_group=tbl_order_detail.channel_group;

update development_co.tbl_daily_marketing_report d inner join (select date, channel_group, sum(coupon_money_after_vat) c_value
from development_co.tbl_order_detail
where oac = 1 and returned = 0 and rejected = 0
group by date, channel_group) t on d.date = t.date and t.channel_group = d.channel_group
set d.coupon_money_value_after_vat = c_value ;

select 'End: daily_marketing_report', now();
