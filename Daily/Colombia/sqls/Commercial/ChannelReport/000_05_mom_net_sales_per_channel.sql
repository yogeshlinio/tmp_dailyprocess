
select @start_current_month:=cast(date_add(now(), interval - day(now())+1 day) as date);

select @end_current_month:=cast(date_add(now(), interval - 1 day) as date);

select @start_previous_month:=date_add(cast(date_add(now(), interval - day(now())+1 day) as date), interval - 1 month);

select @end_previous_month:=date_add(cast(date_add(now(), interval - 1 day) as date), interval - 1 month);
delete from tbl_mom_net_sales_per_channel where date = @end_previous_month;
insert into tbl_mom_net_sales_per_channel(date, channel_group, netSalesPreviousMonth, netSalesCurrentMonth,month_over_month)
select @end_current_month, currentMonth.channel_group, sum(NetSalesPreviousMonth) NetSalesPreviousMonth, sum(NetSalesCurrentMonth) NetSalesCurrentMonth, 
if((sum(NetSalesCurrentMonth)/sum(NetSalesPreviousMonth))-1  is null, 0,1-(sum(NetSalesPreviousMonth)/sum(NetSalesCurrentMonth)))
from
(select channel_group, sum(paid_price_after_vat_sin_cac) +sum(if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat)) as NetSalesCurrentMonth
from tbl_order_detail
where oac = 1 and returned = 0 and date >= @start_current_month and date <= @end_current_month
group by channel_group) currentMonth inner join
(select channel_group, sum(paid_price_after_vat_sin_cac) +sum(if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat)) as NetSalesPreviousMonth
from tbl_order_detail
where oac = 1 and returned = 0 and date >= @start_previous_month and date <= @end_previous_month
group by channel_group) previousMonth on
currentMonth.channel_group = previousMonth.channel_group
group by month(@start_current_month), currentMonth.channel_group;

delete from tbl_mom_net_sales_per_channel where date = @end_current_month;

insert into tbl_mom_net_sales_per_channel(date, channel_group, netSalesPreviousMonth, netSalesCurrentMonth,month_over_month)
select date, 'MTD',sum(NetSalesPreviousMonth), sum(NetSalesCurrentMonth), 1-(sum(NetSalesPreviousMonth)/sum(NetSalesCurrentMonth))
from (
select @end_current_month date, sum(NetSalesPreviousMonth) NetSalesPreviousMonth, sum(NetSalesCurrentMonth) NetSalesCurrentMonth, 
(sum(NetSalesCurrentMonth)/sum(NetSalesPreviousMonth))-1
from
(select channel_group, sum(paid_price_after_vat_sin_cac) +sum(if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat)) as NetSalesCurrentMonth
from tbl_order_detail
where oac = 1 and returned = 0 and date >= @start_current_month and date <= @end_current_month
group by channel_group) currentMonth inner join
(select channel_group, sum(paid_price_after_vat_sin_cac) +sum(if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat)) as NetSalesPreviousMonth
from tbl_order_detail
where oac = 1 and returned = 0 and date >= @start_previous_month and date <= @end_previous_month
group by channel_group) previousMonth on
currentMonth.channel_group = previousMonth.channel_group
group by month(@start_current_month), currentMonth.channel_group) p group by date;

update tbl_mom_net_sales_per_channel
set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) where date =  @end_previous_month;
