#Query: N1:
UPDATE A_Master INNER JOIN marketing_report.channel_report_co
SET 
    A_Master.Source_medium = channel_report_co.source_medium,
	A_Master.Campaign      = channel_report_co.Campaign,
	A_Master.Channel       = channel_report_co.Channel,
	A_Master.ChannelGroup  = channel_report_co.Channel_group
WHERE
	A_Master.idSalesOrder = channel_report_co.orderID
;