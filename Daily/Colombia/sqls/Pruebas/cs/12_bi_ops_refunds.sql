BEGIN

SELECT  'Inicio rutina CC Refunds',now();

#Zona horaria de Colombia

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'customer_service_co.tbl_bi_ops_refunds',
  'start',
  NOW(),
  MAX(date_request),
  count(*),
  count(*)
FROM
  customer_service_co.tbl_bi_ops_refunds;


set time_zone='-5:00';

SELECT  'Insertar datos de solicitudes de reembolsos de bob',now();
-- a
insert into tbl_bi_ops_refunds (Id_Solicitud, fecha_orden, order_nr, Item, status_item, paid_price,shipping_fee,
Valor, sku, Product_name, payment_method, ciudad, is_marketplace)
select id,date_ordered,order_nr, Item, status_item, paid_price,shipping_fee, 
Valor, sku,
Product_name,payment_method,ciudad, is_marketplace from 
(select  
concat(a.order_nr,"-",Item) id,a.date_ordered,a.order_nr, a.Item, a.status_item, a.paid_price,a.shipping_fee, 
a.paid_price+a.shipping_fee as 'Valor', a.sku,
a.Product_name,a.payment_method,a.ciudad, a.is_marketplace, b.Id_Solicitud
from bazayaco.tbl_order_detail a 
left join (select Id_Solicitud from tbl_bi_ops_refunds) b on concat(a.order_nr,"-",Item)=b.Id_Solicitud
where status_item like '%refund_needed%' or status_item like '%store_credit_needed%'
and date_ordered>='2013-08-01') t
where Id_Solicitud is null;

SELECT  'Insertar datos de solicitudes del aplicativo',now();

insert into tbl_bi_ops_refunds(Id_Solicitud, estado, uid, order_nr, Item, correo_cliente, razon_reembolso, motivo_devolucion, 
metodo_reembolso, user_customer_service, date_request, Fecha_estado, guia_devolucion)
select ID_Solicitud, Estado, uid, NumeroOrden, item_id, CorreoCliente, RazonReembolso, 
MotivoDevolucion, MetodoReembolso, name, FechaHora,   
Fecha_estado, guideNumber from
(select a.ID_Solicitud, e.Estado, a.uid, a.NumeroOrden, a.item_id, a.CorreoCliente, a.RazonReembolso, 
a.MotivoDevolucion, a.MetodoReembolso, users.name, a.FechaHora,
e.Fecha as 'Fecha_estado', a.guideNumber, d.Id_Solicitud id from solicitud a 
left join estado e on a.ID_Solicitud=e.ID_Solicitud 
left join users on a.user_id=users.id 
left join (select Id_Solicitud from tbl_bi_ops_refunds) d on 
a.Id_Solicitud=d.Id_Solicitud) as t
where t.id is null
and Id_Solicitud not like '000%'
and length(NumeroOrden)=9
and NumeroOrden like '2%'
and NumeroOrden <>'200000000';

SELECT  'Insertar datos de solicitudes de allice',now();
insert into tbl_bi_ops_refunds (Id_Solicitud, order_nr, Item, date_request, comment, razon_reembolso, 
motivo_devolucion, metodo_reembolso)
select id, order_nr,t.item_id, t.date_return, t.comment, t.dev,
t.reason, t.method from
(select concat(e.order_nr,"-",a.fk_sales_order_item) id, e.order_nr,a.fk_sales_order_item as item_id, 
a.date_created as 'date_return', a.comment, 'Devolucion' dev,
b.name as reason, c.name as method, d.Id_Solicitud id2 from bob_live_co.sales_order_item_return a 
left join bob_live_co.automatic_return_lists b on a.fk_return_list_reason=b.id_automatic_return_lists 
left join bob_live_co.automatic_return_lists c on a.fk_return_list_action=c.id_automatic_return_lists
left join bazayaco.tbl_order_detail e on a.fk_sales_order_item=e.Item 
left join (select Id_Solicitud from tbl_bi_ops_refunds) d on concat(e.order_nr,"-",a.fk_sales_order_item)=d.Id_Solicitud
group by a.fk_sales_order_item) t
where t.id2 is null;

SELECT  'Actualizar datos de cuenta',now();
update tbl_bi_ops_refunds t1 inner join 
(select a.* from info_cuenta a inner join
(select ID_Solicitud, max(fecha) max from info_cuenta
group by ID_Solicitud) b
on a.ID_Solicitud=b.ID_Solicitud
and a.fecha=b.max) t2
on t1.Id_Solicitud=t2.ID_Solicitud
set
t1.titular_cuenta=t2.Titular, 
t1.cedula_titular_cuenta=t2.Cedula, 
t1.banco=t2.Banco, 
t1.tipo_cuenta=t2.Tipo, 
t1.numero_cuenta=t2.NumeroCuenta,
t1.ciudad_cuenta=t2.Ciudad, 
t1.fecha_cuenta=t2.Fecha;

SELECT  'Actualizar estado del item',now();
update tbl_bi_ops_refunds a inner join bazayaco.tbl_order_detail b
on a.Item=b.Item
set a.status_item=b.status_item;

SELECT  'Actualizar campos de la solicitud con tbl_order_detail',now();
update tbl_bi_ops_refunds a inner join bazayaco.tbl_order_detail b
on a.Item=b.Item
set 
a.fecha_orden=b.date_ordered,
a.paid_price=b.paid_price,
a.shipping_fee=b.shipping_fee,
a.Valor=b.paid_price+b.shipping_fee,
a.sku=b.sku,
a.Product_name=b.Product_name, 
a.payment_method=b.payment_method, 
a.ciudad=b.ciudad,
a.is_marketplace=b.is_marketplace
where a.fecha_orden is null;

update tbl_bi_ops_refunds a inner join bazayaco.tbl_order_detail b
on a.Item=b.Item
set 
a.CustId=b.CustId;

SELECT  'Actualizar campos de la solicitud con tbl_order_detail',now();
update  tbl_bi_ops_refunds a inner join
solicitud b on a.Id_Solicitud=b.ID_Solicitud
set aplicativo=1
where aplicativo=0;

SELECT  'Actualizar campos de la solicitud desde allice',now();
update tbl_bi_ops_refunds t1 inner join
(select concat(e.order_nr,"-",a.fk_sales_order_item) id, e.order_nr,a.fk_sales_order_item as item_id, 
a.date_created as 'date_return', a.comment, 'Devolucion' dev,
b.name as reason, c.name as method, d.Id_Solicitud id2 from bob_live_co.sales_order_item_return a 
left join bob_live_co.automatic_return_lists b on a.fk_return_list_reason=b.id_automatic_return_lists 
left join bob_live_co.automatic_return_lists c on a.fk_return_list_action=c.id_automatic_return_lists
left join bazayaco.tbl_order_detail e on a.fk_sales_order_item=e.Item 
left join (select Id_Solicitud from tbl_bi_ops_refunds) d on concat(e.order_nr,"-",a.fk_sales_order_item)=d.Id_Solicitud
group by a.fk_sales_order_item) t2 ON t1.Id_Solicitud = t2.id
set
t1.date_request=t2.date_return, 
t1.comment=t2.comment, 
t1.razon_reembolso=t2.dev, 
t1.motivo_devolucion=t2.reason, 
t1.metodo_reembolso=t2.method,
t1.allice=1;

SELECT  'Actualizar cedula de acuerdo a bob',now();
update tbl_bi_ops_refunds a
inner join bob_live_co.customer h on a.CustID=h.id_customer 
set
a.national_registration_number=h.national_registration_number;

SELECT  'Actualizar nombres y telefonos',now();

DROP   TEMPORARY TABLE IF EXISTS tbl_bi_ops_names_phones;
 CREATE TEMPORARY TABLE tbl_bi_ops_names_phones
select t.first_name, t.last_name, t.fk_customer, t.phone, t.mobile_phone, t.created_at 
from bob_live_co.customer_address t join 
(select fk_customer, max(created_at) max from bob_live_co.customer_address group by fk_customer) t2 on t.created_at=t2.max 
and t.fk_customer=t2.fk_customer;

create index idx_cust on tbl_bi_ops_names_phones(fk_customer);


update tbl_bi_ops_refunds a
inner join tbl_bi_ops_names_phones g on a.CustID=g.fk_customer 
set
a.first_name=g.first_name, 
a.last_name=g.last_name,
a.number=if(g.mobile_phone is null,g.phone, g.mobile_phone);

SELECT  'Actualizar cant items',now();

DROP   TEMPORARY TABLE IF EXISTS tbl_bi_ops_cant_items;
 CREATE TEMPORARY TABLE tbl_bi_ops_cant_items
select order_nr,count(Item) as 'cant_items' from bazayaco.tbl_order_detail group by order_nr;

create index idx_order on tbl_bi_ops_cant_items(order_nr);

update tbl_bi_ops_refunds a
inner join tbl_bi_ops_cant_items as k on 
a.order_nr=k.order_nr 
set
a.cant_items_orden=k.cant_items;


DROP   TEMPORARY TABLE IF EXISTS tbl_bi_ops_cant_items_sol;
 CREATE TEMPORARY TABLE tbl_bi_ops_cant_items_sol
select order_nr,count(Item) as 'cant_items' from tbl_bi_ops_refunds 
where metodo_reembolso='reversion' and
status_item is not null 
group by order_nr;

create index idx_order on tbl_bi_ops_cant_items_sol(order_nr);

update tbl_bi_ops_refunds a
inner join tbl_bi_ops_cant_items_sol as k on 
a.order_nr=k.order_nr 
set
a.cant_items_solicitud=k.cant_items;

update tbl_bi_ops_refunds 
set
cant_items_orden=if(cant_items_orden is null,0,cant_items_orden),
cant_items_solicitud=if(cant_items_solicitud is null,0,cant_items_solicitud);

SELECT  'Actualizar guia y estados de wms',now();

update tbl_bi_ops_refunds a
inner join wms_db.inverselogistics_devolucion as w on 
a.Item=w.item_id 
set
a.guia_devolucion=w.carrier_tracking_code, 
a.status_wms_il=w.status, 
status_wms_money=w.money_status;

SELECT  'Actualizar estados del aplicativo',now();

update tbl_bi_ops_refunds a
inner join estado b on a.Id_Solicitud=b.ID_Solicitud
set a.estado=b.estado
where a.estado='Abierto';

SELECT  'Unificar nombres metodos de reembolso',now();

update tbl_bi_ops_refunds a
inner join tbl_bi_ops_refunds_metodo_reembolso b
on a.metodo_reembolso=b.nombre_allice
set a.metodo_reembolso=b.nombre_aplicativo ;

SELECT  'Actualizar estados de la solicitud',now();

update tbl_bi_ops_refunds
set 
estado='Cerrado',
fecha_estado=now()
where status_item in ('canceled','invalid','fraud_lost','cash_refunded','store_credit_issued','refund_issued');

SELECT  'Actualizar datos de efecty',now();

update tbl_bi_ops_refunds
set 
NOMBRE_EFECTY=upper(if(instr(first_name," ")=0,first_name,
left(first_name,instr(first_name," ")-1))),
APELLIDO1_EFECTY=upper(if(instr(last_name ," ")=0,last_name ,
left(last_name ,instr(last_name ," ")-1))),
APELLIDO2_EFECTY=upper(if(instr(last_name," ")=0,"N/A",
right(last_name,length(last_name)-instr(last_name," "))))
where first_name is not null;

update tbl_bi_ops_refunds
set 
TIPO_DOCUMENTO_EFECTY=IF(national_registration_number like "%-%","NIT",
"CC")
where national_registration_number is not null;

SELECT  'Revisar solicitudes que apliquen',now();

UPDATE tbl_bi_ops_refunds
set
aplica=0;

UPDATE tbl_bi_ops_refunds
set
aplica=1
where status_item in ('clarify_store_credit_not_issued','clarify_refund_needed','refund_needed','store_credit_needed');


SELECT  'Actualizar metodos de reembolso de acuerdo al aplicativo',now();

update tbl_bi_ops_refunds a
inner join solicitud b on a.Id_Solicitud=b.ID_Solicitud
set metodo_reembolso=MetodoReembolso
where a.metodo_reembolso<>b.MetodoReembolso
and MetodoReembolso<>""
and a.estado="Abierto";

SELECT  'Actualizar valores de acuerdo al shipping fee',now();

update tbl_bi_ops_refunds a
inner join tbl_bi_ops_refunds_shipping_fee as b on 
a.motivo_devolucion=b.motivo
set
a.aplica_shipping_fee=b.aplica_shipping_fee;

update tbl_bi_ops_refunds a
set
Valor=if(aplica_shipping_fee=1,paid_price+shipping_fee,paid_price);

/*SELECT  'Revisar solicitudes closed que apliquen segun bodega',now();
update tbl_bi_ops_refunds 
set aplica=1,
novedad=0
WHERE id_solicitud in
(SELECT concat(order_nr,"-",item)
FROM bazayaco.tbl_bi_ops_refunds_change_status
where change_status="SI");*/

SELECT  'Actualizar nulls',now();
update tbl_bi_ops_refunds a 
inner join solicitud b on a.Id_solicitud=b.ID_Solicitud
set
a.uid=b.uid,
correo_cliente=CorreoCliente,
razon_reembolso=RazonReembolso,
motivo_devolucion=MotivoDevolucion,
metodo_reembolso=MetodoReembolso,
date_request=FechaHora
where metodo_reembolso is null;

SELECT  'Actualizar novedades',now();


UPDATE tbl_bi_ops_refunds
set
novedad=null,
tipo_novedad=null;


UPDATE tbl_bi_ops_refunds
set
novedad=5,
tipo_novedad='INE'
where status_item is null;


UPDATE tbl_bi_ops_refunds
set
novedad=8,
tipo_novedad='RYH'
where status_item in ('store_credit_issued','cash_refunded');


update tbl_bi_ops_refunds
set
novedad=6,
tipo_novedad='ONE'
where cant_items_orden is null
and novedad<>5;


update tbl_bi_ops_refunds
set novedad=1,
tipo_novedad='ENC'
where aplica=0
and (novedad is null  or novedad not in (5,6,8));


update tbl_bi_ops_refunds
set
novedad=3,
tipo_novedad='SSC'
where metodo_reembolso='consignacion'
and (novedad is null  or novedad not in (1,5,6,8))
and titular_cuenta is null;


update tbl_bi_ops_refunds 
set
novedad=2,
tipo_novedad='CNC'
where metodo_reembolso='consignacion'
and (novedad is null  or novedad not in (1,5,6,8,3))
and cedula_titular_cuenta<>national_registration_number;


update tbl_bi_ops_refunds
set
novedad=7,
tipo_novedad='NAR'
where 
metodo_reembolso='reversion' and
(novedad is null  or novedad not in (1,5,6,8,3,2))
and cant_items_solicitud<>cant_items_orden;


update tbl_bi_ops_refunds 
set
novedad=9,
tipo_novedad='NAE'
where metodo_reembolso='efecty'
and (Valor>500000 or Valor<20000)
and (novedad is null  or novedad not in (1,5,6,8,3,2,7));


UPDATE tbl_bi_ops_refunds 
set
novedad=10,
tipo_novedad='SNI'
where (novedad is null  or novedad not in (1,5,6,8,3,2,7,9))
and aplicativo+ allice=0;


update tbl_bi_ops_refunds 
set
novedad=4,
tipo_novedad='OK'
where (novedad is null  or novedad not in (1,5,6,8,3,2,7,9,10));


update tbl_bi_ops_refunds
set 
novedad=9,
tipo_novedad='SMR'
where metodo_reembolso="" and novedad=4;


update tbl_bi_ops_refunds
set need_refund=if(novedad=4,1,0);

SELECT  'Datos de consignacion para disfon',now();

update tbl_bi_ops_refunds 
set
TIPO_DOCUMENTO_CONSIG=if(TIPO_DOCUMENTO_EFECTY='CC','C','N'),
TIPO_CUENTA_CONSIG=IF(tipo_cuenta='Ahorros','02','01')
where need_refund=1
and metodo_reembolso="consignacion";

update tbl_bi_ops_refunds a inner join
tbl_bi_ops_refunds_bancos b on a.banco=b.nombre_aplicativo
set
a.CODIGO_BANCO_CONSIG=b.codigo
where need_refund=1
and metodo_reembolso="consignacion";

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'customer_service_co.tbl_bi_ops_refunds',
  'finish',
  NOW(),
  MAX(date_request),
  count(*),
  count(*)
FROM
  customer_service_co.tbl_bi_ops_refunds;


SELECT  'Fin rutina CC Refunds',now();



END
