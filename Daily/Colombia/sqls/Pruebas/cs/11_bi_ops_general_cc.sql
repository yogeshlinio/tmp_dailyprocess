BEGIN

SELECT  'Inicio rutina principal',now();

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at)
SELECT 
  'Colombia', 
  'customer_service_co.bi_ops_general_cc',
  'start',
  NOW();

call bi_ops_cc;

call bi_ops_cc_outbound;

call bi_ops_cc_survey;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at)
SELECT 
  'Colombia', 
  'customer_service_co.bi_ops_general_cc',
  'finish',
  NOW();

SELECT  'Fin rutina principal',now();


END
