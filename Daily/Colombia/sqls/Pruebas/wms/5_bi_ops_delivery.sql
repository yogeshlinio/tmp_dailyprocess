 BEGIN
#Rutina Actualizada por: 	Natali Serrano Guerrero
#Fecha de última Actualización: 	04 de Diciembre 2013
#Funcion: 							Genera tabla maestra de Operaciones

SELECT  'OPS Delivery Report',now();


TRUNCATE TABLE bazayaco.tbl_bi_ops_delivery;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'bazayaco.tbl_bi_ops_delvery parte 1',
  'start',
  NOW(),
  MAX(date_exportable),
  count(*),
  count(*)
FROM
  bazayaco.tbl_bi_ops_delivery
;

SELECT  'Insert itens_venda',now();
INSERT INTO bazayaco.tbl_bi_ops_delivery (order_number, order_id, item_id, sku_simple, product_name, 
supplier_id, supplier_name, min_delivery_time, max_delivery_time, fulfilment_wms, 
status_wms, date_exportable, supplier_leadtime)
SELECT itens_venda.numero_order, itens_venda.order_id, itens_venda.item_id, itens_venda.sku, itens_venda.nome_produto, 
itens_venda.supplier_id, itens_venda.supplier_name, 
itens_venda.tempo_de_entrega_minimo, itens_venda.tempo_de_entrega_maximo, 
itens_venda.tipo_armazenagem, itens_venda.status, 
itens_venda.data_exportable,
itens_venda.tempo_entrega_fornecedor
FROM wmsprod_co.itens_venda
WHERE itens_venda.data_exportable >'2013-05-01 00:00:00';

SELECT  'Calculo de quebrados',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET stockout = If(status_wms='Quebrado' or status_wms='quebra tratada' or status_wms='backorder_tratada',1,0);

SELECT  'Update transportadoras',now();
UPDATE ((bazayaco.tbl_bi_ops_delivery
INNER JOIN wmsprod_co.itens_venda
ON bazayaco.tbl_bi_ops_delivery.item_id = itens_venda.item_id)
INNER JOIN wmsprod_co.romaneio
ON itens_venda.romaneio_id = romaneio.romaneio_id
INNER JOIN wmsprod_co.transportadoras
ON romaneio.transportadora_id = transportadoras.transportadoras_id)
SET bazayaco.tbl_bi_ops_delivery.shipping_courier = nome_transportadora;

SELECT  'Update catalog_shipment_type',now();
UPDATE (tbl_bi_ops_delivery
INNER JOIN bob_live_co.catalog_simple
ON bazayaco.tbl_bi_ops_delivery.sku_simple = catalog_simple.sku)
INNER JOIN bob_live_co.catalog_shipment_type
ON catalog_simple.fk_catalog_shipment_type = catalog_shipment_type.id_catalog_shipment_type
SET bazayaco.tbl_bi_ops_delivery.fulfilment_bob = catalog_shipment_type.name;

SELECT  'Actualizar fulfillment',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET fulfilment_real=fulfillment_real(item_id);

SELECT  'Actualizar fulfillment Dropshipping',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN wmsprod_co.itens_venda
ON bazayaco.tbl_bi_ops_delivery.item_id = itens_venda.item_id 
INNER JOIN wmsprod_co.status_itens_venda
ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id 
SET fulfilment_real = "Dropshipping"
WHERE status_itens_venda.status="ds estoque reservado";

SELECT  'Actualizar fulfillment corrección Crossdocking',now();
SELECT  'Actualizar fulfillment',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET fulfilment_real='Crossdocking'
WHERE fulfilment_real='Dropshipping'
AND date_procured IS NOT NULL;

UPDATE bazayaco.tbl_bi_ops_delivery
SET fulfilment_real = 'Dropshipping'
where status_wms='Waiting dropshipping';

SELECT  'Update tms_status_delivery',now();
UPDATE (bazayaco.tbl_bi_ops_delivery
INNER JOIN wmsprod_co.vw_itens_venda_entrega
ON bazayaco.tbl_bi_ops_delivery.item_id = vw_itens_venda_entrega.item_id)
INNER JOIN wmsprod_co.tms_status_delivery
ON vw_itens_venda_entrega.cod_rastreamento = tms_status_delivery.cod_rastreamento
SET bazayaco.tbl_bi_ops_delivery.date_delivered = tms_status_delivery.date
WHERE (((tms_status_delivery.id_status)=4));

update bazayaco.tbl_bi_ops_delivery p inner join wmsprod_co.itens_venda as t
on t.item_id = p.item_id
inner join wmsprod_co.status_itens_venda s
on t.itens_venda_id = s.itens_venda_id
set p.is_backorder = 1
where (s.status ='backorder');

update bazayaco.tbl_bi_ops_delivery p 
set p.min_delivery_time = if(p.fulfilment_real ='Own Warehouse', p.max_delivery_time - 3, p.max_delivery_time - 2)
where p.is_backorder = 1;

#Insert de las guias de logistica inversa
INSERT INTO  bazayaco.tbl_bi_ops_shipped_tracking_code (track,devolucion)
SELECT carrier_tracking_code,'1' 
FROM wmsprod_co.inverselogistics_agendamiento AS a
INNER JOIN bazayaco.tbl_bi_ops_delivery AS b
ON a.item_id=b.item_id
WHERE date_refund_needed IS NOT NULL;

SELECT  'Promised Linio',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN bob_live_co.sales_order_item
ON bazayaco.tbl_bi_ops_delivery.item_id=sales_order_item.id_sales_order_item
SET tbl_bi_ops_delivery.is_linio_promise=sales_order_item.is_linio_promise;

SELECT  'Delete control register from WMS',now();
DELETE FROM bazayaco.tbl_bi_ops_delivery
WHERE product_name='set up register';

SELECT  'Update order_date from sales_order',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN bob_live_co.sales_order
ON bazayaco.tbl_bi_ops_delivery.order_id = sales_order.id_sales_order
INNER JOIN bob_live_co.customer
ON sales_order.fk_customer = customer.id_customer
SET bazayaco.tbl_bi_ops_delivery.date_ordered = sales_order.created_at,
bazayaco.tbl_bi_ops_delivery.customer_first_name = customer.first_name,
bazayaco.tbl_bi_ops_delivery.customer_last_name = customer.last_name;

SELECT  'Update pedidos_venda',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN wmsprod_co.pedidos_venda
ON bazayaco.tbl_bi_ops_delivery.order_number = pedidos_venda.numero_pedido
SET bazayaco.tbl_bi_ops_delivery.payment_method = pedidos_venda.metodo_de_pagamento,
bazayaco.tbl_bi_ops_delivery.shipping_region = pedidos_venda.estado_cliente,
bazayaco.tbl_bi_ops_delivery.shipping_city = pedidos_venda.cidade_cliente,
bazayaco.tbl_bi_ops_delivery.customer_nit = pedidos_venda.cpf_cliente;

SELECT  'Update tbl_catalog_product_v2',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN bazayaco.tbl_catalog_product_v2
ON bazayaco.tbl_bi_ops_delivery.sku_simple = tbl_catalog_product_v2.sku
SET bazayaco.tbl_bi_ops_delivery.buyer = tbl_catalog_product_v2.buyer,
bazayaco.tbl_bi_ops_delivery.category_1 = tbl_catalog_product_v2.cat1,
bazayaco.tbl_bi_ops_delivery.category_2 = tbl_catalog_product_v2.cat2,
bazayaco.tbl_bi_ops_delivery.category_3 = tbl_catalog_product_v2.cat3,
bazayaco.tbl_bi_ops_delivery.cost = tbl_catalog_product_v2.cost,
bazayaco.tbl_bi_ops_delivery.price = tbl_catalog_product_v2.price;

SELECT  'Update catalog_config',now();
UPDATE ((tbl_bi_ops_delivery
INNER JOIN bob_live_co.catalog_simple
ON bazayaco.tbl_bi_ops_delivery.sku_simple = catalog_simple.sku)
INNER JOIN bob_live_co.catalog_config
ON catalog_simple.fk_catalog_config = catalog_config.id_catalog_config)
SET bazayaco.tbl_bi_ops_delivery.product_weight = greatest(cast(replace(IF(isnull(catalog_config.product_weight),1,catalog_config.product_weight),',','.') as DECIMAL(6,2)),1.0), 
bazayaco.tbl_bi_ops_delivery.package_measure = catalog_config.product_measures, 
bazayaco.tbl_bi_ops_delivery.package_width = greatest(cast(replace(IF(isnull(catalog_config.package_width),1,catalog_config.package_width),',','.') as DECIMAL(6,2)),1.0), 
bazayaco.tbl_bi_ops_delivery.package_length = greatest(cast(replace(IF(isnull(catalog_config.package_length),1,catalog_config.package_length),',','.') as DECIMAL(6,2)),1.0), 
bazayaco.tbl_bi_ops_delivery.package_height = greatest(cast(replace(IF(isnull(catalog_config.package_height),1,catalog_config.package_height),',','.') as DECIMAL(6,2)),1.0), 
bazayaco.tbl_bi_ops_delivery.sku_config = catalog_config.sku,
bazayaco.tbl_bi_ops_delivery.supplier_procurement_time = bazayaco.tbl_bi_ops_delivery.min_delivery_time-1;

SELECT  'Update catalog_shipment_type',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET bazayaco.tbl_bi_ops_delivery.supplier_procurement_time = if(tbl_bi_ops_delivery.supplier_procurement_time<2,2,tbl_bi_ops_delivery.supplier_procurement_time);

SELECT  'Update vw_itens_venda_entrega',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN wmsprod_co.vw_itens_venda_entrega
ON bazayaco.tbl_bi_ops_delivery.item_id = vw_itens_venda_entrega.item_id
SET bazayaco.tbl_bi_ops_delivery.shipping_wms_tracking_code = vw_itens_venda_entrega.cod_rastreamento;

#OK rackspace
SELECT  'CALL bi_ops_tracking_code',now();
CALL bi_ops_tracking_code;

SELECT  'Update tms_tracks',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN bazayaco.tbl_bi_ops_guiasbyitem2
ON bazayaco.tbl_bi_ops_delivery.item_id = bazayaco.tbl_bi_ops_guiasbyitem2.item_id
SET bazayaco.tbl_bi_ops_delivery.shipping_courier_tracking_code = bazayaco.tbl_bi_ops_guiasbyitem2.total_guias;

SELECT  'Update sales_order_item_status',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN (bob_live_co.sales_order_item 
INNER JOIN bob_live_co.sales_order_item_status
ON sales_order_item.fk_sales_order_item_status 
= sales_order_item_status.id_sales_order_item_status)
ON bazayaco.tbl_bi_ops_delivery.item_id 
= sales_order_item.id_sales_order_item
SET bazayaco.tbl_bi_ops_delivery.status_bob = bob_live_co.sales_order_item_status.name;

SELECT  'Update status_itens_venda estoque_reservado',now();
UPDATE ((bazayaco.tbl_bi_ops_delivery
INNER JOIN wmsprod_co.itens_venda
ON bazayaco.tbl_bi_ops_delivery.item_id = itens_venda.item_id)
INNER JOIN wmsprod_co.status_itens_venda
ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id) 
SET bazayaco.tbl_bi_ops_delivery.date_procured = data
WHERE ((status_itens_venda.status)="Estoque reservado");

SELECT  'Update status_itens_venda facturado',now();
UPDATE ((bazayaco.tbl_bi_ops_delivery
INNER JOIN wmsprod_co.itens_venda
ON bazayaco.tbl_bi_ops_delivery.item_id = itens_venda.item_id)
INNER JOIN wmsprod_co.status_itens_venda
ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id) 
SET bazayaco.tbl_bi_ops_delivery.date_ready = data
WHERE status_itens_venda.status="faturado";

SELECT  'Actualizando nombres',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET customer_last_name = udf_cleanString(customer_last_name),
customer_first_name = udf_cleanString(customer_first_name);

SELECT  'Update status_itens_venda expedido',now();
UPDATE ((bazayaco.tbl_bi_ops_delivery
INNER JOIN wmsprod_co.itens_venda
ON bazayaco.tbl_bi_ops_delivery.item_id = itens_venda.item_id)
INNER JOIN wmsprod_co.status_itens_venda
ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id) 
SET bazayaco.tbl_bi_ops_delivery.date_shipped = date(status_itens_venda.data)
WHERE ((status_itens_venda.status)="Expedido");

SELECT  'Calculo Despachado el fin de semana',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN bazayaco.calendar
ON DATE(date_shipped)=dt
AND isweekend=1
SET shipped_is_weekend=1;

SELECT  'Update status_itens_venda expedido',now();
UPDATE ((bazayaco.tbl_bi_ops_delivery
INNER JOIN wmsprod_co.itens_venda
ON bazayaco.tbl_bi_ops_delivery.item_id = itens_venda.item_id)
INNER JOIN wmsprod_co.status_itens_venda
ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id) 
SET bazayaco.tbl_bi_ops_delivery.date_shipped_hr = TIME(status_itens_venda.data)
WHERE ((status_itens_venda.status)="Expedido");

SELECT  'Update status_itens_venda expedido',now();
UPDATE ((bazayaco.tbl_bi_ops_delivery
INNER JOIN wmsprod_co.itens_venda
ON bazayaco.tbl_bi_ops_delivery.item_id = itens_venda.item_id)
INNER JOIN wmsprod_co.status_itens_venda
ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id) 
SET
bazayaco.tbl_bi_ops_delivery.date_shipped_time = TIME(wmsprod_co.status_itens_venda.data),
bazayaco.tbl_bi_ops_delivery.shipped_night1 = if(TIME(wmsprod_co.status_itens_venda.data)<'08:00:00',1,0),
bazayaco.tbl_bi_ops_delivery.shipped_night2 = if(TIME(wmsprod_co.status_itens_venda.data)>'18:00:00',1,0)
WHERE ((wmsprod_co.status_itens_venda.status)="Expedido");

SELECT  'Update status_itens_venda expedido',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET tbl_bi_ops_delivery.shipped_night3=1
WHERE bazayaco.tbl_bi_ops_delivery.shipped_night1=1
OR bazayaco.tbl_bi_ops_delivery.shipped_night2=1;

SELECT  'Crear bazayaco.tbl_bi_ops_delivery_first_attempt',now();
TRUNCATE TABLE bazayaco.tbl_bi_ops_delivery_first_attempt;
INSERT INTO bazayaco.tbl_bi_ops_delivery_first_attempt (item_id, date_first_attempt)
SELECT bazayaco.tbl_bi_ops_delivery.item_id, Min(tms_status_delivery.date) as date_first_attempt
FROM (bazayaco.tbl_bi_ops_delivery
INNER JOIN wmsprod_co.vw_itens_venda_entrega
ON bazayaco.tbl_bi_ops_delivery.item_id = vw_itens_venda_entrega.item_id)
INNER JOIN wmsprod_co.tms_status_delivery
ON vw_itens_venda_entrega.cod_rastreamento = tms_status_delivery.cod_rastreamento
GROUP BY bazayaco.tbl_bi_ops_delivery.item_id, tms_status_delivery.id_status
HAVING (((tms_status_delivery.id_status)=5));

SELECT  'Update bazayaco.tbl_bi_ops_delivery_first_attempt',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN bazayaco.tbl_bi_ops_delivery_first_attempt ON bazayaco.tbl_bi_ops_delivery.item_id = bazayaco.tbl_bi_ops_delivery_first_attempt.item_id 
SET bazayaco.tbl_bi_ops_delivery.date_first_attempt = bazayaco.tbl_bi_ops_delivery_first_attempt.date_first_attempt;

SELECT 'Update presale',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
INNER JOIN wmsprod_co.itens_venda
ON bazayaco.tbl_bi_ops_delivery.item_id = itens_venda.item_id 
SET bazayaco.tbl_bi_ops_delivery.presale = 1
WHERE (((itens_venda.nome_produto) like '%preventa%'));

UPDATE bazayaco.tbl_bi_ops_delivery 
INNER JOIN bazayaco.tbl_catalog_product_v2
ON bazayaco.tbl_bi_ops_delivery.sku_simple = tbl_catalog_product_v2.sku
SET bazayaco.tbl_bi_ops_delivery.product_name = tbl_catalog_product_v2.product_name;

SELECT  'Calculo de promised_procurement_date',now();
#UPDATE bazayaco.tbl_bi_ops_delivery
#SET promised_procurement_date = workday_bi(date(date_exportable), supplier_procurement_time);
UPDATE        tbl_bi_ops_delivery 
   INNER JOIN calcworkdays
           ON     date( date_exportable )           = calcworkdays.date_last 
              AND workdays = supplier_procurement_time
              AND isweekday = 1 
              AND isholiday = 0
SET 
   promised_procurement_date = calcworkdays.date_first 
;

SELECT  'Calculo de promised_procurement_date semanas',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET
promised_procurement_week = weekofyear(promised_procurement_date), 
promised_procurement_month = month(promised_procurement_date), 
promised_procurement_month_name = monthname(promised_procurement_date),
promised_procurement_year = year(promised_procurement_date),
promised_procurement_yearmonth= CONCAT(promised_procurement_year,'-',promised_procurement_month),
promised_procurement_yearweek= CONCAT(promised_procurement_year,'-', promised_procurement_week);

#Actualiza la fecha para los productos en status backorder
Update bazayaco.tbl_bi_ops_delivery d inner join 
(select a.itens_venda_id, a.numero_order, a.item_id, b.date 
from (select itens_venda_id, numero_order, item_id from wmsprod_co.itens_venda) a inner join
(select itens_venda_id,max(data) date from wmsprod_co.status_itens_venda where status = 'backorder' group by itens_venda_id order by date desc) b
on a.itens_venda_id = b.itens_venda_id) t 
on d.item_id = t.item_id
set d.date_backorder = t.date;

#Actualiza la última fecha para los productos en status backorder_tratada
Update bazayaco.tbl_bi_ops_delivery d inner join 
(select a.itens_venda_id, a.numero_order, a.item_id, b.date 
from (select itens_venda_id, numero_order, item_id from wmsprod_co.itens_venda) a inner join
(select itens_venda_id,max(data) date from wmsprod_co.status_itens_venda where status = 'backorder_tratada' group by itens_venda_id order by date desc) b
on a.itens_venda_id = b.itens_venda_id) t 
on d.item_id = t.item_id
set d.date_backorder_tratada = t.date;

SELECT  'Calculo de promised_ready_to_ship_date',now();
#UPDATE bazayaco.tbl_bi_ops_delivery
#SET promised_ready_to_ship_date = workday_bi(date(promised_procurement_date), 1);
update tbl_bi_ops_delivery 
   INNER JOIN calcworkdays
           ON     date( promised_procurement_date ) = calcworkdays.date_last 
              AND workdays  = 1
              AND isweekday = 1
			        AND isholiday = 0
SET 
   promised_ready_to_ship_date = calcworkdays.date_first 
;

SELECT  'Calculo de promised_delivery_date',now();
#UPDATE bazayaco.tbl_bi_ops_delivery
#SET promised_delivery_date = workday_bi(date(date_exportable), if((max_delivery_time is null or max_delivery_time =''  or max_delivery_time=0),7,max_delivery_time));
UPDATE        tbl_bi_ops_delivery 
   INNER JOIN calcworkdays
           ON     date( date_exportable ) = calcworkdays.date_last 
              AND workdays = if((max_delivery_time is null or max_delivery_time =''  or max_delivery_time=0),7,max_delivery_time)
              AND isweekday = 1
			        AND isholiday = 0
SET 
   promised_delivery_date = calcworkdays.date_first 
;


SELECT  'Calculo de promised_delivery_promesalinio',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET
promised_delivery_date = 
			IF(HOUR(date_exportable)<15,
				workday_bi(DATE(date_exportable),2),
				workday_bi(DATE(date_exportable),3))
WHERE is_linio_promise =1
AND DATE(date_ordered)>='2013-03-12'
AND shipping_city in ('Bogotá','Cali','Medellin');

SELECT  'Calculo de promised_delivery_promesalinio not in (Bogotá,Cali,Medellin) and max=2',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET
promised_delivery_date = 
			IF(HOUR(date_exportable)<15,
				workday_bi(DATE(date_exportable),4),
				workday_bi(DATE(date_exportable),5))
WHERE is_linio_promise =1
AND DATE(date_ordered)>='2013-03-12'
AND max_delivery_time = 2
AND shipping_city not in ('Bogotá','Cali','Medellin');

SELECT  'Calculo de promised_delivery_date semanas',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET
promised_delivery_week = weekofyear(promised_delivery_date),
promised_deivery_month = month(promised_delivery_date),
promised_delivery_month_name = monthname(promised_delivery_date),
promised_delivery_year = year(promised_delivery_date),
promised_delivery_yearmonth= CONCAT(promised_delivery_year,'-',promised_deivery_month),
promised_delivery_yearweek= CONCAT(promised_delivery_year,'-', promised_delivery_week);

SELECT  'Calculo de checks',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET check_dates = 
If(date_ordered>date_exportable,"Date ordered > date exported",If(date_exportable>date_procured,"Date exportable > Date procured",
If(date_procured> date_shipped,"Date procured > Date shipped",
If(date_shipped>date_first_attempt,"Date shipped > Date first attempt",If(date_first_attempt>tbl_bi_ops_delivery.date_delivered,"Date delivered> Date 1st attempt","Correct"))))),
check_date_ordered= If(isNull(date_ordered),If((isNull(date_exportable) And isNull(date_procured) And isnull(date_shipped) And isnull(date_first_attempt) And isnull(tbl_bi_ops_delivery.date_delivered)),0,1),
If(date_ordered>date_exportable,1,0)),
check_date_exportable=If(IsNull(date_exportable),If(IsNull(date_procured) And IsNull(date_shipped) And IsNull(date_first_attempt) And IsNull(tbl_bi_ops_delivery.date_delivered),0,1),If(date_exportable>date_procured,1,0)),
check_date_procured = If(IsNull(date_shipped),If(IsNull(date_first_attempt) And IsNull(tbl_bi_ops_delivery.date_delivered),0,1),If(date_shipped>date_first_attempt,1,0)),
check_date_shipped = If(IsNull(date_shipped),If(IsNull(date_first_attempt) And IsNull(tbl_bi_ops_delivery.date_delivered),0,1),If(date_shipped>date_first_attempt,1,0));

SELECT  'Calculo de status',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN bazayaco.tbl_status_match_bob_wms
ON (tbl_bi_ops_delivery.status_wms = tbl_status_match_bob_wms.status_wms)
AND (tbl_bi_ops_delivery.status_bob = tbl_status_match_bob_wms.status_bob)
SET bazayaco.tbl_bi_ops_delivery.check_status_bob_wms = tbl_status_match_bob_wms.correct;

SELECT  'Check Máximo y Minimo tiempo de entrega',now();
UPDATE bazayaco.tbl_bi_ops_delivery AS a
SET a.check_min_max=1
WHERE min_delivery_time>=max_delivery_time;

SELECT  'Calculo de binarios',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET
procured = If(date_procured is not null,1,0),
shipped = If(date_shipped is not null,1,0),
attempted = If(date_first_attempt is not null,1,0),
delivered = If(tbl_bi_ops_delivery.date_delivered is not null,1,0);

SELECT  'Calculo de otros 2',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET
date_first_attempt = if(attempted=0 and delivered=1,tbl_bi_ops_delivery.date_delivered,date_first_attempt);



SELECT  'Calculo de leadtimes',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET 
leadtime_to_procure = dias_habiles(date(date_exportable),date(If(date_procured Is Null,curdate(),date_procured))),
leadtime_to_shipped = If(date_procured Is Null,Null,dias_habiles(date(date_procured) ,date(If(date_shipped Is Null,curdate(),date_shipped)))),
leadtime_to_first_attempt = If(date_shipped Is Null,Null,dias_habiles(date(date_shipped),date(If(date_first_attempt Is Null,curdate(),date_first_attempt)))),
leadtime_to_delivered = If(date_shipped Is Null,Null,dias_habiles(date(date_shipped),date(If(tbl_bi_ops_delivery.date_delivered Is Null,curdate(),tbl_bi_ops_delivery.date_delivered)))),
leadtime_first_attempt = If(date_shipped Is Null,Null,dias_habiles(date(date_exportable),date(If(date_first_attempt Is Null or date(date_first_attempt)<date(date_shipped),curdate(),date(date_first_attempt))))),
leadtime_f_a_wo_hd = If(date_shipped Is Null,Null,DATEDIFF(date(If((date(date_first_attempt)<date(date_shipped) or date_first_attempt Is Null),curdate(),date(date_first_attempt))),date(date_exportable))),
leadtime_delivery = If(date_shipped Is Null,Null,dias_habiles(date(date_exportable),date(If(tbl_bi_ops_delivery.date_delivered Is Null,curdate(),tbl_bi_ops_delivery.date_delivered)))),
leadtime_procured_delivered = If(date_shipped Is Null,Null,dias_habiles(date(date_procured),date(If(tbl_bi_ops_delivery.date_delivered Is Null,curdate(),tbl_bi_ops_delivery.date_delivered)))),
leadtime_exportable_delivered = If(date_exportable Is Null,Null,dias_habiles(date(date_exportable),date(If(tbl_bi_ops_delivery.date_delivered Is Null,curdate(),tbl_bi_ops_delivery.date_delivered)))),
leadtime_shipped_attempt =  If(date_shipped Is Null,Null,dias_habiles(date(date_shipped),date(If(date_first_attempt Is Null,curdate(),date_first_attempt)))),
leadtime_to_procure2 = dias_habiles(date(promised_procurement_date),date(If(date_procured Is Null,curdate(),date_procured)));

SELECT  'Update date_first_attempt_night',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET
tbl_bi_ops_delivery.date_first_attempt_night = if (date_first_attempt is null or date_first_attempt<'2012-01-01',null,bazayaco.workday_bi(tbl_bi_ops_delivery.date_first_attempt, shipped_night3));

SELECT  'Calculo de on times',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET 
ontime_to_procure = If(date(date_procured)<=promised_procurement_date,1,0),
on_time_r2s = If(date_ready is not null,(if(date_ready<=promised_ready_to_ship_date,1,0)),(if(promised_ready_to_ship_date<=curdate(),1,0))),
ontime_first_attempt = If(date_first_attempt<=promised_delivery_date,1,0),
ontime_first_attempt_wo_night = If(date_first_attempt_night<=promised_delivery_date,1,0),
ontime_delivery = If(tbl_bi_ops_delivery.date_delivered<=promised_delivery_date,1,0),
shipped_next_day = If(leadtime_to_first_attempt<=1,1,0);

SELECT  'Calculo de delays',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET 
delay_to_procure = If(date_procured Is Null,If(curdate()>promised_procurement_date,1,0),If(date_procured>promised_procurement_date,1,0)),
###delay_to_shipped es la fecha maxima de entrega menos dos dias habiles, antes de esta fecha
### bodega debe despachar
#si es fin de semana max_delivery_time-2 desde date_shipped, sino max_delivery_time-1.
delay_to_shipped = IF(shipped_is_weekend=1,(IF((IF(DATE(date_shipped) IS NULL,CURDATE(),DATE(date_shipped)))>(workday_bi(date(date_exportable), if((max_delivery_time is null or max_delivery_time =''  or max_delivery_time=0)
					,5,(max_delivery_time-1)))),1,0)),(IF((IF(DATE(date_shipped) IS NULL,CURDATE(),DATE(date_shipped)))>(workday_bi(date(date_exportable), if((max_delivery_time is null or max_delivery_time =''  or max_delivery_time=0)
					,5,(max_delivery_time-2)))),1,0))),
delay_to_first_attempt = If(leadtime_to_first_attempt>2,1,0),
delay_to_delivered = If(leadtime_to_delivered>1,1,0),
delay_first_attempt = If(date_first_attempt Is Null,If(curdate()>promised_delivery_date,1,0),If(date_first_attempt>promised_delivery_date,1,0)),
delay_delivery = If(tbl_bi_ops_delivery.date_delivered Is Null,If(curdate()>promised_delivery_date,1,0),If(tbl_bi_ops_delivery.date_delivered>promised_delivery_date,1,0));

SELECT  'Calculo de leadtimes de delays',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET 
leadtime_delay_to_procure = If(leadtime_to_procure-2<0,0,leadtime_to_procure-1), 
leadtime_delay_to_shipped = If(leadtime_to_shipped-2<0,0,leadtime_to_shipped-2), 
leadtime_delay_to_first_attempt = If(leadtime_to_first_attempt-2<0,0,leadtime_to_first_attempt-2), 
leadtime_delay_to_delivered = If(leadtime_to_delivered-1<0,0,leadtime_to_delivered-1), 
leadtime_delay_first_attempt = If(leadtime_first_attempt-max_delivery_time-2<0,0,leadtime_first_attempt-max_delivery_time-2), 
leadtime_delay_delivery = If(leadtime_delivery-max_delivery_time<0,0,leadtime_delivery-max_delivery_time);

SELECT  'Calculo de delaysn reason',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET 
delay_max_value = greatest(leadtime_delay_to_procure, leadtime_delay_to_shipped, leadtime_delay_to_first_attempt, leadtime_delay_to_delivered), 
delay_reason = If(delay_first_attempt=1,If(delay_max_value = leadtime_delay_to_procure, "Procurement",If(delay_max_value = leadtime_delay_to_shipped, "Warehouse",If(delay_max_value = leadtime_delay_to_first_attempt, "Courier first attempt","Courier after first attempt"))),"On Time");

SELECT  'Calculo de otros',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET 
shipped_same_day_procurement = If(date(date_procured)=date(date_shipped),1,0), 
shipped_same_day_order = If(date(date_ordered)=date(date_shipped),1,0), 
attempted_within_3_days_order = If(date_first_attempt - date_ordered<4,1,0), 
delivered_within_3_days_order = If(tbl_bi_ops_delivery.date_delivered - date_ordered<4,1,0), 
still_to_procure = If(date_procured is null,1,If(status_wms = 'quebado' or status_wms='quebra_tratada',1,0)), 
product_vol_weight = ((package_height * package_length * package_width)/5000), 
product_max_weight = If(product_vol_weight>product_weight,product_vol_weight, product_weight), 
early_to_procure = If(date_procured Is Null,0,If(TIMESTAMPDIFF(DAY, promised_procurement_date,date_procured)<0,1,0)), 
early_to_first_attempt = If(date_first_attempt Is Null,0,If(TIMESTAMPDIFF(DAY, promised_delivery_date,date_first_attempt)<0,1,0)),
ontime_first_attempt = If(ontime_delivery = 1,1,ontime_first_attempt);

SELECT  'Calculo de date_to_ship',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET
date_to_ship = workday_bi(date(date_exportable), if((max_delivery_time is null or max_delivery_time =''  or max_delivery_time=0),4,(max_delivery_time-3)));

SELECT  'Calculo de binarios 2',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET
attempted = If(delivered=1,1,attempted);

TRUNCATE TABLE bazayaco.tbl_bi_ops_shipping_backlog;

SELECT  'Crear tabla de backlog de shipping',now();
INSERT INTO bazayaco.tbl_bi_ops_shipping_backlog (tbl_bi_ops_shipping_backlog.date) 
SELECT distinct(date_to_ship)
FROM bazayaco.tbl_bi_ops_delivery
WHERE date_to_ship <= CURDATE() ORDER BY date_to_ship desc;

SELECT  'Actualizar backlog de shipping',now();
UPDATE bazayaco.tbl_bi_ops_shipping_backlog
SET shipping_backlog = shipping_backlog(date);

SELECT  'Actualizar procurement backlog',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET backlog_procurement = If((procured=0 AND shipped=0 AND leadtime_to_procure > supplier_procurement_time),1,0);

##### Trae Tracker historico
SELECT  'Traer Tracker historico',now();
UPDATE tbl_bi_ops_delivery as a
INNER JOIN tbl_bi_ops_trackers_history as b
ON a.item_id=b.item_id
SET a.tracker = b.tracker;

insert into production.table_monitoring_log
(country, table_name, updated_at, key_date, total_rows, total_rows_check)
values ('Colombia', 'tbl_bi_ops_trackers_history - tracker, item id',now(),now(), row_count(), row_count());

SELECT  'Traer OP de OMS',now();
UPDATE  tbl_bi_ops_delivery AS a
INNER JOIN wmsprod_co.itens_venda AS b
ON a.item_id=b.item_id
INNER JOIN wmsprod_co.estoque AS c
ON b.estoque_id=c.estoque_id
INNER JOIN wmsprod_co.itens_recebimento AS d
ON c.itens_recebimento_id=d.itens_recebimento_id
INNER JOIN wmsprod_co.recebimento AS e
ON d.recebimento_id=e.recebimento_id
SET a.op = e.inbound_document_identificator;

insert into production.table_monitoring_log
(country, table_name, updated_at, key_date, total_rows, total_rows_check)
values ('Colombia', 'tbl_bi_ops_delivery - OP',now(),now(), row_count(), row_count());

SELECT  'Traer OP de tbl_purchase_order (Marketplace y Dropshipping)',now();
UPDATE tbl_bi_ops_delivery AS a
INNER JOIN tbl_purchase_order AS b
ON a.item_id=b.itemid
SET a.op = b.oc;

insert into production.table_monitoring_log
(country, table_name, updated_at, key_date, total_rows, total_rows_check)
values ('Colombia', 'tbl_bi_ops_delivery - OP (marketplace y dropshipping)',now(),now(), row_count(), row_count());

SELECT  'Traer Tracker de OMS',now();
UPDATE tbl_bi_ops_delivery AS a
INNER JOIN
	(SELECT OP,sku_simple,tracker
	FROM tbl_bi_ops_procurement
	WHERE  tbl_bi_ops_procurement.is_cancelled=0
	AND tbl_bi_ops_procurement.is_deleted=0
	AND tracker not in ('','INACTIVO','LINIO','0')
	GROUP BY OP,sku_simple) AS b
ON (a.op=b.OP AND a.sku_simple=b.sku_simple)
SET a.tracker = b.tracker
WHERE b.tracker IS NOT NULL
AND a.tracker IS NULL
AND b.tracker not in ('','INACTIVO','LINIO','0');

insert into production.table_monitoring_log
(country, table_name, updated_at, key_date, total_rows, total_rows_check)
values ('Colombia', 'tbl_bi_ops_delivery - tracker from oms',now(),now(), row_count(), row_count());

SELECT  'Tracker de oms',now();
UPDATE tbl_bi_ops_delivery AS a
INNER JOIN procurement_co.catalog_supplier_attributes AS b
ON a.supplier_id=b.fk_catalog_supplier
SET a.tracker=b.tracker
WHERE a.tracker IS NULL
AND b.tracker not in ('','INACTIVO','LINIO','0');

insert into production.table_monitoring_log
(country, table_name, updated_at, key_date, total_rows, total_rows_check)
values ('Colombia', 'tbl_bi_ops_delivery - tracker oms catalog suppplier attributes',now(),now(), row_count(), row_count());

#SELECT  'Tracker de oms nombre',now();
#UPDATE tbl_bi_ops_delivery AS a
#INNER JOIN procurement_co.catalog_supplier_attributes AS b
#ON a.supplier_name=b.company_name
#SET a.tracker=b.tracker
#WHERE a.tracker IS NULL;

#### Actualización de los otros trackers Tabla trackers and supplier
SELECT  'Traer tracker tabla de trackers and supplier',now();
UPDATE tbl_bi_ops_delivery as a
INNER JOIN tbl_bi_ops_tracking_suppliers as b
ON b.supplier_name = a.supplier_name
SET a.tracker = b.tracker_name
WHERE a.tracker IS NULL;

insert into production.table_monitoring_log
(country, table_name, updated_at, key_date, total_rows, total_rows_check)
values ('Colombia', 'tbl_bi_ops_delivery - tracker from tracking suppliers',now(),now(), row_count(), row_count());

#### Guarda tracker
SELECT  'Guarda Tracker historico',now();
INSERT tbl_bi_ops_trackers_history (item_id,tracker,date_update)
SELECT item_id,tracker,curdate()
FROM tbl_bi_ops_delivery
WHERE tbl_bi_ops_delivery.tracker IS NOT NULL;

insert into production.table_monitoring_log
(country, table_name, updated_at, key_date, total_rows, total_rows_check)
values ('Colombia', 'tbl_bi_ops_trackers_history - Insert trackers históricos',now(),now(), row_count(), row_count());

SELECT  'Actualizar status tracking solo aplica para ordenes antes de oms',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN  tbl_purchase_order ON bazayaco.tbl_bi_ops_delivery.item_id = tbl_purchase_order.itemid
SET bazayaco.tbl_bi_ops_delivery.tracking_status = tbl_purchase_order.status_tracking,
tbl_bi_ops_delivery.tracking_status_date = tbl_purchase_order.fecha_tracking,
tbl_bi_ops_delivery.purchase_order = tbl_purchase_order.oc;

SELECT  'Actualizar status tracking solo aplica para ordenes antes de oms',now();
UPDATE bazayaco.tbl_purchase_order
INNER JOIN  bazayaco.tbl_bi_ops_delivery
ON tbl_purchase_order.itemid=tbl_bi_ops_delivery.item_id
SET  tbl_purchase_order.status_wms=tbl_bi_ops_delivery.status_wms;


SELECT  'Actualizar bazayaco.tbl_bi_ops_delivery fecha 31 de Dic 2012 como Enero',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET
promised_deivery_month =1,
promised_delivery_month_name= 'January',
promised_delivery_year = 2013  
WHERE promised_delivery_date='2012-12-31'; 

SELECT  'Actualizar bazayaco.tbl_bi_ops_delivery fecha 31 de Dic 2012 como Enero',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET
promised_procurement_month=1,
promised_procurement_month_name='January',
promised_procurement_year = 2013
WHERE promised_procurement_date ='2012-12-31';

SELECT  'Crear efectividad de entrega',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET
first_attempt_efective = 1
WHERE date_first_attempt =tbl_bi_ops_delivery.date_delivered;

SELECT  'Cancelados',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET
cancelled = 1
WHERE status_wms in ('Cancelado','quebra tratada','Quebrado');

SELECT  'Cancelados y enviados',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET
cancelled_shipped = 1
WHERE shipped=1 and cancelled = 1;

SELECT  'Actualizar guia y courrier de histórico',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN bazayaco.tbl_bi_ops_shipping_history
ON bazayaco.tbl_bi_ops_delivery.item_id=tbl_bi_ops_shipping_history.item_id
SET
bazayaco.tbl_bi_ops_delivery.shipping_wms_tracking_code=tbl_bi_ops_shipping_history.shipping_tracking_code_wms,
bazayaco.tbl_bi_ops_delivery.shipping_courier_tracking_code=tbl_bi_ops_shipping_history.shipping_courrier_tracking_code,
bazayaco.tbl_bi_ops_delivery.shipping_courier=tbl_bi_ops_shipping_history.courrier_name
WHERE shipping_wms_tracking_code IS NULL;

SELECT  'Actualizar Return1',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN bazayaco.tbl_bi_ops_returns
ON bazayaco.tbl_bi_ops_delivery.item_id=tbl_bi_ops_returns.item_id
SET
bazayaco.tbl_bi_ops_delivery.returns_courrier_docs=tbl_bi_ops_returns.return_docs
WHERE bazayaco.tbl_bi_ops_returns.return_docs=1;

SELECT  'Actualizar Return2',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN bazayaco.tbl_bi_ops_returns
ON bazayaco.tbl_bi_ops_delivery.item_id=tbl_bi_ops_returns.item_id
SET
bazayaco.tbl_bi_ops_delivery.returns_courrier_docs=tbl_bi_ops_returns.return_courrier
WHERE bazayaco.tbl_bi_ops_returns.return_courrier=1;

SELECT  'Update status_itens_venda expedido',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET
bazayaco.tbl_bi_ops_delivery.leadtime_procured_menora2 = if(tbl_bi_ops_delivery.leadtime_to_procure<=2,1,0);

SELECT  'Update status_itens_venda expedido',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET
tbl_bi_ops_delivery.leadtime_to_procure_2 = bazayaco.dias_habiles(date(date_exportable),date(If(date_procured Is Null,curdate(),date_procured)))
WHERE leadtime_procured_menora2=1;

SELECT  'Update note bob',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN bob_live_co.sales_order_item_status_history
ON bazayaco.tbl_bi_ops_delivery.item_id=sales_order_item_status_history.fk_sales_order_item
SET
tbl_bi_ops_delivery.note_cc = sales_order_item_status_history.note
WHERE stockout=1
AND fk_sales_order_item_status=9;

SELECT  'Update nueva order - oos',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET
tbl_bi_ops_delivery.nueva_orden_cc= (SUBSTR(tbl_bi_ops_delivery.note_cc,(SELECT INSTR(tbl_bi_ops_delivery.note_cc, '200')),9));

SELECT  'Update OOS recovered tbl_order_detail',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN tbl_order_detail ON tbl_order_detail.order_nr=tbl_bi_ops_delivery.nueva_orden_cc
SET
tbl_bi_ops_delivery.OOS_recovered = 1
WHERE nueva_orden_cc<>''
and status_wms in ('Quebrado','quebra tratada')
AND tbl_order_detail.oac=1
AND tbl_order_detail.returned=0;

insert into production.table_monitoring_log
(country, table_name, updated_at, key_date, total_rows, total_rows_check)
values ('Colombia', 'tbl_bi_ops_delivery - OOS_recovered',now(),now(), row_count(), row_count());

SELECT  'Update coupon_code REORDER OOS recovered from google docs CC',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN tbl_orders_reo_rep_inv
ON tbl_bi_ops_delivery.item_id=tbl_orders_reo_rep_inv.item_original
SET
tbl_bi_ops_delivery.newcoupon_reorder_replace = tbl_orders_reo_rep_inv.coupon_code,
tbl_bi_ops_delivery.item_id_reorder_replace = tbl_orders_reo_rep_inv.item_nueva
WHERE tbl_orders_reo_rep_inv.coupon_code like 'REO%';

SELECT  'Update coupon_code REORDER OOS recovered from google docs CC',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN bazayaco.tbl_orders_reo_rep_inv
ON bazayaco.tbl_bi_ops_delivery.item_id=tbl_orders_reo_rep_inv.item_original
SET
tbl_bi_ops_delivery.newcoupon_reorder_replace = tbl_orders_reo_rep_inv.coupon_code,
tbl_bi_ops_delivery.item_id_reorder_replace = tbl_orders_reo_rep_inv.item_nueva
WHERE tbl_orders_reo_rep_inv.coupon_code LIKE 'REO%';

SELECT  'Update coupon_code REPLACE OOS recovered from google docs CC',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN bazayaco.tbl_orders_reo_rep_inv
ON bazayaco.tbl_bi_ops_delivery.item_id=tbl_orders_reo_rep_inv.item_original
SET
bazayaco.tbl_bi_ops_delivery.newcoupon_reorder_replace = tbl_orders_reo_rep_inv.coupon_code,
bazayaco.tbl_bi_ops_delivery.item_id_reorder_replace = tbl_orders_reo_rep_inv.item_nueva
WHERE tbl_orders_reo_rep_inv.coupon_code LIKE 'REP%';

SELECT  'Update note tracking OOS',now();
UPDATE bazayaco.tbl_bi_ops_delivery
LEFT JOIN wmsprod_co.itens_venda
ON bazayaco.tbl_bi_ops_delivery.item_id=itens_venda.item_id
SET
bazayaco.tbl_bi_ops_delivery.note_tracking = itens_venda.obs;

SELECT  'Update clasificacion tracking OOS',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET
bazayaco.tbl_bi_ops_delivery.tracking_stockout= IF((SELECT INSTR(note_tracking,'OOS'))=0 or (SELECT INSTR(note_tracking,'OOS'))is null,
IF((SELECT INSTR(note_tracking,'OOT'))=0 or (SELECT INSTR(note_tracking,'OOT'))is null,
IF((SELECT INSTR(note_tracking,'PNP'))=0 or (SELECT INSTR(note_tracking,'PNP'))is null,
IF((SELECT INSTR(note_tracking,'DSE'))=0 or (SELECT INSTR(note_tracking,'DSE'))is null,
IF((SELECT INSTR(note_tracking,'POL'))=0 or (SELECT INSTR(note_tracking,'POL'))is null,
IF((SELECT INSTR(note_tracking,'DTE'))=0 or (SELECT INSTR(note_tracking,'DTE'))is null,
IF((SELECT INSTR(note_tracking,'SOE'))=0 or (SELECT INSTR(note_tracking,'SOE'))is null,
IF((SELECT INSTR(note_tracking,'CBC'))=0 or (SELECT INSTR(note_tracking,'CBC'))is null,
IF((SELECT INSTR(note_tracking,'DTL'))=0 or (SELECT INSTR(note_tracking,'DTL'))is null,
IF((SELECT INSTR(note_tracking,'OSW'))=0 or (SELECT INSTR(note_tracking,'OSW'))is null,
IF((SELECT INSTR(note_tracking,'UNS'))=0 or (SELECT INSTR(note_tracking,'UNS'))is null,
IF((SELECT INSTR(note_tracking,'OSM'))=0 or (SELECT INSTR(note_tracking,'OSM'))is null,
IF((SELECT INSTR(note_tracking,'SWC'))=0 or (SELECT INSTR(note_tracking,'SWC'))is null,
IF((SELECT INSTR(note_tracking,'OOA'))=0 or (SELECT INSTR(note_tracking,'OOA'))is null,
IF((SELECT INSTR(note_tracking,'STE'))=0 or (SELECT INSTR(note_tracking,'STE'))is null,'NA','STE'),'OOA'),'SWC'),'OSM'),'UNS'),'OSW'),'DTL'),'CBC'),'SOE'),'DTE'),'POL'),'DSE'),'PNP'),'OOT'),'OOS');

SELECT  'Calculo de Peso Volumetrico segun Courriers',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET 
Vol_Weight_Courrier= IF(shipping_courier='DESPACHOS SERVIENTREGAS',(((tbl_bi_ops_delivery.package_width*tbl_bi_ops_delivery.package_length*tbl_bi_ops_delivery.package_height)*222)/1000000),
IF(shipping_courier='DESPACHO DESPRISA',(((tbl_bi_ops_delivery.package_width*tbl_bi_ops_delivery.package_length*tbl_bi_ops_delivery.package_height)*400)/1000000),
(((tbl_bi_ops_delivery.package_width*tbl_bi_ops_delivery.package_length*tbl_bi_ops_delivery.package_height)*222)/1000000)));

SELECT  'Update OOS REAL',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET
bazayaco.tbl_bi_ops_delivery.stockout_real = 1
WHERE stockout=1
AND OOS_recovered is null;

update bazayaco.tbl_bi_ops_delivery p inner join wmsprod_co.itens_venda as t
on t.item_id = p.item_id
inner join wmsprod_co.status_itens_venda s
on t.itens_venda_id = s.itens_venda_id
set p.stockout_real = 1
where (s.status ='backorder_tratada');

SELECT  'Calculo de promised_backorder_cs_date',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET promised_backorder_cs_date = workday_bi(DATE(date_backorder), 1)
WHERE is_backorder=1;

SELECT  'Update bazayaco.tbl_bi_ops_stock_update',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN 
(SELECT sku_simple,max(date_update) as date_update FROM bazayaco.tbl_bi_ops_stock_update GROUP BY sku_simple) as t
ON bazayaco.tbl_bi_ops_delivery.sku_simple=t.sku_simple
SET bazayaco.tbl_bi_ops_delivery.date_update_stock = t.date_update;

SELECT  'Update days_wo_update_stock',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET bazayaco.tbl_bi_ops_delivery.days_wo_update_stock = if(date_update_stock IS NULL,NULL,(date(CURDATE())-date(date_update_stock)));

SELECT  'Update status_itens_venda expedido',now();
UPDATE ((bazayaco.tbl_bi_ops_delivery
INNER JOIN wmsprod_co.itens_venda
ON bazayaco.tbl_bi_ops_delivery.item_id = itens_venda.item_id)
INNER JOIN wmsprod_co.status_itens_venda
ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id) 
SET bazayaco.tbl_bi_ops_delivery.date_shipped_hr2 = status_itens_venda.data
WHERE ((status_itens_venda.status)="Expedido");

#SELECT  'Update Fecha de entrega igual a fecha de envío',now();
#UPDATE bazayaco.tbl_bi_ops_delivery
#SET bazayaco.tbl_bi_ops_delivery.date_delivered=tbl_bi_ops_delivery.date_shipped
#WHERE fulfilment_real='Dropshipping';

SELECT  'Update is_market_place',now();
UPDATE bazayaco.tbl_bi_ops_delivery as a
INNER JOIN (SELECT itemid FROM bazayaco.tbl_purchase_order WHERE is_marketplace=1) AS b
ON a.item_id=b.itemid
LEFT JOIN bazayaco.tbl_marketplace AS c
ON a.supplier_name=c.supplier
SET a.is_marketplace=1;

SELECT  'Update is_market_place2',now();
UPDATE bazayaco.tbl_bi_ops_delivery AS a
INNER JOIN bazayaco.tbl_exc_marketplace AS b
ON a.sku_config=b.sku_config
SET a.is_marketplace=1;

###############################################################
#################INDICADORES LOGISTICA INVERSA#################
###############################################################
SELECT  'update action logistica inversa',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN bob_live_co.sales_order_item_return a 
ON bazayaco.tbl_bi_ops_delivery.item_id=a.fk_sales_order_item
LEFT JOIN bob_live_co.automatic_return_lists b 
ON a.fk_return_list_reason=b.id_automatic_return_lists
LEFT JOIN bob_live_co.automatic_return_lists c
ON a.fk_return_list_action=c.id_automatic_return_lists  
SET 
action_inverse_logistic=c.name
WHERE c.list_type='action';

SELECT  'Correción action_inverse_logistic',now();
#Debido a que los primeros registros en BOB estan mal
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
action_inverse_logistic='Cupón (5 días hábiles)'
WHERE date_store_credit_issued is not null;

SELECT  'update reason logistica inversa',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN bob_live_co.sales_order_item_return a 
ON bazayaco.tbl_bi_ops_delivery.item_id=a.fk_sales_order_item
LEFT JOIN bob_live_co.automatic_return_lists b 
ON a.fk_return_list_reason=b.id_automatic_return_lists
SET 
reason_inverse_logistic=b.name
WHERE b.list_type='reason';

SELECT  'Update date_delivered BOB',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN bob_live_co.sales_order_item_status_history as b
ON bazayaco.tbl_bi_ops_delivery.item_id=b.fk_sales_order_item
SET bazayaco.tbl_bi_ops_delivery.date_delivered_bob=b.created_at
WHERE fk_sales_order_item_status=68;

SELECT  'Update date_automatic_return BOB',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN bob_live_co.sales_order_item_status_history as b
ON bazayaco.tbl_bi_ops_delivery.item_id=b.fk_sales_order_item
SET bazayaco.tbl_bi_ops_delivery.date_automatic_return=b.created_at
WHERE fk_sales_order_item_status=132;

SELECT  'Update date_track_return',now();
#se toma el segundo estado de aguardando retorno ya que el primero es el que hace el sistema automaticamente
#y el segundo es luego de aguardando agendamiento
UPDATE bazayaco.tbl_bi_ops_delivery as t1
INNER JOIN 
(SELECT a.item_id,c.changed_at FROM bazayaco.tbl_bi_ops_delivery as a
INNER JOIN wmsprod_co.inverselogistics_devolucion as b
ON a.item_id=b.item_id
INNER JOIN wmsprod_co.inverselogistics_status_history as c
ON b.id=c.return_id
LEFT JOIN
	(SELECT a.order_number,a.item_id,c.status,c.changed_at,c.id FROM bazayaco.tbl_bi_ops_delivery as a
	INNER JOIN wmsprod_co.inverselogistics_devolucion as b
	ON a.item_id=b.item_id
	INNER JOIN wmsprod_co.inverselogistics_status_history as c
	ON b.id=c.return_id
	WHERE c.status='aguardando_retorno'
	GROUP BY a.item_id
	ORDER by changed_at ASC) AS primer_aguardando_retorno
ON c.id=primer_aguardando_retorno.id
WHERE c.status='aguardando_retorno'
AND primer_aguardando_retorno.id IS NULL
GROUP BY item_id
ORDER by c.changed_at ASC) AS t2
ON t1.item_id=t2.item_id
SET t1.date_track_return=t2.changed_at;

SELECT  'Update date_track_return2',now();
#se toma el ultimo estado de aguardando retorno para medir a logistica
UPDATE bazayaco.tbl_bi_ops_delivery as t1
INNER JOIN 
(SELECT a.item_id,c.changed_at FROM bazayaco.tbl_bi_ops_delivery as a
INNER JOIN wmsprod_co.inverselogistics_devolucion as b
ON a.item_id=b.item_id
INNER JOIN wmsprod_co.inverselogistics_status_history as c
ON b.id=c.return_id
LEFT JOIN
	(SELECT a.order_number,a.item_id,c.status,max(c.changed_at),c.id FROM bazayaco.tbl_bi_ops_delivery as a
	INNER JOIN wmsprod_co.inverselogistics_devolucion as b
	ON a.item_id=b.item_id
	INNER JOIN wmsprod_co.inverselogistics_status_history as c
	ON b.id=c.return_id
	WHERE c.status='aguardando_retorno'
	GROUP BY a.item_id
	ORDER by changed_at ASC) AS ultimo_aguardando_retorno
ON c.id=ultimo_aguardando_retorno.id
WHERE c.status='aguardando_retorno'
AND ultimo_aguardando_retorno.id IS NULL
GROUP BY item_id
ORDER by c.changed_at ASC) AS t2
ON t1.item_id=t2.item_id
SET t1.date_track_return2=t2.changed_at;

SELECT  'update fecha recepcion en bodega devolucion',now();
UPDATE bazayaco.tbl_bi_ops_delivery as a
INNER JOIN bazayaco.tbl_bi_ops_ingreso_bodega_devolucion as b
ON a.item_id=b.item_id
SET 
a.date_inbound_warehouse_return=b.date_ingreso_bodega_devolucion;

SELECT  'update fecha recepcion en bodega devolucion desde archivos servientrega',now();
UPDATE tbl_bi_ops_delivery AS t1
INNER JOIN
(SELECT item_id,carrier_tracking_code,b.fecha_entrega
FROM wmsprod_co.inverselogistics_devolucion AS a
LEFT JOIN
(SELECT numero_guia,DATE(max(fecha_entrega)) AS fecha_entrega FROM outbound_co.tbl_outbound_daily_servientrega
WHERE nombre_estadoenvio='ENTREGADO'
AND fecha_entrega <> '1900-01-00 00:00:00'
GROUP BY numero_guia) as b
ON a.carrier_tracking_code=b.numero_guia) AS t2
ON t1.item_id=t2.item_id
SET t1.date_inbound_warehouse_return = t2.fecha_entrega
WHERE t1.date_inbound_warehouse_return IS NULL;

SELECT  'Update date_refund_needed BOB',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN bob_live_co.sales_order_item_status_history as b
ON bazayaco.tbl_bi_ops_delivery.item_id=b.fk_sales_order_item
SET bazayaco.tbl_bi_ops_delivery.date_refund_needed=b.created_at
WHERE fk_sales_order_item_status=119;

SELECT  'Update date_clarify_refund_needed BOB',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN bob_live_co.sales_order_item_status_history as b
ON bazayaco.tbl_bi_ops_delivery.item_id=b.fk_sales_order_item
SET bazayaco.tbl_bi_ops_delivery.date_clarify_refund_needed=b.created_at
WHERE fk_sales_order_item_status=135;

SELECT  'Update date_cash_refunded BOB',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN bob_live_co.sales_order_item_status_history as b
ON bazayaco.tbl_bi_ops_delivery.item_id=b.fk_sales_order_item
SET bazayaco.tbl_bi_ops_delivery.date_cash_refunded=b.created_at
WHERE fk_sales_order_item_status=134;

SELECT  'Update date_store_credit_issued BOB',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN bob_live_co.sales_order_item_status_history as b
ON bazayaco.tbl_bi_ops_delivery.item_id=b.fk_sales_order_item
SET bazayaco.tbl_bi_ops_delivery.date_store_credit_issued=b.created_at
WHERE fk_sales_order_item_status=113;

SELECT  'Update date_closed BOB',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN bob_live_co.sales_order_item_status_history as b
ON bazayaco.tbl_bi_ops_delivery.item_id=b.fk_sales_order_item
SET bazayaco.tbl_bi_ops_delivery.date_closed=b.created_at
WHERE fk_sales_order_item_status=6;

SELECT  'Update is devolucion',now();
UPDATE bazayaco.tbl_bi_ops_delivery as a
INNER JOIN wmsprod_co.inverselogistics_devolucion  as b
ON a.item_id=b.item_id
SET a.is_refund=1;

############## CALCULO DE DIAS NORMALES ##############
SELECT  'Calculo nd_delivered_to_returnaskedautomatic',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
nd_delivered_to_returnaskedautomatic = 
IF(date_delivered_bob IS NULL,
		NULL,
	DATEDIFF(date(date_automatic_return),date(date_delivered_bob)))
WHERE is_refund=1
AND date_automatic_return IS NOT NULL;

SELECT  'Calculo nd_returnaskedautomatic_to_trackreturn',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
nd_returnaskedautomatic_to_trackreturn = 
	IF(date_automatic_return IS NULL,
		NULL,
		DATEDIFF(date(date_track_return),date(date_automatic_return)))
WHERE is_refund=1
AND date_track_return IS NOT NULL;

SELECT  'Calculo nd_trackreturn_to_inboundwarehouse',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
nd_trackreturn_to_inboundwarehouse = 
	IF(date_track_return2 IS NULL,
		NULL,
		DATEDIFF(date(date_inbound_warehouse_return),date(date_track_return2)))
WHERE is_refund=1
AND date_inbound_warehouse_return IS NOT NULL;

SELECT  'Calculo nd_returnaskedautomatic_inboundwarehouse',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
nd_returnaskedautomatic_inboundwarehouse = 
	IF(date_automatic_return IS NULL,
		NULL,
		DATEDIFF(date(date_inbound_warehouse_return),date(date_automatic_return)))
WHERE is_refund=1
AND date_inbound_warehouse_return is not null;

SELECT  'Calculo nd_inboundwarehouse_refundneeded',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
nd_inboundwarehouse_refundneeded = 
	IF(date_inbound_warehouse_return IS NULL,
		NULL,
		DATEDIFF(date(date_refund_needed),date(date_inbound_warehouse_return)))
WHERE is_refund=1
AND date_refund_needed is not null;

SELECT  'Calculo nd_returnaskedautomatic_to_refundneeded',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
nd_returnaskedautomatic_to_refundneeded = 
	IF(date_automatic_return IS NULL,
		NULL,
		DATEDIFF(date(date_refund_needed),date(date_automatic_return)))
WHERE is_refund=1
AND date_refund_needed IS NOT NULL;

SELECT  'Calculo nd_returnaskedautomatic_cashrefunded',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
nd_returnaskedautomatic_cashrefunded = 
	IF((date_automatic_return IS NULL),
		NULL,
		DATEDIFF(date(date_cash_refunded),date(date_automatic_return)))
WHERE is_refund=1
AND date_cash_refunded IS NOT NULL;

SELECT  'Calculo nd_refundneeded_cashrefunded',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET 
nd_refundneeded_cashrefunded = 
	IF(date_refund_needed IS NULL,
		NULL,
		DATEDIFF(date(date_cash_refunded),date(date_refund_needed)))
WHERE is_refund=1
AND date_cash_refunded IS NOT NULL;

SELECT  'Calculo nd_returnaskedautomatic_storecreditissued',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
nd_returnaskedautomatic_storecreditissued = 
	IF((date_automatic_return IS NULL),
		NULL,
		DATEDIFF(date(date_store_credit_issued),date(date_automatic_return)))
WHERE is_refund=1
AND date_store_credit_issued IS NOT NULL;

SELECT  'Calculo nd_refundneeded_storecreditissued',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
nd_refundneeded_storecreditissued = 
	IF(date_refund_needed IS NULL,
		NULL,
		DATEDIFF(date(date_store_credit_issued),date(date_refund_needed)))
WHERE is_refund=1
AND date_store_credit_issued IS NOT NULL;

SELECT  'Calculo nd_clarifyrefundneeded_cashrefunded',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET 
nd_clarifyrefundneeded_cashrefunded = 
	IF(date_clarify_refund_needed IS NULL,
		NULL,
		DATEDIFF(date(date_cash_refunded),date(date_clarify_refund_needed)))
WHERE is_refund=1
AND date_cash_refunded IS NOT NULL;

SELECT  'Calculo nd_clarifyrefundneeded_storecreditissued',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET 
nd_clarifyrefundneeded_storecreditissued = 
	IF(date_clarify_refund_needed IS NULL,
		NULL,
		DATEDIFF(date(date_store_credit_issued),date(date_clarify_refund_needed)))
WHERE is_refund=1
AND date_store_credit_issued IS NOT NULL;

SELECT  'Calculo nd_returnaskedautomatic_closed',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
nd_returnaskedautomatic_closed = 
	IF(date_automatic_return IS NULL,
		NULL,
		DATEDIFF(date(date_closed),date(date_automatic_return)))
WHERE is_refund=1
AND date_closed IS NOT NULL;

############## DIAS HABILES ##############
SELECT  'Calculo wd_delivered_to_returnaskedautomaticc',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
wd_delivered_to_returnaskedautomatic = 
	dias_habiles(date(date_delivered_bob),date(date_automatic_return))
WHERE is_refund=1
AND date_automatic_return IS NOT NULL;

SELECT  'Calculo wd_returnaskedautomatic_to_trackreturn',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
wd_returnaskedautomatic_to_trackreturn = 
	IF(date_automatic_return IS NULL,
		NULL,
		dias_habiles(date(date_automatic_return),date(date_track_return)))
WHERE is_refund=1
AND date_track_return IS NOT NULL;

SELECT  'Calculo wd_trackreturn_to_inboundwarehouse',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
wd_trackreturn_to_inboundwarehouse = 
	IF(date_track_return2 IS NULL,
		NULL,
		dias_habiles(date(date_track_return2),date(date_inbound_warehouse_return)))
WHERE is_refund=1
AND date_inbound_warehouse_return IS NOT NULL;

SELECT  'Calculo wd_returnaskedautomatic_inboundwarehouse',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET 
wd_returnaskedautomatic_inboundwarehouse = 
	IF((date_automatic_return IS NULL),
		NULL,
		dias_habiles(date(date_automatic_return),date(date_inbound_warehouse_return)))
WHERE is_refund=1
AND date_inbound_warehouse_return IS NOT NULL;

SELECT  'Calculo wd_inboundwarehouse_refundneeded',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET 
wd_inboundwarehouse_refundneeded = 
	IF((date_inbound_warehouse_return IS NULL),
		NULL,
		dias_habiles(date(date_inbound_warehouse_return),date(date_refund_needed)))
WHERE is_refund=1
AND date_refund_needed IS NOT NULL;

SELECT  'Calculo wd_returnaskedautomatic_to_refundneeded',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
wd_returnaskedautomatic_to_refundneeded = 
	IF(date_automatic_return IS NULL,
		NULL,
		dias_habiles(date(date_automatic_return),date(date_refund_needed)))
WHERE is_refund=1
AND date_refund_needed IS NOT NULL;

SELECT  'Calculo wd_returnaskedautomatic_cashrefunded',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET 
wd_returnaskedautomatic_cashrefunded = 
	IF((date_automatic_return IS NULL),
		NULL,
		dias_habiles(date(date_automatic_return),date(date_cash_refunded)))
WHERE is_refund=1
AND date_cash_refunded IS NOT NULL;

SELECT  'Calculo wd_refundneeded_cashrefunded',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET 
wd_refundneeded_cashrefunded = 
	IF(date_refund_needed IS NULL,
		NULL,
		dias_habiles(date(date_refund_needed),date(date_cash_refunded)))
WHERE is_refund=1
AND date_cash_refunded IS NOT NULL;

SELECT  'Calculo wd_returnaskedautomatic_storecreditissued',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET 
wd_returnaskedautomatic_storecreditissued = 
	IF((date_automatic_return IS NULL),
		NULL,
		dias_habiles(date(date_automatic_return),date(date_store_credit_issued)))
WHERE is_refund=1
AND date_store_credit_issued IS NOT NULL;

SELECT  'Calculo wd_refundneeded_storecreditissued',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET 
wd_refundneeded_storecreditissued = 
	IF(date_refund_needed IS NULL,
		NULL,
		dias_habiles(date(date_refund_needed),date(date_store_credit_issued)))
WHERE is_refund=1
AND date_store_credit_issued IS NOT NULL;

SELECT  'Calculo wd_clarifyrefundneeded_cashrefunded',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET 
wd_clarifyrefundneeded_cashrefunded = 
	IF(date_clarify_refund_needed IS NULL,
		NULL,
		dias_habiles(date(date_clarify_refund_needed),date(date_cash_refunded)))
WHERE is_refund=1
AND date_cash_refunded IS NOT NULL;

SELECT  'Calculo wd_clarifyrefundneeded_storecreditissued',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
wd_clarifyrefundneeded_storecreditissued = 
	IF(date_clarify_refund_needed IS NULL,
		NULL,
		dias_habiles(date(date_clarify_refund_needed),date(date_store_credit_issued)))
WHERE is_refund=1
AND date_store_credit_issued IS NOT NULL;

SELECT  'Calculo wd_returnaskedautomatic_closed',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
wd_returnaskedautomatic_closed = 
	IF(date_automatic_return IS NULL,
		NULL,
		dias_habiles(date(date_automatic_return),date(date_closed)))
WHERE is_refund=1
AND date_closed IS NOT NULL;

########### BACKLOG LOGISTICA INVERSA ##############
SELECT  'Calculo backlog_delivered_return',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
backlog_delivered_return = 
	IF((dias_habiles(date(date_delivered_bob),curdate()))>5,1,0)
and is_refund=1
AND date_closed is null
AND date_automatic_return IS NULL;

SELECT  'Calculo backlog_returns_track',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
backlog_returns_track = 
	IF(dias_habiles(date(date_automatic_return),curdate())>2,1,0)
WHERE date_track_return is null
AND date_inbound_warehouse_return IS null
AND date_refund_needed IS NULL
AND date_closed is null
AND item_id NOT IN (283410);
#Se reporta inconsistencia en los estados del anterior item;

SELECT  'Calculo backlog_returns_track_ops',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
backlog_returns_track_ops = 
	IF(dias_habiles(date(date_track_return),curdate())>3,1,0)
WHERE date_inbound_warehouse_return is null
AND is_refund=1
AND date_closed is null;

SELECT  'Calculo backlog_returns_ops',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
backlog_returns_ops = 
	IF(dias_habiles(date(date_inbound_warehouse_return),curdate())>1,1,0)
WHERE date_refund_needed is null
AND date_closed is null
AND is_refund=1;

SELECT  'Calculo backlog_returns_finance_consignment',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
backlog_returns_finance_consignment = 
	IF(dias_habiles(date(date_refund_needed),curdate())>5,1,0)
WHERE date_cash_refunded is null
AND date_store_credit_issued IS NULL
AND date_closed is null
AND date_refund_needed IS NOT NULL
AND is_refund=1
AND action_inverse_logistic='Consignacion bancaria (7 dias hábiles)';

SELECT  'Calculo backlog_returns_finance_effecty',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
backlog_returns_finance_effecty = 
	IF(dias_habiles(date(date_refund_needed),curdate())>4,1,0)
WHERE date_cash_refunded is null
AND date_store_credit_issued IS NULL
AND date_closed is null
AND date_refund_needed IS NOT NULL
AND is_refund=1
AND action_inverse_logistic='Efecty (7 dias hábiles)';

SELECT  'Calculo backlog_returns_finance_reversion',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
backlog_returns_finance_reversion = 
	IF(dias_habiles(date(date_refund_needed),curdate())>20,1,0)
WHERE date_cash_refunded is null
AND date_store_credit_issued IS NULL
AND date_closed is null
AND is_refund=1
AND date_refund_needed IS NOT NULL
AND action_inverse_logistic='Reversión TC (25-30 dias hábiles)';

SELECT  'Calculo backlog_returns_cs',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
backlog_returns_cs = 
	IF(dias_habiles(date(date_refund_needed),curdate())>2,1,0)
WHERE date_store_credit_issued is null
AND date_cash_refunded IS NULL
AND date_closed is null
AND date_refund_needed IS NOT NULL
AND is_refund=1
AND action_inverse_logistic='Cupón (5 días hábiles)';

######################### PAYMENT TERMS #########################
SELECT  'date_procured_1wd',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET date_procured_1wd=workday_bi(date_procured,1)
WHERE date_procured IS NOT NULL;

SELECT  'Update is_cinemark',now();
UPDATE bazayaco.tbl_bi_ops_delivery
SET bazayaco.tbl_bi_ops_delivery.is_cinemark=1
WHERE sku_simple in ('CI878OT79HFCLACOL-157154','CI878OT80HFBLACOL-157153');

SELECT  'Update New cat 1',now();
UPDATE bazayaco.tbl_bi_ops_delivery as a
INNER JOIN bazayaco.tbl_catalog_product_v2 as b
ON a.sku_simple=b.sku
SET 
a.new_cat1 = b.new_cat1;


SELECT  'Calculo nd_returnaskedautomatic_cashrefunded',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
INNER JOIN bob_live_co.sales_order_item_return a 
ON bazayaco.tbl_bi_ops_delivery.item_id=a.fk_sales_order_item
LEFT JOIN bob_live_co.automatic_return_lists b 
ON a.fk_return_list_reason=b.id_automatic_return_lists
SET 
nd2_returnaskedautomatic_cashrefunded = 
	IF((date_automatic_return IS NULL),
		NULL,
		DATEDIFF(if((date_cash_refunded Is Null),curdate(),date(date_cash_refunded)),date(date_automatic_return)))
WHERE is_refund=1
AND action_inverse_logistic<>'Cupón (5 días hábiles)';

SELECT  'Calculo nd_returnaskedautomatic_storecreditissued',now();
UPDATE bazayaco.tbl_bi_ops_delivery 
INNER JOIN bob_live_co.sales_order_item_return a 
ON bazayaco.tbl_bi_ops_delivery.item_id=a.fk_sales_order_item
LEFT JOIN bob_live_co.automatic_return_lists b 
ON a.fk_return_list_reason=b.id_automatic_return_lists
LEFT JOIN bob_live_co.automatic_return_lists c
ON a.fk_return_list_action=c.id_automatic_return_lists 
SET 
nd2_returnaskedautomatic_storecreditissued = 
	IF((date_automatic_return IS NULL),
		NULL,
		DATEDIFF(if((date_store_credit_issued Is Null),curdate(),date(date_store_credit_issued)),date(date_automatic_return)))
WHERE is_refund=1
AND action_inverse_logistic='Cupón (5 días hábiles)';

SELECT  'Calculo nd_returnaskedautomatic_cashrefunded',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN bob_live_co.sales_order_item_return a 
ON bazayaco.tbl_bi_ops_delivery.item_id=a.fk_sales_order_item
LEFT JOIN bob_live_co.automatic_return_lists b 
ON a.fk_return_list_reason=b.id_automatic_return_lists
LEFT JOIN bob_live_co.automatic_return_lists c
ON a.fk_return_list_action=c.id_automatic_return_lists 
SET 
wd2_returnaskedautomatic_cashrefunded = 
	IF((date_automatic_return IS NULL),
		NULL,
		dias_habiles(date(date_automatic_return),if((date_cash_refunded Is Null),curdate(),date(date_cash_refunded))))
WHERE is_refund=1
AND action_inverse_logistic<>'Cupón (5 días hábiles)';

SELECT  'Calculo nd_returnaskedautomatic_storecreditissued',now();
UPDATE bazayaco.tbl_bi_ops_delivery
INNER JOIN bob_live_co.sales_order_item_return a 
ON bazayaco.tbl_bi_ops_delivery.item_id=a.fk_sales_order_item
LEFT JOIN bob_live_co.automatic_return_lists b 
ON a.fk_return_list_reason=b.id_automatic_return_lists
LEFT JOIN bob_live_co.automatic_return_lists c
ON a.fk_return_list_action=c.id_automatic_return_lists 
SET 
wd2_returnaskedautomatic_storecreditissued = 
	IF((date_automatic_return IS NULL),
		NULL,
		dias_habiles(date(date_automatic_return),if((date_store_credit_issued Is Null),curdate(),date(date_store_credit_issued))))
WHERE is_refund=1
AND action_inverse_logistic<>'Cupón (5 días hábiles)';

SELECT  'Query de Andrea Bernal',now();
call bi_ops_bodega();

#SELECT  'freight_cost_cat_sku',now();
#call freight_cost_cat_sku();

#SELECT  'handling_cost_sku',now();
#call handling_cost_sku();

#OK rackspace
SELECT  'Macro de facturas',now();
call bi_ops_invoice_report();


#Actualiza si un product es tarjeta de regalo de linio
update bazayaco.tbl_bi_ops_delivery
set is_egift_card = 1
where supplier_name like '%Linio E-Gift Card%' OR supplier_name like '%Linio Colombia S.A.S (E-Gift Card)%';

#Boletas Cinemark en Own Warehouse
UPDATE bazayaco.tbl_bi_ops_delivery
SET fulfilment_real = "Own Warehouse"
WHERE sku_config IN ('CI878OT80HFBLACOL','CI878OT79HFCLACOL', 'CI878OT49JXMLACOL');

SELECT  'Query de Johnatan Adames',now();
CALL bi_ops_shipped_tracking_code();

#Actualiza la mínima fecha de entrega
UPDATE bazayaco.tbl_bi_ops_delivery SET
min_delivery_date = workday_bi(date(date_exportable), (min_delivery_time-1))
where min_delivery_time <> 0;

#Actualiza la fecha de los quebrados
Update bazayaco.tbl_bi_ops_delivery d inner join 
(select a.itens_venda_id, a.numero_order, a.item_id, b.date 
from (select itens_venda_id, numero_order, item_id from wmsprod_co.itens_venda) a inner join
(select itens_venda_id,max(data) date from wmsprod_co.status_itens_venda where status = 'Quebrado' group by itens_venda_id order by date desc) b
on a.itens_venda_id = b.itens_venda_id) t 
on d.item_id = t.item_id
set d.last_quebra_date = t.date;

UPDATE bazayaco.tbl_bi_ops_delivery
SET
bazayaco.tbl_bi_ops_delivery.quebra_sugerido = if(note_tracking regexp 'LACOL|SUGERIR', 'SI', 'NO')
where last_quebra_date is not null;

UPDATE bazayaco.tbl_bi_ops_delivery
SET
bazayaco.tbl_bi_ops_delivery.quebra_sugerido = if(note_tracking not regexp 'LACOL' = 1, 'NO', 'SI')
where last_quebra_date is not null;

#Actualiza la fecha de los quebrados tratados
Update bazayaco.tbl_bi_ops_delivery d inner join 
(select a.itens_venda_id, a.numero_order, a.item_id, b.date 
from (select itens_venda_id, numero_order, item_id from wmsprod_co.itens_venda) a inner join
(select itens_venda_id,max(data) date from wmsprod_co.status_itens_venda where status = 'quebra tratada' group by itens_venda_id order by date desc) b
on a.itens_venda_id = b.itens_venda_id) t 
on d.item_id = t.item_id
set d.last_quebra_tratada_date = t.date;

UPDATE bazayaco.tbl_bi_ops_delivery a
	INNER JOIN wmsprod_co.itens_venda AS b
	ON a.item_id=b.item_id
	INNER JOIN
	(SELECT itens_venda_id,max(data) date FROM wmsprod_co.status_itens_venda WHERE status = 'estoque reservado'
	GROUP BY itens_venda_id) AS c
ON b.itens_venda_id=c.itens_venda_id
SET a.date_estoque_reservado = c.date;

UPDATE bazayaco.tbl_bi_ops_delivery a
	INNER JOIN wmsprod_co.itens_venda AS b
	ON a.item_id=b.item_id
	INNER JOIN
	(SELECT itens_venda_id,max(data) date FROM wmsprod_co.status_itens_venda WHERE status = 'Analisando quebra'
	GROUP BY itens_venda_id) AS c
ON b.itens_venda_id=c.itens_venda_id
SET a.date_analisando_quebra = c.date;


UPDATE bazayaco.tbl_bi_ops_delivery a
	INNER JOIN wmsprod_co.itens_venda AS b
	ON a.item_id=b.item_id
	INNER JOIN
	(SELECT itens_venda_id,max(data) date FROM wmsprod_co.status_itens_venda WHERE status = 'Waiting dropshipping'
	GROUP BY itens_venda_id) AS c
ON b.itens_venda_id=c.itens_venda_id
SET a.date_waiting_dropshipping = c.date;


UPDATE bazayaco.tbl_bi_ops_delivery a
	INNER JOIN wmsprod_co.itens_venda AS b
	ON a.item_id=b.item_id
	INNER JOIN
	(SELECT itens_venda_id,max(data) date FROM wmsprod_co.status_itens_venda WHERE status = 'Aguardando estoque'
	GROUP BY itens_venda_id) AS c
ON b.itens_venda_id=c.itens_venda_id
SET a.date_aguardando_estoque = c.date;

#Checkea los items que ya han sido tratados después de la fecha de quebra
Update bazayaco.tbl_bi_ops_delivery d
set d.is_quebra_tratada = 1
where d.last_quebra_date is not null and d.last_quebra_tratada_date is not null
and d.last_quebra_date < d.last_quebra_tratada_date;

Update bazayaco.tbl_bi_ops_delivery
set wd_to_backlog = dias_habiles_negativos(curdate(), promised_procurement_date)
where date_procured is null;

Update bazayaco.tbl_bi_ops_delivery
set customer_backlog = dias_habiles_negativos(curdate(), promised_delivery_date)
where last_quebra_tratada_date is null
and last_quebra_date is not null;

Update bazayaco.tbl_bi_ops_delivery
set tracking_backlog = dias_habiles_negativos(last_quebra_date, min_delivery_date)
where last_quebra_tratada_date is null
and last_quebra_date is not null;

Update bazayaco.tbl_bi_ops_delivery
set sac_backlog = dias_habiles_negativos(curdate(), last_quebra_date)
where last_quebra_tratada_date is null
and last_quebra_date is not null;

UPDATE bazayaco.tbl_bi_ops_delivery
SET date_backorder_accepted = IF(is_backorder=1,
(IF(date_analisando_quebra IS NOT NULL AND date_analisando_quebra>=date_backorder,date_analisando_quebra,
(IF(date_estoque_reservado IS NOT NULL AND date_estoque_reservado>=date_backorder,date_estoque_reservado,
(IF(date_waiting_dropshipping IS NOT NULL AND date_waiting_dropshipping>=date_backorder,date_waiting_dropshipping,
(IF(date_aguardando_estoque IS NOT NULL AND date_aguardando_estoque>=date_backorder,date_aguardando_estoque,NULL)))))))),NULL);


UPDATE bazayaco.tbl_bi_ops_delivery
SET delay_to_cs = IF(is_backorder=1,
(IF(date_backorder_accepted IS NOT NULL AND date(date_backorder_accepted)>date(promised_backorder_cs_date),1,
(IF(date_backorder_tratada IS NOT NULL AND date_backorder_accepted IS NULL AND date(date_backorder_tratada)>date(promised_backorder_cs_date),1,
IF(date_backorder_accepted IS NULL AND date_backorder_tratada IS NULL AND date(CURDATE())>date(promised_backorder_cs_date),1,0))))),0);

SELECT  'Calculo de Delay Inbound, WH, Outbound',now();
UPDATE bazayaco.tbl_bi_ops_delivery SET 
Inbound=if((ontime_first_attempt=0 and delay_to_procure=1 and delay_to_cs=0),1,0),
cs=if((ontime_first_attempt=0 and delay_to_procure=1 and delay_to_cs=1),1,0),
WH=if((ontime_first_attempt=0 and delay_to_procure=0 and delay_to_cs=0 and delay_to_shipped=1),1,0),
Outbound=if((ontime_first_attempt=0 and delay_to_procure=0 and delay_to_cs=0 and delay_to_shipped=0),1,0), 
Vol_Weight=(tbl_bi_ops_delivery.package_width*tbl_bi_ops_delivery.package_length*tbl_bi_ops_delivery.package_height)/5000,
Size=if(Vol_Weight<2.277,"Small",(if(Vol_Weight>45.486,"Large","Medium"))),
time_warehouse=if(Size='Small',1,(if(Size='Medium',1,2)));

##BACKUP GUIAS
SELECT  'Actualizar tabla de envios historicos',now();
INSERT INTO bazayaco.tbl_bi_ops_shipping_history (order_number,order_id,item_id,sku_simple,shipping_tracking_code_wms)
SELECT itens_venda.numero_order,itens_venda.order_id,itens_venda.item_id,itens_venda.sku,entrega.cod_rastreamento
FROM  wmsprod_co.status_itens_venda 
INNER JOIN wmsprod_co.itens_venda ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id 
INNER JOIN wmsprod_co.itens_entrega ON itens_venda.itens_venda_id = itens_entrega.itens_venda_id 
INNER JOIN wmsprod_co.entrega ON itens_entrega.entrega_id = entrega.entrega_id
WHERE DATE(status_itens_venda.data) IN
(SELECT max(dt) FROM bazayaco.calendar WHERE dt<curdate() AND isweekday=1 AND isholiday=0 ORDER BY dt DESC)
AND status_itens_venda.status='Expedido';

SELECT  'Actualizar guia y courrier de histórico',now();
UPDATE bazayaco.tbl_bi_ops_shipping_history
INNER JOIN wmsprod_co.tms_tracks ON tbl_bi_ops_shipping_history.shipping_tracking_code_wms=tms_tracks.cod_rastreamento
INNER JOIN wmsprod_co.transportadoras ON tms_tracks.id_transportadora=transportadoras.transportadoras_id
SET
tbl_bi_ops_shipping_history.shipping_courrier_tracking_code=tms_tracks.track,
tbl_bi_ops_shipping_history.courrier_name=transportadoras.nome_transportadora
WHERE shipping_courrier_tracking_code IS NULL;
##BACKUP GUIAS FIN

UPDATE bazayaco.tbl_bi_ops_delivery a
INNER JOIN tbl_catalog_product_v2 b ON a.sku_simple = b.sku
SET a.category_bp = b.category_bp;

UPDATE bazayaco.tbl_bi_ops_delivery a
SET a.category_bp = '9,1 Other'
WHERE
	a.category_bp IS NULL;


UPDATE tbl_bi_ops_delivery
SET tracking_stockout = 'STE'
where sku_simple in ('AN247OT08HQLLACOL-190044',
'AC193HB06EGBLACOL-184217',
'AC193HB08EFZLACOL-184215',
'YE855FA34EPPLACOL-153913',
'CO312FA00JOTLACOL-193213',
'CO312FA98JOVLACOL-193215',
'CO312FA07JOMLACOL-193206',
'CO312FA23JNWLACOL-193190',
'CO312FA17JOCLACOL-193196',
'CO312FA15JOELACOL-193198',
'MU919EL72JWPLACOL-160080',
'SO017EL07IJSLACOL-191157',
'AM416EL16VJVLACOL-203143',
'AM416EL20VJRLACOL-203139',
'DP211EL12GHRLACOL-192978',
'HE256TB44HWTLACOL-190539',
'HE256TB41HWWLACOL-190542',
'OD666FA33YWOLACOL-175717');

UPDATE bazayaco.tbl_bi_ops_delivery 
SET tbl_bi_ops_delivery.procured_promised_last_30 = CASE
WHEN datediff(
	curdate(),
	promised_procurement_date
) <= 30
AND datediff(
	curdate(),
	promised_procurement_date
) > 0 THEN
	1
ELSE
	0
END;


UPDATE bazayaco.tbl_bi_ops_delivery 
SET 
 tbl_bi_ops_delivery.days_left_to_procure = CASE
											WHEN datediff(promised_procurement_date,curdate()) < 0 
											THEN 0
											ELSE datediff(promised_procurement_date,curdate())	
											END
WHERE date_procured IS NULL;


UPDATE bazayaco.tbl_bi_ops_delivery
SET tbl_bi_ops_delivery.effective_1st_attempt = CASE
WHEN date_delivered = date_first_attempt THEN
	1
ELSE
	0
END;

SELECT  'promised_ready_to_ship_yearweek',now();
UPDATE tbl_bi_ops_delivery
SET promised_ready_to_ship_yearweek= CONCAT(YEAR(promised_ready_to_ship_date),'-',WEEKOFYEAR(promised_ready_to_ship_date));

SELECT  'promised_ready_to_ship_yearmonth',now();
UPDATE tbl_bi_ops_delivery
SET promised_ready_to_ship_yearmonth= CONCAT(YEAR(promised_ready_to_ship_date),'-',MONTH(promised_ready_to_ship_date));

SELECT  'date_shipped_yearweek',now();
UPDATE tbl_bi_ops_delivery
SET date_shipped_yearweek= CONCAT(YEAR(date_shipped),'-',WEEKOFYEAR(date_shipped));

UPDATE tbl_bi_ops_delivery
SET date_ready_to_pick = CASE
WHEN dayofweek(date_procured) = 1 THEN
	workday_bi (date(date_procured), 1)
ELSE
	(
		CASE
		WHEN dayofweek(date_procured) = 7 THEN
			workday_bi (date(date_procured), 1)
		ELSE
			date(date_procured)
		END
	)
END;

SELECT  'date_ready_to_pick_yearweek',now();
UPDATE tbl_bi_ops_delivery
SET date_ready_to_pick_yearweek= CONCAT(YEAR(date_ready_to_pick),'-',WEEKOFYEAR(date_ready_to_pick));

SELECT  'date_ready_to_pick_yearmonth',now();
UPDATE tbl_bi_ops_delivery
SET date_ready_to_pick_yearmonth= CONCAT(YEAR(date_ready_to_pick),'-',MONTH(date_ready_to_pick));

SELECT  'ready_to_pick',now();
UPDATE tbl_bi_ops_delivery
SET ready_to_pick = 1
WHERE
	date_ready_to_pick IS NOT NULL;

SELECT  'leadtime_pick_to_ship',now();
UPDATE tbl_bi_ops_delivery
SET leadtime_pick_to_ship = dias_habiles(date(date_ready_to_pick),date(If(date_ready Is Null,curdate(),date_ready)))
WHERE date_ready_to_pick IS NULL;

UPDATE tbl_bi_ops_delivery
SET on_time_to_ship = CASE
WHEN date_shipped <= promised_ready_to_ship_date THEN
	1
ELSE
	0
END,
 on_time_r2s = CASE
WHEN date_ready <= promised_ready_to_ship_date THEN
	1
ELSE
	0
END;

UPDATE tbl_bi_ops_delivery AS a
INNER JOIN wmsprod_co.itens_venda AS b
ON a.item_id = b.item_id
SET wh_time = tempo_armazem
WHERE
tempo_armazem > 1;


UPDATE tbl_bi_ops_delivery AS a
INNER JOIN tbl_catalog_product_v2 AS b
ON a.sku_simple=b.sku
SET a.max_vol_w_vs_w = CASE
WHEN product_vol_weight > b.package_weight THEN
	product_vol_weight
ELSE
	b.package_weight
END;

UPDATE tbl_bi_ops_delivery
SET package_measure_new = CASE
WHEN max_vol_w_vs_w > 35 THEN
	'oversized'
ELSE
	(
		CASE
		WHEN max_vol_w_vs_w > 5 THEN
			'large'
		ELSE
			(
				CASE
				WHEN max_vol_w_vs_w > 2 THEN
					'medium'
				ELSE
					'small'
				END
			)
		END
	)
END;

UPDATE tbl_bi_ops_delivery AS a
INNER JOIN wmsprod_co.entrega AS b
ON a.shipping_wms_tracking_code = b.cod_rastreamento
SET a.delivery_fullfilment = b.delivery_fulfillment;

UPDATE tbl_bi_ops_delivery
SET split_order = CASE
WHEN delivery_fullfilment = 'partial' THEN
	1
ELSE
	0
END;

SELECT  'ready_to_ship',now();
UPDATE tbl_bi_ops_delivery
SET ready_to_ship = 1
WHERE
	date_ready IS NOT NULL;

UPDATE tbl_bi_ops_delivery AS a
INNER JOIN wmsprod_co.entrega AS b
ON a.shipping_wms_tracking_code = b.cod_rastreamento
SET a.delivery_fullfilment = b.delivery_fulfillment;

UPDATE tbl_bi_ops_delivery
SET split_order = CASE
WHEN delivery_fullfilment = 'partial' THEN
	1
ELSE
	0
END;

UPDATE tbl_bi_ops_delivery
SET warehouse_delay_counter = 1
WHERE promised_ready_to_ship_date <= curdate()
AND date_ready IS NULL;

UPDATE tbl_bi_ops_delivery
SET workdays_to_ready = IF(date(date_ready) IS NULL,dias_habiles(date(date_ready_to_pick),curdate()),
dias_habiles(date(date_ready_to_pick),date(date_ready)));

UPDATE tbl_bi_ops_delivery
SET workdays_delay_to_ready = CASE
WHEN workdays_to_ready - 1 < 0 THEN
	0
ELSE
	workdays_to_ready - 1
END;

UPDATE bazayaco.tbl_bi_ops_delivery
SET 
leadtime_to_exportable = dias_habiles(date(date_ordered),date(date_exportable)),
leadtime_ready_to_shipped = If(date_ready Is Null,Null,dias_habiles(date(date_ready),date(If(date_shipped Is Null,curdate(),date_shipped)))),
days_to_export = If(date_ordered Is Null,Null,datediff(date(date_ordered),date(date_exportable)));

UPDATE bazayaco.tbl_bi_ops_delivery
SET 
days_to_procure = CASE
WHEN stockout = 1 THEN
	NULL
ELSE
	(
		CASE
		WHEN date_procured < date_exportable THEN
			0
		ELSE
			(
				CASE
				WHEN date_procured IS NULL THEN
					datediff(curdate(), date_exportable)
				ELSE
					datediff(
						date_procured,
						date_exportable
					)
				END
			)
		END
	)
END;

UPDATE bazayaco.tbl_bi_ops_delivery
SET 
days_to_ready = CASE
WHEN date_ready_to_pick IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN date_ready IS NULL THEN
			datediff(
				curdate(),
				date_ready_to_pick
			)
		ELSE
			datediff(
				date_ready,
				date_ready_to_pick
			)
		END
	)
END;

UPDATE bazayaco.tbl_bi_ops_delivery
SET 
days_to_ready = CASE
WHEN date_ready_to_pick IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN date_ready IS NULL THEN
			datediff(
				curdate(),
				date_ready_to_pick
			)
		ELSE
			datediff(
				date_ready,
				date_ready_to_pick
			)
		END
	)
END;

UPDATE bazayaco.tbl_bi_ops_delivery
SET 
days_to_ship = CASE
WHEN date_ready IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN date_shipped IS NULL THEN
			datediff(
				curdate(),
				date_ready
			)
		ELSE
			datediff(
				date_shipped,
				date_ready
			)
		END
	)
END;

UPDATE bazayaco.tbl_bi_ops_delivery
SET 
days_to_1st_attempt = CASE
WHEN date_shipped IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN date_first_attempt IS NULL THEN
			datediff(curdate(), date_shipped)
		ELSE
			datediff(
				date_first_attempt,
				date_shipped
			)
		END
	)
END;

UPDATE bazayaco.tbl_bi_ops_delivery
SET 
days_total_delivery = CASE
WHEN date_shipped IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN date_delivered IS NULL THEN
			datediff(curdate(), date_ordered)
		ELSE
			datediff(
				date_delivered,
				date_ordered
			)
		END
	)
END;

UPDATE tbl_bi_ops_delivery AS a
INNER JOIN bob_live_co.sales_order AS b
ON a.order_number = b.order_nr
INNER JOIN bob_live_co.sales_order_address AS c
ON b.fk_sales_order_address_billing = c.id_sales_order_address
SET a.ship_to_zip = postcode;

UPDATE tbl_bi_ops_delivery
SET ship_to_zip2 = mid(ship_to_zip, 3, 2),
 ship_to_zip3 = RIGHT (ship_to_zip, 3),
 ship_to_zip4 = LEFT (ship_to_zip, 4);

UPDATE 
 ((bob_live_co.sales_order_address
	RIGHT JOIN 	(tbl_bi_ops_delivery
			         INNER JOIN bob_live_co.sales_order 
               ON tbl_bi_ops_delivery.order_number = sales_order.order_nr
							)ON sales_order_address.id_sales_order_address = sales_order.fk_sales_order_address_billing
	)LEFT JOIN bob_live_co.customer_address_region 
	 ON sales_order_address.fk_customer_address_region = customer_address_region.id_customer_address_region
 )
INNER JOIN bob_live_co.sales_order_item ON tbl_bi_ops_delivery.item_id = sales_order_item.id_sales_order_item
SET 
tbl_bi_ops_delivery.ship_to_state = customer_address_region.CODE;

UPDATE tbl_bi_ops_delivery
SET ship_to_met_area = CASE
WHEN ship_to_zip BETWEEN 01000
AND 16999
OR ship_to_zip BETWEEN 53000
AND 53970
OR ship_to_zip BETWEEN 54000
AND 54198
OR ship_to_zip BETWEEN 54600
AND 54658
OR ship_to_zip BETWEEN 54700
AND 54769
OR ship_to_zip BETWEEN 54900
AND 54959
OR ship_to_zip BETWEEN 54960
AND 54990
OR ship_to_zip BETWEEN 52760
AND 52799
OR ship_to_zip BETWEEN 52900
AND 52998
OR ship_to_zip BETWEEN 55000
AND 55549
OR ship_to_zip BETWEEN 55700
AND 55739
OR ship_to_zip BETWEEN 57000
AND 57950 THEN
	1
ELSE
	0
END;

UPDATE tbl_bi_ops_delivery
SET carrier_time = max_delivery_time - wh_time - supplier_leadtime;

UPDATE tbl_bi_ops_delivery
SET tbl_bi_ops_delivery.delay_carrier_shipment = CASE
WHEN size = "large" THEN
	(
		CASE
		WHEN leadtime_shipped_attempt - 5 > 0 THEN
			1
		ELSE
			0
		END
	)
ELSE
	(
		CASE
		WHEN leadtime_shipped_attempt - 3 > 0 THEN
			1
		ELSE
			0
		END
	)
END;

UPDATE tbl_bi_ops_delivery
SET delay_to_1st_attempt = CASE
WHEN leadtime_shipped_attempt > 2 THEN
	1
ELSE
	0
END;

UPDATE tbl_bi_ops_delivery
SET workdays_delay_to_deliver = CASE
WHEN leadtime_to_delivered - 3 < 0 THEN
	0
ELSE
	leadtime_to_delivered - 3
END;

UPDATE tbl_bi_ops_delivery
SET workdays_delay_to_procure = CASE
WHEN leadtime_to_procure - supplier_leadtime < 0 THEN
	0
ELSE
	leadtime_to_procure - supplier_leadtime
END;

UPDATE tbl_bi_ops_delivery
SET workdays_delay_to_ready = CASE
WHEN workdays_to_ready - 1 < 0 THEN
	0
ELSE
	workdays_to_ready - 1
END;

UPDATE tbl_bi_ops_delivery
SET workdays_delay_to_ship = CASE
WHEN leadtime_ready_to_shipped - 1 < 0 THEN
	0
ELSE
	leadtime_ready_to_shipped - 1
END;

UPDATE tbl_bi_ops_delivery
SET workdays_delay_to_ship = CASE
WHEN leadtime_ready_to_shipped - 1 < 0 THEN
	0
ELSE
	leadtime_ready_to_shipped - 1
END;

UPDATE tbl_bi_ops_delivery
SET workdays_delay_to_1st_attempt = CASE
WHEN leadtime_shipped_attempt - 3 < 0 THEN
	0
ELSE
	leadtime_shipped_attempt - 3
END;

UPDATE tbl_bi_ops_delivery
SET delay_reason_maximum_value = maximum (
	workdays_delay_to_procure,
	workdays_delay_to_ready,
	workdays_delay_to_ship,
	workdays_delay_to_1st_attempt
);

UPDATE tbl_bi_ops_delivery
SET delay_reason = CASE
WHEN delay_first_attempt = 1 THEN
	(
		CASE
		WHEN delay_reason_maximum_value = workdays_delay_to_procure THEN
			"procurement"
		ELSE
			(
				CASE
				WHEN delay_reason_maximum_value = workdays_delay_to_ready THEN
					"preparing item for shipping"
				ELSE
					(
						CASE
						WHEN delay_reason_maximum_value = workdays_delay_to_ship THEN
							"carrier late to pick up item"
						ELSE
							(
								CASE
								WHEN delay_reason_maximum_value = workdays_delay_to_1st_attempt THEN
									"carrier late delivering"
								ELSE
									"on time 1st attempt"
								END
							)
						END
					)
				END
			)
		END
	)
ELSE
	"on time 1st attempt"
END;

UPDATE tbl_bi_ops_delivery
SET reason_for_delay = "on time 1st attempt";

UPDATE tbl_bi_ops_delivery
SET reason_for_delay = "procurement"
WHERE
	delay_to_procure = 1
AND delay_first_attempt = 1;

UPDATE tbl_bi_ops_delivery
SET delay_to_ready = CASE
WHEN workdays_to_ready > 1 THEN
	1
ELSE
	0
END;

UPDATE tbl_bi_ops_delivery
SET reason_for_delay = "preparing item for shipping"
WHERE
	reason_for_delay = "on time 1st attempt"
AND delay_to_ready = 1
AND delay_first_attempt = 1;

UPDATE tbl_bi_ops_delivery
SET reason_for_delay = "preparing item for shipping"
WHERE
	reason_for_delay = "on time 1st attempt"
AND delay_to_ready = 1
AND delay_first_attempt = 1;

UPDATE tbl_bi_ops_delivery
SET reason_for_delay = "carrier late delivering"
WHERE
	reason_for_delay = "on time 1st attempt"
AND delay_first_attempt = 1;

UPDATE tbl_bi_ops_delivery
SET delivery_delay = CASE
WHEN date_ready >= curdate() THEN
	0
ELSE
	(
		CASE
		WHEN leadtime_to_delivered > carrier_time THEN
			leadtime_to_delivered - carrier_time
		ELSE
			0
		END
	)
END;

UPDATE tbl_bi_ops_delivery
SET delivery_delay_counter = 1
WHERE
	delivery_delay > 0
AND promised_delivery_date <= curdate();

UPDATE tbl_bi_ops_delivery
SET check_date_ready_to_ship = CASE
WHEN date_ready IS NULL THEN
	(
		CASE
		WHEN date_shipped IS NULL
		AND date_first_attempt IS NULL
		AND date_delivered IS NULL THEN
			0
		ELSE
			1
		END
	)
ELSE
	(
		CASE
		WHEN date_ready > date_shipped THEN
			1
		ELSE
			0
		END
	)
END;

UPDATE tbl_bi_ops_delivery
SET check_date_1st_attempt = CASE
WHEN date_first_attempt IS NULL THEN
	(
		CASE
		WHEN date_delivered IS NULL THEN
			0
		ELSE
			1
		END
	)
ELSE
	(
		CASE
		WHEN date_first_attempt > date_delivered THEN
			1
		ELSE
			0
		END
	)
END;

UPDATE tbl_bi_ops_delivery AS a
INNER JOIN (SELECT numero_order,count(item_id) AS count_of_order_item_id FROM wmsprod_co.itens_venda
GROUP BY numero_order) AS b
ON a.order_number=b.numero_order
SET num_items_per_order = 1 / count_of_order_item_id;

UPDATE tbl_bi_ops_delivery
SET shipped_last_30 = CASE
WHEN datediff(curdate(), date_shipped) <= 30
AND datediff(curdate(), date_shipped) > 0 THEN
	1
ELSE
	0
END;

UPDATE tbl_bi_ops_delivery
SET delivered_promised_last_30 = CASE
WHEN datediff(
	curdate(),
	promised_delivery_date
) <= 30
AND datediff(
	curdate(),
	promised_delivery_date
) > 0 THEN
	1
ELSE
	0
END;

UPDATE tbl_bi_ops_delivery
SET pending_first_attempt = CASE
WHEN date_shipped IS NOT NULL
AND date_first_attempt IS NULL THEN
	1
ELSE
	0
END;

UPDATE tbl_bi_ops_delivery
SET check_dates = CASE
WHEN date_ordered > date_exportable THEN
	"date ordered > date exported"
ELSE
	(
		CASE
		WHEN date_exportable > date_procured THEN
			"date exported > date procured"
		ELSE
			(
				CASE
				WHEN date_procured > date_ready THEN
					"date procured > date ready to ship"
				ELSE
					(
						CASE
						WHEN date_ready > date_shipped THEN
							"date ready to ship > date shipped"
						ELSE
							(
								CASE
								WHEN date_shipped > date_first_attempt THEN
									"date shipped > date 1st attempt"
								ELSE
									(
										CASE
										WHEN date_first_attempt > date_delivered THEN
											"date shipped > date 1st attempt"
										ELSE
											"correct"
										END
									)
								END
							)
						END
					)
				END
			)
		END
	)
END;

UPDATE        tbl_bi_ops_delivery
SET 
   r2s_workday_0  = IF( workdays_to_ready = 0 , 1 , 0 ),
   r2s_workday_1  = IF( workdays_to_ready = 1 , 1 , 0 ),
   r2s_workday_2  = IF( workdays_to_ready = 2 , 1 , 0 ),
   r2s_workday_3  = IF( workdays_to_ready = 3 , 1 , 0 ),
   r2s_workday_4  = IF( workdays_to_ready = 4 , 1 , 0 ),
   r2s_workday_5  = IF( workdays_to_ready = 5 , 1 , 0 ),
   r2s_workday_6  = IF( workdays_to_ready = 6 , 1 , 0 ), 
   r2s_workday_7  = IF( workdays_to_ready = 7 , 1 , 0 ), 
   r2s_workday_8  = IF( workdays_to_ready = 8 , 1 , 0 ),  
   r2s_workday_9  = IF( workdays_to_ready = 9 , 1 , 0 ),  
   r2s_workday_10 = IF( workdays_to_ready >10 , 1 , 0 )
;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'bazayaco.tbl_bi_ops_delvery parte 1',
  'finish',
  NOW(),
  MAX(date_exportable),
  count(*),
  count(*)
FROM
  bazayaco.tbl_bi_ops_delivery
;

SELECT  'Rutina finalizada bi_ops_delivery',now();

END
