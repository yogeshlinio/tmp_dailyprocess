BEGIN
SELECT  'OPS Stock	',now();


TRUNCATE TABLE tbl_bi_ops_stock	;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'bazayaco.tbl_bi_ops_stock parte 1',
  'start',
  NOW(),
  MAX(date_entrance),
  count(*),
  count(*)
FROM
  bazayaco.tbl_bi_ops_stock
;

SELECT  'Insert data',now();
INSERT INTO tbl_bi_ops_stock (stock_item_id, barcode_wms, date_entrance, barcode_bob_duplicated, in_stock, wh_location,sub_location,separation)
SELECT 
estoque.estoque_id, 
estoque.cod_barras, 
If(data_criacao Is Null,Null,data_criacao) AS Expr1, 
estoque.minucioso, 
posicoes.participa_estoque, 
estoque.endereco,
estoque.sub_endereco,
estoque.almoxarifado
FROM (wmsprod_co.estoque LEFT JOIN wmsprod_co.itens_recebimento ON estoque.itens_recebimento_id = itens_recebimento.itens_recebimento_id) 
LEFT JOIN wmsprod_co.posicoes ON estoque.endereco = posicoes.posicao;


SELECT  'Actualizar información sku_ean',now();
UPDATE bazayaco.tbl_bi_ops_stock
INNER JOIN wmsprod_co.traducciones_producto ON tbl_bi_ops_stock.barcode_wms = traducciones_producto.identificador
SET tbl_bi_ops_stock.barcode_wms = traducciones_producto.sku;

UPDATE tbl_bi_ops_stock 
set barcode_wms = 'AN271EL69WVILACOL-115653'
where stock_item_id = '228135';

UPDATE tbl_bi_ops_stock INNER JOIN wmsprod_co.movimentacoes ON tbl_bi_ops_stock.stock_item_id = movimentacoes.estoque_id 
SET tbl_bi_ops_stock.date_exit = data_criacao,
tbl_bi_ops_stock.week_exit=week(data_criacao),
tbl_bi_ops_stock.year_exit= year(data_criacao),
tbl_bi_ops_stock.exit_type= "sold"
 WHERE tbl_bi_ops_stock.wh_location ="vendidos"
and movimentacoes.para_endereco="vendidos";

UPDATE tbl_bi_ops_stock INNER JOIN wmsprod_co.movimentacoes ON tbl_bi_ops_stock.stock_item_id = movimentacoes.estoque_id 
SET tbl_bi_ops_stock.date_exit = data_criacao,
tbl_bi_ops_stock.week_exit=week(data_criacao),
tbl_bi_ops_stock.year_exit= year(data_criacao),
tbl_bi_ops_stock.exit_type= "error"
 WHERE tbl_bi_ops_stock.wh_location ="Error_entrada"
and movimentacoes.para_endereco="Error_entrada";

SELECT  'Datos de la orden',now();
update
tbl_bi_ops_stock a INNER JOIN 
(select t.itens_venda_id,t.item_id,
t.order_id,t.numero_order,t.data_pedido,t.estoque_id,t.data_criacao,t.status from wmsprod_co.itens_venda t 
JOIN (select estoque_id, max(data_criacao) max from wmsprod_co.itens_venda group by estoque_id) 
t2 ON t.data_criacao=t2.max and t.estoque_id=t2.estoque_id ) b  
ON a.stock_item_id = b.estoque_id
set 
a.order_nr=b.numero_order,
a.order_date=b.data_pedido,
a.status_item=b.status,
a.item_id=b.item_id;

SELECT  'Corregir datos de cruce',now();
update bazayaco.tbl_bi_ops_stock
set wh_location='error_cruce_sistemas',
exit_type='error'
where (status_item='Cancelado' or order_nr is null) and wh_location='vendidos';

SELECT  'Corregir datos de hoy',now();
Update tbl_bi_ops_stock
SET in_stock_real=1 
Where in_stock=1;


Update tbl_bi_ops_stock
SET in_stock_real=1 
Where wh_location="vendidos" and date_exit=curdate();
Update tbl_bi_ops_stock
SET in_stock_real=0 
Where date_entrance=curdate();

UPDATE tbl_bi_ops_stock INNER JOIN tbl_catalog_product_v2 
ON tbl_bi_ops_stock.barcode_wms=tbl_catalog_product_v2.sku
SET 
tbl_bi_ops_stock.sku_simple=tbl_catalog_product_v2.sku,
tbl_bi_ops_stock.sku_config=tbl_catalog_product_v2.sku_config,
tbl_bi_ops_stock. barcode_ean_bob =tbl_catalog_product_v2.barcode_ean,
tbl_bi_ops_stock.product_name = tbl_catalog_product_v2.product_name,
tbl_bi_ops_stock.brand = tbl_catalog_product_v2.brand,
tbl_bi_ops_stock.buyer = tbl_catalog_product_v2.buyer,
tbl_bi_ops_stock.supplier_name = tbl_catalog_product_v2.Supplier,
tbl_bi_ops_stock.model = tbl_catalog_product_v2.model,  
tbl_bi_ops_stock.product_weight = tbl_catalog_product_v2.product_weight,
tbl_bi_ops_stock.category_1 = tbl_catalog_product_v2.new_cat1,
tbl_bi_ops_stock.category_2 = tbl_catalog_product_v2.new_cat2,
tbl_bi_ops_stock.category_3 = tbl_catalog_product_v2.new_cat3,
tbl_bi_ops_stock.product_linea_blanca = tbl_catalog_product_v2.linea_blanca,
tbl_bi_ops_stock.product_color = tbl_catalog_product_v2.color,
tbl_bi_ops_stock.product_variation = tbl_catalog_product_v2.variation,
tbl_bi_ops_stock.status_simple =tbl_catalog_product_v2.status_simple,
tbl_bi_ops_stock.visible =tbl_catalog_product_v2.visible,
tbl_bi_ops_stock.cost_wo_vat = ifnull(IF (tbl_catalog_product_v2.specialpurchaseprice is null, 
tbl_catalog_product_v2.cost, 
 if(curdate() > tbl_catalog_product_v2.special_to_date, tbl_catalog_product_v2.cost, 
 if(curdate() < tbl_catalog_product_v2.special_from_date, tbl_catalog_product_v2.cost,
  if(tbl_catalog_product_v2.specialpurchaseprice < tbl_catalog_product_v2.cost,tbl_catalog_product_v2.specialpurchaseprice,tbl_catalog_product_v2.cost)  ))),0),
tbl_bi_ops_stock.tax_percentage = tbl_catalog_product_v2.tax_percent,
tbl_bi_ops_stock.product_special_purchase_price= tbl_catalog_product_v2.specialpurchaseprice,
tbl_bi_ops_stock.product_price =tbl_catalog_product_v2.price,
tbl_bi_ops_stock.product_special_price =tbl_catalog_product_v2.special_price,
tbl_bi_ops_stock.special_from_date  =tbl_catalog_product_v2.special_from_date,
tbl_bi_ops_stock.special_to_date  =tbl_catalog_product_v2.special_to_date,
tbl_bi_ops_stock.product_shipment_cost = tbl_catalog_product_v2.shipment_cost_item,
tbl_bi_ops_stock.min_delivery_time  =tbl_catalog_product_v2.min_delivery_time,
tbl_bi_ops_stock.max_delivery_time  =tbl_catalog_product_v2.max_delivery_time,
tbl_bi_ops_stock.category_bp = tbl_catalog_product_v2.category_bp;


update tbl_bi_ops_stock 
set buyer = trim(both '	' from replace(replace(replace(buyer, '\t', ''), '\n',''),'\r',''));
UPDATE tbl_bi_ops_stock
SET 
cost_with_vat = cost_wo_vat + (cost_wo_vat*(tax_percentage/100));

UPDATE tbl_bi_ops_stock INNER JOIN (wmsprod_co.itens_venda INNER JOIN wmsprod_co.status_itens_venda ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id) ON tbl_bi_ops_stock.stock_item_id = itens_venda.estoque_id
SET tbl_bi_ops_stock.fullfilment_type = "Crossdock"
WHERE ((status_itens_venda.status="Analisando quebra") or (status_itens_venda.status="Aguardando estoque"));
UPDATE tbl_bi_ops_stock 
SET tbl_bi_ops_stock.fullfilment_type = "Consignation"
WHERE wh_location like '%CONSI%';

SELECT  'Dias de inventario',now();
update tbl_bi_ops_stock
set dias_inventario = if(date_exit is null,datediff(curdate(),date_entrance),datediff(date_exit,date_entrance)) ;

SELECT  'Actualizando tracker',now();
UPDATE tbl_bi_ops_stock INNER JOIN tbl_bi_ops_tracking_suppliers
    ON tbl_bi_ops_stock.supplier_name =
    tbl_bi_ops_tracking_suppliers.supplier_name
SET tbl_bi_ops_stock.tracker = tbl_bi_ops_tracking_suppliers.tracker_name; 

SELECT  'Calcular nuevos campos',now();
UPDATE tbl_bi_ops_stock 
SET tbl_bi_ops_stock.current_price = IF(special_to_date>NOW() AND product_special_price is not null,product_special_price,product_price) ,
tbl_bi_ops_stock.current_cost = IF(special_to_date>NOW() AND product_special_purchase_price is not null,product_special_purchase_price,cost_wo_vat);

SELECT  'Calcular binarios',now();
UPDATE tbl_bi_ops_stock 
Set visible_in=if(visible="SI",1,0),
reserved= if(separation="Separando",1,0),
sku_simple_blank= if(sku_simple is null,1,null);

SELECT  'Cuarentena',now();
UPDATE tbl_bi_ops_stock 
Set 
quarantine= 1
where wh_location liKe '%DEF%' or wh_location like '%REP%' OR 
wh_location like '%GRT%' OR wh_location like '%AVE%' 
or wh_location like '%cuerentena%' or wh_location like '%Cuarentena_Inverse%';

SELECT  'Tipos de Cuarentena',now();
UPDATE tbl_bi_ops_stock 
Set 
quarantine_type= if(wh_location liKe '%DEF%', 'DEF',if( wh_location like '%REP%','REP', 
if(wh_location like '%GRT%','GRT', if(wh_location like '%AVE%','AVE',
if(wh_location like '%Cuarentena_Inverse%','Cuarentena_Inverse','PENDING')))))
where quarantine=1;


SELECT  'Actualizar catalog_config',now();
UPDATE ((bazayaco.tbl_bi_ops_stock INNER JOIN bob_live_co.catalog_simple ON tbl_bi_ops_stock.barcode_wms = catalog_simple.sku) 
INNER JOIN bob_live_co.catalog_config ON catalog_simple.fk_catalog_config = catalog_config.id_catalog_config)
SET 
tbl_bi_ops_stock.package_measure = catalog_config.product_measures, 
tbl_bi_ops_stock.package_width = greatest(cast(replace(IF(isnull(catalog_config.package_width),1,catalog_config.package_width),',','.') as DECIMAL(6,2)),1.0), 
tbl_bi_ops_stock.package_length = greatest(cast(replace(IF(isnull(catalog_config.package_length),1,catalog_config.package_length),',','.') as DECIMAL(6,2)),1.0), 
tbl_bi_ops_stock.package_height = greatest(cast(replace(IF(isnull(catalog_config.package_height),1,catalog_config.package_height),',','.') as DECIMAL(6,2)),1.0);

SELECT  'Calculo de Tamaños',now();
UPDATE tbl_bi_ops_stock SET 
vol_weight=(tbl_bi_ops_stock.package_width*tbl_bi_ops_stock.package_length*tbl_bi_ops_stock.package_height)/5000,
Size=if(Vol_Weight<2.277,"Small",(if(Vol_Weight>45.486,"Large","Medium")));

SELECT  'Binarios ítems vendidos',now();
update tbl_bi_ops_stock 
set sold_last_30= case when datediff(curdate(),date_exit)<30 then 1 else 0 end, 
sold_yesterday = case when datediff(curdate(),(date_sub(date_exit, interval 1 day)))=0 then 1 else 0 end, 
sold_last_10 = case when datediff(curdate(),date_exit)<10 then 1 else 0 end, 
sold_last_7 = case when datediff(curdate(),date_exit)<7 then 1 else 0 end 
where exit_type="sold";

SELECT  'Sold last 30',now();
update tbl_bi_ops_stock 
set tbl_bi_ops_stock.sold_last_30_cost_w_o_vat = tbl_bi_ops_stock.cost_wo_vat, 
tbl_bi_ops_stock.sold_last_30_price = tbl_bi_ops_stock.product_price
where tbl_bi_ops_stock.sold_last_30=1;	

truncate table tbl_bi_ops_stock_sold_last_30_count;

SELECT  'Insertar datos tabla sold_last_30_count',now();
insert into tbl_bi_ops_stock_sold_last_30_count ( sold_last_30, sku_simple, count_of_item_counter )
select tbl_bi_ops_stock.sold_last_30, tbl_bi_ops_stock.sku_simple, count(tbl_bi_ops_stock.item_counter) as count_of_item_counter
from tbl_bi_ops_stock group by tbl_bi_ops_stock.sold_last_30, tbl_bi_ops_stock.sku_simple having tbl_bi_ops_stock.sold_last_30=1;

update tbl_bi_ops_stock inner join tbl_bi_ops_stock_sold_last_30_count on tbl_bi_ops_stock.sku_simple = tbl_bi_ops_stock_sold_last_30_count.sku_simple 
set tbl_bi_ops_stock.sold_last_30_counter = 1/ tbl_bi_ops_stock_sold_last_30_count.count_of_item_counter;


truncate table tbl_bi_ops_stock_sku_count;

SELECT  'Insertar datos tabla sku_counter',now();
insert into tbl_bi_ops_stock_sku_count ( sku_simple, sku_count )
select tbl_bi_ops_stock.sku_simple, 
sum(tbl_bi_ops_stock.item_counter) as sumofitem_counter 
from tbl_bi_ops_stock
 where tbl_bi_ops_stock.in_stock_real=1 and tbl_bi_ops_stock.reserved=0
group by tbl_bi_ops_stock.sku_simple, tbl_bi_ops_stock.in_stock_real;

update tbl_bi_ops_stock inner join tbl_bi_ops_stock_sku_count on tbl_bi_ops_stock.sku_simple = tbl_bi_ops_stock_sku_count.sku_simple 
set tbl_bi_ops_stock.sku_counter= 1/ tbl_bi_ops_stock_sku_count.sku_count;

SELECT  'Drop views',now();
drop view vw_max_last_6_wks;
drop view vw_last_6_wks;

SELECT  'Create views',now();
create view vw_last_6_wks as select tbl_bi_ops_stock.sku_simple, count(tbl_bi_ops_stock.date_exit) as countofdate_exit, tbl_bi_ops_stock.week_exit
from tbl_bi_ops_stock where (((tbl_bi_ops_stock.date_exit)>date_sub(curdate(), interval 43 day)))
group by tbl_bi_ops_stock.sku_simple, tbl_bi_ops_stock.week_exit;

create view vw_max_last_6_wks as select vw_last_6_wks.sku_simple, max(vw_last_6_wks.countofdate_exit) as maxofcountofdate_exit, sum(vw_last_6_wks.countofdate_exit) as sumofcountofdate_exit from vw_last_6_wks group by vw_last_6_wks.sku_simple;


SELECT  'Eliminar datos tbl_bi_ops_sum_last_5_out_of_6_wks',now();
TRUNCATE TABLE tbl_bi_ops_stock_sum_last_5_out_of_6_wks;


SELECT  'Insertar datos tabla tbl_bi_ops_stock_sum_last_5_out_of_6_wks',now();
insert into  tbl_bi_ops_stock_sum_last_5_out_of_6_wks( sku_simple, expr1 )
select sku_simple, sumofcountofdate_exit-maxofcountofdate_exit as expr1 from vw_max_last_6_wks
group by sku_simple, sumofcountofdate_exit-maxofcountofdate_exit;

update tbl_bi_ops_stock inner join tbl_bi_ops_stock_sum_last_5_out_of_6_wks on tbl_bi_ops_stock.sku_simple = tbl_bi_ops_stock_sum_last_5_out_of_6_wks.sku_simple 
set tbl_bi_ops_stock.sum_last_5_out_of_6_wks = tbl_bi_ops_stock_sum_last_5_out_of_6_wks.expr1;


SELECT  'Actualizar ultima posicion',now();
UPDATE tbl_bi_ops_stock INNER JOIN wmsprod_co.movimentacoes ON tbl_bi_ops_stock.stock_item_id = movimentacoes.estoque_id 
SET tbl_bi_ops_stock.wh_location_before_sold = de_endereco
where movimentacoes.para_endereco="vendidos";

UPDATE tbl_bi_ops_stock INNER JOIN wmsprod_co.movimentacoes ON tbl_bi_ops_stock.stock_item_id = movimentacoes.estoque_id 
INNER JOIN wmsprod_co.posicoes on tbl_bi_ops_stock.wh_location_before_sold=posicoes.posicao
SET tbl_bi_ops_stock.wh_location_before_sold = de_endereco
where tbl_bi_ops_stock.wh_location="vendidos" and posicoes.participa_estoque<>1 and movimentacoes.para_endereco=tbl_bi_ops_stock.wh_location_before_sold;

SELECT  'Actualizar info de catalog_stock',now();
update bazayaco.tbl_catalog_product_stock a inner join bazayaco.vw_stock_wms b on a.sku=b.sku_simple
set a.stock_wms=b.stock_wms
where b.stock_wms>0;

update bazayaco.tbl_catalog_product_stock a inner join bazayaco.vw_stock_wms_reserved b on a.sku=b.sku_simple
set a.stock_wms_reserved=b.stock_wms_reserved;

update bazayaco.tbl_catalog_product_stock
set stock_wms_reserved=0 where stock_wms_reserved is null;

update tbl_catalog_product_stock
set  
share_reserved_bob=if(reservedbob=0,0,if((reservedbob-stock_wms_reserved)/stock_wms>1,1,(reservedbob-stock_wms_reserved)/stock_wms)),
share_stock_bob=availablebob/stock_wms;

SELECT  'Actualizando niveles de stock de BOB',now();
UPDATE tbl_bi_ops_stock
INNER JOIN  tbl_catalog_product_stock
ON tbl_bi_ops_stock.sku_simple = tbl_catalog_product_stock.sku
SET
tbl_bi_ops_stock.stock_bob=tbl_catalog_product_stock.stockbob,
tbl_bi_ops_stock.own_stock_bob=tbl_catalog_product_stock.ownstock,
tbl_bi_ops_stock.supplier_stock_bob=tbl_catalog_product_stock.supplierstock,
tbl_bi_ops_stock.reserved_bob=tbl_catalog_product_stock.reservedbob,
tbl_bi_ops_stock.stock_available_bob= tbl_catalog_product_stock.availablebob,
tbl_bi_ops_stock.reservedbob=tbl_catalog_product_stock.share_reserved_bob;

SELECT  'Reservas de BOB',now();
update tbl_bi_ops_stock set stock_wms =if(reservedbob is null,in_stock_real, in_stock_real- reservedbob)
where in_stock_real=1 and reserved=0;

SELECT  'Extra queries replanishment model',now();

update tbl_bi_ops_stock set sold = 1
where exit_type="sold";

#sell list por config - 60 días de ventas
truncate table tbl_bi_ops_stock_sell_rate_config; 

Insert tbl_bi_ops_stock_sell_rate_config (sku_config,num_sales, num_items, average_days_in_stock)
select solded.sku_config, solded.vendidos, instockreal.items_total, instockreal.dias from ( select sku_config,sum(sold) as vendidos
from tbl_bi_ops_stock where (in_stock =1 or exit_type='sold') and date_exit BETWEEN CURDATE() - INTERVAL 60 DAY AND CURDATE()
group by sku_config) as solded inner join (select sku_config, sum(item_counter) as items_total, avg(dias_inventario) as dias
from tbl_bi_ops_stock where (in_stock =1)
group by sku_config) as instockreal ON solded.sku_config = instockreal.sku_config; 

update tbl_bi_ops_stock_sell_rate_config set num_items_available= 
( select sum(in_stock) from tbl_bi_ops_stock where reserved = '0' and 
tbl_bi_ops_stock_sell_rate_config.sku_config = tbl_bi_ops_stock.sku_config group by sku_config);

update tbl_bi_ops_stock_sell_rate_config 
set average_sell_rate = 
                       if(num_sales=0,null, case when average_days_in_stock=0 then 0
                                                 else num_sales/60
											end
                          );

update tbl_bi_ops_stock_sell_rate_config set remaining_days = 
if(average_sell_rate is null,null,case when average_sell_rate=0 then 0 else 
num_items_available/average_sell_rate end);

update tbl_bi_ops_stock inner join tbl_bi_ops_stock_sell_rate_config on 
tbl_bi_ops_stock.sku_config = tbl_bi_ops_stock_sell_rate_config.sku_config set 
tbl_bi_ops_stock.average_remaining_days = tbl_bi_ops_stock_sell_rate_config.remaining_days;


#sell list por simple - 60 días de ventas
truncate table tbl_bi_ops_stock_sell_rate_simple; 

Insert tbl_bi_ops_stock_sell_rate_simple (sku_simple,sku_config,num_items,num_sales,average_days_in_stock, cost_oms)
select solded.sku_simple, solded.sku_config, instockreal.items_total, solded.vendidos, instockreal.dias, instockreal.cost_oms_wo_vat from ( select sku_simple,
sku_config,sum(sold) as vendidos
from tbl_bi_ops_stock where (in_stock =1 or exit_type='sold') and date_exit BETWEEN CURDATE() - INTERVAL 60 DAY AND CURDATE()
group by sku_simple) as solded inner join (select sku_simple, sku_config, sum(item_counter) as items_total, avg(dias_inventario) as dias, cost_oms_wo_vat
from tbl_bi_ops_stock where (in_stock =1)
group by sku_simple) as instockreal ON solded.sku_simple = instockreal.sku_simple; 

update tbl_bi_ops_stock_sell_rate_simple set num_items_available= ( 
select sum(in_stock) from tbl_bi_ops_stock where reserved = '0' and 
tbl_bi_ops_stock_sell_rate_simple.sku_simple = tbl_bi_ops_stock.sku_simple group by sku_simple);

update tbl_bi_ops_stock_sell_rate_simple 
set average_sell_rate = 
                       if(num_sales=0,null, case when average_days_in_stock=0 then 0
                                                 else num_sales/60 
											end
                          );
 
update tbl_bi_ops_stock_sell_rate_simple set remaining_days = if(average_sell_rate is null,null,
case when average_sell_rate=0 then 0 else num_items_available/average_sell_rate end);

update tbl_bi_ops_stock_sell_rate_simple set remaining_days_value = if(average_sell_rate is null,null,
case when average_sell_rate=0 then 0 else num_items_available*cost_oms/average_sell_rate*cost_oms end);

update tbl_bi_ops_stock inner join tbl_bi_ops_stock_sell_rate_simple on 
tbl_bi_ops_stock.sku_simple = tbl_bi_ops_stock_sell_rate_simple.sku_simple set 
tbl_bi_ops_stock.average_remaining_days_sim_60 = tbl_bi_ops_stock_sell_rate_simple.remaining_days;

update tbl_bi_ops_stock inner join tbl_bi_ops_stock_sell_rate_simple on 
tbl_bi_ops_stock.sku_simple = tbl_bi_ops_stock_sell_rate_simple.sku_simple set 
tbl_bi_ops_stock.average_remaining_days_value_60 = tbl_bi_ops_stock_sell_rate_simple.remaining_days_value;

update tbl_bi_ops_stock inner join tbl_bi_ops_stock_sell_rate_simple on 
tbl_bi_ops_stock.sku_simple = tbl_bi_ops_stock_sell_rate_simple.sku_simple 
set tbl_bi_ops_stock.sales_rate_60_days = ((tbl_bi_ops_stock_sell_rate_simple.num_sales/60)/tbl_bi_ops_stock_sell_rate_simple.num_items_available);

update tbl_bi_ops_stock inner join tbl_bi_ops_stock_sell_rate_simple on 
tbl_bi_ops_stock.sku_simple = tbl_bi_ops_stock_sell_rate_simple.sku_simple 
set tbl_bi_ops_stock.avaliable_stock_current = tbl_bi_ops_stock_sell_rate_simple.num_items;

update tbl_bi_ops_stock inner join tbl_bi_ops_stock_sell_rate_simple on 
tbl_bi_ops_stock.sku_simple = tbl_bi_ops_stock_sell_rate_simple.sku_simple 
set tbl_bi_ops_stock.average_days_stock = tbl_bi_ops_stock_sell_rate_simple.average_days_in_stock;

#sell list por simple - 7 días de ventas
truncate table tbl_bi_ops_stock_sell_rate_simple_7; 

Insert tbl_bi_ops_stock_sell_rate_simple_7 (sku_simple,sku_config,num_items,num_sales,average_days_in_stock, cost_oms)
select solded.sku_simple, solded.sku_config, instockreal.items_total, solded.vendidos, instockreal.dias, instockreal.cost_oms_wo_vat from ( select sku_simple,
sku_config,sum(sold) as vendidos
from tbl_bi_ops_stock where (in_stock =1 or exit_type='sold') and date_exit BETWEEN CURDATE() - INTERVAL 7 DAY AND CURDATE()
group by sku_simple) as solded inner join (select sku_simple, sku_config, sum(item_counter) as items_total, avg(dias_inventario) as dias, cost_oms_wo_vat
from tbl_bi_ops_stock where (in_stock =1)
group by sku_simple) as instockreal ON solded.sku_simple = instockreal.sku_simple; 

update tbl_bi_ops_stock_sell_rate_simple_7 set num_items_available= ( 
select sum(in_stock) from tbl_bi_ops_stock where reserved = '0' and 
tbl_bi_ops_stock_sell_rate_simple_7.sku_simple = tbl_bi_ops_stock.sku_simple group by sku_simple);

update tbl_bi_ops_stock_sell_rate_simple_7 
set average_sell_rate = 
                       if(num_sales=0,null, case when average_days_in_stock=0 then 0
                                                 else num_sales/7 
											end
                          );
 
update tbl_bi_ops_stock_sell_rate_simple_7 set remaining_days = if(average_sell_rate is null,null,
case when average_sell_rate=0 then 0 else num_items_available/average_sell_rate end);

update tbl_bi_ops_stock_sell_rate_simple_7 set remaining_days_value = if(average_sell_rate is null,null,
case when average_sell_rate=0 then 0 else num_items_available*cost_oms/average_sell_rate*cost_oms end);

update tbl_bi_ops_stock inner join tbl_bi_ops_stock_sell_rate_simple_7 on 
tbl_bi_ops_stock.sku_simple = tbl_bi_ops_stock_sell_rate_simple_7.sku_simple set 
tbl_bi_ops_stock.average_remaining_days_sim_7 = tbl_bi_ops_stock_sell_rate_simple_7.remaining_days;

update tbl_bi_ops_stock inner join tbl_bi_ops_stock_sell_rate_simple_7 on 
tbl_bi_ops_stock.sku_simple = tbl_bi_ops_stock_sell_rate_simple_7.sku_simple set 
tbl_bi_ops_stock.average_remaining_days_value_7 = tbl_bi_ops_stock_sell_rate_simple_7.remaining_days_value;

update tbl_bi_ops_stock inner join tbl_bi_ops_stock_sell_rate_simple_7 on 
tbl_bi_ops_stock.sku_simple = tbl_bi_ops_stock_sell_rate_simple_7.sku_simple 
set tbl_bi_ops_stock.sales_rate_7_days = (tbl_bi_ops_stock_sell_rate_simple_7.num_sales/7);


#sell list por config - 30 días de ventas
truncate table tbl_bi_ops_stock_sell_rate_config_30; 

Insert tbl_bi_ops_stock_sell_rate_config_30 (sku_config,num_sales, num_items, average_days_in_stock)
select solded.sku_config, solded.vendidos, instockreal.items_total, instockreal.dias from ( select sku_config,sum(sold) as vendidos
from tbl_bi_ops_stock where (in_stock =1 or exit_type='sold') and date_exit BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()
group by sku_config) as solded inner join (select sku_config, sum(item_counter) as items_total, avg(dias_inventario) as dias
from tbl_bi_ops_stock where (in_stock =1)
group by sku_config) as instockreal ON solded.sku_config = instockreal.sku_config; 

update tbl_bi_ops_stock_sell_rate_config_30 set num_items_available= 
( select sum(in_stock) from tbl_bi_ops_stock where reserved = '0' and 
tbl_bi_ops_stock_sell_rate_config_30.sku_config = tbl_bi_ops_stock.sku_config group by sku_config);

update tbl_bi_ops_stock_sell_rate_config_30 
set average_sell_rate = 
                       if(num_sales=0,null, case when average_days_in_stock=0 then 0
                                                 else num_sales/30 
											end
                          );

update tbl_bi_ops_stock_sell_rate_config_30 set remaining_days = 
if(average_sell_rate is null,null,case when average_sell_rate=0 then 0 else 
num_items_available/average_sell_rate end);

update tbl_bi_ops_stock inner join tbl_bi_ops_stock_sell_rate_config_30 on 
tbl_bi_ops_stock.sku_config = tbl_bi_ops_stock_sell_rate_config_30.sku_config set 
tbl_bi_ops_stock.average_remaining_days_30 = tbl_bi_ops_stock_sell_rate_config_30.remaining_days;



#Días de inventario por sku_simple con stock disponible en bodega
truncate table tbl_bi_ops_max_days_in_stock; 
insert into tbl_bi_ops_max_days_in_stock(sku_simple, dias_inventario) 
select sku_simple, avg(dias_inventario) from tbl_bi_ops_stock where in_stock = 1 and reserved = 0
group by sku_simple;

update tbl_bi_ops_stock inner join tbl_bi_ops_max_days_in_stock on 
tbl_bi_ops_stock.sku_simple= tbl_bi_ops_max_days_in_stock.sku_simple set 
tbl_bi_ops_stock.max_days_in_stock = tbl_bi_ops_max_days_in_stock.dias_inventario;

SELECT  'Interval',now();

update bazayaco.tbl_bi_ops_stock
set 
tbl_bi_ops_stock.interval_remaining_days_30=if(average_remaining_days_30 is null,"Infinito",
if(average_remaining_days_30<7,"< 8",if(average_remaining_days_30<15,"8-15", if(average_remaining_days_30<30,"15-29", 
if(average_remaining_days_30<45,"30-44",if(average_remaining_days_30<60,"45-59",
if(average_remaining_days_30<90,"60-89",if(average_remaining_days_30<120,"90-119",
if(average_remaining_days_30<150,"120-149",if(average_remaining_days_30<180,"150-179",
if(average_remaining_days_30<210,"180-209",if(average_remaining_days_30<240,"210-239",
if(average_remaining_days_30<270,"240-269",if(average_remaining_days_30<300,"270-299",
if(average_remaining_days_30<330,"300-229",if(average_remaining_days_30<=360,"330-360","> 360"))))))))))))))));

update bazayaco.tbl_bi_ops_stock
set 
tbl_bi_ops_stock.interval_remaining_days_sim_60=if(average_remaining_days is null,"Infinito",
if(average_remaining_days<7,"< 8",if(average_remaining_days<15,"8-15", if(average_remaining_days<30,"15-29", 
if(average_remaining_days<45,"30-44",if(average_remaining_days<60,"45-59",
if(average_remaining_days<90,"60-89",if(average_remaining_days<120,"90-119",
if(average_remaining_days<150,"120-149",if(average_remaining_days<180,"150-179",
if(average_remaining_days<210,"180-209",if(average_remaining_days<240,"210-239",
if(average_remaining_days<270,"240-269",if(average_remaining_days<300,"270-299",
if(average_remaining_days<330,"300-229",if(average_remaining_days<=360,"330-360","> 360"))))))))))))))));

update bazayaco.tbl_bi_ops_stock
set 
tbl_bi_ops_stock.interval_remaining_days_sim_7=if(average_remaining_days_sim_7 is null,"Infinito",
if(average_remaining_days_sim_7<7,"< 8",if(average_remaining_days_sim_7<15,"8-15", if(average_remaining_days_sim_7<30,"15-29", 
if(average_remaining_days_sim_7<45,"30-44",if(average_remaining_days_sim_7<60,"45-59",
if(average_remaining_days_sim_7<90,"60-89",if(average_remaining_days_sim_7<120,"90-119",
if(average_remaining_days_sim_7<150,"120-149",if(average_remaining_days_sim_7<180,"150-179",
if(average_remaining_days_sim_7<210,"180-209",if(average_remaining_days_sim_7<240,"210-239",
if(average_remaining_days_sim_7<270,"240-269",if(average_remaining_days_sim_7<300,"270-299",
if(average_remaining_days_sim_7<330,"300-229",if(average_remaining_days_sim_7<=360,"330-360","> 360"))))))))))))))));

SELECT  'Actualizar skus en stock',now();

truncate table skus_stock;
insert into skus_stock
select distinct(sku_simple) from tbl_bi_ops_stock
where in_stock_real=1 and reserved=0;

update bazayaco.tbl_bi_ops_stock 
set tbl_bi_ops_stock.sku_in_stock=1
where sku_simple in (select sku from bazayaco.skus_stock);

SELECT  'Actualizar binario inv < 2 meses de edad',now();
update bazayaco.tbl_bi_ops_stock 
set tbl_bi_ops_stock.inv_age_men_2=1
where dias_inventario<60 or dias_inventario is null;

UPDATE tbl_bi_ops_stock 
SET tbl_bi_ops_stock.fullfilment_type = "Consignation"
WHERE wh_location_before_sold like '%CONSI%';

SELECT  'Items cancelados en stock',now();
update tbl_bi_ops_stock 
set 
canceled= 1,
fullfilment_type='Inventory'
where status_item IN ('Cancelado', 'quebra tratada', 'quebrado', 'Precancelado', 'backorder_tratada') 
and in_stock=1 and reserved=0;

SELECT  'Binarios ítems vendidos por orden',now();
update tbl_bi_ops_stock 
set sold_last_30_order= case when datediff(curdate(),order_date)<=30 then 1 else 0 end
where canceled<>1;


#Ventas corporativas
update
tbl_bi_ops_stock
set fullfilment_type='Inventory'
where order_nr in (SELECT order_nr FROM bazayaco.vw_ventas_corporativas);

SELECT  'Datos de consignacion',now();

update bazayaco.tbl_bi_ops_stock
set consignation_item="SI"
where supplier_name in (select supplier_name from bazayaco.tbl_bi_ops_consignation_supplier);


update bazayaco.tbl_bi_ops_stock
set consignation_item="SI"
where sku_simple in (select sku from bazayaco.tbl_bi_ops_consignation_sku);

update tbl_bi_ops_stock a inner join wmsprod_co.estoque b on a.stock_item_id= b.estoque_id inner join wmsprod_co.itens_recebimento c on b.itens_recebimento_id=c.itens_recebimento_id
inner join wmsprod_co.recebimento d on c.recebimento_id=d.recebimento_id
set op=d.inbound_document_identificator;

UPDATE bazayaco.tbl_bi_ops_stock a
			INNER JOIN bob_live_co.catalog_config b ON a.sku_config = b.sku
			INNER JOIN bob_live_co.catalog_attribute_option_global_category c ON b.fk_catalog_attribute_option_global_category = c.id_catalog_attribute_option_global_category
			INNER JOIN bob_live_co.catalog_attribute_option_global_sub_category d ON b.fk_catalog_attribute_option_global_sub_category = d.id_catalog_attribute_option_global_sub_category
			INNER JOIN bob_live_co.catalog_attribute_option_global_sub_sub_category e ON b.fk_catalog_attribute_option_global_sub_sub_category = e.id_catalog_attribute_option_global_sub_sub_category
			SET a.category_com_main = c. NAME,
			a.category_com_sub = d. NAME,
			a.category_com_sub_sub = e. NAME ; 


INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'bazayaco.tbl_bi_ops_stock parte 1',
  'finish',
  NOW(),
  MAX(date_entrance),
  count(*),
  count(*)
FROM
  bazayaco.tbl_bi_ops_stock
;

SELECT  'Rutina finalizada: STOCK part 1, seguimos por favor con parte dos :)',now();

END
