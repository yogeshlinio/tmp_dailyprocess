BEGIN
SELECT  'OPS SMS',now();
TRUNCATE TABLE tbl_bi_ops_sms;



SELECT  'Insert cod_rastreamento',now();
INSERT INTO tbl_bi_ops_sms (cod_rastreo,numero_orden,metodo_pago)
SELECT entrega.cod_rastreamento,itens_venda.numero_order,pedidos_venda.metodo_de_pagamento
FROM  wms_db.status_itens_venda 
INNER JOIN wms_db.itens_venda ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id 
INNER JOIN wms_db.itens_entrega ON itens_venda.itens_venda_id = itens_entrega.itens_venda_id 
INNER JOIN wms_db.entrega ON itens_entrega.entrega_id = entrega.entrega_id
INNER JOIN wms_db.pedidos_venda ON itens_venda.order_id = pedidos_venda.order_id
WHERE DATE(status_itens_venda.data)
IN (SELECT max(dt) FROM bazayaco.calendar WHERE dt<curdate() AND isweekday=1 AND isholiday=0 ORDER BY dt DESC)
AND status_itens_venda.status='Expedido'
GROUP BY entrega.cod_rastreamento;


SELECT  'Actualizar datos cliente',now();
UPDATE tbl_bi_ops_sms
INNER JOIN wms_db.pedidos_venda ON tbl_bi_ops_sms.numero_orden=pedidos_venda.numero_pedido
SET
tbl_bi_ops_sms.nombre_cliente=pedidos_venda.nome_cliente,
tbl_bi_ops_sms.telefono_cliente=pedidos_venda.cob_telefone_cliente,
tbl_bi_ops_sms.celular_cliente=pedidos_venda.cob_mobile_client,
tbl_bi_ops_sms.cedula=pedidos_venda.cpf_cliente;


SELECT  'Actualizar guia y courrier',now();
UPDATE tbl_bi_ops_sms
INNER JOIN wms_db.tms_tracks ON tbl_bi_ops_sms.cod_rastreo=tms_tracks.cod_rastreamento
INNER JOIN wms_db.transportadoras ON tms_tracks.id_transportadora=transportadoras.transportadoras_id
SET
tbl_bi_ops_sms.guia=tms_tracks.track,
tbl_bi_ops_sms.courrier=transportadoras.nome_transportadora;


SELECT  'Actualizar el valor a pagar',now();
UPDATE tbl_bi_ops_sms
INNER JOIN wms_db.entrega ON tbl_bi_ops_sms.cod_rastreo=entrega.cod_rastreamento
INNER JOIN wms_db.delivery_invoice ON entrega.entrega_id=delivery_invoice.entrega_id
SET
tbl_bi_ops_sms.valor_pagar=delivery_invoice.total_depois_de_impostos;


SELECT  'Actualizar tabla de envios historicos',now();
INSERT INTO tbl_bi_ops_shipping_history (order_number,order_id,item_id,sku_simple,shipping_tracking_code_wms)
select itens_venda.numero_order,itens_venda.order_id,itens_venda.item_id,itens_venda.sku,entrega.cod_rastreamento
FROM  wms_db.status_itens_venda 
INNER JOIN wms_db.itens_venda ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id 
INNER JOIN wms_db.itens_entrega ON itens_venda.itens_venda_id = itens_entrega.itens_venda_id 
INNER JOIN wms_db.entrega ON itens_entrega.entrega_id = entrega.entrega_id
where date(status_itens_venda.data) in (select max(dt) from bazayaco.calendar where dt<curdate() and isweekday=1 and isholiday=0 order by dt desc)
AND status_itens_venda.status='Expedido';

SELECT  'Actualizar guia y courrier de histórico',now();
UPDATE tbl_bi_ops_shipping_history
INNER JOIN wms_db.tms_tracks ON tbl_bi_ops_shipping_history.shipping_tracking_code_wms=tms_tracks.cod_rastreamento
INNER JOIN wms_db.transportadoras ON tms_tracks.id_transportadora=transportadoras.transportadoras_id
SET
tbl_bi_ops_shipping_history.shipping_courrier_tracking_code=tms_tracks.track,
tbl_bi_ops_shipping_history.courrier_name=transportadoras.nome_transportadora
where shipping_courrier_tracking_code is null;


SELECT  'Actualizar tabla delivery_invoice_backup',now();
INSERT INTO bazayaco.delivery_invoice_backup (delivery_invoice_id,entrega_id,total_impostos,total_depois_de_impostos,data_criacao,status,guia_remision,invoice,num_factura,type_document)
SELECT delivery_invoice.delivery_invoice_id,delivery_invoice.entrega_id,delivery_invoice.total_impostos,delivery_invoice.total_depois_de_impostos,delivery_invoice.data_criacao,delivery_invoice.status,delivery_invoice.guia_remision,delivery_invoice.invoice,delivery_invoice.num_factura,delivery_invoice.type_document
FROM wms_db.delivery_invoice
WHERE delivery_invoice.delivery_invoice_id not in (select delivery_invoice_id from delivery_invoice_backup);

SELECT  'Rutina finalizada',now();
END
