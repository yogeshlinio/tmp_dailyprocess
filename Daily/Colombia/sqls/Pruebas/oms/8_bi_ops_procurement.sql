BEGIN

SELECT  'Calculating WorkDays',now();
call calcworkdays;
SELECT  'Finished Calculating WorkDays',now();


SELECT  'OPS Oms Report',now();
TRUNCATE TABLE bazayaco.tbl_bi_ops_procurement;
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'bazayaco.tbl_bi_ops_procurement',
  'start',
  NOW(),
  MAX(created_date),
  count(*),
  count(*)
FROM
  bazayaco.tbl_bi_ops_procurement
;

SELECT  'Insert procurement_order_item',now();
INSERT INTO bazayaco.tbl_bi_ops_procurement (id_procurement,unit_price,transport_type,created_date,update_date,is_deleted,tax,price_before_tax,
shipping_carrier,id_procurement_order,sku_received,last_time_changed_by_commercial)
SELECT procurement_order_item.id_procurement_order_item,procurement_order_item.unit_price,procurement_order_item.transport_type,
procurement_order_item.created_at,procurement_order_item.updated_at,procurement_order_item.is_deleted,procurement_order_item.tax,
procurement_order_item.price_before_tax,procurement_order_item.fk_shipment_carrier,fk_procurement_order,procurement_order_item.sku_received,
last_time_changed_by_commercial
FROM procurement_live_co.procurement_order_item
WHERE date(procurement_order_item.created_at)>='2013-07-01';

SELECT  'Update ',now();
UPDATE bazayaco.tbl_bi_ops_procurement
INNER JOIN procurement_live_co.procurement_order_item
ON tbl_bi_ops_procurement.id_procurement = procurement_order_item.id_procurement_order_item
INNER JOIN bob_live_co.catalog_simple
ON procurement_order_item.fk_catalog_simple = catalog_simple.id_catalog_simple
SET tbl_bi_ops_procurement.sku_simple = catalog_simple.sku;

SELECT  'Update ',now();
UPDATE bazayaco.tbl_bi_ops_procurement
INNER JOIN procurement_live_co.shipment_carrier
ON tbl_bi_ops_procurement.shipping_carrier = shipment_carrier.id_shipment_carrier
SET tbl_bi_ops_procurement.shipping_carrier_name = shipment_carrier.name;

SELECT  'Update ',now();
UPDATE bazayaco.tbl_bi_ops_procurement
INNER JOIN bazayaco.tbl_catalog_product_v2
ON tbl_bi_ops_procurement.sku_simple = tbl_catalog_product_v2.sku
SET tbl_bi_ops_procurement.product_name = tbl_catalog_product_v2.product_name,
tbl_bi_ops_procurement.buyer = tbl_catalog_product_v2.buyer,
tbl_bi_ops_procurement.category_1 = tbl_catalog_product_v2.cat1,
tbl_bi_ops_procurement.category_2 = tbl_catalog_product_v2.cat2,
tbl_bi_ops_procurement.category_3 = tbl_catalog_product_v2.cat3,
tbl_bi_ops_procurement.min_delivery_time = tbl_catalog_product_v2.min_delivery_time,
tbl_bi_ops_procurement.max_delivery_time = tbl_catalog_product_v2.max_delivery_time,
tbl_bi_ops_procurement.brand = tbl_catalog_product_v2.Brand;

SELECT  'Codigo de OP ',now();
UPDATE bazayaco.tbl_bi_ops_procurement
INNER JOIN procurement_live_co.procurement_order
ON tbl_bi_ops_procurement.id_procurement_order = procurement_order.id_procurement_order
SET tbl_bi_ops_procurement.venture_code = procurement_order.venture_code,
tbl_bi_ops_procurement.check_digit = procurement_order.check_digit,
tbl_bi_ops_procurement.fk_procurement_order_type = procurement_order.fk_procurement_order_type,
tbl_bi_ops_procurement.is_cancelled = procurement_order.is_cancelled,
tbl_bi_ops_procurement.delivered_to_wms = procurement_order.delivered_to_wms,
tbl_bi_ops_procurement.payment_status = procurement_order.payment_status,
tbl_bi_ops_procurement.fk_catalog_supplier = procurement_order.fk_catalog_supplier,
tbl_bi_ops_procurement.sent_at = procurement_order.sent_at;

########### corregir apenas ingresen los datos en oms
SELECT  'Codigo de OP ',now();
UPDATE bazayaco.tbl_bi_ops_procurement
INNER JOIN procurement_live_co.procurement_order
ON tbl_bi_ops_procurement.id_procurement_order = procurement_order.id_procurement_order
SET 
tbl_bi_ops_procurement.negotiation_type = procurement_order.procurement_payment_terms
WHERE date(procurement_order.created_at)>'2013-10-09';

SELECT  'negotiation_type NULL que no estan en oms',now();
UPDATE bazayaco.tbl_bi_ops_procurement
INNER JOIN procurement_live_co.catalog_supplier_attributes
ON tbl_bi_ops_procurement.fk_catalog_supplier = catalog_supplier_attributes.fk_catalog_supplier
SET tbl_bi_ops_procurement.negotiation_type = catalog_supplier_attributes.payment_terms
WHERE tbl_bi_ops_procurement.negotiation_type IS NULL;

SELECT  'Calculo Codigo de OP ',now();
UPDATE bazayaco.tbl_bi_ops_procurement
SET tbl_bi_ops_procurement.OP = CONCAT(venture_code,REPEAT('0', 7-LENGTH(tbl_bi_ops_procurement.id_procurement_order)), tbl_bi_ops_procurement.id_procurement_order,check_digit);

SELECT  'Status de tracking ',now();
UPDATE bazayaco.tbl_bi_ops_procurement
INNER JOIN procurement_live_co.procurement_order_item_date_history
ON tbl_bi_ops_procurement.id_procurement = procurement_order_item_date_history.id_procurement_order_item_date_history
SET tbl_bi_ops_procurement.collect_bob_calculated_date = procurement_order_item_date_history.collect_bob_calculated_date,
tbl_bi_ops_procurement.collect_negotiated_date = procurement_order_item_date_history.collect_negotiated_date,
tbl_bi_ops_procurement.collect_scheduled_date = procurement_order_item_date_history.collect_scheduled_date,
tbl_bi_ops_procurement.collect_real_date = procurement_order_item_date_history.collect_real_date,
tbl_bi_ops_procurement.delivery_bob_calculated_date = procurement_order_item_date_history.delivery_bob_calculated_date,
tbl_bi_ops_procurement.delivery_scheduled_date = procurement_order_item_date_history.delivery_scheduled_date,
tbl_bi_ops_procurement.delivery_real_date = procurement_order_item_date_history.delivery_real_date,
tbl_bi_ops_procurement.delivery_bob_original_calculated_date = procurement_order_item_date_history.delivery_bob_original_calculated_date;


SELECT  'Datos supplier',now();
UPDATE bazayaco.tbl_bi_ops_procurement
INNER JOIN procurement_live_co.catalog_supplier_attributes
ON tbl_bi_ops_procurement.fk_catalog_supplier = catalog_supplier_attributes.fk_catalog_supplier
SET 
tbl_bi_ops_procurement.nit = catalog_supplier_attributes.nit,
tbl_bi_ops_procurement.city_supplier = catalog_supplier_attributes.city,
tbl_bi_ops_procurement.tracker = catalog_supplier_attributes.tracker,
tbl_bi_ops_procurement.zone_supplier = catalog_supplier_attributes.zone,
tbl_bi_ops_procurement.credit_limit = catalog_supplier_attributes.credit_limit,
tbl_bi_ops_procurement.supplier_name = catalog_supplier_attributes.company_name;

SELECT  'Actualizar tracker tabla de trackers and supplier',now();
UPDATE bazayaco.tbl_bi_ops_procurement
INNER JOIN bazayaco.tbl_bi_ops_tracking_suppliers
ON tbl_bi_ops_procurement.supplier_name=tbl_bi_ops_tracking_suppliers.supplier_name
SET tbl_bi_ops_procurement.tracker = tbl_bi_ops_tracking_suppliers.tracker_name;

SELECT  'Truncate Tabla Procurement Pending',now();
TRUNCATE TABLE bazayaco.tbl_bi_ops_procurement_pending;

SELECT  'Insert Tabla Procurement Pending',now();
INSERT INTO bazayaco.tbl_bi_ops_procurement_pending (sku,cantidad)
SELECT tbl_bi_ops_procurement.sku_simple,count(tbl_bi_ops_procurement.sku_simple)
FROM bazayaco.tbl_bi_ops_procurement
WHERE sku_received=0
GROUP BY tbl_bi_ops_procurement.sku_simple;

SELECT  'Type PO',now();
UPDATE bazayaco.tbl_bi_ops_procurement
SET tbl_bi_ops_procurement.type='Crossdocking'
WHERE tbl_bi_ops_procurement.fk_procurement_order_type=6;

SELECT  'Type PO',now();
UPDATE bazayaco.tbl_bi_ops_procurement
SET tbl_bi_ops_procurement.type='Stock'
WHERE tbl_bi_ops_procurement.fk_procurement_order_type=1;

SELECT  'Type PO',now();
UPDATE bazayaco.tbl_bi_ops_procurement
SET tbl_bi_ops_procurement.type='Consignment'
WHERE tbl_bi_ops_procurement.fk_procurement_order_type=7;

SELECT  'Type PO',now();
UPDATE bazayaco.tbl_bi_ops_procurement
SET tbl_bi_ops_procurement.type='Invoice'
WHERE tbl_bi_ops_procurement.fk_procurement_order_type=9;

SELECT  'Mes de creacion PO',now();
UPDATE bazayaco.tbl_bi_ops_procurement
SET bazayaco.tbl_bi_ops_procurement.month=month(created_date);

SELECT  'Dia de creacion PO',now();
UPDATE bazayaco.tbl_bi_ops_procurement
SET bazayaco.tbl_bi_ops_procurement.day=day(created_date);

SELECT  'Collect Supplier',now();
UPDATE bazayaco.tbl_bi_ops_procurement
SET bazayaco.tbl_bi_ops_procurement.collect_supplier='RED POSTAL'
WHERE city_supplier='SOACHA'
OR city_supplier='BOGOTA';

SELECT  'Collect Supplier',now();
UPDATE bazayaco.tbl_bi_ops_procurement
SET bazayaco.tbl_bi_ops_procurement.collect_supplier='RED POSTAL'
WHERE city_supplier='SOACHA'
OR city_supplier='BOGOTA';

SELECT  'Ontime Collect Red Postal',now();
UPDATE bazayaco.tbl_bi_ops_procurement
SET bazayaco.tbl_bi_ops_procurement.on_time_collect=1
WHERE (date(collect_real_date)=date(delivery_real_date) OR date(collect_scheduled_date)=date(delivery_real_date))
AND collect_supplier='RED POSTAL';

#SELECT  'Calculo dias habiles collect_scheduled_date_2wd',now();
#UPDATE bazayaco.tbl_bi_ops_procurement
#SET bazayaco.tbl_bi_ops_procurement.collect_scheduled_date_2wd = bazayaco.workday_bi(date(collect_scheduled_date),2)
#WHERE collect_scheduled_date IS NOT NULL;
SELECT  'Calculo dias habiles collect_scheduled_date_2wd',now();
UPDATE        tbl_bi_ops_procurement 
   INNER JOIN calcworkdays
           ON     date( collect_scheduled_date ) = calcworkdays.date_last 
              AND calcworkdays.workdays = 2 
              AND isweekday = 1
 			        AND isholiday = 0
SET 
   collect_scheduled_date_2wd = calcworkdays.date_first 
WHERE collect_scheduled_date IS NOT NULL;


SELECT  'Ontime TCC',now();
UPDATE bazayaco.tbl_bi_ops_procurement
SET bazayaco.tbl_bi_ops_procurement.on_time_collect=1
WHERE (collect_scheduled_date_2wd<=delivery_real_date OR collect_scheduled_date<=delivery_real_date)
AND collect_supplier='TCC';

SELECT  'Table amount',now();
TRUNCATE TABLE bazayaco.tbl_bi_ops_procurement_op;
INSERT INTO bazayaco.tbl_bi_ops_procurement_op (OP,amount_sku)
SELECT OP,count(OP)
FROM bazayaco.tbl_bi_ops_procurement
GROUP BY OP;

SELECT  'Amount sku',now();
UPDATE bazayaco.tbl_bi_ops_procurement
INNER JOIN bazayaco.tbl_bi_ops_procurement_op
ON tbl_bi_ops_procurement.OP=tbl_bi_ops_procurement_op.OP
SET tbl_bi_ops_procurement.amount_po=tbl_bi_ops_procurement_op.amount_sku;

SELECT  'Amount sku',now();
UPDATE bazayaco.tbl_bi_ops_procurement
SET tbl_bi_ops_procurement.porcentaje_amount_PO=(item_counter/amount_po);

SELECT  'Amount sku',now();
UPDATE bazayaco.tbl_bi_ops_procurement
SET tbl_bi_ops_procurement.porcentaje_on_time=(on_time_collect/amount_po);

SELECT  'Semana de creacion OP',now();
UPDATE bazayaco.tbl_bi_ops_procurement
SET week_created=weekofyear(date(created_date));

SELECT  'Update deposit',now();
UPDATE bazayaco.tbl_bi_ops_procurement
INNER JOIN procurement_live_co.procurement_order_item
ON tbl_bi_ops_procurement.id_procurement = procurement_order_item.id_procurement_order_item
INNER JOIN procurement_live_co.procurement_order_payment
ON procurement_order_item.fk_procurement_order = procurement_order_payment.fk_procurement_order
SET tbl_bi_ops_procurement.deposit=procurement_order_payment.deposit;

#Status de Invoice, adjunto, y numero
SELECT  'Update Invoice Item',now();
UPDATE bazayaco.tbl_bi_ops_procurement as a
INNER JOIN procurement_live_co.invoice_item as b
ON a.id_procurement=b.fk_procurement_order_item
INNER JOIN procurement_live_co.invoice as c
ON b.fk_invoice=c.id_invoice
SET a.invoice_adjunto=c.adjunto,
a.invoice_nr=c.invoice_nr,
a.invoice_item_status=c.status;

SELECT  'cambio de termino de pago OP maritza',now();
UPDATE bazayaco.tbl_bi_ops_procurement
SET negotiation_type='CREDITO 45 DIAS'
WHERE OP='LIN00158657';

UPDATE bazayaco.tbl_bi_ops_procurement
SET negotiation_type='CREDITO 30 DIAS'
where supplier_name like '%Intcomex%'
and date(created_date) > '2013-07-19';

#Actualiza el repurchase status sacado de catalog product v2
update bazayaco.tbl_bi_ops_procurement s
inner join bazayaco.tbl_catalog_product_v2 c
on s.sku_simple = c.sku
set s.repurchase_status = c.repurchase_status;


INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'bazayaco.tbl_bi_ops_procurement',
  'finish',
  NOW(),
  MAX(created_date),
  count(*),
  count(*)
FROM
  bazayaco.tbl_bi_ops_procurement
;

SELECT  'price_after_vat_sku_received',now();
UPDATE tbl_bi_ops_procurement
SET price_after_vat_sku_received = (tax)+(price_before_tax)
WHERE sku_received = 1;

SELECT  'price_after_vat_faltantes',now();
UPDATE tbl_bi_ops_procurement
SET price_after_vat_faltantes = (tax)+(price_before_tax)
WHERE sku_received = 0;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'bazayaco.tbl_bi_ops_procurement',
  'finish',
  NOW(),
  MAX(created_date),
  count(*),
  count(*)
FROM
  bazayaco.tbl_bi_ops_procurement
;

SELECT  'Rutina finalizada bi_ops_procurement',now();

END
