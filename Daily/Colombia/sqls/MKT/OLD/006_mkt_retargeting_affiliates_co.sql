call marketing_report.colombia_data;
call SEM.google_optimization_ad_group_co;
call SEM.google_optimization_transaction_id_co;
call SEM.google_optimization_keyword_co;
call SEM.google_optimization_transaction_id_keyword_co;
call production.retargeting_affiliates_co;
call facebook.facebook_optimization_co;
call facebook.fanpage_optimization_co;
call facebook.facebook_optimization_keyword_co;
call production_co.channel_report;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia',  #Country
  'marketing_report.marketing_co',
  NOW(),
  NOW(),
  1,
  1;

INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Colombia',
  'marketing_report.marketing_co',
  NOW(),
  NOW(),
  1,
  1
;

