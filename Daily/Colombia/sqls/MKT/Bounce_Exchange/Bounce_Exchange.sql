LOAD DATA LOCAL INFILE '@v_file@'
IGNORE INTO TABLE @v_db@.@v_tables@_sample
FIELDS TERMINATED BY ','
IGNORE 1 LINES;


INSERT INTO @v_db@.@v_tables@
SELECT
   '@v_country@',
   '@v_date@',
   @v_db@.@v_tables@_sample.*,
   NULL, NULL, NULL
FROM
@v_db@.@v_tables@_sample
;

TRUNCATE TABLE @v_db@.@v_tables@_sample
;