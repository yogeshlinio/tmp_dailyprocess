UPDATE A_CRM_CAMPAIGN_EMAIL_SAMPLE INNER JOIN 
      ( 
            SELECT
              `Campaign-ID`   AS idCampaing,
              `Mailing-Name`    ,
              `Campaign-Name` AS Description  ,
              `UniqueKey`     AS email,
               min( date(`Dispatch-Start` ) ) as date,
              #`Bounce-Text` AS bounce_reason,
               `Bounce-Type` AS bounce_reason,
              count(*) as bounceCount
             FROM campaing_bounces_sample
             GROUP BY  `Mailing-Name` ,UniqueKey
       )  AS TMP
     USING (  `Mailing-Name`,email)
SET   
    A_CRM_CAMPAIGN_EMAIL_SAMPLE.date = TMP.date ,
    A_CRM_CAMPAIGN_EMAIL_SAMPLE.dateBounce    = TMP.date ,
    A_CRM_CAMPAIGN_EMAIL_SAMPLE.BounceCount = TMP.bounceCount,
    A_CRM_CAMPAIGN_EMAIL_SAMPLE.bounce = 1,
    A_CRM_CAMPAIGN_EMAIL_SAMPLE.bounce_reason = TMP.bounce_reason
;

UPDATE A_CRM_CAMPAIGN_EMAIL_SAMPLE INNER JOIN 
      ( 
            SELECT
               `Campaign-ID`   AS idCampaing,
               `Mailing-Name`,
              `Campaign-Name` as Description  ,
                UniqueKey AS email,
               min( date(`Dispatch-Start` ) ) as date,
              count(*) as OpenCount
                
             FROM campaing_openings_sample
             GROUP BY `Mailing-Name`,UniqueKey
       )  AS TMP
     USING ( `Mailing-Name`,email)
SET   
    A_CRM_CAMPAIGN_EMAIL_SAMPLE.date = LEAST( TMP.date ,A_CRM_CAMPAIGN_EMAIL_SAMPLE.date  ) ,
    A_CRM_CAMPAIGN_EMAIL_SAMPLE.dateOpen   = TMP.date ,
    A_CRM_CAMPAIGN_EMAIL_SAMPLE.OpenCount  = TMP.OpenCount,
    A_CRM_CAMPAIGN_EMAIL_SAMPLE.`open` = 1
;

UPDATE A_CRM_CAMPAIGN_EMAIL_SAMPLE INNER JOIN 
      ( 
            SELECT
               `Campaign-ID`   AS idCampaing,
               `Mailing-Name`,
              `Campaign-Name` as Description  ,
                UniqueKey AS email,
               min( date(`Dispatch-Start` ) ) as date,
              `Link-URL` AS Url,
              count(*) as ClicksCount
             FROM campaing_clicks_sample
             GROUP BY `Mailing-Name`,`UniqueKey`
       )  AS TMP
     USING ( `Mailing-Name`,email)
SET   
    A_CRM_CAMPAIGN_EMAIL_SAMPLE.date = LEAST( TMP.date ,A_CRM_CAMPAIGN_EMAIL_SAMPLE.date  ) ,
    A_CRM_CAMPAIGN_EMAIL_SAMPLE.dateClick   = TMP.date ,
    A_CRM_CAMPAIGN_EMAIL_SAMPLE.ClickCount = TMP.ClicksCount,
    A_CRM_CAMPAIGN_EMAIL_SAMPLE.click = 1,
    A_CRM_CAMPAIGN_EMAIL_SAMPLE.Url = TMP.Url,
    A_CRM_CAMPAIGN_EMAIL_SAMPLE.Category = SUBSTRING_INDEX(      TRIM( LEADING "http://fagms.net/"
                                                     FROM TRIM( LEADING "http://www.linio.com.co##"        
                                                     FROM TRIM( LEADING "http://blog.linio.com.co/"
                                                     FROM TRIM( LEADING "http://www.linio.com.co/" FROM 
                                     TMP.Url )))), '/', 1 )
;

/*
update             A_CRM_CAMPAIGN_EMAIL_SAMPLE AS CMR 
       inner JOIN  interface_openings   AS a
               on a.UniqueKey=CMR.email
set  
   CMR.subscribe   = IF( Alias = 'Subscribe Newsletter', date(`Timestamp`), subscribe),
   CMR.unsubscribe = IF( Alias = 'desuscribirse', date(`Timestamp`), unsubscribe) 
WHERE
   Alias IN (  'Subscribe Newsletter' , 'desuscribirse' ) 
;
*/

DROP TEMPORARY TABLE IF EXISTS CustomerGross;
CREATE TEMPORARY TABLE CustomerGross 
( PRIMARY KEY ( CustomerEmail ) )
SELECT
   CustomerEmail,
   IF( SUM( OrderBeforeCan ) > 0 , 1, 0 ) AS Gross,
   IF( SUM( OrderAfterCan  ) > 0 , 1 ,0 ) AS Net
FROM
   development_co.A_Master
WHERE
   OrderBeforeCan = 1
 OR OrderAfterCan = 1
GROUP BY CustomerEmail;
   
update             A_CRM_CAMPAIGN_EMAIL_SAMPLE AS CMR 
       inner JOIN CustomerGross AS a
               on a.CustomerEmail=CMR.email
set  
     CMR.is_Gross = a.Gross,
     CMR.is_Net   = a.Net
;

DROP  TABLE IF EXISTS TMP_MSG;
CREATE TABLE TMP_MSG ( INDEX  ( `Mailing-Name`,email) )
SELECT
   `Campaign-ID`   AS idCampaing,
   `Mailing-Name`,
   `Campaign-Name` as Description  ,
   UniqueKey AS email,
   min( date(`Dispatch-Start` ) ) as date,
   count(*) as MessageCount
                
FROM campaing_messages_sample
GROUP BY `Mailing-Name`,UniqueKey
;

UPDATE A_CRM_CAMPAIGN_EMAIL_SAMPLE INNER JOIN 
       TMP_MSG  AS TMP
     USING ( `Mailing-Name`,email)
SET   
    A_CRM_CAMPAIGN_EMAIL_SAMPLE.date = LEAST( TMP.date ,A_CRM_CAMPAIGN_EMAIL_SAMPLE.date  ) ,
    A_CRM_CAMPAIGN_EMAIL_SAMPLE.dateMessage   = TMP.date ,
    A_CRM_CAMPAIGN_EMAIL_SAMPLE.MessageCount  = TMP.MessageCount,
    A_CRM_CAMPAIGN_EMAIL_SAMPLE.Message = 1
;
DROP  TABLE IF EXISTS TMP_MSG;


INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'CRM.A_CRM_CAMPAIGN_EMAIL_SAMPLE',
  'finish',
  now(),
  now(),
  count(*),
  count(*)
FROM
  A_CRM_CAMPAIGN_EMAIL_SAMPLE;