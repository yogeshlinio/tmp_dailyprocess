INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'CRM.loading_channels',
  'finish',
  now(),
  max( Timestamp ),
  count(*),
  count(*)
FROM
  campaing_messages_sample
;
