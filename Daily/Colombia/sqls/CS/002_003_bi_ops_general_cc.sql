SELECT  'Inicio rutina CC Inbound',now();

truncate table bi_ops_cc_survey;

SELECT  'Insertar datos a la tabla inicial',now();
insert into bi_ops_cc_survey(asteriskid,call_datetime,call_date,call_time) 
SELECT asteriskID, fecha, date(fecha), time(fecha) FROM evento where mensaje like '%callerid%';

SELECT  'Extraer uniqueid',now();

UPDATE bi_ops_cc_survey
set uniqueid=asteriskid;

UPDATE bi_ops_cc_survey
set uniqueid= left(uniqueid,instr(uniqueid,'--')-1)  
where uniqueid like '%--%';

UPDATE bi_ops_cc_survey
set uniqueid= right(uniqueid,length(uniqueid)-instr(uniqueid,'-')) ;

update bi_ops_cc_survey
set 
uniqueid=left(uniqueid,instr(uniqueid,'-')-1)
where uniqueid like '%-%';


SELECT  'Agregar información de los asesores', now();
update bi_ops_cc_survey a inner join bi_ops_cc b on a.uniqueid=b.uniqueid
set a.agent=b.agent,
a.agent_name=b.agent_name,
a.agent_ext=b.agent_ext;

#Actualizar otras extensiones
update bi_ops_cc a inner join bi_ops_cc_survey b
on a.uniqueid=b.uniqueid
set
a.transfered_survey=1,
a.answered_2=answer_2,
a.good=answer2_option_1,
a.regular=answer2_option_2;

SELECT  'Tiempos de finalización de la llamada',now();

truncate table bi_ops_cc_survey_last;

insert into bi_ops_cc_survey_last(time,asteriskid,respuestas) SELECT 
time(fecha) as 'time',asteriskID,right(mensaje,length(mensaje)-instr(mensaje,'_')) as 'respuestas'
FROM evento where 
mensaje  like '%respuestas%';

update bi_ops_cc_survey a inner join bi_ops_cc_survey_last b on a.asteriskid=b.asteriskID
set a.time_end=b.time,
a.respuestas=b.respuestas;

truncate table bi_ops_cc_survey_last;

insert into bi_ops_cc_survey_last(fecha,asteriskid) SELECT max(fecha) as 'fecha',asteriskID FROM evento group by asteriskID;

update bi_ops_cc_survey a inner join bi_ops_cc_survey_last b on a.asteriskid=b.asteriskid 
set time_end=time(b.fecha)
where time_end is null;

#Determinar duracion de la llamada
SELECT  'Duracion de la llamada',now();
update bi_ops_cc_survey 
set call_duration=hour(subtime(time_end,call_time))*3600+minute(subtime(time_end,call_time))*60+second(subtime(time_end,call_time));

#actualizar respuestas
update bi_ops_cc_survey
set 
respuestas=0
where respuestas is null ;

update bi_ops_cc_survey
set 
abandon_in=1
where respuestas=0 ;


#Actualizar info de los agentes
SELECT  'Coordinadores',now();
UPDATE bi_ops_cc_survey a inner join bi_ops_CS_matrix b on a.agent_ext=b.phone
set
a.coordinator=b.coordinator;

SELECT  'Actualizar respuestas',now();
UPDATE bi_ops_cc_survey a inner join respuesta b on a.asteriskid=b.recordID
set
answer_1=1
 where b.id_pregunta=1;

UPDATE bi_ops_cc_survey a inner join respuesta b on a.asteriskid=b.recordID
set
answer1_option_1=1
 where b.id_pregunta=1 and b.respuesta=1;

UPDATE bi_ops_cc_survey a inner join respuesta b on a.asteriskid=b.recordID
set
answer1_option_2=1
 where b.id_pregunta=1 and b.respuesta=2;

UPDATE bi_ops_cc_survey a inner join respuesta b on a.asteriskid=b.recordID
set
answer_2=1
 where b.id_pregunta=2;

UPDATE bi_ops_cc_survey a inner join respuesta b on a.asteriskid=b.recordID
set
answer2_option_1=1
 where b.id_pregunta=2 and b.respuesta=1;

UPDATE bi_ops_cc_survey a inner join respuesta b on a.asteriskid=b.recordID
set
answer2_option_2=1
 where b.id_pregunta=2 and b.respuesta=2;

UPDATE bi_ops_cc_survey a inner join respuesta b on a.asteriskid=b.recordID
set
answer2_option_3=1
 where b.id_pregunta=2 and b.respuesta=3;

UPDATE bi_ops_cc_survey a inner join respuesta b on a.asteriskid=b.recordID
set
answer_3=1
 where b.id_pregunta=3;

UPDATE bi_ops_cc_survey a inner join respuesta b on a.asteriskid=b.recordID
set
answer3_option_1=1
 where b.id_pregunta=3 and b.respuesta=1;

UPDATE bi_ops_cc_survey a inner join respuesta b on a.asteriskid=b.recordID
set
answer3_option_2=1
 where b.id_pregunta=3 and b.respuesta=2;

SELECT  'Actualizar abandonos',now();
UPDATE bi_ops_cc_survey a inner join respuesta b on a.asteriskid=b.recordID
set
answer1_abandon=if((answer1_option_1+answer1_option_2)=1,0,1),
answer2_abandon=if((answer2_option_1+answer2_option_2+answer2_option_3)=1,0,1),
answer3_abandon=if((answer3_option_1+answer3_option_2)=1,0,1);

truncate table bi_ops_cc_survey_caller_id ;

#Caller_id
insert into bi_ops_cc_survey_caller_id (asteriskid,callerid) select asteriskID,right(mensaje,length(mensaje)-9) as 'caller_id'  FROM evento where mensaje like '%callerid_%' ;

update
 bi_ops_cc_survey a inner join bi_ops_cc_survey_caller_id b 
on a.asteriskid=b.asteriskID 
set a.callerid=b.callerid;

update bi_ops_cc a inner join bi_ops_cc_survey b
on a.uniqueid=b.uniqueid
set
a.transfered_survey=1,
a.answered_2=answer_2,
a.good=answer2_option_1,
a.regular=answer2_option_2;

SELECT  'Rutina finalizada',now();
