SELECT  'Inicio rutina bi_ops_cc',now();

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'customer_service_co.bi_ops_cc',
  'start',
  NOW(),
  MAX(event_date),
  count(*),
  count(*)
FROM
  customer_service_co.bi_ops_cc;


SELECT  'Matriz de servicio al cliente',now();

#Zona horaria de Colombia

set time_zone='-5:00';

#Actualizar datos de la matriz de servicio al cliente..
-- ss
update bi_ops_CS_matrix a 
inner join bi_ops_CS_matrix_sac b on a.identification=b.identification
set 
a.phone=b.phone,
a.name=b.name,
a.coordinator=b.coordinator,
a.process=b.process,
a.shift_pattern=b.shift_pattern,
a.birthday=b.birthday,
a.cellphone=b.cellphone,
a.mail=b.mail,
a.start_date=b.start_date,
a.antique=b.antique,
a.status=b.status,
a.date_change_status=b.date_change_status,
a.temporal=b.temporal;

update bi_ops_CS_matrix a 
inner join bi_ops_CS_matrix_ventas b on a.identification=b.identification
set 
a.phone=b.phone,
a.name=b.name,
a.coordinator=b.coordinator,
a.process=b.process,
a.shift_pattern=b.shift_pattern,
a.birthday=b.birthday,
a.cellphone=b.cellphone,
a.mail=b.mail,
a.start_date=b.start_date,
a.antique=b.antique,
a.status=b.status,
a.date_change_status=b.date_change_status,
a.temporal=b.temporal;

insert into bi_ops_CS_matrix
select a.* from bi_ops_CS_matrix_sac a 
left join bi_ops_CS_matrix b on a.identification=b.identification
where b.phone is null and a.identification<>0;

insert into bi_ops_CS_matrix
select a.* from bi_ops_CS_matrix_ventas a 
left join bi_ops_CS_matrix b on a.identification=b.identification
where b.phone is null and a.identification<>0;

SELECT  'Inicio rutina CC Inbound',now();

truncate table new_queue_stats;

insert into new_queue_stats(queue_stats_id,uniqueid,datetime,qname,qagent,qevent,info1,info2,info3,info4,info5) 
SELECT * FROM queue_stats_2013;

insert into new_queue_stats(queue_stats_id,uniqueid,datetime,qname,qagent,qevent,info1,info2,info3,info4,info5) 
SELECT * FROM queue_stats
where datetime>
(select max(datetime) from customer_service_co.queue_stats_2013);


SELECT  'Registro de datos new_queue_stats',now();
update new_queue_stats a inner join qevent b on a.qevent=b.event_id
inner join qname c on a.qname=c.queue_id
set call_time=if(event in ('COMPLETEAGENT','COMPLETECALLER'),info2,IF(event='TRANSFER',left(info4,instr(info4,'|')-1),null)),
hold_time=if(event in ('COMPLETEAGENT','COMPLETECALLER'),info1,if(event in ('ABANDON','EXITWITHTIMEOUT'),info3,if(event='TRANSFER',info3,null))),
start_position= if(event in ('COMPLETEAGENT','COMPLETECALLER'),info3,IF(event='TRANSFER', right(info4,length(info4)-instr(info4,'|')),if(event in ('ABANDON','EXITWITHTIMEOUT'),info2,null))),
end_position=if(event in ('ABANDON','EXITWITHTIMEOUT'),info1,null)
where b.event in ('ABANDON','COMPLETEAGENT','COMPLETECALLER','CONNECT','TRANSFER','EXITWITHTIMEOUT','RINGNOANSWER')
and c.queue in ('ivr_cola1','ivr_cola2','ivr_cola3');

#set @max_day=cast((select max(event_date) from bi_ops_cc) as date);

#delete from bi_ops_cc
#where event_date>=@max_day;
truncate table bi_ops_cc;

SELECT  'Insertar datos bi_ops_cc',now();
insert into bi_ops_cc (queue_stats_id, uniqueid, event_datetime, event_date, queue, agent, event, call_duration_seg, hold_duration_seg, start_position, end_position,  event_hour, event_minute, event_weekday, event_week, event_month, event_year) 
SELECT a.queue_stats_id, a.uniqueid, a.datetime,date(a.datetime),c.queue,d.agent,b.event, a.call_time,a.hold_time,a.start_position,a.end_position,hour(a.datetime), minute(a.datetime),
weekday(a.datetime),weekofyear(a.datetime),monthname(a.datetime),year(datetime) FROM new_queue_stats a inner join qevent b on a.qevent=b.event_id
inner join qname c on a.qname=c.queue_id inner join qagent d on a.qagent=d.agent_id 
where b.event in ('ABANDON','COMPLETEAGENT','COMPLETECALLER','CONNECT','TRANSFER','RINGNOANSWER')
and c.queue in ('ivr_cola1','ivr_cola2','ivr_cola3') and date(a.datetime)>='2013-10-01'#@max_day
group by uniqueid,event,queue;



SELECT  'Actualizar numeros de clientes',now();

truncate table qnumbers;

insert into qnumbers(uniqueid,number,datetime) select uniqueid, info2,datetime from new_queue_stats where qevent = 15 group by uniqueid;

update bi_ops_cc a inner join qnumbers b on a.uniqueid=b.uniqueid 
set 
a.number=b.number,
a.event_datetime=if(event='ABANDON',b.datetime,event_datetime);


SELECT  'Actualizar datos de espera del agente',now();
truncate table qwait;

insert into qwait(uniqueid,datetime,hold_time_agent) select uniqueid, datetime,info3 from new_queue_stats where qevent = 12 group by uniqueid;

update bi_ops_cc a inner join qwait b on a.uniqueid=b.uniqueid 
set 
a.event_datetime=datetime,
a.agent_hold_time_seg=b.hold_time_agent
where event in ('COMPLETEAGENT','COMPLETECALLER','TRANSFER');

SELECT  'Actualizar grabaciones',now();
update bi_ops_cc a inner join cdr b on a.uniqueid=b.uniqueid
set 
a.record=b.record;


truncate table bi_ops_agent_names;

SELECT  'Phones vs ext',now();
insert into bi_ops_agent_names
select a.agent,if(length=1,concat('200',number),if(length=2,concat('20',number),if(length=3,
concat('2',number),null))) as ext from
(SELECT agent,right(agent,length(agent)-9) as 'number',length(right(agent,length(agent)-9)) 
as 'length' FROM qagent where agent like '%phone%' order by agent asc) as a;

insert into bi_ops_agent_names
select a.agent,right(agent, length(agent)-4) as ext from
(SELECT agent,right(agent,length(agent)-9) as 'number',length(right(agent,length(agent)-9)) 
as 'length' FROM qagent where agent like '%SIP%' and agent not like '%phone%' and agent not like '%panda%' 
order by agent asc) as a;

insert into bi_ops_agent_names
SELECT agent, right(agent, length(agent)-6) FROM qagent where agent like 'Agent%' 
order by agent asc;

SELECT  'Insertar nombres',now();
update bi_ops_cc a inner join bi_ops_agent_names b on a.agent=b.agent inner join 
bi_ops_CS_matrix c on b.ext=c.phone
set a.agent_name=c.name,
a.agent_ext=b.ext
where c.status='ACTIVO'
and process in ('SAC','VENTAS','INBOUND')
and event_date>'2013-12-09';

update bi_ops_cc a inner join bi_ops_agent_names b on a.agent=b.agent inner join 
bi_ops_CS_matrix c on b.ext=c.phone
set a.agent_name=c.name,
a.agent_ext=b.ext
where a.agent_name is null
and event_date>'2013-12-09';

update bi_ops_cc a inner join bi_ops_agent_names b on a.agent=b.agent inner join 
bi_ops_CS_matrix_antext c on b.ext=c.phone
set a.agent_name=c.name,
a.agent_ext=b.ext
where c.status='ACTIVO'
and process in ('SAC','VENTAS','INBOUND')
and a.agent_name is null
and event_date<='2013-12-09';

update bi_ops_cc a inner join bi_ops_agent_names b on a.agent=b.agent inner join 
bi_ops_CS_matrix_antext  c on b.ext=c.phone
set a.agent_name=c.name,
a.agent_ext=b.ext
where a.agent_name is null
and event_date<='2013-12-09';

update bi_ops_cc a 
set a.agent_name="OTRO" where a.agent_name is null;

update bi_ops_cc 
set 
event_minute=minute(event_datetime),
event_hour=hour(event_datetime)
where event_minute is null;

SELECT  'Actualizar shifts',now();

update bi_ops_cc 
set event_shift=if(event_minute<30,concat(event_hour,':','00'),concat(event_hour,':','30'))
where event_shift is null;


SELECT  'Actualizar holidays',now();
update bi_ops_cc a inner join bazayaco.calendar b on a.event_date=b.dt
set holiday=b.isholiday
where holiday is null;

SELECT  'Actualizar horas laborales',now();
update bi_ops_cc
set workinghour=if(event_weekday=6 or holiday=1,
	if(event_hour>=10 and event_hour<18,
		1,0),
		if(event_weekday=5,
			if(event_hour>=9 and event_hour<21,
			1,0),
			if(event_hour>=8 and event_hour<21,
				1,0)))
#where event_date>=@max_day
;


SELECT  'Actualizar net_events',now();
update bi_ops_cc
set net_event=1
where event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON','EXITWITHTIMEOUT') 
and workinghour=1
#and event_date>=@max_day;
;

update bi_ops_cc 
set net_event=1
where event_date in ('2013-03-10', '2013-03-17')
and event in ('COMPLETECALLER','COMPLETEAGENT','ABANDON','TRANSFER','EXITWITHTIMEOUT');


update bi_ops_cc 
set net_event=if(event_hour=18 and event_minute>5,0,if(event_hour=9 and event_minute<55,0,1))
where net_event=0 and agent<>'NONE' and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER') 
AND (event_weekday=6 or holiday=1) and (event_shift='18:00:00' or event_shift='9:30:00')
;

update bi_ops_cc 
set net_event=if(event_hour=21 and event_minute>5,0,if(event_hour=8 and event_minute<55,0,1))
where net_event=0 and agent<>'NONE' and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER') 
AND event_weekday=5 and holiday=0 and (event_shift='21:00:00' or event_shift='8:30:00')
;

update bi_ops_cc 
set net_event=if(event_hour=21 and event_minute>5,0,if(event_hour=7 and event_minute<55,0,1))
where net_event=0 and agent<>'NONE' and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER') 
AND event_weekday in(0,1,2,3,4) and holiday=0 and (event_shift='21:00:00' or event_shift='7:30:00')
;

SELECT  'Extra queries',now();

#Agosto 25 de 2013: Programación hasta las 9 p.m.
update bi_ops_cc 
set net_event=1
where event_date='2013-08-25' and event_hour>18 and event_hour<21;

#Agosto 29 de 2013: Programación hasta las 11 p.m. por promociones de marketing
update bi_ops_cc 
set net_event=1
where event_date='2013-08-29' and event_hour>21 and event_hour<23
and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON');

#Horario adicional Back Friday y Cyber Monday 2013
update  bi_ops_cc
set
net_event=1,
additional_time=1
where event_date='2013-11-27'
and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON')
and event_hour>=21;

update  bi_ops_cc
set
net_event=1,
additional_time=1
where event_date='2013-11-28'
and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON')
and (event_hour>=21 or event_hour<8);

update  bi_ops_cc
set
net_event=1,
additional_time=1
where event_date='2013-11-29'
and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON')
and (event_hour>=21 or event_hour<8);

update  bi_ops_cc
set
net_event=1,
additional_time=1
where event_date='2013-11-30'
and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON')
and (event_hour>=21 or event_hour<9);

update  bi_ops_cc
set
net_event=1,
additional_time=1
where event_date='2013-12-01'
and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON')
and (event_hour>=18 or (event_hour<10 and event_hour>=6));

update  bi_ops_cc
set
net_event=1,
additional_time=1
where event_date='2013-12-02'
and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON')
and (event_hour>=21 or event_hour<8);

update  bi_ops_cc
set
net_event=1,
additional_time=1
where event_date='2013-12-03'
and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON')
and (event_hour>=21 or event_hour<8);

#Horario adicional Diciembre
update  bi_ops_cc
set
net_event=1,
additional_time=1
where event_date='2013-12-17'
and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON')
and event_hour>=21;

update  bi_ops_cc
set
net_event=1,
additional_time=1
where event_date='2013-12-19'
and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON')
and event_hour>=21;

#Quitar llamadas en horario adicional de postventa
update bi_ops_cc
set net_event=0
where additional_time=1
and pos=1
and event_date>='2013-11-27'
and event_date<='2013-12-19' ;

#Yuli Guarin contesto con la extension 2040 de enit
update bi_ops_cc 
set
agent='SIP/phone117',
agent_name='YULI YESENIA GUARIN CRUZ ',
agent_ext=2117
where event_date='2013-08-24' 
and agent_ext=2040 AND event_datetime<'2013-08-24 15:58:24';

#Apoyo a Inbound de Ingrid Mendez. Contesta con la extension 2040
update bi_ops_cc
set
agent='SIP/phone27',
agent_name='INGRID YISETH MENDEZ PEREZ',
agent_ext=2027
where event_date='2013-08-24' 
and agent_ext=2040 AND event_datetime>='2013-08-24 15:58:24';

#Apoyo de backoffice a la linea (Ingrid Mendez y Ana Cuervo)
update bi_ops_cc
set
agent='SIP/phone27',
agent_name='INGRID YISETH MENDEZ PEREZ',
agent_ext=2027
where event_date='2013-08-25' 
and agent_ext=2069;

update bi_ops_cc
set
agent='SIP/phone16',
agent_name='ANA MARIA CUERVO TOCUA',
agent_ext=2016
where event_date='2013-08-25' 
and agent_ext=2028;

#Quitar llamadas de Navidad en horario no laboral
update bi_ops_cc
set net_event=0
where event_hour>=18
and event_date='2013-12-24';

update bi_ops_cc
set net_event=0
where event_hour>=16
and event_date='2013-12-25';

#Quitar llamadas de Año Nuevo en horario no laboral
update bi_ops_cc
set net_event=0
where event_hour>=18
and event_date='2013-12-31';

update bi_ops_cc
set net_event=0
where 
event_date='2013-12-25';

SELECT  'Actualizar binarios de tiempos',now();
update bi_ops_cc
set 
unanswered_in_wh=if(event in ('ABANDON'),1,0),
unanswered_in_wh_cust=if(event in ('ABANDON') and hold_duration_seg<=5,1,0),
answered_in_wh=if(event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER'),1,0),
answered_in_wh_20=IF(event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER') and hold_duration_seg<=20,1,0),
no_answer=if(event='RINGNOANSWER',1,0),
agent_q=if(agent<>'NONE',1,0)
where net_event=1
#and event_date>=@max_day
;

update bi_ops_cc
SET
no_answer=if(event='RINGNOANSWER',1,0)
#where event_date>=@max_day
;

update bi_ops_cc
set 
available_time=0
where net_event=0 or agent_q=0
#and event_date>=@max_day
;

SELECT  'Actualizar datos de preventa y posventa',now();
update bi_ops_cc 
set 
pre=if(queue='ivr_cola1',1,0),
pos=if(queue in ('ivr_cola2','ivr_cola3'),1,0),
pre_answ=if(queue='ivr_cola1' and answered_in_wh=1,1,0),
pos_answ=if(queue in ('ivr_cola2','ivr_cola3') and answered_in_wh=1,1,0)
#where event_date>=@max_day;
;

update bi_ops_cc 
set event_date='2013-06-21',
call_duration_seg=25
where uniqueid='1371851043.10169'
and event='COMPLETEAGENT';


update bi_ops_cc
set agent_name='ABANDONO'
where agent='NONE';

#SELECT  'Start routine bi_ops_cc_occupancy',now();

#call bi_ops_cc_occupancy();

#SELECT  'End routine bi_ops_cc_occupancy',now();

#SELECT  'Start routine agents_schedule',now();

#call agents_schedule();

#SELECT  'End routine agents_schedule',now();


call bi_ops_cc_queue2();

call bi_ops_cc_facebook();

SELECT  'Llamadas transferidas a otra cola',now();

#Eliminar las tabla temporal de llamadas transferidad
DROP   TEMPORARY TABLE IF EXISTS calls_transfered_to_queue;
CREATE TEMPORARY TABLE calls_transfered_to_queue

#Llenar los datos de las llamadas transferidas 
select * from (select event_date,uniqueid,count(distinct(queue)) total,queue as queue_a, "queue_unknown" as queue_b,
"queue_unknown" as queue_c 
from bi_ops_cc where net_event=1 
group by uniqueid) as q
where q.total>=2;

#Llenar los datos de la cola a la que fue transferida
update calls_transfered_to_queue a inner join
bi_ops_cc b on a.uniqueid=b.uniqueid
set a.queue_b=b.queue
where a.queue_a<>b.queue and b.net_event=1;

#Llenar los datos de la cola transferida 2 (en caso que se transfiera dos veces la llamada)
update calls_transfered_to_queue a inner join
bi_ops_cc b on a.uniqueid=b.uniqueid
set a.queue_c=b.queue
where a.queue_a<>b.queue and a.queue_b<>b.queue and b.net_event=1;

#call bi_ops_cc_occupancy();

#Actualizar transferencias en la tabla general de servicio al cliente
SELECT  'Actualizar datos de las transferencias en bi_ops_cc',now();
update bi_ops_cc a 
inner join calls_transfered_to_queue b on a.uniqueid=b.uniqueid
set 
transfered_to_queue=1,
a.initial_queue=b.queue_a,
a.second_queue=b.queue_b,
a.third_queue=b.queue_c,
a.sequence_queue=if(b.total=2,concat(b.queue_a,"-",b.queue_b),concat(b.queue_a,"-",b.queue_b,"-",b.queue_c))
#where a.event_date>=@max_day
;

#Cambiar filtro para los datos 
update bi_ops_cc 
set 
transfered_to_queue=0
where transfered_to_queue=1
and queue<>initial_queue
#and event_date>=@max_day
;

#Llamadas transferidas desde otra cola
update bi_ops_cc 
set transfered_from_queue=1
where transfered_to_queue=0
and initial_queue is not null
#and event_date>=@max_day
;

#Llamadas transferidas dos veces
update bi_ops_cc 
set transfered_to_queue=1
where transfered_to_queue=0
and third_queue is not null
and third_queue<>'queue_unknown'
and queue=second_queue
#and event_date>=@max_day
;

#Arreglar extension de los abandonos
update bi_ops_cc 
set
agent_ext=0 where agent='NONE';

call bi_ops_cc_agent_sales;

call bi_ops_cc_agent_times;

call bi_ops_cc_occupancy_2;

SELECT  'Actualizar extensiones de los retirados',now();

update bi_ops_cc
set
agent_ext=if(length(right(agent,length(agent)-9))=3,concat(2,right(agent,length(agent)-9)),
if(length(right(agent,length(agent)-9))=2,concat(20,right(agent,length(agent)-9)),
concat(200,right(agent,length(agent)-9))))
where agent_ext is null;

#Apoyos
update bi_ops_cc
set agent_ext=right(agent,length(agent)-4) 
where agent  like 'SIP/1%' OR 
agent like 'SIP/3%'
 ;

update bi_ops_cc
set agent_ext=right(agent,length(agent)-4) 
where event_date='2013-11-29'
and agent  like 'SIP/1%' OR 
agent like 'SIP/3%'
 ;

update bi_ops_cc
set net_event=0
where additional_time=1
and pos=1
and event_date>='2013-11-27';

#Rutina customer detail
call bi_ops_customer_detail;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'customer_service_co.bi_ops_cc',
  'finish',
  NOW(),
  MAX(event_date),
  count(*),
  count(*)
FROM
  customer_service_co.bi_ops_cc;


SELECT  'Fin rutina bi_ops_cc',now();