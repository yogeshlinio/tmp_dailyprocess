SELECT  'Inicio rutina CC Outbound',now();

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'customer_service_co.bi_ops_cc_outbound',
  'start',
  NOW(),
  MAX(event_datetime),
  count(*),
  count(*)
FROM
  customer_service_co.bi_ops_cc_outbound;


#Zona horaria de Colombia

set time_zone='-5:00';

update cdr
set 
calldate=answer
 where calldate='0000-00-00 00:00:00' and uniqueid<>"";

truncate table queue_stats_outbound;

insert into queue_stats_outbound (uniqueid,call_date,ext,total_duration,call_duration,disposition,dialout,number,record)
SELECT uniqueid,calldate,src,duration,billsec,disposition,dialout,dst,record FROM cdr where (src like '20%' or src like '21%' or src like '18%') and length(src)=4
and src <> '2000';


truncate table bi_ops_cc_outbound;


SELECT  'Insertar datos bi_ops_cc_outbound',now();
insert into bi_ops_cc_outbound (uniqueid, event_datetime, event_date, agent_ext, event, total_duration_seg, call_duration_seg, event_hour, event_minute, event_weekday, event_week, event_month, event_year,number) 
SELECT uniqueid, call_date,date(call_date),ext,disposition, total_duration,call_duration,hour(call_date), minute(call_date),
weekday(call_date),weekofyear(call_date),monthname(call_date),year(call_date),number FROM queue_stats_outbound;

update bi_ops_cc_outbound b
inner join cdr a
on b.uniqueid=a.uniqueid
set calldate=end
where event_date='0000-00-00';


SELECT  'Insertar nombres',now();

update bi_ops_cc_outbound a inner join bi_ops_agent_names b on a.agent_ext=b.ext inner join 
bi_ops_CS_matrix_antext c on b.ext=c.phone
set 
a.agent_name=c.name,
a.process=c.process,
a.agent=b.agent
where event_date<='2013-12-09';

update bi_ops_cc_outbound a inner join bi_ops_agent_names b on a.agent_ext=b.ext inner join 
bi_ops_CS_matrix c on b.ext=c.phone
set 
a.agent_name=c.name,
a.process=c.process,
a.agent=b.agent
where event_date>'2013-12-09';

update bi_ops_cc_outbound a inner join 
bi_ops_agent_names b on a.agent_ext=b.ext 
inner join bi_ops_CS_matrix_antext c on b.ext=c.phone
set 
a.agent_name=c.name,
a.process=c.process,
a.agent=b.agent
where status='ACTIVO'
and event_date<='2013-12-09';

update bi_ops_cc_outbound a inner join 
bi_ops_agent_names b on a.agent_ext=b.ext 
inner join bi_ops_CS_matrix c on b.ext=c.phone
set 
a.agent_name=c.name,
a.process=c.process,
a.agent=b.agent
where status='ACTIVO'
and event_date>'2013-12-09';

update bi_ops_cc_outbound
set event_shift=if(event_minute<30,concat(event_hour,':','00'),concat(event_hour,':','30')),
hold_duration_seg=total_duration_seg-call_duration_seg;


SELECT  'Actualizar binarios de tiempos',now();
update bi_ops_cc_outbound
set 
answered=if(event ='ANSWERED',1,0),
busy=if(event ='BUSY',1,0),
unanswered=if(event ='NO ANSWER',1,0),
calls=if(event ='FAILED',0,1);

UPDATE bi_ops_cc_outbound 
SET outbound=1 where 
process='OUTBOUND';

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'customer_service_co.bi_ops_cc_outbound',
  'finish',
  NOW(),
  MAX(event_datetime),
  count(*),
  count(*)
FROM
  customer_service_co.bi_ops_cc_outbound;



SELECT  'Fin rutina CC Outbound',now();

