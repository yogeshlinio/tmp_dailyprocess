v_user=$1
v_pwd=$2
v_host=$3
v_country=$4
v_date=`date +"%Y%m%d"`

for v_folder in Commercial/CostosOms/
do
	echo "Loading $v_folder ..."
	sqls="./sqls/${v_folder}"
	for i in $sqls/*.sql
	do
		dateTime=`date`
		echo "($dateTime):: Start Load "
		mysql -u ${v_user} -p${v_pwd} -h ${v_host} -b development_co < $i
        error=`echo $?`
        if [ $error -gt 0 ]; then
            exit 1
        fi		
		dateTime=`date`
	done    
    echo " ${dateTime} End Load ... ${i}"	
done