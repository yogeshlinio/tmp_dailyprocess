v_environment=$1
v_country=$2
v_verbose=$3

PATH=$PATH:/usr/bin/mysql
export PATH
#absolute path to the linio-git repository
lib=`pwd`
lib=$lib/scripts
export lib

#parse config.ini
. $lib/parser_config.sh
read_ini ./config.ini 

#get the var names according the parameter
v_trigger="INI__${v_environment}__TRIGGER"
v_trigger=`eval echo \\$\$v_trigger`

system="./Daily/Global"
cd ${system}
v_dateExc=`date`
echo "Execution..." ${v_dateExc}  ${v_country}
pwd

./apsh/${v_trigger} ${v_environment} ${v_country} ${v_verbose}
error=`echo $?`
if [ $error -gt 0 ]; then
    exit 1
fi