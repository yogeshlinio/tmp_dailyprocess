v_environment=$1
v_country=$2

PATH=$PATH:/usr/bin/mysql
export PATH
#absolute path to the linio-git repository
lib=`pwd`
lib=$lib/scripts
export lib

#parse config.ini
. $lib/parser_config.sh
read_ini ./config.ini 

#get the var names according the parameter
v_user="INI__${v_environment}__USER"
v_user=`eval echo \\$\$v_user`

v_pwd="INI__${v_environment}__PWD"
v_pwd=`eval echo \\$\$v_pwd`

v_trigger="INI__${v_environment}__TRIGGER"
v_trigger=`eval echo \\$\$v_trigger`

v_database="INI__${v_environment}__HOST"
v_database=`eval echo \\$\$v_database`

system="./Daily"
cd ${system}

	v_dateExc=`date`
	echo "Execution..." ${v_dateExc}  ${v_country} 
	if [ -d ${v_country} ];
	then
		echo ${v_country}--"ES un directorio"
	   cd ${v_country}
	   ./apsh/${v_trigger} ${v_user} ${v_pwd} ${v_database} ${v_country} ${v_environment}
	   error=`echo $?`
	   if [ $error -gt 0 ]; then
	       exit 1
	   fi
	   
	   cd ..	
	fi
