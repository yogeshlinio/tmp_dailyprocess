#!/bin/bash

#Module to load data from BOB/UMS/OMS into dwh
#usage:
#     load.sh <country> <data_source>
v_system=`pwd`
v_date=`date +"%Y%m%d"`
v_user=${1}
v_pwd=${2}
v__host=${3}
#echo  "Starting processing ${1}"
v_country=${4}
v_countryPrefix=""


case $v_country in
      "Mexico") v_countryPrefix="MX";;
	  "Colombia") v_countryPrefix="CO";;
	  "Peru") v_countryPrefix="PE";;
	  "Venezuela") v_countryPrefix="VE";;
	  *) echo "Wrong data source";;
esac
echo "v_countryPrefix" ${v_countryPrefix}

#absolute path to the linio-git repository
path="../.."
#parse config.ini
. $path/scripts/parser_config.sh
read_ini $path/configChannels.ini

mysql -u${v_user}  -p${v_pwd} -h $v__host -b ${DB_DB}  < $v_system/sqls/CRM/start_monitoring_log.sql
error=`echo $?`
if [ $error -gt 0 ]; then
   exit 1
fi

for environment in `cat ./resources/Environments_CRM.txt`
do
	echo "--------------------------------------------------------"
		#get the var names according the parameter
		channel_dir="INI__${environment}__DIRECTORY"
		channel_file="INI__${environment}__LOADFILE"
		DB_DATABASE="INI__${environment}__DB_DATABASE"
		DB_TABLE="INI__${environment}__DB_TABLE"
		name="INI__${environment}__NAME"
		type="INI__${environment}__TYPE"

		#shell double referencing
		channel_dir=`eval echo \\$\$channel_dir `
		channel_file=`eval echo \\$\$channel_file`
		DB_TABLE=`eval echo \\$\$DB_TABLE`
		name=`eval echo \\$\$name`
		type=`eval echo \\$\$type`
		DB_DB=`eval echo \\$\$DB_DATABASE`

		channel_dir=`eval echo \$channel_dir `
		channel_file=`eval echo \$channel_file`
		DB_TABLE=`eval echo \$DB_TABLE`
		name=`eval echo \$name`
		type=`eval echo \$type`
		DB_DB=`eval echo "\$DB_DB"`

		#echo "channel_dir	"--$channel_dir
		#echo "channel_file	"--$channel_file
		#echo "DB_TABLE		"--$DB_TABLE
		#echo "v__host		"--$v__host
		#echo "DB_DB			"--$DB_DB
		#echo "name			"--$name
		#echo "type			"--$type
		#pwd
		cat $v_system/sqls/CRM/loadData.sql > ./sqls/CRM/tmp
		cat $v_system/sqls/CRM/tmp  | sed s/@v_file@/${channel_file}/g>  $v_system/sqls/CRM/tmp1
		cat $v_system/sqls/CRM/tmp1 | sed s/@v_host@/${v__host}/g>       $v_system/sqls/CRM/tmp2
		cat $v_system/sqls/CRM/tmp2 | sed s/@v_db@/${DB_DB}/g>           $v_system/sqls/CRM/tmp3
		cat $v_system/sqls/CRM/tmp3 | sed s/@v_tables@/${DB_TABLE}/g>    $v_system/sqls/CRM/tmp4
		#echo "++++"
		var=$(ls /var/tmp/CRM/${v_country}/*${type}.${name}*)
		for i in $var
		do
			#echo "-------------------"
			#echo "Se copia: " $i
			nombre=` echo $i |	awk 'BEGIN { FS = "/" } ; { print $6 }'`
			nombreUnzip=` echo $nombre | awk -F "."  '{print $1"."$2"."$3"."$4}'`
			generico=` echo $nombreUnzip | awk -F "."  '{print $2"."$3"."$4}'`
			#echo "nombreUnzip:: " ${nombreUnzip}
			#echo "generico:: "${generico}
			#echo "Se vacia el directorio sources:: "		
			rm -f $v_system/sources/*
			cp $i $v_system/sources/
			echo "Carga de::" $nombre
			cd sources
			gunzip ${nombre} 
			mv $nombreUnzip ${generico}
			mysql -u${v_user}  -p${v_pwd} -h $v__host -b ${DB_DB}  < $v_system/sqls/CRM/tmp4
			error=`echo $?`
			if [ $error -gt 0 ]; then
				exit 1
			fi
			#echo "Se ejecuta ---mysql -u${v_user} -p${v_pwd} -h ${v__host} -b $DB_DB <" 
			#cat ../sqls/CRM/tmp4
			cd ..
		done
		echo "Se eliminan los temporales de SQL"
		rm $v_system/sqls/CRM/tmp* 
done

#Old files zip
echo "Compressing and moving to Historical"
tar -cvzf /var/tmp/CRM/Historical/${v_date}_${v_country}.tar.gz  /var/tmp/CRM/${v_country}
rm -f /var/tmp/CRM/${v_country}/*

#Monitoring Log
mysql -u${v_user}  -p${v_pwd} -h $v__host -b ${DB_DB}  < $v_system/sqls/CRM/finish_monitoring_log.sql
error=`echo $?`
if [ $error -gt 0 ]; then
   exit 1
fi

exit
