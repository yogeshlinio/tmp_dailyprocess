v_user=$1
v_pwd=$2
v_host=$3
v_country=$4
v_date=`date +"%Y%m%d"`
v_country=Venezuela
v_folder=$5
pwd
    echo "Loading $v_folder ..."
    sqls="./sqls/${v_folder}"
    for i in $sqls/*.sql
    do
		dateTime=`date`
		echo "Start--($dateTime):: $i" 
		mysql -u ${v_user} -p${v_pwd}   -h ${v_host} -b production < $i
		error=`echo $?`
	    if [ $error -gt 0 ]; then
	       exit 1
	    fi
		dateTime=`date`
		echo "($dateTime)::End Load ..."
	done
