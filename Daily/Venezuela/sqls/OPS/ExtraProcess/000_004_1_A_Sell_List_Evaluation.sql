USE operations_ve;
#DROP TABLE IF EXISTS A_Sell_List_Evaluation;
CREATE TABLE if NOT EXISTS `A_Sell_List_Evaluation` (
  `category_bp` varchar(255) ,
  `value_in_stock` decimal(44,6) DEFAULT 0,
  `value_to_sell_mom` decimal(43,6) DEFAULT 0,
  `value_to_sell_wow` decimal(43,6) DEFAULT 0,
  `value_to_sell` decimal(43,6) DEFAULT 0,
  `stock_value_wo_cons` decimal(43,6) DEFAULT 0,
  `value_stock_wow` decimal(44,6) DEFAULT 0,
  `value_wo_consig_wow` decimal(43,6) DEFAULT 0,
  `value_stock_mom` decimal(44,6) DEFAULT 0,
  `value_wo_consig_mom` decimal(43,6) DEFAULT 0,
  PRIMARY KEY ( category_bp ) 
) ENGINE=InnoDB DEFAULT CHARSET=latin1
;
#INSERT A_Sell_List_Evaluation
#( `category_bp`  )
#SELECT CatBP
#FROM GlobalConfig.M_CategoryKPI
#GROUP BY CatBP;

DROP TEMPORARY TABLE IF EXISTS TMP_Sell_List_Evaluation;
CREATE TEMPORARY TABLE TMP_Sell_List_Evaluation ( PRIMARY KEY ( category_bp ) )
SELECT category_bp, MAX(q_three) as value_in_stock, MAX(mom) as value_to_sell_mom,MAX(q_one) as value_to_sell_wow, MAX(q_two) as value_to_sell,
MAX(value_wo_cons) as stock_value_wo_cons, MAX(wow_stock_total) as value_stock_wow, MAX(wow_consi) as value_wo_consig_wow,
MAX(mom_stock_total) as value_stock_mom, MAX(mom_consi) as value_wo_consig_mom
FROM (
      select category_bp, 0 AS q_three, sum(value_sell_list) as q_one, 0 AS q_two, 0 as mom, 0 as value_wo_cons, 0 as wow_stock_total, 0 as wow_consi,
  0 as mom_stock_total, 0 as mom_consi
   from operations_ve.tbl_days_of_inventory_per_day
      where datediff(curdate(), date) = 7 and buyer is not null
      group by category_bp
      UNION 
      select category_bp, 0 , 0, 0, sum(value_sell_list) mom, 0, 0, 0, 0, 0
   from operations_ve.tbl_days_of_inventory_per_day
      where datediff(curdate(), date) = 30 and buyer is not null
      group by category_bp
      UNION 
      select category_bp, 0, 0, sum(value_sell_list),0, 0, 0, 0, 0, 0
      from operations_ve.tbl_days_of_inventory_per_day
      where buyer is not null and date IN (SELECT max(date) FROM operations_ve.tbl_days_of_inventory_per_day)
      group by category_bp
      UNION
      select category_bp, sum(costo_after_vat_in_stock+costo_after_vat_consignation), 0, 0, 0, 0, 0, 0, 0, 0
  from operations_ve.tbl_days_of_inventory_per_day
  where buyer is not null and date IN (SELECT max(date) FROM operations_ve.tbl_days_of_inventory_per_day)
  group by category_bp
   UNION
      select category_bp, 0, 0, 0, 0, sum(costo_after_vat_in_stock), 0, 0, 0, 0
  from operations_ve.tbl_days_of_inventory_per_day
  where buyer is not null and date IN (SELECT max(date) FROM operations_ve.tbl_days_of_inventory_per_day)
  group by category_bp
    UNION
      select category_bp, 0, 0, 0, 0, 0, sum(costo_after_vat_in_stock+costo_after_vat_consignation), 0, 0, 0
  from operations_ve.tbl_days_of_inventory_per_day
  where buyer is not null and date = curdate() - interval 7 day
  group by category_bp
   UNION
      select category_bp, 0, 0, 0, 0, 0, 0, sum(costo_after_vat_in_stock), 0, 0
  from operations_ve.tbl_days_of_inventory_per_day
  where buyer is not null and date = curdate() - interval 7 day
  group by category_bp
  UNION
      select category_bp, 0, 0, 0, 0, 0, 0, 0, sum(costo_after_vat_in_stock+costo_after_vat_consignation), 0
  from operations_ve.tbl_days_of_inventory_per_day
  where buyer is not null and date = curdate() - interval 30 day
  group by category_bp
   UNION
      select category_bp, 0, 0, 0, 0, 0, 0, 0, 0, sum(costo_after_vat_in_stock)
  from operations_ve.tbl_days_of_inventory_per_day
  where buyer is not null and date = curdate() - interval 30 day
  group by category_bp
) A
GROUP BY category_bp;

#1. Value in stock
UPDATE       A_Sell_List_Evaluation AS tbl
  INNER JOIN TMP_Sell_List_Evaluation  AS Resume
   USING ( category_bp )
SET 
   tbl.value_in_stock = IF( Resume.value_in_stock <= 0, tbl.value_in_stock , Resume.value_in_stock ),
   tbl.value_to_sell_mom  = IF( Resume.value_to_sell_mom  <= 0, tbl.value_to_sell_mom  , Resume.value_to_sell_mom  ), 
   tbl.value_to_sell_wow  = IF( Resume.value_to_sell_wow  <= 0, tbl.value_to_sell_wow  , Resume.value_to_sell_wow  ) ,
   tbl.value_to_sell  = IF( Resume.value_to_sell  <= 0, tbl.value_to_sell  , Resume.value_to_sell  ), 
   tbl.stock_value_wo_cons  = IF( Resume.stock_value_wo_cons  <= 0, tbl.stock_value_wo_cons  , Resume.stock_value_wo_cons  ), 
   tbl.value_stock_wow  = IF( Resume.value_stock_wow  <= 0, tbl.value_stock_wow  , Resume.value_stock_wow  ), 
   tbl.value_wo_consig_wow  = IF( Resume.value_wo_consig_wow  <= 0, tbl.value_wo_consig_wow  , Resume.value_wo_consig_wow  ), 
   tbl.value_stock_mom  = IF( Resume.value_stock_mom  <= 0, tbl.value_stock_mom  , Resume.value_stock_mom  ), 
   tbl.value_wo_consig_mom  = IF( Resume.value_wo_consig_mom  <= 0, tbl.value_wo_consig_mom  , Resume.value_wo_consig_mom )

;
