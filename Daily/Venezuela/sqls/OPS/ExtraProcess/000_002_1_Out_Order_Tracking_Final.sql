##### Termina Rutina #####

SELECT  'Replace pro_backlog_history',now();
DROP TABLE IF EXISTS operations_ve.pro_backlog_history_tst;
CREATE TABLE operations_ve.pro_backlog_history_tst LIKE operations_ve.pro_backlog_history;
INSERT INTO operations_ve.pro_backlog_history_tst SELECT * FROM operations_ve.pro_backlog_history;

REPLACE operations_ve.pro_backlog_history_tst
SELECT
 curdate() as Date_reported,
 SUM( IF(     is_presale = 0
          AND status_wms IN ('aguardando estoque','analisando quebra')
                                      , delay_to_procure     , 0 ) ) as procurement_backlog,
 SUM( IF(     date_delivered IS NULL
          AND date_delivered_promised < curdate()
          AND status_wms = 'expedido' , delay_total_delivery , 0 ) ) as deliveries_backlog
FROM
 operations_ve.out_order_tracking_sample_tst
WHERE 
(
         is_presale = 0
    AND status_wms IN ('aguardando estoque','analisando quebra')
)
OR 
(
        date_delivered IS NULL
    AND date_delivered_promised < curdate()
    AND status_wms = 'expedido'
);

### Status Match
DROP TABLE IF EXISTS operations_ve.status_match_dates_tst;
CREATE TABLE operations_ve.status_match_dates_tst (INDEX (status_bob, status_wms))
SELECT
 a.item_id,
 a.order_number,
 a.sku_simple,
 a.sku_name,
 a.status_bob,
 a.status_wms,
 a.status_match_bob_wms,
 #a.status_value_chain,
 max(b.updated_at) AS bob_updated_at,
 max(c.data_ultima_alteracao) AS wms_updated_at,
 DATEDIFF(curdate(),max(b.updated_at)) AS days_since_last_bob_status,
 DATEDIFF(curdate(),max(c.data_ultima_alteracao))     AS days_since_last_wms_status,
 a.check_dates,
 a.check_date_ordered,
 a.check_date_exported,
 a.check_date_ready_to_ship,
 a.check_date_shipped,
 a.check_date_1st_attempt,
 a.payment_method,
 a.shipping_carrier,
 0 AS is_final_bob,
 0 AS is_final_wms,
 'PENDING' AS day_group_bob_status,
 'PENDING' AS day_group_wms_status
FROM operations_ve.out_order_tracking_tst a
INNER JOIN bob_live_ve.sales_order_item b
    ON a.item_id = b.id_sales_order_item
INNER JOIN wmsprod_ve.itens_venda c
    ON a.item_id = c.item_id
GROUP BY a.item_id;

UPDATE
operations_ve.status_match_dates_tst a
INNER JOIN operations_ve.status_match_bob_wms b
    ON a.status_bob = b.status_bob
SET a.is_final_bob = b.is_final_bob;

UPDATE
operations_ve.status_match_dates_tst a
INNER JOIN operations_ve.status_match_bob_wms b
    ON a.status_wms = b.status_wms
SET a.is_final_wms = b.is_final_wms;

UPDATE 
operations_ve.status_match_dates_tst
SET
 day_group_bob_status = if(days_since_last_bob_status = 0,'0d',
                                                if(days_since_last_bob_status = 1,'1d',
                                                if(days_since_last_bob_status = 2,'2d',
                                                if(days_since_last_bob_status = 3,'3d',
                                                if(days_since_last_bob_status = 4,'4d',
                                                if(days_since_last_bob_status = 5,'5d',
                                                if(days_since_last_bob_status = 6,'6d',
                                                if(days_since_last_bob_status = 7,'7d',
                                                if(days_since_last_bob_status BETWEEN 8 AND 15,'8-15d',
                                                if(days_since_last_bob_status BETWEEN 16 AND 30,'15-30d',
                                                if(days_since_last_bob_status BETWEEN 31 AND 60,'31-60d',
                                                if(days_since_last_bob_status BETWEEN 61 AND 90,'61-90d',
                                                if(days_since_last_bob_status > 90,'>90d',NULL)))))))))))))
;

UPDATE 
operations_ve.status_match_dates_tst
SET
 day_group_WMS_status =    if(days_since_last_wms_status = 0,'0d',
                                                if(days_since_last_wms_status = 1,'1d',
                                                if(days_since_last_wms_status = 2,'2d',
                                                if(days_since_last_wms_status = 3,'3d',
                                                if(days_since_last_wms_status = 4,'4d',
                                                if(days_since_last_wms_status = 5,'5d',
                                                if(days_since_last_wms_status = 6,'6d',
                                                if(days_since_last_wms_status = 7,'7d',
                                                if(days_since_last_wms_status BETWEEN 8 AND 15,'8-15d',
                                                if(days_since_last_wms_status BETWEEN 16 AND 30,'15-30d',
                                                if(days_since_last_wms_status BETWEEN 31 AND 60,'31-60d',
                                                if(days_since_last_wms_status BETWEEN 61 AND 90,'61-90d',
                                                if(days_since_last_wms_status > 90,'>90d',NULL)))))))))))))
;

# Inbound Processing Tracking

TRUNCATE operations_ve.tbl_bi_inbound_processing;

INSERT INTO operations_ve.tbl_bi_inbound_processing
(po_name,
date_start,
date_scan,
date_finish,
date_time_start,
date_time_scan,
date_time_finish,
week_start,
month_start,
inbound_user,
itens_recebimento_id,
sku_simple,
ean,
inbound_type,
inbound_status
)
SELECT
a.inbound_document_identificator AS po_name, 
date(a.data_criacao) AS date_start,
date(c.data_criacao) AS date_scan,
date(a.data_fecho) AS date_finish,
a.data_criacao AS date_time_start,
c.data_criacao AS date_time_scan,
a.data_fecho AS date_time_finish,
operations_ve.week_iso(c.data_criacao) AS week_start,
DATE_FORMAT(a.data_criacao,'%Y%m') AS month_start,
b.login AS inbound_user,
c.itens_recebimento_id,
c.sku AS sku_simple,
c.cod_barras AS ean,
IF (c.cod_barras = c.sku,'sku','ean') AS inbound_type,
a.status AS inbound_status
FROM wmsprod_ve.recebimento a
INNER JOIN wmsprod_ve.usuarios b
	ON a.usuario = b.usuarios_id
INNER JOIN wmsprod_ve.itens_recebimento c
	ON a.recebimento_id = c.recebimento_id
INNER JOIN wmsprod_ve.estoque e
	ON e.itens_recebimento_id = c.itens_recebimento_id
;

	UPDATE operations_ve.tbl_bi_inbound_processing a
	INNER JOIN 
	 (SELECT 
			b.fk_procurement_order_type, 
			concat(b.venture_code, lpad(b.id_procurement_order, 7, 0),b.check_digit) AS po_name 
		FROM procurement_live_ve.procurement_order b) c
		ON a.po_name = c.po_name 
	INNER JOIN procurement_live_ve.procurement_order_type d
	 ON c.fk_procurement_order_type = d.id_procurement_order_type
	SET a.purchase_order_type = d.procurement_order_type;

	UPDATE operations_ve.tbl_bi_inbound_processing a
	SET purchase_order_type = 'Others'
	WHERE purchase_order_type is NULL;

UPDATE operations_ve.tbl_bi_inbound_processing a
INNER JOIN wmsprod_ve.estoque b
	ON a.itens_recebimento_id = b.itens_recebimento_id
SET a.stock_item_id = b.estoque_id;

UPDATE operations_ve.tbl_bi_inbound_processing a
INNER JOIN wmsprod_ve.po_oms b
	ON a.po_name = b.`name`
SET a.po_oms_id = b.po_oms_id;

# Aqui !!!
UPDATE operations_ve.tbl_bi_inbound_processing a
INNER JOIN wmsprod_ve.po_oms_sku b
	ON a.po_oms_id = b.fk_po_oms
	AND a.sku_simple = b.sku
SET a.items_in_po = b.quantity;

UPDATE operations_ve.tbl_bi_inbound_processing a
INNER JOIN development_ve.A_Master_Catalog c
	ON a.sku_simple = c.sku_simple
SET 
 a.sku_config = c.sku_config,
 a.sku_name = c.sku_name,
 a.supplier_id = c.id_supplier,
 a.supplier_name = c.Supplier,
 a.package_height = c.Package_Height,
 a.package_length = c.package_length,
 a.package_width = c.package_width,
 a.package_weight = c.package_weight,
 a.buyer = c.Buyer,
 a.head_buyer = c.Head_Buyer,
 a.cat1 = c.Cat1,
 a.cat2 = c.Cat2,
 a.category_bp = c.Cat_BP;

UPDATE operations_ve.tbl_bi_inbound_processing
SET vol_weight = package_height * package_length * package_width / 5000;

UPDATE operations_ve.tbl_bi_inbound_processing
SET max_vol_w_vs_w = CASE
WHEN vol_weight > package_weight THEN
	vol_weight
ELSE
	package_weight
END;

UPDATE operations_ve.tbl_bi_inbound_processing
SET 
 package_measure_new = 	CASE
													WHEN max_vol_w_vs_w > 35 
													THEN 'oversized'
													ELSE ( CASE
																	WHEN max_vol_w_vs_w > 5 
																	THEN 'large'
																	ELSE	(	CASE
																						WHEN max_vol_w_vs_w > 2 
																						THEN 'medium'
																						ELSE 'small'
																					END	)
																END)
												END;


/*
UPDATE operations_ve.tbl_bi_inbound_processing a
INNER JOIN procurement_live_ve.catalog_supplier_attributes b
	ON a.supplier_id = b.fk_catalog_supplier
SET 
 a.procurement_analyst = b.buyer_name;
*/


DROP TABLE IF EXISTS operations_ve.pro_inbound_unique_skus;
CREATE TABLE operations_ve.pro_inbound_unique_skus (index(date_start, sku_simple))
SELECT
	date_start,
	sku_simple,
	1 AS sku_counter,
  sum(item_counter) AS item_counter,
	1/sum(item_counter) AS value
FROM operations_ve.tbl_bi_inbound_processing
GROUP BY 
	date_start,
	sku_simple;

UPDATE operations_ve.tbl_bi_inbound_processing a
INNER JOIN operations_ve.pro_inbound_unique_skus b
	ON a.date_start = b.date_start
	AND a.sku_simple = b.sku_simple
SET a.sku_simple_inbound_counter = b.value;

DROP TABLE IF EXISTS operations_ve.pro_inbound_unique_skus_measures;
CREATE TABLE operations_ve.pro_inbound_unique_skus_measures (index(date_start, sku_simple))
SELECT
	a.date_start,
	a.sku_simple,
	1 AS sku_counter,
  sum(a.item_counter) AS item_counter,
	1/sum(a.item_counter) AS value
FROM operations_ve.tbl_bi_inbound_processing a
INNER JOIN wmsprod_ve.measures_inbound_sku b
	ON a.sku_simple = b.sku
	AND a.date_start = date(b.date)
GROUP BY 
	date_start,
	sku_simple;

UPDATE operations_ve.tbl_bi_inbound_processing a
INNER JOIN operations_ve.pro_inbound_unique_skus_measures b
	ON a.date_start = b.date_start
	AND a.sku_simple = b.sku_simple
SET a.sku_simple_measures_counter = b.value;

UPDATE operations_ve.tbl_bi_inbound_processing a
INNER JOIN operations_ve.pro_inbound_unique_skus_measures b
	ON a.sku_simple = b.sku_simple
	AND a.date_start > date(b.date_start)
SET a.sku_simple_never_measured_counter = b.sku_counter;

UPDATE operations_ve.tbl_bi_inbound_processing a
SET a.sku_simple_never_measured_counter = a.sku_simple_inbound_counter * (1- a.sku_simple_never_measured_counter);


SET @max_promised = curdate();

SELECT 
max(date_delivered_promised) INTO @max_promised
FROM operations_ve.out_order_tracking_tst
WHERE date_delivered_promised < curdate();

DROP TABLE IF EXISTS operations_ve.table_dashboard_dropshipping_supplier;

CREATE TABLE operations_ve.table_dashboard_dropshipping_supplier

SELECT
'% Shipped < 2 workdays' AS kpi,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 2
AND date_delivered_promised = @max_promised,1,0))
/ sum(IF (date_delivered_promised = @max_promised,1,0)) AS LastWD,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 2
AND operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised),1,0))
/ SUM(IF(operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised),1,0)) AS CurrentWeek,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 2
AND operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0))
/ SUM(IF(operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0))  AS LastWeek,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 2
AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised, '%Y%m'),1,0))
/ SUM(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised, '%Y%m'),1,0)) AS CurrentMonth,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 2
AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0))
/ SUM(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0)) AS LastMonth,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 2
AND delivered_promised_last_30=1,1,0))
/ SUM(IF(delivered_promised_last_30=1,1,0)) AS Last30days
FROM
operations_ve.out_order_tracking_tst
WHERE
is_presale = 0
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type="supplier"
AND status_wms IN (
'DS estoque reservado',
'Waiting Dropshipping',
"dropshipping notified",
'handled_by_marketplace',
'packed_by_marketplace',
'awaiting_fulfillment',
'expedido'
)
UNION
SELECT
'% Shipped < 3 workdays' AS kpi,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 3
AND date_delivered_promised = @max_promised,1,0))
/ SUM(IF(date_delivered_promised = @max_promised,1,0)) AS LastWD,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 3
AND operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised),1,0))
/ SUM(IF(operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised),1,0)) AS CurrentWeek,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 3
AND operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0))
/ SUM(IF(operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) AS LastWeek,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 3
AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised, '%Y%m'),1,0))
/ SUM(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised, '%Y%m'),1,0)) AS CurrentMonth,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 3
AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0))
/ SUM(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0)) AS LastMonth,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 3            
AND delivered_promised_last_30=1,1,0))
/ SUM(IF(delivered_promised_last_30=1,1,0)) AS Last30days
FROM
operations_ve.out_order_tracking_tst
WHERE
is_presale = 0
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type="supplier"
AND status_wms IN (
'DS estoque reservado',
'Waiting Dropshipping',
'handled_by_marketplace',
'awaiting_fulfillment',
"dropshipping notified",
"packed_by_marketplace",
'expedido'
)
UNION
SELECT
'% Shipped < 5 workdays' AS kpi,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 5
AND date_delivered_promised = @max_promised,1,0))
/ SUM(IF(date_delivered_promised = @max_promised,1,0)) AS LastWD,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 5
AND operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised),1,0))
/ SUM(IF(operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised),1,0)) AS CurrentWeek,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 5
AND operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) 
/ SUM(IF(operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) AS LastWeek,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 5
AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised, '%Y%m'),1,0))
/ SUM(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised, '%Y%m'),1,0)) AS CurrentMonth,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 5
AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0))
/ SUM(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0)) AS LastMonth,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 5
AND delivered_promised_last_30=1,1,0))
/ SUM(IF(delivered_promised_last_30=1,1,0)) AS Last30days
FROM
operations_ve.out_order_tracking_tst
WHERE
is_presale = 0
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type="supplier"
AND status_wms IN (
'DS estoque reservado',
'Waiting Dropshipping',
'handled_by_marketplace',
'awaiting_fulfillment',
"dropshipping notified",
"packed_by_marketplace",
'expedido')
UNION
SELECT
'% 1st attempt < 6 workdays' AS kpi,
sum(IF (workdays_total_1st_attempt <= 6
AND date_delivered_promised = @max_promised,1,0))
/ SUM(IF(date_delivered_promised = @max_promised,1,0)) AS LastWD,
sum(IF (workdays_total_1st_attempt <= 6
AND operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised),1,0))
/ SUM(IF(operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised),1,0)) AS CurrentWeek,
sum(IF (workdays_total_1st_attempt <= 6
AND operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) 
/ SUM(IF(operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) AS LastWeek,
sum(IF (workdays_total_1st_attempt <= 6
AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised, '%Y%m'),1,0))
/ SUM(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised, '%Y%m'),1,0)) AS CurrentMonth,
sum(IF (workdays_total_1st_attempt <= 6
AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0))
/ SUM(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0)) AS LastMonth,
sum(IF (workdays_total_1st_attempt <= 6
AND delivered_promised_last_30=1,1,0))
/ SUM(IF(delivered_promised_last_30=1,1,0)) AS Last30days
FROM
operations_ve.out_order_tracking_tst
WHERE
is_presale = 0
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type="supplier"
AND status_wms IN (
'DS estoque reservado',
'Waiting Dropshipping',
'handled_by_marketplace',
'awaiting_fulfillment',
"dropshipping notified",
"packed_by_marketplace",
'expedido')
UNION
SELECT
'% 1st attempt < 8 workdays' AS kpi,
sum(IF (workdays_total_1st_attempt <= 8
AND date_delivered_promised = @max_promised,1,0))
/ SUM(IF(date_delivered_promised = @max_promised,1,0)) AS LastWD,
sum(IF (workdays_total_1st_attempt <= 8
AND operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised),1,0))
/ SUM(IF(operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised),1,0)) AS CurrentWeek,
sum(IF (workdays_total_1st_attempt <= 8
AND operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) 
/ SUM(IF(operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) AS LastWeek,
sum(IF (workdays_total_1st_attempt <= 8
AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised, '%Y%m'),1,0))
/ SUM(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised, '%Y%m'),1,0)) AS CurrentMonth,
sum(IF (workdays_total_1st_attempt <= 8
AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0))
/ SUM(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0)) AS LastMonth,
sum(IF (workdays_total_1st_attempt <= 8
AND delivered_promised_last_30=1,1,0))
/ SUM(IF(delivered_promised_last_30=1,1,0)) AS Last30days
FROM
operations_ve.out_order_tracking_tst
WHERE
is_presale = 0
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type="supplier"
AND status_wms IN (
'DS estoque reservado',
'Waiting Dropshipping',
'handled_by_marketplace',
'awaiting_fulfillment',
"dropshipping notified",
"packed_by_marketplace",
'expedido')
UNION
SELECT
'% 1st attempt < 10 workdays' AS kpi,
sum(IF (workdays_total_1st_attempt <= 10
AND date_delivered_promised = @max_promised,1,0))
/ SUM(IF(date_delivered_promised = @max_promised,1,0)) AS LastWD,
sum(IF (workdays_total_1st_attempt <= 10
AND operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised),1,0))
/ SUM(IF(operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised),1,0)) AS CurrentWeek,
sum(IF (workdays_total_1st_attempt <= 10
AND operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0))
/ SUM(IF(operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) AS LastWeek,
sum(IF (workdays_total_1st_attempt <= 10
AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised, '%Y%m'),1,0))
/ SUM(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised, '%Y%m'),1,0)) AS CurrentMonth,
sum(IF (workdays_total_1st_attempt <= 10
AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0))
/ SUM(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0)) AS LastMonth,
sum(IF (workdays_total_1st_attempt <= 10
AND delivered_promised_last_30=1,1,0))
/ SUM(IF(delivered_promised_last_30=1,1,0)) AS Last30days
FROM
operations_ve.out_order_tracking_tst
WHERE
is_presale = 0
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type="supplier"
AND status_wms IN (
'DS estoque reservado',
'Waiting Dropshipping',
'handled_by_marketplace',
'awaiting_fulfillment',
"dropshipping notified",
"packed_by_marketplace",
'expedido')
UNION
SELECT
	'% On time 1st attempt' AS kpi,
	sum(IF(on_time_total_1st_attempt= 1	AND date_delivered_promised = @max_promised,1,0))
 / SUM(IF(date_delivered_promised = @max_promised,1,0)) AS LastWD,
	sum(IF(on_time_total_1st_attempt= 1 AND operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised),1,0))
 / sum(IF(operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised),1,0)) AS CurrentWeek,
sum(IF(on_time_total_1st_attempt= 1 AND operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0))
 / sum(IF(operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) AS LastWeek,
sum(IF(on_time_total_1st_attempt= 1	AND DATE_FORMAT(date_delivered_promised,'%Y%m')=DATE_FORMAT(@max_promised,'%Y%m'),1,0))
 / sum(IF(DATE_FORMAT(date_delivered_promised,'%Y%m')=DATE_FORMAT(@max_promised,'%Y%m'),1,0)) AS CurrentMonth,
sum(IF(on_time_total_1st_attempt= 1	AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0))
 / sum(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0)) AS LastMonth,
sum(IF(on_time_total_1st_attempt= 1 AND delivered_promised_last_30=1,1,0))
 / sum(IF(delivered_promised_last_30=1,1,0)) AS Last30days
FROM
	operations_ve.out_order_tracking_tst
WHERE
	is_presale = 0
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type="supplier"
AND status_wms IN (
	'DS estoque reservado',
	'Waiting Dropshipping',
	'handled_by_marketplace',
	'awaiting_fulfillment',
  "dropshipping notified",
  "packed_by_marketplace",
	'expedido')
UNION
SELECT
	'% OOS' AS kpi,
	sum(IF(is_stockout= 1	AND date_delivered_promised = @max_promised,1,0))
 / SUM(IF(date_delivered_promised = @max_promised,1,0)) AS LastWD,
	sum(IF(is_stockout= 1 AND operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised),1,0))
 / sum(IF(operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised),1,0)) AS CurrentWeek,
sum(IF(is_stockout= 1 AND operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0))
 / sum(IF(operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) AS LastWeek,
sum(IF(is_stockout= 1	AND DATE_FORMAT(date_delivered_promised,'%Y%m')=DATE_FORMAT(@max_promised,'%Y%m'),1,0))
 / sum(IF(DATE_FORMAT(date_delivered_promised,'%Y%m')=DATE_FORMAT(@max_promised,'%Y%m'),1,0)) AS CurrentMonth,
sum(IF(is_stockout= 1	AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0))
 / sum(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0)) AS LastMonth,
sum(IF(is_stockout= 1 AND delivered_promised_last_30=1,1,0))
 / sum(IF(delivered_promised_last_30=1,1,0)) AS Last30days
FROM
	operations_ve.out_order_tracking_tst
WHERE
	is_presale = 0
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type="supplier"
AND status_wms IN (
	'DS estoque reservado',
	'Waiting Dropshipping',
	'handled_by_marketplace',
	'awaiting_fulfillment',
	'expedido',
  'quebra tratada',
  'backorder_tratada',
  'quebrado',
  'Cancelado',
  "dropshipping notified",
  "packed_by_marketplace")
UNION
SELECT
	'%OOS declared < 2 workdays' AS kpi,
	sum(IF(workdays_to_stockout_declared<= 2	AND date_delivered_promised = @max_promised,1,0))
 / SUM(IF(date_delivered_promised = @max_promised,1,0)) AS LastWD,
	sum(IF(workdays_to_stockout_declared<= 2 AND operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised),1,0))
 / sum(IF(operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised),1,0)) AS CurrentWeek,
sum(IF(workdays_to_stockout_declared<= 2 AND operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0))
 / sum(IF(operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) AS LastWeek,
sum(IF(workdays_to_stockout_declared<= 2	AND DATE_FORMAT(date_delivered_promised,'%Y%m')=DATE_FORMAT(@max_promised,'%Y%m'),1,0))
 / sum(IF(DATE_FORMAT(date_delivered_promised,'%Y%m')=DATE_FORMAT(@max_promised,'%Y%m'),1,0)) AS CurrentMonth,
sum(IF(workdays_to_stockout_declared<= 2	AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0))
 / sum(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0)) AS LastMonth,
sum(IF(workdays_to_stockout_declared<= 2 AND delivered_promised_last_30=1,1,0))
 / sum(IF(delivered_promised_last_30=1,1,0)) AS Last30days
FROM
	operations_ve.out_order_tracking_tst
WHERE
	is_presale = 0
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type="supplier"
AND status_wms IN (
	'DS estoque reservado',
	'Waiting Dropshipping',
	'handled_by_marketplace',
	'awaiting_fulfillment',
	'expedido',
  'quebra tratada',
  'backorder_tratada',
  'quebrado',
  'Cancelado',
  "dropshipping notified",
  "packed_by_marketplace")
UNION
SELECT
	'%OOS declared < 3 workdays' AS kpi,
	sum(IF(workdays_to_stockout_declared<= 3	AND date_delivered_promised = @max_promised,1,0))
 / SUM(IF(date_delivered_promised = @max_promised,1,0)) AS LastWD,
	sum(IF(workdays_to_stockout_declared<= 3 AND operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised),1,0))
 / sum(IF(operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised),1,0)) AS CurrentWeek,
sum(IF(workdays_to_stockout_declared<= 3 AND operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0))
 / sum(IF(operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) AS LastWeek,
sum(IF(workdays_to_stockout_declared<= 3	AND DATE_FORMAT(date_delivered_promised,'%Y%m')=DATE_FORMAT(@max_promised,'%Y%m'),1,0))
 / sum(IF(DATE_FORMAT(date_delivered_promised,'%Y%m')=DATE_FORMAT(@max_promised,'%Y%m'),1,0)) AS CurrentMonth,
sum(IF(workdays_to_stockout_declared<= 3	AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0))
 / sum(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0)) AS LastMonth,
sum(IF(workdays_to_stockout_declared<= 3 AND delivered_promised_last_30=1,1,0))
 / sum(IF(delivered_promised_last_30=1,1,0)) AS Last30days
FROM
	operations_ve.out_order_tracking_tst
WHERE
	is_presale = 0
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type="supplier"
AND status_wms IN (
	'DS estoque reservado',
	'Waiting Dropshipping',
	'handled_by_marketplace',
	'awaiting_fulfillment',
	'expedido',
  'quebra tratada',
  'backorder_tratada',
  'quebrado',
  'Cancelado',
  "dropshipping notified",
  "packed_by_marketplace")
UNION
SELECT
	'%OOS declared < 5 workdays' AS kpi,
	sum(IF(workdays_to_stockout_declared<= 5	AND date_delivered_promised = @max_promised,1,0))
 / SUM(IF(date_delivered_promised = @max_promised,1,0)) AS LastWD,
	sum(IF(workdays_to_stockout_declared<= 5 AND operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised),1,0))
 / sum(IF(operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised),1,0)) AS CurrentWeek,
sum(IF(workdays_to_stockout_declared<= 5 AND operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0))
 / sum(IF(operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) AS LastWeek,
sum(IF(workdays_to_stockout_declared<= 5	AND DATE_FORMAT(date_delivered_promised,'%Y%m')=DATE_FORMAT(@max_promised,'%Y%m'),1,0))
 / sum(IF(DATE_FORMAT(date_delivered_promised,'%Y%m')=DATE_FORMAT(@max_promised,'%Y%m'),1,0)) AS CurrentMonth,
sum(IF(workdays_to_stockout_declared<= 5	AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0))
 / sum(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0)) AS LastMonth,
sum(IF(workdays_to_stockout_declared<= 5 AND delivered_promised_last_30=1,1,0))
 / sum(IF(delivered_promised_last_30=1,1,0)) AS Last30days
FROM
	operations_ve.out_order_tracking_tst
WHERE
	is_presale = 0
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type="supplier"
AND status_wms IN (
	'DS estoque reservado',
	'Waiting Dropshipping',
	'handled_by_marketplace',
	'awaiting_fulfillment',
	'expedido',
  'quebra tratada',
  'backorder_tratada',
  'quebrado',
  'Cancelado',
  "dropshipping notified",
  "packed_by_marketplace");
  
  SET @max_promised = curdate();

SELECT 
max(date_delivered_promised) INTO @max_promised
FROM operations_ve.out_order_tracking_tst
WHERE date_delivered_promised < curdate();

DROP TABLE IF EXISTS operations_ve.table_dashboard_dropshipping_merchant;

CREATE TABLE operations_ve.table_dashboard_dropshipping_merchant

SELECT
'% Shipped < 2 workdays' AS kpi,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 2
AND date_delivered_promised = @max_promised,1,0))
/ sum(IF (date_delivered_promised = @max_promised,1,0)) AS LastWD,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 2
AND operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised),1,0))
/ SUM(IF(operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised),1,0)) AS CurrentWeek,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 2
AND operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0))
/ SUM(IF(operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0))  AS LastWeek,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 2
AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised, '%Y%m'),1,0))
/ SUM(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised, '%Y%m'),1,0)) AS CurrentMonth,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 2
AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0))
/ SUM(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0)) AS LastMonth,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 2
AND delivered_promised_last_30=1,1,0))
/ SUM(IF(delivered_promised_last_30=1,1,0)) AS Last30days
FROM
operations_ve.out_order_tracking_tst
WHERE
is_presale = 0
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type="merchant"
AND status_wms IN (
'DS estoque reservado',
'Waiting Dropshipping',
"dropshipping notified",
'handled_by_marketplace',
'packed_by_marketplace',
'awaiting_fulfillment',
'expedido'
)
UNION
SELECT
'% Shipped < 3 workdays' AS kpi,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 3
AND date_delivered_promised = @max_promised,1,0))
/ SUM(IF(date_delivered_promised = @max_promised,1,0)) AS LastWD,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 3
AND operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised),1,0))
/ SUM(IF(operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised),1,0)) AS CurrentWeek,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 3
AND operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0))
/ SUM(IF(operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) AS LastWeek,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 3
AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised, '%Y%m'),1,0))
/ SUM(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised, '%Y%m'),1,0)) AS CurrentMonth,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 3
AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0))
/ SUM(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0)) AS LastMonth,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 3            
AND delivered_promised_last_30=1,1,0))
/ SUM(IF(delivered_promised_last_30=1,1,0)) AS Last30days
FROM
operations_ve.out_order_tracking_tst
WHERE
is_presale = 0
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type="merchant"
AND status_wms IN (
'DS estoque reservado',
'Waiting Dropshipping',
'handled_by_marketplace',
'awaiting_fulfillment',
"dropshipping notified",
"packed_by_marketplace",
'expedido'
)
UNION
SELECT
'% Shipped < 5 workdays' AS kpi,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 5
AND date_delivered_promised = @max_promised,1,0))
/ SUM(IF(date_delivered_promised = @max_promised,1,0)) AS LastWD,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 5
AND operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised),1,0))
/ SUM(IF(operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised),1,0)) AS CurrentWeek,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 5
AND operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) 
/ SUM(IF(operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) AS LastWeek,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 5
AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised, '%Y%m'),1,0))
/ SUM(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised, '%Y%m'),1,0)) AS CurrentMonth,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 5
AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0))
/ SUM(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0)) AS LastMonth,
sum(IF ((workdays_total_shipment-workdays_to_export) <= 5
AND delivered_promised_last_30=1,1,0))
/ SUM(IF(delivered_promised_last_30=1,1,0)) AS Last30days
FROM
operations_ve.out_order_tracking_tst
WHERE
is_presale = 0
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type="merchant"
AND status_wms IN (
'DS estoque reservado',
'Waiting Dropshipping',
'handled_by_marketplace',
'awaiting_fulfillment',
"dropshipping notified",
"packed_by_marketplace",
'expedido')
UNION
SELECT
'% 1st attempt < 6 workdays' AS kpi,
sum(IF (workdays_total_1st_attempt <= 6
AND date_delivered_promised = @max_promised,1,0))
/ SUM(IF(date_delivered_promised = @max_promised,1,0)) AS LastWD,
sum(IF (workdays_total_1st_attempt <= 6
AND operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised),1,0))
/ SUM(IF(operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised),1,0)) AS CurrentWeek,
sum(IF (workdays_total_1st_attempt <= 6
AND operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) 
/ SUM(IF(operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) AS LastWeek,
sum(IF (workdays_total_1st_attempt <= 6
AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised, '%Y%m'),1,0))
/ SUM(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised, '%Y%m'),1,0)) AS CurrentMonth,
sum(IF (workdays_total_1st_attempt <= 6
AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0))
/ SUM(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0)) AS LastMonth,
sum(IF (workdays_total_1st_attempt <= 6
AND delivered_promised_last_30=1,1,0))
/ SUM(IF(delivered_promised_last_30=1,1,0)) AS Last30days
FROM
operations_ve.out_order_tracking_tst
WHERE
is_presale = 0
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type="merchant"
AND status_wms IN (
'DS estoque reservado',
'Waiting Dropshipping',
'handled_by_marketplace',
'awaiting_fulfillment',
"dropshipping notified",
"packed_by_marketplace",
'expedido')
UNION
SELECT
'% 1st attempt < 8 workdays' AS kpi,
sum(IF (workdays_total_1st_attempt <= 8
AND date_delivered_promised = @max_promised,1,0))
/ SUM(IF(date_delivered_promised = @max_promised,1,0)) AS LastWD,
sum(IF (workdays_total_1st_attempt <= 8
AND operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised),1,0))
/ SUM(IF(operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised),1,0)) AS CurrentWeek,
sum(IF (workdays_total_1st_attempt <= 8
AND operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) 
/ SUM(IF(operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) AS LastWeek,
sum(IF (workdays_total_1st_attempt <= 8
AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised, '%Y%m'),1,0))
/ SUM(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised, '%Y%m'),1,0)) AS CurrentMonth,
sum(IF (workdays_total_1st_attempt <= 8
AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0))
/ SUM(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0)) AS LastMonth,
sum(IF (workdays_total_1st_attempt <= 8
AND delivered_promised_last_30=1,1,0))
/ SUM(IF(delivered_promised_last_30=1,1,0)) AS Last30days
FROM
operations_ve.out_order_tracking_tst
WHERE
is_presale = 0
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type="merchant"
AND status_wms IN (
'DS estoque reservado',
'Waiting Dropshipping',
'handled_by_marketplace',
'awaiting_fulfillment',
"dropshipping notified",
"packed_by_marketplace",
'expedido')
UNION
SELECT
'% 1st attempt < 10 workdays' AS kpi,
sum(IF (workdays_total_1st_attempt <= 10
AND date_delivered_promised = @max_promised,1,0))
/ SUM(IF(date_delivered_promised = @max_promised,1,0)) AS LastWD,
sum(IF (workdays_total_1st_attempt <= 10
AND operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised),1,0))
/ SUM(IF(operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised),1,0)) AS CurrentWeek,
sum(IF (workdays_total_1st_attempt <= 10
AND operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0))
/ SUM(IF(operations_ve.week_iso(date_delivered_promised) = operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) AS LastWeek,
sum(IF (workdays_total_1st_attempt <= 10
AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised, '%Y%m'),1,0))
/ SUM(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised, '%Y%m'),1,0)) AS CurrentMonth,
sum(IF (workdays_total_1st_attempt <= 10
AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0))
/ SUM(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0)) AS LastMonth,
sum(IF (workdays_total_1st_attempt <= 10
AND delivered_promised_last_30=1,1,0))
/ SUM(IF(delivered_promised_last_30=1,1,0)) AS Last30days
FROM
operations_ve.out_order_tracking_tst
WHERE
is_presale = 0
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type="merchant"
AND status_wms IN (
'DS estoque reservado',
'Waiting Dropshipping',
'handled_by_marketplace',
'awaiting_fulfillment',
"dropshipping notified",
"packed_by_marketplace",
'expedido')
UNION
SELECT
	'% On time 1st attempt' AS kpi,
	sum(IF(on_time_total_1st_attempt= 1	AND date_delivered_promised = @max_promised,1,0))
 / SUM(IF(date_delivered_promised = @max_promised,1,0)) AS LastWD,
	sum(IF(on_time_total_1st_attempt= 1 AND operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised),1,0))
 / sum(IF(operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised),1,0)) AS CurrentWeek,
sum(IF(on_time_total_1st_attempt= 1 AND operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0))
 / sum(IF(operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) AS LastWeek,
sum(IF(on_time_total_1st_attempt= 1	AND DATE_FORMAT(date_delivered_promised,'%Y%m')=DATE_FORMAT(@max_promised,'%Y%m'),1,0))
 / sum(IF(DATE_FORMAT(date_delivered_promised,'%Y%m')=DATE_FORMAT(@max_promised,'%Y%m'),1,0)) AS CurrentMonth,
sum(IF(on_time_total_1st_attempt= 1	AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0))
 / sum(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0)) AS LastMonth,
sum(IF(on_time_total_1st_attempt= 1 AND delivered_promised_last_30=1,1,0))
 / sum(IF(delivered_promised_last_30=1,1,0)) AS Last30days
FROM
	operations_ve.out_order_tracking_tst
WHERE
	is_presale = 0
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type="merchant"
AND status_wms IN (
	'DS estoque reservado',
	'Waiting Dropshipping',
	'handled_by_marketplace',
	'awaiting_fulfillment',
  "dropshipping notified",
  "packed_by_marketplace",
	'expedido')
UNION
SELECT
	'% OOS' AS kpi,
	sum(IF(is_stockout= 1	AND date_delivered_promised = @max_promised,1,0))
 / SUM(IF(date_delivered_promised = @max_promised,1,0)) AS LastWD,
	sum(IF(is_stockout= 1 AND operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised),1,0))
 / sum(IF(operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised),1,0)) AS CurrentWeek,
sum(IF(is_stockout= 1 AND operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0))
 / sum(IF(operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) AS LastWeek,
sum(IF(is_stockout= 1	AND DATE_FORMAT(date_delivered_promised,'%Y%m')=DATE_FORMAT(@max_promised,'%Y%m'),1,0))
 / sum(IF(DATE_FORMAT(date_delivered_promised,'%Y%m')=DATE_FORMAT(@max_promised,'%Y%m'),1,0)) AS CurrentMonth,
sum(IF(is_stockout= 1	AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0))
 / sum(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0)) AS LastMonth,
sum(IF(is_stockout= 1 AND delivered_promised_last_30=1,1,0))
 / sum(IF(delivered_promised_last_30=1,1,0)) AS Last30days
FROM
	operations_ve.out_order_tracking_tst
WHERE
	is_presale = 0
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type="merchant"
AND status_wms IN (
	'DS estoque reservado',
	'Waiting Dropshipping',
	'handled_by_marketplace',
	'awaiting_fulfillment',
	'expedido',
  'quebra tratada',
  'backorder_tratada',
  'quebrado',
  'Cancelado',
  "dropshipping notified",
  "packed_by_marketplace")
UNION
SELECT
	'%OOS declared < 2 workdays' AS kpi,
	sum(IF(workdays_to_stockout_declared<= 2	AND date_delivered_promised = @max_promised,1,0))
 / SUM(IF(date_delivered_promised = @max_promised,1,0)) AS LastWD,
	sum(IF(workdays_to_stockout_declared<= 2 AND operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised),1,0))
 / sum(IF(operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised),1,0)) AS CurrentWeek,
sum(IF(workdays_to_stockout_declared<= 2 AND operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0))
 / sum(IF(operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) AS LastWeek,
sum(IF(workdays_to_stockout_declared<= 2	AND DATE_FORMAT(date_delivered_promised,'%Y%m')=DATE_FORMAT(@max_promised,'%Y%m'),1,0))
 / sum(IF(DATE_FORMAT(date_delivered_promised,'%Y%m')=DATE_FORMAT(@max_promised,'%Y%m'),1,0)) AS CurrentMonth,
sum(IF(workdays_to_stockout_declared<= 2	AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0))
 / sum(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0)) AS LastMonth,
sum(IF(workdays_to_stockout_declared<= 2 AND delivered_promised_last_30=1,1,0))
 / sum(IF(delivered_promised_last_30=1,1,0)) AS Last30days
FROM
	operations_ve.out_order_tracking_tst
WHERE
	is_presale = 0
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type="merchant"
AND status_wms IN (
	'DS estoque reservado',
	'Waiting Dropshipping',
	'handled_by_marketplace',
	'awaiting_fulfillment',
	'expedido',
  'quebra tratada',
  'backorder_tratada',
  'quebrado',
  'Cancelado',
  "dropshipping notified",
  "packed_by_marketplace")
UNION
SELECT
	'%OOS declared < 3 workdays' AS kpi,
	sum(IF(workdays_to_stockout_declared<= 3	AND date_delivered_promised = @max_promised,1,0))
 / SUM(IF(date_delivered_promised = @max_promised,1,0)) AS LastWD,
	sum(IF(workdays_to_stockout_declared<= 3 AND operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised),1,0))
 / sum(IF(operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised),1,0)) AS CurrentWeek,
sum(IF(workdays_to_stockout_declared<= 3 AND operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0))
 / sum(IF(operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) AS LastWeek,
sum(IF(workdays_to_stockout_declared<= 3	AND DATE_FORMAT(date_delivered_promised,'%Y%m')=DATE_FORMAT(@max_promised,'%Y%m'),1,0))
 / sum(IF(DATE_FORMAT(date_delivered_promised,'%Y%m')=DATE_FORMAT(@max_promised,'%Y%m'),1,0)) AS CurrentMonth,
sum(IF(workdays_to_stockout_declared<= 3	AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0))
 / sum(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0)) AS LastMonth,
sum(IF(workdays_to_stockout_declared<= 3 AND delivered_promised_last_30=1,1,0))
 / sum(IF(delivered_promised_last_30=1,1,0)) AS Last30days
FROM
	operations_ve.out_order_tracking_tst
WHERE
	is_presale = 0
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type="merchant"
AND status_wms IN (
	'DS estoque reservado',
	'Waiting Dropshipping',
	'handled_by_marketplace',
	'awaiting_fulfillment',
	'expedido',
  'quebra tratada',
  'backorder_tratada',
  'quebrado',
  'Cancelado',
  "dropshipping notified",
  "packed_by_marketplace")
UNION
SELECT
	'%OOS declared < 5 workdays' AS kpi,
	sum(IF(workdays_to_stockout_declared<= 5	AND date_delivered_promised = @max_promised,1,0))
 / SUM(IF(date_delivered_promised = @max_promised,1,0)) AS LastWD,
	sum(IF(workdays_to_stockout_declared<= 5 AND operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised),1,0))
 / sum(IF(operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised),1,0)) AS CurrentWeek,
sum(IF(workdays_to_stockout_declared<= 5 AND operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0))
 / sum(IF(operations_ve.week_iso(date_delivered_promised)=operations_ve.week_iso(@max_promised - INTERVAL 1 WEEK),1,0)) AS LastWeek,
sum(IF(workdays_to_stockout_declared<= 5	AND DATE_FORMAT(date_delivered_promised,'%Y%m')=DATE_FORMAT(@max_promised,'%Y%m'),1,0))
 / sum(IF(DATE_FORMAT(date_delivered_promised,'%Y%m')=DATE_FORMAT(@max_promised,'%Y%m'),1,0)) AS CurrentMonth,
sum(IF(workdays_to_stockout_declared<= 5	AND DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0))
 / sum(IF(DATE_FORMAT(date_delivered_promised,'%Y%m') = DATE_FORMAT(@max_promised-INTERVAL 1 MONTH, '%Y%m'),1,0)) AS LastMonth,
sum(IF(workdays_to_stockout_declared<= 5 AND delivered_promised_last_30=1,1,0))
 / sum(IF(delivered_promised_last_30=1,1,0)) AS Last30days
FROM
	operations_ve.out_order_tracking_tst
WHERE
	is_presale = 0
AND fulfillment_type_real = 'Dropshipping'
AND supplier_type="merchant"
AND status_wms IN (
	'DS estoque reservado',
	'Waiting Dropshipping',
	'handled_by_marketplace',
	'awaiting_fulfillment',
	'expedido',
  'quebra tratada',
  'backorder_tratada',
  'quebrado',
  'Cancelado',
  "dropshipping notified",
  "packed_by_marketplace");