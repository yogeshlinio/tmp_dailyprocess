INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check) 
SELECT 
  'Venezuela', 
  'out_order_tracking',
  'finish',
  NOW(),
  max(datetime_ordered),
  count(*),
  count(item_counter)
FROM
  operations_ve.out_order_tracking;