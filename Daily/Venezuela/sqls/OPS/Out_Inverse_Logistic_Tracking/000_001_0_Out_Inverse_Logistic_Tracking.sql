DROP TABLE IF EXISTS operations_ve.out_inverse_logistics_tracking_sample;
CREATE TABLE operations_ve.out_inverse_logistics_tracking_sample LIKE operations_ve.out_inverse_logistics_tracking;
INSERT INTO operations_ve.out_inverse_logistics_tracking_sample (
	item_id,
	order_number,
	sku_config,
	sku_simple,
	sku_name,
	price_w_o_vat,
	cost_w_o_vat,
	supplier_id,
	supplier_name,
	buyer,
	head_buyer,
	package_height,
	package_length,
	package_width,
	package_weight,
	fulfillment_type_bob,
	fulfillment_type_real,
	shipping_carrier,
	date_ordered,
	date_exported,
	date_backorder,
	date_backorder_tratada,
	date_backorder_accepted,
	date_po_created,
	date_po_updated,
	date_po_issued,
	date_po_confirmed,
	#date_sent_tracking_code,
	date_procured,
	date_ready_to_pick,
	date_ready_to_ship,
	date_tracking_code_created,
	date_shipped,
	date_1st_attempt,
	date_delivered,
	date_failed_delivery,
	date_returned,
	date_procured_promised,
	date_ready_to_ship_promised,
	date_delivered_promised,
	date_stock_updated,
	date_declared_stockout,
	date_declared_backorder,
	date_assigned_to_stock,
	date_status_value_chain,
	date_cancelled,
    datetime_cancelled,
	datetime_ordered,
	datetime_exported,
	datetime_po_created,
	datetime_po_updated,
	datetime_po_updatetimed,
	datetime_po_issued,
	datetime_po_confirmed,
	#datetime_sent_tracking_code,
	datetime_procured,
	datetime_ready_to_pick,
	datetime_ready_to_ship,
	datetime_shipped,
	datetime_assigned_to_stock,
	week_procured_promised,
	week_delivered_promised,
	week_r2s_promised,
	week_shipped,
	month_procured_promised,
	month_delivered_promised,
	month_r2s_promised,
	on_time_to_po,
	on_time_to_confirm,
	on_time_to_send_po,
	on_time_to_procure,
	on_time_to_export,
	on_time_to_ready,
	on_time_to_ship,
	on_time_to_1st_attempt,
	on_time_to_deliver,
	on_time_to_request_recollection,
	on_time_to_stockout_declared,
	on_time_to_create_po,
	on_time_to_create_tracking_code,
	on_time_total_1st_attempt,
	on_time_total_delivery,
	is_presale,
	payment_method,
	wms_tracking_code,
	shipping_carrier_tracking_code,
	shipping_carrier_tracking_code_inverse,
	status_bob,
	status_wms,
	item_counter,
	package_measure_new,
	is_marketplace,
	is_returned,
	cat_1,
	cat_2,
	cat_3,
	category_bp,
  ship_to_met_area,
  ship_to_region,
  ship_to_city,
  loyalty,
	has_nps_score
) SELECT
	item_id,
	order_number,
	sku_config,
	sku_simple,
	sku_name,
	price_w_o_vat,
	cost_w_o_vat,
	supplier_id,
	supplier_name,
	buyer,
	head_buyer,
	package_height,
	package_length,
	package_width,
	package_weight,
	fulfillment_type_bob,
	fulfillment_type_real,
	shipping_carrier,
	date_ordered,
	date_exported,
	date_backorder,
	date_backorder_tratada,
	date_backorder_accepted,
	date_po_created,
	date_po_updated,
	date_po_issued,
	date_po_confirmed,
	#date_sent_tracking_code,
	date_procured,
	date_ready_to_pick,
	date_ready_to_ship,
	date_tracking_code_created,
	date_shipped,
	date_1st_attempt,
	date_delivered,
	date_failed_delivery,
	date_returned,
	date_procured_promised,
	date_ready_to_ship_promised,
	date_delivered_promised,
	date_stock_updated,
	date_declared_stockout,
	date_declared_backorder,
	date_assigned_to_stock,
	date_status_value_chain,
	date_cancelled,
    datetime_cancelled,
	datetime_ordered,
	datetime_exported,
	datetime_po_created,
	datetime_po_updated,
	datetime_po_updatetimed,
	datetime_po_issued,
	datetime_po_confirmed,
	#datetime_sent_tracking_code,
	datetime_procured,
	datetime_ready_to_pick,
	datetime_ready_to_ship,
	datetime_shipped,
	datetime_assigned_to_stock,
	week_procured_promised,
	week_delivered_promised,
	week_r2s_promised,
	week_shipped,
	month_procured_promised,
	month_delivered_promised,
	month_r2s_promised,
	on_time_to_po,
	on_time_to_confirm,
	on_time_to_send_po,
	on_time_to_procure,
	on_time_to_export,
	on_time_to_ready,
	on_time_to_ship,
	on_time_to_1st_attempt,
	on_time_to_deliver,
	on_time_to_request_recollection,
	on_time_to_stockout_declared,
	on_time_to_create_po,
	on_time_to_create_tracking_code,
	on_time_total_1st_attempt,
	on_time_total_delivery,
	is_presale,
	payment_method,
	wms_tracking_code,
	shipping_carrier_tracking_code,
	shipping_carrier_tracking_code_inverse,
	status_bob,
	status_wms,
	item_counter,
	package_measure_new,
	is_marketplace,
	is_returned,
	cat_1,
	cat_2,
	cat_3,
	category_bp,
  ship_to_met_area,
  ship_to_region,
  ship_to_city,
  loyalty,
	has_nps_score
FROM
	operations_ve.out_order_tracking;

SELECT  'Update out_inverse_logistics_tracking_sample',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample AS a 
INNER JOIN operations_ve.pro_inverse_logistics_docs AS b
ON a.item_id = b.item_id
SET
a.date_inbound_il = b.date_entrance_ilwh,
a.shipping_carrier_tracking_code_inverse = b.dhl_tracking_code,
a.il_type = b.status,
a.reason_for_entrance_il = b.reason,
a.action_il = b.ilwh_1st_step,
a.damaged_item = b.damaged_item,
a.damaged_package = b.damaged_package,
a.comments_il = b.comments,
a.return_accepted = b.return_accepted,
a.position_il = b.ilwh_position,
a.date_exit_il = b.date_exit_ilwh,
a.date_canceled_il = b.date_cancelled,
a.analyst_il  = b.entered_to_ilwh_by,
a.comments = b.comments_main,
a.is_returned = 1;

SELECT  'is_returned',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample as a 
INNER JOIN wmsprod_ve.inverselogistics_devolucion as b
ON a.item_id = b.item_id 
SET a.is_returned = 1;

SELECT  'Update date_customer_request_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample AS a
INNER JOIN bob_live_ve.sales_order_item_status_history AS b
ON a.item_id=b.fk_sales_order_item
SET a.date_customer_request_il=b.created_at
WHERE b.fk_sales_order_item_status=111;

SELECT  'Update date_customer_request_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample AS a
INNER JOIN bob_live_ve.sales_order_item_status_history AS b
ON a.item_id=b.fk_sales_order_item
SET a.date_customer_request_il=b.created_at
WHERE b.fk_sales_order_item_status=112;

SELECT  'Drop operations_ve.TMP',now();
#se toma el segundo estado de aguardando retorno ya que el primero es el que hace el sistema automaticamente
#y el segundo es luego de aguardando agendamiento
DROP TEMPORARY TABLE IF EXISTS operations_ve.TMP ; 

SELECT  'Create operations_ve.TMP',now();
CREATE TEMPORARY TABLE operations_ve.TMP 
SELECT
		b.return_id,
		b. STATUS,
		min(b.changed_at) as cambiado ,
		c.item_id
	FROM
		wmsprod_ve.inverselogistics_status_history b
	JOIN wmsprod_ve.inverselogistics_devolucion c ON b.return_id = c.id
WHERE b.STATUS = 2
Group by item_id;

SELECT  'create index operations_ve.TMP',now();
create index item_id on operations_ve.TMP(item_id);

SELECT  'date_first_track_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample a
INNER JOIN operations_ve.TMP
ON a.item_id = TMP.item_id
SET a.date_first_track_il = TMP.cambiado
WHERE
	TMP. STATUS = 2;

SELECT  'Drop operations_ve.TMP2',now();
DROP TEMPORARY TABLE IF EXISTS operations_ve.TMP2 ; 

SELECT  'Create operations_ve.TMP2',now();
CREATE TEMPORARY TABLE operations_ve.TMP2 
SELECT
		b.return_id,
		b. STATUS,
		max(b.changed_at) as cambiado ,
		c.item_id
	FROM
		wmsprod_ve.inverselogistics_status_history b
	JOIN wmsprod_ve.inverselogistics_devolucion c ON b.return_id = c.id
WHERE b.STATUS = 2
Group by item_id;

SELECT  'create index operations_ve.TMP2',now();
create index item_id on operations_ve.TMP2(item_id);

SELECT  'TMP2. STATUS',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample a
INNER JOIN operations_ve.TMP2
ON a.item_id = TMP2.item_id
SET a.date_last_track_il = TMP2.cambiado
WHERE
	TMP2. STATUS = 2;

SELECT  'calcworkdays',now();
CALL operations_ve.calcworkdays;

SELECT  'Update date_quality_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample AS a
INNER JOIN bob_live_ve.sales_order_item_status_history AS b
ON a.item_id=b.fk_sales_order_item
SET a.date_quality_il=b.created_at
WHERE b.fk_sales_order_item_status=104;

SELECT  'Update date_cash_refunded_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample AS a
INNER JOIN bob_live_ve.sales_order_item_status_history AS b
ON a.item_id=b.fk_sales_order_item
SET a.date_cash_refunded_il=b.created_at
WHERE b.fk_sales_order_item_status=113;

SELECT  'Update date_voucher_refunded_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample AS a
INNER JOIN bob_live_ve.sales_order_item_status_history AS b
ON a.item_id=b.fk_sales_order_item
SET a.date_voucher_refunded_il=b.created_at
WHERE b.fk_sales_order_item_status=102;

SELECT  'Update date_closed BOB',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample AS a
INNER JOIN bob_live_ve.sales_order_item_status_history AS b
ON a.item_id=b.fk_sales_order_item
SET a.date_closed_il=b.created_at
WHERE b.fk_sales_order_item_status=6;

SELECT  'date_refunded_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample AS a
SET a.date_refunded_il= IF(a.date_cash_refunded_il IS NULL,a.date_voucher_refunded_il,a.date_cash_refunded_il);

SELECT  'Drop TMP3analyst',now();
DROP TEMPORARY TABLE IF EXISTS operations_ve.TMP3analyst;

SELECT  'Create TMP3analyst',now();
CREATE TEMPORARY TABLE operations_ve.TMP3analyst
SELECT d.*, e.nome
FROM
(SELECT
		b.user_id,
		b.return_id,
		b. STATUS,
		b.changed_at,
		c.item_id
	FROM
		wmsprod_ve.inverselogistics_status_history b
	JOIN wmsprod_ve.inverselogistics_devolucion c ON b.return_id = c.id) d 
INNER JOIN wmsprod_ve.usuarios as e ON d.user_id = e.usuarios_id
WHERE
	d. STATUS = 6;

SELECT  'create index TMP3analyst',now();
create index item on operations_ve.TMP3analyst(item_id);

SELECT  'date_inbound_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample a
INNER JOIN operations_ve.TMP3analyst as d ON a.item_id = d.item_id
SET a.analyst_il = d.nome,
a.date_inbound_il  = d.changed_at
WHERE
	d. STATUS = 6;


############## CALCULO DE DIAS  ##############

UPDATE operations_ve.out_inverse_logistics_tracking_sample
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_inbound_il) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking_sample.date_exit_il) = calcworkdays.date_last
SET 
days_to_process_return = 
	IF(date_exit_il IS NULL,
		DATEDIFF(date(CURDATE()),date(date_inbound_il)),
		DATEDIFF(date(date_exit_il),date(date_inbound_il))),
time_to_process_return =
If( date_inbound_il is Null,
TIMESTAMPDIFF( SECOND ,date_inbound_il, CURDATE())/3600,
TIMESTAMPDIFF( SECOND ,date_inbound_il, date_exit_il)/3600),
workdays_to_process_return = 
	IF(date_exit_il IS NULL,
		NULL,calcworkdays.workdays
		)
WHERE out_inverse_logistics_tracking_sample.is_returned=1
AND out_inverse_logistics_tracking_sample.date_inbound_il IS NOT NULL;


UPDATE operations_ve.out_inverse_logistics_tracking_sample
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_inbound_il) = calcworkdays.date_first
		AND date(CURDATE()) = calcworkdays.date_last
SET 
workdays_to_process_return = calcworkdays.workdays
WHERE out_inverse_logistics_tracking_sample.is_returned=1
AND out_inverse_logistics_tracking_sample.date_inbound_il IS NOT NULL
AND out_inverse_logistics_tracking_sample.date_exit_il IS NULL;



UPDATE operations_ve.out_inverse_logistics_tracking_sample
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_failed_delivery) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking_sample.date_cancelled) = calcworkdays.date_last
SET 
days_to_process_fail_delivery = 
	IF(date_failed_delivery < date_cancelled ,
		DATEDIFF(date(date_cancelled),date(date_failed_delivery)),
   IF( date_failed_delivery < date_ready_to_ship ,
		DATEDIFF(date(date_ready_to_ship),date(date_failed_delivery)),DATEDIFF(date(CURDATE()),date(date_failed_delivery)))),
time_to_process_fail_delivery =
IF(date_failed_delivery < date_cancelled ,
		TIMESTAMPDIFF( SECOND ,date_failed_delivery, datetime_cancelled)/3600,
   IF( date_failed_delivery < date_ready_to_ship ,
		TIMESTAMPDIFF( SECOND ,date_failed_delivery, datetime_ready_to_ship)/3600,TIMESTAMPDIFF( SECOND ,date_failed_delivery, CURDATE())/3600)),
workdays_to_process_fail_delivery = 
	IF(date_failed_delivery < date_cancelled , calcworkdays.workdays , NULL)
WHERE out_inverse_logistics_tracking_sample.is_fail_delivery=1
AND out_inverse_logistics_tracking_sample.date_failed_delivery IS NOT NULL;


UPDATE operations_ve.out_inverse_logistics_tracking_sample
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_failed_delivery) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking_sample.date_ready_to_ship) = calcworkdays.date_last
SET 
workdays_to_process_fail_delivery = 
	IF(date_failed_delivery < date_ready_to_ship , calcworkdays.workdays , NULL)
WHERE out_inverse_logistics_tracking_sample.is_fail_delivery=1
AND out_inverse_logistics_tracking_sample.date_failed_delivery IS NOT NULL;


UPDATE operations_ve.out_inverse_logistics_tracking_sample
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_failed_delivery) = calcworkdays.date_first
		AND date(CURDATE()) = calcworkdays.date_last
SET 
workdays_to_process_fail_delivery = calcworkdays.workdays 
WHERE out_inverse_logistics_tracking_sample.is_fail_delivery=1
AND (date_failed_delivery > date_ready_to_ship OR date_failed_delivery > datetime_cancelled )
AND out_inverse_logistics_tracking_sample.date_failed_delivery IS NOT NULL;

SELECT  'days_to_inbound_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_last_track_il) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking_sample.date_inbound_il) = calcworkdays.date_last
                          AND calcworkdays.isweekday = 1 
	            AND calcworkdays.isholiday = 0
SET 
days_to_inbound_il = 
	IF(date_last_track_il IS NULL,
		NULL,
		DATEDIFF(date(date_inbound_il),date(date_last_track_il))),
workdays_to_inbound_il = 
	IF(date_last_track_il IS NULL,
		NULL,calcworkdays.workdays
		)
WHERE out_inverse_logistics_tracking_sample.is_returned=1
AND out_inverse_logistics_tracking_sample.date_inbound_il IS NOT NULL;

SELECT  'days_to_quality_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_inbound_il) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking_sample.date_quality_il)= calcworkdays.date_last
                        AND calcworkdays.isweekday = 1 
	            AND calcworkdays.isholiday = 0
SET 
days_to_quality_il = 
	IF(date_inbound_il IS NULL,
		NULL,
		DATEDIFF(date(date_quality_il),date(date_inbound_il))),
workdays_to_quality_il = 
	IF((date_inbound_il IS NULL),
		NULL,calcworkdays.workdays)
WHERE operations_ve.out_inverse_logistics_tracking_sample.is_returned =1
AND date_quality_il is not null;

SELECT  'days_to_customer_request_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample AS a
INNER JOIN operations_ve.calcworkdays
	ON date(date_delivered) = calcworkdays.date_first
		AND date(date_customer_request_il) = calcworkdays.date_last
                        AND calcworkdays.isweekday = 1 
	            AND calcworkdays.isholiday = 0
SET 
days_to_customer_request_il = 
IF(date_delivered IS NULL,
		NULL,
	DATEDIFF(date(date_customer_request_il),date(date_delivered))),
workdays_to_customer_request_il  = 
IF(date_customer_request_il IS NULL,
		NULL,calcworkdays.workdays)
WHERE is_returned = 1
AND date_customer_request_il IS NOT NULL;

SELECT  'days_to_track_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_customer_request_il) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking_sample.date_first_track_il) = calcworkdays.date_last
    AND calcworkdays.isweekday = 1 
	  AND calcworkdays.isholiday = 0
SET 
days_to_track_il = 
	IF(date_customer_request_il IS NULL,
		NULL,
		DATEDIFF(date(date_first_track_il),date(date_customer_request_il))),
workdays_to_track_il = 
	IF(date_customer_request_il IS NULL,
		NULL,calcworkdays.workdays
		)
WHERE is_returned =1
AND date_first_track_il IS NOT NULL;

SELECT  'days_to_refunded_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_customer_request_il) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking_sample.date_refunded_il)= calcworkdays.date_last
    AND calcworkdays.isweekday = 1 
	  AND calcworkdays.isholiday = 0
SET 
days_to_refunded_il = 
	IF((date_customer_request_il IS NULL),
		NULL,
		DATEDIFF(date(date_refunded_il),date(date_customer_request_il))),
workdays_to_refunded_il = 
	IF((date_customer_request_il IS NULL),
		NULL,calcworkdays.workdays)
WHERE is_returned =1
AND date_refunded_il IS NOT NULL;

SELECT  'days_to_closed_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample 
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_customer_request_il)= calcworkdays.date_first
		AND date(out_inverse_logistics_tracking_sample.date_closed_il)= calcworkdays.date_last
  AND calcworkdays.isweekday = 1
AND calcworkdays.isholiday = 0
SET 
days_to_closed_il = 
	IF(date_customer_request_il IS NULL,
		NULL,
		DATEDIFF(date(date_closed_il),date(date_customer_request_il))),
workdays_to_closed_il = 
	IF(date_customer_request_il IS NULL,
		NULL,calcworkdays.workdays)
WHERE is_returned =1
AND date_closed_il IS NOT NULL;

SELECT  'Drop operations_ve.TMP2',now();
DROP TEMPORARY TABLE IF EXISTS operations_ve.TMP2 ; 

SELECT  'Create operations_ve.TMP2',now();
CREATE TEMPORARY TABLE operations_ve.TMP2 
SELECT c.*, d.description 
FROM wmsprod_ve.inverselogistics_devolucion as c INNER JOIN wmsprod_ve.inverselogistics_devolucion_razon as d on c.reason_id = d.reason_id;

SELECT  'create index operations_ve.TMP2',now();
create index item_id on operations_ve.TMP2(item_id);

SELECT  'id_ilwh',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample as a
INNER JOIN operations_ve.TMP2 b ON a.item_id = b.item_id
SET a.status_wms_il = b.status,
a.id_ilwh = b.id,
a.reason_for_entrance_il = b.description,
date_canceled_il = b.created_at;

SELECT  'Drop tmp_pro_min_date_exported',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample AS a
INNER JOIN wmsprod_ve.inverselogistics_catalog_status AS b
ON a.status_wms_il=b.id_status
SET a.status_wms_il=b.status_inverselogistic;

SELECT  'is_fail_delivery',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample as a
INNER JOIN wmsprod_ve.tms_status_delivery b ON a.wms_tracking_code = b.cod_rastreamento
SET a.il_type = 'Fail delivery',
a.is_fail_delivery = 1, a.date_inbound_il = b.date WHERE b.id_status = 10;

SELECT  'is_fail_delivery',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample a
INNER JOIN wmsprod_ve.ws_log_lista_embarque b ON b.cod_rastreamento = a.wms_tracking_code
SET a.reason_for_entrance_il = b.observaciones
WHERE
 a.is_fail_delivery = 1;

SELECT  'is_returned',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample as a
SET a.il_type = 'Return'
WHERE a.is_returned = 1;

SELECT  'Drop operations_ve.TMP3 ',now();
DROP TEMPORARY TABLE IF EXISTS operations_ve.TMP3 ; 

SELECT  'Create operations_ve.TMP3 ',now();
CREATE TEMPORARY TABLE operations_ve.TMP3 
SELECT c.*, d.description 
FROM wmsprod_ve.inverselogistics_devolucion as c INNER JOIN wmsprod_ve.inverselogistics_devolucion_accion as d on c.action_id = d.action_id;

SELECT  'Create index operations_ve.TMP3 ',now();
create index item_id on operations_ve.TMP3(item_id);

SELECT  'Create index operations_ve.TMP3 ',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample as a
INNER JOIN operations_ve.TMP3 b ON a.item_id = b.item_id
SET a.action_il = b.description;

SELECT  'date_exit_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample  as  a
INNER JOIN wmsprod_ve.inverselogistics_devolucion as b ON a.item_id = b.item_id
SET a.date_exit_il = CASE
WHEN a.status_wms_il = 'retornar_stock'
OR a.status_wms_il= 'enviar_cuarentena' 
OR a.status_wms_il= 'reenvio_cliente' THEN
	modified_at
ELSE
	NULL
END;

SELECT  'status_wms_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample a
SET a.return_accepted = CASE
WHEN a.status_wms_il = 'retornar_stock'
OR a.status_wms_il = 'enviar_cuarentena' THEN
	1
ELSE
	NULL
END;

SELECT  'is_exits_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample as a 
SET a.is_exits_il = 1 
WHERE date_exit_il is not null;

SELECT  'is_pending_return_wh_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample as a 
SET a.is_pending_return_wh_il = 1 
WHERE status_wms_il in ('aguardando_agendamiento','aguardando_retorno');

SELECT  'Drop TMP2analyst',now();
DROP TEMPORARY TABLE IF EXISTS operations_ve.TMP2analyst ; 

SELECT  'Create TMP2analyst',now();
CREATE TEMPORARY TABLE operations_ve.TMP2analyst
SELECT
		b.cod_rastreamento,
		b.id_status,
		b.id_user,
		c.nome
	FROM
		wmsprod_ve.tms_status_delivery b
	JOIN wmsprod_ve.usuarios c ON b.id_user = c.usuarios_id 
WHERE
	b.id_status = 10 ;

SELECT  'create index TMP2analyst',now();
create index cod on operations_ve.TMP2analyst(cod_rastreamento);

SELECT  'analyst_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample a
INNER JOIN operations_ve.TMP2analyst as  b ON b.cod_rastreamento = a.wms_tracking_code
SET a.analyst_il = b.nome
WHERE
	a.il_type = 'Fail delivery';

SELECT  'delivered_last_30_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample
SET out_inverse_logistics_tracking_sample.delivered_last_30_il = CASE
WHEN datediff(curdate(), date_delivered) <= 45
AND datediff(curdate(), date_delivered) >= 15 THEN
	1
ELSE
	0
END;

SELECT  'date_first_track_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_customer_request_il)= calcworkdays.date_first
		AND date(curdate())= calcworkdays.date_last
SET 
backlog_track_il = 
	IF(calcworkdays.workdays >2,1,0)
WHERE date_first_track_il is null
AND date_inbound_il IS null
AND date_quality_il IS NULL
AND date_closed_il is null;

SELECT  'backlog_quality_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_inbound_il)= calcworkdays.date_first
		AND date(curdate())= calcworkdays.date_last
SET 
backlog_quality_il = 
	IF(calcworkdays.workdays >3,1,0)
WHERE date_quality_il is null
AND date_closed_il is null
AND is_returned=1;

/*
UPDATE operations_ve.out_inverse_logistics_tracking_sample AS t1
INNER JOIN
(SELECT numero_guia
FROM outbound_co.tbl_outbound_daily_servientrega
GROUP BY numero_guia) as t2
ON t1.shipping_carrier_tracking_code_inverse = t2.numero_guia
SET t1.return_receivedserv = 1;


UPDATE operations_ve.out_inverse_logistics_tracking_sample
SET 
backlog_currier_il = 
	IF(((return_receivedserv = 1) AND (date_inbound_il IS NULL)),1,0)
WHERE is_returned=1
AND date_quality_il IS NULL
AND date_closed_il is null;



UPDATE operations_ve.out_inverse_logistics_tracking_sample
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_delivered)= calcworkdays.date_first
		AND date(curdate())= calcworkdays.date_last
SET 
backlog_refund_consignment_il = 
	IF(calcworkdays.workdays>15,1,0)
AND date_cash_refunded_il is null
AND date_voucher_refunded_il IS NULL
AND date_closed_il is null
AND date_quality_il IS NOT NULL
AND is_returned =1
AND action_il='Reembolso tarjeta o cuenta (10-20 d�as h�biles)';




UPDATE operations_ve.out_inverse_logistics_tracking_sample
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_quality_il)= calcworkdays.date_first
		AND date(curdate())= calcworkdays.date_last
SET 
backlog_refund_effecty_il = 
	IF(calcworkdays.workdays>4,1,0)
AND date_cash_refunded_il is null
AND date_voucher_refunded_il IS NULL
AND date_closed_il is null
AND date_quality_il IS NOT NULL
AND is_returned =1
AND action_il='Reembolso en cr�dito Linio (2 d�as h�biles)';

*/

SELECT  'backlog_refund_reversion_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_quality_il)= calcworkdays.date_first
		AND date(curdate())= calcworkdays.date_last
SET 
backlog_refund_reversion_il = 
	IF(calcworkdays.workdays>20,1,0)
AND date_cash_refunded_il is null
AND date_voucher_refunded_il IS NULL
AND date_closed_il is null
AND date_quality_il IS NOT NULL
AND is_returned =1
AND action_il='Reembolso tarjeta o cuenta (10-20 d�as h�biles)';

SELECT  'backlog_refund_voucher_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking_sample
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_quality_il)= calcworkdays.date_first
		AND date(curdate())= calcworkdays.date_last
SET 
backlog_refund_voucher_il = 
	IF(calcworkdays.workdays>2,1,0)
AND date_cash_refunded_il is null
AND date_voucher_refunded_il IS NULL
AND date_closed_il is null
AND date_quality_il IS NOT NULL
AND is_returned =1
AND action_il='Reembolso en cr�dito Linio (2 d�as h�biles)';

UPDATE operations_ve.out_inverse_logistics_tracking_sample a
INNER JOIN operations_ve.calcworkdays b
 ON a.date_inbound_il = b.date_first
 AND 2 = b.workdays
SET date_processed_return_promised = b.date_last
WHERE is_returned = 1;