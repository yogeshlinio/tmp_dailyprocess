UPDATE A_CRM_INTERFACE_EMAIL INNER JOIN
      (
            SELECT
              `Interface-ID`,
              Alias,
              `UniqueKey`AS email,
              min( date( TIMESTAMP ) ) as date,
              #`Bounce-Text` AS bounce_reason,
              `Type` AS bounce_reason,
              count(*) as bounceCount
             FROM interface_bounces
             GROUP BY  `Interface-ID`, UniqueKey
       )  AS TMP
     USING (  `Interface-ID`, email )
SET
    A_CRM_INTERFACE_EMAIL.dateBounce    = TMP.date ,
    A_CRM_INTERFACE_EMAIL.BounceCount = TMP.bounceCount,
    A_CRM_INTERFACE_EMAIL.bounce = 1,
    A_CRM_INTERFACE_EMAIL.bounce_reason = TMP.bounce_reason
;

UPDATE A_CRM_INTERFACE_EMAIL INNER JOIN
      (
            SELECT
              `Interface-ID`,
              Alias,
              `UniqueKey` as email,
              min( date(`Timestamp` ) ) as date,
              count(*) as OpenCount
             FROM interface_openings
             GROUP BY `Interface-ID`, UniqueKey
       )  AS TMP
     USING ( `Interface-ID`, email )
SET
    A_CRM_INTERFACE_EMAIL.dateOpen   = TMP.date ,
    A_CRM_INTERFACE_EMAIL.OpenCount  = TMP.OpenCount,
    A_CRM_INTERFACE_EMAIL.`open` = 1
;

UPDATE A_CRM_INTERFACE_EMAIL INNER JOIN
      (
            SELECT
              `Interface-ID`,
              Alias,
              `UniqueKey` as email,
              min( date(`Timestamp` ) ) as date,
              `Link-URL`,
              count(*) AS ClicksCount
             FROM interface_clicks
             GROUP BY `Interface-ID`, UniqueKey
       )  AS TMP
     USING ( `Interface-ID`, email )
SET

    A_CRM_INTERFACE_EMAIL.dateClick   = TMP.date ,
    A_CRM_INTERFACE_EMAIL.ClickCount = TMP.ClicksCount,
    A_CRM_INTERFACE_EMAIL.click = 1,
    A_CRM_INTERFACE_EMAIL.`Link-URL` = TMP.`Link-URL`
;

/*
update             A_CRM_INTERFACE_EMAIL AS CMR
       inner JOIN  interface_openings   AS a
               on a.UniqueKey=CMR.email
set
   CMR.subscribe   = IF( Alias = 'Subscribe Newsletter', date(`Timestamp`), subscribe),
   CMR.unsubscribe = IF( Alias = 'desuscribirse', date(`Timestamp`), unsubscribe)
WHERE
   Alias IN (  'Subscribe Newsletter' , 'desuscribirse' )
;
*/

DROP  TABLE IF EXISTS TMP_MSG;
CREATE TABLE TMP_MSG ( INDEX  ( `Interface-ID`, email ) )
SELECT
  `Profil-ID`,
  `Interface-ID`,
  Alias,
  `UniqueKey` as email,
  count(*) as MessageCount,
  MIN( date( Timestamp ) ) as date

FROM interface_messages
GROUP BY `Interface-ID`, UniqueKey
;

UPDATE A_CRM_INTERFACE_EMAIL INNER JOIN
       TMP_MSG  AS TMP
     USING ( `Interface-ID`, email )
SET

    A_CRM_INTERFACE_EMAIL.`Message-Timestamp`  = TMP.date ,
    A_CRM_INTERFACE_EMAIL.MessageCount  = TMP.MessageCount,
    A_CRM_INTERFACE_EMAIL.Message = 1
;
DROP  TABLE IF EXISTS TMP_MSG;


INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  step,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Venezuela',
  'CRM.A_CRM_INTERFACE_EMAIL',
  'finish',
  now(),
  now(),
  count(*),
  count(*)
FROM
  A_CRM_INTERFACE_EMAIL;

truncate interface_bounces_sample;
truncate interface_clicks_sample;  
truncate interface_messages_sample   ;
truncate interface_openings_sample;   
