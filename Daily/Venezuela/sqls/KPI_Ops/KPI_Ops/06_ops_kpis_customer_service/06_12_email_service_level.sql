/*-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 06_12_email_service_level
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'6. Customer Service' AS 'group',
	0612 AS id,
	DATE_FORMAT(Fecha_creacion,'%Y%m') AS MonthNum,
	@operations@.week_iso(Fecha_creacion) AS WeekNum,
	Fecha_creacion as date,
	'% Service Level - Email answer within 4 working hours' AS kpi,
	'# Touchpoints answered on target / # Touchpoints answered' AS formula,
	DATE_FORMAT(Created_at,'%H') AS breakdown_1,
	Assignee AS breakdown_2,
	count(1) AS items,
	sum(4_horas_de_respuesta) AS items_sec,
	sum(4_horas_de_respuesta) /count(1)AS value,
	now() AS updated_at
FROM customer_service.tbl_zendesk_general
WHERE Pais=UPPER('@v_countryPrefix@') 
AND es_inc_front_office=1
AND DATE(Fecha_creacion) BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY DATE(Fecha_creacion), DATE_FORMAT(Created_at,'%a'), Assignee
;*/