/*-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 06_05_average_handling_time
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'6. Customer Service' AS 'group',
	0605 AS id,
	DATE_FORMAT(event_date,'%Y%m') AS MonthNum,
	@operations@.week_iso(event_date) AS WeekNum,
	event_date AS date,
	'Average Handling Time' AS kpi,
	'Total time to resolve calls / # calls received' AS formula,
	DATE_FORMAT(event_date,'%a') AS breakdown_1,
	event_hour AS breakdown_2,
	(SUM(net_event)*60) AS items,
	SUM(call_duration_seg) AS items_sec,
	SUM(call_duration_seg)/(SUM(net_event)*60)  AS value,
	now() AS updated_at
FROM customer_service.bi_ops_cc_@v_countryPrefix@
WHERE pos_answ=1 
AND event_date BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY event_date, DATE_FORMAT(event_date,'%a'), event_hour;*/