/*-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 06_01_touchpoints_over_gross_orders
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

-- -- -- -- Touchpoints Table -- -- -- -- 

DROP TEMPORARY TABLE IF EXISTS @operations@.tmp_contacts;
CREATE TEMPORARY TABLE @operations@.tmp_contacts
AS
SELECT 
	Date,
	channel,
	agent_name,
	SUM(Contact)as Contact
FROM
(SELECT
	'Phone' AS channel,
	agent_name,
	event_date as Date,
	COUNT(1) as Contact
FROM
 customer_service.bi_ops_cc_@v_countryPrefix@
WHERE
 event_date >= '2014-01-01'
AND 
 pos_answ = 1
GROUP BY event_date, agent_name
UNION ALL
SELECT 
	'Facebook' AS channel,
	'' AS agent_name,
	Fecha as Date,
	`comment` as Contact 
FROM customer_service.tbl_facebook_comments 
GROUP BY Fecha, agent_name
UNION ALL
SELECT 
	'Chat' AS channel,
	operator_nickname AS agent_name,
	date(chat_start_time) as Date,
	COUNT(*) as Contact 
FROM customer_service.tbl_olark_chat 
GROUP BY date(chat_start_time), operator_nickname
UNION ALL
SELECT 
	'Email' AS channel,
	Assignee AS agent_name,
	Fecha_creacion as Date,
	COUNT(*)as Contact 
FROM customer_service.tbl_zendesk_general 
WHERE es_inc_front_office=1 
AND Pais = UPPER('@v_countryPrefix@')
AND solved=1 
GROUP BY Fecha_creacion, Assignee
 )a
GROUP BY Date, channel, agent_name;

-- -- -- -- Orders Table -- -- -- -- 

DROP TEMPORARY TABLE IF EXISTS @operations@.tmp_gross_orders;
CREATE TEMPORARY TABLE @operations@.tmp_gross_orders (INDEX(Date))
SELECT
	Date,
	COUNT(DISTINCT OrderNum) AS orders
FROM @development@.A_Master
WHERE OrderBeforeCan = 1
GROUP BY Date
ORDER BY Date DESC;

-- -- -- --  KPI -- -- -- -- 

SET @interval = @v_interval@;

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'6. Customer Service' AS 'group',
	0601 AS id,
	DATE_FORMAT(a.date,'%Y%m') AS MonthNum,
	@operations@.week_iso(a.date) AS WeekNum,
	a.date AS date,
	'Touchpoints over Gross Orders' AS kpi,
	'# Touchpoints / Gross Orders' AS formula,
	channel AS breakdown_1,
	agent_name AS breakdown_2,
	AVG(b.orders) AS items,
	SUM(a.contact) AS items_sec,
	SUM(a.contact)/AVG(b.orders)  AS value,
	now() AS updated_at
FROM @operations@.tmp_contacts a
	INNER JOIN @operations@.tmp_gross_orders b
ON a.date = b.date
WHERE a.date BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY a.date, channel, agent_name;*/