/*-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 06_06_abandone_rate_operative_schedule_phone
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'6. Customer Service' AS 'group',
	0606 AS id,
	DATE_FORMAT(event_date,'%Y%m') AS MonthNum,
	@operations@.week_iso(event_date) AS WeekNum,
	event_date AS date,
	'% Abandone Rate in Operating Schedule - Phone' AS kpi,
	'# Calls & Chats abandoned / # Calls & Chats received' AS formula,
	'' AS breakdown_1,
	'' AS breakdown_2,	
	SUM(IF(net_event = 1, 1, 0)) AS items,
	SUM(IF(unanswered_in_wh = 1, 1, 0)) AS items_sec,
	SUM(IF(unanswered_in_wh = 1, 1, 0)) /SUM(IF(net_event = 1, 1, 0)) AS value,
	now() AS updated_at
FROM customer_service.bi_ops_cc_@v_countryPrefix@
WHERE pos=1 
AND 	event_date BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY event_date
ORDER BY event_date DESC;
*/