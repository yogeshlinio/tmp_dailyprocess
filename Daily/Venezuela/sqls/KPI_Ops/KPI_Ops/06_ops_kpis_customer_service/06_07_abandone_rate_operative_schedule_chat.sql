/*-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 06_07_abandone_rate_operative_schedule_chat
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	'MX' AS country,
	'6. Customer Service' AS 'group',
	0607 AS id,
	DATE_FORMAT(chat_start_time,'%Y%m') AS MonthNum,
	@operations@.week_iso(chat_start_time) AS WeekNum,
	date(chat_start_time) AS date,
	'% Abandone Rate in Operating Schedule - Chat' AS kpi,
	'# Calls & Chats abandoned / # Calls & Chats received' AS formula,
	'' AS breakdown_1,
	'' AS breakdown_2,
	count(chat_start_time) AS items,
	sum(IF(operator_first_response_delay ='None' AND visitor_messages_sent>0,1,0)) AS items_sec,
	sum(IF(operator_first_response_delay ='None' AND visitor_messages_sent>0,1,0))
        /count(chat_start_time) AS value,
	now() AS updated_at
FROM customer_service.tbl_olark_chat
WHERE  visitor_nickname not like 'Mexico (%' 
AND visitor_nickname not like 'Mexico #%' 
AND visitor_nickname not like 'unknown #%'
AND visitor_nickname not like 'USA (%'
AND visitor_nickname not like 'USA #%'
AND visitor_nickname not like 'Spain (%'
AND visitor_nickname not like 'Colombia (%'
AND visitor_nickname not like 'Panama (%'
AND visitor_nickname not like 'Chile (%'
AND visitor_nickname not like 'Peru (%'
AND visitor_nickname not like 'Peru #%'
AND visitor_location like  'Mexico%' 
AND HOUR(chat_start_time) in (08,09,10,11,12,13,14,15,16,17,18,19,20,21,22)
AND date(chat_start_time) BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY date(chat_start_time)
ORDER BY date(chat_start_time) DESC
;*/