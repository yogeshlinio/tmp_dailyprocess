/*-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 06_10_phone_service_level
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'6. Customer Service' AS 'group',
	0610 AS id,
	DATE_FORMAT(event_date,'%Y%m') AS MonthNum,
	@operations@.week_iso(event_date) AS WeekNum,
	event_date as date,
	'% Service Level - Phone answer within 20sec' AS kpi,
	'# Touchpoints answered on target / # Touchpoints answered' AS formula,
	DATE_FORMAT(event_date,'%a')  AS breakdown_1,
	event_hour AS breakdown_2,
	sum(net_event) AS items,
	sum(answered_in_wh_20) AS items_sec,
	sum(answered_in_wh_20) / sum(net_event) AS value,
	now() AS updated_at
FROM customer_service.bi_ops_cc_@v_countryPrefix@
WHERE pos=1
AND event_date BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY event_date,DATE_FORMAT(event_date,'%a'), event_hour;
*/