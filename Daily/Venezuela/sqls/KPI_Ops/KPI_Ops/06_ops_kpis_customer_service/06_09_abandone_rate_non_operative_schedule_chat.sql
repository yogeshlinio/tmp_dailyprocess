/*-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 06_09_abandone_rate_non_operative_schedule_chat
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'6. Customer Service' AS 'group',
	0609 AS id,
	DATE_FORMAT(chat_start_time,'%Y%m') AS MonthNum,
	@operations@.week_iso(chat_start_time) AS WeekNum,
	date(chat_start_time) AS date,
	'% Abandone Rate in Non Operating Schedule - chat' AS kpi,
	'# Calls & Chats abandoned / # Calls & Chats received' AS formula,
	'' AS breakdown_1,
	'' AS breakdown_2,
	count(1) AS items,
	sum(if(HOUR(chat_start_time)not in (08,09,10,11,12,13,14,15,16,17,18,19,20,21,22) 
	AND WEEKDAY(chat_start_time) in (0,1,2,3,4),1,if(HOUR(chat_start_time)not in 
	(09,10,11,12,13,14,15,16,17,18,19) 
	AND WEEKDAY(chat_start_time)=5,1,if(HOUR(chat_start_time)not in 
	(10,11,12,13,14,15,16,17,18) 
	AND WEEKDAY(chat_start_time)=6,1,0)))) AS items_sec,
	sum(if(HOUR(chat_start_time)not in (08,09,10,11,12,13,14,15,16,17,18,19,20,21,22) 
	AND WEEKDAY(chat_start_time) in (0,1,2,3,4),1,if(HOUR(chat_start_time)not in 
	(09,10,11,12,13,14,15,16,17,18,19) 
	AND WEEKDAY(chat_start_time)=5,1,if(HOUR(chat_start_time)not in 
	(10,11,12,13,14,15,16,17,18) 
	AND WEEKDAY(chat_start_time)=6,1,0))))/count(1) AS value,
	now() AS updated_at
FROM customer_service.tbl_olark_chat
WHERE  visitor_nickname not like 'Mexico (%' 
AND visitor_nickname not like 'Mexico #%' AND visitor_nickname not like 'unknown #%'
AND visitor_nickname not like 'USA (%'
AND visitor_nickname not like 'USA #%'
AND visitor_nickname not like 'Spain (%'
AND visitor_nickname not like 'Colombia (%'
AND visitor_nickname not like 'Panama (%'
AND visitor_nickname not like 'Chile (%'
AND visitor_nickname not like 'Peru (%'
AND visitor_nickname not like 'Peru #%'
AND visitor_location like  'Mexico%' 
AND date(chat_start_time) BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY date(chat_start_time)
ORDER BY date(chat_start_time) DESC
;*/