/*-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 06_03_adherence
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'6. Customer Service' AS 'group',
	0603 AS id,
	DATE_FORMAT(a.Fecha,'%Y%m') AS MonthNum,
	@operations@.week_iso(a.Fecha) AS WeekNum,
	a.Fecha AS date,
	'% Adherence' AS kpi,
	'Hours Available / Hours Scheduled' AS formula,
	WEEKDAY(a.Fecha) AS breakdown_1,
	Agente AS breakdown_2,
	(SUM(HorasConexion)-sum(HorasACW)-SUM(HorasPausas)) AS items,
	sum(HorasProgramadas) AS items_sec,
	(SUM(HorasConexion)-sum(HorasACW)-SUM(HorasPausas))/sum(HorasProgramadas) AS value,
	now() AS updated_at
FROM customer_service.rep_llamadas_agt_regional_day a
WHERE a.Fecha BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY Fecha, WEEKDAY(a.Fecha), Agente;*/