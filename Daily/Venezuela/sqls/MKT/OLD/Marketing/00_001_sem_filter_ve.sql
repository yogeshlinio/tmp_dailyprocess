
delete from SEM.sem_filter_campaign_ad_group_ve;

insert into SEM.sem_filter_campaign_ad_group_ve select 'SEM', x.date, x.campaign, x.source, x.medium, x.ad_group, x.impressions, x.clicks, x.visits, x.ad_cost, x.bounce, x.cart from SEM.campaign_ad_group_ve x where source='google' and medium='CPC' and campaign not like '%r.%' and campaign not like '%d.%' and campaign not like '%w.%' and campaign not like '%[D%' and campaign not like '%brandb.%';

insert into SEM.sem_filter_campaign_ad_group_ve select 'GDN', x.date, x.campaign, x.source, x.medium, x.ad_group, x.impressions, x.clicks, x.visits, x.ad_cost, x.bounce, x.cart from SEM.campaign_ad_group_ve x where source='google' and medium='cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%');

delete from SEM.sem_filter_transaction_id_ve;

insert into SEM.sem_filter_transaction_id_ve select 'SEM', x.* from SEM.transaction_id_ve x where source='google' and medium='CPC' and campaign not like '%r.%' and campaign not like '%d.%' and campaign not like '%w.%' and campaign not like '%[D%' and campaign not like '%brandb.%';

insert into SEM.sem_filter_transaction_id_ve select 'GDN', x.* from SEM.transaction_id_ve x where source='google' and medium='cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%');
