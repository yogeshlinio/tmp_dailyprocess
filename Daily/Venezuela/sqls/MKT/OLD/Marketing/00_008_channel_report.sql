
update production_ve.tbl_order_detail t inner join SEM.transaction_id_ve c on t.order_nr=c.transaction_id set t.source_medium= concat(c.source,' / ',c.medium),t.campaign=c.campaign where source_medium is null;

#Channel Report---VENEZUELA

#Define channel---
##UPDATE tbl_order_detail SET source='',channel='', channel_group='';
#Voucher is priority statemnt
#If voucher is empty use GA source
UPDATE tbl_order_detail SET source='' where source is null;
UPDATE tbl_order_detail SET campaign='' where campaign is null;
UPDATE tbl_order_detail SET source=source_medium;
UPDATE tbl_order_detail SET source=coupon_code where source_medium is null;
UPDATE tbl_order_detail SET source=source_medium where channel_group='Non Identified';

#NEW REGISTERS
UPDATE tbl_order_detail SET channel='New Register' 
WHERE (source like 'NL%' OR source like 'NR%' OR source like
'COM%' OR source like 'BNR%') AND source_medium is null;
UPDATE tbl_order_detail SET source=source_medium
WHERE (coupon_code like 'NL%' OR coupon_code='SINIVA' OR coupon_code like 'COM%' 
OR coupon_code like 'NR%' OR coupon_code like 'BNR%')
 AND source_medium is not null;
#BLOG
-- UPDATE tbl_order_detail SET channel='Blog Linio' WHERE source like 'blog%' 
-- OR source='blog.linio.com.ve / referral' or source like 'BL%';

#REFERRAL
UPDATE tbl_order_detail SET channel='Referral Sites' WHERE source like '%referral%' 
AND source<>'facebook.com / referral'
AND source<>'m.facebook.com / referral'
AND source<>'linio.com.co / referral'
AND source<>'linio.com / referral'
AND source<>'linio.com.mx / referral'
AND source<>'google.co.ve / referral'
AND source<>'linio.com.ve / referral'
AND source<>'linio.com.pe / referral'
AND source<>'www-staging.linio.com.ve / referral'
AND source<>'google.com / referral'
AND source<>'.com.ve / referral'
AND source<>'blog.linio.com.ve / referral' AND source<>'t.co / referral'
AND source not like '%google%' AND source not like '%blog%';

UPDATE tbl_order_detail SET channel='Other Affiliates' where source like '%affiliate%' or source like '%afiliate%';

UPDATE tbl_order_detail set channel='SoloCPM' where source like '%solocpm%';

UPDATE tbl_order_detail set channel='PromoDescuentos' where source like '%promodescuentos%';

UPDATE tbl_order_detail SET channel='Buscape' WHERE source='Buscape / Price Comparison';
#FB ADS
UPDATE tbl_order_detail SET channel='Facebook Ads' WHERE source='facebook / socialmediaads' or source='facebook / socialmediapaid' or source='facebook / (not set)';

UPDATE tbl_order_detail set channel= 'Dark Post' where campaign like '%darkpost%';

#SEM - GDN
UPDATE tbl_order_detail SET channel='Bing' where source like '%bing%';
UPDATE tbl_order_detail SET channel='SEM Branded' WHERE (source='google / cpc' or source like '%bing%') AND campaign like '%linio%';
UPDATE tbl_order_detail SET channel='SEM' WHERE source='google / cpc' AND campaign not like '%linio%';
UPDATE tbl_order_detail SET channel='Other Branded' WHERE source = '201.151.86.164' or source ='10.0.0.9' or source = 'www-staging.linio.com.mx' or source='blog.linio.com.mx' or source='213.229.186.15';


#GDN
UPDATE tbl_order_detail SET channel='Google Display Network' WHERE source='google / cpc' AND 
(campaign like 'r.%' or campaign like '[D%' or campaign like '%d.%');

#RETARGETING
UPDATE tbl_order_detail set channel='Other Retargeting' where source like '%retargeting%';
UPDATE tbl_order_detail SET channel='Sociomantic' WHERE source='sociomantic / (not set)' OR source='sociomantic / retargeting' or source='facebook / retargeting';
UPDATE tbl_order_detail SET channel='Vizury' WHERE source='vizury / retargeting' OR source='vizury.com / referral';
UPDATE tbl_order_detail SET channel='VEInteractive' WHERE source like '%VEInteractive%';
UPDATE tbl_order_detail SET channel='Main ADV' where source in ('retargeting / mainadv', 'mainadv / retargeting') or source like '%solocpm%';
UPDATE tbl_order_detail SET channel='AdRoll' where source like '%adroll%';

#RETARGETING
#UPDATE tbl_order_detail SET channel='Facebook R.' WHERE source='facebook / retargeting';
#Criteo
UPDATE tbl_order_detail set channel='Criteo' where source like '%criteo%';
#PAMPA
UPDATE tbl_order_detail set channel='Pampa Network' where source like '%pampa%';
#SOCIAL MEDIA
UPDATE tbl_order_detail SET channel='Facebook Referral' WHERE 
(source='facebook.com / referral' OR source='m.facebook.com / referral') AND source not like '%socialmediaads%';
UPDATE tbl_order_detail SET channel='Twitter Referral' WHERE source='t.co / referral' OR source='twitter / socialmedia';
UPDATE tbl_order_detail SET channel='FB Posts' 
WHERE source='facebook / socialmedia' OR source='SocialMedia / FacebookVoucher' or source='twitter / socialmedia';
#SERVICIO AL CLIENTE
UPDATE tbl_order_detail set channel='Other Tele Sales' where source = 'CS /%' or source = '%/ CS' or source like 'telesales /%' or source like '%/ telesales'; 
UPDATE tbl_order_detail SET channel='Inbound' WHERE source like 'Tele%' or source like '%inbound%' or source like '%outbound%';
UPDATE tbl_order_detail SET channel='OutBound' WHERE source like 'REC%';
UPDATE tbl_order_detail SET channel='CS Generic Voucher' WHERE source like 'CCemp%';

UPDATE tbl_order_detail SET channel='Search Organic' WHERE source like '%organic' OR source='google.com.co / referral';

UPDATE tbl_order_detail SET channel='Search Organic' where source like 'google.%' and source like '%referral';

UPDATE tbl_order_detail SET channel='Yahoo Answers' where source like '%answers.yahoo.com%';

#NEWSLETTER
UPDATE tbl_order_detail set Channel='Newsletter' WHERE source like '%CRM' AND source<>'TeleSales / CRM' or source like '%email%';
UPDATE tbl_order_detail SET channel='MKT Bs. 50' WHERE source='MKT02kVLM';
UPDATE tbl_order_detail SET channel='MKT 15%' WHERE source='MKT048S8J';
UPDATE tbl_order_detail SET channel='MKT Bs. 50' WHERE source='MKT04GPXB';
UPDATE tbl_order_detail SET channel='MKT 15% hasta 31 de diciembre' WHERE source='MKT06Ru';
UPDATE tbl_order_detail SET channel='MKT Bs. 50' WHERE source='MKT07kiKz';
UPDATE tbl_order_detail SET channel='MKT 15%' WHERE source='MKT0a3YVI';
UPDATE tbl_order_detail SET channel='MKT 50Bs  hasta 31 de diciembre' WHERE source='MKT0FTh';
UPDATE tbl_order_detail SET channel='Tramontina Utilita Sacarcorcho' WHERE source='MKT0hAb7W';
UPDATE tbl_order_detail SET channel='Victorinox Memoria USB Slim 4GB Verde' WHERE source='MKT0iuXXU';
UPDATE tbl_order_detail SET channel='HP FlashDrive Pen 4GB Usb V195B Azul' WHERE source='MKT0j2Efo';
UPDATE tbl_order_detail SET channel='GBTech Audífono GBTech X5 GBX5 Amarillo' WHERE source='MKT0oShGI';
UPDATE tbl_order_detail SET channel='Perfect Choice Audifono Diadema PC-110309 Con Banda Para Cuello' WHERE source='MKT0P2Bsq';
UPDATE tbl_order_detail SET channel='MKT Bs. 50' WHERE source='MKT0qCcAP';
UPDATE tbl_order_detail SET channel='Decocar Combo Tropical Azul' WHERE source='MKT0rjeet';
UPDATE tbl_order_detail SET channel='MKT 15%' WHERE source='MKT0WFMCm';
UPDATE tbl_order_detail SET channel='Ocean Vaso Corto Studio Set 6' WHERE source='MKT0yj5mw';
UPDATE tbl_order_detail SET channel='MKT Bs. 50' WHERE source='MKT0Z8YYn';
UPDATE tbl_order_detail SET channel='X-mini Altavoz Capsule v1.1 Rojo' WHERE source='MKT10hQfI';
UPDATE tbl_order_detail SET channel='HP P-FD4GBHP165-GEL Pendrive USB' WHERE source='MKT11vw0x';
UPDATE tbl_order_detail SET channel='SABA Set de 2 Cuchillos Colores' WHERE source='MKT12jfHL';
UPDATE tbl_order_detail SET channel='MKT Bs. 50' WHERE source='MKT14zmPN';
UPDATE tbl_order_detail SET channel='Kingston DT101G2/4GB PenDrive USB' WHERE source='MKT16mWW7';
UPDATE tbl_order_detail SET channel='Ifrogz Audífono EarPollution Plugz Azul Cielo' WHERE source='MKT18qw1j';
UPDATE tbl_order_detail SET channel='MKT 30%' WHERE source='MKT1DcqIV';
UPDATE tbl_order_detail SET channel='GENIUS Speakers 31731006103 Blanco' WHERE source='MKT1Hn1B5';
UPDATE tbl_order_detail SET channel='Bajo Techo Pizarra Autoadhesiva Tipo Vinil' WHERE source='MKT1HWmFm';
UPDATE tbl_order_detail SET channel='MKT 15%' WHERE source='MKT1i3zA8';
UPDATE tbl_order_detail SET channel='MKT Bs. 50' WHERE source='MKT1ktP10';
UPDATE tbl_order_detail SET channel='Desarmador trompo con matraca con 12 puntas' WHERE source='MKT1ORfvU';
UPDATE tbl_order_detail SET channel='SABA – Taza de Porcelana Rayas (Facebook)' WHERE source='MKT1P4NbB';
UPDATE tbl_order_detail SET channel='MKT 15%' WHERE source='MKT1rTCoJ';
UPDATE tbl_order_detail SET channel='MKT Bs. 50' WHERE source='MKT1xLsRR';
UPDATE tbl_order_detail SET channel='Botella de Alumino para Agua No soy de plástico Gaiam-Plata' WHERE source='MKT1Z54Lh';
UPDATE tbl_order_detail SET channel='MKT 15% hasta 31 de diciembre' WHERE source='MKT1ZtV';
UPDATE tbl_order_detail SET channel='SABA – Taza de Porcelana Rayas ' WHERE source='MKT2ww6wJ';
UPDATE tbl_order_detail SET channel='SABA – Taza de Porcelana Rayas ' WHERE source='MKT7XzxhT';
UPDATE tbl_order_detail SET channel='Cupón de reemplazo a cupón de Mkt' WHERE source='MKTAeF';
UPDATE tbl_order_detail SET channel='MKT 15%' WHERE source='MKTbS3gmp';
UPDATE tbl_order_detail SET channel='MKT 15%' WHERE source='MKTcwB9vv';
UPDATE tbl_order_detail SET channel='SABA Porta Botellas Neopreno Negro' WHERE source='MKTdtlbpI';
UPDATE tbl_order_detail SET channel='ILUV Audífonos in ear Bubble Gum Rojo IEP205-R' WHERE source='MKTECOjZv';
UPDATE tbl_order_detail SET channel='Reemplazo cupón de Marketing' WHERE source='MKTenQ';
UPDATE tbl_order_detail SET channel='Tostador Proctor Silex 24605Y 4 Rebanadas-Blanco' WHERE source='MKTf4A6Sk';
UPDATE tbl_order_detail SET channel='HP P-FD4GBHP165-GEL Pendrive USB' WHERE source='MKTf8VV4U';
UPDATE tbl_order_detail SET channel='Morrocoy Silla de Director' WHERE source='MKTGOhXfE';
UPDATE tbl_order_detail SET channel='Kalosh Almohada 95% Pluma y 5% Plumón Blanca' WHERE source='MKTkZ047R';
UPDATE tbl_order_detail SET channel='MKT Bs. 50' WHERE source='MKTLf9Mdg';
UPDATE tbl_order_detail SET channel='Kingston Tarjeta de memoria SDC4/4GB-1ADP' WHERE source='MKTljJ3QL';
UPDATE tbl_order_detail SET channel='PRO CHEF Parrillera Hamburguer' WHERE source='MKTmCN60a';
UPDATE tbl_order_detail SET channel='Credito por bono no funcionando 2' WHERE source='MKTMKT';
UPDATE tbl_order_detail SET channel='Credito por bono no funcionando 3' WHERE source='MKTMKT250';
UPDATE tbl_order_detail SET channel='Contigo West loop Vaso Térmico con autosello 16 Oz/ 473 ml Rojo' WHERE source='MKToVzJ1d';
UPDATE tbl_order_detail SET channel='MKT 50Bs  hasta 31 de diciembre' WHERE source='MKTQaQk';
UPDATE tbl_order_detail SET channel='MKT 15%' WHERE source='MKTr1jvkH';
UPDATE tbl_order_detail SET channel='ARGOM Teclado multimedia en español ARG-KB-7805 Negro' WHERE source='MKTr2DzaC';
UPDATE tbl_order_detail SET channel='GBTech MP4 GBTech GB1106 Azul 4 GB' WHERE source='MKTRp7vye';
UPDATE tbl_order_detail SET channel='SABA Set de 3 utensilios para Parrilla' WHERE source='MKTsd9V9B';
UPDATE tbl_order_detail SET channel='Decocar Cava Americana 11,4Lt Roja' WHERE source='MKTthEw98';
UPDATE tbl_order_detail SET channel='MKT Bs. 50' WHERE source='MKTuWtOQ3';
UPDATE tbl_order_detail SET channel='Marketing en ferias' WHERE source='MKTVngFxL';
UPDATE tbl_order_detail SET channel='SABA - Dispensador de Aceite y Vinagre 2 en 1 / SABA-511 / Transparente' WHERE source='MKTvpeCte';
UPDATE tbl_order_detail SET channel='MKT 30%' WHERE source='MKTy6v5rt';
UPDATE tbl_order_detail SET channel='Easy Line Mouse Retráctil EL-993346 Negro' WHERE source='MKTzBMwc3';
UPDATE tbl_order_detail SET channel='Reactivation campaign' WHERE source='MKTzOcbUq';



#OTHER
UPDATE tbl_order_detail SET channel='Exchange' WHERE source like 'CCE%';
UPDATE tbl_order_detail SET channel='Content Mistake' WHERE source like 'CON%';
UPDATE tbl_order_detail SET channel='Procurement Mistake' WHERE source like 'PRC%';
UPDATE tbl_order_detail SET channel='Employee Vouchers' 
WHERE (source like 'TE%' AND source<>'TeleSales / CRM') OR source like 'EA%';


#MERCADO LIBRE
UPDATE tbl_order_detail SET channel='Mercado Libre Voucher' WHERE source like 'ML%';

#PERIODISTAS

UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT02kER0';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT08jq2i';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT0arRZF';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT0hoq3F';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT0JgIhn';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT0LeO2h';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT0Olwjk';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT1d8pkJ';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT1fspLY';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT1h2uie';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT1jzjKP';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT1llem9';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT1Pk3Ap';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT1u1JSo';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT1v5kLS';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT1Yz6yi';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT4rXdCS';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTA0aIrT';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTac0eLL';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTCcHdRX';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKThMerN5';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKThNplZF';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTiE46NZ';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTJy4dxm';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTlzVdEY';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTmvE0F7';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTn7VdJO';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTna93IX';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTpYM3yV';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTQdcuzY';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTrI3ds7';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTS55mkn';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTSR8lJR';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTt1kyoi';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTTRutlc';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTuUrfbj';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTvkHUlg';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTvsJVH4';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTwdi2uO';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTXVRpth';





#PARTNERSHIPS
UPDATE tbl_order_detail SET channel='Other Partnerships' where source like '%PARTNERSHIP%' or coupon_code like '%PARTNERSHIP%';

#DIRECT
UPDATE tbl_order_detail SET channel='Linio.com Referral' WHERE  
source='linio.com.co / referral' OR
source='linio.com / referral' OR
source='linio.com.mx / referral' OR
source='linio.com.ve / referral' OR
source='linio.com.pe / referral' OR
source='www-staging.linio.com.ve / referral';
UPDATE tbl_order_detail SET channel='Directo / Typed' WHERE  source='(direct) / (none)';



#CORPORATE SALES
UPDATE tbl_order_detail SET channel='Corporate Sales' WHERE source like 'CDEAL%';

UPDATE tbl_order_detail set channel='Corporate Sales' where coupon_code LIKE 'CDEAL%' or source_medium like '%CORPORATESALES%';


#OFFLINE
UPDATE tbl_order_detail SET channel='SMS' where coupon_code in ('VIERNES10',
'NOCTURNO',
'USB01',
'ELECTROLINIO',
'TODOLINIO',
'LINIO',
'VIERNESLOCO');

UPDATE tbl_order_detail set channel='Valla' where coupon_code in ('GUAIRITA',
'PUENTE',
'HIERRO',
'TRINIDAD',
'BANDERA',
'BRISAS'
);

UPDATE tbl_order_detail set channel='Publitruck' where coupon_code = 'CARACAS'; 

UPDATE tbl_order_detail set channel='Paradas Bus' where coupon_code in ('LIBERTADOR',
'MERCEDES',
'CHACAITO',
'PALOS',
'BOLEITA',
'ALEGRE',
'CAFETAL',
'GALLEGOS',
'CAURIMARE',
'PAULA',
'INTERCOMUNAL',
'VALLE',
'CARICUAO',
'BOLIVAR',
'BELLO',
'MEDINA',
'MONTE',
'REDOMA',
'TAHONA',
'MANZANARES',
'ILUSTRES',
'MARTIN',
'ANTIMANO',
'PAEZ',
'CASANOVA'
);


#N/A
UPDATE tbl_order_detail SET channel='Unknown-No voucher' WHERE source='';
UPDATE tbl_order_detail SET channel='Unknown-Voucher' WHERE channel='' AND source<>'';

#CAC Vouchers
UPDATE tbl_order_detail set channel = 'CAC Deals' where (coupon_code like 'CAC%');
UPDATE tbl_order_detail set channel='FB Ads CAC' WHERE (coupon_code like 'CAC%') and source_medium='facebook / socialmediaads';
UPDATE tbl_order_detail set channel='FB CAC' WHERE (coupon_code like 'CAC%') and source_medium='facebook / socialmedia';
UPDATE tbl_order_detail set channel='NL CAC' WHERE coupon_code like 'CAC%' and source_medium='postal / crm';

#Channel Report Extra Stuff to define Channel---
#Venta corporativa 

#Curebit
UPDATE tbl_order_detail set channel = 'Curebit' where coupon_code like '%cbit%' or source_medium like '%curebit%';

UPDATE tbl_order_detail t inner join mobileapp_transaction_id m on t.order_nr=m.transaction_id set channel = 'Mobile App';

#Partnerships Channel Extra


#Mercado libre Orders 


#DEFINE CHANNEL GROUP

UPDATE tbl_order_detail SET channel_group='Mobile App' WHERE channel='Mobile App';

##UPDATE tbl_order_detail set channel_group ='Corporate Sales' where channel='Corporate Sales';

#Facebook Ads Group
UPDATE tbl_order_detail SET channel_group='Facebook Ads' WHERE channel='Facebook Ads' or channel = 'Dark Post' or channel='Facebook Referral';

#OTHER
UPDATE tbl_order_detail SET channel_group='Mercado Libre ' WHERE channel='Mercado Libre Voucher';

#SEM Group
UPDATE tbl_order_detail SET channel_group='Search Engine Marketing' WHERE channel='SEM' or channel='Bing' or channel='Other Branded';

#Social Media Group
UPDATE tbl_order_detail SET channel_group='Social Media' 
WHERE channel='Twitter Referral' OR
channel='FB Posts' or channel='Youtube';
UPDATE tbl_order_detail SET channel='Youtube' where source='youtube / socialmedia';

#Referal Platform
UPDATE tbl_order_detail set channel_group = 'Referal Platform' where channel='Curebit';

#Retargeting Group
UPDATE tbl_order_detail SET channel_group='Retargeting' WHERE channel='Sociomantic' OR channel like '%retargeting'
OR channel='Facebook R.' OR channel='Vizury' OR channel = 'VEInteractive' OR channel = 'GDN Retargeting' OR channel = 'Criteo' or channel='Main ADV' or channel='AdRoll' or channel='Other Retargeting';

#GDN Group
UPDATE tbl_order_detail SET channel_group='Google Display Network' WHERE channel like 'GDN' or channel='Yeei'; 

#NewsLetter Group
UPDATE tbl_order_detail SET channel_group='NewsLetter' WHERE channel='Newsletter';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 15% hasta 31 de diciembre';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 50Bs  hasta 31 de diciembre';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Tramontina Utilita Sacarcorcho';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Victorinox Memoria USB Slim 4GB Verde';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='HP FlashDrive Pen 4GB Usb V195B Azul';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='GBTech Audífono GBTech X5 GBX5 Amarillo';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Perfect Choice Audifono Diadema PC-110309 Con Banda Para Cuello';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Decocar Combo Tropical Azul';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Ocean Vaso Corto Studio Set 6';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='X-mini Altavoz Capsule v1.1 Rojo';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='HP P-FD4GBHP165-GEL Pendrive USB';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='SABA Set de 2 Cuchillos Colores';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Kingston DT101G2/4GB PenDrive USB';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Ifrogz Audífono EarPollution Plugz Azul Cielo';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 30%';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='GENIUS Speakers 31731006103 Blanco';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Bajo Techo Pizarra Autoadhesiva Tipo Vinil';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Desarmador trompo con matraca con 12 puntas';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='SABA – Taza de Porcelana Rayas (Facebook)';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Botella de Alumino para Agua No soy de plástico Gaiam-Plata';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 15% hasta 31 de diciembre';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='SABA – Taza de Porcelana Rayas ';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='SABA – Taza de Porcelana Rayas ';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Cupón de reemplazo a cupón de Mkt';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='SABA Porta Botellas Neopreno Negro';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='ILUV Audífonos in ear Bubble Gum Rojo IEP205-R';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Reemplazo cupón de Marketing';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Tostador Proctor Silex 24605Y 4 Rebanadas-Blanco';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='HP P-FD4GBHP165-GEL Pendrive USB';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Morrocoy Silla de Director';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Kalosh Almohada 95% Pluma y 5% Plumón Blanca';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Kingston Tarjeta de memoria SDC4/4GB-1ADP';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='PRO CHEF Parrillera Hamburguer';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Credito por bono no funcionando 2';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Credito por bono no funcionando 3';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Contigo West loop Vaso Térmico con autosello 16 Oz/ 473 ml Rojo';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 50Bs  hasta 31 de diciembre';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='ARGOM Teclado multimedia en español ARG-KB-7805 Negro';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='GBTech MP4 GBTech GB1106 Azul 4 GB';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='SABA Set de 3 utensilios para Parrilla';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Decocar Cava Americana 11,4Lt Roja';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Marketing en ferias';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='SABA - Dispensador de Aceite y Vinagre 2 en 1 / SABA-511 / Transparente';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 30%';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Easy Line Mouse Retráctil EL-993346 Negro';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Reactivation campaign';

#CAC Deals Group
UPDATE tbl_order_detail set channel_group='CAC Deals' where channel='CAC Deals' or channel='FB Ads CAC' or channel='FB CAC' or channel='NL CAC';

#SEO Group
UPDATE tbl_order_detail SET channel_group='Search Engine Optimization' WHERE channel='Search Organic' or channel='Yahoo Answers';

#Partnerships Group

#CORPORATE SALES

UPDATE tbl_order_detail SET channel_group='Other (identified)' WHERE channel='Corporate Sales';

#Corporate Sales Group
UPDATE tbl_order_detail SET channel_group='Other (identified)' WHERE channel='Corporate Sales';

#Offline Group
UPDATE tbl_order_detail SET channel_group='Offline Marketing' where channel='SMS' or channel='Buscape' or channel='Valla' or channel='Publitruck' or channel='Paradas Bus';

#Tele Sales Group
UPDATE tbl_order_detail SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales' OR channel='CS Generic Voucher' or channel='Other Tele Sales';

#Branded Group
UPDATE tbl_order_detail SET channel_group='Branded' 
WHERE channel='Linio.com Referral' OR channel='Directo / Typed' OR
channel='SEM Branded';

#ML Group
UPDATE tbl_order_detail SET channel_group='Mercado Libre' 
WHERE channel='Mercado Libre Referral' OR channel='Mercado Libre Voucher' 
OR channel='Otros - Mercado Libre';

#Blog Group
-- UPDATE tbl_order_detail SET channel_group='Blogs'
-- WHERE channel='Blog Linio';

#Affiliates Group
UPDATE tbl_order_detail SET channel_group='Affiliates' 
WHERE channel='Pampa Network' or channel='Other Affiliates' or channel='SoloCPM' or channel='PromoDescuentos';


#Other (identified) Group
UPDATE tbl_order_detail SET channel_group='Other (identified)' 
WHERE channel='Referral Sites' OR channel='Reactivation Letters' 
OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' OR
channel='Exchange' OR
channel='Content Mistake' OR
channel='Procurement Mistake' OR
channel='Employee Voucher' OR channel='Cupón para periodistas';

#Extra Channel Group
update tbl_order_detail set channel_group='Google Display Network' where source_medium = 'Noticias24 / banner5A';
update tbl_order_detail set channel_group='Google Display Network' where source_medium = 'Noticias24 / intersticial';
update tbl_order_detail set channel_group='Google Display Network' where source_medium = 'Noticias24 / Display';
update tbl_order_detail set channel_group='Other (identified)' where source_medium = 'googleads.g.doubleclick.net / referral';
update tbl_order_detail set channel_group='Google Display Network' where source_medium = 'LaPatilla / banner650';
update tbl_order_detail set channel_group='Tele Sales' where source_medium = 'CS / Inbound';
update tbl_order_detail set channel_group='Tele Sales' where source_medium = 'CS / Outbound';
update tbl_order_detail set channel_group='Google Display Network' where source_medium = 'LaPatilla / Display';
update tbl_order_detail set channel_group='Google Display Network' where source_medium = 'Noticias24 / banner';
update tbl_order_detail set channel_group='Other (identified)' where source_medium = 'google.co.ve / referral';

#Unknown Group
UPDATE tbl_order_detail SET channel_group='Non identified' 
WHERE channel='Unknown-No voucher' OR channel='Unknown-Voucher' or (channel=1 and channel_group='') or channel_group='';

#Define Channel Type---
#TO BE DONE

#Ownership

UPDATE tbl_order_detail set Ownership='' where channel_group='Social Media';
UPDATE tbl_order_detail set Ownership='' where channel_group='NewsLetter';
UPDATE tbl_order_detail set Ownership='' where channel_group='Search Engine Optimization';
UPDATE tbl_order_detail set Ownership='' where channel_group='Blogs';
UPDATE tbl_order_detail set Ownership='' where channel_group='Offline Marketing';
UPDATE tbl_order_detail set Ownership='' where channel_group='Partnerships';
UPDATE tbl_order_detail set Ownership='' where channel_group='Mercado Libre';
UPDATE tbl_order_detail set Ownership='' where channel_group='Tele Sales';

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Venezuela',  #Country
  'marketing_report.marketing_ve',
  NOW(),
  NOW(),
  1,
  1;
  
-- SELECT 'Start: channel_report_no_voucher ', now();
-- call channel_report_no_voucher();

-- SELECT 'Start: visits_costs_channel ', now();
-- call visits_costs_channel();

-- call daily_marketing();
-- call monthly_marketing();