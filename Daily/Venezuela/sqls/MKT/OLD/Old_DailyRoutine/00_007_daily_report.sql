
truncate production_ve.tbl_dailyReport;
insert into production_ve.tbl_dailyReport (date) select dt from production_ve.calendar where dt>'2012-07-31' and dt<now();

truncate production_ve.tbl_dailyReport;
insert into production_ve.tbl_dailyReport (date) select dt from production_ve.calendar where dt>'2012-07-31' and dt<now();

#gross
update production_ve.tbl_dailyReport join 
(select date,sum(unit_price) as grossrev, count(distinct order_nr,oac,returned,pending) as grossorders, 
	sum(unit_price_after_vat) as grosssalesprecancel
            from production_ve.tbl_order_detail where   (obc = '1')
            group by date) as tbl_gross on tbl_gross.date=tbl_dailyReport.date
set tbl_dailyReport.grossrev=tbl_gross.grossrev , tbl_dailyReport.grossorders=tbl_gross.grossorders, 
tbl_dailyReport.grosssalesprecancel=tbl_gross.grosssalesprecancel;

#cancel
update production_ve.tbl_dailyReport join 
(select date,sum(unit_price_after_vat) as cancelrev, count(distinct order_nr) as cancelorders
from production_ve.tbl_order_detail 
where   (cancel = '1')
group by date) as tbl_cancel on tbl_cancel.date=tbl_dailyReport.date
set tbl_dailyReport.cancellations=tbl_cancel.cancelrev , tbl_dailyReport.cancelorders=tbl_cancel.cancelorders;


#pending
update production_ve.tbl_dailyReport join 
(select date,sum(unit_price_after_vat) as pendingrev, count(distinct order_nr) as pendingorders
from production_ve.tbl_order_detail 
where   (pending = '1')
group by date) as tbl_pending on tbl_pending.date=tbl_dailyReport.date
set tbl_dailyReport.pendingcop=tbl_pending.pendingrev , tbl_dailyReport.pendingorders=tbl_pending.pendingorders;


#cogs_gross_post_cancel
update production_ve.tbl_dailyReport join 
(select date,(sum(costo_after_vat) + sum(delivery_cost_supplier)) as cogs_gross_post_cancel
from production_ve.tbl_order_detail 
where   (oac = '1')
group by date) as tbl_cogs_post_cancel on tbl_cogs_post_cancel.date=tbl_dailyReport.date
set tbl_dailyReport.cogs_gross_post_cancel=tbl_cogs_post_cancel.cogs_gross_post_cancel;

#pending
update production_ve.tbl_dailyReport join 
(select date,sum(unit_price_after_vat) as returnrev, count(distinct order_nr) as returnorders
from production_ve.tbl_order_detail 
where   (returned = '1')
group by date) as tbl_returned on tbl_returned.date=tbl_dailyReport.date
set tbl_dailyReport.returnedcop=tbl_returned.returnrev , tbl_dailyReport.returnedorders=tbl_returned.returnorders;

#net
update production_ve.tbl_dailyReport join 
(select date,sum(unit_price_after_vat) as netsalesprevoucher, sum(coupon_money_after_vat) as vouchermktcost,
sum(paid_price_after_vat) as netsales,count(distinct order_nr) as netorders,
 (sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
count(order_nr)/count(distinct order_nr) as averageitemsperbasket,
count(distinct custid) as customers,
sum(shipping_fee) as shippingfee, 
sum(shipping_cost)as shippingcost, 
sum(payment_cost) as paymentcost
from production_ve.tbl_order_detail 
where   (returned = '0') and (oac = '1')
group by date) as tbl_net on tbl_net.date=tbl_dailyReport.date
set tbl_dailyReport.netsalesprevoucher=tbl_net.netsalesprevoucher , tbl_dailyReport.vouchermktcost=tbl_net.vouchermktcost,
tbl_dailyReport.netsales=tbl_net.netsales,tbl_dailyReport.netorders=tbl_net.netorders,
tbl_dailyReport.cogs_on_net_sales=tbl_net.cogs_on_net_sales,tbl_dailyReport.averageitemsperbasket=tbl_net.averageitemsperbasket,
tbl_dailyReport.customers=tbl_net.customers,
tbl_dailyReport.shippingfee=tbl_net.shippingfee/1.12,
tbl_dailyReport.shippingcost=tbl_net.shippingcost,tbl_dailyReport.paymentcost=tbl_net.paymentcost;

 
#categorías
#electrodomésticos
update production_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production_ve.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 = 'electrodomésticos')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_appiliances=tbl_cat.netsales , tbl_dailyReport.cogs_appiliances=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_appliances=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_appliances=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_appliances=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_appliances=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_appliances=tbl_cat.payment_fee;

#cámaras y fotografía
update production_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production_ve.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 = 'cámaras y fotografía')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_photography=tbl_cat.netsales , tbl_dailyReport.cogs_photography=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_photography=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_photography=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_photography=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_photography=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_photography=tbl_cat.payment_fee;


#tv, video y audio
update production_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production_ve.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 ='tv, audio y video')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_tv_audio_video=tbl_cat.netsales , tbl_dailyReport.cogs_tv_audio_video=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_tv_audio_video=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_tv_audio_video=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_tv_audio_video=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_tv_audio_video=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_tv_audio_video=tbl_cat.payment_fee;

#libros
update production_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production_ve.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 = 'libros')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_books=tbl_cat.netsales , tbl_dailyReport.cogs_books=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_books=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_books=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_books=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_books=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_books=tbl_cat.payment_fee;

#videojuegos
update production_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production_ve.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 = 'videojuegos')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_videogames=tbl_cat.netsales , tbl_dailyReport.cogs_videogames=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_videogames=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_videogames=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_videogames=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_videogames=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_videogames=tbl_cat.payment_fee;

#fashion
update production_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production_ve.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 = 'accesorios de moda' or n1 = 'ropa, calzado y accesorios')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_fashion=tbl_cat.netsales , tbl_dailyReport.cogs_fashion=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_fashion=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_fashion=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_fashion=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_fashion=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_fashion=tbl_cat.payment_fee;


#cuidado personal
update production_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production_ve.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 like '%cuidado personal%' or n1 = 'secadores')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_health_beauty=tbl_cat.netsales , tbl_dailyReport.cogs_health_beauty=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_health_beauty=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_health_beauty=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_health_beauty=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_health_beauty=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_health_beauty=tbl_cat.payment_fee;


#teléfonos y gps
update production_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production_ve.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 = 'celulares, teléfonos y gps' or n1 = 'ipad 100')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_cellphones=tbl_cat.netsales , tbl_dailyReport.cogs_cellphones=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_cellphones=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_cellphones=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_cellphones=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_cellphones=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_cellphones=tbl_cat.payment_fee;


 
#computadores y tablets    
update production_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production_ve.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 = 'computadoras y tabletas')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_computing=tbl_cat.netsales , tbl_dailyReport.cogs_computing=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_computing=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_computing=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_computing=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_computing=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_computing=tbl_cat.payment_fee;


#hogar y muebles   
update production_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production_ve.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 = 'línea blanca' or n1 = 'comerdor y cocina' or n1 = 'hogar')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_hogar=tbl_cat.netsales , tbl_dailyReport.cogs_hogar=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_hogar=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_hogar=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_hogar=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_hogar=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_hogar=tbl_cat.payment_fee;


#juguetes, niños y bebés 
update production_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 like 'juguetes%')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_kids_babies=tbl_cat.netsales , tbl_dailyReport.cogs_kids_babies=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_kids_babies=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_kids_babies=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_kids_babies=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_kids_babies=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_kids_babies=tbl_cat.payment_fee;

#Camping y Exteriores
update production_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 ='camping y exteriores')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_outdoor=tbl_cat.netsales , tbl_dailyReport.cogs_outdoor=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_outdoor=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_outdoor=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_outdoor=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_kids_outdoor=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_outdoor=tbl_cat.payment_fee;

#paidchannels
 update production_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as net_sales_paid_channel,
sum(coupon_money_after_vat) as voucher_paid_channel
from production_ve.tbl_order_detail 
where  (oac = '1') and (returned = '0') 
and   ((source_medium like '%price comparison%')
  or (source_medium like '%socialmediaads%')
  or (source_medium like '%cpc%')
  or (source_medium like '%sociomantic%'))
group by date) as tbl_paid on tbl_paid.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_paid_channel=tbl_paid.net_sales_paid_channel
 , tbl_dailyReport.voucher_paid_channel=tbl_paid.voucher_paid_channel;

# new customers
update production_ve.tbl_dailyReport join  
(select firstorder as date,count(*) as newcustomers from production_ve.view_cohort 
group by firstorder) as tbl_nc on tbl_nc.date=tbl_dailyReport.date
set tbl_dailyReport.newcustomers=tbl_nc.newcustomers;

