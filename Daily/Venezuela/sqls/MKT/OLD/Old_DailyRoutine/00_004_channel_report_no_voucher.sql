
select  'start channel_report_no_voucher: start',now();

#Channel Report---

#ga_cost_Campaign
select @last_date:=max(date) from ga_cost_campaign where source = 'vizury';

insert into ga_cost_campaign(date, source, medium, campaign, adCost)
SELECT date, "Vizury" as source, "Retargeting" as medium, "(not set)" as campaign, (sum(paid_price_after_vat) +sum(shipping_fee_after_vat) )*0.08 as cost
from tbl_order_detail
where source_medium like 'vizury /%' and obc = 1 and date > @last_date
group by date
order by date desc;

select @last_date:=max(date) from ga_cost_campaign where source like 'VEInteractive%';

insert into ga_cost_campaign(date, source, medium, campaign, adCost)
SELECT date, "VEInteractive" as source, "Retargeting" as medium, "(not set)" as campaign, (sum(paid_price_after_vat) +sum(shipping_fee_after_vat) )*0.085 as cost
from tbl_order_detail
where `source_medium` like 'VEInteractive%' and obc = 1 and date > @last_date
group by date
order by date desc;

update ga_visits_cost_source_medium set source_medium = concat( source, ' / ', medium) where source_medium is null;


#NEW REGISTERS
UPDATE ga_visits_cost_source_medium SET channel='New Register' 
WHERE (source_medium like 'NL%' OR source_medium like 'NR%' OR source_medium like
'COM%' OR source_medium like 'BNR%') AND source_medium is null;

UPDATE ga_visits_cost_source_medium SET channel='Blog Linio' WHERE source_medium like 'blog%' 
OR source_medium='blog.linio.com.ve / referral' or source_medium like 'BL%';

#REFERRAL
UPDATE ga_visits_cost_source_medium SET channel='Referral Sites' WHERE source_medium like '%referral' 
AND source_medium<>'facebook.com / referral'
AND source_medium<>'m.facebook.com / referral'
AND source_medium<>'linio.com.co / referral'
AND source_medium<>'linio.com / referral'
AND source_medium<>'linio.com.mx / referral'
AND source_medium<>'google.co.ve / referral'
AND source_medium<>'linio.com.ve / referral'
AND source_medium<>'linio.com.pe / referral'
AND source_medium<>'www-staging.linio.com.ve / referral'
AND source_medium<>'google.com / referral'
AND source_medium<>'.com.ve / referral'
AND source_medium<>'blog.linio.com.ve / referral' AND source_medium<>'t.co / referral'
AND source_medium not like '%google%';


UPDATE ga_visits_cost_source_medium SET channel='Buscape' WHERE source_medium='Buscape / Price Comparison';
#FB ADS
UPDATE ga_visits_cost_source_medium SET channel='Facebook Ads' WHERE source_medium='facebook / socialmediaads' or source_medium='facebook / socialmediapaid';
#SEM - GDN
UPDATE ga_visits_cost_source_medium SET channel='SEM Branded' WHERE source_medium='google / cpc'
 AND (campaign like 'brand%');
UPDATE ga_visits_cost_source_medium SET channel='SEM' WHERE source_medium='google / cpc' 
AND (campaign not like 'brand%');
UPDATE ga_visits_cost_source_medium SET channel='GDN' 
WHERE source_medium='google / cpc' AND campaign not like 'b.%' AND campaign like 'r.%';
UPDATE ga_visits_cost_source_medium SET channel='GDN' 
WHERE source like 'googleads.g.doubleclick.net%';
UPDATE ga_visits_cost_source_medium SET channel='GDN' 
WHERE source_medium='google / cpc' AND campaign like '[D[D%';

#RETARGETING
UPDATE ga_visits_cost_source_medium SET channel='Sociomantic' WHERE source_medium='sociomantic / (not set)' OR source_medium='sociomantic / retargeting';
UPDATE ga_visits_cost_source_medium SET channel='Vizury' WHERE source_medium='vizury / retargeting';
UPDATE ga_visits_cost_source_medium SET channel='VEInteractive' WHERE source like '%VEInteractive%';
UPDATE ga_visits_cost_source_medium SET channel='GDN Retargeting' 
WHERE source_medium='google / cpc' AND campaign like '[D[R%';
#RETARGETING
UPDATE ga_visits_cost_source_medium SET channel='Facebook R.' WHERE source_medium='facebook / retargeting';
#SOCIAL MEDIA
UPDATE ga_visits_cost_source_medium SET channel='Facebook Referral' WHERE 
(source_medium='facebook.com / referral' OR source_medium='m.facebook.com / referral') AND source_medium not like '%socialmediaads%';
UPDATE ga_visits_cost_source_medium SET channel='Twitter Referral' WHERE source_medium='t.co / referral' OR source_medium='twitter / socialmedia';
UPDATE ga_visits_cost_source_medium SET channel='FB Posts' 
WHERE source_medium='facebook / socialmedia' OR source_medium='SocialMedia / FacebookVoucher';
#SERVICIO AL CLIENTE
UPDATE ga_visits_cost_source_medium SET channel='Inbound' WHERE source_medium like 'Tele%';
UPDATE ga_visits_cost_source_medium SET channel='OutBound' WHERE source_medium like 'REC%';
UPDATE ga_visits_cost_source_medium SET channel='CS Generic Voucher' WHERE source_medium like 'CCemp%';

UPDATE ga_visits_cost_source_medium SET channel='Search Organic' WHERE source_medium like '%organic' OR source_medium like 'google.% / referral';
#NEWSLETTER
UPDATE ga_visits_cost_source_medium set Channel='Newsletter' WHERE source_medium like '%CRM' AND source_medium<>'TeleSales / CRM';
UPDATE ga_visits_cost_source_medium SET channel='MKT Bs. 50' WHERE source_medium='MKT02kVLM';
UPDATE ga_visits_cost_source_medium SET channel='MKT 15%' WHERE source_medium='MKT048S8J';
UPDATE ga_visits_cost_source_medium SET channel='MKT Bs. 50' WHERE source_medium='MKT04GPXB';
UPDATE ga_visits_cost_source_medium SET channel='MKT 15% hasta 31 de diciembre' WHERE source_medium='MKT06Ru';
UPDATE ga_visits_cost_source_medium SET channel='MKT Bs. 50' WHERE source_medium='MKT07kiKz';
UPDATE ga_visits_cost_source_medium SET channel='MKT 15%' WHERE source_medium='MKT0a3YVI';
UPDATE ga_visits_cost_source_medium SET channel='MKT 50Bs  hasta 31 de diciembre' WHERE source_medium='MKT0FTh';
UPDATE ga_visits_cost_source_medium SET channel='Tramontina Utilita Sacarcorcho' WHERE source_medium='MKT0hAb7W';
UPDATE ga_visits_cost_source_medium SET channel='Victorinox Memoria USB Slim 4GB Verde' WHERE source_medium='MKT0iuXXU';
UPDATE ga_visits_cost_source_medium SET channel='HP FlashDrive Pen 4GB Usb V195B Azul' WHERE source_medium='MKT0j2Efo';
UPDATE ga_visits_cost_source_medium SET channel='GBTech Audífono GBTech X5 GBX5 Amarillo' WHERE source_medium='MKT0oShGI';
UPDATE ga_visits_cost_source_medium SET channel='Perfect Choice Audifono Diadema PC-110309 Con Banda Para Cuello' WHERE source_medium='MKT0P2Bsq';
UPDATE ga_visits_cost_source_medium SET channel='MKT Bs. 50' WHERE source_medium='MKT0qCcAP';
UPDATE ga_visits_cost_source_medium SET channel='Decocar Combo Tropical Azul' WHERE source_medium='MKT0rjeet';
UPDATE ga_visits_cost_source_medium SET channel='MKT 15%' WHERE source_medium='MKT0WFMCm';
UPDATE ga_visits_cost_source_medium SET channel='Ocean Vaso Corto Studio Set 6' WHERE source_medium='MKT0yj5mw';
UPDATE ga_visits_cost_source_medium SET channel='MKT Bs. 50' WHERE source_medium='MKT0Z8YYn';
UPDATE ga_visits_cost_source_medium SET channel='X-mini Altavoz Capsule v1.1 Rojo' WHERE source_medium='MKT10hQfI';
UPDATE ga_visits_cost_source_medium SET channel='HP P-FD4GBHP165-GEL Pendrive USB' WHERE source_medium='MKT11vw0x';
UPDATE ga_visits_cost_source_medium SET channel='SABA Set de 2 Cuchillos Colores' WHERE source_medium='MKT12jfHL';
UPDATE ga_visits_cost_source_medium SET channel='MKT Bs. 50' WHERE source_medium='MKT14zmPN';
UPDATE ga_visits_cost_source_medium SET channel='Kingston DT101G2/4GB PenDrive USB' WHERE source_medium='MKT16mWW7';
UPDATE ga_visits_cost_source_medium SET channel='Ifrogz Audífono EarPollution Plugz Azul Cielo' WHERE source_medium='MKT18qw1j';
UPDATE ga_visits_cost_source_medium SET channel='MKT 30%' WHERE source_medium='MKT1DcqIV';
UPDATE ga_visits_cost_source_medium SET channel='GENIUS Speakers 31731006103 Blanco' WHERE source_medium='MKT1Hn1B5';
UPDATE ga_visits_cost_source_medium SET channel='Bajo Techo Pizarra Autoadhesiva Tipo Vinil' WHERE source_medium='MKT1HWmFm';
UPDATE ga_visits_cost_source_medium SET channel='MKT 15%' WHERE source_medium='MKT1i3zA8';
UPDATE ga_visits_cost_source_medium SET channel='MKT Bs. 50' WHERE source_medium='MKT1ktP10';
UPDATE ga_visits_cost_source_medium SET channel='Desarmador trompo con matraca con 12 puntas' WHERE source_medium='MKT1ORfvU';
UPDATE ga_visits_cost_source_medium SET channel='SABA – Taza de Porcelana Rayas (Facebook)' WHERE source_medium='MKT1P4NbB';
UPDATE ga_visits_cost_source_medium SET channel='MKT 15%' WHERE source_medium='MKT1rTCoJ';
UPDATE ga_visits_cost_source_medium SET channel='MKT Bs. 50' WHERE source_medium='MKT1xLsRR';
UPDATE ga_visits_cost_source_medium SET channel='Botella de Alumino para Agua No soy de plástico Gaiam-Plata' WHERE source_medium='MKT1Z54Lh';
UPDATE ga_visits_cost_source_medium SET channel='MKT 15% hasta 31 de diciembre' WHERE source_medium='MKT1ZtV';
UPDATE ga_visits_cost_source_medium SET channel='SABA – Taza de Porcelana Rayas ' WHERE source_medium='MKT2ww6wJ';
UPDATE ga_visits_cost_source_medium SET channel='SABA – Taza de Porcelana Rayas ' WHERE source_medium='MKT7XzxhT';
UPDATE ga_visits_cost_source_medium SET channel='Cupón de reemplazo a cupón de Mkt' WHERE source_medium='MKTAeF';
UPDATE ga_visits_cost_source_medium SET channel='MKT 15%' WHERE source_medium='MKTbS3gmp';
UPDATE ga_visits_cost_source_medium SET channel='MKT 15%' WHERE source_medium='MKTcwB9vv';
UPDATE ga_visits_cost_source_medium SET channel='SABA Porta Botellas Neopreno Negro' WHERE source_medium='MKTdtlbpI';
UPDATE ga_visits_cost_source_medium SET channel='ILUV Audífonos in ear Bubble Gum Rojo IEP205-R' WHERE source_medium='MKTECOjZv';
UPDATE ga_visits_cost_source_medium SET channel='Reemplazo cupón de Marketing' WHERE source_medium='MKTenQ';
UPDATE ga_visits_cost_source_medium SET channel='Tostador Proctor Silex 24605Y 4 Rebanadas-Blanco' WHERE source_medium='MKTf4A6Sk';
UPDATE ga_visits_cost_source_medium SET channel='HP P-FD4GBHP165-GEL Pendrive USB' WHERE source_medium='MKTf8VV4U';
UPDATE ga_visits_cost_source_medium SET channel='Morrocoy Silla de Director' WHERE source_medium='MKTGOhXfE';
UPDATE ga_visits_cost_source_medium SET channel='Kalosh Almohada 95% Pluma y 5% Plumón Blanca' WHERE source_medium='MKTkZ047R';
UPDATE ga_visits_cost_source_medium SET channel='MKT Bs. 50' WHERE source_medium='MKTLf9Mdg';
UPDATE ga_visits_cost_source_medium SET channel='Kingston Tarjeta de memoria SDC4/4GB-1ADP' WHERE source_medium='MKTljJ3QL';
UPDATE ga_visits_cost_source_medium SET channel='PRO CHEF Parrillera Hamburguer' WHERE source_medium='MKTmCN60a';
UPDATE ga_visits_cost_source_medium SET channel='Credito por bono no funcionando 2' WHERE source_medium='MKTMKT';
UPDATE ga_visits_cost_source_medium SET channel='Credito por bono no funcionando 3' WHERE source_medium='MKTMKT250';
UPDATE ga_visits_cost_source_medium SET channel='Contigo West loop Vaso Térmico con autosello 16 Oz/ 473 ml Rojo' WHERE source_medium='MKToVzJ1d';
UPDATE ga_visits_cost_source_medium SET channel='MKT 50Bs  hasta 31 de diciembre' WHERE source_medium='MKTQaQk';
UPDATE ga_visits_cost_source_medium SET channel='MKT 15%' WHERE source_medium='MKTr1jvkH';
UPDATE ga_visits_cost_source_medium SET channel='ARGOM Teclado multimedia en español ARG-KB-7805 Negro' WHERE source_medium='MKTr2DzaC';
UPDATE ga_visits_cost_source_medium SET channel='GBTech MP4 GBTech GB1106 Azul 4 GB' WHERE source_medium='MKTRp7vye';
UPDATE ga_visits_cost_source_medium SET channel='SABA Set de 3 utensilios para Parrilla' WHERE source_medium='MKTsd9V9B';
UPDATE ga_visits_cost_source_medium SET channel='Decocar Cava Americana 11,4Lt Roja' WHERE source_medium='MKTthEw98';
UPDATE ga_visits_cost_source_medium SET channel='MKT Bs. 50' WHERE source_medium='MKTuWtOQ3';
UPDATE ga_visits_cost_source_medium SET channel='Marketing en ferias' WHERE source_medium='MKTVngFxL';
UPDATE ga_visits_cost_source_medium SET channel='SABA - Dispensador de Aceite y Vinagre 2 en 1 / SABA-511 / Transparente' WHERE source_medium='MKTvpeCte';
UPDATE ga_visits_cost_source_medium SET channel='MKT 30%' WHERE source_medium='MKTy6v5rt';
UPDATE ga_visits_cost_source_medium SET channel='Easy Line Mouse Retráctil EL-993346 Negro' WHERE source_medium='MKTzBMwc3';
UPDATE ga_visits_cost_source_medium SET channel='Reactivation campaign' WHERE source_medium='MKTzOcbUq';



#OTHER
UPDATE ga_visits_cost_source_medium SET channel='Exchange' WHERE source_medium like 'CCE%';
UPDATE ga_visits_cost_source_medium SET channel='Content Mistake' WHERE source_medium like 'CON%';
UPDATE ga_visits_cost_source_medium SET channel='Procurement Mistake' WHERE source_medium like 'PRC%';
UPDATE ga_visits_cost_source_medium SET channel='Employee Vouchers' 
WHERE (source_medium like 'TE%' AND source_medium<>'TeleSales / CRM') OR source_medium like 'EA%';


#MERCADO LIBRE
UPDATE ga_visits_cost_source_medium SET channel='Mercado Libre Voucher' WHERE source_medium like 'ML%';

#PERIODISTAS

UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT02kER0';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT08jq2i';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT0arRZF';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT0hoq3F';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT0JgIhn';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT0LeO2h';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT0Olwjk';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT1d8pkJ';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT1fspLY';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT1h2uie';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT1jzjKP';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT1llem9';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT1Pk3Ap';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT1u1JSo';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT1v5kLS';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT1Yz6yi';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT4rXdCS';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTA0aIrT';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTac0eLL';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTCcHdRX';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKThMerN5';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKThNplZF';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTiE46NZ';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTJy4dxm';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTlzVdEY';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTmvE0F7';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTn7VdJO';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTna93IX';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTpYM3yV';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTQdcuzY';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTrI3ds7';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTS55mkn';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTSR8lJR';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTt1kyoi';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTTRutlc';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTuUrfbj';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTvkHUlg';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTvsJVH4';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTwdi2uO';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTXVRpth';





#PARTNERSHIPS

#DIRECT
UPDATE ga_visits_cost_source_medium SET channel='Linio.com Referral' WHERE  
source_medium='linio.com.co / referral' OR
source_medium='linio.com / referral' OR
source_medium='linio.com.mx / referral' OR
source_medium='linio.com.ve / referral' OR
source_medium='linio.com.pe / referral' OR
source_medium='www-staging.linio.com.ve / referral';
UPDATE ga_visits_cost_source_medium SET channel='Directo / Typed' WHERE  source_medium='(direct) / (none)';



#CORPORATE SALES
UPDATE ga_visits_cost_source_medium SET channel='Corporate Sales' WHERE source_medium like 'CDEAL%';


#OFFLINE

#N/A
UPDATE ga_visits_cost_source_medium SET channel='Unknown-No voucher' WHERE source_medium='';
UPDATE ga_visits_cost_source_medium SET channel='Unknown-Voucher' WHERE (channel='' AND source_medium<>'') or channel is null;

#Channel Report Extra Stuff to define Channel---
#Venta corporativa 



#Partnerships Channel Extra


#Mercado libre Orders 


#DEFINE CHANNEL GROUP

#Facebook Ads Group
UPDATE ga_visits_cost_source_medium SET channel_group='Facebook Ads' WHERE channel='Facebook Ads';

#OTHER
UPDATE ga_visits_cost_source_medium SET channel_group='Mercado Libre ' WHERE channel='Mercado Libre Voucher';

#SEM Group
UPDATE ga_visits_cost_source_medium SET channel_group='Search Engine Marketing' WHERE channel='SEM';

#Social Media Group
UPDATE ga_visits_cost_source_medium SET channel_group='Social Media' 
WHERE channel='Facebook Referral' OR channel='Twitter Referral' OR
channel='FB Posts';

#Retargeting Group
UPDATE ga_visits_cost_source_medium SET channel_group='Retargeting' WHERE channel='Sociomantic' OR channel like '%retargeting'
OR channel='Facebook R.' OR channel='Vizury'  OR channel = 'VEInteractive' OR channel = 'GDN Retargeting';

#GDN Group
UPDATE ga_visits_cost_source_medium SET channel_group='Google Display Network' WHERE channel like 'GDN'; 

#NewsLetter Group
UPDATE ga_visits_cost_source_medium SET channel_group='NewsLetter' WHERE channel='Newsletter';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 15% hasta 31 de diciembre';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 50Bs  hasta 31 de diciembre';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Tramontina Utilita Sacarcorcho';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Victorinox Memoria USB Slim 4GB Verde';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='HP FlashDrive Pen 4GB Usb V195B Azul';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='GBTech Audífono GBTech X5 GBX5 Amarillo';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Perfect Choice Audifono Diadema PC-110309 Con Banda Para Cuello';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Decocar Combo Tropical Azul';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Ocean Vaso Corto Studio Set 6';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='X-mini Altavoz Capsule v1.1 Rojo';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='HP P-FD4GBHP165-GEL Pendrive USB';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='SABA Set de 2 Cuchillos Colores';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Kingston DT101G2/4GB PenDrive USB';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Ifrogz Audífono EarPollution Plugz Azul Cielo';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 30%';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='GENIUS Speakers 31731006103 Blanco';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Bajo Techo Pizarra Autoadhesiva Tipo Vinil';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Desarmador trompo con matraca con 12 puntas';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='SABA – Taza de Porcelana Rayas (Facebook)';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Botella de Alumino para Agua No soy de plástico Gaiam-Plata';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 15% hasta 31 de diciembre';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='SABA – Taza de Porcelana Rayas ';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='SABA – Taza de Porcelana Rayas ';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Cupón de reemplazo a cupón de Mkt';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='SABA Porta Botellas Neopreno Negro';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='ILUV Audífonos in ear Bubble Gum Rojo IEP205-R';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Reemplazo cupón de Marketing';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Tostador Proctor Silex 24605Y 4 Rebanadas-Blanco';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='HP P-FD4GBHP165-GEL Pendrive USB';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Morrocoy Silla de Director';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Kalosh Almohada 95% Pluma y 5% Plumón Blanca';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Kingston Tarjeta de memoria SDC4/4GB-1ADP';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='PRO CHEF Parrillera Hamburguer';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Credito por bono no funcionando 2';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Credito por bono no funcionando 3';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Contigo West loop Vaso Térmico con autosello 16 Oz/ 473 ml Rojo';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 50Bs  hasta 31 de diciembre';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='ARGOM Teclado multimedia en español ARG-KB-7805 Negro';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='GBTech MP4 GBTech GB1106 Azul 4 GB';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='SABA Set de 3 utensilios para Parrilla';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Decocar Cava Americana 11,4Lt Roja';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Marketing en ferias';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='SABA - Dispensador de Aceite y Vinagre 2 en 1 / SABA-511 / Transparente';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 30%';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Easy Line Mouse Retráctil EL-993346 Negro';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Reactivation campaign';


#SEO Group
UPDATE ga_visits_cost_source_medium SET channel_group='Search Engine Optimization' WHERE channel='Search Organic';

#Partnerships Group

#CORPORATE SALES

UPDATE ga_visits_cost_source_medium SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';

#Corporate Sales Group
UPDATE ga_visits_cost_source_medium SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';

#Offline Group


#Tele Sales Group
UPDATE ga_visits_cost_source_medium SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales' OR channel='CS Generic Voucher';

#Branded Group
UPDATE ga_visits_cost_source_medium SET channel_group='Branded' 
WHERE channel='Linio.com Referral' OR channel='Directo / Typed' OR
channel='SEM Branded';

#ML Group
UPDATE ga_visits_cost_source_medium SET channel_group='Mercado Libre' 
WHERE channel='Mercado Libre Referral' OR channel='Mercado Libre Voucher' 
OR channel='Otros - Mercado Libre';

#Blog Group
UPDATE ga_visits_cost_source_medium SET channel_group='Blogs'
WHERE channel='Blog Linio';

#Affiliates Group
UPDATE ga_visits_cost_source_medium SET channel_group='Affiliates' 
WHERE channel='Buscape';


#Other (identified) Group
UPDATE ga_visits_cost_source_medium SET channel_group='Other (identified)' 
WHERE channel='Referral Sites' OR channel='Reactivation Letters' 
OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' OR
channel='Exchange' OR
channel='Content Mistake' OR
channel='Procurement Mistake' OR
channel='Employee Voucher' OR channel='Cupón para periodistas';



#Unknown Group
UPDATE ga_visits_cost_source_medium SET channel_group='Non identified' 
WHERE channel='Unknown-No voucher' OR channel='Unknown-Voucher';

#Define Channel Type---
#TO BE DONE


#Channel Report---

update ga_cost_campaign set source_medium = concat( source, ' / ', medium) where source_medium is null;


#NEW REGISTERS
UPDATE ga_cost_campaign SET channel='New Register' 
WHERE (source_medium like 'NL%' OR source_medium like 'NR%' OR source_medium like
'COM%' OR source_medium like 'BNR%') AND source_medium is null;

UPDATE ga_cost_campaign SET channel='Blog Linio' WHERE source_medium like 'blog%' 
OR source_medium='blog.linio.com.ve / referral' or source_medium like 'BL%';

#REFERRAL
UPDATE ga_cost_campaign SET channel='Referral Sites' WHERE source_medium like '%referral' 
AND source_medium<>'facebook.com / referral'
AND source_medium<>'m.facebook.com / referral'
AND source_medium<>'linio.com.co / referral'
AND source_medium<>'linio.com / referral'
AND source_medium<>'linio.com.mx / referral'
AND source_medium<>'google.co.ve / referral'
AND source_medium<>'linio.com.ve / referral'
AND source_medium<>'linio.com.pe / referral'
AND source_medium<>'www-staging.linio.com.ve / referral'
AND source_medium<>'google.com / referral'
AND source_medium<>'.com.ve / referral'
AND source_medium<>'blog.linio.com.ve / referral' AND source_medium<>'t.co / referral'
AND source_medium not like '%google%';


UPDATE ga_cost_campaign SET channel='Buscape' WHERE source_medium='Buscape / Price Comparison';
#FB ADS
UPDATE ga_cost_campaign SET channel='Facebook Ads' WHERE source_medium='facebook / socialmediaads' or source_medium='facebook / socialmediapaid';
#SEM - GDN
UPDATE ga_cost_campaign SET channel='SEM Branded' WHERE source_medium='google / cpc' AND (campaign like 'brand%');
UPDATE ga_cost_campaign SET channel='SEM' WHERE source_medium='google / cpc' AND (campaign not like 'brand%' );
UPDATE ga_cost_campaign SET channel='GDN' WHERE source_medium='google / cpc' AND campaign not like 'b.%' AND campaign like 'r.%';
UPDATE ga_cost_campaign SET channel='GDN' WHERE source like 'googleads.g.doubleclick.net%';
UPDATE ga_cost_campaign SET channel='GDN' 
WHERE source_medium='google / cpc' AND campaign like '[D[D%';
#RETARGETING
UPDATE ga_cost_campaign SET channel='Sociomantic' WHERE source_medium='sociomantic / (not set)' OR source_medium='sociomantic / retargeting';
UPDATE ga_cost_campaign SET channel='Vizury' WHERE source_medium='vizury / retargeting';
UPDATE ga_cost_campaign SET channel='VEInteractive' WHERE source like '%VEInteractive%';
UPDATE ga_cost_campaign SET channel='GDN Retargeting' 
WHERE source_medium='google / cpc' AND campaign like '[D[R%';
#RETARGETING
UPDATE ga_cost_campaign SET channel='Facebook R.' WHERE source_medium='facebook / retargeting';
#SOCIAL MEDIA
UPDATE ga_cost_campaign SET channel='Facebook Referral' WHERE 
(source_medium='facebook.com / referral' OR source_medium='m.facebook.com / referral') AND source_medium not like '%socialmediaads%';
UPDATE ga_cost_campaign SET channel='Twitter Referral' WHERE source_medium='t.co / referral' OR source_medium='twitter / socialmedia';
UPDATE ga_cost_campaign SET channel='FB Posts' 
WHERE source_medium='facebook / socialmedia' OR source_medium='SocialMedia / FacebookVoucher';
#SERVICIO AL CLIENTE
UPDATE ga_cost_campaign SET channel='Inbound' WHERE source_medium like 'Tele%';
UPDATE ga_cost_campaign SET channel='OutBound' WHERE source_medium like 'REC%';
UPDATE ga_cost_campaign SET channel='CS Generic Voucher' WHERE source_medium like 'CCemp%';

UPDATE ga_cost_campaign SET channel='Search Organic' WHERE source_medium like '%organic' OR source_medium like 'google.% / referral';
#NEWSLETTER
UPDATE ga_cost_campaign set Channel='Newsletter' WHERE source_medium like '%CRM' AND source_medium<>'TeleSales / CRM';
UPDATE ga_cost_campaign SET channel='MKT Bs. 50' WHERE source_medium='MKT02kVLM';
UPDATE ga_cost_campaign SET channel='MKT 15%' WHERE source_medium='MKT048S8J';
UPDATE ga_cost_campaign SET channel='MKT Bs. 50' WHERE source_medium='MKT04GPXB';
UPDATE ga_cost_campaign SET channel='MKT 15% hasta 31 de diciembre' WHERE source_medium='MKT06Ru';
UPDATE ga_cost_campaign SET channel='MKT Bs. 50' WHERE source_medium='MKT07kiKz';
UPDATE ga_cost_campaign SET channel='MKT 15%' WHERE source_medium='MKT0a3YVI';
UPDATE ga_cost_campaign SET channel='MKT 50Bs  hasta 31 de diciembre' WHERE source_medium='MKT0FTh';
UPDATE ga_cost_campaign SET channel='Tramontina Utilita Sacarcorcho' WHERE source_medium='MKT0hAb7W';
UPDATE ga_cost_campaign SET channel='Victorinox Memoria USB Slim 4GB Verde' WHERE source_medium='MKT0iuXXU';
UPDATE ga_cost_campaign SET channel='HP FlashDrive Pen 4GB Usb V195B Azul' WHERE source_medium='MKT0j2Efo';
UPDATE ga_cost_campaign SET channel='GBTech Audífono GBTech X5 GBX5 Amarillo' WHERE source_medium='MKT0oShGI';
UPDATE ga_cost_campaign SET channel='Perfect Choice Audifono Diadema PC-110309 Con Banda Para Cuello' WHERE source_medium='MKT0P2Bsq';
UPDATE ga_cost_campaign SET channel='MKT Bs. 50' WHERE source_medium='MKT0qCcAP';
UPDATE ga_cost_campaign SET channel='Decocar Combo Tropical Azul' WHERE source_medium='MKT0rjeet';
UPDATE ga_cost_campaign SET channel='MKT 15%' WHERE source_medium='MKT0WFMCm';
UPDATE ga_cost_campaign SET channel='Ocean Vaso Corto Studio Set 6' WHERE source_medium='MKT0yj5mw';
UPDATE ga_cost_campaign SET channel='MKT Bs. 50' WHERE source_medium='MKT0Z8YYn';
UPDATE ga_cost_campaign SET channel='X-mini Altavoz Capsule v1.1 Rojo' WHERE source_medium='MKT10hQfI';
UPDATE ga_cost_campaign SET channel='HP P-FD4GBHP165-GEL Pendrive USB' WHERE source_medium='MKT11vw0x';
UPDATE ga_cost_campaign SET channel='SABA Set de 2 Cuchillos Colores' WHERE source_medium='MKT12jfHL';
UPDATE ga_cost_campaign SET channel='MKT Bs. 50' WHERE source_medium='MKT14zmPN';
UPDATE ga_cost_campaign SET channel='Kingston DT101G2/4GB PenDrive USB' WHERE source_medium='MKT16mWW7';
UPDATE ga_cost_campaign SET channel='Ifrogz Audífono EarPollution Plugz Azul Cielo' WHERE source_medium='MKT18qw1j';
UPDATE ga_cost_campaign SET channel='MKT 30%' WHERE source_medium='MKT1DcqIV';
UPDATE ga_cost_campaign SET channel='GENIUS Speakers 31731006103 Blanco' WHERE source_medium='MKT1Hn1B5';
UPDATE ga_cost_campaign SET channel='Bajo Techo Pizarra Autoadhesiva Tipo Vinil' WHERE source_medium='MKT1HWmFm';
UPDATE ga_cost_campaign SET channel='MKT 15%' WHERE source_medium='MKT1i3zA8';
UPDATE ga_cost_campaign SET channel='MKT Bs. 50' WHERE source_medium='MKT1ktP10';
UPDATE ga_cost_campaign SET channel='Desarmador trompo con matraca con 12 puntas' WHERE source_medium='MKT1ORfvU';
UPDATE ga_cost_campaign SET channel='SABA – Taza de Porcelana Rayas (Facebook)' WHERE source_medium='MKT1P4NbB';
UPDATE ga_cost_campaign SET channel='MKT 15%' WHERE source_medium='MKT1rTCoJ';
UPDATE ga_cost_campaign SET channel='MKT Bs. 50' WHERE source_medium='MKT1xLsRR';
UPDATE ga_cost_campaign SET channel='Botella de Alumino para Agua No soy de plástico Gaiam-Plata' WHERE source_medium='MKT1Z54Lh';
UPDATE ga_cost_campaign SET channel='MKT 15% hasta 31 de diciembre' WHERE source_medium='MKT1ZtV';
UPDATE ga_cost_campaign SET channel='SABA – Taza de Porcelana Rayas ' WHERE source_medium='MKT2ww6wJ';
UPDATE ga_cost_campaign SET channel='SABA – Taza de Porcelana Rayas ' WHERE source_medium='MKT7XzxhT';
UPDATE ga_cost_campaign SET channel='Cupón de reemplazo a cupón de Mkt' WHERE source_medium='MKTAeF';
UPDATE ga_cost_campaign SET channel='MKT 15%' WHERE source_medium='MKTbS3gmp';
UPDATE ga_cost_campaign SET channel='MKT 15%' WHERE source_medium='MKTcwB9vv';
UPDATE ga_cost_campaign SET channel='SABA Porta Botellas Neopreno Negro' WHERE source_medium='MKTdtlbpI';
UPDATE ga_cost_campaign SET channel='ILUV Audífonos in ear Bubble Gum Rojo IEP205-R' WHERE source_medium='MKTECOjZv';
UPDATE ga_cost_campaign SET channel='Reemplazo cupón de Marketing' WHERE source_medium='MKTenQ';
UPDATE ga_cost_campaign SET channel='Tostador Proctor Silex 24605Y 4 Rebanadas-Blanco' WHERE source_medium='MKTf4A6Sk';
UPDATE ga_cost_campaign SET channel='HP P-FD4GBHP165-GEL Pendrive USB' WHERE source_medium='MKTf8VV4U';
UPDATE ga_cost_campaign SET channel='Morrocoy Silla de Director' WHERE source_medium='MKTGOhXfE';
UPDATE ga_cost_campaign SET channel='Kalosh Almohada 95% Pluma y 5% Plumón Blanca' WHERE source_medium='MKTkZ047R';
UPDATE ga_cost_campaign SET channel='MKT Bs. 50' WHERE source_medium='MKTLf9Mdg';
UPDATE ga_cost_campaign SET channel='Kingston Tarjeta de memoria SDC4/4GB-1ADP' WHERE source_medium='MKTljJ3QL';
UPDATE ga_cost_campaign SET channel='PRO CHEF Parrillera Hamburguer' WHERE source_medium='MKTmCN60a';
UPDATE ga_cost_campaign SET channel='Credito por bono no funcionando 2' WHERE source_medium='MKTMKT';
UPDATE ga_cost_campaign SET channel='Credito por bono no funcionando 3' WHERE source_medium='MKTMKT250';
UPDATE ga_cost_campaign SET channel='Contigo West loop Vaso Térmico con autosello 16 Oz/ 473 ml Rojo' WHERE source_medium='MKToVzJ1d';
UPDATE ga_cost_campaign SET channel='MKT 50Bs  hasta 31 de diciembre' WHERE source_medium='MKTQaQk';
UPDATE ga_cost_campaign SET channel='MKT 15%' WHERE source_medium='MKTr1jvkH';
UPDATE ga_cost_campaign SET channel='ARGOM Teclado multimedia en español ARG-KB-7805 Negro' WHERE source_medium='MKTr2DzaC';
UPDATE ga_cost_campaign SET channel='GBTech MP4 GBTech GB1106 Azul 4 GB' WHERE source_medium='MKTRp7vye';
UPDATE ga_cost_campaign SET channel='SABA Set de 3 utensilios para Parrilla' WHERE source_medium='MKTsd9V9B';
UPDATE ga_cost_campaign SET channel='Decocar Cava Americana 11,4Lt Roja' WHERE source_medium='MKTthEw98';
UPDATE ga_cost_campaign SET channel='MKT Bs. 50' WHERE source_medium='MKTuWtOQ3';
UPDATE ga_cost_campaign SET channel='Marketing en ferias' WHERE source_medium='MKTVngFxL';
UPDATE ga_cost_campaign SET channel='SABA - Dispensador de Aceite y Vinagre 2 en 1 / SABA-511 / Transparente' WHERE source_medium='MKTvpeCte';
UPDATE ga_cost_campaign SET channel='MKT 30%' WHERE source_medium='MKTy6v5rt';
UPDATE ga_cost_campaign SET channel='Easy Line Mouse Retráctil EL-993346 Negro' WHERE source_medium='MKTzBMwc3';
UPDATE ga_cost_campaign SET channel='Reactivation campaign' WHERE source_medium='MKTzOcbUq';



#OTHER
UPDATE ga_cost_campaign SET channel='Exchange' WHERE source_medium like 'CCE%';
UPDATE ga_cost_campaign SET channel='Content Mistake' WHERE source_medium like 'CON%';
UPDATE ga_cost_campaign SET channel='Procurement Mistake' WHERE source_medium like 'PRC%';
UPDATE ga_cost_campaign SET channel='Employee Vouchers' 
WHERE (source_medium like 'TE%' AND source_medium<>'TeleSales / CRM') OR source_medium like 'EA%';


#MERCADO LIBRE
UPDATE ga_cost_campaign SET channel='Mercado Libre Voucher' WHERE source_medium like 'ML%';

#PERIODISTAS

UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT02kER0';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT08jq2i';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT0arRZF';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT0hoq3F';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT0JgIhn';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT0LeO2h';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT0Olwjk';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT1d8pkJ';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT1fspLY';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT1h2uie';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT1jzjKP';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT1llem9';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT1Pk3Ap';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT1u1JSo';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT1v5kLS';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT1Yz6yi';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT4rXdCS';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTA0aIrT';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTac0eLL';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTCcHdRX';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKThMerN5';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKThNplZF';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTiE46NZ';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTJy4dxm';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTlzVdEY';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTmvE0F7';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTn7VdJO';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTna93IX';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTpYM3yV';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTQdcuzY';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTrI3ds7';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTS55mkn';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTSR8lJR';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTt1kyoi';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTTRutlc';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTuUrfbj';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTvkHUlg';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTvsJVH4';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTwdi2uO';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTXVRpth';





#PARTNERSHIPS

#DIRECT
UPDATE ga_cost_campaign SET channel='Linio.com Referral' WHERE  
source_medium='linio.com.co / referral' OR
source_medium='linio.com / referral' OR
source_medium='linio.com.mx / referral' OR
source_medium='linio.com.ve / referral' OR
source_medium='linio.com.pe / referral' OR
source_medium='www-staging.linio.com.ve / referral';
UPDATE ga_cost_campaign SET channel='Directo / Typed' WHERE  source_medium='(direct) / (none)';



#CORPORATE SALES
UPDATE ga_cost_campaign SET channel='Corporate Sales' WHERE source_medium like 'CDEAL%';


#OFFLINE

#N/A
UPDATE ga_cost_campaign SET channel='Unknown-No voucher' WHERE source_medium='';
UPDATE ga_cost_campaign SET channel='Unknown-Voucher' WHERE (channel='' AND source_medium<>'') or channel is null;

#Channel Report Extra Stuff to define Channel---
#Venta corporativa 



#Partnerships Channel Extra


#Mercado libre Orders 


#DEFINE CHANNEL GROUP

#Facebook Ads Group
UPDATE ga_cost_campaign SET channel_group='Facebook Ads' WHERE channel='Facebook Ads';

#OTHER
UPDATE ga_cost_campaign SET channel_group='Mercado Libre ' WHERE channel='Mercado Libre Voucher';

#SEM Group
UPDATE ga_cost_campaign SET channel_group='Search Engine Marketing' WHERE channel='SEM';

#Social Media Group
UPDATE ga_cost_campaign SET channel_group='Social Media' 
WHERE channel='Facebook Referral' OR channel='Twitter Referral' OR
channel='FB Posts';

#Retargeting Group
UPDATE ga_cost_campaign SET channel_group='Retargeting' WHERE channel='Sociomantic' OR channel like '%retargeting'
OR channel='Facebook R.' OR channel='Vizury' OR channel = 'VEInteractive' OR channel = 'GDN Retargeting';

#GDN group
UPDATE ga_cost_campaign SET channel_group='Google Display Network' WHERE channel like 'GDN'; 

#NewsLetter Group
UPDATE ga_cost_campaign SET channel_group='NewsLetter' WHERE channel='Newsletter';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 15% hasta 31 de diciembre';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 50Bs  hasta 31 de diciembre';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Tramontina Utilita Sacarcorcho';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Victorinox Memoria USB Slim 4GB Verde';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='HP FlashDrive Pen 4GB Usb V195B Azul';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='GBTech Audífono GBTech X5 GBX5 Amarillo';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Perfect Choice Audifono Diadema PC-110309 Con Banda Para Cuello';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Decocar Combo Tropical Azul';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Ocean Vaso Corto Studio Set 6';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='X-mini Altavoz Capsule v1.1 Rojo';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='HP P-FD4GBHP165-GEL Pendrive USB';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='SABA Set de 2 Cuchillos Colores';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Kingston DT101G2/4GB PenDrive USB';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Ifrogz Audífono EarPollution Plugz Azul Cielo';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 30%';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='GENIUS Speakers 31731006103 Blanco';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Bajo Techo Pizarra Autoadhesiva Tipo Vinil';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Desarmador trompo con matraca con 12 puntas';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='SABA – Taza de Porcelana Rayas (Facebook)';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Botella de Alumino para Agua No soy de plástico Gaiam-Plata';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 15% hasta 31 de diciembre';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='SABA – Taza de Porcelana Rayas ';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='SABA – Taza de Porcelana Rayas ';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Cupón de reemplazo a cupón de Mkt';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='SABA Porta Botellas Neopreno Negro';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='ILUV Audífonos in ear Bubble Gum Rojo IEP205-R';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Reemplazo cupón de Marketing';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Tostador Proctor Silex 24605Y 4 Rebanadas-Blanco';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='HP P-FD4GBHP165-GEL Pendrive USB';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Morrocoy Silla de Director';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Kalosh Almohada 95% Pluma y 5% Plumón Blanca';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Kingston Tarjeta de memoria SDC4/4GB-1ADP';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='PRO CHEF Parrillera Hamburguer';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Credito por bono no funcionando 2';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Credito por bono no funcionando 3';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Contigo West loop Vaso Térmico con autosello 16 Oz/ 473 ml Rojo';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 50Bs  hasta 31 de diciembre';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='ARGOM Teclado multimedia en español ARG-KB-7805 Negro';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='GBTech MP4 GBTech GB1106 Azul 4 GB';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='SABA Set de 3 utensilios para Parrilla';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Decocar Cava Americana 11,4Lt Roja';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Marketing en ferias';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='SABA - Dispensador de Aceite y Vinagre 2 en 1 / SABA-511 / Transparente';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 30%';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Easy Line Mouse Retráctil EL-993346 Negro';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Reactivation campaign';


#SEO Group
UPDATE ga_cost_campaign SET channel_group='Search Engine Optimization' WHERE channel='Search Organic';

#Partnerships Group

#CORPORATE SALES

UPDATE ga_cost_campaign SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';

#Corporate Sales Group
UPDATE ga_cost_campaign SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';

#Offline Group


#Tele Sales Group
UPDATE ga_cost_campaign SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales' OR channel='CS Generic Voucher';

#Branded Group
UPDATE ga_cost_campaign SET channel_group='Branded' 
WHERE channel='Linio.com Referral' OR channel='Directo / Typed' OR
channel='SEM Branded';

#ML Group
UPDATE ga_cost_campaign SET channel_group='Mercado Libre' 
WHERE channel='Mercado Libre Referral' OR channel='Mercado Libre Voucher' 
OR channel='Otros - Mercado Libre';

#Blog Group
UPDATE ga_cost_campaign SET channel_group='Blogs'
WHERE channel='Blog Linio';

#Affiliates Group
UPDATE ga_cost_campaign SET channel_group='Affiliates' 
WHERE channel='Buscape';


#Other (identified) Group
UPDATE ga_cost_campaign SET channel_group='Other (identified)' 
WHERE channel='Referral Sites' OR channel='Reactivation Letters' 
OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' OR
channel='Exchange' OR
channel='Content Mistake' OR
channel='Procurement Mistake' OR
channel='Employee Voucher' OR channel='Cupón para periodistas';



#Unknown Group
UPDATE ga_cost_campaign SET channel_group='Non identified' 
WHERE channel='Unknown-No voucher' OR channel='Unknown-Voucher';

select  'start channel_report_no_voucher: end',now();

