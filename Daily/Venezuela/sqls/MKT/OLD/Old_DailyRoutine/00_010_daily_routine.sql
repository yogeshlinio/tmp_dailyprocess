

#ventas netas mes corrido
/*insert into production_ve.tbl_sales_growth_mom_cat
select date(now() - interval 1 day) date, tab.n1, ventas_netas_t, ventas_netas_t_1, 
cast(((ventas_netas_t/ventas_netas_t_1)-1) as decimal(30,10)) mom from (
select n1,sum(paid_price_after_vat) + sum(if(shipping_fee_after_vat is null, shipping_fee_after_vat,0)) ventas_netas_t
 from production_ve.tbl_order_detail where date>= date(now() - interval day(now()) day) and date <= now() 
 and month(date) = month(now() - interval 1 day) and oac = 1 and returned = 0 group by n1) tab inner join 
(select n1,sum(paid_price_after_vat) + sum(if(shipping_fee_after_vat is null, shipping_fee_after_vat,0)) ventas_netas_t_1
 from production_ve.tbl_order_detail where date<= date(now() - interval 1 month)
 and date >= date(now() - interval 1 month - interval day(now()) day ) 
and month(date) = month(now() - interval 1 day) - 1
 and oac = 1 and returned = 0 group by n1) tab2
on tab.n1 = tab2.n1;

insert into production_ve.tbl_sales_growth_mom_buyer
select date(now() - interval 1 day) date, tab.buyer, ventas_netas_t, ventas_netas_t_1, 
cast(((ventas_netas_t/ventas_netas_t_1)-1) as decimal(30,10)) mom from (
select buyer,sum(paid_price_after_vat) + sum(if(shipping_fee_after_vat is null, shipping_fee_after_vat,0)) ventas_netas_t
 from production_ve.tbl_order_detail where date>= date(now() - interval day(now()) day) and date <= now() 
 and month(date) = month(now() - interval 1 day) and oac = 1 and returned = 0 group by buyer) tab inner join 
(select buyer,sum(paid_price_after_vat) + sum(if(shipping_fee_after_vat is null, shipping_fee_after_vat,0)) ventas_netas_t_1
 from production_ve.tbl_order_detail where date<= date(now() - interval 1 month)
 and date >= date(now() - interval 1 month - interval day(now()) day ) 
and month(date) =  month(now() - interval 1 day) - 1
 and oac = 1 and returned = 0 group by buyer) tab2
on tab.buyer = tab2.buyer;*/

select  'bireporting: start',now();

#sales cube
-- call bireporting.etl_fact_sales_comercial();

select  'bireporting: ok',now();
