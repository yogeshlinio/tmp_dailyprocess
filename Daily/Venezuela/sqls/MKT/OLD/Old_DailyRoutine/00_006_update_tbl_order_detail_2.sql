
#Cupones de devolución
UPDATE tbl_order_detail t SET paid_price = unit_price, paid_price_after_vat = unit_price/1.12
WHERE coupon_code like 'CCE%' 
or coupon_code like 'PRCcre%' 
or coupon_code like 'OPScre%'
or coupon_Code like 'DEP%';

#pc2 
select 'start pc2',now();
truncate production_ve.tbl_group_order_detail;
insert into production_ve.tbl_group_order_detail (select orderid, count(order_nr) as items,
sum(greatest(cast(replace(if(isnull(peso),1,peso),',','.') as decimal(21,6)),1.0))as pesototal,
sum(paid_price) as grandtotal
from production_ve.tbl_order_detail where  oac='1' and returned='0' group by order_nr);	

truncate production_ve.tbl_group_order_detail_gross;
insert into production_ve.tbl_group_order_detail_gross (select orderid, count(order_nr) as items,
sum(greatest(cast(replace(if(isnull(peso),1,peso),',','.') as decimal(21,6)),1.0))as pesototal,
sum(paid_price) as grandtotal
from production_ve.tbl_order_detail where  obc='1' group by order_nr);	


update   production_ve.tbl_order_detail as t 
left join production_ve.tbl_group_order_detail as e 
on e.orderid = t.orderid 
set t.orderpeso = e.pesototal , t.nr_items=e.items , t.ordertotal=e.grandtotal
where t.oac='1' and t.returned='0';

update   production_ve.tbl_order_detail as t 
left join production_ve.tbl_group_order_detail_gross as e 
on e.orderid = t.orderid 
set t.orderpeso_gross = e.pesototal , t.gross_items=e.items , t.ordertotal_gross=e.grandtotal
where t.obc='1';

update production_ve.tbl_order_detail as t
set gross_orders = if(gross_items is null,0,1/gross_items) where obc = '1';

##update production_ve.tbl_order_detail as t
##set net_orders = if(nr_items is null, 0,1/nr_items) where oac = '1 'and returned = '0';

-- New Net Orders

/*
create table production_ve.temporary_net_orders as select order_nr, count(*) as items from production_ve.tbl_order_detail where oac=1 group by order_nr;

create index order_nr on production_ve.temporary_net_orders(order_nr);

update production_ve.tbl_order_detail t inner join production_ve.temporary_net_orders n on t.order_nr=n.order_nr set t.net_orders=1/n.items;

drop table production_ve.temporary_net_orders;*/

#pc2: shipping_fee
update production_ve.tbl_order_detail set ordershippingfee=0.0 where ordershippingfee is null;
update production_ve.tbl_order_detail set shipping_fee=ordershippingfee/cast(nr_items as decimal) where oac='1' and returned='0';
update tbl_order_detail set shipping_fee_2=ordershippingfee/cast(nr_items as decimal);

update production_ve.tbl_order_detail set shipping_fee_after_vat=if(shipping_fee is null, 0, shipping_fee/1.12);
update tbl_order_detail set shipping_fee_after_vat_2=if(shipping_fee_2 is null, 0, shipping_fee_2/1.12);
update tbl_order_detail set grand_total_after_vat=shipping_fee_after_vat_2+paid_price_after_vat;
update tbl_order_detail set gross_grand_total_after_vat=shipping_fee_after_vat_2+paid_price_after_vat where obc = '1';
update tbl_order_detail set net_grand_total_after_vat=shipping_fee_after_vat+paid_price_after_vat 
where (oac = 1 and returned = 0);

#pc2: cs and wh
update production_ve.tbl_order_detail set cs=13.52/cast(nr_items as decimal),wh=16.64/cast(nr_items as decimal) where date>='2012-08-01' and oac='1' and returned='0';
update production_ve.tbl_order_detail set cs=37.5/cast(nr_items as decimal),wh=40.5/cast(nr_items as decimal) where date>='2013-03-01' and oac='1' and returned='0';

/* update production_ve.tbl_order_detail set shipping_cost=(select  bogota from bob_live_ve.shipping_cost where peso=least(ceil(orderpeso),200))/cast(nr_items as decimal) where ciudad like '%bogota%' and oac='1' and returned='0' and date<='2012-12-11';
update production_ve.tbl_order_detail set shipping_cost=(select  resto_pais from bob_live_ve.shipping_cost where peso=least(ceil(orderpeso),200))/cast(nr_items as decimal) where ciudad not like '%bogota%' and oac='1' and returned='0'and date<='2012-12-11';
update production_ve.tbl_order_detail set shipping_cost= ordershippingfee/cast(nr_items as decimal) where oac='1' and returned='0' and date>'2012-12-11'; */

#shipping_cost
update production_ve.tbl_order_detail set shipping_cost = 0.04 * (paid_price_after_vat+if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat));

#pc2: payment_cost

update production_ve.tbl_order_detail set payment_cost=(ordertotal+ordershippingfee)*0.03/cast(nr_items as decimal) where payment_method ='Novared_Creditcard_Redirect' and oac='1' and returned='0';
update production_ve.tbl_order_detail set payment_cost=(ordertotal+ordershippingfee)*0.0371/cast(nr_items as decimal) where payment_method ='Sitef_Creditcard' and oac='1' and returned='0';

#wh shipping_cost para drop shipping
update production_ve.tbl_order_detail set wh =0.0, shipping_cost=0.0 where sku in (select sku from bob_live_ve.catalog_simple where fk_catalog_shipment_type=2);

#proveedor entrega:
-- update  production_ve.tbl_order_detail set delivery_cost_supplier=0.0 where proveedor='izc mayorista s.a.s';

#null fiedls
update production_ve.tbl_order_detail set shipping_fee=0.0 where shipping_fee is null;
update production_ve.tbl_order_detail set delivery_cost_supplier=0.0 where delivery_cost_supplier is null;
update production_ve.tbl_order_detail set wh=0.0 where wh is null;
update production_ve.tbl_order_detail set cs=0.0 where cs is null;
update production_ve.tbl_order_detail set payment_cost=0.0 where payment_cost is null;
update production_ve.tbl_order_detail set shipping_cost=0.0 where shipping_cost is null;

#campaign
-- update production_ve.tbl_order_detail set campaign = 'lizamoran' where campaign = 'liza.moran';

#shipping_cost
-- call shipping_cost_order_city();

/*update production_ve.tbl_order_detail join(select date, t2.orderid, t2.sku_config, city, shipment_zone, t1.shipping_cost as shipping_cost_menor, t2.shipping_cost, 
if(t1.shipping_cost > 0 and t1.shipping_cost < t2.shipping_cost, '1','0') as r 
from 
production_ve.tbl_shipment_cost_calculado t1
right join production_ve.tbl_shipping_cost_order_city t2
on t1.sku_config = t2.sku_config and fk_shipment_zone = shipment_zone
where month(date) >= 2 and year(date) >= 2013
group by  date, t2.orderid, t2.sku_config, city, shipment_zone) tabla
on tbl_order_detail.orderid = tabla.orderid
set tbl_order_detail.shipping_cost = tabla.shipping_cost_menor where r = '1'
and month(tbl_order_detail.date) >= 2 and year(tbl_order_detail.date) >= 2013; */

