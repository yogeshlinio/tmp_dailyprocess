
#tbl_monthly_cohort_sin_groupon
truncate production_ve.tbl_monthly_cohort_sin_groupon;
insert into tbl_monthly_cohort_sin_groupon(custid, netsales, orders, firstorder, lastorder, idfirstorder, idlastorder, frecuencia,
coupon_code,  channel, channel_group)
select t1.*, t2.coupon_code, t2.channel,t2.channel_group from
(select 
        tbl_order_detail.custid as custid,
        sum(tbl_order_detail.paid_price_after_vat) as netsales,
        count(distinct tbl_order_detail.order_nr) as orders,
        min(tbl_order_detail.date) as firstorder,
        max(tbl_order_detail.date) as lastorder,
        min(tbl_order_detail.orderid) as idfirstorder,
        max(tbl_order_detail.orderid) as idlastorder,
        count(distinct tbl_order_detail.date) as frecuencia
    from
        production_ve.tbl_order_detail
    where
        ((tbl_order_detail.oac = '1')
            and (tbl_order_detail.returned = '0'))
    group by tbl_order_detail.custid
    order by sum(tbl_order_detail.paid_price_after_vat) desc) as t1
inner join (select  orderid,coupon_code,channel,channel_group from production_ve.tbl_order_detail where oac = '1' and returned = '0'and coupon_code not like 'gr%')
as t2 on t1.idfirstorder = t2.orderid
group by t1.custid, t2.coupon_code
order by sum(t1.netsales) desc;

update production_ve.tbl_order_detail inner join  
(select firstOrder as date,custID as newCustomer, idFirstOrder from production_ve.view_cohort ) as tbl_nc on tbl_nc.date=tbl_order_detail.date and tbl_nc.newCustomer = tbl_order_detail.custID and tbl_nc.idFirstOrder = orderID
set tbl_order_detail.new_customers=1/nr_items;

update production_ve.tbl_order_detail p inner join bob_live_ve.catalog_config conf on p.sku_config=conf.sku inner join bob_live_ve.catalog_attribute_option_global_category c on c.id_catalog_attribute_option_global_category=conf.fk_catalog_attribute_option_global_category set p.new_cat1 = c.name where p.new_cat1 is null;

update production_ve.tbl_order_detail p inner join bob_live_ve.catalog_config conf on p.sku_config=conf.sku inner join bob_live_ve.catalog_attribute_option_global_sub_category c on c.id_catalog_attribute_option_global_sub_category=conf.fk_catalog_attribute_option_global_sub_category set p.new_cat2 = c.name where p.new_cat2 is null;

update production_ve.tbl_order_detail p inner join bob_live_ve.catalog_config conf on p.sku_config=conf.sku inner join bob_live_ve.catalog_attribute_option_global_sub_sub_category c on c.id_catalog_attribute_option_global_sub_sub_category=conf.fk_catalog_attribute_option_global_sub_sub_category set p.new_cat3 = c.name where p.new_cat3 is null;


#tbl_monthly_cohort
truncate production_ve.tbl_monthly_cohort;
insert into tbl_monthly_cohort(custid,cohort,idfirstorder,idlastorder)
select custid,
cast(concat(year(firstorder),if(length(month(firstorder)) < 2,concat('0', month(firstorder)),month(firstorder)))as signed) as cohort,
idfirstorder,idlastorder from production_ve.view_cohort;

update production_ve.tbl_monthly_cohort inner join production_ve.tbl_order_detail on idFirstOrder = orderID and tbl_monthly_cohort.CustID = tbl_order_detail.CustID
set tbl_monthly_cohort.coupon_code = tbl_order_detail.coupon_code;

update production_ve.tbl_monthly_cohort set coupon_code = '' where coupon_code is null;

select  'update tbl_order_detail: end',now();

#New_customers
update production_ve.tbl_order_detail inner join  
(select firstOrder as date,custID as newCustomer, idFirstOrder from production_ve.view_cohort_gross ) as tbl_nc on tbl_nc.date=tbl_order_detail.date and tbl_nc.newCustomer = tbl_order_detail.custID and tbl_nc.idFirstOrder = orderID
set tbl_order_detail.new_customers_gross=1/gross_items;

update production_ve.tbl_order_detail set PC1 = ifnull(unit_price_after_vat,0)-ifnull(coupon_money_value,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0);

update production_ve.tbl_order_detail set PC2 = PC1-ifnull(payment_cost,0)-ifnull(shipping_cost,0)-ifnull(wh,0)-ifnull(cs,0);

#channel clv
truncate production_ve.tbl_monthly_cohort_channel;
insert into production_ve.tbl_monthly_cohort_channel (fk_customer,cohort,idfirstorder,channel_group)
select tbl_monthly_cohort.custid as fk_customer,cohort,idfirstorder,tbl_order_detail.channel_group as channel
from production_ve.tbl_monthly_cohort left join production_ve.tbl_order_detail on orderid=idfirstorder
group by tbl_monthly_cohort.custid;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Venezuela', 
  'tbl_order_detail',
  NOW(),
  max(date),
  count(*),
  count(*)
FROM
  production_ve.tbl_order_detail;

select  'daily routine: tbl_order_detail end',now();
