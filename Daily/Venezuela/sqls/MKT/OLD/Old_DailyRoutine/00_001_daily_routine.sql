
select  'daily routine: start',now();

select  'daily routine: tbl_catalog_product start',now();
#table catalog_product_v2
drop table if exists production_ve.simple_variation;
create table production_ve.simple_variation as (select * from production_ve.simple_variation_view);
alter table production_ve.simple_variation add primary key (fk_catalog_simple) ;

#tbl_catalog_stock
truncate production_ve.tbl_catalog_product_stock;
insert into production_ve.tbl_catalog_product_stock(fk_catalog_simple,sku,reservedbob,stockbob,availablebob)
select catalog_simple.id_catalog_simple,catalog_simple.sku,
if(sum(is_reserved) is null, 0, sum(is_reserved))  as reservedbob ,catalog_stock.quantity as availablebob,
catalog_stock.quantity-if(sum(is_reserved) is null, 0, sum(is_reserved)) as disponible
from 
(bob_live_ve.catalog_simple join bob_live_ve.catalog_stock on catalog_simple.id_catalog_simple=catalog_stock.fk_catalog_simple) left join bob_live_ve.sales_order_item
 on sales_order_item.sku=catalog_simple.sku
group by catalog_simple.sku
order by catalog_stock.quantity desc;
drop table if exists tbl_catalog_product_v3;
create table production_ve.tbl_catalog_product_v3 as (select * from production_ve.catalog_product);
update production_ve.tbl_catalog_product_v3  set visible='no' where cat1 is null;

#actualiza lo recientamente reservado, desde el 01 de enero
update production_ve.tbl_catalog_product_stock inner join (select tbl_catalog_product_stock.fk_catalog_simple,
if(sum(is_reserved) is null, 0, sum(is_reserved))  as recently_reserved
from 
production_ve.tbl_catalog_product_stock inner join bob_live_ve.sales_order_item on tbl_catalog_product_stock.sku=sales_order_item.sku
where date(sales_order_item.created_at)>'2013-01-01' group by fk_catalog_simple) as a 
on  tbl_catalog_product_stock.fk_catalog_simple = a.fk_catalog_simple
set tbl_catalog_product_stock.recently_reservedbob = a.recently_reserved;


alter table production_ve.tbl_catalog_product_v3 add primary key (sku) ;

drop table if exists tbl_catalog_product_v2;
alter table production_ve.tbl_catalog_product_v3 rename to  production_ve.tbl_catalog_product_v2 ;

alter table production_ve.tbl_catalog_product_v2 
add index (sku_config ) ;

select  'daily routine: tbl_catalog_product ok',now();

#special cost
#call special_cost();
