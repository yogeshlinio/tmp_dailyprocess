
truncate tbl_dailyReport_local;
insert into tbl_dailyReport_local (date) select dt from calendar where dt>'2012-07-31' and dt<now();

update tbl_dailyReport_local
set year = year(date) , month = month(date) , yrmonth = concat(year,if(month<10,concat(0,month),month));

#Visits
UPDATE tbl_dailyReport_local t INNER JOIN ga_daily_visits g 
ON t.date = g.date
SET t.Visits = g.visits;

#Visits
UPDATE tbl_dailyReport_local t INNER JOIN ga_daily_visits g 
ON t.date = g.date
SET t.UniqueVisits = g.unique_visits;

#Mobile Visits
UPDATE tbl_dailyReport_local t INNER JOIN ga_daily_visits g 
ON t.date = g.date
SET t.MobileVisits = if(g.mobile_visits is null, 0, g.mobile_visits);

#Gross
update tbl_dailyReport_local join 
(select date,sum(`unit_price`) as grossrev, count(distinct `order_nr`,`OAC`,`RETURNED`,`PENDING`) as grossorders, 
	sum(`unit_price_after_vat`) as GrossSalesPreCancel
            from `tbl_order_detail` where   (`OBC` = '1')
            group by `date`) AS tbl_gross on tbl_gross.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.GrossRev=tbl_gross.grossrev , tbl_dailyReport_local.GrossOrders=tbl_gross.grossorders, 
tbl_dailyReport_local.GrossSalesPreCancel=tbl_gross.GrossSalesPreCancel;

#Cancel
update tbl_dailyReport_local join 
(select date,sum(`unit_price_after_vat`) as cancelrev, count(distinct `order_nr`) as cancelorders
from `tbl_order_detail` 
where   (`CANCEL` = '1')
group by `date`) AS tbl_cancel on tbl_cancel.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.Cancellations=tbl_cancel.cancelrev , tbl_dailyReport_local.CancelOrders=tbl_cancel.cancelorders;


#Pending
update tbl_dailyReport_local join 
(select date,sum(if(`unit_price_after_vat` is null, 0, unit_price_after_vat))  as pendingrev, if( count(distinct `order_nr`) is null, 0,  count(distinct `order_nr`)) as pendingorders
from `tbl_order_detail` 
where   (`PENDING` = '1')
group by `date`) AS tbl_pending on tbl_pending.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.PendingCOP=tbl_pending.pendingrev , tbl_dailyReport_local.PendingOrders=tbl_pending.pendingorders;

#cogs_gross_post_cancel
update tbl_dailyReport_local join 
(select date,(sum(`costo_after_vat`) + sum(`delivery_cost_supplier`)) as cogs_gross_post_cancel
from `tbl_order_detail` 
where   (`OAC` = '1')
group by `date`) AS tbl_cogs_post_cancel on tbl_cogs_post_cancel.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.cogs_gross_post_cancel=tbl_cogs_post_cancel.cogs_gross_post_cancel;

#Returned
update tbl_dailyReport_local join 
(select date,sum(`unit_price_after_vat`) as returnrev, count(distinct `order_nr`) as returnorders
from `tbl_order_detail` 
where   (`RETURNED` = '1')
group by `date`) AS tbl_returned on tbl_returned.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.ReturnedCOP=tbl_returned.returnrev , tbl_dailyReport_local.ReturnedOrders=tbl_returned.returnorders;

#Net
update tbl_dailyReport_local join 
(select date,sum(`unit_price_after_vat`) as NetSalesPreVoucher, SUM(coupon_money_after_vat) as VoucherMktCost,
SUM(`paid_price_after_vat`) as NetSales,count(distinct `order_nr`) as NetOrders,
 (sum(`costo_after_vat`) + sum(`delivery_cost_supplier`))as cogs_on_net_sales,
count(`order_nr`)/count(distinct `order_nr`) as AverageItemsPerBasket,
count(distinct `CustID`) as customers,
SUM(`shipping_fee`) as ShippingFee, 
SUM(`shipping_cost`)as ShippingCost, 
SUM(`payment_cost`) as PaymentCost
from `tbl_order_detail` 
where   (`RETURNED` = '0') AND (`OAC` = '1')
group by `date`) AS tbl_net on tbl_net.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.NetSalesPreVoucher=tbl_net.NetSalesPreVoucher ,
tbl_dailyReport_local.VoucherMktCost=tbl_net.VoucherMktCost,
tbl_dailyReport_local.NetSales=tbl_net.NetSales,
tbl_dailyReport_local.NetOrders=tbl_net.NetOrders,
tbl_dailyReport_local.cogs_on_net_sales=tbl_net.cogs_on_net_sales,
tbl_dailyReport_local.AverageItemsPerBasket=tbl_net.AverageItemsPerBasket,
tbl_dailyReport_local.customers=tbl_net.customers,
tbl_dailyReport_local.ShippingFee=tbl_net.ShippingFee/1.12,
tbl_dailyReport_local.ShippingCost=tbl_net.ShippingCost,
tbl_dailyReport_local.PaymentCost=tbl_net.PaymentCost;

#Marketing spend
UPDATE tbl_dailyReport_local t
INNER JOIN (select date, sum(adCost) adCost from ga_cost_campaign group by date) g 
ON t.date = g.date SET t.marketing_spend = adCost;

# New Customers
update tbl_dailyReport_local join  
(select firstOrder as date,COUNT(*) as newCustomers from `view_cohort` 
group by firstOrder) as tbl_nc on tbl_nc.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.newcustomers=tbl_nc.newCustomers,
tbl_dailyReport_local.ShippingCostPerOrder=tbl_dailyReport_local.ShippingCost/tbl_nc.newCustomers,
tbl_dailyReport_local.PaymentCostPerOrder=tbl_dailyReport_local.PaymentCost/tbl_nc.newCustomers;

#PaidChannels
 update production_ve.tbl_dailyReport_local join 
(select date,sum(paid_price_after_vat) as net_sales_paid_channel,
sum(coupon_money_after_vat) as voucher_paid_channel
from production_ve.tbl_order_detail 
where  (oac = '1') and (returned = '0') 
and   ((source_medium like '%price comparison%')
  or (source_medium like '%socialmediaads%')
  or (source_medium like '%cpc%')
  or (source_medium like '%sociomantic%'))
group by date) as tbl_paid on tbl_paid.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_paid_channel=tbl_paid.net_sales_paid_channel
 , tbl_dailyReport_local.voucher_paid_channel=tbl_paid.voucher_paid_channel;

 
#Categorías
#Electrodomésticos
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` = 'electrodomésticos')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Appliances=tbl_cat.netsales , tbl_dailyReport_local.cogs_Appliances=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Appliances=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Appliances=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Appliances=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Appliances=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Appliances=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Appliances = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Appliances = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Appliances= tbl_cat.grossRev;

#Cámaras y Fotografía
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` = 'cámaras y fotografía')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Photography=tbl_cat.netsales , tbl_dailyReport_local.cogs_Photography=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Photography=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Photography=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Photography=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Photography=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Photography=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Photography = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Photography = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Photography= tbl_cat.grossRev ;


#TV, Video y Audio
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 ='tv, audio y video' )
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_TV_Audio_Video=tbl_cat.netsales , tbl_dailyReport_local.cogs_TV_Audio_Video=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_TV_Audio_Video=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_TV_Audio_Video=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_TV_Audio_Video=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_TV_Audio_Video=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_TV_Audio_Video=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_TV_Audio_Video = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_TV_Audio_Video = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_TV_Audio_Video= tbl_cat.grossRev ;

#Libros
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 = 'libros')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Books=tbl_cat.netsales , tbl_dailyReport_local.cogs_Books=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Books=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Books=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Books=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Books=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Books=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Books = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Books = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Books= tbl_cat.grossRev  ;
#Videojuegos
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` = 'videojuegos')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Videogames=tbl_cat.netsales , tbl_dailyReport_local.cogs_Videogames=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Videogames=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Videogames=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Videogames=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Videogames=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Videogames=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Videogames = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Videogames = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Videogames= tbl_cat.grossRev ;

#Fashion
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 = 'ropa, calzado y accesorios')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Fashion=tbl_cat.netsales , tbl_dailyReport_local.cogs_Fashion=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Fashion=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Fashion=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Fashion=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Fashion=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Fashion=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Fashion = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Fashion = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Fashion= tbl_cat.grossRev  ;


#Cuidado Personal
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 like '%cuidado personal%' or n1 = 'rizadoras' or n1 = 'cabello' or n1 = 'secadores')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Health_Beauty=tbl_cat.netsales , tbl_dailyReport_local.cogs_Health_Beauty=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Health_Beauty=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Health_Beauty=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Health_Beauty=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Health_Beauty=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Health_Beauty=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Health_Beauty = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Health_Beauty = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Health_Beauty= tbl_cat.grossRev  ;


#Teléfonos y GPS
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND( n1 = 'celulares, teléfonos y gps' or n1 = 'ipad 100')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Cellphones=tbl_cat.netsales , tbl_dailyReport_local.cogs_Cellphones=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Cellphones=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Cellphones=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Cellphones=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Cellphones=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Cellphones=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Cellphones = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Cellphones = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Cellphones= tbl_cat.grossRev   ;


 
#Computadores y Tablets    
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 = 'computadoras y tabletas')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Computing=tbl_cat.netsales , tbl_dailyReport_local.cogs_Computing=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Computing=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Computing=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Computing=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Computing=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Computing=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Computing = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Computing = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Computing = tbl_cat.grossRev  ;


#Home y Muebles   
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 = 'línea blanca' or n1 = 'comerdor y cocina' or n1 = 'hogar')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Home=tbl_cat.netsales , 
tbl_dailyReport_local.cogs_Home=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Home=tbl_cat.shipping_fee,
 tbl_dailyReport_local.ShippingCost_Home=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Home=tbl_cat.WH_cost, 
tbl_dailyReport_local.CS_Cost_Home=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Home=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Home = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Home = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Home = tbl_cat.grossRev ;


#Juguetes, Niños y Bebés 
 update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 like 'juguetes%')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Kids_Babies=tbl_cat.netsales , tbl_dailyReport_local.cogs_Kids_Babies=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Kids_Babies=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Kids_Babies=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Kids_Babies=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Kids_Babies=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Kids_Babies=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Kids_Babies = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Kids_Babies = tbl_cat.inboundCosts , 
tbl_dailyReport_local.GrossRev_Kids_Babies = tbl_cat.grossRev ;

#Deportes
 update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` ='Deportes')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Sports=tbl_cat.netsales , 
tbl_dailyReport_local.cogs_Sports=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Sports=tbl_cat.shipping_fee, 
tbl_dailyReport_local.ShippingCost_Sports=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Sports=tbl_cat.WH_cost, 
tbl_dailyReport_local.CS_Cost_Sports=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Sports=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Sports = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Sports = tbl_cat.inboundCosts , 
tbl_dailyReport_local.GrossRev_Sports = tbl_cat.grossRev ;
