INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Venezuela', 
  'marketing_ve.channel_report',
  'finish',
  NOW(),
  MAX(date),
  count(*),
  count(*)
FROM
marketing_ve.channel_report
;

