USE marketing_ve;

UPDATE channel_report c
INNER JOIN marketing.ga_transaction g
ON transaction_id = OrderNum
AND g.country = c.country
SET 
    c.campaign = g.campaign,
    c.source_medium = concat(g.source,' / ', g.medium),
	c.source = g.source,
	c.medium = g.medium,
	c.is_fashion = g.is_fashion,
	c.profile=g.profile,
	c.keyword = g.keyword,
	c.ad_group = g.ad_group,
	c.landing_page = g.landing_page,
	c.is_mobile = g.is_mobile;