USE marketing_ve;

drop temporary table if exists total;
create  temporary table total (index(date,  fk_channel, country))
select date, fk_channel_group,fk_channel, country, sum(gross_revenue) rev, count(1) uds
from channel_performance
where channel_group = 'Display'
group by date, fk_channel, country;

drop temporary table if exists costos;
create  temporary table costos (index(date, id_channel, country))
select M_Other_Cost.country, date, id_channel,sum(value) spent
from development_mx.M_Other_Cost
inner join marketing.channel
on type = channel
inner join development_mx.A_E_BI_ExchangeRate_USD usd
on usd.country = M_Other_Cost.country
and usd.Month_Num = date_format(date, '%Y%m')
group by M_Other_Cost.country, date, id_channel;

update channel_performance cp
inner join 
total t
on cp.date = t.date
and cp.fk_channel = t.fk_channel
and cp.country = t.country
inner join
costos t2
on cp.date = t2.date
and cp.fk_channel = t2.id_channel
and cp.country = t2.country
set cp.marketing_cost =if(t.rev= 0, spent/t.uds,spent*gross_revenue/t.rev);
