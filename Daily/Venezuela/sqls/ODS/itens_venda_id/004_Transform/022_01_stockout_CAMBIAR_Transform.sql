# CAMBIAR A id_sales_order_item TABLE!!
UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
JOIN development_co_project.A_Master AS b
ON a.stockout_order_nr=b.OrderNum
SET
a.stockout_recovered = 1
WHERE (stockout_order_nr <> '')
and status_wms in ('Quebrado','quebra tratada')
AND b.OrderAfterCan=1;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
SET a.stockout_real = 1
WHERE a.is_stockout = 1
AND a.stockout_recovered IS NULL;