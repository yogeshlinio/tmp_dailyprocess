#Query: A 113 U Charge/Fee per PM
UPDATE        Id_Sales_Order_Item_Sample_@v_countryPrefix@ AS Id_Sales_Order_Item_Sample
   INNER JOIN @development@.M_PaymentMethod_Fee AS Fee
           ON     date_format( Id_Sales_Order_Item_Sample.date_order_placed , "%Y%m" ) =  Fee.MonthNum 
              AND Id_Sales_Order_Item_Sample.PaymentMethod = Fee.PaymentMethod 
			  AND Id_Sales_Order_Item_Sample.Installment   = Fee.Installment
SET
    Id_Sales_Order_Item_Sample.Fee          = Fee.Fee,
    Id_Sales_Order_Item_Sample.Extra_Charge = Fee.ExtraCharge;

UPDATE Id_Sales_Order_Item_Sample_@v_countryPrefix@ AS Id_Sales_Order_Item_Sample
SET
    Fee = 5
WHERE
Payment_Method="CashOnDelivery_Payment" AND  date( date_order_placed ) >="2013-3-1";

