USE development_ve;

UPDATE development_ve.Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET
   development_ve.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCost = (development_ve.Id_Sales_Order_Item_Sample_@v_countryPrefix@.PaidPriceAfterTax+development_ve.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingFee)*(4/100);

UPDATE             development_ve.Id_Sales_Order_Item_Sample_@v_countryPrefix@
        INNER JOIN development_ve.OPS_ShippingCostPerItem
                ON     (development_ve.Id_Sales_Order_Item_Sample_@v_countryPrefix@.SKUSimple = OPS_ShippingCostPerItem.SKU_Simple)
                   AND (development_ve.Id_Sales_Order_Item_Sample_@v_countryPrefix@.OrderNum   = OPS_ShippingCostPerItem.OrderNum)
SET
    development_ve.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCost = OPS_ShippingCostPerItem.Shipping_Cost_per_Item;


UPDATE development_ve.Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET
   development_ve.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCost = development_ve.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingFee+development_ve.Id_Sales_Order_Item_Sample_@v_countryPrefix@.CostAfterTax*(1/100)
WHERE (((development_ve.Id_Sales_Order_Item_Sample_@v_countryPrefix@.Date)>="2013-3-1"));


UPDATE        Id_Sales_Order_Item_Sample_@v_countryPrefix@
   INNER JOIN M_Monthly_ShippingCost 
        USING ( MonthNum, CatBP )
SET 
   Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCost = ShippingCostAvg+ CostAfterTax*(1/100);

UPDATE development_ve.Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET
   development_ve.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCost = 0
WHERE prefixCode="SHIP";



