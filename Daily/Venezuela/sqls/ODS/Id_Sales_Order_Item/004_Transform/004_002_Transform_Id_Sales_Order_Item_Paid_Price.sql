USE development_ve;
UPDATE            Id_Sales_Order_Item_Sample_@v_countryPrefix@ AS Id_Sales_Order_Item_Sample
       INNER JOIN Id_Sales_Order_Item_Key_Map
	        USING ( Country , Id_Sales_Order_Item )
       INNER JOIN  @development@.COM_Adjust_Wrong_Price_on_Daily_Report
           ON     ( Id_Sales_Order_Item_Key_Map.SKU_Simple = COM_Adjust_Wrong_Price_on_Daily_Report.SKU_simple)
              AND ( Id_Sales_Order_Item_Key_Map.Order_Num  = COM_Adjust_Wrong_Price_on_Daily_Report.OrderNum)
SET
   Id_Sales_Order_Item_Sample.Price           = COM_Adjust_Wrong_Price_on_Daily_Report.Price,
   Id_Sales_Order_Item_Sample.Price_After_Tax = COM_Adjust_Wrong_Price_on_Daily_Report.Price_after_tax,
   Id_Sales_Order_Item_Sample.Tax             = COM_Adjust_Wrong_Price_on_Daily_Report.Tax
;

