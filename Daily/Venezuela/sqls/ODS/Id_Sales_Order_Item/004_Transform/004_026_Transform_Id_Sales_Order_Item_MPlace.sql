UPDATE       Id_Sales_Order_Item_Sample_@v_countryPrefix@
  INNER JOIN bob_live_ve.sales_order_item AS soi 
          ON Id_Sales_Order_Item_Sample_@v_countryPrefix@.ItemId = soi.id_sales_order_item 
SET
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.isMPlace  = 1,
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.MPlaceFee = ( PriceAfterTax - CostAfterTax ) / PriceAfterTax

WHERE     
          OrderAfterCan = 1 
      and soi.is_option_marketplace = 1
