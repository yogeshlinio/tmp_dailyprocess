#Query: A 125 U Coupon/After Vat coupon for Credit Vouchers
UPDATE Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET
    Paid_Price             = Paid_Price           + Coupon_Value,
	Paid_Price_After_Tax   = Paid_Price_After_Tax + Coupon_Value_AfterTax,
    Coupon_Value           = 0,
    Coupon_Value_After_Tax = 0 
WHERE
Prefix_Code LIKE "%CCE%"        OR
Prefix_Code LIKE "%PRCcre%"     OR
Prefix_Code LIKE "%OPScre%"     OR
Prefix_Code LIKE "%XMAS-LINIO%" OR
Prefix_Code LIKE "SC%"         OR
Prefix_Code LIKE "%UNI%" OR
Prefix_Code LIKE "%ZOOM%" OR
Prefix_Code LIKE "%IPSOS%" OR
Prefix_Code LIKE "%JOHN%" 
;

														   
UPDATE            Id_Sales_Order_Item_Sample_@v_countryPrefix@ AS Id_Sales_Order_Item_Sample
       INNER JOIN Id_Sales_Order_Item_Key_Map
	        USING ( Country , Id_Sales_Order_Item )
       INNER JOIN @development@.Adjust_wrong_vouchers_used_by_CC
              ON     ( Id_Sales_Order_Item_Key_Map.Sku_Simple = Adjust_wrong_vouchers_used_by_CC.SKU_Simple )
                 AND ( Id_Sales_Order_Item_Key_Map.Order_Num  = Adjust_wrong_vouchers_used_by_CC.OrderNum   )
SET
   Id_Sales_Order_Item_Sample.Coupon_Value            = Adjust_wrong_vouchers_used_by_CC.Coupon_money_value,
   Id_Sales_Order_Item_Sample.Coupon_Value_After_Tax  = Adjust_wrong_vouchers_used_by_CC.After_vat_coupon;


   