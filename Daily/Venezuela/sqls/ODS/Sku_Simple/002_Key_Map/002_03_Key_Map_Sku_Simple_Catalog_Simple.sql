UPDATE            Sku_Simple_Key_Map_Sample_@v_countryPrefix@ AS Sku_Simple_Key_Map_Sample
       INNER JOIN @bob_live@.catalog_simple
	        USING ( id_catalog_simple )
SET
  Sku_Simple_Key_Map_Sample.id_catalog_attribute_option_global_repurchase = catalog_simple.fk_catalog_attribute_option_global_repurchase

  ;