DROP TEMPORARY TABLE IF EXISTS operations_@v_countryPrefix@ .TMP3analyst ; 
CREATE TEMPORARY TABLE operations_@v_countryPrefix@ .TMP3analyst
SELECT d.*, e.nome
FROM
(SELECT
		b.user_id,
		b.return_id,
		b. STATUS,
		b.changed_at,
		c.item_id
	FROM
		@v_wmsprod@.inverselogistics_status_history b
	JOIN @v_wmsprod@.inverselogistics_devolucion c ON b.return_id = c.id) d 
INNER JOIN @v_wmsprod@.usuarios as e ON d.user_id = e.usuarios_id
WHERE
	d. STATUS = 6;
	
create index item on operations_@v_countryPrefix@.TMP3analyst(item_id);


UPDATE return_il_Sample_@v_countryPrefix@ as return_il_Sample
INNER JOIN operations_@v_countryPrefix@.TMP3analyst as d ON return_il_Sample.item_id = d.item_id
SET return_il_Sample.analyst_il = d.nome,
return_il_Sample.date_inbound_il  = d.changed_at
WHERE
	d. STATUS = 6;


   
