UPDATE return_il_Sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays AS b
	ON DATE(a.date_customer_request_il)= b.date_first
		AND DATE(curdate())= b.date_last
SET 
backlog_track_il = IF(b.workdays >2,1,0),
workdays_backlog_track_il = IF(b.workdays >2,(b.workdays-2),0)
WHERE date_first_track_il IS NULL
AND date_inbound_il IS NULL
AND date_quality_il IS NULL
AND date_closed_il IS null
AND date_customer_request_il IS NOT NULL;


UPDATE return_il_Sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays AS b
	ON DATE(a.date_inbound_il)= b.date_first
		AND DATE(curdate())= b.date_last
SET 
backlog_quality_il = IF(b.workdays >3,1,0),
workdays_backlog_quality_il = IF(b.workdays >3,(b.workdays-3),0)
WHERE date_quality_il IS NULL
AND date_closed_il IS NULL
AND is_returned=1;


UPDATE return_il_Sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays AS b
	ON date(a.date_quality_il)= b.date_first
		AND date(curdate())= b.date_last
SET 
backlog_refund_reversion_il = IF(b.workdays>20,1,0),
workdays_backlog_refund_reversion_il = IF(b.workdays>20,(b.workdays-20),0)
WHERE date_cash_refunded_il IS NULL
AND date_voucher_refunded_il IS NULL
AND date_closed_il IS NULL
AND date_quality_il IS NOT NULL
AND is_returned = 1
AND action_il='Reembolso tarjeta o cuenta (10-20 días hábiles)';

UPDATE return_il_Sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays AS b
	ON date(a.date_quality_il)= b.date_first
		AND date(curdate())= b.date_last
SET 
backlog_refund_voucher_il = IF(b.workdays>2,1,0),
workdays_backlog_refund_voucher_il = IF(b.workdays>2,(b.workdays-2),0)
WHERE date_cash_refunded_il IS NULL
AND date_voucher_refunded_il IS NULL
AND date_closed_il IS NULL
AND date_quality_il IS NOT NULL
AND is_returned =1
AND action_il='Reembolso en crédito Linio (2 días hábiles)';









