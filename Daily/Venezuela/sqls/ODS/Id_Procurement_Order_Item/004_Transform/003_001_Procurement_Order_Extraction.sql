UPDATE            Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
    INNER JOIN Id_Procurement_Order_Item_Key_Map_Sample_@v_countryPrefix@
	    USING ( country , Id_Procurement_Order_Item )
    INNER JOIN @procurement_live@.procurement_order AS b
	    USING ( Id_Procurement_Order )
SET
   /*Defaults and Simple Extraction*/
	a.fk_procurement_order_type = b.fk_procurement_order_type;

