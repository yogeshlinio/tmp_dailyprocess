UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
SET 
	month_payment = DATE_FORMAT(date_paid, "%x-%m"),
	week_payment = operations_@v_countryPrefix@.week_iso (date_paid);
	
	UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
SET 
	week_po_created = operations_@v_countryPrefix@.week_iso (date_po_created),
	week_goods_received = operations_@v_countryPrefix@.week_iso (date_goods_received),
	month_po_created = DATE_FORMAT(date_po_created, "%x-%m"),
	month_goods_received = DATE_FORMAT(date_goods_received,"%x-%m"),
	goods_received_last_15 = IF(DATEDIFF(curdate(),date_goods_received) <= 15,1,0),
	goods_received_last_30 = IF(DATEDIFF(curdate(),date_goods_received) <= 30,1,0),
	goods_received_last_45 = IF(DATEDIFF(curdate(),date_goods_received) <= 45,1,0);
	
	
UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@
SET date_promised_procurement = date_delivery_bob_original_calculated
WHERE purchase_order_type<>'Stock';