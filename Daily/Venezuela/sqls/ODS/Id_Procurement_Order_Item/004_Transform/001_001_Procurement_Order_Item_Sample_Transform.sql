UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
SET a.purchase_order_type='Crossdocking'
WHERE (a.fk_procurement_order_type=6 OR a.fk_procurement_order_type=2);

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
SET a.purchase_order_type='Own Warehouse'
WHERE a.fk_procurement_order_type=1;

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
SET a.purchase_order_type='Consignment'
WHERE (a.fk_procurement_order_type=7 OR a.fk_procurement_order_type=11);

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
SET a.purchase_order_type='Materia Prima'
WHERE a.fk_procurement_order_type=8;

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
SET a.purchase_order_type='Invoice'
WHERE a.fk_procurement_order_type=9;

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
SET a.purchase_order_type='Inbound'
WHERE a.fk_procurement_order_type=10;
