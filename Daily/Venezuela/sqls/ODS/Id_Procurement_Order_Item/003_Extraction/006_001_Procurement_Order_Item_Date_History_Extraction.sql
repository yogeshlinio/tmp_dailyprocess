UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
INNER JOIN @procurement_live@.procurement_order_item_date_history AS b 
	ON a.id_procurement_order_item = b.fk_procurement_order_item
SET 
	a.date_goods_received = b.delivery_real_date,
	#a.date_po_confirmed = b.confirm_date,
	a.date_collection_scheduled = DATE(collect_scheduled_date),
	a.date_collection_negotiated = DATE(collect_negotiated_date),
	a.date_delivery_calculated_bob = DATE(delivery_bob_calculated_date),
	a.date_delivery_scheduled = DATE(delivery_scheduled_date),
	a.date_delivery_bob_original_calculated = DATE(b.delivery_bob_original_calculated_date);

#Refencia que usa itens_venda en external references
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'ODS.Id_Procurement_Order_Item',
  'dates_po',
  NOW(),
  NOW(),
  0,
  0
;