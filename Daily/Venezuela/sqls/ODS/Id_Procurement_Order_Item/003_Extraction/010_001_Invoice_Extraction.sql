UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
INNER JOIN @procurement_live@.invoice_item AS b
	ON a.id_procurement_order_item = b.fk_procurement_order_item
INNER JOIN @procurement_live@.invoice AS c
	ON b.fk_invoice = c.id_invoice
SET 
 a.invoice_number = c.invoice_nr,
 a.date_invoice_issued = DATE(c.issue_date),
 a.date_invoice_created = DATE(c.created_at) -- ,
 #a.date_invoice_receipt = DATE(c.invoice_date)
 ;