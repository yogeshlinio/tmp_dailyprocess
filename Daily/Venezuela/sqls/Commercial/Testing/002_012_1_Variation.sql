UPDATE A_Master_Catalog_Sample t
  JOIN bob_live_ve.catalog_simple
    ON t.id_catalog_simple=catalog_simple.id_catalog_simple
JOIN (
select 
        `bob_live_ve`.`catalog_simple_books`.`fk_catalog_simple` AS `fk_catalog_simple`,
        `bob_live_ve`.`catalog_simple_books`.`variation` AS `variation`
from
        `bob_live_ve`.`catalog_simple_books`
where
        ((`bob_live_ve`.`catalog_simple_books`.`variation` is not null)
            and (`bob_live_ve`.`catalog_simple_books`.`variation` <> '...')
            and (`bob_live_ve`.`catalog_simple_books`.`variation` <> '�')
            and (`bob_live_ve`.`catalog_simple_books`.`variation` <> '?')
            and (`bob_live_ve`.`catalog_simple_books`.`variation` <> ',,,')) 
union 
select 
        `bob_live_ve`.`catalog_simple_electronics`.`fk_catalog_simple` AS `fk_catalog_simple`,
        `bob_live_ve`.`catalog_simple_electronics`.`variation` AS `variation`
         from
        `bob_live_ve`.`catalog_simple_electronics`
         where
        ((`bob_live_ve`.`catalog_simple_electronics`.`variation` is not null)
            and (`bob_live_ve`.`catalog_simple_electronics`.`variation` <> '...')
            and (`bob_live_ve`.`catalog_simple_electronics`.`variation` <> '�')
            and (`bob_live_ve`.`catalog_simple_electronics`.`variation` <> '?')
            and (`bob_live_ve`.`catalog_simple_electronics`.`variation` <> ',,,')) 
union 
select 
        `bob_live_ve`.`catalog_simple_toys_baby`.`fk_catalog_simple` AS `fk_catalog_simple`,
        `bob_live_ve`.`catalog_simple_toys_baby`.`variation` AS `variation`
    from
        `bob_live_ve`.`catalog_simple_toys_baby`
    where
        ((`bob_live_ve`.`catalog_simple_toys_baby`.`variation` is not null)
            and (`bob_live_ve`.`catalog_simple_toys_baby`.`variation` <> '...')
            and (`bob_live_ve`.`catalog_simple_toys_baby`.`variation` <> '�')
            and (`bob_live_ve`.`catalog_simple_toys_baby`.`variation` <> '?')
            and (`bob_live_ve`.`catalog_simple_toys_baby`.`variation` <> ',,,')) 
union 
select 
        `bob_live_ve`.`catalog_simple_health_beauty`.`fk_catalog_simple` AS `fk_catalog_simple`,
        `bob_live_ve`.`catalog_simple_health_beauty`.`variation` AS `variation`
    from
        `bob_live_ve`.`catalog_simple_health_beauty`
    where
        ((`bob_live_ve`.`catalog_simple_health_beauty`.`variation` is not null)
            and (`bob_live_ve`.`catalog_simple_health_beauty`.`variation` <> '...')
            and (`bob_live_ve`.`catalog_simple_health_beauty`.`variation` <> '�')
            and (`bob_live_ve`.`catalog_simple_health_beauty`.`variation` <> '?')
            and (`bob_live_ve`.`catalog_simple_health_beauty`.`variation` <> ',,,'))
union 
select 
        `bob_live_ve`.`catalog_simple_home_living`.`fk_catalog_simple` AS `fk_catalog_simple`,
        `bob_live_ve`.`catalog_simple_home_living`.`variation` AS `variation`
    from
        `bob_live_ve`.`catalog_simple_home_living`
    where
        ((`bob_live_ve`.`catalog_simple_home_living`.`variation` is not null)
            and (`bob_live_ve`.`catalog_simple_home_living`.`variation` <> '�')
            and (`bob_live_ve`.`catalog_simple_home_living`.`variation` <> '?')
            and (`bob_live_ve`.`catalog_simple_home_living`.`variation` <> ',,,')) 
union 
select 
        `bob_live_ve`.`catalog_simple_media`.`fk_catalog_simple` AS `fk_catalog_simple`,
        `bob_live_ve`.`catalog_simple_media`.`variation` AS `variation`
    from
        `bob_live_ve`.`catalog_simple_media`
    where
        ((`bob_live_ve`.`catalog_simple_media`.`variation` is not null)
            and (`bob_live_ve`.`catalog_simple_media`.`variation` <> '...')
            and (`bob_live_ve`.`catalog_simple_media`.`variation` <> '�')
            and (`bob_live_ve`.`catalog_simple_media`.`variation` <> '?')
            and (`bob_live_ve`.`catalog_simple_media`.`variation` <> ',,,')) 
union 
select 
        `bob_live_ve`.`catalog_simple_other`.`fk_catalog_simple` AS `fk_catalog_simple`,
        `bob_live_ve`.`catalog_simple_other`.`variation` AS `variation`
    from
        `bob_live_ve`.`catalog_simple_other`
    where
        ((`bob_live_ve`.`catalog_simple_other`.`variation` is not null)
            and (`bob_live_ve`.`catalog_simple_other`.`variation` <> '...')
            and (`bob_live_ve`.`catalog_simple_other`.`variation` <> '�')
            and (`bob_live_ve`.`catalog_simple_other`.`variation` <> '?')
            and (`bob_live_ve`.`catalog_simple_other`.`variation` <> ',,,')) 
union 
select 
        `bob_live_ve`.`catalog_simple_sports`.`fk_catalog_simple` AS `fk_catalog_simple`,
        `bob_live_ve`.`catalog_simple_sports`.`variation` AS `variation`
    from
        `bob_live_ve`.`catalog_simple_sports`
    where
        ((`bob_live_ve`.`catalog_simple_sports`.`variation` is not null)
            and (`bob_live_ve`.`catalog_simple_sports`.`variation` <> '...')
            and (`bob_live_ve`.`catalog_simple_sports`.`variation` <> '�')
            and (`bob_live_ve`.`catalog_simple_sports`.`variation` <> '?')
            and (`bob_live_ve`.`catalog_simple_sports`.`variation` <> ',,,')) 
union 
select 
        `bob_live_ve`.`catalog_simple_fashion`.`fk_catalog_simple` AS `fk_catalog_simple`,
        `bob_live_ve`.`catalog_attribute_option_fashion_size`.`name` AS `name`
    from
        (`bob_live_ve`.`catalog_simple_fashion`
        join `bob_live_ve`.`catalog_attribute_option_fashion_size`)
    where
        ((`bob_live_ve`.`catalog_simple_fashion`.`fk_catalog_attribute_option_fashion_size` = `bob_live_ve`.`catalog_attribute_option_fashion_size`.`id_catalog_attribute_option_fashion_size`)
            and (`bob_live_ve`.`catalog_attribute_option_fashion_size`.`name` is not null))
) vari 
ON catalog_simple.id_catalog_simple=vari.fk_catalog_simple
SET t.variation=vari.variation