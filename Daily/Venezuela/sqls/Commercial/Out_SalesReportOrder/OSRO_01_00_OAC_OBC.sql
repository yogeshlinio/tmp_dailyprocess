/*ORDER BEFORE CAN*/
DROP TEMPORARY TABLE IF EXISTS A_E_BI_OBC;
CREATE TEMPORARY TABLE A_E_BI_OBC ( PRIMARY KEY ( OrderNum ) ) 
SELECT 
   OrderNum,
      COUNT(*) AS Items_in_Order, 
      SUM( OrderBeforeCan  ) AS OBC,
      SUM( OrderAfterCan   ) AS OAC,
      SUM( Cancelled       ) AS Cancelled,
      SUM( Pending         ) AS Pending, 
      SUM( Rejected        ) AS Rejected,
      SUM( Returns         ) AS Returns,

      0 AS OBC_Inst,
      0 AS OAC_Inst,
      0 AS Pending_Inst, 
      0 AS Cancelled_Inst,
      0 AS Rejected_Inst,
      0 AS Returns_Inst

FROM
   A_Master
GROUP BY OrderNum;

UPDATE A_E_BI_OBC SET OBC_Inst           = 1 WHERE Items_in_Order = OBC;
UPDATE A_E_BI_OBC SET OAC_Inst           = 1 WHERE OAC      != 0;
UPDATE A_E_BI_OBC SET Returns_Inst       = 1 WHERE OAC_Inst  = 0 AND Returns != 0;
UPDATE A_E_BI_OBC SET Pending_Inst       = 1 WHERE OAC_Inst  = 0 AND Returns_Inst = 0 AND Pending      != 0;
UPDATE A_E_BI_OBC SET Rejected_Inst      = 1 WHERE OAC_Inst  = 0 AND Returns_Inst = 0 AND Pending_Inst  = 0 AND Rejected != 0;
UPDATE A_E_BI_OBC SET Cancelled_Inst = 1 WHERE OAC_Inst  = 0 AND Returns_Inst = 0 AND Pending_Inst  = 0 AND Rejected  = 0 AND ( OBC > 0 OR Cancelled > 0  );
  
UPDATE            Out_SalesReportOrder 
       INNER JOIN A_E_BI_OBC      USING ( OrderNum )
SET
    Out_SalesReportOrder.OrderBeforeCan     = A_E_BI_OBC.OBC_Inst           ,
    Out_SalesReportOrder.OrderAfterCan      = A_E_BI_OBC.OAC_Inst           ,
    Out_SalesReportOrder.Cancelled     = A_E_BI_OBC.Cancelled_Inst,
    Out_SalesReportOrder.Returns       = A_E_BI_OBC.RETURNS_Inst,
    Out_SalesReportOrder.Pending       = A_E_BI_OBC.Pending_Inst,
    Out_SalesReportOrder.Rejected      = A_E_BI_OBC.Rejected_Inst
;
