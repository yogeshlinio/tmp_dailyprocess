#Query: CACcustomer
drop table if EXISTS development_ve.CACcustomer;
create table development_ve.CACcustomer
(
 `CustomerNum` VARCHAR (20) NOT NULL,
 `FirstNetOrder` VARCHAR (20) NOT NULL,
 `FirstGrossOrder` VARCHAR (20) NOT NULL,
 `CohortMonth` INT NOT NULL,
 `Campaign` VARCHAR (50) NOT NULL,
 `ChannelGroup` VARCHAR (36) NOT NULL,
 `Channel` VARCHAR(36) NOT NULL,
 `SourceMedium` VARCHAR (36) NOT NULL
) 
SELECT
  DISTINCT CustomerNum AS CustomerNum,
           FirstOrderNum AS FirstNetOrder,
           CohortMonthNum AS CohortMonth,
           Campaign AS Campaign,
           Channelgroup AS ChannelGroup ,
           Source_medium AS SourceMedium 
from development_ve.A_Master
where FirstOrderNum = OrderNum
 and Channelgroup = 'CAC Deals'
;

create index CustomerNum on development_ve.CACcustomer(CustomerNum);

UPDATE development_ve.A_Master INNER JOIN development_ve.CACcustomer on 
A_Master.CustomerNum = CACcustomer.CustomerNum
SET 
    A_Master.CACCustomer = 1;

UPDATE development_ve.A_Master set CACCustomer=0 where CACCustomer is null;