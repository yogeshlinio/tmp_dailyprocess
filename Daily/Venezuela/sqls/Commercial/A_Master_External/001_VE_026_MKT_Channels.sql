#Query: N1:
UPDATE development_ve.A_Master INNER JOIN marketing_ve.channel_report
SET 
    A_Master.Source_medium = channel_report.source_medium,
		A_Master.Campaign = channel_report.Campaign,
		A_Master.Channel = channel_report.Channel,
		A_Master.Channelgroup = channel_report.Channel_group
WHERE
	A_Master.IdSalesOrder = channel_report.orderID;

	
	

-- *** VENEZUELA ***

INSERT INTO production.table_monitoring_log(
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Venezuela', 
  'A_Master.ChannelGroup',
  'Finish',
  NOW(),
  MAX(date),
  count(*),
  count(distinct channelgroup)
FROM
  development_ve.A_Master;