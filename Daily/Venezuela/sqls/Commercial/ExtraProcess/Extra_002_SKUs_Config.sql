USE development_ve;
SET @MonthNum = "000000";
SELECT date_format( CURRENT_DATE() -INTERVAL 1 Day ,"%Y%m" )  INTO @MonthNum;
#DROP TABLE IF EXISTS A_SKUsVisible;
CREATE TABLE IF NOT EXISTS A_SKUsVisible
(
   MonthNum int,
   SKU_Simple varchar(25),
   SKU_Config varchar(25),
   SKU_Name   varchar(50),
   isMPlace   int,
   UpdatedAt  datetime,
   PRIMARY KEY ( MonthNum, SKU_Simple  ),
   KEY( SKU_Simple )
)
;

REPLACE development_ve.A_SKUsVisible
SELECT
  @MonthNum   AS MonthNum,
  SKU_Simple  AS SKU_Simple,
  SKU_Config  AS SKU_Config,
  SKU_Name    AS SKU_Name,
  isMarketPlace    AS isMPlace,
  now()       AS UpdatedAt
FROM
  development_ve.A_Master_Catalog
WHERE
  isVisible = 1
;

