USE development_ve;
UPDATE             development_ve.A_Master_Sample
       INNER JOIN  COM_Adjust_Wrong_Price_on_Daily_Report
           ON     ( development_ve.A_Master_Sample.SKUSimple     = COM_Adjust_Wrong_Price_on_Daily_Report.SKU_simple)
              AND (development_ve.A_Master_Sample.OrderNum = COM_Adjust_Wrong_Price_on_Daily_Report.OrderNum)
SET
   development_ve.A_Master_Sample.Price                = COM_Adjust_Wrong_Price_on_Daily_Report.Price,
   development_ve.A_Master_Sample.PriceAfterTax        = COM_Adjust_Wrong_Price_on_Daily_Report.Price_after_tax,
   development_ve.A_Master_Sample.Tax                  = COM_Adjust_Wrong_Price_on_Daily_Report.Tax
;

