DROP   TEMPORARY TABLE IF EXISTS development_ve.A_Rejections_Shipped;
CREATE TEMPORARY TABLE development_ve.A_Rejections_Shipped ( INDEX ( ItemID ) )
SELECT
   fk_sales_order_item as ItemID
FROM
   @bob_live@.sales_order_item_status_history 
WHERE 
   fk_sales_order_item_status  = 5
GROUP BY ItemID
;


DROP   TEMPORARY TABLE IF EXISTS development_ve.A_Rejections_Delivered;
CREATE TEMPORARY TABLE development_ve.A_Rejections_Delivered ( INDEX ( ItemID ) )
SELECT
   fk_sales_order_item as ItemID
FROM
   @bob_live@.sales_order_item_status_history 
WHERE 
   fk_sales_order_item_status  = 52
GROUP BY ItemID
;

DROP   TEMPORARY TABLE IF EXISTS development_ve.A_Rejections_Shipped_NoDelivered;
CREATE TEMPORARY TABLE development_ve.A_Rejections_Shipped_NoDelivered ( INDEX ( ItemID ) )
SELECT
   ItemID
FROM
   development_ve.A_Rejections_Shipped 
WHERE 
   ItemId  not IN ( SELECT * FROM development_ve.A_Rejections_Delivered )
;


UPDATE            development_ve.A_Master_Sample 
       INNER JOIN development_ve.A_Rejections_Shipped_NoDelivered
            USING ( ItemID )
SET
   development_ve.A_Master_Sample.Rejected = 1

WHERE
#       Out_SalesReportItem.Cancellations = 1
#   AND 
       development_ve.A_Master_Sample.Status in (
                                       "canceled" ,
                                       "cancelled" ,
                                       "refund_needed" ,
                                       "store_credit_issued" ,
                                       "clarify_refund_not_processed",
                                       "store_credit_needed",
                                       "store_credit_issued",
                                       "clarify_store_credit_not_issued" ,
                                       "refunded"
                                     )

;
