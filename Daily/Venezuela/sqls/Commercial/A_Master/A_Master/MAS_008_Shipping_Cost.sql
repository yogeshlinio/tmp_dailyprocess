UPDATE  A_Master_Sample
SET
    A_Master_Sample.ShippingCost = ( A_Master_Sample.PaidPriceAfterTax+ A_Master_Sample.ShippingFee)*(4/100);

UPDATE              A_Master_Sample
        INNER JOIN  OPS_ShippingCostPerItem
                ON     ( A_Master_Sample.SKUSimple = OPS_ShippingCostPerItem.SKU_Simple)
                   AND ( A_Master_Sample.OrderNum   = OPS_ShippingCostPerItem.OrderNum)
SET
     A_Master_Sample.ShippingCost = OPS_ShippingCostPerItem.Shipping_Cost_per_Item;


UPDATE  A_Master_Sample
SET
    A_Master_Sample.ShippingCost =  A_Master_Sample.ShippingFee+ A_Master_Sample.CostAfterTax*(1/100)
WHERE ((( A_Master_Sample.Date)>="2013-3-1"));


UPDATE        A_Master_Sample
   INNER JOIN M_Monthly_ShippingCost 
        USING ( MonthNum, CatBP )
SET 
   A_Master_Sample.ShippingCost = ShippingCostAvg+ CostAfterTax*(1/100);

UPDATE  A_Master_Sample
SET
    A_Master_Sample.ShippingCost = 0
WHERE prefixCode="SHIP";



