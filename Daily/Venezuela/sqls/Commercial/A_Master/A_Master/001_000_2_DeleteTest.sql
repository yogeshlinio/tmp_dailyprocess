/*
DELETE FROM A_Master_Sample
WHERE
	 A_Master_Sample.firstName LIKE "%test%"
;
*/
#Preadjust
/*
id:28 (BI Reporting Issues Tracker)
Solicitud: Leonardo Fraile
Descripción: Please delete in A_Master all items that come from supplier Rocket Internet GmbH.
Motivo: Incorrect Data
*/
DELETE
FROM
	A_Master_Sample
WHERE
	FirstName LIKE '%test%'
OR FirstName LIKE '%prueba%'
OR Supplier = 'Rocket Internet GmbH' ;

