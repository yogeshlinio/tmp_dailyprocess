UPDATE              A_Master_Sample
       INNER JOIN  COM_Adjust_Wrong_Price_on_Daily_Report
           ON     (  A_Master_Sample.SKUSimple     = COM_Adjust_Wrong_Price_on_Daily_Report.SKU_simple)
              AND ( A_Master_Sample.OrderNum = COM_Adjust_Wrong_Price_on_Daily_Report.OrderNum)
SET
    A_Master_Sample.Price                = COM_Adjust_Wrong_Price_on_Daily_Report.Price,
    A_Master_Sample.PriceAfterTax        = COM_Adjust_Wrong_Price_on_Daily_Report.Price_after_tax,
    A_Master_Sample.Tax                  = COM_Adjust_Wrong_Price_on_Daily_Report.Tax
;

