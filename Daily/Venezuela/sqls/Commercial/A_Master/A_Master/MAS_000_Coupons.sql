UPDATE  A_Master_Sample
SET
    PaidPrice           = PaidPrice         + CouponValue,
	PaidPriceAfterTax   = PaidPriceAfterTax + CouponValueAfterTax,
    CouponValue         = 0,
    CouponValueAfterTax = 0 
WHERE
UPPER(PrefixCode) in ("UNI","CCE","PRCCRE","OPSCRE","SC","XMAS-LINIO","XMAS-LINIO-","XMAX-LINIO","ZOOM","IPSOS","JOHN","BONO");


														   
UPDATE             A_Master_Sample
       INNER JOIN  Adjust_wrong_vouchers_used_by_CC
              ON     ( A_Master_Sample.SkuSimple    = Adjust_wrong_vouchers_used_by_CC.SKU_Simple)
                 AND ( A_Master_Sample.OrderNum     = Adjust_wrong_vouchers_used_by_CC.OrderNum)
SET
    A_Master_Sample.CouponValue          = Adjust_wrong_vouchers_used_by_CC.Coupon_money_value,
    A_Master_Sample.CouponValueAfterTax  = Adjust_wrong_vouchers_used_by_CC.After_vat_coupon;


   