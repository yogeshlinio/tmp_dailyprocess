 #Query: A 113 U Charge/Fee per PM
UPDATE        A_Master_Sample
   INNER JOIN M_PaymentMethod_Fee AS Fee
        USING ( MonthNum, PaymentMethod, Installment )
SET
    A_Master_Sample.Fees        = Fee.Fee,
    A_Master_Sample.ExtraCharge = Fee.ExtraCharge;

UPDATE development_ve.A_Master_Sample
SET
    Fees = 5
WHERE
(((PaymentMethod)="CashOnDelivery_Payment") AND ((Date)>="2013-3-1"));
