DROP   TEMPORARY TABLE IF EXISTS development_ve.A_Returns;
CREATE TEMPORARY TABLE development_ve.A_Returns ( INDEX ( fk_sales_order_item  ) )
SELECT
 fk_sales_order_item
FROM
    bob_live_ve.sales_order_item_status_history
WHERE
   bob_live_ve.sales_order_item_status_history.fk_sales_order_item_status IN ( 111 , 112 )
GROUP BY fk_sales_order_item
;

REPLACE development_ve.A_Returns
SELECT 
   c.item_id as fk_sales_order_item
FROM          wmsprod_ve.inverselogistics_status_history b 
   INNER JOIN wmsprod_ve.inverselogistics_devolucion c 
           ON b.return_id = c.id
WHERE
   b.status = 6;


UPDATE           development_ve.A_Returns
      INNER JOIN development_ve.A_Master_Sample
              ON development_ve.A_Returns.fk_sales_order_item = development_ve.A_Master_Sample.ItemID 
SET
    development_ve.A_Master_Sample.Returns = 1
;

#Query: A 102 U OrderBefore-AfterCan
UPDATE development_ve.A_Master_Sample
SET 
     development_ve.A_Master_Sample.Cancellations  = IF(    development_ve.A_Master_Sample.Cancellations = 1 
                                                         AND development_ve.A_Master_Sample.Returns       = 0 , 1 , 0 )
;

/*
* DATE           2013/12/05
* DEVELOPED BY   Carlos Antonio Mondragón Soria
* DESCRIPTION MKT - Status Wrong in Returns 
*/
UPDATE development_ve.A_Master_Sample
SET
   OrderAfterCan = 0,
   Cancellations = 0,
   Pending       = 0
WHERE
   Returns = 1;
