USE development_ve;

UPDATE development_ve.A_Master_Sample
SET
   development_ve.A_Master_Sample.ShippingCost = (development_ve.A_Master_Sample.PaidPriceAfterTax+development_ve.A_Master_Sample.ShippingFee)*(4/100);

UPDATE             development_ve.A_Master_Sample
        INNER JOIN development_ve.OPS_ShippingCostPerItem
                ON     (development_ve.A_Master_Sample.SKUSimple = OPS_ShippingCostPerItem.SKU_Simple)
                   AND (development_ve.A_Master_Sample.OrderNum   = OPS_ShippingCostPerItem.OrderNum)
SET
    development_ve.A_Master_Sample.ShippingCost = OPS_ShippingCostPerItem.Shipping_Cost_per_Item;


UPDATE development_ve.A_Master_Sample
SET
   development_ve.A_Master_Sample.ShippingCost = development_ve.A_Master_Sample.ShippingFee+development_ve.A_Master_Sample.CostAfterTax*(1/100)
WHERE (((development_ve.A_Master_Sample.Date)>="2013-3-1"));


UPDATE        A_Master_Sample
   INNER JOIN M_Monthly_ShippingCost 
        USING ( MonthNum, CatBP )
SET 
   A_Master_Sample.ShippingCost = ShippingCostAvg+ CostAfterTax*(1/100);

UPDATE development_ve.A_Master_Sample
SET
   development_ve.A_Master_Sample.ShippingCost = 0
WHERE prefixCode="SHIP";



