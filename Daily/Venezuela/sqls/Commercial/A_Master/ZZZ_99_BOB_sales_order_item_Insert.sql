CREATE TABLE IF NOT EXISTS A_Master LIKE A_Master_Template;
ALTER TABLE A_Master_Sample
            DROP COLUMN fk_customer_address_region      ,
            DROP COLUMN fk_sales_order_address_shipping ,
            DROP COLUMN fk_sales_order_item_status      ,
            DROP COLUMN fk_catalog_shipment_type        
;

#REPLACE A_Master SELECT    A_Master_Sample.* FROM A_Master_Sample;
DROP TABLE IF EXISTS A_Master;
CREATE TABLE A_Master LIKE A_Master_Template;
INSERT A_Master
SELECT * FROM A_Master_Sample; 

DROP TABLE IF EXISTS A_Master_Backup;
CREATE TABLE A_Master_Backup LIKE A_Master;
INSERT A_Master_Backup
SELECT * FROM A_Master;