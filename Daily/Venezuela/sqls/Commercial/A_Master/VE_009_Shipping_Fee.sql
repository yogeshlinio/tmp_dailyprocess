/*
* MX_009_001 Shipping Fee
*/ 
#Query: M1_ShipFee_table
UPDATE A_Master_Sample 
SET
   ShippingFeeAfterTax = A_Master_Sample.ShippingFee / ( 1 +  ( A_Master_Sample.TaxPercent / 100 ) )          
;
