UPDATE       A_Master_Sample
  INNER JOIN bob_live_ve.sales_order_item AS soi 
          ON A_Master_Sample.ItemId = soi.id_sales_order_item 
SET
    A_Master_Sample.isMPlace  = 1,
    A_Master_Sample.MPlaceFee = ( PriceAfterTax - CostAfterTax ) / PriceAfterTax

WHERE     
          OrderAfterCan = 1 
      and soi.is_option_marketplace = 1;
	  
UPDATE         A_Master_Sample
SET 
	A_Master_Sample.CostAfterTax = A_Master_Sample.PriceAfterTax - (PriceAfterTax * MPlaceFee),
	A_Master_Sample.Cost         = ( A_Master_Sample.PriceAfterTax - (PriceAfterTax * MPlaceFee)) * ( 1 + ( taxPercent / 100 ) )
	where 
	isMPlace      = 1 
	and OrderAfterCan =1 
;
