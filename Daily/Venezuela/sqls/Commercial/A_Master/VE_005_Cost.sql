call  production.monitoring_step( 'ODS.Id_Procurement_Order_Item' , "Venezuela", 'Cost_Oms' , 1000 );

/*
*  Costo de BOB
*/
UPDATE        A_Master_Sample
   INNER JOIN A_Master_Catalog
           ON A_Master_Sample.SKUSimple = A_Master_Catalog.SKU_simple
SET
   A_Master_Sample.Cost         = A_Master_Catalog.cost,
   A_Master_Sample.CostAfterTax = A_Master_Catalog.cost / 
                                                 ( 1 +  ( A_Master_Sample.TaxPercent / 100 ) )          
WHERE
      A_Master_Sample.Cost is null 
   OR A_Master_Sample.Cost = 0
;
   
/*
* Costo Promedio
*/    
UPDATE        A_Master_Sample
   INNER JOIN A_Master_Catalog p
           ON A_Master_Sample.SKUSimple= p.sku_simple 
SET 
   A_Master_Sample.Cost = p.Cost_OMS  ,
   A_Master_Sample.CostAftertax = p.Cost_OMS /( ( 100 +  A_Master_Sample.TaxPercent )/100 )
   
WHERE
 p.Cost_OMS > 0 
  and
  (A_Master_Sample.Cost is null 
	   OR A_Master_Sample.Cost = 0)
;

/*
* VE_005_Cost_OMS
*/
UPDATE             A_Master_Sample  
        INNER JOIN ODS.Id_Procurement_Order_Item_Sample_ve b 
	            ON A_Master_Sample.ItemId = b.item_id
SET 
    A_Master_Sample.Cost           = b.cost_oms,
    A_Master_Sample.CostAftertax   = b.cost_oms_after_tax
;

-- UPDATE development_ve.A_Master_dev
-- INNER JOIN wmsprod_ve.itens_venda ON A_Master_dev.ItemID = itens_venda.item_id
-- SET A_Master_dev.Cost = itens_venda.cost_item,
--  A_Master_dev.CostAfterTax = itens_venda.cost_item / (
-- 	1 + (
-- 		A_Master_dev.TaxPercent / 100
-- 	)
-- );
	   
	   
 /*
UPDATE      A_Master_Sample t
       JOIN tbl_bi_ops_delivery d
ON t.item=d.item_id
JOIN M_Cambios_Costos cc
ON d.op=cc.sku
SET t.cost_oms=cc.cost
WHERE cc.is_config=0 AND cc.is_op=1 AND cc.is_coupon=0 AND (cc.cost_to_apply=0 OR cc.cost_to_apply=1)
AND t.date >= CASE WHEN no_range=1 THEN t.date 
                      ELSE cc.from_date END 
  AND t.date <= CASE WHEN no_range=1 THEN t.date
                      ELSE cc.to_date END;


*/

/*
Correccion solicitadad por: Leonardo Fraile el 20 de enero de 2014 a las 14:55
*/
UPDATE development_ve.A_Master
SET Cost = 450,
	CostAfterTax = 401.785
WHERE
	SKUconfig = 'KR108EL75SCALAVEN'
AND OrderNum IN (200565542, 200417542, 200695542);


UPDATE              development_ve.A_Master_Sample
       INNER JOIN   development_ve.COM_Adjust_Wrong_Costs_on_Dialy_Report
           ON     ( development_ve.A_Master_Sample.SKUSimple = COM_Adjust_Wrong_Costs_on_Dialy_Report.SKU_Simple)
              AND ( development_ve.A_Master_Sample.OrderNum = COM_Adjust_Wrong_Costs_on_Dialy_Report.OrderNum)
SET
    development_ve.A_Master_Sample.Cost            = COM_Adjust_Wrong_Costs_on_Dialy_Report.Cost,
    development_ve.A_Master_Sample.CostAfterTax    = COM_Adjust_Wrong_Costs_on_Dialy_Report.Cost_after_tax;


	