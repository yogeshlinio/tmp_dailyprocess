USE development_ve;
UPDATE         A_Master_Sample
    INNER JOIN sc_live_ve_reporting.sales_order_item
            ON sales_order_item.src_id = ItemId
SET
  A_Master_Sample.cost         =   sales_order_item.`unit_price` - COALESCE( sales_order_item.`paid_commission` , 0 ) ,
  A_Master_Sample.costAfterTax = ( sales_order_item.`unit_price` - COALESCE( sales_order_item.`paid_commission`  , 0 ) ) / ( 1 + ( TaxPercent / 100  )  )  
;

UPDATE A_Master_Sample
INNER JOIN marketplace.sku_commission ON sku_simple = SKUSimple
SET Cost = Price - Price * (percentage / 100),
 CostAfterTax = PriceAfterTax - PriceAfterTax * (percentage / 100)
WHERE
	(Cost = 0 OR Cost IS NULL OR Cost=Price)
AND A_Master_Sample.country=sku_commission.country
AND isMPlace = 1;