TRUNCATE tbl_catalog_product_stock ;
INSERT INTO tbl_catalog_product_stock (
	fk_catalog_simple,
	sku,
	reservedbob,
	stockbob,
	availablebob
) SELECT
	   catalog_simple.id_catalog_simple,
	   catalog_simple.sku,
     IF (sum(is_reserved) IS NULL,0,sum(is_reserved)) AS reservedBOB,
     ifnull(catalog_stock.quantity, 0) AS stockbob,
     ifnull(catalog_stock.quantity, 0) - IF (sum(sales_order_item.is_reserved) IS NULL,0, sum(is_reserved)) AS availablebob
FROM
	(
		           bob_live_ve.catalog_source
		INNER JOIN bob_live_ve.catalog_simple 
            ON catalog_source.fk_catalog_simple = catalog_simple.id_catalog_simple
    INNER JOIN bob_live_ve.catalog_config
            ON catalog_simple.fk_catalog_config = catalog_config.id_catalog_config
 	LEFT  JOIN bob_live_ve.catalog_stock 
	        ON catalog_source.id_catalog_source = catalog_stock.fk_catalog_source
	)
    LEFT JOIN bob_live_ve.sales_order_item ON sales_order_item.sku = catalog_simple.sku
GROUP BY
	catalog_simple.sku
ORDER BY
	catalog_stock.quantity DESC;


UPDATE       tbl_catalog_product_stock 
  INNER JOIN bob_live_ve.catalog_warehouse_stock 
          ON tbl_catalog_product_stock.fk_catalog_simple = catalog_warehouse_stock.fk_catalog_simple
SET
tbl_catalog_product_stock.ownstock= catalog_warehouse_stock.quantity;

UPDATE        tbl_catalog_product_stock 
   INNER JOIN bob_live_ve.catalog_supplier_stock 
           ON tbl_catalog_product_stock.fk_catalog_simple = catalog_supplier_stock.fk_catalog_simple
SET 
tbl_catalog_product_stock.supplierstock= catalog_supplier_stock.quantity;

UPDATE tbl_catalog_product_stock set availablebob = 0 where availablebob < 0;
