INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Venezuela', 
  'DailyReport',
  'finish',
  NOW(),
  MAX(`Date Placed`),
  count(*),
  count(*)
FROM
   development_ve.DailyReport 
;


