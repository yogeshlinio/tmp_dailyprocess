/*
 Nombre: content_sharing_tool.sql
 Autor: Luis Ochoa, Rafael Guzman
 Fecha Creación:28/01/2014
 Descripción:
 Version: 1.0
 */
 
delete from production.catalog_config;

alter table production.catalog_config AUTO_INCREMENT = 1;

insert into production.catalog_config(country, id_catalog_config, fk_catalog_brand, sku_config, status_supplier_config, description, name, pet_status, pet_approved, created_at, updated_at, updated_by_config, activated_at, sku_supplier_config, creation_source_config, model, imported, product_measures, product_weight, package_height, package_length, package_width, package_weight, short_description, color, product_warranty, fk_supplier, status, sku_simple) select 'Mexico', c.id_catalog_config, c.fk_catalog_brand, c.sku, c.status_supplier_config, c.description, c.name, c.pet_status, c.pet_approved, c.created_at, c.updated_at, c.updated_by_config, c.activated_at, c.sku_supplier_config, c.creation_source_config, c.model, c.imported, c.product_measures, c.product_weight, c.package_height, c.package_length, c.package_width, c.package_weight, c.short_description, c.color, c.product_warranty, c.fk_supplier, c.status, s.sku from bob_live_mx.catalog_config c, bob_live_mx.catalog_simple s where s.fk_catalog_config=c.id_catalog_config;

insert into production.catalog_config(country, id_catalog_config, fk_catalog_brand, sku_config, status_supplier_config, description, name, pet_status, pet_approved, created_at, updated_at, updated_by_config, activated_at, sku_supplier_config, creation_source_config, model, imported, product_measures, product_weight, package_height, package_length, package_width, package_weight, short_description, color, product_warranty, fk_supplier, status, sku_simple) select 'Colombia', c.id_catalog_config, c.fk_catalog_brand, c.sku, c.status_supplier_config, c.description, c.name, c.pet_status, c.pet_approved, c.created_at, c.updated_at, c.updated_by_config, c.activated_at, c.sku_supplier_config, c.creation_source_config, c.model, c.imported, c.product_measures, c.product_weight, c.package_height, c.package_length, c.package_width, c.package_weight, c.short_description, c.color, c.product_warranty, c.fk_supplier, c.status, s.sku from bob_live_co.catalog_config c, bob_live_co.catalog_simple s where s.fk_catalog_config=c.id_catalog_config;

insert into production.catalog_config(country, id_catalog_config, fk_catalog_brand, sku_config, status_supplier_config, description, name, pet_status, pet_approved, created_at, updated_at, updated_by_config, activated_at, sku_supplier_config, creation_source_config, model, imported, product_measures, product_weight, package_height, package_length, package_width, package_weight, short_description, color, product_warranty, fk_supplier, status, sku_simple) select 'Peru', c.id_catalog_config, c.fk_catalog_brand, c.sku, c.status_supplier_config, c.description, c.name, c.pet_status, c.pet_approved, c.created_at, c.updated_at, c.updated_by_config, c.activated_at, c.sku_supplier_config, c.creation_source_config, c.model, c.imported, c.product_measures, c.product_weight, c.package_height, c.package_length, c.package_width, c.package_weight, c.short_description, c.color, c.product_warranty, c.fk_supplier, c.status, s.sku from bob_live_pe.catalog_config c, bob_live_pe.catalog_simple s where s.fk_catalog_config=c.id_catalog_config;

insert into production.catalog_config(country, id_catalog_config, fk_catalog_brand, sku_config, status_supplier_config, description, name, pet_status, pet_approved, created_at, updated_at, updated_by_config, activated_at, sku_supplier_config, creation_source_config, model, imported, product_measures, product_weight, package_height, package_length, package_width, package_weight, short_description, color, product_warranty, fk_supplier, status, sku_simple) select 'Venezuela', c.id_catalog_config, c.fk_catalog_brand, c.sku, c.status_supplier_config, c.description, c.name, c.pet_status, c.pet_approved, c.created_at, c.updated_at, c.updated_by_config, c.activated_at, c.sku_supplier_config, c.creation_source_config, c.model, c.imported, c.product_measures, c.product_weight, c.package_height, c.package_length, c.package_width, c.package_weight, c.short_description, c.color, c.product_warranty, c.fk_supplier, c.status, s.sku from bob_live_ve.catalog_config c, bob_live_ve.catalog_simple s where s.fk_catalog_config=c.id_catalog_config;

update production.catalog_config c inner join bob_live_mx.catalog_simple s on c.id_catalog_config=s.fk_catalog_config set c.id_catalog_simple=s.id_catalog_simple, c.barcode_ean=s.barcode_ean where country = 'Mexico';

update production.catalog_config c inner join bob_live_co.catalog_simple s on c.id_catalog_config=s.fk_catalog_config set c.id_catalog_simple=s.id_catalog_simple, c.barcode_ean=s.barcode_ean where country = 'Colombia';

update production.catalog_config c inner join bob_live_pe.catalog_simple s on c.id_catalog_config=s.fk_catalog_config set c.id_catalog_simple=s.id_catalog_simple, c.barcode_ean=s.barcode_ean where country = 'Peru';

update production.catalog_config c inner join bob_live_ve.catalog_simple s on c.id_catalog_config=s.fk_catalog_config set c.id_catalog_simple=s.id_catalog_simple, c.barcode_ean=s.barcode_ean where country = 'Venezuela';

update production.catalog_config c inner join bob_live_mx.catalog_brand b on c.fk_catalog_brand=b.id_catalog_brand set c.brand_name=b.name where country = 'Mexico';

update production.catalog_config c inner join bob_live_co.catalog_brand b on c.fk_catalog_brand=b.id_catalog_brand set c.brand_name=b.name where country = 'Colombia';

update production.catalog_config c inner join bob_live_pe.catalog_brand b on c.fk_catalog_brand=b.id_catalog_brand set c.brand_name=b.name where country = 'Peru';

update production.catalog_config c inner join bob_live_ve.catalog_brand b on c.fk_catalog_brand=b.id_catalog_brand set c.brand_name=b.name where country = 'Venezuela';

call production.catalog_visible;

update production.catalog_config c inner join production.catalog_visible v on c.sku_simple=v.sku_simple set visible = 1 where country = 'Mexico';

update production.catalog_config  set visible = 0 where visible is null and country='Mexico';

call production_co.catalog_visible;

update production.catalog_config c inner join production_co.catalog_visible v on c.sku_simple=v.sku_simple set visible = 1 where country = 'Colombia';

update production.catalog_config  set visible = 0 where visible is null and country='Colombia';

call production_pe.catalog_visible;

update production.catalog_config c inner join production_pe.catalog_visible v on c.sku_simple=v.sku_simple set visible = 1 where country = 'Peru';

update production.catalog_config  set visible = 0 where visible is null and country='Peru';

call production_ve.catalog_visible;

update production.catalog_config c inner join production_ve.catalog_visible v on c.sku_simple=v.sku_simple set visible = 1 where country = 'Venezuela';

update production.catalog_config  set visible = 0 where visible is null and country='Venezuela';
