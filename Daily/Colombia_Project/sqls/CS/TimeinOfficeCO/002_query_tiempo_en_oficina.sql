use customer_service_co

#------------------------------------------------------------------------------
#POSTVENTA
#------------------------------------------------------------------------------

SELECT  'INICIO POSTVENTA',now();

#Seleccionar m�xima fecha
set @max_day=cast((select max(fecha) from bi_ops_cc_adherencia_sac)  as date);
set time_zone='-5:00';

#Borrar datos del �ltimo d�a
delete from bi_ops_cc_adherencia_sac
where fecha>=@max_day;

#Insertar datos de los asesores
insert into bi_ops_cc_adherencia_sac (cedula, nombre, patron_de_turno, coordinador, fecha)
select b.*, a.date
from
(select identification, nombre, shift_pattern, coordinator from CS_dyalogo.dy_agentes a
inner join bi_ops_matrix_sac b on CAST(a.identificacion AS UNSIGNED)=cast(b.identification as unsigned)
where process like 'SAC%'
or process like '%BACK%'
or process like '%Facebook%'
or process like '%CHAT%'
or process like '%Mercado%'
and status='ACTIVO') b
join 
(select distinct(event_date) date from bi_ops_cc_dyalogo
order by event_date asc) a
where a.date>=@max_day;

#Insertar de conexi�n y desconexi�n
update bi_ops_cc_adherencia_sac a
inner join (SELECT 
agente_documento_id, campana_nombre_dyalogo, date(fecha_hora_inicio) fecha2, min(fecha_hora_inicio) hora_inicio, max(fecha_hora_fin) hora_fin
FROM 
CS_dyalogo.dy_v_historico_sesiones_por_campana 
group by date(fecha_hora_inicio), agente_documento_id) b
on cast(a.cedula as unsigned)=cast(b.agente_documento_id as unsigned) and a.fecha=b.fecha2
set 
hora_conexion=hora_inicio,
hora_desconexion=hora_fin,
campana_dyalogo=campana_nombre_dyalogo
where fecha>=@max_day;

#Insertar tiempo total de conexi�n
update customer_service_co.bi_ops_cc_adherencia_sac
set
tiempo_conexion_segundos=time_to_sec(TIMEDIFF(hora_desconexion, hora_conexion))
where fecha>=@max_day;

#Insertar descansos
update bi_ops_cc_adherencia_sac a
inner join (SELECT date(fecha_hora_inicio) fecha2, identificacion, sum(duracion) duracion FROM CS_dyalogo.dy_descansos a 
inner join CS_dyalogo.dy_tipos_descanso b
on a.id_tipo_descanso=b.id
inner join CS_dyalogo.dy_agentes c
on a.id_agente=c.id
where tipo='Break'
group by date(fecha_hora_inicio), identificacion) b
on cast(a.cedula as unsigned)=cast(b.identificacion as unsigned) and a.fecha=b.fecha2
set
total_break_segundos=duracion
where fecha>=@max_day;

update bi_ops_cc_adherencia_sac a
inner join (SELECT date(fecha_hora_inicio) fecha2, identificacion, sum(duracion) duracion FROM CS_dyalogo.dy_descansos a 
inner join CS_dyalogo.dy_tipos_descanso b
on a.id_tipo_descanso=b.id
inner join CS_dyalogo.dy_agentes c
on a.id_agente=c.id
where tipo='Bano'
group by date(fecha_hora_inicio), identificacion) b
on cast(a.cedula as unsigned)=cast(b.identificacion as unsigned) and a.fecha=b.fecha2
set
total_ba�o_segundos=duracion
where fecha>=@max_day;


update bi_ops_cc_adherencia_sac a
inner join (SELECT date(fecha_hora_inicio) fecha2, identificacion, sum(duracion) duracion FROM CS_dyalogo.dy_descansos a 
inner join CS_dyalogo.dy_tipos_descanso b
on a.id_tipo_descanso=b.id
inner join CS_dyalogo.dy_agentes c
on a.id_agente=c.id
where tipo='Almuerzo'
group by date(fecha_hora_inicio), identificacion) b
on cast(a.cedula as unsigned)=cast(b.identificacion as unsigned) and a.fecha=b.fecha2
set
total_almuerzo_segundos=duracion
where fecha>=@max_day;

update bi_ops_cc_adherencia_sac a
inner join (SELECT date(fecha_hora_inicio) fecha2, identificacion, sum(duracion) duracion FROM CS_dyalogo.dy_descansos a 
inner join CS_dyalogo.dy_tipos_descanso b
on a.id_tipo_descanso=b.id
inner join CS_dyalogo.dy_agentes c
on a.id_agente=c.id
where tipo='Capacitacion'
group by date(fecha_hora_inicio), identificacion) b
on cast(a.cedula as unsigned)=cast(b.identificacion as unsigned) and a.fecha=b.fecha2
set
total_capacitacion_segundos=duracion
where fecha>=@max_day;

update bi_ops_cc_adherencia_sac a
inner join (SELECT date(fecha_hora_inicio) fecha2, identificacion, sum(duracion) duracion FROM CS_dyalogo.dy_descansos a 
inner join CS_dyalogo.dy_tipos_descanso b
on a.id_tipo_descanso=b.id
inner join CS_dyalogo.dy_agentes c
on a.id_agente=c.id
where tipo='Feedback'
group by date(fecha_hora_inicio), identificacion) b
on cast(a.cedula as unsigned)=cast(b.identificacion as unsigned) and a.fecha=b.fecha2
set
total_feedback_segundos=duracion
where fecha>=@max_day;


update bi_ops_cc_adherencia_sac a
inner join (SELECT date(fecha_hora_inicio) fecha2, identificacion, sum(duracion) duracion FROM CS_dyalogo.dy_descansos a 
inner join CS_dyalogo.dy_tipos_descanso b
on a.id_tipo_descanso=b.id
inner join CS_dyalogo.dy_agentes c
on a.id_agente=c.id
where (tipo='Pausas activas' or tipo='Otros procesos')
group by date(fecha_hora_inicio), identificacion) b
on cast(a.cedula as unsigned)=cast(b.identificacion as unsigned) and a.fecha=b.fecha2
set
total_otros_procesos_segundos=duracion
where fecha>=@max_day;

#Actualizar tiempo efectivo
update bi_ops_cc_adherencia_sac
set
 total_tiempo_efectivo_segundos=tiempo_conexion_segundos-
(total_break_segundos+
total_ba�o_segundos+
total_capacitacion_segundos+
total_almuerzo_segundos+
total_feedback_segundos+
total_otros_procesos_segundos)
where fecha>=@max_day;

#Actualizar tiempos de conexi�n
update bi_ops_cc_adherencia_sac
set
tiempo_horario_segundos=time_to_sec(TIMEDIFF(hora_fin_horario, hora_inicio_horario))
where fecha>=date_sub(@max_day, interval 7 day);


update bi_ops_cc_adherencia_sac
set
tiempo_horario_segundos=tiempo_conexion_segundos
where novedad in ('Descanso','Incapacidad','Calamidad domestica','Lactancia','Enfermeria','Permiso','Tecnologia')
and fecha>=date_sub(@max_day, interval 7 day);

#Actualizar inasistencias y retrasos
update bi_ops_cc_adherencia_sac
set 
inasistencia=1
where hora_inicio_horario is not null and
tiempo_conexion_segundos is null
and fecha>=date_sub(@max_day, interval 7 day);

update bi_ops_cc_adherencia_sac a
set retraso=1
where time_to_sec(addtime(time(hora_conexion), -hora_inicio_horario))>120
and fecha>=date_sub(@max_day, interval 7 day);

update bi_ops_cc_adherencia_sac
set retraso=0
where fecha>=date_sub(@max_day, interval 7 day)
and novedad='Retraso justificado';

update customer_service_co.bi_ops_cc_adherencia_sac
set inasistencia=0
where fecha>=date_sub(@max_day, interval 7 day)
and inasistencia=1
and novedad in ('Incapacidad','Permiso','Tecnologia','Calamidad domestica','Descanso');

SELECT 'Actualiza Semana';
UPDATE  bi_ops_cc_adherencia_sac SET Semana=date_format(fecha,'%Y-%u');

#------------------------------------------------------------------------------
#PREVENTA
#------------------------------------------------------------------------------

SELECT  'INICIO PREVENTA',now();

#Seleccionar m�xima fecha
set @max_day=cast((select max(fecha) from bi_ops_cc_adherencia_ventas)  as date);
set time_zone='-5:00';

#Borrar datos del �ltimo d�a
delete from bi_ops_cc_adherencia_ventas
where fecha>=@max_day;

#Insertar datos de los asesores
insert into bi_ops_cc_adherencia_ventas (cedula, nombre, patron_de_turno, coordinador, fecha)
select b.*, a.date
from
(select identification, nombre, shift_pattern, coordinator from CS_dyalogo.dy_agentes a
inner join bi_ops_matrix_ventas b on CAST(a.identificacion AS UNSIGNED)=cast(b.identification as unsigned)
where process like 'VENTAS%'
or process like '%SAC%'
and status='ACTIVO') b
join 
(select distinct(event_date) date from bi_ops_cc_dyalogo
order by event_date asc) a
where a.date>=@max_day;

#Insertar de conexi�n y desconexi�n
update bi_ops_cc_adherencia_ventas a
inner join (SELECT 
agente_documento_id, campana_nombre_dyalogo, date(fecha_hora_inicio) fecha2, min(fecha_hora_inicio) hora_inicio, max(fecha_hora_fin) hora_fin
FROM 
CS_dyalogo.dy_v_historico_sesiones_por_campana 
group by date(fecha_hora_inicio), agente_documento_id) b
on cast(a.cedula as unsigned)=cast(b.agente_documento_id as unsigned) and a.fecha=b.fecha2
set 
hora_conexion=hora_inicio,
hora_desconexion=hora_fin,
campana_dyalogo=campana_nombre_dyalogo
where fecha>=@max_day;

#Insertar tiempo total de conexi�n
update bi_ops_cc_adherencia_ventas
set
tiempo_conexion_segundos=time_to_sec(TIMEDIFF(hora_desconexion, hora_conexion))
where fecha>=@max_day;

#Insertar descansos
update bi_ops_cc_adherencia_ventas a
inner join (SELECT date(fecha_hora_inicio) fecha2, identificacion, sum(duracion) duracion FROM CS_dyalogo.dy_descansos a 
inner join CS_dyalogo.dy_tipos_descanso b
on a.id_tipo_descanso=b.id
inner join CS_dyalogo.dy_agentes c
on a.id_agente=c.id
where tipo='Break'
group by date(fecha_hora_inicio), identificacion) b
on cast(a.cedula as unsigned)=cast(b.identificacion as unsigned) and a.fecha=b.fecha2
set
total_break_segundos=duracion
where fecha>=@max_day;

update bi_ops_cc_adherencia_ventas a
inner join (SELECT date(fecha_hora_inicio) fecha2, identificacion, sum(duracion) duracion FROM CS_dyalogo.dy_descansos a 
inner join CS_dyalogo.dy_tipos_descanso b
on a.id_tipo_descanso=b.id
inner join CS_dyalogo.dy_agentes c
on a.id_agente=c.id
where tipo='Bano'
group by date(fecha_hora_inicio), identificacion) b
on cast(a.cedula as unsigned)=cast(b.identificacion as unsigned) and a.fecha=b.fecha2
set
total_ba�o_segundos=duracion
where fecha>=@max_day;


update bi_ops_cc_adherencia_ventas a
inner join (SELECT date(fecha_hora_inicio) fecha2, identificacion, sum(duracion) duracion FROM CS_dyalogo.dy_descansos a 
inner join CS_dyalogo.dy_tipos_descanso b
on a.id_tipo_descanso=b.id
inner join CS_dyalogo.dy_agentes c
on a.id_agente=c.id
where tipo='Almuerzo'
group by date(fecha_hora_inicio), identificacion) b
on cast(a.cedula as unsigned)=cast(b.identificacion as unsigned) and a.fecha=b.fecha2
set
total_almuerzo_segundos=duracion
where fecha>=@max_day;

update bi_ops_cc_adherencia_ventas a
inner join (SELECT date(fecha_hora_inicio) fecha2, identificacion, sum(duracion) duracion FROM CS_dyalogo.dy_descansos a 
inner join CS_dyalogo.dy_tipos_descanso b
on a.id_tipo_descanso=b.id
inner join CS_dyalogo.dy_agentes c
on a.id_agente=c.id
where tipo='Capacitacion'
group by date(fecha_hora_inicio), identificacion) b
on cast(a.cedula as unsigned)=cast(b.identificacion as unsigned) and a.fecha=b.fecha2
set
total_capacitacion_segundos=duracion
where fecha>=@max_day;

update bi_ops_cc_adherencia_ventas a
inner join (SELECT date(fecha_hora_inicio) fecha2, identificacion, sum(duracion) duracion FROM CS_dyalogo.dy_descansos a 
inner join CS_dyalogo.dy_tipos_descanso b
on a.id_tipo_descanso=b.id
inner join CS_dyalogo.dy_agentes c
on a.id_agente=c.id
where tipo='Feedback'
group by date(fecha_hora_inicio), identificacion) b
on cast(a.cedula as unsigned)=cast(b.identificacion as unsigned) and a.fecha=b.fecha2
set
total_feedback_segundos=duracion
where fecha>=@max_day;


update bi_ops_cc_adherencia_ventas a
inner join (SELECT date(fecha_hora_inicio) fecha2, identificacion, sum(duracion) duracion FROM CS_dyalogo.dy_descansos a 
inner join CS_dyalogo.dy_tipos_descanso b
on a.id_tipo_descanso=b.id
inner join CS_dyalogo.dy_agentes c
on a.id_agente=c.id
where (tipo='Pausas activas' or tipo='Otros procesos')
group by date(fecha_hora_inicio), identificacion) b
on cast(a.cedula as unsigned)=cast(b.identificacion as unsigned) and a.fecha=b.fecha2
set
total_otros_procesos_segundos=duracion
where fecha>=@max_day;

#Actualizar tiempo efectivo
update bi_ops_cc_adherencia_ventas
set
 total_tiempo_efectivo_segundos=tiempo_conexion_segundos-
(total_break_segundos+total_break_segundos+
total_ba�o_segundos+
total_capacitacion_segundos+
total_almuerzo_segundos+
total_feedback_segundos+
total_otros_procesos_segundos)
where fecha>=@max_day;

#Actualizar tiempos de conexi�n
update bi_ops_cc_adherencia_ventas
set
tiempo_horario_segundos=time_to_sec(TIMEDIFF(hora_fin_horario, hora_inicio_horario))
where fecha>=date_sub(@max_day, interval 7 day);

update bi_ops_cc_adherencia_ventas
set
tiempo_horario_segundos=tiempo_conexion_segundos
where novedad in ('Descanso','Incapacidad','Calamidad domestica','Lactancia','Enfermeria','Permiso','Tecnologia')
and fecha>=date_sub(@max_day, interval 7 day);

#Actualizar inasistencias y retrasos
update bi_ops_cc_adherencia_ventas
set 
inasistencia=1
where hora_inicio_horario is not null and
tiempo_conexion_segundos is null
and fecha>=date_sub(@max_day, interval 7 day);

update bi_ops_cc_adherencia_ventas a
set retraso=1
where time_to_sec(addtime(time(hora_conexion), -hora_inicio_horario))>300
and fecha>=date_sub(@max_day, interval 7 day);

update customer_service_co.bi_ops_cc_adherencia_ventas
set inasistencia=0
where fecha>=date_sub(@max_day, interval 7 day)
and inasistencia=1
and novedad in ('Incapacidad','Permiso','Tecnologia','Calamidad domestica','Descanso');

SELECT 'Actualiza Semana';
UPDATE  bi_ops_cc_adherencia_ventas SET Semana=date_format(fecha,'%Y-%u');
