INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  step,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Colombia',
  'customer_service_co.time_in_office',
  'finish',
  NOW(),
  max(Fecha),
  count(1),
  count(1)
FROM
  customer_service_co.bi_ops_cc_adherencia_sac
