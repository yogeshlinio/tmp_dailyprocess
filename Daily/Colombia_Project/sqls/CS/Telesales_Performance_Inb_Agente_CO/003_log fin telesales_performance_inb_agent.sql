INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'telesales_performance_inb_agent',
  'fin',
  NOW(),
  NOW(),
  1,
  1;