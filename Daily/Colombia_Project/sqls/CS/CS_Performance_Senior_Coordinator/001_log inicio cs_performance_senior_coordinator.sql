INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'cs_performance_senior_coordinator',
  'start',
  NOW(),
  NOW(),
  1,
  1;