use cs_performance_co;

SET @KPI_Month=cast(date_format(curdate(),"%Y%m")-1 as unsigned);
SET @Perfomance_max_date=cast((select max(event_date) from customer_service_co.bi_ops_cc_dyalogo
where date_format(event_date,"%Y%m%")=@KPI_Month) as date);


delete from performance_cs_leaders
where MonthNum=@KPI_Month;

#Insertar coordinadores activos
insert into  performance_cs_leaders 
(MonthNum, Coordinator, Identification, proceso, coordinador, status, start_date, shift_pattern)
select @KPI_Month, name, identification, process,  coordinator, status, start_date, shift_pattern from bi_ops_matrix_sac_history
where process like '%coordinador sac%' and status='ACTIVO'
and date=@Perfomance_max_date;


#Actualizar datos de Inbound 
update performance_cs_leaders a
inner join
(select nombre_matrix,count(1) cant, total_duration_calls_sec, conection_available_hours_month*3600 conection_available_sec, avg(value_adherence) adherence,
avg(value_tmo) tmo,   avg(value_quality) quality, avg(value_survey) survey
from performance_inbound_telesales_agents a 
inner join bi_ops_cc_leaders b
on a.coordinador=b.coordinador
where MonthNum=@KPI_Month
and proceso like '%SAC%'
and status='ACTIVO') b
on a.MonthNum=b.MonthNum
set
total_agents_inb=cant,
call_time_seg=total_duration_calls_sec,
conection_available_sec_month=conection_available_sec,
value_adherence_inb=adherence,
value_tmo_inb=tmo,
value_quality_inb=quality,
value_survey_inb=survey
where a.MonthNum=@KPI_Month
;



#Actualizar datos de Chat 
update performance_cs_leaders a
inner join
(select MonthNum,count(1) cant, avg(value_adherence) adherence,
  avg(value_quality) quality, avg(value_survey) survey
from performance_inbound_telesales_agents a 
inner join bi_ops_cc_leaders b
on a.coordinador=b.coordinador
where MonthNum=@KPI_Month
and proceso like '%CHAT%'
and status='ACTIVO') b
on a.MonthNum=b.MonthNum
set
total_agents_chat=cant,
value_adherence_chat=adherence,
value_quality_chat=quality,
value_survey_chat=survey
where a.MonthNum=@KPI_Month;

#Actualizar datos de Front Office
update performance_cs_leaders a
inner join
(select MonthNum, count(1) cant, avg(value_adherence) adherence, avg(kpi_first_reply_time) first_reply_time,
  avg(value_quality) quality, avg(value_survey) survey
from performance_inbound_telesales_agents a 
inner join bi_ops_cc_leaders b
on a.coordinador=b.coordinador
where MonthNum=@KPI_Month
and proceso like '%BACK%'
and status='ACTIVO') b
on a.MonthNum=b.MonthNum
set
total_agents_fo=cant,
value_adherence_front=adherence,
value_reply_on_time_front=first_reply_time,
value_quality_front=quality,
value_survey_front=survey
where a.MonthNum=@KPI_Month;

#Actualizar datos de Back Office
update performance_cs_leaders a
inner join
(select MonthNum, count(1) cant, avg(value_adherence) adherence, avg(kpi_first_reply_time) first_reply_time, avg(kpi_solution) solution,
  avg(value_quality) quality, avg(value_survey) survey
from performance_inbound_telesales_agents a 
inner join bi_ops_cc_leaders b
on a.coordinador=b.coordinador
where MonthNum=@KPI_Month
and proceso like '%BACK%'
and status='ACTIVO') b
on a.MonthNum=b.MonthNum
set
total_agents_bo=cant,
value_adherence_back=adherence,
value_reply_on_time_back=first_reply_time,
value_solved_on_time=solution,
value_quality_back=quality,
value_survey_back=survey
where a.MonthNum=@KPI_Month;


#Actualizar nota de cumplimiento de monitoreos
update performance_cs_leaders a 
inner join customer_service_co.bi_ops_cc_cumplimiento_monitoreos b 
on a.Identification=b.cedula
and a.MonthNum=b.mes
set
value_punctuality=nota
where MonthNum=@KPI_Month;

#Actualizar nota de cumplimiento de feedback
update performance_cs_leaders a 
inner join customer_service_co.bi_ops_cc_feedback_ventas b 
on a.Identification=b.cedula
and a.MonthNum=b.mes
set
value_feedback=nota
where MonthNum=@KPI_Month;

#Agregar nivel de servicio y abandono
update performance_cs_leaders
set
value_service_level=(select sum(answered_in_wh_20)/sum(net_event) from customer_service_co.bi_ops_cc_dyalogo
where date_format(event_date,"%Y%m")=@KPI_Month
and pos=1 and net_event=1),
value_abandon=(select sum(unanswered_in_wh)/sum(net_event) from customer_service_co.bi_ops_cc_dyalogo
where date_format(event_date,"%Y%m")=@KPI_Month
and pos=1 and net_event=1)
where MonthNum=@KPI_Month;

#Actualizar indicadores
update  performance_cs_leaders 
SET
value_adherence=(value_adherence_inb*total_agents_inb+value_adherence_front*total_agents_fo+value_adherence_back*total_agents_bo+value_adherence_chat*total_agents_chat)/
				(total_agents_inb+total_agents_fo+total_agents_bo+total_agents_chat),
value_occupancy=call_time_seg/conection_available_sec_month,
value_reply_on_time=(value_reply_on_time_front*total_agents_fo+value_reply_on_time_back*total_agents_bo)/
					(total_agents_fo+total_agents_bo),
value_quality=(value_quality_inb*total_agents_inb+value_quality_front*total_agents_fo+value_quality_back*total_agents_bo+value_quality_chat*total_agents_chat)/
				(total_agents_inb+total_agents_fo+total_agents_bo+total_agents_chat),
value_training_team=(value_training_team_inb*total_agents_inb+value_training_team_front*total_agents_fo+value_training_team_back*total_agents_bo+value_training_team_chat*total_agents_chat)/
				(total_agents_inb+total_agents_fo+total_agents_bo+total_agents_chat),
value_survey=(value_survey_inb*total_agents_inb+value_survey_front*total_agents_fo+value_survey_back*total_agents_bo+value_survey_chat*total_agents_chat)/
				(total_agents_inb+total_agents_fo+total_agents_bo+total_agents_chat),
value_activity=(value_activity_inb*total_agents_inb+value_activity*total_agents_chat)/
				(total_agents_inb+total_agents_chat),
value_absence=(value_absence_inb*total_agents_inb+value_absence_front*total_agents_fo+value_absence_back*total_agents_bo+value_absence_chat*total_agents_chat)/
				(total_agents_inb+total_agents_fo+total_agents_bo+total_agents_chat),
value_delays=(value_delays_inb*total_agents_inb+value_delays_front*total_agents_fo+value_delays_back*total_agents_bo+value_delays_chat*total_agents_chat)/
				(total_agents_inb+total_agents_fo+total_agents_bo+total_agents_chat)
where MonthNum=@KPI_Month;

update  performance_cs_leaders a
inner join customer_service_co.bi_ops_cc_training b
on a.Identification=b.cedula and a.MonthNum=b.mes
set
value_training_leader=nota
where MonthNum=@KPI_Month;

#Valor general de training
update performance_cs_leaders
set 
value_training=value_training_team*0.4+value_training_leader*0.6
where MonthNum=@KPI_Month;

#Actualizar KPI
update performance_cs_leaders
set
kpi_adherence=if(value_adherence>=0.98,1,if(value_adherence>=0.95,0.5,0)) ,
kpi_occupancy=if(value_occupancy>=0.7,1,value_occupancy/0.7),
kpi_reply_on_time=value_reply_on_time,
kpi_solved_on_time=value_solved_on_time,
kpi_service_level=if(value_service_level>=0.80,1,0),
kpi_abandon=if(value_abandon<=0.03,1,0) ,
kpi_tmo=if(value_tmo<=6,1,if(value_tmo<=6.25,0.98,if(value_tmo<=7,0.8,if(value_tmo<=8,0.5,0)))),
kpi_quality=if(value_quality>=90,value_quality/100,if(value_quality>=80,0.7,if(value_quality>=70,0.5,0))),
kpi_training=if(value_training>=90,value_training/100,if(value_training>=80,0.7,if(value_training>=70,0.5,0))),
kpi_survey=value_survey,
kpi_activity=value_activity,
kpi_punctuality=value_punctuality/100,
kpi_feedback=value_feedback/100,
kpi_absence=value_absence,
kpi_delays=value_delays
where MonthNum=@KPI_Month;

#Principales KPI del mes
update performance_cs_leaders
set
kpi_productivity=kpi_adherence*0.15+kpi_tmo*0.10+kpi_reply_on_time*0.15+kpi_solved_on_time*0.15+kpi_service_level*0.15+kpi_abandon*0.15,
kpi_quality_training_nsu=kpi_quality*0.35+kpi_training*0.2+kpi_survey*0.25+kpi_activity*0.1+kpi_punctuality*0.05+kpi_feedback*0.05,
kpi_absences_and_delays=kpi_absence*0.5+kpi_delays*0.5
where MonthNum=@KPI_Month;

#Indicador General del Mes
update performance_cs_leaders
set
kpi_general=kpi_productivity*0.4+kpi_quality_training_nsu*0.5+kpi_absences_and_delays*0.1
where MonthNum=@KPI_Month;

#Borrar datos de asesores de Postventa si existen
delete from perfomance_coord_general
where yrmonth=@KPI_Month
and proceso='CS';

#Agregar datos a la evaluación general
insert into perfomance_coord_general
select MonthNum, Coordinator,  0, kpi_productivity, kpi_quality_training_nsu, kpi_absences_and_delays,0,
kpi_general, 'CS'  from performance_cs_leaders
where status='ACTIVO' 
and MonthNum=@KPI_Month;