use cs_performance_co;


SET @KPI_Month=cast(date_format(curdate(),"%Y%m")-1 as unsigned);
SET @Perfomance_max_date=cast((select max(fecha_creacion) from customer_service.tbl_zendesk_general
where date_format(fecha_creacion,"%Y%m%")=@KPI_Month) as date);


delete from performance_back_office_cs_agents
where MonthNum=@KPI_Month;

#Insertar datos de la matriz
insert into  performance_back_office_cs_agents 
(MonthNum, Agent, Identification, proceso, subproceso, coordinador, status, start_date, shift_pattern)
select @KPI_Month, name, identification, process, subprocess, coordinator, status, start_date, shift_pattern from bi_ops_matrix_sac_history 
where process like '%BACK OFFICE%' and status like '%ACTIVO%'
and date=@Perfomance_max_date;

#Actualizar datos de zendesk
update  performance_back_office_cs_agents a 
inner join (select date_format(Fecha_Solucion,"%Y%m") yrmonth, name, identification, sum(summation_column) tickets_solved, sum(reply_on_time_bo) tickets_replied_on_time,
sum(solved_on_time_bo) tickets_solved_on_time,
sum(calificados) rated, sum(calificados_bueno) rated_good FROM customer_service.tbl_zendesk_general a
inner join customer_service_co.bi_ops_matrix_sac b on a.Assignee=b.user_zendesk
where pais='CO'
and date_format(Fecha_Solucion,"%Y%m")=@KPI_Month
and time_to_reply_bo_hours is not null
group by identification) b 
on a.Identification=b.identification
set
total_tickets_solved_zendesk=tickets_solved,
total_reply_on_time_zendesk=tickets_replied_on_time,
total_tickets_solved_on_time_zendesk=tickets_solved_on_time,
survey_answered=rated,
survey_answered_good=rated_good
where MonthNum=@KPI_Month;

#Actualizar datos de reembolsos
update  performance_back_office_cs_agents a 
set
total_solved_not_zendesk=(select  count(1) refunded
 from operations_co.out_inverse_logistics_tracking
where (date_format(date_cash_refunded_il,"%Y%m")=@KPI_Month or date_format(date_voucher_refunded_il,"%Y%m")=@KPI_Month)
and workdays_to_refunded_il is not null),
total_solved_on_time_not_zendesk=(select 
sum(if(action_il='Consignaci�n bancaria (7 dias h�biles)',if(workdays_to_refunded_il<=5,1,0),if(action_il='Efecty (7 d�as h�biles)',if(workdays_to_refunded_il<=4,1,0),
if(action_il='Reversi�n TC  (25-30 dias h�biles)',if(workdays_to_refunded_il<=20,1,0),if(action_il='Voucher (5 d�as h�biles)',if(workdays_to_refunded_il<=2,1,0),0))))) refunded_on_time 
 from operations_co.out_inverse_logistics_tracking
where (date_format(date_cash_refunded_il,"%Y%m")=@KPI_Month or date_format(date_voucher_refunded_il,"%Y%m")=@KPI_Month)
and workdays_to_refunded_il is not null)
where MonthNum=@KPI_Month
and subproceso like '%IL - REEMBOLSOS%';

#Actualizar datos de gu�as de log�stica inversa
update  performance_back_office_cs_agents a 
set
total_solved_not_zendesk=(select  count(1) cant_il from operations_co.out_inverse_logistics_tracking
where date_format(date_first_track_il,"%Y%m")=@KPI_Month
and workdays_to_track_il is not null),
total_solved_on_time_not_zendesk=(select  sum(if(workdays_to_track_il<=2,1,0)) cant_il_on_time from operations_co.out_inverse_logistics_tracking
where date_format(date_first_track_il,"%Y%m")=@KPI_Month
and workdays_to_track_il is not null)
where MonthNum=@KPI_Month
and subproceso like '%IL- GUIAS%';


#Agregar antiguedad de los asesores
update performance_back_office_cs_agents 
set
Antique='Nuevo'
where DATEDIFF(CAST(@Perfomance_max_date AS DATE),start_date)<31
and MonthNum=@KPI_Month;

#Eliminar datos que no aparecen en la matriz de agentes
delete from performance_back_office_cs_agents
where shift_pattern is null
and MonthNum=@KPI_Month;

#Actualizar horas de los turnos
update  performance_back_office_cs_agents a 
inner join customer_service_co.bi_ops_cc_shift_details b 
on a.shift_pattern=b.shift
set shift_hours=turno_diario
where MonthNum=@KPI_Month;

#Datos de adherencia 1
update performance_back_office_cs_agents a 
inner join (select cedula, sum(tiempo_conexion_segundos)/3600 conexion, (sum(total_break_segundos)+
sum(total_ba�o_segundos)+sum(total_capacitacion_segundos)+sum(total_almuerzo_segundos) +
sum(total_feedback_segundos)+sum(total_otros_procesos_segundos))/3600 total_pausas,
sum(tiempo_horario_segundos)/3600 horario, sum(inasistencia) inasistencias, sum(retraso) retrasos from customer_service_co.bi_ops_cc_adherencia_sac
where date_format(fecha,"%Y%m%")=@KPI_Month
group by cedula) b 
on cast(a.Identification as unsigned)=cast(b.cedula as unsigned)
set
conection_hours_month=conexion,
pauses_hours_month=total_pausas,
conection_available_hours_month=horario,
total_absence=inasistencias,
total_delays=retrasos
where MonthNum=@KPI_Month;

#Datos de adherencia 2
update performance_back_office_cs_agents a 
inner join (select cedula, sum(tiempo_conexion_segundos)/3600 conexion, (sum(total_break_segundos)+
sum(total_ba�o_segundos)+sum(total_capacitacion_segundos)+sum(total_almuerzo_segundos) +
sum(total_feedback_segundos)+sum(total_otros_procesos_segundos))/3600 total_pausas,
sum(tiempo_horario_segundos)/3600 horario, sum(inasistencia) inasistencias, sum(retraso) retrasos from customer_service_co.bi_ops_cc_adherencia_sac
where Semana in (select semana from
(select semana, count(distinct(fecha)) cant from  customer_service_co.bi_ops_cc_adherencia_sac
where date_format(fecha,"%Y%m%")=@KPI_Month
group by semana) t
where cant=7)
group by cedula) b 
on cast(a.Identification as unsigned)=cast(b.cedula as unsigned)
set
conection_hours_4weeks=conexion,
pauses_hours_4weeks=total_pausas,
conection_available_hours_4weeks=horario
where MonthNum=@KPI_Month;

#Pausas disponibles
update  performance_back_office_cs_agents
set pauses_available_hours_month=if(shift_hours=8,740/60,410/60) 
where MonthNum=@KPI_Month;

#Datos de calidad
update performance_back_office_cs_agents a 
inner join (select cedula, count(1) monitoreos, sum(if(error_critico=100,0,1)) errores_criticos, sum(error_no_critico) sum_nota
 from customer_service_co.tbl_bi_ops_cc_calidad_formato_bo
where date_format(fechahora,"%Y%m%")=@KPI_Month
group by cedula) b 
on cast(a.Identification as unsigned)=cast(b.cedula as unsigned)
set
total_monitoring=monitoreos,
critical_mistakes=errores_criticos,
sum_califications_not_critical=sum_nota
where MonthNum=@KPI_Month;

#Nota de training
update  performance_back_office_cs_agents a
inner join customer_service_co.bi_ops_cc_training b
on cast(a.Identification as unsigned)=cast(b.cedula as unsigned)and a.MonthNum=b.mes
set
training_calification=nota
where MonthNum=@KPI_Month;

#Valores Indicadores
update performance_back_office_cs_agents
set
value_adherence= if((conection_hours_4weeks-pauses_hours_4weeks)/(conection_available_hours_4weeks-pauses_available_hours_month)>1,1,
(conection_hours_4weeks-pauses_hours_4weeks)/(conection_available_hours_4weeks-pauses_available_hours_month)),
value_first_reply_time=if(total_solved_not_zendesk=0,total_reply_on_time_zendesk/total_tickets_solved_zendesk,0),
value_solution=if(total_solved_not_zendesk=0,total_tickets_solved_on_time_zendesk/total_tickets_solved_zendesk,total_solved_on_time_not_zendesk/total_solved_not_zendesk),
value_quality=((total_monitoring-critical_mistakes)*100/total_monitoring)*0.6+(sum_califications_not_critical/total_monitoring)*0.4,
value_training=training_calification,
value_survey=survey_answered_good/survey_answered,
value_absence=total_absence,
value_delays=total_delays
where MonthNum=@KPI_Month;

#Actualizar valores equipo IL
drop table if exists data_zendesk_il;

create temporary table data_zendesk_il
select MonthNum, sum(total_reply_on_time_zendesk)/sum(total_tickets_solved_zendesk) replied,
sum(survey_answered_good)/sum(survey_answered) survey from performance_back_office_cs_agents 
where MonthNum=@KPI_Month and
total_tickets_solved_zendesk>0
and subproceso like '%IL - REEMBOLSOS%';

update performance_back_office_cs_agents a
inner join data_zendesk_il b
on a.MonthNum=b.MonthNum
set
value_first_reply_time=replied,
value_survey=survey
where a.MonthNum=@KPI_Month
and subproceso like '%IL - REEMBOLSOS%';


#Actualizar KPI
update performance_back_office_cs_agents
set
kpi_adherence=if(value_adherence>=0.98,1,if(value_adherence>=0.95,0.5,0)) ,
kpi_first_reply_time=value_first_reply_time,
kpi_solution=value_solution,
kpi_quality=if(value_quality>=90,value_quality/100,if(value_quality>=80,0.7,if(value_quality>=70,0.5,0))),
kpi_training=if(value_training>=90,value_training/100,if(value_training>=80,0.7,if(value_training>=70,0.5,0))),
kpi_survey=value_survey,
kpi_absence=if(value_absence=0,1,if(value_absence=1,0.5,0)),
kpi_delays=if(value_delays=0,1,if(value_delays=1,0.9,if(value_delays=2,0.8,0)))
where MonthNum=@KPI_Month;

#Principales KPI del mes
update performance_back_office_cs_agents
set
kpi_productivity=if(total_solved_not_zendesk=0,kpi_adherence*0.25+kpi_first_reply_time*0.35+kpi_solution*0.4,kpi_adherence*(0.25/0.65)+kpi_solution*(0.4/0.65)),
kpi_quality_training_nsu=if(total_solved_not_zendesk=0,kpi_quality*0.5+kpi_training*0.3+kpi_survey*0.2,kpi_quality*(0.5/0.8)+kpi_training*(0.3/0.8)),
kpi_absences_and_delays=kpi_absence*0.5+kpi_delays*0.5
where MonthNum=@KPI_Month;

#Indicador General del Mes
update performance_back_office_cs_agents
set
kpi_general=kpi_productivity*0.4+kpi_quality_training_nsu*0.5+kpi_absences_and_delays*0.1
where MonthNum=@KPI_Month;

#Borrar datos de front office
delete from performance_back_office_cs_agents
where MonthNum=@KPI_Month
and Identification in 
(select Identification from performance_front_office_cs_agents
where MonthNum=@KPI_Month);

#Borrar datos de asesores de Back si existen
delete from perfomance_agents_general
where yrmonth=@KPI_Month
and proceso='BACK OFFICE';

#Agregar datos a la evaluaci�n general
insert into perfomance_agents_general
select MonthNum, null, Identification, Agent, coordinador, 0, kpi_productivity, kpi_quality_training_nsu, kpi_absences_and_delays,
kpi_general, 'BACK OFFICE'  from performance_back_office_cs_agents
where status='ACTIVO' and proceso='BACK OFFICE'
and kpi_general is not null
and MonthNum=@KPI_Month;