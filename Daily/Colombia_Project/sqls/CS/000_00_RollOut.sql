CREATE TABLE IF NOT EXISTS `rep_telesales_regional` (
  `Fecha` date DEFAULT NULL,
  `Pais` varchar(255) DEFAULT NULL,
  `Franja` varchar(255) DEFAULT NULL,
  `Recibidas` double DEFAULT NULL,
  `Atendidas` double DEFAULT NULL,
  `Abandonadas` double DEFAULT NULL,
  `Duracion` double DEFAULT NULL,
  `Espera` double DEFAULT NULL,
  `Transferencia` double DEFAULT NULL,
  `Atendidas20` double DEFAULT NULL,
  `Satisfaccion` double DEFAULT NULL,
  `gross_Ord` double DEFAULT NULL,
  `gross_Rev` double DEFAULT NULL,
  `Items` double DEFAULT NULL,
  `net_Ord` double DEFAULT NULL,
  `net_Rev` double DEFAULT NULL,
  `Tgross_orders` double DEFAULT NULL,
  `Semana` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1
;
DELETE FROM `rep_telesales_regional` WHERE Pais = "MX";