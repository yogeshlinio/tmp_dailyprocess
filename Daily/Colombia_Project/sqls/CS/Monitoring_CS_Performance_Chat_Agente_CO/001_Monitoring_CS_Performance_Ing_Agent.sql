INSERT production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'customer_service_co.tbl_bi_ops_cc_chats_performance_agents',
  "populated",
  date(date),
  NOW(),
  count(*),
  count(*)
FROM
   customer_service_co.tbl_bi_ops_cc_chats_performance_agents
WHERE 
       date = last_day( curdate() - interval 1 MONTH )
   and DAYOFMONTH( curdate() ) >= 5
HAVING COUNT(*) > 0
   and 0 IN ( select count(*) FROM production.table_monitoring_log 
                              WHERE     country = "Colombia" 
                                    and table_name = "customer_service_co.tbl_bi_ops_cc_chats_performance_agents"
                                    and date( updated_at ) between LAST_DAY( curdate() - INTERVAL 1 MONTH ) + INTERVAL 5 DAY
                                    and LAST_DAY( curdate() )
            ) 
;
  
INSERT production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'cs_performance_co.bi_ops_matrix_sac_history',
  "populated",
  date(date),
  NOW(),
  count(*),
  count(*)
FROM
   cs_performance_co.bi_ops_matrix_sac_history 
WHERE 
       date = last_day( curdate() - interval 1 MONTH )
   and DAYOFMONTH( curdate() ) >= 5
HAVING COUNT(*) > 0
   and 0 IN ( select count(*) FROM production.table_monitoring_log 
                              WHERE     country = "Colombia" 
                                    and table_name = "cs_performance_co.bi_ops_matrix_sac_history"
                                    and date( updated_at ) between LAST_DAY( curdate() - INTERVAL 1 MONTH ) + INTERVAL 5 DAY
                                    and LAST_DAY( curdate() )
            ) 
;


INSERT production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'customer_service_co.activities',
  "populated",
  date(date_time),
  NOW(),
  count(*),
  count(*)
FROM
   customer_service_co.activities
WHERE 
        date(date_time) = last_day( curdate() - interval 1 MONTH )
   and DAYOFMONTH( curdate() ) >= 5
HAVING COUNT(*) > 0
   and 0 IN ( select count(*) FROM production.table_monitoring_log 
                              WHERE     country = "Colombia" 
                                    and table_name = "customer_service_co.activities"
                                    and date( updated_at ) between LAST_DAY( curdate() - INTERVAL 1 MONTH ) + INTERVAL 5 DAY
                                    and LAST_DAY( curdate() )
            ) ;

INSERT production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'customer_service_co.bi_ops_cc_training',
  "populated",
  date_format(concat(mes, "01" ) , "%Y-%m-%d" ),
  NOW(),
  count(*),
  count(*)
FROM
   customer_service_co.bi_ops_cc_training
WHERE 
        mes = date_format( curdate() - interval 1 MONTH , "%Y%m" )
   and DAYOFMONTH( curdate() ) >= 5
HAVING COUNT(*) > 0
   and 0 IN ( select count(*) FROM production.table_monitoring_log 
                              WHERE     country = "Colombia" 
                                    and table_name = "customer_service_co.bi_ops_cc_training"
                                    and date( updated_at ) between LAST_DAY( curdate() - INTERVAL 1 MONTH ) + INTERVAL 5 DAY
                                    and LAST_DAY( curdate() )
            ) ;

INSERT production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'customer_service_co.cs_performance_source_chats_agents',
  "populated",
  NOW(),
  NOW(),
  COUNT(*),
  COUNT(*)
FROM
(
(
SELECT 
   table_name,
   COUNT(*) AS Entries_Count
FROM
   production.table_monitoring_log
WHERE 
        date( updated_at ) =last_day( curdate() - interval 1 MONTH )
    AND 
table_name = "customer_service_co.bi_ops_cc_dyalogo"
    AND country    = "Colombia"
HAVING     COUNT(*) > 0
)
UNION
(
SELECT 
   table_name,
   COUNT(*) AS Entries
FROM
   production.table_monitoring_log
WHERE 
        date( updated_at ) = last_day( curdate() - interval 1 MONTH )
    AND table_name = "cs_performance_co.bi_ops_matrix_sac_history"
    AND country    = "Colombia"
HAVING COUNT(*) > 0
)
UNION
(
SELECT 
   table_name,
   COUNT(*) AS Entries
FROM
   production.table_monitoring_log
WHERE 
        date( updated_at ) = last_day( curdate() - interval 1 MONTH )
    AND table_name = "customer_service_co.activities"
    AND country    = "Colombia"
HAVING COUNT(*) > 0
)
UNION
(
SELECT 
   table_name,
   COUNT(*) AS Entries
FROM
   production.table_monitoring_log
WHERE 
        month( updated_at ) =month(last_day( curdate() - interval 1 MONTH ))
    AND table_name = "customer_service_co.bi_ops_cc_training"
    AND country    = "Colombia"
HAVING COUNT(*) > 0
)
) AS Entries
HAVING COUNT(*) >= 4
   and 0 IN ( select count(*) FROM production.table_monitoring_log 
                              WHERE     country = "Colombia" 
                                    and table_name = 'customer_service_co.cs_performance_source_chats_agents'
                                    and date( updated_at ) between LAST_DAY( curdate() - INTERVAL 1 MONTH ) + INTERVAL 5 DAY
                                    and LAST_DAY( curdate() )
            ) ;
;