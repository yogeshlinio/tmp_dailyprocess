INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  step,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Mexico',
  'cdr',
  'finish',
  NOW(),
  max(calldate),
  count(*),
  count(calldate)
FROM
  customer_service.cdr;

