﻿INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia_Project', 
  'out_procurement_tracking',
  'finish',
  NOW() + INTERVAL 1 HOUR,
  MAX(date_po_created),
  count(*),
  count(*)
FROM
  operations_co.out_procurement_tracking
;