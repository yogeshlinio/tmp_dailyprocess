UPDATE             Out_Stock_Hist_Sample opt
set 
	opt.planner = IF (	buyer IN (
		"B0 - Jessica",
		"B3 - Maria Fernanda",
		"B4 - Freddy",
		"B5 - Katherine",
		"B6 - Ximena",
		"B8 - Guillermo",
		"B8 - Cristina"
	),
	"Home",
IF (
	buyer IN (
		"B2 - Alexander",
		"B1 - Maritza"
	),
	"Electronics",
	"Fashion"
)
);
