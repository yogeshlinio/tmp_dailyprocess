SET @MonthNum = "000000";
SELECT date_format( (SELECT max(date_entrance) FROM operations_co.out_stock_hist WHERE date_entrance < curdate()) ,"%Y%m" )  INTO @MonthNum;

/*
DROP TABLE IF EXISTS operations_co.A_Stock;
CREATE TABLE IF NOT EXISTS operations_co.A_Stock
(
   MonthNum int,
   stock_item_id int,
   SKU_Simple  varchar(25),
   SKU_Config  varchar(25),
   fulfillment_type_real  varchar(100),
   Days_inStock  int,
   Visibles      int,
   Reserved      int,
   CostAfterTax decimal(15,2),
   UpdatedAt  datetime,
   PRIMARY KEY ( MonthNum, stock_item_id ),
   KEY( SKU_Simple )
)
;
*/


REPLACE operations_co.A_Stock
SELECT
  @MonthNum   AS MonthNum,
  stock_item_id AS stock_item_id,
  SKU_Simple  AS SKU_Simple,
  SKU_Config  AS SKU_Config,
  fulfillment_type_real,
  Days_in_stock ,
  Visible,
	reserved,
  cost_w_o_vat, 
  now()       AS UpdatedAt
FROM
  operations_co.out_stock_hist 
WHERE
      in_stock  = 1
  AND fulfillment_type_real not like '%Consignment%'  
;