﻿#OSH

DROP TABLE  IF EXISTS operations_co.out_stock_hist_sample;
CREATE TABLE  operations_co.out_stock_hist_sample LIKE operations_co.out_stock_hist; 

INSERT INTO operations_co.out_stock_hist_sample (
	stock_item_id,
	barcode_wms,
	date_entrance,
	week_entrance,
	barcode_bob_duplicated,
	in_stock,
	wh_location,
	sub_location,
	position_type,
  reserved
) 
SELECT
	estoque.estoque_id,
	estoque.cod_barras,
	date(data_criacao),
	operations_co.week_iso(data_criacao),
	estoque.minucioso,
	posicoes.participa_estoque,
	estoque.endereco,
	estoque.sub_endereco,
	posicoes.tipo_posicao,
    IF(estoque.almoxarifado = "separando"
			OR estoque.almoxarifado = "estoque reservado"
			OR estoque.almoxarifado = "aguardando separacao", 1, 0)
FROM	(wmsprod_co.estoque
			LEFT JOIN wmsprod_co.itens_recebimento 
				ON estoque.itens_recebimento_id = itens_recebimento.itens_recebimento_id)
			LEFT JOIN wmsprod_co.posicoes 
				ON estoque.endereco = posicoes.posicao
;



#REVISAR SI SE USA
/*DELETE operations_co.pro_unique_ean_bob.*
FROM
	operations_co.pro_unique_ean_bob;


INSERT INTO operations_co.pro_unique_ean_bob (ean) SELECT
	produtos.ean AS ean
FROM
	bob_live_co.catalog_simple
 INNER JOIN wmsprod_co.produtos ON catalog_simple.sku = produtos.sku
WHERE
	catalog_simple. STATUS NOT LIKE "deleted"
GROUP BY
	produtos.ean
HAVING
	count(produtos.ean = 1);*/

#### sales order item data
DROP TEMPORARY TABLE IF EXISTS operations_co.tmp_sales_for_stock;
CREATE TEMPORARY TABLE operations_co.tmp_sales_for_stock ( INDEX (estoque_id))
SELECT 
		t.itens_venda_id,
		t.item_id,
		t.order_id,
		t.numero_order,
		t.data_pedido,
		t.estoque_id,
		t.data_criacao,
		t.status 
	FROM wmsprod_co.itens_venda t 
JOIN (SELECT 
				estoque_id, 
				max(data_criacao) AS max 
			FROM wmsprod_co.itens_venda 
			GROUP BY estoque_id) t2 
ON t.estoque_id=t2.estoque_id 
;

UPDATE	operations_co.out_stock_hist_sample a 
 INNER JOIN operations_co.tmp_sales_for_stock b
	ON a.stock_item_id = b.estoque_id
SET 
	a.order_nr = b.numero_order,
	a.date_ordered = b.data_pedido,
	a.status_item = b.status,
	a.item_id = b.item_id;

UPDATE operations_co.out_stock_hist_sample
 INNER JOIN wmsprod_co.traducciones_producto 
ON out_stock_hist_sample.barcode_wms = traducciones_producto.identificador
SET out_stock_hist_sample.sku_simple = sku;

UPDATE operations_co.out_stock_hist_sample 
SET 
	sku_simple_blank= IF(sku_simple IS NULL,1,0);


#Fecha y datos de cuando sale vendido un producto
UPDATE operations_co.out_stock_hist_sample 
 INNER JOIN wmsprod_co.movimentacoes 
ON out_stock_hist_sample.stock_item_id = movimentacoes.estoque_id 
SET 
	out_stock_hist_sample.date_exit = data_criacao,
	out_stock_hist_sample.week_exit = operations_co.week_iso(data_criacao),
	out_stock_hist_sample.exit_type = "sold",
	out_stock_hist_sample.sold = 1
WHERE 
		out_stock_hist_sample.wh_location ="vendidos"
	AND movimentacoes.para_endereco="vendidos";

#Fecha y datos de cuando sale errado un producto
UPDATE operations_co.out_stock_hist_sample 
 INNER JOIN wmsprod_co.movimentacoes 
ON out_stock_hist_sample.stock_item_id = movimentacoes.estoque_id 
SET 
	out_stock_hist_sample.date_exit = data_criacao,
	out_stock_hist_sample.week_exit = operations_co.week_iso(data_criacao),
	out_stock_hist_sample.exit_type = "error"
WHERE 
		out_stock_hist_sample.wh_location ="Error_entrada"
	AND movimentacoes.para_endereco="Error_entrada";


UPDATE operations_co.out_stock_hist_sample 
 INNER JOIN wmsprod_co.movimentacoes 
	ON out_stock_hist_sample.stock_item_id = movimentacoes.estoque_id 
SET out_stock_hist_sample.wh_location_before_sold = de_endereco
WHERE movimentacoes.para_endereco="vendidos";


#CALL production.monitoring( "A_Master_Catalog" , "Colombia_Project",  240 );

#### A Master Catalog values
UPDATE operations_co.out_stock_hist_sample
 INNER JOIN development_co_project.A_Master_Catalog 
	ON out_stock_hist_sample.sku_simple = development_co_project.A_Master_Catalog.sku_simple
SET 
 out_stock_hist_sample.buyer = 							A_Master_Catalog.buyer,
 out_stock_hist_sample.head_buyer = 						A_Master_Catalog.Head_Buyer,
 out_stock_hist_sample.barcode_bob = 						A_Master_Catalog.barcode_ean,
 out_stock_hist_sample.sku_config = 						A_Master_Catalog.sku_config,
 out_stock_hist_sample.sku_name = 							A_Master_Catalog.sku_name,
 out_stock_hist_sample.sku_color = 						A_Master_Catalog.color,
 out_stock_hist_sample.sku_attribute = 					A_Master_Catalog.attribute,
 out_stock_hist_sample.brand = 							A_Master_Catalog.Brand,
 out_stock_hist_sample.visible = 							A_Master_Catalog.isVisible,
 out_stock_hist_sample.is_marketplace = 					A_Master_Catalog.isMarketPlace,
 #out_stock_hist_sample.stock_wms =						out_catalog_product_stock.TBD,
 #out_stock_hist_sample.reserved_wms = 					out_catalog_product_stock.TBD,
 #out_stock_hist_sample.sku_supplier_config = 				catalog_config.sku_supplier_config,
 out_stock_hist_sample.model = 							A_Master_Catalog.model,
 out_stock_hist_sample.supplier_name = 					A_Master_Catalog.Supplier,
 out_stock_hist_sample.supplier_id = 						A_Master_Catalog.id_supplier,
 out_stock_hist_sample.supplier_leadtime = 				A_Master_Catalog.delivery_time_supplier,
 out_stock_hist_sample.fulfillment_type_bob = 				A_Master_Catalog.fulfillment_type,
 out_stock_hist_sample.cost_w_o_vat = ifnull(
	IF (A_Master_Catalog.special_purchase_price IS NULL,A_Master_Catalog.cost_oms,
	IF (curdate() > A_Master_Catalog.special_to_date,A_Master_Catalog.cost_oms,
	IF (curdate() < A_Master_Catalog.special_from_date,A_Master_Catalog.cost_oms,
	IF (A_Master_Catalog.special_purchase_price < A_Master_Catalog.cost_oms,
				A_Master_Catalog.special_purchase_price,
				A_Master_Catalog.cost_oms)))),0),
 out_stock_hist_sample.price = ifnull(
	IF (A_Master_Catalog.special_price IS NULL,A_Master_Catalog.price,
	IF (curdate() > A_Master_Catalog.special_to_date,A_Master_Catalog.price,
	IF (curdate() < A_Master_Catalog.special_from_date,A_Master_Catalog.price,
	IF (A_Master_Catalog.special_price < A_Master_Catalog.price,
				A_Master_Catalog.special_price,
				A_Master_Catalog.price)))),0),
 out_stock_hist_sample.special_price = 					A_Master_Catalog.special_price,
 out_stock_hist_sample.delivery_cost_supplier = 			A_Master_Catalog.delivery_cost_supplier,
 out_stock_hist_sample.special_from_date = 				A_Master_Catalog.special_from_date,
 out_stock_hist_sample.special_to_date = 					A_Master_Catalog.special_to_date,
 out_stock_hist_sample.status_simple = 					A_Master_Catalog.status_simple,
 out_stock_hist_sample.status_config = 					A_Master_Catalog.status_config,
 out_stock_hist_sample.pet_status = 						A_Master_Catalog.pet_status,
 out_stock_hist_sample.tax_percentage = 					A_Master_Catalog.tax_percent,
 out_stock_hist_sample.category_1 = 						A_Master_Catalog.Cat1,
 out_stock_hist_sample.category_2 = 						A_Master_Catalog.Cat2,
 out_stock_hist_sample.category_3 = 						A_Master_Catalog.Cat3,
 out_stock_hist_sample.category_kpi = 						A_Master_Catalog.Cat_KPI,
 out_stock_hist_sample.category_bp = 						A_Master_Catalog.Cat_BP ;

UPDATE operations_co.out_stock_hist_sample
 INNER JOIN development_co_project.A_Master_Catalog 
	ON out_stock_hist_sample.sku_simple = development_co_project.A_Master_Catalog.sku_simple
SET 
 out_stock_hist_sample.cost_w_o_vat = ifnull(
	IF (A_Master_Catalog.special_purchase_price IS NULL,A_Master_Catalog.cost,
	IF (curdate() > A_Master_Catalog.special_to_date,A_Master_Catalog.cost,
	IF (curdate() < A_Master_Catalog.special_from_date,A_Master_Catalog.cost,
	IF (A_Master_Catalog.special_purchase_price < A_Master_Catalog.cost,
				A_Master_Catalog.special_purchase_price,
				A_Master_Catalog.cost)))),0)
WHERE cost_w_o_vat IS NULL or cost_w_o_vat = 0
;


#####Fulfillment Type Real

SELECT  'Update Own Warehouse',now();
DROP TABLE IF EXISTS operations_co.fulfillment_type_own_warehouse;
CREATE TABLE operations_co.fulfillment_type_own_warehouse (INDEX (item_id))
SELECT
 b.item_id,
 "Own Warehouse" as fulfillment_type_real
FROM
wmsprod_co.itens_venda b
INNER JOIN wmsprod_co.status_itens_venda c
 ON b.itens_venda_id = c.itens_venda_id
WHERE c.STATUS ='Estoque reservado';

UPDATE
 operations_co.out_stock_hist_sample a
INNER JOIN operations_co.fulfillment_type_own_warehouse b
 ON a.item_id = b.item_id
SET a.fulfillment_type_real = b.fulfillment_type_real;

SELECT  'Update Consignment',now();
DROP TABLE IF EXISTS operations_co.fulfillment_type_consignment;

CREATE TABLE operations_co.fulfillment_type_consignment (INDEX (item_id))
SELECT
 b.item_id,
 'Consignment' AS fulfillment_type_real
FROM
wmsprod_co.itens_venda b
INNER JOIN wmsprod_co.estoque c
 ON b.estoque_id = c.estoque_id
INNER JOIN wmsprod_co.itens_recebimento d
 ON c.itens_recebimento_id = d.itens_recebimento_id
INNER JOIN wmsprod_co.recebimento e
 ON d.recebimento_id = e.recebimento_id
INNER JOIN wmsprod_co.po_oms f
 ON e.inbound_document_identificator = f.name
INNER JOIN procurement_live_co.procurement_order g
 ON f.po_oms_id = g.id_procurement_order
WHERE (e.inbound_document_type_id = 7 OR e.inbound_document_type_id = 11);


UPDATE
 operations_co.out_stock_hist_sample a
INNER JOIN operations_co.fulfillment_type_consignment b
 ON a.item_id = b.item_id
SET a.fulfillment_type_real = b.fulfillment_type_real;

SELECT  'Update Dropshipping',now();
DROP TABLE IF EXISTS operations_co.fulfillment_type_dropshipping;
CREATE TABLE operations_co.fulfillment_type_dropshipping (INDEX (item_id))
SELECT
 b.item_id,
 "Dropshipping" as fulfillment_type_real
FROM
wmsprod_co.itens_venda b
INNER JOIN wmsprod_co.status_itens_venda c
 ON b.itens_venda_id = c.itens_venda_id
WHERE c.STATUS IN (	'DS estoque reservado',
			'Waiting dropshipping',
			'Dropshipping notified',
			'awaiting_fulfillment',
			'Electronic good'
		);

UPDATE
 operations_co.out_stock_hist_sample a
INNER JOIN operations_co.fulfillment_type_dropshipping b
 ON a.item_id = b.item_id
SET a.fulfillment_type_real = b.fulfillment_type_real;


SELECT  'Update Crossdocking',now();
DROP TABLE IF EXISTS operations_co.fulfillment_type_crossdocking;
CREATE TABLE operations_co.fulfillment_type_crossdocking (INDEX (item_id))
SELECT
 b.item_id,
 "Crossdocking" as fulfillment_type_real
FROM
wmsprod_co.itens_venda b
INNER JOIN wmsprod_co.status_itens_venda c
 ON b.itens_venda_id = c.itens_venda_id
WHERE c.STATUS IN (	'analisando quebra',
			'aguardando estoque');

UPDATE
 operations_co.out_stock_hist_sample a
INNER JOIN operations_co.fulfillment_type_crossdocking b
 ON a.item_id = b.item_id
SET a.fulfillment_type_real = b.fulfillment_type_real;
			
UPDATE operations_co.out_stock_hist_sample 
SET 
	cancelled= 1,
	fulfillment_type_real='Own Warehouse'
WHERE status_item IN (	'Cancelado', 
												'quebra tratada', 
												'quebrado', 
												'Precancelado', 
												'backorder_tratada');

UPDATE operations_co.out_stock_hist_sample
INNER JOIN wmsprod_co.movimentacoes 
	ON out_stock_hist_sample.stock_item_id = movimentacoes.estoque_id
SET out_stock_hist_sample.fulfillment_type_real = 'Own Warehouse'
WHERE
	movimentacoes.de_endereco = 'retorno de cancelamento'
OR movimentacoes.de_endereco = 'vendidos'; 

UPDATE operations_co.out_stock_hist_sample
SET fulfillment_type_bp = CASE
				WHEN fulfillment_type_real IN( 'crossdocking', 'consignment')
				THEN fulfillment_type_real
				WHEN fulfillment_type_real = 'Own Warehouse'
				THEN 'Outright Buying'
				WHEN fulfillment_type_real = 'dropshipping' 
				THEN 'other'
END;


/*
UPDATE operations_co.out_stock_hist_sample 
SET out_stock_hist_sample.fulfillment_type_real = "Consignment"
WHERE wh_location like '%CONSI%';
*/

### Datos de Purchase Order

UPDATE operations_co.out_stock_hist_sample a
 INNER JOIN wmsprod_co.estoque b 
	ON a.stock_item_id = b.estoque_id
 INNER JOIN wmsprod_co.itens_recebimento c 
	ON b.itens_recebimento_id = c.itens_recebimento_id
 INNER JOIN wmsprod_co.recebimento d 
	ON c.recebimento_id = d.recebimento_id
SET 
	purchase_order = d.inbound_document_identificator;
	

DROP TEMPORARY TABLE IF EXISTS operations_co.tmp_purchase_order_details;
CREATE TEMPORARY TABLE operations_co.tmp_purchase_order_details (INDEX(purchase_order, sku_simple))
SELECT
 purchase_order,
 sku_simple,
 cost_oms,
 purchase_order_type,
 payment_terms
FROM operations_co.out_procurement_tracking
WHERE is_cancelled = 0 
and 	is_deleted = 0
GROUP BY 
  purchase_order,
 sku_simple
;

UPDATE operations_co.out_stock_hist_sample a 
 INNER JOIN operations_co.tmp_purchase_order_details b 
ON a.purchase_order=b.purchase_order and a.sku_simple=b.sku_simple 
SET
	 a.cost_w_o_vat= if(b.cost_oms <= 0 OR b.cost_oms IS NULL,a.cost_w_o_vat,b.cost_oms),
	 a.purchase_order_type=b.purchase_order_type,
	 a.payment_terms=b.payment_terms
;

UPDATE operations_co.out_stock_hist_sample a 
 INNER JOIN operations_co.tbl_purchase_order_stock b 
	ON a.purchase_order = b.oc 
	AND a.sku_simple = b.sku
SET
	a.cost_w_o_vat = b.costo,
	a.purchase_order_type = tipo_orden,
	a.payment_terms = b.tipo_pago;


UPDATE operations_co.out_stock_hist_sample a
 INNER JOIN operations_co.tbl_purchase_order b 
	ON a.purchase_order = b.oc 
	AND a.sku_simple = b.sku
SET
	a.cost_w_o_vat = b.costo,
	a.purchase_order_type = 'Crossdocking',
	a.payment_terms = b.tipo_pago;
	
UPDATE
 operations_co.out_stock_hist_sample a
SET a.fulfillment_type_real = 'Consignment'
WHERE purchase_order_type = 'Consignment';


### Datos de costos
UPDATE operations_co.out_stock_hist_sample
SET 
 out_stock_hist_sample.cost = out_stock_hist_sample.cost_w_o_vat*(1 + tax_percentage / 100);

/*#Se actualizan costos de oms de acuerdo a las notas a credito
UPDATE operations_co.out_stock_hist_sample od
JOIN operations_co.tbl_cambios_costos cc
ON od.sku_simple=cc.sku
SET od.cost_w_o_vat = cc.cost
WHERE od.date_ordered BETWEEN cc.from_date AND cc.to_date;
*/

UPDATE operations_co.out_stock_hist_sample
SET cogs = cost_w_o_vat + delivery_cost_supplier;


UPDATE operations_co.out_stock_hist_sample a
 INNER JOIN bob_live_co.catalog_simple b
 ON a.sku_simple = b.sku
 INNER JOIN operations_co.simple_variation c
 ON b.id_catalog_simple = c.fk_catalog_simple
SET
 a.sku_variation =  c.variation;


call operations_co.out_catalog_product_stock;

# Indicadores de Stock para consulta
UPDATE operations_co.out_catalog_product_stock
SET share_reserved_bob = 	IF (	reservedbob = 0,0,
													IF (	(reservedbob - stock_wms_reserved) / stock_wms > 1,1,(reservedbob - stock_wms_reserved) / stock_wms)), share_stock_bob = availablebob / stock_wms;

SELECT
	'Actualizando niveles de stock de BOB',
	now();
	
UPDATE operations_co.out_catalog_product_stock a
         INNER JOIN
    operations_co.vw_stock_wms b ON a.sku = b.sku_simple 
SET 
    a.stock_wms = b.stock_wms
WHERE
    b.stock_wms > 0;



UPDATE operations_co.out_catalog_product_stock a
         INNER JOIN
    operations_co.vw_stock_wms_reserved b ON a.sku = b.sku_simple 
SET 
    a.stock_wms_reserved = b.stock_wms_reserved;
	

UPDATE operations_co.out_catalog_product_stock 
SET 
    stock_wms_reserved = 0
WHERE
    stock_wms_reserved IS NULL;


UPDATE operations_co.out_catalog_product_stock 
SET 
    share_reserved_bob = IF(reservedbob = 0,
        0,
        IF((reservedbob - stock_wms_reserved) / stock_wms > 1,
            1,
            (reservedbob - stock_wms_reserved) / stock_wms)),
    share_stock_bob = availablebob / stock_wms;

UPDATE operations_co.out_stock_hist_sample
 INNER JOIN operations_co.out_catalog_product_stock 
	ON out_stock_hist_sample.sku_simple = out_catalog_product_stock.sku
SET 
 out_stock_hist_sample.stock_bob = out_catalog_product_stock.stockbob,
 out_stock_hist_sample.own_stock_bob = out_catalog_product_stock.ownstock,
 out_stock_hist_sample.supplier_stock_bob = out_catalog_product_stock.supplierstock,
 out_stock_hist_sample.reserved_bob = out_catalog_product_stock.reservedbob,
 out_stock_hist_sample.stock_available_bob = out_catalog_product_stock.availablebob,
 out_stock_hist_sample.reservedbob = out_catalog_product_stock.share_reserved_bob;


#Parche del catálogo quitar despues
UPDATE operations_co.out_stock_hist_sample
SET buyer = trim(BOTH ' ' FROM REPLACE(REPLACE(REPLACE(buyer, '\t', ''), '\n',''),'\r',''));

UPDATE operations_co.out_stock_hist_sample
SET planner =
IF (	out_stock_hist_sample.buyer IN (
		"B0 - Jessica",
		"B3 - Maria Fernanda",
		"B4 - Freddy",
		"B5 - Katherine",
		"B6 - Ximena",
		"B8 - Guillermo",
		"B8 - Cristina"
	),
	"Home",

IF (
	out_stock_hist_sample.buyer IN (
		"B2 - Alexander",
		"B1 - Maritza"
	),
	"Electronics",
	"Fashion"
)
);


UPDATE operations_co.out_stock_hist_sample
SET out_stock_hist_sample.days_in_stock = 	CASE
																			WHEN 	date_exit IS NULL 
																			THEN	datediff(curdate(), date_entrance)
																			ELSE	datediff(date_exit, date_entrance)
																		END;
TRUNCATE operations_co.skus_age;

INSERT INTO operations_co.skus_age
select * from operations_co.vw_inv_avg_ageing;

UPDATE operations_co.out_stock_hist_sample 
INNER JOIN operations_co.skus_age 
ON 
	out_stock_hist_sample.sku_simple= skus_age.sku_simple 
SET 
	out_stock_hist_sample.sku_age = truncate(skus_age.avg_age,0);


UPDATE operations_co.out_stock_hist_sample 
 INNER JOIN operations_co.tbl_bi_ops_tracking_suppliers
	ON out_stock_hist_sample.supplier_id = tbl_bi_ops_tracking_suppliers.id
SET out_stock_hist_sample.tracker = tbl_bi_ops_tracking_suppliers.tracker_name;


UPDATE operations_co.out_stock_hist_sample
 INNER JOIN development_co_project.A_Master_Catalog 
	ON out_stock_hist_sample.sku_simple 	= A_Master_Catalog.sku_simple
SET 
	out_stock_hist_sample.content_qc 			= A_Master_Catalog.pet_approved,
	out_stock_hist_sample.status_config 		= A_Master_Catalog.status_config,
	out_stock_hist_sample.status_simple 		= A_Master_Catalog.status_simple,
	out_stock_hist_sample.status_pet = IF(A_Master_Catalog.pet_status IS NULL, 'Pending All', 
IF(A_Master_Catalog.pet_status = "creation,edited", "Pending Images", 
IF(A_Master_Catalog.pet_status = "creation,images", "Pending Edited", 
IF(A_Master_Catalog.pet_status = "creation", "Pending EnI", 
IF(A_Master_Catalog.pet_status = "creation,edited,images", "Ok", "Error")))));

#Actualizar el sku status
UPDATE operations_co.out_stock_hist_sample
SET out_stock_hist_sample.status_sku = 
		IF(out_stock_hist_sample.status_config="deleted" or out_stock_hist_sample.status_simple="deleted", "Deleted", 
		IF(out_stock_hist_sample.status_config="inactive" or out_stock_hist_sample.status_simple="inactive", "Inactive", 
		IF(out_stock_hist_sample.status_config="active" and out_stock_hist_sample.status_simple="active", "Active", NULL)));

#Actualizar la razón de no visibilidad
UPDATE operations_co.out_stock_hist_sample
SET out_stock_hist_sample.reason_not_visible = 
		IF(out_stock_hist_sample.status_pet <> "OK", out_stock_hist_sample.status_pet, 
		IF(out_stock_hist_sample.status_sku <> "active", out_stock_hist_sample.status_sku, 
		IF(out_stock_hist_sample.content_qc = 0, "Quality Check Failed", "Stock levels")))
WHERE visible = 0;


UPDATE operations_co.out_stock_hist_sample
SET 
 out_stock_hist_sample.quarantine = 1
 WHERE	wh_location LIKE '%DEF%'
OR		wh_location LIKE '%REP%'
OR		wh_location LIKE '%GRT%'
OR		wh_location LIKE '%AVE%'
OR		wh_location LIKE '%cuerentena%'
OR		wh_location LIKE '%Cuarentena_Inverse%'
;

UPDATE operations_co.out_stock_hist_sample 
Set 
    quarantine_type = if(wh_location liKe '%DEF%',
        'DEF',
        if(wh_location like '%REP%',
            'REP',
            if(wh_location like '%GRT%',
                'GRT',
                if(wh_location like '%AVE%',
                    'AVE',
					if(wh_location like 'cuerentena',
                    'Cuarentena',
						if(wh_location like '%Cuarentena_Inverse%',
							'Cuarentena_Inverse',
							'PENDING'))))))
where
    quarantine = 1;


DROP TEMPORARY TABLE IF EXISTS operations_co.tmp_movimentacoes;
CREATE TEMPORARY TABLE operations_co.tmp_movimentacoes (PRIMARY KEY (estoque_id))
SELECT 
								estoque_id, 
								para_endereco, 
								min(date(data_criacao)) AS data_movimentacao 
							FROM wmsprod_co.movimentacoes 
							WHERE para_endereco LIKE '%DEF%' 
							OR 		para_endereco LIKE '%REP%' 
							OR		para_endereco LIKE '%GRT%' 
							OR 		para_endereco LIKE '%AVE%' 
							OR 		para_endereco LIKE '%cuerentena%'
							GROUP BY estoque_id;

UPDATE operations_co.out_stock_hist_sample t 
  INNER JOIN operations_co.tmp_movimentacoes t2
ON 	t.stock_item_id = t2.estoque_id
SET t.quarantine_date = t2.data_movimentacao;

UPDATE operations_co.out_stock_hist_sample
SET 
 quarantine_date=date_entrance,
 quarantine_inbound = 1
WHERE quarantine=1 
and quarantine_date IS NULL;

UPDATE operations_co.out_stock_hist_sample
 INNER JOIN bob_live_co.catalog_config 
	ON out_stock_hist_sample.sku_config = catalog_config.sku
SET 
out_stock_hist_sample.product_measures = catalog_config.product_measures, 
out_stock_hist_sample.package_width = greatest(cast(replace(IF(isnull(catalog_config.package_width),1,catalog_config.package_width),',','.') as DECIMAL(6,2)),1.0), 
out_stock_hist_sample.package_length = greatest(cast(replace(IF(isnull(catalog_config.package_length),1,catalog_config.package_length),',','.') as DECIMAL(6,2)),1.0), 
out_stock_hist_sample.package_height = greatest(cast(replace(IF(isnull(catalog_config.package_height),1,catalog_config.package_height),',','.') as DECIMAL(6,2)),1.0),
out_stock_hist_sample.package_weight = greatest(cast(replace(IF(isnull(catalog_config.package_weight),1,catalog_config.package_weight),',','.') as DECIMAL(6,2)),1.0)
;


UPDATE operations_co.out_stock_hist_sample
SET out_stock_hist_sample.vol_weight = package_height * package_length * package_width / 5000;

UPDATE operations_co.out_stock_hist_sample
SET out_stock_hist_sample.max_vol_w_vs_w = CASE
WHEN vol_weight > package_weight THEN
	vol_weight
ELSE
	package_weight
END;

UPDATE operations_co.out_stock_hist_sample
SET out_stock_hist_sample.package_measure_new = 	CASE
																						WHEN max_vol_w_vs_w > 35 
																						THEN 'oversized'
																					ELSE
																					(	CASE
																							WHEN max_vol_w_vs_w > 5 
																							THEN 'large'
																							ELSE
																							(	CASE
																									WHEN max_vol_w_vs_w > 2 
																									THEN 'medium'
																									ELSE 'small'
																								END)
																							END)
																					END;

																					
UPDATE operations_co.out_stock_hist_sample
SET 
 out_stock_hist_sample.sold_last_60 = CASE
																		WHEN datediff(curdate(), date_exit) < 60 
																		THEN 1
																		ELSE 0
																	END,
 out_stock_hist_sample.sold_last_30 = CASE
																		WHEN datediff(curdate(), date_exit) < 30 
																		THEN 1
																		ELSE 0
																	END,
 out_stock_hist_sample.sold_last_7 = CASE
																		WHEN datediff(curdate(), date_exit) < 7 
																		THEN 1
																		ELSE 0
																	END,
 out_stock_hist_sample.sold_yesterday = 	CASE
																		WHEN datediff(curdate(),(date_sub(date_exit, INTERVAL 1 DAY))) = 0 
																		THEN 1
																		ELSE 0
																	END,
 out_stock_hist_sample.entrance_last_30 = CASE
																		WHEN datediff(curdate(), date_entrance) < 30 
																		THEN 1
																		ELSE 0
																	END;

																	

UPDATE operations_co.out_stock_hist_sample
SET out_stock_hist_sample.in_stock_cost_w_o_vat = out_stock_hist_sample.cost_w_o_vat
WHERE
	out_stock_hist_sample.in_stock = 1;

UPDATE operations_co.out_stock_hist_sample
SET out_stock_hist_sample.sold_last_30_cost_w_o_vat = out_stock_hist_sample.cost_w_o_vat,
 out_stock_hist_sample.sold_last_30_price = out_stock_hist_sample.price
WHERE
	out_stock_hist_sample.sold_last_30 = 1;

/* Should go in Extra Process */
TRUNCATE operations_co.pro_stock_hist_sku_count;

INSERT INTO operations_co.pro_stock_hist_sku_count (
	sku_simple, 
	sku_count) 
SELECT
	out_stock_hist_sample.sku_simple,
	sum(out_stock_hist_sample.item_counter) AS sumofitem_counter
FROM
	operations_co.out_stock_hist_sample
GROUP BY
	out_stock_hist_sample.sku_simple,
	out_stock_hist_sample.in_stock
HAVING
	out_stock_hist_sample.in_stock = 1;

UPDATE operations_co.out_stock_hist_sample
 INNER JOIN operations_co.pro_stock_hist_sku_count 
 ON out_stock_hist_sample.sku_simple = pro_stock_hist_sku_count.sku_simple
SET out_stock_hist_sample.sku_counter = 1 / pro_stock_hist_sku_count.sku_count;


TRUNCATE 
	operations_co.pro_stock_hist_sold_last_30_count;
INSERT INTO operations_co.pro_stock_hist_sold_last_30_count (
	sold_last_30,
	sku_simple,
	count_of_item_counter
) SELECT
	out_stock_hist_sample.sold_last_30,
	out_stock_hist_sample.sku_simple,
	count(out_stock_hist_sample.item_counter) AS count_of_item_counter
FROM
	operations_co.out_stock_hist_sample
GROUP BY
	out_stock_hist_sample.sold_last_30,
	out_stock_hist_sample.sku_simple
HAVING
	out_stock_hist_sample.sold_last_30 = 1;


UPDATE operations_co.out_stock_hist_sample
 INNER JOIN operations_co.pro_stock_hist_sold_last_30_count 
	ON out_stock_hist_sample.sku_simple = pro_stock_hist_sold_last_30_count.sku_simple
SET out_stock_hist_sample.sold_last_30_counter = 1 / pro_stock_hist_sold_last_30_count.count_of_item_counter;


UPDATE operations_co.out_stock_hist_sample
SET out_stock_hist_sample.position_item_size_type = CASE
																							WHEN position_type = "safe" 
																							THEN "small"
																							WHEN position_type = "mezanine" 
																							THEN "small"
																							WHEN position_type = "muebles" 
																							THEN "large"
																							ELSE "tbd"
																						END;
	
TRUNCATE	operations_co.items_procured_in_transit;

INSERT INTO operations_co.items_procured_in_transit (
	sku_simple,
	number_ordered,
	unit_price
) SELECT
	catalog_simple.sku,
	count(procurement_order_item.id_procurement_order_item) AS countofid_procurement_order_item,
	avg(procurement_order_item.unit_price) AS avgofunit_price
FROM
	bob_live_co.catalog_simple
 INNER JOIN procurement_live_co.procurement_order_item
ON catalog_simple.id_catalog_simple = procurement_order_item.fk_catalog_simple
	 INNER JOIN procurement_live_co.procurement_order 
ON procurement_order_item.fk_procurement_order = procurement_order.id_procurement_order
WHERE	procurement_order_item.is_deleted = 0
AND		procurement_order.is_cancelled = 0
AND		procurement_order_item.sku_received = 0
GROUP BY
	catalog_simple.sku;


UPDATE operations_co.out_stock_hist_sample
 INNER JOIN operations_co.items_procured_in_transit 
	ON out_stock_hist_sample.sku_simple = items_procured_in_transit.sku_simple
SET 
 out_stock_hist_sample.items_procured_in_transit = number_ordered,
 out_stock_hist_sample.procurement_price = unit_price;

	
#CALL production.monitoring( "operations_co.out_procurement_tracking" , "Colombia",  240 );


/*
# Pendiente agregar columnas desde procurement:
*/



UPDATE operations_co.out_stock_hist_sample 
SET 
	cancelled= 1,
	fulfillment_type_real='Own Warehouse'
WHERE status_item IN (	'Cancelado', 
												'quebra tratada', 
												'quebrado', 
												'Precancelado', 
												'backorder_tratada') 
AND in_stock = 1 
AND reserved = 0;


#Próximos 3 queries chequean si el sku está en stock, cuando hay al menos un item disponible
TRUNCATE TABLE operations_co.skus_in_stock;

INSERT INTO operations_co.skus_in_stock
select distinct(sku_simple) from operations_co.out_stock_hist_sample
WHERE in_stock=1 and reserved=0;

UPDATE operations_co.out_stock_hist_sample
SET out_stock_hist_sample.sku_in_stock=1
WHERE sku_simple in (select sku from operations_co.skus_in_stock);

#Proximos 6 queries revisan si se vendió los últimos 30 15 y 7 dias para calcular los dias de inventario de planning method
UPDATE operations_co.out_stock_hist_sample
SET sold_last_30_order = 	CASE
														WHEN datediff(curdate(), date_ordered) <= 30 
														THEN 1
														ELSE 0
													END
WHERE	cancelled <> 1;


UPDATE operations_co.out_stock_hist_sample
SET sold_last_15_order = 	CASE
														WHEN datediff(curdate(), date_ordered) <= 15 
														THEN 1
														ELSE 0
													END
WHERE	cancelled <> 1;


UPDATE operations_co.out_stock_hist_sample
SET sold_last_7_order = 	CASE
														WHEN datediff(curdate(), date_ordered) <= 7 
														THEN 1
														ELSE 0
													END
WHERE	cancelled <> 1;

UPDATE operations_co.out_stock_hist_sample
SET 
 out_stock_hist_sample.sold_last_30_order_cost_w_o_vat = out_stock_hist_sample.cost_w_o_vat,
 out_stock_hist_sample.sold_last_30_order_price = out_stock_hist_sample.price
WHERE
	out_stock_hist_sample.sold_last_30_order = 1;
	
	
UPDATE operations_co.out_stock_hist_sample
SET 
 out_stock_hist_sample.sold_last_15_order_cost_w_o_vat = out_stock_hist_sample.cost_w_o_vat
WHERE
	out_stock_hist_sample.sold_last_15_order = 1;
	

UPDATE operations_co.out_stock_hist_sample
SET 
 out_stock_hist_sample.sold_last_7_order_cost_w_o_vat = out_stock_hist_sample.cost_w_o_vat
WHERE
	out_stock_hist_sample.sold_last_7_order = 1;

/*Aquí me quedé */
# What is on the outlet
TRUNCATE TABLE operations_co.pro_skus_in_outlet;

INSERT INTO operations_co.pro_skus_in_outlet 
SELECT
	sku,
	NAME
FROM
	(
		SELECT
			cc.*, 
			lft,
			rgt,
			rgt - lft,
			c2.sku,
			c. NAME,
			c. STATUS
		FROM
			bob_live_co.catalog_config_has_catalog_category cc
		 INNER JOIN bob_live_co.catalog_category c ON id_catalog_category = fk_catalog_category
		 INNER JOIN bob_live_co.catalog_config c2 ON cc.fk_catalog_config = c2.id_catalog_config
		WHERE
			id_catalog_category > 1
		) AS a
WHERE
	a. NAME LIKE '%liquida%';

UPDATE operations_co.out_stock_hist_sample a
 INNER JOIN operations_co.pro_skus_in_outlet b 
	ON a.sku_config = b.sku
SET outlet = 1
WHERE	a.in_stock = 1
AND 	a.reserved = 0;

#Sell list por config - 42 días de ventas
TRUNCATE operations_co.sell_rate_config;

INSERT INTO operations_co.sell_rate_config (
	sku_config,
	num_sales,
	num_items,
	average_days_in_stock
) SELECT
	sold.sku_config,
	sold.vendidos,
	instockreal.items_total,
	instockreal.days
FROM
	(	SELECT
			sku_config,
			sum(sold) AS vendidos
		FROM
			operations_co.out_stock_hist_sample
		WHERE
			(	in_stock = 1
				OR exit_type = 'sold')
		AND date_exit BETWEEN CURDATE() - INTERVAL 42 DAY
		AND CURDATE()
		GROUP BY
			sku_config
	) AS sold
 INNER JOIN (
	SELECT
		sku_config,
		sum(item_counter) AS items_total,
		avg(days_in_stock) AS days
	FROM
		operations_co.out_stock_hist_sample
	WHERE
		in_stock = 1
	GROUP BY
		sku_config
) AS instockreal ON sold.sku_config = instockreal.sku_config;

UPDATE operations_co.sell_rate_config
SET num_items_available = (
	SELECT
		sum(in_stock)
	FROM
		operations_co.out_stock_hist_sample
	WHERE	reserved = 0
	AND 	sell_rate_config.sku_config = out_stock_hist_sample.sku_config
	GROUP BY
		sku_config
);

UPDATE operations_co.sell_rate_config
SET average_sell_rate =
IF (num_sales = 0,NULL,	CASE
													WHEN average_days_in_stock = 0 
													THEN 0
													ELSE num_sales / 42
												END
);

UPDATE operations_co.sell_rate_config
SET remaining_days =	IF (average_sell_rate IS NULL,NULL,	CASE															
															WHEN average_sell_rate = 0 
															THEN 0
															ELSE num_items_available / average_sell_rate
															END
);


UPDATE operations_co.out_stock_hist_sample
 INNER JOIN operations_co.sell_rate_config 
	ON out_stock_hist_sample.sku_config = sell_rate_config.sku_config
SET out_stock_hist_sample.average_remaining_days = sell_rate_config.remaining_days,
	out_stock_hist_sample.sales_rate_config_42 = ((sell_rate_config.num_sales/42)/sell_rate_config.num_items_available);


#Sell list por config - 30 días de ventas
TRUNCATE operations_co.sell_rate_config_30;

INSERT INTO operations_co.sell_rate_config_30 (
	sku_config,
	num_sales,
	num_items,
	average_days_in_stock
) SELECT
	sold.sku_config,
	sold.vendidos,
	instockreal.items_total,
	instockreal.days
FROM
	(	SELECT
			sku_config,
			sum(sold) AS vendidos
		FROM
			operations_co.out_stock_hist_sample
		WHERE
			(	in_stock = 1
				OR exit_type = 'sold')
		AND date_exit BETWEEN CURDATE() - INTERVAL 30 DAY
		AND CURDATE()
		GROUP BY
			sku_config
	) AS sold
 INNER JOIN (
	SELECT
		sku_config,
		sum(item_counter) AS items_total,
		avg(days_in_stock) AS days
	FROM
		operations_co.out_stock_hist_sample
	WHERE
		in_stock = 1
	GROUP BY
		sku_config
) AS instockreal ON sold.sku_config = instockreal.sku_config;

UPDATE operations_co.sell_rate_config_30
SET num_items_available = (
	SELECT
		sum(in_stock)
	FROM
		operations_co.out_stock_hist_sample
	WHERE	reserved = 0
	AND 	sell_rate_config_30.sku_config = out_stock_hist_sample.sku_config
	GROUP BY
		sku_config
);

UPDATE operations_co.sell_rate_config_30
SET average_sell_rate =
IF (num_sales = 0,NULL,	CASE
													WHEN average_days_in_stock = 0 
													THEN 0
													ELSE num_sales / 30
												END
);

UPDATE operations_co.sell_rate_config_30
SET remaining_days =	IF (average_sell_rate IS NULL,NULL,	CASE
																														WHEN average_sell_rate = 0 
																														THEN 0
																														ELSE num_items_available / average_sell_rate
																													END
																													);

UPDATE operations_co.out_stock_hist_sample
 INNER JOIN operations_co.sell_rate_config_30 
	ON out_stock_hist_sample.sku_config = sell_rate_config_30.sku_config
SET out_stock_hist_sample.average_remaining_days_30 = sell_rate_config_30.remaining_days;

#Sell list por config - 7 días de ventas
TRUNCATE operations_co.sell_rate_config_7;

INSERT INTO operations_co.sell_rate_config_7 (
	sku_config,
	num_sales,
	num_items,
	average_days_in_stock
) SELECT
	sold.sku_config,
	sold.vendidos,
	instockreal.items_total,
	instockreal.days
FROM
	(	SELECT
			sku_config,
			sum(sold) AS vendidos
		FROM
			operations_co.out_stock_hist_sample
		WHERE
			(	in_stock = 1
				OR exit_type = 'sold')
		AND date_exit BETWEEN CURDATE() - INTERVAL 7 DAY
		AND CURDATE()
		GROUP BY
			sku_config
	) AS sold
 INNER JOIN (
	SELECT
		sku_config,
		sum(item_counter) AS items_total,
		avg(days_in_stock) AS days
	FROM
		operations_co.out_stock_hist_sample
	WHERE
		in_stock = 1
	GROUP BY
		sku_config
) AS instockreal ON sold.sku_config = instockreal.sku_config;

UPDATE operations_co.sell_rate_config_7
SET num_items_available = (
	SELECT
		sum(in_stock)
	FROM
		operations_co.out_stock_hist_sample
	WHERE	reserved = 0
	AND 	sell_rate_config_7.sku_config = out_stock_hist_sample.sku_config
	GROUP BY
		sku_config
);

UPDATE operations_co.sell_rate_config_7
SET average_sell_rate =
IF (num_sales = 0,NULL,	CASE
													WHEN average_days_in_stock = 0 
													THEN 0
													ELSE num_sales / 7
												END
);

UPDATE operations_co.sell_rate_config_7
SET remaining_days =	IF (average_sell_rate IS NULL,NULL,	CASE
																														WHEN average_sell_rate = 0 
																														THEN 0
																														ELSE num_items_available / average_sell_rate
																													END
																													);

UPDATE operations_co.out_stock_hist_sample
 INNER JOIN operations_co.sell_rate_config_7 
	ON out_stock_hist_sample.sku_config = sell_rate_config_7.sku_config
SET out_stock_hist_sample.average_remaining_days_7 = sell_rate_config_7.remaining_days;

DROP TABLE IF EXISTS operations_co.pro_max_days_in_stock;
CREATE TABLE operations_co.pro_max_days_in_stock
SELECT
	sku_config,
	avg(days_in_stock) as max_days_in_stock
FROM
	operations_co.out_stock_hist_sample
WHERE
	in_stock = 1
GROUP BY
	sku_config;


UPDATE operations_co.out_stock_hist_sample
 INNER JOIN operations_co.pro_max_days_in_stock 
	ON out_stock_hist_sample.sku_config = pro_max_days_in_stock.sku_config
SET out_stock_hist_sample.max_days_in_stock = pro_max_days_in_stock.max_days_in_stock;


UPDATE operations_co.out_stock_hist_sample SET is_sell_list = 1
WHERE max_days_in_stock > 30
and in_stock= 1
and reserved= 0
and fulfillment_type_real <> 'Consignment' 
and fulfillment_type_real is not null
and category_1 = 'Home and Living' 
and (average_remaining_days > 150
or average_remaining_days IS NULL);

UPDATE operations_co.out_stock_hist_sample SET is_sell_list = 1
WHERE max_days_in_stock > 30
and in_stock= 1
and reserved= 0
and fulfillment_type_real <> 'Consignment' 
and fulfillment_type_real is not null
and category_1 = 'Fashion' 
and (average_remaining_days > 120
or average_remaining_days IS NULL);

UPDATE operations_co.out_stock_hist_sample SET is_sell_list = 1
WHERE max_days_in_stock > 30
and in_stock= 1
and reserved= 0
and fulfillment_type_real <> 'Consignment' 
and fulfillment_type_real is not null
and category_1 = 'Electrónicos' 
and (average_remaining_days > 60
or average_remaining_days IS NULL);

UPDATE operations_co.out_stock_hist_sample SET is_sell_list = 1
WHERE max_days_in_stock > 30
and in_stock= 1
and reserved= 0
and fulfillment_type_real <> 'Consignment' 
and fulfillment_type_real is not null
and category_1 not in ('Home and Living','Fashion','Electrónicos') 
and (average_remaining_days > 90
or average_remaining_days IS NULL);


update operations_co.out_stock_hist_sample
set 
out_stock_hist_sample.interval_remaining_days=if(average_remaining_days is null,"Infinito",
if(average_remaining_days<7,"< 8",if(average_remaining_days<15,"8-15", if(average_remaining_days<30,"15-29", 
if(average_remaining_days<45,"30-44",if(average_remaining_days<60,"45-59",
if(average_remaining_days<90,"60-89",if(average_remaining_days<120,"90-119",
if(average_remaining_days<150,"120-149",if(average_remaining_days<180,"150-179",
if(average_remaining_days<210,"180-209",if(average_remaining_days<240,"210-239",
if(average_remaining_days<270,"240-269",if(average_remaining_days<300,"270-299",
if(average_remaining_days<330,"300-229",if(average_remaining_days<=360,"330-360","> 360"))))))))))))))));


update operations_co.out_stock_hist_sample
set 
out_stock_hist_sample.interval_sku_age=if(max_days_in_stock is null,"Infinito",
if(max_days_in_stock<7,"< 8",if(max_days_in_stock<15,"8-15", if(max_days_in_stock<30,"15-29", 
if(max_days_in_stock<45,"30-44",if(max_days_in_stock<60,"45-59",
if(max_days_in_stock<90,"60-89",if(max_days_in_stock<120,"90-119",
if(max_days_in_stock<150,"120-149",if(max_days_in_stock<180,"150-179",
if(max_days_in_stock<210,"180-209",if(max_days_in_stock<240,"210-239",
if(max_days_in_stock<270,"240-269",if(max_days_in_stock<300,"270-299",
if(max_days_in_stock<330,"300-229",if(max_days_in_stock<=360,"330-360","> 360"))))))))))))))));