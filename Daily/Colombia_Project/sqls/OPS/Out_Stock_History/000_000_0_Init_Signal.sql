﻿INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  UPDATEd_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia_Project', 
  'out_stock_hist',
  'start',
  NOW(),
  max(date_exit),
  count(*),
  count(item_counter)
FROM
  operations_co.out_stock_hist;