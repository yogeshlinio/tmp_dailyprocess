REPLACE operations_co.out_order_tracking SELECT * FROM operations_co.out_order_tracking_sample;

#DROP TABLE IF EXISTS operations_co.out_order_tracking;
#CREATE TABLE operations_co.out_order_tracking LIKE operations_co.out_order_tracking_sample; 
#INSERT INTO operations_co.out_order_tracking SELECT * FROM operations_co.out_order_tracking_sample; 

#INSERT INTO `production`.`table_monitoring_report` VALUES (NULL, 'Daily Procurement - CO', NOW());
#INSERT INTO `production`.`table_monitoring_report` VALUES (NULL, 'Daily Warehouse - CO', NOW());
#INSERT INTO `production`.`table_monitoring_report` VALUES (NULL, 'Daily Deliveries - CO', NOW());
#INSERT INTO `production`.`table_monitoring_report` VALUES (NULL, 'Daily Fulfillment - CO', NOW());

DROP TABLE IF EXISTS production_co.out_order_tracking;
CREATE TABLE production_co.out_order_tracking LIKE operations_co.out_order_tracking; 
INSERT INTO production_co.out_order_tracking SELECT * FROM operations_co.out_order_tracking; 
