SELECT  'Monitoring_log',now();
INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia_Project', 
  'out_order_tracking',
  'start',
  NOW(),
  max(datetime_ordered),
  count(*),
  count(item_counter)
FROM
  operations_co.out_order_tracking;