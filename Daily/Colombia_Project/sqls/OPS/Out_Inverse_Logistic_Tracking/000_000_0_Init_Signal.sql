-- --------------------------------------------------------------------------------
-- Nombre:1100_ops_out_inverse_logistics_trackins.sql
-- Pais: Colombia
-- Autor: Natali Serrano, Luis Ochoa
-- Fecha creaci�n: 28/01/2014
-- Version: 1.0
-- Fecha ultima modificacion: 2014-03-05
-- --------------------------------------------------------------------------------
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia_Project', 
  'out_inverse_logistics_tracking',
  'start',
  NOW(),
  max(date_ordered),
  count(*),
  count(item_counter)
FROM
  operations_co.out_inverse_logistics_tracking;
