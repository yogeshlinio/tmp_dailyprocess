DROP TABLE IF EXISTS operations_co.out_inverse_logistics_tracking_sample;
CREATE TABLE operations_co.out_inverse_logistics_tracking_sample LIKE operations_co.out_inverse_logistics_tracking;
INSERT INTO operations_co.out_inverse_logistics_tracking_sample (
	item_id,
	order_number,
	sku_config,
	sku_simple,
	sku_name,
	price_w_o_vat,
	cost_w_o_vat,
	supplier_id,
	supplier_name,
	buyer,
	head_buyer,
	package_height,
	package_length,
	package_width,
	package_weight,
	fulfillment_type_bob,
	fulfillment_type_real,
	shipping_carrier,
	date_ordered,
	date_exported,
	date_backorder,
	date_backorder_tratada,
	date_backorder_accepted,
	date_po_created,
	date_po_updated,
	date_po_issued,
	date_po_confirmed,
	#date_sent_tracking_code,
	date_procured,
	date_ready_to_pick,
	date_ready_to_ship,
	date_tracking_code_created,
	date_shipped,
	date_1st_attempt,
	date_delivered,
	date_failed_delivery,
	date_returned,
	date_procured_promised,
	date_ready_to_ship_promised,
	date_delivered_promised,
	date_stock_updated,
	date_declared_stockout,
	date_declared_backorder,
	date_assigned_to_stock,
	date_status_value_chain,
	date_cancelled,
    datetime_cancelled,
	datetime_ordered,
	datetime_exported,
	datetime_po_created,
	datetime_po_updated,
	datetime_po_updatetimed,
	datetime_po_issued,
	datetime_po_confirmed,
	#datetime_sent_tracking_code,
	datetime_procured,
	datetime_ready_to_pick,
	datetime_ready_to_ship,
	datetime_shipped,
	datetime_assigned_to_stock,
	week_procured_promised,
	week_delivered_promised,
	week_r2s_promised,
	week_shipped,
	month_procured_promised,
	month_delivered_promised,
	month_r2s_promised,
	on_time_to_po,
	on_time_to_confirm,
	on_time_to_send_po,
	on_time_to_procure,
	on_time_to_export,
	on_time_to_ready,
	on_time_to_ship,
	on_time_to_1st_attempt,
	on_time_to_deliver,
	on_time_to_request_recollection,
	on_time_to_stockout_declared,
	on_time_to_create_po,
	on_time_to_create_tracking_code,
	on_time_total_1st_attempt,
	on_time_total_delivery,
	is_presale,
	payment_method,
	wms_tracking_code,
	shipping_carrier_tracking_code,
	shipping_carrier_tracking_code_inverse,
	status_bob,
	status_wms,
	item_counter,
	package_measure_new,
	is_marketplace,
	is_returned,
	cat_1,
	cat_2,
	cat_3,
	category_bp,
    ship_to_met_area,
    ship_to_region,
    ship_to_city,
	loyalty,
	has_nps_score
) SELECT
	item_id,
	order_number,
	sku_config,
	sku_simple,
	sku_name,
	price_w_o_vat,
	cost_w_o_vat,
	supplier_id,
	supplier_name,
	buyer,
	head_buyer,
	package_height,
	package_length,
	package_width,
	package_weight,
	fulfillment_type_bob,
	fulfillment_type_real,
	shipping_carrier,
	date_ordered,
	date_exported,
	date_backorder,
	date_backorder_tratada,
	date_backorder_accepted,
	date_po_created,
	date_po_updated,
	date_po_issued,
	date_po_confirmed,
	#date_sent_tracking_code,
	date_procured,
	date_ready_to_pick,
	date_ready_to_ship,
	date_tracking_code_created,
	date_shipped,
	date_1st_attempt,
	date_delivered,
	date_failed_delivery,
	date_returned,
	date_procured_promised,
	date_ready_to_ship_promised,
	date_delivered_promised,
	date_stock_updated,
	date_declared_stockout,
	date_declared_backorder,
	date_assigned_to_stock,
	date_status_value_chain,
	date_cancelled,
    datetime_cancelled,
	datetime_ordered,
	datetime_exported,
	datetime_po_created,
	datetime_po_updated,
	datetime_po_updatetimed,
	datetime_po_issued,
	datetime_po_confirmed,
	#datetime_sent_tracking_code,
	datetime_procured,
	datetime_ready_to_pick,
	datetime_ready_to_ship,
	datetime_shipped,
	datetime_assigned_to_stock,
	week_procured_promised,
	week_delivered_promised,
	week_r2s_promised,
	week_shipped,
	month_procured_promised,
	month_delivered_promised,
	month_r2s_promised,
	on_time_to_po,
	on_time_to_confirm,
	on_time_to_send_po,
	on_time_to_procure,
	on_time_to_export,
	on_time_to_ready,
	on_time_to_ship,
	on_time_to_1st_attempt,
	on_time_to_deliver,
	on_time_to_request_recollection,
	on_time_to_stockout_declared,
	on_time_to_create_po,
	on_time_to_create_tracking_code,
	on_time_total_1st_attempt,
	on_time_total_delivery,
	is_presale,
	payment_method,
	wms_tracking_code,
	shipping_carrier_tracking_code,
	shipping_carrier_tracking_code_inverse,
	status_bob,
	status_wms,
	item_counter,
	package_measure_new,
	is_marketplace,
	is_returned,
	cat_1,
	cat_2,
	cat_3,
	category_bp,
    ship_to_met_area,
    ship_to_region,
    ship_to_city,
	loyalty,
	has_nps_score
FROM
	operations_co.out_order_tracking;

SELECT  'Update date_customer_request_il',now();
UPDATE operations_co.out_inverse_logistics_tracking_sample AS a
INNER JOIN bob_live_co.sales_order_item_status_history AS b
ON a.item_id=b.fk_sales_order_item
SET a.date_customer_request_il=b.created_at
WHERE b.fk_sales_order_item_status=132;

#se toma el segundo estado de aguardando retorno ya que el primero es el que hace el sistema automaticamente
#y el segundo es luego de aguardando agendamiento
#DROP TEMPORARY TABLE IF EXISTS operations_co.TMP ; 
#CREATE TEMPORARY TABLE operations_co.TMP 
#SELECT
#		b.return_id,
#		b. STATUS,
#		min(b.changed_at) as cambiado ,
#		c.item_id
#	FROM
#		wmsprod_co.inverselogistics_status_history b
#	JOIN wmsprod_co.inverselogistics_devolucion c ON b.return_id = c.id
#WHERE b.STATUS = 2
#Group by item_id;

#create index item_id on operations_co.TMP(item_id);

#UPDATE operations_co.out_inverse_logistics_tracking_sample a
#INNER JOIN operations_co.TMP
#ON a.item_id = TMP.item_id
#SET a.date_first_track_il = TMP.cambiado
#WHERE
#	TMP. STATUS = 2;

#se toma el segundo estado de aguardando retorno ya que el primero es el que hace el sistema automaticamente
#y el segundo es luego de aguardando agendamiento
UPDATE operations_co.out_inverse_logistics_tracking_sample as t1
INNER JOIN 
(SELECT a.item_id,c.changed_at FROM operations_co.out_inverse_logistics_tracking_sample as a
INNER JOIN wmsprod_co.inverselogistics_devolucion as b
ON a.item_id=b.item_id
INNER JOIN wmsprod_co.inverselogistics_status_history as c
ON b.id=c.return_id
LEFT JOIN
	(SELECT a.order_number,a.item_id,c.status,c.changed_at,c.id FROM operations_co.out_inverse_logistics_tracking_sample as a
	INNER JOIN wmsprod_co.inverselogistics_devolucion as b
	ON a.item_id=b.item_id
	INNER JOIN wmsprod_co.inverselogistics_status_history as c
	ON b.id=c.return_id
	WHERE c.status=2
	GROUP BY a.item_id
	ORDER by changed_at ASC) AS primer_aguardando_retorno
ON c.id=primer_aguardando_retorno.id
WHERE c.status=2
AND primer_aguardando_retorno.id IS NULL
GROUP BY item_id
ORDER by c.changed_at ASC) AS t2
ON t1.item_id=t2.item_id
SET t1.date_first_track_il=t2.changed_at;

DROP TEMPORARY TABLE IF EXISTS operations_co.TMP2 ; 
CREATE TEMPORARY TABLE operations_co.TMP2 
SELECT
		b.return_id,
		b. STATUS,
		max(b.changed_at) as cambiado ,
		c.item_id
	FROM
		wmsprod_co.inverselogistics_status_history b
	JOIN wmsprod_co.inverselogistics_devolucion c ON b.return_id = c.id
WHERE b.STATUS = 2
Group by item_id;

create index item_id on operations_co.TMP2(item_id);

UPDATE operations_co.out_inverse_logistics_tracking_sample a
INNER JOIN operations_co.TMP2
ON a.item_id = TMP2.item_id
SET a.date_last_track_il = TMP2.cambiado
WHERE
	TMP2. STATUS = 2;

CALL operations_co.calcworkdays;


SELECT  'Update date_quality_il',now();
UPDATE operations_co.out_inverse_logistics_tracking_sample AS a
INNER JOIN bob_live_co.sales_order_item_status_history AS b
ON a.item_id=b.fk_sales_order_item
SET a.date_quality_il=b.created_at
WHERE b.fk_sales_order_item_status=119;

SELECT  'Update date_cash_refunded_il',now();
UPDATE operations_co.out_inverse_logistics_tracking_sample AS a
INNER JOIN bob_live_co.sales_order_item_status_history AS b
ON a.item_id=b.fk_sales_order_item
SET a.date_cash_refunded_il=b.created_at
WHERE b.fk_sales_order_item_status=134;

SELECT  'Update date_voucher_refunded_il',now();
UPDATE operations_co.out_inverse_logistics_tracking_sample AS a
INNER JOIN bob_live_co.sales_order_item_status_history AS b
ON a.item_id=b.fk_sales_order_item
SET a.date_voucher_refunded_il=b.created_at
WHERE b.fk_sales_order_item_status=113;

SELECT  'Update date_closed BOB',now();
UPDATE operations_co.out_inverse_logistics_tracking_sample AS a
INNER JOIN bob_live_co.sales_order_item_status_history AS b
ON a.item_id=b.fk_sales_order_item
SET a.date_closed_il=b.created_at
WHERE b.fk_sales_order_item_status=6;

UPDATE operations_co.out_inverse_logistics_tracking_sample AS a
SET a.date_refunded_il= IF(a.date_cash_refunded_il IS NULL,a.date_voucher_refunded_il,a.date_cash_refunded_il);

DROP TEMPORARY TABLE IF EXISTS operations_co.TMP3analyst ; 
CREATE TEMPORARY TABLE operations_co.TMP3analyst
SELECT d.*, e.nome
FROM
(SELECT
		b.user_id,
		b.return_id,
		b. STATUS,
		b.changed_at,
		c.item_id
	FROM
		wmsprod_co.inverselogistics_status_history b
	JOIN wmsprod_co.inverselogistics_devolucion c ON b.return_id = c.id) d 
INNER JOIN wmsprod_co.usuarios as e ON d.user_id = e.usuarios_id
WHERE
	d. STATUS = 6;

create index item on operations_co.TMP3analyst(item_id);

UPDATE operations_co.out_inverse_logistics_tracking_sample a
INNER JOIN operations_co.TMP3analyst as d ON a.item_id = d.item_id
SET a.analyst_il = d.nome
#a.date_inbound_il  = d.changed_at
WHERE
	d. STATUS = 6;

SELECT  'update fecha recepcion en bodega devolucion',now();
UPDATE operations_co.out_inverse_logistics_tracking_sample as a
INNER JOIN operations_co.tbl_bi_ops_ingreso_bodega_devolucion as b
ON a.item_id=b.item_id
SET 
a.date_inbound_il=b.date_ingreso_bodega_devolucion;

DROP TEMPORARY TABLE IF EXISTS operations_co.guia ; 
CREATE TEMPORARY TABLE operations_co.guia 
SELECT numero_guia,DATE(max(fecha_entrega)) AS fecha_entrega
FROM outbound_co.tbl_outbound_daily_servientrega
WHERE nombre_estadoenvio='ENTREGADO'
AND fecha_entrega <> '1900-01-00 00:00:00'
GROUP BY numero_guia;

create index num on operations_co.guia(numero_guia);

DROP TEMPORARY TABLE IF EXISTS operations_co.devo ; 
CREATE TEMPORARY TABLE operations_co.devo 
SELECT item_id,carrier_tracking_code
FROM wmsprod_co.inverselogistics_devolucion;

create index num on operations_co.devo(carrier_tracking_code);

DROP TEMPORARY TABLE IF EXISTS operations_co.hay ; 
CREATE TEMPORARY TABLE operations_co.hay 
Select * from operations_co.devo AS a
LEFT JOIN
operations_co.guia  as b
ON a.carrier_tracking_code=b.numero_guia;

create index up on operations_co.hay(item_id);

SELECT  'update fecha recepcion en bodega devolucion desde archivos servientrega',now();
UPDATE operations_co.out_inverse_logistics_tracking_sample AS t1
INNER JOIN
operations_co.hay AS t2
ON t1.item_id=t2.item_id
SET t1.date_inbound_il = t2.fecha_entrega
WHERE t1.date_inbound_il IS NULL;

SELECT  'Correccion errores',now();
UPDATE operations_co.out_inverse_logistics_tracking_sample a
SET a.date_inbound_il  = a.date_quality_il
WHERE
	a.date_inbound_il > a.date_quality_il;

############## CALCULO DE DIAS  ##############

CALL operations_co.calcworkdays;

UPDATE operations_co.out_inverse_logistics_tracking_sample
INNER JOIN operations_co.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_inbound_il) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking_sample.date_exit_il) = calcworkdays.date_last
SET 
days_to_process_return = 
	IF(date_exit_il IS NULL,
		DATEDIFF(date(CURDATE()),date(date_inbound_il)),
		DATEDIFF(date(date_exit_il),date(date_inbound_il))),
time_to_process_return =
If( date_inbound_il is Null,
TIMESTAMPDIFF( SECOND ,date_inbound_il, CURDATE())/3600,
TIMESTAMPDIFF( SECOND ,date_inbound_il, date_exit_il)/3600),
workdays_to_process_return = 
	IF(date_exit_il IS NULL,
		NULL,calcworkdays.workdays
		)
WHERE out_inverse_logistics_tracking_sample.is_returned=1
AND out_inverse_logistics_tracking_sample.date_inbound_il IS NOT NULL;


UPDATE operations_co.out_inverse_logistics_tracking_sample
INNER JOIN operations_co.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_inbound_il) = calcworkdays.date_first
		AND date(CURDATE()) = calcworkdays.date_last
SET 
workdays_to_process_return = calcworkdays.workdays
WHERE out_inverse_logistics_tracking_sample.is_returned=1
AND out_inverse_logistics_tracking_sample.date_inbound_il IS NOT NULL
AND out_inverse_logistics_tracking_sample.date_exit_il IS NULL;



UPDATE operations_co.out_inverse_logistics_tracking_sample
INNER JOIN operations_co.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_failed_delivery) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking_sample.date_cancelled) = calcworkdays.date_last
SET 
days_to_process_fail_delivery = 
	IF(date_failed_delivery < date_cancelled ,
		DATEDIFF(date(date_cancelled),date(date_failed_delivery)),
   IF( date_failed_delivery < date_ready_to_ship ,
		DATEDIFF(date(date_ready_to_ship),date(date_failed_delivery)),DATEDIFF(date(CURDATE()),date(date_failed_delivery)))),
time_to_process_fail_delivery =
IF(date_failed_delivery < date_cancelled ,
		TIMESTAMPDIFF( SECOND ,date_failed_delivery, datetime_cancelled)/3600,
   IF( date_failed_delivery < date_ready_to_ship ,
		TIMESTAMPDIFF( SECOND ,date_failed_delivery, datetime_ready_to_ship)/3600,TIMESTAMPDIFF( SECOND ,date_failed_delivery, CURDATE())/3600)),
workdays_to_process_fail_delivery = 
	IF(date_failed_delivery < date_cancelled , calcworkdays.workdays , NULL)
WHERE out_inverse_logistics_tracking_sample.is_fail_delivery=1
AND out_inverse_logistics_tracking_sample.date_failed_delivery IS NOT NULL;


UPDATE operations_co.out_inverse_logistics_tracking_sample
INNER JOIN operations_co.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_failed_delivery) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking_sample.date_ready_to_ship) = calcworkdays.date_last
SET 
workdays_to_process_fail_delivery = 
	IF(date_failed_delivery < date_ready_to_ship , calcworkdays.workdays , NULL)
WHERE out_inverse_logistics_tracking_sample.is_fail_delivery=1
AND out_inverse_logistics_tracking_sample.date_failed_delivery IS NOT NULL;


UPDATE operations_co.out_inverse_logistics_tracking_sample
INNER JOIN operations_co.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_failed_delivery) = calcworkdays.date_first
		AND date(CURDATE()) = calcworkdays.date_last
SET 
workdays_to_process_fail_delivery = calcworkdays.workdays 
WHERE out_inverse_logistics_tracking_sample.is_fail_delivery=1
AND (date_failed_delivery > date_ready_to_ship OR date_failed_delivery > datetime_cancelled )
AND out_inverse_logistics_tracking_sample.date_failed_delivery IS NOT NULL;

UPDATE operations_co.out_inverse_logistics_tracking_sample
INNER JOIN operations_co.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_last_track_il) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking_sample.date_inbound_il) = calcworkdays.date_last
SET 
days_to_inbound_il = 
	IF(date_last_track_il IS NULL,
		NULL,
		DATEDIFF(date(date_inbound_il),date(date_last_track_il))),
workdays_to_inbound_il = 
	IF(date_last_track_il IS NULL,
		NULL,calcworkdays.workdays
		)
WHERE out_inverse_logistics_tracking_sample.is_returned=1
AND out_inverse_logistics_tracking_sample.date_inbound_il IS NOT NULL;

UPDATE operations_co.out_inverse_logistics_tracking_sample
INNER JOIN operations_co.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_inbound_il) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking_sample.date_quality_il)= calcworkdays.date_last
SET 
days_to_quality_il = 
	IF(date_inbound_il IS NULL,
		NULL,
		DATEDIFF(date(date_quality_il),date(date_inbound_il))),
workdays_to_quality_il = 
	IF((date_inbound_il IS NULL),
		NULL,calcworkdays.workdays)
WHERE operations_co.out_inverse_logistics_tracking_sample.is_returned =1
AND date_quality_il is not null;

UPDATE operations_co.out_inverse_logistics_tracking_sample AS a
INNER JOIN operations_co.calcworkdays
	ON date(date_delivered) = calcworkdays.date_first
		AND date(date_customer_request_il) = calcworkdays.date_last
SET 
days_to_customer_request_il = 
IF(date_delivered IS NULL,
		NULL,
	DATEDIFF(date(date_customer_request_il),date(date_delivered))),
workdays_to_customer_request_il  = 
IF(date_customer_request_il IS NULL,
		NULL,calcworkdays.workdays)
WHERE is_returned = 1
AND date_customer_request_il IS NOT NULL;

UPDATE operations_co.out_inverse_logistics_tracking_sample
INNER JOIN operations_co.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_customer_request_il) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking_sample.date_first_track_il) = calcworkdays.date_last
SET 
days_to_track_il = 
	IF(date_customer_request_il IS NULL,
		NULL,
		DATEDIFF(date(date_first_track_il),date(date_customer_request_il))),
workdays_to_track_il = 
	IF(date_customer_request_il IS NULL,
		NULL,calcworkdays.workdays
		)
WHERE is_returned =1
AND date_first_track_il IS NOT NULL;

UPDATE operations_co.out_inverse_logistics_tracking_sample
INNER JOIN operations_co.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_quality_il) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking_sample.date_refunded_il)= calcworkdays.date_last
SET 
days_to_refunded_il = 
	IF((date_quality_il IS NULL),
		NULL,
		DATEDIFF(date(date_refunded_il),date(date_quality_il))),
workdays_to_refunded_il = 
	IF((date_quality_il IS NULL),
		NULL,calcworkdays.workdays)
WHERE is_returned =1
AND date_refunded_il IS NOT NULL;

UPDATE operations_co.out_inverse_logistics_tracking_sample
INNER JOIN operations_co.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_customer_request_il) = calcworkdays.date_first
AND 
IF(date(out_inverse_logistics_tracking_sample.date_refunded_il) IS NULL,CURDATE(),date(out_inverse_logistics_tracking_sample.date_refunded_il)) = calcworkdays.date_last
SET 
workdays_total_refunded_il = 
	IF((date_customer_request_il IS NULL),
		NULL,calcworkdays.workdays)
WHERE is_returned =1
AND date_quality_il IS NOT NULL;


UPDATE operations_co.out_inverse_logistics_tracking_sample 
INNER JOIN operations_co.calcworkdays
	ON date(out_inverse_logistics_tracking_sample.date_customer_request_il)= calcworkdays.date_first
		AND date(out_inverse_logistics_tracking_sample.date_closed_il)= calcworkdays.date_last
SET 
days_to_closed_il = 
	IF(date_customer_request_il IS NULL,
		NULL,
		DATEDIFF(date(date_closed_il),date(date_customer_request_il))),
workdays_to_closed_il = 
	IF(date_customer_request_il IS NULL,
		NULL,calcworkdays.workdays)
WHERE is_returned =1
AND date_closed_il IS NOT NULL;



DROP TEMPORARY TABLE IF EXISTS operations_co.TMP2 ; 
CREATE TEMPORARY TABLE operations_co.TMP2 
SELECT c.*, d.description 
FROM wmsprod_co.inverselogistics_devolucion as c INNER JOIN wmsprod_co.inverselogistics_devolucion_razon as d on c.reason_id = d.reason_id;

create index item_id on operations_co.TMP2(item_id);

UPDATE operations_co.out_inverse_logistics_tracking_sample as a
INNER JOIN operations_co.TMP2 b ON a.item_id = b.item_id
SET a.status_wms_il = b.status,
a.id_ilwh = b.id,
a.reason_for_entrance_il = b.description,
date_canceled_il = b.created_at;

SELECT  'Drop tmp_pro_min_date_exported',now();
UPDATE operations_co.out_inverse_logistics_tracking_sample AS a
INNER JOIN wmsprod_co.inverselogistics_catalog_status AS b
ON a.status_wms_il=b.id_status
SET a.status_wms_il=b.status_inverselogistic;

UPDATE operations_co.out_inverse_logistics_tracking_sample as a
INNER JOIN wmsprod_co.tms_status_delivery b ON a.wms_tracking_code = b.cod_rastreamento
SET a.il_type = 'Fail delivery',
a.is_fail_delivery = 1, a.date_inbound_il = b.date WHERE b.id_status = 10;

UPDATE operations_co.out_inverse_logistics_tracking_sample a
INNER JOIN wmsprod_co.ws_log_lista_embarque b ON b.cod_rastreamento = a.wms_tracking_code
SET a.reason_for_entrance_il = b.observaciones
WHERE
 a.is_fail_delivery = 1;

UPDATE operations_co.out_inverse_logistics_tracking_sample as a
SET a.il_type = 'Return'
WHERE a.is_returned = 1;

DROP TEMPORARY TABLE IF EXISTS operations_co.TMP3 ; 
CREATE TEMPORARY TABLE operations_co.TMP3 
SELECT c.*, d.description 
FROM wmsprod_co.inverselogistics_devolucion as c INNER JOIN wmsprod_co.inverselogistics_devolucion_accion as d on c.action_id = d.action_id;

create index item_id on operations_co.TMP3(item_id);

UPDATE operations_co.out_inverse_logistics_tracking_sample as a
INNER JOIN operations_co.TMP3 b ON a.item_id = b.item_id
SET a.action_il = b.description
;

UPDATE operations_co.out_inverse_logistics_tracking_sample  as  a
INNER JOIN wmsprod_co.inverselogistics_devolucion as b ON a.item_id = b.item_id
SET a.date_exit_il = CASE
WHEN a.status_wms_il = 'retornar_stock'
OR a.status_wms_il= 'enviar_cuarentena'
OR a.status_wms_il= 'reenvio_cliente' THEN
	modified_at
ELSE
	NULL
END;

UPDATE operations_co.out_inverse_logistics_tracking_sample a
SET a.return_accepted = CASE
WHEN a.status_wms_il = 'retornar_stock'
OR a.status_wms_il = 'enviar_cuarentena' THEN
	1
ELSE
	NULL
END;

UPDATE operations_co.out_inverse_logistics_tracking_sample as a 
SET a.is_exits_il = 1 
WHERE date_exit_il is not null;

UPDATE operations_co.out_inverse_logistics_tracking_sample as a 
SET a.is_pending_return_wh_il = 1 
WHERE status_wms_il in ('aguardando_agendamiento','aguardando_retorno');

DROP TEMPORARY TABLE IF EXISTS operations_co.TMP2analyst ; 
CREATE TEMPORARY TABLE operations_co.TMP2analyst
SELECT
		b.cod_rastreamento,
		b.id_status,
		b.id_user,
		c.nome
	FROM
		wmsprod_co.tms_status_delivery b
	JOIN wmsprod_co.usuarios c ON b.id_user = c.usuarios_id 
WHERE
	b.id_status = 10 ;

create index cod on operations_co.TMP2analyst(cod_rastreamento);

UPDATE operations_co.out_inverse_logistics_tracking_sample a
INNER JOIN operations_co.TMP2analyst as  b ON b.cod_rastreamento = a.wms_tracking_code
SET a.analyst_il = b.nome
WHERE
	a.il_type = 'Fail delivery';

UPDATE operations_co.out_inverse_logistics_tracking_sample
SET out_inverse_logistics_tracking_sample.delivered_last_30_il = CASE
WHEN datediff(curdate(), date_delivered) <= 45
AND datediff(curdate(), date_delivered) >= 15 THEN
	1
ELSE
	0
END;

UPDATE operations_co.out_inverse_logistics_tracking_sample AS a
INNER JOIN operations_co.calcworkdays AS b
	ON DATE(a.date_customer_request_il)= b.date_first
		AND DATE(curdate())= b.date_last
SET 
backlog_track_il = IF(b.workdays >2,1,0),
workdays_backlog_track_il = IF(b.workdays >2,(b.workdays-2),0)
WHERE date_first_track_il IS NULL
AND date_inbound_il IS NULL
AND date_quality_il IS NULL
AND date_closed_il IS null
AND date_customer_request_il IS NOT NULL;

UPDATE operations_co.out_inverse_logistics_tracking_sample AS a
INNER JOIN operations_co.calcworkdays AS b
	ON DATE(a.date_inbound_il)= b.date_first
		AND DATE(curdate())= b.date_last
SET 
backlog_quality_il = IF(b.workdays >3,1,0),
workdays_backlog_quality_il = IF(b.workdays >3,(b.workdays-3),0)
WHERE date_quality_il IS NULL
AND date_closed_il IS NULL
AND is_returned=1;

UPDATE operations_co.out_inverse_logistics_tracking_sample AS t1
INNER JOIN
(SELECT numero_guia,max(fecha_envio) as fecha_envio
FROM outbound_co.tbl_outbound_daily_servientrega
GROUP BY numero_guia) as t2
ON t1.shipping_carrier_tracking_code_inverse = t2.numero_guia
SET t1.return_receivedserv = 1,
t1.date_inbound_carrier_il = date(t2.fecha_envio);

UPDATE operations_co.out_inverse_logistics_tracking_sample AS a
INNER JOIN operations_co.calcworkdays AS b
	ON date(a.date_inbound_carrier_il)= b.date_first
		AND date(curdate())= b.date_last
SET 
backlog_currier_il = IF(((return_receivedserv = 1) AND (date_inbound_il IS NULL)),1,0),
workdays_backlog_currier_il = IF(((return_receivedserv = 1) AND (date_inbound_il IS NULL)),b.workdays,0)
WHERE is_returned=1
AND date_quality_il IS NULL
AND date_closed_il IS NULL;

UPDATE operations_co.out_inverse_logistics_tracking_sample AS a
INNER JOIN operations_co.calcworkdays AS b
	ON date(a.date_quality_il)= b.date_first
		AND date(curdate())= b.date_last
SET 
backlog_refund_consignment_il = IF(b.workdays>5,1,0),
workdays_backlog_refund_consignment_il = IF(b.workdays>5,(b.workdays-5),0)
WHERE date_cash_refunded_il IS NULL
AND date_voucher_refunded_il IS NULL
AND date_closed_il IS NULL
AND date_quality_il IS NOT NULL
AND is_returned =1
AND action_il='Consignacion bancaria (7 dias h�biles)';

UPDATE operations_co.out_inverse_logistics_tracking_sample AS a
INNER JOIN operations_co.calcworkdays AS b
	ON date(a.date_quality_il)= b.date_first
		AND date(curdate())= b.date_last
SET 
backlog_refund_effecty_il = IF(b.workdays>4,1,0),
workdays_backlog_refund_effecty_il = IF(b.workdays>4,(b.workdays-4),0)
WHERE date_cash_refunded_il IS NULL
AND date_voucher_refunded_il IS NULL
AND date_closed_il IS NULL
AND date_quality_il IS NOT NULL
AND is_returned =1
AND action_il='Efecty (7 dias h�biles)';

UPDATE operations_co.out_inverse_logistics_tracking_sample AS a
INNER JOIN operations_co.calcworkdays AS b
	ON date(a.date_quality_il)= b.date_first
		AND date(curdate())= b.date_last
SET 
backlog_refund_reversion_il = IF(b.workdays>20,1,0),
workdays_backlog_refund_reversion_il = IF(b.workdays>20,(b.workdays-20),0)
WHERE date_cash_refunded_il IS NULL
AND date_voucher_refunded_il IS NULL
AND date_closed_il IS NULL
AND date_quality_il IS NOT NULL
AND is_returned = 1
AND action_il='Reversi�n TC (25-30 dias h�biles)';

UPDATE operations_co.out_inverse_logistics_tracking_sample AS a
INNER JOIN operations_co.calcworkdays AS b
	ON date(a.date_quality_il)= b.date_first
		AND date(curdate())= b.date_last
SET 
backlog_refund_voucher_il = IF(b.workdays>2,1,0),
workdays_backlog_refund_voucher_il = IF(b.workdays>2,(b.workdays-2),0)
WHERE date_cash_refunded_il IS NULL
AND date_voucher_refunded_il IS NULL
AND date_closed_il IS NULL
AND date_quality_il IS NOT NULL
AND is_returned =1
AND action_il='Voucher (5 d�as h�biles)';

UPDATE operations_co.out_inverse_logistics_tracking_sample AS a
INNER JOIN operations_co.calcworkdays AS b
	ON a.date_customer_request_il= b.date_first
	AND a.date_first_track_il= b.date_last
SET on_time_track_il = 1
WHERE b.isweekday = 1 
AND b.isholiday = 0
AND is_returned =1
AND b.workdays<=2;

UPDATE operations_co.out_inverse_logistics_tracking_sample AS a
INNER JOIN operations_co.calcworkdays AS b
	ON a.date_inbound_carrier_il= b.date_first
	AND a.date_inbound_il= b.date_last
SET on_time_currier_il = 1
WHERE b.isweekday = 1 
AND b.isholiday = 0
AND is_returned =1
AND b.workdays<=1;

UPDATE operations_co.out_inverse_logistics_tracking_sample AS a
INNER JOIN operations_co.calcworkdays AS b
	ON a.date_inbound_il= b.date_first
	AND a.date_quality_il= b.date_last
SET on_time_quality_il = 1
WHERE b.isweekday = 1 
AND b.isholiday = 0
AND is_returned =1
AND b.workdays<=3;

UPDATE operations_co.out_inverse_logistics_tracking_sample AS a
INNER JOIN operations_co.calcworkdays AS b
	ON a.date_quality_il= b.date_first
	AND a.date_cash_refunded_il= b.date_last
SET on_time_refund_consignment_il = 1
WHERE b.isweekday = 1 
AND b.isholiday = 0
AND action_il='Consignacion bancaria (7 dias h�biles)'
AND is_returned =1
AND b.workdays<=5;

UPDATE operations_co.out_inverse_logistics_tracking_sample AS a
INNER JOIN operations_co.calcworkdays AS b
	ON a.date_quality_il= b.date_first
	AND a.date_cash_refunded_il= b.date_last
SET on_time_refund_effecty_il = 1
WHERE b.isweekday = 1 
AND b.isholiday = 0
AND action_il='Efecty (7 dias h�biles)'
AND is_returned =1
AND b.workdays<=4;

UPDATE operations_co.out_inverse_logistics_tracking_sample AS a
INNER JOIN operations_co.calcworkdays AS b
	ON a.date_quality_il= b.date_first
	AND a.date_cash_refunded_il= b.date_last
SET on_time_refund_reversion_il = 1
WHERE b.isweekday = 1 
AND b.isholiday = 0
AND action_il='Reversi�n TC (25-30 dias h�biles)'
AND is_returned =1
AND b.workdays<=20;

UPDATE operations_co.out_inverse_logistics_tracking_sample AS a
INNER JOIN operations_co.calcworkdays AS b
	ON a.date_quality_il= b.date_first
	AND a.date_voucher_refunded_il= b.date_last
SET on_time_refund_voucher_il = 1
WHERE b.isweekday = 1 
AND b.isholiday = 0
AND action_il='Voucher (5 d�as h�biles)'
AND is_returned =1
AND b.workdays<=2;

UPDATE operations_co.out_inverse_logistics_tracking_sample a
INNER JOIN operations_co.calcworkdays b
 ON a.date_inbound_il = b.date_first
 AND 2 = b.workdays
SET date_processed_return_promised = b.date_last
WHERE is_returned = 1;