DROP TABLE IF EXISTS vw_itens_venda_entrega ;
CREATE TABLE vw_itens_venda_entrega  ( KEY ( entrega_id ) ) 
SELECT * FROM wmsprod_co.vw_itens_venda_entrega  ;
DROP TABLE IF EXISTS A_BI_Taxed_pay;
CREATE TABLE  A_BI_Taxed_pay ( KEY( itens_venda_id ), KEY( numero_order  ), KEY( item_id ) ) 
SELECT 
   a.delivery_invoice_id,
   a.data_criacao as data_criacao_invoice,
   a.status,
   a.num_factura,
   b.item_id,
   b.numero_order,
   b.sku,
   b.nome_produto,
   b.preco_unitario,
   b.desconto_voucher,
   b.shipping_amount,
   b.tax_percent,
   0000000000000.00 AS valor_total_com_desconto,
   "0000-00-00"     AS data_criacao,
   SPACE(250) AS metodo_de_pagamento,
   SPACE(250) AS nome_cliente,
   SPACE(250) AS cpf_cliente,
   SPACE(250) AS coupon_code,
   SPACE(250) AS shipping_carrier,
   SPACE(250) AS shipping_carrier_tracking_code,
   SPACE(250) AS status_bob,
   SPACE(250) AS status_wms,
   0000000000000.00 AS Price,
   0000000000000.00 AS PriceAfterTax,
   00000000.0000000 AS TaxPercent,
   b.itens_venda_id

FROM 
              wmsprod_co.delivery_invoice a
   INNER JOIN vw_itens_venda_entrega b
           ON a.entrega_id = b.entrega_id
;
/*WITH C*/
UPDATE            A_BI_Taxed_pay AS b
       INNER JOIN wmsprod_co.itens_venda c
               ON b.itens_venda_id = c.itens_venda_id
SET
   b.data_criacao = c.data_criacao
;

/*with d*/
UPDATE            A_BI_Taxed_pay AS c
       INNER JOIN wmsprod_co.pedidos_venda d
               ON c.numero_order = d.numero_pedido
SET
   c.valor_total_com_desconto = d.valor_total_com_desconto,
   c.metodo_de_pagamento      = d.metodo_de_pagamento,
   c.nome_cliente             = d.nome_cliente,
   c.cpf_cliente              = d.cpf_cliente,
   c.coupon_code              = d.coupon_code
;
/*With G*/
UPDATE            A_BI_Taxed_pay AS c
       INNER JOIN development_co_project.A_Master g
               ON c.item_id=g.ItemID
SET
c.Price         = g.Price,
c.PriceAfterTax = g.PriceAfterTax,
c.TaxPercent    = g.TaxPercent
;

UPDATE            A_BI_Taxed_pay AS c
       INNER JOIN operations_co.out_order_tracking f
               ON c.numero_order = f.order_number
SET
   c.shipping_carrier               = f.shipping_carrier,
   c.shipping_carrier_tracking_code = f.shipping_carrier_tracking_code,
   c.status_bob                     = f.status_bob,
   c.status_wms                     =f.status_wms
;


INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  UPDATEd_at, 
  key_date)
SELECT 
  'Colombia_Project', 
  'ExtraProcess.Inventory_PerDay',
  'finish',
  NOW(),
  NOW();

