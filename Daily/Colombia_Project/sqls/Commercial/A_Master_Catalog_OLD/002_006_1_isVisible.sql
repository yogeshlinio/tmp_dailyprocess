UPDATE A_Master_Catalog_Sample 
SET 
   isVisible = 1
WHERE
NOT(
			   isnull( Stock ) OR  Stock <= 0
      OR `pet_approved` = 0 
			OR `pet_status` <> 'creation,edited,images'
			OR `status_config` IN ( 'deleted','inactive')
			OR `status_simple` IN ( 'deleted','inactive')
      OR `display_if_out_of_stock` = 1
    )
;
