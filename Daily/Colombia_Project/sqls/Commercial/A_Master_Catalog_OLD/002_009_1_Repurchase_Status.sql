UPDATE            A_Master_Catalog_Sample
       INNER JOIN bob_live_co.catalog_simple
            USING ( id_catalog_simple )
       INNER JOIN bob_live_co.catalog_attribute_option_global_repurchase
               ON fk_catalog_attribute_option_global_repurchase = id_catalog_attribute_option_global_repurchase
SET
		repurchase_status = catalog_attribute_option_global_repurchase.name
;