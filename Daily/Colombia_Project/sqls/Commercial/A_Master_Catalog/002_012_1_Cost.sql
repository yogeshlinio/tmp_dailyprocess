/*
*
*/
DROP TEMPORARY TABLE IF EXISTS sku_simple_in_stock;
CREATE TEMPORARY TABLE sku_simple_in_stock (INDEX (sku_simple))
SELECT
b.sku AS sku_simple,
sum(c.participa_estoque) AS ItemsInStock
FROM wmsprod_co.estoque a
INNER JOIN wmsprod_co.traducciones_producto b
        ON a.cod_barras = b.identificador
LEFT JOIN wmsprod_co.posicoes c
        ON a.endereco = c.posicao
GROUP BY b.identificador
;
 
DROP TABLE IF EXISTS TMP_CostPerSKU;
CREATE TABLE TMP_CostPerSKU
(
   PRIMARY KEY( sku_simple, cost_oms, cost_oms_after_tax) 
)
SELECT
   c.sku AS sku_simple,
   /*
   * Change request by Laura Forero
   * duing cost without tax in OMS
   */
   a.unit_price + a.tax AS Cost_oms,
   a.tax, b.currency_type, date_format( date(b.created_at), '%Y%m') AS monthNum,
   a.price_before_tax AS cost_oms_after_tax,
   concat(b.venture_code, lpad(b.id_procurement_order, 7, 0),b.check_digit) AS purchase_order,
   count(*) AS Unidades,
   00000000000000000000000.00  AS costo_total,
   00000000000000000000000.00  AS costo_total_after_tax
FROM 
           procurement_live_co.procurement_order_item a
INNER JOIN procurement_live_co.procurement_order b
                ON a.fk_procurement_order = b.id_procurement_order
INNER JOIN bob_live_co.catalog_simple c
                ON a.fk_catalog_simple = c.id_catalog_simple
INNER JOIN sku_simple_in_stock d
                ON c.sku = d.sku_simple
WHERE a.is_deleted = 0
AND                      b.is_cancelled = 0
AND                      d.ItemsInStock > 0
GROUP BY  a.fk_catalog_simple, a.unit_price
;


/* -- */

UPDATE TMP_CostPerSKU AS cp
INNER JOIN (SELECT Month_Num,XR FROM development_mx.A_E_BI_ExchangeRate_USD WHERE Country = 'COL') er 
ON cp.monthNum = er.Month_Num
SET
	cp.cost_oms = cp.cost_oms * er.XR,
	cp.cost_oms_after_tax = cp.cost_oms_after_tax * er.XR
WHERE
	cp.currency_type = 'dolar'
;

/*
*
*/
update TMP_CostPerSKU 
set 
   cost_oms           = cost_oms * 1000,
   cost_oms_after_tax = cost_oms_after_tax * 1000
   
where sku_simple in 
(
'VA287FA35XIELACOL-116259','VA287FA35XIELACOL-116260',
'VA287FA35XIELACOL-116261','VA287FA35XIELACOL-116262',
'VA287FA35XIELACOL-116264','VA287FA36XIDLACOL-116251',
'VA287FA36XIDLACOL-116254','VA287FA36XIDLACOL-116258',
'VA287FA37XICLACOL-116243','VA287FA37XICLACOL-116244',
'VA287FA37XICLACOL-116245','VA287FA37XICLACOL-116246',
'VA287FA37XICLACOL-116247','VA287FA37XICLACOL-116248',
'VA287FA37XICLACOL-116249','VA287FA37XICLACOL-116250',
'VA287FA38XIBLACOL-116238','VA287FA38XIBLACOL-116239');

UPDATE TMP_CostPerSKU 
SET
   costo_total           = cost_oms * unidades,
   costo_total_after_tax = cost_oms_after_tax * unidades
;

   

/*
* Cost_OMS in Catalog
*/
UPDATE         A_Master_Catalog_Sample
   INNER JOIN(
              SELECT sku_simple, costo_total/unidades_totales as costo_ponderado , costo_total_after_tax / unidades_totales as costo_ponderado_after_tax
			    FROM (
                        SELECT sku_simple, sum(costo_total) costo_total, sum(costo_total_after_tax ) costo_total_after_tax , sum(unidades) unidades_totales 
                        FROM TMP_CostPerSKU group by sku_simple desc
				) 
   			  t) p 
ON A_Master_Catalog_Sample.Sku_Simple= p.sku_simple 
SET 
   A_Master_Catalog_Sample.Cost_oms = p.costo_ponderado     
WHERE
   p.costo_ponderado > 0 
;   