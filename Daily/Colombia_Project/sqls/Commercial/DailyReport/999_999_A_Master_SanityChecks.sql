INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'DailyReport',
  "finish",
  NOW(),
  MAX(date),
  count(*),
  count(*)
FROM
development_co.A_Master
;


