INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia_Project', 
  'A_Master_Catalog',
  "finish",
  NOW(),
  NOW(),
  count(*),
  count(*)
FROM
A_Master_Catalog
;


