UPDATE       A_Master_Catalog_Sample
  INNER JOIN bob_live_co.`catalog_tax_class`
       USING ( id_catalog_tax_class )
SET
   A_Master_Catalog_Sample.`tax_percent` = `catalog_tax_class`.tax_percent,
   A_Master_Catalog_Sample.`tax_class`   =  catalog_tax_class.name
;
/*
UPDATE A_Master_Catalog_Sample t
LEFT JOIN bob_live_co.catalog_tax_class 
ON catalog_tax_class.id_catalog_tax_class = t.id_catalog_tax_class
SET t.tax_class=catalog_tax_class.name;
*/