/*
* Update couriers on A_Master
*/
UPDATE        A_Master_Sample
   INNER JOIN wmsprod_co.itens_venda
           ON A_Master_Sample.itemID = itens_venda.item_id
   INNER JOIN wmsprod_co.romaneio
           ON itens_venda.romaneio_id = romaneio.romaneio_id
   INNER JOIN wmsprod_co.transportadoras
           ON romaneio.transportadora_id = transportadoras.transportadoras_id
 SET  
    A_Master_Sample.fk_courier = transportadoras_id,
	  A_Master_Sample.courier    = nome_transportadora
 ;

/*
* Update shipment_zone_mapping on costos ponderados
*/ 
update            M_Shipping_Costs_Ponderados t 
       inner join bob_live_co.shipment_zone_mapping t2 
               on     t.ciudad = t2.area_2 
                  and t.departamento=t2.area_1
set 
   t.fk_shipment_zone_mapping = t2.id_shipment_zone_mapping ;

/*
* Shipping Cost general
*/
update            A_Master_Sample t 
       inner join tbl_payment_method_groups pm 
               on t.PaymentMethod = pm.payment_method
       inner join M_Shipping_Costs_Ponderados s 
               on     s.payment_method_group = pm.payment_method_group
		              and t.fk_shipment_zone_mapping = s.fk_shipment_zone_mapping 
                  and PackageWeight >= s.peso_min 
                  and PackageWeight <= s.peso_max
       inner join operations_co.out_order_tracking on itemid = item_id
set 
   ShippingCost =  CASE WHEN t.fk_courier=1 THEN IFNULL(IF(s.deprisa=0,s.shipping_cost_ponderado,s.deprisa),s.shipping_cost_ponderado)
                               WHEN t.fk_courier=3 THEN IFNULL(IF(s.servientrega=0,s.shipping_cost_ponderado,s.servientrega),s.shipping_cost_ponderado)
                               WHEN t.fk_courier=2 THEN IFNULL(IF(s.thomas=0,s.shipping_cost_ponderado,s.thomas),s.shipping_cost_ponderado)
							   ELSE s.shipping_cost_ponderado
                          END
where  
       NetItemsInOrder = 1 
   and MonthNum >= 201401 
   and t.isMPlace=0;

/*
* Temp. table for update shipping costs where fulfilment is ownwarehouse
*/
DROP TEMPORARY TABLE IF EXISTS OwnWareHouse;
CREATE TEMPORARY TABLE OwnWareHouse ( INDEX ( PaymentMethod ), INDEX( shipping_carrier_tracking_code ) )    
SELECT 
           t.idSalesOrder, 
		   fulfillment_type_real,
		   t.PaymentMethod, 
		   t.fk_courier,
		   shipping_carrier_tracking_code,
		   NetItemsInOrder, 
           sum(t.PackageWeight) peso_total
     from            A_Master_Sample t 
	      INNER JOIN operations_co.out_order_tracking 
		          ON itemid = item_id 
     where 
	           MonthNum >= 201401 
		   AND OrderAfterCan = 1 
		   AND fulfillment_type_real in ( 'Own Warehouse','Consignment')
           and fk_courier IS NOT NULL
           and t.isMPlace=0
     group by 
	       t.idSalesOrder, 
		   fulfillment_type_real,
		   t.fk_courier, 
		   t.PaymentMethod, 
		   shipping_carrier_tracking_code
;		   

/*
* Shipping Cost where fulfillment is ownwarehouse
*/
UPDATE
               OwnWareHouse t 
    INNER JOIN tbl_payment_method_groups pm 
            on t.PaymentMethod = pm.payment_method
    INNER JOIN M_Shipping_Costs_Ponderados s
            ON s.payment_method_group = pm.payment_method_group
    INNER JOIN operations_co.out_order_tracking  
            on t.shipping_carrier_tracking_code=out_order_tracking.shipping_carrier_tracking_code
    INNER JOIN A_Master_Sample 
            on     A_Master_Sample.itemid = out_order_tracking.item_id
               and A_Master_Sample.fk_shipment_zone_mapping = s.fk_shipment_zone_mapping
set 
   A_Master_Sample.ShippingCost = CASE WHEN t.fk_courier=1 THEN IFNULL(IF(s.deprisa=0,s.shipping_cost_ponderado,s.deprisa)*IF(A_Master_Sample.PackageWeight=0,1,A_Master_Sample.PackageWeight)/IF(t.peso_total=0,1,t.peso_total),s.shipping_cost_ponderado*IF(A_Master_Sample.PackageWeight=0,1,A_Master_Sample.PackageWeight)/IF(t.peso_total=0,1,t.peso_total))
                               WHEN t.fk_courier=3 THEN IFNULL(IF(s.servientrega=0,s.shipping_cost_ponderado,s.servientrega)*IF(A_Master_Sample.PackageWeight=0,1,A_Master_Sample.PackageWeight)/IF(t.peso_total=0,1,t.peso_total),s.shipping_cost_ponderado*IF(A_Master_Sample.PackageWeight=0,1,A_Master_Sample.PackageWeight)/IF(t.peso_total=0,1,t.peso_total))
                               WHEN t.fk_courier=2 THEN IFNULL(IF(s.thomas=0,s.shipping_cost_ponderado,s.thomas)*IF(A_Master_Sample.PackageWeight=0,1,A_Master_Sample.PackageWeight)/IF(t.peso_total=0,1,t.peso_total),s.shipping_cost_ponderado*IF(A_Master_Sample.PackageWeight=0,1,A_Master_Sample.PackageWeight)/IF(t.peso_total=0,1,t.peso_total))
                               ELSE s.shipping_cost_ponderado*IF(A_Master_Sample.PackageWeight=0,1,A_Master_Sample.PackageWeight)/IF(t.peso_total=0,1,t.peso_total)
                          END
where t.peso_total >= s.peso_min and t.peso_total <= s.peso_max 
and A_Master_Sample.isMPlace=0
and A_Master_Sample.MonthNum >= 201401  
and OrderAfterCan = 1 and out_order_tracking.fulfillment_type_real in ( 'Own Warehouse','Consignment');

/*
* Temp. table for update shipping costs where fulfilment is crossdocking
*/
DROP TEMPORARY TABLE IF EXISTS CrossDocking;
CREATE TEMPORARY TABLE CrossDocking ( INDEX ( PaymentMethod ), INDEX( shipping_carrier_tracking_code ), INDEX( PackageWeight ) )    
SELECT 
	           t.idSalesOrder, 
			   t.itemid,
			   fulfillment_type_real,
			   t.fk_shipment_zone_mapping,
			   t.PaymentMethod PaymentMethod,
			   NetItemsInOrder, 
			   PackageWeight,
         shipping_carrier_tracking_code
        from            A_Master_Sample  t 
		     INNER JOIN operations_co.out_order_tracking ON itemid = item_id 
        where     MonthNum >= 201401 
		      AND OrderAfterCan = 1 
			  AND fulfillment_type_real = 'Crossdocking'
        group by t.idSalesOrder, 
		         t.itemid,
				 fulfillment_type_real,
				 t.PaymentMethod
;				 

/*
* Shipping Cost where fulfillment is crossdocking
*/
UPDATE 
                 CrossDocking t 
       left join tbl_payment_method_groups pm 
	          ON t.PaymentMethod = pm.payment_method
       left join M_Shipping_Costs_Ponderados s 
	          ON     s.payment_method_group = pm.payment_method_group
                 and t.fk_shipment_zone_mapping = s.fk_shipment_zone_mapping 
                 and t.PackageWeight>= s.peso_min 
				 AND t.PackageWeight<= s.peso_max
       left join A_Master_Sample 
	          ON     t.itemid = A_Master_Sample.itemid
                 and A_Master_Sample.fk_shipment_zone_mapping = s.fk_shipment_zone_mapping
 SET  
    ShippingCost = s.shipping_cost_ponderado
where
         A_Master_Sample.MonthNum >= 201401  
	 AND OrderAfterCan = 1;

UPDATE A_Master_Sample 
INNER JOIN operations_co.out_order_tracking 
on itemid = item_id
 SET  ShippingCost = 0 
where fulfillment_type_real = 'Dropshipping' AND MonthNum >= 201401;   

#SRT implementation
UPDATE A_Master_Sample
INNER JOIN bob_live_co.sales_order_item
ON itemid = id_sales_order_item
SET ShippingCost = sales_order_item.shipment_cost
WHERE date > '2014-03-05'
AND sales_order_item.shipment_cost !=0;