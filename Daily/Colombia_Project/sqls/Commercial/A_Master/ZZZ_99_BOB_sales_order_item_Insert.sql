ALTER TABLE A_Master_Sample
            DROP COLUMN fk_customer_address_region      ,
            DROP COLUMN fk_sales_order_address_shipping ,
            DROP COLUMN fk_sales_order_item_status      ,
            DROP COLUMN fk_catalog_shipment_type        ,
			#Agregado por Mario M.: para corregir el error de A_Master de Abr. 10/2014
			DROP COLUMN AboutLinio
;

#REPLACE A_Master SELECT    A_Master_Sample.* FROM A_Master_Sample;
DROP TABLE IF EXISTS A_Master;
CREATE TABLE A_Master LIKE A_Master_Sample;
INSERT A_Master
SELECT * FROM A_Master_Sample; 

DROP TABLE IF EXISTS A_Master_Backup;
CREATE TABLE A_Master_Backup LIKE A_Master;
INSERT A_Master_Backup
SELECT * FROM A_Master;


DROP TABLE IF EXISTS  development_co.A_Master;
CREATE TABLE development_co.A_Master LIKE A_Master;
INSERT development_co.A_Master SELECT * FROM A_Master;