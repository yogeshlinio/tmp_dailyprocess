DROP TABLE IF EXISTS development_co_project.A_E_BI_First_Orders;
CREATE TABLE development_co_project.A_E_BI_First_Orders (  PRIMARY KEY ( CustomerNum , id ), INDEX( CustomerNum ), id int auto_increment )
SELECT
  CustomerNum,
  OrderNum,
  NULL AS id,
  MonthNum,
  Date,
  OrderAfterCan,
  OrderBeforeCan,
  GrandTotal
FROM  
  development_co_project.A_Master_Sample 
WHERE  
  OrderAfterCan = 1
  AND Date > "2012-05-09"
GROUP BY    OrderNum
ORDER BY 
  CustomerNum , Date asc, `Time`;

DELETE FROM development_co_project.A_E_BI_First_Orders WHERE id > 1 ;

UPDATE            development_co_project.A_E_BI_First_Orders
       INNER JOIN development_co_project.A_Master_Sample  USING( CustomerNum )
SET
  development_co_project.A_Master_Sample.FirstOrderNum  = A_E_BI_First_Orders.OrderNum,
  development_co_project.A_Master_Sample.CohortMonthNum = A_E_BI_First_Orders.MonthNum,
  development_co_project.A_Master_Sample.NewReturning  = IF( A_E_BI_First_Orders.OrderNum = development_co_project.A_Master_Sample.OrderNum, "NEW", "RETURNING"  )
;
