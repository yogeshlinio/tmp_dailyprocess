/*
* Inserta nuevas ordenes VC
*/
INSERT INTO development_co_project.M_Corporative_Sales(`Order`, Items, MonthName, Month_Num, sku_config, sku_simple, sku_name, CostAfterTax, TaxPercent)
SELECT ordernum
	, COUNT(1)
	, DATE_FORMAT(date,'%M')
	, monthnum
	, skuconfig
	, skusimple
	, skuname
	, A_Master_Sample.CostAfterTax
	, A_Master_Sample.taxpercent
FROM development_co_project.A_Master_Sample
LEFT JOIN development_co_project.M_Corporative_Sales
ON A_Master_Sample.ordernum=M_Corporative_Sales.order
WHERE (couponcode LIKE 'VC%' 
OR couponcode LIKE 'REOVC%' 
OR couponcode LIKE 'DEVVC%' 
OR couponcode LIKE  'REPVC%')
AND OrderBeforeCan=1
AND M_Corporative_Sales.order IS NULL
GROUP BY ordernum,skusimple;

/*
* Actualiza CostAfterTax de M_Corporative_Sales
*/

UPDATE M_Corporative_Sales 
       INNER JOIN A_Master
               ON M_Corporative_Sales.Order = A_Master.OrderNum
			   AND M_Corporative_Sales.sku_simple = A_Master.SkuSimple
SET 
  M_Corporative_Sales.CostAfterTax = A_Master.CostAfterTax 
;

/*
* Actualiza A_Master
*/
UPDATE            A_Master_Sample  
       INNER JOIN M_Corporative_Sales 
               ON A_Master_Sample.OrderNum = M_Corporative_Sales.Order 
			   AND A_Master_Sample.SkuSimple = M_Corporative_Sales.sku_simple
SET 
   A_Master_Sample.CostAfterTax  = IF( useCostNegotiate = 1, M_Corporative_Sales.CostNegotiate,  A_Master_Sample.CostAfterTax  ) , 
   A_Master_Sample.ShippingCost = IF( useShippingCost = 1, A_Master_Sample.ShippingCost, 0 ) , 
   A_Master_Sample.DeliveryCostSupplier   = IF( useInboundCost = 1, A_Master_Sample.DeliveryCostSupplier, 0 ) ,
   A_Master_Sample.FLWHCost = IF( useWH = 1, A_Master_Sample.FLWHCost , 0 )
;