
UPDATE A_Master_Sample 
SET 
   Price         = 19900, 
   PriceAfterTax = 19900/ TaxPercent, 
   PaidPrice  = 9900, 
   PaidPriceAfterTax = 9900/1.16, 
   CouponValue = Price - PaidPrice
 
WHERE 
       CouponCode = 'CAC0403' 
   AND SkuSimple = 'CR610EL90NARLACOL-99496';
   
/*
	Ventas Corporativas
*/
UPDATE development_co_project.A_Master_Sample
SET
   development_co_project.A_Master_Sample.CouponValue         = 0,
   development_co_project.A_Master_Sample.CouponValueAfterTax = 0
WHERE
   development_co_project.A_Master_Sample.OrderNum IN (200236123,
200123283,
200433763,
200645493,
200588663,
200316963,
200336963,
200616963,
200749963);

/*
  Ventas Extraordinarias
*/
UPDATE        A_Master_Sample t
   INNER JOIN tbl_ventas_extraordinarias v 
           ON CouponCode = v.coupon_code
SET
   t.PaidPrice          = (v.paid_price/31),
   t.PaidPriceAfterTax  = (v.paid_price/31) / ( 1 + taxPercent / 100 ),   
   t.Price         = (v.unit_price/31) ,
   t.PriceAfterTax = (v.unit_price/31) / ( 1 + taxPercent / 100 ) ,   
   t.ShippingFee         = (v.shipping_fee/31),
   t.ShippingFeeAfterTax = (v.shipping_fee/31)/ ( 1 + taxPercent / 100 )    
   
where  
   v.coupon_code not like 'VC%' and MonthNum = 201308;
   
   
UPDATE A_Master_Sample
SET 
   PaidPrice          = CouponValue, 
   PaidPriceAfterTax  = CouponValueAfterTax,    
   CouponValue        = 0,
   CouponValueAfterTax = 0 
WHERE OrderBeforeCan = 1 
and ( 
        CouponCode like 'REO%' 
     or CouponCode like 'REP%' 
	 or CouponCode like 'INV%' 
	 or CouponCode like 'GAR%' 
	 or CouponCode like 'CAN%' 
	 or CouponCode like 'DEV%'
	 or CouponCode like 'CSS%'
	 or CouponCode like 'ER%'
    )
	AND PaidPrice = 0
	and Price = CouponValue
;

UPDATE A_Master_Sample
SET 
   PaidPrice          = CouponValue + PaidPrice, 
   PaidPriceAfterTax  = CouponValueAfterTax + PaidPriceAfterTax,    
   CouponValue        = 0,
   CouponValueAfterTax = 0 
WHERE OrderBeforeCan = 1 
and ( 
        CouponCode like 'REO%' 
     or CouponCode like 'REP%' 
	 or CouponCode like 'INV%' 
	 or CouponCode like 'GAR%' 
	 or CouponCode like 'CAN%' 
	 or CouponCode like 'DEV%'
	 or CouponCode like 'CSSREO%'
	 or CouponCode like 'CSSREP%'
	 or CouponCode like 'CSSINV%'
	 or CouponCode like 'CSSGAR%'
	 or CouponCode like 'CSSCAN%'
	 or CouponCode like 'CSSDEV%'
	 or CouponCode like 'CSSER%'
	 or CouponCode like 'ER%'
    )
	AND PaidPrice > 0
;

/*
*  M_Cambios_Costos  
*/
UPDATE A_Master_Sample od
  JOIN M_Cambios_Costos cc
    ON od.skuConfig = cc.sku
SET 
    od.PaidPrice           = od.CouponValue
  , od.CouponValue         = IF(od.Price-od.CouponValue<0,0,od.Price-od.CouponValue)
  , od.PaidPriceAfterTax   = od.PaidPrice/(1 + od.TaxPercent/100)
  , od.CouponValueAfterTax = od.CouponValue/(1 + od.TaxPercent/100)
WHERE 
         cc.is_config=1 
	 AND cc.is_op=0 
	 AND cc.is_coupon=0 
	 AND cc.cost_to_apply=6
     AND od.date >= CASE WHEN no_range=1 THEN od.date 
                         ELSE cc.from_date END 
     AND od.date <= CASE WHEN no_range=1 THEN od.date
                         ELSE cc.to_date END; 

						 
UPDATE      A_Master_Sample od
       JOIN M_Cambios_Costos cc
         ON od.skuSimple = cc.sku
SET 
    od.PaidPrice           = od.CouponValue
  , od.CouponValue         = IF(od.Price-od.CouponValue<0,0,od.Price-od.CouponValue)
  , od.PaidPriceAfterTax   = od.PaidPrice/(1 + od.TaxPercent/100)
  , od.CouponValueAfterTax = od.CouponValue/(1 + od.TaxPercent/100)
WHERE 
       cc.is_config=0 
   AND cc.is_op=0 
   AND cc.is_coupon=0 
   AND cc.cost_to_apply=6
   AND od.date >= CASE WHEN no_range=1 THEN od.date 
                       ELSE cc.from_date END 
   AND od.date <= CASE WHEN no_range=1 THEN od.date
                       ELSE cc.to_date END; 

UPDATE      A_Master_Sample od
       JOIN M_Cambios_Costos cc
         ON od.CouponCode = cc.sku
SET 
    od.PaidPrice           = od.CouponValue
  , od.CouponValue         = IF(od.Price-od.CouponValue<0,0,od.Price-od.CouponValue)
  , od.PaidPriceAfterTax   = od.PaidPrice/(1 + od.TaxPercent/100)
  , od.CouponValueAfterTax = od.CouponValue/(1 + od.TaxPercent/100)
WHERE 
        cc.is_config=0 
	AND cc.is_op=0 
	AND cc.is_coupon=1 
	AND cc.cost_to_apply=6
    AND od.date >= CASE WHEN no_range=1 THEN od.date 
                      ELSE cc.from_date END 
    AND od.date <= CASE WHEN no_range=1 THEN od.date
                      ELSE cc.to_date END; 
					  
/*					  
UPDATE A_Master_Sample t
JOIN tbl_bi_ops_delivery d
ON t.item=d.item_id
JOIN M_Cambios_Costos cc
ON d.op=cc.sku
SET t.paid_price=cc.cost
  , t.coupon_money_value = IF(t.unit_price-t.coupon_money_value<0,0,t.unit_price-t.coupon_money_value)
  , t.paid_price_after_vat = t.paid_price/(1 + t.tax_percent/100)
  , t.coupon_money_after_vat = t.coupon_money_value/(1 + t.tax_percent/100)
WHERE cc.is_config=0 AND cc.is_op=1 AND cc.is_coupon=0 AND cc.cost_to_apply=6
AND t.date >= CASE WHEN no_range=1 THEN t.date 
                      ELSE cc.from_date END 
  AND t.date <= CASE WHEN no_range=1 THEN t.date
                      ELSE cc.to_date END;
*/
					  

					   