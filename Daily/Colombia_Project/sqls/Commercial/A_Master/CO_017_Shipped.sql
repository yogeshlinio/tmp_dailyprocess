/*
* CO_017_Shipped
*/

DROP TABLE IF EXISTS TMP_DateShipped;
CREATE TABLE TMP_DateShipped (Index (ItemID))
SELECT
 item_id AS ItemID,
 date_shipped
FROM operations_co.out_order_tracking_sample
WHERE date_shipped IS NOT NULL;

UPDATE A_Master_Sample a
INNER JOIN TMP_DateShipped b on b.ItemID = a.ItemID
SET
 a.DateShipped = b.date_shipped;
 
UPDATE development_co_project.A_Master_Sample
SET shipped = 1
where DateShipped is not null; 