UPDATE A_Master_Sample
SET
   PaymentFees = least( A_Master_Sample.GrandTotal * ( A_Master_Sample.Fees / 100 )
                 +A_Master_Sample.ExtraCharge,  A_Master_Sample.Min_Value)
;

SELECT trm into @ultima_tasa 
FROM  tbl_paypal_trm_diaria d
WHERE fecha=(SELECT MAX(fecha)
			FROM tbl_paypal_trm_diaria);


UPDATE A_Master_Sample t
LEFT JOIN tbl_paypal_trm_diaria d
ON t.date=d.fecha
set t.PaymentFees=CASE WHEN d.fecha IS NULL 
THEN 0.038*(GrandTotal)+(0.3*@ultima_tasa)/CAST(t.NetItemsInOrder AS DECIMAL)
            ELSE 0.038*(GrandTotal)+(0.3*d.trm)/CAST(t.NetItemsInOrder AS DECIMAL) 
       END
WHERE t.PaymentMethod ='Paypal_Express_Checkout' and (t.OrderAfterCan = 1);


UPDATE A_Master_Sample t
SET PaymentFees=GrandTotal*0.01/CAST(NetItemsInOrder AS DECIMAL)
WHERE city LIKE '%bogota%' 
AND  PaymentMethod ='CashOnDelivery_Payment' 
AND (t.OrderAfterCan = 1);

UPDATE A_Master_Sample t
SET PaymentFees=GrandTotal*0.015/CAST(NetItemsInOrder AS DECIMAL)
WHERE city NOT LIKE '%bogota%' 
AND  PaymentMethod ='CashOnDelivery_Payment' 
AND (t.OrderAfterCan = 1);



/*
*  Colombia's Adjust
*/
