/*
* MX_013 CreditNote Count Per Supplier
*/
#Query: M1_CanonCountFeb
DROP TABLE IF EXISTS  A_CountPerSupplier;
CREATE TABLE IF NOT EXISTS  A_CountPerSupplier
(
 supplier          varchar(50),
 Category_BP       varchar(20),
 Month_Num   int,
 count    int,
 key( supplier, Category_BP, Month_Num )
);
TRUNCATE TABLE  A_CountPerSupplier;
REPLACE INTO  A_CountPerSupplier
SELECT 
   supplier,
   CatBP AS Category_BP,
   MonthNum,
   Count(*) AS Cuenta
FROM  A_Master_Sample
where  A_Master_Sample.OrderAfterCan=1
  and CatBP != ""
GROUP BY supplier, CatBP, MonthNum
;

REPLACE INTO  A_CountPerSupplier
SELECT 
   supplier,
   "" AS Category_BP,
   MonthNum,
   Count(*) AS Cuenta
FROM  A_Master_Sample
where  A_Master_Sample.OrderAfterCan=1
GROUP BY supplier, MonthNum
;

 #Query: M1_OSRI_2013
CREATE TABLE IF NOT EXISTS `A_CostSupplier` (
  `supplier` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `Month_Num` int(11) NOT NULL DEFAULT '0',
  `Category_BP` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `Cost` decimal(12,2) DEFAULT NULL,
  `Cost_after_tax` decimal(12,2) DEFAULT NULL,
  `Count_SKU` int(11) DEFAULT NULL,
  `Factor_Cost` decimal(17,5) DEFAULT NULL,
  `Factor_Cost_AT` decimal(17,5) DEFAULT NULL,
  PRIMARY KEY (`supplier`,`Month_Num`,`Category_BP`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
;

TRUNCATE TABLE  A_CostSupplier;


INSERT INTO  A_CostSupplier
(
  supplier,
  Month_Num,
  Category_BP,
  Cost,
  Cost_After_Tax
)
SELECT
  supplier,
  Month_Num,
  Category_BP,
  SUM( Cost ) * FactorMonth( now() - INTERVAL 1 DAY, Month_Num ) AS Cost,
  SUM( Cost_after_tax) * FactorMonth( now() - INTERVAL 1 DAY, Month_Num ) AS Cost_after_tax
FROM
   GlobalConfig.M_CreditNotes
WHERE 
   Country = "COL"
GROUP BY
  supplier,
  Month_Num,
  Category_BP
;

#SELECT  A_Master_Sample.*
#FROM  A_Master_Sample
#WHERE ((( A_Master_Sample.Date)>="2013/1/4"));
#1. Update Quanty of Item per Supplier
UPDATE             A_CostSupplier as BI_Cost
       INNER JOIN  A_CountPerSupplier   as CountPerSupplier
            USING ( supplier , Category_BP, Month_Num )
SET
    BI_Cost.Count_SKU = CountPerSupplier.count;

UPDATE   A_CostSupplier as BI_Cost    
SET
      `Factor_Cost`    = Cost / Count_SKU,
      `Factor_Cost_AT` = Cost_after_tax / Count_SKU
;

UPDATE
                   A_Master_Sample             AS Out_Sales 
       INNER JOIN  A_CostSupplier   AS BI_Cost 
               ON     Out_Sales.Supplier = BI_Cost.Supplier
                  AND Out_Sales.MonthNum = BI_Cost.Month_Num
SET
   Out_Sales.CreditNotes                 = Factor_Cost_AT
#   Out_Sales.Cost                 = Out_Sales.Cost            - Factor_Cost,
#   Out_Sales.CreditNotes          = Factor_Cost,
#   Out_Sales.CostAfterTax         = Out_Sales.CostAfterTax    - Factor_Cost_AT
#   Out_Sales.CreditNotesAfterTax  = Factor_Cost_AT
WHERE          Out_Sales.OrderAfterCan=1
         AND BI_Cost.Category_BP = ""
           AND Date >= "2013/02/01"
;

UPDATE
                   A_Master_Sample  AS Out_Sales 
       INNER JOIN  A_CostSupplier   AS BI_Cost 
               ON     Out_Sales.MonthNum = BI_Cost.Month_Num
                  AND Out_Sales.Supplier = BI_Cost.Supplier
                  AND Out_Sales.CatBP    = BI_Cost.Category_BP

SET
   Out_Sales.CreditNotes                 = Factor_Cost_AT
WHERE        Out_Sales.OrderAfterCan=1
         AND Date >= "2013/02/01"
         AND BI_Cost.Category_BP != ""	
;
