UPDATE        A_Master_Sample 
   INNER JOIN A_E_In_COM_Fees_per_PM 
           ON A_Master_Sample.PaymentMethod = A_E_In_COM_Fees_per_PM.Payment_Method
SET
   A_Master_Sample.Fees        = A_E_In_COM_Fees_per_PM.Fee,
   A_Master_Sample.ExtraCharge = A_E_In_COM_Fees_per_PM.Extra_Charge,
   A_Master_Sample.Min_Value   = A_E_In_COM_Fees_per_PM.Min_Value;