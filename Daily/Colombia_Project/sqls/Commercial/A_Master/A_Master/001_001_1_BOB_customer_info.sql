UPDATE            A_Master_Sample
       INNER JOIN @bob_live@.sales_order_address
               ON @bob_live@.sales_order_address.id_sales_order_address = A_Master_Sample.fk_sales_order_address_shipping
SET
   A_Master_Sample.postcode                   = sales_order_address.postcode, 
   A_Master_Sample.city                       = ifnull(sales_order_address.municipality,sales_order_address.city),
   A_Master_Sample.fk_customer_address_region = sales_order_address.fk_customer_address_region
#   A_Master_Sample.taxPercent                 = IF( sales_order_address.region  like 'Archipielago De San Andres, Providencia Y Santa Catalina%', 
#                                                       0 , taxPercent ),
#   A_Master_Sample.tax                       = IF( sales_order_address.region  like 'Archipielago De San Andres, Providencia Y Santa Catalina%', 
#                                                       0 , tax        ) 
;
UPDATE            A_Master_Sample
       INNER JOIN @bob_live@.sales_order_address
               ON @bob_live@.sales_order_address.id_sales_order_address = A_Master_Sample.fk_sales_order_address_shipping
SET 
   A_Master_Sample.City  = CASE 
                              WHEN CAST( A_Master_Sample.city AS CHAR ) = 'LA GUAJIRA' AND CAST( sales_order_address.region AS CHAR )= 'MAICAO'          
							         THEN 'Maicao' 
                              WHEN CAST( A_Master_Sample.city AS CHAR ) = 'Bogota' AND CAST( sales_order_address.region AS CHAR ) = 'Bogota'                             
								     THEN 'Bogota' 
                              WHEN CAST( A_Master_Sample.city AS CHAR ) = '​BOGOTÁ D.C' AND CAST( sales_order_address.region AS CHAR ) = 'Bogota'                             
								     THEN 'Bogota' 
                              WHEN CAST( A_Master_Sample.city AS CHAR ) = '​BOGOTÁ D.C' AND CAST( sales_order_address.region AS CHAR ) = 'CUNDINAMARCA'                  
								     THEN 'Bogota' 
                              WHEN CAST( A_Master_Sample.city AS CHAR ) = '​​ARMENIA (QUI)' AND CAST( sales_order_address.region AS CHAR ) = 'QUINDIO'  
								     THEN 'Armenia'
                              WHEN CAST( A_Master_Sample.city AS CHAR ) = 'pie de cuesta' AND CAST( sales_order_address.region AS CHAR ) = 'Santander'
								     THEN 'Piedecuesta'
                              WHEN CAST( A_Master_Sample.city AS CHAR ) = 'Putumayo' AND CAST( sales_order_address.region AS CHAR ) = 'Orito'
								     THEN 'Orito'
                              WHEN CAST( A_Master_Sample.city AS CHAR ) = 'Tolima' AND CAST( sales_order_address.region AS CHAR ) = 'Ibague'
								     THEN 'Ibague'
                     else A_Master_Sample.city
						   END
;

UPDATE            A_Master_Sample
       INNER JOIN @bob_live@.customer_address_region
               ON @bob_live@.customer_address_region.id_customer_address_region = A_Master_Sample.fk_customer_address_region
SET
   A_Master_Sample.State = @bob_live@.customer_address_region.code 
;

UPDATE        A_Master_Sample 
   INNER JOIN tbl_shipment_zone
           ON City = Ciudad
   INNER JOIN @bob_live@.shipment_zone_mapping
					 ON tbl_shipment_zone.fk_zone = shipment_zone_mapping.fk_shipment_zone
SET
   A_Master_Sample.fk_shipment_zone         = shipment_zone_mapping.fk_shipment_zone,
   A_Master_Sample.fk_shipment_zone_mapping = shipment_zone_mapping.id_shipment_zone_mapping
;