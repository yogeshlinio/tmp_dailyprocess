UPDATE A_Master_Sample
SET 
   DeliveryCostSupplier=0.0 
WHERE
   Supplier='IZC MAYORISTA S.A.S';

UPDATE A_Master_Sample od
  JOIN M_Cambios_Costos cc
    ON od.skuConfig = cc.sku
SET 
    od.DeliveryCostSupplier = cc.cost
WHERE cc.is_config=1 AND cc.is_op=0 AND cc.is_coupon=0 AND cc.cost_to_apply=2
AND od.date >= CASE WHEN no_range=1 THEN od.date 
                      ELSE cc.from_date END 
  AND od.date <= CASE WHEN no_range=1 THEN od.date
                      ELSE cc.to_date END;
   
UPDATE      A_Master_Sample od
       JOIN M_Cambios_Costos cc
         ON od.skuSimple = cc.sku
SET 
    od.DeliveryCostSupplier = cc.cost
WHERE 
        cc.is_config=0 
	AND cc.is_op=0 
	AND cc.is_coupon=0 
	AND cc.cost_to_apply=2
    AND od.date >= CASE WHEN no_range=1 THEN od.date 
                        ELSE cc.from_date END 
    AND od.date <= CASE WHEN no_range=1 THEN od.date
                        ELSE cc.to_date END; 

UPDATE      A_Master_Sample od
       JOIN M_Cambios_Costos cc
         ON od.CouponCode = cc.sku
SET 
     od.DeliveryCostSupplier = cc.cost
WHERE 
        cc.is_config=0 
	AND cc.is_op=0 
	AND cc.is_coupon=1 
	AND cc.cost_to_apply=2
    AND od.date >= CASE WHEN no_range=1 THEN od.date 
                      ELSE cc.from_date END 
    AND od.date <= CASE WHEN no_range=1 THEN od.date
                      ELSE cc.to_date END; 
	
/*
*
UPDATE A_Master_Sample t
JOIN tbl_bi_ops_delivery d
ON t.item=d.item_id
JOIN M_Cambios_Costos cc
ON d.op=cc.sku
SET t.delivery_cost_supplier=cc.cost
WHERE cc.is_config=0 AND cc.is_op=1 AND cc.is_coupon=0 AND cc.cost_to_apply=2
AND t.date >= CASE WHEN no_range=1 THEN t.date 
                      ELSE cc.from_date END 
  AND t.date <= CASE WHEN no_range=1 THEN t.date
                      ELSE cc.to_date END;

*/	
						