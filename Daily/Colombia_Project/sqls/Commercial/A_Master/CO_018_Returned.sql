DROP TEMPORARY TABLE IF EXISTS TMP_Returned;
CREATE TEMPORARY TABLE TMP_Returned (  index( fk_sales_order_item  ) )
SELECT
fk_sales_order_item   
FROM  bob_live_co.sales_order_item_status_history
WHERE
#returned
fk_sales_order_item_status  = 8
GROUP BY  
fk_sales_order_item;

DROP TEMPORARY TABLE IF EXISTS TMP_Deliver;
CREATE TEMPORARY TABLE TMP_Deliver (  index( fk_sales_order_item  ) )
SELECT
fk_sales_order_item   
FROM  bob_live_co.sales_order_item_status_history
WHERE
#delivered, delivered_electronically
fk_sales_order_item_status  in (68,137)
GROUP BY  
fk_sales_order_item;


DROP TEMPORARY TABLE IF EXISTS TMP_RefundedNeeded;
CREATE TEMPORARY TABLE TMP_RefundedNeeded (  index( fk_sales_order_item  ) )
SELECT
fk_sales_order_item   
FROM  bob_live_co.sales_order_item_status_history
WHERE
   fk_sales_order_item_status  in ( 87, 119 )
GROUP BY  
fk_sales_order_item;

DROP TEMPORARY TABLE IF EXISTS TMP;
CREATE TEMPORARY TABLE TMP ( KEY ( fk_sales_order_item ) )
SELECT fk_sales_order_item ,
#refund_needed
 MAX( IF( fk_sales_order_item_status = 119 , created_at  , null  ) ) AS fecha FROM  
bob_live_co.sales_order_item_status_history
WHERE
    fk_sales_order_item IN     ( SELECT * FROM TMP_Deliver ) 
AND fk_sales_order_item IN     ( SELECT * FROM TMP_RefundedNeeded ) 
AND fk_sales_order_item NOT IN ( SELECT * FROM TMP_Returned ) 
GROUP BY fk_sales_order_item ;

REPLACE TMP
#returned
SELECT fk_sales_order_item , MAX( IF( fk_sales_order_item_status = 8 , created_at  , null  ) ) FROM  
bob_live_co.sales_order_item_status_history
WHERE
   
fk_sales_order_item IN ( SELECT * FROM TMP_Returned ) 
GROUP BY fk_sales_order_item ;

UPDATE            A_Master_Sample
       INNER JOIN TMP  ON A_Master_Sample.ItemId = TMP.fk_sales_order_item 
SET
 A_Master_Sample.Refunded = 1,
 A_Master_Sample.`DateRefunded` = TMP.Fecha
;
/*
*
*  MX_018_001	DateDelivered
*/
UPDATE 
               A_Master_Sample  
    INNER JOIN operations_co.out_order_tracking b
            ON A_Master_Sample.ItemID = b.item_id
SET
   A_Master_Sample.DateDelivered =  b.date_delivered,
   A_Master_Sample.Delivered =  1
   
WHERE 
   b.date_delivered is not null
;

DROP TEMPORARY TABLE IF EXISTS TMP_Returned;
CREATE TEMPORARY TABLE TMP_Returned (  index( fk_sales_order_item  ) )
SELECT
fk_sales_order_item   
FROM  bob_live_co.sales_order_item_status_history
WHERE
#returned
fk_sales_order_item_status  = 8
GROUP BY  
fk_sales_order_item;

DROP TEMPORARY TABLE IF EXISTS TMP_Deliver;
CREATE TEMPORARY TABLE TMP_Deliver (  index( fk_sales_order_item  ) )
SELECT
fk_sales_order_item   
FROM  bob_live_co.sales_order_item_status_history
WHERE
#delivered, delivered_electronically
fk_sales_order_item_status  in (68,137)
GROUP BY  
fk_sales_order_item;

/*
* MX_018_002 Delivered
*/
DROP   TEMPORARY TABLE IF EXISTS A_Delivered;
CREATE TEMPORARY TABLE A_Delivered ( INDEX ( fk_sales_order_item  ) )
SELECT
 *
FROM
    bob_live_co.sales_order_item_status_history
WHERE
   bob_live_co.sales_order_item_status_history.fk_sales_order_item_status IN ( 68 , 137 )
GROUP BY fk_sales_order_item
;

UPDATE           A_Delivered
      INNER JOIN A_Master_Sample
              ON A_Delivered.fk_sales_order_item = A_Master_Sample.ItemID 
SET
    A_Master_Sample.DateDelivered = A_Delivered.created_at,
    A_Master_Sample.Delivered = 1;

UPDATE A_Master_Sample
SET
    A_Master_Sample.NetDelivered = 1
WHERE	
fk_sales_order_item_status  =119
;

DROP TEMPORARY TABLE IF EXISTS TMP;
CREATE TEMPORARY TABLE TMP ( KEY ( fk_sales_order_item ) )
SELECT fk_sales_order_item ,
#refund_needed
 MAX( IF( fk_sales_order_item_status = 119 , created_at  , null  ) ) AS fecha FROM  
bob_live_co.sales_order_item_status_history
WHERE
    fk_sales_order_item IN     ( SELECT * FROM TMP_Deliver ) 
AND fk_sales_order_item IN     ( SELECT * FROM TMP_RefundedNeeded ) 
AND fk_sales_order_item NOT IN ( SELECT * FROM TMP_Returned ) 
GROUP BY fk_sales_order_item ;

REPLACE TMP
#returned
SELECT fk_sales_order_item , MAX( IF( fk_sales_order_item_status = 8 , created_at  , null  ) ) FROM  
bob_live_co.sales_order_item_status_history
WHERE
    fk_sales_order_item_status  in ( select id_sales_order_item_status from bob_live_co.sales_order_item_status
                where id_sales_order_item_status IN ( 6 , 52 ) )
;

UPDATE A_Master_Sample
SET
    A_Master_Sample.DeliveredReturn = 1
WHERE
        Delivered     = 1
    AND NetDelivered  = 0;
