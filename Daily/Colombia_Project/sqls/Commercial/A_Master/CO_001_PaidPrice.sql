#Tarjetas de Regalo Partnerships
UPDATE      A_Master_Sample
       JOIN M_GiftCards_Partnerships
         ON CouponCode=cupon
SET 
    PaidPrice         = CouponValue+PaidPrice,
    PaidPriceAfterTax = PaidPrice/(1+TaxPercent/100);