/*IMPLEMENT ON COLOMBIA*/


update        A_Master_Sample t 
   inner join bob_live_co.catalog_simple m
           on     t.skuSimple = m.sku
set 
   t.Cost          = (t.Price*(1-(ifnull(m.percent_fee_mp,0)/100))),
   t.CostAfterTax = (t.PriceAfterTax)*(1-(ifnull(m.percent_fee_mp,0)/100)),
   t.DeliveryCostSupplier = 0, 
   t.FLWHCost = 0, 
   t.ShippingCost = 0
WHERE 
   isMPlace = 1;

#Costos en A_Master_Catalog
update  A_Master_Catalog od inner join 
(select sku_Simple, sku_Name,isMarketPlace, A_Master_Catalog.supplier, A_Master_Catalog.id_supplier, 
avg(percent_fee_mp) commission,
A_Master_Catalog.price, A_Master_Catalog.special_price, A_Master_Catalog.special_to_date, A_Master_Catalog.special_from_date
from A_Master_Catalog inner join bob_live_co.catalog_simple c 
on A_Master_Catalog.sku_simple = c.sku 
WHERE  (A_Master_Catalog.cost = 0 or A_Master_Catalog.cost is null) and isMarketPlace = 1
group by sku_Simple, A_Master_Catalog.id_supplier) t on od.sku_Simple = t.sku_Simple
set 
od.cost = (ifnull(IF (t.special_price is null, 
t.price, 
 if(curdate() > t.special_to_date, t.price, 
 if(curdate() < t.special_from_date, t.price,
  t.special_price))),0))*(1-commission/100)
WHERE od.isMarketPlace = 1 and (od.cost = 0 or od.cost is null);



