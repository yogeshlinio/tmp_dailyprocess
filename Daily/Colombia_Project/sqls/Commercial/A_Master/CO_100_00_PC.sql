 #Query: M1_COGS_PC
UPDATE A_Master_Sample 
SET 
    A_Master_Sample.COGS       =  A_Master_Sample.CostAfterTax 
                                     +A_Master_Sample.DeliveryCostSupplier
                                     -A_Master_Sample.CreditNotes                                     
                                      ,

    A_Master_Sample.PCOne     =  A_Master_Sample.priceAfterTax
                                     +A_Master_Sample.ShippingFeeAfterTax
                                     -A_Master_Sample.CouponValueAfterTax
                                     +A_Master_Sample.Interest
                                     -A_Master_Sample.CostAfterTax
                                     -A_Master_Sample.DeliveryCostSupplier
                                     +A_Master_Sample.CreditNotes, 

    A_Master_Sample.PCOnePFive =  A_Master_Sample.priceAfterTax
                                      +A_Master_Sample.ShippingFeeAfterTax
                                      +A_Master_Sample.Interest
                                      -A_Master_Sample.CouponValueAfterTax
                                      -A_Master_Sample.CostAfterTax
                                      -A_Master_Sample.DeliveryCostSupplier
                                      +A_Master_Sample.CreditNotes                                     
                                      -A_Master_Sample.ShippingCost
                                      -A_Master_Sample.PaymentFees
									  -A_Master_Sample.PackagingCost, 
									  
	A_Master_Sample.PCTwo =  A_Master_Sample.PCOnePFive
											-A_Master_Sample.FLWHCost
											-A_Master_Sample.FLCSCost,
								  
    
    A_Master_Sample.Rev =  A_Master_Sample.PriceAfterTax  
                              +A_Master_Sample.ShippingFeeAfterTax
                              -A_Master_Sample.CouponValueAfterTax
                              +A_Master_Sample.Interest;