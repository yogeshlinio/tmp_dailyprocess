/*
*  Costo de BOB
*/
DROP TEMPORARY TABLE IF EXISTS tmp_inStock;
CREATE TEMPORARY TABLE tmp_inStock( INDEX ( estoque_id  ) )
SELECT
	estoque.estoque_id,
	posicoes.participa_estoque
FROM	(wmsprod_co.estoque
			LEFT JOIN wmsprod_co.itens_recebimento 
				ON estoque.itens_recebimento_id = itens_recebimento.itens_recebimento_id)
			LEFT JOIN wmsprod_co.posicoes 
				ON estoque.endereco = posicoes.posicao
HAVING participa_estoque >0
;


	UPDATE        A_Master_Sample
	   INNER JOIN A_Master_Catalog
			   ON A_Master_Sample.SKUSimple = A_Master_Catalog.SKU_simple
		INNER JOIN tmp_inStock
			   ON tmp_inStock.estoque_id = A_Master_Sample.itemId	   
	SET
	   A_Master_Sample.Cost         = A_Master_Catalog.cost ,
	   A_Master_Sample.CostAfterTax = A_Master_Catalog.cost / ( 1 +  ( A_Master_Sample.TaxPercent / 100 ) )         
    WHERE
          A_Master_Sample.Cost is null 
      OR  A_Master_Sample.Cost = 0	
	;

	
/*
* Costo Promedio
*/      
UPDATE        A_Master_Sample
   INNER JOIN A_Master_Catalog p
           ON A_Master_Sample.SKUSimple= p.sku_simple 
SET 
   A_Master_Sample.Cost = p.Cost_OMS  ,
   A_Master_Sample.CostAftertax = p.Cost_OMS /( ( 100 +  A_Master_Sample.TaxPercent )/100 )
WHERE
 p.Cost_OMS > 0 
 and
  (A_Master_Sample.Cost is null 
	   OR A_Master_Sample.Cost = 0)
;   

/*
* MX_005_003 Cost_OMS
*/
UPDATE             A_Master_Sample  
        INNER JOIN wmsprod_co.itens_venda b 
                ON b.item_id = A_Master_Sample.ItemID
        INNER JOIN wmsprod_co.estoque c 
                ON c.estoque_id = b.estoque_id 
        INNER JOIN wmsprod_co.itens_recebimento d
                ON c.itens_recebimento_id = d.itens_recebimento_id 
        INNER JOIN procurement_live_co.wms_received_item e 
                ON c.itens_recebimento_id=e.id_wms 
        INNER JOIN procurement_live_co.procurement_order_item f
                ON e.fk_procurement_order_item=f.id_procurement_order_item 
SET 
    A_Master_Sample.Cost           = f.unit_price   , 
    A_Master_Sample.CostAftertax   = f.unit_price /( ( 100 +  A_Master_Sample.TaxPercent )/100 )
WHERE 
       f.is_deleted = 0
   #AND (    A_Master_Sample.Date >= "2013-07-01" 
   #      OR A_Master_Sample.Cost is null 
   #      OR A_Master_Sample.Cost <= 0 )
   AND f.unit_price is not null 
   AND f.unit_price > 0
       ;
 
--  UPDATE development_co_project.A_Master_Sample
-- INNER JOIN wmsprod_co.itens_venda ON A_Master_Sample.ItemID = itens_venda.item_id
-- SET A_Master_Sample.Cost = itens_venda.cost_item,
--  A_Master_Sample.CostAfterTax = itens_venda.cost_item / (
-- 	1 + (
-- 		A_Master_Sample.TaxPercent / 100
-- 	)
-- );
 
 /*
UPDATE      A_Master_Sample t
       JOIN tbl_bi_ops_delivery d
ON t.item=d.item_id
JOIN M_Cambios_Costos cc
ON d.op=cc.sku
SET t.cost_oms=cc.cost
WHERE cc.is_config=0 AND cc.is_op=1 AND cc.is_coupon=0 AND (cc.cost_to_apply=0 OR cc.cost_to_apply=1)
AND t.date >= CASE WHEN no_range=1 THEN t.date 
                      ELSE cc.from_date END 
  AND t.date <= CASE WHEN no_range=1 THEN t.date
                      ELSE cc.to_date END;


*/