UPDATE        development_co_project.A_Master_Sample 
   INNER JOIN development_co_project.A_E_In_COM_Fees_per_PM 
           ON     A_Master_Sample.PaymentMethod = A_E_In_COM_Fees_per_PM.Payment_Method
SET
   development_co_project.A_Master_Sample.Fees        = development_co_project.A_E_In_COM_Fees_per_PM.Fee,
   development_co_project.A_Master_Sample.ExtraCharge = development_co_project.A_E_In_COM_Fees_per_PM.Extra_Charge,
   development_co_project.A_Master_Sample.Min_Value   = development_co_project.A_E_In_COM_Fees_per_PM.Min_Value;