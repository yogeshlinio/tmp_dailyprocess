/*DROP TEMPORARY TABLE IF EXISTS TMP_Exported;
CREATE TEMPORARY TABLE TMP_Exported (  index( ItemID  ) )
SELECT
   fk_sales_order_item AS ItemId,
   max( created_at ) AS DateExported
FROM  bob_live_co.sales_order_item_status_history
WHERE
	#exported, exported_electronically
   fk_sales_order_item_status in ( 4,136 )
GROUP BY  
fk_sales_order_item;

UPDATE        A_Master_Sample  
   INNER JOIN TMP_Exported 
        USING ( ItemId )
SET
   A_Master_Sample.Exported     = 1,
   A_Master_Sample.DateExported = TMP_Exported.DateExported
;*/

DROP TEMPORARY TABLE IF EXISTS TMP_Exported;
CREATE TEMPORARY TABLE TMP_Exported (  index( ItemID  ) )
SELECT
   item_id AS ItemId,
   date_exported AS DateExported
FROM  operations_co.out_order_tracking_sample
;

UPDATE        A_Master_Sample  
   INNER JOIN TMP_Exported 
        USING ( ItemId )
SET
   A_Master_Sample.Exported     = 1,
   A_Master_Sample.DateExported = TMP_Exported.DateExported
;