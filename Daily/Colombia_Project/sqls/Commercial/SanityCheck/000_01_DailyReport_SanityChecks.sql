INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Mexico', 
  'DailyReport',
  NOW(),
  MAX(`Date Placed`),
  count(*),
  count(*)
FROM
   DailyReport 
;


