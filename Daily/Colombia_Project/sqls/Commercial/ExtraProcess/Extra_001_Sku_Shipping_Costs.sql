drop table if exists development_co_project.tbl_sku_shipping_cost;
	create table development_co_project.tbl_sku_shipping_cost ( index( SkuSimple) ) as
	select SkuSimple, sum(shipping_cost_ponderado*items)/sum(items) shipping_cost 
from development_co_project.A_Master_Sample t 
inner join development_co_project.M_Shipping_Costs_Ponderados s 
on  ProductWeight >= peso_min
 and ProductWeight <= peso_max
inner join 
	(select A_Master_Sample.MonthNum, 
	A_Master_Sample.fk_shipment_zone_mapping,
	count(1)/c.items r, count(1) items
	from development_co_project.A_Master_Sample 
	inner join (select MonthNum, 
	count(distinct idSalesOrder) orders,
	 count(1) items
	from development_co_project.A_Master_Sample 
	where OrderBeforeCan = 1
	and MonthNum >= date_format(curdate() - interval 6 month, '%Y%m')
	and MonthNum <= date_format(curdate(), '%Y%m')
	group by MonthNum ) c 
on A_Master_Sample.MonthNum =c.MonthNum
where OrderBeforeCan = 1
and A_Master_Sample.MonthNum = 201308
group by A_Master_Sample.MonthNum, State,
fk_shipment_zone_mapping
order by r desc
limit 6) distribucion_ventas 
on distribucion_ventas.fk_shipment_zone_mapping = s.fk_shipment_zone_mapping
where  payment_method_group = 'COD'
group by SkuSimple;