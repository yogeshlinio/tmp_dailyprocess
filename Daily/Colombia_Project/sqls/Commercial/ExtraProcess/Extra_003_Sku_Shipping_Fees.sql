
drop table if exists development_co_project.tbl_sku_shipping_fees;
	create table development_co_project.tbl_sku_shipping_fees ( index( sku) ) as
select tab1.sku, sum(handling_cost*items)/sum(items)+ sum(freight_cost*items)/sum(items) shipping_fee,
sum(handling_cost*items)/sum(items) handling_cost, sum(freight_cost*items)/sum(items) freight_cost
from (
	select sku, name,fk_shipment_zone,
	#Se elige el mínimo handling cost de todos los posibles
	min(handling_cost) handling_cost,
	ifnull(min(freight_cost_ex), min(freight_cost)) freight_cost
	from (
	select cs.sku, cc.name,c.fk_catalog_category, handling_cost, s.cost_structure, s.fk_shipment_zone,
	if(ss.cost_structure = 'i',ss.freight_cost, ifnull(ifnull(cc.package_weight, cc.product_weight*ss.freight_cost),ss.freight_cost)) freight_cost_ex,
	if(s.cost_structure = 'i',s.freight_cost, ifnull(ifnull(cc.package_weight, cc.product_weight*s.freight_cost),s.freight_cost)) freight_cost
	 from bob_live_co.shipment_freight_cost s 
	inner join bob_live_co.catalog_config_has_catalog_category c 
	on c.fk_catalog_category = s.fk_catalog_category
	inner join bob_live_co.catalog_config cc 
	on id_catalog_config = c.fk_catalog_config
	inner join bob_live_co.catalog_simple cs 
	on id_catalog_config = cs.fk_catalog_config
	inner join bob_live_co.shipment_handling_cost sh 
	on sh.fk_catalog_category = c.fk_catalog_category
	left join bob_live_co.shipment_freight_cost_sku ss 
	on ss.fk_shipment_zone = s.fk_shipment_zone
	 and ss.fk_catalog_simple = cs.id_catalog_simple
	group by cs.sku, c.fk_catalog_category, s.fk_shipment_zone
	order by cs.sku,s.fk_shipment_zone, s.cost_structure ) t

	group by sku, fk_shipment_zone) tab1
inner join 
	(select A_Master_Sample.MonthNum, 
	A_Master_Sample.fk_shipment_zone,
	count(1)/c.items r, count(1) items
	from development_co_project.A_Master_Sample 
	inner join (select MonthNum, 
	count(distinct idSalesOrder) orders,
	 count(1) items
	from development_co_project.A_Master_Sample  
	where OrderBeforeCan = 1
	and MonthNum >= date_format(curdate() - interval 6 month, '%Y%m')
	and MonthNum <= date_format(curdate(), '%Y%m')
	group by MonthNum ) c 
	on A_Master_Sample.MonthNum =c.MonthNum
	where OrderBeforeCan = 1
	and A_Master_Sample.MonthNum >= date_format(curdate() - interval 6 month, '%Y%m')
	and A_Master_Sample.MonthNum <= date_format(curdate(), '%Y%m')
	group by A_Master_Sample.MonthNum, 
	fk_shipment_zone
	order by r desc
	limit 6) distribucion_ventas 
on tab1.fk_shipment_zone = distribucion_ventas.fk_shipment_zone
where freight_cost > 0 and freight_cost is not null
group by sku;
