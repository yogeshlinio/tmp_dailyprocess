#Query: CACReport
drop table if EXISTS CACReport;
create table CACReport
(SELECT
	A_Master_0.StoreId,
	A_Master_0.Date,
	date_format(date,'%u') AS 'Week',
	A_Master_0.OrderNum,
	A_Master_0.ItemID,
	A_Master_0.Supplier,
	A_Master_0.Brand,
	A_Master_0.CatBP,
	A_Master_0.Cat1,
	A_Master_0.Cat2,
	A_Master_0.Cat3,
	A_Master_0.SKUConfig,
	A_Master_0.SKUSimple,
	A_Master_0.SKUName,
	A_Master_0.isVisible,
	A_Master_0.isMPlace,
	A_Master_0.PriceAfterTax,
	A_Master_0.PrefixCode,
	A_Master_0.CouponCode,
	A_Master_0.CouponValueAfterTax,
	A_Master_0.NonMKTCouponAfterTax,
	A_Master_0.GrandTotal,
	A_Master_0.CostAfterTax,
	A_Master_0.WHCost,
	A_Master_0.CSCost,
	A_Master_0.FLWHCost,
	A_Master_0.FLCSCost,
	A_Master_0.ShipmentType,
	A_Master_0.DeliveryCostSupplier,
	A_Master_0.ShippingCost,
	A_Master_0.ShippingFee,
	A_Master_0.PaymentMethod,
	A_Master_0.IssuingBank,
	A_Master_0.Installment,
	A_Master_0.PaymentFees,
	A_Master_0.InstallmentFeeAfterTax,
	A_Master_0.NetInterest,
	A_Master_0.CustomerNum,
	A_Master_0.CustomerEmail,
	A_Master_0.State,
	A_Master_0.City,
	A_Master_0.`STATUS`,
	A_Master_0.OrderBeforeCan,
	A_Master_0.OrderAfterCan,
	A_Master_0.Cancellations,
	A_Master_0.Pending,
	A_Master_0.`RETURNS`,
	A_Master_0.Rejected,
	A_Master_0.Cancelled,
	A_Master_0.Delivered,

	A_Master_0.NetDelivered,
	A_Master_0.Exported,
	A_Master_0.Collected,
	A_Master_0.Refunded,
	A_Master_0.Exportable,
	A_Master_0.Shipped,
	A_Master_0.MCI,
	A_Master_0.MSI,
	A_Master_0.NewReturning,
	A_Master_0.COGS,
	A_Master_0.PCOne,
	A_Master_0.PCOnePFive,
	A_Master_0.Rev,
	A_Master_0.Rev * A_Master_0.MPlaceFee AS 'MP Fee',
IF (
	A_Master_0.isMPlace = 1,
	A_Master_0.Rev,
	0
) AS 'MP Revenue',
 A_Master_0.OtherRev,
 A_Master_0.CreditNotes,
 PERIOD_DIFF(
	DATE_FORMAT(
		CONCAT(A_Master_0.MonthNum, '01'),
		'%Y%m'
	),
	DATE_FORMAT(
		CONCAT(A_Master_0.CohortMonthNum, '01'),
		'%Y%m'
	)
) AS 'Dif',
 A_Master_0.Campaign,
 A_Master_0.Source_medium,
 A_Master_0.Channel,
 A_Master_0.ChannelGroup,
 A_Master_0.FirstOrderNum
FROM
	A_Master A_Master_0
WHERE
	MonthNum >= date_format(
		date(now()) - INTERVAL 2 MONTH,
		'%Y%m'));