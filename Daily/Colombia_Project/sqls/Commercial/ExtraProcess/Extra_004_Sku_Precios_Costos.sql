
drop table if exists development_co_project.tbl_sku_precios_costos;
	create table development_co_project.tbl_sku_precios_costos (
    index (sku_simple)
) as SELECT A_Master_Catalog.sku_simple,
    ifnull(IF(A_Master_Catalog.special_price is null,
                A_Master_Catalog.price,
                if(now() > A_Master_Catalog.special_to_date,
                    A_Master_Catalog.price,
                    if(now() < A_Master_Catalog.special_from_date,
                        A_Master_Catalog.price,
                        A_Master_Catalog.special_price))),
            0) as 'precio_actual',
    ifnull(IF(A_Master_Catalog.special_purchase_price is null,
                A_Master_Catalog.cost_oms,
                if(now() > A_Master_Catalog.special_to_date,
                    A_Master_Catalog.cost_oms,
                    if(now() < A_Master_Catalog.special_from_date,
                        A_Master_Catalog.cost_oms,
                        A_Master_Catalog.special_purchase_price))),
            0) as 'costo_actual' FROM
    development_co_project.A_Master_Catalog
group by A_Master_Catalog.sku_simple;

