USE development_co_project;

SELECT 'Skus_live START:', NOW();

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'development_co_project.tbl_skus_live',
  'start',
  NOW(),
  MAX(date),
  count(*),
  count(*)
FROM
  development_co_project.tbl_skus_live
;

SELECT @v_maxDate:=MAX(date)
FROM tbl_skus_live;

#truncate table tbl_skus_live;

DELETE FROM tbl_skus_live WHERE date=@v_maxDate;

#tomar is_market_place de tbl_purchase_order a nivel de sku
insert into tbl_skus_live( yrmonth, week, date, n1, n2,n3,buyer, 
sku_config, visible, brand, supplier, sku_simple, is_marketplace, id_supplier)
select yrmonth,weekofyear(date) as week, date, cat1 n1, cat2 n2 , cat3 n3, t.buyer, 
c.sku_config, c.visible, t.Brand as brand, supplier, c.sku_simple,t.ismarketplace, t.id_supplier
from development_co_project.A_Master_Catalog t inner join catalog_history c on t.sku_simple = c.sku_simple
WHERE date>= @v_maxDate;

#update tbl_skus_live
# set buyer = trim(both '	' from replace(replace(replace(buyer, '\t', ''), '\n',''),'\r',''));

/*UPDATE bazayaco.tbl_skus_live t INNER JOIN bazayaco.tbl_catalog_product_v2 c 
ON t.sku_config = c.sku_config SET t.buyer = trim(substring_index(c.buyer,'-',-1)) 
WHERE c.buyer is not null and c.buyer <>'';*/

UPDATE tbl_skus_live slive
JOIN (SELECT a.date, a.sku_simple, MAX(IFNULL(b.is_option_marketplace,0)) AS is_marketplace
FROM tbl_skus_live a
JOIN bob_live_co.catalog_simple b
ON a.sku_simple=b.sku
WHERE a.date>=@v_maxDate
GROUP BY a.date, a.sku_simple) mkp
ON slive.date=mkp.date AND slive.sku_simple=mkp.sku_simple
SET slive.is_marketplace=mkp.is_marketplace
WHERE slive.date>=@v_maxDate;

#Excepciones de marketplace
update tbl_skus_live s
inner join tbl_exc_marketplace m
on s.sku_config = m.sku_config
set s.is_marketplace = 1;

update tbl_skus_live s
set is_marketplace = 0
where supplier like 'CINE%'; 

update tbl_skus_live set is_marketplace = 0 where is_marketplace is null;

update tbl_skus_live set yrmonth = date_format(date, '%Y%m') where yrmonth is null;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'development_co_project.tbl_skus_live',
  'finish',
  NOW(),
  MAX(date),
  count(*),
  count(*)
FROM
  development_co_project.tbl_skus_live
;

SELECT 'Skus_live END:', NOW();