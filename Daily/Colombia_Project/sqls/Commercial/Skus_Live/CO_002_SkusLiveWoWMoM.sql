USE development_co_project;

SELECT @c_date:=CURDATE() - INTERVAL 1 DAY
FROM dual;

#Datos mensuales
SELECT 'Inicio rutina SKUs Live WoW-MoM', now();
DROP TABLE IF EXISTS tmp_skus_live_mom_cur;
DROP TABLE IF EXISTS tmp_skus_live_mom_last;
DROP TABLE IF EXISTS tmp_skus_live_mom_cur2;
DROP TABLE IF EXISTS tmp_skus_live_mom_last2;

SELECT 'Creando tablas temporales mensuales', now();
#Mes actual a nivel de Config
CREATE TABLE tmp_skus_live_mom_cur AS
SELECT yrmonth
     , MONTH(date) AS mes
     , date
     , n1, n2, n3
	 , buyer
     , sku_config
     , supplier
     , brand
     , 1 AS TotalSKUs
     , MAX(visible) AS visible
     , MAX(IFNULL(is_marketplace,0)) AS marketplace
FROM tbl_skus_live
WHERE date = CURDATE() - INTERVAL 1 DAY
GROUP BY yrmonth
     , MONTH(date)
     , date
     , n1, n2, n3
	 , buyer
     , sku_config
     , supplier
     , brand;

ALTER TABLE tmp_skus_live_mom_cur ADD INDEX skuidx (sku_config);

#Mes anterior a nivel de config
CREATE TABLE tmp_skus_live_mom_last AS
SELECT yrmonth
     , MONTH(date) AS mes
     , date
     , n1, n2, n3
	 , buyer
     , sku_config
     , supplier
     , brand
     , 1 AS TotalSKUs
     , MAX(visible) AS visible
     , MAX(IFNULL(is_marketplace,0)) AS marketplace
FROM tbl_skus_live
WHERE date = (CURDATE() - INTERVAL 1 DAY) - INTERVAL 1 MONTH
GROUP BY yrmonth
     , MONTH(date)
     , date
     , n1, n2, n3
	 , buyer
     , sku_config
     , supplier
     , brand;

ALTER TABLE tmp_skus_live_mom_last ADD INDEX skuidx (sku_config);

#Mes actual en cifras de visibles
CREATE TABLE tmp_skus_live_mom_cur2 AS
SELECT c.yrmonth
      ,c.mes
      ,c.n1
      ,c.n2
      ,c.n3
      ,c.buyer
	  ,SUM(c.visible) AS Visible_currMonth
	  ,SUM(c.TotalSKUs)-SUM(c.visible) AS NonVisible_currMonth
      ,SUM(c.marketplace) AS MKP_currMonth
      ,SUM(c.TotalSKUs) AS Total_currMonth
FROM tmp_skus_live_mom_cur c
GROUP BY c.yrmonth
      ,c.mes
      ,c.n1
      ,c.n2
      ,c.n3
      ,c.buyer;

#Mes anterior en cifras de visibles
CREATE TABLE tmp_skus_live_mom_last2 AS
SELECT c.yrmonth
      ,c.mes
      ,c.n1
      ,c.n2
      ,c.n3
      ,c.buyer
	  ,SUM(c.visible) AS Visible_lastMonth
	  ,SUM(c.TotalSKUs)-SUM(c.visible) AS NonVisible_lastMonth
      ,SUM(c.marketplace) AS MKP_lastMonth
      ,SUM(c.TotalSKUs) AS Total_lastMonth
FROM tmp_skus_live_mom_last c
GROUP BY c.yrmonth
      ,c.mes
      ,c.n1
      ,c.n2
      ,c.n3
      ,c.buyer;

SELECT @v_maxDate:=MAX(fecha)
FROM tbl_skus_live_mom;

#IF (@v_maxDate=@c_date) THEN
#   SELECT 'Borrando ultimo dia de tbl_skus_live_mom', now();
DELETE FROM tbl_skus_live_mom WHERE fecha=@v_maxDate AND @v_maxDate=@c_date;
#END IF;

#Carga cifras de MoM
SELECT 'Cargando cifras mensuales', now();
INSERT INTO tbl_skus_live_mom
SELECT c.yrmonth, c.mes, CURDATE() - INTERVAL 1 DAY AS fecha, c.n1 AS new_cat1, c.n2 AS new_cat2, c.n3 AS new_cat3, c.buyer
    , c.Visible_currMonth
    , l.Visible_lastMonth
	, c.NonVisible_currMonth
	, l.NonVisible_lastMonth
    , c.MKP_currMonth
	, l.MKP_lastMonth
    , c.Total_currMonth
    , l.Total_lastMonth
    , IFNULL((c.Visible_currMonth/l.Visible_lastMonth)-1,0) AS MoM_By_Visible
    , IFNULL((c.NonVisible_currMonth/l.NonVisible_lastMonth)-1,0) AS MoM_By_NonVisible
    , IFNULL((c.Total_currMonth/l.Total_lastMonth)-1,0) AS MoM_By_Total
    , IFNULL((c.MKP_currMonth/l.MKP_lastMonth)-1,0) AS MoM_By_MKP
FROM tmp_skus_live_mom_cur2 c
JOIN tmp_skus_live_mom_last2 l
ON  c.n1=l.n1
AND c.n2=l.n2
AND c.n3=l.n3
AND c.buyer=l.buyer;

SELECT @v_maxDate:=MAX(date)
FROM tbl_skus_live_mom_new;

   SELECT 'Borrando ultimo dia de tbl_skus_live_mom_new', now();
   DELETE FROM tbl_skus_live_mom_new WHERE date=@v_maxDate and (@v_maxDate=@c_date);


#Carga nuevos SKUs
INSERT INTO tbl_skus_live_mom_new
SELECT c.yrmonth
     , c.mes
     , c.date
     , c.n1 AS new_cat1
     , c.n2 AS new_cat2
     , c.n3 AS new_cat3
     , c.buyer
     , c.sku_config
     , c.supplier
     , c.brand
     , c.TotalSKUs
     , c.visible
     , c.marketplace
     , cs.created_at
     , cc.pet_approved
     , cc.pet_approved
     , cc.pet_status
     , cs.price
     , cs.cost
     , cs.status AS simple_status
     , cc.status AS config_status
     , st.availablebob
FROM tmp_skus_live_mom_cur c
LEFT JOIN tmp_skus_live_mom_last l
ON c.sku_config=l.sku_config
JOIN bob_live_co.catalog_config cc
ON c.sku_config=cc.sku
JOIN bob_live_co.catalog_simple cs
ON cs.fk_catalog_config=cc.id_catalog_config 
JOIN tbl_catalog_product_stock st
ON st.fk_catalog_simple=cs.id_catalog_simple
WHERE l.sku_config IS NULL;

SELECT @v_maxDate:=MAX(date)
FROM tbl_skus_live_mom_inout;


   SELECT 'Borrando ultimo dia de tbl_skus_live_mom_inout', now();
   DELETE FROM tbl_skus_live_mom_inout WHERE date=@v_maxDate and (@v_maxDate=@c_date);


#Carga datos de SKUs que entra y que salen
INSERT INTO tbl_skus_live_mom_inout
SELECT c.yrmonth
     , c.mes
     , c.date
     , c.n1 AS new_cat1
     , c.n2 AS new_cat2
     , c.n3 AS new_cat3
	 , c.buyer
     , c.sku_config
     , c.supplier
     , c.brand
     , c.TotalSKUs
     , c.visible
     , c.marketplace
     , l.visible AS last_visible
     , cs.created_at
     , cc.pet_approved
     , cc.pet_approved
     , cc.pet_status
     , cs.price
     , cs.cost
     , cs.status AS simple_status
     , cc.status AS config_status
     , st.availablebob
FROM tmp_skus_live_mom_cur c
JOIN tmp_skus_live_mom_last l
ON c.sku_config=l.sku_config
JOIN bob_live_co.catalog_config cc
ON c.sku_config=cc.sku
JOIN bob_live_co.catalog_simple cs
ON cs.fk_catalog_config=cc.id_catalog_config 
JOIN tbl_catalog_product_stock st
ON st.fk_catalog_simple=cs.id_catalog_simple;

#Datos semanales
SELECT 'Borrando tablas temporales WoW', now();
DROP TABLE IF EXISTS tmp_skus_live_wow_cur;
DROP TABLE IF EXISTS tmp_skus_live_wow_last;
DROP TABLE IF EXISTS tmp_skus_live_wow_cur2;
DROP TABLE IF EXISTS tmp_skus_live_wow_last2;

#WOW first
SELECT 'Creando tablas temporales semanales', now();
CREATE TABLE tmp_skus_live_wow_cur AS
SELECT yrmonth
     , WEEKOFYEAR(date) AS semana
     , DAYNAME(date) AS dia
     , date
     , n1, n2, n3
	 , buyer
     , sku_config
     , supplier
     , brand
     , 1 AS TotalSKUs
     , MAX(visible) AS visible
     , MAX(IFNULL(is_marketplace,0)) AS marketplace
FROM tbl_skus_live
WHERE date = CURDATE() - INTERVAL 1 DAY
GROUP BY yrmonth
     , WEEKOFYEAR(date)
     , DAYNAME(date)
     , date
     , n1, n2, n3
	 , buyer
     , sku_config
     , supplier
     , brand;

ALTER TABLE tmp_skus_live_wow_cur ADD INDEX skuidx (sku_config);

CREATE TABLE tmp_skus_live_wow_last AS
SELECT yrmonth
     , WEEKOFYEAR(date) AS semana
     , DAYNAME(date) AS dia
     , date
     , n1, n2, n3
	 , buyer
     , sku_config
     , supplier
     , brand
     , 1 AS TotalSKUs
     , MAX(visible) AS visible
     , MAX(IFNULL(is_marketplace,0)) AS marketplace
FROM tbl_skus_live
WHERE date = (CURDATE() - INTERVAL 1 DAY) - INTERVAL 1 WEEK
GROUP BY yrmonth
     , WEEKOFYEAR(date)
     , DAYNAME(date)
     , date
     , n1, n2, n3
	 , buyer
     , sku_config
     , supplier
     , brand;

ALTER TABLE tmp_skus_live_wow_last ADD INDEX skuidx (sku_config);

CREATE TABLE tmp_skus_live_wow_cur2 AS
SELECT c.yrmonth
      ,c.semana
      ,c.n1
      ,c.n2
      ,c.n3
      ,c.buyer
	  ,SUM(c.visible) AS Visible_currWeek
	  ,SUM(c.TotalSKUs)-SUM(c.visible) AS NonVisible_currWeek
      ,SUM(c.marketplace) AS MKP_currWeek
      ,SUM(c.TotalSKUs) AS Total_currWeek
FROM tmp_skus_live_wow_cur c
GROUP BY c.yrmonth
      ,c.semana
      ,c.n1
      ,c.n2
      ,c.n3
      ,c.buyer;

CREATE TABLE tmp_skus_live_wow_last2 AS
SELECT c.yrmonth
      ,c.semana
      ,c.n1
      ,c.n2
      ,c.n3
      ,c.buyer
	  ,SUM(c.visible) AS Visible_lastWeek
	  ,SUM(c.TotalSKUs)-SUM(c.visible) AS NonVisible_lastWeek
      ,SUM(c.marketplace) AS MKP_lastWeek
      ,SUM(c.TotalSKUs) AS Total_lastWeek
FROM tmp_skus_live_wow_last c
GROUP BY c.yrmonth
      ,c.semana
      ,c.n1
      ,c.n2
      ,c.n3
      ,c.buyer;

SELECT @v_maxDate:=MAX(fecha)
FROM tbl_skus_live_wow;


   SELECT 'Borrando ultimo dia de tbl_skus_live_wow_new', now();
   DELETE FROM tbl_skus_live_wow WHERE fecha=@v_maxDate and (@v_maxDate=@c_date);


#Carga cifras WoW
SELECT 'Cargando cifras semanales', now();
INSERT INTO tbl_skus_live_wow 
SELECT c.yrmonth, c.semana, DAYNAME(CURDATE() - INTERVAL 1 DAY) AS dia, CURDATE() - INTERVAL 1 DAY AS fecha
    , c.n1 AS new_cat1, c.n2 AS new_cat2, c.n3 AS new_cat3, c.buyer
    , c.Visible_currWeek
    , l.Visible_lastWeek
	, c.NonVisible_currWeek
	, l.NonVisible_lastWeek
    , c.MKP_currWeek
	, l.MKP_lastWeek
    , c.Total_currWeek
    , l.Total_lastWeek
    , IFNULL((c.Visible_currWeek/l.Visible_lastWeek)-1,0) AS WoW_By_Visible
    , IFNULL((c.NonVisible_currWeek/l.NonVisible_lastWeek)-1,0) AS WoW_By_NonVisible
    , IFNULL((c.Total_currWeek/l.Total_lastWeek)-1,0) AS WoW_By_Total
    , IFNULL((c.MKP_currWeek/l.MKP_lastWeek)-1,0) AS WoW_By_MKP
FROM tmp_skus_live_wow_cur2 c
JOIN tmp_skus_live_wow_last2 l
ON  c.n1=l.n1
AND c.n2=l.n2
AND c.n3=l.n3
AND c.buyer=l.buyer;

SELECT @v_maxDate:=MAX(date)
FROM tbl_skus_live_wow_new;


   SELECT 'Borrando ultimo dia de tbl_skus_live_wow_new', now();
   DELETE FROM tbl_skus_live_wow_new WHERE date=@v_maxDate and (@v_maxDate=@c_date);


#Carga nuevos SKUs
INSERT INTO tbl_skus_live_wow_new
SELECT c.yrmonth
     , c.semana
     , c.dia
     , c.date
     , c.n1 AS new_cat1
     , c.n2 AS new_cat2
     , c.n3 AS new_cat3
     , c.buyer
     , c.sku_config
     , c.supplier
     , c.brand
     , c.TotalSKUs
     , c.visible
     , c.marketplace
     , cs.created_at
     , cc.pet_approved
     , cc.pet_approved
     , cc.pet_status
     , cs.price
     , cs.cost
     , cs.status AS simple_status
     , cc.status AS config_status
     , st.availablebob
FROM tmp_skus_live_wow_cur c
LEFT JOIN tmp_skus_live_wow_last l
ON c.sku_config=l.sku_config
JOIN bob_live_co.catalog_config cc
ON c.sku_config=cc.sku
JOIN bob_live_co.catalog_simple cs
ON cs.fk_catalog_config=cc.id_catalog_config 
JOIN tbl_catalog_product_stock st
ON st.fk_catalog_simple=cs.id_catalog_simple
WHERE l.sku_config IS NULL;

SELECT @v_maxDate:=MAX(date)
FROM tbl_skus_live_wow_inout;


   SELECT 'Borrando ultimo dia de tbl_skus_live_wow_inout', now();
   DELETE FROM tbl_skus_live_wow_inout WHERE date=@v_maxDate and (@v_maxDate=@c_date);


#Carga datos de SKUs que entra y que salen
INSERT INTO tbl_skus_live_wow_inout
SELECT c.yrmonth
     , c.semana
     , c.date
	 , c.dia
     , c.n1 AS new_cat1
     , c.n2 AS new_cat2
     , c.n3 AS new_cat3
	 , c.buyer
     , c.sku_config
     , c.supplier
     , c.brand
     , c.TotalSKUs
     , c.visible
     , c.marketplace
     , l.visible AS last_visible
     , cs.created_at
     , cc.pet_approved
     , cc.pet_approved
     , cc.pet_status
     , cs.price
     , cs.cost
     , cs.status AS simple_status
     , cc.status AS config_status
     , st.availablebob
FROM tmp_skus_live_wow_cur c
JOIN tmp_skus_live_wow_last l
ON c.sku_config=l.sku_config
JOIN bob_live_co.catalog_config cc
ON c.sku_config=cc.sku
JOIN bob_live_co.catalog_simple cs
ON cs.fk_catalog_config=cc.id_catalog_config 
JOIN tbl_catalog_product_stock st
ON st.fk_catalog_simple=cs.id_catalog_simple;

#Datos diarios
SELECT 'Borrando tablas temporales DoD', now();
DROP TABLE IF EXISTS tmp_skus_live_dod_cur;
DROP TABLE IF EXISTS tmp_skus_live_dod_last;
DROP TABLE IF EXISTS tmp_skus_live_dod_cur2;
DROP TABLE IF EXISTS tmp_skus_live_dod_last2;

#DOD first
SELECT 'Creando tablas temporales diarias', now();
CREATE TABLE tmp_skus_live_dod_cur AS
SELECT yrmonth
     , WEEKOFYEAR(date) AS semana
     , DAYNAME(date) AS dia
     , date
     , n1, n2, n3
	 , buyer
     , sku_config
     , supplier
     , brand
     , 1 AS TotalSKUs
     , MAX(visible) AS visible
     , MAX(IFNULL(is_marketplace,0)) AS marketplace
FROM tbl_skus_live
WHERE date = CURDATE() - INTERVAL 1 DAY
GROUP BY yrmonth
     , WEEKOFYEAR(date)
     , DAYNAME(date)
     , date
     , n1, n2, n3
	 , buyer
     , sku_config
     , supplier
     , brand;

ALTER TABLE tmp_skus_live_dod_cur ADD INDEX skuidx (sku_config);

CREATE TABLE tmp_skus_live_dod_last AS
SELECT yrmonth
     , WEEKOFYEAR(date) AS semana
     , DAYNAME(date) AS dia
     , date
     , n1, n2, n3
	 , buyer
     , sku_config
     , supplier
     , brand
     , 1 AS TotalSKUs
     , MAX(visible) AS visible
     , MAX(IFNULL(is_marketplace,0)) AS marketplace
FROM tbl_skus_live
WHERE date = CURDATE() - INTERVAL 2 DAY
GROUP BY yrmonth
     , WEEKOFYEAR(date)
     , DAYNAME(date)
     , date
     , n1, n2, n3
	 , buyer
     , sku_config
     , supplier
     , brand;

ALTER TABLE tmp_skus_live_dod_last ADD INDEX skuidx (sku_config);

CREATE TABLE tmp_skus_live_dod_cur2 AS
SELECT c.yrmonth
	  ,c.semana
      ,c.date
      ,c.n1
      ,c.n2
      ,c.n3
      ,c.buyer
	  ,SUM(c.visible) AS Visible_currDay
	  ,SUM(c.TotalSKUs)-SUM(c.visible) AS NonVisible_currDay
      ,SUM(c.marketplace) AS MKP_currDay
      ,SUM(c.TotalSKUs) AS Total_currDay
FROM tmp_skus_live_dod_cur c
GROUP BY c.yrmonth
      ,c.date
      ,c.n1
      ,c.n2
      ,c.n3
      ,c.buyer;

CREATE TABLE tmp_skus_live_dod_last2 AS
SELECT c.yrmonth
      ,c.semana
      ,c.date
      ,c.n1
      ,c.n2
      ,c.n3
      ,c.buyer
	  ,SUM(c.visible) AS Visible_lastDay
	  ,SUM(c.TotalSKUs)-SUM(c.visible) AS NonVisible_lastDay
      ,SUM(c.marketplace) AS MKP_lastDay
      ,SUM(c.TotalSKUs) AS Total_lastDay
FROM tmp_skus_live_dod_last c
GROUP BY c.yrmonth
      ,c.date
      ,c.n1
      ,c.n2
      ,c.n3
      ,c.buyer;

SELECT @v_maxDate:=MAX(fecha)
FROM tbl_skus_live_dod;


   SELECT 'Borrando ultimo dia de tbl_skus_live_dod', now();
   DELETE FROM tbl_skus_live_dod WHERE fecha=@v_maxDate and (@v_maxDate=@c_date);


#Carga cifras DOD
SELECT 'Cargando cifras diarias', now();
INSERT INTO tbl_skus_live_dod
SELECT c.yrmonth, c.semana, DAYNAME(CURDATE() - INTERVAL 1 DAY) AS dia, CURDATE() - INTERVAL 1 DAY AS fecha
    , c.n1 AS new_cat1, c.n2 AS new_cat2, c.n3 AS new_cat3, c.buyer
    , c.Visible_currDay
    , l.Visible_lastDay
	, c.NonVisible_currDay
	, l.NonVisible_lastDay
    , c.MKP_currDay
	, l.MKP_lastDay
    , c.Total_currDay
    , l.Total_lastDay
    , IFNULL((c.Visible_currDay/l.Visible_lastDay)-1,0) AS DoD_By_Visible
    , IFNULL((c.NonVisible_currDay/l.NonVisible_lastDay)-1,0) AS DoD_By_NonVisible
    , IFNULL((c.Total_currDay/l.Total_lastDay)-1,0) AS DoD_By_Total
    , IFNULL((c.MKP_currDay/l.MKP_lastDay)-1,0) AS DoD_By_MKP
FROM tmp_skus_live_dod_cur2 c
JOIN tmp_skus_live_dod_last2 l
ON  c.n1=l.n1
AND c.n2=l.n2
AND c.n3=l.n3
AND c.buyer=l.buyer;

SELECT @v_maxDate:=MAX(date)
FROM tbl_skus_live_dod_new;


   SELECT 'Borrando ultimo dia de tbl_skus_live_dod_new', now();
   DELETE FROM tbl_skus_live_dod_new WHERE date=@v_maxDate and (@v_maxDate=@c_date) ;


#Carga nuevos SKUs
INSERT INTO tbl_skus_live_dod_new
SELECT c.yrmonth
     , c.semana
     , c.dia
     , c.date
     , c.n1 AS new_cat1
     , c.n2 AS new_cat2
     , c.n3 AS new_cat3
     , c.buyer
     , c.sku_config
     , c.supplier
     , c.brand
     , c.TotalSKUs
     , c.visible
     , c.marketplace
     , cs.created_at
     , cc.pet_approved
     , cc.pet_approved
     , cc.pet_status
     , cs.price
     , cs.cost
     , cs.status AS simple_status
     , cc.status AS config_status
     , st.availablebob
FROM tmp_skus_live_dod_cur c
LEFT JOIN tmp_skus_live_dod_last l
ON c.sku_config=l.sku_config
JOIN bob_live_co.catalog_config cc
ON c.sku_config=cc.sku
JOIN bob_live_co.catalog_simple cs
ON cs.fk_catalog_config=cc.id_catalog_config 
JOIN tbl_catalog_product_stock st
ON st.fk_catalog_simple=cs.id_catalog_simple
WHERE l.sku_config IS NULL;

SELECT @v_maxDate:=MAX(date)
FROM tbl_skus_live_dod_inout;


   SELECT 'Borrando ultimo dia de tbl_skus_live_dod_inout', now();
   DELETE FROM tbl_skus_live_dod_inout WHERE date=@v_maxDate and (@v_maxDate=@c_date);

#Carga datos de SKUs que entra y que salen
INSERT INTO tbl_skus_live_dod_inout
SELECT c.yrmonth
     , c.semana
     , c.dia
     , c.date
     , c.n1 AS new_cat1
     , c.n2 AS new_cat2
     , c.n3 AS new_cat3
	 , c.buyer
     , c.sku_config
     , c.supplier
     , c.brand
     , c.TotalSKUs
     , c.visible
     , c.marketplace
     , l.visible AS last_visible
     , cs.created_at
     , cc.pet_approved
     , cc.pet_approved
     , cc.pet_status
     , cs.price
     , cs.cost
     , cs.status AS simple_status
     , cc.status AS config_status
     , st.availablebob
FROM tmp_skus_live_dod_cur c
JOIN tmp_skus_live_dod_last l
ON c.sku_config=l.sku_config
JOIN bob_live_co.catalog_config cc
ON c.sku_config=cc.sku
JOIN bob_live_co.catalog_simple cs
ON cs.fk_catalog_config=cc.id_catalog_config 
JOIN tbl_catalog_product_stock st
ON st.fk_catalog_simple=cs.id_catalog_simple;

SELECT 'FIN RUTINA skus_live_wow_mom: OK', now();