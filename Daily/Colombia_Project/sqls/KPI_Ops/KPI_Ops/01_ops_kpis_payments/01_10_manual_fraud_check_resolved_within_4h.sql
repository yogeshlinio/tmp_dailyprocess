/*-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 01_10_manual_fraud_check_resolved_within_4h
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'1. Payments' AS 'group',
	0110 AS id,
	DATE_FORMAT(DateOrderPlaced,'%Y%m') AS MonthNum,
	@operations@.week_iso(DateOrderPlaced) AS WeekNum,
	Date(DateOrderPlaced) AS date,
	'% Manual Fraud Check Resolved within 4 hrs - Credit/Debit Cards' AS kpi,
	'Manual Fraud Checked <= 4 hrs / # Exportable' AS formula,
	PaymentMethod AS breakdown_1,
	'' AS breakdown_2,
	Count(*) AS items,
	SUM(IF(TimeToResolveManual <=4,1,0)) AS items_sec,
	SUM(IF(TimeToResolveManual <=4,1,0))/Count(*) AS rate_resolved_manual_under_4h,
	now() AS updated_t 
FROM customer_service.tbl_fraud_check_report
WHERE DateOrderPlaced BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND PaymentMethod IN (	'Amex_Gateway',
						'Banorte_Payworks',
						'Banorte_Payworks_Debit')
GROUP BY Date(DateOrderPlaced), PaymentMethod;*/