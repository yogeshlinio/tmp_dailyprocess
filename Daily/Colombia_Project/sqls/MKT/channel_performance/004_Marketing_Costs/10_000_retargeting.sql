USE marketing_co;

SET @country := 'col';


USE @marketing@;

SET @country :='@v_country@';


#Vizury
update channel_performance
set marketing_cost=0
where channel='vizury';

drop temporary table if exists aux_vizury;
create temporary table aux_vizury (index(date,campaign))
select sum(rev*orderbeforecan*(1-cr.canc_ratio)-ShippingCost)*.08 cost, a.date, a.country  ,ve.ga as campaign
from @development@.A_Master a
inner join cancellation_ratio cr
on a.date=cr.date
inner join
marketing_report.vizury_report b
on a.ordernum=b.order_id
inner join marketing_report.vizury_equivalence ve
on ve.vizury=b.campaign 
where a.country=substring(@country,1,3)
group by a.date, b.campaign; 

drop temporary table if exists aux_cp;
create temporary table aux_cp (index(date),index(country))
select sum(gross_revenue) as total_gr, date ,country, count(1) cuantos, yrmonth, campaign
from channel_performance 
where country=substring(@country,1,3) and channel='Vizury' group by date, campaign;


update channel_performance g	
inner join
aux_cp gr
using(date, campaign)
inner join  
aux_vizury t
using(date, campaign)
set g.marketing_cost=t.cost/gr.cuantos
where g.channel='Vizury'
;


	
#sociomantic
update channel_performance
set marketing_cost=0
where channel='Sociomantic';

drop temporary table if exists aux_socio;
create temporary table aux_socio (unique key(date,campaign))
select sum(a.cost) cost, a.date, a.country, b.campaign_ga as campaign
from marketing_report.sociomantic_copy a
inner join marketing.retargeting_campaigns b
on a.campaign=b.campaign_plataforma
where  a.country=substring(@country,1,3)
group by a.date, a.country, campaign;



drop temporary table if exists aux_cp;
create temporary table aux_cp (unique key(date, campaign))
select sum(gross_revenue) as total_gr, country, date, count(1) cuantos, campaign
from channel_performance 
where channel='Sociomantic' 
group by date, country, campaign;


update channel_performance g	
inner join
aux_socio t
using(date,country,campaign)
inner join 
aux_cp gr
using(date,country,campaign)
inner join
development_mx.A_E_BI_ExchangeRate er 
on date_format(t.date,'%Y%m')=er.month_num and gr.country=t.country
set g.marketing_cost=t.cost/gr.cuantos*er.xr
where g.channel='Sociomantic'
and er.country=t.country;

	
#Triggit	
update channel_performance
set marketing_cost=0
where channel='triggit';

drop temporary table if exists aux_triggit;
create temporary table aux_triggit (index(date))
select sum(clicks)*cost_click cost, report_start_date as date, targeting_group_description as campaign
from marketing_report.triggit_@v_countryPrefix@
inner join 
marketing_report.costo_click_triggit cct
on cct.country=substring(@country,1,3)
group by date, campaign;

drop temporary table if exists aux_cp;
create temporary table aux_cp (index(date),index(country))
select sum(gross_revenue) as total_gr, country, date, count(1) cuantos, campaign
from channel_performance where channel='Triggit' group by date, country, campaign;

update channel_performance g	
inner join
aux_cp gr
using(date, country, campaign)
inner join  
aux_triggit t
using(date,campaign)
inner join
development_mx.A_E_BI_ExchangeRate_USD er 
on date_format(t.date,'%Y%m')=er.month_num 
set g.marketing_cost=(t.cost/gr.cuantos)*er.xr
where g.channel='Triggit'
and er.country=gr.country 
;



#Criteo
update channel_performance
set marketing_cost=0
where channel='Criteo';

drop temporary table if exists aux_criteo;
create temporary table aux_criteo (index(date))
select sum(cost) cost, date, b.ga as campaign
from marketing_report.criteo a
inner join marketing_report.criteo_equivalence b
on a.campaign=b.criteo
group by date,campaign;



drop temporary table if exists aux_cp;
create temporary table aux_cp (index(date),index(country))
select sum(gross_revenue) as total_gr, country, date, count(1) cuantos, campaign
from channel_performance where channel='Criteo' group by date, campaign;

update channel_performance g	
inner join
aux_cp gr
using(country,date,campaign)
inner join  
aux_criteo t
using(date, campaign)
inner join
development_mx.A_E_BI_ExchangeRate_USD er 
on date_format(t.date,'%Y%m')=er.month_num
set g.marketing_cost=t.cost*if(gr.total_gr=0,1/gr.cuantos,g.gross_revenue/gr.total_gr)*er.xr
where g.channel='Criteo';
