	USE marketing_co;

	update channel_report c
	join marketing.channel
	join marketing.channel_group
	set c.fk_channel_group = channel.fk_channel_group,
			c.channel_group = channel_group.channel_group
	where c.fk_channel = channel.id_channel
	and channel_group.id_channel_group = channel.fk_channel_group
	and channel.id_channel = channel.id_channel;