USE development_co_project;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'channel_group_anterior',
  'start',
  NOW(),
  MAX(date),
  count(*),
  count(*)
FROM
    marketing_report.channel_report_co
;

#Tabla temporal registros actuales
set @rowactual:=0;
DROP TABLE IF EXISTS tmp_tbl_order_detail_actual;
CREATE TABLE tmp_tbl_order_detail_actual AS
SELECT @rowactual:=@rowactual+1 AS row_actual, o.*
FROM 
(Select * FROM A_Master Group by CustomerNum asc, OrderNum asc) o
WHERE CustomerNum > 0 AND CustomerNum IN(Select distinct CustomerNum 
From A_Master Where date>=curdate() -interval 1 day);

#Tabla temporal registros anteriores
set @rowanterior:=1;
DROP TABLE IF EXISTS tmp_tbl_order_detail_anterior;
CREATE TABLE tmp_tbl_order_detail_anterior AS
SELECT @rowanterior:=@rowanterior+1 AS row_anterior, a.*
FROM (Select * FROM A_Master Group by CustomerNum asc, OrderNum asc) a
WHERE CustomerNum > 0 
AND CustomerNum IN
(Select distinct CustomerNum From A_Master Where date>=curdate() -interval 1 day);

#Tabla temporal para unir group y group anterior
DROP TABLE IF EXISTS tmp_tbl_order_detail_union;
CREATE TABLE tmp_tbl_order_detail_union AS
SELECT  o.`CustomerNum`, o.`OrderNum`, 
If(a.CustomerNum = o.CustomerNum, a.`ChannelGroup`, null) as channel_group_anterior
from tmp_tbl_order_detail_actual o
Inner Join tmp_tbl_order_detail_anterior a
On o.row_actual = a.row_anterior;

UPDATE  marketing_report.channel_report_co AS b
INNER JOIN tmp_tbl_order_detail_union o 
ON b.OrderNum = o.OrderNum
SET b.channel_group_anterior = o.channel_group_anterior;


DROP TABLE IF EXISTS tmp_tbl_order_detail_actual;
DROP TABLE IF EXISTS tmp_tbl_order_detail_anterior;
DROP TABLE IF EXISTS tmp_tbl_order_detail_union;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'channel_group_anterior',
  'finish',
  NOW(),
  MAX(date),
  count(*),
  count(*)
FROM
  marketing_report.channel_report_co
;



INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'channel report',
  'finish',
  NOW(),
  MAX(date),
  count(*),
  count(*)
FROM
marketing_report.channel_report_co
;


