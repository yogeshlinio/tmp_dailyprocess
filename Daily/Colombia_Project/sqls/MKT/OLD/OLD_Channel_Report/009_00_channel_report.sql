use marketing_report;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'channel report',
  'start',
  NOW(),
  MAX(date),
  count(*),
  count(*)
FROM
channel_report_co
;

UPDATE channel_report_co
        INNER JOIN
    SEM.transaction_id_co ON transaction_id = OrderNum 
SET 
    channel_report_co.campaign = transaction_id_co.campaign,
    channel_report_co.source_medium = concat(transaction_id_co.source,
            ' / ',
            transaction_id_co.medium)
WHERE
    channel_report_co.source_medium is null;
	
	UPDATE channel_report_co
        INNER JOIN
    SEM.transaction_id_fashion_co ON transaction_id = OrderNum 
SET 
    channel_report_co.campaign = transaction_id_fashion_co.campaign,
    channel_report_co.source_medium = concat(transaction_id_fashion_co.source,
            ' / ',
            transaction_id_fashion_co.medium)
WHERE
    channel_report_co.source_medium is null;

update channel_report_co 
set 
    source = ''
where source is null;
 
update channel_report_co 
set 
    source = coupon_code
where
    coupon_code is not null;

update channel_report_co 
set 
    source = source_medium
where
        source = ''
	and source_medium is not null;

update channel_report_co 
set 
    source = Source_medium
where
    channel_group = 'Non identified' and source_medium is not null;

update channel_report_co 
set 
    channel = 'New Register'
where
    (source like 'NL%' or source like 'NR%')
        and source_medium is null
		and coupon_name like 'NEWSLETTER 10000%';


update channel_report_co 
set 
    source = source_medium
where
    (coupon_code like 'NL%'
        or coupon_code like 'NR%'
        or coupon_code = 'SINIVA'
        or coupon_code like 'COM%'
        or coupon_code like 'NUEVA%'
        or coupon_code like 'CASA%'
        or coupon_code like 'PAG%'
        or coupon_code = '100COMBO-AR238EL90VPTLACOL-15x'
        or coupon_code like 'CERO%'
        or coupon_code like 'MADRID%'
        or coupon_code like '%COMBO-%'
        or coupon_code like 'shilher-esmaltescombo%'
        or coupon_code like 'dev%'
        OR coupon_code like 'VUELVEDINERO%'
        or coupon_code like 'DINEROVUELVE%'
        or coupon_code like 'HAVA01'
        or coupon_code like 'CASHBACK%'
		or coupon_code like 'REO%'
		or coupon_code like 'REP%'
		or coupon_code like 'INV%'
		or coupon_code like 'ER%'
		or coupon_code like 'GAR%'
		or coupon_code like 'CAN%'
		or coupon_code like 'INV%')
        and source_medium is not null;



update channel_report_co 
set 
    channel = 'Referral Sites'
where
    source like '%referral'
        and (source not like 'google%'
        and source not like 'linio%'
        and source not like 'facebook%'
        or source <> 'linio.com.co / referral'
        and source <> 'facebook / retargeting');



update channel_report_co 
set 
    channel = 'Buscape'
where
    source like '%buscape%';

update channel_report_co 
set 
    channel = 'Trade Tracker'
where
    source like '%tradetracker / Affiliates%';

update channel_report_co 
set 
    channel = 'Pampa Network'
where
    source like 'pampa%';
update channel_report_co 
set 
    channel = 'Curebit'
where
    coupon_code like 'cbit%'
        and source_medium like '%curebit%';

UPDATE channel_report_co 
SET 
    channel = 'Triggit'
where
    source like '%triggit / retargeting%';

update channel_report_co 
set 
    channel = 'SEM Branded'
where
    source = 'google / cpc'
        and campaign like '%linio%';
update channel_report_co 
set 
    channel = 'SEM (unbranded)'
where
    source = 'google / cpc'
        and campaign not like '%linio%'
        and (campaign not like 'er.%'
        and campaign not like '[D%'
        and campaign not like 'r.%');

update channel_report_co 
set 
    channel = 'Google Display Network'
where
    source = 'google / cpc'
        and (campaign like 'er.%'
        or campaign like '[D%'
        or campaign like 'r.%');

update channel_report_co 
set 
    channel = 'Bing'
where
    source = 'bing / cpc';

update channel_report_co 
set 
    channel = 'Exo'
where
    source like 'exo%';

update channel_report_co 
set 
    channel = 'ICCK'
where
   source not like 'exo%'
        and (source like '%display' or source like 'ICCK%'
				or source like '%Blu%radio%' or source like '%El%Espectador%'
				or source like '%Shock%'
				or source in (1549193,
								1546958,
								1546959,
								1546960,
								1546956,
								1548963,
								1546961,
								1546861
								)
			);
				
update channel_report_co 
set 
    channel = 'MSN'
where
    source like 'MSN%Display';

update channel_report_co 
set 
    channel = 'Sociomantic'
where
    source like 'socioma%'
        or source = 'facebook / retargeting';
update channel_report_co 
set 
    channel = 'Vizury'
where
    source like '%vizury%';
update channel_report_co 
set 
    channel = 'VEInteractive'
where
    source like '%VEInteractive%';

update channel_report_co 
set 
    channel = 'Mercado Libre - Alamaula'
where
    source = '%mercado%referral%'
        or source like 'ML%';
update channel_report_co 
set 
    channel = 'Alamaula'
where
    source like 'alamaula%'
        or source like 'ALA%';



update channel_report_co 
set 
    channel = 'Twitter'
where
    source = 't.co / referral'
        or source = 'socialmedia / smtweet'
        or source like 'Twitter%'
        or source like 'TW%';

update channel_report_co 
set 
    channel = 'Linio Fan Page'
where
    (source = 'FASHION25'
        or source like '%facebook%'
        or source like '%LINIOFB%'
        or (source like 'FB%'
        and source not like 'FBCAC%')
        or source = 'faebook / socialmedia')
        and (source <> 'facebook / socialmediaads'
        and source <> 'facebook / retargeting');


update channel_report_co 
set 
    channel = 'Fashion Fan Page'
where
    source like 'FBF%'
        OR source like '%LINIOFF%';

update channel_report_co 
set 
    channel = 'Fashion Fan Page'
where
    coupon_code like 'HAVA01'
        and yrmonth = 201307;

update channel_report_co 
set 
    channel = 'Linio Fan Page'
where
    source = 'facebook / socialmedia'
        and ((campaign not like '%fashion%'
        and yrmonth = 201306)
        and campaign is not null);

update channel_report_co 
set 
    channel = 'Facebook Ads'
where
    source = 'facebook / socialmediaads';		

update channel_report_co 
set 
    channel = 'FB Dark Post'
where
    source = 'facebook / socialmediaads'
        and campaign like '%darkpost%';

update channel_report_co 
set 
    channel = 'Fashion Fan Page'
where
    source = 'facebook / socialmedia_fashion';
	
update channel_report_co 
set 
    channel = 'FBF Dark Post'
where
    source = 'facebook / socialmedia_fashion'
        and campaign like '%darkpost%';


update channel_report_co 
set 
    channel = 'Fashion Fan Page'
where
    source = 'facebook / socialmedia'
        and campaign like '%fashion%'
        and yrmonth <= 201307;

update channel_report_co 
set 
    channel = 'Linio Fan Page'
where
    source = 'facebook / socialmedia'
        and yrmonth >= 201308;

update channel_report_co 
set 
    channel = 'Alejandra Arce Voucher'
where
    source like 'voucher5%'
        or source like 'voucher2%';
update channel_report_co 
set 
    channel = 'FB Ads CAC Mayo'
where
    source like 'CACFA04%';
update channel_report_co 
set 
    channel = 'FB Ads CAC Junio'
where
    source like 'CACFA05%';

update channel_report_co 
set 
    channel = 'FB Ads CAC'
where
    source like 'CACADS%';

update channel_report_co 
set 
    channel = 'Facebook Retargeting'
where
    source = 'facebook / retargeting';
update channel_report_co 
set 
    channel = 'Criteo'
where
    source = 'criteo / retargeting'
        or source = 'criteodisp / retargeting'
        or source = 'criteofbx / retargeting'
		or source like 'criteo%';
update channel_report_co 
set 
    channel = 'My Things'
where
    source = 'mythings / retargeting';
update channel_report_co 
set 
    channel = 'Main ADV'
where
    source like '%mainadv%';

update channel_report_co 
set 
    channel = 'Search Organic'
where
    source like '%organic'
        or source = 'google.com.co / referral';

update channel_report_co 
set 
    channel = 'Newsletter'
where
    (source = 'Postal / CRM'
        or source = 'Email Marketing / CRM'
        or source = 'EMM / CRM'
        OR source like '%LINIONL%'
        or source = 'Postal / (not set)')
        or (source like '%CRM%'
        and source not like 'VEInteractive%')
        or (source like 'LOCO%'
        and (source is not null
        and source not like '%blog%'));
		
update channel_report_co
set
	channel = 'Newsletter'
where channel_group = 'Non identified' 
	and coupon_code_description like 'CRM%';


update channel_report_co 
set 
    Channel = 'Voucher Navidad'
where
    source like 'NAVIDAD%'
        or source like 'LINIONAVIDAD%';
update channel_report_co 
set 
    Channel = 'Voucher Fin de Año'
where
    source = 'FINDEAÑO31';
update channel_report_co 
set 
    Channel = 'Bandeja CAC'
where
    source = 'BANDEJA0203';
update channel_report_co 
set 
    Channel = 'Tapete CAC'
where
    source like 'TAPETE%';
update channel_report_co 
set 
    Channel = 'Perfumes CAC'
where
    source like 'PERFUME%';
update channel_report_co 
set 
    Channel = 'VoucherBebes-Feb'
where
    source like 'BEBES%';
update channel_report_co 
set 
    channel = 'Sartenes CAC'
where
    source like 'SARTENES%';
update channel_report_co 
set 
    channel = 'Estufa CAC'
where
    source like 'ESTUFA%';
update channel_report_co 
set 
    channel = 'Vuelve CAC'
where
    source like 'VUELVE%'
        and (coupon_code not like 'VUELVEDINERO');
update channel_report_co 
set 
    channel = 'Memoria CAC'
where
    source like 'MEMORIA%';
update channel_report_co 
set 
    channel = 'Audifonos CAC'
where
    source like 'AUDIFONO%';
update channel_report_co 
set 
    channel = 'CACs Mayo'
where
    source like 'CAC05%'
        or source = 'CAC0420'
        or (source = 'Postal / CRM'
        and campaign like '%cac%'
        and yrmonth = 201305);
update channel_report_co 
set 
    channel = 'CACs Abril'
where
    (source like 'CAC04%'
        and source <> 'CAC0420')
        or (source = 'Postal / CRM'
        and campaign like '%cac%'
        and yrmonth = 201304);
update channel_report_co 
set 
    channel = 'CACs Marzo'
where
    source like 'CAC03%'
        or (source = 'Postal / CRM'
        and campaign like '%cac%'
        and yrmonth = 201303);
update channel_report_co 
set 
    channel = 'CACs Junio'
where
    source like 'CAC06%'
        or (source = 'Postal / CRM'
        and campaign like '%cac%'
        and yrmonth = 201306);
update channel_report_co 
set 
    channel = 'CACs Julio'
where
    source like 'CAC07%'
        or (source = 'Postal / CRM'
        and campaign like '%cac%'
        and yrmonth = 201307);
update channel_report_co 
set 
    channel = 'Reactivación Mayo'
where
    source = 'Regalo50'
        or source = 'Regalo40'
        or source = 'Regalo60'
        or source like 'GRITA%'
        or Source = 'regalo30'
        or source LIKE 'aqui%';
update channel_report_co 
set 
    channel = 'Fashion'
where
    source like 'MODA%'
        or source like 'ESTILO%';

update channel_report_co 
set 
    channel = 'Venir Reactivation'
where
    source like 'VENIR%';
update channel_report_co 
set 
    channel = 'Regresa Reactivation'
where
    source like 'REGRESA%'
        and (source <> 'Regresas'
        and source <> 'Regresas49'
        and source <> 'Regresa49');
update channel_report_co 
set 
    channel = 'Bienvenido CAC'
where
    source like 'BIENVENIDO%';
update channel_report_co 
set 
    channel = 'Amigo CAC'
where
    source like 'AMIGO%';
update channel_report_co 
set 
    channel = 'Tiramos la Casa por la ventana'
where
    source like 'CASA%'
        and (channel = '' or channel is null);
update channel_report_co 
set 
    channel = 'Reactivación Junio'
where
    source = 'LOCURA50'
        or source = 'LOCURA040'
        or source = 'LOCURA40'
        or source = 'LOCURA60'
        or (source = 'Postal / CRM'
        and campaign like '%reactivation%'
        and yrmonth = 201307)
        or (coupon_code_description like 'CRM%'
        and coupon_code_description not like '%CAC%'
        and yrmonth = 201307
        and coupon_code = source);

update channel_report_co 
set 
    channel = 'Reactivación Julio'
where
    (source = 'Postal / CRM'
        and campaign like '%reactivation%'
        and yrmonth = 201307)
        or (coupon_code_description like 'CRM%'
        and coupon_code_description not like '%CAC%'
        and yrmonth = 201308
        and coupon_code = source);

update channel_report_co 
set 
    channel = 'Happy Birthday'
where
    coupon_code like 'HB%';

update channel_report_co 
set 
    channel = 'Newsletter'
where
    coupon_code in ('CAC08100' , 'CAC0850', 'CAC0820');

update channel_report_co 
set 
    channel = 'NL CAC'
where
    (coupon_code like 'CAC%'
        or coupon_code like 'CACNL%') and (source_medium = 'Postal / CRM' 
		or source_medium is null);
		
update channel_report_co
set channel = 'Newsletter'
where coupon_code = source and source_medium = 'Postal / CRM';

update channel_report_co 
set 
    channel = 'FB CAC'
where
    (coupon_code like 'CACFB%')
		and (source_medium = 'facebook / socialmedia' or source_medium is null);

update channel_report_co 
set 
    channel = 'FBF CAC'
where
    (coupon_code like 'CACFF%')
	and (source_medium = 'facebook / socialmedia_fashion' or source_medium is null);


UPDATE channel_report_co 
SET 
    channel = 'Other Branded'
WHERE
    source like 'blog%'
        or OrderNum = '200133986'
        or OrderNum = '200882196'
        or source = 'BL%'
        or source like 'madres%'
        or source = 'mujer.linio.com.co / referral'
        or source like 'lector%';

update channel_report_co 
set 
    channel = 'Reactivation Letters'
where
    (source like 'REACT%'
        or source like 'CLIENTVIP%'
        or source like 'LINIOVIP%'
        or source = 'VUELVE'
        or source like 'CLIENTEVIP%')
        and source <> 'CLIENTEVIP1';
update channel_report_co 
set 
    channel = 'Customer Adquisition Letters'
where
    source like 'CONOCE%'
        or source = 'CLIENTEVIP1';
update channel_report_co 
set 
    channel = 'Internal Sells'
where
    source like 'INTER%';
update channel_report_co 
set 
    Channel = 'Employee Vouchers'
where
    source like 'OCT%' or source like 'SEP%'
        or source like 'EMP%'
        or (source like 'CS%'
        and (channel is null or channel = ''));
update channel_report_co 
set 
    channel = 'Parcel Vouchers'
where
    source like 'DESCUENTO%'
        or source = 'LINIO0KO'
        or source = 'LINIOiju';
update channel_report_co 
set 
    channel = 'New Register'
where
    source like 'NL%' or source like 'NR%';
update channel_report_co 
set 
    channel = 'Disculpas'
where
    source like 'H1yJvf'
        or source like 'H87h7C'
        or source = 'KI025EL78LLXLACOL-25324'
        or source = 'KI025EL85GRQLACOL-4516';
update channel_report_co 
set 
    channel = 'Suppliers Email'
where
    source = 'suppliers_mail / email';
update channel_report_co 
set 
    channel = 'Out Of Stock'
where
    source like 'NST%' or source = 'N0Ctor';
update channel_report_co 
set 
    channel = 'Devoluciones'
where
    source like 'DEV%';
update channel_report_co 
set 
    channel = 'Broken Vouchers'
where
    source like 'HVE%';
update channel_report_co 
set 
    channel = 'Broken Discount'
where
    source like 'KER%' or source = 'D1UqWU';
update channel_report_co 
set 
    channel = 'Voucher Empleados'
where
    source like 'EMPLEADO%';
update channel_report_co 
set 
    channel = 'Gift Cards'
where
    source like 'GC%';
update channel_report_co 
set 
    channel = 'Friends and Family'
where
    source like 'FF%';
update channel_report_co 
set 
    channel = 'Leads'
where
    source like 'LC%';
update channel_report_co 
set 
    channel = 'Nueva Página'
where
    source like 'NUEVA%'
        and (channel = '' or channel is null);
update channel_report_co 
set 
    channel = 'Promesa Linio'
where
    source like 'PROMESA%';
update channel_report_co 
set 
    channel = 'Creating Happiness'
where
    source like 'FELIZ%'
        or source like 'MUFELIZ%';
update channel_report_co 
set 
    channel = 'NO IVA Abril'
where
    source like 'PAG%'
        or SOURCE like 'CERO%';
update channel_report_co 
set 
    channel = 'Invitados VIP Círculo de la moda'
where
    source like 'CIRCULOREG%';

update channel_report_co 
set 
    channel = 'Banco Helm'
where
    source like 'HELM%';
update channel_report_co 
set 
    channel = 'Banco de Bogotá'
where
    source like 'CLIENTEBOG%'
        or source like 'MOVISTAR%'
        or source like 'BOG%'
        or source like 'BDB%'
        or source = 'BB42'
        or source = 'BB46'
        or source = 'BBLAV'
        or source = 'BBNEV'
        or source = 'PA953EL96NHFLACOL'
        or source = 'PA953EL96NHFLACOL'
        or source = 'BH859EL51WBWLACOL'
        or source = 'CU054HL58AXPLACOL'
        or source = 'SA860TB24WCXLACOL'
        or source = 'HA997TB17DLQLACOL'
        or source = 'SA860TB35WCMLACOL'
        or source = 'SA860TB37WCKLACOL'
        or source = 'BO944TB20JEPLACOL'
        or source = 'MG614TB23HTWLACOL'
        or source = 'BA186TB36HTJLACOL'
        or source = 'PR506TB63BFCLACOL'
        or source = 'MI392TB32VRBLACOL'
        or source = 'MI392TB33VRALACOL'
        or source = 'BO944TB79JCILACOL'
        or source = 'BO944TB24JELLACOL'
        or source = 'HA997TB23DLKLACOL';
update channel_report_co 
set 
    channel = 'Post BDB'
where
    source like 'POSTBD%';

update channel_report_co 
set 
    channel = 'Unknown Partnership March'
where
    source = 'LG082EL56ECLLACOL-20343'
        or SOURCE = 'SO017EL14ZQRLACOL48'
        or SOURCE = 'LG082EL97RQSLACOL48'
        or SOURCE = 'NI088EL26CJLLACOL48'
        or SOURCE = 'GE008EL90BEZLACOL48'
        or SOURCE = 'BDE862EL18WGZLACOL48'
        or SOURCE = 'LU939HL19XSMLACOL48'
        or SOURCE = 'LU939HL11XSULACOL48'
        or SOURCE = 'LU939HL03XTCLACOL48'
        or SOURCE = 'AR842EL87LLOLACOL48'
        or SOURCE = 'HO107EL04MOPLACOL48'
        or SOURCE = 'TC124EL64NBRLACOL48'
        or SOURCE = 'CO052EL34DGFLACOL48'
        or SOURCE = 'MI392EL71GRCLACOL48'
        or SOURCE = 'GU689FA67MQALACOL48'
        or SOURCE = 'TO692FA65MQCLACOL48'
        or SOURCE = 'GU689FA66MQBLACOL48'
        or SOURCE = 'FR119EL73NBILACOL48'
        or SOURCE = 'HA135EL15KYALACOL48'
        or SOURCE = 'CU054EL66KZXLACOL48'
        or SOURCE = 'IN122TB66NBPLACOL48';

update channel_report_co 
set 
    channel = 'Banco Davivienda'
where
    source like 'DAV%' or source = 'DV_AG'
        or source = 'DVPLANCHA46';
update channel_report_co 
set 
    channel = 'Banco BBVA'
where
    (source like 'BBVA%'
        and source <> 'BBVADP')
        or source = 'SA860TB32WCPLACOL'
        or source = 'SA860TB43WCELACOL'
        or source = 'BA186TB35HTKLACOL'
        or source = 'PR506TB55BFKLACOL'
        or source = 'PR506TB62BFDLACOL'
        or source = 'PA953EL97NHELACOL'
        or source = 'PO916EL07ETSLACOL'
        or source = 'PO916EL04ETVLACOL'
        or source = 'LU939HL23XSILACOL'
        or source = 'LU939HL32XRZLACOL'
        or source = 'HA997TB15DLSLACOL'
        or source = 'SA860TB35WCMLACOLACOL'
        or (source like '%BBVA%'
        and source <> 'FBBBVAS');

update channel_report_co 
set 
    channel = 'Campus Party'
where
    source like 'CP%';
update channel_report_co 
set 
    channel = 'Publimetro'
where
    source like 'PUBLIMETRO%';
update channel_report_co 
set 
    channel = 'El Espectador'
where
    source = 'ESPECT0yx'
        or source like 'FIXED%';
update channel_report_co 
set 
    channel = 'Groupon'
where
    coupon_code_description like '%Groupon%'
        and (source not like 'gracias%'
        and source not like 'GRI%'
        and source is not null);
update channel_report_co 
set 
    channel = 'Dafiti'
where
    source = 'AMIGODAFITI';
update channel_report_co 
set 
    channel = 'Sin IVA 16'
where
    source = 'SINIVA16';
update channel_report_co 
set 
    channel = 'En Medio'
where
    source = 'ENMEDIO';
update channel_report_co 
set 
    channel = 'Bancolombia'
where
    source like 'AMEX%'
        or source = 'ABNSbkpSmM'
        or source = 'ATR08i1X6D'
        or source = 'TABAMEX1DA2h'
        or source = 'ATR08i1X6D'
        or source = 'DEV1sd0c'
        or source = 'DEVfjFW6'
        or source = 'DEVbCM2o'
        or source = 'DEVN6TJN'
        or source = 'DEVFvwqn'
        or source = 'DEV0QUfY'
        or source = 'DEVmhwdB'
        or source = 'DEV0rCuq'
        or source = 'DEVlLjUZ'
        or source = 'DEVzKPuf'
        or source = 'DEV6IJP6'
        or source = 'DEV08L8A'
        or source = 'DEV0Fvg'
        or source = 'DEVcTttT'
        or source = 'DEVywRNF'
        or source = 'DEV0umCc'
        or source = 'DEVFcXZN'
        or source = 'AMEXTAB8BSoc';
update channel_report_co 
set 
    channel = 'Citi Bank'
where
    source like 'CITI%'
        or source like 'POSTCITI%';

update channel_report_co 
set 
    channel = 'Lenddo'
where
		source Like 'LEND%'
        or source like 'LENNDO%';
update channel_report_co 
set 
    channel = 'VISA'
where
    source like 'VISA%';

update channel_report_co 
set 
    channel = substr(substr(coupon_code_description,
            locate('-', coupon_code_description) + 1),
        1,
        locate('-',
                substr(coupon_code_description,
                    locate('-', coupon_code_description) + 1)) - 1)
where
    coupon_code = source
        and yrmonth >= 201305
        and coupon_code_description like 'Partnerships-%';
		
		

update channel_report_co 
set 
    channel = 'Reorders CITI'
where
    source like 'INVCITI%';
update channel_report_co 
set 
    channel = 'Replace CITI'
where
    source like 'REPCITI%';

update channel_report_co 
set 
    channel = 'Referral Sites'
where
    source like '%referral'
        and (source not like 'google%'
        and source not like 'linio%'
        and source not like 'facebook%'
        or source <> 'linio.com.co / referral'
        and source <> 'facebook / retargeting');

update channel_report_co 
set 
    channel_group = 'Other (identified)'
where
    (coupon_code_description like 'Partnerships%')
        and coupon_code = source
        and yrmonth >= 201305;


update channel_report_co 
set 
    channel = 'Linio.com Referral'
where
    source = 'linio.com / referral'
        or source = 'info.linio.com.co / referral'
        or source = 'linio.com.co / referral'
        or source = 'r.linio.com.co / referral';
update channel_report_co 
set 
    channel = 'Directo / Typed'
where
    source = '(direct) / (none)';


update channel_report_co 
set 
    channel = 'Liniofashion.com.co Referral'
where
    source = 'liniofashion.com.co / referral';

UPDATE channel_report_co 
set 
    channel = 'Curebit'
where
    coupon_code like '%cbit%'
        or source_medium like '%curebit%';
UPDATE channel_report_co 
set 
    channel_group = 'Referal Platform'
where
    channel = 'Curebit';

update channel_report_co 
set 
    channel = 'Referral Program'
where
    source like 'REF%';

update channel_report_co 
set 
    channel = 'Corporate Sales'
where
    source like 'VC%' or source like 'VCJ%'
        or source like 'VCH%'
        or source like 'CVE%'
        or source like 'VCA%'
        or source = 'BIENVENIDO10'
        or coupon_code like 'VCLiq%'
        or source like '%Corporate%Sales%';

update channel_report_co 
set 
    channel = 'Corporate Sales'
where
    OrderNum = '200729359'
        or OrderNum = '200655719'
        or OrderNum = '200935119'
        or OrderNum = '200636439'
        or OrderNum = '200691959'
        or OrderNum = '200461959';

update channel_report_co 
set 
    channel = 'Corporate Sales'
where
    OrderNum in ('200775621' , '200152221',
        '200863421',
        '200163941',
        '200667415',
        '200914741');

update channel_report_co 
set 
    channel = 'Journalist Vouchers'
where
    source = 'CAMILOCARM50'
        or source = 'JUANPABVAR50'
        or source = 'LAURACHARR50'
        or source = 'MONICAPARA50'
        or source = 'DIEGONARVA50'
        or source = 'LUCYBUENO50'
        or source = 'DIANALEON50'
        or source = 'SILVIAPARR50'
        or source = 'LAURAAYALA50'
        or source = 'LEONARDONI50'
        or source = 'CYNTHIARUIZ50'
        or source = 'JAVIERLEAESC50'
        or source = 'LORENAPULEC50'
        or source = 'MARIAISABE50'
        or source = 'PAOLACALLE50'
        or source = 'ISABELSALAZ50'
        or source = 'VICTORSOLA50'
        or source = 'FABIANRAM50'
        or source = 'ANDRESROD50'
        or source = 'HENRYGON50'
        or source = 'JOHANMA50'
        or source = 'ALFONSOAYA50'
        or source = 'VICTORSOLANAV'
        or SOURCE = 'LUISFERNANDOBOTERO'
        or source = 'PILARBOLIVAR'
        or source = 'MARCELAESTRADA'
        or source = 'LUXLANCHEROS';

update channel_report_co 
set 
    channel = 'ADN'
where
    source like 'ADN%';
update channel_report_co 
set 
    channel = 'Metro'
where
    source like 'METRO%';
update channel_report_co 
set 
    channel = 'TRANSMILENIO'
where
    source like 'TRANSMILENIO%'
        or SOURCE = 'Calle76'
        or SOURCE = 'Calle100'
        or SOURCE = 'Calle63'
        or SOURCE = 'Calle45'
        or SOURCE = 'Marly'
        or source = 'HEROES'
        or SOURCE = 'ESCUELAMILITAR';
update channel_report_co 
set 
    channel = 'SMS SanValentin'
where
    source like 'SANVALENTIN%';
update channel_report_co 
set 
    channel = 'SuperDescuentos'
where
    source like 'SUPERDESCUENTOS%';
update channel_report_co 
set 
    channel = 'Folletos'
where
    source = 'Regalo';
update channel_report_co 
set 
    channel = 'Mujer'
where
    source like 'MUJER%'
        and source <> 'mujer.linio.com.co / referral';
update channel_report_co 
set 
    channel = 'DiaEspecial'
where
    source like 'DiaEspecial%';
update channel_report_co 
set 
    channel = 'Hora Loca SMS'
where
    source like 'HORALOCA%';
update channel_report_co 
set 
    channel = 'Calle Vouchers'
where
    source like 'call%';
update channel_report_co 
set 
    channel = 'SMS'
where
    source like 'SMS%'
        and source <> 'SMSNueva';
update channel_report_co 
set 
    channel = 'BBVADP'
where
    source = 'BBVADP';
update channel_report_co 
set 
    channel = 'Other PR'
where
    source = 'LG082EL97RQSLACOLDP'
        or source = 'SO017EL14ZQRLACOLDP';
update channel_report_co 
set 
    channel = 'Valla Abril'
where
    source = 'Regalo50';
update channel_report_co 
set 
    channel = 'SMS Nueva pagina NC'
where
    source = 'RegaloSMS';
update channel_report_co 
set 
    channel = 'SMS Nueva pagina Costumers'
where
    source = 'SMSNueva';
update channel_report_co 
set 
    channel = 'SMS Más temprano más barato'
where
    source = 'BaratoSMS';
update channel_report_co 
set 
    channel = 'Barranquilla'
where
    source like '%quilla%'
        ;
update channel_report_co 
set 
    channel = 'Regresa (React.)'
where
    source = 'Regresa'
        or source = 'Regresas49'
        or source = 'Regresa49';
update channel_report_co 
set 
    channel = 'Te Esperamos (React.)'
where
    source like 'teesperamos%';
update channel_report_co 
set 
    channel = 'Gracias (Repurchase)'
where
    source like 'gracias%';
update channel_report_co 
set 
    channel = 'Falabella Customers'
where
    source = '50Bienvenido'
        or source like '50bienvenido%';
update channel_report_co 
set 
    channel = 'Cali'
where
    source = 'Fabian' or source = 'Gloria'
        or source = 'Miguel'
        or source like 'Cali%';
update channel_report_co 
set 
    channel = 'CC Avenida Chile'
where
    source like 'PARATI%';
update channel_report_co 
set 
    channel = 'Correo Bogota'
where
    source = '100Regalo';
update channel_report_co 
set 
    channel = 'Feria Libro'
where
    source like 'Feria%';
update channel_report_co 
set 
    channel = 'Correo directo bogota'
where
    source = '50Regalo'
        or source = '100Regalo'
        or source = '200Regalo';
update channel_report_co 
set 
    channel = 'SMS Madres'
where
    source = 'MADRE30';
update channel_report_co 
set 
    channel = 'CEROIVA'
where
    source like 'CEROIVA%';
update channel_report_co 
set 
    channel = 'Circulo de la Moda'
where
    source like 'CIRCULOMODA%';
update channel_report_co 
set 
    channel = 'SMS Mas Barato'
where
    source like 'MASBARATO%';
update channel_report_co 
set 
    channel = 'Mailing 2'
where
    source = 'REGALO1' or source = 'REGALO2'
        or source = 'REGALO3'
        or source = 'REGALO050'
        or source = 'REGALO100'
        or source = 'REGALO200';
update channel_report_co 
set 
    channel = 'Reactivation60 dias'
where
    source = 'Vuelve50';
update channel_report_co 
set 
    channel = 'Reactivation120 dias'
where
    source = 'vuelve050';
update channel_report_co 
set 
    channel = 'Reactivation180 dias'
where
    source = '50Vuelve';
update channel_report_co 
set 
    channel = 'Valla 2nd stage'
where
    source like 'valla%';
update channel_report_co 
set 
    channel = 'Repurchase50'
where
    source = '050Gracias'
        or source like '%050gracias%'
        or source like '%50gra%';
update channel_report_co 
set 
    channel = 'Repurchase20'
where
    source = '020Gracias';
update channel_report_co 
set 
    channel = 'Catalogo Junio'
where
    source = 'CATALOGO50';

update channel_report_co 
set 
    coupon_code_description = 'Offline-Envios600000casas-20'
where
    source = 'TODO20';


select 'Offline Marketing', now();
update channel_report_co 
set 
    channel = substr(substr(coupon_code_description,
            locate('-', coupon_code_description) + 1),
        1,
        locate('-',
                substr(coupon_code_description,
                    locate('-', coupon_code_description) + 1)) - 1)
where
   yrmonth >= 201305
        and coupon_code_description like 'offline%';


update channel_report_co 
set 
    channel_group = 'Offline Marketing'
where
    (coupon_code_description like 'offline%')
        and yrmonth >= 201305;

commit;

update channel_report_co 
set 
    channel = substr(substr(coupon_code_description,
            locate('-', coupon_code_description) + 1),
        1,
        locate('-',
                substr(coupon_code_description,
                    locate('-', coupon_code_description) + 1)) - 1)
where
    coupon_code = source
        and yrmonth >= 201305
        and coupon_code_description like 'PR-%';

update channel_report_co 
set 
    channel_group = 'Other (identified)'
where
    (coupon_code_description like 'PR-%')
        and coupon_code = source
        and yrmonth >= 201305;

/*UPDATE channel_report_co 
set 
    channel = 'Subastas Linio',
    channel_group = 'Other (identified)',
    channel_type = 'Marketing CO'
where
    CustomerNum = 0 AND OrderNum IN (5 , 8, 11);*/

update channel_report_co 
set 
    channel = 'Corporate Sales'
where
    OrderNum = '200091496'
        or OrderNum = '200078186'
        or OrderNum = '200066886'
        or OrderNum = '200088876'
        or OrderNum = '200091629'
        or OrderNum = '200061629'
        or OrderNum = '200021629'
        or OrderNum = '200041629'
        or OrderNum = '200035629'
        or OrderNum = '200015629'
        or OrderNum = '200075629'
        or OrderNum = '200085629'
        or OrderNum = '200065629'
        or OrderNum = '200018629'
        or OrderNum = '200078629'
        or OrderNum = '200098629'
        or OrderNum = '200037636'
        or OrderNum = '200027356'
        or OrderNum = '200078656'
        or OrderNum = '200058656'
        or OrderNum = '200032829'
        or OrderNum = '200051569'
        or OrderNum = '200097636'
        or OrderNum = '200023129'
        or OrderNum = '200025729'
        or OrderNum = '200037969'
        or OrderNum = '200094566'
        or OrderNum = '200064566'
        or OrderNum = '200017946'
        or OrderNum = '200056446'
        or OrderNum = '200091732'
        or OrderNum = '200061732'
        or OrderNum = '200021732'
        or OrderNum = '200067932'
        or OrderNum = '200012232'
        or OrderNum = '200062112'
        or OrderNum = '200027812'
        or OrderNum = '200047812'
        or OrderNum = '200018812'
        or OrderNum = '200021972'
        or OrderNum = '200035972'
        or OrderNum = '200055972'
        or OrderNum = '200027672'
        or OrderNum = '200058672'
        or OrderNum = '200095272'
        or OrderNum = '200096272'
        or OrderNum = '200066272'
        or OrderNum = '200046272'
        or OrderNum = '200012272'
        or OrderNum = '200072272'
        or OrderNum = '200078186'
        or OrderNum = '200091496'
        or OrderNum = '200034532'
        or OrderNum = '200014532'
        or OrderNum = '200045272'
        or OrderNum = '200075862'
        or OrderNum = '200046796'
        or OrderNum = '200013572'
        or OrderNum = '200066886'
        or OrderNum = '200088876'
        or OrderNum = '200027356'
        or OrderNum = '200078656'
        or OrderNum = '200051569'
        or OrderNum = '200032829'
        or OrderNum = '200097636'
        or OrderNum = '200023129'
        or OrderNum = '200025729'
        or OrderNum = '200043322'
        or OrderNum = '200083356'
        or OrderNum = '200031416'
        or OrderNum = '200014936'
        or OrderNum = '200047812'
        or OrderNum = '200027812'
        or OrderNum = '200026765'
        or OrderNum = '200065495'
        or OrderNum = '200094437'
        or OrderNum = '200021561'
        or OrderNum = '200012651'
        or OrderNum = '200044861'
        or OrderNum = '200021791'
        or OrderNum = '200057991'
        or OrderNum = '200083781'
        or OrderNum = '200064581'
        or OrderNum = '200011781'
        or OrderNum = '200073781'
        or OrderNum = '200023781'
        or OrderNum = '200036831'
        or OrderNum = '200029831'
        or OrderNum = '200099831'
        or OrderNum = '200089831'
        or OrderNum = '200019831'
        or OrderNum = '200089683'
        or OrderNum = '200046911'
        or OrderNum = '200041933'
        or OrderNum = '200085933'
        or OrderNum = '200045933'
        or OrderNum = '200037933'
        or OrderNum = '200017933'
        or OrderNum = '200041933'
        or OrderNum = '200085933'
        or OrderNum = '200045933'
        or OrderNum = '200037933'
        or OrderNum = '200017933'
        or OrderNum = '200781642'
        or OrderNum = '200677842'
        or OrderNum = '200991742'
        or OrderNum = '200271142'
        or OrderNum = '200245142'
        or OrderNum = '200325142'
        or OrderNum = '200365142'
        or OrderNum = '200895142'
        or OrderNum = '200555142'
        or OrderNum = '200271142'
        or OrderNum = '200423142'
        or OrderNum = '200841142'
        or OrderNum = '200195962'
        or OrderNum = '200829492'
        or OrderNum = '200168292'
        or OrderNum = '200918692'
        or OrderNum = '200853382'
        or OrderNum = '200532252'
        or OrderNum = '200529252'
        or OrderNum = '200884952'
        or OrderNum = '200828952'
        or OrderNum = '200359852'
        or OrderNum = '200297852'
        or OrderNum = '200452752'
        or OrderNum = '200411152'
        or OrderNum = '200914352'
        or OrderNum = '200685116';

update channel_report_co 
set 
    channel = 'Inbound'
where
     source = 'CS / Inbound'
        or source like '%inbound%';

update channel_report_co 
set 
    channel = 'Outbound'
where
	source like 'CS %' 
	and source <> 'CS / Inbound'
    and source not like '%inbound%';

update channel_report_co 
set 
    channel = 'Unknown'
where
    source = '' or channel is null;
update channel_report_co 
set 
    channel = 'Unknown-Voucher'
where
    channel = ''
        and (source <> '' and coupon_code is not null);
update channel_report_co 
set 
    channel = 'Unknown-GA Tracking'
where
    channel = '' and source <> ''
        and coupon_code is null
        and source_medium is not null;

UPDATE channel_report_co 
set 
    channel_group = 'Advertising'
where
    OrderNum like '50000%'
        and coupon_code not like 'VC%';

update channel_report_co 
set 
    channel = 'Unknown-CS'
where
    (source is null or source = '')
        and (AssistedSalesOperator <> ''
        or AssistedSalesOperator is not null);

/*Este query sobra, nunca se dara este escenario.
UPDATE channel_report_co 
set 
    channel = 'Dark Post'
where
    campaign like '%darkpost%';
*/
	
/* Se agrego a un OR del update principal de ORs del canal
update channel_report_co 
set 
    channel_group = 'Other (identified)'
where
    channel = 'Libro Bogota';
*/
	
SELECT 'Start: CHANNEL GROUPING', now();

update channel_report_co 
set 
    channel_group = 'Facebook Ads'
where
    channel = 'Facebook Ads'
        or channel = 'Alejandra Arce Voucher';
        /*or channel = 'Dark Post';*/

/* CACs se pasan a Linio Fan Page, lo demas a FB CAC y FBF CAC
UPDATE channel_report_co 
SET 
    channel_group = 'CAC Deals'
WHERE
    channel = 'FB Ads CAC Junio'
        or channel = 'FB Ads CAC Mayo'
        or channel = 'FB Ads CAC'
        or channel = 'FB CAC'
        or channel = 'FBF CAC'
        or channel = 'Bandeja CAC'
        or channel = 'Tapete CAC'
        or channel = 'Perfumes CAC'
        or channel = 'Sartenes CAC'
        or channel = 'Estufa CAC'
        or channel = 'Vuelve CAC'
        or channel = 'Memoria CAC'
        or channel = 'Audifonos CAC'
        or channel = 'Bienvenido CAC'
        or channel = 'Amigo CAC'
        or channel = 'CACs Marzo'
        or channel = 'CACs Abril'
        or channel = 'CACs Mayo'
        or channel = 'CACs Junio'
        or channel = 'CACs Julio'
        or channel = 'NL CAC';
*/
		
update channel_report_co 
set 
    channel_group = 'Search Engine Marketing'
where
    channel = 'SEM (unbranded)'
        or channel = 'Other Branded';

/* Desaparece como channel group, ahora es Linio Fan Page (abajo)
update channel_report_co 
set 
    channel_group = 'Social Media'
where
    channel = 'Facebook Referral'
        or channel = 'Twitter'
        or channel = 'FB Posts'
        or channel = 'FB-Diana Call'
        or channel = 'Muy Feliz'
        or channel = 'Linio Fan Page';
*/
update channel_report_co 
set 
    channel_group = 'Social Media'
where
    channel = 'Facebook Referral'
        or channel = 'FB Posts'
        or channel = 'FB-Diana Call'
        or channel = 'Muy Feliz'
        or channel = 'Linio Fan Page'
		or channel = 'Bandeja CAC'
        or channel = 'Tapete CAC'
        or channel = 'Perfumes CAC'
        or channel = 'Sartenes CAC'
        or channel = 'Estufa CAC'
        or channel = 'Vuelve CAC'
        or channel = 'Memoria CAC'
        or channel = 'Audifonos CAC'
        or channel = 'Bienvenido CAC'
        or channel = 'Amigo CAC'
        or channel = 'CACs Marzo'
        or channel = 'CACs Abril'
        or channel = 'CACs Mayo'
        or channel = 'CACs Junio'
        or channel = 'CACs Julio'
		or channel = 'FB Ads CAC Junio'
        or channel = 'FB Ads CAC Mayo'
        or channel = 'FB Ads CAC'
        or channel = 'FB CAC'
		or channel = 'FB Dark Post';

update channel_report_co 
set 
    channel_group = 'Social Media'
where
    channel = 'Fashion Fan Page'
		or channel = 'FBF CAC'
        or channel = 'FBF Dark Post';
		
	UPDATE channel_report_co 
set channel='FB MP', channel_group = 'Social Media'
WHERE campaign like '%_!MP' escape '!' and source_medium='facebook / socialmedia';

UPDATE channel_report_co 
set channel='FBF MP' , channel_group = 'Social Media'
WHERE campaign like '%_!MP' escape '!' and source_medium='facebook / socialmedia_fashion';

UPDATE channel_report_co 
set channel='FB Ads MP' , channel_group = 'Social Media'
WHERE campaign like '%_!MP' escape '!' and source_medium='facebook / socialmediaads';

UPDATE channel_report_co 
set channel='NL MP' , channel_group = 'Social Media'
WHERE campaign like '%_!MP' escape '!' and source_medium='postal / CRM';

UPDATE channel_report_co 
set channel='SEM Branded MP' , channel_group = 'Social Media'
WHERE campaign like 'brandb.%_MP' escape '!' and source_medium='google / cpc';
/*or  channel = 'FBF CAC'
		
/*update channel_report_co 
set 
    channel_group = 'Social Media'
where
    channel = 'Fashion Fan Page'
        or channel = 'Linio Fan Page'
        or channel = 'Dark Post';*/
/*or  channel = 'FBF CAC'*/


update channel_report_co 
set 
    channel_group = 'Retargeting'
where
    channel = 'Sociomantic'
        or channel = 'Vizury'
        or channel = 'GDN Retargeting'
        or channel = 'Facebook Retargeting'
        or channel = 'Criteo'
        or channel = 'My Things'
        or channel = 'Main ADV'
        or channel = 'Triggit';


update channel_report_co 
set 
    channel_group = 'Display'
where
    channel = 'Google Display Network'
        or channel = 'Exo'
        or channel = 'ICCK'
		or channel = 'MSN';


update channel_report_co 
set 
    channel_group = 'NewsLetter'
where
    channel = 'Newsletter'
        or channel = 'Voucher Navidad'
        or channel = 'Voucher Fin de Año'
        or channel = 'VoucherBebes-Feb'
        or channel = 'Venir Reactivation'
        or channel = 'Regresa Reactivation'
        or channel = 'Aqui Reactivation'
        or channel = 'Tiramos la Casa por la ventana'
        or channel = 'Creating Happiness'
        or channel = 'Grita Reactivation'
        or channel = 'Reactivación Mayo'
        or channel = 'Fashion'
        or channel = 'Reactivación Junio'
        or channel = 'Reactivación Julio'
        or channel = 'VEInteractive'
        or channel = 'Happy Birthday'
		or channel = 'NL CAC';

/*
update channel_report_co set channel_group='NewsLetter' where
(coupon_code_description like 'CRM%') and coupon_code=source and yrmonth>=201305;*/

update channel_report_co 
set 
    channel_group = 'NewsLetter'
where
    channel = 'New Register'
        and (AssistedSalesOperator is null or AssistedSalesOperator = '');

update channel_report_co 
set 
    channel_group = 'Search Engine Optimization'
where
    channel = 'Search Organic';


update channel_report_co 
set 
    channel_group = 'Other (identified)'
where
    channel = 'Banco Helm'
        or channel = 'Banco de Bogotá'
        or channel = 'Banco Davivienda'
        or channel = 'Banco Davivienda'
        or channel = 'Banco BBVA'
        or channel = 'Campus Party'
        or channel = 'El Espectador'
        or channel = 'Groupon'
        or channel = 'Dafiti'
        or channel = 'Sin IVA 16'
        or channel = 'En Medio'
        or channel = 'Bancolombia'
        or channel = 'AMEX'
        or channel = 'Citi Bank'
        or channel = 'Lenddo'
        or channel = 'Unknown Partnership March'
        or channel = 'Post BDB'
        or channel = 'VISA'
        or channel = 'Coomservi'
        or channel = 'Recuperación - Partnership'
        or channel = 'Reorders CITI'
        or channel = 'Replace CITI'
        or channel = 'BDBJULIO'
        or channel = 'Bancolombia E'
        or channel = 'BDB JUNIO'
		or channel = 'Twitter'
		or channel = 'Libro Bogota'
		or channel = 'Corporate Sales'
		or channel = 'Referral Sites'
        or channel = 'Reactivation Letters'
        or channel like 'Customer Adquisition%'
        or channel = 'Internal Sells'
        or channel = 'Employee Vouchers'
        or channel = 'Parcel Vouchers'
        or channel = 'Disculpas'
        or channel = 'Suppliers Email'
        or channel = 'Out Of Stock'
        or channel = 'Broken Discount'
        or channel = 'Broken Vouchers'
        or channel = 'Devoluciones'
        or channel = 'Voucher Empleados'
        or channel = 'Gift Cards'
        or channel = 'Friends and Family'
        or channel = 'Leads'
        or channel = 'Referral Program'
        or channel = 'Nueva Página'
        or channel = 'Promesa Linio'
        or channel = 'NO IVA Abril'
        or channel = 'Invitados VIP Círculo de la moda'
        or channel = 'Cash Back'
		or channel = 'Mercado Libre - Alamaula'
        or channel = 'Otros - Mercado Libre'
        or channel = 'Alamaula';

/* Se agrego a un OR del update principal de ORs del canal
update channel_report_co 
set 
    channel_group = 'Other (identified)'
where
    channel = 'Corporate Sales';
*/

update channel_report_co 
set 
    channel_group = 'Offline Marketing'
where
    channel = 'ADN'
        or channel = 'Journalist Vouchers'
        or channeL = 'Metro'
        or channel = 'TRANSMILENIO'
        or channel = 'SMS SanValentin'
        or channel = 'SuperDescuentos'
        or channel = 'Folletos'
        or channel = 'MUJER'
        or channel = 'DiaEspecial'
        or channel = 'Hora Loca SMS'
        or channel = 'Calle Vouchers'
        or channel = 'SMS'
        or channel = 'BBVADP'
        or channel = 'Other PR'
        or channel = 'Valla Abril'
        or channel = 'SMS Nueva pagina NC'
        or channel = 'SMS Nueva pagina Costumers'
        or channel = 'SMS Más temprano más barato'
        or channel = 'Barranquilla'
        or channel = 'Regresa (React.)'
        or channel = 'Te Esperamos (React.)'
        or channel = 'Gracias (Repurchase)'
        or channel = 'Falabella Customers'
        or channel = 'Cali'
        or channel = 'CC Avenida Chile'
        or channel = 'Correo Bogota'
        or channel = 'Feria Libro'
        or channel = 'Correo directo bogota'
        or channel = 'CEROIVA'
        or channel = 'SMS Madres'
        or channel = 'Circulo de la Moda'
        or channel = 'SMS Mas Barato'
        or channel = 'Mailing 2'
        or channel = 'Publimetro'
        or channel = 'Reactivation60 dias'
        or channel = 'Reactivation120 dias'
        or channel = 'Reactivation180 dias'
        or channel = 'Valla 2nd stage'
        or channel = 'Repurchase50'
        or channel = 'Repurchase20'
        or channel = 'Catalogo Junio'
        or channel = 'Repurchase 50'
        or channel = 'SMS Mas barato nadie puede'
        or channel = 'Repurchase  50'
        or channel = 'SMS padres2'
        or channel = 'SMS Independencia'
        or channel = 'SMS aniversario base interna'
        or channel = 'Repurchase2'
        or channel = 'Periodista'
        or channel = 'Envios 600.000 casas'
;
 

update channel_report_co 
set 
    channel = 'Invalids'
where
    source like 'INV%'
        and (channel is null or channel = ''
        or channel_group = 'Non identified');
		
update channel_report_co 
set 
    channel = 'Garantías'
where
    source like 'GAR%'
        and (channel is null or channel = ''
        or channel_group = 'Non identified');


update channel_report_co 
set 
    channel = 'Reorder'
where
    source like 'REO%'
        and (channel is null or channel = ''
        or channel_group = 'Non identified');

update channel_report_co 
set 
    channel = 'Replace'
where
    source like 'REP%'
        and (channel is null or channel = ''
        or channel_group = 'Non identified');


update channel_report_co 
set 
    channel_group = 'Tele Sales'
where
		channel = 'Outbound'
        or channel = 'Inbound'
		or channel = 'Unknown-CS';

update channel_report_co 
set 
    channel_group = 'Tele Sales'
where
    channel = 'New Register'
        and AssistedSalesOperator is not null;


update channel_report_co 
set 
    channel_group = 'Branded'
where
    channel = 'Linio.com Referral'
        or channel = 'Directo / Typed'
        or channel = 'SEM Branded'
        or channel = 'Liniofashion.com.co Referral'
        or channel = 'Bing';

/* Se agrego a un OR del update principal de ORs del Other (identified), desaparece como channel group
update channel_report_co 
set 
    channel_group = 'Mercado Libre - Alamaula'
where
    channel = 'Mercado Libre - Alamaula'
        or channel = 'Otros - Mercado Libre'
        or channel = 'Alamaula';
*/

update channel_report_co 
set 
    channel_group = 'Affiliates'
where
    channel = 'Buscape'
        or channel = 'Trade Tracker';
update channel_report_co 
set 
    channel_group = 'Affiliates'
where
    channel = 'Buscape'
        or channel = 'Trade Tracker';

update channel_report_co 
set 
    channel = 'Cash Back'
WHERE
    source_medium is null
        and coupon_code in ('DINEROVUELVE' , 'VUELVEDINERO');

/* Se agrego a un OR del update principal de ORs del canal
update channel_report_co 
set 
    channel_group = 'Other (identified)'
where
    channel = 'Referral Sites'
        or channel = 'Reactivation Letters'
        or channel like 'Customer Adquisition%'
        or channel = 'Internal Sells'
        or channel = 'Employee Vouchers'
        or channel = 'Parcel Vouchers'
        or channel = 'Disculpas'
        or channel = 'Suppliers Email'
        or channel = 'Out Of Stock'
        or channel = 'Broken Discount'
        or channel = 'Broken Vouchers'
        or channel = 'Devoluciones'
        or channel = 'Voucher Empleados'
        or channel = 'Gift Cards'
        or channel = 'Friends and Family'
        or channel = 'Leads'
        or channel = 'Referral Program'
        or channel = 'Nueva Página'
        or channel = 'Promesa Linio'
        or channel = 'NO IVA Abril'
        or channel = 'Invitados VIP Círculo de la moda'
        or channel = 'Cash Back';
*/

update channel_report_co 
set 
    channel_group = 'Non identified'
where
    channel = 'Unknown'
        or channel = 'Unknown-Voucher'
        or channel = 'Unknown-GA Tracking';

update channel_report_co 
set 
    channel_group = 'Affiliates'
where
    channel = 'Pampa Network';


update channel_report_co 
set 
    channel_type = 0
where
    channel_group = 'Non identified';



update channel_report_co 
set 
    channel_type = 'Marketing CO'
where
    (channel_group = 'NewsLetter'
        or channel_group = 'Social Media'
        or channel_group = 'Fashion Fan Page'
        or channel_group = 'Partnerships'
        or channel_group = 'Search Engine Optimization'
        or channel_group = 'Corporate Sales'
        or channel_group = 'Mercado Libre - Alamaula'
        or channel_group = 'Blogs'
        or channel_group = 'Branded'
        or channel_group = 'Other (identified)'
        or channel = 'Exo'
        or channel = 'ICCK'
        or channel_group = 'Tele Sales'
        or channel_group = 'Offline Marketing')
        and channel_group <> 'Non identified';


update channel_report_co 
set 
    channel_type = 'Marketing Regional'
where
    (channel_group = 'Facebook Ads'
        or channel_group = 'Search Engine Marketing'
        or channel_group = 'Retargeting'
        or channel_group = 'Affiliates'
        or channel = 'Google Display Network')
        and channel_group <> 'Non identified';

update channel_report_co 
set 
    channel_type = 0
where
    channel_group = 'Non identified';



update channel_report_co 
set 
    Ownership = 'Vanessa Socha'
where
    channel_group = 'Linio Fan Page'
        and channel <> 'Fashion Fan Page';
update channel_report_co 
set 
    Ownership = 'Andrés Ponce'
where
    channel = 'Fashion Fan Page';
update channel_report_co 
set 
    Ownership = 'Jose Sequera'
where
    channel_group = 'NewsLetter';
update channel_report_co 
set 
    Ownership = 'Paola'
where
    channel_group = 'Blogs';
update channel_report_co 
set 
    Ownership = 'Juan Camilo García'
where
    channel_group = 'Offline Marketing';
update channel_report_co 
set 
    Ownership = 'Giancarlo Benavides'
where
    channel_group = 'Partnerships';
update channel_report_co 
set 
    Ownership = 'Sindy'
where
    channel_group = 'Mercado Libre - Alamaula';
update channel_report_co 
set 
    Ownership = 'Lorena Gamboa'
where
    channel_group = 'Tele Sales';
update channel_report_co 
set 
    Ownership = 'Fabián/Camilo/Jair'
where
    channel_group = 'Corporate Sales';


UPDATE channel_report_co 
set 
    channel_group = 'Advertising'
where
    OrderNum like '50000%'
        and (coupon_code not like 'VC%'
        AND coupon_code not like 'VISA%');
UPDATE channel_report_co 
set 
    channel_group = 'Advertising'
where
    OrderNum like '50000%'
        and coupon_code like '%VISA%';


update channel_report_co 
set 
    channel = substr(substr(coupon_code_description,
            locate('-', coupon_code_description) + 1),
        1,
        locate('-',
                substr(coupon_code_description,
                    locate('-', coupon_code_description) + 1)) - 1)
where
 yrmonth >= 201305
        and coupon_code_description like 'offline%';

update channel_report_co 
set 
    channel_group = 'Offline Marketing'
where
    (coupon_code_description like 'offline%')
        and yrmonth >= 201305;
		
UPDATE channel_report_co t
        inner join
    production_co.mobileapp_transaction_id m ON t.OrderNum = m.transaction_id 
set 
    channel = 'Mobile App';
	
	

	UPDATE channel_report_co t
set 
    channel = 'Mobile App'
where source = 'android20';
	
UPDATE channel_report_co 
SET 
    channel_group = 'Mobile App'
WHERE
    channel = 'Mobile App';
