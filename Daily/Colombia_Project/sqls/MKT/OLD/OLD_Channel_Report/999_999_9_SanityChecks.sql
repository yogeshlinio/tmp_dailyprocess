INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'marketing_co.channel_report',
  'finish',
  NOW(),
  MAX(date),
  count(*),
  count(*)
FROM
marketing_co.channel_report
;

