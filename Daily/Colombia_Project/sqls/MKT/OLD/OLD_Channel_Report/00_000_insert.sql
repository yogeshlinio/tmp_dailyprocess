USE marketing_co;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'marketing_co.channel_report',
  'start',
  NOW(),
  MAX(date),
  count(*),
  count(*)
FROM
channel_report
;

SELECT @c_date:=CURDATE() 
FROM dual;

SELECT @v_maxdate:=MAX(date)
FROM channel_report;


SELECT 'Borrando ultimo dia de channel_report CO', now();
DELETE FROM channel_report WHERE date=@v_maxDate and @v_maxdate=@c_date;

SELECT @v_maxorderID:=MAX(orderID)
FROM channel_report;


INSERT INTO channel_report
(yrmonth, date,OrderNum,
orderID,
coupon_code,
assistedsalesoperator,
coupon_code_description)
SELECT 
MonthNum, date, OrderNum, IdSalesOrder,
CouponCode, AssistedSalesOperator, CouponCodeDescription
from development_co_project.A_Master
where IdSalesOrder > @v_maxorderID 
group by idSalesOrder
order by date asc;

UPDATE channel_report
INNER JOIN bob_live_co.sales_rule 
		ON channel_report.coupon_code= sales_rule.code
INNER JOIN bob_live_co.sales_rule_set 
       ON bob_live_co.sales_rule.fk_sales_rule_set = bob_live_co.sales_rule_set.id_sales_rule_set 
SET coupon_name = sales_rule_set.name;