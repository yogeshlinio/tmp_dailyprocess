-- Colombia
-- Fanpage Optimization

set @days:=30;

delete from facebook.fanpage_campaign_co where datediff(curdate(), date)<@days;

delete from facebook.fanpage_transaction_id_co where datediff(curdate(), date)<@days;

insert into facebook.fanpage_campaign_co(date, campaign, source, medium, impressions, clicks, visits, ad_cost, bounce, cart) select distinct date, campaign, source, medium, impressions, clicks, visits, ad_cost, bounce, cart from SEM.campaign_ad_group_co where campaign like '%fanpage%' and (campaign not like '%IMA-%' and campaign not like '%MAI-%');

insert into facebook.fanpage_transaction_id_co select * from SEM.transaction_id_co where campaign like '%fanpage%' and (campaign not like '%IMA-%' and campaign not like '%MAI-%');

update facebook.fanpage_campaign_co set campaign = replace(campaign, '_', '.');

update facebook.fanpage_transaction_id_co set campaign = replace(campaign, '_', '.');

delete from facebook.fanpage_optimization_co where datediff(curdate(), date)<@days;

insert into facebook.fanpage_optimization_co (date, campaign, visits, carts) select date, campaign, visits, cart from facebook.fanpage_campaign_co where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=facebook.week_iso(date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set f.obc_transactions = (select count(distinct z.transaction_id) from production_co.tbl_order_detail a, facebook.fanpage_transaction_id_co z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.date=z.date and a.obc=1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set f.gross_revenue = (select sum(total_value) from facebook.fanpage_transaction_id_co z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set f.oac_transactions = (select count(distinct z.transaction_id) from production_co.tbl_order_detail a, facebook.fanpage_transaction_id_co z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.date=z.date and a.oac=1) where datediff(curdate(), date)<@days;

create table facebook.temporary_co as select r.date, r.campaign, (select t.custid from production_co.tbl_order_detail t where t.order_nr=r.transaction_id and oac=1 group by t.custid) as custid, r.transaction_id, (select sum(t.paid_price) from production_co.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as paid_price, (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) from production_co.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as PC2, (select sum(new_customers) from production_co.tbl_order_detail z where z.order_nr=r.transaction_id and z.oac=1 group by z.order_nr) as new_customers from facebook.fanpage_transaction_id_co r where datediff(curdate(), date)<@days group by transaction_id;

-- Back Cohort

create table facebook.temporary_back_cohort_co(
date date,
campaign varchar(255),
custid int,
transaction_id varchar(255),
oac_transactions_30_cohort float,
oac_transactions_60_cohort float,
paid_price_30_cohort float,
paid_price_60_cohort float,
PC2_30_cohort float,
PC2_60_cohort float,
repeated int
);


create index custid on facebook.temporary_back_cohort_co(custid);

create index all_index on facebook.temporary_back_cohort_co(date, campaign);

insert into facebook.temporary_back_cohort_co (date, campaign, custid, transaction_id) select date, campaign, custid, transaction_id from facebook.temporary_co where new_customers !=0 and datediff(curdate(), date)<@days;

create table facebook.backup_temporary_back_cohort_co as select * from facebook.temporary_back_cohort_co;

update facebook.temporary_back_cohort_co a set repeated = (select count(*) as total from facebook.backup_temporary_back_cohort_co b where a.date=b.date and a.campaign=b.campaign group by date, campaign, custid having count(*)>1);

update facebook.temporary_back_cohort_co a set repeated = 1 where repeated is null;

drop table facebook.backup_temporary_back_cohort_co;

update facebook.temporary_back_cohort_co b set oac_transactions_30_cohort = (select count(distinct t.order_nr) as oac_transactions from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers =0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_co b set paid_price_30_cohort = (select sum(t.paid_price) as paid_price from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers =0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_co b set PC2_30_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers =0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_co b set oac_transactions_60_cohort = (select count(distinct t.order_nr) as oac_transactions from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers =0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_co b set paid_price_60_cohort = (select sum(t.paid_price) as paid_price from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers =0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_co b set PC2_60_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers =0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_co set oac_transactions_30_cohort=oac_transactions_30_cohort/repeated, paid_price_30_cohort=paid_price_30_cohort/repeated, PC2_30_cohort=PC2_30_cohort/repeated, oac_transactions_60_cohort=oac_transactions_60_cohort/repeated, paid_price_60_cohort=paid_price_60_cohort/repeated, PC2_60_cohort=PC2_60_cohort/repeated where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set oac_transactions_30_cohort = (select sum(oac_transactions_30_cohort) from facebook.temporary_back_cohort_co b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set cum_net_rev_30_cohort = (select sum(paid_price_30_cohort) from facebook.temporary_back_cohort_co b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set PC2_30_cohort = (select sum(PC2_30_cohort) from facebook.temporary_back_cohort_co b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set oac_transactions_60_cohort = (select sum(oac_transactions_60_cohort) from facebook.temporary_back_cohort_co b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set cum_net_rev_60_cohort = (select sum(paid_price_60_cohort) from facebook.temporary_back_cohort_co b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set PC2_60_cohort = (select sum(PC2_60_cohort) from facebook.temporary_back_cohort_co b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

drop table facebook.temporary_back_cohort_co;

update facebook.fanpage_optimization_co set oac_transactions_30_cohort = 0 where oac_transactions_30_cohort is null;

update facebook.fanpage_optimization_co set oac_transactions_60_cohort = 0 where oac_transactions_60_cohort is null;

update facebook.fanpage_optimization_co set cum_net_rev_30_cohort = 0 where cum_net_rev_30_cohort is null;

update facebook.fanpage_optimization_co set cum_net_rev_60_cohort = 0 where cum_net_rev_60_cohort is null;

-- Back Cohort

alter table facebook.temporary_co add column avg_discount float;

update facebook.temporary_co r set avg_discount = (select avg(1-(i.unit_price/i.original_unit_price)) from bob_live_co.sales_order o inner join bob_live_co.sales_order_item i on i.fk_sales_order = o.id_sales_order where r.transaction_id=o.order_nr group by o.order_nr) where datediff(curdate(), date)<@days; 

create index temporary_co on facebook.temporary_co(campaign, date);

update facebook.fanpage_optimization_co f set net_revenue= (select sum(paid_price) from facebook.temporary_co z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set new_customers= (select sum(z.new_customers) from facebook.temporary_co z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set PC2_absolute= (select sum(PC2) from facebook.temporary_co z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set `avg_discount(%)`= (select (avg(avg_discount))*100 from facebook.temporary_co z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

drop table facebook.temporary_co;

update facebook.fanpage_optimization_co set oac_transactions = 0 where oac_transactions is null;

update facebook.fanpage_optimization_co set net_revenue = 0 where net_revenue is null;

update facebook.fanpage_optimization_co set new_customers = 0 where new_customers is null;

create table facebook.optimization_co as select date, campaign, sum(visits)as visits from facebook.fanpage_optimization_co where datediff(curdate(), date)<@days group by date, campaign;

create index optimization_co on facebook.optimization_co(date, campaign);

create table facebook.cost_campaign_co as select date, campaign, sum(spent) as spent, sum(clicks) as clicks, sum(social_impressions) as impressions from facebook.facebook_campaign where datediff(curdate(), date)<@days group by date, campaign;

create index cost_campaign_co on facebook.cost_campaign_co(date, campaign);

create table facebook.final_co as select a.date, a.campaign, a.visits, b.spent, b.clicks, b.impressions from facebook.optimization_co a, facebook.cost_campaign_co b where a.date=b.date and a.campaign=b.campaign;

drop table facebook.optimization_co;

drop table facebook.cost_campaign_co;

create index final_co on facebook.final_co(date, campaign);

update facebook.fanpage_optimization_co f inner join facebook.final_co final_co on f.date=final_co.date and f.campaign=final_co.campaign set f.cost=(f.visits/(final_co.visits))*final_co.spent, f.clicks=(f.visits/(final_co.visits))*final_co.clicks, f.impressions=(f.visits/(final_co.visits))*final_co.impressions where datediff(curdate(), f.date)<@days;

drop table facebook.final_co;

update facebook.fanpage_optimization_co set cost = 0 where cost is null;

update facebook.fanpage_optimization_co set clicks = 0 where clicks is null;

update facebook.fanpage_optimization_co set impressions = 0 where impressions is null;

update facebook.fanpage_optimization_co set category = substr(substr(campaign, locate('.',campaign)+1), 1, locate('.', substr(campaign, locate('.',campaign)+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co set gender = substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1) ,1 , locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))-1) ;

update facebook.fanpage_optimization_co set age = substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,1 ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co set segmentation = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) ,1 ,locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co set extra_field = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) , locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))+1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, PC2_absolute=PC2_absolute/xr, cum_net_rev_30_cohort=cum_net_rev_30_cohort/xr, cum_net_rev_60_cohort=cum_net_rev_60_cohort/xr, PC2_30_cohort=PC2_30_cohort/xr, PC2_60_cohort=PC2_60_cohort/xr where r.country='COL' and datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co set cost=cost/1.23 where date >= '2013-05-01' and datediff(curdate(), date)<@days;

##update facebook.fanpage_optimization_co set oac_transactions=0, gross_revenue=0, net_revenue=0, new_customers=0, oac_transactions_30_cohort=0, oac_transactions_60_cohort=0, cum_net_rev_30_cohort=0, cum_net_rev_60_cohort=0 where obc_transactions=0 and datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co set obc_transactions=oac_transactions, gross_revenue=net_revenue where obc_transactions=0 and oac_transactions!=0 and datediff(curdate(), date)<@days;

