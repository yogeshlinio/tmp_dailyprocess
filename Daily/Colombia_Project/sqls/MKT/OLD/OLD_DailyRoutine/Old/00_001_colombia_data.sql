
set @days:=60;

set @itemid:= (select max(item) from production_co.tbl_order_detail); 

insert into production_co.tbl_order_detail(yrmonth, date, custid, order_nr, item, unit_price_after_vat, coupon_money_after_vat, shipping_fee_after_vat, costo_after_vat, cost_oms, delivery_cost_supplier, payment_cost, shipping_cost, wh, cs, oac, obc, returned, pending, cancel, rejected, category_bp, source, `source/medium`, channel_group, channel, campaign, new_cat1, new_cat2, new_cat3, payment_method, product_name, buyer, brand, unit_price, coupon_money_value, sku, sku_config, coupon_code, coupon_code_description, is_marketplace, coupon_code_description) select monthnum, date, customernum, ordernum, itemid, priceaftertax, couponvalueaftertax, shippingfee, costaftertax, costaftertax, deliverycostsupplier, paymentfees, shippingcost, flwhcost, flcscost, orderaftercan, orderbeforecan, returns, pending, cancellations, rejected, CatBP, source, source_medium, channelgroup, channel, campaign, cat1, cat2, cat3, paymentmethod, skuname, buyer, brand, price, couponvalue, skusimple, skuconfig, couponcode, couponcodedescription, isMPlace, CouponCodeDescription from development_co_project.A_Master where itemid>@itemid;

update production_co.tbl_order_detail t inner join development_co_project.A_Master o on t.item=o.itemID set t.unit_price_after_vat=o.priceaftertax, t.coupon_money_after_vat=o.couponvalueaftertax, t.shipping_fee_after_vat=o.shippingfee, t.costo_after_vat=o.costaftertax, t.delivery_cost_supplier=o.deliverycostsupplier, t.payment_cost=o.paymentfees, t.shipping_cost=o.shippingcost, t.wh=o.flwhcost, t.cs=o.flcscost, t.oac=o.orderaftercan, t.obc=o.orderbeforecan, t.returned=o.returns, t.pending=o.pending, t.cancel=o.cancellations, t.rejected=o.Rejected, t.category_bp=o.CatBP, t.source=o.Source, t.`source/medium`=o.Source_medium, t.channel_group=o.ChannelGroup, t.channel=o.Channel, t.campaign=o.Campaign, t.is_marketplace=o.isMPlace, t.coupon_code_description=o.couponcodedescription where datediff(curdate(), t.date)<@days;

-- New Customers

update production_co.tbl_order_detail set new_customers= null;

update production_co.tbl_order_detail set new_customers_gross= null;

create table marketing_report.new_customers_freeze select itemid, ordernum, newreturning, count(*) as items, 0 as total, 0.0 as percent from development_co_project.A_Master where newreturning='NEW' group by itemid;

create index itemid on marketing_report.new_customers_freeze(itemid, ordernum);

update marketing_report.new_customers_freeze f set total = (select count(*) from development_co_project.A_Master a where a.ordernum=f.ordernum);

update marketing_report.new_customers_freeze set percent=items/total;

update production_co.tbl_order_detail t inner join marketing_report.new_customers_freeze f on t.item=f.itemid set t.new_customers=f.percent;

drop table marketing_report.new_customers_freeze;

-- New Customers Gross

create table marketing_report.new_customers_freeze select itemid, ordernum, newreturning, count(*) as items, 0 as total, 0.0 as percent from development_co_project.A_Master where newreturninggross='NEW' group by itemid;

create index itemid on marketing_report.new_customers_freeze(itemid, ordernum);

update marketing_report.new_customers_freeze f set total = (select count(*) from development_co_project.A_Master a where a.ordernum=f.ordernum);

update marketing_report.new_customers_freeze set percent=items/total;

update production_co.tbl_order_detail t inner join marketing_report.new_customers_freeze f on t.item=f.itemid set t.new_customers_gross=f.percent;

drop table marketing_report.new_customers_freeze;

update production_co.tbl_order_detail set new_customers=0 where new_customers is null;

update production_co.tbl_order_detail set new_customers_gross=0 where new_customers_gross is null;

/*drop table production_co.tbl_order_detail;

create table production_co.tbl_order_detail as select * from bazayaco.tbl_order_detail;

alter table production_co.tbl_order_detail 
add PRIMARY KEY (`Item`),
add KEY `CustID` (`CustID`),
add KEY `OrderID` (`OrderID`),
add KEY `inv_2` (`order_nr`),
add KEY `sku` (`sku`),
add KEY `ciudad` (`ciudad`),
add KEY `ind_sku_2` (`sku`),
add KEY `sku_config` (`sku_config`),
add KEY `fk_shipment_zone_mapping` (`fk_shipment_zone_mapping`),
add KEY `payment_method` (`payment_method`),
add KEY `campaign` (`campaign`),
add KEY `source_medium` (`source/medium`),
add KEY `supplier` (`proveedor`),
add KEY `is_marketplace` (`is_marketplace`),
add KEY `order_nr` (`order_nr`),
add KEY `coupon_code` (`coupon_code`),
add KEY `channel_group` (`channel_group`,`channel`,`date`),
add KEY `nets` (`oac`,`returned`,`rejected`);*/

##update production_co.tbl_order_detail t inner join SEM.transaction_id_co c on t.order_nr=c.transaction_id set t.`source/medium`= concat(c.source,' / ',c.medium),t.campaign=c.campaign where datediff(curdate(), t.date)<@days;

update development_co_project.A_Master a inner join marketing_report.channel_report_co c on a.ordernum=c.ordernum set a.source_medium=c.source_medium, a.source=c.source, a.campaign=c.campaign, a.channelgroup=c.channel_group, a.channel=c.channel;

UPDATE production_co.tbl_order_detail t inner join production_co.mobileapp_transaction_id m on t.order_nr=m.transaction_id set channel = 'Mobile App';

UPDATE production_co.tbl_order_detail SET channel_group='Mobile App' WHERE channel='Mobile App';


INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'production_co.tbl_order_detail',
  NOW(),
  MAX(date),
  count(*),
  count(*)
FROM
  production.tbl_order_detail
;

