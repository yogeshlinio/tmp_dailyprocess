
delete from SEM.sem_filter_campaign_keyword_co;

insert into SEM.sem_filter_campaign_keyword_co select 'SEM', x.date, x.campaign, x.source, x.medium, x.ad_group, x.keyword, x.impressions, x.clicks, x.visits, x.ad_cost, x.bounce, x.cart from SEM.campaign_keyword_co x where source='google' and medium='CPC' and campaign not like '%r.%' and campaign not like '%d.%' and campaign not like '%w.%' and campaign not like '%[D%' and campaign not like '%brandb.%';

insert into SEM.sem_filter_campaign_keyword_co select 'GDN', x.date, x.campaign, x.source, x.medium, x.ad_group, x.keyword, x.impressions, x.clicks, x.visits, x.ad_cost, x.bounce, x.cart from SEM.campaign_keyword_co x where source='google' and medium='cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%');

delete from SEM.sem_filter_transaction_id_keyword_co;

insert into SEM.sem_filter_transaction_id_keyword_co select 'SEM', x.* from SEM.transaction_id_keyword_co x where source='google' and medium='CPC' and campaign not like '%r.%' and campaign not like '%d.%' and campaign not like '%w.%' and campaign not like '%[D%' and campaign not like '%brandb.%';

insert into SEM.sem_filter_transaction_id_keyword_co select 'GDN', x.* from SEM.transaction_id_keyword_co x where source='google' and medium='cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%');
