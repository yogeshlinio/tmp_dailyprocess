call marketing_co;

INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Colombia',
  'marketing_report.marketing_co',
  NOW(),
  NOW(),
  1,
  1
;

