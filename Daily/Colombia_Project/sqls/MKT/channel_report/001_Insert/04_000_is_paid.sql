USE marketing_co;

UPDATE channel_report
INNER JOIN marketing.channel 
ON channel_report.fk_channel = channel.id_channel
SET channel_report.is_paid = channel.is_paid;