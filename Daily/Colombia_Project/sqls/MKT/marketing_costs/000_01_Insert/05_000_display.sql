USE marketing_co;

set @country = 'COL';
set @country2 = 'Colombia';

#Exoclick
drop temporary table if exists total;
create  temporary table total (index(date,  fk_channel))
select date, fk_channel, country, sum(gross_revenue) rev, count(1) uds
from channel_performance
where channel = 'Exoclick'
group by date, fk_channel, country;


drop temporary table if exists costos;
create  temporary table costos (index(date, id_channel))
select date, @country country,id_channel, sum(cost) as spent 
from marketing_report.exoclick
inner join marketing.channel
where channel = 'Exoclick'
group by date;

update channel_performance cp
inner join 
total t
on cp.date = t.date
and cp.fk_channel = t.fk_channel
and cp.country = t.country
inner join
costos t2
on cp.date = t2.date
and cp.fk_channel = t2.id_channel
and cp.country = t2.country
set cp.marketing_cost = if(t.rev = 0, spent/t.uds, spent*gross_revenue/t.rev);

#ICCK
drop temporary table if exists total;
create  temporary table total (index(date,  fk_channel, country))
select date, fk_channel, country, sum(gross_revenue) rev, count(1) uds
from channel_performance
where channel = 'ICCK'
group by date, fk_channel, country;


drop temporary table if exists costos;
create  temporary table costos (index(date, id_channel))
select date, @country country,id_channel, sum(clicks)*250 as spent 
from marketing_report.icck
inner join marketing.channel
where channel = 'ICCK'
and country=@country2 
group by date;

update channel_performance cp
inner join 
total t
on cp.date = t.date
and cp.fk_channel = t.fk_channel
and cp.country = t.country
inner join
costos t2
on cp.date = t2.date
and cp.fk_channel = t2.id_channel
and cp.country = t2.country
set cp.marketing_cost =if(t.rev = 0, spent/t.uds, spent*gross_revenue/t.rev);
