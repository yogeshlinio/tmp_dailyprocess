
USE @marketing@;

SET @country :='@v_country@';


#Vizury
drop temporary table if exists aux_vizury;
create temporary table aux_vizury (index(date),index(country))
select sum(rev*orderbeforecan-rev*cancelled-ShippingCost)*.08 cost, a.date, a.country ,a.monthnum
from @development@.A_Master a
inner join
marketing_report.vizury_report b
on a.ordernum=b.order_id
where a.country=substring('@counstr_v@',1,3)
group by a.date;

drop temporary table if exists aux_cp;
create temporary table aux_cp (index(date),index(country))
select sum(gross_revenue) as total_gr, date ,country, count(1) cuantos, yrmonth
from channel_performance 
where country=substring('@counstr_v@',1,3) and channel='Vizury' group by date;

update channel_performance g	
inner join
aux_cp gr
on gr.date=g.date and gr.country=g.country 
inner join  
aux_vizury t
on gr.date=t.date 
set g.marketing_cost=t.cost*if(gr.total_gr=0,1,g.gross_revenue)/if(gr.total_gr=0,gr.cuantos,gr.total_gr)
where g.channel='Vizury'
;


	
#sociomantic
drop temporary table if exists aux_socio;
create temporary table aux_socio (index(date),index(country))
select sum(cost) cost, date, country
from marketing_report.sociomantic 
where  country=substring('@counstr_v@',1,3)
group by date, country;

drop temporary table if exists aux_cp;
create temporary table aux_cp (index(date),index(country))
select sum(gross_revenue) as total_gr, country, date, count(1) cuantos
from channel_performance 
where channel='Sociomantic' 
group by date, country;


update channel_performance g	
inner join
aux_socio t
on g.date=t.date and g.country=t.country 
inner join 
aux_cp gr
on gr.date=t.date and gr.country=t.country 
inner join
development_mx.A_E_BI_ExchangeRate er 
on date_format(t.date,'%Y%m')=er.month_num and gr.country=t.country
set g.marketing_cost=t.cost*if(gr.total_gr=0,1,g.gross_revenue)/if(gr.total_gr=0,gr.cuantos,gr.total_gr)*er.xr
where g.channel='Sociomantic'
and er.country=t.country;

	

	
#Triggit	
drop temporary table if exists aux_triggit;
create temporary table aux_triggit (index(date))
select sum(clicks)*cost_click cost, report_start_date as date
from marketing_report.triggit_mx
inner join 
marketing_report.costo_click_triggit cct
on cct.country=@country
group by date;

drop temporary table if exists aux_cp;
create temporary table aux_cp (index(date),index(country))
select sum(gross_revenue) as total_gr, country, date, count(1) cuantos
from channel_performance where channel='Triggit' group by date, country;

update channel_performance g	
inner join
aux_cp gr
on gr.date=g.date and gr.country=g.country 
inner join  
aux_triggit t
on gr.date=t.date 
inner join
development_mx.A_E_BI_ExchangeRate_USD er 
on date_format(t.date,'%Y%m')=er.month_num
set g.marketing_cost=t.cost*if(gr.total_gr=0,1,g.gross_revenue)/if(gr.total_gr=0,gr.cuantos,gr.total_gr)*er.xr
where g.channel='Triggit'
and er.country=gr.country 
;




#Criteo

drop temporary table if exists aux_criteo;
create temporary table aux_criteo (index(date))
select sum(cost) cost, date
from marketing_report.criteo
group by date;

drop temporary table if exists aux_cp;
create temporary table aux_cp (index(date),index(country))
select sum(gross_revenue) as total_gr, country, date, count(1) cuantos
from channel_performance where channel='Criteo' group by date;

update channel_performance g	
inner join
aux_cp gr
on gr.date=g.date and gr.country=g.country 
inner join  
aux_criteo t
on gr.date=t.date 
inner join
development_mx.A_E_BI_ExchangeRate_USD er 
on date_format(t.date,'%Y%m')=er.month_num and er.country=gr.country
set g.marketing_cost=t.cost*if(gr.total_gr=0,1,g.gross_revenue)/if(gr.total_gr=0,gr.cuantos,gr.total_gr)*er.xr
where g.channel='Criteo';