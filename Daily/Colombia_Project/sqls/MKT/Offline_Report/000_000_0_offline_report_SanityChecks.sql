INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia_Project', 
  'offline_report',
  "start",
  NOW(),
  NOW(),
  count(*),
  count(*)
FROM
marketing_report.offline_report
;


