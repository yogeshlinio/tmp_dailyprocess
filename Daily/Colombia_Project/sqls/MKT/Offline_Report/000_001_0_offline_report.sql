USE development_co_project;

DROP TABLE IF EXISTS netas;
DROP TABLE IF EXISTS gross;

CREATE TEMPORARY TABLE IF NOT EXISTS netas AS (SELECT itemid, OrderAfterCan, Rev, a.country
   , a.date, a.MonthNum
   , a.ordernum, a.SKUSimple, a.SKUName
   , a.shippingFee, a.PaidPriceAfterTax, a.pcone
   , a.PCOnePFive, a.PCTwo, a.returns, a.PriceAfterTax
   , a.price, a.originalprice, NetItemsInOrder, couponcode, b.from_date, b.to_date, b.discount_amount, c.Value
   , a.cat1, a.cat2
   , if (a.NewReturning='NEW',1,0) / ItemsInOrder as "NewCustomer"
   , if (a.NewReturning='RETURNING',1,0) / ItemsInOrder as "ReturningCustomer"
   , q.discount_percentage, q.duration_in_days
   FROM development_co_project.A_Master a 
 INNER JOIN bob_live_co.sales_rule b
 ON a.CouponCode=b.code
inner join bob_live_co.sales_rule_set q
on  b.fk_sales_rule_set=q.id_sales_rule_set
 LEFT JOIN development_mx.M_Offline c
 ON a.CouponCode=c.TypeCost
   WHERE a.OrderAfterCan=1 and a.couponcodedescription like 'Offline%'
   GROUP BY a.itemid);

ALTER TABLE netas ADD INDEX (itemid); 
ALTER TABLE netas ADD INDEX (couponcode); 


CREATE TEMPORARY TABLE IF NOT EXISTS gross AS (SELECT itemid, OrderBeforeCan, Rev, a.country
   , a.date, a.MonthNum
   , a.ordernum, a.SKUSimple, a.SKUName
   , a.shippingFee, a.PaidPriceAfterTax, a.pcone
   , a.PCOnePFive, a.PCTwo, a.returns, a.PriceAfterTax
   , a.price, a.originalprice, ItemsInOrder, couponcode, b.from_date, b.to_date, b.discount_amount, c.Value
   , a.cat1, a.cat2
   , if (a.NewReturning='NEW',1,0) / ItemsInOrder as "NewCustomer"
   , if (a.NewReturning='RETURNING',1,0) / ItemsInOrder as "ReturningCustomer"
   , q.discount_percentage, q.duration_in_days
   FROM development_co_project.A_Master a 
 INNER JOIN bob_live_co.sales_rule b
 ON a.CouponCode=b.code
inner join bob_live_co.sales_rule_set q
on  b.fk_sales_rule_set=q.id_sales_rule_set
 LEFT JOIN development_mx.M_Offline c
 ON a.CouponCode=c.TypeCost
   WHERE a.OrderBeforeCan=1 and a.couponcodedescription like 'Offline%'
   GROUP BY a.itemid);

ALTER TABLE gross ADD INDEX (itemid); 
ALTER TABLE netas ADD INDEX (couponcode);

TRUNCATE TABLE marketing_report.offline_report;

INSERT INTO marketing_report.offline_report (ID
										   , country
										   , date
										   , couponcode
										   , cat1
										   , cat2
										   , MonthNum
										   , from_date
										   , to_date
										   , discount_amount
										   , TotalDiscount
										   , MarketingCost
										   , GrossRev
										   , GrossRevUSD
										   , NetRev
										   , NetRevUSD
										   , PC1
										   , PC1USD
										   , PC15
										   , PC15USD
										   , PC2
										   , PC2USD
										   , PaidPrice
										   , PaidPriceUSD
										   , VoucherRate
										   , NetOrders
										   , GrossOrders
										   , NewCustomer
										   , ReturningCustomer
										   , TotalOrders
										   , TotalItems
										   , AvgBasketSize
										   , AvgTicketSize
										   , AvgTicketSizeUSD
										   , DiscountPerc
										   , DurationInDays)
SELECT 0, h.country, h.date
 , h.couponcode
 , h.cat1
 , h.cat2
 , h.MonthNum
 , h.from_date
 , h.to_date
 , h.discount_amount
 , sum(h.discount_amount) TotalDiscount
 , h.Value as MarketingCost
 , sum(h.GrossRev), sum(h.GrossRevUSD)
 , sum(h.NetRev), sum(h.NetRevUSD)
 , sum(h.pc1), sum(h.pc1USD), sum(h.pc15), sum(h.pc15USD), sum(h.pc2), sum(h.pc2USD)
 , sum(h.PaidPrice), sum(h.PaidPriceUSD)
 , h.VoucherRate
 , ifnull(sum(h.NumXOrder), 0) AS "Net Orders"
 , ifnull(sum(h.NumXOrderGross), 0) AS "Gross Orders"
 , Sum(h.NewCustomer)
 , Sum(h.ReturningCustomer)
 , count(distinct ordernum) as "TotalOrders"
 , count(distinct ItemID) as "TotalItems"
 , count(distinct ItemID) / count(distinct ordernum) as "AvgBasketSize"
 , sum(h.NetRev) / count(distinct ordernum) as "AvgTicketSize"
 , sum(h.NetRevUSD) / count(distinct ordernum) as "AvgTicketSizeUSD"
 , h.DiscountPerc, h.DurationInDays
FROM (SELECT IF( m.Country is null, l.Country, m.Country) as "Country"
 , IF( m.Date is null, l.Date, m.Date) as "Date"
 , IF( m.MonthNum is null, l.MonthNum , m.MonthNum) as "MonthNum"
 , IF( m.ordernum is null, l.ordernum, m.ordernum) as "ordernum"
 , IF( m.ItemID is null, l.ItemID, m.ItemID) as "ItemID"
 , IF( m.SKUSimple is null, l.SKUSimple, m.SKUSimple) as "SKUSimple"
 , IF( m.SKUName is null, l.SKUName, m.SKUName) as "SKUName"
 , (SUM( l.PaidPriceAfterTax) + SUM( l.ShippingFee)) "GrossRev"
 , (SUM( l.PaidPriceAfterTax) + SUM( l.ShippingFee)) / u.XR "GrossRevUSD"
 , (SUM( l.PaidPriceAfterTax) + SUM( l.ShippingFee)) / e.XR "GrossRevEUR"
 , IF( m.Rev is null , 0, m.Rev) as "NetRev"
 , IF( m.Rev is null , 0, m.Rev) / u.XR as "NetRevUSD"    
 , IF( m.Rev is null , 0, m.Rev) / e.XR as "NetRevEUR"
    , IF( m.pcone is null, l.pcone, m.pcone) as "pc1"
 , IF( m.pcone is null, l.pcone, m.pcone) / u.XR as "pc1USD" 
 , IF( m.pcone is null, l.pcone, m.pcone) / e.XR as "pc1(EUR)" 
 , IF( m.PCOnePFive is null, l.PCOnePFive, m.PCOnePFive) as "pc15"
 , IF( m.PCOnePFive is null, l.PCOnePFive, m.PCOnePFive) / u.XR as "pc15USD"
 , IF( m.PCOnePFive is null, l.PCOnePFive, m.PCOnePFive) / e.XR as "pc15(EUR)"
    , IF( m.PCTwo is null, l.PCTwo, m.PCTwo) as "pc2"
 , IF( m.PCTwo is null, l.PCTwo, m.PCTwo) / u.XR as "pc2USD"
 , IF( m.PCTwo is null, l.PCTwo, m.PCTwo) / e.XR as "pc2(EUR)"
 , IFNULL(CAST(m.OrderAfterCan AS DECIMAL), 0) as "NetOrders"
 , IFNULL(CAST(l.OrderBeforeCan AS DECIMAL), 0) as "GrossOrders"
 , IF( m.PaidPriceAfterTax is null, l.PaidPriceAfterTax, m.PaidPriceAfterTax) AS "PaidPrice"
 , IF( m.PaidPriceAfterTax is null, l.PaidPriceAfterTax, m.PaidPriceAfterTax) / u.XR AS "PaidPriceUSD"
 , IF( m.PaidPriceAfterTax is null, l.PaidPriceAfterTax, m.PaidPriceAfterTax) / e.XR AS "PaidPrice (EUR)"
 , (1- (IF( m.price is null, l.price, m.price) / IF( m.originalprice is null, l.originalprice, m.originalprice))) " VoucherRate"
 , IF( m.ShippingFee is null, l.ShippingFee, m.ShippingFee) as "ShippingFeeAfterTax"
 , IF( m.ShippingFee is null, l.ShippingFee, m.ShippingFee) / u.XR as "ShippingFeeAfterTax (USD)"
 , IF( m.ShippingFee is null, l.ShippingFee, m.ShippingFee) / e.XR as "ShippingFeeAfterTax (EUR)"
 , IF( m.price is null, l.price, m.price) as "price"
 , IF( m.price is null, l.price, m.price) / u.XR as "price (USD)"
 , IF( m.price is null, l.price, m.price) / e.XR as "price (EUR)"
 , IF( m.PaidPriceAfterTax is null, l.PaidPriceAfterTax, m.PaidPriceAfterTax) / IF( m.price is null, l.price, m.price) as "Voucher Rate"
 , IFNULL(CAST(m.OrderAfterCan AS DECIMAL), 0) / m.NetItemsInOrder as "NumXOrder"
 , IFNULL(CAST(l.OrderBeforeCan AS DECIMAL), 0) / l.ItemsInOrder as "NumXOrderGross"
 , l.couponcode, l.from_date, l.to_date, l.discount_amount, l.Value, l.cat1, l.cat2
 , IF( m.NewCustomer is null, l.NewCustomer, m.NewCustomer) as "NewCustomer"
 , IF( m.ReturningCustomer is null, l.ReturningCustomer, m.ReturningCustomer) as "ReturningCustomer"
 , IF ( m.discount_percentage is null, l.discount_percentage, m.discount_percentage) as "DiscountPerc"
 , IF ( m.duration_in_days is null, l.duration_in_days, m.duration_in_days) as "DurationInDays"
-- select count(*)
FROM netas m
RIGHT JOIN gross l
ON m.itemid = l.itemid
INNER JOIN development_mx.A_E_BI_ExchangeRate e
ON l.MonthNum = e.Month_Num
AND l.country = e.country
INNER JOIN development_mx.A_E_BI_ExchangeRate_USD u
ON l.MonthNum = u.Month_Num
AND l.country = u.country
GROUP BY l.itemid) h
GROUP BY date, couponcode, cat1, cat2;

CALL marketing_report.offline_report_co();