UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
SET a.vol_weight_carrier= IF(shipping_carrier='DESPACHOS SERVIENTREGAS',
													(((a.package_width*a.package_length*a.package_height)*222)/1000000),
												IF(a.shipping_carrier='DESPACHO DESPRISA',
													(((a.package_width*a.package_length*a.package_height)*400)/1000000),
													(((a.package_width*a.package_length*a.package_height)*222)/1000000)));

