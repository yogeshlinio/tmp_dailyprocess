#Se implementa aqui, pues aunque es una extracción depende de una transformación
UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN @procurement_live@.wms_imported_dropship_orders
ON a.item_id = wms_imported_dropship_orders.fk_sales_order_item
JOIN @procurement_live@.procurement_order_item_dropshipping 
ON wms_imported_dropship_orders.id_wms_imported_dropship_order = procurement_order_item_dropshipping.fk_wms_imported_dropship_order
JOIN @procurement_live@.procurement_order_item
ON procurement_order_item_dropshipping.fk_procurement_order_item = procurement_order_item.id_procurement_order_item
JOIN @procurement_live@.procurement_order
ON procurement_order_item.fk_procurement_order = procurement_order.id_procurement_order
SET 
 a.date_po_created = procurement_order.created_at,
 a.date_po_issued = procurement_order.sent_at
WHERE a.fulfillment_type_real = "dropshipping";