# CAMBIAR A id_sales_order_item TABLE!!
#stockout_recovered =Es la orden nueva que se recupero despues de un stockout.
UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
JOIN (select stockout_order_nr from itens_venda_oot_sample_@v_countryPrefix@ where stockout_order_nr <> '') as b
ON b.stockout_order_nr = a.order_number
SET
a.stockout_recovered = 1
WHERE a.status_wms in ('Expedido');

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
SET a.stockout_real = 1
WHERE a.is_stockout = 1;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
JOIN (SELECT order_number FROM itens_venda_oot_sample_@v_countryPrefix@ WHERE stockout_recovered =1) as b
ON a.stockout_order_nr=b.order_number
SET a.stockout_real = 0
WHERE a.is_stockout = 1;

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
JOIN operations_@v_countryPrefix@.tbl_orders_reo_rep_inv AS b
ON a.item_id=b.item_original
SET
a.stockout_recovered_voucher = b.coupon_code,
a.stockout_item_id = b.item_nueva
WHERE b.coupon_code like 'REO%';