UPDATE itens_venda_oot_sample_@v_countryPrefix@ a
JOIN procurement_live_@v_countryPrefix@.catalog_supplier_attributes c
ON a.supplier_id = c.fk_catalog_supplier
JOIN procurement_live_@v_countryPrefix@.fos_user d
ON c.fk_user = d.id
SET a.procurement_analyst = d.username;