UPDATE itens_venda_oot_sample_@v_countryPrefix@ as t1
INNER JOIN 
(SELECT a.item_id,c.changed_at FROM itens_venda_oot_sample_@v_countryPrefix@  as a
INNER JOIN @v_wmsprod@.inverselogistics_devolucion as b
ON a.item_id=b.item_id
INNER JOIN @v_wmsprod@.inverselogistics_status_history as c
ON b.id=c.return_id
LEFT JOIN
	(SELECT a.order_number,a.item_id,c.status,c.changed_at,c.id FROM itens_venda_oot_sample_@v_countryPrefix@ as a
	INNER JOIN @v_wmsprod@.inverselogistics_devolucion as b
	ON a.item_id=b.item_id
	INNER JOIN @v_wmsprod@.inverselogistics_status_history as c
	ON b.id=c.return_id
	WHERE c.status=2
	GROUP BY a.item_id
	ORDER by changed_at ASC) AS primer_aguardando_retorno
ON c.id=primer_aguardando_retorno.id
WHERE c.status=2
AND primer_aguardando_retorno.id IS NULL
GROUP BY item_id
ORDER by c.changed_at ASC) AS t2
ON t1.item_id=t2.item_id
SET t1.date_first_track_il=t2.changed_at;

DROP TEMPORARY TABLE IF EXISTS operations_@v_countryPrefix@.TMP2 ; 
CREATE TEMPORARY TABLE operations_@v_countryPrefix@.TMP2 
SELECT
		b.return_id,
		b. STATUS,
		max(b.changed_at) as cambiado ,
		c.item_id
	FROM
		@v_wmsprod@.inverselogistics_status_history b
	JOIN @v_wmsprod@.inverselogistics_devolucion c ON b.return_id = c.id
WHERE b.STATUS = 2
Group by item_id;

create index item_id on operations_@v_countryPrefix@.TMP2(item_id);

UPDATE itens_venda_oot_sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.TMP2
ON a.item_id = TMP2.item_id
SET a.date_last_track_il = TMP2.cambiado;
   

