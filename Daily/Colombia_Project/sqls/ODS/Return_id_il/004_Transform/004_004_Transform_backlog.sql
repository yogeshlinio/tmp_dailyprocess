UPDATE return_il_Sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays AS b
	ON DATE(a.date_customer_request_il)= b.date_first
		AND DATE(curdate())= b.date_last
SET 
backlog_track_il = IF(b.workdays >2,1,0),
workdays_backlog_track_il = IF(b.workdays >2,(b.workdays-2),0)
WHERE date_first_track_il IS NULL
AND date_inbound_il IS NULL
AND date_quality_il IS NULL
AND date_closed_il IS null
AND date_customer_request_il IS NOT NULL;


UPDATE return_il_Sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays AS b
	ON DATE(a.date_inbound_il)= b.date_first
		AND DATE(curdate())= b.date_last
SET 
backlog_quality_il = IF(b.workdays >3,1,0),
workdays_backlog_quality_il = IF(b.workdays >3,(b.workdays-3),0)
WHERE date_quality_il IS NULL
AND date_closed_il IS NULL
AND is_returned=1;


UPDATE return_il_Sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays AS b
	ON date(a.date_quality_il)= b.date_first
		AND date(curdate())= b.date_last
SET 
backlog_refund_reversion_il = IF(b.workdays>20,1,0),
workdays_backlog_refund_reversion_il = IF(b.workdays>20,(b.workdays-20),0)
WHERE date_cash_refunded_il IS NULL
AND date_voucher_refunded_il IS NULL
AND date_closed_il IS NULL
AND date_quality_il IS NOT NULL
AND is_returned = 1
AND action_il='Reversión TC (25-30 dias hábiles)';

UPDATE return_il_Sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays AS b
	ON date(a.date_quality_il)= b.date_first
		AND date(curdate())= b.date_last
SET 
backlog_refund_voucher_il = IF(b.workdays>2,1,0),
workdays_backlog_refund_voucher_il = IF(b.workdays>2,(b.workdays-2),0)
WHERE date_cash_refunded_il IS NULL
AND date_voucher_refunded_il IS NULL
AND date_closed_il IS NULL
AND date_quality_il IS NOT NULL
AND is_returned =1
AND action_il='Voucher (5 días hábiles)';



UPDATE return_il_Sample_@v_countryPrefix@ AS t1
INNER JOIN
(SELECT numero_guia,fecha_envio
FROM outbound_co.tbl_outbound_daily_servientrega
GROUP BY numero_guia) as t2
ON t1.shipping_carrier_tracking_code_inverse = t2.numero_guia
SET t1.return_receivedserv = 1,
t1.date_inbound_carrier_il = date(t2.fecha_envio);

UPDATE return_il_Sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays AS b
	ON date(a.date_inbound_carrier_il)= b.date_first
		AND date(curdate())= b.date_last
SET 
backlog_currier_il = IF(((return_receivedserv = 1) AND (date_inbound_il IS NULL)),1,0),
workdays_backlog_currier_il = IF(((return_receivedserv = 1) AND (date_inbound_il IS NULL)),b.workdays,0)
WHERE is_returned=1
AND date_quality_il IS NULL
AND date_closed_il IS NULL;

UPDATE return_il_Sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays AS b
	ON date(a.date_quality_il)= b.date_first
		AND date(curdate())= b.date_last
SET 
backlog_refund_consignment_il = IF(b.workdays>5,1,0),
workdays_backlog_refund_consignment_il = IF(b.workdays>5,(b.workdays-5),0)
WHERE date_cash_refunded_il IS NULL
AND date_voucher_refunded_il IS NULL
AND date_closed_il IS NULL
AND date_quality_il IS NOT NULL
AND is_returned =1
AND action_il='Consignacion bancaria (7 dias hábiles)';

UPDATE return_il_Sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays AS b
	ON date(a.date_quality_il)= b.date_first
		AND date(curdate())= b.date_last
SET 
backlog_refund_effecty_il = IF(b.workdays>4,1,0),
workdays_backlog_refund_effecty_il = IF(b.workdays>4,(b.workdays-4),0)
WHERE date_cash_refunded_il IS NULL
AND date_voucher_refunded_il IS NULL
AND date_closed_il IS NULL
AND date_quality_il IS NOT NULL
AND is_returned =1
AND action_il='Efecty (7 dias hábiles)';

