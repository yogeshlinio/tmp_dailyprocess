
UPDATE return_il_Sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays AS b
	ON a.date_customer_request_il= b.date_first
	AND a.date_first_track_il= b.date_last
SET on_time_track_il = 1
WHERE b.isweekday = 1 
AND b.isholiday = 0
AND is_returned =1
AND b.workdays<=2;

UPDATE return_il_Sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays AS b
	ON a.date_inbound_carrier_il= b.date_first
	AND a.date_inbound_il= b.date_last
SET on_time_currier_il = 1
WHERE b.isweekday = 1 
AND b.isholiday = 0
AND is_returned =1
AND b.workdays<=1;

UPDATE return_il_Sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays AS b
	ON a.date_inbound_il= b.date_first
	AND a.date_quality_il= b.date_last
SET on_time_quality_il = 1
WHERE b.isweekday = 1 
AND b.isholiday = 0
AND is_returned =1
AND b.workdays<=3;

UPDATE return_il_Sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays AS b
	ON a.date_quality_il= b.date_first
	AND a.date_cash_refunded_il= b.date_last
SET on_time_refund_consignment_il = 1
WHERE b.isweekday = 1 
AND b.isholiday = 0
AND action_il='Consignacion bancaria (7 dias hábiles)'
AND is_returned =1
AND b.workdays<=5;

UPDATE return_il_Sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays AS b
	ON a.date_quality_il= b.date_first
	AND a.date_cash_refunded_il= b.date_last
SET on_time_refund_effecty_il = 1
WHERE b.isweekday = 1 
AND b.isholiday = 0
AND action_il='Efecty (7 dias hábiles)'
AND is_returned =1
AND b.workdays<=4;

UPDATE return_il_Sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays AS b
	ON a.date_quality_il= b.date_first
	AND a.date_cash_refunded_il= b.date_last
SET on_time_refund_reversion_il = 1
WHERE b.isweekday = 1 
AND b.isholiday = 0
AND action_il='Reversión TC (25-30 dias hábiles)'
AND is_returned =1
AND b.workdays<=20;

UPDATE return_il_Sample_@v_countryPrefix@ AS a
INNER JOIN operations_@v_countryPrefix@.calcworkdays AS b
	ON a.date_quality_il= b.date_first
	AND a.date_voucher_refunded_il= b.date_last
SET on_time_refund_voucher_il = 1
WHERE b.isweekday = 1 
AND b.isholiday = 0
AND action_il='Voucher (5 días hábiles)'
AND is_returned =1
AND b.workdays<=2;
  





