DROP TEMPORARY TABLE IF EXISTS operations_@v_countryPrefix@.TMP3analyst ; 
CREATE TEMPORARY TABLE operations_@v_countryPrefix@.TMP3analyst
SELECT d.*, e.nome
FROM
(SELECT
		b.user_id,
		b.return_id,
		b. STATUS,
		b.changed_at,
		c.item_id
	FROM
		@v_wmsprod@.inverselogistics_status_history b
	JOIN @v_wmsprod@.inverselogistics_devolucion c ON b.return_id = c.id) d 
INNER JOIN @v_wmsprod@.usuarios as e ON d.user_id = e.usuarios_id
WHERE
	d. STATUS = 6;
	
create index item on operations_@v_countryPrefix@.TMP3analyst(item_id);


UPDATE return_il_Sample_@v_countryPrefix@ as return_il_Sample
INNER JOIN operations_@v_countryPrefix@.TMP3analyst as d ON return_il_Sample.item_id = d.item_id
SET return_il_Sample.analyst_il = d.nome
WHERE
	d. STATUS = 6;
	
UPDATE return_il_Sample_@v_countryPrefix@ as a
INNER JOIN operations_@v_countryPrefix@.tbl_bi_ops_ingreso_bodega_devolucion as b
ON a.item_id=b.item_id
SET 
a.date_inbound_il=b.date_ingreso_bodega_devolucion;

DROP TEMPORARY TABLE IF EXISTS operations_@v_countryPrefix@.guia ; 
CREATE TEMPORARY TABLE operations_@v_countryPrefix@.guia 
SELECT numero_guia,DATE(max(fecha_entrega)) AS fecha_entrega
FROM outbound_@v_countryPrefix@.tbl_outbound_daily_servientrega
WHERE nombre_estadoenvio='ENTREGADO'
AND fecha_entrega <> '1900-01-00 00:00:00'
GROUP BY numero_guia;

create index num on operations_@v_countryPrefix@.guia(numero_guia);

DROP TEMPORARY TABLE IF EXISTS operations_@v_countryPrefix@.devo ; 
CREATE TEMPORARY TABLE operations_@v_countryPrefix@.devo 
SELECT item_id,carrier_tracking_code
FROM @v_wmsprod@.inverselogistics_devolucion;

create index num on operations_@v_countryPrefix@.devo(carrier_tracking_code);

DROP TEMPORARY TABLE IF EXISTS operations_@v_countryPrefix@.hay ; 
CREATE TEMPORARY TABLE operations_@v_countryPrefix@.hay 
Select * from operations_@v_countryPrefix@.devo AS a
LEFT JOIN
operations_@v_countryPrefix@.guia  as b
ON a.carrier_tracking_code=b.numero_guia;

create index up on operations_@v_countryPrefix@.hay(item_id);

SELECT  'update fecha recepcion en bodega devolucion desde archivos servientrega',now();
UPDATE return_il_Sample_@v_countryPrefix@ AS t1
INNER JOIN
operations_@v_countryPrefix@.hay AS t2
ON t1.item_id=t2.item_id
SET t1.date_inbound_il = t2.fecha_entrega
WHERE t1.date_inbound_il IS NULL;

SELECT  'Correccion errores',now();
UPDATE return_il_Sample_@v_countryPrefix@ a
SET a.date_inbound_il  = a.date_quality_il
WHERE
	a.date_inbound_il > a.date_quality_il;


   
