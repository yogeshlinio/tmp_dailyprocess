UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
SET a.purchase_order_type='Crossdocking'
WHERE a.fk_procurement_order_type=6;

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
SET a.purchase_order_type='Own Warehouse'
WHERE a.fk_procurement_order_type=9;

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
SET a.purchase_order_type='Consignment'
WHERE (a.fk_procurement_order_type=7 OR a.fk_procurement_order_type=11);

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
SET a.purchase_order_type='Dropshipping'
WHERE a.fk_procurement_order_type=8;


UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
SET a.purchase_order_type='Marketplace'
WHERE a.fk_procurement_order_type=10;

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
SET a.purchase_order_type='Backorder'
WHERE a.fk_procurement_order_type=12;

UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
SET a.purchase_order_type='Materia Prima'
WHERE a.fk_procurement_order_type=13;