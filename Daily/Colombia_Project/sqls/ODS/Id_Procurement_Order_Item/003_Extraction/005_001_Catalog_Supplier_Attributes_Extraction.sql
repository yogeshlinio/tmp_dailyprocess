UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ AS a
INNER JOIN @procurement_live@.catalog_supplier_attributes AS b 
	ON a.supplier_id = b.fk_catalog_supplier
SET 
 a.catalog_payment_terms = b.payment_terms,
 a.supplier_tax_id = b.nit,
 #a.procurement_analyst = b.buyer_name,
 a.credit_limit = b.credit_limit,
 a.pick_at_city = b.city,
 a.pick_at_region = b.zone;