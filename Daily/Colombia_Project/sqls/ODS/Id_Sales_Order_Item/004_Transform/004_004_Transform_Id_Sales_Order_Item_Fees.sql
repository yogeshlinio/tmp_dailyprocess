#Query: A 113 U Charge/Fee per PM
UPDATE        Id_Sales_Order_Item_Sample_@v_countryPrefix@ AS Id_Sales_Order_Item_Sample
   INNER JOIN @development@.A_E_In_COM_Fees_per_PM 
           ON Id_Sales_Order_Item_Sample.Payment_Method = A_E_In_COM_Fees_per_PM.Payment_Method
SET
   Id_Sales_Order_Item_Sample.Fees         = A_E_In_COM_Fees_per_PM.Fee,
   Id_Sales_Order_Item_Sample.Extra_Charge = A_E_In_COM_Fees_per_PM.Extra_Charge,
   Id_Sales_Order_Item_Sample.Min_Value    = A_E_In_COM_Fees_per_PM.Min_Value;