/*
* Inserta nuevas ordenes VC
*/
INSERT INTO development_co_project.M_Corporative_Sales(`Order`, Items, MonthName, Month_Num, sku_config, sku_simple, sku_name, CostAfterTax, TaxPercent)
SELECT ordernum
	, COUNT(1)
	, DATE_FORMAT(date,'%M')
	, monthnum
	, skuconfig
	, skusimple
	, skuname
	, Id_Sales_Order_Item_Sample_@v_countryPrefix@.CostAfterTax
	, Id_Sales_Order_Item_Sample_@v_countryPrefix@.taxpercent
FROM development_co_project.Id_Sales_Order_Item_Sample_@v_countryPrefix@
LEFT JOIN development_co_project.M_Corporative_Sales
ON Id_Sales_Order_Item_Sample_@v_countryPrefix@.ordernum=M_Corporative_Sales.order
WHERE (couponcode LIKE 'VC%' 
OR couponcode LIKE 'REOVC%' 
OR couponcode LIKE 'DEVVC%' 
OR couponcode LIKE  'REPVC%')
AND OrderBeforeCan=1
AND M_Corporative_Sales.order IS NULL
GROUP BY ordernum,skusimple;

/*
* Actualiza A_Master
*/
UPDATE            Id_Sales_Order_Item_Sample_@v_countryPrefix@  
       INNER JOIN M_Corporative_Sales 
               ON Id_Sales_Order_Item_Sample_@v_countryPrefix@.OrderNum = M_Corporative_Sales.Order 
SET 
   Id_Sales_Order_Item_Sample_@v_countryPrefix@.CostAfterTax  = IF( useCostNegotiate = 1, M_Corporative_Sales.CostNegotiate,  Id_Sales_Order_Item_Sample_@v_countryPrefix@.CostAfterTax  ) , 
   Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCost = IF( useShippingCost = 1, Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingCost, 0 ) , 
   Id_Sales_Order_Item_Sample_@v_countryPrefix@.DeliveryCostSupplier   = IF( useInboundCost = 1, Id_Sales_Order_Item_Sample_@v_countryPrefix@.DeliveryCostSupplier, 0 ) ,
   Id_Sales_Order_Item_Sample_@v_countryPrefix@.FLWHCost = IF( useWH = 1, Id_Sales_Order_Item_Sample_@v_countryPrefix@.FLWHCost , 0 )
;