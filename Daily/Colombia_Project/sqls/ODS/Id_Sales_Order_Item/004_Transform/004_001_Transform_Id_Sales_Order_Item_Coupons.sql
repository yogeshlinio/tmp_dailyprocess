UPDATE            Id_Sales_Order_Item_Sample_@v_countryPrefix@ AS Id_Sales_Order_Item_Sample
       INNER JOIN Id_Sales_Order_Item_Key_Map
	        USING ( Country , Id_Sales_Order_Item )
SET 
   Price         = 19900, 
   Price_After_Tax = 19900/ TaxPercent, 
   Paid_Price  = 9900, 
   Paid_Price_After_Tax = 9900/1.16, 
   Coupon_Value = Price - Paid_Price
 
WHERE 
       Coupon_Code = 'CAC0403' 
   AND Sku_Simple = 'CR610EL90NARLACOL-99496';
   
/*
	Ventas Corporativas
*/
UPDATE  Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET
    Coupon_Value         = 0,
    Coupon_ValueAfterTax = 0
WHERE
    Order_Num IN (200236123,
200123283,
200433763,
200645493,
200588663,
200316963,
200336963,
200616963,
200749963);

/*
  Ventas Extraordinarias
*/
UPDATE        Id_Sales_Order_Item_Sample_@v_countryPrefix@ t
   INNER JOIN @development@.tbl_ventas_extraordinarias v 
           ON Coupon_Code = v.coupon_code
SET
   t.Paid_Price             = (v.paid_price/31),
   t.Paid_Price_After_Tax   = (v.paid_price/31) / ( 1 + tax_Percent / 100 ),   
   t.Price                  = (v.unit_price/31) ,
   t.Price_After_Tax        = (v.unit_price/31) / ( 1 + tax_Percent / 100 ) ,   
   t.Shipping_Fee           = (v.shipping_fee/31),
   t.Shipping_Fee_After_Tax = (v.shipping_fee/31)/ ( 1 + tax_Percent / 100 )    
   
where  
   v.coupon_code not like 'VC%' and date_format( date_order_placed, "%Y%m" ) = 201308;
   
UPDATE Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET 
   Paid_Price            = Coupon_Value, 
   Paid_Price_After_Tax  = Coupon_ValueAfterTax,    
   Coupon_Value          = 0,
   Coupon_ValueAfterTax  = 0 
WHERE Order_Before_Can = 1 
and ( 
        Coupon_Code like 'REO%' 
     or Coupon_Code like 'REP%' 
	 or Coupon_Code like 'INV%' 
	 or Coupon_Code like 'GAR%' 
	 or Coupon_Code like 'CAN%' 
	 or Coupon_Code like 'DEV%'
	 or Coupon_Code like 'CSSDEV%'
    )
;

/*
*  M_Cambios_Costos  
*/
UPDATE Id_Sales_Order_Item_Sample_@v_countryPrefix@ od
  JOIN M_Cambios_Costos cc
    ON od.skuConfig = cc.sku
SET 
    od.Paid_Price           = od.Coupon_Value
  , od.Coupon_Value         = IF(od.Price-od.Coupon_Value<0,0,od.Price-od.Coupon_Value)
  , od.Paid_Price_After_Tax = od.Paid_Price  /(1 + od.Tax_Percent/100)
  , od.Coupon_ValueAfterTax = od.Coupon_Value/(1 + od.Tax_Percent/100)
WHERE 
         cc.is_config=1 
	 AND cc.is_op=0 
	 AND cc.is_coupon=0 
	 AND cc.cost_to_apply=6
     AND od.date >= CASE WHEN no_range=1 THEN od.date 
                         ELSE cc.from_date END 
     AND od.date <= CASE WHEN no_range=1 THEN od.date
                         ELSE cc.to_date END; 

						 
UPDATE      Id_Sales_Order_Item_Sample_@v_countryPrefix@ od
       JOIN M_Cambios_Costos cc
         ON od.skuSimple = cc.sku
SET 
    od.Paid_Price           = od.Coupon_Value
  , od.Coupon_Value         = IF(od.Price-od.Coupon_Value<0,0,od.Price-od.Coupon_Value)
  , od.Paid_Price_After_Tax   = od.Paid_Price/(1 + od.TaxPercent/100)
  , od.Coupon_ValueAfterTax = od.Coupon_Value/(1 + od.TaxPercent/100)
WHERE 
       cc.is_config=0 
   AND cc.is_op=0 
   AND cc.is_coupon=0 
   AND cc.cost_to_apply=6
   AND od.date >= CASE WHEN no_range=1 THEN od.date 
                       ELSE cc.from_date END 
   AND od.date <= CASE WHEN no_range=1 THEN od.date
                       ELSE cc.to_date END; 

UPDATE      Id_Sales_Order_Item_Sample_@v_countryPrefix@ od
       JOIN M_Cambios_Costos cc
         ON od.Coupon_Code = cc.sku
SET 
    od.Paid_Price           = od.Coupon_Value
  , od.Coupon_Value         = IF(od.Price-od.Coupon_Value<0,0,od.Price-od.Coupon_Value)
  , od.Paid_Price_After_Tax = od.Paid_Price/(1 + od.Tax_Percent/100)
  , od.Coupon_ValueAfterTax = od.Coupon_Value/(1 + od.Tax_Percent/100)
WHERE 
        cc.is_config=0 
	AND cc.is_op=0 
	AND cc.is_coupon=1 
	AND cc.cost_to_apply=6
    AND od.date >= CASE WHEN no_range=1 THEN od.date 
                      ELSE cc.from_date END 
    AND od.date <= CASE WHEN no_range=1 THEN od.date
                      ELSE cc.to_date END; 
					  
/*					  
UPDATE Id_Sales_Order_Item_Sample_@v_countryPrefix@ t
JOIN tbl_bi_ops_delivery d
ON t.item=d.item_id
JOIN M_Cambios_Costos cc
ON d.op=cc.sku
SET t.paid_price=cc.cost
  , t.coupon_money_value = IF(t.unit_price-t.coupon_money_value<0,0,t.unit_price-t.coupon_money_value)
  , t.paid_price_after_vat = t.paid_price/(1 + t.tax_percent/100)
  , t.coupon_money_after_vat = t.coupon_money_value/(1 + t.tax_percent/100)
WHERE cc.is_config=0 AND cc.is_op=1 AND cc.is_coupon=0 AND cc.cost_to_apply=6
AND t.date >= CASE WHEN no_range=1 THEN t.date 
                      ELSE cc.from_date END 
  AND t.date <= CASE WHEN no_range=1 THEN t.date
                      ELSE cc.to_date END;
*/
					  

					   