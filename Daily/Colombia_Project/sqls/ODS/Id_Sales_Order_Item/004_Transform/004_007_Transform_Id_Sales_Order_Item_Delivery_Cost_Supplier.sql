UPDATE Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET 
   DeliveryCostSupplier=0.0 
WHERE
   Supplier='IZC MAYORISTA S.A.S';

UPDATE Id_Sales_Order_Item_Sample_@v_countryPrefix@ od
  JOIN M_Cambios_Costos cc
    ON od.skuConfig = cc.sku
SET 
    od.DeliveryCostSupplier = cc.cost
WHERE cc.is_config=1 AND cc.is_op=0 AND cc.is_coupon=0 AND cc.cost_to_apply=2
AND od.date >= CASE WHEN no_range=1 THEN od.date 
                      ELSE cc.from_date END 
  AND od.date <= CASE WHEN no_range=1 THEN od.date
                      ELSE cc.to_date END;
   
UPDATE      Id_Sales_Order_Item_Sample_@v_countryPrefix@ od
       JOIN M_Cambios_Costos cc
         ON od.skuSimple = cc.sku
SET 
    od.DeliveryCostSupplier = cc.cost
WHERE 
        cc.is_config=0 
	AND cc.is_op=0 
	AND cc.is_coupon=0 
	AND cc.cost_to_apply=2
    AND od.date >= CASE WHEN no_range=1 THEN od.date 
                        ELSE cc.from_date END 
    AND od.date <= CASE WHEN no_range=1 THEN od.date
                        ELSE cc.to_date END; 

UPDATE      Id_Sales_Order_Item_Sample_@v_countryPrefix@ od
       JOIN M_Cambios_Costos cc
         ON od.CouponCode = cc.sku
SET 
     od.DeliveryCostSupplier = cc.cost
WHERE 
        cc.is_config=0 
	AND cc.is_op=0 
	AND cc.is_coupon=1 
	AND cc.cost_to_apply=2
    AND od.date >= CASE WHEN no_range=1 THEN od.date 
                      ELSE cc.from_date END 
    AND od.date <= CASE WHEN no_range=1 THEN od.date
                      ELSE cc.to_date END; 
	
