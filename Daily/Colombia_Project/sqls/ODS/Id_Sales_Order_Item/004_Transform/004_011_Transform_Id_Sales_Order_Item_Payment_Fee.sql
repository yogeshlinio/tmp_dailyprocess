SET @ultima_tasa = 0000000000000000.0000;
UPDATE Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET
   PaymentFees = greatest( Id_Sales_Order_Item_Sample_@v_countryPrefix@.GrandTotal * ( Id_Sales_Order_Item_Sample_@v_countryPrefix@.Fees / 100 )
                 +Id_Sales_Order_Item_Sample_@v_countryPrefix@.ExtraCharge,  Id_Sales_Order_Item_Sample_@v_countryPrefix@.Min_Value)
;

SELECT trm into @ultima_tasa
FROM  tbl_paypal_trm_diaria d
WHERE fecha=(SELECT MAX(fecha)
			FROM tbl_paypal_trm_diaria);


UPDATE Id_Sales_Order_Item_Sample_@v_countryPrefix@ t
LEFT JOIN tbl_paypal_trm_diaria d
ON t.date=d.fecha
set t.PaymentFees=CASE WHEN d.fecha IS NULL 
THEN 0.038*(GrandTotal)+(0.3*@ultima_tasa)/CAST(t.NetItemsInOrder AS DECIMAL)
            ELSE 0.038*(GrandTotal)+(0.3*d.trm)/CAST(t.NetItemsInOrder AS DECIMAL) 
       END
WHERE t.PaymentMethod ='Paypal_Express_Checkout' and (t.OrderAfterCan = 1);


UPDATE Id_Sales_Order_Item_Sample_@v_countryPrefix@ t
SET PaymentFees=GrandTotal*0.01/CAST(NetItemsInOrder AS DECIMAL)
WHERE city LIKE '%bogota%' 
AND  PaymentMethod ='CashOnDelivery_Payment' 
AND date <= '2014-03-05'
AND (t.OrderAfterCan = 1);

UPDATE Id_Sales_Order_Item_Sample_@v_countryPrefix@ t
SET PaymentFees=GrandTotal*0.015/CAST(NetItemsInOrder AS DECIMAL)
WHERE city NOT LIKE '%bogota%' 
AND  PaymentMethod ='CashOnDelivery_Payment' 
AND date <= '2014-03-05'
AND (t.OrderAfterCan = 1);

UPDATE Id_Sales_Order_Item_Sample_@v_countryPrefix@ t
SET PaymentFees=0
WHERE PaymentMethod ='CashOnDelivery_Payment' 
AND date > '2014-03-05'
AND (t.OrderAfterCan = 1);

#[Lore]Con la SRT los payment fees de los productos COD est�n incluidos en el shipping cost que tomamos de la SRT

/*
*  Colombia's Adjust
*/
