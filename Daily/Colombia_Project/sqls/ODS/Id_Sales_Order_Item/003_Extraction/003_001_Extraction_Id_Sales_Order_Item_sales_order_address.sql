UPDATE            Id_Sales_Order_Item_Sample_@v_countryPrefix@ AS Id_Sales_Order_Item_Sample
       INNER JOIN Id_Sales_Order_Item_Key_Map
	        USING ( country , Id_Sales_Order_Item )
       INNER JOIN @bob_live@.sales_order_address
	        USING ( id_sales_order_address )
SET
   /*Defaults and Simple Extraction*/
   Id_Sales_Order_Item_Sample.postcode               = sales_order_address.postcode, 
   Id_Sales_Order_Item_Sample.city                   = CASE 
                                                          WHEN         CAST( ifnull(sales_order_address.municipality,sales_order_address.city) AS CHAR ) = 'LA GUAJIRA' 
														           AND CAST( sales_order_address.region AS CHAR )= 'MAICAO'          
							                                  THEN 'Maicao' 
                                                          WHEN         CAST( ifnull(sales_order_address.municipality,sales_order_address.city) AS CHAR ) = 'Bogota' 
														           AND CAST( sales_order_address.region AS CHAR ) = 'Bogota'                             
								                             THEN 'Bogota' 
                                                          WHEN         CAST( ifnull(sales_order_address.municipality,sales_order_address.city) AS CHAR ) = '​BOGOTÁ D.C' 
														           AND CAST( sales_order_address.region AS CHAR ) = 'Bogota'                             
								                             THEN 'Bogota' 
                                                          WHEN         CAST( ifnull(sales_order_address.municipality,sales_order_address.city) AS CHAR ) = '​BOGOTÁ D.C' 
														           AND CAST( sales_order_address.region AS CHAR ) = 'CUNDINAMARCA'                  
								                             THEN 'Bogota' 
                                                          WHEN         CAST( ifnull(sales_order_address.municipality,sales_order_address.city) AS CHAR ) = '​​ARMENIA (QUI)' 
														           AND CAST( sales_order_address.region AS CHAR ) = 'QUINDIO'  
								                             THEN 'Armenia'
                                                          WHEN         CAST( ifnull(sales_order_address.municipality,sales_order_address.city) AS CHAR ) = 'pie de cuesta' 
														           AND CAST( sales_order_address.region AS CHAR ) = 'Santander'
								                             THEN 'Piedecuesta'
                                                          WHEN         CAST( ifnull(sales_order_address.municipality,sales_order_address.city) AS CHAR ) = 'Putumayo' 
														           AND CAST( sales_order_address.region AS CHAR ) = 'Orito'
								                             THEN 'Orito'
                                                          WHEN         CAST( ifnull(sales_order_address.municipality,sales_order_address.city) AS CHAR ) = 'Tolima' 
														           AND CAST( sales_order_address.region AS CHAR ) = 'Ibague'
								                             THEN 'Ibague'
                                                          ELSE ifnull(sales_order_address.municipality,sales_order_address.city)
						                               END
;