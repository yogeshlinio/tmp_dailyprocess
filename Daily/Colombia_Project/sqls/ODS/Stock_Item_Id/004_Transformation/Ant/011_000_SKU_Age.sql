UPDATE operations_co.out_stock_hist
SET out_stock_hist.days_in_stock = 	CASE
																			WHEN 	date_exit IS NULL 
																			THEN	datediff(curdate(), date_entrance)
																			ELSE	datediff(date_exit, date_entrance)
																		END;
TRUNCATE operations_co.skus_age;

INSERT INTO operations_co.skus_age
select * from operations_co.vw_inv_avg_ageing;

UPDATE operations_co.out_stock_hist 
INNER JOIN operations_co.skus_age 
ON 
	out_stock_hist.sku_simple= skus_age.sku_simple 
SET 
	out_stock_hist.sku_age = truncate(skus_age.avg_age,0);