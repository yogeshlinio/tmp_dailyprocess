UPDATE operations_co.out_stock_hist
 INNER JOIN bob_live_co.catalog_config 
	ON out_stock_hist.sku_config = catalog_config.sku
SET 
out_stock_hist.product_measures = catalog_config.product_measures, 
out_stock_hist.package_width = greatest(cast(replace(IF(isnull(catalog_config.package_width),1,catalog_config.package_width),',','.') as DECIMAL(6,2)),1.0), 
out_stock_hist.package_length = greatest(cast(replace(IF(isnull(catalog_config.package_length),1,catalog_config.package_length),',','.') as DECIMAL(6,2)),1.0), 
out_stock_hist.package_height = greatest(cast(replace(IF(isnull(catalog_config.package_height),1,catalog_config.package_height),',','.') as DECIMAL(6,2)),1.0),
out_stock_hist.package_weight = greatest(cast(replace(IF(isnull(catalog_config.package_weight),1,catalog_config.package_weight),',','.') as DECIMAL(6,2)),1.0)
;


UPDATE operations_co.out_stock_hist
SET out_stock_hist.vol_weight = package_height * package_length * package_width / 5000;

UPDATE operations_co.out_stock_hist
SET out_stock_hist.max_vol_w_vs_w = CASE
WHEN vol_weight > package_weight THEN
	vol_weight
ELSE
	package_weight
END;

UPDATE operations_co.out_stock_hist
SET out_stock_hist.package_measure_new = 	CASE
																						WHEN max_vol_w_vs_w > 35 
																						THEN 'oversized'
																					ELSE
																					(	CASE
																							WHEN max_vol_w_vs_w > 5 
																							THEN 'large'
																							ELSE
																							(	CASE
																									WHEN max_vol_w_vs_w > 2 
																									THEN 'medium'
																									ELSE 'small'
																								END)
																							END)
																					END;