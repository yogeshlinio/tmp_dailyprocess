UPDATE operations_co.out_stock_hist
SET out_stock_hist.position_item_size_type = CASE
																							WHEN position_type = "safe" 
																							THEN "small"
																							WHEN position_type = "mezanine" 
																							THEN "small"
																							WHEN position_type = "muebles" 
																							THEN "large"
																							ELSE "tbd"
																						END;