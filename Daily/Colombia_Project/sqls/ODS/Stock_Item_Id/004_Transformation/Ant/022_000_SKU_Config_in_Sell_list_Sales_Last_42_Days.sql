#Sell list por config - 42 días de ventas
TRUNCATE operations_co.sell_rate_config;

INSERT INTO operations_co.sell_rate_config (
	sku_config,
	num_sales,
	num_items,
	average_days_in_stock
) SELECT
	sold.sku_config,
	sold.vendidos,
	instockreal.items_total,
	instockreal.days
FROM
	(	SELECT
			sku_config,
			sum(sold) AS vendidos
		FROM
			operations_co.out_stock_hist
		WHERE
			(	in_stock = 1
				OR exit_type = 'sold')
		AND date_exit BETWEEN CURDATE() - INTERVAL 42 DAY
		AND CURDATE()
		GROUP BY
			sku_config
	) AS sold
 INNER JOIN (
	SELECT
		sku_config,
		sum(item_counter) AS items_total,
		avg(days_in_stock) AS days
	FROM
		operations_co.out_stock_hist
	WHERE
		in_stock = 1
	GROUP BY
		sku_config
) AS instockreal ON sold.sku_config = instockreal.sku_config;

UPDATE operations_co.sell_rate_config
SET num_items_available = (
	SELECT
		sum(in_stock)
	FROM
		operations_co.out_stock_hist
	WHERE	reserved = 0
	AND 	sell_rate_config.sku_config = out_stock_hist.sku_config
	GROUP BY
		sku_config
);

UPDATE operations_co.sell_rate_config
SET average_sell_rate =
IF (num_sales = 0,NULL,	CASE
													WHEN average_days_in_stock = 0 
													THEN 0
													ELSE num_sales / 42
												END
);

UPDATE operations_co.sell_rate_config
SET remaining_days =	IF (average_sell_rate IS NULL,NULL,	CASE															
															WHEN average_sell_rate = 0 
															THEN 0
															ELSE num_items_available / average_sell_rate
															END
);


UPDATE operations_co.out_stock_hist
 INNER JOIN operations_co.sell_rate_config 
	ON out_stock_hist.sku_config = sell_rate_config.sku_config
SET out_stock_hist.average_remaining_days = sell_rate_config.remaining_days;

UPDATE operations_co.out_stock_hist 
 INNER JOIN operations_co.sell_rate_config 
	ON out_stock_hist.sku_config = sell_rate_config.sku_config 
SET 
	out_stock_hist.sales_rate_config_42 = ((sell_rate_config.num_sales/42)/sell_rate_config.num_items_available);
