UPDATE operations_co.out_stock_hist
 INNER JOIN development_co_project.A_Master_Catalog 
	ON out_stock_hist.sku_simple 	= A_Master_Catalog.sku_simple
SET 
	out_stock_hist.content_qc 			= A_Master_Catalog.pet_approved,
	out_stock_hist.status_config 		= A_Master_Catalog.status_config,
	out_stock_hist.status_simple 		= A_Master_Catalog.status_simple,
	out_stock_hist.status_pet = IF(A_Master_Catalog.pet_status IS NULL, 'Pending All', 
IF(A_Master_Catalog.pet_status = "creation,edited", "Pending Images", 
IF(A_Master_Catalog.pet_status = "creation,images", "Pending Edited", 
IF(A_Master_Catalog.pet_status = "creation", "Pending EnI", 
IF(A_Master_Catalog.pet_status = "creation,edited,images", "Ok", "Error")))));

#Actualizar el sku status
UPDATE operations_co.out_stock_hist
SET out_stock_hist.status_sku = 
		IF(out_stock_hist.status_config="deleted" or out_stock_hist.status_simple="deleted", "Deleted", 
		IF(out_stock_hist.status_config="inactive" or out_stock_hist.status_simple="inactive", "Inactive", 
		IF(out_stock_hist.status_config="active" and out_stock_hist.status_simple="active", "Active", NULL)));

#Actualizar la razón de no visibilidad
UPDATE operations_co.out_stock_hist
SET out_stock_hist.reason_not_visible = 
		IF(out_stock_hist.status_pet <> "OK", out_stock_hist.status_pet, 
		IF(out_stock_hist.status_sku <> "active", out_stock_hist.status_sku, 
		IF(out_stock_hist.content_qc = 0, "Quality Check Failed", "Stock levels")))
WHERE visible = 0;