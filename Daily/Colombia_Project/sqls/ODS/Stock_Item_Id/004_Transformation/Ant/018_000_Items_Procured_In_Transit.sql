TRUNCATE operations_co.items_procured_in_transit;

INSERT INTO operations_co.items_procured_in_transit (
	sku_simple,
	number_ordered,
	unit_price
) SELECT
	catalog_simple.sku,
	count(procurement_order_item.id_procurement_order_item) AS countofid_procurement_order_item,
	avg(procurement_order_item.unit_price) AS avgofunit_price
FROM
	bob_live_co.catalog_simple
 INNER JOIN procurement_live_co.procurement_order_item
ON catalog_simple.id_catalog_simple = procurement_order_item.fk_catalog_simple
	 INNER JOIN procurement_live_co.procurement_order 
ON procurement_order_item.fk_procurement_order = procurement_order.id_procurement_order
WHERE	procurement_order_item.is_deleted = 0
AND		procurement_order.is_cancelled = 0
AND		procurement_order_item.sku_received = 0
GROUP BY
	catalog_simple.sku;


UPDATE operations_co.out_stock_hist
 INNER JOIN operations_co.items_procured_in_transit 
	ON out_stock_hist.sku_simple = items_procured_in_transit.sku_simple
SET 
 out_stock_hist.items_procured_in_transit = number_ordered,
 out_stock_hist.procurement_price = unit_price;