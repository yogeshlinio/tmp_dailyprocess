#Calcular PC 1.5
DROP TEMPORARY TABLE IF EXISTS operations_co.TMP_Reserved;
CREATE TEMPORARY TABLE operations_co.TMP_Reserved ( PRIMARY KEY ( sku_simple  ) )
SELECT
   sku_simple,
		sum(ifnull(in_stock, 0)) stock_wms
FROM
   operations_co.out_stock_hist
WHERE
      reserved = 0
  AND in_stock = 1
GROUP BY
sku_simple
;

/*PASO 1 INSERTAMOS VALORES GENERALES*/
DROP TABLE IF EXISTS operations_co.tbl_bi_ops_pc_sample;
CREATE TABLE operations_co.tbl_bi_ops_pc_sample LIKE operations_co.tbl_bi_ops_pc; 
ALTER TABLE operations_co.tbl_bi_ops_pc_sample 
   ADD COLUMN tax_percent DOUBLE, 
   ADD COLUMN delivery_cost_supplier DECIMAL(15,2),
   ADD COLUMN eligible_free_shipping INT,
   ADD COLUMN precio_actual DECIMAL(15,2)
    ;
INSERT operations_co.tbl_bi_ops_pc_sample (
   sku,
   tax_percent,
   delivery_cost_supplier,
   eligible_free_shipping
   )    
SELECT 
	sku_simple,
    tax_Percent,
    delivery_cost_supplier,
    eligible_free_shipping
FROM
   development_co_project.A_Master_Catalog
;
/*INSERT REFFERENCES TO SH*/
UPDATE       operations_co.tbl_bi_ops_pc_sample AS c
  INNER JOIN development_co_project.tbl_sku_precios_costos sh 
          ON sh.sku_simple = c.sku
SET
   c.pc_15 = sh.precio_actual - COALESCE(sh.costo_actual,0,sh.costo_actual)-
			                        - COALESCE(c.delivery_cost_supplier,0,c.delivery_cost_supplier)
                              - COALESCE(sh.costo_actual IS NULL,0,0.025 * sh.costo_actual),
   c.precio_actual = sh.precio_actual
;

/*UPDATE REFERENCES TO sf*/
UPDATE       operations_co.tbl_bi_ops_pc_sample AS c
  INNER JOIN development_co_project.tbl_sku_shipping_fees sf 
          ON sf.sku = c.sku
SET
   c.pc_15 = c.pc_15 + IF(c.eligible_free_shipping = 1,0,IF (sf.shipping_fee IS NULL,0,sf.shipping_fee)) / 
                         (1 +(c.tax_percent / 100));

UPDATE        operations_co.tbl_bi_ops_pc_sample AS c
   INNER JOIN development_co_project.tbl_sku_shipping_cost shc 
           ON shc.skusimple = c.sku
SET 
   c.pc_15 = c.pc_15 -IF(shc.shipping_cost IS NULL,0,shc.shipping_cost);


UPDATE        operations_co.tbl_bi_ops_pc_sample AS c
SET
   c.pc_15_percentage = c.pc_15 / c.precio_actual
;
ALTER TABLE operations_co.tbl_bi_ops_pc_sample 
   DROP COLUMN tax_percent , 
   DROP COLUMN delivery_cost_supplier ,
   DROP COLUMN eligible_free_shipping ,
   DROP COLUMN precio_actual
;

INSERT INTO operations_co.tbl_bi_ops_pc 
SELECT * FROM operations_co.tbl_bi_ops_pc_sample; 

UPDATE operations_co.out_stock_hist 
INNER JOIN operations_co.tbl_bi_ops_pc 
	ON out_stock_hist.sku_simple = tbl_bi_ops_pc.sku
SET out_stock_hist.pc_15 = tbl_bi_ops_pc.pc_15,
out_stock_hist.pc_15_percentage = tbl_bi_ops_pc.pc_15_percentage;
