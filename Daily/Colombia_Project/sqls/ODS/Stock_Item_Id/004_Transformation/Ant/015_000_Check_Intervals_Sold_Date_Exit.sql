UPDATE operations_co.out_stock_hist
SET 
 out_stock_hist.sold_last_60 = CASE
																		WHEN datediff(curdate(), date_exit) < 60 
																		THEN 1
																		ELSE 0
																	END,
 out_stock_hist.sold_last_30 = CASE
																		WHEN datediff(curdate(), date_exit) < 30 
																		THEN 1
																		ELSE 0
																	END,
 out_stock_hist.sold_last_7 = CASE
																		WHEN datediff(curdate(), date_exit) < 7 
																		THEN 1
																		ELSE 0
																	END,
 out_stock_hist.sold_yesterday = 	CASE
																		WHEN datediff(curdate(),(date_sub(date_exit, INTERVAL 1 DAY))) = 0 
																		THEN 1
																		ELSE 0
																	END,
 out_stock_hist.entrance_last_30 = CASE
																		WHEN datediff(curdate(), date_entrance) < 30 
																		THEN 1
																		ELSE 0
																	END;

																	

UPDATE operations_co.out_stock_hist
SET out_stock_hist.in_stock_cost_w_o_vat = out_stock_hist.cost_w_o_vat
WHERE
	out_stock_hist.in_stock = 1;

UPDATE operations_co.out_stock_hist
SET out_stock_hist.sold_last_30_cost_w_o_vat = out_stock_hist.cost_w_o_vat,
 out_stock_hist.sold_last_30_price = out_stock_hist.price
WHERE
	out_stock_hist.sold_last_30 = 1;