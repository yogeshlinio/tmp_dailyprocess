update operations_co.out_stock_hist
set 
out_stock_hist.interval_remaining_days=if(average_remaining_days is null,"Infinito",
if(average_remaining_days<7,"< 8",if(average_remaining_days<15,"8-15", if(average_remaining_days<30,"15-29", 
if(average_remaining_days<45,"30-44",if(average_remaining_days<60,"45-59",
if(average_remaining_days<90,"60-89",if(average_remaining_days<120,"90-119",
if(average_remaining_days<150,"120-149",if(average_remaining_days<180,"150-179",
if(average_remaining_days<210,"180-209",if(average_remaining_days<240,"210-239",
if(average_remaining_days<270,"240-269",if(average_remaining_days<300,"270-299",
if(average_remaining_days<330,"300-229",if(average_remaining_days<=360,"330-360","> 360"))))))))))))))));


update operations_co.out_stock_hist
set 
out_stock_hist.interval_sku_age=if(max_days_in_stock is null,"Infinito",
if(max_days_in_stock<7,"< 8",if(max_days_in_stock<15,"8-15", if(max_days_in_stock<30,"15-29", 
if(max_days_in_stock<45,"30-44",if(max_days_in_stock<60,"45-59",
if(max_days_in_stock<90,"60-89",if(max_days_in_stock<120,"90-119",
if(max_days_in_stock<150,"120-149",if(max_days_in_stock<180,"150-179",
if(max_days_in_stock<210,"180-209",if(max_days_in_stock<240,"210-239",
if(max_days_in_stock<270,"240-269",if(max_days_in_stock<300,"270-299",
if(max_days_in_stock<330,"300-229",if(max_days_in_stock<=360,"330-360","> 360"))))))))))))))));
