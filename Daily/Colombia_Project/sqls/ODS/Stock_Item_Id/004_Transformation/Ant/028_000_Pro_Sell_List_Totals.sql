SET @i = 0 ; 
DROP TEMPORARY TABLE IF EXISTS operations_co.TMP ; 
CREATE TEMPORARY TABLE operations_co.TMP 
SELECT
	sku_config,
	Count(*) AS items_in_stock,
	sum(cost_w_o_vat) AS cost_w_o_vat_in_stock
FROM
	operations_co.out_stock_hist
WHERE
	in_stock = 1
AND reserved = 0
AND (
			fulfillment_type_real <> 'consignment'
			OR fulfillment_type_real IS NOT NULL
		)
GROUP BY
	sku_config;

DROP TABLE IF EXISTS operations_co.top_skus_sell_list ; 
CREATE TABLE operations_co.top_skus_sell_list 
SELECT
	sku_config,
	items_in_stock,
	cost_w_o_vat_in_stock,
	@i := @i + 1 AS rkn
FROM
	operations_co.TMP
ORDER BY
	cost_w_o_vat_in_stock DESC ;

# Sell List
DELETE FROM operations_co.pro_sell_list_hist_totals where date_reported = curdate();

INSERT INTO operations_co.pro_sell_list_hist_totals (
	date_reported,
	category_bp,
	cost_w_o_vat_over_two_items,
	cost_w_o_vat_under_two_items) 
SELECT
	J.date_reported,
	J.Categoria,
	J.Value_more_than_two,
	J.Value_less_than_two
FROM
	(	SELECT
			curdate() AS date_reported,
			B.category_bp AS 'Categoria',
			B.cost_w_o_vat AS Value_more_than_two,
			D.cost_w_o_vat AS Value_less_than_two
		FROM
			(	SELECT
					category_bp,
					sum(cost_w_o_vat) AS cost_w_o_vat
				FROM
					(	SELECT
							category_bp,
							sku_config,
							sum(cost_w_o_vat) AS cost_w_o_vat
						FROM
							operations_co.out_stock_hist
						WHERE
							is_sell_list = 1
						AND category_bp IS NOT NULL
						GROUP BY
							sku_config
						HAVING
							count(sku_config) > 2
					) A
				GROUP BY
					category_bp
			) B
		LEFT JOIN (	SELECT
									category_bp,
									sum(cost_w_o_vat) AS cost_w_o_vat
								FROM
									(	SELECT
											category_bp,
											sum(cost_w_o_vat) AS cost_w_o_vat
										FROM
											operations_co.out_stock_hist
										WHERE
											is_sell_list = 1
										GROUP BY
											sku_config
										HAVING
											count(sku_config) <= 2
									) C
								GROUP BY
									category_bp
									) D 
		ON B.category_bp = D.category_bp
	) J;