TRUNCATE TABLE operations_co.skus_in_stock;

INSERT INTO operations_co.skus_in_stock
select distinct(sku_simple) from operations_co.out_stock_hist
WHERE in_stock=1 and reserved=0;

UPDATE operations_co.out_stock_hist
SET out_stock_hist.sku_in_stock=1
WHERE sku_simple in (select sku from operations_co.skus_in_stock);