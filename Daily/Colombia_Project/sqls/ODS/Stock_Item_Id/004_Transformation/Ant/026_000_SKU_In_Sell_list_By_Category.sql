UPDATE operations_co.out_stock_hist SET is_sell_list = 1
WHERE max_days_in_stock > 30
and in_stock= 1
and reserved= 0
and fulfillment_type_real <> 'Consignment' 
and fulfillment_type_real is not null
and category_1 = 'Home and Living' 
and (average_remaining_days > 150
or average_remaining_days IS NULL);

UPDATE operations_co.out_stock_hist SET is_sell_list = 1
WHERE max_days_in_stock > 30
and in_stock= 1
and reserved= 0
and fulfillment_type_real <> 'Consignment' 
and fulfillment_type_real is not null
and category_1 = 'Fashion' 
and (average_remaining_days > 120
or average_remaining_days IS NULL);

UPDATE operations_co.out_stock_hist SET is_sell_list = 1
WHERE max_days_in_stock > 30
and in_stock= 1
and reserved= 0
and fulfillment_type_real <> 'Consignment' 
and fulfillment_type_real is not null
and category_1 = 'Electrónicos' 
and (average_remaining_days > 60
or average_remaining_days IS NULL);

UPDATE operations_co.out_stock_hist SET is_sell_list = 1
WHERE max_days_in_stock > 30
and in_stock= 1
and reserved= 0
and fulfillment_type_real <> 'Consignment' 
and fulfillment_type_real is not null
and category_1 not in ('Home and Living','Fashion','Electrónicos') 
and (average_remaining_days > 90
or average_remaining_days IS NULL);