# What is on the outlet
TRUNCATE TABLE operations_co.pro_skus_in_outlet;

INSERT INTO operations_co.pro_skus_in_outlet 
SELECT
	sku,
	NAME
FROM
	(
		SELECT
			cc.*, 
			lft,
			rgt,
			rgt - lft,
			c2.sku,
			c. NAME,
			c. STATUS
		FROM
			bob_live_co.catalog_config_has_catalog_category cc
		 INNER JOIN bob_live_co.catalog_category c ON id_catalog_category = fk_catalog_category
		 INNER JOIN bob_live_co.catalog_config c2 ON cc.fk_catalog_config = c2.id_catalog_config
		WHERE
			id_catalog_category > 1
		) AS a
WHERE
	a. NAME LIKE '%liquida%';

UPDATE operations_co.out_stock_hist a
 INNER JOIN operations_co.pro_skus_in_outlet b 
	ON a.sku_config = b.sku
SET outlet = 1
WHERE	a.in_stock = 1
AND 	a.reserved = 0;