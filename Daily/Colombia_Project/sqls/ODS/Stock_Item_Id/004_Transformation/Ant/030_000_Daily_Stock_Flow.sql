
DROP TEMPORARY TABLE IF EXISTS TMP_Stock;
CREATE TEMPORARY TABLE TMP_Stock ( PRIMARY KEY (Sku_Simple) ) 
SELECT
  Sku_Simple,
  #MAX( Fecha_Event)as Fecha,
  SUM( in_Stock ) AS Stock,
  SUM( ifnull( items_procured_in_transit,0) ) items_pending
FROM
   operations_co.out_stock_hist
GROUP BY 
  Sku_Simple
;

DROP TABLE IF EXISTS SKU_Inventory_Hist;
CREATE TABLE SKU_Inventory_Hist ( PRIMARY KEY (  sku_simple, Fecha_Event, In_Out )  )
SELECT
	Out_Hist.sku_simple,
	Out_Hist.sku_config,
  Out_Hist.date_entrance as Fecha_Event,

  count(Out_Hist.date_entrance) entries,
	sum( Out_Hist.cost_w_o_vat  ) * (TMP_Stock.Stock) costo,
	sum( Out_Hist.cost_w_o_vat  ) * count(date_entrance) costo_entries,
  " In" AS In_Out
FROM
             operations_co.out_stock_hist AS Out_Hist
  INNER JOIN TMP_Stock
       USING (  SKU_Simple )
GROUP BY
  date_entrance,
  sku_simple
;

REPLACE SKU_Inventory_Hist
SELECT
   Out_Hist.sku_simple,
	 Out_Hist.sku_config,
 	 Out_Hist.date_exit,

   count(Out_Hist.date_exit) as entries,
	sum( Out_Hist.cost_w_o_vat  ) * (TMP_Stock.Stock) costo,
	sum( COALESCE( Out_Hist.cost_w_o_vat, 0) ) * count(Out_Hist.date_exit) costo_entries,
 	 'Out' AS In_Out
FROM
             operations_co.out_stock_hist AS Out_Hist
  INNER JOIN TMP_Stock
       USING (  SKU_Simple )
WHERE date_exit is not null
GROUP BY
   date_exit,
   sku_simple
;


DROP  TABLE IF EXISTS Daily_Stock_Flow;
CREATE  TABLE Daily_Stock_Flow ( PRIMARY KEY ( SKU_Simple, Fecha_Event ) )
SELECT 
   Fecha_Event,
   sku_simple,
	 sku_config,

   0 AS Stock,
   SUM( IF( In_Out = " In"  , Entries , 0 )) AS Entradas,
   SUM( IF( In_Out = "Out" , Entries , 0 )) AS Salidas,
   SUM( IF( In_Out = " In"  , costo   , 0 ))   AS Costo_Entradas,
   SUM( IF( In_Out = " In" , costo_entries , 0 ))   AS Costo_PerDay_Entradas,

   SUM( IF( In_Out = "Out"  , costo   , 0 ))   AS Costo_Salidas,
   SUM( IF( In_Out = "Out" , costo_entries , 0 ))   AS Costo_PerDay_Salidas,
   0   AS Items_Pending
,In_Out
FROM SKU_Inventory_Hist
GROUP BY 
   Fecha_Event, 
   SKU_Simple
;
UPDATE        Daily_Stock_Flow
   INNER JOIN TMP_Stock
       ON     Daily_Stock_Flow.SKU_Simple  = TMP_Stock.SKU_Simple
SET
   Daily_Stock_Flow.Stock         = TMP_Stock.Stock,
   Daily_Stock_Flow.Items_Pending = TMP_Stock.Items_Pending
;  
