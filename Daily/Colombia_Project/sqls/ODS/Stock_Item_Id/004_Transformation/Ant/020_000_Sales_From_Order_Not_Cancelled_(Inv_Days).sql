UPDATE operations_co.out_stock_hist
SET sold_last_30_order = 	CASE
														WHEN datediff(curdate(), date_ordered) <= 30 
														THEN 1
														ELSE 0
													END
WHERE	cancelled <> 1;


UPDATE operations_co.out_stock_hist
SET sold_last_15_order = 	CASE
														WHEN datediff(curdate(), date_ordered) <= 15 
														THEN 1
														ELSE 0
													END
WHERE	cancelled <> 1;


UPDATE operations_co.out_stock_hist
SET sold_last_7_order = 	CASE
														WHEN datediff(curdate(), date_ordered) <= 7 
														THEN 1
														ELSE 0
													END
WHERE	cancelled <> 1;

UPDATE operations_co.out_stock_hist
SET 
 out_stock_hist.sold_last_30_order_cost_w_o_vat = out_stock_hist.cost_w_o_vat,
 out_stock_hist.sold_last_30_order_price = out_stock_hist.price
WHERE
	out_stock_hist.sold_last_30_order = 1;
	
	
UPDATE operations_co.out_stock_hist
SET 
 out_stock_hist.sold_last_15_order_cost_w_o_vat = out_stock_hist.cost_w_o_vat
WHERE
	out_stock_hist.sold_last_15_order = 1;
	

UPDATE operations_co.out_stock_hist
SET 
 out_stock_hist.sold_last_7_order_cost_w_o_vat = out_stock_hist.cost_w_o_vat
WHERE
	out_stock_hist.sold_last_7_order = 1;