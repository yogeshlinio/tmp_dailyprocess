DROP TABLE IF EXISTS operations_co.pro_max_days_in_stock;

CREATE TABLE operations_co.pro_max_days_in_stock
SELECT
	sku_config,
	avg(days_in_stock) as max_days_in_stock
FROM
	operations_co.out_stock_hist
WHERE
	in_stock = 1
GROUP BY
	sku_config;


UPDATE operations_co.out_stock_hist
 INNER JOIN operations_co.pro_max_days_in_stock 
	ON out_stock_hist.sku_config = pro_max_days_in_stock.sku_config
SET out_stock_hist.max_days_in_stock = pro_max_days_in_stock.max_days_in_stock;