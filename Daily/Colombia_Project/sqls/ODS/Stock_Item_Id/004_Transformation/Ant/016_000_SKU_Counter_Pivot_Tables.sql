TRUNCATE operations_co.pro_stock_hist_sku_count;

INSERT INTO operations_co.pro_stock_hist_sku_count (
	sku_simple, 
	sku_count) 
SELECT
	out_stock_hist.sku_simple,
	sum(out_stock_hist.item_counter) AS sumofitem_counter
FROM
	operations_co.out_stock_hist
GROUP BY
	out_stock_hist.sku_simple,
	out_stock_hist.in_stock
HAVING
	out_stock_hist.in_stock = 1;

UPDATE operations_co.out_stock_hist
 INNER JOIN operations_co.pro_stock_hist_sku_count 
 ON out_stock_hist.sku_simple = pro_stock_hist_sku_count.sku_simple
SET out_stock_hist.sku_counter = 1 / pro_stock_hist_sku_count.sku_count;