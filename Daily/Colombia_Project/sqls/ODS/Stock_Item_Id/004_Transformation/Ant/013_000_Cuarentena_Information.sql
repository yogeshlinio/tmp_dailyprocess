UPDATE operations_co.out_stock_hist
SET 
 out_stock_hist.quarantine = 1
 WHERE	wh_location LIKE '%DEF%'
OR		wh_location LIKE '%REP%'
OR		wh_location LIKE '%GRT%'
OR		wh_location LIKE '%AVE%'
OR		wh_location LIKE '%cuerentena%'
OR		wh_location LIKE '%Cuarentena_Inverse%'
;

UPDATE operations_co.out_stock_hist 
Set 
    quarantine_type = if(wh_location liKe '%DEF%',
        'DEF',
        if(wh_location like '%REP%',
            'REP',
            if(wh_location like '%GRT%',
                'GRT',
                if(wh_location like '%AVE%',
                    'AVE',
					if(wh_location like 'cuerentena',
                    'Cuarentena',
						if(wh_location like '%Cuarentena_Inverse%',
							'Cuarentena_Inverse',
							'PENDING'))))))
where
    quarantine = 1;


DROP TEMPORARY TABLE IF EXISTS operations_co.tmp_movimentacoes;
CREATE TEMPORARY TABLE operations_co.tmp_movimentacoes (PRIMARY KEY (estoque_id))
SELECT 
								estoque_id, 
								para_endereco, 
								min(date(data_criacao)) AS data_movimentacao 
							FROM wmsprod_co.movimentacoes 
							WHERE para_endereco LIKE '%DEF%' 
							OR 		para_endereco LIKE '%REP%' 
							OR		para_endereco LIKE '%GRT%' 
							OR 		para_endereco LIKE '%AVE%' 
							OR 		para_endereco LIKE '%cuerentena%'
							GROUP BY estoque_id;

UPDATE operations_co.out_stock_hist t 
  INNER JOIN operations_co.tmp_movimentacoes t2
ON 	t.stock_item_id = t2.estoque_id
SET t.quarantine_date = t2.data_movimentacao;

UPDATE operations_co.out_stock_hist
SET 
 quarantine_date=date_entrance,
 quarantine_inbound = 1
WHERE quarantine=1 
and quarantine_date IS NULL;