UPDATE            Stock_Item_Id_Sample_@v_countryPrefix@          AS Stock_Item_Id
       INNER JOIN Stock_Item_Id_Key_Map_Sample_@v_countryPrefix@ AS Stock_Item_Id_Key_Map
            USING ( Country , stock_item_id )
SET
   Stock_Item_Id.wh_location = Stock_Item_Id_Key_Map.posicao,
   Stock_Item_Id.quarantine = 1,
   Stock_Item_Id.quarantine_type = if(posicao liKe '%DEF%',
                                                    'DEF',
                                   if(posicao like '%REP%',
                                                    'REP',
                                   if(posicao like '%GRT%',
                                                    'GRT',
                                   if(posicao like '%AVE%',
                                                    'AVE',
					              if(posicao like 'cuerentena',
                                                   'Cuarentena',
						          if(posicao like '%Cuarentena_Inverse%',
							                       'Cuarentena_Inverse',
							                       'PENDING'))))))
WHERE	
        posicao LIKE '%DEF%'
OR		posicao LIKE '%REP%'
OR		posicao LIKE '%GRT%'
OR		posicao LIKE '%AVE%'
OR		posicao LIKE '%REP%'
OR		posicao LIKE '%cuerentena%'
OR		posicao LIKE '%Cuarentena_Inverse%'
;

