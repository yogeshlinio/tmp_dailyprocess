SELECT  'Update Own Warehouse',now();

DROP TABLE IF EXISTS operations_co.fulfillment_type_own_warehouse;
CREATE TABLE operations_co.fulfillment_type_own_warehouse (INDEX (item_id))
SELECT
 b.item_id,
 "Own Warehouse" as fulfillment_type_real
FROM
wmsprod_co.itens_venda b
INNER JOIN wmsprod_co.status_itens_venda c
 ON b.itens_venda_id = c.itens_venda_id
WHERE c.STATUS ='Estoque reservado';

UPDATE
 operations_co.out_stock_hist a
INNER JOIN operations_co.fulfillment_type_own_warehouse b
 ON a.item_id = b.item_id
SET a.fulfillment_type_real = b.fulfillment_type_real;



SELECT  'Update Consignment',now();

DROP TABLE IF EXISTS operations_co.fulfillment_type_consignment;
CREATE TABLE operations_co.fulfillment_type_consignment (INDEX (item_id))
SELECT
 b.item_id,
 'Consignment' AS fulfillment_type_real
FROM
wmsprod_co.itens_venda b
INNER JOIN wmsprod_co.estoque c
 ON b.estoque_id = c.estoque_id
INNER JOIN wmsprod_co.itens_recebimento d
 ON c.itens_recebimento_id = d.itens_recebimento_id
INNER JOIN wmsprod_co.recebimento e
 ON d.recebimento_id = e.recebimento_id
INNER JOIN wmsprod_co.po_oms f
 ON e.inbound_document_identificator = f.name
INNER JOIN procurement_live_co.procurement_order g
 ON f.po_oms_id = g.id_procurement_order
WHERE (e.inbound_document_type_id = 7 OR e.inbound_document_type_id = 11);


UPDATE
 operations_co.out_stock_hist a
INNER JOIN operations_co.fulfillment_type_consignment b
 ON a.item_id = b.item_id
SET a.fulfillment_type_real = b.fulfillment_type_real;




SELECT  'Update Dropshipping',now();

DROP TABLE IF EXISTS operations_co.fulfillment_type_dropshipping;
CREATE TABLE operations_co.fulfillment_type_dropshipping (INDEX (item_id))
SELECT
 b.item_id,
 "Dropshipping" as fulfillment_type_real
FROM
wmsprod_co.itens_venda b
INNER JOIN wmsprod_co.status_itens_venda c
 ON b.itens_venda_id = c.itens_venda_id
WHERE c.STATUS IN (	'DS estoque reservado',
			'Waiting dropshipping',
			'Dropshipping notified',
			'awaiting_fulfillment',
			'Electronic good'
		);

UPDATE
 operations_co.out_stock_hist a
INNER JOIN operations_co.fulfillment_type_dropshipping b
 ON a.item_id = b.item_id
SET a.fulfillment_type_real = b.fulfillment_type_real;




SELECT  'Update Crossdocking',now();

DROP TABLE IF EXISTS operations_co.fulfillment_type_crossdocking;
CREATE TABLE operations_co.fulfillment_type_crossdocking (INDEX (item_id))
SELECT
 b.item_id,
 "Crossdocking" as fulfillment_type_real
FROM
wmsprod_co.itens_venda b
INNER JOIN wmsprod_co.status_itens_venda c
 ON b.itens_venda_id = c.itens_venda_id
WHERE c.STATUS IN (	'analisando quebra',
			'aguardando estoque');

UPDATE
 operations_co.out_stock_hist a
INNER JOIN operations_co.fulfillment_type_crossdocking b
 ON a.item_id = b.item_id
SET a.fulfillment_type_real = b.fulfillment_type_real;



SELECT  'Update Own Warehouse',now();
			
UPDATE operations_co.out_stock_hist 
SET 
	cancelled= 1,
	fulfillment_type_real='Own Warehouse'
WHERE status_item IN (	'Cancelado', 
												'quebra tratada', 
												'quebrado', 
												'Precancelado', 
												'backorder_tratada');

UPDATE operations_co.out_stock_hist
INNER JOIN wmsprod_co.movimentacoes 
	ON out_stock_hist.stock_item_id = movimentacoes.estoque_id
SET out_stock_hist.fulfillment_type_real = 'Own Warehouse'
WHERE
	movimentacoes.de_endereco = 'retorno de cancelamento'
OR movimentacoes.de_endereco = 'vendidos'; 
