UPDATE operations_co.out_stock_hist
SET fulfillment_type_bp = CASE
				WHEN fulfillment_type_real IN( 'crossdocking', 'consignment')
				THEN fulfillment_type_real
				WHEN fulfillment_type_real = 'Own Warehouse'
				THEN 'Outright Buying'
				WHEN fulfillment_type_real = 'dropshipping' 
				THEN 'other'
END;