UPDATE operations_co.out_stock_hist
 INNER JOIN development_co_project.A_Master_Catalog 
	ON out_stock_hist.sku_simple = development_co_project.A_Master_Catalog.sku_simple
SET 
 out_stock_hist.cost_w_o_vat = ifnull(
	IF (A_Master_Catalog.special_purchase_price IS NULL,A_Master_Catalog.cost,
	IF (curdate() > A_Master_Catalog.special_to_date,A_Master_Catalog.cost,
	IF (curdate() < A_Master_Catalog.special_from_date,A_Master_Catalog.cost,
	IF (A_Master_Catalog.special_purchase_price < A_Master_Catalog.cost,
				A_Master_Catalog.special_purchase_price,
				A_Master_Catalog.cost)))),0)
WHERE cost_w_o_vat IS NULL or cost_w_o_vat = 0
;