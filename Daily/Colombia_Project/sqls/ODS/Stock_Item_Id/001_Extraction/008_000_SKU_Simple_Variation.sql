UPDATE operations_co.out_stock_hist a
 INNER JOIN bob_live_co.catalog_simple b
 ON a.sku_simple = b.sku
 INNER JOIN operations_co.simple_variation c
 ON b.id_catalog_simple = c.fk_catalog_simple
SET
 a.sku_variation =  c.variation;