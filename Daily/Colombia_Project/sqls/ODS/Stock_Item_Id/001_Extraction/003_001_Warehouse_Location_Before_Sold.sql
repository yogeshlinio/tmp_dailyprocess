UPDATE operations_co.out_stock_hist 
 INNER JOIN wmsprod_co.movimentacoes 
	ON out_stock_hist.stock_item_id = movimentacoes.estoque_id 
SET out_stock_hist.wh_location_before_sold = de_endereco
WHERE movimentacoes.para_endereco="vendidos";