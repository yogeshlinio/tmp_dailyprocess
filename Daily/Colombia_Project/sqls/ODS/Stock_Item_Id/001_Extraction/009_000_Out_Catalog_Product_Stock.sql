call operations_co.out_catalog_product_stock;

# Indicadores de Stock para consulta
UPDATE operations_co.out_catalog_product_stock
SET share_reserved_bob = 	IF (	reservedbob = 0,0,
													IF (	(reservedbob - stock_wms_reserved) / stock_wms > 1,1,(reservedbob - stock_wms_reserved) / stock_wms)), share_stock_bob = availablebob / stock_wms;

SELECT
	'Actualizando niveles de stock de BOB',
	now();
	
UPDATE operations_co.out_catalog_product_stock a
         INNER JOIN
    operations_co.vw_stock_wms b ON a.sku = b.sku_simple 
SET 
    a.stock_wms = b.stock_wms
WHERE
    b.stock_wms > 0;



UPDATE operations_co.out_catalog_product_stock a
         INNER JOIN
    operations_co.vw_stock_wms_reserved b ON a.sku = b.sku_simple 
SET 
    a.stock_wms_reserved = b.stock_wms_reserved;
	

UPDATE operations_co.out_catalog_product_stock 
SET 
    stock_wms_reserved = 0
WHERE
    stock_wms_reserved IS NULL;


UPDATE operations_co.out_catalog_product_stock 
SET 
    share_reserved_bob = IF(reservedbob = 0,
        0,
        IF((reservedbob - stock_wms_reserved) / stock_wms > 1,
            1,
            (reservedbob - stock_wms_reserved) / stock_wms)),
    share_stock_bob = availablebob / stock_wms;

UPDATE operations_co.out_stock_hist
 INNER JOIN operations_co.out_catalog_product_stock 
	ON out_stock_hist.sku_simple = out_catalog_product_stock.sku
SET 
 out_stock_hist.stock_bob = out_catalog_product_stock.stockbob,
 out_stock_hist.own_stock_bob = out_catalog_product_stock.ownstock,
 out_stock_hist.supplier_stock_bob = out_catalog_product_stock.supplierstock,
 out_stock_hist.reserved_bob = out_catalog_product_stock.reservedbob,
 out_stock_hist.stock_available_bob = out_catalog_product_stock.availablebob,
 out_stock_hist.reservedbob = out_catalog_product_stock.share_reserved_bob;