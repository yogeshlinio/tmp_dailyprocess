DROP TEMPORARY TABLE IF EXISTS operations_co.tmp_purchase_order_details;
CREATE TEMPORARY TABLE operations_co.tmp_purchase_order_details (INDEX(purchase_order, sku_simple))
SELECT
 purchase_order,
 sku_simple,
 cost_oms,
 purchase_order_type,
 payment_terms
FROM operations_co.out_procurement_tracking
WHERE is_cancelled = 0 
and 	is_deleted = 0
GROUP BY 
  purchase_order,
 sku_simple
;

UPDATE operations_co.out_stock_hist a 
 INNER JOIN operations_co.tmp_purchase_order_details b 
ON a.purchase_order=b.purchase_order and a.sku_simple=b.sku_simple 
SET
	 a.cost_w_o_vat= if(b.cost_oms <= 0 OR b.cost_oms IS NULL,a.cost_w_o_vat,b.cost_oms),
	 a.purchase_order_type=b.purchase_order_type,
	 a.payment_terms=b.payment_terms
;

UPDATE operations_co.out_stock_hist a 
 INNER JOIN operations_co.tbl_purchase_order_stock b 
	ON a.purchase_order = b.oc 
	AND a.sku_simple = b.sku
SET
	a.cost_w_o_vat = b.costo,
	a.purchase_order_type = tipo_orden,
	a.payment_terms = b.tipo_pago;


UPDATE operations_co.out_stock_hist a
 INNER JOIN operations_co.tbl_purchase_order b 
	ON a.purchase_order = b.oc 
	AND a.sku_simple = b.sku
SET
	a.cost_w_o_vat = b.costo,
	a.purchase_order_type = 'Crossdocking',
	a.payment_terms = b.tipo_pago;
	
UPDATE
 operations_co.out_stock_hist a
SET a.fulfillment_type_real = 'Consignment'
WHERE purchase_order_type = 'Consignment';