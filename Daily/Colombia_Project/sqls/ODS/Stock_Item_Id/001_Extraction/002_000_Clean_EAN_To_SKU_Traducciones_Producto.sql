UPDATE operations_co.out_stock_hist
 INNER JOIN wmsprod_co.traducciones_producto 
ON out_stock_hist.barcode_wms = traducciones_producto.identificador
SET out_stock_hist.sku_simple = sku;

UPDATE operations_co.out_stock_hist 
SET 
	sku_simple_blank= IF(sku_simple IS NULL,1,0);