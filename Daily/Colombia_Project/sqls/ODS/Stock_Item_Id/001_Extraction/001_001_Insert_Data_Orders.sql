/*
	Sales order item asociado a cada item que pertenece a bodega
*/
DROP TEMPORARY TABLE IF EXISTS operations_co.tmp_sales_for_stock;
CREATE TEMPORARY TABLE operations_co.tmp_sales_for_stock ( INDEX (estoque_id))
SELECT 
		t.itens_venda_id,
		t.item_id,
		t.order_id,
		t.numero_order,
		t.data_pedido,
		t.estoque_id,
		t.data_criacao,
		t.status 
	FROM wmsprod_co.itens_venda t 
JOIN (SELECT 
				estoque_id, 
				max(data_criacao) AS max 
			FROM wmsprod_co.itens_venda 
			GROUP BY estoque_id) t2 
ON t.estoque_id=t2.estoque_id 
;

UPDATE	operations_co.out_stock_hist a 
 INNER JOIN operations_co.tmp_sales_for_stock b
	ON a.stock_item_id = b.estoque_id
SET 
	a.order_nr = b.numero_order,
	a.date_ordered = b.data_pedido,
	a.status_item = b.status,
	a.item_id = b.item_id;
