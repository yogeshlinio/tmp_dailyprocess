/*
Fecha y datos de cuando sale vendido un producto
*/
UPDATE operations_co.out_stock_hist 
 INNER JOIN wmsprod_co.movimentacoes 
ON out_stock_hist.stock_item_id = movimentacoes.estoque_id 
SET 
	out_stock_hist.date_exit = data_criacao,
	out_stock_hist.week_exit = operations_co.week_iso(data_criacao),
	out_stock_hist.exit_type = "sold",
	out_stock_hist.sold = 1
WHERE 
		out_stock_hist.wh_location ="vendidos"
	AND movimentacoes.para_endereco="vendidos";

/*
Fecha y datos de cuando sale errado un producto
*/
UPDATE operations_co.out_stock_hist 
 INNER JOIN wmsprod_co.movimentacoes 
ON out_stock_hist.stock_item_id = movimentacoes.estoque_id 
SET 
	out_stock_hist.date_exit = data_criacao,
	out_stock_hist.week_exit = operations_co.week_iso(data_criacao),
	out_stock_hist.exit_type = "error"
WHERE 
		out_stock_hist.wh_location ="Error_entrada"
	AND movimentacoes.para_endereco="Error_entrada";