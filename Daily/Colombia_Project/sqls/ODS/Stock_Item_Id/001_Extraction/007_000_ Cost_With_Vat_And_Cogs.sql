UPDATE operations_co.out_stock_hist
SET 
 out_stock_hist.cost = out_stock_hist.cost_w_o_vat*(1 + tax_percentage / 100);

UPDATE operations_co.out_stock_hist
SET cogs = cost_w_o_vat + delivery_cost_supplier;