UPDATE operations_co.out_stock_hist
SET buyer = trim(BOTH ' ' FROM REPLACE(REPLACE(REPLACE(buyer, '\t', ''), '\n',''),'\r',''));

UPDATE operations_co.out_stock_hist
SET planner =
IF (	out_stock_hist.buyer IN (
		"B0 - Jessica",
		"B3 - Maria Fernanda",
		"B4 - Freddy",
		"B5 - Katherine",
		"B6 - Ximena",
		"B8 - Guillermo",
		"B8 - Cristina"
	),
	"Home",

IF (
	out_stock_hist.buyer IN (
		"B2 - Alexander",
		"B1 - Maritza"
	),
	"Electronics",
	"Fashion"
)
);