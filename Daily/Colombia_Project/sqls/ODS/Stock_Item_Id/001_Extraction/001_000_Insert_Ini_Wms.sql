INSERT INTO operations_co.out_stock_hist (
	stock_item_id,
	barcode_wms,
	date_entrance,
	week_entrance,
	barcode_bob_duplicated,
	in_stock,
	wh_location,
	sub_location,
	position_type,
  reserved
) 
SELECT
	estoque.estoque_id,
	estoque.cod_barras,
	date(data_criacao),
	operations_co.week_iso(data_criacao),
	estoque.minucioso,
	posicoes.participa_estoque,
	estoque.endereco,
	estoque.sub_endereco,
	posicoes.tipo_posicao,
    IF(estoque.almoxarifado = "separando"
			OR estoque.almoxarifado = "estoque reservado"
			OR estoque.almoxarifado = "aguardando separacao", 1, 0)
FROM	(wmsprod_co.estoque
			LEFT JOIN wmsprod_co.itens_recebimento 
				ON estoque.itens_recebimento_id = itens_recebimento.itens_recebimento_id)
			LEFT JOIN wmsprod_co.posicoes 
				ON estoque.endereco = posicoes.posicao
;