UPDATE            Sku_Simple_Sample_@v_countryPrefix@ AS Sku_Simple_Sample
       INNER JOIN Sku_Simple_Key_Map
	        USING ( country , sku_simple )
       INNER JOIN @bob_live@.catalog_simple
	        USING ( id_catalog_simple )
SET
   /*Defaults and Simple Extraction*/
   Sku_Simple_Sample.cost                   = catalog_simple.cost                   ,
   Sku_Simple_Sample.delivery_cost_supplier = catalog_simple.delivery_cost_supplier ,
   Sku_Simple_Sample.eligible_free_shipping = catalog_simple.eligible_free_shipping ,
   Sku_Simple_Sample.original_price         = catalog_simple.original_price         ,
   Sku_Simple_Sample.special_price           = catalog_simple.special_price * (  1 + tax_percent / 100 )           ,   
   Sku_Simple_Sample.special_price_after_tax = catalog_simple.special_price          ,   

   
   Sku_Simple_Sample.special_from_date      = catalog_simple.special_from_date      ,   
   Sku_Simple_Sample.special_to_date        = catalog_simple.special_to_date        ,      

   Sku_Simple_Sample.Price                  = catalog_simple.price                  ,
   Sku_Simple_Sample.special_purchase_price = catalog_simple.specialpurchaseprice   ,   
   Sku_Simple_Sample.fixed_price            = catalog_simple.fixed_price            ,
   Sku_Simple_Sample.isActive_SKUSimple     = if( catalog_simple.status = "active" , 1 , 0 ),
   Sku_Simple_Sample.status_simple          = catalog_simple.status                 ,
   Sku_Simple_Sample.isMarketPlace          = catalog_simple.is_option_marketplace  ,

   Sku_Simple_Sample.created_at_simple      = catalog_simple.created_at ,
   
   
   Sku_Simple_Sample.min_delivery_time      = catalog_simple.min_delivery_time ,
   Sku_Simple_Sample.max_delivery_time      = catalog_simple.max_delivery_time ,
   Sku_Simple_Sample.promise_delivery_badge = catalog_simple.promise_delivery_badge ,
   Sku_Simple_Sample.delivery_time_supplier = catalog_simple.delivery_time_supplier 
 ;
 
 
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  '@v_country@', 
  'ODS.Sku_simple',
  "catalog_simple",
  NOW(),
  NOW(),
  0,
  0
;



