
#create index customer on bob_live_mx.customer(id_customer);
#create index customers_rev_id on dev_marketing.customers_rev_mx(custid);
#create index custid on production.tbl_order_detail(custid);

#New Version
select  'CRM Customers Revenue Table: start',now();

delete from dev_marketing.customers_rev_mx;

-- alter table dev_marketing.customers_rev_mx add column custid integer;

/*create table dev_marketing.unique_id(
email varchar(255),
unique_id varchar(255)
);
create index email on dev_marketing.unique_id(email);

insert into dev_marketing.unique_id select email, unique_id from dev_marketing.customers_rev_mx;*/

-- create index customers_rev_id on dev_marketing.customers_rev_mx(custid);

-- alter table dev_marketing.customers_rev_mx drop unique_id;
alter table dev_marketing.customers_rev_mx modify MONETARY_VALUE float;
alter table dev_marketing.customers_rev_mx modify Sending_Time integer;
alter table dev_marketing.customers_rev_mx modify RECENCY integer;
alter table dev_marketing.customers_rev_mx modify AV_TICKET float;

insert into dev_marketing.customers_rev_mx(date_registred, first_name, last_name, custid, email, gender, MONETARY_VALUE)
select c.created_at, c.first_name, c.last_name, c.id_customer, c.email, c.gender, net.MONETARY_VALUE from bob_live_mx.customer c left join (select i.custid as custid, sum(i.actual_paid_price) as MONETARY_VALUE from production.tbl_order_detail i where i.oac=1 group by i.custid order by MONETARY_VALUE desc)net on c.id_customer=net.custid;

update dev_marketing.customers_rev_mx set Source_data = 'Accoount_creation';

insert into dev_marketing.customers_rev_mx(date_registred, email, gender)
select created_at, email, gender from bob_live_mx.newsletter_subscription where fk_customer is null; 

update dev_marketing.customers_rev_mx set Source_data = 'Bob_newsletter' where Source_data is null;

insert into dev_marketing.customers_rev_mx (email) select email from dev_marketing.open_hour_crm_mx r where r.email not in (select email from dev_marketing.customers_rev_mx);  

alter table dev_marketing.customers_rev_mx modify MONETARY_VALUE varchar(100);

INSERT INTO dev_marketing.customers_rev_mx (
	date_registred,
	email,
	Source_data,
	first_name,
	last_name,
	gender
) SELECT
	date,
	email,
	Source,
	first_name,
	last_name,
	gender
FROM
	(
		SELECT
			A.*
		FROM
			test_linio.nuevosMailsMEX A
		LEFT JOIN dev_marketing.customers_rev_mx B ON A.email = B.email
		WHERE
			B.email IS NULL
	) C;

INSERT INTO dev_marketing.customers_rev_mx (
	date_registred,
	email,
	Source_data,
	first_name,
	last_name,
	gender
) SELECT
	date,
	email,
	Base,
	first_name,
	last_name,
	gender
FROM
	(
		SELECT
			A.*
		FROM
			test_linio.mailsMEX A
		LEFT JOIN dev_marketing.customers_rev_mx B ON A.email = B.email
		WHERE
			B.email IS NULL
	) C;


update dev_marketing.customers_rev_mx set MONETARY_VALUE = 'none' where MONETARY_VALUE is null;

update dev_marketing.customers_rev_mx set gender = 'neutral' where gender is null;

update dev_marketing.customers_rev_mx rev inner join production.tbl_order_detail i on rev.custid=i.custid set TYPE_CUSTOMER= 'customer' where i.oac=1;

update dev_marketing.customers_rev_mx rev set TYPE_CUSTOMER= 'non customer' where TYPE_CUSTOMER is null;

update dev_marketing.customers_rev_mx rev inner join dev_marketing.open_hour_crm_mx crm on rev.email=crm.email set Sending_Time= most_frequently_open_hour;

alter table dev_marketing.customers_rev_mx modify Sending_Time varchar(100);

update dev_marketing.customers_rev_mx set Sending_Time= 'never open/sent newsletter' where Sending_Time is null;

update dev_marketing.customers_rev_mx rev set RECENCY =
(select datediff(curdate(), max(date)) as days from production.tbl_order_detail i where i.custid=rev.custid and i.oac=1 group by custid);

alter table dev_marketing.customers_rev_mx modify RECENCY varchar(100);

update dev_marketing.customers_rev_mx rev set RECENCY = 'never bought' where RECENCY is null;

update dev_marketing.customers_rev_mx rev set AV_TICKET= ( 
select avg(actual_paid_price) from production.tbl_order_detail i where oac=1 and rev.custid=i.custid group by custid);

alter table dev_marketing.customers_rev_mx modify AV_TICKET varchar(100);

update dev_marketing.customers_rev_mx rev set AV_TICKET= 'none' where AV_TICKET is null;

update dev_marketing.customers_rev_mx rev set FREQUENCY= (select datediff(max(date), min(date))/(count(distinct date)-1) as avg_time from production.tbl_order_detail i where oac=1 and i.custid=rev.custid group by custid);

update dev_marketing.customers_rev_mx set FREQUENCY= 0 where FREQUENCY is null;

update dev_marketing.customers_rev_mx rev set LOCATION= ( 
select region from bob_live_mx.customer_address a where rev.custid=a.fk_customer group by fk_customer);

update dev_marketing.customers_rev_mx rev set LOCATION= 'unknown' where LOCATION is null;

update dev_marketing.customers_rev_mx rev set LOCATION= ( 
select region from production.tbl_order_detail i where rev.custid=i.custid group by custid) where LOCATION='unknown' or location='';

update dev_marketing.customers_rev_mx rev set LOCATION= 'unknown' where LOCATION is null;

#UMS FORMAT
update dev_marketing.customers_rev_mx set MONETARY_VALUE = '0' where MONETARY_VALUE='none'; 
update dev_marketing.customers_rev_mx set Sending_Time= '100' where Sending_Time='never open/sent newsletter'; 
update dev_marketing.customers_rev_mx set RECENCY = '0' where RECENCY='never bought'; 
update dev_marketing.customers_rev_mx set AV_TICKET= '0' where AV_TICKET='none';

alter table dev_marketing.customers_rev_mx modify MONETARY_VALUE float;
alter table dev_marketing.customers_rev_mx modify Sending_Time integer;
alter table dev_marketing.customers_rev_mx modify RECENCY integer;
alter table dev_marketing.customers_rev_mx modify AV_TICKET float;
#UMS FORMAT

update dev_marketing.customers_rev_mx rev inner join bob_live_mx.customer_address c on rev.custid=c.fk_customer set rev.sms=c.mobile_phone;

update dev_marketing.customers_rev_mx rev inner join bob_live_mx.customer c on rev.custid=c.id_customer set rev.birthday=c.birthday;

update dev_marketing.customers_rev_mx rev set rev.new_registry = case when datediff(curdate(), date_registred)<30 then 1 else 0 end;

update dev_marketing.customers_rev_mx set new_registry = 0 where new_registry is null;

update dev_marketing.customers_rev_mx rev inner join production.tbl_order_detail i on rev.custid=i.custid set rev.boughtcoupon= case when (select count(coupon_code) from production.tbl_order_detail z where z.custid=rev.custid group by custid)>0 then 1 else 0 end, transactions = (select count(distinct order_nr) from production.tbl_order_detail m where oac=1 and rev.custid=m.custid group by custid);

update dev_marketing.customers_rev_mx set boughtcoupon = 0 where boughtcoupon is null;

update dev_marketing.customers_rev_mx set transactions = 0 where transactions is null;

-- alter table dev_marketing.customers_rev_mx drop column custid;

alter ignore table dev_marketing.customers_rev_mx add primary key (email);

alter ignore table dev_marketing.customers_rev_mx drop primary key;

-- Control Group

/* alter table dev_marketing.customers_rev_mx AUTO_INCREMENT = 1;

alter table dev_marketing.customers_rev_mx add unique_id int;

update dev_marketing.customers_rev_mx x inner join dev_marketing.unique_id u on x.email=u.email set x.unique_id=u.unique_id;

alter table dev_marketing.customers_rev_mx modify unique_id int auto_increment primary key;

alter table dev_marketing.customers_rev_mx modify unique_id varchar(100);

update dev_marketing.customers_rev_mx set unique_id='01' where unique_id='1';

update dev_marketing.customers_rev_mx set unique_id='02' where unique_id='2';

update dev_marketing.customers_rev_mx set unique_id='03' where unique_id='3';

update dev_marketing.customers_rev_mx set unique_id='04' where unique_id='4';

update dev_marketing.customers_rev_mx set unique_id='05' where unique_id='5';

update dev_marketing.customers_rev_mx set unique_id='06' where unique_id='6';

update dev_marketing.customers_rev_mx set unique_id='07' where unique_id='7';

update dev_marketing.customers_rev_mx set unique_id='08' where unique_id='8';

update dev_marketing.customers_rev_mx set unique_id='09' where unique_id='9';

update dev_marketing.customers_rev_mx set control_group=1 where substring(unique_id, -2) in ('00', '12', '13');

update dev_marketing.customers_rev_mx set control_group=2 where substring(unique_id, -2) in ('01','02','03','04','05','06','07','08','09','10','11');

/*update dev_marketing.customers_rev_mx set control_group=3 where substring(unique_id, -2) = (select vara from dev_marketing.control_group3_rules where date=curdate()) or substring(unique_id, -2) = (select varb from dev_marketing.control_group3_rules where date=curdate()) or substring(unique_id, -2) = (select varc from dev_marketing.control_group3_rules where date=curdate()) or substring(unique_id, -2) = (select vard from dev_marketing.control_group3_rules where date=curdate());*/

/* update dev_marketing.customers_rev_mx set control_group=0 where control_group is null;

-- update dev_marketing.customers_rev_mx set target_group=1 where weekday(curdate()) in (0, 2) and control_group = 2;

-- update dev_marketing.customers_rev_mx set target_group=0 where control_group = 2 and target_group is null;

update dev_marketing.customers_rev_mx set target_group=0 where control_group=2;

update dev_marketing.customers_rev_mx set target_group=0 where control_group=1;

update dev_marketing.customers_rev_mx set target_group=1 where control_group=0;

update dev_marketing.customers_rev_mx set target_group=0 where control_group=3;

update dev_marketing.customers_rev_mx set Account= case when custid is not null then 1 else 0 end;*/

update dev_marketing.customers_rev_mx set Account= case when custid is not null then 1 else 0 end;

update dev_marketing.customers_rev_mx a inner join bob_live_mx.newsletter_subscription b on a.email = b.email set a.Suscribed = case when b.status = 'subscribed' then 1 else 0 end;


UPDATE dev_marketing.customers_rev_mx A
INNER JOIN test_linio.mailsMEX B ON A.email = B.email
SET A.Source_data = B.base;


DROP TEMPORARY TABLE IF EXISTS dev_marketing.TMP ; 
CREATE TEMPORARY TABLE dev_marketing.TMP 
SELECT
email,code, sales_rule.is_active, to_date, used_discount_amount
from bob_live_mx.newsletter_subscription 
inner join bob_live_mx.sales_rule
on fk_sales_rule=id_sales_rule 
inner join bob_live_mx.sales_rule_set on
fk_sales_rule_set=id_sales_rule_set
where code like 'NL%';

create index email on dev_marketing.TMP(email);

UPDATE dev_marketing.customers_rev_mx A
INNER JOIN TMP
ON A.email = TMP.email
SET A.Subscription_voucher = TMP.code,
    A.active = TMP.is_active,
    A.days_to_inactive = datediff(TMP.to_date, curdate()),
    A.used = CASE WHEN TMP.used_discount_amount > 0 THEN 1 ELSE 0 END
;

update dev_marketing.customers_rev_mx rev 
set rev.new_registry_voucher = case when datediff(curdate(), date_registred)<29 
and TYPE_CUSTOMER = 'non customer' 
and Subscription_voucher is not NULL
and active = 1
and days_to_inactive >= 2
and used = 0
then 1 else 0 end;

-- drop table dev_marketing.unique_id;

select  'CRM Customers Revenue Table: end',now();
#End New Version
