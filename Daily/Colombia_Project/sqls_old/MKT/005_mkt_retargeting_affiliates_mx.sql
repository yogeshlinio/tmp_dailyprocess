/*
Nombre: 
Autor: Jean-Charles Rout Machado
Fecha: 20-01-2014
Descripcion:
*/
set @days:=30;

delete from production.retargeting_affiliates_mx where datediff(curdate(), date)<@days;

-- FBX

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'FBX', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source='facebook' and medium='retargeting' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

create table production.temporary_retargeting_affiliates_mx(
date date,
impressions int,
clicks int,
cost float);

create index date on production.temporary_retargeting_affiliates_mx(date);

create table production.final_mx(
date date,
source varchar(255),
medium varchar(255),
campaign varchar(255),
times float,
visits int,
day_visits int);

create index channel on production.final_mx(date, source, medium, campaign);

insert into production.temporary_retargeting_affiliates_mx(date) select date from production.retargeting_affiliates_mx where channel = 'FBX' and datediff(curdate(), date)<@days group by date;

update production.temporary_retargeting_affiliates_mx r set cost = (select sum(a.cost)+sum(b.cost) from marketing_report.sociomantic_fbx_retargeting_mx a, marketing_report.sociomantic_fbx_loyalty_mx b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_mx r set impressions = (select sum(a.impressions)+sum(b.impressions) from marketing_report.sociomantic_fbx_retargeting_mx a, marketing_report.sociomantic_fbx_loyalty_mx b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_mx r set clicks = (select sum(a.clicks)+sum(b.clicks) from marketing_report.sociomantic_fbx_retargeting_mx a, marketing_report.sociomantic_fbx_loyalty_mx b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

insert into production.final_mx(date, source, medium, campaign, visits) select date, source, medium, campaign, visits from production.retargeting_affiliates_mx where channel='FBX' and datediff(curdate(), date)<@days;

update production.final_mx f set day_visits = (select sum(visits) from production.retargeting_affiliates_mx r where r.date=f.date group by date) where datediff(curdate(), date)<@days;

update production.final_mx f set times = visits/day_visits;

update production.retargeting_affiliates_mx r set cost = (select t.cost*x.times from production.temporary_retargeting_affiliates_mx t, production.final_mx x where t.date=r.date and x.date=t.date and r.channel='FBX' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='FBX' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx r set impressions = (select t.impressions*x.times from production.temporary_retargeting_affiliates_mx t, production.final_mx x where t.date=r.date and x.date=t.date and r.channel='FBX' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='FBX' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx r set clicks = (select t.clicks*x.times from production.temporary_retargeting_affiliates_mx t, production.final_mx x where t.date=r.date and x.date=t.date and r.channel='FBX' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='FBX' and datediff(curdate(), date)<@days;

delete from production.temporary_retargeting_affiliates_mx;

delete from production.final_mx;

-- Sociomantic

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Sociomantic', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%socioman%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

insert into production.temporary_retargeting_affiliates_mx(date) select date from production.retargeting_affiliates_mx where channel = 'Sociomantic' and datediff(curdate(), date)<@days group by date;

update production.temporary_retargeting_affiliates_mx r set cost = (select sum(a.cost)+sum(b.cost) from marketing_report.sociomantic_basket_mx a, marketing_report.sociomantic_loyalty_mx b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_mx r set impressions = (select sum(a.impressions)+sum(b.impressions) from marketing_report.sociomantic_basket_mx a, marketing_report.sociomantic_loyalty_mx b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_mx r set clicks = (select sum(a.clicks)+sum(b.clicks) from marketing_report.sociomantic_basket_mx a, marketing_report.sociomantic_loyalty_mx b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

insert into production.final_mx(date, source, medium, campaign, visits) select date, source, medium, campaign, visits from production.retargeting_affiliates_mx where channel='Sociomantic' and datediff(curdate(), date)<@days;

update production.final_mx f set day_visits = (select sum(visits) from production.retargeting_affiliates_mx r where r.channel='Sociomantic' and r.date=f.date group by date) where datediff(curdate(), date)<@days;

update production.final_mx f set times = visits/day_visits;

update production.retargeting_affiliates_mx r set cost = (select t.cost*x.times from production.temporary_retargeting_affiliates_mx t, production.final_mx x where t.date=r.date and x.date=t.date and r.channel='Sociomantic' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='Sociomantic' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx r set impressions = (select t.impressions*x.times from production.temporary_retargeting_affiliates_mx t, production.final_mx x where t.date=r.date and x.date=t.date and r.channel='Sociomantic' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='Sociomantic' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx r set clicks = (select t.clicks*x.times from production.temporary_retargeting_affiliates_mx t, production.final_mx x where t.date=r.date and x.date=t.date and r.channel='Sociomantic' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='Sociomantic' and datediff(curdate(), date)<@days;

delete from production.temporary_retargeting_affiliates_mx;

delete from production.final_mx;

-- Criteo

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Criteo', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%criteo%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

insert into production.temporary_retargeting_affiliates_mx(date) select date from production.retargeting_affiliates_mx where channel = 'Criteo' and datediff(curdate(), date)<@days group by date;

update production.temporary_retargeting_affiliates_mx r set cost = (select sum(a.cost) from marketing_report.criteo_mx a where a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_mx r set impressions = (select sum(a.impressions) from marketing_report.criteo_mx a where a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_mx r set clicks = (select sum(a.clicks) from marketing_report.criteo_mx a where a.date=r.date) where datediff(curdate(), date)<@days;

insert into production.final_mx(date, source, medium, campaign, visits) select date, source, medium, campaign, visits from production.retargeting_affiliates_mx where channel='Criteo' and datediff(curdate(), date)<@days;

update production.final_mx f set day_visits = (select sum(visits) from production.retargeting_affiliates_mx r where r.channel='Criteo' and r.date=f.date group by date) where datediff(curdate(), date)<@days;

update production.final_mx f set times = visits/day_visits;

update production.retargeting_affiliates_mx r set cost = (select t.cost*x.times from production.temporary_retargeting_affiliates_mx t, production.final_mx x where t.date=r.date and x.date=t.date and r.channel='Criteo' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='Criteo' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx r set impressions = (select t.impressions*x.times from production.temporary_retargeting_affiliates_mx t, production.final_mx x where t.date=r.date and x.date=t.date and r.channel='Criteo' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='Criteo' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx r set clicks = (select t.clicks*x.times from production.temporary_retargeting_affiliates_mx t, production.final_mx x where t.date=r.date and x.date=t.date and r.channel='Criteo' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='Criteo' and datediff(curdate(), date)<@days;

drop table production.temporary_retargeting_affiliates_mx;

drop table production.final_mx;

-- Vizury

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Vizury', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%vizury%' and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Vizury' and datediff(curdate(), date)<@days;

-- Veinteractive

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Veinteractive', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%veinteractive%' and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Veinteractive' and datediff(curdate(), date)<@days;

-- GLG

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'GLG', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%glg%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='GLG' and datediff(curdate(), date)<@days;

-- Pampa Network

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Pampa Network', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%pampa%' and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Pampa Network' and datediff(curdate(), date)<@days;

-- SOLOCPM/Main ADV

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Main ADV', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where (source like '%maindadv%' or source like '%solocpm%') and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Main ADV' and datediff(curdate(), date)<@days;

-- Mercado Libre

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Mercado Libre', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%mercadolibre%' and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

-- Metros Cubicos

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Metros Cubicos', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where (source like '%metroscubiocs%' or source like '%Metros_Cubicos%' or source like '%Metros+Cubicos%') and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

-- Promodescuentos

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Promodescuentos', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%promodescuentos%' and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Promodescuentos' and datediff(curdate(), date)<@days;

-- My Things

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'My Things', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%mythings%' and medium='retargeting' and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='My Things' and datediff(curdate(), date)<@days;

-- Avazu

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Avazu', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%avazu%' and medium='retargeting' and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Avazu' and datediff(curdate(), date)<@days;

-- Adjal

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Adjal', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%adjal%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Adjal' and datediff(curdate(), date)<@days;

-- ClickMagic

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'ClickMagic', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%clickmagic%' and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='ClickMagic' and datediff(curdate(), date)<@days;

-- Commission Junction

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Commission Junction', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%cj%' and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Commission Junction' and datediff(curdate(), date)<@days;

-- DiverselyDigital

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'DiverselyDigital', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where (source like '%diverselydigital%' or source like '%diverslydigital%') and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='DiverselyDigital' and datediff(curdate(), date)<@days;

-- ILoveCPA

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'ILoveCPA', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%ilovecpa%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='ILoveCPA' and datediff(curdate(), date)<@days;

-- Lomadee

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Lomadee', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%lomadee%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Lomadee' and datediff(curdate(), date)<@days;

-- PubliciIdeas

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'PubliciIdeas', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%publiciideas%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='PubliciIdeas' and datediff(curdate(), date)<@days;

-- Soicos

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Soicos', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%soicos%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Soicos' and datediff(curdate(), date)<@days;

-- Trade Tracker

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Trade Tracker', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where (source like '%tradetracker%' or source = 'Trade Tracker') and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Trade Tracker' and datediff(curdate(), date)<@days;

-- Buscape

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Buscape', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%buscape%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

-- Freewoo

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Freewoo', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%freewoo%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

-- BOB 

update production.retargeting_affiliates_mx r set net_transactions = (select count(distinct transaction_id) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx r set gross_transactions = (select count(distinct transaction_id) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.obc=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx r set net_revenue = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx r set gross_revenue = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.obc=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx r set PC2 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx r set new_customers = (select sum(new_customers) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx r set new_customers_gross = (select sum(new_customers_gross) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.obc=1) where datediff(curdate(), date)<@days;

-- Cohort

create table production.temporary_mx(
date date,
source varchar(255),
medium varchar(255),
campaign varchar(255),
custid int,
transaction_id varchar(255)
);

create index temporary on production.temporary_mx(date, source, medium, campaign);
create index transaction_id on production.temporary_mx(transaction_id);

insert into production.temporary_mx select distinct x.date, x.source, x.medium, x.campaign, t.custid, x.transaction_id from SEM.transaction_id_mx x, production.tbl_order_detail t where x.transaction_id=t.order_nr and t.new_customers is not null and t.oac=1 and datediff(curdate(), t.date)<@days;

update production.retargeting_affiliates_mx a set net_transactions_30 = (select count(distinct t.order_nr) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set net_revenue_30 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set PC2_30 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set net_transactions_60 = (select count(distinct t.order_nr) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set net_revenue_60 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set PC2_60 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set net_transactions_90 = (select count(distinct t.order_nr) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set net_revenue_90 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set PC2_90 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set net_transactions_120 = (select count(distinct t.order_nr) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set net_revenue_120 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set PC2_120 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set net_transactions_150 = (select count(distinct t.order_nr) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set net_revenue_150 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set PC2_150 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set net_transactions_180 = (select count(distinct t.order_nr) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set net_revenue_180 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set PC2_180 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

drop table production.temporary_mx;

-- Date

update production.retargeting_affiliates_mx set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=SEM.week_iso(date) where datediff(curdate(), date)<@days;

-- Exchange Rate

update production.retargeting_affiliates_mx s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, net_revenue_30=net_revenue_30/xr, net_revenue_60=net_revenue_60/xr, net_revenue_90=net_revenue_90/xr, net_revenue_120=net_revenue_120/xr, net_revenue_150=net_revenue_150/xr, net_revenue_180=net_revenue_180/xr, PC2=PC2/xr, PC2_30=PC2_30/xr, PC2_60=PC2_60/xr, PC2_90=PC2_90/xr, PC2_120=PC2_120/xr, PC2_150=PC2_150/xr, PC2_180=PC2_180/xr where r.country='MEX' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost/1.23 where channel = 'Criteo' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost/16.35 where channel in ('Vizury', 'Veinteractive', 'GLG', 'Pampa Network', 'Adjal', 'ClickMagic', 'Commission Junction', 'DiverselyDigital', 'ILoveCPA', 'Lomadee', 'PubliciIdeas', 'Soicos', 'Trade Tracker', 'Main ADV', 'Promodescuentos', 'My Things', 'Avazu') and datediff(curdate(), date)<@days; 

-- Cost

update production.retargeting_affiliates_mx set cost = cost*0.08*2.5 where channel = 'Vizury' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1 where channel = 'Main ADV' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1 where channel = 'Promodescuentos' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1 where channel = 'My Things' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1 where channel = 'Avazu' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.08*2.5 where channel = 'Veinteractive' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1 where channel = 'GLG' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.15*2.5 where channel = 'Pampa Network' and date <= '2013-05-12' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1*2.5 where channel = 'Pampa Network' and date > '2013-05-12' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1*2.5 where channel = 'Adjal' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1*2.5 where channel = 'ClickMagic' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1 where channel = 'Commission Junction' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1 where channel = 'DiverselyDigital' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1 where channel = 'ILoveCPA' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1*2.5 where channel = 'Lomadee' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1*2.5 where channel = 'PubliciIdeas' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1*2.5 where channel = 'Soicos' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1*2.5 where channel = 'Trade Tracker' and datediff(curdate(), date)<@days;

-- NULL

update production.retargeting_affiliates_mx  set gross_revenue=0 where gross_revenue is null;

update production.retargeting_affiliates_mx  set net_revenue=0 where net_revenue is null;

update production.retargeting_affiliates_mx  set new_customers=0 where new_customers is null;

update production.retargeting_affiliates_mx  set new_customers_gross=0 where new_customers_gross is null;

update production.retargeting_affiliates_mx set net_revenue_30=0 where net_revenue_30 is null;

update production.retargeting_affiliates_mx set net_revenue_60=0 where net_revenue_60 is null;

update production.retargeting_affiliates_mx set net_revenue_90=0 where net_revenue_90 is null;

update production.retargeting_affiliates_mx set net_revenue_120=0 where net_revenue_120 is null;

update production.retargeting_affiliates_mx set net_revenue_150=0 where net_revenue_150 is null;

update production.retargeting_affiliates_mx set net_revenue_180=0 where net_revenue_180 is null;
