/*
Nombre: google_optimization_keyword_mx
Autor: Jean-Charles Rout Machado
Fecha: 20-01-2014
Descripcion:
*/

set @days:=90;

call SEM.sem_filter_keyword_mx;

delete from SEM.google_optimization_keyword_mx where datediff(curdate(), date)<@days;

insert into SEM.google_optimization_keyword_mx(date, channel, campaign, ad_group, keyword, impressions, clicks, visits, bounce, cart, cost) select date, channel, campaign, ad_group, keyword, sum(impressions), sum(clicks), sum(visits), sum(bounce), sum(cart), sum(ad_cost) from SEM.sem_filter_campaign_keyword_mx where datediff(curdate(), date)<@days group by channel, date, campaign, ad_group, keyword;

update SEM.google_optimization_keyword_mx set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_mx set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=SEM.week_iso(date) where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_mx x set gross_transactions =  (select count(distinct transaction_id) from SEM.sem_filter_transaction_id_keyword_mx s, production.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx x set net_transactions =  (select count(distinct transaction_id) from SEM.sem_filter_transaction_id_keyword_mx s, production.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx x set gross_revenue =  (select sum(t.actual_paid_price) from SEM.sem_filter_transaction_id_keyword_mx s, production.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx x set net_revenue =  (select sum(t.actual_paid_price) from SEM.sem_filter_transaction_id_keyword_mx s, production.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx x set new_customers =  (select sum(t.new_customers) from SEM.sem_filter_transaction_id_keyword_mx s, production.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx x set new_customers_gross =  (select sum(t.new_customers_gross) from SEM.sem_filter_transaction_id_keyword_mx s, production.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx x set category = substring_index(campaign, '.',1) where datediff(curdate(), date)<@days;

-- cohort

create table SEM.temporary_mx(
date date,
campaign varchar(255),
ad_group varchar(255),
keyword varchar(255),
custid int,
transaction_id varchar(255)
);

create index temporary on SEM.temporary_mx(date, ad_group, campaign, keyword);
create index transaction_id on SEM.temporary_mx(transaction_id);

insert into SEM.temporary_mx select distinct x.date, x.campaign, x.ad_group, x.keyword, x.customer_id, x.transaction_id from SEM.google_optimization_transaction_id_keyword_mx x, production.tbl_order_detail t where x.transaction_id=t.order_nr and t.new_customers is not null and t.oac=1 and datediff(curdate(), x.date)<@days;

update SEM.google_optimization_keyword_mx a set net_transactions_30 = (select count(distinct t.order_nr) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx a set net_revenue_30 = (select sum(actual_paid_price) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx a set net_transactions_60 = (select count(distinct t.order_nr) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx a set net_revenue_60 = (select sum(actual_paid_price) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx a set net_transactions_90 = (select count(distinct t.order_nr) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx a set net_revenue_90 = (select sum(actual_paid_price) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx a set net_transactions_120 = (select count(distinct t.order_nr) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx a set net_revenue_120 = (select sum(actual_paid_price) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx a set net_transactions_150 = (select count(distinct t.order_nr) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx a set net_revenue_150 = (select sum(actual_paid_price) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_mx a set net_transactions_180 = (select count(distinct t.order_nr) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx a set net_revenue_180 = (select sum(actual_paid_price) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

drop table SEM.temporary_mx;

update SEM.google_optimization_keyword_mx set net_revenue_30=0 where net_revenue_30 is null;

update SEM.google_optimization_keyword_mx set net_revenue_60=0 where net_revenue_60 is null;

update SEM.google_optimization_keyword_mx set net_revenue_90=0 where net_revenue_90 is null;

update SEM.google_optimization_keyword_mx set net_revenue_120=0 where net_revenue_120 is null;

update SEM.google_optimization_keyword_mx set net_revenue_150=0 where net_revenue_150 is null;

update SEM.google_optimization_keyword_mx set net_revenue_180=0 where net_revenue_180 is null;

-- cohort

update SEM.google_optimization_keyword_mx s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, net_revenue_30=net_revenue_30/xr, net_revenue_60=net_revenue_60/xr, net_revenue_90=net_revenue_90/xr, net_revenue_120=net_revenue_120/xr, net_revenue_150=net_revenue_150/xr, net_revenue_180=net_revenue_180/xr, cost=cost/xr where r.country='MEX' and datediff(curdate(), s.date)<@days;

update SEM.google_optimization_keyword_mx set CPC=cost/clicks, CPO=cost/net_transactions, CTR=clicks/impressions, CAC=cost/new_customers, CIR=cost/net_revenue, checkout_rate=cart/visits, conversion_rate=net_transactions/clicks, bounce_rate=bounce/clicks, transactions_to_cart=net_transactions/cart where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_mx set gross_CPO=cost/gross_transactions, gross_CAC=cost/new_customers_gross, gross_CIR=cost/gross_revenue, gross_conversion_rate=gross_transactions/clicks, gross_transactions_to_cart=gross_transactions/cart where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_mx set gross_revenue=0 where gross_revenue is null;

update SEM.google_optimization_keyword_mx set net_revenue=0 where net_revenue is null;

update SEM.google_optimization_keyword_mx set new_customers=0 where new_customers is null;

update SEM.google_optimization_keyword_mx set new_customers_gross=0 where new_customers_gross is null;

update SEM.google_optimization_keyword_mx set CPC=0 where CPC is null;

update SEM.google_optimization_keyword_mx set CPO=0 where CPO is null;

update SEM.google_optimization_keyword_mx set CTR=0 where CTR is null;

update SEM.google_optimization_keyword_mx set CAC=0 where CAC is null;

update SEM.google_optimization_keyword_mx set CIR=0 where CIR is null;

update SEM.google_optimization_keyword_mx set checkout_rate=0 where checkout_rate is null;

update SEM.google_optimization_keyword_mx set conversion_rate=0 where conversion_rate is null;

update SEM.google_optimization_keyword_mx set bounce_rate=0 where bounce_rate is null;

update SEM.google_optimization_keyword_mx set transactions_to_cart=0 where transactions_to_cart is null;

update SEM.google_optimization_keyword_mx set gross_CPO=0 where gross_CPO is null;

update SEM.google_optimization_keyword_mx set gross_CAC=0 where gross_CAC is null;

update SEM.google_optimization_keyword_mx set gross_CIR=0 where gross_CIR is null;

update SEM.google_optimization_keyword_mx set gross_conversion_rate=0 where gross_conversion_rate is null;

update SEM.google_optimization_keyword_mx set gross_transactions_to_cart=0 where gross_transactions_to_cart is null;
