/*
Nombre: google_optimization_transaction_id_keyword_mx
Autor: Jean-Charles Rout Machado
Fecha: 20-01-2014
Descripcion:
*/

delete from SEM.google_optimization_transaction_id_keyword_mx;

insert into SEM.google_optimization_transaction_id_keyword_mx select concat(year(x.date),if(month(x.date)<10,concat(0,month(x.date)),month(x.date))), SEM.week_iso(x.date), x.date, case when weekday(x.date) = 0 then 'Monday' when weekday(x.date) = 1 then 'Tuesday' when weekday(x.date) = 2 then 'Wednesday' when weekday(x.date) = 3 then 'Thursday' when weekday(x.date) = 4 then 'Friday' when weekday(x.date) = 5 then 'Saturday' when weekday(x.date) = 6 then 'Sunday' end, x.channel, substring_index(x.campaign, '.',1), x.campaign, x.ad_group, x.keyword, t.custid, t.order_nr, t.sku, t.product_name, t.n1, t.actual_paid_price, t.status_item, case when t.oac=1 then 'net' when t.status_item = 'invalid' then 'invalid' else 'gross' end, t.payment_method, case when t.new_customers is not null then 1 else 0 end from SEM.sem_filter_transaction_id_keyword_mx x, production.tbl_order_detail t where t.order_nr=x.transaction_id;

update SEM.google_optimization_transaction_id_keyword_mx s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr where r.country='MEX';
