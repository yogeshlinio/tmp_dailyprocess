 
INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Mexico', 
  'out_order_tracking',
  'start',
  NOW(),
  max(date_time_ordered),
  count(*),
  count(item_counter)
FROM
  production.out_order_tracking;


CALL rafael.calcworkdays;
/*
DELETE rafael.out_order_tracking.*
FROM
	rafael.out_order_tracking;
*/
DROP TABLE IF EXISTS rafael.out_order_tracking_sample;

CREATE TABLE rafael.out_order_tracking_sample LIKE rafael.out_order_tracking;


INSERT INTO rafael.out_order_tracking_sample (
	order_item_id,
	order_number,
	sku_simple,
	status_wms,
  supplier_leadtime
) SELECT
	itens_venda.item_id,
	itens_venda.numero_order,
	itens_venda.sku,
	itens_venda.STATUS,
  itens_venda.tempo_entrega_fornecedor
FROM
	wmsprod.itens_venda

WHERE DATE_FORMAT(itens_venda.data_criacao,'%Y%m') >= (DATE_FORMAT(curdate()- INTERVAL 4 MONTH,'%Y%m'))
;

UPDATE 
 ((bob_live_mx.sales_order_address
	RIGHT JOIN 	(rafael.out_order_tracking_sample
			         INNER JOIN bob_live_mx.sales_order 
               ON out_order_tracking_sample.order_number = sales_order.order_nr
							)ON sales_order_address.id_sales_order_address = sales_order.fk_sales_order_address_billing
	)LEFT JOIN bob_live_mx.customer_address_region 
	 ON sales_order_address.fk_customer_address_region = customer_address_region.id_customer_address_region
 )
INNER JOIN bob_live_mx.sales_order_item ON out_order_tracking_sample.order_item_id = sales_order_item.id_sales_order_item
SET 
 out_order_tracking_sample.date_ordered = date(sales_order.created_at),
 date_time_ordered = sales_order.created_at,
 out_order_tracking_sample.payment_method = sales_order.payment_method,
 out_order_tracking_sample.ship_to_state = customer_address_region. CODE,
 out_order_tracking_sample.min_delivery_time = sales_order_item.min_delivery_time,
 out_order_tracking_sample.max_delivery_time = sales_order_item.shipment_total_delivery_time,
 out_order_tracking_sample.supplier_leadtime = sales_order_item.delivery_time_supplier,
 out_order_tracking_sample.tax_percent = sales_order_item.tax_percent,
 out_order_tracking_sample.price = sales_order_item.unit_price,
 out_order_tracking_sample.cost = sales_order_item.cost
 ;

UPDATE 
 ((bob_live_mx.sales_order_address
	RIGHT JOIN 	(rafael.out_order_tracking_sample
			         INNER JOIN bob_live_mx.sales_order 
               ON out_order_tracking_sample.order_number = sales_order.order_nr
							)ON sales_order_address.id_sales_order_address = sales_order.fk_sales_order_address_billing
	)LEFT JOIN bob_live_mx.customer_address_region 
	 ON sales_order_address.fk_customer_address_region = customer_address_region.id_customer_address_region
 )
INNER JOIN bob_live_mx.sales_order_item ON out_order_tracking_sample.order_item_id = sales_order_item.id_sales_order_item
SET 
 out_order_tracking_sample.supplier_leadtime = sales_order_item.delivery_time_supplier
WHERE supplier_leadtime is null
 ;

UPDATE rafael.out_order_tracking_sample
INNER JOIN bob_live_mx.catalog_simple ON out_order_tracking_sample.sku_simple = catalog_simple.sku
INNER JOIN bob_live_mx.catalog_config ON catalog_simple.fk_catalog_config = catalog_config.id_catalog_config
INNER JOIN bob_live_mx.supplier ON catalog_config.fk_supplier = supplier.id_supplier
SET out_order_tracking_sample.sku_config = catalog_config.sku,
 out_order_tracking_sample.sku_name = catalog_config. NAME,
 out_order_tracking_sample.supplier_id = supplier.id_supplier,
 out_order_tracking_sample.supplier_name = supplier. NAME,
 out_order_tracking_sample.min_delivery_time = CASE
WHEN out_order_tracking_sample.min_delivery_time IS NULL THEN
	catalog_simple.min_delivery_time
ELSE
	out_order_tracking_sample.min_delivery_time
END,
 out_order_tracking_sample.max_delivery_time = CASE
WHEN out_order_tracking_sample.max_delivery_time IS NULL THEN
	catalog_simple.max_delivery_time
ELSE
	out_order_tracking_sample.max_delivery_time
END,
 out_order_tracking_sample.package_height = catalog_config.package_height,
 out_order_tracking_sample.package_length = catalog_config.package_length,
 out_order_tracking_sample.package_width = catalog_config.package_width,
 out_order_tracking_sample.package_weight = catalog_config.package_weight;


UPDATE rafael.out_order_tracking_sample
INNER JOIN bob_live_mx.catalog_simple ON out_order_tracking_sample.sku_simple = catalog_simple.sku
INNER JOIN bob_live_mx.catalog_config ON catalog_simple.fk_catalog_config = catalog_config.id_catalog_config
SET out_order_tracking_sample.supplier_leadtime = CASE
WHEN out_order_tracking_sample.supplier_leadtime IS NULL THEN
	(
		CASE
		WHEN catalog_config.supplier_lead_time IS NULL THEN
			0
		ELSE
			catalog_config.supplier_lead_time
		END
	)
ELSE
	out_order_tracking_sample.supplier_leadtime
END;


UPDATE rafael.out_order_tracking_sample
INNER JOIN wmsprod.itens_venda
	ON out_order_tracking_sample.order_item_id = itens_venda.item_id
INNER JOIN wmsprod.status_itens_venda 
	ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
SET out_order_tracking_sample.fullfilment_type_real = "Inventory"
WHERE
	status_itens_venda. STATUS = "Estoque reservado";

UPDATE (
	rafael.out_order_tracking_sample
	INNER JOIN wmsprod.itens_venda ON out_order_tracking_sample.order_item_id = itens_venda.item_id
)
INNER JOIN wmsprod.status_itens_venda ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
SET out_order_tracking_sample.fullfilment_type_real = "dropshipping"
WHERE
	status_itens_venda. STATUS IN (
		"DS estoque reservado",
		'Waiting dropshipping',
    'dropshipping notified',
		'Electronic good');

UPDATE rafael.out_order_tracking_sample
INNER JOIN (
	wmsprod.itens_venda
	INNER JOIN wmsprod.status_itens_venda ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
) ON out_order_tracking_sample.order_item_id = itens_venda.item_id
SET out_order_tracking_sample.fullfilment_type_real = "crossdock"
WHERE
	status_itens_venda. STATUS = "analisando quebra"
OR status_itens_venda. STATUS = "aguardando estoque";


TRUNCATE
	rafael.pro_min_date_exported;


INSERT INTO rafael.pro_min_date_exported (
	fk_sales_order_item,
	NAME,
	min_of_created_at
) 
SELECT
	sales_order_item_status_history.fk_sales_order_item,
	sales_order_item_status. NAME,
	min(	sales_order_item_status_history.created_at) AS min_of_created_at
FROM
	bob_live_mx.sales_order_item_status
INNER JOIN bob_live_mx.sales_order_item_status_history 
	ON sales_order_item_status.id_sales_order_item_status = sales_order_item_status_history.fk_sales_order_item_status
GROUP BY
	sales_order_item_status_history.fk_sales_order_item,
	sales_order_item_status. NAME
HAVING sales_order_item_status. NAME IN( 	"exported", 
																					"exportable",
																					"exported electronically")
;


UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.pro_min_date_exported ON out_order_tracking_sample.order_item_id = pro_min_date_exported.fk_sales_order_item
SET out_order_tracking_sample.date_exported = date(min_of_created_at),
 out_order_tracking_sample.date_time_exported = min_of_created_at;


UPDATE rafael.out_order_tracking_sample
INNER JOIN wmsprod.itens_venda ON out_order_tracking_sample.order_item_id = itens_venda.item_id
INNER JOIN wmsprod.status_itens_venda ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
SET 
 out_order_tracking_sample.date_assigned_to_stock = date(DATA),
 out_order_tracking_sample.date_procured = date(DATA),
 out_order_tracking_sample.date_time_procured = DATA
WHERE
	status_itens_venda. STATUS = "estoque reservado";

UPDATE rafael.out_order_tracking_sample
INNER JOIN wmsprod.itens_venda ON out_order_tracking_sample.order_item_id = itens_venda.item_id
INNER JOIN wmsprod.status_itens_venda ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
SET 
 out_order_tracking_sample.date_stockout_declared = date(DATA)
WHERE
	status_itens_venda. STATUS = "quebrado";

UPDATE rafael.out_order_tracking_sample
INNER JOIN wmsprod.itens_venda ON out_order_tracking_sample.order_item_id = itens_venda.item_id
INNER JOIN wmsprod.itens_recebimento ON itens_venda.itens_venda_id = itens_recebimento.itens_venda_id
SET out_order_tracking_sample.date_procured = date(itens_recebimento.data_criacao),
 out_order_tracking_sample.date_time_procured = itens_recebimento.data_criacao
WHERE itens_recebimento.data_criacao IS NOT NULL;


UPDATE rafael.out_order_tracking_sample
INNER JOIN wmsprod.itens_venda ON out_order_tracking_sample.order_item_id = itens_venda.item_id
INNER JOIN wmsprod.status_itens_venda ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
SET out_order_tracking_sample.date_procured = date(DATA),
 out_order_tracking_sample.date_time_procured = DATA
WHERE
  out_order_tracking_sample.fullfilment_type_real = "dropshipping"
AND
	status_itens_venda. STATUS = "DS estoque reservado";

/*UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.fullfilment_type_real = "crossdock"
WHERE
	out_order_tracking_sample.fullfilment_type_real = "dropshipping"
AND date_procured IS NOT NULL;*/

UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_ready_to_pick = CASE
WHEN dayofweek(date_procured) = 1 THEN
	rafael.workday (date_procured, 1)
ELSE
	(
		CASE
		WHEN dayofweek(date_procured) = 7 THEN
			rafael.workday (date_procured, 1)
		ELSE
			date_procured
		END
	)
END;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_time_ready_to_pick = CASE
WHEN dayofweek(date_time_procured) = 1 THEN
	rafael.workday (date_time_procured, 1)
ELSE
	(
		CASE
		WHEN dayofweek(date_time_procured) = 7 THEN
			rafael.workday (date_time_procured, 1)
		ELSE
			date_time_procured
		END
	)
END;


UPDATE rafael.out_order_tracking_sample
SET 
 out_order_tracking_sample.date_ready_to_pick = 
	CASE
	WHEN date_ready_to_pick IN ('2012-11-19',
															'2012-12-25',
															'2013-1-1',
															'2013-2-4',
															'2013-3-18',
															'2013-05-01',
															'2013-12-25',
                              '2014-01-01'

														) 
	THEN rafael.workday (date_ready_to_pick, 1)
	WHEN date_ready_to_pick IN ('2013-3-28',
															'2013-12-24','2013-12-31')
	THEN rafael.workday (date_ready_to_pick, 2)
	ELSE date_ready_to_pick
	END,
 out_order_tracking_sample.date_time_ready_to_pick = CASE
	WHEN date_ready_to_pick IN ('2012-11-19',
															'2012-12-25',
															'2013-1-1',
															'2013-2-4',
															'2013-3-18',
															'2013-05-01',
															'2013-12-25',
                              '2014-01-01'
														) 
	THEN rafael.workday (date_time_ready_to_pick, 1)
	WHEN date_ready_to_pick IN ('2013-3-28',
															'2013-12-24','2013-12-31')
	THEN rafael.workday (date_time_ready_to_pick, 2)
	ELSE date_time_ready_to_pick
	END;


TRUNCATE rafael.pro_max_date_ready_to_ship;


INSERT INTO rafael.pro_max_date_ready_to_ship (date_ready, order_item_id) SELECT
	min(DATA) AS date_ready,
	out_order_tracking_sample.order_item_id
FROM
	rafael.out_order_tracking_sample
INNER JOIN wmsprod.itens_venda ON out_order_tracking_sample.order_item_id = itens_venda.item_id
INNER JOIN wmsprod.status_itens_venda ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
WHERE
	status_itens_venda. STATUS = "faturado"
GROUP BY
	out_order_tracking_sample.order_item_id;


UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.pro_max_date_ready_to_ship ON out_order_tracking_sample.order_item_id = pro_max_date_ready_to_ship.order_item_id
SET out_order_tracking_sample.date_ready_to_ship = date(
	pro_max_date_ready_to_ship.date_ready
),
 out_order_tracking_sample.date_time_ready_to_ship = pro_max_date_ready_to_ship.date_ready;


UPDATE rafael.out_order_tracking_sample
INNER JOIN (
	wmsprod.itens_venda b
	INNER JOIN wmsprod.status_itens_venda d ON b.itens_venda_id = d.itens_venda_id
) ON out_order_tracking_sample.order_item_id = b.item_id
SET out_order_tracking_sample.date_shipped = (
	SELECT
		max(date(DATA))
	FROM
		wmsprod.itens_venda c,
		wmsprod.status_itens_venda e
	WHERE
		e. STATUS = "expedido"
	AND b.itens_venda_id = c.itens_venda_id
	AND e.itens_venda_id = d.itens_venda_id
),
 out_order_tracking_sample.date_time_shipped = (
	SELECT
		max(DATA)
	FROM
		wmsprod.itens_venda c,
		wmsprod.status_itens_venda e
	WHERE
		e. STATUS = "expedido"
	AND b.itens_venda_id = c.itens_venda_id
	AND e.itens_venda_id = d.itens_venda_id
);


UPDATE rafael.out_order_tracking_sample
INNER JOIN wmsprod.vw_itens_venda_entrega vw ON out_order_tracking_sample.order_item_id = vw.item_id
INNER JOIN wmsprod.tms_status_delivery del ON vw.cod_rastreamento = del.cod_rastreamento
SET out_order_tracking_sample.date_delivered = (
	SELECT
		CASE
	WHEN tms.date < '2012-05-01' THEN
		NULL
	ELSE
		max(tms.date)
	END
	FROM
		wmsprod.vw_itens_venda_entrega itens,
		wmsprod.tms_status_delivery tms
	WHERE
		itens.cod_rastreamento = vw.cod_rastreamento
	AND tms.cod_rastreamento = del.cod_rastreamento
	AND tms.id_status = 4
);

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  updated_at
  )
SELECT 
  'Mexico', 
  'out_order_tracking_sample.date_delivered',
  NOW()
;

UPDATE rafael.out_order_tracking_sample
INNER JOIN wmsprod.itens_venda ON out_order_tracking_sample.order_item_id = itens_venda.item_id
SET out_order_tracking_sample.wh_time = tempo_armazem
WHERE
	tempo_armazem > 1;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_procured_promised = rafael.workday (
	date_exported,
	supplier_leadtime
),
 out_order_tracking_sample.date_delivered_promised = rafael.workday (
	date_exported,
	max_delivery_time
)
WHERE
	out_order_tracking_sample.date_exported IS NOT NULL;


/*
UPDATE rafael.out_order_tracking_sample
INNER JOIN wmsprod.itens_venda ON out_order_tracking_sample.order_item_id = itens_venda.item_id
INNER JOIN wmsprod.status_itens_venda ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
SET out_order_tracking_sample.date_procured_promised = rafael.workday (status_itens_venda.data,
	2)

WHERE
	out_order_tracking_sample.date_exported IS NOT NULL
AND
  wmsprod.status_itens_venda.`status` = "dropshipping notified"
AND
 out_order_tracking_sample.fullfilment_type_real = "dropshipping" ; 
*/

UPDATE rafael.out_order_tracking_sample 
INNER JOIN bob_live_mx.sales_order_item ON sales_order_item.id_sales_order_item = out_order_tracking_sample.order_item_id
SET out_order_tracking_sample.is_linio_promise = sales_order_item.is_linio_promise;


UPDATE rafael.out_order_tracking_sample
SET is_linio_promise = 0
WHERE
	is_linio_promise IS NULL;


UPDATE rafael.out_order_tracking_sample
SET date_delivered_promised = CASE
WHEN payment_method IN (
	'Banorte_PagoReferenciado',
	'Oxxo_Direct'
) THEN
	rafael.workday (date_exported, 2)
ELSE
	rafael.workday (date_ordered, 2)
END
WHERE
	is_linio_promise = 1;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_procured_promised = rafael.workday (out_order_tracking_sample.date_procured_promised,1)
WHERE
	out_order_tracking_sample.date_procured_promised >= '2012-11-19'
AND out_order_tracking_sample.date_exported < '2012-11-19';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_delivered_promised = rafael.workday (out_order_tracking_sample.date_delivered_promised,1)
WHERE
	out_order_tracking_sample.date_delivered_promised >= '2012-11-19'
AND out_order_tracking_sample.date_exported < '2012-11-19';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_procured_promised = rafael.workday (out_order_tracking_sample.date_procured_promised,1)
WHERE
	out_order_tracking_sample.date_procured_promised >= '2012-12-25'
AND out_order_tracking_sample.date_exported < '2012-12-25';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_delivered_promised = rafael.workday (
	out_order_tracking_sample.date_delivered_promised,
	1
)
WHERE
	out_order_tracking_sample.date_delivered_promised >= '2012-12-25'
AND out_order_tracking_sample.date_exported < '2012-12-25';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_procured_promised = rafael.workday (
	out_order_tracking_sample.date_procured_promised,
	1
)
WHERE
	out_order_tracking_sample.date_procured_promised >= '2013-1-1'
AND out_order_tracking_sample.date_exported < '2013-1-1';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_delivered_promised = rafael.workday (
	out_order_tracking_sample.date_delivered_promised,
	1
)
WHERE
	out_order_tracking_sample.date_delivered_promised >= '2013-1-1'
AND out_order_tracking_sample.date_exported < '2013-1-1';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_ready_to_ship_promised = rafael.workday (
	out_order_tracking_sample.date_ready_to_ship_promised,
	1
)
WHERE
	out_order_tracking_sample.date_ready_to_ship_promised >= '2013-1-1'
AND out_order_tracking_sample.date_exported < '2013-1-1';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_procured_promised = rafael.workday (
	out_order_tracking_sample.date_procured_promised,
	1
)
WHERE
	out_order_tracking_sample.date_procured_promised >= '2013-2-4'
AND out_order_tracking_sample.date_exported < '2013-2-4';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_delivered_promised = rafael.workday (
	out_order_tracking_sample.date_delivered_promised,
	1
)
WHERE
	out_order_tracking_sample.date_delivered_promised >= '2013-2-4'
AND out_order_tracking_sample.date_exported < '2013-2-4';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_ready_to_ship_promised = rafael.workday (
	out_order_tracking_sample.date_ready_to_ship_promised,
	1
)
WHERE
	out_order_tracking_sample.date_ready_to_ship_promised >= '2013-2-4'
AND out_order_tracking_sample.date_exported < '2013-2-4';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_procured_promised = rafael.workday (
	out_order_tracking_sample.date_procured_promised,
	1
)
WHERE
	out_order_tracking_sample.date_procured_promised >= '2013-3-18'
AND out_order_tracking_sample.date_exported < '2013-3-18';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_delivered_promised = rafael.workday (
	out_order_tracking_sample.date_delivered_promised,
	1
)
WHERE
	out_order_tracking_sample.date_delivered_promised >= '2013-3-18'
AND out_order_tracking_sample.date_exported < '2013-3-18';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_ready_to_ship_promised = rafael.workday (
	out_order_tracking_sample.date_ready_to_ship_promised,
	1
)
WHERE
	out_order_tracking_sample.date_ready_to_ship_promised >= '2013-3-18'
AND out_order_tracking_sample.date_exported < '2013-3-18';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_procured_promised = rafael.workday (
	out_order_tracking_sample.date_procured_promised,
	1
)
WHERE
	out_order_tracking_sample.date_procured_promised >= '2013-3-28'
AND out_order_tracking_sample.date_exported < '2013-3-28';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_delivered_promised = rafael.workday (
	out_order_tracking_sample.date_delivered_promised,
	1
)
WHERE
	out_order_tracking_sample.date_delivered_promised >= '2013-3-28'
AND out_order_tracking_sample.date_exported < '2013-3-28';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_ready_to_ship_promised = rafael.workday (
	out_order_tracking_sample.date_ready_to_ship_promised,
	1
)
WHERE
	out_order_tracking_sample.date_ready_to_ship_promised >= '2013-3-28'
AND out_order_tracking_sample.date_exported < '2013-3-28';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_procured_promised = rafael.workday (
	out_order_tracking_sample.date_procured_promised,
	1
)
WHERE
	out_order_tracking_sample.date_procured_promised >= '2013-3-29'
AND out_order_tracking_sample.date_exported < '2013-3-29';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_delivered_promised = rafael.workday (
	out_order_tracking_sample.date_delivered_promised,
	1
)
WHERE
	out_order_tracking_sample.date_delivered_promised >= '2013-3-29'
AND out_order_tracking_sample.date_exported < '2013-3-29';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_ready_to_ship_promised = rafael.workday (
	out_order_tracking_sample.date_ready_to_ship_promised,
	1
)
WHERE
	out_order_tracking_sample.date_ready_to_ship_promised >= '2013-3-29'
AND out_order_tracking_sample.date_exported < '2013-3-29';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_procured_promised = rafael.workday (
	out_order_tracking_sample.date_procured_promised,
	1
)
WHERE
	out_order_tracking_sample.date_procured_promised >= '2013-05-01'
AND out_order_tracking_sample.date_exported < '2013-05-01';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_delivered_promised = rafael.workday (
	out_order_tracking_sample.date_delivered_promised,
	1
)
WHERE
	out_order_tracking_sample.date_delivered_promised >= '2013-05-01'
AND out_order_tracking_sample.date_exported < '2013-05-01';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_ready_to_ship_promised = rafael.workday (
	out_order_tracking_sample.date_ready_to_ship_promised,
	1
)
WHERE
	out_order_tracking_sample.date_ready_to_ship_promised >= '2013-05-01'
AND out_order_tracking_sample.date_exported < '2013-05-01';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_procured_promised = rafael.workday (
	out_order_tracking_sample.date_procured_promised,
	1
)
WHERE
	out_order_tracking_sample.date_procured_promised >= '2013-09-16'
AND out_order_tracking_sample.date_exported < '2013-09-16';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_delivered_promised = rafael.workday (
	out_order_tracking_sample.date_delivered_promised,
	1
)
WHERE
	out_order_tracking_sample.date_delivered_promised >= '2013-09-16'
AND out_order_tracking_sample.date_exported < '2013-09-16';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_ready_to_ship_promised = rafael.workday (
	out_order_tracking_sample.date_ready_to_ship_promised,
	1
)
WHERE
	out_order_tracking_sample.date_ready_to_ship_promised >= '2013-05-01'
AND out_order_tracking_sample.date_exported < '2013-09-16';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_ready_to_ship_promised = rafael.workday (
	date_procured_promised,
	wh_time
);


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_ready_to_ship_promised = rafael.workday (out_order_tracking_sample.date_ready_to_ship_promised,1)
WHERE
	out_order_tracking_sample.date_ready_to_ship_promised >= '2013-03-18'
AND 
  out_order_tracking_sample.date_procured_promised < '2013-03-18';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_ready_to_ship_promised = rafael.workday (out_order_tracking_sample.date_ready_to_ship_promised,1)
WHERE
  out_order_tracking_sample.date_ready_to_ship_promised >= '2013-03-28'
AND
  out_order_tracking_sample.date_procured_promised < '2013-03-28';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_ready_to_ship_promised = rafael.workday (out_order_tracking_sample.date_ready_to_ship_promised,1)
WHERE
  out_order_tracking_sample.date_ready_to_ship_promised >= '2013-03-29'
AND
  out_order_tracking_sample.date_procured_promised < '2013-03-29';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_ready_to_ship_promised = rafael.workday (out_order_tracking_sample.date_ready_to_ship_promised,1)
WHERE
  out_order_tracking_sample.date_ready_to_ship_promised >= '2013-05-01'
AND
  out_order_tracking_sample.date_procured_promised < '2013-05-01';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_ready_to_ship_promised = rafael.workday (out_order_tracking_sample.date_ready_to_ship_promised,1)
WHERE
  out_order_tracking_sample.date_ready_to_ship_promised >= '2013-09-16'
AND
  out_order_tracking_sample.date_procured_promised < '2013-09-16';



UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_procured_promised = rafael.workday (out_order_tracking_sample.date_procured_promised,1)
WHERE
	out_order_tracking_sample.date_procured_promised >= '2013-11-18'
AND out_order_tracking_sample.date_exported < '2013-11-18';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_delivered_promised = rafael.workday (out_order_tracking_sample.date_delivered_promised,1)
WHERE
	out_order_tracking_sample.date_delivered_promised >= '2013-11-18'
AND out_order_tracking_sample.date_exported < '2013-11-18';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_ready_to_ship_promised = rafael.workday (out_order_tracking_sample.date_ready_to_ship_promised,1)
WHERE
	out_order_tracking_sample.date_ready_to_ship_promised >= '2013-11-18'
AND out_order_tracking_sample.date_exported < '2013-11-18';

UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_procured_promised = rafael.workday (out_order_tracking_sample.date_procured_promised,2)
WHERE
	out_order_tracking_sample.date_procured_promised >= '2013-12-24'
AND out_order_tracking_sample.date_exported < '2013-12-24';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_delivered_promised = rafael.workday (out_order_tracking_sample.date_delivered_promised,2)
WHERE
	out_order_tracking_sample.date_delivered_promised >= '2013-12-24'
AND out_order_tracking_sample.date_exported < '2013-12-24';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_ready_to_ship_promised = rafael.workday (out_order_tracking_sample.date_ready_to_ship_promised,2)
WHERE
	out_order_tracking_sample.date_ready_to_ship_promised >= '2013-12-24'
AND out_order_tracking_sample.date_exported < '2013-12-24';

UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_procured_promised = rafael.workday (out_order_tracking_sample.date_procured_promised,1)
WHERE
	out_order_tracking_sample.date_procured_promised >= '2013-12-25'
AND out_order_tracking_sample.date_exported < '2013-12-25';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_delivered_promised = rafael.workday (out_order_tracking_sample.date_delivered_promised,1)
WHERE
	out_order_tracking_sample.date_delivered_promised >= '2013-12-25'
AND out_order_tracking_sample.date_exported < '2013-12-25';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_ready_to_ship_promised = rafael.workday (out_order_tracking_sample.date_ready_to_ship_promised,1)
WHERE
	out_order_tracking_sample.date_ready_to_ship_promised >= '2013-12-25'
AND out_order_tracking_sample.date_exported < '2013-12-25';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_procured_promised = rafael.workday (out_order_tracking_sample.date_procured_promised,2)
WHERE
	out_order_tracking_sample.date_procured_promised >= '2013-12-31'
AND out_order_tracking_sample.date_exported < '2013-12-31';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_delivered_promised = rafael.workday (out_order_tracking_sample.date_delivered_promised,2)
WHERE
	out_order_tracking_sample.date_delivered_promised >= '2013-12-31'
AND out_order_tracking_sample.date_exported < '2013-12-31';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_ready_to_ship_promised = rafael.workday (out_order_tracking_sample.date_ready_to_ship_promised,2)
WHERE
	out_order_tracking_sample.date_ready_to_ship_promised >= '2013-12-31'
AND out_order_tracking_sample.date_exported < '2013-12-31';

UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_procured_promised = rafael.workday (out_order_tracking_sample.date_procured_promised,1)
WHERE
	out_order_tracking_sample.date_procured_promised >= '2014-01-01'
AND out_order_tracking_sample.date_exported < '2014-01-01';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_delivered_promised = rafael.workday (out_order_tracking_sample.date_delivered_promised,1)
WHERE
	out_order_tracking_sample.date_delivered_promised >= '2014-01-01'
AND out_order_tracking_sample.date_exported < '2014-01-01';


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_ready_to_ship_promised = rafael.workday (out_order_tracking_sample.date_ready_to_ship_promised,1)
WHERE
	out_order_tracking_sample.date_ready_to_ship_promised >= '2014-01-01'
AND out_order_tracking_sample.date_exported < '2014-01-01';

UPDATE rafael.out_order_tracking_sample
INNER JOIN bob_live_mx.sales_order_item ON out_order_tracking_sample.order_item_id = sales_order_item.id_sales_order_item
INNER JOIN bob_live_mx.catalog_shipment_type ON sales_order_item.fk_catalog_shipment_type = catalog_shipment_type.id_catalog_shipment_type
SET out_order_tracking_sample.fullfilment_type_bob = catalog_shipment_type. NAME;


DELETE rafael.pro_1st_attempt.*
FROM
	rafael.pro_1st_attempt;


INSERT INTO rafael.pro_1st_attempt (order_item_id, min_of_date) SELECT
	out_order_tracking_sample.order_item_id,
	min(tms_status_delivery.date) AS min_of_date
FROM
	(
		rafael.out_order_tracking_sample
		INNER JOIN wmsprod.vw_itens_venda_entrega ON out_order_tracking_sample.order_item_id = vw_itens_venda_entrega.item_id
	)
INNER JOIN wmsprod.tms_status_delivery ON vw_itens_venda_entrega.cod_rastreamento = tms_status_delivery.cod_rastreamento
GROUP BY
	out_order_tracking_sample.order_item_id,
	tms_status_delivery.id_status
HAVING
	tms_status_delivery.id_status = 5;


UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.pro_1st_attempt ON out_order_tracking_sample.order_item_id = pro_1st_attempt.order_item_id
SET out_order_tracking_sample.date_1st_attempt = min_of_date;


UPDATE rafael.out_order_tracking_sample
INNER JOIN wmsprod.vw_itens_venda_entrega vw ON out_order_tracking_sample.order_item_id = vw.item_id
INNER JOIN wmsprod.tms_status_delivery del ON vw.cod_rastreamento = del.cod_rastreamento
SET out_order_tracking_sample.date_1st_attempt = (
	SELECT
		max(tms.date)
	FROM
		wmsprod.tms_status_delivery tms,
		wmsprod.vw_itens_venda_entrega itens
	WHERE
		del.cod_rastreamento = tms.cod_rastreamento
	AND vw.item_id = itens.item_id
	AND tms.id_status = 4
)
WHERE
	out_order_tracking_sample.date_1st_attempt IS NULL
OR out_order_tracking_sample.date_1st_attempt < '2011-05-01';

UPDATE rafael.out_order_tracking_sample
INNER JOIN (
	SELECT
		a.item_id,
		a.tempo_de_entrega,
		a.tempo_entrega_fornecedor,
		a.data_criacao,
		a.estimated_delivery_date,
		a.estimated_delivery_days,
		a.estimated_days_on_wh,
		b.days_back,
		b.date_created
	FROM
		wmsprod.itens_venda a
	JOIN wmsprod.item_backorder b ON a.itens_venda_id = b.itens_venda_id
) c ON out_order_tracking_sample.order_item_id = c.item_id
SET out_order_tracking_sample.date_backorder_created = c.date_created;

UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.has_been_backorder = CASE
WHEN date_backorder_created IS NOT NULL THEN
	1
ELSE
	0
END;

#Nuevo día de delivery promised si fue backorder
UPDATE rafael.out_order_tracking_sample
INNER JOIN wmsprod.itens_venda ON out_order_tracking_sample.order_item_id = itens_venda.item_id
SET out_order_tracking_sample.date_delivered_promised = itens_venda.estimated_delivery_date
WHERE
	has_been_backorder = 1;

UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.week_exported = rafael.week_iso (date_exported),
 out_order_tracking_sample.week_procured_promised = rafael.week_iso (date_procured_promised),
 out_order_tracking_sample.week_delivered_promised = rafael.week_iso (date_delivered_promised),
 out_order_tracking_sample.month_num_delivered_promised = date_format(
	date_delivered_promised,
	"%Y-%m"
),
 out_order_tracking_sample.month_num_procured_promised = date_format(
	date_procured_promised,
	"%Y-%m"
),
 out_order_tracking_sample.week_ready_to_ship_promised = rafael.week_iso (
	date_ready_to_ship_promised
),
 out_order_tracking_sample.week_ready_to_pick = rafael.week_iso (date_ready_to_pick),
 rafael.out_order_tracking_sample.week_ordered = rafael.week_iso (date_ordered),
 month_num_ready_to_pick = date_format(date_ready_to_pick, "%Y-%m"),
 out_order_tracking_sample.week_shipped = rafael.week_iso (date_shipped),
 out_order_tracking_sample.month_num_ordered = date_format(date_ordered, "%Y-%m");


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.month_procured_promised = monthname(date_procured_promised);


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.month_num_procured_promised = date_format(
	date_procured_promised,
	"%Y-%m"
);


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.month_delivered_promised = monthname(date_delivered_promised);


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.month_num_delivered_promised = date_format(
	date_delivered_promised,
	"%Y-%m"
);


UPDATE rafael.out_order_tracking_sample
SET month_num_procured_promised = '2012-12'
WHERE
	date_procured_promised = '2012-12-31';


UPDATE rafael.out_order_tracking_sample
SET month_num_delivered_promised = '2012-12'
WHERE
	date_delivered_promised = '2012-12-31';


UPDATE rafael.out_order_tracking_sample
INNER JOIN wmsprod.itens_venda ON out_order_tracking_sample.order_item_id = itens_venda.item_id
INNER JOIN wmsprod.romaneio ON itens_venda.romaneio_id = romaneio.romaneio_id
INNER JOIN wmsprod.transportadoras ON romaneio.transportadora_id = transportadoras.transportadoras_id
SET out_order_tracking_sample.shipping_carrier = nome_transportadora;


UPDATE rafael.out_order_tracking_sample ot
INNER JOIN bob_live_mx.sales_order_item soi ON ot.order_item_id = soi.id_sales_order_item
INNER JOIN bob_live_mx.shipment_carrier sc ON soi.fk_shipment_carrier = sc.id_shipment_carrier
SET ot.shipping_carrier_srt = sc. NAME;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.year_procured_promised = date_format(
	date_procured_promised,
	"%Y"
);


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.year_delivered_promised = date_format(
	date_delivered_promised,
	"%Y"
);


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.sl0 = 1
WHERE
	(
		(
			(
				out_order_tracking_sample.supplier_leadtime
			) = 0
		)
	);


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.stockout = "1"
WHERE
	out_order_tracking_sample.status_wms = "quebra tratada"
OR out_order_tracking_sample.status_wms = "quebrado";

# Nuevo Calc WD

#Workdays to export
UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_ordered = calcworkdays.date_first
		AND out_order_tracking_sample.date_exported = calcworkdays.date_last
SET out_order_tracking_sample.workdays_to_export =	calcworkdays.workdays;

UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_ordered = calcworkdays.date_first
		AND curdate() = calcworkdays.date_last
SET out_order_tracking_sample.workdays_to_export =	calcworkdays.workdays
WHERE date_exported IS NULL;

#Workdays to procure
UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_exported 			= calcworkdays.date_first
		AND out_order_tracking_sample.date_procured 	= calcworkdays.date_last
SET out_order_tracking_sample.workdays_to_procure 	=	calcworkdays.workdays
WHERE stockout = 0;

UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_exported 			= calcworkdays.date_first
		AND curdate() 																= calcworkdays.date_last
SET out_order_tracking_sample.workdays_to_procure =	calcworkdays.workdays
WHERE stockout = 0
AND date_procured IS NULL;

#Workdays to ready
UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_ready_to_pick	= calcworkdays.date_first
		AND out_order_tracking_sample.date_ready_to_ship 	= calcworkdays.date_last
SET out_order_tracking_sample.workdays_to_ready 	=	calcworkdays.workdays
;

UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_ready_to_pick	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET out_order_tracking_sample.workdays_to_ready 	=	calcworkdays.workdays
WHERE date_ready_to_ship IS NULL;

#Workdays to ship
UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_ready_to_ship	= calcworkdays.date_first
		AND out_order_tracking_sample.date_shipped 		= calcworkdays.date_last
SET out_order_tracking_sample.workdays_to_ship 		=	calcworkdays.workdays
;

UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_ready_to_ship	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET out_order_tracking_sample.workdays_to_ship 	=	calcworkdays.workdays
WHERE date_shipped IS NULL;

#workdays_to_1st_attempt
UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_shipped	= calcworkdays.date_first
		AND out_order_tracking_sample.date_1st_attempt 		= calcworkdays.date_last
SET out_order_tracking_sample.workdays_to_1st_attempt 		=	calcworkdays.workdays
;

UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_shipped	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET out_order_tracking_sample.workdays_to_1st_attempt 	=	calcworkdays.workdays
WHERE date_1st_attempt IS NULL;

#workdays_to_deliver
UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_shipped	= calcworkdays.date_first
		AND out_order_tracking_sample.date_delivered 		= calcworkdays.date_last
SET out_order_tracking_sample.workdays_to_deliver 		=	calcworkdays.workdays
;

UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_shipped	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET out_order_tracking_sample.workdays_to_deliver 	=	calcworkdays.workdays
WHERE date_delivered IS NULL;

#workdays_total_1st_attempt
UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_exported	= calcworkdays.date_first
		AND out_order_tracking_sample.date_1st_attempt 		= calcworkdays.date_last
SET out_order_tracking_sample.workdays_total_1st_attempt 		=	calcworkdays.workdays
WHERE date_shipped IS NOT NULL;

UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_exported	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET out_order_tracking_sample.workdays_total_1st_attempt 	=	calcworkdays.workdays
WHERE date_shipped IS NOT NULL
AND date_1st_attempt IS NULL;

#workdays_total_delivery
UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_exported	= calcworkdays.date_first
		AND out_order_tracking_sample.date_delivered 		= calcworkdays.date_last
SET out_order_tracking_sample.workdays_total_delivery 		=	calcworkdays.workdays
WHERE date_shipped IS NOT NULL;

UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_exported	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET out_order_tracking_sample.workdays_total_delivery 	=	calcworkdays.workdays
WHERE date_shipped IS NOT NULL
AND date_1st_attempt IS NULL;

# Aquí acaba nuevo calcworkdays

UPDATE rafael.out_order_tracking_sample
SET days_to_export = CASE
WHEN date_ordered IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN date_exported IS NULL THEN
			datediff(curdate(), date_ordered)
		ELSE
			datediff(date_exported, date_ordered)
		END
	)
END,
 days_to_procure = CASE
WHEN stockout = 1 THEN
	NULL
ELSE
	(
		CASE
		WHEN date_procured < date_exported THEN
			0
		ELSE
			(
				CASE
				WHEN date_procured IS NULL THEN
					datediff(curdate(), date_exported)
				ELSE
					datediff(
						date_procured,
						date_exported
					)
				END
			)
		END
	)
END,
 days_to_ready = CASE
WHEN date_ready_to_pick IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN date_ready_to_ship IS NULL THEN
			datediff(
				curdate(),
				date_ready_to_pick
			)
		ELSE
			datediff(
				date_ready_to_ship,
				date_ready_to_pick
			)
		END
	)
END,
 days_to_ship = CASE
WHEN date_ready_to_ship IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN date_shipped IS NULL THEN
			datediff(
				curdate(),
				date_ready_to_ship
			)
		ELSE
			datediff(
				date_shipped,
				date_ready_to_ship
			)
		END
	)
END,
 days_to_1st_attempt = CASE
WHEN date_shipped IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN date_1st_attempt IS NULL THEN
			datediff(curdate(), date_shipped)
		ELSE
			datediff(
				date_1st_attempt,
				date_shipped
			)
		END
	)
END,
 out_order_tracking_sample.days_total_delivery = CASE
WHEN date_shipped IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN date_delivered IS NULL THEN
			datediff(curdate(), date_ordered)
		ELSE
			datediff(
				date_delivered,
				date_ordered
			)
		END
	)
END;


UPDATE rafael.out_order_tracking_sample
SET date_delivered_errors = CASE
WHEN days_total_delivery < 0 THEN
	1
ELSE
	0
END;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.delay_to_procure = CASE
WHEN date_procured IS NULL THEN
	(
		CASE
		WHEN curdate() > date_procured_promised THEN
			1
		ELSE
			0
		END
	)
ELSE
	(
		CASE
		WHEN date_procured > date_procured_promised THEN
			1
		ELSE
			0
		END
	)
END,
 out_order_tracking_sample.delay_to_ready = CASE
WHEN workdays_to_ready > 1 THEN
	1
ELSE
	0
END,
 out_order_tracking_sample.delay_to_ship = CASE
WHEN workdays_to_ship > 1 THEN
	1
ELSE
	0
END,
 out_order_tracking_sample.delay_to_1st_attempt = CASE
WHEN workdays_to_1st_attempt > 2 THEN
	1
ELSE
	0
END,
 out_order_tracking_sample.delay_to_deliver = CASE
WHEN workdays_to_deliver > 2 THEN
	1
ELSE
	0
END,
 out_order_tracking_sample.delay_total_1st_attempt = CASE
WHEN date_1st_attempt IS NULL THEN
	(
		CASE
		WHEN curdate() > date_delivered_promised THEN
			1
		ELSE
			0
		END
	)
ELSE
	(
		CASE
		WHEN date_1st_attempt > date_delivered_promised THEN
			1
		ELSE
			0
		END
	)
END,
 out_order_tracking_sample.delay_total_delivery = CASE
WHEN date_delivered IS NULL THEN
	(
		CASE
		WHEN curdate() > date_delivered_promised THEN
			1
		ELSE
			0
		END
	)
ELSE
	(
		CASE
		WHEN date_delivered > date_delivered_promised THEN
			1
		ELSE
			0
		END
	)
END,
 out_order_tracking_sample.workdays_delay_to_procure = CASE
WHEN workdays_to_procure - supplier_leadtime < 0 THEN
	0
ELSE
	workdays_to_procure - supplier_leadtime
END,
 out_order_tracking_sample.workdays_delay_to_ready = CASE
WHEN workdays_to_ready - 1 < 0 THEN
	0
ELSE
	workdays_to_ready - 1
END,
 out_order_tracking_sample.workdays_delay_to_ship = CASE
WHEN workdays_to_ship - 1 < 0 THEN
	0
ELSE
	workdays_to_ship - 1
END,
 out_order_tracking_sample.workdays_delay_to_1st_attempt = CASE
WHEN workdays_to_1st_attempt - 3 < 0 THEN
	0
ELSE
	workdays_to_1st_attempt - 3
END,
 out_order_tracking_sample.workdays_delay_to_deliver = CASE
WHEN workdays_to_deliver - 3 < 0 THEN
	0
ELSE
	workdays_to_deliver - 3
END,
 out_order_tracking_sample.on_time_to_procure = CASE
WHEN date_procured <= date_procured_promised THEN
	1
ELSE
	0
END,
 out_order_tracking_sample.on_time_total_1st_attempt = CASE
WHEN date_1st_attempt <= date_delivered_promised THEN
	1
ELSE
	0
END,
 out_order_tracking_sample.on_time_total_delivery = CASE
WHEN date_delivered <= date_delivered_promised THEN
	1
WHEN date_delivered IS NULL THEN
	0
ELSE
	0
END;


UPDATE rafael.out_order_tracking_sample
INNER JOIN bob_live_mx.catalog_config ON out_order_tracking_sample.sku_config = catalog_config.sku
SET out_order_tracking_sample.presale = "1"
WHERE
	catalog_config. NAME LIKE "preventa%";


UPDATE rafael.out_order_tracking_sample
INNER JOIN bob_live_mx.catalog_config ON out_order_tracking_sample.sku_config = catalog_config.sku
INNER JOIN bob_live_mx.catalog_attribute_option_global_buyer_name ON catalog_attribute_option_global_buyer_name.id_catalog_attribute_option_global_buyer_name = catalog_config.fk_catalog_attribute_option_global_buyer_name
INNER JOIN bob_live_mx.catalog_attribute_option_global_head_buyer_name ON catalog_attribute_option_global_head_buyer_name.id_catalog_attribute_option_global_head_buyer_name = catalog_config.fk_catalog_attribute_option_global_head_buyer_name
SET out_order_tracking_sample.buyer = catalog_attribute_option_global_buyer_name. NAME,
 out_order_tracking_sample.head_buyer = catalog_attribute_option_global_head_buyer_name. NAME;


UPDATE rafael.out_order_tracking_sample
INNER JOIN bob_live_mx.sales_order_item ON out_order_tracking_sample.order_item_id = sales_order_item.id_sales_order_item
INNER JOIN bob_live_mx.sales_order_item_status ON sales_order_item.fk_sales_order_item_status = sales_order_item_status.id_sales_order_item_status
SET out_order_tracking_sample.status_bob = sales_order_item_status. NAME;


UPDATE rafael.out_order_tracking_sample
INNER JOIN wmsprod.vw_itens_venda_entrega ON out_order_tracking_sample.order_item_id = vw_itens_venda_entrega.item_id
SET out_order_tracking_sample.wms_tracking_code = cod_rastreamento;


UPDATE rafael.out_order_tracking_sample
INNER JOIN wmsprod.tms_tracks ON out_order_tracking_sample.wms_tracking_code = tms_tracks.cod_rastreamento
SET out_order_tracking_sample.shipping_carrier_tracking_code = track;


UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.status_match_bob_wms ON out_order_tracking_sample.status_wms = status_match_bob_wms.status_wms
AND out_order_tracking_sample.status_bob = status_match_bob_wms.status_bob
SET out_order_tracking_sample.status_match_bob_wms = status_match_bob_wms.correct;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.check_dates = CASE
WHEN date_ordered > date_exported THEN
	"date ordered > date exported"
ELSE
	(
		CASE
		WHEN date_exported > date_procured THEN
			"date exported > date procured"
		ELSE
			(
				CASE
				WHEN date_procured > date_ready_to_ship THEN
					"date procured > date ready to ship"
				ELSE
					(
						CASE
						WHEN date_ready_to_ship > date_shipped THEN
							"date ready to ship > date shipped"
						ELSE
							(
								CASE
								WHEN date_shipped > date_1st_attempt THEN
									"date shipped > date 1st attempt"
								ELSE
									(
										CASE
										WHEN date_1st_attempt > date_delivered THEN
											"date shipped > date 1st attempt"
										ELSE
											"correct"
										END
									)
								END
							)
						END
					)
				END
			)
		END
	)
END;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.check_date_1st_attempt = CASE
WHEN date_1st_attempt IS NULL THEN
	(
		CASE
		WHEN date_delivered IS NULL THEN
			0
		ELSE
			1
		END
	)
ELSE
	(
		CASE
		WHEN date_1st_attempt > date_delivered THEN
			1
		ELSE
			0
		END
	)
END;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.check_date_shipped = CASE
WHEN date_shipped IS NULL THEN
	(
		CASE
		WHEN date_1st_attempt IS NULL
		AND date_delivered IS NULL THEN
			0
		ELSE
			1
		END
	)
ELSE
	(
		CASE
		WHEN date_shipped > date_1st_attempt THEN
			1
		ELSE
			0
		END
	)
END;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.check_date_ready_to_ship = CASE
WHEN date_ready_to_ship IS NULL THEN
	(
		CASE
		WHEN date_shipped IS NULL
		AND date_1st_attempt IS NULL
		AND date_delivered IS NULL THEN
			0
		ELSE
			1
		END
	)
ELSE
	(
		CASE
		WHEN date_ready_to_ship > date_shipped THEN
			1
		ELSE
			0
		END
	)
END;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.check_date_procured = CASE
WHEN date_procured IS NULL THEN
	(
		CASE
		WHEN date_ready_to_ship IS NULL
		AND date_shipped IS NULL
		AND date_1st_attempt IS NULL
		AND date_delivered IS NULL THEN
			0
		ELSE
			1
		END
	)
ELSE
	(
		CASE
		WHEN date_procured > date_ready_to_ship THEN
			1
		ELSE
			0
		END
	)
END;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.check_date_exported = CASE
WHEN date_exported IS NULL THEN
	(
		CASE
		WHEN date_procured IS NULL
		AND date_ready_to_ship IS NULL
		AND date_shipped IS NULL
		AND date_1st_attempt IS NULL
		AND date_delivered IS NULL THEN
			0
		ELSE
			1
		END
	)
ELSE
	(
		CASE
		WHEN date_exported > date_procured THEN
			1
		ELSE
			0
		END
	)
END;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.check_date_ordered = CASE
WHEN date_ordered IS NULL THEN
	(
		CASE
		WHEN date_exported IS NULL
		AND date_procured IS NULL
		AND date_ready_to_ship IS NULL
		AND date_shipped IS NULL
		AND date_1st_attempt IS NULL
		AND date_delivered IS NULL THEN
			0
		ELSE
			1
		END
	)
ELSE
	(
		CASE
		WHEN date_ordered > date_exported THEN
			1
		ELSE
			0
		END
	)
END;




UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.still_to_procure = 1
WHERE
	out_order_tracking_sample.date_procured IS NULL;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.still_to_procure = 0
WHERE
	out_order_tracking_sample.status_wms = "quebrado"
OR out_order_tracking_sample.status_wms = "quebra tratada";


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.vol_weight = package_height * package_length * package_width / 5000;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.max_vol_w_vs_w = CASE
WHEN vol_weight > package_weight THEN
	vol_weight
ELSE
	package_weight
END;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.package_measure = CASE
WHEN max_vol_w_vs_w > 45.486 THEN
	"large"
ELSE
	(
		CASE
		WHEN max_vol_w_vs_w > 2.277 THEN
			"medium"
		ELSE
			"small"
		END
	)
END;

UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.package_measure_new = CASE
WHEN max_vol_w_vs_w > 35 THEN
	'oversized'
ELSE
	(
		CASE
		WHEN max_vol_w_vs_w > 5 THEN
			'large'
		ELSE
			(
				CASE
				WHEN max_vol_w_vs_w > 2 THEN
					'medium'
				ELSE
					'small'
				END
			)
		END
	)
END;


UPDATE rafael.out_order_tracking_sample
INNER JOIN bob_live_mx.sales_order ON out_order_tracking_sample.order_number = sales_order.order_nr
INNER JOIN bob_live_mx.sales_order_address ON sales_order.fk_sales_order_address_shipping= sales_order_address.id_sales_order_address
SET out_order_tracking_sample.ship_to_zip = postcode;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.delay_carrier_shipment = CASE
WHEN package_measure = "large" THEN
	(
		CASE
		WHEN workdays_to_1st_attempt - 5 > 0 THEN
			1
		ELSE
			0
		END
	)
ELSE
	(
		CASE
		WHEN workdays_to_1st_attempt - 3 > 0 THEN
			1
		ELSE
			0
		END
	)
END;


UPDATE rafael.out_order_tracking_sample
SET 
 out_order_tracking_sample.days_left_to_procure = CASE
																										WHEN datediff(date_procured_promised,curdate()) < 0 
																										THEN 0
																										ELSE datediff(date_procured_promised,curdate())	
																									END
WHERE date_procured IS NULL;

/*
UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.deliv_within_3_days_of_order = CASE
WHEN datediff(
	date_delivered,
	date_ordered
) <= 5 THEN
	(
		CASE
		WHEN rafael.calcworkdays (
			date_ordered,
			date_delivered
		) <= 3 THEN
			1
		ELSE
			0
		END
	)
ELSE
	0
END,
 out_order_tracking_sample.first_att_within_3_days_of_order = CASE
WHEN datediff(
	date_1st_attempt,
	date_ordered
) <= 5 THEN
	(
		CASE
		WHEN rafael.calcworkdays (
			date_ordered,
			date_1st_attempt
		) <= 3 THEN
			1
		ELSE
			0
		END
	)
ELSE
	0
END;
*/

UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.shipped_same_day_as_order = CASE
WHEN date_ordered = date_shipped THEN
	1
ELSE
	0
END;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.shipped_same_day_as_procured = CASE
WHEN date_procured = date_shipped THEN
	1
ELSE
	0
END;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.reason_for_delay = "on time 1st attempt";


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.reason_for_delay = "procurement"
WHERE
	out_order_tracking_sample.delay_to_procure = 1
AND out_order_tracking_sample.delay_total_1st_attempt = 1;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.reason_for_delay = "preparing item for shipping"
WHERE
	out_order_tracking_sample.reason_for_delay = "on time 1st attempt"
AND out_order_tracking_sample.delay_to_ready = 1
AND out_order_tracking_sample.delay_total_1st_attempt = 1;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.reason_for_delay = "preparing item for shipping"
WHERE
	out_order_tracking_sample.reason_for_delay = "on time 1st attempt"
AND out_order_tracking_sample.delay_to_ready = 1
AND out_order_tracking_sample.delay_total_1st_attempt = 1;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.reason_for_delay = "carrier late delivering"
WHERE
	out_order_tracking_sample.reason_for_delay = "on time 1st attempt"
AND out_order_tracking_sample.delay_total_1st_attempt = 1;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.early_to_procure = CASE
WHEN (
	CASE
	WHEN date_procured IS NULL THEN
		datediff(
			date_procured_promised,
			curdate()
		)
	ELSE
		datediff(
			date_procured_promised,
			date_procured
		)
	END
) > 0 THEN
	1
ELSE
	0
END,
 out_order_tracking_sample.early_to_1st_attempt = CASE
WHEN (
	CASE
	WHEN date_1st_attempt IS NULL THEN
		datediff(
			date_delivered_promised,
			curdate()
		)
	ELSE
		datediff(
			date_delivered_promised,
			date_1st_attempt
		)
	END
) > 0 THEN
	1
ELSE
	0
END;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.delay_reason_maximum_value = rafael.maximum (
	workdays_delay_to_procure,
	workdays_delay_to_ready,
	workdays_delay_to_ship,
	workdays_delay_to_1st_attempt
);


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.delay_reason = CASE
WHEN delay_total_1st_attempt = 1 THEN
	(
		CASE
		WHEN delay_reason_maximum_value = workdays_delay_to_procure THEN
			"procurement"
		ELSE
			(
				CASE
				WHEN delay_reason_maximum_value = workdays_delay_to_ready THEN
					"preparing item for shipping"
				ELSE
					(
						CASE
						WHEN delay_reason_maximum_value = workdays_delay_to_ship THEN
							"carrier late to pick up item"
						ELSE
							(
								CASE
								WHEN delay_reason_maximum_value = workdays_delay_to_1st_attempt THEN
									"carrier late delivering"
								ELSE
									"on time 1st attempt"
								END
							)
						END
					)
				END
			)
		END
	)
ELSE
	"on time 1st attempt"
END;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.on_time_to_ship = CASE
WHEN date_shipped <= date_ready_to_ship_promised THEN
	1
ELSE
	0
END,
 out_order_tracking_sample.on_time_r2s = CASE
WHEN date_ready_to_ship <= date_ready_to_ship_promised THEN
	1
ELSE
	0
END;


DELETE rafael.pro_order_tracking_num_items_per_order.*
FROM
	rafael.pro_order_tracking_num_items_per_order;


INSERT INTO rafael.pro_order_tracking_num_items_per_order (
	order_number,
	count_of_order_item_id
) SELECT
	out_order_tracking_sample.order_number,
	count(
		out_order_tracking_sample.order_item_id
	) AS countoforder_item_id
FROM
	rafael.out_order_tracking_sample
GROUP BY
	out_order_tracking_sample.order_number;


UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.pro_order_tracking_num_items_per_order ON out_order_tracking_sample.order_number = pro_order_tracking_num_items_per_order.order_number
SET out_order_tracking_sample.num_items_per_order = 1 / pro_order_tracking_num_items_per_order.count_of_order_item_id;

/*
UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.procurement_actual_time = CASE
WHEN date_exported IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN date_procured IS NULL THEN
			rafael.calcworkdays (date_exported, curdate())
		ELSE
			rafael.calcworkdays (
				date_exported,
				date_procured
			)
		END
	)
END;
*/

UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.procurement_delay = CASE
WHEN date_exported >= curdate() THEN
	0
ELSE
	(
		CASE
		WHEN date_sub(
			workdays_to_procure,
			INTERVAL supplier_leadtime DAY
		) > 0 THEN
			date_sub(
				workdays_to_procure,
				INTERVAL supplier_leadtime DAY
			)
		ELSE
			0
		END
	)
END;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.procurement_delay_counter = 1
WHERE
	out_order_tracking_sample.procurement_delay > 0;

/*
UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.warehouse_actual_time = CASE
WHEN date_ready_to_pick IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN date_ready_to_ship IS NULL THEN
			rafael.calcworkdays (
				date_ready_to_pick,
				curdate()
			)
		ELSE
			rafael.calcworkdays (
				date_ready_to_pick,
				date_ready_to_ship
			)
		END
	)
END;
*/

# Otro nuevo Calc Workdays
#Warehouse Delay
UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_ready_to_ship = calcworkdays.date_first
		AND out_order_tracking_sample.date_ready_to_ship_promised = calcworkdays.date_last
SET 
	out_order_tracking_sample.warehouse_delay  =	calcworkdays.workdays,
	out_order_tracking_sample.warehouse_delay_counter = 1
WHERE date_ready_to_ship_promised <= curdate();

UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_ready_to_ship = calcworkdays.date_first
		AND curdate() = calcworkdays.date_last
SET 
	out_order_tracking_sample.warehouse_delay  =	calcworkdays.workdays,
	out_order_tracking_sample.warehouse_delay_counter = 1
WHERE date_ready_to_ship_promised <= curdate()
AND date_ready_to_ship IS NULL;

UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.carrier_time = max_delivery_time - wh_time - supplier_leadtime;

/*
UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.actual_carrier_time = CASE
WHEN date_ready_to_ship IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN date_delivered IS NULL THEN
			rafael.calcworkdays (
				date_ready_to_ship,
				curdate()
			)
		ELSE
			rafael.calcworkdays (
				date_ready_to_ship,
				date_delivered
			)
		END
	)
END;
*/

UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.delivery_delay = CASE
WHEN date_ready_to_ship >= curdate() THEN
	0
ELSE
	(
		CASE
		WHEN workdays_to_deliver > carrier_time THEN
			workdays_to_deliver - carrier_time
		ELSE
			0
		END
	)
END;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.delivery_delay_counter = 1
WHERE
	out_order_tracking_sample.delivery_delay > 0
AND out_order_tracking_sample.date_delivered_promised <= curdate();


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.delay_exceptions = CASE
WHEN out_order_tracking_sample.supplier_leadtime >= out_order_tracking_sample.max_delivery_time THEN
	1
ELSE
	0
END;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.ready_to_ship = 1
WHERE
	out_order_tracking_sample.date_ready_to_ship IS NOT NULL;

/*
UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.wh_workdays_to_r2s = CASE
WHEN date_ready_to_ship IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN date_ready_to_pick IS NULL THEN
			NULL
		ELSE
			rafael.calcworkdays (
				date_ready_to_pick,
				date_ready_to_ship
			)
		END
	)
END;
*/

UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.week_r2s_promised = rafael.week_iso (
	date_ready_to_ship_promised
),
 out_order_tracking_sample.month_num_r2s_promised = date_format(
	date_ready_to_ship_promised,
	"%Y-%m"
),
 out_order_tracking_sample.week_ordered = rafael.week_iso (date_ordered);

# Nuevo Calc WD

UPDATE        rafael.out_order_tracking_sample
   INNER JOIN rafael.calcworkdays
           ON     out_order_tracking_sample.date_ready_to_pick = calcworkdays.date_first
              AND calcworkdays.date_last = curdate() 
SET 
   r2s_workday_0  = IF( workdays_to_ready  = 0 AND workdays >= 0, 1 , r2s_workday_0 ),
   r2s_workday_1  = IF( workdays_to_ready <= 1 AND workdays >= 1, 1 , r2s_workday_1 ),
   r2s_workday_2  = IF( workdays_to_ready <= 2 AND workdays >= 2, 1 , r2s_workday_2 ),
   r2s_workday_3  = IF( workdays_to_ready <= 3 AND workdays >= 3, 1 , r2s_workday_3 ),
   r2s_workday_4  = IF( workdays_to_ready <= 4 AND workdays >= 4, 1 , r2s_workday_4 ),
   r2s_workday_5  = IF( workdays_to_ready <= 5 AND workdays >= 5, 1 , r2s_workday_5 ),
   r2s_workday_6  = IF( workdays_to_ready <= 6 AND workdays >= 6, 1 , r2s_workday_6 ), 
   r2s_workday_7  = IF( workdays_to_ready <= 7 AND workdays >= 7, 1 , r2s_workday_7 ), 
   r2s_workday_8  = IF( workdays_to_ready <= 8 AND workdays >= 8, 1 , r2s_workday_8 ),  
   r2s_workday_9  = IF( workdays_to_ready <= 9 AND workdays >= 9, 1 , r2s_workday_9 ),  
   r2s_workday_10 = IF( workdays_to_ready  >10 AND workdays >=10, 1 , r2s_workday_10)
;

# Acaba Nuevo Calc WD

UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.ready_to_pick = 1
WHERE
	out_order_tracking_sample.date_ready_to_pick IS NOT NULL;

UPDATE 
 rafael.out_order_tracking_sample
INNER JOIN bob_live_mx.catalog_simple
	ON catalog_simple.sku = out_order_tracking_sample.sku_simple
INNER JOIN procurement_live_mx.procurement_order_item 
	ON catalog_simple.id_catalog_simple = procurement_order_item.fk_catalog_simple
INNER JOIN procurement_live_mx.procurement_order
	ON procurement_order_item.fk_procurement_order = procurement_order.id_procurement_order
LEFT JOIN procurement_live_mx.catalog_supplier_attributes 
	ON procurement_order.fk_catalog_supplier = catalog_supplier_attributes.fk_catalog_supplier
SET out_order_tracking_sample.oms_payment_terms = catalog_supplier_attributes.payment_terms,
 out_order_tracking_sample.oms_payment_event = catalog_supplier_attributes.payment_event;

UPDATE rafael.out_order_tracking_sample
	INNER JOIN procurement_live_mx.catalog_supplier_attributes 
		ON catalog_supplier_attributes.fk_catalog_supplier = out_order_tracking_sample.supplier_id
	SET 
 out_order_tracking_sample.Procurement_analist = catalog_supplier_attributes.buyer_name;


UPDATE rafael.out_order_tracking_sample 
INNER JOIN procurement_live_mx.wms_imported_dropship_orders
	ON out_order_tracking_sample.order_item_id = wms_imported_dropship_orders.fk_sales_order_item
INNER JOIN procurement_live_mx.procurement_order_item_dropshipping 
	ON wms_imported_dropship_orders.id_wms_imported_dropship_order = procurement_order_item_dropshipping.fk_wms_imported_dropship_order
INNER JOIN procurement_live_mx.procurement_order_item
	ON procurement_order_item_dropshipping.fk_procurement_order_item = procurement_order_item.id_procurement_order_item
INNER JOIN procurement_live_mx.procurement_order
	ON procurement_order_item.fk_procurement_order = procurement_order.id_procurement_order
SET 
 out_order_tracking_sample.date_po_created = procurement_order.created_at,
 out_order_tracking_sample.date_po_issued = procurement_order.sent_at
WHERE rafael.out_order_tracking_sample.fullfilment_type_real = "dropshipping";

UPDATE rafael.out_order_tracking_sample 
INNER JOIN procurement_live_mx.wms_imported_orders
	ON out_order_tracking_sample.order_item_id = wms_imported_orders.fk_sales_order_item
INNER JOIN procurement_live_mx.procurement_order_item_crossdocking 
	ON wms_imported_orders.id_wms_imported_order = procurement_order_item_crossdocking.fk_wms_imported_order
INNER JOIN procurement_live_mx.procurement_order_item
	ON procurement_order_item_crossdocking.fk_procurement_order_item = procurement_order_item.id_procurement_order_item
INNER JOIN procurement_live_mx.procurement_order
	ON procurement_order_item.fk_procurement_order = procurement_order.id_procurement_order
SET 
 out_order_tracking_sample.date_po_created = procurement_order.created_at,
 out_order_tracking_sample.date_po_issued = procurement_order.sent_at
WHERE rafael.out_order_tracking_sample.fullfilment_type_real LIKE "crossdock%";

UPDATE rafael.out_order_tracking_sample 
INNER JOIN procurement_live_mx.wms_imported_dropship_orders
	ON out_order_tracking_sample.order_item_id = wms_imported_dropship_orders.fk_sales_order_item
INNER JOIN procurement_live_mx.procurement_order_item_dropshipping 
	ON wms_imported_dropship_orders.id_wms_imported_dropship_order = procurement_order_item_dropshipping.fk_wms_imported_dropship_order
INNER JOIN procurement_live_mx.procurement_order_item
	ON procurement_order_item_dropshipping.fk_procurement_order_item = procurement_order_item.id_procurement_order_item
INNER JOIN procurement_live_mx.procurement_order_item_date_history
	ON procurement_order_item.id_procurement_order_item = procurement_order_item_date_history.fk_procurement_order_item
SET 
 out_order_tracking_sample.date_po_confirmed = procurement_order_item_date_history.confirm_date
WHERE rafael.out_order_tracking_sample.fullfilment_type_real = "dropshipping";

UPDATE rafael.out_order_tracking_sample 
INNER JOIN procurement_live_mx.wms_imported_orders
	ON out_order_tracking_sample.order_item_id = wms_imported_orders.fk_sales_order_item
INNER JOIN procurement_live_mx.procurement_order_item_crossdocking 
	ON wms_imported_orders.id_wms_imported_order = procurement_order_item_crossdocking.fk_wms_imported_order
INNER JOIN procurement_live_mx.procurement_order_item
	ON procurement_order_item_crossdocking.fk_procurement_order_item = procurement_order_item.id_procurement_order_item
INNER JOIN procurement_live_mx.procurement_order_item_date_history
	ON procurement_order_item.id_procurement_order_item = procurement_order_item_date_history.fk_procurement_order_item
SET 
 out_order_tracking_sample.date_po_confirmed = procurement_order_item_date_history.confirm_date
WHERE rafael.out_order_tracking_sample.fullfilment_type_real LIKE "crossdock%";


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_payable_promised = date_procured
WHERE
	out_order_tracking_sample.oms_payment_event = "entrega";


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_payable_promised = date_procured
WHERE
	out_order_tracking_sample.oms_payment_event = "pedido";


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.date_payable_promised = date_procured - supplier_leadtime
WHERE
	out_order_tracking_sample.oms_payment_event = "factura";


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.delay_linio_promise = CASE
WHEN is_linio_promise = 1
AND workdays_to_1st_attempt > 2 THEN
	1
ELSE
	0
END;


UPDATE rafael.out_order_tracking_sample
INNER JOIN wmsprod.entrega ON out_order_tracking_sample.wms_tracking_code = entrega.cod_rastreamento
SET out_order_tracking_sample.delivery_fullfilment = entrega.delivery_fulfillment;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.split_order = CASE
WHEN delivery_fullfilment = 'partial' THEN
	1
ELSE
	0
END;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.effective_1st_attempt = CASE
WHEN date_delivered = date_1st_attempt THEN
	1
ELSE
	0
END;


DELETE
FROM
	rafael.pro_value_from_order;


INSERT INTO rafael.pro_value_from_order SELECT
	order_number,
	1 / count(order_item_id) AS value_from_order
FROM
	rafael.out_order_tracking_sample t
GROUP BY
	order_number
ORDER BY
	date_ordered DESC;


UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.pro_value_from_order ON out_order_tracking_sample.order_number = pro_value_from_order.order_number
SET out_order_tracking_sample.value_from_order = pro_value_from_order.value_from_order;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.shipped = CASE
WHEN date_shipped IS NOT NULL THEN
	1
ELSE
	0
END;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.delivered = CASE
WHEN date_delivered IS NOT NULL THEN
	1
ELSE
	0
END;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.first_attempt = 1
WHERE
	date_1st_attempt IS NOT NULL;

UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.exported_last_30 = CASE
WHEN datediff(
	curdate(),
	date_exported
) <= 30
AND datediff(
	curdate(),
	date_exported
) > 0 THEN
	1
ELSE
	0
END;

UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.procured_promised_last_30 = CASE
WHEN datediff(
	curdate(),
	date_procured_promised
) <= 30
AND datediff(
	curdate(),
	date_procured_promised
) > 0 THEN
	1
ELSE
	0
END;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.shipped_last_30 = CASE
WHEN datediff(curdate(), date_shipped) <= 30
AND datediff(curdate(), date_shipped) > 0 THEN
	1
ELSE
	0
END;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.delivered_promised_last_30 = CASE
WHEN datediff(
	curdate(),
	date_delivered_promised
) <= 30
AND datediff(
	curdate(),
	date_delivered_promised
) > 0 THEN
	1
ELSE
	0
END;


UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.pending_first_attempt = CASE
WHEN date_shipped IS NOT NULL
AND date_1st_attempt IS NULL THEN
	1
ELSE
	0
END;


UPDATE rafael.out_order_tracking_sample
SET ship_to_met_area = CASE
WHEN ship_to_zip BETWEEN 01000
AND 16999
OR ship_to_zip BETWEEN 53000
AND 53970
OR ship_to_zip BETWEEN 54000
AND 54198
OR ship_to_zip BETWEEN 54600
AND 54658
OR ship_to_zip BETWEEN 54700
AND 54769
OR ship_to_zip BETWEEN 54900
AND 54959
OR ship_to_zip BETWEEN 54960
AND 54990
OR ship_to_zip BETWEEN 52760
AND 52799
OR ship_to_zip BETWEEN 52900
AND 52998
OR ship_to_zip BETWEEN 55000
AND 55549
OR ship_to_zip BETWEEN 55700
AND 55739
OR ship_to_zip BETWEEN 57000
AND 57950 THEN
	1
ELSE
	0
END;

UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.deliveries_zip_code ON out_order_tracking_sample.ship_to_zip = deliveries_zip_code.d_codigo
SET out_order_tracking_sample.municipality = deliveries_zip_code.d_mnpio;


UPDATE rafael.out_order_tracking_sample
SET 
 price_after_tax = price/(1+(tax_percent/100)),
 cost_after_tax = cost/(1+(tax_percent/100));


UPDATE rafael.out_order_tracking_sample a
INNER JOIN bob_live_mx.catalog_config b ON a.sku_config = b.sku
INNER JOIN bob_live_mx.catalog_attribute_option_global_category c ON b.fk_catalog_attribute_option_global_category = c.id_catalog_attribute_option_global_category
INNER JOIN bob_live_mx.catalog_attribute_option_global_sub_category d ON b.fk_catalog_attribute_option_global_sub_category = d.id_catalog_attribute_option_global_sub_category
INNER JOIN bob_live_mx.catalog_attribute_option_global_sub_sub_category e ON b.fk_catalog_attribute_option_global_sub_sub_category = e.id_catalog_attribute_option_global_sub_sub_category
SET a.category_com_main = c. NAME,
 a.category_com_sub = d. NAME,
 a.category_com_sub_sub = e. NAME;


UPDATE rafael.out_order_tracking_sample
SET ship_to_zip2 = mid(ship_to_zip, 3, 2),
 ship_to_zip3 = RIGHT (ship_to_zip, 3),
 ship_to_zip4 = LEFT (ship_to_zip, 4);


UPDATE rafael.out_order_tracking_sample a
INNER JOIN production.tbl_order_detail b ON a.order_number = b.order_nr
SET a.coupon_code = b.coupon_code;


UPDATE rafael.out_order_tracking_sample
SET is_corporate_sale = 1
WHERE
	coupon_code LIKE 'CDEAL%';


UPDATE rafael.out_order_tracking_sample a
INNER JOIN development_mx.A_E_M1_New_CategoryBP b ON a.category_com_sub = b.cat2
SET a.category_bp = b.CatBP;


UPDATE rafael.out_order_tracking_sample a
INNER JOIN development_mx.A_E_M1_New_CategoryBP b ON a.category_com_main = b.cat1
SET a.category_bp = b.CatBP
WHERE
	a.category_bp IS NULL;


UPDATE rafael.out_order_tracking_sample
SET fullfilment_type_bp = CASE
WHEN fullfilment_type_real = 'crossdock' THEN
	'crossdocking'
WHEN fullfilment_type_real = 'inventory'
AND fullfilment_type_bob = 'consignment' THEN
	'consignment'
WHEN fullfilment_type_real = 'inventory'
AND fullfilment_type_bob <> 'consignment' THEN
	'outright buying'
WHEN fullfilment_type_real = 'dropshipping' THEN
	'other'
END;


UPDATE rafael.out_order_tracking_sample
SET date_time_1st_attempt = date_1st_attempt + INTERVAL 12 HOUR,
 date_time_delivered = date_delivered + INTERVAL 12 HOUR;


UPDATE rafael.out_order_tracking_sample
SET is_late_delivery_candidate = 1
WHERE
	(
		date_delivered_promised <= curdate()
		AND date_shipped IS NULL
	)
OR (
	date_delivered_promised = adddate(curdate(), INTERVAL 1 DAY)
	AND date_procured IS NULL
);

call monitoring("Mexico","M1_Market_Place");

UPDATE
 rafael.out_order_tracking_sample
INNER JOIN development_mx.M1_Market_Place
	ON out_order_tracking_sample.order_item_id = M1_Market_Place.ItemID
SET
 is_market_place = 1
;

# Nuevo Calc Workdays
UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_exported = calcworkdays.date_first
		AND out_order_tracking_sample.date_procurement_order = calcworkdays.date_last
SET 
 out_order_tracking_sample.workdays_to_po =	calcworkdays.workdays,
 out_order_tracking_sample.days_to_po = DATEDIFF(date_procurement_order,date_exported)
WHERE date_procurement_order IS NOT NULL
AND date_exported IS NOT NULL;

UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_exported = calcworkdays.date_first
		AND out_order_tracking_sample.date_procurement_order = calcworkdays.date_last
SET 
 out_order_tracking_sample.workdays_to_po =	calcworkdays.workdays,
 out_order_tracking_sample.days_to_po = datediff(curdate(), date_exported)
WHERE date_procurement_order IS NULL
AND date_exported IS NOT NULL;
# Nuevo Calc Workdays

# Workdays to confirm

UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_po_issued= calcworkdays.date_first
		AND out_order_tracking_sample.date_po_confirmed = calcworkdays.date_last
SET out_order_tracking_sample.workdays_to_confirm =	calcworkdays.workdays;

UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_po_issued = calcworkdays.date_first
		AND curdate() = calcworkdays.date_last
SET out_order_tracking_sample.workdays_to_export =	calcworkdays.workdays
WHERE date_po_confirmed IS NULL;


# Workdays to send po

UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_ordered = calcworkdays.date_first
		AND out_order_tracking_sample.date_po_issued = calcworkdays.date_last
SET out_order_tracking_sample.workdays_to_send_po =	calcworkdays.workdays;

UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_po_issued = calcworkdays.date_first
		AND curdate() = calcworkdays.date_last
SET out_order_tracking_sample.workdays_to_send_po =	calcworkdays.workdays
WHERE date_po_issued IS NULL;

#24hr_shipments_Cust_persp

UPDATE rafael.out_order_tracking_sample
SET 24hr_shipments_Cust_persp = CASE
		WHEN date_ordered is null and date_shipped IS NULL THEN NULL
    WHEN date_shipped is null THEN
			datediff(curdate(), date_ordered)
		ELSE
			datediff(date_shipped, date_ordered)
		END;

#24hr_shipmnets_WH_persp

UPDATE rafael.out_order_tracking_sample
SET 24hr_shipmnets_WH_persp = CASE
		WHEN date_ready_to_pick is null and date_shipped IS NULL THEN NULL
    WHEN date_shipped is null THEN
			datediff(curdate(), date_ready_to_pick)
		ELSE
			datediff(date_shipped, date_ready_to_pick)
		END;

UPDATE rafael.out_order_tracking
SET out_order_tracking.date_ready_to_pick_bono = date_ready_to_pick
WHERE
date_time_ready_to_pick <= TIMESTAMP(date_ready_to_pick)+ INTERVAL 16 HOUR 
;

UPDATE rafael.out_order_tracking
SET out_order_tracking.date_ready_to_pick_bono = date_ready_to_pick + INTERVAL 1 DAY 
WHERE date_time_ready_to_pick > TIMESTAMP(date_ready_to_pick)+ INTERVAL 16 HOUR;

UPDATE rafael.out_order_tracking
SET out_order_tracking.is_bono = 1
Where date_time_ready_to_pick <= TIMESTAMP(date_ready_to_pick)+ INTERVAL 16 HOUR 
AND date_time_ready_to_ship <= TIMESTAMP(date_ready_to_pick)+ INTERVAL 16 HOUR
AND package_measure_new in ('small', 'medium','large');

UPDATE rafael.out_order_tracking
SET out_order_tracking.is_bono = 1
Where date_time_ready_to_pick > TIMESTAMP(date_ready_to_pick)+ INTERVAL 16 HOUR 
AND date_time_ready_to_ship > TIMESTAMP(date_ready_to_pick)+ INTERVAL 16 HOUR
AND date_time_ready_to_ship < TIMESTAMP(date_ready_to_pick+ INTERVAL 1 DAY)+ INTERVAL 16 HOUR
AND package_measure_new in ('small', 'medium','large') ;

UPDATE rafael.out_order_tracking
SET out_order_tracking.is_bono = 1
Where date_time_ready_to_pick <= TIMESTAMP(date_ready_to_pick)+ INTERVAL 16 HOUR 
AND date_time_ready_to_ship <= TIMESTAMP(date_ready_to_pick+ INTERVAL 1 DAY)+ INTERVAL 16 HOUR
AND package_measure_new = 'oversized';

UPDATE rafael.out_order_tracking
SET out_order_tracking.is_bono = 1
Where date_time_ready_to_pick > TIMESTAMP(date_ready_to_pick)+ INTERVAL 16 HOUR 
AND date_time_ready_to_ship > TIMESTAMP(date_ready_to_pick)+ INTERVAL 16 HOUR
AND date_time_ready_to_ship < TIMESTAMP(date_ready_to_pick+ INTERVAL 2 DAY)+ INTERVAL 16 HOUR
AND package_measure_new = 'overzised';

UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.procured_3days = 1 
WHERE out_order_tracking_sample.workdays_to_procure <= 3;
	

UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.procured_5days = 1 
WHERE out_order_tracking_sample.workdays_to_procure <= 5;
	

UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.procured_10days = 1 
WHERE out_order_tracking_sample.workdays_to_procure <= 10;



UPDATE rafael.out_order_tracking_sample
SET out_order_tracking_sample.procured_more10days = 1 
WHERE out_order_tracking_sample.workdays_to_procure > 10;


UPDATE rafael.out_order_tracking_sample
INNER JOIN rafael.calcworkdays
	ON out_order_tracking_sample.date_ordered = calcworkdays.date_first
		AND out_order_tracking_sample.date_stockout_declared  = calcworkdays.date_last
SET out_order_tracking_sample.workdays_to_stockout_declared =	calcworkdays.workdays
WHERE date_stockout_declared is not null ;

UPDATE rafael.out_order_tracking_sample
SET item_counter = 1;

REPLACE rafael.out_order_tracking SELECT * FROM rafael.out_order_tracking_sample;

REPLACE rafael.pro_backlog_history
SELECT
 curdate() as Date_reported,
 SUM( IF(     presale = 0
          AND status_wms IN ('aguardando estoque','analisando quebra')
                                      , delay_to_procure     , 0 ) ) as procurement_backlog,
 SUM( IF(     date_delivered IS NULL
          AND date_delivered_promised < curdate()
          AND status_wms = 'expedido' , delay_total_delivery , 0 ) ) as deliveries_backlog
FROM
 rafael.out_order_tracking_sample
WHERE 
(
         presale = 0
    AND status_wms IN ('aguardando estoque','analisando quebra')
)
OR 
(
        date_delivered IS NULL
    AND date_delivered_promised < curdate()
    AND status_wms = 'expedido'
);


DROP TABLE IF EXISTS production.out_order_tracking;

CREATE TABLE production.out_order_tracking LIKE rafael.out_order_tracking; 

INSERT INTO production.out_order_tracking SELECT * FROM rafael.out_order_tracking; 


INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check) 
SELECT 
  'Mexico', 
  'out_order_tracking',
  'finish',
  NOW(),
  max(date_time_ordered),
  count(*),
  count(item_counter)
FROM
  production.out_order_tracking;


INSERT INTO `production`.`table_monitoring_report` VALUES (NULL, 'Daily Procurement - MX', NOW());

INSERT INTO `production`.`table_monitoring_report` VALUES (NULL, 'Daily Warehouse - MX', NOW());
