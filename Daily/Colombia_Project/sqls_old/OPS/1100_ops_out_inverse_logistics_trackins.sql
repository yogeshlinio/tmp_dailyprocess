
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Mexico', 
  'out_inverse_logistics_tracking',
  'start',
  NOW(),
  max(date_ordered),
  count(*),
  count(item_counter)
FROM
  production.out_inverse_logistics_tracking;

TRUNCATE	rafael.pro_inverse_logistics_WMS;


INSERT INTO rafael.pro_inverse_logistics_WMS (
	id_inverse_logistics_docs,
	order_nr,
	item_id,
	sku_simple,
	shipping_carrier_return,
	dhl_tracking_code,
	reason,
	ilwh_1st_step,
	comments,
	ilwh_position,
	date_cancelled,
	comments_main
) SELECT
	id,
	order_number,
	item_id,
	sku,
	carrier,
	carrier_tracking_code,
	reason_id,
	action_id,
	COMMENT,
	STATUS,
	created_at,
	comments_il
FROM
	wmsprod.inverselogistics_devolucion;


UPDATE rafael.pro_inverse_logistics_WMS a
INNER JOIN (
	SELECT
		b.return_id,
		b. STATUS,
		b.changed_at,
		c.item_id
	FROM
		wmsprod.inverselogistics_status_history b
	JOIN wmsprod.inverselogistics_devolucion c ON b.return_id = c.id
) d ON a.item_id = d.item_id
SET a.date_entrance_ilwh = d.changed_at
WHERE
	d. STATUS = 'retornado';

UPDATE rafael.pro_inverse_logistics_WMS a
INNER JOIN (
	SELECT
		b.return_id,
		b. STATUS,
		b.changed_at,
		c.item_id
	FROM
		wmsprod.inverselogistics_status_history b
	JOIN wmsprod.inverselogistics_devolucion c ON b.return_id = c.id
) d ON a.item_id = d.item_id
SET a.date_return_solicitation = d.changed_at
WHERE
	d. STATUS = 'aguardando_retorno';


INSERT INTO rafael.pro_inverse_logistics_WMS (
	id_inverse_logistics_docs,
	wms_tracking_code,
	reason,
	date_cancelled
) SELECT
	id,
	cod_rastreamento,
	id_status,
	date
FROM
	wmsprod.tms_status_delivery
WHERE
	tms_status_delivery.id_status = 10;


UPDATE rafael.pro_inverse_logistics_WMS a
SET a. STATUS = CASE
WHEN a.reason = 10 THEN
	'Entrega no Exitosa'
ELSE
	'Devolucion'
END;


UPDATE rafael.pro_inverse_logistics_WMS a
SET a.damaged_item = CASE
WHEN a.reason = 7 THEN
	1
ELSE
	NULL
END;


UPDATE rafael.pro_inverse_logistics_WMS a
SET a.damaged_package = CASE
WHEN a.reason = 1 THEN
	1
ELSE
	NULL
END;


UPDATE rafael.pro_inverse_logistics_WMS a
SET a.return_accepted = CASE
WHEN a.ilwh_position = 'retornar_stock'
OR a.ilwh_position = 'enviar_cuarentena' THEN
	1
ELSE
	NULL
END;


UPDATE rafael.pro_inverse_logistics_WMS a
SET a.create_new_order = CASE
WHEN a.ilwh_1st_step = 9
OR a.ilwh_1st_step = 10
AND a.return_accepted = 1 THEN
	1
ELSE
	NULL
END;


UPDATE rafael.pro_inverse_logistics_WMS a
INNER JOIN wmsprod.inverselogistics_devolucion b ON a.item_id = b.item_id
SET a.date_exit_ilwh = CASE
WHEN b. STATUS = 'retornar_stock'
OR b. STATUS = 'enviar_cuarentena' THEN
	modified_at
ELSE
	NULL
END;


UPDATE rafael.pro_inverse_logistics_WMS a
INNER JOIN wmsprod.inverselogistics_devolucion b ON a.item_id = b.item_id
SET a.ilwh_exit_to = CASE
WHEN b. STATUS = 'retornar_stock' THEN
	'A'
WHEN b. STATUS = 'enviar_cuarentena' THEN
	'V'
WHEN b. STATUS = 'reenvio_cliente' THEN
	'C'
ELSE
	NULL
END;


UPDATE rafael.pro_inverse_logistics_WMS a
INNER JOIN (
	SELECT
		b.user_id,
		b.return_id,
		b. STATUS,
		b.changed_at,
		c.item_id
	FROM
		wmsprod.inverselogistics_status_history b
	JOIN wmsprod.inverselogistics_devolucion c ON b.return_id = c.id
) d ON a.item_id = d.item_id
SET a.entered_to_ilwh_by = d.user_id
WHERE
	d. STATUS = 'retornado';


UPDATE rafael.pro_inverse_logistics_WMS a
INNER JOIN wmsprod.tms_status_delivery b ON b.cod_rastreamento = a.wms_tracking_code
SET a.entered_to_ilwh_by = b.id_user
WHERE
	a.reason = 10;


UPDATE rafael.pro_inverse_logistics_WMS a
INNER JOIN wmsprod.ws_log_lista_embarque b ON b.cod_rastreamento = a.wms_tracking_code
SET a.reason = b.observaciones
WHERE
 a.reason = 10;

UPDATE rafael.pro_inverse_logistics_WMS a
SET a.reason = CASE
WHEN a.reason = 1 THEN
 "D-Producto Dañado"
WHEN a.reason = 2 THEN
 "D-Producto Incorrecto"
WHEN a.reason = 3 THEN
 "D-Producto Duplicado"
WHEN a.reason = 4 THEN
 "D-Cliente no satisfecho"
WHEN a.reason = 5 THEN
 "D-Producto Incompleto"
WHEN a.reason = 6 THEN
 "D.Retraso en entrega de pedido"
WHEN a.reason = 7 THEN
 "D-Producto Usado"
WHEN a.reason = 8 THEN
 "D-Otro"
ELSE
a.reason
END;

UPDATE rafael.pro_inverse_logistics_WMS a
SET a.ilwh_2nd_step = a.ilwh_1st_step
WHERE
	a.ilwh_position = 'retornar_stock'
OR a.ilwh_position = 'enviar_cuarentena';


UPDATE rafael.pro_inverse_logistics_WMS a
SET a.ilwh_1st_step = CASE
WHEN a.ilwh_1st_step = 9 THEN
	"Otro producto igual"
WHEN a.ilwh_1st_step = 10 THEN
	"Otro producto diferente"
WHEN a.ilwh_1st_step = 11 THEN
	"Reembolso en crédito Linio"
WHEN a.ilwh_1st_step = 12 THEN
	"Rembolso tarjeta o cuenta"
ELSE
	NULL
END;


UPDATE rafael.pro_inverse_logistics_WMS a
SET a.ilwh_2nd_step = CASE
WHEN a.ilwh_2nd_step = 9 THEN
	"Otro producto igual"
WHEN a.ilwh_2nd_step = 10 THEN
	"Otro producto diferente"
WHEN a.ilwh_2nd_step = 11 THEN
	"Reembolso en crédito Linio"
WHEN a.ilwh_2nd_step = 12 THEN
	"Rembolso tarjeta o cuenta"
ELSE
	NULL
END;


INSERT INTO rafael.pro_inverse_logistics_WMS (
	id_inverse_logistics_docs,
	date_entrance_ilwh,
	order_nr,
	item_id,
	description,
	sku_simple,
	ean,
	wms_tracking_code,
	shipping_carrier,
	shipping_carrier_return,
	entrance_again,
	dhl_tracking_code,
	payment_method,
	STATUS,
	reason,
	client_commercial_ops,
	ilwh_1st_step,
	damaged_item,
	damaged_package,
	comments,
	return_accepted,
	ilwh_position,
	waranty_review_pending,
	create_new_order,
	correct_when_shipped,
	zendesk_ticket,
	call_1,
	call_2,
	call_3,
	ilwh_2nd_step,
	date_cancelled,
	date_exit_ilwh,
	ilwh_exit_to,
	exit_id,
	cancelled_wms,
	cancelled_bob,
	entered_to_ilwh_by,
	comments_main
) SELECT
	id_inverse_logistics_docs,
	date_entrance_ilwh,
	order_nr,
	item_id,
	description,
	sku_simple,
	ean,
	wms_tracking_code,
	shipping_carrier,
	shipping_carrier_return,
	entrance_again,
	dhl_tracking_code,
	payment_method,
	STATUS,
	reason,
	client_commercial_ops,
	ilwh_1st_step,
	damaged_item,
	damaged_package,
	comments,
	return_accepted,
	ilwh_position,
	waranty_review_pending,
	create_new_order,
	correct_when_shipped,
	zendesk_ticket,
	call_1,
	call_2,
	call_3,
	ilwh_2nd_step,
	date_cancelled,
	date_exit_ilwh,
	ilwh_exit_to,
	exit_id,
	cancelled_wms,
	cancelled_bob,
	entered_to_ilwh_by,
	comments_main
FROM
	rafael.pro_inverse_logistics_docs;


UPDATE rafael.pro_inverse_logistics_WMS a
SET a.reason = "D-Producto Dañado"
WHERE a.reason LIKE "D-Producto Da%";



DELETE rafael.out_inverse_logistics_tracking.*
FROM
	rafael.out_inverse_logistics_tracking;


INSERT INTO rafael.out_inverse_logistics_tracking (
	order_item_id,
	order_number,
	sku_config,
	sku_simple,
	sku_name,
	price_after_tax,
	cost_after_tax,
	supplier_id,
	supplier_name,
	buyer,
	head_buyer,
	package_height,
	package_length,
	package_width,
	package_weight,
	date_ordered,
	fullfilment_type_bob,
	fullfilment_type_real,
	shipping_carrier,
	date_shipped,
	date_delivered,
	date_delivered_promised,
	week_delivered_promised,
	month_num_delivered_promised,
	on_time_to_procure,
	presale,
	payment_method,
	ship_to_state,
	ship_to_zip,
	ship_to_zip2,
	ship_to_zip3,
	wms_tracking_code,
	shipping_carrier_tracking_code,
	status_bob,
	status_wms,
	item_counter,
	package_measure,
	shipped_last_30,
	delivered_promised_last_30,
	ship_to_met_area,
	category_com_main,
	category_com_sub,
	category_com_sub_sub,
	category_bp,
	fullfilment_type_bp,
  is_market_place
) SELECT
	order_item_id,
	order_number,
	sku_config,
	sku_simple,
	sku_name,
	price_after_tax,
	cost_after_tax,
	supplier_id,
	supplier_name,
	buyer,
	head_buyer,
	package_height,
	package_length,
	package_width,
	package_weight,
	date_ordered,
	fullfilment_type_bob,
	fullfilment_type_real,
	shipping_carrier,
	date_shipped,
	date_delivered,
	date_delivered_promised,
	week_delivered_promised,
	month_num_delivered_promised,
	on_time_to_procure,
	presale,
	payment_method,
	ship_to_state,
	ship_to_zip,
	ship_to_zip2,
	ship_to_zip3,
	wms_tracking_code,
	shipping_carrier_tracking_code,
	status_bob,
	status_wms,
	item_counter,
	package_measure,
	shipped_last_30,
	delivered_promised_last_30,
	ship_to_met_area,
	category_com_main,
	category_com_sub,
	category_com_sub_sub,
	category_bp,
	fullfilment_type_bp,
  is_market_place
FROM
	rafael.out_order_tracking;


UPDATE rafael.out_inverse_logistics_tracking a
INNER JOIN rafael.pro_inverse_logistics_WMS b ON a.order_item_id = b.item_id
SET a.date_entrance_ilwh = b.date_entrance_ilwh,
 a.id_ilwh = b.id_inverse_logistics_docs,
 a.previous_ilwh_entrance = b.entrance_again,
 a.tracking_number_returns = b.dhl_tracking_code,
 a.reason_delivery_fail_return = b.reason,
 a.damaged_item = b.damaged_item,
 a.damaged_package = b.damaged_package,
 a.comments = b.comments_main,
 a.return_accepted = b.return_accepted,
 a.ilwh_position = b.ilwh_position,
 a.ilwh_cancellation_date = b.date_entrance_ilwh,
 a.ilwh_exit_date = b.date_exit_ilwh,
 a.entered_to_ilwh = b.entered_to_ilwh_by,
 a.payment_method_type = b.payment_method,
 a.date_return_solicitation = b.date_return_solicitation;


UPDATE rafael.out_inverse_logistics_tracking a
JOIN rafael.pro_inverse_logistics_WMS b ON a.wms_tracking_code = b.wms_tracking_code
SET a.ilwh_status = 'Delivery Fail',
 a.reason_delivery_fail_return = b.reason,
 a.id_ilwh = b.id_inverse_logistics_docs,
 a.ilwh_cancellation_date = b.date_cancelled,
 a.date_entrance_ilwh = b.date_cancelled
WHERE
 b. STATUS = 'Entrega no Exitosa';

UPDATE rafael.out_inverse_logistics_tracking a
INNER JOIN rafael.pro_inverse_logistics_WMS b ON a.order_item_id = b.item_id
SET a.ilwh_status = CASE
WHEN b. STATUS = 'Bloqueo Interno' THEN
	'Internal block'
WHEN b. STATUS = 'Entrega no Exitosa' THEN
	'Delivery Fail'
WHEN b. STATUS LIKE 'Devol%' THEN
	'Return'
ELSE
	NULL
END;


UPDATE rafael.out_inverse_logistics_tracking a
INNER JOIN rafael.pro_inverse_logistics_WMS b ON a.order_item_id = b.item_id
SET a.ilwh_1st_step = CASE
WHEN b.ilwh_1st_step = 'Cancelado' THEN
	'Cancelled'
WHEN b.ilwh_1st_step LIKE 'Re-Env%' THEN
	'Re-Delivery'
WHEN b.ilwh_1st_step LIKE 'Recolecci%' THEN
	'Split recolection'
ELSE
	b.ilwh_1st_step
END;


UPDATE rafael.out_inverse_logistics_tracking a
INNER JOIN rafael.pro_inverse_logistics_WMS b ON a.order_item_id = b.item_id
SET a.ilwh_2nd_step = CASE
WHEN b.ilwh_2nd_step LIKE 'Cancelado por L%' THEN
	'Cancelled by Logistics'
WHEN b.ilwh_2nd_step = 'Cancelado por el cliente' THEN
	'Cancelled by Client'
WHEN b.ilwh_2nd_step = 'Cancelado' THEN
	'Cancelled by Logistics'
WHEN b.ilwh_2nd_step LIKE 'Re-Env%' THEN
	'Re-Delivery'
WHEN b.ilwh_2nd_step LIKE 'cliente soli%' THEN
	'Re-Delivery'
ELSE
	b.ilwh_2nd_step
END;


UPDATE rafael.out_inverse_logistics_tracking a
INNER JOIN rafael.pro_inverse_logistics_WMS b ON a.order_item_id = b.item_id
SET a.ilwh_exit_to = CASE
WHEN b.ilwh_exit_to LIKE '%V%' THEN
	'Sale'
WHEN b.ilwh_exit_to LIKE 'A%' THEN
	'Stock'
WHEN b.ilwh_exit_to LIKE 'P%' THEN
	'Supplier'
WHEN b.ilwh_exit_to LIKE 'C%' THEN
	'Client'
ELSE
	NULL
END;


UPDATE rafael.out_inverse_logistics_tracking a
SET a.`RETURN` =
IF (a.ilwh_status = 'Return', 1, 0);


UPDATE rafael.out_inverse_logistics_tracking a
SET a.failed_delivery =
IF (
	a.ilwh_status = 'Delivery Fail',
	1,
	0
);


UPDATE rafael.out_inverse_logistics_tracking a
SET a.internal_block = 1
WHERE
	a.ilwh_status = 'Internal Block';


UPDATE rafael.out_inverse_logistics_tracking a
SET a.entered_to_ilwh = 1
WHERE
	a.id_ilwh IS NOT NULL;


UPDATE rafael.out_inverse_logistics_tracking a
SET a.1st_cancelled =
IF (
	ilwh_1st_step = 'Cancelled',
	1,
	0
),
 a.1st_partial_retrieval =
IF (
	ilwh_1st_step = 'Split Recolection',
	1,
	0
),
 a.1st_re_delivery =
IF (
	ilwh_1st_step = 'Re-Delivery',
	1,
	0
),
 a.1st_non_applicable =
IF (ilwh_1st_step IS NULL, 1, 0);


UPDATE rafael.out_inverse_logistics_tracking a
SET a.2nd_cancelled =
IF (
	ilwh_2nd_step = 'Cancelled',
	1,
	0
),
 a.2nd_cancelled_by_client =
IF (
	ilwh_2nd_step = 'Cancelled by Client',
	1,
	0
),
 a.2nd_cancelled_by_logistics =
IF (
	ilwh_2nd_step = 'Cancellled by Logistics',
	1,
	0
),
 a.2nd_re_delivery =
IF (
	ilwh_2nd_step = 'Re-Delivery',
	1,
	0
);


UPDATE rafael.out_inverse_logistics_tracking a
SET a.exit_to_client_failed_delivery =
IF (ilwh_exit_to = 'Client', 1, 0),
 a.exit_to_stock_failed_delivery =
IF (ilwh_exit_to = 'Stock', 1, 0),
 a.exit_to_supplier_failed_delivery =
IF (
	ilwh_exit_to = 'Supplier',
	1,
	0
),
 a.exit_to_resale_failed_delivery =
IF (ilwh_exit_to = 'Sale', 1, 0)
WHERE
	a.failed_delivery = 1;


UPDATE rafael.out_inverse_logistics_tracking a
SET a.exit_to_client_returns =
IF (ilwh_exit_to = 'Client', 1, 0),
 a.exit_to_stock_returns =
IF (ilwh_exit_to = 'Stock', 1, 0),
 a.exit_to_supplier_returns =
IF (
	ilwh_exit_to = 'Supplier',
	1,
	0
),
 a.exit_to_resale_returns =
IF (ilwh_exit_to = 'Sale', 1, 0)
WHERE
	a. `RETURN` = 1;


UPDATE rafael.out_inverse_logistics_tracking a
SET a.exit_to_client_internal_block =
IF (ilwh_exit_to = 'Client', 1, 0),
 a.exit_to_stock_internal_block =
IF (ilwh_exit_to = 'Stock', 1, 0),
 a.exit_to_supplier_internal_block =
IF (
	ilwh_exit_to = 'Supplier',
	1,
	0
),
 a.exit_to_resale_internal_block =
IF (ilwh_exit_to = 'Sale', 1, 0)
WHERE
	a.internal_block = 1;


UPDATE rafael.out_inverse_logistics_tracking a
SET a.reintegrate_to_stock =
IF (ilwh_position = 'IN' OR ilwh_position = 'retornar_stock', 1, 0);


UPDATE rafael.out_inverse_logistics_tracking a
SET a.succesfull_redelivery =
IF (ilwh_position = 'Client', 1, 0);


UPDATE rafael.out_inverse_logistics_tracking a
SET a.pending_exits =
IF (a.ilwh_exit_date IS NOT NULL, 1, 0);


UPDATE rafael.out_inverse_logistics_tracking
SET out_inverse_logistics_tracking.last_30 = CASE
WHEN datediff(curdate(), date_delivered) <= 45
AND datediff(curdate(), date_delivered) >= 15 THEN
	1
ELSE
	0
END;



UPDATE rafael.out_inverse_logistics_tracking
SET item_counter = 1;

DROP TABLE IF EXISTS production.out_inverse_logistics_tracking;

CREATE TABLE production.out_inverse_logistics_tracking LIKE rafael.out_inverse_logistics_tracking;

INSERT INTO production.out_inverse_logistics_tracking SELECT * FROM rafael.out_inverse_logistics_tracking;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
	updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Mexico', 
  'out_inverse_logistics_tracking',
  'finish',
  NOW(),
  max(date_ordered),
  count(*),
  count(item_counter)
FROM
  production.out_inverse_logistics_tracking;
