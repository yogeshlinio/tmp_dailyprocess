INSERT A_Consolidation_CRM_GA
SELECT 
  A_Consolidation_GA.`country`,
  A_Consolidation_GA.`yrmonth` ,
  A_Consolidation_GA.`week` ,
  A_Consolidation_GA.`date` ,
  A_Consolidation_GA.`source_medium` ,
  A_Consolidation_GA.`campaign` ,
  A_Consolidation_GA.`visits` ,
  A_Consolidation_GA.`carts` ,
  A_Consolidation_GA.`gross_transactions`,
  A_Consolidation_GA.`gross_revenue` ,
  A_Consolidation_GA.`gross_items` ,
  A_Consolidation_GA.`avg_gross_rev` ,
  A_Consolidation_GA.`gross_conversion_rate` ,
  A_Consolidation_GA.`net_transactions` ,
  A_Consolidation_GA.`net_revenue` ,
  A_Consolidation_GA.`net_items` ,
  A_Consolidation_GA.`avg_net_rev` ,
  A_Consolidation_GA.`net_conversion_rate`,
  A_Consolidation_GA.`PC1` ,
  A_Consolidation_GA.`PC1.5`,
  A_Consolidation_GA.`PC2` ,
  A_Consolidation_GA.`%PC1` ,
  A_Consolidation_GA.`%PC1.5` ,
  A_Consolidation_GA.`%PC2` ,

   COALESCE( Sent, 0 )      AS MessageSent, 
   COALESCE( Bounces , 0 )   AS Bounces,  
   COALESCE( `%Bounce`, 0 ) AS `%Bounce`, 
   COALESCE( Openings , 0 ) AS  Openings,    
   COALESCE( `%Open` , 0 )  AS `%Open`,     
   COALESCE( Clicks , 0 )   AS Clicks, 
   COALESCE( `%Click` , 0 ) AS `%Click`

FROM      A_Consolidation_GA
LEFT JOIN A_Evaluation
       ON     Country = "Mexico"
          AND A_Consolidation_GA.campaign = A_Evaluation.Campaign
WHERE     A_Evaluation.Campaign is null
      AND Country = "Mexico"
ORDER BY A_Consolidation_GA.date
;

INSERT IGNORE A_Consolidation_CRM_GA
SELECT 
  "Mexico" AS `country`,
  date_format( A_Evaluation.Date, "%Y%m" ) AS `yrmonth` ,
  A_Evaluation.`week` ,
  A_Evaluation.`date` ,
  NULL AS `source_medium` ,
  A_Evaluation.`campaign` ,
  0 AS `visits` ,
  0 AS `carts` ,
  0 AS `gross_transactions`,
  0 AS `gross_revenue` ,
  0 AS `gross_items` ,
  0 AS `avg_gross_rev` ,
  0 AS `gross_conversion_rate` ,
  0 AS `net_transactions` ,
  0 AS `net_revenue` ,
  0 AS `net_items` ,
  0 AS `avg_net_rev` ,
  0 AS `net_conversion_rate`,
  0 AS `PC1` ,
  0 AS `PC1.5`,
  0 AS `PC2` ,
  0 AS `%PC1` ,
  0 AS `%PC1.5` ,
  0 AS `%PC2` ,

   COALESCE( Sent, 0 )      AS MessageSent, 
   COALESCE( Bounces , 0 )   AS Bounces,  
   COALESCE( `%Bounce`, 0 ) AS `%Bounce`, 
   COALESCE( Openings , 0 ) AS  Openings,    
   COALESCE( `%Open` , 0 )  AS `%Open`,     
   COALESCE( Clicks , 0 )   AS Clicks, 
   COALESCE( `%Click` , 0 ) AS `%Click`

FROM           A_Consolidation_GA
   RIGHT JOIN A_Evaluation
           ON A_Consolidation_GA.campaign = A_Evaluation.Campaign
WHERE
             A_Consolidation_GA.campaign IS NULL
   AND NOT (     A_Consolidation_GA.campaign IS NOT NULL 
             AND A_Consolidation_GA.Country != "Mexico"
           )
;