	
INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Mexico', 
  'out_procurement_tracking',
  'start',
  NOW(),
  max(date_created),
  count(*),
  count(item_counter)
FROM
  production.out_procurement_tracking;




TRUNCATE rafael.out_procurement_tracking ; 


	INSERT INTO rafael.out_procurement_tracking (
		id_procurement_order_item,
		id_procurement_order,
		id_catalog_simple,
		cost_oms,
		is_deleted,
		date_payment_scheduled,
		date_payment_estimation,
		date_payment_promised,
		is_received,
		is_confirmed) 
SELECT
		id_procurement_order_item,
		fk_procurement_order,
		fk_catalog_simple,
		unit_price,
		is_deleted,
		date(schedule_payment_date),
		date(estimated_payment_date),
		date(promised_payment_date),
		sku_received,
		is_confirm
	FROM
		procurement_live_mx.procurement_order_item ; 


UPDATE rafael.out_procurement_tracking a
INNER JOIN procurement_live_mx.procurement_order b ON a.id_procurement_order = b.id_procurement_order
SET 
  a.po_name = concat(	b.venture_code, 
											lpad(b.id_procurement_order, 7, 0),
											b.check_digit),
	a.is_cancelled = b.is_cancelled,
	a.procurement_payment_terms = b.procurement_payment_terms,
	a.procurement_payment_event = b.procurement_payment_event,
	a.procurement_payment_status = b.payment_status,
	a.date_created = date(b.created_at) ; 


UPDATE rafael.out_procurement_tracking a
INNER JOIN bob_live_mx.catalog_simple b ON a.id_catalog_simple = b.id_catalog_simple
INNER JOIN bob_live_mx.catalog_config c ON b.fk_catalog_config = c.id_catalog_config
INNER JOIN bob_live_mx.supplier d ON c.fk_supplier = d.id_supplier
SET 
 a.sku_simple = b.sku,
 a.sku_name = c. NAME,
 a.supplier_id = d.id_supplier,
 a.supplier_name = d. NAME ; 


UPDATE rafael.out_procurement_tracking a
INNER JOIN bob_live_mx.catalog_simple b ON a.id_catalog_simple = b.id_catalog_simple
INNER JOIN bob_live_mx.catalog_config c ON b.fk_catalog_config = c.id_catalog_config
INNER JOIN bob_live_mx.catalog_attribute_option_global_buyer_name d ON c.fk_catalog_attribute_option_global_buyer_name = d.id_catalog_attribute_option_global_buyer_name
INNER JOIN bob_live_mx.catalog_attribute_option_global_head_buyer_name e ON c.fk_catalog_attribute_option_global_head_buyer_name = e.id_catalog_attribute_option_global_head_buyer_name
SET a.buyer = d. NAME,
 a.head_buyer = e. NAME ; 


UPDATE rafael.out_procurement_tracking a
INNER JOIN bob_live_mx.catalog_simple b ON a.id_catalog_simple = b.id_catalog_simple
INNER JOIN bob_live_mx.catalog_config c ON b.fk_catalog_config = c.id_catalog_config
INNER JOIN bob_live_mx.catalog_attribute_option_global_category d ON c.fk_catalog_attribute_option_global_category = d.id_catalog_attribute_option_global_category
INNER JOIN bob_live_mx.catalog_attribute_option_global_sub_category e ON c.fk_catalog_attribute_option_global_sub_category = e.id_catalog_attribute_option_global_sub_category
INNER JOIN bob_live_mx.catalog_attribute_option_global_sub_sub_category f ON c.fk_catalog_attribute_option_global_sub_sub_category = f.id_catalog_attribute_option_global_sub_sub_category
SET 
 a.cat_1 = d. NAME,
 a.cat_2 = e. NAME,
 a.cat_3 = f. NAME ; 


UPDATE rafael.out_procurement_tracking a
INNER JOIN procurement_live_mx.catalog_supplier_attributes b ON a.supplier_id = b.fk_catalog_supplier
SET 
 a.supplier_name = b. NAME,
 a.catalog_payment_type = b.payment_type,
 a.catalog_payment_terms = b.payment_terms,
 a.procurement_analyst = b.buyer_name ; 


UPDATE rafael.out_procurement_tracking a
INNER JOIN procurement_live_mx.procurement_order_item_date_history b ON a.id_procurement_order_item = b.fk_procurement_order_item
SET a.date_goods_received = b.delivery_real_date ; 


UPDATE rafael.out_procurement_tracking a
INNER JOIN procurement_live_mx.wms_received_item b ON a.id_procurement_order_item = b.fk_procurement_order_item
INNER JOIN wmsprod.itens_recebimento c ON b.id_wms = c.itens_recebimento_id
INNER JOIN wmsprod.estoque d ON c.itens_recebimento_id = d.itens_recebimento_id
SET 
 a.stock_item_id = d.estoque_id,
 a.wh_location = d.endereco ; 





UPDATE rafael.out_procurement_tracking a
INNER JOIN wmsprod.itens_venda b ON a.stock_item_id = b.estoque_id
SET a.order_item_id = b.item_id ; 


UPDATE rafael.out_procurement_tracking a
INNER JOIN procurement_live_mx.invoice_item b ON a.id_procurement_order_item = b.fk_procurement_order_item
INNER JOIN procurement_live_mx.invoice c ON b.fk_invoice = c.id_invoice
SET 
 a.date_invoice_issued = date(c.emission_date),
 a.date_invoice_created = date(c.created_at),
 a.date_invoice_received = date(c.invoice_date),
 a.invoice_number = c.invoice_nr ; 


UPDATE rafael.out_procurement_tracking a
LEFT JOIN procurement_live_mx.procurement_order_payment_items b ON a.id_procurement_order_item = b.fk_procurement_order_item
INNER JOIN procurement_live_mx.procurement_order_payment c ON b.fk_procurement_order_payment = c.id_procurement_order_payment
SET a.procurement_payment_type = c.payment_type ; 


UPDATE rafael.out_procurement_tracking a
LEFT JOIN procurement_live_mx.procurement_order_payment_items b ON a.id_procurement_order_item = b.fk_procurement_order_item
INNER JOIN procurement_live_mx.procurement_order_payment c ON b.fk_procurement_order_payment = c.id_procurement_order_payment
SET a.date_paid = date(c.operation_date) ; 


UPDATE rafael.out_procurement_tracking a
INNER JOIN procurement_live_mx.procurement_order_item b ON a.id_procurement_order_item = b.id_procurement_order_item
SET 
 inbound_type = CASE
									WHEN b.transport_type = 2 
									THEN 'FOB'
									ELSE 'CIF'
									END ; 


UPDATE rafael.out_procurement_tracking a
INNER JOIN bob_live_mx.catalog_simple b ON a.id_catalog_simple = b.id_catalog_simple
INNER JOIN bob_live_mx.catalog_shipment_type c ON b.fk_catalog_shipment_type = c.id_catalog_shipment_type
SET a.fullfilment_type_bob = c. NAME ; UPDATE rafael.out_procurement_tracking a
INNER JOIN rafael.out_stock_hist b ON a.stock_item_id = b.stock_item_id
SET 
 a.fullfilment_type_real = b.fullfilment_type_real,
 a.fullfilment_type_bp = b.fullfilment_type_bp ; 


UPDATE rafael.out_procurement_tracking a
INNER JOIN procurement_live_mx.procurement_order_item_date_history b ON a.id_procurement_order_item = b.fk_procurement_order_item
SET 
 a.date_collection_scheduled = date(collect_scheduled_date),
 a.date_collection_negotiated = date(collect_negotiated_date),
 a.date_collection_real = date(collect_real_date),
 a.date_delivery_calculated_bob = date(delivery_bob_calculated_date),
 a.date_delivery_scheduled = date(delivery_scheduled_date),
 a.date_delivery_real = date(delivery_real_date) ; 


UPDATE rafael.out_procurement_tracking a
INNER JOIN wmsprod.estoque b ON a.stock_item_id = b.estoque_id
SET 
 a.date_exit_wh = date(data_ultima_movimentacao),
 a.exit_type = "sold"
WHERE
	a.wh_location = "vendidos" ; 

UPDATE rafael.out_procurement_tracking a
INNER JOIN wmsprod.estoque b ON a.stock_item_id = b.estoque_id
SET a.date_exit_wh = date(data_ultima_movimentacao),
 a.exit_type = "error"
WHERE
	a.wh_location = "error" ; 


UPDATE rafael.out_procurement_tracking a
INNER JOIN bob_live_mx.supplier_address b ON a.supplier_id = b.fk_supplier
SET 
 a.pick_at_zip = b.postcode ; 

UPDATE rafael.out_procurement_tracking a
SET 
 a.pick_at_zip2 = LEFT (pick_at_zip, 2),
 a.pick_at_zip3 = LEFT (pick_at_zip, 3) ; 


UPDATE rafael.out_procurement_tracking
SET 
 week_po_created = rafael.week_iso (date_created),
 week_goods_received = rafael.week_iso (date_goods_received),
 week_payment = rafael.week_iso (date_paid) ; 


UPDATE rafael.out_procurement_tracking
SET 
 month_po_created = date_format(date_created, "%x-%m"),
 month_goods_received = date_format(date_goods_received,"%x-%m"),
 month_payment = date_format(date_paid, "%x-%m") ; 


UPDATE rafael.out_procurement_tracking a
INNER JOIN bob_live_mx.sales_order_item b ON a.order_item_id = b.id_sales_order_item
SET a.cost_bob = b.cost ; 

UPDATE rafael.out_procurement_tracking a
INNER JOIN bob_live_mx.catalog_simple b ON a.id_catalog_simple = b.id_catalog_simple
SET a.cost_bob = b.cost
WHERE
	a.cost_bob = 0 ; 






UPDATE rafael.out_procurement_tracking
SET 
 is_paid = 1,
 amount_paid = cost_oms
WHERE
	date_paid IS NOT NULL ; 


UPDATE rafael.out_procurement_tracking
SET 
 is_invoiced = 1
WHERE
 date_paid IS NOT NULL ; 


UPDATE rafael.out_procurement_tracking
SET 
 payment_terms_real = CASE
												WHEN date_paid IS NULL 
												THEN NULL
												ELSE
											 (CASE
													WHEN date_goods_received IS NULL 
													THEN datediff(date_paid, curdate())
													ELSE datediff(date_paid, date_goods_received)
												END)
											END ; 

UPDATE rafael.out_procurement_tracking
SET payment_terms_scheduled = CASE
																WHEN date_payment_scheduled IS NULL 
																THEN NULL
																ELSE (	CASE
																					WHEN date_goods_received IS NULL 
																					THEN datediff(date_payment_scheduled, curdate())
																					ELSE datediff(date_payment_scheduled,	date_goods_received)
																				END)
																END ; 

UPDATE rafael.out_procurement_tracking
SET 
 payment_terms_expected = CASE
														WHEN date_goods_received IS NULL 
														THEN datediff(date_payment_estimation,curdate())
													ELSE (	CASE
																		WHEN date_payment_promised IS NULL 
																		THEN datediff(date_payment_estimation,date_goods_received)
																	ELSE datediff(date_payment_promised,date_goods_received)
																		END)
													END ; 


UPDATE rafael.out_procurement_tracking a
INNER JOIN procurement_live_mx.procurement_order b ON a.id_procurement_order = b.id_procurement_order
INNER JOIN procurement_live_mx.procurement_order_type c ON b.fk_procurement_order_type = c.id_procurement_order_type
SET a.procurement_order_type = c.procurement_order_type ; 


UPDATE rafael.out_procurement_tracking
SET 
 goods_received_last_15 = 1
WHERE
	datediff(curdate(),date_goods_received) <= 15 ; 

UPDATE rafael.out_procurement_tracking
SET 
 goods_received_last_30 = 1
WHERE
	datediff(curdate(),date_goods_received) <= 30 ; 

UPDATE rafael.out_procurement_tracking
SET 
 goods_received_last_45 = 1
WHERE
	datediff(curdate(),date_goods_received) <= 45 ; 

UPDATE rafael.out_procurement_tracking
SET 
 paid_last_15 = 1
WHERE
 datediff(curdate(), date_paid) <= 15 ; 

UPDATE rafael.out_procurement_tracking
SET 
 paid_last_30 = 1
WHERE
 datediff(curdate(), date_paid) <= 30 ; 

UPDATE rafael.out_procurement_tracking
SET 
 paid_last_45 = 1
WHERE
	datediff(curdate(), date_paid) <= 45 ; 


UPDATE rafael.out_procurement_tracking
SET 
 cost_oms_gt_bob = 1
WHERE
 cost_oms > cost_bob ; 


UPDATE rafael.out_procurement_tracking
SET 
 cost_oms_bob_gap = cost_oms - cost_bob
WHERE
 cost_oms_gt_bob = 1 ; 


UPDATE rafael.out_procurement_tracking a
SET a.date_payment_programmed = CASE
																	WHEN a.is_confirmed = 1 
																	THEN date_payment_scheduled
																	ELSE (	CASE
																						WHEN a.is_received = 1 
																						THEN date_payment_promised
																						ELSE date_payment_estimation
																					END)
																	END
WHERE
	procurement_payment_event = "pedido" ; 

UPDATE rafael.out_procurement_tracking a
SET 
 a.date_payment_programmed = CASE
															WHEN a.is_invoiced = 1 
															THEN (CASE
																			WHEN a.is_confirmed = 1 
																			THEN date_payment_scheduled
																			ELSE date_payment_promised
																		END)
															ELSE (CASE
																			WHEN a.is_received = 1 
																			THEN date_payment_promised
																			ELSE date_payment_estimation
																		END)
															END
WHERE
	procurement_payment_event = "factura" ; 

UPDATE rafael.out_procurement_tracking a
SET 
 a.date_payment_programmed = CASE
															WHEN a.is_received = 1 
															THEN (CASE
																			WHEN a.is_confirmed = 1
																			AND a.is_invoiced = 1 
																			THEN date_payment_scheduled
																			ELSE date_payment_promised
																		END)
															ELSE date_payment_estimation
														 END
WHERE
	procurement_payment_event = "entrega" ; 


UPDATE rafael.out_procurement_tracking
SET payment_terms_programmed = datediff(date_payment_programmed,date_goods_received) ; 

UPDATE rafael.out_procurement_tracking
SET payment_terms = payment_terms_real ; 

UPDATE rafael.out_procurement_tracking
SET payment_terms = payment_terms_programmed
WHERE
	payment_terms IS NULL ; 

UPDATE rafael.out_procurement_tracking
SET payment_terms = payment_terms_scheduled
WHERE
	payment_terms IS NULL ; 

UPDATE rafael.out_procurement_tracking
SET payment_terms = payment_terms_scheduled
WHERE
	payment_terms IS NULL ; 

UPDATE rafael.out_procurement_tracking
SET payment_terms = payment_terms_expected
WHERE
	payment_terms IS NULL ;


UPDATE rafael.out_procurement_tracking
SET item_counter = 1;

DROP TABLE IF EXISTS production.out_procurement_tracking;

CREATE TABLE production.out_procurement_tracking LIKE rafael.out_procurement_tracking;

INSERT INTO production.out_procurement_tracking SELECT * FROM rafael.out_procurement_tracking;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Mexico', 
  'out_procurement_tracking',
  'finish',
  NOW(),
  max(date_created),
  count(*),
  count(item_counter)
FROM
  production.out_procurement_tracking;
