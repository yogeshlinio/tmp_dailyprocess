
	



DELETE rafael.out_catalog_tracking.*
FROM
	rafael.out_catalog_tracking ; 


	INSERT INTO rafael.out_catalog_tracking (
		sku_simple,
		delivery_time_supplier,
		delivery_cost_supplier,
		sku_supplier_simple,
		cost,
		price,
		special_price,
		special_to_date,
		special_from_date,
		barcode_ean,
		eligible_free_shipping
	) SELECT
		sku,
		delivery_time_supplier,
		delivery_cost_supplier,
		sku_supplier_simple,
		cost,
		price,
		special_price,
		special_to_date,
		special_from_date,
		barcode_ean,
		eligible_free_shipping
	FROM
		bob_live_mx.catalog_simple
	WHERE
		STATUS <> 'deleted' ; 


		UPDATE rafael.out_catalog_tracking a
	INNER JOIN bob_live_mx.catalog_simple b ON a.sku_simple = b.sku
	INNER JOIN bob_live_mx.catalog_config c ON b.fk_catalog_config = c.id_catalog_config
	SET a.sku_config = c.sku,
	a.sku_name = c. NAME,
	a.package_height = c.package_height,
	a.package_length = c.package_length,
	a.package_width = c.package_width,
	a.package_weight = c.package_weight,
	a.id_supplier = c.fk_supplier,
	a.sku_supplier_config = c.sku_supplier_config,
	a.model = c.model,
	a.pet_approved = c.pet_approved ; 


UPDATE rafael.out_catalog_tracking a
INNER JOIN bob_live_mx.catalog_simple b ON a.sku_simple = b.sku
INNER JOIN bob_live_mx.catalog_shipment_type c ON b.fk_catalog_shipment_type = c.id_catalog_shipment_type
SET a.shipment_type = c. NAME ; 


UPDATE rafael.out_catalog_tracking a
INNER JOIN bob_live_mx.catalog_config b ON a.sku_config = b.sku
INNER JOIN bob_live_mx.catalog_attribute_option_global_inbound_shipment_type c ON b.fk_catalog_attribute_option_global_inbound_shipment_type = c.id_catalog_attribute_option_global_inbound_shipment_type
SET a.inbound_shipment_type = c. NAME ; 


UPDATE rafael.out_catalog_tracking a
INNER JOIN bob_live_mx.supplier b ON a.id_supplier = b.id_supplier
SET a.supplier = b. NAME ; 


UPDATE rafael.out_catalog_tracking a
INNER JOIN bob_live_mx.catalog_config b ON a.sku_config = b.sku
INNER JOIN bob_live_mx.catalog_brand c ON b.fk_catalog_brand = c.id_catalog_brand
SET a.brand = c. NAME ; 


UPDATE rafael.out_catalog_tracking a
INNER JOIN bob_live_mx.catalog_config b ON a.sku_config = b.sku
INNER JOIN bob_live_mx.catalog_attribute_option_global_buyer_name c ON b.fk_catalog_attribute_option_global_buyer_name = c.id_catalog_attribute_option_global_buyer_name
SET a.buyer = c. NAME ; 


UPDATE rafael.out_catalog_tracking a
INNER JOIN bob_live_mx.catalog_config b ON a.sku_config = b.sku
INNER JOIN bob_live_mx.catalog_attribute_option_global_head_buyer_name c ON b.fk_catalog_attribute_option_global_head_buyer_name = c.id_catalog_attribute_option_global_head_buyer_name
SET a.head_buyer = c. NAME ; 


UPDATE rafael.out_catalog_tracking a
SET a.catalog_warehouse_stock = (
	SELECT
		sum(item_counter)
	FROM
		rafael.out_stock_hist b
	WHERE
		b.in_stock = 1
	AND b.reserved = 0
	AND a.sku_simple = b.sku_simple
	GROUP BY
		b.sku_simple
) ; 


UPDATE rafael.out_catalog_tracking a
INNER JOIN bob_live_mx.catalog_simple b ON a.sku_simple = b.sku
INNER JOIN bob_live_mx.catalog_supplier_stock c ON b.id_catalog_simple = c.fk_catalog_simple
SET a.catalog_supplier_stock = c.quantity ; 


UPDATE rafael.out_catalog_tracking a
INNER JOIN bob_live_mx.catalog_simple b ON a.sku_simple = b.sku
SET a.is_option_marketplace = b.is_option_marketplace,
 a.percent_fee_mp = b.percent_fee_mp ;


UPDATE rafael.out_catalog_tracking
SET item_counter = 1;

DROP TABLE IF EXISTS production.out_catalog_tracking;

CREATE TABLE production.out_catalog_tracking LIKE rafael.out_catalog_tracking;

INSERT INTO production.out_catalog_tracking SELECT * FROM rafael.out_catalog_tracking;
