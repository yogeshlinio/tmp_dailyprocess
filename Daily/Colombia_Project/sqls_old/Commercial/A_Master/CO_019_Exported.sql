DROP TEMPORARY TABLE IF EXISTS TMP_Exported;
CREATE TEMPORARY TABLE TMP_Exported (  index( ItemID  ) )
SELECT
   fk_sales_order_item AS ItemId,
   max( created_at ) AS DateExported
FROM  bob_live_co.sales_order_item_status_history
WHERE
   fk_sales_order_item IN ( SELECT ItemId FROM OSRI WHERE Status = "Exported" )
GROUP BY  
fk_sales_order_item;

UPDATE        A_Master_Sample  
   INNER JOIN TMP_Exported 
        USING ( ItemId )
SET
   A_Master_Sample.Exported     = 1,
   A_Master_Sample.DateExported = TMP_Exported.DateExported
;