/*
*  MX_012_001 Interest
*/
DROP TEMPORARY TABLE IF EXISTS A_FactorInterest;
CREATE TEMPORARY TABLE A_FactorInterest (
    OrderNum int,
    id_sales_order int,
    Total_Interests DECIMAL(15,5),
    Order_PriceAfterTax decimal(15,5),
    Order_Items int,

    OrderNet_PriceAfterTax decimal(15,5),
    OrderNet_Items int,

    PRIMARY KEY ( OrderNum ),
    INDEX ( id_sales_order )
);

INSERT A_FactorInterest
SELECT
  OrderNum,
  IdSalesOrder AS id_sales_order,
  0 AS Total_Interests,
  SUM( PriceAfterTax ) as Order_PriceAfterTax,
  COUNT(*) AS Order_Items,
  SUM( IF( OrderAfterCan = 1 , PriceAfterTax , 0 ) ) as OrderNet_Price_after_tax,
  SUM( IF( OrderAfterCan = 1 , 1, 0 ) ) AS OrderNet_Items
FROM
  A_Master_Sample
GROUP BY OrderNum;

UPDATE            A_FactorInterest
       INNER JOIN bob_live_co.sales_order 
               ON  A_FactorInterest.id_sales_order = bob_live_co.sales_order.id_sales_order
SET
     A_FactorInterest.Total_Interests = bob_live_co.sales_order.total_interests;

UPDATE            A_Master_Sample
       INNER JOIN A_FactorInterest USING ( OrderNum )
SET
   A_Master_Sample.Interest   = ( A_FactorInterest.Total_Interests *
                                                     (A_Master_Sample.PriceAfterTax /
                                                      A_FactorInterest.Order_PriceAfterTax) 
                                                   ) /
                                                   ( 1 + taxPercent / 100 )  ,
   A_Master_Sample.NetInterest = IF( OrderAfterCan = 1,
                                                        ( A_FactorInterest.Total_Interests *
                                                          (A_Master_Sample.PriceAfterTax /
                                                           A_FactorInterest.OrderNet_PriceAfterTax)
                                                        )/
                                                        ( 1 + taxPercent / 100 )  ,
                                                        0
                                                      )
;

UPDATE            A_Master_Sample
SET
   A_Master_Sample.MCI            = IF( Interest        > 0 , 1, 0) ,
   A_Master_Sample.NetMCI         = IF( NetInterest     > 0 , 1, 0 ),
   A_Master_Sample.MSI            = IF(    Interest = 0
                                                      AND Installment > 0, 1 , 0),
   A_Master_Sample.InstallmentFee = IF( Interest  > 0 , Interest , InstallmentFee )   
;
