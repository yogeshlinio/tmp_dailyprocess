
DROP   TEMPORARY TABLE IF EXISTS A_Returns;
CREATE TEMPORARY TABLE A_Returns ( INDEX ( fk_sales_order_item  ) )
SELECT
 fk_sales_order_item
FROM
    bob_live_co.sales_order_item_status_history
WHERE
   bob_live_co.sales_order_item_status_history.fk_sales_order_item_status IN ( 8 )
GROUP BY fk_sales_order_item
;

REPLACE A_Returns
SELECT 
   c.item_id as fk_sales_order_item
FROM          wmsprod_co.inverselogistics_status_history b 
   INNER JOIN wmsprod_co.inverselogistics_devolucion c 
           ON b.return_id = c.id
WHERE
   b.status = 'retornado';


UPDATE           A_Returns
      INNER JOIN A_Master_Sample
              ON A_Returns.fk_sales_order_item = A_Master_Sample.ItemID 
SET
    A_Master_Sample.Returns = 1
;

#Query: A 102 U OrderBefore-AfterCan
UPDATE A_Master_Sample
SET 
     A_Master_Sample.Cancellations  = IF(    A_Master_Sample.Cancellations = 1 
                                                         AND A_Master_Sample.Returns       = 0 , 1 , 0 )
;

/*
* DATE           2013/12/05
* DEVELOPED BY   Carlos Antonio Mondragón Soria
* DESCRIPTION MKT - Status Wrong in Returns 
*/
UPDATE A_Master_Sample
SET
   OrderAfterCan = 0,
   Cancellations = 0,
   Pending       = 0
WHERE
   Returns = 1;





