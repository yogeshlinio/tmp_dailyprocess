DROP   TEMPORARY TABLE IF EXISTS A_S_BI_Delivered;
CREATE TEMPORARY TABLE A_S_BI_Delivered ( INDEX ( fk_sales_order_item  ) )
SELECT
 ItemId AS fk_sales_order_item,
 DateDelivered as created_at
FROM
   A_Master_Sample
WHERE
    A_Master_Sample.DateDelivered <> "0000-00-00" 
AND A_Master_Sample.DateDelivered is not null
GROUP BY ItemId 
;
UPDATE           A_S_BI_Delivered
      INNER JOIN A_Master_Sample
              ON A_S_BI_Delivered.fk_sales_order_item = A_Master_Sample.ItemID 
SET
    A_Master_Sample.DateCollected = A_S_BI_Delivered.created_at,
    A_Master_Sample.Collected = 1
;
DROP   TEMPORARY TABLE IF EXISTS A_S_BI_Delivered;
CREATE TEMPORARY TABLE A_S_BI_Delivered ( INDEX ( fk_sales_order_item  ) )
SELECT
 fk_sales_order_item,
 created_at
FROM
    bob_live_co.sales_order_item_status_history
WHERE
   bob_live_co.sales_order_item_status_history.fk_sales_order_item_status IN ( 52 )
GROUP BY fk_sales_order_item
;

UPDATE           A_S_BI_Delivered
      INNER JOIN A_Master_Sample
              ON A_S_BI_Delivered.fk_sales_order_item = A_Master_Sample.ItemID 
SET
    A_Master_Sample.DateCollected = A_S_BI_Delivered.created_at,
    A_Master_Sample.Collected = 1
WHERE
    PaymentMethod in ( "CreditCardOnDelivery_Payment" ,
                       "CashOnDelivery_Payment"  )
;

DROP   TEMPORARY TABLE IF EXISTS A_S_BI_Exportable;
CREATE TEMPORARY TABLE A_S_BI_Exportable ( INDEX ( fk_sales_order_item  ) )
SELECT
  fk_sales_order_item,
  created_at
FROM
    bob_live_co.sales_order_item_status_history
WHERE
   bob_live_co.sales_order_item_status_history.fk_sales_order_item_status IN ( 3 )
GROUP BY fk_sales_order_item
;


UPDATE           A_S_BI_Exportable
      INNER JOIN A_Master_Sample
              ON A_S_BI_Exportable.fk_sales_order_item = A_Master_Sample.ItemID 
SET

    A_Master_Sample.DateCollected = A_S_BI_Exportable.created_at,
    A_Master_Sample.Collected = 1
WHERE
    PaymentMethod in ( "Amex_Gateway" ,
                        "Banorte_Payworks",
                        "Banorte_Payworks_Debit",
                        "Paypal_Express_Checkout",
                        "Oxxo_Direct",
                        "Banorte_PagoReferenciado",
                        "Bancomer_PagoReferenciado",
                        /*Old Collected*/
                        'Adquira_Interredes', 
                        'Amex_Gateway', 
                        'Bancomer_PagoReferenciado', 
                        'Banorte_PagoReferenciado', 
                        'Banorte_Payworks', 
                        'Banorte_Payworks_Debit', 
                        'DineroMail_Api', 
                        'DineroMail_HostedPaymentPage', 
                        'Oxxo_Direct', 
                        'Paypal_Express_Checkout'
                       )
;

DROP   TEMPORARY TABLE IF EXISTS A_S_BI_Manual_Fraud_Check_Pending;
CREATE TEMPORARY TABLE A_S_BI_Manual_Fraud_Check_Pending ( INDEX ( fk_sales_order_item  ) )
SELECT
 *
FROM
    bob_live_co.sales_order_item_status_history
WHERE
   bob_live_co.sales_order_item_status_history.fk_sales_order_item_status IN ( 90  )
GROUP BY fk_sales_order_item
;

UPDATE           A_S_BI_Manual_Fraud_Check_Pending
      INNER JOIN A_Master_Sample
              ON A_S_BI_Manual_Fraud_Check_Pending.fk_sales_order_item = A_Master_Sample.ItemID 
SET
    A_Master_Sample.DateCollected = A_S_BI_Manual_Fraud_Check_Pending.created_at,
    A_Master_Sample.Collected = 1
WHERE
    PaymentMethod in (   "Amex_Gateway" 
                        , "Banorte_Payworks"
#                       , "Banorte_Payworks_Debit"
                        'Amex_Gateway', 
                        'Banorte_Payworks', 
                        'Banorte_Payworks_Debit', 
                        'DineroMail_Api', 
                        'DineroMail_HostedPaymentPage'
                       )
;


UPDATE           A_S_BI_Manual_Fraud_Check_Pending
      INNER JOIN A_Master_Sample
              ON A_S_BI_Manual_Fraud_Check_Pending.fk_sales_order_item = A_Master_Sample.ItemID 
SET
    A_Master_Sample.DateCollected = A_S_BI_Manual_Fraud_Check_Pending.created_at,
    A_Master_Sample.Collected = 1
WHERE
    PaymentMethod in (   "Amex_Gateway" ,
                         "Banorte_Payworks"
#                       , "Banorte_Payworks_Debit"
                         'Amex_Gateway', 
                         'Banorte_Payworks', 
                         'Banorte_Payworks_Debit', 
                         'DineroMail_Api', 
                         'DineroMail_HostedPaymentPage'
                         'DineroMail_HostedPaymentPage'
                       )
;



DROP   TEMPORARY TABLE IF EXISTS A_S_BI_Auto_Fraud_Check_Pending;
CREATE TEMPORARY TABLE A_S_BI_Auto_Fraud_Check_Pending ( INDEX ( fk_sales_order_item  ) )
SELECT
 *
FROM
    bob_live_co.sales_order_item_status_history
WHERE
   bob_live_co.sales_order_item_status_history.fk_sales_order_item_status IN ( 88 )
GROUP BY fk_sales_order_item
;

UPDATE           A_S_BI_Auto_Fraud_Check_Pending
      INNER JOIN A_Master_Sample
              ON A_S_BI_Auto_Fraud_Check_Pending.fk_sales_order_item = A_Master_Sample.ItemID 
SET
    A_Master_Sample.DateCollected = A_S_BI_Auto_Fraud_Check_Pending.created_at,
    A_Master_Sample.Collected = 1
WHERE
    PaymentMethod in (   "Amex_Gateway" 
                        , "Banorte_Payworks"
#                       , "Banorte_Payworks_Debit"
                        'Amex_Gateway', 
                        'Banorte_Payworks', 
                        'Banorte_Payworks_Debit', 
                        'DineroMail_Api', 
                        'DineroMail_HostedPaymentPage'
                       )
;



DROP   TEMPORARY TABLE IF EXISTS A_S_BI_Fraud_Check_Pending;
CREATE TEMPORARY TABLE A_S_BI_Fraud_Check_Pending ( INDEX ( fk_sales_order_item  ) )
SELECT
 *
FROM
    bob_live_co.sales_order_item_status_history
WHERE
   bob_live_co.sales_order_item_status_history.fk_sales_order_item_status IN ( 55 )
GROUP BY fk_sales_order_item
;

UPDATE           A_S_BI_Fraud_Check_Pending
      INNER JOIN A_Master_Sample
              ON A_S_BI_Fraud_Check_Pending.fk_sales_order_item = A_Master_Sample.ItemID 
SET
    A_Master_Sample.DateCollected = A_S_BI_Fraud_Check_Pending.created_at,
    A_Master_Sample.Collected = 1
WHERE
    PaymentMethod in (   "Amex_Gateway" 
                        , "Banorte_Payworks"
#                       , "Banorte_Payworks_Debit"
                        'Amex_Gateway', 
                        'Banorte_Payworks', 
                        'Banorte_Payworks_Debit', 
                        'DineroMail_Api', 
                        'DineroMail_HostedPaymentPage'
                       )
;

/*ADJUST FOR ITEMS NOT COLLECTED*/
UPDATE A_Master_Sample
SET
    A_Master_Sample.InstallmentFee = 0
WHERE
   Collected = 0
;


