DROP TABLE IF EXISTS TMP_Coupons;
CREATE TABLE TMP_Coupons ( INDEX( CouponCode ) )
SELECT
   bob_live_co.sales_rule.code AS CouponCode,
   bob_live_co.sales_rule_set.code_prefix	AS PrefixCode
FROM
        	bob_live_co.sales_rule 
LEFT JOIN bob_live_co.sales_rule_set 
       ON bob_live_co.sales_rule.fk_sales_rule_set = bob_live_co.sales_rule_set.id_sales_rule_set 
;

/* 
*   UPDATE FIELDS ON OSRI
*/
UPDATE            A_Master_Sample
       INNER JOIN TMP_Coupons
	          USING ( CouponCode )
SET
    A_Master_Sample.PrefixCode = TMP_Coupons.PrefixCode
;
