/*WE NEED A_E_IN_COM_FEES_PER_PM*/
/*
*  004 Fees
update bazayaco.tbl_order_detail set payment_cost=greatest(0.029*(orderTotal+orderShippingFee)+400,2500)/CAST(nr_items AS DECIMAL)
 where  payment_method ='Pagosonline_Creditcard' and (OAC = 1 and RETURNED = 0 and rejected  = 0);
update bazayaco.tbl_order_detail set payment_cost=greatest(0.019*(orderTotal+orderShippingFee)+400,2500)/CAST(nr_items AS DECIMAL)
 where  payment_method ='Pagosonline_Pse' and (OAC = 1 and RETURNED = 0 and rejected  = 0);
update bazayaco.tbl_order_detail set payment_cost=(orderTotal+orderShippingFee)*0.01/CAST(nr_items AS DECIMAL)
 where ciudad like '%bogota%' and  payment_method ='CashOnDelivery_Payment' and (OAC = 1 and RETURNED = 0 and rejected  = 0);
update bazayaco.tbl_order_detail set payment_cost=(orderTotal+orderShippingFee)*0.015/CAST(nr_items AS DECIMAL) 
where ciudad not like '%bogota%' and  payment_method ='CashOnDelivery_Payment' and (OAC = 1 and RETURNED = 0 and rejected  = 0);
update bazayaco.tbl_order_detail set payment_cost=2000/CAST(nr_items AS DECIMAL) 
where payment_method ='Consignacion_Payment' and (OAC = 1 and RETURNED = 0 and rejected  = 0); 
update bazayaco.tbl_order_detail set payment_cost=(0.01664*(orderTotal+orderShippingFee))/CAST(nr_items AS DECIMAL)
 where  payment_method ='Efecty_Payment' and (OAC = 1 and RETURNED = 0 and rejected  = 0);

SELECT @ultima_tasa:=trm
FROM bazayaco.tbl_paypal_trm_diaria
WHERE fecha=(SELECT MAX(fecha)
			FROM bazayaco.tbl_paypal_trm_diaria);

UPDATE bazayaco.tbl_order_detail t
LEFT JOIN bazayaco.tbl_paypal_trm_diaria d
ON t.date=d.fecha
set t.payment_cost=CASE WHEN d.fecha IS NULL THEN 0.038*(t.orderTotal+t.orderShippingFee)+(0.3*@ultima_tasa)/CAST(t.nr_items AS DECIMAL)
            ELSE 0.038*(t.orderTotal+t.orderShippingFee)+(0.3*d.trm)/CAST(t.nr_items AS DECIMAL) 
       END
where t.payment_method ='Paypal_Express_Checkout' and (t.OAC = 1 and t.RETURNED = 0 and t.rejected  = 0);

update bazayaco.tbl_order_detail t
JOIN bazayaco.tbl_paypal_trm_diaria d
ON t.date=d.fecha
set t.payment_cost=0.038*(t.orderTotal+t.orderShippingFee)+(0.3*d.trm)/CAST(t.nr_items AS DECIMAL)
 where t.payment_method ='Paypal_Express_Checkout' and (t.OAC = 1 and t.RETURNED = 0 and t.rejected  = 0);


*/