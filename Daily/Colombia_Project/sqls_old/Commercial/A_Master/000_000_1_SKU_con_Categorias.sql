 #Query: A 201 A SKU con Categorias
/*
INSERT INTO A_E_Pro_SKU_Con_Categorias ( sku, catalog_config_name, catalog_category_name, lft, rgt )
SELECT 
     bob_live_co.catalog_config.sku, 
     bob_live_co.catalog_config.name, 
     bob_live_co.catalog_category.name, 
     bob_live_co.catalog_category.lft, 
     bob_live_co.catalog_category.rgt
FROM (            bob_live_co.catalog_config 
       INNER JOIN bob_live_co.catalog_config_has_catalog_category 
               ON bob_live_co.catalog_config.id_catalog_config = bob_live_co.catalog_config_has_catalog_category.fk_catalog_config) 
       INNER JOIN bob_live_co.catalog_category 
               ON bob_live_co.catalog_config_has_catalog_category.fk_catalog_category = bob_live_co.catalog_category.id_catalog_category;

DELETE FROM A_E_Pro_SKU_Con_Categorias WHERE  lft = 1;
UPDATE A_E_Pro_SKU_Con_Categorias SET A_E_Pro_SKU_Con_Categorias.dif = rgt-lft;
*/