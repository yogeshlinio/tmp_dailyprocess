 #Query: M1_ShippingCost_Junio
UPDATE A_Master_Sample SET 
A_Master_Sample.ShippingCost = 104
WHERE A_Master_Sample.Itemid=267502;

/*
* MX_099_004 Ordenes coorporativas 
* Should be IMPLEMENT on A_E_M1_Ordenes_CorporativA

 #Query: M1_CostoCorporativo
UPDATE            A_Master_Sample  
       INNER JOIN A_E_M1_Ordenes_Corporativas 
               ON A_Master_Sample.OrderNum = A_E_M1_Ordenes_Corporativas.Order 
SET 
   A_Master_Sample.ShippingFee  = IF( useShippingFee  = 1, A_E_M1_Ordenes_Corporativas.ShippingFee,  A_Master_Sample.ShippingFee  ) , 
   A_Master_Sample.ShippingCost = IF( useShippingCost = 1, A_E_M1_Ordenes_Corporativas.ShippingCost, A_Master_Sample.ShippingCost ) , 
   A_Master_Sample.PaymentFees  = IF( usePaymentFee   = 1, A_E_M1_Ordenes_Corporativas.PaymentFee  , A_Master_Sample.PaymentFees  ) 
;
*/
/*
* MX_099 Payment_Fee / Transaction_Fee
*/
UPDATE A_Master_Sample
SET
    PaymentFees = TransactionFeeAfterTax
WHERE Date >= "2013-07-01"
;