/*
 #Query: A 216 S Category Tree No Duplicates
 #ATENTION!!!!!!
 #THIS QUERY MUST BE CHANGED ONCE WE VALIDATE THAT IS THE
 #ONLY ERROR SHOWED
DROP VIEW IF EXISTS A_214_Category_Tree_No_Duplicates_1;
CREATE VIEW A_214_Category_Tree_No_Duplicates_1 AS
SELECT 
        A_E_Pro_CategoryTree.sku, 
	A_E_Pro_CategoryTree.catalog_config_name AS FirstOfcatalog_config_name, 
	A_E_Pro_CategoryTree.Cat1 AS FirstOfCat1, 
	(A_E_Pro_CategoryTree.Cat2) AS FirstOfCat2, 
	(A_E_Pro_CategoryTree.Cat3) AS FirstOfCat3, 

CAST( FLOOR( IF(  IsNull( bob_live_co.catalog_config.product_weight ),
	           OPS_Average_Weight_per_Category.Average_Weight_per_Category,
#		       IF( ! Val( bob_live_co.catalog_config.product_weight ) ,
#	               OPS_Average_Weight_per_Category.Average_Weight_per_Category,
#		           Val( bob_live_co.catalog_config.product_weight ))) ) +1 AS SIGNED  ) AS Product_Weight, 		   
                   Val( bob_live_co.catalog_config.product_weight ) ) ) +1 AS SIGNED  ) AS Product_Weight ,
	
CAST( FLOOR(  IF(  IsNull( bob_live_co.catalog_config.package_weight ) ,
	           OPS_Average_Weight_per_Category.Average_Weight_per_Category ,
#		       IF( ! Val( bob_live_co.catalog_config.package_weight ) ,
#	               OPS_Average_Weight_per_Category.Average_Weight_per_Category,
#		           Val( bob_live_co.catalog_config.package_weight ))))  +1 AS SIGNED  ) AS Package_Weight
		           Val( bob_live_co.catalog_config.package_weight )))  +1 AS SIGNED  ) AS Package_Weight
FROM 
    (           A_E_Pro_CategoryTree 
	  LEFT JOIN OPS_Average_Weight_per_Category   ON A_E_Pro_CategoryTree.Cat1 = OPS_Average_Weight_per_Category.Categoria) 
	  LEFT JOIN bob_live_co.catalog_config ON A_E_Pro_CategoryTree.sku = bob_live_co.catalog_config.sku
GROUP BY A_E_Pro_CategoryTree.sku, 
CAST( FLOOR(  IF(  IsNull( bob_live_co.catalog_config.product_weight ),
	                    OPS_Average_Weight_per_Category.Average_Weight_per_Category,
#		                  IF( ! Val( bob_live_co.catalog_config.product_weight ) ,
#	                        OPS_Average_Weight_per_Category.Average_Weight_per_Category,
#		                      Val( bob_live_co.catalog_config.product_weight ))))  +1 AS SIGNED ) ,
		                      Val( bob_live_co.catalog_config.product_weight )))  +1 AS SIGNED ) ,
CAST( FLOOR( IF(  IsNull( bob_live_co.catalog_config.package_weight ) ,
	           OPS_Average_Weight_per_Category.Average_Weight_per_Category ,
#		       IF( ! Val( bob_live_co.catalog_config.package_weight ) ,
#	               OPS_Average_Weight_per_Category.Average_Weight_per_Category,
#		           Val( bob_live_co.catalog_config.package_weight ))) ) +1 AS SIGNED ) 
		           Val( bob_live_co.catalog_config.package_weight )))  +1 AS SIGNED ) 
;
DELETE FROM A_E_Pro_CategoryTree_No_Duplicates; 
REPLACE INTO A_E_Pro_CategoryTree_No_Duplicates
SELECT *
FROM A_214_Category_Tree_No_Duplicates_1;

*/