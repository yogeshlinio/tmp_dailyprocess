/*WE NEED TO FILL THE BOB_TABLE_STATUS_DEF
#Query: A 102 U OrderBefore-AfterCan
UPDATE            A_Master_Sample 
       INNER JOIN M_Bob_Order_Status_Definition 
              ON     ( A_Master_Sample.PaymentMethod = M_Bob_Order_Status_Definition.Payment_Method) 
                 AND ( A_Master_Sample.Status        = M_Bob_Order_Status_Definition.Status) 
SET 
     A_Master_Sample.OrderBeforeCan = M_Bob_Order_Status_Definition.OrderBeforeCan, 
     A_Master_Sample.OrderAfterCan  = M_Bob_Order_Status_Definition.OrderAfterCan, 
     A_Master_Sample.Cancellations  = M_Bob_Order_Status_Definition.Cancellations, 
     A_Master_Sample.Pending        = M_Bob_Order_Status_Definition.Pending
;
*/