/*
* MX_006 FL_Cost
*/
#Query: A 129 U WH/CS cost per Item
DROP TEMPORARY TABLE IF EXISTS WH_CS_Per_day;
CREATE TEMPORARY TABLE WH_CS_Per_day ( PRIMARY KEY ( Month_Num ) )
SELECT
    Month_Num,
    dayofmonth(  last_day( concat( Month_Num , "01" ) ) ) as TotalDays,
    Value  as FL_WH_cost_per_day_NC,
    0      as FL_CS_cost_per_day_NC
FROM
    (   SELECT 	MonthNum AS Month_Num, Value FROM
	   (
		   SELECT * FROM development_mx.M_Costs WHERE Country = 'COL' AND TypeCost = 'FL_WH_cost' 		
		   ORDER BY updatedAt DESC
	    ) a GROUP BY MonthNum   
	) as Cost
WHERE
   Month_Num >= 201309
GROUP BY Month_Num;


UPDATE WH_CS_Per_day 
   INNER JOIN 
      (   SELECT 	MonthNum AS Month_Num, Value FROM
	      (
		      SELECT * FROM development_mx.M_Costs WHERE Country = 'COL' AND TypeCost = 'FL_CS_cost' 		
		      ORDER BY updatedAt DESC
	       ) a GROUP BY MonthNum   
	   ) as Cost
	USING ( Month_Num     )
SET
    FL_CS_cost_per_day_NC = Value  
WHERE
   Month_Num >= 201309
;

UPDATE            WH_CS_Per_day 
       INNER JOIN ( 
                   SELECT MonthNum AS Month_Num,
                           dayofmonth( max( Date  ) ) Days,
                           count( distinct OrderNum ) AS NetOrder
                    FROM
                       A_Master_Sample
                    WHERE
                       OrderAfterCan = 1
                    GROUP BY MonthNum
                  ) AS OrdersPerMonth
            USING ( Month_Num )
SET
     WH_CS_Per_day.FL_WH_cost_per_day_NC = ( WH_CS_Per_day.FL_WH_cost_per_day_NC * OrdersPerMonth.Days ) /( OrdersPerMonth.NetOrder * WH_CS_Per_day.TotalDays ),
     WH_CS_Per_day.FL_CS_cost_per_day_NC = ( WH_CS_Per_day.FL_CS_cost_per_day_NC * OrdersPerMonth.Days ) /( OrdersPerMonth.NetOrder *  WH_CS_Per_day.TotalDays )
;

REPLACE OPS_WH_CS_per_Order_for_PC2
SELECT
   Month_Num,
   WH_CS_Per_day.FL_WH_cost_per_day_NC,
   WH_CS_Per_day.FL_CS_cost_per_day_NC
FROM
   WH_CS_Per_day
;

UPDATE            A_Master_Sample 
    INNER JOIN OPS_WH_CS_per_Order_for_PC2 
            ON A_Master_Sample.MonthNum = OPS_WH_CS_per_Order_for_PC2.Month_Num 
SET 
    A_Master_Sample.FLWHCost = OPS_WH_CS_per_Order_for_PC2.WH_Cost_per_Order/ A_Master_Sample.ItemsInOrder, 
    A_Master_Sample.FLCSCost = OPS_WH_CS_per_Order_for_PC2.CS_Cost_per_Order/ A_Master_Sample.ItemsInOrder;


