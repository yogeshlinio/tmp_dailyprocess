/*
*  MX_018_001	DateDelivered
UPDATE 
               A_Master_Sample  
    INNER JOIN production_co_co.out_order_tracking b
            ON A_Master_Sample.ItemID = b.order_item_id
SET
   A_Master_Sample.DateDelivered =  b.date_delivered
WHERE 
   b.date_delivered is not null
;


* MX_018_002 Delivered

DROP   TEMPORARY TABLE IF EXISTS A_Delivered;
CREATE TEMPORARY TABLE A_Delivered ( INDEX ( fk_sales_order_item  ) )
SELECT
 *
FROM
    bob_live_co.sales_order_item_status_history
WHERE
   bob_live_co.sales_order_item_status_history.fk_sales_order_item_status IN ( 52 )
GROUP BY fk_sales_order_item
;

UPDATE           A_Delivered
      INNER JOIN A_Master_Sample
              ON A_Delivered.fk_sales_order_item = A_Master_Sample.ItemID 
SET
    A_Master_Sample.Delivered = 1;

UPDATE A_Master_Sample
SET
    A_Master_Sample.NetDelivered = 1
WHERE
    Status in ( select name from BOB_sales_order_item_status
                where id_sales_order_item_status IN ( 6 , 52 ) )
;

UPDATE A_Master_Sample
SET
    A_Master_Sample.DeliveredReturn = 1
WHERE
        Delivered     = 1
    AND NetDelivered  = 0;
*/