/*
* MX_009_001 Shipping Fee
*/ 
#Query: M1_ShipFee_table
UPDATE A_Master_Sample 
SET
   ShippingFee = round(((A_Master_Sample.PackageWeight/ A_Master_Sample.OrderWeight )*A_Master_Sample.ShippingFee),3) 
;


/*
* MX_009_002 
*/
 #Query: M1_ShipFee_CAC
UPDATE A_Master_Sample 
SET 
   A_Master_Sample.ShippingFee = 55
WHERE 
   A_Master_Sample.CouponCode In ( "CAC0xY3Iz","CACs4tyXP");

