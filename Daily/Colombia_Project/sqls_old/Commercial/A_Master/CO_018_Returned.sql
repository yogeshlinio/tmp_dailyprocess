DROP TEMPORARY TABLE IF EXISTS TMP_Returned;
CREATE TEMPORARY TABLE TMP_Returned (  index( fk_sales_order_item  ) )
SELECT
fk_sales_order_item   
FROM  bob_live_co.sales_order_item_status_history
WHERE
fk_sales_order_item_status  = 8
GROUP BY  
fk_sales_order_item;

DROP TEMPORARY TABLE IF EXISTS TMP_Deliver;
CREATE TEMPORARY TABLE TMP_Deliver (  index( fk_sales_order_item  ) )
SELECT
fk_sales_order_item   
FROM  bob_live_co.sales_order_item_status_history
WHERE
fk_sales_order_item_status  = 52
GROUP BY  
fk_sales_order_item;


DROP TEMPORARY TABLE IF EXISTS TMP_RefundedNeeded;
CREATE TEMPORARY TABLE TMP_RefundedNeeded (  index( fk_sales_order_item  ) )
SELECT
fk_sales_order_item   
FROM  bob_live_co.sales_order_item_status_history
WHERE
fk_sales_order_item_status  =87
GROUP BY  
fk_sales_order_item;

DROP TEMPORARY TABLE IF EXISTS TMP;
CREATE TEMPORARY TABLE TMP ( KEY ( fk_sales_order_item ) )
SELECT fk_sales_order_item ,
 MAX( IF( fk_sales_order_item_status = 87 , created_at  , null  ) ) AS fecha FROM  
bob_live_co.sales_order_item_status_history
WHERE
    fk_sales_order_item IN     ( SELECT * FROM TMP_Deliver ) 
AND fk_sales_order_item IN     ( SELECT * FROM TMP_RefundedNeeded ) 
AND fk_sales_order_item NOT IN ( SELECT * FROM TMP_Returned ) 
GROUP BY fk_sales_order_item ;

REPLACE TMP
SELECT fk_sales_order_item , MAX( IF( fk_sales_order_item_status = 8 , created_at  , null  ) ) FROM  
bob_live_co.sales_order_item_status_history
WHERE
   
fk_sales_order_item IN ( SELECT * FROM TMP_Returned ) 
GROUP BY fk_sales_order_item ;

UPDATE            A_Master_Sample
       INNER JOIN TMP  ON A_Master_Sample.ItemId = TMP.fk_sales_order_item 
SET
 A_Master_Sample.Returned = 1,
 A_Master_Sample.`DateReturned` = TMP.Fecha
;
