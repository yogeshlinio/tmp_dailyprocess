/*
USE development_mx; 
#Query: A 212 A CategoryTree
DROP TEMPORARY TABLE IF EXISTS A_203_S_Cat1;
CREATE TEMPORARY TABLE A_203_S_Cat1 ( sku varchar(50) , KEY( sku ), KEY( MaxOfdif ) )
SELECT 
   A_E_Pro_SKU_Con_Categorias.sku AS SKU, 
   A_E_Pro_SKU_Con_Categorias.catalog_config_name AS catalog_config_name, 
   Max(A_E_Pro_SKU_Con_Categorias.dif) AS MaxOfdif
FROM A_E_Pro_SKU_Con_Categorias
GROUP BY A_E_Pro_SKU_Con_Categorias.sku, A_E_Pro_SKU_Con_Categorias.catalog_config_name;
 
DROP TEMPORARY TABLE IF EXISTS A_204_S_Cat1;
CREATE TEMPORARY TABLE A_204_S_Cat1 ( sku varchar(50) ,  KEY( sku ) ) 
SELECT 
     A_203_S_Cat1.sku, 
	 A_203_S_Cat1.catalog_config_name, 
	 A_E_Pro_SKU_Con_Categorias.catalog_category_name
FROM            A_203_S_Cat1 
     INNER JOIN A_E_Pro_SKU_Con_Categorias 
	       ON  (   A_203_S_Cat1.MaxOfdif = A_E_Pro_SKU_Con_Categorias.dif) AND (A_203_S_Cat1.sku = A_E_Pro_SKU_Con_Categorias.sku);


DROP TEMPORARY TABLE IF EXISTS A_205_S_Cat2;
CREATE TEMPORARY TABLE A_205_S_Cat2 ( sku varchar(50) ,  KEY( sku ) ) 
SELECT 
   A_E_Pro_SKU_Con_Categorias.sku, 
   A_E_Pro_SKU_Con_Categorias.catalog_config_name, 
   A_E_Pro_SKU_Con_Categorias.catalog_category_name, 
   A_E_Pro_SKU_Con_Categorias.dif
FROM            A_E_Pro_SKU_Con_Categorias 
     INNER JOIN A_203_S_Cat1 
	     ON A_E_Pro_SKU_Con_Categorias.sku = A_203_S_Cat1.sku
WHERE (((A_E_Pro_SKU_Con_Categorias.dif)< maxofdif));
 
 

DROP TEMPORARY TABLE IF EXISTS A_206_S_Cat2;
CREATE TEMPORARY TABLE A_206_S_Cat2 ( sku varchar(50) ,  KEY( sku ), KEY( MaxOfdif ) ) 
SELECT 
   A_205_S_Cat2.sku AS SKU, 
   A_205_S_Cat2.catalog_config_name AS catalog_config_name, 
   Max(A_205_S_Cat2.dif) AS MaxOfdif
FROM A_205_S_Cat2
GROUP BY A_205_S_Cat2.sku, A_205_S_Cat2.catalog_config_name;


DROP TEMPORARY TABLE IF EXISTS A_207_S_Cat2;
CREATE TEMPORARY TABLE A_207_S_Cat2 ( sku varchar(50), key( sku ) )
SELECT 
   A_206_S_Cat2.sku, 
   A_206_S_Cat2.catalog_config_name, 
   A_E_Pro_SKU_Con_Categorias.catalog_category_name
FROM            A_206_S_Cat2 
     INNER JOIN A_E_Pro_SKU_Con_Categorias 
	     ON (       A_206_S_Cat2.sku = A_E_Pro_SKU_Con_Categorias.sku) 
		      AND ( A_206_S_Cat2.MaxOfdif = A_E_Pro_SKU_Con_Categorias.dif);
 
DROP TEMPORARY TABLE IF EXISTS A_208_S_Cat3;
CREATE TEMPORARY TABLE A_208_S_Cat3 ( sku varchar(50), key( sku ) ) 
SELECT 
   A_E_Pro_SKU_Con_Categorias.sku, 
   A_E_Pro_SKU_Con_Categorias.catalog_config_name, 
   A_E_Pro_SKU_Con_Categorias.catalog_category_name, 
   A_E_Pro_SKU_Con_Categorias.dif
FROM 
               A_E_Pro_SKU_Con_Categorias 
	INNER JOIN A_206_S_Cat2 
	    ON A_E_Pro_SKU_Con_Categorias.sku = A_206_S_Cat2.sku
WHERE (((A_E_Pro_SKU_Con_Categorias.dif)<maxofdif));
 
 

DROP TEMPORARY TABLE IF EXISTS A_209_S_Cat3;
CREATE TEMPORARY TABLE A_209_S_Cat3 ( sku varchar(50), key( sku ), key( MaxOfdif ) ) 
SELECT 
       A_208_S_Cat3.sku AS SKU, 
	   A_208_S_Cat3.catalog_config_name AS catalog_config_name, 
	   Max(A_208_S_Cat3.dif) AS MaxOfdif
FROM A_208_S_Cat3
GROUP BY A_208_S_Cat3.sku, A_208_S_Cat3.catalog_config_name;

 
 
 
DROP TEMPORARY TABLE IF EXISTS A_210_S_Cat3;
CREATE TEMPORARY TABLE A_210_S_Cat3 ( sku varchar(50), key( sku ) )
SELECT 
   A_E_Pro_SKU_Con_Categorias.sku, 
   A_E_Pro_SKU_Con_Categorias.catalog_config_name, 
   A_E_Pro_SKU_Con_Categorias.catalog_category_name
FROM            A_209_S_Cat3 
     INNER JOIN A_E_Pro_SKU_Con_Categorias 
	     ON (      A_209_S_Cat3.sku = A_E_Pro_SKU_Con_Categorias.sku) 
		      AND (A_209_S_Cat3.MaxOfdif = A_E_Pro_SKU_Con_Categorias.dif);

TRUNCATE A_E_Pro_CategoryTree; 
INSERT INTO A_E_Pro_CategoryTree ( sku, catalog_config_name, Cat1 )
SELECT 
   A_204_S_Cat1.sku, 
   A_204_S_Cat1.catalog_config_name, 
   A_204_S_Cat1.catalog_category_name AS Cat1
FROM A_204_S_Cat1 ;

UPDATE            A_E_Pro_CategoryTree 
       INNER JOIN A_207_S_Cat2 USING ( sku )
SET
    Cat2 = A_207_S_Cat2.catalog_category_name;
 
UPDATE            A_E_Pro_CategoryTree 
       INNER JOIN A_210_S_Cat3 USING ( sku )
SET
    Cat3 = A_210_S_Cat3.catalog_category_name;
	*/
