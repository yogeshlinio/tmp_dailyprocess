/*IMPLEMENT ON COLOMBIA*/

/*
*  Inventory
*/
DROP TEMPORARY TABLE IF EXISTS TMP_out_order_tracking;
CREATE TEMPORARY TABLE TMP_out_order_tracking ( KEY ( order_item_id ) )
SELECT
    CAST(  order_item_id AS DECIMAL ) AS order_item_id
FROM
   production_co.out_order_tracking
WHERE
   fullfilment_type_real = "inventory"
;

UPDATE         A_Master_Sample 
SET
   
   A_Master_Sample.isMPlaceSince = 0,
   A_Master_Sample.MPlaceFee     = 0,
   A_Master_Sample.isMPlace      = 0
WHERE 
   ItemId in (
               SELECT *  FROM TMP_out_order_tracking
             )
AND isMPlace = 1
#AND Supplier not in ( select Bob_Supplier_Name from A_E_BI_Marketplace_Commission WHERE ownWarehouse = 1)
;