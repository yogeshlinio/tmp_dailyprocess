/*
*  MX_008_001 Shipping_Cost 201210 201211
*/
/*
* MX_008_003 Shipping_Cost from June
*/
 #Query: M1_ShipmentCostFromJune13
UPDATE A_Master_Sample 
SET 
    A_Master_Sample.ShippingCost = ( A_Master_Sample.ShipmentCost/( 1.2 * 0.9 ) ) * 1.05
WHERE (((A_Master_Sample.Date)>="2013/06/01"));

/*
*  Inventory
*  production_co.out_order_tracking should be implementted
*/
DROP TEMPORARY TABLE IF EXISTS TMP_out_order_tracking;
CREATE TEMPORARY TABLE TMP_out_order_tracking ( KEY ( order_item_id ) )
SELECT
    CAST(  order_item_id AS DECIMAL ) AS order_item_id
FROM
   production_co.out_order_tracking
WHERE
   fullfilment_type_real = "dropshipping"
;

UPDATE        TMP_out_order_tracking
   INNER JOIN A_Master_Sample
           ON order_item_id = ItemId
SET
   A_Master_Sample.ShippingCost = 0
WHERE MonthNum >= date_format( now() - INTERVAL 3 MONTH , "%Y%m" ) 
;