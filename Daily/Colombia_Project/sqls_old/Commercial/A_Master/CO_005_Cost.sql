/*
*  MX_005_002 Adjust null cost
*/
UPDATE        A_Master_Sample
   INNER JOIN A_Master_Catalog
           ON A_Master_Sample.SKUSimple = A_Master_Catalog.SKU_simple
SET
   A_Master_Sample.Cost         = A_Master_Catalog.cost,
   A_Master_Sample.CostAfterTax = A_Master_Catalog.cost / 
                                                 ( 1 +  ( A_Master_Sample.TaxPercent / 100 ) )          
WHERE
      A_Master_Sample.Cost is null 
   OR A_Master_Sample.Cost = 0
;

/*
* MX_005_003 Cost_OMS
*/
UPDATE             A_Master_Sample  
        INNER JOIN wmsprod_co.itens_venda b 
                ON b.item_id = A_Master_Sample.ItemID
        INNER JOIN wmsprod_co.estoque c 
                ON c.estoque_id = b.estoque_id 
        INNER JOIN wmsprod_co.itens_recebimento d
                ON c.itens_recebimento_id = d.itens_recebimento_id 
        INNER JOIN procurement_live_co.wms_received_item e 
                ON c.itens_recebimento_id=e.id_wms 
        INNER JOIN procurement_live_co.procurement_order_item f
                ON e.fk_procurement_order_item=f.id_procurement_order_item 
SET 
    A_Master_Sample.Cost           = f.unit_price ,
    A_Master_Sample.CostAftertax   = f.unit_price / ( ( 100 +  A_Master_Sample.TaxPercent )/100 )
WHERE 
       f.is_deleted = 0
   AND (    A_Master_Sample.Date >= "2013-07-01" 
         OR A_Master_Sample.Cost is null 
         OR A_Master_Sample.Cost <= 0 )
   AND f.unit_price is not null 
   AND f.unit_price > 0
       ;
 
/*
UPDATE        A_Master_Sample
   INNER JOIN A_Master_Catalog 
           ON A_Master_Sample.SKUSimple = A_Master_Catalog.sku_simple
SET 
   A_Master_Sample.Cost         = A_Master_Catalog.cost,
   A_Master_Sample.CostAfterTax = A_Master_Catalog.cost /( 1 + A_Master_Sample.TaxPercent/ 100 )
WHERE 
(      
      A_Master_Sample.Cost is null  
   OR A_Master_Sample.Cost <= 0 
)
AND A_Master_Catalog.cost is not null 
AND A_Master_Catalog.cost > 0
;
*/
