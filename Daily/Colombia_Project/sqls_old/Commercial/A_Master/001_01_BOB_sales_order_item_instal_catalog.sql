/* 
*   UPDATE FIELDS ON OSRI
*/
UPDATE            A_Master_Sample
       INNER JOIN A_Master_Catalog
	             ON A_Master_Catalog.SKU_Simple = A_Master_Sample.SKUSimple
SET
    A_Master_Sample.SKUConfig   = A_Master_Catalog.sku_config,
    A_Master_Sample.SKUName     = A_Master_Catalog.sku_name,	
    A_Master_Sample.IdSupplier  = A_Master_Catalog.id_catalog_supplier,
    A_Master_Sample.Supplier    = A_Master_Catalog.Supplier,
    A_Master_Sample.Buyer       = A_Master_Catalog.Buyer,
    A_Master_Sample.HeadBuyer   = A_Master_Catalog.Head_Buyer,
    A_Master_Sample.Brand       = A_Master_Catalog.Brand,
    A_Master_Sample.isMPlace    = A_Master_Catalog.isMarketPlace,
    A_Master_Sample.isMPlaceSince = A_Master_Catalog.isMarketPlace_Since,
    A_Master_Sample.isMPlaceUntil = A_Master_Catalog.isMarketPlace_Until,

    A_Master_Sample.MPlaceFee     = A_Master_Catalog.MPlace_Fee,

    A_Master_Sample.isVisible           = A_Master_Catalog.isVisible,
    A_Master_Sample.isActiveSKUConfig   = A_Master_Catalog.isActive_SKUConfig,
    A_Master_Sample.isActiveSKUSimple   = A_Master_Catalog.isActive_SKUSimple,

    A_Master_Sample.Cat1       = A_Master_Catalog.Cat1,
    A_Master_Sample.Cat2       = A_Master_Catalog.Cat2,
    A_Master_Sample.Cat3       = A_Master_Catalog.Cat3,
   
    A_Master_Sample.OldCat1    = A_Master_Catalog.OldCat1, 
    A_Master_Sample.OldCat2    = A_Master_Catalog.OldCat2, 
    A_Master_Sample.OldCat3    = A_Master_Catalog.OldCat3, 
    A_Master_Sample.CatKPI     = A_Master_Catalog.Cat_KPI,
    A_Master_Sample.CatBP      = A_Master_Catalog.Cat_BP,
    
    A_Master_Sample.PackageWeight = A_Master_Catalog.Package_Weight,
    A_Master_Sample.ProductWeight = A_Master_Catalog.Product_weight,

    A_Master_Sample.DeliveryCostSupplier = A_Master_Catalog.Delivery_Cost_Supplier  
    
;

/*
*	 001_999_001 Adjusts 
*/
UPDATE A_Master_Sample
SET
    A_Master_Sample.OriginalPrice = A_Master_Sample.Price
WHERE A_Master_Sample.OriginalPrice <= 0;



