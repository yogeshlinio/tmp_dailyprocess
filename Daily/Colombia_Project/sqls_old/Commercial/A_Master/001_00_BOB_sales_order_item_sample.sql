/*
UPDATE A_Master_Sample
INNER JOIN bob_live_co.sales_order
ON A_Master_Sample.idSalesORder = bob_live_co.sales_order.id_sales_order
SET 
`ShippingFee`   	 = bob_live_co.sales_order.shipping_amount  /1.16 
;
*/
SET @since = "2013-01-01";
TRUNCATE A_Master;
DROP TABLE IF EXISTS A_Master_Sample; 
CREATE TABLE A_Master_Sample LIKE A_Master;
ALTER TABLE A_Master_Sample
            ADD COLUMN fk_customer_address_region      INT NOT NULL,
            ADD COLUMN fk_sales_order_address_shipping INT NOT NULL,
            ADD COLUMN fk_sales_order_item_status      INT NOT NULL,
            ADD COLUMN fk_catalog_shipment_type        INT NOT NULL,
            ADD INDEX ( couponCode ),
            ADD INDEX ( fk_customer_address_region ),
            ADD INDEX ( fk_sales_order_address_shipping ),
            ADD INDEX ( fk_sales_order_item_status ),
            ADD INDEX (fk_catalog_shipment_type  ),
            ADD Index PaymentMethodStatus ( paymentMethod , status )
;

INSERT A_Master_Sample
( 
  `Country`   	,
  `MonthNum`   	,
  `Date`   	,
  `OrderNum`   	,
   idSalesOrder	,
  `ItemID`   	,
  `StoreId`   	,
  `SKUSimple`   	,
  `SKUName`   	,
  `OriginalPrice`   	,
  `TaxPercent`   	,
  `Tax`   	,
  `Price`   	,
  `PriceAfterTax`   	,
  `CouponCode`   	,
  `CouponValue`   	,
  `CouponValueAfterTax`   	,
  `PaidPrice`   	,
  `PaidPriceAfterTax`   	,
  `GrandTotal`   	,
  `Cost`   	,
  `CostAfterTax`   	,
  `ShipmentCost`   	,
  `ShippingFee`   	,
  `PaymentMethod`   	,
  `CustomerNum`   	,
  `CustomerEmail`   	,
  `FirstName`   	,
  `LastName`   	,
  `fk_sales_order_address_shipping` ,
  `fk_sales_order_item_status`,
  `fk_catalog_shipment_type`
)
SELECT
"COL" AS Country,
date_format( bob_live_co.sales_order.created_at , "%Y%m" )	AS	`MonthNum`   	,
bob_live_co.sales_order.created_at	AS	`Date`   	,
bob_live_co.sales_order.order_nr	AS	`OrderNum`   	,
bob_live_co.sales_order.id_sales_order	AS	idSalesOrder	,
bob_live_co.sales_order_item.id_sales_order_item	AS	`ItemID`   	,
bob_live_co.sales_order.store_id	AS	`StoreId`   	,
bob_live_co.sales_order_item.sku	AS	`SKUSimple`   	,
bob_live_co.sales_order_item.name	AS	`SKUName`   	,
bob_live_co.sales_order_item.original_unit_price	AS	`OriginalPrice`   	,
bob_live_co.sales_order_item.tax_percent	AS	`TaxPercent`   	,
bob_live_co.sales_order_item.tax_amount	AS	`Tax`   	,
bob_live_co.sales_order_item.unit_price	AS	`Price`   	,
bob_live_co.sales_order_item.unit_price /( ( 100 +  bob_live_co.sales_order_item.tax_percent )/100 )	AS	`PriceAfterTax`   	,
bob_live_co.sales_order.coupon_code	AS	`CouponCode`   	,
bob_live_co.sales_order_item.coupon_money_value	AS	`CouponValue`   	,
bob_live_co.sales_order_item.coupon_money_value/( ( 100 +  bob_live_co.sales_order_item.tax_percent )/100 )	AS	`CouponValueAfterTax`   	,
bob_live_co.sales_order_item.paid_price	AS	`PaidPrice`   	,
bob_live_co.sales_order_item.paid_price/( ( 100 +  bob_live_co.sales_order_item.tax_percent )/100 )	AS	`PaidPriceAfterTax`   	,
bob_live_co.sales_order.Grand_Total	AS	`GrandTotal`   	,
bob_live_co.sales_order_item.cost	AS	`Cost`   	,
bob_live_co.sales_order_item.cost /( ( 100 +  bob_live_co.sales_order_item.tax_percent )/100 )	AS	`CostAfterTax`   	,
bob_live_co.sales_order_item.shipment_cost	AS	`ShipmentCost`   	,
#bob_live_co.sales_order.shipping_amount  /( ( 100 +  bob_live_co.sales_order_item.tax_percent )/100 )	AS	`ShippingFee`   	,
bob_live_co.sales_order.shipping_amount  /1.16 AS	`ShippingFee`   	,

bob_live_co.sales_order.payment_method	AS	`PaymentMethod`   	,
bob_live_co.sales_order.fk_customer	AS	`CustomerNum`   	,
bob_live_co.sales_order.customer_email	AS	`CustomerEmail`   	,
bob_live_co.sales_order.customer_first_name	AS	`FirstName`   	,
bob_live_co.sales_order.customer_last_name	AS	`LastName`   	,
bob_live_co.sales_order.fk_sales_order_address_shipping	AS	`fk_customer_address_shipping` ,
bob_live_co.sales_order_item.fk_sales_order_item_status ,
bob_live_co.sales_order_item.fk_catalog_shipment_type
FROM 
              bob_live_co.sales_order 
   INNER JOIN bob_live_co.sales_order_item
           ON bob_live_co.sales_order_item.fk_sales_order = bob_live_co.sales_order.id_sales_order
#WHERE
#     bob_live_co.sales_order.created_at Between @since and ( LAST_DAY( @since ) + INTERVAL 1 DAY )
#  OR bob_live_co.sales_order.updated_at Between @since and ( LAST_DAY( @since ) + INTERVAL 1 DAY )
#  OR bob_live_co.sales_order_item.created_at Between @since and ( LAST_DAY( @since ) + INTERVAL 1 DAY )
#  OR bob_live_co.sales_order_item.updated_at Between @since and ( LAST_DAY( @since ) + INTERVAL 1 DAY )
;
UPDATE            A_Master_Sample
       INNER JOIN bob_live_co.sales_order_address
               ON bob_live_co.sales_order_address.id_sales_order_address = A_Master_Sample.fk_sales_order_address_shipping
SET
   A_Master_Sample.postcode                   = bob_live_co.sales_order_address.postcode, 
   A_Master_Sample.city                       = bob_live_co.sales_order_address.city,
   A_Master_Sample.fk_customer_address_region = bob_live_co.sales_order_address.fk_customer_address_region
;
UPDATE            A_Master_Sample
       INNER JOIN bob_live_co.customer_address_region
               ON bob_live_co.customer_address_region.id_customer_address_region = A_Master_Sample.fk_customer_address_region
SET
   A_Master_Sample.State = bob_live_co.customer_address_region.code 
;

UPDATE            A_Master_Sample
       INNER JOIN bob_live_co.sales_order_item_status 
               ON A_Master_Sample.fk_sales_order_item_status = bob_live_co.sales_order_item_status.id_sales_order_item_status
SET
   A_Master_Sample.status = bob_live_co.sales_order_item_status.name;


UPDATE            A_Master_Sample
       INNER JOIN bob_live_co.catalog_shipment_type 
               ON A_Master_Sample.fk_catalog_shipment_type = bob_live_co.catalog_shipment_type.id_catalog_shipment_type
SET
   A_Master_Sample.ShipmentType = bob_live_co.catalog_shipment_type.name_en;


DROP TEMPORARY TABLE IF EXISTS TMP_Track;
CREATE TEMPORARY TABLE TMP_Track ( PRIMARY KEY ( order_item_id ) )
SELECT
  CAST( order_item_id as DECIMAL) AS  order_item_id,
  fullfilment_type_real
FROM production_co.out_order_tracking
GROUP BY order_item_id
;

UPDATE            A_Master_Sample
       INNER JOIN TMP_Track
               ON A_Master_Sample.ItemId = TMP_Track.order_item_id
SET
   A_Master_Sample.ShipmentType = TMP_Track.fullfilment_type_real
;
