UPDATE A_Master_Sample
SET
   PaymentFees =  A_Master_Sample.PaidPrice /( 1 + A_Master_Sample.TaxPercent / 100 )  * ( A_Master_Sample.Fees / 100 )
                 +A_Master_Sample.ExtraCharge
;

/*
*   MX_001_006_Paypal_Vouchers
*/
UPDATE A_Master_Sample
 SET 
    A_Master_Sample.CouponValue           = 0, 
    A_Master_Sample.CouponValueAfterTax   = 0, 
    A_Master_Sample.PaymentFees           = 0
    #A_Master_Sample.Commercial_price_after_tax = A_Master_Sample.price_after_tax
WHERE 
     A_Master_Sample.CouponCode in ( "COM1Tw3Bz" ,
                                                    "COM1QuGJR" , 
                                                    "COMq517VW" ,
                                                    "COMc4jiTb"  ,  
                                                    "COM57OMUy" )
  OR A_Master_Sample.CouponCode like "%MKTpaypal%"
;
