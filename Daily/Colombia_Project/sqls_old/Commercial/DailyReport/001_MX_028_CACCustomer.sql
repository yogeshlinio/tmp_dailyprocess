#Query: N1:

drop table if EXISTS development_mx.CACcustomer;
create table development_mx.CACcustomer
(
 `CustomerNum` VARCHAR (20) NOT NULL,
 `FirstNetOrder` VARCHAR (20) NOT NULL,
 `FirstGrossOrder` VARCHAR (20) NOT NULL,
 `CohortMonth` INT NOT NULL,
 `Campaign` VARCHAR (50) NOT NULL,
 `ChannelGroup` VARCHAR (36) NOT NULL,
 `Channel` VARCHAR(36) NOT NULL,
 `SourceMedium` VARCHAR (36) NOT NULL
) 
SELECT
  DISTINCT CustomerNum AS CustomerNum,
           FirstOrderNum AS FirstNetOrder,
           CohortMonthNum AS CohortMonth,
           Campaign AS Campaign,
           Channel_group AS ChannelGroup ,
           Source_medium AS SourceMedium 
from development_mx.A_Master
where FirstOrderNum = OrderNum
 and Channel_group = 'CAC Deals'
;

create index CustomerNum on development_mx.A_Master(CustomerNum);

UPDATE A_Master INNER JOIN CACcustomer on 
A_Master.CustomerNum = CACcustomer.CustomerNum
SET 
    A_Master.CACCustomer = if(A_Master.CustomerNum = CACcustomer.CustomerNum, 1, 0);