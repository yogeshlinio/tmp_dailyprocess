#Query: N1:
UPDATE development_mx.A_Master INNER JOIN production.tbl_order_detail
SET 
    A_Master.Source_medium = tbl_order_detail.source_medium,
		A_Master.Campaign = tbl_order_detail.Campaign,
		A_Master.Channel = tbl_order_detail.Channel,
		A_Master.Channel_group = tbl_order_detail.Channel_group
WHERE
	A_Master.ItemID = tbl_order_detail.item
;