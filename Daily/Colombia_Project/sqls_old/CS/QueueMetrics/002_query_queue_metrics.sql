use test;
drop  TABLE if EXISTS sampleQueue;
create  TABLE sampleQueue
select * from queuemetrics.queue_log  where FROM_UNIXTIME(time_id,'%Y-%m-%d')

#in ('2013-12-30','2013-12-31')

>=date_sub(date_format(curdate(),'%Y-%m-%d'),interval 1 day)
 and FROM_UNIXTIME(time_id,'%Y-%m-%d')<date_format(curdate(),'%Y-%m-%d')
;
SELECT FROM_UNIXTIME(time_id,'%Y-%m-%d')AS Fecha
,FROM_UNIXTIME(time_id,'%H:%i:%s')AS Time
,FROM_UNIXTIME(time_id,'%H')AS Hora,
Llamante,Cola,Espera,Duracion,Administrada,Contador,Atendida,Abandonada,Transferencia,Id
FROM
(SELECT VERB,DATA1 AS Espera,DATA2 AS Duracion,call_id,agent as Administrada,'1' as Atendida,'0' as Abandonada,'0' as Transferencia
,'1' as Contador,call_id as id
 FROM sampleQueue WHERE VERB IN ('COMPLETEAGENT','COMPLETECALLER') AND queue in ('54646','54646-2','43556'))A
LEFT JOIN
(SELECT VERB,DATA2 AS Llamante,call_id,time_id,CASE 1 WHEN FROM_UNIXTIME(time_id,'%Y-%m-%d')<'2013-09-09' THEN 'linio Post/Pre' WHEN queue='54646' THEN 'Pre-Venta' WHEN queue='54646-2' THEN 'Post-Venta' WHEN queue='43556' THEN 'HelloFood' END as Cola FROM sampleQueue WHERE VERB IN ('ENTERQUEUE') AND queue in ('54646','54646-2','43556'))B
ON A.call_id=B.call_id
UNION ALL
SELECT FROM_UNIXTIME(time_id,'%Y-%m-%d')AS Fecha
,FROM_UNIXTIME(time_id,'%H:%i:%s')AS Time
,FROM_UNIXTIME(time_id,'%H')AS Hora,
Llamante,Cola,Espera,Duracion,Administrada,Contador,Atendida,Abandonada,Transferencia,id
FROM
(SELECT VERB,DATA3 AS Espera,null AS Duracion,call_id,null as Administrada,'0' as Atendida,'1' as Abandonada,'0' as Transferencia
,'1' as Contador,call_id as id
 FROM sampleQueue WHERE VERB IN ('ABANDON') AND queue in ('54646','54646-2','43556'))A
LEFT JOIN
(SELECT VERB,DATA2 AS Llamante,call_id,time_id,CASE 1 WHEN FROM_UNIXTIME(time_id,'%Y-%m-%d')<'2013-09-09' THEN 'linio Post/Pre' WHEN queue='54646' THEN 'Pre-Venta' WHEN queue='54646-2' THEN 'Post-Venta' WHEN queue='43556'   THEN 'HelloFood' END as Cola FROM sampleQueue WHERE VERB IN ('ENTERQUEUE') AND queue in ('54646','54646-2','43556'))B
ON A.call_id=B.call_id
UNION ALL
SELECT FROM_UNIXTIME(time_id,'%Y-%m-%d')AS Fecha
,FROM_UNIXTIME(time_id,'%H:%i:%s')AS Time
,FROM_UNIXTIME(time_id,'%H')AS Hora,
Llamante,Cola,Espera,Duracion,Administrada,Contador,Atendida,Abandonada,Transferencia,id
FROM
(SELECT VERB,DATA4 AS Duracion,call_id,agent as Administrada,'0' as Atendida,'0' as Abandonada,'1' as Transferencia
,'1' as Contador,call_id as id
 FROM sampleQueue WHERE VERB IN ('TRANSFER') AND queue in ('54646','54646-2','43556'))A
LEFT JOIN
(SELECT VERB,DATA2 AS Llamante,call_id,time_id,CASE 1 WHEN FROM_UNIXTIME(time_id,'%Y-%m-%d')<'2013-09-09' THEN 'linio Post/Pre' WHEN queue='54646' THEN 'Pre-Venta' WHEN queue='54646-2' THEN 'Post-Venta' WHEN queue='43556'   THEN 'HelloFood' END as Cola FROM sampleQueue WHERE VERB IN ('ENTERQUEUE') AND queue in ('54646','54646-2','43556'))B
ON A.call_id=B.call_id
LEFT JOIN
(SELECT VERB,DATA1 AS Espera,call_id FROM sampleQueue WHERE VERB IN ('CONNECT') AND queue in ('54646','54646-2','43556'))C
ON A.call_id=C.call_id
;
