/*
Reemplaza a call sp_sales_Report;
*/
#----------------------------------------------------------------------
#   Insert into table_monitoring_log START
#----------------------------------------------------------------------
INSERT INTO production.table_monitoring_log
SELECT
null,
"Mexico",
"tbl_sales_hist_ev",
"Start",
now(),
now(),
0,
0
;

DROP TEMPORARY TABLE IF EXISTS sales_order_ev;
CREATE TEMPORARY TABLE sales_order_ev (key(order_nr))
SELECT
	created_at,
	assisted_sales_operator,
	order_nr 
FROM 
	bob_live_mx.sales_order
WHERE 
	DATE_FORMAT(created_at, '%Y%m')>= 201309 and assisted_sales_operator is not null;


DROP TEMPORARY TABLE IF EXISTS A_Master_ev;
CREATE TEMPORARY TABLE A_Master_ev (key(OrderNum))
SELECT 
	Master.OrderNum,
	Master.ItemID,
  Master.OriginalPrice,
	Master.Price,
  Master.CouponCode,
  Master.CouponValue,
  Master.PaidPrice,
  Master.TaxPercent,
  Master.PaidPriceAfterTax,

  Master.Cancellations,
	Master.OrderAfterCan,
	Master.OrderBeforeCan,
  Master.Returns,
  
	Master.Status,
	Master.PaymentMethod,
  Master.Installment,

	Master.DateCollected,
	Master.DateDelivered,
	Sales.created_at,
	Sales.assisted_sales_operator
FROM 
	           development_mx.A_Master as Master
  INNER JOIN sales_order_ev       AS Sales 
          ON Sales.order_nr = Master.OrderNum
;

#INSERT INTO tbl_sales_hist_ev
DROP TABLE IF EXISTS tbl_sales_hist_ev;
CREATE TABLE tbl_sales_hist_ev ( 
                                 PRIMARY KEY ( Date, telesales,agent_Name, OrderNum, ItemID ) , 
                                         KEY ( telesales) , 
                                         KEY ( OrderNum ),
                                         KEY ( ItemID )
 )
SELECT
	DATE(created_at) AS Date,
	Master.created_at,
	Master.assisted_sales_operator AS telesales,
	cast( Nombre as char(50) ) AS agent_Name,
	Master.OrderNum,
	Master.ItemID,
    Master.OriginalPrice,
	Master.Price,
    Master.CouponCode,
    Master.CouponValue,
    Master.PaidPrice,
    Master.TaxPercent,
    Master.PaidPriceAfterTax,

    Master.Cancellations,
	Master.OrderAfterCan,
	Master.OrderBeforeCan,
    Master.Returns,
  
	Master.Status,
	Master.PaymentMethod,
    Master.Installment,

	Master.DateCollected,
	Master.DateDelivered
FROM
	        tbl_staff_ev a
LEFT JOIN A_Master_ev AS Master 
       ON a.telesales = Master.assisted_sales_operator AND a.telesales is not null
ORDER BY OrderNum, ItemId
;

TRUNCATE rep_detalle_ventas_mes;
INSERT INTO rep_detalle_ventas_mes (
yearMonth,
agent_Name
)
SELECT DISTINCT
	DATE_FORMAT(case when DateCollected = '0000-00-00' then date else Datecollected end, '%Y%m') yearMonth,
	agent_Name
FROM
	tbl_sales_hist_ev;


DROP TEMPORARY TABLE IF EXISTS grossTABLE;
CREATE TEMPORARY TABLE grossTABLE (key(yearM))
SELECT 
	yearM,
	agent_Name,
	COUNT(OrderNum) AS gross_Ord,
	SUM(grossRev) AS gross_Rev 

FROM 
(	SELECT
		DATE_FORMAT(Date,'%Y%m')yearM,
		agent_Name,
		OrderNum,
		sum(Price)grossRev 
	FROM 
		tbl_sales_hist_ev
	GROUP BY
		DATE_FORMAT(Date,'%Y%m'),
		agent_Name,
		OrderNum
)A
GROUP BY
	yearM,
	agent_Name
;

UPDATE customer_service.rep_detalle_ventas_mes A
LEFT JOIN
grossTABLE B
ON A.agent_name=B.agent_Name and A.yearMonth=B.yearM
SET grossOrder=gross_Ord;


UPDATE customer_service.rep_detalle_ventas_mes A
LEFT JOIN
grossTABLE B
ON A.agent_name=B.agent_Name and A.yearMonth=B.yearM
SET grossRev=gross_rev;


DROP TEMPORARY TABLE IF EXISTS netTABLE;
CREATE TEMPORARY TABLE netTABLE (key(yearM))
SELECT 
	yearM,
	agent_Name,
	COUNT(OrderNum) AS net_Ord,
	SUM(netRev) AS net_Rev 

FROM 
(	SELECT
DATE_FORMAT(case when DateCollected = '0000-00-00' then date else Datecollected end  ,'%Y%m') As yearM,		


		agent_Name,
		OrderNum,
		SUM(Price)netRev 
	FROM 
		tbl_sales_hist_ev
	WHERE
		 
`Status` in ('delivered','shipped_and_stock_updated','closed','ready_to_ship','exported')
		
	
	GROUP BY
		DATE_FORMAT(case when DateCollected = '0000-00-00' then date else Datecollected end  ,'%Y%m'),
		agent_Name,
		OrderNum
)A
GROUP BY
	yearM,
	agent_Name
;


UPDATE customer_service.rep_detalle_ventas_mes A
LEFT JOIN
netTABLE B
ON A.agent_name=B.agent_Name and A.yearMonth=B.yearM
SET netOrder=net_Ord;


UPDATE customer_service.rep_detalle_ventas_mes A
LEFT JOIN
netTABLE B
ON A.agent_name=B.agent_Name and A.yearMonth=B.yearM
SET netRev=net_Rev;


UPDATE customer_service.rep_detalle_ventas_mes A
LEFT JOIN
netTABLE B
ON A.agent_name=B.agent_Name and A.yearMonth=B.yearM
LEFT JOIN
(
SELECT *,CONCAT(CAST(`Year` AS CHAR),`Month`)yr FROM 
tbl_monthly_target) C
ON B.yearM=C.yr
SET targetOrdAch=B.net_Ord/C.orderTarget;


UPDATE customer_service.rep_detalle_ventas_mes A
LEFT JOIN
netTABLE B
ON A.agent_name=B.agent_Name and A.yearMonth=B.yearM
LEFT JOIN
(
SELECT *,CONCAT(CAST(`Year` AS CHAR),`Month`)yr FROM 
tbl_monthly_target) C
ON B.yearM=C.yr
SET targetRevAch=B.net_Rev/C.revenueTarget;


DROP TEMPORARY TABLE IF EXISTS pendingTABLE;
CREATE TEMPORARY TABLE pendingTABLE (key(yearM))

SELECT 
	yearM,
	agent_Name,
	COUNT(OrderNum) AS peding_Ord,
	SUM(grossRev) AS pending_Rev 

FROM 
(	SELECT
		DATE_FORMAT(Date,'%Y%m')yearM,
		agent_Name,
		OrderNum,
		sum(Price)grossRev 
	FROM 
		tbl_sales_hist_ev
where `Status` in ('payment_confirmation_pending_reminded_2','manual_fraud_check_pending','payment_pending','payment_confirmation_pending_reminded_1')
	
	
	
	GROUP BY
		DATE_FORMAT(Date,'%Y%m'),
		agent_Name,
		OrderNum
)A
GROUP BY
	yearM,
	agent_Name
;


UPDATE customer_service.rep_detalle_ventas_mes A
LEFT JOIN
pendingTABLE B
ON A.agent_name=B.agent_Name and A.yearMonth=B.yearM
SET pendingOrder=peding_Ord;


UPDATE customer_service.rep_detalle_ventas_mes A
LEFT JOIN
pendingTABLE B
ON A.agent_name=B.agent_Name and A.yearMonth=B.yearM
SET pendingRev=pending_Rev;

TRUNCATE rep_detalle_ventas_dia;
INSERT INTO rep_detalle_ventas_dia (
Date,
agent_Name
)
SELECT DISTINCT
	case when DateCollected = '0000-00-00' then date else Datecollected end  As Date,
	agent_Name
FROM
	tbl_sales_hist_ev;


DROP TEMPORARY TABLE IF EXISTS grossTABLE;
CREATE TEMPORARY TABLE grossTABLE (key(Date,agent_Name))

SELECT 
	Date,
	agent_Name,
	COUNT(OrderNum) AS gross_Ord,
	SUM(grossRev) AS gross_Rev 

FROM 
(	SELECT
		Date,
		agent_Name,
		OrderNum,
		sum(Price)grossRev 
	FROM 
		tbl_sales_hist_ev
	
	
	
	GROUP BY
		Date,
		agent_Name,
		OrderNum
)A
GROUP BY
	date,
	agent_Name
;

UPDATE customer_service.rep_detalle_ventas_dia A
LEFT JOIN
grossTABLE B
ON A.agent_name=B.agent_Name and A.Date=B.Date
SET grossOrder=gross_Ord;


UPDATE customer_service.rep_detalle_ventas_dia A
LEFT JOIN
grossTABLE B
ON A.agent_name=B.agent_Name and A.Date=B.Date
SET grossRev=gross_rev;


DROP TEMPORARY TABLE IF EXISTS netTABLE;
CREATE TEMPORARY TABLE netTABLE (key(Date,agent_Name))
SELECT 
	Date,
	agent_Name,
	COUNT(OrderNum) AS net_Ord,
	SUM(netRev) AS net_Rev 

FROM 
(	SELECT
case when DateCollected = '0000-00-00' then date else Datecollected end  As Date,		


		agent_Name,
		OrderNum,
		SUM(Price)netRev 
	FROM 
		tbl_sales_hist_ev
	WHERE
		 
`Status` in ('delivered','shipped_and_stock_updated','closed','ready_to_ship','exported')
		
	
	GROUP BY
		case when DateCollected = '0000-00-00' then date else Datecollected end,
		agent_Name,
		OrderNum
)A
GROUP BY
	Date,
	agent_Name
;

UPDATE customer_service.rep_detalle_ventas_dia A
LEFT JOIN
netTABLE B
ON A.agent_name=B.agent_Name and A.Date=B.Date
SET netOrder=net_Ord;


UPDATE customer_service.rep_detalle_ventas_dia A
LEFT JOIN
netTABLE B
ON A.agent_name=B.agent_Name and A.Date=B.Date
SET netRev=net_Rev;



DROP TEMPORARY TABLE IF EXISTS pendingTABLE;
CREATE TEMPORARY TABLE pendingTABLE (key(Date,agent_Name))

SELECT 
	Date,
	agent_Name,
	COUNT(OrderNum) AS peding_Ord,
	SUM(grossRev) AS pending_Rev 

FROM 
(	SELECT
		Date,
		agent_Name,
		OrderNum,
		sum(Price)grossRev 
	FROM 
		tbl_sales_hist_ev
where `Status` in ('payment_confirmation_pending_reminded_2','manual_fraud_check_pending','payment_pending','payment_confirmation_pending_reminded_1')
	
	
	
	GROUP BY
		Date,
		agent_Name,
		OrderNum
)A
GROUP BY
	Date,
	agent_Name
;


UPDATE customer_service.rep_detalle_ventas_dia A
LEFT JOIN
pendingTABLE B
ON A.agent_name=B.agent_Name and A.Date=B.Date
SET pendingOrder=peding_Ord;


UPDATE customer_service.rep_detalle_ventas_dia A
LEFT JOIN
pendingTABLE B
ON A.agent_name=B.agent_Name and A.Date=B.Date
SET pendingRev=pending_Rev;

#----------------------Llamadas----------------------------------------

UPDATE customer_service.rep_detalle_ventas_dia A
LEFT JOIN
VW_LlamadasxAsesorPreVenta B
ON A.agent_name=B.TELESALES and A.Date=B.Fecha
SET Llamadas=Atn;

#----------------------------------------------------------------------
#   Insert into table_monitoring_log finish
#----------------------------------------------------------------------
INSERT INTO production.table_monitoring_log
SELECT
   null,
   "Mexico",
   "tbl_sales_hist_ev",
   "Finish",
   now(),
   now(),
   COUNT(*),
   COUNT(*)
FROM 
   tbl_sales_hist_ev
;
