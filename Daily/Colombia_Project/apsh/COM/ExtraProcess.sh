v_user=$1
v_pwd=$2
v_host=$3
v_activity="ExtraProcess"
v_date=`date +"%Y%m%d"`
v_country=Colombia
echo    "$v_country Daily Info..."


v_country=marketing
v_event=marketing_report.global_report
v_reply=4000 
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
#. /home/cmondragon/production/ToolKit/monitoring.sh
. $lib/ToolKit/monitoring.sh


for v_folder in $v_activity
do
    echo "Loading $v_folder ..."

    sqls="./sqls/Commercial/${v_folder}"
    for i in $sqls/*.sql
    do
       dateTime=`date`
	   echo $dateTime $i 	   
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b development_co_project < $i
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		
	   
    done
    dateTime=`date`
    echo " ${dateTime} End Load ..."
done