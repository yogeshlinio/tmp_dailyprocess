v_user=$1
v_pwd=$2
v_host=$3
v_date=`date +"%Y%m%d"`
v_country=Mexico

echo    "$v_country Customer Service Sales Report for 30..."
v_event=questions
v_reply=80
v_dependecy=0 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
. $lib/ToolKit/monitoring_asterisk_server.sh

       echo "Loading start monitoring log ..."
       sqls="./sqls/CS/QueueMetrics30"
       sources="./sources"
       dateTime=`date`
       echo $dateTime $i 	   
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b customer_service < $sqls/001_log_inicio_queue30.sql

       echo "Extract information from Asterisk ..."
       cat $sqls/002_query_queue_metrics30.sql | mysql -u emartinez -pe8e2gDQBXnHaXfgWg -h 172.18.0.8 -b queuemetrics >  $sources/result_30.txt

       echo "Update tbl_queueMetrics"
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b customer_service < $sqls/003_etl_queue30.sql
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		

	   
	. $lib/ToolKit/monitoring.sh       
       echo "Update interval"
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b customer_service < $sqls/004_update_frame.sql
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		


       echo "End Log"
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b customer_service < $sqls/005_log_fin_queue30.sql
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		

       dateTime=`date`
       echo " ${dateTime} End Load ..."
exit;
