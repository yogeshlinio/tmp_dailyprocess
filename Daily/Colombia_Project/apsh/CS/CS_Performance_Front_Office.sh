v_user=$1
v_pwd=$2
v_host=$3
v_date=`date +"%Y%m%d"`
v_country=Colombia

echo    "$v_country Performance Front Office CS Agents..."
v_event=cs_performance_fo_agent
v_step=fin
. $lib/ToolKit/monitoringNonExecute.sh

echo    "$v_country Performance Front Office CS Agents..."
v_event=customer_service_co.cs_performance_source_fo_agents
v_reply=200
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
v_step=populated
. $lib/ToolKit/monitoring.sh

for v_folder in CS_Performance_Front_Office
do
    echo "Loading $v_folder ..."
    sqls="./sqls/CS/${v_folder}"
    for i in $sqls/*.sql
    do
       dateTime=`date`
	   echo $dateTime $i 	   
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b customer_service < $i
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		
	   
    done
    dateTime=`date`
    echo " ${dateTime} End Load ..."
done
exit;
