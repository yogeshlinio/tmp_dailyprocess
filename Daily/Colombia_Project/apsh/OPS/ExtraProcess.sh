v_user=$1
v_pwd=$2
v_host=$3
v_activity="ExtraProcess"
v_date=`date +"%Y%m%d"`
v_country=Colombia_Project
echo    "$v_country Daily Info..."

v_event=out_stock_hist
v_step=finish
v_reply=8000
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
. $lib/ToolKit/monitoring.sh

# Solicitado por Laura 29/05/2014
#[10:06:50 a.m.] Laura Forero Manrique: Mario por fa ayúdame a corregir eso de esperar out inverse logistics
#[10:06:58 a.m.] Laura Forero Manrique: eso no tiene q esperar eso jamás :)
#v_event=out_inverse_logistics_tracking
#v_step=finish
#v_reply=8000
#v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
#. $lib/ToolKit/monitoring.sh
  
for v_folder in $v_activity
do
    echo "Loading $v_folder ..."

    sqls="./sqls/OPS/${v_activity}/"
    for i in $sqls/*.sql
    do
       dateTime=`date`
	   echo $dateTime $i 	   
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b operations_co < $i
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		
	   
    done
    dateTime=`date`
    echo " ${dateTime} End Load ..."
done