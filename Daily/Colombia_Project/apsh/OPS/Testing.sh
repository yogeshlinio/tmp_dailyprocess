v_user=$1
v_pwd=$2
v_host=$3
v_activity="Testing"
v_date=`date +"%Y%m%d"`
v_country=Colombia_Project
echo    "$v_country Daily Info..."

for v_folder in $v_activity
do
    echo "Loading $v_folder ..."

    sqls="./sqls/OPS/${v_folder}"
    for i in $sqls/*.sql
    do
       dateTime=`date`
	   echo $dateTime $i 	   
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b operations_co < $i
	   error=`echo $?`
	   if [ $error -gt 0 ]; then
	       exit 1
	   fi

    done
    dateTime=`date`
    echo " ${dateTime} End Load ..."
done
exit 0