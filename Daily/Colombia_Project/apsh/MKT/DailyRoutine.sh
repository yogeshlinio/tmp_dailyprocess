v_user=$1
v_pwd=$2
v_country=$3
v_date=`date +"%Y%m%d"`

path=/var/lib/jenkins/git_repository

#Waiting for Facebook
v_country=Regional
v_event=facebook_campaigns
v_reply=1000  #Wait two hours...
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
. $path/scripts/ToolKit/monitoring.sh
v_country=$3
echo -e "\n\n\n"

#Waiting for bazayaco.tbl_order_detail
v_country=Colombia_Project
#v_event="bazayaco.tbl_order_detail parte 2"
v_event=A_Master
v_step=finish
v_reply=2000  #Wait anything up eight hours...
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
. $path/scripts/ToolKit/monitoring.sh
echo -e "\n\n\n"

for v_folder in MKT/DailyRoutine
do
    echo "Loading $v_folder ..."

    sqls="./sqls/${v_folder}"
    for i in $sqls/*.sql
    do
       dateTime=`date`
       echo "|->"  $dateTime $i
	   mysql -u $v_user -p${v_pwd}   -h 172.17.12.191  -b marketing_report < $i
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		
	   
	done
    dateTime=`date`
    echo " ${dateTime} End Load ..."
done

