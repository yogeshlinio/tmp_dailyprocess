v_user=$1
v_pwd=$2
v_host=$3
v_folder=$5
v_date=`date +"%Y%m%d"`
v_country=Mexico

dateTime=`date`
echo "(${dateTime}): Start :$i"
sqls="./sqls/Commercial/${v_folder}"
    for i in $sqls/*.sql
    do
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b development_co_project < $i
       error=`echo $?`
       if [ $error -gt 0 ]; then
          exit 1
       fi		
	   
	   dateTime=`date`
	   echo "${dateTime} $i"
    done
exit;