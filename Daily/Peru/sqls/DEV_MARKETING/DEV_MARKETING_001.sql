

#create index customer on bob_live_pe.customer(id_customer);
#create index customers_rev_id on dev_marketing.customers_rev_pe(custid);
#create index custid on production_pe.tbl_order_detail(custid);

#New Version
select  'CRM Customers Revenue Table: start',now();

delete from dev_marketing.customers_rev_pe;

-- alter table dev_marketing.customers_rev_pe add column custid integer;

-- create index customers_rev_id on dev_marketing.customers_rev_pe(custid);

alter table dev_marketing.customers_rev_pe modify MONETARY_VALUE float;
alter table dev_marketing.customers_rev_pe modify Sending_Time integer;
alter table dev_marketing.customers_rev_pe modify RECENCY integer;
alter table dev_marketing.customers_rev_pe modify AV_TICKET float;

DROP TEMPORARY TABLE IF EXISTS dev_marketing.MONETARY_VALUE ; 
CREATE TEMPORARY TABLE dev_marketing.MONETARY_VALUE 
SELECT
c.created_at, 
c.first_name, 
c.last_name, 
c.id_customer, 
c.email, 
c.gender 
from bob_live_pe.customer c;

create index id on dev_marketing.MONETARY_VALUE(id_customer);

DROP TEMPORARY TABLE IF EXISTS dev_marketing.MONETARY_VALUE2 ; 
CREATE TEMPORARY TABLE dev_marketing.MONETARY_VALUE2
SELECT
i.custid as custid,
sum(i.paid_price) as MONETARY_VALUE 
from production_pe.tbl_order_detail i 
where i.oac=1 group by i.custid order by MONETARY_VALUE desc;

create index cust on dev_marketing.MONETARY_VALUE2(custid);

CREATE TEMPORARY TABLE dev_marketing.MONETARY_VALUE3
SELECT
dev_marketing.c.*,
dev_marketing.net.*
from dev_marketing.MONETARY_VALUE c 
left join dev_marketing.MONETARY_VALUE2 net 
on c.id_customer=net.custid;

insert into dev_marketing.customers_rev_pe(date_registred, first_name, last_name, custid, email, gender, MONETARY_VALUE)
select created_at, first_name, last_name, id_customer, email, gender, MONETARY_VALUE from dev_marketing.MONETARY_VALUE3;

update dev_marketing.customers_rev_pe set Source_data = 'Accoount_creation';

insert into dev_marketing.customers_rev_pe(date_registred, email, gender, Source_data)
select created_at, email, gender, source  from bob_live_pe.newsletter_subscription where fk_customer is null; 

update dev_marketing.customers_rev_pe set Source_data = CONCAT('Bob_newsletter ', Source_data) where Source_data not like 'Accoount_creation';

#insert into dev_marketing.customers_rev_pe (email) select email from dev_marketing.open_hour_crm_pe r where r.email not in (select email from dev_marketing.customers_rev_pe);  

INSERT INTO dev_marketing.customers_rev_pe (
	date_registred,
	email,
	Source_data
) SELECT
	Date,
	email,
	Source
FROM
	(
		SELECT
			A.*
		FROM
			CRM_bases.mailsPE A
		LEFT JOIN dev_marketing.customers_rev_pe B ON A.email = B.email
		WHERE
			B.email IS NULL
	) C;
	
INSERT INTO dev_marketing.customers_rev_pe (
	date_registred,
	email,
	Source_data,
	first_name,
	last_name,
	gender
) SELECT
	Date,
	email,
	Source,
	first_name,
	last_name,
	gender
FROM
	(
		SELECT
			A.*
		FROM
			CRM_bases.nuevosMailsPE A
		LEFT JOIN dev_marketing.customers_rev_pe B ON A.email = B.email
		WHERE
			B.email IS NULL
	) C;

alter table dev_marketing.customers_rev_pe modify MONETARY_VALUE varchar(100);

update dev_marketing.customers_rev_pe set MONETARY_VALUE = 'none' where MONETARY_VALUE is null;

update dev_marketing.customers_rev_pe set gender = 'neutral' where gender is null;

update dev_marketing.customers_rev_pe rev inner join production_pe.tbl_order_detail i on rev.custid=i.custid set TYPE_CUSTOMER= 'customer' where i.oac=1;

update dev_marketing.customers_rev_pe rev set TYPE_CUSTOMER= 'non customer' where TYPE_CUSTOMER is null;

#update dev_marketing.customers_rev_pe rev inner join dev_marketing.open_hour_crm_pe crm on rev.email=crm.email set Sending_Time= most_frequently_open_hour;

alter table dev_marketing.customers_rev_pe modify Sending_Time varchar(100);

update dev_marketing.customers_rev_pe set Sending_Time= 'never open/sent newsletter' where Sending_Time is null;

update dev_marketing.customers_rev_pe rev set RECENCY =
(select datediff(curdate(), max(date)) as days from production_pe.tbl_order_detail i where i.custid=rev.custid and i.oac=1 group by custid);

alter table dev_marketing.customers_rev_pe modify RECENCY varchar(100);

update dev_marketing.customers_rev_pe rev set RECENCY = 'never bought' where RECENCY is null;

update dev_marketing.customers_rev_pe rev set AV_TICKET= ( 
select avg(paid_price) from production_pe.tbl_order_detail i where oac=1 and rev.custid=i.custid group by custid);

alter table dev_marketing.customers_rev_pe modify AV_TICKET varchar(100);

update dev_marketing.customers_rev_pe rev set AV_TICKET= 'none' where AV_TICKET is null;

update dev_marketing.customers_rev_pe rev set FREQUENCY= (select datediff(max(date), min(date))/(count(distinct date)-1) as avg_time from production_pe.tbl_order_detail i where oac=1 and i.custid=rev.custid group by custid);

update dev_marketing.customers_rev_pe set FREQUENCY= 0 where FREQUENCY is null;

update dev_marketing.customers_rev_pe rev set LOCATION= ( 
select region from bob_live_pe.customer_address a where rev.custid=a.fk_customer group by fk_customer);

update dev_marketing.customers_rev_pe rev set LOCATION= 'unknown' where LOCATION is null;

update dev_marketing.customers_rev_pe rev set LOCATION= ( 
select region from production_pe.tbl_order_detail i where rev.custid=i.custid group by custid) where LOCATION='unknown' or location='';

update dev_marketing.customers_rev_pe rev set LOCATION= 'unknown' where LOCATION is null;

#UMS FORMAT
update dev_marketing.customers_rev_pe set MONETARY_VALUE = '0' where MONETARY_VALUE='none'; 
update dev_marketing.customers_rev_pe set Sending_Time= '100' where Sending_Time='never open/sent newsletter'; 
update dev_marketing.customers_rev_pe set RECENCY = '0' where RECENCY='never bought'; 
update dev_marketing.customers_rev_pe set AV_TICKET= '0' where AV_TICKET='none';

alter table dev_marketing.customers_rev_pe modify MONETARY_VALUE float;
alter table dev_marketing.customers_rev_pe modify Sending_Time integer;
alter table dev_marketing.customers_rev_pe modify RECENCY integer;
alter table dev_marketing.customers_rev_pe modify AV_TICKET float;
#UMS FORMAT

update dev_marketing.customers_rev_pe rev inner join bob_live_pe.customer_address c on rev.custid=c.fk_customer set rev.sms=c.mobile_phone;

update dev_marketing.customers_rev_pe rev inner join bob_live_pe.customer c on rev.custid=c.id_customer set rev.birthday=c.birthday;

update dev_marketing.customers_rev_pe rev set rev.new_registry = case when datediff(curdate(), date_registred)<30 then 1 else 0 end;

update dev_marketing.customers_rev_pe set new_registry = 0 where new_registry is null;

update dev_marketing.customers_rev_pe rev inner join production_pe.tbl_order_detail i on rev.custid=i.custid set rev.boughtcoupon= case when (select count(coupon_code) from production_pe.tbl_order_detail z where z.custid=rev.custid group by custid)>0 then 1 else 0 end, transactions = (select count(distinct order_nr) from production_pe.tbl_order_detail m where oac=1 and rev.custid=m.custid group by custid);

update dev_marketing.customers_rev_pe set boughtcoupon = 0 where boughtcoupon is null;

update dev_marketing.customers_rev_pe set transactions = 0 where transactions is null;

-- alter table dev_marketing.customers_rev_pe drop column custid;

alter ignore table dev_marketing.customers_rev_pe add primary key (email);

alter ignore table dev_marketing.customers_rev_pe drop primary key;

UPDATE dev_marketing.customers_rev_pe A
INNER JOIN bob_live_pe.newsletter_subscription B ON A.email = B.email
SET A.Source_data = B.source;


update dev_marketing.customers_rev_pe set Source_data = CONCAT('Bob_newsletter ', Source_data) where Source_data not like 'Accoount_creation';



UPDATE dev_marketing.customers_rev_pe A
INNER JOIN CRM_bases.mailsPE B ON A.email = B.email
SET A.Source_data = B.Source;

UPDATE dev_marketing.customers_rev_pe A
INNER JOIN CRM_bases.nuevosMailsPE B ON A.email = B.email
SET A.Source_data = B.Source;

DROP TEMPORARY TABLE IF EXISTS dev_marketing.TMP1 ; 
CREATE TEMPORARY TABLE dev_marketing.TMP1 
SELECT
email,code, sales_rule.is_active, to_date, used_discount_amount
from bob_live_pe.newsletter_subscription 
inner join bob_live_pe.sales_rule
on fk_sales_rule=id_sales_rule 
inner join bob_live_pe.sales_rule_set on
fk_sales_rule_set=id_sales_rule_set
where code like 'NL%';

create index email on dev_marketing.TMP1(email);

UPDATE dev_marketing.customers_rev_pe A
INNER JOIN TMP1
ON A.email = TMP1.email
SET A.Subscription_voucher = TMP1.code,
    A.days_to_inactive = datediff(TMP1.to_date,curdate()),
    A.used = CASE WHEN TMP1.used_discount_amount > 0 THEN 1 ELSE 0 END;

update dev_marketing.customers_rev_pe AS A 
INNER JOIN dev_marketing.TMP1
ON A.email = TMP1.email
SET A.active = CASE WHEN dev_marketing.TMP1.is_active= 1 AND A.used = 0 AND A.days_to_inactive <= 30 and A.days_to_inactive >= 0 THEN 1 ELSE 0 END;

update dev_marketing.customers_rev_pe rev 
set rev.new_registry_voucher = case when datediff(curdate(), date_registred)<=30 
and TYPE_CUSTOMER = 'non customer' 
and Subscription_voucher is not NULL
and active = 1
and days_to_inactive >= 2
and used = 0
then 1 else 0 end;

delete from dev_marketing.customers_rev_pe where ((email like 'test%' or email like '%test' or email like '%test%' or first_name like '%test%' or last_name like '%test%') and TYPE_CUSTOMER = 'non customer') or email like '%mailsolution%' or email like '%@bounceexchangetest.com%';

select  'CRM Customers Revenue Table: end',now();
#End New Version
