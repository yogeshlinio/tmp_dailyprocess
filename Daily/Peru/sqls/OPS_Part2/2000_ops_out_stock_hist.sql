/*
 Nombre: 2000_ops_out_stock_hist.sql
 Autor: Luis Ochoa, Rafael Guzman
 Fecha Creación:28/01/2014
 Descripción:
 Version: 1.0
 */
INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Peru', 
  'out_stock_hist',
  'start',
  NOW(),
  max(date_exit),
  count(*),
  count(item_counter)
FROM
  operations_pe.out_stock_hist;

TRUNCATE	operations_pe.out_stock_hist;


INSERT INTO operations_pe.out_stock_hist (
	stock_item_id,
	barcode_wms,
	date_entrance,
	barcode_bob_duplicated,
	in_stock,
	wh_location) 
SELECT
	estoque.estoque_id,
	estoque.cod_barras,
	date(data_criacao),
	estoque.minucioso,
	posicoes.participa_estoque,
	estoque.endereco
FROM
	(
		wmsprod_pe.estoque
		LEFT JOIN wmsprod_pe.itens_recebimento ON estoque.itens_recebimento_id = itens_recebimento.itens_recebimento_id
	)
LEFT JOIN wmsprod_pe.posicoes ON estoque.endereco = posicoes.posicao
;


UPDATE operations_pe.out_stock_hist
INNER JOIN wmsprod_pe.movimentacoes ON out_stock_hist.stock_item_id = movimentacoes.estoque_id
SET out_stock_hist.date_entrance = date(movimentacoes.data_criacao)
WHERE
	movimentacoes.de_endereco = 'retorno de cancelamento'
OR movimentacoes.de_endereco = 'vendidos';


TRUNCATE	operations_pe.pro_unique_ean_bob;


INSERT INTO operations_pe.pro_unique_ean_bob (ean) SELECT
	produtos.ean AS ean
FROM
	bob_live_pe.catalog_simple
INNER JOIN wmsprod_pe.produtos ON catalog_simple.sku = produtos.sku
WHERE
	catalog_simple. STATUS NOT LIKE "deleted"
GROUP BY
	produtos.ean
HAVING
	count(produtos.ean = 1);


UPDATE operations_pe.out_stock_hist
INNER JOIN wmsprod_pe.traducciones_producto ON out_stock_hist.barcode_wms = traducciones_producto.identificador
SET out_stock_hist.sku_simple = sku;


UPDATE operations_pe.out_stock_hist
INNER JOIN wmsprod_pe.estoque ON out_stock_hist.stock_item_id = estoque.estoque_id
SET out_stock_hist.date_exit = date(data_ultima_movimentacao),
 out_stock_hist.exit_type = "sold"
WHERE
	out_stock_hist.wh_location = "vendidos";


UPDATE operations_pe.out_stock_hist
INNER JOIN wmsprod_pe.estoque ON out_stock_hist.stock_item_id = estoque.estoque_id
SET out_stock_hist.date_exit = date(data_ultima_movimentacao),
 out_stock_hist.exit_type = "error"
WHERE
	out_stock_hist.wh_location = "error";


UPDATE operations_pe.out_stock_hist
INNER JOIN bob_live_pe.catalog_simple 
  ON out_stock_hist.sku_simple = catalog_simple.sku
INNER JOIN bob_live_pe.catalog_config 
  ON catalog_simple.fk_catalog_config = catalog_config.id_catalog_config
INNER JOIN bob_live_pe.catalog_shipment_type 
  ON catalog_simple.fk_catalog_shipment_type = catalog_shipment_type.id_catalog_shipment_type
INNER JOIN bob_live_pe.supplier ON catalog_config.fk_supplier = supplier.id_supplier
SET 
 out_stock_hist.barcode_bob = barcode_ean,
 out_stock_hist.sku_config = catalog_config.sku,
 out_stock_hist.sku_name = catalog_config. NAME,
 out_stock_hist.sku_supplier_config = catalog_config.sku_supplier_config,
 out_stock_hist.model = catalog_config.model,
 out_stock_hist.cost = catalog_simple.cost,
 out_stock_hist.supplier_name = supplier. NAME,
 out_stock_hist.supplier_id = supplier.id_supplier,
 out_stock_hist.fullfilment_type_bob = catalog_shipment_type. NAME,
 out_stock_hist.price = catalog_simple.price;


UPDATE operations_pe.out_stock_hist
INNER JOIN bob_live_pe.catalog_config ON out_stock_hist.sku_config = catalog_config.sku
INNER JOIN bob_live_pe.catalog_attribute_option_global_buyer ON catalog_attribute_option_global_buyer.id_catalog_attribute_option_global_buyer = catalog_config.fk_catalog_attribute_option_global_buyer
SET out_stock_hist.buyer = catalog_attribute_option_global_buyer. NAME;


UPDATE operations_pe.out_stock_hist
SET out_stock_hist.sold_last_30 = CASE
WHEN datediff(curdate(), date_exit) < 30 THEN
	1
ELSE
	0
END,
 out_stock_hist.sold_yesterday = CASE
WHEN datediff(
	curdate(),
	(
		date_sub(date_exit, INTERVAL 1 DAY)
	)
) = 0 THEN
	1
ELSE
	0
END,
 out_stock_hist.sold_last_10 = CASE
WHEN datediff(curdate(), date_exit) < 10 THEN
	1
ELSE
	0
END,
 out_stock_hist.sold_last_7 = CASE
WHEN datediff(curdate(), date_exit) < 7 THEN
	1
ELSE
	0
END
WHERE
	out_stock_hist.exit_type = "sold";


UPDATE operations_pe.out_stock_hist
SET out_stock_hist.entrance_last_30 = CASE
WHEN datediff(curdate(), date_entrance) < 30 THEN
	1
ELSE
	0
END,
 out_stock_hist.oms_po_last_30 = CASE
WHEN datediff(
	curdate(),
	date_procurement_order
) < 30 THEN
	1
ELSE
	0
END;


UPDATE operations_pe.out_stock_hist
SET out_stock_hist.days_in_stock = CASE
WHEN date_exit IS NULL THEN
	datediff(curdate(), date_entrance)
ELSE
	datediff(date_exit, date_entrance)
END;


UPDATE operations_pe.out_stock_hist
SET out_stock_hist.fullfilment_type_real = "inventory";


UPDATE operations_pe.out_stock_hist
INNER JOIN (
	wmsprod_pe.itens_venda
	INNER JOIN wmsprod_pe.status_itens_venda ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
) ON out_stock_hist.stock_item_id = itens_venda.estoque_id
SET out_stock_hist.fullfilment_type_real = "crossdock"
WHERE
	status_itens_venda. STATUS = "analisando quebra"
OR status_itens_venda. STATUS = "aguardando estoque";


UPDATE (
	operations_pe.out_stock_hist
	INNER JOIN wmsprod_pe.itens_venda ON out_stock_hist.stock_item_id = itens_venda.estoque_id
)
INNER JOIN wmsprod_pe.status_itens_venda ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
SET out_stock_hist.fullfilment_type_real = "dropshipping"
WHERE
	(
		(
			(status_itens_venda. STATUS) = "ds estoque reservado"
		)
	);


UPDATE operations_pe.out_stock_hist
INNER JOIN wmsprod_pe.movimentacoes ON out_stock_hist.stock_item_id = movimentacoes.estoque_id
SET out_stock_hist.fullfilment_type_real = 'inventory'
WHERE
	movimentacoes.de_endereco = 'retorno de cancelamento'
OR movimentacoes.de_endereco = 'vendidos';


UPDATE operations_pe.out_stock_hist
INNER JOIN bob_live_pe.catalog_simple ON out_stock_hist.sku_simple = catalog_simple.sku
INNER JOIN bob_live_pe.catalog_tax_class ON catalog_simple.fk_catalog_tax_class = catalog_tax_class.id_catalog_tax_class
SET out_stock_hist.tax_percentage = tax_percent,
 out_stock_hist.cost_w_o_vat = out_stock_hist.cost / (1 + tax_percent / 100);


UPDATE operations_pe.out_stock_hist
SET out_stock_hist.in_stock_cost_w_o_vat = out_stock_hist.cost_w_o_vat
WHERE
	out_stock_hist.in_stock = 1;


UPDATE operations_pe.out_stock_hist
SET out_stock_hist.sold_last_30_cost_w_o_vat = out_stock_hist.cost_w_o_vat,
 out_stock_hist.sold_last_30_price = out_stock_hist.price
WHERE
	out_stock_hist.sold_last_30 = 1;


DELETE operations_pe.pro_stock_hist_sku_count.*
FROM
	operations_pe.pro_stock_hist_sku_count;


INSERT INTO operations_pe.pro_stock_hist_sku_count (sku_simple, sku_count) SELECT
	out_stock_hist.sku_simple,
	count(*) AS sumofitem_counter
FROM
	operations_pe.out_stock_hist
GROUP BY
	out_stock_hist.sku_simple,
	out_stock_hist.in_stock
HAVING
	out_stock_hist.in_stock = 1;


UPDATE operations_pe.out_stock_hist
INNER JOIN operations_pe.pro_stock_hist_sku_count ON out_stock_hist.sku_simple = pro_stock_hist_sku_count.sku_simple
SET out_stock_hist.sku_counter = 1 / pro_stock_hist_sku_count.sku_count;


UPDATE operations_pe.out_stock_hist
SET out_stock_hist.reserved = '0';


UPDATE operations_pe.out_stock_hist
INNER JOIN wmsprod_pe.estoque ON out_stock_hist.stock_item_id = estoque.estoque_id
SET out_stock_hist.reserved = "1"
WHERE
	(
		(
			(estoque.almoxarifado) = "separando"
			OR (estoque.almoxarifado) = "estoque reservado"
			OR (estoque.almoxarifado) = "aguardando separacao"
		)
	);


UPDATE operations_pe.out_stock_hist
INNER JOIN bob_live_pe.catalog_simple ON out_stock_hist.sku_simple = catalog_simple.sku
SET out_stock_hist.supplier_leadtime = catalog_simple.delivery_time_supplier;


UPDATE operations_pe.out_stock_hist
INNER JOIN (
	(
		bob_live_pe.catalog_config
		INNER JOIN bob_live_pe.catalog_simple ON catalog_config.id_catalog_config = catalog_simple.fk_catalog_config
	)
	INNER JOIN bob_live_pe.catalog_stock ON catalog_simple.id_catalog_simple = catalog_stock.fk_catalog_simple
) ON out_stock_hist.sku_config = catalog_config.sku
SET out_stock_hist.visible = 1
WHERE
	(
		(
			(catalog_config.pet_status) = "creation,edited,images"
		)
		AND (
			(
				catalog_config.pet_approved
			) = 1
		)
		AND (
			(catalog_config. STATUS) = "active"
		)
		AND (
			(catalog_simple. STATUS) = "active"
		)
		AND ((catalog_simple.price) > 0)
		AND (
			(
				catalog_config.display_if_out_of_stock
			) = 0
		)
		AND ((catalog_stock.quantity) > 0)
		AND ((catalog_simple.cost) > 0)
	)
OR (
	(
		(catalog_config.pet_status) = "creation,edited,images"
	)
	AND (
		(
			catalog_config.pet_approved
		) = 1
	)
	AND (
		(catalog_config. STATUS) = "active"
	)
	AND (
		(catalog_simple. STATUS) = "active"
	)
	AND ((catalog_simple.price) > 0)
	AND (
		(
			catalog_config.display_if_out_of_stock
		) = 1
	)
	AND ((catalog_stock.quantity) > 0)
	AND ((catalog_simple.cost) > 0)
);


UPDATE operations_pe.out_stock_hist
SET out_stock_hist.week_exit = operations_pe.week_iso (date_exit),
out_stock_hist.week_entrance = operations_pe.week_iso (date_entrance);


DELETE operations_pe.pro_stock_hist_sold_last_30_count.*
FROM
	operations_pe.pro_stock_hist_sold_last_30_count;


INSERT INTO operations_pe.pro_stock_hist_sold_last_30_count (
	sold_last_30,
	sku_simple,
	count_of_item_counter
) SELECT
	out_stock_hist.sold_last_30,
	out_stock_hist.sku_simple,
	count(*) AS count_of_item_counter
FROM
	operations_pe.out_stock_hist
GROUP BY
	out_stock_hist.sold_last_30,
	out_stock_hist.sku_simple
HAVING
	out_stock_hist.sold_last_30 = 1;


UPDATE operations_pe.out_stock_hist
INNER JOIN operations_pe.pro_stock_hist_sold_last_30_count ON out_stock_hist.sku_simple = pro_stock_hist_sold_last_30_count.sku_simple
SET out_stock_hist.sold_last_30_counter = 1 / pro_stock_hist_sold_last_30_count.count_of_item_counter;


UPDATE operations_pe.out_stock_hist
SET out_stock_hist.sku_simple_blank = 1
WHERE
	out_stock_hist.sku_simple IS NULL;


UPDATE (
	wmsprod_pe.itens_recebimento
	INNER JOIN wmsprod_pe.estoque ON itens_recebimento.itens_recebimento_id = estoque.itens_recebimento_id
)
INNER JOIN operations_pe.out_stock_hist ON estoque.estoque_id = out_stock_hist.stock_item_id
SET out_stock_hist.quarantine = 1
WHERE
	itens_recebimento.endereco = "cuarentena"
OR itens_recebimento.endereco = "cuarenta2";


DELETE operations_pe.pro_sum_last_5_out_of_6_wks.*
FROM
	operations_pe.pro_sum_last_5_out_of_6_wks;


INSERT INTO operations_pe.pro_sum_last_5_out_of_6_wks (sku_simple, expr1) SELECT
	max_last_6_wks.sku_simple,
	sumofcountofdate_exit - maxofcountofdate_exit AS expr1
FROM
	operations_pe.max_last_6_wks
GROUP BY
	max_last_6_wks.sku_simple,
	sumofcountofdate_exit - maxofcountofdate_exit;


UPDATE operations_pe.out_stock_hist
INNER JOIN operations_pe.pro_sum_last_5_out_of_6_wks ON out_stock_hist.sku_simple = pro_sum_last_5_out_of_6_wks.sku_simple
SET out_stock_hist.sum_sold_last_5_out_of_6_wks = pro_sum_last_5_out_of_6_wks.expr1;


UPDATE operations_pe.out_stock_hist
INNER JOIN bob_live_pe.catalog_config ON out_stock_hist.sku_config = catalog_config.sku
SET out_stock_hist.package_height = catalog_config.package_height,
 out_stock_hist.package_length = catalog_config.package_length,
 out_stock_hist.package_width = catalog_config.package_width;


UPDATE operations_pe.out_stock_hist
SET out_stock_hist.vol_weight = package_height * package_length * package_width / 5000;


UPDATE operations_pe.out_stock_hist
INNER JOIN bob_live_pe.catalog_config ON out_stock_hist.sku_config = catalog_config.sku
SET out_stock_hist.package_weight = catalog_config.package_weight;


UPDATE operations_pe.out_stock_hist
SET out_stock_hist.max_vol_w_vs_w = CASE
WHEN vol_weight > package_weight THEN
	vol_weight
ELSE
	package_weight
END;


UPDATE operations_pe.out_stock_hist
SET out_stock_hist.package_measure = CASE
WHEN max_vol_w_vs_w > 45.486 THEN
	"large"
ELSE
	(
		CASE
		WHEN max_vol_w_vs_w > 2.277 THEN
			"medium"
		ELSE
			"small"
		END
	)
END;

UPDATE operations_pe.out_stock_hist
SET out_stock_hist.package_measure_new = 	CASE
																						WHEN max_vol_w_vs_w > 35 
																						THEN 'oversized'
																					ELSE
																					(	CASE
																							WHEN max_vol_w_vs_w > 5 
																							THEN 'large'
																							ELSE
																							(	CASE
																									WHEN max_vol_w_vs_w > 2 
																									THEN 'medium'
																									ELSE 'small'
																								END)
																							END)
																					END; 


UPDATE operations_pe.out_stock_hist
INNER JOIN wmsprod_pe.itens_inventario ON out_stock_hist.barcode_wms = itens_inventario.cod_barras
SET out_stock_hist.sub_position = sub_endereco;


UPDATE wmsprod_pe.posicoes
INNER JOIN operations_pe.out_stock_hist ON posicoes.posicao = out_stock_hist.wh_location
SET out_stock_hist.position_type = posicoes.tipo_posicao;


UPDATE operations_pe.out_stock_hist
SET out_stock_hist.position_item_size_type = CASE
WHEN position_type = "safe" THEN
	"small"
ELSE
	(
		CASE
		WHEN position_type = "mezanine" THEN
			"small"
		ELSE
			(
				CASE
				WHEN position_type = "muebles" THEN
					"large"
				ELSE
					"tbd"
				END
			)
		END
	)
END;


DELETE
FROM
	operations_pe.items_procured_in_transit;


INSERT INTO operations_pe.items_procured_in_transit (
	sku_simple,
	number_ordered,
	unit_price
) SELECT
	catalog_simple.sku,
	count(
		procurement_order_item.id_procurement_order_item
	) AS countofid_procurement_order_item,
	avg(
		procurement_order_item.unit_price
	) AS avgofunit_price
FROM
	bob_live_pe.catalog_simple
INNER JOIN (
	procurement_live_pe.procurement_order_item
	INNER JOIN procurement_live_pe.procurement_order ON procurement_order_item.fk_procurement_order = procurement_order.id_procurement_order
) ON catalog_simple.id_catalog_simple = procurement_order_item.fk_catalog_simple
WHERE
	(
		(
			(
				procurement_order_item.is_deleted
			) = 0
		)
		AND (
			(
				procurement_order.is_cancelled
			) = 0
		)
		AND (
			(
				procurement_order_item.sku_received
			) = 0
		)
	)
GROUP BY
	catalog_simple.sku;


UPDATE operations_pe.out_stock_hist
INNER JOIN operations_pe.items_procured_in_transit ON out_stock_hist.sku_simple = items_procured_in_transit.sku_simple
SET out_stock_hist.items_procured_in_transit = number_ordered,
 out_stock_hist.procurement_price = unit_price;


UPDATE (
	procurement_live_pe.procurement_order
	INNER JOIN (
		(
			bob_live_pe.catalog_simple
			INNER JOIN operations_pe.out_stock_hist ON catalog_simple.sku = out_stock_hist.sku_simple
		)
		INNER JOIN procurement_live_pe.procurement_order_item ON catalog_simple.id_catalog_simple = procurement_order_item.fk_catalog_simple
	) ON procurement_order.id_procurement_order = procurement_order_item.fk_procurement_order
)
SET out_stock_hist.date_procurement_order = procurement_order.created_at;


UPDATE operations_pe.out_stock_hist
SET out_stock_hist.sold = 1
WHERE
	out_stock_hist.exit_type = "sold";


TRUNCATE
	operations_pe.sell_rate_simple;


INSERT INTO operations_pe.sell_rate_simple (
	sku_simple,
	num_items,
	num_sales,
	average_days_in_stock
) SELECT
	sku_simple,
	count(*),
	sum(sold),
	avg(days_in_stock)
FROM
	operations_pe.out_stock_hist
GROUP BY
	sku_simple;


UPDATE operations_pe.sell_rate_simple srs
SET srs.num_items_available = (
	SELECT
		sum(in_stock)
	FROM
		operations_pe.out_stock_hist osh
	WHERE
		reserved = '0'
	AND srs.sku_simple = osh.sku_simple
	GROUP BY
		sku_simple
);


UPDATE operations_pe.sell_rate_simple
SET sell_rate_simple.average_sell_rate = CASE
WHEN average_days_in_stock = 0 THEN
	0
ELSE
	num_sales / average_days_in_stock
END;


UPDATE operations_pe.sell_rate_simple
SET sell_rate_simple.remaining_days = CASE
WHEN average_sell_rate = 0 THEN
	0
ELSE
	num_items_available / average_sell_rate
END;


		TRUNCATE operations_pe.sell_rate_config ; 


			INSERT INTO operations_pe.sell_rate_config (
				sku_config,
				num_items,
				num_sales
			) SELECT
				sku_config,
				count(*),
				sum(sold)
			FROM
				operations_pe.out_stock_hist
			WHERE
				datediff(curdate(), date_exit) <= 42
			GROUP BY
				sku_config ; 


				UPDATE operations_pe.sell_rate_config a
			INNER JOIN (
				SELECT
					sku_config,
					avg(days_in_stock) avg_days
				FROM
					operations_pe.out_stock_hist
				WHERE
					in_stock = 1
				AND reserved = 0
				GROUP BY
					sku_config
			) b ON a.sku_config = b.sku_config
			SET a.average_days_in_stock = CASE
			WHEN b.avg_days >= 42 THEN
				42
			ELSE
				b.avg_days
			END ; 


			UPDATE operations_pe.sell_rate_config a
			SET a.num_items_available = (
				SELECT
					sum(b.in_stock)
				FROM
					operations_pe.out_stock_hist b
				WHERE
					b.reserved = '0'
				AND a.sku_config = b.sku_config
				GROUP BY
					sku_config
			) ; 


			UPDATE operations_pe.sell_rate_config a
			SET a.average_sell_rate = CASE
			WHEN a.average_days_in_stock = 0 THEN
				0
			ELSE
				a.num_sales / a.average_days_in_stock
			END ; 


			UPDATE operations_pe.sell_rate_config
			SET sell_rate_config.remaining_days = CASE
			WHEN average_sell_rate = 0 THEN
				0
			ELSE
				num_items_available / average_sell_rate
			END ;


UPDATE operations_pe.out_stock_hist
INNER JOIN operations_pe.sell_rate_config ON out_stock_hist.sku_config = sell_rate_config.sku_config
SET out_stock_hist.average_remaining_days = sell_rate_config.remaining_days;


DELETE
FROM
	operations_pe.pro_max_days_in_stock;


				INSERT INTO operations_pe.pro_max_days_in_stock (
					sku_config,
					max_days_in_stock,
					min_days_in_stock
				) SELECT
					sku_config,
					max(days_in_stock),
					min(days_in_stock)
				FROM
					operations_pe.out_stock_hist
				WHERE
					in_stock = 1
				AND reserved = 0
				GROUP BY
					sku_config ; 


					UPDATE operations_pe.out_stock_hist
				INNER JOIN operations_pe.pro_max_days_in_stock ON out_stock_hist.sku_config = pro_max_days_in_stock.sku_config
				SET out_stock_hist.max_days_in_stock = pro_max_days_in_stock.max_days_in_stock,
				out_stock_hist.min_days_in_stock = pro_max_days_in_stock.min_days_in_stock ;

UPDATE operations_pe.out_stock_hist
SET out_stock_hist.week_payable_promised = operations_pe.week_iso (date_payable_promised);


UPDATE operations_pe.out_stock_hist
SET out_stock_hist.week_procurement_order = operations_pe.week_iso (date_procurement_order);


UPDATE operations_pe.out_stock_hist
INNER JOIN bob_live_pe.catalog_simple ON out_stock_hist.sku_simple = catalog_simple.sku
SET out_stock_hist.delivery_cost_supplier = catalog_simple.delivery_cost_supplier;


UPDATE operations_pe.out_stock_hist
SET cogs = cost_w_o_vat + delivery_cost_supplier;


UPDATE operations_pe.out_stock_hist
SET out_stock_hist.days_payable_since_entrance = CASE
WHEN date_entrance IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN oms_payment_event IN ('factura', 'pedido') THEN
			oms_payment_terms - supplier_leadtime
		ELSE
			(
				CASE
				WHEN oms_payment_event = 'entrega' THEN
					oms_payment_terms
				END
			)
		END
	)
END;


UPDATE operations_pe.out_stock_hist a
INNER JOIN bob_live_pe.catalog_config b ON a.sku_config = b.sku
INNER JOIN bob_live_pe.catalog_attribute_option_global_category c ON b.fk_catalog_attribute_option_global_category = c.id_catalog_attribute_option_global_category
INNER JOIN bob_live_pe.catalog_attribute_option_global_sub_category d ON b.fk_catalog_attribute_option_global_sub_category = d.id_catalog_attribute_option_global_sub_category
INNER JOIN bob_live_pe.catalog_attribute_option_global_sub_sub_category e ON b.fk_catalog_attribute_option_global_sub_sub_category = e.id_catalog_attribute_option_global_sub_sub_category
SET a.category_com_main = c. NAME,
 a.category_com_sub = d. NAME,
 a.category_com_sub_sub = e. NAME;


UPDATE operations_pe.out_stock_hist a
INNER JOIN development_mx.A_E_M1_New_CategoryBP b ON a.category_com_sub = b.cat2
SET a.category_bp = b.CatBP ; 


UPDATE operations_pe.out_stock_hist a
INNER JOIN development_mx.A_E_M1_New_CategoryBP b ON a.category_com_main = b.cat1
SET a.category_bp = b.CatBP
WHERE
	a.category_bp IS NULL ; 


UPDATE operations_pe.out_stock_hist
SET fullfilment_type_bp = CASE
WHEN fullfilment_type_real = 'crossdock' THEN
	'crossdocking'
WHEN fullfilment_type_real = 'inventory'
AND fullfilment_type_bob = 'consignment' THEN
	'consignment'
WHEN fullfilment_type_real = 'inventory'
AND fullfilment_type_bob <> 'consignment' THEN
	'outright buying'
WHEN fullfilment_type_real = 'dropshipping' THEN
	'other'
END;



UPDATE operations_pe.out_stock_hist set is_sell_list = 1
WHERE max_days_in_stock > 30
and in_stock= 1
and reserved= 0
and fullfilment_type_bob <> 'Consignment' 
and fullfilment_type_bob is not null
and category_com_main = 'Home and Living' 
and (average_remaining_days > 150
or average_remaining_days is null);

UPDATE operations_pe.out_stock_hist set is_sell_list = 1
where max_days_in_stock > 30
and in_stock= 1
and reserved= 0
and fullfilment_type_bob <> 'Consignment' 
and fullfilment_type_bob is not null
and category_com_main = 'Fashion' 
and (average_remaining_days > 120
or average_remaining_days is null);

UPDATE operations_pe.out_stock_hist set is_sell_list = 1
where max_days_in_stock > 30
and in_stock= 1
and reserved= 0
and fullfilment_type_bob <> 'Consignment' 
and fullfilment_type_bob is not null
and category_com_main = 'Electrónicos' 
and (average_remaining_days > 60
or average_remaining_days is null);

UPDATE operations_pe.out_stock_hist set is_sell_list = 1
where max_days_in_stock > 30
and in_stock= 1
and reserved= 0
and fullfilment_type_bob <> 'Consignment' 
and fullfilment_type_bob is not null
and category_com_main not in ('Home and Living','Fashion','Electrónicos') 
and (average_remaining_days > 90
or average_remaining_days is null);


SET @i = 0 ; 
DROP TEMPORARY TABLE IF EXISTS TMP ; 
CREATE TEMPORARY TABLE TMP 
SELECT
	sku_config,
	Count(*) AS items_in_stock,
	sum(cost_w_o_vat) AS cost_w_o_vat_in_stock
FROM
	operations_pe.out_stock_hist
WHERE
	in_stock = 1
AND reserved = 0
AND (
			fullfilment_type_bob <> 'consignment'
			OR fullfilment_type_bob IS NOT NULL
		)
GROUP BY
	sku_config;

DROP TABLE IF EXISTS operations_pe.top_skus_sell_list ; 
CREATE TABLE operations_pe.top_skus_sell_list 
SELECT
	sku_config,
	items_in_stock,
	cost_w_o_vat_in_stock,
	@i := @i + 1 AS rkn
FROM
	TMP
ORDER BY
	cost_w_o_vat_in_stock DESC ;

UPDATE operations_pe.out_stock_hist
SET item_counter = 1;

# Sell List

INSERT INTO production_pe.pro_sell_list_hist_totals (
	date_reported,
	category_bp,
	cost_w_o_vat_over_two_items,
	cost_w_o_vat_under_two_items) 
SELECT
	J.date_reported,
	J.Categoria,
	J.Value_more_than_two,
	J.Value_less_than_two
FROM
	(	SELECT
			curdate() AS date_reported,
			B.category_bp AS 'Categoria',
			B.cost_w_o_vat AS Value_more_than_two,
			D.cost_w_o_vat AS Value_less_than_two
		FROM
			(	SELECT
					category_bp,
					sum(cost_w_o_vat) AS cost_w_o_vat
				FROM
					(	SELECT
							category_bp,
							sku_config,
							sum(cost_w_o_vat) AS cost_w_o_vat
						FROM
							production_pe.out_stock_hist
						WHERE
							is_sell_list = 1
						AND category_bp IS NOT NULL
						GROUP BY
							sku_config
						HAVING
							count(sku_config) > 2
					) A
				GROUP BY
					category_bp
			) B
		LEFT JOIN (	SELECT
									category_bp,
									sum(cost_w_o_vat) AS cost_w_o_vat
								FROM
									(	SELECT
											category_bp,
											sum(cost_w_o_vat) AS cost_w_o_vat
										FROM
											production_pe.out_stock_hist
										WHERE
											is_sell_list = 1
										GROUP BY
											sku_config
										HAVING
											count(sku_config) <= 2
									) C
								GROUP BY
									category_bp
									) D 
		ON B.category_bp = D.category_bp
	) J;


DROP TABLE IF EXISTS production_pe.out_stock_hist;

CREATE TABLE production_pe.out_stock_hist LIKE operations_pe.out_stock_hist; 

INSERT INTO production_pe.out_stock_hist SELECT * FROM operations_pe.out_stock_hist;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Peru', 
  'out_stock_hist',
  'finish',
  NOW(),
  max(date_exit),
  count(*),
  count(item_counter)
FROM
  operations_pe.out_stock_hist;

DROP TABLE IF EXISTS production_pe.items_procured_in_transit;

CREATE TABLE production_pe.items_procured_in_transit LIKE operations_pe.items_procured_in_transit;

INSERT INTO production_pe.items_procured_in_transit SELECT * FROM operations_pe.items_procured_in_transit; 


DROP TABLE IF EXISTS production_pe.pro_sell_list_hist_totals;

CREATE TABLE production_pe.pro_sell_list_hist_totals LIKE operations_pe.pro_sell_list_hist_totals;

INSERT INTO production_pe.pro_sell_list_hist_totals SELECT * FROM operations_pe.pro_sell_list_hist_totals;

DROP TABLE IF EXISTS production_pe.top_skus_sell_list;

CREATE TABLE production_pe.top_skus_sell_list LIKE operations_pe.top_skus_sell_list;

INSERT INTO production_pe.top_skus_sell_list SELECT * FROM operations_pe.top_skus_sell_list;

