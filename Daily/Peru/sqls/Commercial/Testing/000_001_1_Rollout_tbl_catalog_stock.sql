DROP TABLE IF EXISTS tbl_catalog_product_stock;
CREATE TABLE `tbl_catalog_product_stock` (
  `fk_catalog_simple` int(11) NOT NULL,
  `sku` varchar(25) NOT NULL,
  `reservedbob` int(11) DEFAULT '0',
  `ownstock` int(11) DEFAULT '0',
  `supplierstock` int(11) DEFAULT '0',
  `stockbob` int(11) DEFAULT '0',
  `availablebob` int(11) DEFAULT '0',
  `stock_wms` int(11) DEFAULT '0',
  `stock_wms_reserved` int(11) DEFAULT '0',
  `share_reserved_bob` decimal(10,6) DEFAULT NULL,
  `share_stock_bob` decimal(10,6) DEFAULT NULL,
  `inv_age` decimal(10,6) DEFAULT '0.000000',
  PRIMARY KEY (`fk_catalog_simple`),
  UNIQUE KEY `sku_UNIQUE` (`sku`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
;