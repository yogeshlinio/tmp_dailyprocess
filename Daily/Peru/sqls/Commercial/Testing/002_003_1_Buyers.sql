/*
*   UPDATE A_Master_Catalog.buyer 
*   UPDATE A_Master_Catalog.head_buyer 
*/         
UPDATE           A_Master_Catalog_Sample         
      INNER JOIN bob_live_pe.catalog_attribute_option_global_buyer
           USING ( id_catalog_attribute_option_global_buyer )
SET
    A_Master_Catalog_Sample.Head_Buyer = catalog_attribute_option_global_buyer.name,            
    A_Master_Catalog_Sample.Buyer      = catalog_attribute_option_global_buyer.name;            
