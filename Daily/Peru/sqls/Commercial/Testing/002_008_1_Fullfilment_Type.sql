UPDATE        A_Master_Catalog_Sample
  INNER JOIN bob_live_pe.catalog_shipment_type
       USING ( id_catalog_shipment_type )
SET
	A_Master_Catalog_Sample.fulfillment_type = catalog_shipment_type.name  
;