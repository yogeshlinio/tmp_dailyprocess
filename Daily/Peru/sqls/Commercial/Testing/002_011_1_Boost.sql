UPDATE            A_Master_Catalog_Sample t
             JOIN bob_live_pe.catalog_simple
               ON t.id_catalog_simple=catalog_simple.id_catalog_simple
             JOIN bob_live_pe.catalog_config
               ON catalog_simple.fk_catalog_config = catalog_config.id_catalog_config
        INNER JOIN bob_live_pe.catalog_product_boost
               ON bob_live_pe.catalog_product_boost.fk_catalog_config = bob_live_pe.catalog_config.id_catalog_config
SET 
    t.boost=bob_live_pe.catalog_product_boost.boost;