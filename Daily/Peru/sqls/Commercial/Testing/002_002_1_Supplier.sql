/*
* UPDATE A_Master_Catalog.supplier
*/          
UPDATE            A_Master_Catalog_Sample
       INNER JOIN bob_live_pe.catalog_supplier 
               ON catalog_supplier.id_catalog_supplier = A_Master_Catalog_Sample.id_supplier
SET
    A_Master_Catalog_Sample.Supplier = catalog_supplier.name_en
;
UPDATE            A_Master_Catalog_Sample
       INNER JOIN bob_live_pe.supplier 
                ON A_Master_Catalog_Sample.id_supplier = supplier.id_supplier
SET
    A_Master_Catalog_Sample.Supplier        = supplier.name,
    A_Master_Catalog_Sample.delivery_type   = supplier.type_delivery,
	A_Master_Catalog_Sample.supplier_type   = supplier.type,
  	A_Master_Catalog_Sample.Products_origin = supplier.products_origin	
;


