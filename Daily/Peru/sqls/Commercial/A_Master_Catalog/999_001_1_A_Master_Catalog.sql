DROP TABLE IF EXISTS A_Master_Catalog;
CREATE TABLE A_Master_Catalog LIKE A_Master_Catalog_Template;
INSERT A_Master_Catalog SELECT * FROM A_Master_Catalog_Sample;


DROP TABLE IF EXISTS A_Master_Catalog_Sample_Backup;
CREATE TABLE A_Master_Catalog_Sample_Backup LIKE A_Master_Catalog_Template;
INSERT A_Master_Catalog_Sample_Backup SELECT * FROM A_Master_Catalog;
