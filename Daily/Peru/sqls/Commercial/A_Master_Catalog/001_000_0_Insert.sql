DROP TABLE IF EXISTS A_Master_Catalog_Sample;

CREATE TABLE  A_Master_Catalog_Sample LIKE A_Master_Catalog_Template;
INSERT  A_Master_Catalog_Sample 
(
   `id_catalog_config`,
   `id_catalog_simple`,
   `sku_simple`,
   `sku_config`,
   `sku_name`,
   `id_catalog_attribute_option_global_category`,
   `id_catalog_attribute_option_global_sub_category`,
   `id_catalog_attribute_option_global_sub_sub_category`,
   
   `Product_Weight`,
   `Package_Weight`,
   `Package_Height`,
   `Package_Length`,
   `Package_Width`,
   `Product_Measures`,
   
   `id_supplier`,
   `id_catalog_attribute_option_global_buyer`,
   `id_catalog_brand`,
   `model`,
   `color`,   
   `barcode_ean`,

   `cost`,
   `cost_oms`,   
   `delivery_cost_supplier`,
   `eligible_free_shipping`,

   `original_price` ,
   `special_price` , 
   `special_from_date` ,   
   `special_to_date`  ,   
   
   `Price`,
   `price_comparison`,
   `special_purchase_price`,     
   `fixed_price`,

   `isActive_SKUConfig`,
   `isActive_SKUSimple`,
   `display_if_out_of_stock`,
   `pet_status`,
   `pet_approved`,
   `status_config`,
   `status_simple`,

	 `isMarketPlace`,

  `created_at_Config` ,
  `created_at_Simple` ,
  `id_catalog_tax_class` ,
  `id_catalog_shipment_type`,

	`min_delivery_time`,
	`max_delivery_time`,

	`promise_delivery_badge`,
	`delivery_time_supplier`


)
SELECT
   catalog_config.id_catalog_config,
   catalog_simple.id_catalog_simple,
   catalog_simple.sku AS sku_simple, 
   catalog_config.sku AS sku_config,
   catalog_config.name as sku_name,

   fk_catalog_attribute_option_global_category AS id_catalog_attribute_option_global_category,
   fk_catalog_attribute_option_global_sub_category AS id_catalog_attribute_option_global_sub_category,
   fk_catalog_attribute_option_global_sub_sub_category AS id_catalog_attribute_option_global_sub_sub_category,
   catalog_config.product_weight   AS `Product_Weight`,
   catalog_config.package_weight   AS `Package_Weight`,
   catalog_config.package_height   AS `Package_Height`,
   catalog_config.package_length   AS `Package_Length`,
   catalog_config.package_width    AS `Package_Width`,
   catalog_config.product_measures AS `Product_Measures`,
   catalog_source.fk_supplier      AS `id_supplier`,
   catalog_config.fk_catalog_attribute_option_global_buyer AS id_catalog_attribute_option_global_buyer,
   catalog_config.fk_catalog_brand AS `id_catalog_brand` ,
   catalog_config.model AS model,    
   catalog_config.color AS color,   
   catalog_source.barcode_ean,
   
   catalog_simple.cost                   AS `cost`,
   catalog_simple.cost                   AS `cost_oms`,   
   catalog_simple.delivery_cost_supplier AS delivery_cost_supplier,
   catalog_simple.eligible_free_shipping AS `eligible_free_shipping`,
   catalog_simple.original_price         AS `original_price`,
   catalog_simple.special_price          AS `special_price`,   
   catalog_simple.special_from_date      AS `special_from_date`,   
   catalog_simple.special_to_date        AS `special_to_date`,      

   catalog_simple.price                  AS `Price`,
   catalog_config.price_comparison       AS `price_comparison`,
   catalog_simple.specialpurchaseprice   AS `special_purchase_price`,   
	 1         AS `fixed_price`,
   if( catalog_config.status = "active" , 1 , 0 ) as isActive_SKUConfig,
   if( catalog_simple.status = "active" , 1 , 0 ) as isActive_SKUSimple,
   catalog_config.display_if_out_of_stock AS display_if_out_of_stock,
	 catalog_config.pet_status             AS `pet_status`,
	 catalog_config.pet_approved           AS `pet_approved` ,
	 catalog_config.status                 AS `status_config` ,
	 catalog_simple.status                 AS `status_simple`,
	 catalog_simple.is_option_marketplace  AS `is_marketplace`,

   catalog_config.created_at AS  created_at_config ,
   catalog_simple.created_at AS  created_at_simple ,
   catalog_simple.fk_catalog_tax_class AS id_catalog_tax_class,
   catalog_source.fk_catalog_shipment_type AS id_catalog_shipment_type,

	 catalog_simple.min_delivery_time AS `min_delivery_time`,
	 catalog_simple.max_delivery_time AS `max_delivery_time`,
	 catalog_simple.promise_delivery_badge AS `promise_delivery_badge`,
	 catalog_simple.delivery_time_supplier AS `delivery_time_supplier`

FROM
		          bob_live_pe.catalog_source
  INNER JOIN `bob_live_pe`.`catalog_simple`
          ON catalog_source.fk_catalog_simple = catalog_simple.id_catalog_simple
	INNER JOIN `bob_live_pe`.`catalog_config`
	        ON `id_catalog_config` = `fk_catalog_config` 
where id_catalog_simple != 158044
;