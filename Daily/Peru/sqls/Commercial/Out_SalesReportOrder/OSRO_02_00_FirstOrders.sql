USE development_pe;
DROP TABLE IF EXISTS A_E_BI_First_Orders;
CREATE TABLE A_E_BI_First_Orders (  PRIMARY KEY ( OrderNum , id ), INDEX( CustomerNum ), id int auto_increment )
SELECT
  CustomerNum,
  OrderNum,
  NULL AS id,
  Month_Num,
  DATE
  OrderAfterCan,
  OrderBeforeCan,
  Grand_Total
FROM  
  Out_SalesReportOrder
WHERE  
  OrderAfterCan = 1
ORDER BY 
CustomerNum , Date asc;
DELETE FROM A_E_BI_First_Orders WHERE id > 1 ;

UPDATE            A_E_BI_First_Orders
       INNER JOIN Out_SalesReportOrder USING( CustomerNum )
SET
  First_Net_Order  = A_E_BI_First_Orders.OrderNum,
  Net_Cohort_Month = A_E_BI_First_Orders.Month_Num,
  New_Returning    = IF( A_E_BI_First_Orders.OrderNum = Out_SalesReportOrder.OrderNum, "NEW", "RETURNING"  )
#If fisrt_net_order = orderNum -> NEW else RETURNING
;


DROP TABLE IF EXISTS A_E_BI_First_Orders;
CREATE TABLE A_E_BI_First_Orders (  PRIMARY KEY ( OrderNum , id ), INDEX( CustomerNum ), id int auto_increment )
SELECT
  CustomerNum,
  OrderNum,
  NULL AS id,
  Month_Num,
  DATE
  OrderAfterCan,
  OrderBeforeCan,
  Grand_Total
FROM  
  Out_SalesReportOrder
WHERE  
  OrderBeforeCan = 1
ORDER BY 
CustomerNum , Date asc;
DELETE FROM A_E_BI_First_Orders WHERE id > 1 ;

UPDATE            A_E_BI_First_Orders
       INNER JOIN Out_SalesReportOrder USING( CustomerNum )
SET
  First_Gross_Order  = A_E_BI_First_Orders.OrderNum,
  Gross_Cohort_Month = A_E_BI_First_Orders.Month_Num;

DROP TABLE IF EXISTS A_E_BI_First_Orders;
CREATE TABLE A_E_BI_First_Orders (  PRIMARY KEY ( OrderNum , id ), INDEX( CustomerNum ), id int auto_increment )
SELECT
  CustomerNum,
  OrderNum,
  NULL AS id,
  Month_Num,
  DATE
  OrderAfterCan,
  OrderBeforeCan,
  Grand_Total
FROM  
  Out_SalesReportOrder
WHERE  
  OrderAfterCan = 1 OR Returns = 1
ORDER BY 
CustomerNum , Date asc;
DELETE FROM A_E_BI_First_Orders WHERE id > 1 ;

UPDATE            A_E_BI_First_Orders
       INNER JOIN Out_SalesReportOrder USING( CustomerNum )
SET
  First_Net_Return_Order  = A_E_BI_First_Orders.OrderNum
;





/*
  null as `First_Net_Order` , #Find OAC,
  null as `First_Gross_Order`, #find OBC
  null as `Net_Cohort_Month` , #MonthNum's first order
  null as `Gross_Cohort_Month` ,
*/
