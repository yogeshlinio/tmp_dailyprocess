#Query: N1:
drop table if EXISTS development_pe.CACcustomer;
create table development_pe.CACcustomer
(
 `CustomerNum` VARCHAR (20) NOT NULL,
 `FirstNetOrder` VARCHAR (20) NOT NULL,
 `FirstGrossOrder` VARCHAR (20) NOT NULL,
 `CohortMonth` INT NOT NULL,
 `Campaign` VARCHAR (50) NOT NULL,
 `ChannelGroup` VARCHAR (36) NOT NULL,
 `Channel` VARCHAR(36) NOT NULL,
 `SourceMedium` VARCHAR (36) NOT NULL
) 
SELECT
  DISTINCT CustomerNum AS CustomerNum,
           FirstOrderNum AS FirstNetOrder,
           CohortMonthNum AS CohortMonth,
           Campaign AS Campaign,
           Channelgroup AS ChannelGroup ,
           Source_medium AS SourceMedium 
from development_pe.A_Master
where FirstOrderNum = OrderNum
 and Channelgroup = 'CAC Deals'
;

create index CustomerNum on development_pe.CACcustomer(CustomerNum);

UPDATE development_pe.A_Master INNER JOIN development_pe.CACcustomer on 
A_Master.CustomerNum = CACcustomer.CustomerNum
SET 
    A_Master.CACCustomer = 1;

UPDATE development_pe.A_Master set CACCustomer=0 where CACCustomer is null;