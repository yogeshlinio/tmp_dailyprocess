INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Peru', 
  'DailyReport',
  'finish',
  NOW(),
  MAX(`Date Placed`),
  count(*),
  count(*)
FROM
   development_pe.DailyReport 
;


