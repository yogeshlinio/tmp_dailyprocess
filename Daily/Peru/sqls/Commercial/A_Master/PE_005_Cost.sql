USE development_pe;
/*
*  Costo de BOB
*/
UPDATE        A_Master_Sample
   INNER JOIN A_Master_Catalog
           ON A_Master_Sample.SKUSimple = A_Master_Catalog.SKU_simple
SET
   A_Master_Sample.Cost         = A_Master_Catalog.cost ,
   A_Master_Sample.CostAfterTax = A_Master_Catalog.cost  / 
                                                 ( 1 +  ( A_Master_Sample.TaxPercent / 100 ) )                  

WHERE
      A_Master_Sample.Cost is null 
   OR A_Master_Sample.Cost = 0
;
   
/*
* Costo Promedio
*/      
UPDATE        A_Master_Sample
   INNER JOIN A_Master_Catalog p
           ON A_Master_Sample.SKUSimple= p.sku_simple 
SET 
   A_Master_Sample.Cost = p.Cost_OMS  ,
   A_Master_Sample.CostAftertax = p.Cost_OMS /( ( 100 +  A_Master_Sample.TaxPercent )/100 )
   
WHERE
 p.Cost_OMS > 0 
  and
  (A_Master_Sample.Cost is null 
	   OR A_Master_Sample.Cost = 0)
;   

/*
* PE_005_Cost_OMS
*/
DROP TEMPORARY TABLE IF EXISTS Costs_OMS;
CREATE TEMPORARY TABLE Costs_OMS ( 
                                   Country varchar(45) NOT NULL, 
								   ItemId INT NOT NULL,  
								   TaxPercent FLOAT(10,5),
                                   Cost_OMS DECIMAL(15,2) NOT NULL, 
								   Cost_OMS_After_Tax DECIMAL(15,2) NOT NULL, 
								   id_procurement_order INT NOT NULL, 
								   currency_type varchar(45) NOT NULL, 
								   Month_Num INT NOT NULL,
								   PRIMARY KEY ( ItemId ),
								   KEY( id_procurement_order ),
								   KEY ( Country, Month_num )
 								   )
SELECT 
   "PER" AS Country,
   ItemId,
   TaxPercent
FROM
   A_Master_Sample  
;

UPDATE             Costs_OMS  
        INNER JOIN wmsprod_pe.itens_venda b 
                ON b.item_id = Costs_OMS.ItemID
        INNER JOIN wmsprod_pe.estoque c 
                ON c.estoque_id = b.estoque_id 
        INNER JOIN wmsprod_pe.itens_recebimento d
                ON c.itens_recebimento_id = d.itens_recebimento_id 
        INNER JOIN procurement_live_pe.wms_received_item e 
                ON c.itens_recebimento_id=e.id_wms 
        INNER JOIN procurement_live_pe.procurement_order_item f
                ON e.fk_procurement_order_item=f.id_procurement_order_item 
SET 
    Costs_OMS.Cost_OMS             = f.price_before_tax * ( ( 100 +  Costs_OMS.TaxPercent )/100 ) ,
    Costs_OMS.Cost_OMS_After_tax   = f.price_before_tax ,
	Costs_OMS.id_procurement_order = f.fk_procurement_order
WHERE 
       f.is_deleted = 0
   #AND (    Costs_OMS.Date >= "2013-07-01" 
   #      OR Costs_OMS.Cost is null 
   #      OR Costs_OMS.Cost <= 0 )
   AND f.unit_price is not null 
   AND f.unit_price > 0
       ;
	   
UPDATE             Costs_OMS	   
        INNER JOIN procurement_live_pe.procurement_order f
		     USING ( id_procurement_order ) 
SET	   
    Costs_OMS.currency_type = f.currency_type,
    Costs_OMS.Month_Num     = date_format( date(f.created_at), '%Y%m') 
;
update            Costs_OMS opt
       inner join development_mx.A_E_BI_ExchangeRate_USD er 
	        USING ( Country, Month_Num )     
set 
    opt.cost_oms = opt.cost_oms * er.XR,
	opt.cost_oms_after_tax = opt.cost_oms_after_tax * er.XR
where
    opt.currency_type = 'dolar' 
;



update            Costs_OMS opt
       inner join A_Master_Sample
	        USING ( ItemId )     
set 
    A_Master_Sample.Cost           = opt.cost_oms  ,
    A_Master_Sample.CostAftertax   = opt.cost_oms_after_tax
WHERE
       opt.cost_oms is not null 
   AND opt.cost_oms > 0

;

UPDATE            development_pe.A_Master_Sample
       INNER JOIN development_pe.COM_Adjust_Wrong_Costs_on_Daily_Report
                   ON     (A_Master_Sample.SKUSimple = COM_Adjust_Wrong_Costs_on_Daily_Report.SKU_Simple)
                      AND (A_Master_Sample.OrderNum = COM_Adjust_Wrong_Costs_on_Daily_Report.Order_Num)
SET
    A_Master_Sample.Cost         = COM_Adjust_Wrong_Costs_on_Daily_Report.Cost,
    A_Master_Sample.CostAfterTax = COM_Adjust_Wrong_Costs_on_Daily_Report.Cost_after_tax;

-- UPDATE development_pe.A_Master_Sample
-- INNER JOIN wmsprod_pe.itens_venda ON A_Master_Sample.ItemID = itens_venda.item_id
-- SET A_Master_Sample.Cost = itens_venda.cost_item,
--  A_Master_Sample.CostAfterTax = itens_venda.cost_item / (
-- 	1 + (
-- 		A_Master_Sample.TaxPercent / 100
-- 	)
-- );