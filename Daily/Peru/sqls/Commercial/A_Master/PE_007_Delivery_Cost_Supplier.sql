/* INBOUND COST FOR DELIVERY COST SUPPLIER*/
USE development_pe;
#Se inicia la variable en 0
UPDATE  A_Master_Sample
SET  A_Master_Sample.DeliveryCostSupplier = 0;

#Todos los items que sean de Dropshippong  se les agrega una bandera de -1
#Query: 7_M1_Costo0Revistas
UPDATE  A_Master_Sample
SET  A_Master_Sample.DeliveryCostSupplier = - 1
WHERE
	 A_Master_Sample.ShipmentType = 'Dropshipping'
	and MonthNum>=201309;

#Todos los items que sean de revistas  se les agrega una bandera de -1
#Query: 7_M1_Costo0Revistas
UPDATE  A_Master_Sample
INNER JOIN A_E_6_M1_Costos_Revistas ON  A_Master_Sample.SKUSimple = A_E_6_M1_Costos_Revistas.SKU_Simple
SET  A_Master_Sample.DeliveryCostSupplier = - 1
and MonthNum>=201309;


#Query: A 129 U WH/CS cost per Item
DROP TEMPORARY TABLE
IF EXISTS  WH_CS_Per_day;
#Se crea la tabla que contiene los costos por mes
CREATE TEMPORARY TABLE  WH_CS_Per_day (
	Month_Num INT,
	TotalDays INT,
	FL_Inboun_cost_per_day_NC FLOAT,
	PRIMARY KEY (Month_Num)
) SELECT
	Month_Num,
	dayofmonth(
		last_day(concat(Month_Num, "01"))
	) AS TotalDays,

VALUE
	AS FL_Inboun_cost_per_day_NC
FROM
	(
		SELECT
			MonthNum AS Month_Num,
		VALUE
		FROM
			(
				SELECT
					*
				FROM
					development_mx.M_Costs
				WHERE
					Country = 'PER'
				AND TypeCost = 'Inbound'
				ORDER BY
					updatedAt DESC
			) a
		GROUP BY
			MonthNum
	) AS Cost
GROUP BY
	Month_Num;

/*********************************************/
DROP TEMPORARY TABLE
IF EXISTS tmpFLWHCost;
/*********************************************/
#Se cra la tabla que contiene el numero de Items por mes
CREATE TEMPORARY TABLE tmpFLWHCost  
SELECT
	MonthNum,
	(
		COUNT(A_Master_Sample.ItemID) + SUM(A_Master_Sample.DeliveryCostSupplier)
	) AS item_aux
FROM
	A_Master_Sample
WHERE
	A_Master_Sample.OrderAfterCan = 1
GROUP BY
	MonthNum 
;

/*********************************************/
UPDATE WH_CS_Per_day
INNER JOIN tmpFLWHCost a on  
WH_CS_Per_day.Month_Num =a.MonthNum
SET
 WH_CS_Per_day.FL_Inboun_cost_per_day_NC = WH_CS_Per_day.FL_Inboun_cost_per_day_NC / a.item_aux;
/*********************************************/
UPDATE WH_CS_Per_day
INNER JOIN (
	SELECT
		MonthNum AS Month_Num,
		dayofmonth(max(Date)) Days
	FROM
		 A_Master_Sample
	GROUP BY
		MonthNum
) AS OrdersPerMonth USING (Month_Num)
SET 
WH_CS_Per_day.FL_Inboun_cost_per_day_NC = (	WH_CS_Per_day.FL_Inboun_cost_per_day_NC * OrdersPerMonth.Days) / (WH_CS_Per_day.TotalDays);
/*********************************************/
DROP TABLE IF EXISTS OPS_WH_Inbound_per_Order_for_PC2;
create table  OPS_WH_Inbound_per_Order_for_PC2 SELECT
	Month_Num,
	WH_CS_Per_day.FL_Inboun_cost_per_day_NC
FROM
	 WH_CS_Per_day;

/*********************************************/
UPDATE  A_Master_Sample
INNER JOIN OPS_WH_Inbound_per_Order_for_PC2 ON (
	A_Master_Sample.MonthNum = OPS_WH_Inbound_per_Order_for_PC2.Month_Num
	AND A_Master_Sample.OrderAfterCan = 1
	AND DeliveryCostSupplier = 0
)
SET A_Master_Sample.DeliveryCostSupplier = OPS_WH_Inbound_per_Order_for_PC2.FL_Inboun_cost_per_day_NC;
/********************************************/
UPDATE  A_Master_Sample
SET A_Master_Sample.DeliveryCostSupplier = 0
WHERE
	DeliveryCostSupplier =- 1