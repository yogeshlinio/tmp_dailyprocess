UPDATE            development_pe.A_Master_Sample
       INNER JOIN bob_live_pe.sales_order_address
               ON bob_live_pe.sales_order_address.id_sales_order_address = development_pe.A_Master_Sample.fk_sales_order_address_shipping
SET
   development_pe.A_Master_Sample.postcode                   = bob_live_pe.sales_order_address.postcode, 
   development_pe.A_Master_Sample.city                       = bob_live_pe.sales_order_address.city,
   development_pe.A_Master_Sample.fk_customer_address_region = bob_live_pe.sales_order_address.fk_customer_address_region
;
UPDATE            development_pe.A_Master_Sample
       INNER JOIN bob_live_pe.customer_address_region
               ON bob_live_pe.customer_address_region.id_customer_address_region = development_pe.A_Master_Sample.fk_customer_address_region
SET
   development_pe.A_Master_Sample.State = bob_live_pe.customer_address_region.code 
;
