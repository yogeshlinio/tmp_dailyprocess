 #Query: A 113 U Charge/Fee per PM
UPDATE           development_pe.A_Master_Sample
      INNER JOIN development_pe.A_E_In_COM_Fees_per_PM
              ON development_pe.A_Master_Sample.PaymentMethod = A_E_In_COM_Fees_per_PM.Payment_Method
SET
    development_pe.A_Master_Sample.Fees        = A_E_In_COM_Fees_per_PM.Fee,
    development_pe.A_Master_Sample.ExtraCharge = A_E_In_COM_Fees_per_PM.Extra_Charge;
