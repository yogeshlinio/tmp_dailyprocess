UPDATE            A_Master_Sample
       INNER JOIN bob_live_pe.catalog_shipment_type 
               ON A_Master_Sample.fk_catalog_shipment_type = bob_live_pe.catalog_shipment_type.id_catalog_shipment_type
SET
   A_Master_Sample.ShipmentType = bob_live_pe.catalog_shipment_type.name_en;


DROP TEMPORARY TABLE IF EXISTS TMP_Track;
CREATE TEMPORARY TABLE TMP_Track ( PRIMARY KEY ( item_id ) )
SELECT
  CAST( item_id as DECIMAL) AS  item_id,
  fulfillment_type_real
FROM operations_pe.out_order_tracking
GROUP BY item_id
;

UPDATE            A_Master_Sample
       INNER JOIN TMP_Track
               ON A_Master_Sample.ItemId = TMP_Track.item_id
SET
   A_Master_Sample.ShipmentType = TMP_Track.fulfillment_type_real,
   A_Master_Sample.FulfillmentTypeReal = TMP_Track.fulfillment_type_real
;
