UPDATE A_Master_Sample
SET
     A_Master_Sample.PaymentFees = If( IsNull(
                                                    ( A_Master_Sample.PaidPrice +  A_Master_Sample.ShippingFee) *
                                                    ( A_Master_Sample.Fees/100)
                                                     )  OR
                                       ( A_Master_Sample.PaidPrice +  A_Master_Sample.ShippingFee) *
         ( A_Master_Sample.Fees/100) = 0,
         A_Master_Sample.ExtraCharge/ A_Master_Sample.ItemsInOrder,
        ( A_Master_Sample.PaidPrice +  A_Master_Sample.ShippingFee)*( A_Master_Sample.Fees/100));
