#Query: A 125 U Coupon/After Vat coupon for Credit Vouchers
UPDATE  A_Master_Sample
SET
     A_Master_Sample.CouponValue = If( A_Master_Sample.PrefixCode="CCE",0,
                                                 If( A_Master_Sample.PrefixCode="PRCcre",0,
                                                 If( A_Master_Sample.PrefixCode="OPScre",0,
												 If( A_Master_Sample.PrefixCode like "PRCcoup%",0,
												 If( A_Master_Sample.PrefixCode like "PREcoup%",0,
                                                      A_Master_Sample.CouponValue))))),
    A_Master_Sample.CouponValueAfterTax = If( A_Master_Sample.PrefixCode="CCE",0,
                                                        If( A_Master_Sample.PrefixCode="PRCcre",0,
                                                        If( A_Master_Sample.PrefixCode="OPScre",0,
														If( A_Master_Sample.PrefixCode like "PRCcoup%",0,
														If( A_Master_Sample.PrefixCode like "PREcoup%",0,
                                                            A_Master_Sample.CouponValueAfterTax)))));


/*
* PATCH ABOUT PAGOEFECT
* Developed by: Carlos Mondragon
* Request by: Daniel Huerta
* Date:       7/10/2013
* Description: Move value of the coupon PAGOEFEC to the column NON_MKT (to be applied into PC3)
*              
*/
DROP TEMPORARY TABLE IF EXISTS TMP_PAGO;
CREATE TEMPORARY TABLE TMP_PAGO ( PRIMARY KEY ( ItemId ) )
SELECT
   ItemId,
   CouponValue
FROM
    A_Master_Sample
WHERE
    A_Master_Sample.PrefixCode = "PAGO";

SET @NonMKTCoupon     = 0000000000.0000;
SELECT  ( SUM(CouponValue) - 20000  ) /COUNT(*) INTO @NonMKTCoupon FROM TMP_PAGO;

UPDATE        TMP_PAGO
   INNER JOIN A_Master_Sample
        USING ( ItemId )
SET
    A_Master_Sample.NonMKTCouponAfterTax = @NonMKTCoupon / ( 1 + TaxPercent / 100 )  ,
    A_Master_Sample.CouponValueAfterTax = 0,
    A_Master_Sample.CouponValue = 0,
    A_Master_Sample.PaidPrice         =  A_Master_Sample.Price,
    A_Master_Sample.PaidPriceAfterTax =  A_Master_Sample.PriceAfterTax

WHERE
    A_Master_Sample.PrefixCode = "PAGO";
