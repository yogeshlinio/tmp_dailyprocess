UPDATE  A_Master_Sample
SET  A_Master_Sample.WHCost = 0,
 A_Master_Sample.FLWHCost = 0,
 A_Master_Sample.FLCSCost = 0,
 A_Master_Sample.PackagingCost = 0,
 A_Master_Sample.CSCost = 0;
#WHERE A_Master_Sample.MonthNum >= 201309;

#Query: A 129 U WH/CS cost per Item
DROP TEMPORARY TABLE IF EXISTS WH_CS_Per_day;
CREATE TEMPORARY TABLE WH_CS_Per_day ( PRIMARY KEY ( Month_Num ) )
SELECT
    Month_Num,
    dayofmonth(  last_day( concat( Month_Num , "01" ) ) ) as TotalDays,
    0  		as FL_WH_cost_per_day_NC,
    Value  	as FL_CS_cost_per_day_NC,
	0 	   	as Pack_cost_per_day_NC
FROM
    (   SELECT 	MonthNum AS Month_Num, Value FROM
	   (
		   SELECT * FROM development_mx.M_Costs WHERE Country = 'PER' AND TypeCost = 'FL_CS_cost' 		
		   ORDER BY updatedAt DESC
	    ) a GROUP BY MonthNum   
	) as Cost
GROUP BY Month_Num;

UPDATE WH_CS_Per_day 
   INNER JOIN 
      (   SELECT 	MonthNum AS Month_Num, Value FROM
	      (
		      SELECT * FROM development_mx.M_Costs WHERE Country = 'PER' AND TypeCost = 'FL_WH_cost' 		
		      ORDER BY updatedAt DESC
	       ) a GROUP BY MonthNum   
	   ) as Cost
	USING (Month_Num)
SET
    FL_WH_cost_per_day_NC = Value  
;
	
/*
*Actualizacón de Packaging Cost
*/
UPDATE WH_CS_Per_day
INNER JOIN (
	SELECT
		MonthNum AS Month_Num,
	VALUE
	FROM
		(
			SELECT
				*
			FROM
				development_mx.M_Costs
			WHERE
				Country = 'PER'
			AND TypeCost = 'Packaging'
			ORDER BY
				updatedAt DESC
		) a
	GROUP BY
		MonthNum
) AS Cost USING (Month_Num)
SET Pack_cost_per_day_NC =
VALUE
WHERE
	Month_Num >= 201309;
	
/*********************************************/
DROP TEMPORARY TABLE
IF EXISTS tmpFLWHCost;

/*********************************************/
CREATE TEMPORARY TABLE tmpFLWHCost  
SELECT
	MonthNum,
	(
		COUNT(A_Master_Sample.ItemID) + SUM(A_Master_Sample.WHCost)
	) AS item_aux,
	sum(if(PackageWeight <= 0.3, 1, 0) + if(PackageWeight <= 0.3, WHCost, 0)) +
    2*(sum(if(PackageWeight > 0.3 and PackageWeight <= 0.5, 1, 0) + if(PackageWeight > 0.3 and PackageWeight <= 0.5, WHCost, 0))) +
    4*sum(if(PackageWeight > 0.5 and PackageWeight <= 2.14, 1, 0) + if(PackageWeight > 0.5 and PackageWeight <= 2.14, WHCost, 0)) +
    8*sum(if(PackageWeight > 2.14, 1, 0) + if(PackageWeight > 2.14, WHCost, 0)) as "NetItemsGroup"
FROM
	A_Master_Sample
WHERE
	A_Master_Sample.OrderAfterCan = 1
GROUP BY
	MonthNum 
;

/*********************************************/
UPDATE WH_CS_Per_day
INNER JOIN tmpFLWHCost a on  
WH_CS_Per_day.Month_Num =a.MonthNum
SET
 WH_CS_Per_day.FL_WH_cost_per_day_NC = WH_CS_Per_day.FL_WH_cost_per_day_NC / a.item_aux ,
 WH_CS_Per_day.Pack_cost_per_day_NC  = WH_CS_Per_day.Pack_cost_per_day_NC / a.NetItemsGroup;
/*********************************************/

UPDATE WH_CS_Per_day
INNER JOIN (
	SELECT
		MonthNum AS Month_Num,
		dayofmonth(max(Date)) Days
	FROM
		 A_Master_Sample
	GROUP BY
		MonthNum
) AS OrdersPerMonth USING (Month_Num)
SET 
WH_CS_Per_day.FL_WH_cost_per_day_NC = (	WH_CS_Per_day.FL_WH_cost_per_day_NC * OrdersPerMonth.Days) / (WH_CS_Per_day.TotalDays),
WH_CS_Per_day.Pack_cost_per_day_NC = (	WH_CS_Per_day.Pack_cost_per_day_NC * OrdersPerMonth.Days) / (WH_CS_Per_day.TotalDays);

/*********************************************/
UPDATE WH_CS_Per_day
INNER JOIN (
	SELECT
		MonthNum AS Month_Num,
		dayofmonth(max(Date)) Days,
		count(ItemId) AS NetItems
	FROM
		A_Master_Sample
	WHERE
		OrderAfterCan = 1
	GROUP BY
		MonthNum
) AS OrdersPerMonth USING (Month_Num)
SET WH_CS_Per_day.FL_CS_cost_per_day_NC = (	WH_CS_Per_day.FL_CS_cost_per_day_NC * OrdersPerMonth.Days) / (	OrdersPerMonth.NetItems * WH_CS_Per_day.TotalDays);
/*********************************************/
REPLACE  OPS_WH_CS_per_Order_for_PC2 SELECT
	Month_Num,
	WH_CS_Per_day.FL_WH_cost_per_day_NC,
	WH_CS_Per_day.FL_CS_cost_per_day_NC
FROM
	 WH_CS_Per_day;

/*********************************************/
UPDATE  A_Master_Sample
INNER JOIN OPS_WH_CS_per_Order_for_PC2 ON (
	A_Master_Sample.MonthNum = OPS_WH_CS_per_Order_for_PC2.Month_Num
	AND A_Master_Sample.OrderAfterCan = 1
)
SET A_Master_Sample.FLCSCost = OPS_WH_CS_per_Order_for_PC2.CS_Cost_per_Order_PEN;

/*********************************************/
UPDATE  A_Master_Sample
INNER JOIN OPS_WH_CS_per_Order_for_PC2 ON (
	A_Master_Sample.MonthNum = OPS_WH_CS_per_Order_for_PC2.Month_Num
	AND A_Master_Sample.OrderAfterCan = 1
	AND WHCost = 0
)
SET A_Master_Sample.FLWHCost = OPS_WH_CS_per_Order_for_PC2.WH_Cost_per_Order_PEN;
/*
* update PackagingCost
*/
UPDATE  A_Master_Sample
INNER JOIN WH_CS_Per_day a ON (
	A_Master_Sample.MonthNum = a.Month_Num
	AND A_Master_Sample.OrderAfterCan = 1
	AND WHCost = 0
)
SET A_Master_Sample.PackagingCost = a.Pack_cost_per_day_NC * (if(PackageWeight <= 0.3, 1, if(PackageWeight > 0.3 and PackageWeight <= 0.5, 2, if(PackageWeight > 0.5 and PackageWeight <= 2.14, 4, if(PackageWeight > 2.14, 8, 0)))))

UPDATE  A_Master_Sample
SET  A_Master_Sample.WHCost = 0,
 A_Master_Sample.FLWHCost = 0,
 A_Master_Sample.FLCSCost = 0,
 A_Master_Sample.PackagingCost = 0,
 A_Master_Sample.CSCost = 0;
#WHERE A_Master_Sample.MonthNum >= 201309;

/*
* MX_099_001 Costo de Revistas
*/
#Query: 7_M1_Costo0Revistas
UPDATE  A_Master_Sample
SET  A_Master_Sample.WHCost = - 1
WHERE
	 A_Master_Sample.ShipmentType = 'Dropshipping';

/*
* MX_099_001 Costo de Revistas
*/
#Query: 7_M1_Costo0Revistas
UPDATE  A_Master_Sample
INNER JOIN A_E_6_M1_Costos_Revistas ON  A_Master_Sample.SKUSimple = A_E_6_M1_Costos_Revistas.SKU_Simple
SET  A_Master_Sample.WHCost = - 1;

/*
* MX_006 FL_Cost
*/
#Query: A 129 U WH/CS cost per Item
DROP TEMPORARY TABLE
IF EXISTS  WH_CS_Per_day;

CREATE TEMPORARY TABLE  WH_CS_Per_day (
	Month_Num INT,
	TotalDays INT,
	FL_WH_cost_per_day_NC FLOAT,
	FL_CS_cost_per_day_NC FLOAT,
	Pack_cost_per_day_NC FLOAT,
	PRIMARY KEY (Month_Num)
) SELECT
	Month_Num,
	dayofmonth(
		last_day(concat(Month_Num, "01"))
	) AS TotalDays,

	0 AS FL_WH_cost_per_day_NC,
	VALUE AS FL_CS_cost_per_day_NC,
	0 AS Pack_cost_per_day_NC
FROM
	(
		SELECT
			MonthNum AS Month_Num,
		VALUE
		FROM
			(
				SELECT
					*
				FROM
					 M_Costs
				WHERE
					Country = 'MEX'
				AND TypeCost = 'FL_CS_cost'
				ORDER BY
					updatedAt DESC
			) a
		GROUP BY
			MonthNum
	) AS Cost
#WHERE
#	Month_Num >= 201309
GROUP BY
	Month_Num;

/*
*Actualización de Customer Service
*/
UPDATE  WH_CS_Per_day
INNER JOIN (
	SELECT
		MonthNum AS Month_Num,
	VALUE
	FROM
		(
			SELECT
				*
			FROM
				 M_Costs
			WHERE
				Country = 'MEX'
			AND TypeCost = 'FL_WH_cost'
			ORDER BY
				updatedAt DESC
		) a
	GROUP BY
		MonthNum
) AS Cost USING (Month_Num)
SET FL_WH_cost_per_day_NC =
VALUE ;
#WHERE
#	Month_Num >= 201309;
	
/*
*Actualizacón de Packaging Cost
*/
UPDATE  WH_CS_Per_day
INNER JOIN (
	SELECT
		MonthNum AS Month_Num,
	VALUE
	FROM
		(
			SELECT
				*
			FROM
				 M_Costs
			WHERE
				Country = 'MEX'
			AND TypeCost = 'Packaging'
			ORDER BY
				updatedAt DESC
		) a
	GROUP BY
		MonthNum
) AS Cost USING (Month_Num)
SET Pack_cost_per_day_NC =
VALUE
WHERE
	Month_Num >= 201309;
/*********************************************/
DROP TEMPORARY TABLE
IF EXISTS tmpFLWHCost;

/*********************************************/
CREATE TEMPORARY TABLE tmpFLWHCost  
SELECT
	MonthNum,
	(
		COUNT(A_Master_Sample.ItemID) + SUM(A_Master_Sample.WHCost)
	) AS item_aux,
	sum(if(PackageWeight <= 0.3, 1, 0) + if(PackageWeight <= 0.3, WHCost, 0)) +
    2*(sum(if(PackageWeight > 0.3 and PackageWeight <= 0.5, 1, 0) + if(PackageWeight > 0.3 and PackageWeight <= 0.5, WHCost, 0))) +
    4*sum(if(PackageWeight > 0.5 and PackageWeight <= 2.14, 1, 0) + if(PackageWeight > 0.5 and PackageWeight <= 2.14, WHCost, 0)) +
    8*sum(if(PackageWeight > 2.14, 1, 0) + if(PackageWeight > 2.14, WHCost, 0)) as "NetItemsGroup"
FROM
	A_Master_Sample
WHERE
	A_Master_Sample.OrderAfterCan = 1
GROUP BY
	MonthNum 
;

/*********************************************/
UPDATE WH_CS_Per_day
INNER JOIN tmpFLWHCost a on  
WH_CS_Per_day.Month_Num =a.MonthNum
SET
 WH_CS_Per_day.FL_WH_cost_per_day_NC = WH_CS_Per_day.FL_WH_cost_per_day_NC / a.item_aux ,
 WH_CS_Per_day.Pack_cost_per_day_NC  = WH_CS_Per_day.Pack_cost_per_day_NC / a.NetItemsGroup;
/*********************************************/

UPDATE WH_CS_Per_day
INNER JOIN (
	SELECT
		MonthNum AS Month_Num,
		dayofmonth(max(Date)) Days
	FROM
		 A_Master_Sample
	GROUP BY
		MonthNum
) AS OrdersPerMonth USING (Month_Num)
SET 
WH_CS_Per_day.FL_WH_cost_per_day_NC = (	WH_CS_Per_day.FL_WH_cost_per_day_NC * OrdersPerMonth.Days) / (WH_CS_Per_day.TotalDays),
WH_CS_Per_day.Pack_cost_per_day_NC = (	WH_CS_Per_day.Pack_cost_per_day_NC * OrdersPerMonth.Days) / (WH_CS_Per_day.TotalDays);

/*********************************************/
UPDATE WH_CS_Per_day
INNER JOIN (
	SELECT
		MonthNum AS Month_Num,
		dayofmonth(max(Date)) Days,
		count(ItemId) AS NetItems
	FROM
		A_Master_Sample
	WHERE
		OrderAfterCan = 1
	GROUP BY
		MonthNum
) AS OrdersPerMonth USING (Month_Num)
SET WH_CS_Per_day.FL_CS_cost_per_day_NC = (	WH_CS_Per_day.FL_CS_cost_per_day_NC * OrdersPerMonth.Days) / (	OrdersPerMonth.NetItems * WH_CS_Per_day.TotalDays);
/*********************************************/
REPLACE  OPS_WH_CS_per_Order_for_PC2 SELECT
	Month_Num,
	WH_CS_Per_day.FL_WH_cost_per_day_NC,
	WH_CS_Per_day.FL_CS_cost_per_day_NC
FROM
	 WH_CS_Per_day;

/*********************************************/
UPDATE  A_Master_Sample
INNER JOIN OPS_WH_CS_per_Order_for_PC2 ON (
	A_Master_Sample.MonthNum = OPS_WH_CS_per_Order_for_PC2.Month_Num
	AND A_Master_Sample.OrderAfterCan = 1
)
SET A_Master_Sample.FLCSCost = OPS_WH_CS_per_Order_for_PC2.CS_Cost_per_Order_MXN;

/*********************************************/
UPDATE  A_Master_Sample
INNER JOIN OPS_WH_CS_per_Order_for_PC2 ON (
	A_Master_Sample.MonthNum = OPS_WH_CS_per_Order_for_PC2.Month_Num
	AND A_Master_Sample.OrderAfterCan = 1
	AND WHCost = 0
)
SET A_Master_Sample.FLWHCost = OPS_WH_CS_per_Order_for_PC2.WH_Cost_per_Order_MXN;
/*
* update PackagingCost
*/
UPDATE  A_Master_Sample
INNER JOIN WH_CS_Per_day a ON (
	A_Master_Sample.MonthNum = a.Month_Num
	AND A_Master_Sample.OrderAfterCan = 1
	AND WHCost = 0
)
SET A_Master_Sample.PackagingCost = a.Pack_cost_per_day_NC * (if(PackageWeight <= 0.3, 1, if(PackageWeight > 0.3 and PackageWeight <= 0.5, 2, if(PackageWeight > 0.5 and PackageWeight <= 2.14, 4, if(PackageWeight > 2.14, 8, 0)))))