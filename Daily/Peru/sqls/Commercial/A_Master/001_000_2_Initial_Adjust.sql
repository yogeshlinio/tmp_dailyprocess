/*
* MX_099_004 Ordenes coorporativas
*/
 #Query:          
UPDATE            A_Master_Sample  
       INNER JOIN M_Corrections 
               ON A_Master_Sample.ItemId = M_Corrections.ItemId 
SET 
   A_Master_Sample.ShippingFee  = IF( useShippingFee  = 1, M_Corrections.ShippingFee,  A_Master_Sample.ShippingFee  ) , 
   A_Master_Sample.ShippingCost = IF( useShippingCost = 1, M_Corrections.ShippingCost, A_Master_Sample.ShippingCost ) , 
   A_Master_Sample.PaymentFees  = IF( usePaymentFee   = 1, M_Corrections.PaymentFee  , A_Master_Sample.PaymentFees  ) ,
   A_Master_Sample.Cost         = IF( useCost         = 1, M_Corrections.Cost        , A_Master_Sample.Cost         ) ,
   A_Master_Sample.CostAfterTax = IF( useCost         = 1, M_Corrections.Cost / ( ( 100 +  TaxPercent )/100 )	        ,                                   A_Master_Sample.CostAfterTax ) 
;