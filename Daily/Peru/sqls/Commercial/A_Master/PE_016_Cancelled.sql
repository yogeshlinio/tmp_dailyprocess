UPDATE A_Master_Sample            
SET
   A_Master_Sample.Cancelled = 1
WHERE 
       Cancellations = 1 
   AND Rejected      = 0
;
