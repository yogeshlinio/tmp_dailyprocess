USE development_pe;
#CORRECT NULLS
UPDATE            development_pe.A_Master_Sample
       INNER JOIN bob_live_pe.catalog_config 
               ON development_pe.A_Master_Sample.SKUConfig = bob_live_pe.catalog_config.sku
SET
   development_pe.A_Master_Sample.Packageweight  = bob_live_pe.catalog_config.package_weight
WHERE
  (
        development_pe.A_Master_Sample.PackageWeight IS NULL 
     OR development_pe.A_Master_Sample.PackageWeight <= 0 ) 
AND bob_live_pe.catalog_config.Package_weight IS NOT NULL 
AND bob_live_pe.catalog_config.Package_weight > 0  
; 

UPDATE            development_pe.A_Master_Sample
SET
   development_pe.A_Master_Sample.PackageWeight  = 1
WHERE
      development_pe.A_Master_Sample.PackageWeight = 0 
   OR development_pe.A_Master_Sample.PackageWeight IS NULL;

DROP  TABLE IF EXISTS A_E_BI_ShippingCostPerOrder;
CREATE TABLE A_E_BI_ShippingCostPerOrder (
    OrderNum int,
    Date date,
    id_sales_order int,
    Month_Num int,
    City varchar(50),
    Payment_Method varchar(50),
    Deliver VARCHAR(50) default "",
    Weight decimal(15,5) default null,
    Cost                DECIMAL(15,5),
    ShippingCost       DECIMAL(15,5),
    ShippingCost_Servi DECIMAL(15,5),
    ShippingCost_Urban DECIMAL(15,5),
    Order_Items int,

    PRIMARY KEY ( OrderNum ),
    INDEX ( id_sales_order ),
    INDEX ( City, Weight )
);

INSERT A_E_BI_ShippingCostPerOrder
SELECT
    OrderNum,
    Date,
    idSalesOrder,
    MonthNum,
    City,
    PaymentMethod,
    "" as Deliver,
    SUM( PackageWeight ) as Weight ,
    SUM( Cost ) as Cost,
    0 as ShippingCost,
    0 as ShippingCost_Servi,
    0 as ShippingCost_Urban,
    COUNT(*) AS Order_Items
FROM
   development_pe.A_Master_Sample
GROUP BY OrderNum;

UPDATE
                  A_E_BI_ShippingCostPerOrder 
       INNER JOIN A_E_BI_DeliverCost 
               ON     A_E_BI_ShippingCostPerOrder.City = A_E_BI_DeliverCost.City
                  AND A_E_BI_ShippingCostPerOrder.Weight 
                         BETWEEN A_E_BI_DeliverCost.Minweight AND A_E_BI_DeliverCost.Maxweight
SET
    A_E_BI_ShippingCostPerOrder.Deliver       = "Servientrega",
    A_E_BI_ShippingCostPerOrder.ShippingCost = A_E_BI_DeliverCost.Shipping_Cost 
WHERE
       Payment_Method = 'CashOnDelivery_Payment'
   AND A_E_BI_DeliverCost.fk_id_deliver = 1
;

UPDATE
                  A_E_BI_ShippingCostPerOrder 
       INNER JOIN A_E_BI_DeliverCost 
               ON     A_E_BI_ShippingCostPerOrder.City = A_E_BI_DeliverCost.City
                  AND A_E_BI_ShippingCostPerOrder.Weight 
                         BETWEEN A_E_BI_DeliverCost.Minweight AND A_E_BI_DeliverCost.Maxweight
SET
    A_E_BI_ShippingCostPerOrder.ShippingCost_Servi = A_E_BI_DeliverCost.Shipping_Cost 
WHERE
       Payment_Method != 'CashOnDelivery_Payment'
   AND A_E_BI_DeliverCost.fk_id_deliver = 2
;

UPDATE
                  A_E_BI_ShippingCostPerOrder 
       INNER JOIN A_E_BI_DeliverCost 
               ON     IF( A_E_BI_ShippingCostPerOrder.City IN ( "LIMA", "CALLAO" ), "LIMA", "PROVINCIAS"  ) = A_E_BI_DeliverCost.City
                  AND A_E_BI_ShippingCostPerOrder.Weight 
                         BETWEEN A_E_BI_DeliverCost.Minweight AND A_E_BI_DeliverCost.Maxweight
SET
    A_E_BI_ShippingCostPerOrder.ShippingCost_Urban = A_E_BI_DeliverCost.Shipping_Cost 
WHERE
       Payment_Method != 'CashOnDelivery_Payment'
   AND A_E_BI_DeliverCost.fk_id_deliver = 3
;

UPDATE
                  A_E_BI_ShippingCostPerOrder 
SET
    A_E_BI_ShippingCostPerOrder.Deliver       = IF( ShippingCost_Servi <= ShippingCost_Urban,  "Servientrega", "Urbano" ),
    A_E_BI_ShippingCostPerOrder.ShippingCost = LEAST( ShippingCost_Servi, ShippingCost_Urban ) 
WHERE
       Payment_Method != 'CashOnDelivery_Payment'
   AND ShippingCost_Urban != 0 
   AND ShippingCost_Servi != 0
;

UPDATE
                  A_E_BI_ShippingCostPerOrder 
SET
    A_E_BI_ShippingCostPerOrder.ShippingCost = ShippingCost + IF( Cost < 300 , 2 , Cost * 0.005 )
WHERE
       Deliver = 'Servientrega'
AND A_E_BI_ShippingCostPerOrder.ShippingCost != 0
;
UPDATE
                  A_E_BI_ShippingCostPerOrder 
SET
    A_E_BI_ShippingCostPerOrder.ShippingCost = ShippingCost + Cost * 0.01 
WHERE
       Deliver = 'Urbano'
AND A_E_BI_ShippingCostPerOrder.ShippingCost != 0
;


UPDATE             A_E_BI_ShippingCostPerOrder 
SET
   ShippingCost = ShippingCost * 1.18
;

UPDATE             A_E_BI_ShippingCostPerOrder 
SET
   ShippingCost = Weight * 3.22
WHERE 
      ShippingCost = 0
  AND Deliver = ""  ;

#SELECT * FROM A_E_BI_ShippingCostPerOrder 
#where Date >=  "2013-06-15"
#ORDER BY Date, Payment_Method, City
#;

#SELECT SUM( ShippingCost ) FROM A_E_BI_ShippingCostPerOrder 
#where Month_Num = 201306




UPDATE development_pe.A_Master_Sample SET ShippingCost = 0;
UPDATE            A_E_BI_ShippingCostPerOrder
       INNER JOIN development_pe.A_Master_Sample
            USING ( OrderNum )
SET
    development_pe.A_Master_Sample.ShippingCost = ( A_E_BI_ShippingCostPerOrder.ShippingCost * 
                                           ( development_pe.A_Master_Sample.PackageWeight / A_E_BI_ShippingCostPerOrder.Weight ) 
                                        );

#;#CORRECT NULLS
UPDATE            development_pe.A_Master_Sample
       INNER JOIN bob_live_pe.catalog_config 
               ON development_pe.A_Master_Sample.SKUConfig = bob_live_pe.catalog_config.sku
SET
   development_pe.A_Master_Sample.PackageWeight  = bob_live_pe.catalog_config.package_weight
WHERE
  (
        development_pe.A_Master_Sample.PackageWeight IS NULL 
     OR development_pe.A_Master_Sample.PackageWeight <= 0 ) 
AND bob_live_pe.catalog_config.Package_weight IS NOT NULL 
AND bob_live_pe.catalog_config.Package_weight > 0  
; 

UPDATE            development_pe.A_Master_Sample
SET
   development_pe.A_Master_Sample.PackageWeight  = 1
WHERE
      development_pe.A_Master_Sample.PackageWeight = 0 
   OR development_pe.A_Master_Sample.PackageWeight IS NULL;

DROP  TABLE IF EXISTS A_E_BI_ShippingCostPerOrder;
CREATE TABLE A_E_BI_ShippingCostPerOrder (
    OrderNum int,
    Date date,
    id_sales_order int,
    Month_Num int,
    City varchar(50),
    Payment_Method varchar(50),
    Deliver VARCHAR(50) default "",
    Weight decimal(15,5) default null,
    Cost                DECIMAL(15,5),
    ShippingCost       DECIMAL(15,5),
    ShippingCost_Servi DECIMAL(15,5),
    ShippingCost_Urban DECIMAL(15,5),
    Order_Items int,

    PRIMARY KEY ( OrderNum ),
    INDEX ( id_sales_order ),
    INDEX ( City, Weight )
);

INSERT A_E_BI_ShippingCostPerOrder
SELECT
    OrderNum,
    Date,
    idSalesOrder,
    MonthNum,
    City,
    PaymentMethod,
    "" as Deliver,
    SUM( PackageWeight ) as Weight ,
    SUM( Cost ) as Cost,
    0 as ShippingCost,
    0 as ShippingCost_Servi,
    0 as ShippingCost_Urban,
    COUNT(*) AS Order_Items
FROM
   development_pe.A_Master_Sample
GROUP BY OrderNum;

UPDATE
                  A_E_BI_ShippingCostPerOrder 
       INNER JOIN A_E_BI_DeliverCost 
               ON     A_E_BI_ShippingCostPerOrder.City = A_E_BI_DeliverCost.City
                  AND A_E_BI_ShippingCostPerOrder.Weight 
                         BETWEEN A_E_BI_DeliverCost.Minweight AND A_E_BI_DeliverCost.Maxweight
SET
    A_E_BI_ShippingCostPerOrder.Deliver       = "Servientrega",
    A_E_BI_ShippingCostPerOrder.ShippingCost = A_E_BI_DeliverCost.Shipping_Cost 
WHERE
       Payment_Method = 'CashOnDelivery_Payment'
   AND A_E_BI_DeliverCost.fk_id_deliver = 1
;

UPDATE
                  A_E_BI_ShippingCostPerOrder 
       INNER JOIN A_E_BI_DeliverCost 
               ON     A_E_BI_ShippingCostPerOrder.City = A_E_BI_DeliverCost.City
                  AND A_E_BI_ShippingCostPerOrder.Weight 
                         BETWEEN A_E_BI_DeliverCost.Minweight AND A_E_BI_DeliverCost.Maxweight
SET
    A_E_BI_ShippingCostPerOrder.ShippingCost_Servi = A_E_BI_DeliverCost.Shipping_Cost 
WHERE
       Payment_Method != 'CashOnDelivery_Payment'
   AND A_E_BI_DeliverCost.fk_id_deliver = 2
;

UPDATE
                  A_E_BI_ShippingCostPerOrder 
       INNER JOIN A_E_BI_DeliverCost 
               ON     IF( A_E_BI_ShippingCostPerOrder.City IN ( "LIMA", "CALLAO" ), "LIMA", "PROVINCIAS"  ) = A_E_BI_DeliverCost.City
                  AND A_E_BI_ShippingCostPerOrder.Weight 
                         BETWEEN A_E_BI_DeliverCost.Minweight AND A_E_BI_DeliverCost.Maxweight
SET
    A_E_BI_ShippingCostPerOrder.ShippingCost_Urban = A_E_BI_DeliverCost.Shipping_Cost 
WHERE
       Payment_Method != 'CashOnDelivery_Payment'
   AND A_E_BI_DeliverCost.fk_id_deliver = 3
;

UPDATE
                  A_E_BI_ShippingCostPerOrder 
SET
    A_E_BI_ShippingCostPerOrder.Deliver       = IF( ShippingCost_Servi <= ShippingCost_Urban,  "Servientrega", "Urbano" ),
    A_E_BI_ShippingCostPerOrder.ShippingCost = LEAST( ShippingCost_Servi, ShippingCost_Urban ) 
WHERE
       Payment_Method != 'CashOnDelivery_Payment'
   AND ShippingCost_Urban != 0 
   AND ShippingCost_Servi != 0
;

UPDATE
                  A_E_BI_ShippingCostPerOrder 
SET
    A_E_BI_ShippingCostPerOrder.ShippingCost = ShippingCost + IF( Cost < 300 , 2 , Cost * 0.005 )
WHERE
       Deliver = 'Servientrega'
AND A_E_BI_ShippingCostPerOrder.ShippingCost != 0
;
UPDATE
                  A_E_BI_ShippingCostPerOrder 
SET
    A_E_BI_ShippingCostPerOrder.ShippingCost = ShippingCost + Cost * 0.01 
WHERE
       Deliver = 'Urbano'
AND A_E_BI_ShippingCostPerOrder.ShippingCost != 0
;


UPDATE             A_E_BI_ShippingCostPerOrder 
SET
   ShippingCost = ShippingCost * 1.18
;

UPDATE             A_E_BI_ShippingCostPerOrder 
SET
   ShippingCost = Weight * 3.22
WHERE 
      ShippingCost = 0
  AND Deliver = ""  ;

#SELECT * FROM A_E_BI_ShippingCostPerOrder 
#where Date >=  "2013-06-15"
#ORDER BY Date, Payment_Method, City
#;

#SELECT SUM( ShippingCost ) FROM A_E_BI_ShippingCostPerOrder 
#where Month_Num = 201306

UPDATE development_pe.A_Master_Sample SET ShippingCost = 0;
UPDATE            A_E_BI_ShippingCostPerOrder
       INNER JOIN development_pe.A_Master_Sample
            USING ( OrderNum )
SET
    development_pe.A_Master_Sample.ShippingCost = ( A_E_BI_ShippingCostPerOrder.ShippingCost * 
                                           ( development_pe.A_Master_Sample.PackageWeight / A_E_BI_ShippingCostPerOrder.Weight ) 
                                        );

#SELECT sum( Package_weight ) , sum( ShippingCost )
#FROM development_pe.A_Master_Sample where Month_Num = 201306 
#and Payment_Method = "Adyen_HostedPaymentPage" AND City = "Miraflores" 
#AND Date >= "2013-06-15"
#AND OrderAfterCan = 1
#SELECT
#*
#FROM
#                  A_E_BI_ShippingCostPerOrder 
#       INNER JOIN A_E_BI_DeliverCost 
#               ON     IF( A_E_BI_ShippingCostPerOrder.City IN ( "LIMA", "CALLAO" ), "LIMA", "PROVINCIAS"  ) = A_E_BI_DeliverCost.City
#                  AND A_E_BI_ShippingCostPerOrder.Weight 
#                         BETWEEN A_E_BI_DeliverCost.Minweight AND A_E_BI_DeliverCost.Maxweight
#WHERE
#       Payment_Method != 'CashOnDelivery_Payment'
#   AND A_E_BI_DeliverCost.fk_id_deliver = 3
#AND OrderNum = 200058741
#;
