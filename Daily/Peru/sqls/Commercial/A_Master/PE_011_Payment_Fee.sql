USE development_pe;


UPDATE development_pe.A_Master_Sample
SET
    development_pe.A_Master_Sample.PaymentFees = If( IsNull(
                                                    (development_pe.A_Master_Sample.PaidPrice + development_pe.A_Master_Sample.ShippingFee) *
                                                    (development_pe.A_Master_Sample.Fees/100)
                                                     )  OR
                                       (development_pe.A_Master_Sample.PaidPrice + development_pe.A_Master_Sample.ShippingFee) *
         (development_pe.A_Master_Sample.Fees/100) = 0,
        development_pe.A_Master_Sample.ExtraCharge/development_pe.A_Master_Sample.ItemsInOrder,
        (development_pe.A_Master_Sample.PaidPrice + development_pe.A_Master_Sample.ShippingFee)*(development_pe.A_Master_Sample.Fees/100));
