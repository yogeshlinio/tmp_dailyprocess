#Query: A 125 U Coupon/After Vat coupon for Credit Vouchers
USE development_pe;
UPDATE development_pe.A_Master_Sample
SET
    development_pe.A_Master_Sample.CouponValue = If(development_pe.A_Master_Sample.PrefixCode="CCE",0,
                                                 If(development_pe.A_Master_Sample.PrefixCode="PRCcre",0,
                                                 If(development_pe.A_Master_Sample.PrefixCode="OPScre",0,
												 If(development_pe.A_Master_Sample.PrefixCode like "PRCcoup%",0,
												 If(development_pe.A_Master_Sample.PrefixCode like "PREcoup%",0,
                                                     development_pe.A_Master_Sample.CouponValue))))),
   development_pe.A_Master_Sample.CouponValueAfterTax = If(development_pe.A_Master_Sample.PrefixCode="CCE",0,
                                                        If(development_pe.A_Master_Sample.PrefixCode="PRCcre",0,
                                                        If(development_pe.A_Master_Sample.PrefixCode="OPScre",0,
														If(development_pe.A_Master_Sample.PrefixCode like "PRCcoup%",0,
														If(development_pe.A_Master_Sample.PrefixCode like "PREcoup%",0,
                                                           development_pe.A_Master_Sample.CouponValueAfterTax)))));


/*
* PATCH ABOUT PAGOEFECT
* Developed by: Carlos Mondragon
* Request by: Daniel Huerta
* Date:       7/10/2013
* Description: Move value of the coupon PAGOEFEC to the column NON_MKT (to be applied into PC3)
*              
*/
DROP TEMPORARY TABLE IF EXISTS TMP_PAGO;
CREATE TEMPORARY TABLE TMP_PAGO ( PRIMARY KEY ( ItemId ) )
SELECT
   ItemId,
   CouponValue
FROM
   development_pe.A_Master_Sample
WHERE
   development_pe.A_Master_Sample.PrefixCode = "PAGO";

SET @NonMKTCoupon     = 0000000000.0000;
SELECT  ( SUM(CouponValue) - 20000  ) /COUNT(*) INTO @NonMKTCoupon FROM TMP_PAGO;

UPDATE        TMP_PAGO
   INNER JOIN A_Master_Sample
        USING ( ItemId )
SET
   development_pe.A_Master_Sample.NonMKTCouponAfterTax = @NonMKTCoupon / ( 1 + TaxPercent / 100 )  ,
   development_pe.A_Master_Sample.CouponValueAfterTax = 0,
   development_pe.A_Master_Sample.CouponValue = 0,
   development_pe.A_Master_Sample.PaidPrice         = development_pe.A_Master_Sample.Price,
   development_pe.A_Master_Sample.PaidPriceAfterTax = development_pe.A_Master_Sample.PriceAfterTax

WHERE
   development_pe.A_Master_Sample.PrefixCode = "PAGO";
