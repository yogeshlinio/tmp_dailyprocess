UPDATE        development_pe.A_Master_Sample
   INNER JOIN bob_live_pe.sales_order_item soi 
           ON A_Master_Sample.ItemId = soi.id_sales_order_item 
   INNER JOIN bob_live_pe.sales_order so 
           ON soi.fk_sales_order=so.id_sales_order 
   INNER JOIN bob_live_pe.supplier s 
           ON s.id_supplier=soi.fk_supplier 
SET
   A_Master_Sample.isMPlace  = 1,
   A_Master_Sample.MPlaceFee = ( PriceAfterTax - CostAfterTax ) / PriceAfterTax
where is_option_marketplace>0 and OrderAfterCan='1'
;

UPDATE         A_Master_Sample
SET 
	A_Master_Sample.CostAfterTax = A_Master_Sample.PriceAfterTax - (PriceAfterTax * MPlaceFee),
	A_Master_Sample.Cost         = ( A_Master_Sample.PriceAfterTax - (PriceAfterTax * MPlaceFee)) * ( 1 + ( taxPercent / 100 ) )
	where 
	isMPlace      = 1 
	and OrderAfterCan =1 
;