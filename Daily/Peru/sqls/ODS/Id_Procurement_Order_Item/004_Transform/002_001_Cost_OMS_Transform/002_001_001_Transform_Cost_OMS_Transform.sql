UPDATE Id_Procurement_Order_Item_Sample_@v_countryPrefix@ opt
JOIN development_mx.A_E_BI_ExchangeRate_USD er ON date_format(opt.date_po_created, '%Y%m') = er.Month_Num 
SET 
 opt.cost_oms = opt.cost_oms * er.XR,
 opt.cost_oms_after_tax = opt.cost_oms_after_tax * er.XR
WHERE
    opt.currency_type = 'dolar' and er.Country = 'PER';

	
	
