DROP   TEMPORARY TABLE IF EXISTS A_S_BI_Refunded;
CREATE TEMPORARY TABLE A_S_BI_Refunded ( INDEX ( fk_sales_order_item  ) )
SELECT
 *
FROM
    bob_live_pe.sales_order_item_status_history
WHERE
   fk_sales_order_item_status IN ( 103 )
GROUP BY fk_sales_order_item
;

UPDATE Id_Sales_Order_Item_Sample_@v_countryPrefix@ SET Refunded = 1 where Status = "refunded";


UPDATE           A_S_BI_Refunded
      INNER JOIN Id_Sales_Order_Item_Sample_@v_countryPrefix@
              ON A_S_BI_Refunded.fk_sales_order_item = Id_Sales_Order_Item_Sample_@v_countryPrefix@.ItemID 
SET
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.DateRefunded = A_S_BI_Refunded.created_at
WHERE
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.Refunded = 1
;
