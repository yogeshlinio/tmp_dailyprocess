UPDATE            Id_Sales_Order_Item_Sample_@v_countryPrefix@ AS Id_Sales_Order_Item_Sample
       INNER JOIN Id_Sales_Order_Item_Key_Map
	        USING ( Country , Id_Sales_Order_Item )
       INNER JOIN @developmen@.COM_Adjust_Wrong_Costs_on_Daily_Report
                   ON     (Id_Sales_Order_Item_Key_Map.SKU_Simple = COM_Adjust_Wrong_Costs_on_Daily_Report.SKU_Simple)
                      AND (Id_Sales_Order_Item_Key_Map.Order_Num  = COM_Adjust_Wrong_Costs_on_Daily_Report.Order_Num)
SET
    Id_Sales_Order_Item_Sample.Cost           = COM_Adjust_Wrong_Costs_on_Daily_Report.Cost,
    Id_Sales_Order_Item_Sample.Cost_After_Tax = COM_Adjust_Wrong_Costs_on_Daily_Report.Cost_after_tax;