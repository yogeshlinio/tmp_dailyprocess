USE development_pe;

DROP   TEMPORARY TABLE IF EXISTS A_E_BI_CreditNotes_tmp;
CREATE TEMPORARY TABLE  A_E_BI_CreditNotes_tmp
SELECT C.*,D.contador 
    FROM           A_E_BI_CreditNotes C
       LEFT JOIN ( SELECT 
                 A.Supplier,
 A.MonthNum,
 A.Cat1,
 A.Cat2, 
                 count(*) contador
                   FROM Id_Sales_Order_Item_Sample_@v_countryPrefix@ A
                   WHERE A.OrderAfterCan = 1
                   GROUP BY  A.Supplier,A.MonthNum,A.cat1,A.cat2
              ) D 
   ON     C.Supplier=D.supplier 
  AND D.MonthNum=C.Month_Num 
  AND D.cat2=C.cat2 
  AND D.cat1=C.cat1
;

UPDATE Id_Sales_Order_Item_Sample_@v_countryPrefix@ A,
       development_pe.A_E_BI_CreditNotes_tmp B 
SET 
    A.CreditNotes=B.Cost_after_tax/B.contador
WHERE 
       A.Supplier=B.supplier 
   AND A.MonthNum=B.Month_Num 
   AND A.cat2=B.cat2 
   AND A.cat1=B.cat1 
   AND A.OrderAfterCan = 1;

   
	USE development_pe;

	DROP TEMPORARY TABLE
	IF EXISTS tmp_CreditNotes;

	CREATE TEMPORARY TABLE tmp_CreditNotes SELECT
		C.*, D.contador
	FROM
		GlobalConfig.M_CreditNotes C
	LEFT JOIN (
		SELECT
			A.Supplier,
			A.MonthNum,
			A.CatBP,
			count(*) contador
		FROM
			Id_Sales_Order_Item_Sample_@v_countryPrefix@ A
		WHERE
			A.OrderAfterCan = 1
		GROUP BY
			A.Supplier,
			A.MonthNum,
			A.CatBP
	) D ON C.Supplier = D.supplier
	AND D.MonthNum = C.Month_Num
	AND D.CatBP = C.Category_BP;

	UPDATE Id_Sales_Order_Item_Sample_@v_countryPrefix@ A,
	 development_pe.tmp_CreditNotes B
	SET A.CreditNotes = B.Cost_after_tax / B.contador
	WHERE
		A.Supplier = B.supplier
	AND A.MonthNum = B.Month_Num
	AND A.CatBP = B.Category_BP
	AND A.OrderAfterCan = 1;
