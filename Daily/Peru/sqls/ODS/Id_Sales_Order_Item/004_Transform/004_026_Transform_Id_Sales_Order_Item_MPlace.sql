UPDATE        development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@
   INNER JOIN bob_live_pe.sales_order_item soi 
           ON Id_Sales_Order_Item_Sample_@v_countryPrefix@.ItemId = soi.id_sales_order_item 
   INNER JOIN bob_live_pe.sales_order so 
           ON soi.fk_sales_order=so.id_sales_order 
   INNER JOIN bob_live_pe.supplier s 
           ON s.id_supplier=soi.fk_supplier 
SET
   Id_Sales_Order_Item_Sample_@v_countryPrefix@.isMPlace  = 1,
   Id_Sales_Order_Item_Sample_@v_countryPrefix@.MPlaceFee = ( PriceAfterTax - CostAfterTax ) / PriceAfterTax
where is_option_marketplace>0 and OrderAfterCan='1'
;
