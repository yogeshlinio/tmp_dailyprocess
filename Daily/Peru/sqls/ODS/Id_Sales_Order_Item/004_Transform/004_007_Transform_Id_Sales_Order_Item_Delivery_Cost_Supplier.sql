/* INBOUND COST FOR DELIVERY COST SUPPLIER*/
USE development_pe;
#Se inicia la variable en 0
UPDATE development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@.DeliveryCostSupplier = 0;

#Todos los items que sean de Dropshippong  se les agrega una bandera de -1
#Query: 7_M1_Costo0Revistas
UPDATE development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@.DeliveryCostSupplier = - 1
WHERE
	development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShipmentType = 'Dropshipping'
	and MonthNum>=201309;

#Todos los items que sean de revistas  se les agrega una bandera de -1
#Query: 7_M1_Costo0Revistas
UPDATE development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@
INNER JOIN A_E_6_M1_Costos_Revistas ON development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@.SKUSimple = A_E_6_M1_Costos_Revistas.SKU_Simple
SET development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@.DeliveryCostSupplier = - 1
and MonthNum>=201309;


#Query: A 129 U WH/CS cost per Item
DROP TEMPORARY TABLE
IF EXISTS development_pe.WH_CS_Per_day;
#Se crea la tabla que contiene los costos por mes
CREATE TEMPORARY TABLE development_pe.WH_CS_Per_day (
	Month_Num INT,
	TotalDays INT,
	FL_Inboun_cost_per_day_NC FLOAT,
	PRIMARY KEY (Month_Num)
) SELECT
	Month_Num,
	dayofmonth(
		last_day(concat(Month_Num, "01"))
	) AS TotalDays,

VALUE
	AS FL_Inboun_cost_per_day_NC
FROM
	(
		SELECT
			MonthNum AS Month_Num,
		VALUE
		FROM
			(
				SELECT
					*
				FROM
					development_mx.M_Costs
				WHERE
					Country = 'PER'
				AND TypeCost = 'Inbound'
				ORDER BY
					updatedAt DESC
			) a
		GROUP BY
			MonthNum
	) AS Cost
GROUP BY
	Month_Num;

/*********************************************/
DROP TEMPORARY TABLE
IF EXISTS tmpFLWHCost;
/*********************************************/
#Se cra la tabla que contiene el numero de Items por mes
CREATE TEMPORARY TABLE tmpFLWHCost  
SELECT
	MonthNum,
	(
		COUNT(Id_Sales_Order_Item_Sample_@v_countryPrefix@.ItemID) + SUM(Id_Sales_Order_Item_Sample_@v_countryPrefix@.DeliveryCostSupplier)
	) AS item_aux
FROM
	Id_Sales_Order_Item_Sample_@v_countryPrefix@
WHERE
	Id_Sales_Order_Item_Sample_@v_countryPrefix@.OrderAfterCan = 1
GROUP BY
	MonthNum 
;

/*********************************************/
UPDATE WH_CS_Per_day
INNER JOIN tmpFLWHCost a on  
WH_CS_Per_day.Month_Num =a.MonthNum
SET
 WH_CS_Per_day.FL_Inboun_cost_per_day_NC = WH_CS_Per_day.FL_Inboun_cost_per_day_NC / a.item_aux;
/*********************************************/
UPDATE WH_CS_Per_day
INNER JOIN (
	SELECT
		MonthNum AS Month_Num,
		dayofmonth(max(Date)) Days
	FROM
		development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@
	GROUP BY
		MonthNum
) AS OrdersPerMonth USING (Month_Num)
SET 
WH_CS_Per_day.FL_Inboun_cost_per_day_NC = (	WH_CS_Per_day.FL_Inboun_cost_per_day_NC * OrdersPerMonth.Days) / (WH_CS_Per_day.TotalDays);
/*********************************************/
DROP TABLE IF EXISTS OPS_WH_Inbound_per_Order_for_PC2;
create table development_pe.OPS_WH_Inbound_per_Order_for_PC2 SELECT
	Month_Num,
	WH_CS_Per_day.FL_Inboun_cost_per_day_NC
FROM
	development_pe.WH_CS_Per_day;

/*********************************************/
UPDATE development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@
INNER JOIN OPS_WH_Inbound_per_Order_for_PC2 ON (
	Id_Sales_Order_Item_Sample_@v_countryPrefix@.MonthNum = OPS_WH_Inbound_per_Order_for_PC2.Month_Num
	AND Id_Sales_Order_Item_Sample_@v_countryPrefix@.OrderAfterCan = 1
	AND DeliveryCostSupplier = 0
)
SET Id_Sales_Order_Item_Sample_@v_countryPrefix@.DeliveryCostSupplier = OPS_WH_Inbound_per_Order_for_PC2.FL_Inboun_cost_per_day_NC;
/********************************************/
UPDATE development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET Id_Sales_Order_Item_Sample_@v_countryPrefix@.DeliveryCostSupplier = 0
WHERE
	DeliveryCostSupplier =- 1