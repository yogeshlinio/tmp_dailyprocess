
DROP   TEMPORARY TABLE IF EXISTS A_Returns;
CREATE TEMPORARY TABLE A_Returns ( INDEX ( fk_sales_order_item  ) )
SELECT
 fk_sales_order_item,
 created_at as dateReturned
FROM
    bob_live_pe.sales_order_item_status_history
WHERE
#automatic return (132), returned (8) no se usa
   fk_sales_order_item_status IN ( 120 )
GROUP BY fk_sales_order_item
;

REPLACE A_Returns
SELECT 
   c.item_id    AS fk_sales_order_item,
   b.changed_at AS created_at
FROM          wmsprod_pe.inverselogistics_status_history b 
   INNER JOIN wmsprod_pe.inverselogistics_devolucion c 
           ON b.return_id = c.id
WHERE
   b.status = 6;


UPDATE           A_Returns
      INNER JOIN Id_Sales_Order_Item_Sample_@v_countryPrefix@
              ON A_Returns.fk_sales_order_item = Id_Sales_Order_Item_Sample_@v_countryPrefix@.ItemID 
SET
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.Returns = 1,
	Id_Sales_Order_Item_Sample_@v_countryPrefix@.DateReturned = A_Returns.dateReturned
	
;

#Query: A 102 U OrderBefore-AfterCan
UPDATE Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET 
     Id_Sales_Order_Item_Sample_@v_countryPrefix@.Cancellations  = IF(       Id_Sales_Order_Item_Sample_@v_countryPrefix@.Cancellations = 1 
                                            AND Id_Sales_Order_Item_Sample_@v_countryPrefix@.Returns       = 0 , 1 , 0 )
;

/*
* DATE           2013/12/05
* DEVELOPED BY   Carlos Antonio Mondragón Soria
* DESCRIPTION MKT - Status Wrong in Returns 
*/
UPDATE Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET
   OrderAfterCan = 0,
   Cancellations = 0,
   Pending       = 0
WHERE
   Returns = 1;
