
DROP   TEMPORARY TABLE IF EXISTS A_S_BI_Delivered;
CREATE TEMPORARY TABLE A_S_BI_Delivered ( INDEX ( fk_sales_order_item  ) )
SELECT
 ItemId AS fk_sales_order_item,
 DateDelivered as created_at
FROM
   Id_Sales_Order_Item_Sample_@v_countryPrefix@
WHERE
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.DateDelivered <> "0000-00-00" 
AND Id_Sales_Order_Item_Sample_@v_countryPrefix@.DateDelivered is not null
GROUP BY ItemId 
;
UPDATE           A_S_BI_Delivered
      INNER JOIN Id_Sales_Order_Item_Sample_@v_countryPrefix@
              ON A_S_BI_Delivered.fk_sales_order_item = Id_Sales_Order_Item_Sample_@v_countryPrefix@.ItemID 
SET
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.DateCollected = A_S_BI_Delivered.created_at,
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.Collected = 1
;
DROP   TEMPORARY TABLE IF EXISTS A_S_BI_Delivered;
CREATE TEMPORARY TABLE A_S_BI_Delivered ( INDEX ( fk_sales_order_item  ) )
SELECT
 ItemId AS fk_sales_order_item,
 DateDelivered as created_at
FROM
    Id_Sales_Order_Item_Sample_@v_countryPrefix@
WHERE
   Delivered = 1
;

UPDATE           A_S_BI_Delivered
      INNER JOIN Id_Sales_Order_Item_Sample_@v_countryPrefix@
              ON A_S_BI_Delivered.fk_sales_order_item = Id_Sales_Order_Item_Sample_@v_countryPrefix@.ItemID 
SET
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.DateCollected = A_S_BI_Delivered.created_at,
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.Collected = 1
WHERE
    PaymentMethod in ( "CreditCardOnDelivery_Payment" ,
                       "CashOnDelivery_Payment"  )
;

DROP   TEMPORARY TABLE IF EXISTS A_S_BI_Exportable;
CREATE TEMPORARY TABLE A_S_BI_Exportable ( INDEX ( fk_sales_order_item  ) )
SELECT
  fk_sales_order_item,
  created_at
FROM
   bob_live_pe.sales_order_item_status_history
WHERE
	#exportable
   fk_sales_order_item_status IN ( 3 )
GROUP BY fk_sales_order_item
;


UPDATE           A_S_BI_Exportable
      INNER JOIN Id_Sales_Order_Item_Sample_@v_countryPrefix@
              ON A_S_BI_Exportable.fk_sales_order_item = Id_Sales_Order_Item_Sample_@v_countryPrefix@.ItemID 
SET

    Id_Sales_Order_Item_Sample_@v_countryPrefix@.DateCollected = A_S_BI_Exportable.created_at,
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.Collected = 1
WHERE
/*Ordenes de Prepag*/

    PaymentMethod in (  "Amex_Gateway" ,
                        "Paypal_Express_Checkout",
                        /*Old Collected*/
                        'Adquira_Interredes', 
                        'Amex_Gateway', 
                        'DineroMail_Api', 
                        'DineroMail_HostedPaymentPage', 
                        'Paypal_Express_Checkout'
                       )
;



DROP   TEMPORARY TABLE IF EXISTS A_S_BI_Manual_Fraud_Check_Pending;
CREATE TEMPORARY TABLE A_S_BI_Manual_Fraud_Check_Pending ( INDEX ( fk_sales_order_item  ) )
SELECT
 *
FROM
    bob_live_pe.sales_order_item_status_history
WHERE
	#manual_fraud_check_pending
   bob_live_pe.sales_order_item_status_history.fk_sales_order_item_status IN ( 114  )
GROUP BY fk_sales_order_item
;

UPDATE           A_S_BI_Manual_Fraud_Check_Pending
      INNER JOIN Id_Sales_Order_Item_Sample_@v_countryPrefix@
              ON A_S_BI_Manual_Fraud_Check_Pending.fk_sales_order_item = Id_Sales_Order_Item_Sample_@v_countryPrefix@.ItemID 
SET
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.DateCollected = A_S_BI_Manual_Fraud_Check_Pending.created_at,
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.Collected = 1
WHERE
    PaymentMethod in (   "Amex_Gateway" 
                        'Amex_Gateway', 
                        'DineroMail_Api', 
                        'DineroMail_HostedPaymentPage'
                       )
;


UPDATE           A_S_BI_Manual_Fraud_Check_Pending
      INNER JOIN Id_Sales_Order_Item_Sample_@v_countryPrefix@
              ON A_S_BI_Manual_Fraud_Check_Pending.fk_sales_order_item = Id_Sales_Order_Item_Sample_@v_countryPrefix@.ItemID 
SET
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.DateCollected = A_S_BI_Manual_Fraud_Check_Pending.created_at,
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.Collected = 1
WHERE
    PaymentMethod in (   "Amex_Gateway" ,
                         'Amex_Gateway', 
                         'DineroMail_Api', 
                         'DineroMail_HostedPaymentPage'
                         'DineroMail_HostedPaymentPage'
                       )
;

DROP   TEMPORARY TABLE IF EXISTS A_S_BI_Auto_Fraud_Check_Pending;
CREATE TEMPORARY TABLE A_S_BI_Auto_Fraud_Check_Pending ( INDEX ( fk_sales_order_item  ) )
SELECT
 *
FROM
    bob_live_pe.sales_order_item_status_history
WHERE
	#auto_fraud_check_pending
   bob_live_pe.sales_order_item_status_history.fk_sales_order_item_status IN ( 112 )
GROUP BY fk_sales_order_item
;

UPDATE           A_S_BI_Auto_Fraud_Check_Pending
      INNER JOIN Id_Sales_Order_Item_Sample_@v_countryPrefix@
              ON A_S_BI_Auto_Fraud_Check_Pending.fk_sales_order_item = Id_Sales_Order_Item_Sample_@v_countryPrefix@.ItemID 
SET
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.DateCollected = A_S_BI_Auto_Fraud_Check_Pending.created_at,
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.Collected = 1
WHERE
    PaymentMethod in (   "Amex_Gateway" 
                        'Amex_Gateway', 
                        'DineroMail_Api', 
                        'DineroMail_HostedPaymentPage'
                       )
;



DROP   TEMPORARY TABLE IF EXISTS A_S_BI_Fraud_Check_Pending;
CREATE TEMPORARY TABLE A_S_BI_Fraud_Check_Pending ( INDEX ( fk_sales_order_item  ) )
SELECT
 *
FROM
    bob_live_pe.sales_order_item_status_history
WHERE
	#fraud_check_pending, auto_fraud_check_pending, manual_fraud_check_pending
   bob_live_pe.sales_order_item_status_history.fk_sales_order_item_status IN ( 112 , 112,114 ) #auto fraud check pending
GROUP BY fk_sales_order_item
;

UPDATE           A_S_BI_Fraud_Check_Pending
      INNER JOIN Id_Sales_Order_Item_Sample_@v_countryPrefix@
              ON A_S_BI_Fraud_Check_Pending.fk_sales_order_item = Id_Sales_Order_Item_Sample_@v_countryPrefix@.ItemID 
SET
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.DateCollected = A_S_BI_Fraud_Check_Pending.created_at,
    Id_Sales_Order_Item_Sample_@v_countryPrefix@.Collected = 1
WHERE
    PaymentMethod in (   "Amex_Gateway" 
                        'Amex_Gateway', 
                        'DineroMail_Api', 
                        'DineroMail_HostedPaymentPage'
                       )
;
