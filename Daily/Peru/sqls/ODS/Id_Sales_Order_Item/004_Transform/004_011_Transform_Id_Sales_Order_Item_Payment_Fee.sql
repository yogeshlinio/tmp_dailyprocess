USE development_pe;


UPDATE development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET
    development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@.PaymentFees = If( IsNull(
                                                    (development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@.PaidPrice + development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingFee) *
                                                    (development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@.Fees/100)
                                                     )  OR
                                       (development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@.PaidPrice + development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingFee) *
         (development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@.Fees/100) = 0,
        development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ExtraCharge/development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ItemsInOrder,
        (development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@.PaidPrice + development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@.ShippingFee)*(development_pe.Id_Sales_Order_Item_Sample_@v_countryPrefix@.Fees/100));
