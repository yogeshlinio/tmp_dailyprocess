#Query: A 125 U Coupon/After Vat coupon for Credit Vouchers
UPDATE  Id_Sales_Order_Item_Sample_@v_countryPrefix@
SET
     Coupon_Value = If( Prefix_Code="CCE",0,
                                                 If( Prefix_Code="PRCcre",0,
                                                 If( Prefix_Code="OPScre",0,
												 If( Prefix_Code="PRCcoup",0,
                                                      CouponValue)))),
    Coupon_Value_After_Tax = If( Prefix_Code="CCE",0,
                                                        If( Prefix_Code="PRCcre",0,
                                                        If( Prefix_Code="OPScre",0,
														If( Prefix_Code="PRCcoup",0,
                                                            CouponValueAfterTax))));


/*
* PATCH ABOUT PAGOEFECT
* Developed by: Carlos Mondragon
* Request by: Daniel Huerta
* Date:       7/10/2013
* Description: Move value of the coupon PAGOEFEC to the column NON_MKT (to be applied into PC3)
*              
*/
DROP TEMPORARY TABLE IF EXISTS TMP_PAGO;
CREATE TEMPORARY TABLE TMP_PAGO ( PRIMARY KEY ( Id_Sales_Order_Item ) )
SELECT
   Id_Sales_Order_Item,
   Coupon_Value
FROM
   Id_Sales_Order_Item_Sample_@v_countryPrefix@
WHERE
    Prefix_Code = "PAGO";

SET @NonMKTCoupon     = 0000000000.0000;
SELECT  ( SUM(Coupon_Value) - 20000  ) /COUNT(*) INTO @NonMKTCoupon FROM TMP_PAGO;

UPDATE        TMP_PAGO
   INNER JOIN Id_Sales_Order_Item_Sample_@v_countryPrefix@
        USING ( Id_Sales_Order_Item )
SET
    #NonMKTCouponAfterTax = @NonMKTCoupon / ( 1 + Tax_Percent / 100 )  , #ADD THIS ONE INTO THE CORRECT A_MASTER FIELD
    Coupon_Value_After_Tax = 0,
    Coupon_Value           = 0,
    Paid_Price             =  Price,
    Paid_Price_After_Tax   =  Price_After_Tax

WHERE
    Prefix_Code = "PAGO";

   





