UPDATE              Sku_Simple_Sample_@v_countryPrefix@ AS Sku_Simple_Sample
         INNER JOIN Sku_Simple_Key_Map
  	          USING ( country , sku_simple )
         INNER JOIN @bob_live@.catalog_attribute_option_global_repurchase
               USING ( id_catalog_attribute_option_global_repurchase )
SET
		repurchase_status = catalog_attribute_option_global_repurchase.name
;