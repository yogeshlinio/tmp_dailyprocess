DROP TEMPORARY TABLE IF EXISTS operations_@v_countryPrefix@.TMP ; 
CREATE TEMPORARY TABLE operations_@v_countryPrefix@.TMP 
SELECT
		b.return_id,
		b. STATUS,
		min(b.changed_at) as cambiado ,
		c.item_id
	FROM
		@v_wmsprod@.inverselogistics_status_history b
	JOIN @v_wmsprod@.inverselogistics_devolucion c ON b.return_id = c.id
WHERE b.STATUS = 2
Group by item_id;

create index item_id on operations_@v_countryPrefix@.TMP(item_id);

UPDATE return_il_Sample_@v_countryPrefix@ as  a
INNER JOIN operations_@v_countryPrefix@.TMP
ON a.item_id = TMP.item_id
SET a.date_first_track_il = TMP.cambiado
WHERE
	TMP. STATUS = 2;



DROP TEMPORARY TABLE IF EXISTS operations_@v_countryPrefix@.TMP2 ; 
CREATE TEMPORARY TABLE operations_@v_countryPrefix@.TMP2 
SELECT
		b.return_id,
		b. STATUS,
		max(b.changed_at) as cambiado ,
		c.item_id
	FROM
		@v_wmsprod@.inverselogistics_status_history b
	JOIN @v_wmsprod@.inverselogistics_devolucion c ON b.return_id = c.id
WHERE b.STATUS = 2
Group by item_id;

create index item_id on operations_@v_countryPrefix@.TMP2(item_id);

UPDATE return_il_Sample_@v_countryPrefix@ as a
INNER JOIN operations_@v_countryPrefix@.TMP2
ON a.item_id = TMP2.item_id
SET a.date_last_track_il = TMP2.cambiado
WHERE
	TMP2. STATUS = 2;

   
