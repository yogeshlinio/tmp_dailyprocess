SELECT  'Inicio rutina CC Survey',now();

use customer_service_pe;

#Horario Colombia

set time_zone='-5:00';

#Datos ultimo d�a
set @max_day=cast((select max(call_date) from bi_ops_cc_survey) as date);


delete from bi_ops_cc_survey
where call_date>=@max_day;

select date(@max_day);

#Ingresar datos a la tabla principal
insert into customer_service_pe.bi_ops_cc_survey
(uniqueid,
call_datetime,
call_date,
agent_name,
agent_ext,
call_time,
call_duration,
callerid,
respuestas,
abandon_in,
answer1_option_1,
answer1_option_2,
answer1_abandon,
answer2_option_1,
answer2_option_2,
answer2_option_3,
answer2_abandon,
answer3_option_1,
answer3_option_2,
answer3_option_3,
answer3_abandon,
answer_1,
answer_2,
answer_3)
Select B.uniqueid,
B.timestamp call_datetime,
date(B.timestamp)call_date,
C.name agent_name,
B.agent agent_ext,
time(B.timestamp) call_time,
B.duration call_duration,
B.phone callerid,
((case when B.id_quest_1=0 then 0 else 1 end)+(case when B.id_quest_2=0 then 0 else 1 end)+(case when B.id_quest_3=0 then 0 else 1 end)) respuestas,
((case when B.id_quest_1=0 then 1 else 0 end)+(case when B.id_quest_2=0 then 1 else 0 end)+(case when B.id_quest_3=0 then 1 else 0 end)) abandon_in,

(case when B.sel_opt_1=1 then 1 else 0 end) answer1_option_1,
(case when B.sel_opt_1=2 then 1 else 0 end) answer1_option_2,
(case when B.id_quest_1=0 then 1 else 0 end) answer1_abandon,

(case when B.sel_opt_2=1 then 1 else 0 end) answer2_option_1,
(case when B.sel_opt_2=2 then 1 else 0 end) answer2_option_2,
(case when B.sel_opt_2=3 then 1 else 0 end) answer2_option_3,
(case when B.id_quest_2=0 then 1 else 0 end) answer2_abandon,

(case when B.sel_opt_3=1 then 1 else 0 end) answer3_option_1,
(case when B.sel_opt_3=2 then 1 else 0 end) answer3_option_2,
(case when B.sel_opt_3=3 then 1 else 0 end) answer3_option_3,
(case when B.id_quest_3=0 then 1 else 0 end) answer3_abandon,

(case when B.id_quest_1=0 then 0 else 1 end) answer_1,
(case when B.id_quest_2=0 then 0 else 1 end) answer_2,
(case when B.id_quest_3=0 then 0 else 1 end) answer_3

from  customer_service_pe.reports B 
left join customer_service_pe.users C on B.agent=C.extension
where 
#qname='1002'
#and action in ('COMPLETEAGENT','COMPLETECALLER') and 
date(B.timestamp)>=@max_day
and B.status is not null;

#Agregar datos a tabla principal
update bi_ops_cc_pe a inner join bi_ops_cc_survey b
on a.queue_stats_id=b.uniqueid
set
a.transfered_survey=1,
a.answered_3=answer_3,
a.good=answer3_option_1,
a.regular=answer3_option_2,
a.answ_1_2=if(answer_1=1 and answer_2=1,1,0),
a.first_contact=answer2_option_1,
a.resolution=answer1_option_1,
a.answered_1=answer_1,
a.answered_2=answer_2
where a.transfered_survey =0;

SELECT  'Rutina finalizada Survey',now();