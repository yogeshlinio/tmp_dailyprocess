USE customer_service_pe;

#Agregar datos genrales tabla de tiempos
DROP  TABLE if EXISTS sampleQueue;

CREATE  TABLE sampleQueue
select a.*, date(FROM_UNIXTIME(timestamp,'%Y-%m-%d')) date, FROM_UNIXTIME(timestamp,'%Y-%m-%d %H:%i:%s') datetime
 from queuelog a  where date(FROM_UNIXTIME(timestamp,'%Y-%m-%d'))>='2014-05-01'
and action in ('ADDMEMBER','REMOVEMEMBER');

#Horas de ingreso
truncate table time_in_office;

insert into time_in_office (date, agent, fecha_entrada)
select date, agent,min(datetime) fecha_entrada from sampleQueue
where agent like '%sip%'
and action='ADDMEMBER'
group by agent,date;

#Horas de salida
update time_in_office a
inner join
(select date, agent,max(datetime) fecha from sampleQueue
where agent like '%sip%'
and action='REMOVEMEMBER'
group by agent,date) b
on a.date=b.date and a.agent=b.agent
set
a.fecha_salida=b.fecha;

#Actualizar tiempo
update time_in_office
set
tiempo_conexion=time_to_sec(TIMEDIFF(fecha_salida, fecha_entrada));