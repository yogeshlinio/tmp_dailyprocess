INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  step,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Peru',
  'customer_service_pe.time_in_office',
  'start',
  NOW(),
  max(Date),
  count(1),
  count(1)
FROM
  customer_service_pe.time_in_office
