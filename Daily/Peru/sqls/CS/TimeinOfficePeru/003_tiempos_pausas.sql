use customer_service_pe;

# CREAR TABLA 1: auxiliary_tbl_pe
set @max_day=cast((select max(date) from auxiliary_tbl_pe)  as date);

delete from auxiliary_tbl_pe
where date>=@max_day;

insert into auxiliary_tbl_pe(date, extension, hour, event2, calldate, id, queue,event)
select date(A.calldate) date,A.src extension,date_format(A.calldate,'%H:%i:%s') hour,
A.dst event2,A.calldate calldate,
A.uniqueid id,W.X queue,
(Case 
when A.dst='*938' then 'LOG' 
when A.dst='*038' then 'DESLOG'
when A.dst='**02' then 'SSHH'
when A.dst='**03' then 'BREAK' 
when A.dst='**04' then 'ALMUERZO'
when A.dst='**05' then 'CAPACITACION' 
when A.dst='**06' then 'PAUSAS ACTIVAS'
when A.dst='**07' then 'FEEDBACK' 
when A.dst='**08' then 'OTROS PROCESOS' end)event

from cdr A

left join (select callid,qname as X from queuelog
where FROM_UNIXTIME(timestamp,'%Y-%m-%d')>=@max_day and
action in ('ADDMEMBER','REMOVEMEMBER'))W on W.callid=A.uniqueid

where date(A.calldate)>=@max_day and A.dst like ('%*%') and A.lastapp='Hangup'
group by 1,2,3,4
;


SELECT  'Inicio pausas de los agentes Pe',now();

#truncate table auxiliary_tbl_adjusted;

set @max_day=cast((select max(date) from auxiliary_tbl_adjusted_pe)  as date);

delete from auxiliary_tbl_adjusted_pe
where date>=@max_day;

#Ingresar datos de tiempos ajustando fecha y ordenados por hora y agente
insert into auxiliary_tbl_adjusted_pe (id, datetime, date, hour, extension, event, queue)
select id, concat(date,' ',hour) as datetime, date, hour, extension, event, queue from
(select id,date(date) date, hour, extension, event, queue
from customer_service_pe.auxiliary_tbl_pe) t
where date>=@max_day
order by extension asc, concat(date,' ',hour) asc ;

truncate table bi_ops_cc_pauses_pe;

#Ingresar diferencias de tiempos
insert into bi_ops_cc_pauses_pe(id_tbl,datetime1, datetime2,datetime3, dif, extension, queue, event)
SELECT f1.id_tbl,f1.datetime datetime1, f2.datetime datetime2, f3.datetime datetime3,
SEC_TO_TIME( UNIX_TIMESTAMP( f3.datetime ) - UNIX_TIMESTAMP( f1.datetime ) ) AS dif,
f1.extension, f1.queue,concat(f1.event,"-",f2.event,"-",f3.event) event
FROM auxiliary_tbl_adjusted_pe f1

inner JOIN auxiliary_tbl_adjusted_pe f2 ON f1.id_tbl +1= f2.id_tbl
and f1.datetime<f2.datetime and date(f1.datetime)=date(f2.datetime)
and f1.extension=f2.extension

inner JOIN auxiliary_tbl_adjusted_pe f3 ON f1.id_tbl +2= f3.id_tbl
and f1.datetime<f3.datetime and date(f1.datetime)=date(f3.datetime)
and f1.extension=f3.extension


GROUP BY f1.datetime,f1.extension
ORDER BY f1.id_tbl;

#Actualizar pausas
update bi_ops_cc_pauses_pe
set
type='SSHH'
where event ='DESLOG-SSHH-LOG';

update bi_ops_cc_pauses_pe
set
type='AMUERZO'
where event ='DESLOG-AMUERZO-LOG';

update bi_ops_cc_pauses_pe
set
type='CAPACITACION'
where event ='DESLOG-CAPACITACION-LOG';

update bi_ops_cc_pauses_pe
set
type='PAUSAS ACTIVAS'
where event ='DESLOG-PAUSAS ACTIVAS-LOG';

update bi_ops_cc_pauses_pe
set
type='FEEDBACK'
where event ='DESLOG-FEEDBACK-LOG';

update bi_ops_cc_pauses_pe
set
type='OTROS PROCESOS'
where event ='DESLOG-OTROS PROCESOS-LOG';

#Actualizar tiempo en segundos
update bi_ops_cc_pauses_pe
set
tiempo_segundos=time_to_sec(dif);

update
 bi_ops_cc_pauses_pe a 
inner join tbl_staff_CC_PE b on a.extension=b.Extension
set
a.nombre=b.Nombre;

SELECT  'Fin pausas de los agentes Pe',now();