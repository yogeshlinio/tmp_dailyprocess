USE marketing_pe;

update channel_report c
join marketing.marketing_vouchers
join marketing.channel cgc
join marketing.channel_group cg
set 
	c.fk_channel = marketing_vouchers.fk_channel
where c.coupon_code like concat(marketing.marketing_vouchers.coupon_code, '%')
and marketing_vouchers.coupon_code_description is null
and marketing_vouchers.coupon_name is null
and cgc.fk_channel_group =  cg.id_channel_group
and marketing_vouchers.fk_channel = cgc.id_channel
and cg.voucher_over_smc = 1;

update channel_report c
join marketing.marketing_vouchers
join marketing.channel cgc
join marketing.channel_group cg
set 
	c.fk_channel = marketing.marketing_vouchers.fk_channel
where c.coupon_code_description like concat(marketing.marketing_vouchers.coupon_code_description, '%')
and (marketing_vouchers.coupon_code is null or marketing_vouchers.coupon_code = '')
and marketing_vouchers.coupon_name is null
and cgc.fk_channel_group =  cg.id_channel_group
and marketing_vouchers.fk_channel = cgc.id_channel
and cg.voucher_over_smc = 1;

update channel_report c
join marketing.marketing_vouchers
join marketing.channel cgc
join marketing.channel_group cg
set 
	c.fk_channel = marketing.marketing_vouchers.fk_channel
where c.coupon_name like concat(marketing.marketing_vouchers.coupon_name, '%')
and (marketing_vouchers.coupon_code is null or marketing_vouchers.coupon_code = '')
and marketing_vouchers.coupon_code_description is null
and cgc.fk_channel_group =  cg.id_channel_group
and marketing_vouchers.fk_channel = cgc.id_channel
and cg.voucher_over_smc = 1;

update channel_report c
join marketing.marketing_vouchers
join marketing.channel cgc
join marketing.channel_group cg
set 
	c.fk_channel = marketing.marketing_vouchers.fk_channel
where c.coupon_code like concat(marketing.marketing_vouchers.coupon_code, '%')
and c.coupon_code_description like concat(marketing.marketing_vouchers.coupon_code_description, '%')
and marketing_vouchers.coupon_name is null
and cgc.fk_channel_group =  cg.id_channel_group
and marketing_vouchers.fk_channel = cgc.id_channel
and cg.voucher_over_smc = 1;

update channel_report c
join marketing.marketing_vouchers
join marketing.channel cgc
join marketing.channel_group cg
set 
	c.fk_channel = marketing.marketing_vouchers.fk_channel
where c.coupon_code like concat(marketing.marketing_vouchers.coupon_code, '%')
and c.coupon_name like concat(marketing.marketing_vouchers.coupon_name, '%')
and marketing_vouchers.coupon_code_description is null
and cgc.fk_channel_group =  cg.id_channel_group
and marketing_vouchers.fk_channel = cgc.id_channel
and cg.voucher_over_smc = 1;

update channel_report c
join marketing.marketing_vouchers
join marketing.channel cgc
join marketing.channel_group cg
set 
	c.fk_channel = marketing.marketing_vouchers.fk_channel
where c.coupon_code_description like concat(marketing.marketing_vouchers.coupon_code_description, '%')
and c.coupon_name like concat(marketing.marketing_vouchers.coupon_name, '%')
and (marketing_vouchers.coupon_code is null or marketing_vouchers.coupon_code = '')
and cgc.fk_channel_group =  cg.id_channel_group
and marketing_vouchers.fk_channel = cgc.id_channel
and cg.voucher_over_smc = 1;

update channel_report c
join marketing.marketing_vouchers
join marketing.channel cgc
join marketing.channel_group cg
set 
	c.fk_channel = marketing.marketing_vouchers.fk_channel
where c.coupon_code_description like concat(marketing.marketing_vouchers.coupon_code_description, '%')
and c.coupon_name like concat(marketing.marketing_vouchers.coupon_name, '%')
and c.coupon_code like concat(marketing.marketing_vouchers.coupon_code, '%')
and cgc.fk_channel_group =  cg.id_channel_group
and marketing_vouchers.fk_channel = cgc.id_channel
and cg.voucher_over_smc = 1;

update channel_report c
set
	fk_channel = 187
where	
	(coupon_code is not null or coupon_code = ''
	OR coupon_code_description is not null or coupon_code_description = ''
	OR coupon_name is not null or coupon_name = '') 
	and (fk_channel is null or fk_channel = 0);

update channel_report c
join marketing.channel
set	
	c.channel = channel.channel
where c.fk_channel = channel.id_channel;