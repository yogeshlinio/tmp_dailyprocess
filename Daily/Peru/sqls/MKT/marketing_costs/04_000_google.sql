USE @marketing@;

set @country := substring('@v_country@',1,3);

#No Linio Fashion
drop temporary table if exists tmp_google;
create temporary table tmp_google
(index(country), index(date), index(source), index(medium), index(campaign), 
index(is_mobile), index(is_fashion), index(profile))
select country, date,  source, medium, campaign, is_mobile, is_fashion, profile, sum(ad_cost)/usd.xr cost
from marketing.ga_performance
inner join development_mx.A_E_BI_ExchangeRate usd
on ga_performance.yrmonth = usd.month_num
and ga_performance.country = usd.country
where ad_cost>0 
and profile<>'liniofashion'
and country = @country
group by date,country,source, medium, campaign, is_mobile, is_fashion, profile;

drop temporary table if exists tmp_google_rev;
create temporary table tmp_google_rev
(index(country), index(date), index(source), index(medium), index(campaign), 
index(is_mobile), index(is_fashion), index(profile))
select country, date,  source, medium, campaign,  is_mobile, is_fashion, profile, sum(gross_revenue) rev
, count(1) uds
from @marketing@.channel_performance
where source = 'google'
and profile<>'liniofashion'
group by date,country,source, medium, campaign, is_mobile, is_fashion, profile;


drop temporary table if exists tmp_cost_rev;
create temporary table tmp_cost_rev
(index(date), index(source), index(medium), index(campaign),
index(is_mobile), index(is_fashion), index(profile))
select t.*, rev, uds
from tmp_google t inner join tmp_google_rev t2
on t.date = t2.date
and t.source = t2.source
and t.medium = t2.medium
and t.campaign = t2.campaign
and t.is_mobile =t2.is_mobile
and t.is_fashion = t2.is_fashion
and t.profile = t2.profile;

UPDATE channel_performance cp
inner join
tmp_cost_rev as t use index(Campaign)
	on  cp.source = t.source
	and cp.medium = t.medium
	and cp.is_fashion = t.is_fashion
	and cp.date = t.date 
	and cp.campaign=t.campaign	
	and cp.is_mobile=t.is_mobile	
	and cp.profile=t.profile
set cp.marketing_cost=if(t.rev = 0, cost/uds, cost*gross_revenue/t.rev)
;

#Linio Fashion
drop temporary table if exists tmp_google;
create temporary table tmp_google
(index(country), index(date), index(source), index(medium), index(campaign), 
index(is_mobile), index(is_fashion), index(profile))
select country, date,  source, medium, campaign, is_mobile, is_fashion, profile, sum(ad_cost)/usd.xr cost
from marketing.ga_performance
inner join development_mx.A_E_BI_ExchangeRate usd
on ga_performance.yrmonth = usd.month_num
and ga_performance.country = usd.country
where ad_cost>0 
and profile='liniofashion'
and country = @country
and (campaign like 'fb.%' or ad_group like '%|f|%' or campaign like '%fashion%')
group by date,country,source, medium, campaign, is_mobile, is_fashion, profile;

drop temporary table if exists tmp_google_rev;
create temporary table tmp_google_rev
(index(country), index(date), index(source), index(medium), index(campaign), 
index(is_mobile), index(is_fashion), index(profile))
select country, date,  source, medium, campaign,  is_mobile, is_fashion, profile, sum(gross_revenue) rev
, count(1) uds
from @marketing@.channel_performance
where source = 'google'
and profile='liniofashion'
and (campaign like 'fb.%' or ad_group like '%|f|%' or campaign like '%fashion%')
group by date,country,source, medium, campaign, is_mobile, is_fashion, profile;


drop temporary table if exists tmp_cost_rev;
create temporary table tmp_cost_rev
(index(date), index(source), index(medium), index(campaign),
index(is_mobile), index(is_fashion), index(profile))
select t.*, rev, uds
from tmp_google t inner join tmp_google_rev t2
on t.date = t2.date
and t.source = t2.source
and t.medium = t2.medium
and t.campaign = t2.campaign
and t.is_mobile =t2.is_mobile
and t.is_fashion = t2.is_fashion
and t.profile = t2.profile;

UPDATE channel_performance cp
inner join
tmp_cost_rev as t use index(Campaign)
	on  cp.source = t.source
	and cp.medium = t.medium
	and cp.is_fashion = t.is_fashion
	and cp.date = t.date 
	and cp.campaign=t.campaign	
	and cp.is_mobile=t.is_mobile	
	and cp.profile=t.profile
set cp.marketing_cost=if(t.rev = 0, cost/uds, cost*gross_revenue/t.rev)
;




