INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Peru', 
  'marketing_pe.channel_report',
  'finish',
  NOW(),
  MAX(date),
  count(*),
  count(*)
FROM
marketing_pe.channel_report
;

