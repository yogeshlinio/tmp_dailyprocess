call daily_routine;


INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Peru',
  'production_pe.daily_routine',
  NOW(),
  NOW(),
  1,
  1
;

