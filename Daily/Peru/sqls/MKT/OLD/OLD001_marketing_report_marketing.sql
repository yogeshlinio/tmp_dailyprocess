call marketing_pe;

INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Peru',
  'marketing_report.marketing_pe',
  NOW(),
  NOW(),
  1,
  1
;

