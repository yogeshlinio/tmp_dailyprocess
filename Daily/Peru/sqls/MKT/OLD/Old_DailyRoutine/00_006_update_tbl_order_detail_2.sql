
#Cupones de devolución

UPDATE tbl_order_detail t SET paid_price = unit_price, paid_price_after_vat = unit_price/1.18
WHERE coupon_code like 'CCE%' 
or coupon_code like 'PRCcre%' 
or coupon_code like 'OPScre%'
or coupon_Code like 'DEP%'
or coupon_Code like 'DEV%';

#incluir nuevos cupones de descuento


#pc2 
select 'start pc2',now();
truncate production_pe.tbl_group_order_detail;
insert into production_pe.tbl_group_order_detail (select orderid, count(order_nr) as items,
sum(greatest(cast(replace(if(isnull(peso),1,peso),',','.') as decimal(21,6)),1.0))as pesototal,
sum(paid_price) as grandtotal
from production_pe.tbl_order_detail where  oac='1' and returned='0' group by order_nr);	

truncate production_pe.tbl_group_order_detail_gross;
insert into production_pe.tbl_group_order_detail_gross (select orderid, count(order_nr) as items,
sum(greatest(cast(replace(if(isnull(peso),1,peso),',','.') as decimal(21,6)),1.0))as pesototal,
sum(paid_price) as grandtotal
from production_pe.tbl_order_detail where  obc='1' group by order_nr);	


update   production_pe.tbl_order_detail as t 
left join production_pe.tbl_group_order_detail as e 
on e.orderid = t.orderid 
set t.orderpeso = e.pesototal , t.nr_items=e.items , t.ordertotal=e.grandtotal
where t.oac='1' and t.returned='0';

update   production_pe.tbl_order_detail as t 
left join production_pe.tbl_group_order_detail_gross as e 
on e.orderid = t.orderid 
set t.orderpeso_gross = e.pesototal , t.gross_items=e.items , t.ordertotal_gross=e.grandtotal
where t.obc='1';

update production_pe.tbl_order_detail as t
set gross_orders = if(gross_items is null,0,1/gross_items) where obc = '1';

##update production_pe.tbl_order_detail as t
##set net_orders = if(nr_items is null, 0,1/nr_items) where oac = '1 'and returned = '0';

-- New Net Order

/*
create table production_pe.temporary_net_orders as select order_nr, count(*) as items from production_pe.tbl_order_detail where oac=1 group by order_nr;

create index order_nr on production_pe.temporary_net_orders(order_nr);

update production_pe.tbl_order_detail t inner join production_pe.temporary_net_orders n on t.order_nr=n.order_nr set t.net_orders=1/n.items;

drop table production_pe.temporary_net_orders;*/

#pc2: shipping_fee
update production_pe.tbl_order_detail set ordershippingfee=0.0 where ordershippingfee is null;
update production_pe.tbl_order_detail set shipping_fee=ordershippingfee/cast(nr_items as decimal) where oac='1' and returned='0';
update tbl_order_detail set shipping_fee_2=ordershippingfee/cast(nr_items as decimal);

update production_pe.tbl_order_detail set shipping_fee_after_vat=if(shipping_fee is null, 0, shipping_fee/1.18);
update tbl_order_detail set shipping_fee_after_vat_2=if(shipping_fee_2 is null, 0, shipping_fee_2/1.18);
update tbl_order_detail set grand_total_after_vat=shipping_fee_after_vat_2+paid_price_after_vat;
update tbl_order_detail set gross_grand_total_after_vat=shipping_fee_after_vat_2+paid_price_after_vat where obc = '1';
update tbl_order_detail set net_grand_total_after_vat=shipping_fee_after_vat+paid_price_after_vat 
where (oac = 1 and returned = 0);



UPDATE production_pe.tbl_order_detail A, bob_live_pe.customer B
   SET A.first_name = B.first_name,
    A.last_name = B.last_name,
    A.gender = B.gender,
    A.birthday = B.birthday,
    A.customer_age = DATEDIFF(CURRENT_DATE, STR_TO_DATE(B.birthday, '%Y-%m-%d'))/365 
WHERE A.custid=B.id_customer;




UPDATE production_pe.tbl_order_detail A
   set A.age_group = case when A.customer_age<15 then "0<x<14"
        when A.customer_age<20 then "15<x<19"
        when A.customer_age<25 then "20<x<24"
        when A.customer_age<31 then "25<x<30"
        when A.customer_age<41 then "31<x<40"
        when A.customer_age<51 then "41<x<50"
        when A.customer_age<61 then "51<x<60"
      else "x>61" end ;



#pc2: cs and wh
#update production_pe.tbl_order_detail set cs=9.9/cast(nr_items as decimal),wh=14.619/cast(nr_items as decimal) 
#where date>='2012-05-01' and oac='1' and returned='0';


#update production_pe.tbl_order_detail set cs=6.402/cast(nr_items as decimal),wh=10.527/cast(nr_items as decimal) 
#where date>='2013-02-01' and oac='1' and returned='0';

#CS and WH Cost_ 

UPDATE tbl_order_detail INNER JOIN tbl_OPS_WH_CS ON tbl_order_detail.month
 = tbl_OPS_WH_CS.month SET tbl_order_detail.wh = tbl_OPS_WH_CS.wh/
tbl_order_detail.nr_items, tbl_order_detail.cs = 
tbl_OPS_WH_CS.cs/tbl_order_detail.nr_items;


/* with Daily Report INFO
update production_pe.tbl_order_detail set cs= 15.74/cast(nr_items as decimal) where date>='2012-05-01' and oac='1' and returned='0';
update production_pe.tbl_order_detail set cs= 15.44/cast(nr_items as decimal) where date>='2012-07-01' and oac='1' and returned='0';
update production_pe.tbl_order_detail set cs= 9.24/cast(nr_items as decimal) where date>='2012-08-01' and oac='1' and returned='0';
update production_pe.tbl_order_detail set cs= 9.90/cast(nr_items as decimal) where date>='2012-09-01' and oac='1' and returned='0';
update production_pe.tbl_order_detail set cs= 8.21/cast(nr_items as decimal) where date>='2013-02-01' and oac='1' and returned='0';
update production_pe.tbl_order_detail set cs= 8.98/cast(nr_items as decimal) where date>='2013-03-01' and oac='1' and returned='0';
update production_pe.tbl_order_detail set cs= 8.98/cast(nr_items as decimal) where date>='2013-05-01' and oac='1' and returned='0';

update production_pe.tbl_order_detail set wh=14.619/cast(nr_items as decimal)where date>='2012-05-01' and oac='1' and returned='0';
update production_pe.tbl_order_detail set wh=12.87/cast(nr_items as decimal)where date>='2012-08-01' and oac='1' and returned='0';
update production_pe.tbl_order_detail set wh=14.619/cast(nr_items as decimal)where date>='2012-09-01' and oac='1' and returned='0';
update production_pe.tbl_order_detail set wh=12.64/cast(nr_items as decimal)where date>='2013-02-01' and oac='1' and returned='0';
update production_pe.tbl_order_detail set wh=6.5/cast(nr_items as decimal)where date>='2013-03-01' and oac='1' and returned='0';
update production_pe.tbl_order_detail set wh=5.247/cast(nr_items as decimal)where date>='2013-04-01' and oac='1' and returned='0';
update production_pe.tbl_order_detail set wh=5.412/cast(nr_items as decimal)where date>='2013-05-01' and oac='1' and returned='0';
*/
/*shipping_cost_new */
/*
UPDATE production_pe.tbl_order_detail LEFT JOIN production_pe.tbl_shipping_cost_per_Kg ON 
round(production_pe.tbl_order_detail.orderpeso) = production_pe.tbl_shipping_cost_per_Kg.kg 
LEFT JOIN production_pe.order_managment ON 
production_pe.tbl_order_detail.item = order_managment.item_id 
SET production_pe.tbl_order_detail.shipping_cost = 
if(production_pe.order_managment.shipping_cost is null,
production_pe.tbl_shipping_cost_per_Kg.shipping_cost,production_pe.order_managment.shipping_cost);
*/

UPDATE production_pe.tbl_order_detail LEFT JOIN production_pe.order_managment ON 
production_pe.tbl_order_detail.item = order_managment.item_id 
SET production_pe.tbl_order_detail.shipping_cost = order_managment.shipping_cost
where production_pe.tbl_order_detail.date <= '2013-01-31';

#UPDATE production_pe.tbl_order_detail LEFT JOIN production_pe.1tbl_shipping_cost_per_kg ON 
#round(production_pe.tbl_order_detail.orderpeso) = production_pe.1tbl_shipping_cost_per_kg.kg 
#LEFT JOIN production_pe.order_managment ON 
#production_pe.tbl_order_managment.item_id = order_managment.item_id 
#SET production_pe.tbl_order_detail.shipping_cost = 
#if(IsNull(order_managment.shipping_cost),
#production_pe.1tbl_shipping_cost_per_kg.shipping_cost,production_pe.order_managment.shipping_cost)
#where production_pe.tbl_order_detail.date >= '2013-01-31';


/* update production_pe.tbl_order_detail inner join production_pe.tbl_catalog_product_v2 
on tbl_order_detail.sku = tbl_catalog_product_v2.sku set shipping_cost = 3.22 where 
product_weight <= 1 and date>='2013-02-01' and date <='2013-02-28';

update production_pe.tbl_order_detail inner join production_pe.tbl_catalog_product_v2 
on tbl_order_detail.sku = tbl_catalog_product_v2.sku set shipping_cost = 3.22*product_weight 
where product_weight > 1 and date>='2013-02-01' and date <='2013-02-28';*/

/*update production_pe.tbl_order_detail inner join production_pe.tbl_catalog_product_v2 
on tbl_order_detail.sku = tbl_catalog_product_v2.sku set shipping_cost = 3.22 where 
date > '2013-02-01' and product_weight <= 1;

update production_pe.tbl_order_detail inner join production_pe.tbl_catalog_product_v2 
on tbl_order_detail.sku = tbl_catalog_product_v2.sku set shipping_cost = 3.22*product_weight 
where product_weight > 1 and date > '2013-02-01' ; */
/*
update production_pe.tbl_order_detail set shipping_cost= 8.872155092/cast(nr_items as decimal) 
where date>='2013-02-01' and oac='1' and returned='0';

update production_pe.tbl_order_detail set shipping_cost= 18.24550717/cast(nr_items as decimal) 
where date>='2013-03-01' and oac='1' and returned='0';

update production_pe.tbl_order_detail set shipping_cost= 12.64688839/cast(nr_items as decimal) 
where date>='2013-04-01' and oac='1' and returned='0';

update production_pe.tbl_order_detail set shipping_cost= 14.21703354/cast(nr_items as decimal) 
where date>='2013-05-01' and oac='1' and returned='0';
*/
#pc2: payment_cost
update production_pe.tbl_order_detail set payment_cost=(ordertotal+ordershippingfee)*0.035/cast(nr_items as decimal) 
where payment_method ='safetypay_payment' and oac='1' and returned='0';

update production_pe.tbl_order_detail set payment_cost=(ordertotal+ordershippingfee)*0.0214/cast(nr_items as decimal)
 where payment_method ='Adyen_HostedPaymentPage' and oac='1' and returned='0';

update production_pe.tbl_order_detail set payment_cost=(ordertotal+ordershippingfee)*0.045/cast(nr_items as decimal)
 where payment_method ='CreditCardOnDelivery_Payment' and oac='1' and returned='0';

update production_pe.tbl_order_detail set payment_cost=(2.8)/cast(nr_items as decimal)
 where payment_method ='AgenciaBCP_Payment' and oac='1' and returned='0';

update production_pe.tbl_order_detail set payment_cost=(2.5)/cast(nr_items as decimal)
 where payment_method ='AgenciaBBVA_Payment' and oac='1' and returned='0';

update production_pe.tbl_order_detail set payment_cost=(ordertotal+ordershippingfee)*0.005/cast(nr_items as decimal)
 where payment_method ='CashOnDelivery_Payment' and oac='1' and returned='0';

update production_pe.tbl_order_detail set payment_cost=(ordertotal+ordershippingfee)*0.0035/cast(nr_items as decimal)
 where payment_method ='VisaNet_HostedPaymentPage' and oac='1' and returned='0';

update production_pe.tbl_order_detail set payment_cost=(ordertotal+ordershippingfee)*0.0375/cast(nr_items as decimal)
 where payment_method ='Pagosonline_LAP' and oac='1' and returned='0';



#wh shipping_cost para drop shipping
update production_pe.tbl_order_detail set wh =0.0, shipping_cost=0.0 
where sku in (select sku from bob_live_pe.catalog_simple where fk_catalog_shipment_type=2);



#null fiedls
update production_pe.tbl_order_detail set shipping_fee=0.0 where shipping_fee is null;
update production_pe.tbl_order_detail set delivery_cost_supplier=0.0 where delivery_cost_supplier is null;
update production_pe.tbl_order_detail set wh=0.0 where wh is null;
update production_pe.tbl_order_detail set cs=0.0 where cs is null;
update production_pe.tbl_order_detail set payment_cost=0.0 where payment_cost is null;
update production_pe.tbl_order_detail set shipping_cost=0.0 where shipping_cost is null;

