
select  'daily routine: start',now();

select  'daily routine: tbl_catalog_product start',now();
#table catalog_product_v2
drop table if exists production_pe.simple_variation;
create table production_pe.simple_variation as (select * from production_pe.simple_variation_view);
alter table production_pe.simple_variation add primary key (fk_catalog_simple) ;

#tbl_catalog_stock
truncate production_pe.tbl_catalog_product_stock;
insert into production_pe.tbl_catalog_product_stock(fk_catalog_simple,sku,reservedbob,stockbob,availablebob)
select catalog_simple.id_catalog_simple,catalog_simple.sku,
if(sum(is_reserved) is null, 0, sum(is_reserved))  as reservedbob ,catalog_stock.quantity as availablebob,
catalog_stock.quantity-if(sum(is_reserved) is null, 0, sum(is_reserved)) as disponible
from 
(bob_live_pe.catalog_simple join bob_live_pe.catalog_stock on catalog_simple.id_catalog_simple=catalog_stock.fk_catalog_simple) left join bob_live_pe.sales_order_item
 on sales_order_item.sku=catalog_simple.sku
group by catalog_simple.sku
order by catalog_stock.quantity desc;
drop table if exists tbl_catalog_product_v3;
create table production_pe.tbl_catalog_product_v3 as (select * from production_pe.catalog_product);
update production_pe.tbl_catalog_product_v3  set visible='no' where cat1 is null;

#actualiza lo recientamente reservado, desde el 01 de enero
update production_pe.tbl_catalog_product_stock inner join (select tbl_catalog_product_stock.fk_catalog_simple,
if(sum(is_reserved) is null, 0, sum(is_reserved))  as recently_reserved
from 
production_pe.tbl_catalog_product_stock inner join bob_live_pe.sales_order_item on tbl_catalog_product_stock.sku=sales_order_item.sku
where date(sales_order_item.created_at)>'2013-01-01' group by fk_catalog_simple) as a 
on  tbl_catalog_product_stock.fk_catalog_simple = a.fk_catalog_simple
set tbl_catalog_product_stock.recently_reservedbob = a.recently_reserved;


alter table production_pe.tbl_catalog_product_v3 add primary key (sku) ;

drop table if exists tbl_catalog_product_v2;
alter table production_pe.tbl_catalog_product_v3 rename to  production_pe.tbl_catalog_product_v2 ;

alter table production_pe.tbl_catalog_product_v2 
add index (sku_config ) ;

select  'daily routine: tbl_catalog_product ok',now();

#special cost
#call special_cost();
select  'daily routine: tbl_order_detail start',now();

call production_pe.update_tbl_order_detail();

select  'daily routine: tbl_order_detail end',now();


#ventas netas mes corrido
/*insert into production_pe.tbl_sales_growth_mom_cat
select date(now() - interval 1 day) date, tab.n1, ventas_netas_t, ventas_netas_t_1, 
cast(((ventas_netas_t/ventas_netas_t_1)-1) as decimal(30,10)) mom from (
select n1,sum(paid_price_after_vat) + sum(if(shipping_fee_after_vat is null, shipping_fee_after_vat,0)) ventas_netas_t
 from production_pe.tbl_order_detail where date>= date(now() - interval day(now()) day) and date <= now() 
 and month(date) = month(now() - interval 1 day) and oac = 1 and returned = 0 group by n1) tab inner join 
(select n1,sum(paid_price_after_vat) + sum(if(shipping_fee_after_vat is null, shipping_fee_after_vat,0)) ventas_netas_t_1
 from production_pe.tbl_order_detail where date<= date(now() - interval 1 month)
 and date >= date(now() - interval 1 month - interval day(now()) day ) 
and month(date) = month(now() - interval 1 day) - 1
 and oac = 1 and returned = 0 group by n1) tab2
on tab.n1 = tab2.n1;

insert into production_pe.tbl_sales_growth_mom_buyer
select date(now() - interval 1 day) date, tab.buyer, ventas_netas_t, ventas_netas_t_1, 
cast(((ventas_netas_t/ventas_netas_t_1)-1) as decimal(30,10)) mom from (
select buyer,sum(paid_price_after_vat) + sum(if(shipping_fee_after_vat is null, shipping_fee_after_vat,0)) ventas_netas_t
 from production_pe.tbl_order_detail where date>= date(now() - interval day(now()) day) and date <= now() 
 and month(date) = month(now() - interval 1 day) and oac = 1 and returned = 0 group by buyer) tab inner join 
(select buyer,sum(paid_price_after_vat) + sum(if(shipping_fee_after_vat is null, shipping_fee_after_vat,0)) ventas_netas_t_1
 from production_pe.tbl_order_detail where date<= date(now() - interval 1 month)
 and date >= date(now() - interval 1 month - interval day(now()) day ) 
and month(date) =  month(now() - interval 1 day) - 1
 and oac = 1 and returned = 0 group by buyer) tab2
on tab.buyer = tab2.buyer;*/

select  'bireporting: start',now();

#sales cube
-- call bireporting.etl_fact_sales_comercial();

select  'bireporting: ok',now();


end