
set itemid integer;
select @last_date:=max(date) from production_pe.tbl_order_detail;
select @last_date_fa:=max(date) from production_pe.ga_cost_campaign where source = 'facebook';
select @last_date_socio:=max(date) from production_pe.ga_cost_campaign where source = 'Sociomantic';

select  'update tbl_order_detail: start',now();

#update status
update production_pe.tbl_order_detail join bob_live_pe.sales_order_item on id_sales_order_item=item join bob_live_pe.sales_order_item_status on fk_sales_order_item_status=id_sales_order_item_status
set status_item= sales_order_item_status.name;

#update obc, oac, pending, cancel, returned
update production_pe.tbl_order_detail join production_pe.status_bob 
on tbl_order_detail.payment_method=status_bob.payment_method 
and tbl_order_detail.status_item=status_bob.status_bob
set tbl_order_detail.obc=status_bob.obc, 
tbl_order_detail.oac=status_bob.oac,
tbl_order_detail.pending=status_bob.pending,
tbl_order_detail.cancel=status_bob.cancel,
tbl_order_detail.returned=status_bob.returned;

#update categories
update production_pe.tbl_order_detail join production_pe.tbl_catalog_product_v2 
on tbl_order_detail.sku=tbl_catalog_product_v2.sku 
set n1=cat1,n2=cat2,n3=cat3;

#insert facebookads and sociomantic
insert into production_pe.ga_cost_campaign(date, source, medium, impressions, adclicks, adcost) 
select date(date_value), 'facebook', 
'socialmedia', sum(if(impressions is null, 0, impressions)) impressions, sum(if(clicks is null, 0, clicks)) clicks, sum(if(cost_mxn is null, 0.,cost_mxn)) cost from production_pe.mkt_facebook_ad_cost where date(date_value) > @last_date_fa
and category = 'Social Media' group by date_value, channel;

insert into production_pe.ga_cost_campaign(date, source, medium, impressions, adclicks, adcost) 
select date(date_value), 'facebook', 
'socialmediaads', sum(if(impressions is null, 0, impressions)) impressions, sum(if(clicks is null, 0, clicks)) clicks, sum(if(cost_mxn is null, 0.,cost_mxn)) cost from production_pe.mkt_facebook_ad_cost where date(date_value) > @last_date_fa
and category <> 'Social Media' group by date_value, channel;

insert into production_pe.ga_cost_campaign(date, source, medium, campaign,impressions, adclicks, adcost) 
select date(date_value), 'Sociomantic', 
'Retargeting', '(not set)', sum(if(impressions is null, 0, impressions)) impressions,
 sum(if(clicks is null, 0, clicks)) clicks, sum(if(cost_mxn is null, 0.,cost_mxn)) cost 
from production_pe.mkt_channel_sociomantic where date(date_value) > @last_date_socio
group by date(date_value);


#insert new data
set itemid = (select item from production_pe.tbl_order_detail order by item desc limit 1);


insert into production_pe.tbl_order_detail (custid,orderid,order_nr,payment_method,unit_price,paid_price,
coupon_money_value,coupon_code,date,hour,sku,item,
status_item,obc,pending,cancel,oac,returned,n1,n2,n3,
tax_percent,unit_price_after_vat,
paid_price_after_vat,coupon_money_after_vat,cost_pet,costo_oferta,costo_after_vat,delivery_cost_supplier,
buyer,brand,product_name,peso,proveedor,ciudad,region,ordershippingfee, about_linio)
(select sales_order.fk_customer as custid,sales_order.id_sales_order as orderid,sales_order.order_nr as order_nr,sales_order.payment_method as payment_method,
sales_order_item.unit_price as unit_price,sales_order_item.paid_price as paid_price,sales_order_item.coupon_money_value as coupon_money_value,sales_order.coupon_code as coupon_code,
cast(sales_order.created_at as date) as date,concat(hour(sales_order.created_at),':',minute(sales_order.created_at),':',second(sales_order.created_at)) as hour,sales_order_item.sku as sku,
sales_order_item.id_sales_order_item as item,
sales_order_item_status.name as status_item,
(select status_bob.obc from production_pe.status_bob 
where ((status_bob.payment_method = sales_order.payment_method) and (sales_order_item_status.name = status_bob.status_bob)) limit 1) as obc,
(select status_bob.pending from production_pe.status_bob where ((status_bob.payment_method = sales_order.payment_method) and 
(sales_order_item_status.name = status_bob.status_bob)) limit 1) as pending,
(select status_bob.cancel from production_pe.status_bob where ((status_bob.payment_method = sales_order.payment_method) and (sales_order_item_status.name = status_bob.status_bob)) limit 1) as cancel,
(select status_bob.oac from production_pe.status_bob where ((status_bob.payment_method = sales_order.payment_method) and (sales_order_item_status.name = status_bob.status_bob)) limit 1) as oac,
(select status_bob.returned from production_pe.status_bob where ((status_bob.payment_method = sales_order.payment_method)
and (sales_order_item_status.name = status_bob.status_bob)) limit 1) as returned,
tbl_catalog_product_v2.cat1 as n1,
tbl_catalog_product_v2.cat2 as n2,
tbl_catalog_product_v2.cat3 as n3,
tbl_catalog_product_v2.tax_percent as tax_percent,
(sales_order_item.unit_price / (1 + (tbl_catalog_product_v2.tax_percent / 100))) as unit_price_after_vat,
(sales_order_item.paid_price / (1 + (tbl_catalog_product_v2.tax_percent / 100))) as paid_price_after_vat,
(sales_order_item.coupon_money_value / (1 + (tbl_catalog_product_v2.tax_percent / 100))) as coupon_money_after_vat,
tbl_catalog_product_v2.cost as cost_pet,
if(tbl_catalog_product_v2.cost<sales_order_item.cost,sales_order_item.cost,0) as costo_oferta,
 (sales_order_item.cost/1.18) as costo_after_vat,
if(isnull(sales_order_item.delivery_cost_supplier),tbl_catalog_product_v2.inbound,
sales_order_item.delivery_cost_supplier) as delivery_cost_supplier,
tbl_catalog_product_v2.buyer as buyer,
tbl_catalog_product_v2.brand as brand,
tbl_catalog_product_v2.product_name as product_name,
tbl_catalog_product_v2.product_weight as peso,
tbl_catalog_product_v2.supplier as proveedor,
if(isnull(sales_order_address.municipality),sales_order_address.city,sales_order_address.municipality) 
as ciudad,
sales_order_address.region as region,
sales_order.shipping_amount as ordershippingfee,
sales_order.about_linio as about_linio
from ((((bob_live_pe.sales_order_item join bob_live_pe.sales_order_item_status) join bob_live_pe.sales_order) 
join production_pe.tbl_catalog_product_v2) join bob_live_pe.sales_order_address) 
where
((sales_order_item.fk_sales_order_item_status = sales_order_item_status.id_sales_order_item_status) 
and (sales_order_item.id_sales_order_item > itemid) and 
(sales_order.id_sales_order = sales_order_item.fk_sales_order) 
and (sales_order.fk_sales_order_address_shipping = sales_order_address.id_sales_order_address)
and (sales_order_item.sku = tbl_catalog_product_v2.sku)
));


update production_pe.tbl_order_detail set sku_config = left(sku, 17);

update production_pe.tbl_order_detail o set 
o.package_weight = 
(select catalog_config.package_weight from bob_live_pe.catalog_config where o.sku_config = catalog_config.sku);

update production_pe.tbl_order_detail set costo_after_vat = cost_pet/1.18 
where costo_after_vat is null;

update tbl_order_detail 
inner join bob_live_pe.sales_order s on s.order_nr = tbl_order_detail.order_nr
inner join bob_live_pe.sales_order_item sa on s.id_sales_order = sa.fk_sales_order and 
sa.id_sales_order_item = tbl_order_detail.item set costo_oferta = (if(sa.cost is null, 0,sa.cost));


update production_pe.tbl_order_detail
set year = year(date) , month = month(date) , yrmonth = concat(year,if(month<10,concat(0,month),month))
where date >=@last_date;
#update source_medium
update production_pe.tbl_order_detail t inner join SEM.transaction_id_pe c on t.order_nr=c.transaction_id set t.source_medium= concat(c.source,' / ',c.medium),t.campaign=c.campaign where t.item>=itemid;

-- Category BP

update production_pe.tbl_order_detail t inner join development_pe.A_E_M1_New_CategoryBP m on m.Cat1=t.new_cat1 set t.category_bp=m.catbp;

update production_pe.tbl_order_detail t inner join development_pe.A_E_M1_New_CategoryBP m on m.Cat2=t.new_cat2 set t.category_bp=m.catbp where (t.new_cat1   =  "Entretenimiento" or t.new_cat1 like "Electr%nicos");

update production_pe.tbl_order_detail
set year = year(date) , month = month(date) , yrmonth = concat(year,if(month<10,concat(0,month),month))
where date >=@last_date;

update tbl_order_detail 
inner join bob_live_pe.catalog_config cc on cc.sku = tbl_order_detail.sku_config
inner join bob_live_pe.catalog_attribute_option_global_category caogc on 
caogc.id_catalog_attribute_option_global_category = cc.fk_catalog_attribute_option_global_category
set new_cat1 = (if(caogc.name is null, 0,caogc.name));

update tbl_order_detail 
inner join bob_live_pe.catalog_config cc on cc.sku = tbl_order_detail.sku_config
inner join bob_live_pe.catalog_attribute_option_global_sub_category caogsc on 
caogsc.id_catalog_attribute_option_global_sub_category = cc.fk_catalog_attribute_option_global_sub_category
set new_cat2 = (if(caogsc.name is null, 0,caogsc.name));

update tbl_order_detail 
inner join bob_live_pe.catalog_config cc on cc.sku = tbl_order_detail.sku_config
inner join bob_live_pe.catalog_attribute_option_global_sub_sub_category caogssc on 
caogssc.id_catalog_attribute_option_global_sub_sub_category = cc.fk_catalog_attribute_option_global_sub_sub_category
set new_cat3 = (if(caogssc.name is null, 0,caogssc.name));

update production_pe.tbl_order_detail inner join bob_live_pe.sales_rule on code = coupon_code
inner join bob_live_pe.sales_rule_set on id_sales_rule_set = fk_sales_rule_set
set description_voucher = description 
where coupon_code is not null;
