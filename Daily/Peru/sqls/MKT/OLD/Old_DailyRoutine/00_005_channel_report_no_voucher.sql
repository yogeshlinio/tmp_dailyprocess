select  'start channel_report_no_voucher: start',now();

#Channel Report PERU---

#ga_cost_Campaign
select @last_date:=max(date) from ga_cost_campaign where source = 'vizury';

insert into ga_cost_campaign(date, source, medium, campaign, adCost)
SELECT date, "Vizury" as source, "Retargeting" as medium, "(not set)" as campaign, (sum(paid_price_after_vat) +sum(shipping_fee_after_vat) )*0.08 as cost
from tbl_order_detail
where source_medium like 'vizury /%' and obc = 1 and date > @last_date
group by date
order by date desc;

select @last_date:=max(date) from ga_cost_campaign where source like 'VEInteractive%';

insert into ga_cost_campaign(date, source, medium, campaign, adCost)
SELECT date, "VEInteractive" as source, "Retargeting" as medium, "(not set)" as campaign, (sum(paid_price_after_vat) +sum(shipping_fee_after_vat) )*0.085 as cost
from tbl_order_detail
where `source_medium` like 'VEInteractive%' and obc = 1 and date > @last_date
group by date
order by date desc;

#ga_visits_cost_source_medium

#Define channel---
update ga_visits_cost_source_medium set source_medium = concat( source, ' / ', medium) where source_medium is null;

#NEW REGISTERS
UPDATE ga_visits_cost_source_medium SET channel='New Register' 
WHERE (source_medium like 'NL%' OR source_medium like 'NR%' OR source_medium like
'COM%' OR source_medium like 'BNR%') AND source_medium is null;
#BLOG
UPDATE ga_visits_cost_source_medium SET channel='Blog Linio' WHERE source_medium like 'blog%' 
OR source_medium='blog.linio.com.pe / referral' or source_medium='blogs.peru21.pe / referral';

#REFERRAL
UPDATE ga_visits_cost_source_medium SET channel='Referral Sites' WHERE source_medium like '%referral' 
AND source_medium<>'facebook.com / referral'
AND source_medium<>'m.facebook.com / referral'
AND source_medium<>'linio.com.co / referral'
AND source_medium<>'linio.com / referral'
AND source_medium<>'linio.com.mx / referral'
AND source_medium<>'google.co.ve / referral'
AND source_medium<>'linio.com.ve / referral'
AND source_medium<>'linio.com.pe / referral'
AND source_medium<>'www-staging.linio.com.ve / referral'
AND source_medium<>'google.com / referral'
AND source_medium<>'.com.ve / referral'
AND source_medium<>'blog.linio.com.ve / referral' AND source_medium<>'t.co / referral'
AND source_medium not like '%google%' AND source_medium not like '%blog%';


UPDATE ga_visits_cost_source_medium SET channel='Buscape' 
WHERE source_medium='Buscape / Price Comparison';

#FB ADS
UPDATE ga_visits_cost_source_medium SET channel='Facebook Ads' WHERE source_medium='facebook / socialmediaads';

#RETARGETING
UPDATE ga_visits_cost_source_medium SET channel='Sociomantic' WHERE source_medium='sociomantic / (not set)' OR source_medium='sociomantic / retargeting';
UPDATE ga_visits_cost_source_medium SET channel='Cart Recovery' WHERE source_medium='Cart_Recovery / CRM';
UPDATE ga_visits_cost_source_medium SET channel='Vizury' WHERE source_medium='vizury / retargeting' OR source_medium like '%vizury%';
UPDATE ga_visits_cost_source_medium SET channel = 'Facebook R.' WHERE (source  like 'face%' or source like '%book') and medium like 'retargeting%';
UPDATE ga_visits_cost_source_medium SET channel = 'Facebook R.' WHERE (source  = 'RetargetingRM');
UPDATE ga_visits_cost_source_medium SET channel='VEInteractive' WHERE source like '%VEInteractive%';
UPDATE ga_visits_cost_source_medium SET channel='GDN Retargeting' WHERE source_medium='google / cpc' AND campaign like '[D[R%';

#SOCIAL MEDIA
UPDATE ga_visits_cost_source_medium SET channel='Facebook Referral' WHERE 
(source_medium='facebook.com / referral' OR source_medium='m.facebook.com / referral') AND source_medium not like '%socialmediaads%';
UPDATE ga_visits_cost_source_medium SET channel='Twitter Referral' WHERE source_medium='t.co / referral' OR source_medium='twitter / socialmedia';
UPDATE ga_visits_cost_source_medium SET channel='FB Posts' WHERE source_medium='facebook / socialmedia';
update ga_visits_cost_source_medium set channel = 'FB Posts' where (source like 'face%' or source like '%book') and medium like 'social%media';
UPDATE ga_visits_cost_source_medium SET channel = 'FB Posts' WHERE source = 'SocialMedia' and medium = 'FacebookVoucher';
#SERVICIO AL CLIENTE
UPDATE ga_visits_cost_source_medium SET channel='Inbound' WHERE source_medium like 'Tele%';
UPDATE ga_visits_cost_source_medium SET channel='OutBound' WHERE source_medium like 'REC%';
#SEO
#SEO
UPDATE ga_visits_cost_source_medium SET channel='Search Organic' WHERE source_medium like '%organic' OR source_medium='google.com.co / referral'
OR source_medium like 'google.%referral%';
UPDATE ga_visits_cost_source_medium SET channel='Search Organic' WHERE source like '%google%com%' and medium like 'referral%';
update ga_visits_cost_source_medium set channel = 'Search Organic' where medium like 'organic%';

#SEM - GDN

UPDATE ga_visits_cost_source_medium SET channel='SEM' WHERE source_medium='google / cpc' 
AND campaign not like 'brand%';
UPDATE ga_visits_cost_source_medium SET channel='SEM Branded' WHERE source_medium='google / cpc' 
AND campaign like 'brand%' ;
UPDATE ga_visits_cost_source_medium SET channel='GDN' WHERE source_medium='google / cpc' AND campaign not like 'b.%' AND campaign like 'r.%';
UPDATE ga_visits_cost_source_medium SET channel='GDN' WHERE source like '%.doubleclick.net%';
UPDATE ga_visits_cost_source_medium SET channel='GDN' WHERE source_medium='google / cpc' AND campaign like '[D[D%';
#NEWSLETTER,
UPDATE ga_visits_cost_source_medium set Channel='Newsletter' WHERE (source_medium like '%CRM' OR source_medium like '%email') AND 
(source<>'TeleSales / CRM' AND source_medium<>'CDEAL / email' AND source_medium<>'Cart_Recovery / CRM');

#OTHER
UPDATE ga_visits_cost_source_medium SET channel='Exchange' WHERE source_medium like 'CCE%';
UPDATE ga_visits_cost_source_medium SET channel='Content Mistake' WHERE source_medium like 'CON%';
UPDATE ga_visits_cost_source_medium SET channel='Procurement Mistake' WHERE source_medium like 'PRC%';
UPDATE ga_visits_cost_source_medium SET channel='Employee Vouchers' WHERE source_medium like 'TE%' OR source_medium like 'EA%'
 OR (source_medium like 'LIN%' AND source_medium not like '%linio%');

#PARTNERSHIPS
UPDATE ga_visits_cost_source_medium SET channel='Seguros Pacifico' WHERE source_medium like 'CDEALpac%';
UPDATE ga_visits_cost_source_medium SET channel='Romero' WHERE source_medium like 'CDEALrom%';
UPDATE ga_visits_cost_source_medium SET channel='Belcorp' WHERE source_medium like 'CDEALbc%';
UPDATE ga_visits_cost_source_medium SET channel='Email Deal' WHERE source_medium like 'CDEAL / email';
update ga_visits_cost_source_medium set channel = source where medium like 'Partne%';

update ga_visits_cost_source_medium set channel = 'BBVA' where source like 'CDEAL%';
UPDATE ga_visits_cost_source_medium SET channel='Tracking Villa María' WHERE source_medium='CDEALCDEALVMM' OR source_medium='CDEALVMM';
UPDATE ga_visits_cost_source_medium SET channel='BBVA' WHERE source like '%BBVA%' or campaign like '%BBVA%' or medium like '%BBVA%';
UPDATE ga_visits_cost_source_medium SET channel='BBVA' WHERE (source_medium like 'CDEAL%' OR source_medium='MKT3M0' OR
source_medium='MKT09h' OR
source_medium='MKT0dV' OR
source_medium='MKT0hG' OR
source_medium='MKT0mf' OR
source_medium='MKT0Nh' OR
source_medium='MKT0ni' OR
source_medium='MKT0Q8' OR
source_medium='MKT0rg' OR
source_medium='MKT0t5' OR
source_medium='MKT0VM' OR
source_medium='MKT12' OR
source_medium='MKT17H' OR
source_medium='MKT17u' OR
source_medium='MKT18i' OR
source_medium='MKT1CU' OR
source_medium='MKT1DD' OR
source_medium='MKT1L9' OR
source_medium='MKT1n7' OR
source_medium='MKT1NG' OR
source_medium='MKT1Ql' OR
source_medium='MKT1TF' OR
source_medium='MKT1uA' OR
source_medium='MKT35s' OR
source_medium='MKT6Fy' OR
source_medium='MKT7FG' OR
source_medium='MKT8PF' OR
source_medium='MKT8pI' OR
source_medium='MKT9Ki' OR
source_medium='MKT9xQ' OR
source_medium='MKTApv' OR
source_medium='MKTauE' OR
source_medium='MKTBBVAvm4' OR
source_medium='MKTbKL' OR
source_medium='MKTEhO' OR
source_medium='MKTFEv' OR
source_medium='MKTFww' OR
source_medium='MKTiQS' OR
source_medium='MKTm1V' OR
source_medium='MKTmbS' OR
source_medium='MKTMpH' OR
source_medium='MKTtest' OR
source_medium='MKTtFM' OR
source_medium='MKTUvb' OR
source_medium='MKTuWQ' OR
source_medium='MKTzmJ') AND channel='';
UPDATE ga_visits_cost_source_medium SET channel='Edifica' WHERE source_medium like 'CDEALed%';
#DIRECT
UPDATE ga_visits_cost_source_medium SET channel='Linio.com Referral' WHERE  
source_medium='linio.com.co / referral' OR
source_medium='linio.com / referral' OR
source_medium='linio.com.mx / referral' OR
source_medium='linio.com.ve / referral' OR
source_medium='linio.com.pe / referral' OR
source_medium='www-staging.linio.com.ve / referral';

UPDATE ga_visits_cost_source_medium SET channel='Directo / Typed' WHERE source_medium='(direct) / (none)' OR 
source_medium like '%direct%none%';

UPDATE ga_visits_cost_source_medium SET channel='Directo / Typed' WHERE source like 'Linio%' and medium like 'banner%' ;

#OFFLINE
UPDATE ga_visits_cost_source_medium SET channel='PR vouchers' WHERE channel like 'PR%';

#CORPORATE SALES

#OFFLINE

#N/A
UPDATE 
ga_visits_cost_source_medium SET channel='Unknown-No voucher' 
WHERE source_medium='';
UPDATE ga_visits_cost_source_medium SET 
channel='Unknown-Voucher' WHERE channel='' AND source<>'';

#Channel Report Extra Stuff to define Channel---
#Venta corporativa 



#Partnerships Channel Extra


#Mercado libre Orders 


#DEFINE CHANNEL GROUP

#Facebook Ads Group
UPDATE ga_visits_cost_source_medium SET channel_group='Facebook Ads' WHERE channel='Facebook Ads';

#SEM Group
UPDATE ga_visits_cost_source_medium SET channel_group='Search Engine Marketing' WHERE channel='SEM';

#Social Media Group
UPDATE ga_visits_cost_source_medium SET 
channel_group='Social Media' WHERE channel='Facebook Referral' 
OR channel='Twitter Referral' or channel='FB Posts';

#Retargeting Group
UPDATE ga_visits_cost_source_medium SET channel_group='Retargeting' WHERE channel='Sociomantic' or source_medium like '%retargeting' OR 
channel='Cart Recovery' OR channel='Vizury' or channel = 'Facebook R.'  OR channel = 'VEInteractive' OR channel = 'GDN Retargeting';

#GDN group
UPDATE ga_visits_cost_source_medium SET channel_group='Google Display Network' WHERE channel like 'GDN'; 

#NewsLetter Group
UPDATE ga_visits_cost_source_medium SET channel_group='NewsLetter' WHERE channel='Newsletter';

#SEO Group
UPDATE ga_visits_cost_source_medium SET channel_group='Search Engine Optimization' WHERE channel='Search Organic';

#Partnerships Group

UPDATE ga_visits_cost_source_medium SET channel_group='Partnerships' WHERE channel='Seguros Pacifico' OR
channel='Romero' OR channel='Email Deal' OR channel='CDEAL' OR channel='BBVA' OR channel='Belcorp';

#Corporate Sales Group
UPDATE ga_visits_cost_source_medium SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';

#Offline Group
UPDATE ga_visits_cost_source_medium SET channel_group='Offline Marketing' WHERE channel='PR vouchers';

#Tele Sales Group
UPDATE ga_visits_cost_source_medium SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales';

#Branded Group
UPDATE ga_visits_cost_source_medium SET channel_group='Branded' 
WHERE channel='Linio.com Referral' OR channel='Directo / Typed' OR
channel='SEM Branded';

#ML Group
UPDATE ga_visits_cost_source_medium SET channel_group='Mercado Libre' WHERE channel='Mercado Libre Referral' OR channel='Mercado Libre Voucher' OR channel='Otros - Mercado Libre';

#Blog Group
UPDATE ga_visits_cost_source_medium SET channel_group='Blogs' WHERE channel='Blog Linio';

#Affiliates Group
UPDATE ga_visits_cost_source_medium SET channel_group='Affiliates' WHERE channel='Buscape';


#Other (identified) Group
UPDATE ga_visits_cost_source_medium SET channel_group='Other (identified)' 
WHERE channel='Referral Sites' OR channel='Reactivation Letters' 
OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' OR
channel='Exchange' OR
channel='Content Mistake' OR
channel='Procurement Mistake' OR
channel='Employee Voucher';

#Unknown Group
UPDATE ga_visits_cost_source_medium SET channel_group='Non identified' WHERE channel='Unknown-No voucher' OR channel='Unknown-Voucher';





#Define Channel Type---
#TO BE DONE

#ga_cost_campaign

#Define channel---
update ga_cost_campaign set source_medium = concat( source, ' / ', medium) where source_medium is null;

#NEW REGISTERS
UPDATE ga_cost_campaign SET channel='New Register' 
WHERE (source_medium like 'NL%' OR source_medium like 'NR%' OR source_medium like
'COM%' OR source_medium like 'BNR%') AND source_medium is null;
#BLOG
UPDATE ga_cost_campaign SET channel='Blog Linio' WHERE source_medium like 'blog%' 
OR source_medium='blog.linio.com.pe / referral' or source_medium='blogs.peru21.pe / referral';

#REFERRAL
UPDATE ga_cost_campaign SET channel='Referral Sites' WHERE source_medium like '%referral' 
AND source_medium<>'facebook.com / referral'
AND source_medium<>'m.facebook.com / referral'
AND source_medium<>'linio.com.co / referral'
AND source_medium<>'linio.com / referral'
AND source_medium<>'linio.com.mx / referral'
AND source_medium<>'google.co.ve / referral'
AND source_medium<>'linio.com.ve / referral'
AND source_medium<>'linio.com.pe / referral'
AND source_medium<>'www-staging.linio.com.ve / referral'
AND source_medium<>'google.com / referral'
AND source_medium<>'.com.ve / referral'
AND source_medium<>'blog.linio.com.ve / referral' AND source_medium<>'t.co / referral'
AND source_medium not like '%google%' AND source_medium not like '%blog%';


UPDATE ga_cost_campaign SET channel='Buscape' 
WHERE source_medium='Buscape / Price Comparison';

#FB ADS
UPDATE ga_cost_campaign SET channel='Facebook Ads' WHERE source_medium='facebook / socialmediaads';

#RETARGETING
UPDATE ga_cost_campaign SET channel='Sociomantic' WHERE source_medium='sociomantic / (not set)' OR source_medium='sociomantic / retargeting';
UPDATE ga_cost_campaign SET channel='Cart Recovery' WHERE source_medium='Cart_Recovery / CRM';
UPDATE ga_cost_campaign SET channel='Vizury' WHERE source_medium='vizury / retargeting' OR source_medium like '%vizury%';
UPDATE ga_cost_campaign SET channel = 'Facebook R.' WHERE (source  like 'face%' or source like '%book') and medium like 'retargeting%';
UPDATE ga_cost_campaign SET channel = 'Facebook R.' WHERE (source  = 'RetargetingRM');
UPDATE ga_cost_campaign SET channel='VEInteractive' WHERE source like '%VEInteractive%';
UPDATE ga_cost_campaign SET channel='GDN Retargeting' WHERE source_medium='google / cpc' AND campaign like '[D[R%';

#SOCIAL MEDIA
UPDATE ga_cost_campaign SET channel='Facebook Referral' WHERE 
(source_medium='facebook.com / referral' OR source_medium='m.facebook.com / referral') AND source_medium not like '%socialmediaads%';
UPDATE ga_cost_campaign SET channel='Twitter Referral' WHERE source_medium='t.co / referral' OR source_medium='twitter / socialmedia';
UPDATE ga_cost_campaign SET channel='FB Posts' WHERE source_medium='facebook / socialmedia';
update ga_cost_campaign set channel = 'FB Posts' where (source like 'face%' or source like '%book') and medium like 'social%media';
UPDATE ga_cost_campaign SET channel = 'FB Posts' WHERE source = 'SocialMedia' and medium = 'FacebookVoucher';
#SERVICIO AL CLIENTE
UPDATE ga_cost_campaign SET channel='Inbound' WHERE source_medium like 'Tele%';
UPDATE ga_cost_campaign SET channel='OutBound' WHERE source_medium like 'REC%';
#SEO
#SEO
UPDATE ga_cost_campaign SET channel='Search Organic' WHERE source_medium like '%organic' OR source_medium='google.com.co / referral'
OR source_medium like 'google.%referral%';
UPDATE ga_cost_campaign SET channel='Search Organic' WHERE source like '%google%com%' and medium like 'referral%';
update ga_cost_campaign set channel = 'Search Organic' where medium like 'organic%';

#SEM - GDN
UPDATE ga_cost_campaign SET channel='SEM Branded' WHERE source_medium='google / cpc' AND (campaign like 'brand%');
UPDATE ga_cost_campaign SET channel='SEM' WHERE source_medium='google / cpc' AND (campaign not like 'brand%');
UPDATE ga_cost_campaign SET channel='GDN' WHERE source_medium='google / cpc' AND campaign not like 'b.%' AND campaign like 'r.%';
UPDATE ga_cost_campaign SET channel='GDN' WHERE source like '%.doubleclick.net%';
UPDATE ga_cost_campaign SET channel='GDN' WHERE source_medium='google / cpc' AND campaign like '[D[D%';
#NEWSLETTER,
UPDATE ga_cost_campaign set Channel='Newsletter' WHERE (source_medium like '%CRM' OR source_medium like '%email') AND 
(source<>'TeleSales / CRM' AND source_medium<>'CDEAL / email' AND source_medium<>'Cart_Recovery / CRM');

#OTHER
UPDATE ga_cost_campaign SET channel='Exchange' WHERE source_medium like 'CCE%';
UPDATE ga_cost_campaign SET channel='Content Mistake' WHERE source_medium like 'CON%';
UPDATE ga_cost_campaign SET channel='Procurement Mistake' WHERE source_medium like 'PRC%';
UPDATE ga_cost_campaign SET channel='Employee Vouchers' WHERE source_medium like 'TE%' OR source_medium like 'EA%'
 OR (source_medium like 'LIN%' AND source_medium not like '%linio%');

#PARTNERSHIPS
UPDATE ga_cost_campaign SET channel='Seguros Pacifico' WHERE source_medium like 'CDEALpac%';
UPDATE ga_cost_campaign SET channel='Romero' WHERE source_medium like 'CDEALrom%';
UPDATE ga_cost_campaign SET channel='Belcorp' WHERE source_medium like 'CDEALbc%';
UPDATE ga_cost_campaign SET channel='Email Deal' WHERE source_medium like 'CDEAL / email';
update ga_cost_campaign set channel = source where medium like 'Partne%';

update ga_cost_campaign set channel = 'BBVA' where source like 'CDEAL%';
UPDATE ga_cost_campaign SET channel='Tracking Villa María' WHERE source_medium='CDEALCDEALVMM' OR source_medium='CDEALVMM';
UPDATE ga_cost_campaign SET channel='BBVA' WHERE source like '%BBVA%' or campaign like '%BBVA%' or medium like '%BBVA%';
UPDATE ga_cost_campaign SET channel='BBVA' WHERE (source_medium like 'CDEAL%' OR source_medium='MKT3M0' OR
source_medium='MKT09h' OR
source_medium='MKT0dV' OR
source_medium='MKT0hG' OR
source_medium='MKT0mf' OR
source_medium='MKT0Nh' OR
source_medium='MKT0ni' OR
source_medium='MKT0Q8' OR
source_medium='MKT0rg' OR
source_medium='MKT0t5' OR
source_medium='MKT0VM' OR
source_medium='MKT12' OR
source_medium='MKT17H' OR
source_medium='MKT17u' OR
source_medium='MKT18i' OR
source_medium='MKT1CU' OR
source_medium='MKT1DD' OR
source_medium='MKT1L9' OR
source_medium='MKT1n7' OR
source_medium='MKT1NG' OR
source_medium='MKT1Ql' OR
source_medium='MKT1TF' OR
source_medium='MKT1uA' OR
source_medium='MKT35s' OR
source_medium='MKT6Fy' OR
source_medium='MKT7FG' OR
source_medium='MKT8PF' OR
source_medium='MKT8pI' OR
source_medium='MKT9Ki' OR
source_medium='MKT9xQ' OR
source_medium='MKTApv' OR
source_medium='MKTauE' OR
source_medium='MKTBBVAvm4' OR
source_medium='MKTbKL' OR
source_medium='MKTEhO' OR
source_medium='MKTFEv' OR
source_medium='MKTFww' OR
source_medium='MKTiQS' OR
source_medium='MKTm1V' OR
source_medium='MKTmbS' OR
source_medium='MKTMpH' OR
source_medium='MKTtest' OR
source_medium='MKTtFM' OR
source_medium='MKTUvb' OR
source_medium='MKTuWQ' OR
source_medium='MKTzmJ') AND channel='';
UPDATE ga_cost_campaign SET channel='Edifica' WHERE source_medium like 'CDEALed%';
#DIRECT
UPDATE ga_cost_campaign SET channel='Linio.com Referral' WHERE  
source_medium='linio.com.co / referral' OR
source_medium='linio.com / referral' OR
source_medium='linio.com.mx / referral' OR
source_medium='linio.com.ve / referral' OR
source_medium='linio.com.pe / referral' OR
source_medium='www-staging.linio.com.ve / referral';

UPDATE ga_cost_campaign SET channel='Directo / Typed' WHERE source_medium='(direct) / (none)' OR 
source_medium like '%direct%none%';

UPDATE ga_cost_campaign SET channel='Directo / Typed' WHERE source like 'Linio%' and medium like 'banner%' ;

#OFFLINE
UPDATE ga_cost_campaign SET channel='PR vouchers' WHERE channel like 'PR%';

#CORPORATE SALES

#OFFLINE

#N/A
UPDATE 
ga_cost_campaign SET channel='Unknown-No voucher' 
WHERE source_medium='';
UPDATE ga_cost_campaign SET 
channel='Unknown-Voucher' WHERE channel='' AND source<>'';

#Channel Report Extra Stuff to define Channel---
#Venta corporativa 



#Partnerships Channel Extra


#Mercado libre Orders 


#DEFINE CHANNEL GROUP

#Facebook Ads Group
UPDATE ga_cost_campaign SET channel_group='Facebook Ads' WHERE channel='Facebook Ads';

#SEM Group
UPDATE ga_cost_campaign SET channel_group='Search Engine Marketing' WHERE channel='SEM';

#Social Media Group
UPDATE ga_cost_campaign SET 
channel_group='Social Media' WHERE channel='Facebook Referral' 
OR channel='Twitter Referral' or channel='FB Posts';

#Retargeting Group
UPDATE ga_cost_campaign SET channel_group='Retargeting' WHERE channel='Sociomantic' or source_medium like '%retargeting' OR 
channel='Cart Recovery' OR channel='Vizury' or channel = 'Facebook R.'  OR channel = 'VEInteractive' OR channel = 'GDN Retargeting';

#GDN Group
UPDATE ga_cost_campaign SET channel_group='Google Display Network' WHERE channel like 'GDN'; 

#NewsLetter Group
UPDATE ga_cost_campaign SET channel_group='NewsLetter' WHERE channel='Newsletter';

#SEO Group
UPDATE ga_cost_campaign SET channel_group='Search Engine Optimization' WHERE channel='Search Organic';

#Partnerships Group

UPDATE ga_cost_campaign SET channel_group='Partnerships' WHERE channel='Seguros Pacifico' OR
channel='Romero' OR channel='Email Deal' OR channel='CDEAL' OR channel='BBVA' OR channel='Belcorp';

#Corporate Sales Group
UPDATE ga_cost_campaign SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';

#Offline Group
UPDATE ga_cost_campaign SET channel_group='Offline Marketing' WHERE channel='PR vouchers';

#Tele Sales Group
UPDATE ga_cost_campaign SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales';

#Branded Group
UPDATE ga_cost_campaign SET channel_group='Branded' 
WHERE channel='Linio.com Referral' OR channel='Directo / Typed' OR
channel='SEM Branded';

#ML Group
UPDATE ga_cost_campaign SET channel_group='Mercado Libre' WHERE channel='Mercado Libre Referral' OR channel='Mercado Libre Voucher' OR channel='Otros - Mercado Libre';

#Blog Group
UPDATE ga_cost_campaign SET channel_group='Blogs' WHERE channel='Blog Linio';

#Affiliates Group
UPDATE ga_cost_campaign SET channel_group='Affiliates' WHERE channel='Buscape';


#Other (identified) Group
UPDATE ga_cost_campaign SET channel_group='Other (identified)' 
WHERE channel='Referral Sites' OR channel='Reactivation Letters' 
OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' OR
channel='Exchange' OR
channel='Content Mistake' OR
channel='Procurement Mistake' OR
channel='Employee Voucher';

#Unknown Group
UPDATE ga_cost_campaign SET channel_group='Non identified' WHERE channel='Unknown-No voucher' OR channel='Unknown-Voucher';

select  'start channel_report_no_voucher: end',now();
