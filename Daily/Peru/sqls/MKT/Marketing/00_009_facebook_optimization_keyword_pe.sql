
-- Peru
-- Facebook Optimization Region

set @days:=30;

update facebook.ga_facebook_ads_keyword_pe set campaign = replace(campaign, '_', '.');

update facebook.ga_facebook_transaction_id_keyword_pe set campaign = replace(campaign, '_', '.');

delete from facebook.facebook_optimization_keyword_pe where datediff(curdate(), date)<@days;

insert into facebook.facebook_optimization_keyword_pe (date, campaign, keyword, country, region, city, obc_transactions, visits, gross_revenue, carts) select date, campaign, keyword, country, region, city, transactions, visits, transaction_revenue, goal2starts from facebook.ga_facebook_ads_keyword_pe where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=facebook.week_iso(date) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe f set f.oac_transactions = (select count(distinct z.transaction_id) from production_pe.tbl_order_detail a, facebook.ga_facebook_transaction_id_keyword_pe z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.keyword=z.keyword and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and a.oac=1) where datediff(curdate(), date)<@days;

create table facebook.temporary_pe as select r.date, r.campaign, r.keyword, r.country, r.region, r.city, (select t.custid from production_pe.tbl_order_detail t where t.order_nr=r.transaction_id and oac=1 group by t.custid) as custid, r.transaction_id, (select sum(t.paid_price) from production_pe.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as paid_price, (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) from production_pe.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as PC2, (select sum(new_customers) from production_pe.tbl_order_detail z where z.order_nr=r.transaction_id and z.oac=1 group by z.order_nr) as new_customers from facebook.ga_facebook_transaction_id_keyword_pe r where datediff(curdate(), date)<@days group by transaction_id;

-- Back Cohort

create table facebook.temporary_back_cohort_pe(
date date,
campaign varchar(255),
keyword varchar(255),
country varchar(255),
region varchar(255),
city varchar(255),
custid int,
transaction_id varchar(255),
oac_transactions_30_cohort float,
oac_transactions_60_cohort float,
paid_price_30_cohort float,
paid_price_60_cohort float,
PC2_30_cohort float,
PC2_60_cohort float,
repeated int
);


create index custid on facebook.temporary_back_cohort_pe(custid);

create index all_index on facebook.temporary_back_cohort_pe(date, campaign, country, city);

insert into facebook.temporary_back_cohort_pe (date, campaign, keyword, country, region, city, custid, transaction_id) select date, campaign, keyword, country, region, city, custid, transaction_id from facebook.temporary_pe where new_customers is not null and datediff(curdate(), date)<@days;

create table facebook.backup_temporary_back_cohort_pe as select * from facebook.temporary_back_cohort_pe;

update facebook.temporary_back_cohort_pe a set repeated = (select count(*) as total from facebook.backup_temporary_back_cohort_pe b where a.date=b.date and a.campaign=b.campaign and a.country=b.country and a.region=b.region and a.city=b.city and a.keyword=b.keyword  group by date, campaign, keyword, country, region, city, custid having count(*)>1);

update facebook.temporary_back_cohort_pe a set repeated = 1 where repeated is null;

drop table facebook.backup_temporary_back_cohort_pe;

update facebook.temporary_back_cohort_pe b set oac_transactions_30_cohort = (select count(distinct t.order_nr) as oac_transactions from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe b set paid_price_30_cohort = (select sum(t.paid_price) as paid_price from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe b set PC2_30_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe b set oac_transactions_60_cohort = (select count(distinct t.order_nr) as oac_transactions from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe b set paid_price_60_cohort = (select sum(t.paid_price) as paid_price from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe b set PC2_60_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe set oac_transactions_30_cohort=oac_transactions_30_cohort/repeated, paid_price_30_cohort=paid_price_30_cohort/repeated, PC2_30_cohort=PC2_30_cohort/repeated, oac_transactions_60_cohort=oac_transactions_60_cohort/repeated, paid_price_60_cohort=paid_price_60_cohort/repeated, PC2_60_cohort=PC2_60_cohort/repeated where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe f set oac_transactions_30_cohort = (select sum(oac_transactions_30_cohort) from facebook.temporary_back_cohort_pe b where f.date=b.date and f.campaign=b.campaign and f.keyword=b.keyword and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe f set cum_net_rev_30_cohort = (select sum(paid_price_30_cohort) from facebook.temporary_back_cohort_pe b where f.date=b.date and f.campaign=b.campaign and f.keyword=b.keyword and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe f set PC2_30_cohort = (select sum(PC2_30_cohort) from facebook.temporary_back_cohort_pe b where f.date=b.date and f.campaign=b.campaign and f.keyword=b.keyword and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe f set oac_transactions_60_cohort = (select sum(oac_transactions_60_cohort) from facebook.temporary_back_cohort_pe b where f.date=b.date and f.campaign=b.campaign and f.keyword=b.keyword and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe f set cum_net_rev_60_cohort = (select sum(paid_price_60_cohort) from facebook.temporary_back_cohort_pe b where f.date=b.date and f.campaign=b.campaign and f.keyword=b.keyword and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe f set PC2_60_cohort = (select sum(PC2_60_cohort) from facebook.temporary_back_cohort_pe b where f.date=b.date and f.campaign=b.campaign and f.keyword=b.keyword and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

drop table facebook.temporary_back_cohort_pe;

update facebook.facebook_optimization_keyword_pe set oac_transactions_30_cohort = 0 where oac_transactions_30_cohort is null;

update facebook.facebook_optimization_keyword_pe set oac_transactions_60_cohort = 0 where oac_transactions_60_cohort is null;

update facebook.facebook_optimization_keyword_pe set cum_net_rev_30_cohort = 0 where cum_net_rev_30_cohort is null;

update facebook.facebook_optimization_keyword_pe set cum_net_rev_60_cohort = 0 where cum_net_rev_60_cohort is null;

-- Back Cohort

alter table facebook.temporary_pe add column avg_discount float;

update facebook.temporary_pe r set avg_discount = (select avg(1-(i.unit_price/i.original_unit_price)) from bob_live_co.sales_order o inner join bob_live_co.sales_order_item i on i.fk_sales_order = o.id_sales_order where r.transaction_id=o.order_nr group by o.order_nr) where datediff(curdate(), date)<@days; 

create index temporary_pe on facebook.temporary_pe(campaign, date, country, city);

update facebook.facebook_optimization_keyword_pe f set net_revenue= (select sum(paid_price) from facebook.temporary_pe z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and f.keyword=z.keyword) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe f set new_customers= (select sum(z.new_customers) from facebook.temporary_pe z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and f.keyword=z.keyword) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe f set PC2_absolute= (select sum(PC2) from facebook.temporary_pe z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and f.keyword=z.keyword) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe f set `avg_discount(%)`= (select (avg(avg_discount))*100 from facebook.temporary_pe z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and f.keyword=z.keyword) where datediff(curdate(), date)<@days;

drop table facebook.temporary_pe;

update facebook.facebook_optimization_keyword_pe set oac_transactions = 0 where oac_transactions is null;

update facebook.facebook_optimization_keyword_pe set net_revenue = 0 where net_revenue is null;

update facebook.facebook_optimization_keyword_pe set new_customers = 0 where new_customers is null;

create table facebook.optimization_pe as select date, campaign, sum(visits)as visits from facebook.facebook_optimization_keyword_pe where datediff(curdate(), date)<@days group by date, campaign;

create index optimization_pe on facebook.optimization_pe(date, campaign);

create table facebook.cost_campaign_pe as select date, campaign, sum(spent) as spent, sum(clicks) as clicks, sum(social_impressions) as impressions from facebook.facebook_campaign where datediff(curdate(), date)<@days group by date, campaign;

create index cost_campaign_pe on facebook.cost_campaign_pe(date, campaign);

create table facebook.final_pe as select a.date, a.campaign, a.visits, b.spent, b.clicks, b.impressions from facebook.optimization_pe a, facebook.cost_campaign_pe b where a.date=b.date and a.campaign=b.campaign;

drop table facebook.optimization_pe;

drop table facebook.cost_campaign_pe;

create index final_pe on facebook.final_pe(date, campaign);

update facebook.facebook_optimization_keyword_pe f inner join facebook.final_pe final_pe on f.date=final_pe.date and f.campaign=final_pe.campaign set f.cost=(f.visits/(final_pe.visits))*final_pe.spent, f.clicks=(f.visits/(final_pe.visits))*final_pe.clicks, f.impressions=(f.visits/(final_pe.visits))*final_pe.impressions where datediff(curdate(), f.date)<@days;

drop table facebook.final_pe;

update facebook.facebook_optimization_keyword_pe set cost = 0 where cost is null;

update facebook.facebook_optimization_keyword_pe set clicks = 0 where clicks is null;

update facebook.facebook_optimization_keyword_pe set impressions = 0 where impressions is null;

update facebook.facebook_optimization_keyword_pe set category = substr(substr(campaign, locate('.',campaign)+1), 1, locate('.', substr(campaign, locate('.',campaign)+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe set gender = substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1) ,1 , locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe set age = substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,1 ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe set segmentation = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) ,1 ,locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe set extra_field = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) , locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))+1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, PC2_absolute=PC2_absolute/xr, cum_net_rev_30_cohort=cum_net_rev_30_cohort/xr, cum_net_rev_60_cohort=cum_net_rev_60_cohort/xr, PC2_30_cohort=PC2_30_cohort/xr, PC2_60_cohort=PC2_60_cohort/xr where r.country='PER' and datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe set cost=cost/1.23 where date >= '2014-01-01' and datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe set obc_transactions=oac_transactions, gross_revenue=net_revenue where obc_transactions=0 and oac_transactions!=0 and datediff(curdate(), date)<@days;
