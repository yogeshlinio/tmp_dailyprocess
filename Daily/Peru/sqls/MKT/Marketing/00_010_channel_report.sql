
update production_pe.tbl_order_detail t inner join SEM.transaction_id_pe c on t.order_nr=c.transaction_id set t.source_medium= concat(c.source,' / ',c.medium),t.campaign=c.campaign where source_medium is null;

#Channel Report PERU---

#Define channel---
##UPDATE tbl_order_detail SET source='',channel='', channel_group='';
#Voucher is priority statemnt
#If voucher is empty use GA source
UPDATE tbl_order_detail SET source='' where source is null;
UPDATE tbl_order_detail SET campaign='' where campaign is null;
UPDATE tbl_order_detail SET source=source_medium;
UPDATE tbl_order_detail SET source=coupon_code where source_medium is null;
UPDATE tbl_order_detail SET source=source_medium where channel_group='Non Identified';

#NEW REGISTERS
UPDATE tbl_order_detail SET channel='New Register' 
WHERE (source like 'NL%' OR source like 'NR%' OR source like
'COM%' OR source like 'BNR%') AND source_medium is null;
UPDATE tbl_order_detail SET source=source_medium
WHERE (coupon_code like 'NL%' OR coupon_code='SINIVA' OR coupon_code like 'COM%' 
OR coupon_code like 'NR%' OR coupon_code like 'BNR%' OR coupon_code like 'MKT%')
 AND source_medium is not null;
#BLOG
-- UPDATE tbl_order_detail SET channel='Blog Linio' WHERE source like 'blog%' 
-- OR source='blog.linio.com.pe / referral' or source='blogs.peru21.pe / referral';

#REFERRAL
UPDATE tbl_order_detail SET channel='Referral Sites' WHERE source like '%referral%' 
AND source<>'facebook.com / referral'
AND source<>'m.facebook.com / referral'
AND source<>'linio.com.co / referral'
AND source<>'linio.com / referral'
AND source<>'linio.com.mx / referral'
AND source<>'google.co.ve / referral'
AND source<>'linio.com.ve / referral'
AND source<>'linio.com.pe / referral'
AND source<>'www-staging.linio.com.ve / referral'
AND source<>'google.com / referral'
AND source<>'.com.ve / referral'
AND source<>'blog.linio.com.ve / referral' AND source<>'t.co / referral'
AND source not like '%google%' AND source not like '%blog%';

UPDATE tbl_order_detail SET channel='Other Affiliates' where source like '%affiliate%' or source like '%afiliate%';
UPDATE tbl_order_detail SET channel='Trade Tracker' where source like '%tradetracker%';

UPDATE tbl_order_detail SET channel='Buscape' 
WHERE source='Buscape / Price Comparison' OR source like '%buscape%';

UPDATE tbl_order_detail set channel='SoloCPM' where source like '%solocpm%';

#FB ADS
UPDATE tbl_order_detail SET channel='Facebook Ads' WHERE source='facebook / socialmediaads' OR source 
like '%facebook%ads%' or source='facebook / (not set)';

UPDATE tbl_order_detail set channel= 'Dark Post' where campaign like '%darkpost%';

#Criteo
UPDATE tbl_order_detail set channel='Criteo' where source like '%criteo%';

#PAMPA
UPDATE tbl_order_detail set channel='Pampa Network' where source like '%pampa%';

UPDATE tbl_order_detail set channel='PromoDescuentos' where source like '%promodescuentos%';

#SEM - GDN

UPDATE tbl_order_detail SET channel='Bing' where source like '%bing%';
UPDATE tbl_order_detail SET channel='SEM' WHERE source='google / cpc' AND 
(campaign not like 'brandb%' and campaign not like 'er.%' and campaign not like '[D%');
UPDATE tbl_order_detail SET channel='SEM Branded' WHERE (source='google / cpc' or source like '%bing%') AND campaign like 'brandb%';
UPDATE tbl_order_detail SET channel='Other Branded' WHERE source = '201.151.86.164' or source ='10.0.0.9' or source = 'www-staging.linio.com.mx' or source='blog.linio.com.mx' or source='213.229.186.15';

#GDN
UPDATE tbl_order_detail SET channel='Google Display Network' WHERE source='google / cpc' AND 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%');

#RETARGETING
UPDATE tbl_order_detail set channel='Other Retargeting' where source like '%retargeting%';
UPDATE tbl_order_detail SET channel='Sociomantic' WHERE source='sociomantic / (not set)' OR source='sociomantic / retargeting' or source='facebook / retargeting';
UPDATE tbl_order_detail SET channel='Cart Recovery' WHERE source='Cart_Recovery / CRM';
UPDATE tbl_order_detail SET channel='Vizury' WHERE source='vizury / retargeting' OR source like '%vizury%';
#UPDATE tbl_order_detail SET channel='Facebook R.' WHERE source='facebook / retargeting';
UPDATE tbl_order_detail SET channel='VEInteractive' WHERE source like 'VEInteractive%';
UPDATE tbl_order_detail SET channel='Main ADV' where source = 'retargeting / mainadv' or source like '%mainadv%' or source like '%solocpm%';
UPDATE tbl_order_detail SET channel='AdRoll' where source like '%adroll%';
UPDATE tbl_order_detail SET channel='Triggit' where source like '%triggit / retargeting%';


#SOCIAL MEDIA
UPDATE tbl_order_detail SET channel='Facebook Referral' WHERE (source='facebook.com / referral' OR source='m.facebook.com / referral' OR source like '%facebook%referral%') 
AND source not like '%ads%';
UPDATE tbl_order_detail SET channel='Twitter Referral' WHERE source='t.co / referral';
UPDATE tbl_order_detail SET channel='FB Posts' WHERE source='facebook / socialmedia' OR (source like
'%social%media%' and source<>'facebook / socialmediaads') or source='twitter / socialmedia';
UPDATE tbl_order_detail SET channel='Youtube' where source='youtube / socialmedia';
#SERVICIO AL CLIENTE
UPDATE tbl_order_detail set channel='Other Tele Sales' where source = 'CS /%' or source = '%/ CS' or source like 'telesales /%' or source like '%/ telesales'; 
UPDATE tbl_order_detail SET channel='Inbound' WHERE source like 'Tele%' or source like '%inbound%';
UPDATE tbl_order_detail SET channel='OutBound' WHERE source like 'REC%';
#SEO
UPDATE tbl_order_detail SET channel='Search Organic' WHERE source like '%organic' OR source='google.com.co / referral'
OR source like 'google%referral%';

UPDATE tbl_order_detail SET channel='Search Organic' where source like 'google.%' and source like '%referral';

UPDATE tbl_order_detail SET channel='Yahoo Answers' where source like '%answers.yahoo.com%';

#NEWSLETTER
UPDATE tbl_order_detail set Channel='Newsletter' WHERE (source like '%CRM' OR source like '%email') AND 
(source<>'TeleSales / CRM' AND source not like 'CDEAL%' AND source<>'Cart_Recovery / CRM' and source not like 'VE%');

#OTHER
UPDATE tbl_order_detail SET channel='Exchange' WHERE source like 'CCE%';
UPDATE tbl_order_detail SET channel='Content Mistake' WHERE source like 'CON%';
UPDATE tbl_order_detail SET channel='Procurement Mistake' WHERE source like 'PRC%';
UPDATE tbl_order_detail SET channel='Employee Vouchers' WHERE (source like 'TE%' AND source
<>'TeleSales / CRM') OR source like 'EA%' OR (source like 'LIN%' AND source not like 'linio.com%');

#PARTNERSHIPS
UPDATE tbl_order_detail SET channel='Other Partnerships' where source like '%PARTNERSHIP%' or coupon_code like '%PARTNERSHIP%';

UPDATE tbl_order_detail SET channel='Seguros Pacifico' WHERE source like 'CDEALpac%';
UPDATE tbl_order_detail SET channel='Romero' WHERE source like 'CDEALrom%';
UPDATE tbl_order_detail SET channel='Belcorp' WHERE source like 'CDEALbc%';
UPDATE tbl_order_detail SET channel='Email Deal' WHERE source like 'CDEAL / email';
UPDATE tbl_order_detail SET channel='Edifica' WHERE source like 'CDEALed%';
UPDATE tbl_order_detail SET channel='Tracking Villa María' WHERE source='CDEALCDEALVMM' OR source='CDEALVMM';
UPDATE tbl_order_detail SET channel='BBVA' WHERE (source like 'CDEALbb%' OR source='MKT3M0' OR
source='MKT09h' OR
source='MKT0dV' OR
source='MKT0hG' OR
source='MKT0mf' OR
source='MKT0Nh' OR
source='MKT0ni' OR
source='MKT0Q8' OR
source='MKT0rg' OR
source='MKT0t5' OR
source='MKT0VM' OR
source='MKT12' OR
source='MKT17H' OR
source='MKT17u' OR
source='MKT18i' OR
source='MKT1CU' OR
source='MKT1DD' OR
source='MKT1L9' OR
source='MKT1n7' OR
source='MKT1NG' OR
source='MKT1Ql' OR
source='MKT1TF' OR
source='MKT1uA' OR
source='MKT35s' OR
source='MKT6Fy' OR
source='MKT7FG' OR
source='MKT8PF' OR
source='MKT8pI' OR
source='MKT9Ki' OR
source='MKT9xQ' OR
source='MKTApv' OR
source='MKTauE' OR
source='MKTBBVAvm4' OR
source='MKTbKL' OR
source='MKTEhO' OR
source='MKTFEv' OR
source='MKTFww' OR
source='MKTiQS' OR
source='MKTm1V' OR
source='MKTmbS' OR
source='MKTMpH' OR
source='MKTtest' OR
source='MKTtFM' OR
source='MKTUvb' OR
source='MKTuWQ' OR
source='MKTzmJ') AND channel='';
UPDATE tbl_order_detail SET channel='Email CDEAL' WHERE source like 'CDEAL / mailing';

#DIRECT
UPDATE tbl_order_detail SET channel='Linio.com Referral' WHERE  
source='linio.com.co / referral' OR
source='linio.com / referral' OR
source='linio.com.mx / referral' OR
source='linio.com.ve / referral' OR
source='linio.com.pe / referral' OR
source='www-staging.linio.com.ve / referral' OR
source like '%linio%referal%';

UPDATE tbl_order_detail SET channel='Directo / Typed' WHERE source='(direct) / (none)' OR 
source like '%direct%none%';

#OFFLINE
UPDATE tbl_order_detail SET channel='PR vouchers' WHERE source like 'PR%';
UPDATE tbl_order_detail SET channel='Publimetro' WHERE description_voucher like '%publimetro%';
UPDATE tbl_order_detail SET channel='TV' WHERE coupon_code = 'enemigos';

#CORPORATE SALES
UPDATE tbl_order_detail set channel='Corporate Sales' where coupon_code LIKE 'CDEAL%' or source_medium like '%CORPORATESALES%';

#OFFLINE

#N/A
UPDATE 
tbl_order_detail SET channel='Unknown-No voucher' 
WHERE source='';
UPDATE tbl_order_detail SET 
channel='Unknown-Voucher' WHERE channel='' AND source<>'';

#CAC Vouchers
UPDATE tbl_order_detail set channel = 'CAC Deals' where (coupon_code like 'CAC%' or campaign like '%CAC%') and campaign not in ('PER_fanpage_m_22-50_fashion_20130624ZapatillasCocaCola__', 'PER_fanpage__22-40_fashion_20130617zapatillascocacola__');

UPDATE tbl_order_detail set channel='FB Ads CAC' WHERE (coupon_code like 'CAC%') and source_medium='facebook / socialmediaads';
UPDATE tbl_order_detail set channel='FB CAC' WHERE (coupon_code like 'CAC%') and source_medium='facebook / socialmedia';
UPDATE tbl_order_detail set channel='NL CAC' WHERE coupon_code like 'CAC%' and source_medium='postal / crm';

#Channel Report Extra Stuff to define Channel---
#Venta corporativa 

#Curebit
UPDATE tbl_order_detail set channel = 'Curebit' where coupon_code like '%cbit%' or source_medium like '%curebit%';

UPDATE tbl_order_detail t inner join mobileapp_transaction_id m on t.order_nr=m.transaction_id set channel = 'Mobile App';

#Partnerships Channel Extra


#Mercado libre Orders 


#DEFINE CHANNEL GROUP

UPDATE tbl_order_detail SET channel_group='Mobile App' WHERE channel='Mobile App';

##UPDATE tbl_order_detail set channel_group ='Corporate Sales' where channel='Corporate Sales';

#Facebook Ads Group
UPDATE tbl_order_detail SET channel_group='Facebook Ads' WHERE channel='Facebook Ads' or channel = 'Dark Post' or channel='Facebook Referral' ;

#SEM Group
UPDATE tbl_order_detail SET channel_group='Search Engine Marketing' WHERE channel='SEM' or channel='Bing' or channel='Other Branded';

#Social Media Group
UPDATE tbl_order_detail SET 
channel_group='Social Media' WHERE channel='Twitter Referral' or channel='FB Posts' or channel='Youtube';

#Referal Platform
UPDATE tbl_order_detail set channel_group = 'Referal Platform' where channel='Curebit';

#Retargeting Group
UPDATE tbl_order_detail SET channel_group='Retargeting' WHERE channel='Sociomantic' or source like '%retargeting' OR 
channel='Cart Recovery' OR channel='Vizury' OR channel='Facebook R.'  OR channel = 'VEInteractive' OR channel = 'GDN Retargeting' OR channel = 'Criteo' or channel='Main ADV' or channel='AdRoll' or channel='Other Retargeting' or channel='Triggit';

#GDN group
UPDATE tbl_order_detail SET channel_group='Google Display Network' WHERE channel like 'Google Display Network'; 

#NewsLetter Group
UPDATE tbl_order_detail SET channel_group='NewsLetter' WHERE channel='Newsletter';

#CAC Deals Group
UPDATE tbl_order_detail set channel_group='CAC Deals' where channel='CAC Deals' or channel='FB Ads CAC' or channel='FB CAC' or channel='NL CAC';

#SEO Group
UPDATE tbl_order_detail SET channel_group='Search Engine Optimization' WHERE channel='Search Organic' or channel='Yahoo Answers';

#Partnerships Group

UPDATE tbl_order_detail SET channel_group='Other (identified)' WHERE channel='Seguros Pacifico' OR
channel='Romero' OR channel='Email Deal' OR channel='CDEAL' OR channel='BBVA' OR channel='Belcorp' or
channel='Email CDEAL' or channel='Corporate Sales';

#Corporate Sales Group
UPDATE tbl_order_detail SET channel_group='Other (identified)' WHERE channel='Corporate Sales';

#Offline Group
UPDATE tbl_order_detail SET channel_group='Offline Marketing' WHERE channel='PR vouchers' or
channel='Publimetro' or channel='TV';

#Tele Sales Group
UPDATE tbl_order_detail SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales' or channel='Other Tele Sales';

#Branded Group
UPDATE tbl_order_detail SET channel_group='Branded' 
WHERE channel='Linio.com Referral' OR channel='Directo / Typed' OR
channel='SEM Branded';

#ML Group
UPDATE tbl_order_detail SET channel_group='Mercado Libre' WHERE channel='Mercado Libre Referral' OR channel='Mercado Libre Voucher' OR channel='Otros - Mercado Libre';

#Blog Group
-- UPDATE tbl_order_detail SET channel_group='Blogs' WHERE channel='Blog Linio';

#Affiliates Group
UPDATE tbl_order_detail SET channel_group='Affiliates' WHERE channel='Buscape' or channel='Pampa Network' or channel='Other Affiliates' or channel='SoloCPM' or channel='PromoDescuentos' or channel='Trade Tracker';


#Other (identified) Group
UPDATE tbl_order_detail SET channel_group='Other (identified)' 
WHERE channel='Referral Sites' OR channel='Reactivation Letters' 
OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' OR
channel='Exchange' OR
channel='Content Mistake' OR
channel='Procurement Mistake' OR
channel='Employee Voucher';

#Extra Channel Group
update tbl_order_detail set channel_group='Other (identified)' where source_medium = 'Banner+Interno / Banner SKY';
update tbl_order_detail set channel_group='Other (identified)' where source_medium = 'LA+REPUBLICA / BANNER-DESPLEGABLE';
update tbl_order_detail set channel_group='Other (identified)' where source_medium = 'mail.google.com / referral';
update tbl_order_detail set channel_group='Tele Sales' where source_medium = 'CS / Inbound';

#Unknown Group
UPDATE tbl_order_detail SET channel_group='Non identified' WHERE channel='Unknown-No voucher' OR channel='Unknown-Voucher';

#Define Channel Type---
#TO BE DONE

#Ownership

UPDATE tbl_order_detail set Ownership='' where channel_group='Social Media';
UPDATE tbl_order_detail set Ownership='' where channel_group='NewsLetter';
UPDATE tbl_order_detail set Ownership='' where channel_group='Search Engine Optimization';
UPDATE tbl_order_detail set Ownership='' where channel_group='Blogs';
UPDATE tbl_order_detail set Ownership='' where channel_group='Offline Marketing';
UPDATE tbl_order_detail set Ownership='' where channel_group='Partnerships';
UPDATE tbl_order_detail set Ownership='' where channel_group='Mercado Libre';
UPDATE tbl_order_detail set Ownership='' where channel_group='Tele Sales';


INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Peru',  #Country
  'marketing_report.marketing_pe',
  NOW(),
  NOW(),
  1,
  1;

-- call channel_report_no_voucher();
-- call visits_costs_channel();
-- call monthly_marketing();
-- call daily_marketing();