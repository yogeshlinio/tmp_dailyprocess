
set @days:=90;

delete from SEM.google_optimization_ad_group_pe where datediff(curdate(), date)<@days;

insert into SEM.google_optimization_ad_group_pe(date, channel, campaign, ad_group, impressions, clicks, visits, bounce, cart, cost) select date, channel, campaign, ad_group, sum(impressions), sum(clicks), sum(visits), sum(bounce), sum(cart), sum(ad_cost) from SEM.sem_filter_campaign_ad_group_pe where datediff(curdate(), date)<@days group by channel, date, campaign, ad_group;

update SEM.google_optimization_ad_group_pe set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_pe set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=SEM.week_iso(date) where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_pe x set gross_transactions =  (select count(distinct transaction_id) from SEM.sem_filter_transaction_id_pe s, production_pe.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe x set net_transactions =  (select count(distinct transaction_id) from SEM.sem_filter_transaction_id_pe s, production_pe.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe x set gross_revenue =  (select sum(t.paid_price) from SEM.sem_filter_transaction_id_pe s, production_pe.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe x set net_revenue =  (select sum(t.paid_price) from SEM.sem_filter_transaction_id_pe s, production_pe.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe x set new_customers =  (select sum(t.new_customers) from SEM.sem_filter_transaction_id_pe s, production_pe.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe x set new_customers_gross =  (select sum(t.new_customers_gross) from SEM.sem_filter_transaction_id_pe s, production_pe.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe x set category = substring_index(campaign, '.',1) where datediff(curdate(), date)<@days;

-- cohort

create table SEM.temporary_pe(
date date,
campaign varchar(255),
ad_group varchar(255),
custid int,
transaction_id varchar(255)
);

create index temporary on SEM.temporary_pe(date, ad_group, campaign);
create index transaction_id on SEM.temporary_pe(transaction_id);

insert into SEM.temporary_pe select distinct x.date, x.campaign, x.ad_group, x.customer_id, x.transaction_id from SEM.google_optimization_transaction_id_pe x, production_pe.tbl_order_detail t where x.transaction_id=t.order_nr and t.new_customers is not null and t.oac=1 and datediff(curdate(), x.date)<@days;

update SEM.google_optimization_ad_group_pe a set net_transactions_30 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe a set net_revenue_30 = (select sum(paid_price) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe a set net_transactions_60 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe a set net_revenue_60 = (select sum(paid_price) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe a set net_transactions_90 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe a set net_revenue_90 = (select sum(paid_price) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe a set net_transactions_120 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe a set net_revenue_120 = (select sum(paid_price) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe a set net_transactions_150 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe a set net_revenue_150 = (select sum(paid_price) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_pe a set net_transactions_180 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe a set net_revenue_180 = (select sum(paid_price) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

drop table SEM.temporary_pe;

update SEM.google_optimization_ad_group_pe set net_revenue_30=0 where net_revenue_30 is null;

update SEM.google_optimization_ad_group_pe set net_revenue_60=0 where net_revenue_60 is null;

update SEM.google_optimization_ad_group_pe set net_revenue_90=0 where net_revenue_90 is null;

update SEM.google_optimization_ad_group_pe set net_revenue_120=0 where net_revenue_120 is null;

update SEM.google_optimization_ad_group_pe set net_revenue_150=0 where net_revenue_150 is null;

update SEM.google_optimization_ad_group_pe set net_revenue_180=0 where net_revenue_180 is null;

-- cohort

update SEM.google_optimization_ad_group_pe s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, net_revenue_30=net_revenue_30/xr, net_revenue_60=net_revenue_60/xr, net_revenue_90=net_revenue_90/xr, net_revenue_120=net_revenue_120/xr, net_revenue_150=net_revenue_150/xr, net_revenue_180=net_revenue_180/xr where r.country='PER' and datediff(curdate(), s.date)<@days;

update SEM.google_optimization_ad_group_pe set cost=cost/1.23 where yrmonth<=201302 and datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_pe set cost=cost/16.35 where yrmonth between 201303 and 201311 and datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_pe set cost=cost/1.23 where yrmonth >= 201312 and datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_pe set CPC=cost/clicks, CPO=cost/net_transactions, CTR=clicks/impressions, CAC=cost/new_customers, CIR=cost/net_revenue, checkout_rate=cart/visits, conversion_rate=net_transactions/clicks, bounce_rate=bounce/clicks, transactions_to_cart=net_transactions/cart where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_pe set gross_CPO=cost/gross_transactions, gross_CAC=cost/new_customers_gross, gross_CIR=cost/gross_revenue, gross_conversion_rate=gross_transactions/clicks, gross_transactions_to_cart=gross_transactions/cart where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_pe set gross_revenue=0 where gross_revenue is null;

update SEM.google_optimization_ad_group_pe set net_revenue=0 where net_revenue is null;

update SEM.google_optimization_ad_group_pe set new_customers=0 where new_customers is null;

update SEM.google_optimization_ad_group_pe set new_customers_gross=0 where new_customers_gross is null;

update SEM.google_optimization_ad_group_pe set CPC=0 where CPC is null;

update SEM.google_optimization_ad_group_pe set CPO=0 where CPO is null;

update SEM.google_optimization_ad_group_pe set CTR=0 where CTR is null;

update SEM.google_optimization_ad_group_pe set CAC=0 where CAC is null;

update SEM.google_optimization_ad_group_pe set CIR=0 where CIR is null;

update SEM.google_optimization_ad_group_pe set checkout_rate=0 where checkout_rate is null;

update SEM.google_optimization_ad_group_pe set conversion_rate=0 where conversion_rate is null;

update SEM.google_optimization_ad_group_pe set bounce_rate=0 where bounce_rate is null;

update SEM.google_optimization_ad_group_pe set transactions_to_cart=0 where transactions_to_cart is null;

update SEM.google_optimization_ad_group_pe set gross_CPO=0 where gross_CPO is null;

update SEM.google_optimization_ad_group_pe set gross_CAC=0 where gross_CAC is null;

update SEM.google_optimization_ad_group_pe set gross_CIR=0 where gross_CIR is null;

update SEM.google_optimization_ad_group_pe set gross_conversion_rate=0 where gross_conversion_rate is null;

update SEM.google_optimization_ad_group_pe set gross_transactions_to_cart=0 where gross_transactions_to_cart is null;
