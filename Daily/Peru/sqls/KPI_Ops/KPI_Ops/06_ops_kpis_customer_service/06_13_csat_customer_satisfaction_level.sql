-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 06_13_csat_customer_satisfaction_level
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'6. Customer Service' AS 'group',
	0613 AS id,
	DATE_FORMAT(event_date,'%Y%m') AS MonthNum,
	operations_pe.week_iso(event_date) AS WeekNum,
	event_date as date,
	'CSAT (Customer Satisfaction Level)' AS kpi,
	'Avg Score on post call survey' AS formula,
	DATE_FORMAT(event_date,'%a') AS breakdown_1,
	agent_name AS breakdown_2,
	sum(answered_3) AS items,
	sum(good) AS items_sec,
	sum(good)/sum(answered_3) AS value,
	now() AS updated_at
FROM customer_service_pe.bi_ops_cc_pe
WHERE pos=1
AND event_date BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
GROUP BY event_date, DATE_FORMAT(event_date,'%a'), agent_name;