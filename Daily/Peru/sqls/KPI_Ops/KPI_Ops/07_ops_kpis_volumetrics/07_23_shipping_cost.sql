-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 07_23_shipping_cost
-- Created by: Rafael Guzman
-- Created date: 2014-05-29
-- Updated date: 2014-05-29
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = @v_interval@;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'7. Volumetrics' AS 'group',
	0723 AS id,
	DATE_FORMAT(a.Date,'%Y%m') AS MonthNum,
	@operations@.week_iso(a.Date) AS WeekNum,
	Date AS date,
	'Shipping Cost' AS kpi,
	'' AS formula,
	'' AS breakdown_1,
	'' AS breakdown_2,
	count(*) AS items,
	'' AS items_sec,
	SUM(a.ShippingCost/b.XR) AS value,
	now() AS updated_at
FROM @development@.A_Master a
INNER JOIN development_mx.A_E_BI_ExchangeRate_USD b
	ON a.Country = b.Country
	AND a.MonthNum = b.Month_Num
WHERE a.Date BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND Shipped = 1
GROUP BY Date;