/*-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
-- Procedure Name: 01_14_paid_within_24h_deposits
-- Created by: Rafael Guzman
-- Created date: 2014-05-13
-- Updated date: 2014-05-13
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 

SET @interval = 12;
SET @max_promised = curdate();

REPLACE operations_test.ops_kpi
SELECT
	UPPER('@v_countryPrefix@') AS country,
	'1. Payments' AS 'group',
	0114 AS id,
	DATE_FORMAT(date_exported,'%Y%m') AS MonthNum,
	@operations@.week_iso(date_exported) AS WeekNum,
	date_exported AS date,
	'% Paid within 24 h  - Retail / Bank Deposits' AS kpi,
	'# Orders Paid within 24h / # Orders Paid' AS formula,
	payment_method AS breakdown_1,
	'' AS breakdown_2,
	Count(*) AS items,
	SUM(IF(time_to_export <= 24,1,0)) AS items_sec,
	SUM(IF(time_to_export <= 24,1,0))/Count(*) AS value,
	now() AS updated_at
FROM @operations@.out_order_tracking
WHERE date_exported BETWEEN curdate() - INTERVAL @interval MONTH AND curdate()
AND payment_method IN (	'Banorte_PagoReferenciado',
						'Oxxo_Direct')
GROUP BY date_exported, payment_method;*/