INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Peru', 
  'CRM.A_CRM_CAMPAIGN_EMAIL_SAMPLE',
  'start',
  now(),
  now(),
  0,
  0
;

DROP TABLE IF EXISTS A_CRM_CAMPAIGN_EMAIL_SAMPLE;
CREATE TABLE A_CRM_CAMPAIGN_EMAIL_SAMPLE (

 idCampaing VARCHAR (20) NOT NULL,
 `Mailing-Name` VARCHAR (50) NOT NULL,
 Description VARCHAR (50) NOT NULL,
 email VARCHAR (50) NOT NULL,
 is_Gross BIT DEFAULT 0 ,
 is_Net BIT   DEFAULT 0,
 date date NOT NULL ,
 subscribe date NOT NULL,
 unsubscribe date NOT NULL,
 Url varchar(50) NOT NULL,
 Category varchar(50) NOT NULL,

 dateMessage date NOT NULL,
 Message bit NOT NULL,
 MessageCount int NOT NULL,


 dateOpen date NOT NULL,
 OPEN bit NOT NULL,
 OpenCount int NOT NULL,

 dateClick date NOT NULL,
 click bit NOT NULL,
 ClickCount int NOT NULL,

 dateBounce date NOT NULL,
 bounce bit NOT NULL,
 bounce_reason text NOT NULL,
 BounceCount int NOT NULL,
  
 created_at datetime,
 updated_at datetime,
 PRIMARY KEY (`Mailing-Name` , email )
);

INSERT A_CRM_CAMPAIGN_EMAIL_SAMPLE (
 idCampaing,
 `Mailing-Name`,
 Description,
 email,
 date,
  created_at,
  updated_at 
) SELECT
 `Campaign-ID` AS idCampaing,
 `Mailing-Name`,
 `Campaign-Name` AS Description,
 UniqueKey AS email,
 `Dispatch-Start` AS Date,
  now(),
  now()
FROM
 campaing_bounces_sample
GROUP BY
 `Mailing-Name`, UniqueKey
;

REPLACE A_CRM_CAMPAIGN_EMAIL_SAMPLE (
 idCampaing,
 `Mailing-Name`,
 Description,
 email,
 date,
  created_at,
  updated_at
) SELECT
 `Campaign-ID` AS idCampaing,
 `Mailing-Name`,
 `Campaign-Name` AS Description,
 UniqueKey AS email,
 `Dispatch-Start` AS Date,
  now(),
  now()
FROM
 campaing_clicks_sample
GROUP BY
 `Mailing-Name`, UniqueKey
;

REPLACE A_CRM_CAMPAIGN_EMAIL_SAMPLE (
 idCampaing,
 `Mailing-Name`,
 Description,
 email,
 date,
  created_at,
  updated_at
) SELECT
 `Campaign-ID` AS idCampaing,
 `Mailing-Name`,
 `Campaign-Name` AS Description,
 UniqueKey AS email,
 `Dispatch-Start` AS Date,
  now(),
  now()
FROM
 campaing_openings_sample
GROUP BY
 `Mailing-Name`, UniqueKey;

REPLACE A_CRM_CAMPAIGN_EMAIL_SAMPLE (
 idCampaing,
 `Mailing-Name`,
 Description,
 email,
 date,
  created_at,
  updated_at
) SELECT
 `Campaign-ID` AS idCampaing,
 `Mailing-Name`,
 `Campaign-Name` AS Description,
 UniqueKey AS email,
 `Dispatch-Start` AS Date,
  now() , 
  now()
FROM
 campaing_messages_sample
GROUP BY
  `Mailing-Name`, UniqueKey;

REPLACE A_CRM_CAMPAIGN_EMAIL_SAMPLE (
 idCampaing,
 `Mailing-Name`,
 Description,
 email,
 date,
  created_at,
  updated_at
) SELECT
 `Campaign-ID` AS idCampaing,
 `Mailing-Name`,
 `Campaign-Name` AS Description,
 UniqueKey AS email,
 `Dispatch-Start` AS Date,
  now() , 
  now()
FROM
 campaing_complaints_sample
GROUP BY
 `Mailing-Name`, UniqueKey;