UPDATE A_CRM_CAMPAIGN_EMAIL INNER JOIN 
      ( 
            SELECT
              `Campaign-ID`   AS idCampaing,
              `Mailing-Name`    ,
              `Campaign-Name` AS Description  ,
              `UniqueKey`     AS email,
               min( date(`Dispatch-Start` ) ) as date,
              #`Bounce-Text` AS bounce_reason,
               `Bounce-Type` AS bounce_reason,
              count(*) as bounceCount
             FROM campaing_bounces
             GROUP BY  `Mailing-Name` ,UniqueKey
       )  AS TMP
     USING (  `Mailing-Name`,email)
SET   
    A_CRM_CAMPAIGN_EMAIL.date = TMP.date ,
    A_CRM_CAMPAIGN_EMAIL.dateBounce    = TMP.date ,
    A_CRM_CAMPAIGN_EMAIL.BounceCount = TMP.bounceCount,
    A_CRM_CAMPAIGN_EMAIL.bounce = 1,
    A_CRM_CAMPAIGN_EMAIL.bounce_reason = TMP.bounce_reason
;

UPDATE A_CRM_CAMPAIGN_EMAIL INNER JOIN 
      ( 
            SELECT
               `Campaign-ID`   AS idCampaing,
               `Mailing-Name`,
              `Campaign-Name` as Description  ,
                UniqueKey AS email,
               min( date(`Dispatch-Start` ) ) as date,
              count(*) as OpenCount
                
             FROM campaing_openings
             GROUP BY `Mailing-Name`,UniqueKey
       )  AS TMP
     USING ( `Mailing-Name`,email)
SET   
    A_CRM_CAMPAIGN_EMAIL.date = LEAST( TMP.date ,A_CRM_CAMPAIGN_EMAIL.date  ) ,
    A_CRM_CAMPAIGN_EMAIL.dateOpen   = TMP.date ,
    A_CRM_CAMPAIGN_EMAIL.OpenCount  = TMP.OpenCount,
    A_CRM_CAMPAIGN_EMAIL.`open` = 1
;

UPDATE A_CRM_CAMPAIGN_EMAIL INNER JOIN 
      ( 
            SELECT
               `Campaign-ID`   AS idCampaing,
               `Mailing-Name`,
              `Campaign-Name` as Description  ,
                UniqueKey AS email,
               min( date(`Dispatch-Start` ) ) as date,
              `Link-URL` AS Url,
              count(*) as ClicksCount
             FROM campaing_clicks
             GROUP BY `Mailing-Name`,`UniqueKey`
       )  AS TMP
     USING ( `Mailing-Name`,email)
SET   
    A_CRM_CAMPAIGN_EMAIL.date = LEAST( TMP.date ,A_CRM_CAMPAIGN_EMAIL.date  ) ,
    A_CRM_CAMPAIGN_EMAIL.dateClick   = TMP.date ,
    A_CRM_CAMPAIGN_EMAIL.ClickCount = TMP.ClicksCount,
    A_CRM_CAMPAIGN_EMAIL.click = 1,
    A_CRM_CAMPAIGN_EMAIL.Url = TMP.Url,
    A_CRM_CAMPAIGN_EMAIL.Category = SUBSTRING_INDEX(      TRIM( LEADING "http://fagms.net/"
                                                     FROM TRIM( LEADING "http://www.linio.com.pe##"        
                                                     FROM TRIM( LEADING "http://blog.linio.com.pe/"
                                                     FROM TRIM( LEADING "http://www.linio.com.pe/" FROM 
                                     TMP.Url )))), '/', 1 )
;

/*
update             A_CRM_CAMPAIGN_EMAIL AS CMR 
       inner JOIN  interface_openings   AS a
               on a.UniqueKey=CMR.email
set  
   CMR.subscribe   = IF( Alias = 'Subscribe Newsletter', date(`Timestamp`), subscribe),
   CMR.unsubscribe = IF( Alias = 'desuscribirse', date(`Timestamp`), unsubscribe) 
WHERE
   Alias IN (  'Subscribe Newsletter' , 'desuscribirse' ) 
;
*/

DROP TEMPORARY TABLE IF EXISTS CustomerGross;
CREATE TEMPORARY TABLE CustomerGross 
( PRIMARY KEY ( CustomerEmail ) )
SELECT
   CustomerEmail,
   IF( SUM( OrderBeforeCan ) > 0 , 1, 0 ) AS Gross,
   IF( SUM( OrderAfterCan  ) > 0 , 1 ,0 ) AS Net
FROM
   development_pe.A_Master
WHERE
   OrderBeforeCan = 1
 OR OrderAfterCan = 1
GROUP BY CustomerEmail;
   
update             A_CRM_CAMPAIGN_EMAIL AS CMR 
       inner JOIN CustomerGross AS a
               on a.CustomerEmail=CMR.email
set  
     CMR.is_Gross = a.Gross,
     CMR.is_Net   = a.Net
;

DROP  TABLE IF EXISTS TMP_MSG;
CREATE TABLE TMP_MSG ( INDEX  ( `Mailing-Name`,email) )
SELECT
   `Campaign-ID`   AS idCampaing,
   `Mailing-Name`,
   `Campaign-Name` as Description  ,
   UniqueKey AS email,
   min( date(`Dispatch-Start` ) ) as date,
   count(*) as MessageCount
                
FROM campaing_messages
GROUP BY `Mailing-Name`,UniqueKey
;

UPDATE A_CRM_CAMPAIGN_EMAIL INNER JOIN 
       TMP_MSG  AS TMP
     USING ( `Mailing-Name`,email)
SET   
    A_CRM_CAMPAIGN_EMAIL.date = LEAST( TMP.date ,A_CRM_CAMPAIGN_EMAIL.date  ) ,
    A_CRM_CAMPAIGN_EMAIL.dateMessage   = TMP.date ,
    A_CRM_CAMPAIGN_EMAIL.MessageCount  = TMP.MessageCount,
    A_CRM_CAMPAIGN_EMAIL.Message = 1
;
DROP  TABLE IF EXISTS TMP_MSG;


INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Peru', 
  'CRM.A_CRM_CAMPAIGN_EMAIL',
  'finish',
  now(),
  now(),
  count(*),
  count(*)
FROM
  A_CRM_CAMPAIGN_EMAIL;
  
  
  
  