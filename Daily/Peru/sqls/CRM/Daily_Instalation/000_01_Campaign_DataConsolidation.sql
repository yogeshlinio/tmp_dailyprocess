INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Peru', 
  'CRM.A_CRM_CAMPAIGN_EMAIL',
  'start',
  now(),
  now(),
  0,
  0
;



UPDATE        A_CRM_CAMPAIGN_EMAIL
   INNER JOIN A_CRM_CAMPAIGN_EMAIL_SAMPLE
        USING (`Mailing-Name`,`email` ) 
SET
 A_CRM_CAMPAIGN_EMAIL.is_Gross  = A_CRM_CAMPAIGN_EMAIL.is_Gross OR A_CRM_CAMPAIGN_EMAIL_SAMPLE.is_Gross ,
 A_CRM_CAMPAIGN_EMAIL.is_Net    = A_CRM_CAMPAIGN_EMAIL.is_Net   OR A_CRM_CAMPAIGN_EMAIL_SAMPLE.is_Net   ,

 A_CRM_CAMPAIGN_EMAIL.Url       = A_CRM_CAMPAIGN_EMAIL_SAMPLE.Url,       
 A_CRM_CAMPAIGN_EMAIL.Category  = A_CRM_CAMPAIGN_EMAIL_SAMPLE.Category,  

 A_CRM_CAMPAIGN_EMAIL.dateMessage  = A_CRM_CAMPAIGN_EMAIL_SAMPLE.dateMessage,
 A_CRM_CAMPAIGN_EMAIL.Message      = A_CRM_CAMPAIGN_EMAIL.Message OR A_CRM_CAMPAIGN_EMAIL_SAMPLE.Message,     
 A_CRM_CAMPAIGN_EMAIL.MessageCount = A_CRM_CAMPAIGN_EMAIL.MessageCount + A_CRM_CAMPAIGN_EMAIL_SAMPLE.MessageCount,  

 A_CRM_CAMPAIGN_EMAIL.dateOpen  = A_CRM_CAMPAIGN_EMAIL_SAMPLE.dateOpen,
 A_CRM_CAMPAIGN_EMAIL.OPEN      = A_CRM_CAMPAIGN_EMAIL.OPEN OR A_CRM_CAMPAIGN_EMAIL_SAMPLE.OPEN,
 A_CRM_CAMPAIGN_EMAIL.OpenCount = A_CRM_CAMPAIGN_EMAIL.OpenCount + A_CRM_CAMPAIGN_EMAIL_SAMPLE.OpenCount,

 A_CRM_CAMPAIGN_EMAIL.dateClick  = A_CRM_CAMPAIGN_EMAIL_SAMPLE.dateClick,
 A_CRM_CAMPAIGN_EMAIL.click      = A_CRM_CAMPAIGN_EMAIL.click  OR A_CRM_CAMPAIGN_EMAIL_SAMPLE.click ,  
 A_CRM_CAMPAIGN_EMAIL.ClickCount = A_CRM_CAMPAIGN_EMAIL.ClickCount OR A_CRM_CAMPAIGN_EMAIL_SAMPLE.ClickCount,

 A_CRM_CAMPAIGN_EMAIL.dateBounce    = A_CRM_CAMPAIGN_EMAIL_SAMPLE.dateBounce,
 A_CRM_CAMPAIGN_EMAIL.bounce        = A_CRM_CAMPAIGN_EMAIL_SAMPLE.bounce OR A_CRM_CAMPAIGN_EMAIL_SAMPLE.bounce   ,     
 A_CRM_CAMPAIGN_EMAIL.bounce_reason = IF( A_CRM_CAMPAIGN_EMAIL.bounce_reason = "",  A_CRM_CAMPAIGN_EMAIL_SAMPLE.bounce , "" ),
 A_CRM_CAMPAIGN_EMAIL.BounceCount   = A_CRM_CAMPAIGN_EMAIL.BounceCount  + A_CRM_CAMPAIGN_EMAIL_SAMPLE.BounceCount,
 A_CRM_CAMPAIGN_EMAIL.Updated_at    = now()
;

INSERT IGNORE A_CRM_CAMPAIGN_EMAIL
SELECT A_CRM_CAMPAIGN_EMAIL_SAMPLE.*
FROM A_CRM_CAMPAIGN_EMAIL_SAMPLE;

INSERT IGNORE campaing_bounces SELECT * FROM campaing_bounces_sample;
INSERT IGNORE campaing_clicks  SELECT * FROM campaing_clicks_sample;
INSERT IGNORE campaing_complaints SELECT * FROM campaing_complaints_sample;
INSERT IGNORE campaing_messages   SELECT * FROM campaing_messages_sample;
INSERT IGNORE campaing_openings   SELECT * FROM campaing_openings_sample;

TRUNCATE campaing_bounces_sample;
TRUNCATE campaing_clicks_sample;
TRUNCATE campaing_complaints_sample;
TRUNCATE campaing_messages_sample;
TRUNCATE campaing_openings_sample;
TRUNCATE A_CRM_CAMPAIGN_EMAIL_SAMPLE;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Peru', 
  'CRM.A_CRM_CAMPAIGN_EMAIL',
  'finish',
  now(),
  now(),
  0,
  0
;
