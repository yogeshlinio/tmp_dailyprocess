DROP TABLE IF EXISTS A_Consolidation_GA;
CREATE TABLE A_Consolidation_GA ( 
  `country` varchar(255) DEFAULT NULL,
  `yrmonth` int(11) DEFAULT NULL,
  `week` varchar(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `source_medium` varchar(255) DEFAULT NULL,
  `campaign` varchar(255) ,
  `visits` int(11) DEFAULT NULL,
  `carts` int(11) DEFAULT NULL,
  `gross_transactions` int(11) DEFAULT NULL,
  `gross_revenue` float DEFAULT NULL,
  `gross_items` int(11) DEFAULT NULL,
  `avg_gross_rev` float DEFAULT NULL,
  `gross_conversion_rate` float DEFAULT NULL,
  `net_transactions` int(11) DEFAULT NULL,
  `net_revenue` float DEFAULT NULL,
  `net_items` int(11) DEFAULT NULL,
  `avg_net_rev` float DEFAULT NULL,
  `net_conversion_rate` float DEFAULT NULL,
  `PC1` float DEFAULT NULL,
  `PC1.5` float DEFAULT NULL,
  `PC2` float DEFAULT NULL,
  `%PC1` float DEFAULT NULL,
  `%PC1.5` float DEFAULT NULL,
  `%PC2` float DEFAULT NULL,
  PRIMARY KEY (campaign)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT A_Consolidation_GA  
SELECT
  `country` ,
  `yrmonth`,
  MIN(`week`) ,
  MIN(`date`),
  `source_medium` ,
  `campaign` ,
  SUM( `visits` ) ,
  SUM( `carts` ),
  SUM( `gross_transactions` ),
  SUM( `gross_revenue` ),
  SUM( `gross_items` ),
  SUM( `avg_gross_rev` ),
  SUM( `gross_conversion_rate` ),
  SUM( `net_transactions` ),
  SUM( `net_revenue` ),
  SUM( `net_items` ),
  SUM( `avg_net_rev` ),
  SUM( `net_conversion_rate` ),
  SUM( `PC1` ),
  SUM( `PC1.5` ),
  SUM( `PC2` ),
  SUM( `%PC1` ),
  SUM( `%PC1.5` ),
  SUM( `%PC2` )
FROM
dev_marketing.performance_report
WHERE
   country = "Peru"
GROUP BY `campaign` 