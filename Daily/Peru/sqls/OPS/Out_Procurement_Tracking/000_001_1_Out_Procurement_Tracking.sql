﻿DROP TABLE  IF EXISTS operations_pe.out_procurement_tracking_sample;
CREATE TABLE  operations_pe.out_procurement_tracking_sample LIKE operations_pe.out_procurement_tracking; 

INSERT INTO operations_pe.out_procurement_tracking_sample (
		id_procurement_order_item,
		id_procurement_order,
		id_catalog_simple,
		procurement_carrier_id,
		transport_type,
		is_deleted,
		#is_confirmed,
		is_receipt,
		#date_payment_scheduled,
		#date_payment_estimation,
		#date_payment_promised,
		cost_oms,
		tax,
		cost_oms_after_tax,
		inbound_type
) 
SELECT
		id_procurement_order_item,
		fk_procurement_order,
		fk_catalog_simple,
		fk_shipment_carrier,
		transport_type,
		is_deleted,
		#is_confirm,
		sku_received,
		#date(schedule_payment_date),
		#date(estimated_payment_date),
		#date(promised_payment_date),
		unit_price,
		tax,
		unit_price - tax,
		if(transport_type = 2, 'FOB', 'CIF')
	FROM
		procurement_live_pe.procurement_order_item
; 

UPDATE operations_pe.out_procurement_tracking_sample a
INNER JOIN procurement_live_pe.procurement_order b 
	ON a.id_procurement_order = b.id_procurement_order
INNER JOIN procurement_live_pe.procurement_order_type c 
	ON b.fk_procurement_order_type = c.id_procurement_order_type
SET 		
  a.purchase_order = concat(	b.venture_code, 
											lpad(b.id_procurement_order, 7, 0),
											b.check_digit),
	a.fk_procurement_order_type = b.fk_procurement_order_type,
	a.procurement_payment_status = b.payment_status,
	a.is_cancelled = b.is_cancelled,
	a.date_po_created = date(b.created_at),
	a.date_po_updated = date(b.updated_at),
	a.date_po_issued = date(b.sent_at),
	a.purchase_order_type = c.procurement_order_type,
	a.currency_type = b.currency_type
; 

update operations_pe.out_procurement_tracking_sample opt
        inner join
    development_mx.A_E_BI_ExchangeRate_USD er ON date_format(opt.date_po_created, '%Y%m') = er.Month_Num 
set 
    opt.cost_oms = opt.cost_oms * er.XR,
	opt.cost_oms_after_tax = opt.cost_oms_after_tax * er.XR
where
    opt.currency_type = 'dolar' and er.Country = 'PER';

SELECT  'Type PO',now();
UPDATE operations_pe.out_procurement_tracking_sample as a
SET a.purchase_order_type='Crossdocking'
WHERE (a.fk_procurement_order_type=6 OR a.fk_procurement_order_type=2 OR a.fk_procurement_order_type=9 );

SELECT  'Type PO',now();
UPDATE operations_pe.out_procurement_tracking_sample
SET out_procurement_tracking_sample.purchase_order_type='Own Warehouse'
WHERE out_procurement_tracking_sample.fk_procurement_order_type=1;

SELECT  'Type PO',now();
UPDATE operations_pe.out_procurement_tracking_sample
SET out_procurement_tracking_sample.purchase_order_type='Consignment'
WHERE (out_procurement_tracking_sample.fk_procurement_order_type=7
OR out_procurement_tracking_sample.fk_procurement_order_type=11);
/*
SELECT  'Type PO',now();
UPDATE operations_pe.out_procurement_tracking_sample
SET out_procurement_tracking_sample.purchase_order_type='Materia Prima'
WHERE out_procurement_tracking_sample.fk_procurement_order_type=8;

SELECT  'Type PO',now();
UPDATE operations_pe.out_procurement_tracking_sample
SET out_procurement_tracking_sample.purchase_order_type='Invoice'
WHERE out_procurement_tracking_sample.fk_procurement_order_type=9;

SELECT  'Type PO',now();
UPDATE operations_pe.out_procurement_tracking_sample
SET out_procurement_tracking_sample.purchase_order_type='Inbound'
WHERE out_procurement_tracking_sample.fk_procurement_order_type=10;
*/


UPDATE operations_pe.out_procurement_tracking_sample a
INNER JOIN development_pe.A_Master_Catalog b 
	ON a.id_catalog_simple = b.id_catalog_simple
SET
	a.sku_simple = b.sku_simple,
	a.sku_config = b.sku_config,
	a.sku_name = b.sku_name,
	a.supplier_id = b.id_supplier,
	a.supplier_name = b.Supplier,
	a.brand = b.brand,
	a.fulfillment_type_bob = b.fulfillment_type,
	a.cat_1 = b.Cat1,
	a.cat_2 = b.Cat2,
	a.cat_3 = b.Cat3,
	a.category_bp = b.Cat_BP,
	a.category_kpi = b.Cat_KPI,
	a.buyer = b.Buyer,
	a.head_buyer = b.Head_Buyer,
	a.tax = if(a.tax IS NULL, b.tax_percent,a.tax),
	a.cost_oms_after_tax = if(a.cost_oms_after_tax IS NULL, a.cost_oms/( 1 + b.tax_percent/100),a.cost_oms_after_tax);

UPDATE operations_pe.out_procurement_tracking_sample a
INNER JOIN procurement_live_pe.catalog_supplier_attributes b 
	ON a.supplier_id = b.fk_catalog_supplier
SET 
 #a.supplier_name = b.name,
 #a.supplier_tax_id = b.nit,
 #a.catalog_payment_type = b.payment_type,
 a.catalog_payment_terms = b.payment_terms,
 a.procurement_analyst = b.buyer#,
 #a.pick_at_zip = b.post_code,
 #a.pick_at_zip2 = mid(b.post_code, 3, 2),
 #a.pick_at_zip3 = RIGHT (b.post_code, 3),
# a.pick_at_city = b.,
 #a.pick_at_region = b.zone
; 




UPDATE operations_pe.out_procurement_tracking_sample
INNER JOIN procurement_live_pe.catalog_supplier_attributes
ON out_procurement_tracking_sample.supplier_id = catalog_supplier_attributes.fk_catalog_supplier
SET 
out_procurement_tracking_sample.credit_limit = catalog_supplier_attributes.credit_line;

UPDATE operations_pe.out_procurement_tracking_sample a
INNER JOIN procurement_live_pe.procurement_order_item_date_history b 
	ON a.id_procurement_order_item = b.fk_procurement_order_item
SET 
	a.date_goods_received = b.delivery_real_date#,
#	a.date_po_confirmed = b.confirm_date
;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check) 
SELECT 
  'Peru', 
  'out_procurement_tracking_sample',
  'dates_po',
  NOW(),
  max(date_po_created),
  count(*),
  count(item_counter)
FROM
  operations_pe.out_procurement_tracking_sample;


DROP TEMPORARY TABLE IF EXISTS estoque_recebemento;
CREATE TEMPORARY TABLE estoque_recebemento 
( 
   INDEX ( id_procurement_order_item ),
   INDEX ( itens_recebimento_id  ), 
   INDEX ( purchase_order ),
   INDEX ( recebimento_id ),
   INDEX ( estoque_id )
)
SELECT 
   id_procurement_order_item,
   purchase_order,
   recebimento_id,
   0 as itens_recebimento_id,
   0 as estoque_id,
   space(100) as endereco,
   space(100) as para_endereco

FROM
              operations_pe.out_procurement_tracking_sample a
   INNER JOIN wmsprod_pe.recebimento b
           ON a.purchase_order = b.inbound_document_identificator
;

UPDATE            estoque_recebemento AS b
       INNER JOIN wmsprod_pe.itens_recebimento c 
	           ON b.recebimento_id = c.recebimento_id
SET 
   b.itens_recebimento_id = c.itens_recebimento_id
;

UPDATE             estoque_recebemento AS c
        INNER JOIN wmsprod_pe.estoque d  
	            ON c.itens_recebimento_id = d.itens_recebimento_id
SET 
   c.estoque_id = d.estoque_id    ,
   c.endereco   = d.endereco
;

UPDATE             estoque_recebemento AS c
        INNER JOIN wmsprod_pe.movimentacoes d
	            ON c.estoque_id = d.estoque_id
SET 
   c.para_endereco = d.para_endereco
;
   
UPDATE           operations_pe.out_procurement_tracking_sample a
     INNER JOIN estoque_recebemento as d
          USING ( id_procurement_order_item )
SET 
 a.stock_item_id = d.estoque_id,
 a.wh_location = d.endereco,
 a.exit_type = 	CASE WHEN ( d.endereco = 'vendidos' AND d.para_endereco = "vendidos" )
										THEN 'sold'
										WHEN ( d.endereco LIKE '%error%' AND d.para_endereco  LIKE '%error%' )
										THEN 'error'
										ELSE NULL
								END;

UPDATE operations_pe.out_procurement_tracking_sample a
INNER JOIN wmsprod_pe.itens_venda b 
	ON a.stock_item_id = b.estoque_id
SET 
	a.item_id = b.item_id; 


UPDATE operations_pe.out_procurement_tracking_sample a
INNER JOIN procurement_live_pe.invoice_item b ON a.id_procurement_order_item = b.fk_procurement_order_item
INNER JOIN procurement_live_pe.invoice c ON b.fk_invoice = c.id_invoice
SET 
 a.invoice_number = c.invoice_nr,
 a.date_invoice_issued = date(c.issue_date),
 a.date_invoice_created = date(c.created_at)#,
 #a.date_invoice_receipt = date(c.invoice_date)
; 

DROP TEMPORARY TABLE IF EXISTS operations_pe.tmp_stock_ft;

CREATE TEMPORARY TABLE operations_pe.tmp_stock_ft (INDEX (stock_item_id))
SELECT
	stock_item_id,
	fulfillment_type_real,
	fulfillment_type_bp
FROM operations_pe.out_stock_hist;

UPDATE operations_pe.out_procurement_tracking_sample a
INNER JOIN operations_pe.tmp_stock_ft b 
	ON a.stock_item_id = b.stock_item_id
SET 
 a.fulfillment_type_real = b.fulfillment_type_real,
 a.fulfillment_type_bp = b.fulfillment_type_bp ;

UPDATE operations_pe.out_procurement_tracking_sample a
INNER JOIN procurement_live_pe.procurement_order_item_date_history b 
	ON a.id_procurement_order_item = b.fk_procurement_order_item
SET 
 a.date_collection_scheduled = date(collect_scheduled_date),
 a.date_collection_negotiated = date(collect_negotiated_date),
 a.date_delivery_calculated_bob = date(delivery_bob_calculated_date),
 a.date_delivery_scheduled = date(delivery_scheduled_date),
 a.date_delivery_bob_original_calculated = date(b.delivery_bob_original_calculated_date);


UPDATE operations_pe.out_procurement_tracking_sample a
INNER JOIN bob_live_pe.sales_order_item b 
ON a.item_id = b.id_sales_order_item
SET 
 a.date_ordered = date(b.created_at),
 a.cost_bob = b.cost ; 

UPDATE operations_pe.out_procurement_tracking_sample a
INNER JOIN bob_live_pe.catalog_simple b 
ON a.id_catalog_simple = b.id_catalog_simple
SET a.cost_bob = b.cost
WHERE
	a.cost_bob = 0 ;


UPDATE operations_pe.out_procurement_tracking_sample
SET 
	week_po_created = operations_pe.week_iso (date_po_created),
  week_payment = operations_pe.week_iso (date_paid),
	week_goods_received = operations_pe.week_iso (date_goods_received),
	month_po_created = date_format(date_po_created, "%x-%m"),
  month_payment = date_format(date_paid, "%x-%m"),
	month_goods_received = date_format(date_goods_received,"%x-%m"),
	goods_received_last_15 = IF(datediff(curdate(),date_goods_received) <= 15,1,0),
	goods_received_last_30 = IF(datediff(curdate(),date_goods_received) <= 30,1,0),
	goods_received_last_45 = IF(datediff(curdate(),date_goods_received) <= 45,1,0)
;

UPDATE operations_pe.out_procurement_tracking_sample
SET 
 cost_oms_gt_bob = 1
WHERE
 cost_oms > cost_bob ;
 
DROP TEMPORARY TABLE IF EXISTS operations_pe.tmp_po_totals;
CREATE TEMPORARY TABLE operations_pe.tmp_po_totals (INDEX (purchase_order))
SELECT 
	purchase_order,
	count(*) AS num_items
FROM operations_pe.out_procurement_tracking_sample
WHERE is_deleted = 0
AND		is_cancelled = 0
GROUP BY purchase_order;

UPDATE operations_pe.out_procurement_tracking_sample a
INNER JOIN operations_pe.tmp_po_totals b
ON a.purchase_order = b.purchase_order
SET 
	a.items_in_po = b.num_items,
	a.perc_items_in_po = 1/b.num_items;

/*UPDATE operations_pe.out_procurement_tracking_sample a
INNER JOIN procurement_live_pe.procurement_order_payment_items b 
	ON a.id_procurement_order_item = b.fk_procurement_order_item
INNER JOIN procurement_live_pe.procurement_order_payment c 
	ON b.fk_procurement_order_payment = c.id_procurement_order_payment
SET 
 a.procurement_payment_type = c.payment_type,
 a.date_paid = date(c.operation_date) ; */

/*UPDATE operations_pe.out_procurement_tracking_sample
SET 
 is_paid = 1,
 amount_paid = cost_oms
WHERE
	date_paid IS NOT NULL ;*/

/*UPDATE operations_pe.out_procurement_tracking_sample
SET 
 is_invoiced = 1
WHERE
 date_paid IS NOT NULL ; */


/*UPDATE operations_pe.out_procurement_tracking_sample
SET 
 payment_terms = CASE
									WHEN date_paid IS NULL 
									THEN NULL
									ELSE
									(CASE
										WHEN date_goods_received IS NULL 
										THEN datediff(date_paid, curdate())
										ELSE datediff(date_paid, date_goods_received)
									 END)
								 END,
payment_terms_scheduled = CASE
																WHEN date_payment_scheduled IS NULL 
																THEN NULL
																ELSE (	CASE
																					WHEN date_goods_received IS NULL 
																					THEN datediff(date_payment_scheduled, curdate())
																					ELSE datediff(date_payment_scheduled,	date_goods_received)
																				END)
																END,
payment_terms_expected = CASE
														WHEN date_goods_received IS NULL 
														THEN datediff(date_payment_estimation,curdate())
													ELSE (	CASE
																		WHEN date_payment_promised IS NULL 
																		THEN datediff(date_payment_estimation,date_goods_received)
																	ELSE datediff(date_payment_promised,date_goods_received)
																		END)
													END ; */

/*UPDATE operations_pe.out_procurement_tracking_sample
SET 
 paid_last_15 = IF(datediff(curdate(),date_paid) <= 15,1,0),
 paid_last_30 = IF(datediff(curdate(),date_paid) <= 30,1,0),
 paid_last_45 = IF(datediff(curdate(),date_paid) <= 45,1,0)
;*/

/*UPDATE operations_pe.out_procurement_tracking_sample a
SET a.date_payment_programmed = CASE
																	WHEN a.is_confirmed = 1 
																	THEN date_payment_scheduled
																	ELSE (	CASE
																						WHEN a.is_receipt = 1 
																						THEN date_payment_promised
																						ELSE date_payment_estimation
																					END)
																	END
WHERE
	procurement_payment_event = "pedido" ; */

/*UPDATE operations_pe.out_procurement_tracking_sample a
SET 
 a.date_payment_programmed = CASE
															WHEN a.is_invoiced = 1 
															THEN (CASE
																			WHEN a.is_confirmed = 1 
																			THEN date_payment_scheduled
																			ELSE date_payment_promised
																		END)
															ELSE (CASE
																			WHEN a.is_receipt = 1 
																			THEN date_payment_promised
																			ELSE date_payment_estimation
																		END)
															END
WHERE
	procurement_payment_event = "factura" ; */

/*UPDATE operations_pe.out_procurement_tracking_sample a
SET 
 a.date_payment_programmed = CASE
															WHEN a.is_receipt = 1 
															THEN (CASE
																			WHEN a.is_confirmed = 1
																			AND a.is_invoiced = 1 
																			THEN date_payment_scheduled
																			ELSE date_payment_promised
																		END)
															ELSE date_payment_estimation
														 END
WHERE
	procurement_payment_event = "entrega" ; */


UPDATE operations_pe.out_procurement_tracking_sample
SET payment_terms_programmed = datediff(date_payment_programmed,date_goods_received) ; 

#Aplica para peru cuando la fecha de pagado este bn, de esta forma se obtiene el termino de pago real y no el negociado.
#UPDATE operations_pe.out_procurement_tracking_sample t1
#INNER JOIN
#(SELECT a.id_procurement_order_item,
#IF(datediff(c.fecha_pago,a.date_goods_received)<=0,0,datediff(c.fecha_pago,a.date_goods_received)) as payment_terms
#FROM operations_pe.out_procurement_tracking_sample a
#INNER JOIN procurement_live_pe.invoice_item b ON a.id_procurement_order_item = b.fk_procurement_order_item
#INNER JOIN procurement_live_pe.invoice c ON b.fk_invoice = c.id_invoice) t2
#ON t1.id_procurement_order_item=t2.id_procurement_order_item
#SET t1.payment_terms=t2.payment_terms;

#Peyment_terms segun lo negociado, por el momento solo se puede obtener asi
#ya que las fechas de factura pagadano estan bien.
UPDATE operations_pe.out_procurement_tracking_sample AS a
#Quick FIX!!! WE NEED TO FIGURE OUT WHAT HAPPEN HERE
INNER JOIN operations_pe.catalog_supplier AS b
#INNER JOIN procurement_live_pe.catalog_supplier AS b
ON a.supplier_id = b.id_catalog_supplier
SET a.payment_terms = b.payday;

UPDATE operations_pe.out_procurement_tracking_sample
INNER JOIN operations_pe.calcworkdays
	ON out_procurement_tracking_sample.date_po_created = calcworkdays.date_first
		AND out_procurement_tracking_sample.date_po_confirmed = calcworkdays.date_last
SET out_procurement_tracking_sample.workdays_to_confirm =	calcworkdays.workdays;

#1.
UPDATE operations_pe.out_procurement_tracking_sample
INNER JOIN operations_pe.calcworkdays
ON date(date_delivery_bob_original_calculated) = calcworkdays.date_last 
              AND date_first = date_po_created
              AND isweekday = 1 
              AND isholiday = 0
SET workdays_to_receipt_shedule = calcworkdays.workdays;
#2.
SELECT  'Calculo promised procurement date stock1',now();
UPDATE operations_pe.out_procurement_tracking_sample
INNER JOIN operations_pe.calcworkdays
ON date(date_delivery_bob_original_calculated) = calcworkdays.date_last 
              AND workdays = 10
              AND isweekday = 1 
              AND isholiday = 0
SET date_promised_procurement = calcworkdays.date_first
WHERE workdays_to_receipt_shedule<=5
AND purchase_order_type='Stock';
#3.
SELECT  'Calculo promised procurement date stock2',now();
UPDATE operations_pe.out_procurement_tracking_sample
INNER JOIN operations_pe.calcworkdays
ON date(date_delivery_bob_original_calculated) = calcworkdays.date_last 
              AND workdays = 5
              AND isweekday = 1 
              AND isholiday = 0
SET date_promised_procurement = calcworkdays.date_first
WHERE workdays_to_receipt_shedule>5
AND purchase_order_type='Stock';
#4.
SELECT  'Calculo promised procurement date otros',now();
UPDATE operations_pe.out_procurement_tracking_sample
SET date_promised_procurement = date_delivery_bob_original_calculated
WHERE purchase_order_type<>'Stock';
#5.
SELECT  'Calculo promised procurement date otros',now();
UPDATE operations_pe.out_procurement_tracking_sample
SET  pending_to_receipt=1
WHERE date(date_promised_procurement)<=CURDATE()
AND is_cancelled = 0
AND is_deleted = 0
AND is_receipt = 0
AND date_promised_procurement IS NOT NULL;

SELECT  'cost_oms_after_tax',now();
UPDATE operations_pe.out_procurement_tracking_sample
SET price_receipt = cost_oms_after_tax
WHERE is_receipt = 1;

SELECT  'cost_oms_after_tax',now();
UPDATE operations_pe.out_procurement_tracking_sample
SET price_not_receipt = cost_oms_after_tax
WHERE is_receipt = 0;

UPDATE operations_pe.out_procurement_tracking_sample
INNER JOIN operations_pe.calcworkdays
	ON out_procurement_tracking_sample.date_po_created = calcworkdays.date_first
		AND out_procurement_tracking_sample.date_goods_received = calcworkdays.date_last
SET out_procurement_tracking_sample.workdays_to_goods_receipt_since_created =	calcworkdays.workdays;

UPDATE operations_pe.out_procurement_tracking_sample
INNER JOIN operations_pe.calcworkdays
	ON out_procurement_tracking_sample.date_po_confirmed = calcworkdays.date_first
		AND out_procurement_tracking_sample.date_goods_received = calcworkdays.date_last
SET out_procurement_tracking_sample.workdays_to_goods_receipt_since_confirmed =	calcworkdays.workdays;

UPDATE operations_pe.out_procurement_tracking_sample
INNER JOIN operations_pe.calcworkdays
	ON out_procurement_tracking_sample.date_po_confirmed = calcworkdays.date_first
		AND out_procurement_tracking_sample.date_collection_scheduled = calcworkdays.date_last
SET out_procurement_tracking_sample.workdays_to_scheduled =	calcworkdays.workdays;

UPDATE operations_pe.out_procurement_tracking_sample
INNER JOIN operations_pe.calcworkdays
	ON out_procurement_tracking_sample.date_po_confirmed = calcworkdays.date_first
		AND out_procurement_tracking_sample.date_delivery_scheduled = calcworkdays.date_last
SET out_procurement_tracking_sample.workdays_to_scheduled =	calcworkdays.workdays;


UPDATE operations_pe.out_procurement_tracking_sample
SET item_counter = 1;