﻿-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
SELECT  'Calculating WorkDays',now();
call operations_pe.calcworkdays;
SELECT  'Finished Calculating WorkDays',now();
SELECT  'OPS Oms Report',now();
