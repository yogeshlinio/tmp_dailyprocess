﻿-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Peru', 
  'out_procurement_tracking',
  'start',
  NOW(),
  MAX(date_po_created),
  count(*),
  count(*)
FROM
  operations_pe.out_procurement_tracking
;