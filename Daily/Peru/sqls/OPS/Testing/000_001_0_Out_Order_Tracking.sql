/*
DROP TABLE IF EXISTS operations_pe.tbl_purchase_order;

CREATE TABLE operations_pe.tbl_purchase_order LIKE bazayaco.tbl_purchase_order;

INSERT INTO operations_pe.tbl_purchase_order SELECT * FROM bazayaco.tbl_purchase_order;
*/

SELECT  'Drop out_order_tracking_sample',now();
DROP TABLE IF EXISTS operations_pe.out_order_tracking_sample;

SELECT  'Create out_order_tracking_sample',now();
CREATE TABLE operations_pe.out_order_tracking_sample LIKE operations_pe.out_order_tracking;

SELECT  'Insert Itens_venda',now();
INSERT INTO operations_pe.out_order_tracking_sample (
	item_id,
	order_number,
	sku_simple,
	status_wms,
	order_id,
	min_delivery_time,
	max_delivery_time,
	date_exported,
	datetime_exported,
	supplier_leadtime,
	wh_time,
	carrier_time,
	is_linio_promise
) SELECT
	a.item_id,
	a.numero_order,
	a.sku,
	a.STATUS,
	a.order_id,
	a.tempo_de_entrega_minimo,
	a.tempo_de_entrega_maximo,
	date(a.data_exportable),
	a.data_exportable,
	a.tempo_entrega_fornecedor,
	a.tempo_armazem,
	a.tempo_expedicao,
	if(a.linio_promise IS NULL, 0, a.linio_promise)
FROM
	wmsprod_pe.itens_venda AS a
INNER JOIN 
(SELECT itens_venda_id,MIN(date) AS date FROM wmsprod_pe.comunication_itens_venda
GROUP BY itens_venda_id) AS b
 ON a.itens_venda_id = b.itens_venda_id
WHERE DATE_FORMAT(b.date,'%Y%m') >= (DATE_FORMAT(curdate()- INTERVAL 3 MONTH,'%Y%m'))
AND 1=0;

SELECT  'Drop tmp_itens_venda_exported_items',now();
DROP TEMPORARY TABLE IF EXISTS operations_pe.tmp_itens_venda_exported_items;

SELECT  'Create tmp_itens_venda_exported_items',now();
CREATE TEMPORARY TABLE operations_pe.tmp_itens_venda_exported_items (INDEX (item_id))
SELECT
b.item_id,
a.date AS datetime_exported,
date(a.date) as date_exported
FROM
wmsprod_pe.comunication_itens_venda a
INNER JOIN wmsprod_pe.itens_venda b
                ON a.itens_venda_id = b.itens_venda_id
WHERE comunication = 'Comunicacion completa';

SELECT  'Update date_exported',now();
UPDATE operations_pe.out_order_tracking_sample AS a
INNER JOIN operations_pe.tmp_itens_venda_exported_items AS b ON a.item_id = b.item_id
SET a.date_exported = b.date_exported,
a.datetime_exported = b.datetime_exported;

SELECT  'Drop tmp_pro_min_date_exported',now();
DROP TEMPORARY TABLE IF EXISTS operations_pe.tmp_pro_min_date_exported;

SELECT  'Create tmp_pro_min_date_exported',now();
CREATE TEMPORARY TABLE operations_pe.tmp_pro_min_date_exported (INDEX (fk_sales_order_item))
SELECT
	sales_order_item_status_history.fk_sales_order_item,
	sales_order_item_status. NAME,
	min(	sales_order_item_status_history.created_at) AS min_of_created_at
FROM
	bob_live_pe.sales_order_item_status
INNER JOIN bob_live_pe.sales_order_item_status_history 
	ON sales_order_item_status.id_sales_order_item_status = sales_order_item_status_history.fk_sales_order_item_status
GROUP BY
	sales_order_item_status_history.fk_sales_order_item,
	sales_order_item_status. NAME
HAVING sales_order_item_status. NAME IN( 	"exported", 
	"exportable",
	"exported electronically");

SELECT  'Update date_exported',now();
UPDATE operations_pe.out_order_tracking_sample AS a
INNER JOIN operations_pe.tmp_pro_min_date_exported as b ON a.item_id = b.fk_sales_order_item
SET a.date_exported = date(min_of_created_at),
a.datetime_exported = min_of_created_at
WHERE date_exported IS NULL ;

UPDATE 
operations_pe.out_order_tracking_sample
INNER JOIN bob_live_pe.sales_order_item 
ON out_order_tracking_sample.item_id = sales_order_item.id_sales_order_item
SET 
 out_order_tracking_sample.supplier_leadtime = sales_order_item.delivery_time_supplier
Where supplier_leadtime is null ;


SELECT  'Update wms_tracking_code',now();
UPDATE operations_pe.out_order_tracking_sample AS a
INNER JOIN wmsprod_pe.vw_itens_venda_entrega AS b
ON a.item_id = b.item_id
SET a.wms_tracking_code = b.cod_rastreamento;

SELECT  'Update transportadoras',now();
UPDATE operations_pe.out_order_tracking_sample AS a
INNER JOIN wmsprod_pe.itens_venda AS b
	ON a.item_id = b.item_id
INNER JOIN wmsprod_pe.romaneio AS c
	ON b.romaneio_id = c.romaneio_id
INNER JOIN wmsprod_pe.transportadoras AS d
	ON c.transportadora_id = d.transportadoras_id
SET 
 a.shipping_carrier = d.nome_transportadora,
 a.id_shipping_carrier = d.transportadoras_id;

UPDATE
 operations_pe.out_order_tracking_sample a
INNER JOIN wmsprod_pe.entrega b
	ON a.wms_tracking_code = b.cod_rastreamento
INNER JOIN wmsprod_pe.pedidos_romaneio c
	ON b.entrega_id = c.entrega_id
INNER JOIN wmsprod_pe.romaneio AS d
	ON c.romaneio_id = d.romaneio_id
INNER JOIN wmsprod_pe.transportadoras AS e
	ON d.transportadora_id = e.transportadoras_id
SET
	a.shipping_carrier = e.nome_transportadora,
	a.id_shipping_carrier = e.transportadoras_id
WHERE shipping_carrier IS NULL;
 
 UPDATE operations_pe.out_order_tracking_sample ot
INNER JOIN bob_live_pe.sales_order_item soi ON ot.item_id = soi.id_sales_order_item
INNER JOIN bob_live_pe.shipment_carrier sc ON soi.fk_shipment_carrier = sc.id_shipment_carrier
SET ot.shipping_carrier_srt = sc. NAME;




SELECT  'Update A_Master_Catalog',now();
UPDATE operations_pe.out_order_tracking_sample AS a
INNER JOIN development_pe.A_Master_Catalog AS b 
 ON a.sku_simple = b.sku_simple
SET
	a.cat_1 = b.Cat1,
	a.cat_2 = b.Cat2,
	a.cat_3 = b.Cat3,
	a.category_bp = Cat_BP,
	a.category_kpi = Cat_KPI,
	a.head_buyer = b.Head_Buyer,
	a.buyer = b.Buyer,
	a.sku_config = b.sku_config,
	a.sku_name = b.sku_name,
	a.supplier_id = b.id_supplier,
 	a.supplier_name = b.Supplier,
 	a.package_height = b.package_height,
 	a.package_length = b.package_length,
 	a.package_width = b.package_width,
 	a.package_weight = b.package_weight;


#UPDATE operations_pe.out_order_tracking_sample as a
#INNER JOIN operations_pe.tbl_bi_ops_tracking_suppliers as b
#ON b.id = a.supplier_id
#SET a.procurement_analyst = b.tracker_name;

#vol_weight_carrier aplica solo para CO
SELECT  'Update vol_weight',now();	
UPDATE operations_pe.out_order_tracking_sample AS a
SET 
	a.vol_weight = a.package_height * a.package_length * a.package_width / 5000;
	#a.vol_weight_carrier= IF(shipping_carrier='DESPACHOS SERVIENTREGAS',
	#												(((a.package_width*a.package_length*a.package_height)*222)/1000000),
	#											IF(a.shipping_carrier='DESPACHO DESPRISA',
	#												(((a.package_width*a.package_length*a.package_height)*400)/1000000),
	#												(((a.package_width*a.package_length*a.package_height)*222)/1000000)));

SELECT  'Update max_vol_w_vs_w',now();	
UPDATE operations_pe.out_order_tracking_sample AS a
SET
a.max_vol_w_vs_w = 	CASE
											WHEN a.vol_weight > a.package_weight 
											THEN a.vol_weight
											ELSE a.package_weight
END;

SELECT  'Update package_measure_new',now();	
UPDATE operations_pe.out_order_tracking_sample AS a
SET
a.package_measure_new = CASE
													WHEN a.max_vol_w_vs_w > 35 
													THEN 'oversized'
													WHEN a.max_vol_w_vs_w > 5 
													THEN 'large'
													WHEN a.max_vol_w_vs_w > 2 
													THEN 'medium'
													ELSE 'small'
												END;
												


												
########## Fulfillment Type Real

SELECT  'Drop fulfillment_type_own_warehouse',now();
DROP TABLE IF EXISTS operations_pe.fulfillment_type_own_warehouse;
SELECT  'Create fulfillment_type_own_warehouse',now();
CREATE TABLE operations_pe.fulfillment_type_own_warehouse (INDEX (item_id))
SELECT
 b.item_id,
 "Own Warehouse" as fulfillment_type_real
FROM
wmsprod_pe.itens_venda b
INNER JOIN wmsprod_pe.status_itens_venda c
 ON b.itens_venda_id = c.itens_venda_id
WHERE c.STATUS ='Estoque reservado';

SELECT  'Update fulfillment_type_real',now();
UPDATE
 operations_pe.out_order_tracking_sample a
INNER JOIN operations_pe.fulfillment_type_own_warehouse b
 ON a.item_id = b.item_id
SET a.fulfillment_type_real = b.fulfillment_type_real;

SELECT  'Drop fulfillment_type_consignment',now();
DROP TABLE IF EXISTS operations_pe.fulfillment_type_consignment;

SELECT  'Create fulfillment_type_consignment',now();
CREATE TABLE operations_pe.fulfillment_type_consignment (INDEX (item_id))
SELECT
 b.item_id,
 'Consignment' AS fulfillment_type_real
FROM
wmsprod_pe.itens_venda b
INNER JOIN wmsprod_pe.estoque c
 ON b.estoque_id = c.estoque_id
INNER JOIN wmsprod_pe.itens_recebimento d
 ON c.itens_recebimento_id = d.itens_recebimento_id
INNER JOIN wmsprod_pe.recebimento e
 ON d.recebimento_id = e.recebimento_id
INNER JOIN wmsprod_pe.po_oms f
 ON e.inbound_document_identificator = f.name
INNER JOIN procurement_live_pe.procurement_order g
 ON f.po_oms_id = g.id_procurement_order
WHERE e.inbound_document_type_id = 4
AND g.fk_procurement_order_type = 7;

SELECT  'Update fulfillment_type_real',now();
UPDATE
 operations_pe.out_order_tracking_sample a
INNER JOIN operations_pe.fulfillment_type_consignment b
 ON a.item_id = b.item_id
SET a.fulfillment_type_real = b.fulfillment_type_real;

SELECT  'Drop fulfillment_type_dropshipping',now();
DROP TABLE IF EXISTS operations_pe.fulfillment_type_dropshipping;

SELECT  'Create fulfillment_type_dropshipping',now();
CREATE TABLE operations_pe.fulfillment_type_dropshipping (INDEX (item_id))
SELECT
 b.item_id,
 "Dropshipping" as fulfillment_type_real
FROM
wmsprod_pe.itens_venda b
INNER JOIN wmsprod_pe.status_itens_venda c
 ON b.itens_venda_id = c.itens_venda_id
WHERE c.STATUS IN (	'DS estoque reservado',
			'Waiting dropshipping',
			'Dropshipping notified',
			'awaiting_fulfillment',
			'Electronic good'
		);

SELECT  'Update fulfillment_type_real',now();
UPDATE
 operations_pe.out_order_tracking_sample a
INNER JOIN operations_pe.fulfillment_type_dropshipping b
 ON a.item_id = b.item_id
SET a.fulfillment_type_real = b.fulfillment_type_real;

SELECT  'Drop fulfillment_type_crossdocking',now();
DROP TABLE IF EXISTS operations_pe.fulfillment_type_crossdocking;
SELECT  'Create fulfillment_type_crossdocking',now();
CREATE TABLE operations_pe.fulfillment_type_crossdocking (INDEX (item_id))
SELECT
 b.item_id,
 "Crossdocking" as fulfillment_type_real
FROM
wmsprod_pe.itens_venda b
INNER JOIN wmsprod_pe.status_itens_venda c
 ON b.itens_venda_id = c.itens_venda_id
WHERE c.STATUS IN (	'analisando quebra',
			'aguardando estoque');

SELECT  'Update fulfillment_type_bob',now();
UPDATE operations_pe.out_order_tracking_sample AS a
INNER JOIN bob_live_pe.sales_order_item AS b
	ON a.item_id = b.id_sales_order_item
INNER JOIN bob_live_pe.catalog_shipment_type AS c
	ON b.fk_catalog_shipment_type = c.id_catalog_shipment_type
SET a.fulfillment_type_bob = c.NAME,
a.supplier_leadtime=b.delivery_time_supplier;

SELECT  'Update fulfillment_type_real',now();
UPDATE
 operations_pe.out_order_tracking_sample a
INNER JOIN operations_pe.fulfillment_type_crossdocking b
 ON a.item_id = b.item_id
SET a.fulfillment_type_real = if(a.fulfillment_type_bob = 'Dropshipping_cod', a.fulfillment_type_bob, b.fulfillment_type_real);

SELECT  'Update fulfillment_type_bp',now();
UPDATE operations_pe.out_order_tracking_sample
SET fulfillment_type_bp = CASE
				WHEN fulfillment_type_real IN( 'crossdocking', 'consignment')
				THEN fulfillment_type_real
                WHEN (fulfillment_type_real = 'Own Warehouse' and fulfillment_type_bob = 'Consignment')
				THEN fulfillment_type_bob
				WHEN fulfillment_type_real = 'Own Warehouse'
				THEN 'Outright Buying'
				WHEN fulfillment_type_real = 'dropshipping' 
				THEN 'other'
				WHEN fulfillment_type_real is null
				THEN fulfillment_type_bob
END;

UPDATE operations_pe.out_order_tracking_sample AS a
INNER JOIN wmsprod_pe.itens_venda AS b
	ON a.item_id=b.item_id
	INNER JOIN
	(SELECT itens_venda_id,max(data) date
	FROM wmsprod_pe.status_itens_venda WHERE status = 'Cancelado'
	GROUP BY itens_venda_id) AS c
ON b.itens_venda_id=c.itens_venda_id
SET a.date_cancelled = DATE(c.date),
a.datetime_cancelled = c.date;

SELECT  'Calculo wh_time',now();
UPDATE operations_pe.out_order_tracking_sample AS a
SET 
a.wh_time=if((a.package_measure_new='Small' OR a.package_measure_new='Medium'),1,2)
WHERE fulfillment_type_bob not like 'Dropshipp%'
AND wh_time = 0 ;

/*
version para MX
UPDATE operations_pe.out_order_tracking_sample
INNER JOIN wmsprod_pe.itens_venda ON out_order_tracking_sample.item_id = itens_venda.item_id
SET out_order_tracking_sample.wh_time = tempo_armazem
WHERE
tempo_armazem > 1;
*/

########## Queries Fechas


SELECT  'Truncate pro_order_tracking_dates',now();
TRUNCATE operations_pe.pro_order_tracking_dates;

SELECT  'Insert pro_order_tracking_dates',now();
INSERT INTO operations_pe.pro_order_tracking_dates (
	item_id,
	order_number,
	order_id,
  sku_simple,
  status_wms,
  date_exported,
	datetime_exported,
  supplier_leadtime,
  min_delivery_time,
	max_delivery_time,
  fulfillment_type_real,
  wh_time
) SELECT
	a.item_id,
	a.order_number,
	a.order_id,
  a.sku_simple,
  a.status_wms,
  a.date_exported,
	a.datetime_exported,
  a.supplier_leadtime,
  a.min_delivery_time,
	a.max_delivery_time,
  a.fulfillment_type_real,
  a.wh_time
FROM
	operations_pe.out_order_tracking_sample a;

/*UPDATE operations_pe.pro_order_tracking_dates AS a
INNER JOIN development_pe.A_Master_Catalog AS b 
 ON a.sku_simple = b.sku_simple
SET
 a.supplier_id = b.id_supplier;*/

SELECT  'Update date_ordered',now();
UPDATE 
((bob_live_pe.sales_order_address AS a
RIGHT JOIN (operations_pe.pro_order_tracking_dates AS b
				INNER JOIN bob_live_pe.sales_order AS c
				ON b.order_number = c.order_nr)
ON a.id_sales_order_address = c.fk_sales_order_address_billing)
LEFT JOIN bob_live_pe.customer_address_region AS d
ON a.fk_customer_address_region = d.id_customer_address_region)
INNER JOIN bob_live_pe.sales_order_item AS e
ON b.item_id = e.id_sales_order_item
SET 
 b.date_ordered = date(c.created_at),
 b.datetime_ordered = c.created_at;

SELECT  'Drop TMP_cod_rastreamento',now();
DROP TEMPORARY TABLE IF EXISTS operations_pe.TMP_cod_rastreamento;
SELECT  'Create TMP_cod_rastreamento',now();
CREATE TEMPORARY TABLE operations_pe.TMP_cod_rastreamento ( INDEX ( cod_rastreamento )  )
	SELECT
	
	itens.cod_rastreamento,
		CASE
	WHEN max(tms.date) < '2012-05-01' THEN
		NULL
	ELSE
		max(tms.date)
	END as date_delivered
	FROM
		           wmsprod_pe.vw_itens_venda_entrega AS itens
		INNER JOIN wmsprod_pe.tms_status_delivery AS tms
	            ON tms.cod_rastreamento = itens.cod_rastreamento
    WHERE 		
	    tms.id_status = 4
GROUP BY itens.cod_rastreamento;

SELECT  'Drop TMP_cod_rastreamento_Item',now();
DROP TEMPORARY TABLE IF EXISTS operations_pe.TMP_cod_rastreamento_Item;
SELECT  'Create TMP_cod_rastreamento_Item',now();
CREATE TEMPORARY TABLE operations_pe.TMP_cod_rastreamento_Item ( INDEX ( item_id ) )  
SELECT
	a.cod_rastreamento,
   a.date_delivered,
   b.item_id
FROM
              operations_pe.TMP_cod_rastreamento AS a
   INNER JOIN wmsprod_pe.vw_itens_venda_entrega AS b
           ON b.cod_rastreamento = a.cod_rastreamento
;

SELECT  'Update date_delivered',now();
UPDATE            operations_pe.pro_order_tracking_dates  AS a
       INNER JOIN operations_pe.TMP_cod_rastreamento_Item AS b
            USING ( item_id )
SET 
    a.date_delivered =  b.date_delivered;
	
#Actualiza historicamente
UPDATE            operations_pe.out_order_tracking  AS a
       INNER JOIN TMP_cod_rastreamento_Item AS b
            USING ( item_id )
SET 
    a.date_delivered =  b.date_delivered
WHERE a.date_delivered IS NULL;

#Query repetido.
#UPDATE 
#((bob_live_pe.sales_order_address AS a
#RIGHT JOIN (operations_pe.pro_order_tracking_dates AS b
#				INNER JOIN bob_live_pe.sales_order AS c
#				ON b.order_number = c.order_nr)
#ON a.id_sales_order_address = c.fk_sales_order_address_billing)
#LEFT JOIN bob_live_pe.customer_address_region AS d
#ON a.fk_customer_address_region = d.id_customer_address_region)
#INNER JOIN bob_live_pe.sales_order_item AS e
#ON b.item_id = e.id_sales_order_item
#SET 
# b.date_ordered = date(c.created_at),
# b.datetime_ordered = c.created_at
#;

SELECT  'Update date_backorder',now();
UPDATE operations_pe.pro_order_tracking_dates AS a
	INNER JOIN wmsprod_pe.itens_venda AS b
	ON a.item_id=b.item_id
	INNER JOIN
	(SELECT itens_venda_id,max(data) date
	FROM wmsprod_pe.status_itens_venda WHERE status = 'backorder'
	GROUP BY itens_venda_id) AS c
ON b.itens_venda_id=c.itens_venda_id
SET a.date_backorder = DATE(c.date),
a.datetime_backorder = c.date,
a.is_backorder = 1;

SELECT  'Update date_backorder_tratada',now();
UPDATE operations_pe.pro_order_tracking_dates AS a
	INNER JOIN wmsprod_pe.itens_venda AS b
	ON a.item_id=b.item_id
	INNER JOIN
	(SELECT itens_venda_id,max(data) date
	FROM wmsprod_pe.status_itens_venda WHERE status = 'backorder_tratada'
	GROUP BY itens_venda_id) AS c
ON b.itens_venda_id=c.itens_venda_id
SET a.date_backorder_tratada = c.date,
a.datetime_backorder_tratada = c.date;

DROP TEMPORARY TABLE IF EXISTS operations_pe.tmp_usuarios_cs;
 
CREATE TEMPORARY TABLE operations_pe.tmp_usuarios_cs(INDEX (item_id))
SELECT
 c.item_id,
 a.`status`,
 a.itens_venda_id,
 b.nome AS UserName
FROM wmsprod_pe.status_itens_venda a
INNER JOIN wmsprod_pe.usuarios b
 ON a.usuarios_id = b.usuarios_id
INNER JOIN wmsprod_pe.itens_venda c
ON a.itens_venda_id = c.itens_venda_id
where a.`status` in ('backorder_tratada');


UPDATE operations_pe.pro_order_tracking_dates AS a
INNER JOIN operations_pe.tmp_usuarios_cs AS b
ON b.item_id = a.item_id
SET a.user_cs_bo = b.UserName;

# date_backorder_accepted
UPDATE operations_pe.pro_order_tracking_dates AS a
 INNER JOIN wmsprod_pe.itens_venda AS b
 ON a.item_id=b.item_id
 INNER JOIN
 (SELECT 
  usuarios_id,
  itens_venda_id,
  max(data) date
 FROM wmsprod_pe.status_itens_venda 
  WHERE status in('Analisando quebra','Waiting dropshipping','dropshipping notified') 
 GROUP BY itens_venda_id) AS c
ON b.itens_venda_id=c.itens_venda_id
INNER JOIN wmsprod_pe.usuarios e
 ON c.usuarios_id = e.usuarios_id
AND c.date > a.date_backorder
SET a.date_backorder_accepted = c.date,
a.datetime_backorder_accepted = c.date,
a.user_cs_bo = e.nome;


SELECT  'Update date oms',now();
UPDATE  operations_pe.pro_order_tracking_dates AS a
INNER JOIN wmsprod_pe.itens_venda AS b
ON a.item_id=b.item_id
INNER JOIN wmsprod_pe.estoque AS c
ON b.estoque_id=c.estoque_id
INNER JOIN wmsprod_pe.itens_recebimento AS d
ON c.itens_recebimento_id=d.itens_recebimento_id
INNER JOIN procurement_live_pe.wms_received_item AS e 
ON d.itens_recebimento_id = e.id_wms
INNER JOIN operations_pe.out_procurement_tracking AS f
ON e.fk_procurement_order_item = f.id_procurement_order_item
SET 
a.date_po_created = date(f.date_po_created),
a.datetime_po_created = f.date_po_created,
a.date_po_updated  = date(f.date_po_created),
a.datetime_po_updated = f.date_po_updated,
a.date_po_issued = date(f.date_po_issued),
a.datetime_po_issued = f.date_po_issued,
a.date_po_confirmed = date(f.date_po_confirmed),
a.datetime_po_confirmed = f.date_po_confirmed;

#UPDATE operations_pe.pro_order_tracking_dates AS a
#INNER JOIN operations_pe.tbl_purchase_order AS b
#ON a.item_id=b.itemid
#SET a.date_po_created = date(b.fecha_creacion),
#a.datetime_po_created = b.fecha_creacion;

#es igual que date_shipped
#UPDATE ((operations_pe.pro_order_tracking_dates AS a
#INNER JOIN wmsprod_pe.itens_venda AS b
#ON a.item_id = b.item_id)
#INNER JOIN wmsprod_pe.status_itens_venda AS c
#ON b.itens_venda_id = c.itens_venda_id) 
#SET a.date_shipped = date(c.data),
#a.date_shipped = c.data
#WHERE ((c.status)="Expedido");

SELECT  'Update date_procured',now();
UPDATE operations_pe.pro_order_tracking_dates AS a
INNER JOIN wmsprod_pe.itens_venda AS b
ON a.item_id = b.item_id
INNER JOIN (SELECT itens_venda_id,MIN(DATA) as DATA FROM wmsprod_pe.status_itens_venda
WHERE STATUS = "estoque reservado"
GROUP BY itens_venda_id) AS c
ON b.itens_venda_id = c.itens_venda_id
SET 
a.date_procured = DATE(DATA),
a.datetime_procured = DATA,
a.date_assigned_to_stock  = DATE(DATA),
a.datetime_assigned_to_stock  = DATA;

UPDATE operations_pe.out_order_tracking_sample AS out_order_tracking
INNER JOIN wmsprod_pe.itens_venda
                ON out_order_tracking.item_id = itens_venda.item_id
INNER JOIN wmsprod_pe.estoque
                ON itens_venda.estoque_id = estoque.estoque_id
INNER JOIN wmsprod_pe.itens_recebimento
                ON estoque.itens_recebimento_id = itens_recebimento.itens_recebimento_id
SET
 date_procured = date(itens_recebimento.data_criacao),
datetime_procured = itens_recebimento.data_criacao
WHERE itens_recebimento.data_criacao IS NOT NULL;

SELECT  'Update date_ready_to_pick',now();
UPDATE operations_pe.pro_order_tracking_dates AS a
INNER JOIN wmsprod_pe.itens_venda AS b
	ON a.item_id = b.item_id
INNER JOIN (SELECT itens_venda_id,MAX(DATA) AS DATA FROM wmsprod_pe.status_itens_venda
WHERE STATUS = "Aguardando separacao"
GROUP BY itens_venda_id) AS c
	ON b.itens_venda_id = c.itens_venda_id
SET 
 a.date_ready_to_pick = DATE(c.DATA),
 a.datetime_ready_to_pick = c.DATA;
	
#Corrije la fecha cuando es fds o festivo.
UPDATE operations_pe.pro_order_tracking_dates AS a
INNER JOIN operations_pe.calcworkdays AS c
ON  date(a.datetime_ready_to_pick ) = c.date_first 
              AND c.workdays = 1
              AND c.isholiday_first = 1
SET a.date_ready_to_pick = c.date_last,
a.datetime_ready_to_pick = c.date_last;



UPDATE operations_pe.pro_order_tracking_dates AS a
INNER JOIN operations_pe.calcworkdays AS c
ON  date(a.datetime_ready_to_pick ) = c.date_first 
              AND c.workdays = 1
              AND c.isweekend_first = 1
SET a.date_ready_to_pick = c.date_last,
a.datetime_ready_to_pick = c.date_last;

SELECT  'Truncate pro_max_date_ready_to_ship',now();
TRUNCATE operations_pe.pro_max_date_ready_to_ship;

SELECT  'Insert pro_max_date_ready_to_ship',now();
INSERT INTO operations_pe.pro_max_date_ready_to_ship (date_ready, order_item_id) SELECT
	MAX(c.DATA) AS date_ready,
	a.item_id
FROM
	operations_pe.pro_order_tracking_dates AS a
INNER JOIN wmsprod_pe.itens_venda AS b
	ON a.item_id = b.item_id
INNER JOIN wmsprod_pe.status_itens_venda AS c
	ON b.itens_venda_id = c.itens_venda_id
WHERE
	c.STATUS = "faturado"
GROUP BY
	a.item_id;

SELECT  'Update datetime_ready_to_ship',now();
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.pro_max_date_ready_to_ship ON pro_order_tracking_dates.item_id = pro_max_date_ready_to_ship.order_item_id
SET pro_order_tracking_dates.date_ready_to_ship = date(pro_max_date_ready_to_ship.date_ready),
 pro_order_tracking_dates.datetime_ready_to_ship = pro_max_date_ready_to_ship.date_ready;

SELECT  'Update date_shipping_list',now();
UPDATE operations_pe.pro_order_tracking_dates AS a
INNER JOIN wmsprod_pe.itens_venda AS b
ON a.item_id = b.item_id
INNER JOIN (SELECT itens_venda_id,MAX(DATA) AS DATA FROM wmsprod_pe.status_itens_venda
WHERE STATUS = "Aguardando expedicao"
GROUP BY itens_venda_id) AS c
ON b.itens_venda_id = c.itens_venda_id
SET a.date_shipping_list = DATE(data),
a.datetime_shipping_list = data;

SELECT  'Update date_shipped',now();
UPDATE operations_pe.pro_order_tracking_dates a
INNER JOIN wmsprod_pe.itens_venda b
	ON a.item_id = b.item_id
INNER JOIN wmsprod_pe.status_itens_venda d 
	ON b.itens_venda_id = d.itens_venda_id
SET
 a.date_shipped = (SELECT
												max(date(DATA))
											FROM
												wmsprod_pe.itens_venda c,
												wmsprod_pe.status_itens_venda e
											WHERE
												e. STATUS = "expedido"
											AND b.itens_venda_id = c.itens_venda_id
											AND e.itens_venda_id = d.itens_venda_id
										),
  a.datetime_shipped = (SELECT
													max(DATA)
													FROM
														wmsprod_pe.itens_venda c,
														wmsprod_pe.status_itens_venda e
													WHERE
														e. STATUS = "expedido"
													AND b.itens_venda_id = c.itens_venda_id
													AND e.itens_venda_id = d.itens_venda_id
										);

SELECT  'Truncate pro_1st_attempt',now();
TRUNCATE operations_pe.pro_1st_attempt;

SELECT  'Insert pro_1st_attempt',now();
INSERT INTO operations_pe.pro_1st_attempt (order_item_id, min_of_date) SELECT
	pro_order_tracking_dates.item_id,
	min(tms_status_delivery.date) AS min_of_date
FROM
	(
		operations_pe.pro_order_tracking_dates
		INNER JOIN wmsprod_pe.vw_itens_venda_entrega ON pro_order_tracking_dates.item_id = vw_itens_venda_entrega.item_id
	)
INNER JOIN wmsprod_pe.tms_status_delivery ON vw_itens_venda_entrega.cod_rastreamento = tms_status_delivery.cod_rastreamento
GROUP BY
	pro_order_tracking_dates.item_id,
	tms_status_delivery.id_status
HAVING
	tms_status_delivery.id_status = 5;

SELECT  'Update date_1st_attempt',now();
UPDATE operations_pe.pro_order_tracking_dates 
INNER JOIN operations_pe.pro_1st_attempt ON pro_order_tracking_dates.item_id = pro_1st_attempt.order_item_id
SET pro_order_tracking_dates.date_1st_attempt = pro_1st_attempt.min_of_date;

SELECT  'Update pro_order_tracking_dates',now();
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN wmsprod_pe.vw_itens_venda_entrega vw 
	ON pro_order_tracking_dates.item_id = vw.item_id
INNER JOIN wmsprod_pe.tms_status_delivery del 
	ON vw.cod_rastreamento = del.cod_rastreamento
SET operations_pe.pro_order_tracking_dates.date_1st_attempt = (
	SELECT
		max(tms.date)
	FROM
		wmsprod_pe.tms_status_delivery tms,
		wmsprod_pe.vw_itens_venda_entrega itens
	WHERE
		del.cod_rastreamento = tms.cod_rastreamento
	AND vw.item_id = itens.item_id
	AND tms.id_status = 4
)
WHERE
	pro_order_tracking_dates.date_1st_attempt IS NULL
OR pro_order_tracking_dates.date_1st_attempt < '2011-05-01';

SELECT  'Update date_failed_delivery',now();
UPDATE operations_pe.pro_order_tracking_dates AS a
INNER JOIN wmsprod_pe.vw_itens_venda_entrega AS b
ON a.item_id = b.item_id
INNER JOIN wmsprod_pe.tms_status_delivery AS c
ON b.cod_rastreamento = c.cod_rastreamento
SET a.date_failed_delivery = c.date
WHERE c.id_status=10;

#no aplica
#UPDATE operations_pe.pro_order_tracking_dates as a
#INNER JOIN operations_pe.tbl_bi_ops_ingreso_bodega_devolucion as b
#ON a.item_id=b.item_id
#SET 
#a.date_returned=b.date_ingreso_bodega_devolucion;

SELECT  'Update date_procured_promised',now();
UPDATE        operations_pe.pro_order_tracking_dates 
   INNER JOIN operations_pe.calcworkdays
           ON     date( date_exported )           = calcworkdays.date_first 
              AND workdays = supplier_leadtime
              AND isweekday = 1 
              AND isholiday = 0
SET 
	date_procured_promised = calcworkdays.date_last
WHERE
	pro_order_tracking_dates.date_exported IS NOT NULL;


SELECT  'Update date_ready_to_ship_promised',now();
UPDATE operations_pe.pro_order_tracking_dates 
INNER JOIN operations_pe.calcworkdays
ON date(date_procured_promised) = calcworkdays.date_first 
AND workdays = wh_time
AND isweekday = 1
AND isholiday = 0
SET 
	date_ready_to_ship_promised = calcworkdays.date_last
WHERE
	operations_pe.pro_order_tracking_dates.date_procured_promised IS NOT NULL;
	
UPDATE operations_pe.pro_order_tracking_dates 
INNER JOIN operations_pe.calcworkdays
ON date(date_exported) = calcworkdays.date_first 
AND workdays = 2
AND isweekday = 1
AND isholiday = 0
SET 
	date_ready_to_ship_promised = calcworkdays.date_last
WHERE
	operations_pe.pro_order_tracking_dates.fulfillment_type_real = 'Dropshipping';

#No aplica para peru.
#UPDATE operations_pe.pro_order_tracking_dates AS a
#INNER JOIN 
#(SELECT sku_simple,max(date_update) as date_update FROM operations_pe.tbl_bi_ops_stock_update GROUP BY sku_simple) as t
#ON a.sku_simple=t.sku_simple
#SET a.date_stock_updated = t.date_update;

SELECT  'Update date_delivered_promised',now();
UPDATE  operations_pe.pro_order_tracking_dates 
   INNER JOIN operations_pe.calcworkdays
		ON     date( date_exported ) = calcworkdays.date_first 
			AND workdays = if((max_delivery_time is null or max_delivery_time =''  or max_delivery_time=0),7,max_delivery_time)
			AND isweekday = 1
			AND isholiday = 0
SET 
   date_delivered_promised = calcworkdays.date_last
WHERE
	pro_order_tracking_dates.date_exported IS NOT NULL;

SELECT  'Drop tmp_order_tracking_stockout_declared',now();
DROP TEMPORARY TABLE IF EXISTS operations_pe.tmp_order_tracking_stockout_declared;

SELECT  'Create tmp_order_tracking_stockout_declared',now();
CREATE TEMPORARY TABLE operations_pe.tmp_order_tracking_stockout_declared (INDEX (itens_venda_id))
SELECT
                a.itens_venda_id,
                a.numero_order,
                a.item_id,
                b.date
FROM
                wmsprod_pe.itens_venda a
INNER JOIN (  SELECT
								itens_venda_id,
								max(DATA) date
							FROM
								wmsprod_pe.status_itens_venda
							WHERE STATUS = 'Quebrado'
							GROUP BY itens_venda_id) b
ON  a.itens_venda_id = b.itens_venda_id;
 
SELECT  'Update date_declared_stockout',now();
UPDATE operations_pe.pro_order_tracking_dates d
INNER JOIN operations_pe.tmp_order_tracking_stockout_declared t
                ON d.item_id = t.item_id
SET d.date_declared_stockout = t.date,
d.datetime_declared_stockout = t.date;

SELECT  'Update date_declared_backorder',now();
UPDATE operations_pe.pro_order_tracking_dates d
INNER JOIN (
	SELECT
		a.item_id,
		a.tempo_de_entrega,
		a.tempo_entrega_fornecedor,
		a.data_criacao,
		a.estimated_delivery_date,
		a.estimated_delivery_days,
		a.estimated_days_on_wh,
		b.days_back,
		b.date_created
	FROM
		wmsprod_pe.itens_venda a
	JOIN wmsprod_pe.item_backorder b ON a.itens_venda_id = b.itens_venda_id
) c ON d.item_id = c.item_id
SET d.date_declared_backorder = c.date_created,
d.datetime_declared_backorder = c.date_created;

SELECT  'Update date_status_value_chain',now();
UPDATE operations_pe.pro_order_tracking_dates AS a
INNER JOIN
(SELECT  
	d.item_id,
	b.status,
	b.data 
FROM
(select 
	itens_venda_id,
	status, 
	max(data) as data
from
(SELECT 
	itens_venda_id,
	status,
	data
FROM wmsprod_pe.status_itens_venda
ORDER BY data desc) a
GROUP BY itens_venda_id) AS b
INNER JOIN wmsprod_pe.itens_venda AS d
ON b.itens_venda_id=d.itens_venda_id) AS t
ON a.item_id = t.item_id
SET 
a.date_status_value_chain = t.data,
a.status_value_chain=t.status;

SELECT  'Update date_1st_attempt',now();
UPDATE operations_pe.pro_order_tracking_dates AS a
SET a.date_status_value_chain=date_1st_attempt,
status_value_chain='Intentado'
WHERE status_wms='Expedido'
AND date_1st_attempt IS NOT NULL;

SELECT  'Update date_status_value_chain2',now();
UPDATE operations_pe.pro_order_tracking_dates AS a
SET a.date_status_value_chain=date_delivered,
status_value_chain='Entregado'
WHERE status_wms='Expedido'
AND date_delivered IS NOT NULL;

SELECT  'Update date_status_value_chain3',now();
UPDATE operations_pe.pro_order_tracking_dates AS a
INNER JOIN wmsprod_pe.inverselogistics_devolucion AS b
ON a.item_id=b.item_id
SET a.date_status_value_chain=b.modified_at,
status_value_chain=b.status;

#workdays to create po
SELECT  'Update workdays_to_po',now();
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
	ON pro_order_tracking_dates.date_exported = calcworkdays.date_first
		AND pro_order_tracking_dates.date_po_created = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_po =	calcworkdays.workdays;

SELECT  'Update workdays_to_po',now();
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
	ON pro_order_tracking_dates.date_exported = calcworkdays.date_first
		AND curdate() = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_po =	calcworkdays.workdays
WHERE date_exported IS NULL;

#Workdays to confirm
SELECT  'Update workdays_to_confirm',now();
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
	ON pro_order_tracking_dates.date_po_created = calcworkdays.date_first
		AND pro_order_tracking_dates.date_po_confirmed = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_confirm =	calcworkdays.workdays;

SELECT  'Update workdays_to_confirm2',now();
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
	ON pro_order_tracking_dates.date_po_created = calcworkdays.date_first
		AND curdate() = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_confirm =	calcworkdays.workdays
WHERE date_exported IS NULL;

#Workdays to send po
SELECT  'Update workdays_to_send_po',now();
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
	ON pro_order_tracking_dates.date_po_confirmed = calcworkdays.date_first
		AND pro_order_tracking_dates.date_po_issued = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_send_po =	calcworkdays.workdays;

SELECT  'Update workdays_to_send_po',now();
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
	ON pro_order_tracking_dates.date_po_confirmed = calcworkdays.date_first
		AND curdate() = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_send_po =	calcworkdays.workdays
WHERE date_exported IS NULL;

#Workdays to export
SELECT  'Update workdays_to_export',now();
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
	ON pro_order_tracking_dates.date_ordered = calcworkdays.date_first
		AND pro_order_tracking_dates.date_exported = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_export =	calcworkdays.workdays;

SELECT  'Update workdays_to_export2',now();
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
	ON pro_order_tracking_dates.date_ordered = calcworkdays.date_first
		AND curdate() = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_export =	calcworkdays.workdays
WHERE date_exported IS NULL;

#Workdays to ready
SELECT  'Update workdays_to_ready',now();
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
	ON pro_order_tracking_dates.date_ready_to_pick	= calcworkdays.date_first
		AND pro_order_tracking_dates.date_ready_to_ship 	= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_ready 	=	calcworkdays.workdays;

SELECT  'Update workdays_to_ready2',now();
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
	ON pro_order_tracking_dates.date_ready_to_pick	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_ready 	=	calcworkdays.workdays
WHERE date_ready_to_ship IS NULL;

#Workdays to ship
SELECT  'Update workdays_to_ship',now();
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
	ON pro_order_tracking_dates.date_ready_to_ship	= calcworkdays.date_first
		AND pro_order_tracking_dates.date_shipped 		= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_ship 		=	calcworkdays.workdays;

SELECT  'Update workdays_to_ship2',now();
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
	ON pro_order_tracking_dates.date_ready_to_ship	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_ship 	=	calcworkdays.workdays
WHERE date_shipped IS NULL;

#workdays_to_1st_attempt
SELECT  'Update workdays_to_1st_attempt',now();
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
	ON pro_order_tracking_dates.date_shipped	= calcworkdays.date_first
		AND pro_order_tracking_dates.date_1st_attempt 		= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_1st_attempt 		=	calcworkdays.workdays;

SELECT  'Update workdays_to_1st_attempt',now();
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
	ON pro_order_tracking_dates.date_shipped	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_1st_attempt 	=	calcworkdays.workdays
WHERE date_1st_attempt IS NULL;

#workdays_to_deliver
SELECT  'Update workdays_to_deliver',now();
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
	ON pro_order_tracking_dates.date_shipped	= calcworkdays.date_first
		AND pro_order_tracking_dates.date_delivered 		= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_deliver 		=	calcworkdays.workdays;

SELECT  'Update workdays_to_deliver2',now();
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
	ON pro_order_tracking_dates.date_shipped	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_deliver 	=	calcworkdays.workdays
WHERE date_delivered IS NULL;

#workdays to request recollection
DROP TEMPORARY TABLE IF EXISTS operations_pe.tmp_stockout_seller_center;
 
CREATE TEMPORARY TABLE operations_pe.tmp_stockout_seller_center (INDEX (itens_venda_id))
SELECT
                a.itens_venda_id,
                a.numero_order,
                a.item_id,
                b.data
FROM
                wmsprod_pe.itens_venda a
INNER JOIN ( SELECT
 *
FROM wmsprod_pe.status_itens_venda
WHERE 
        detail LIKE '%stock out%'
AND 
status = 'cancelado') b
ON  a.itens_venda_id = b.itens_venda_id;

SELECT  'Drop tmp_order_tracking_stockout_declared',now();
DROP TEMPORARY TABLE IF EXISTS operations_pe.tmp_order_tracking_stockout_declared;

SELECT  'Create tmp_order_tracking_stockout_declared',now();
CREATE TEMPORARY TABLE operations_pe.tmp_order_tracking_stockout_declared (INDEX (itens_venda_id))
SELECT
                a.itens_venda_id,
                a.numero_order,
                a.item_id,
                b.date
FROM
                wmsprod_pe.itens_venda a
INNER JOIN ( 	SELECT
								itens_venda_id,
								max(DATA) date
							FROM
								wmsprod_pe.status_itens_venda
							WHERE STATUS = 'Quebrado'
							GROUP BY itens_venda_id) b
ON  a.itens_venda_id = b.itens_venda_id;

SELECT  'Update date_declared_stockout',now();
UPDATE operations_pe.pro_order_tracking_dates d
INNER JOIN tmp_order_tracking_stockout_declared t
                ON d.item_id = t.item_id
SET d.date_declared_stockout = t.date;

UPDATE operations_pe.pro_order_tracking_dates d
INNER JOIN operations_pe.tmp_stockout_seller_center t
                ON d.item_id = t.item_id
SET d.date_declared_stockout = t.data,
d.datetime_declared_stockout = t.data;

#workdays to stockout declared
SELECT  'Update date_declared_stockout2',now();
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
 ON pro_order_tracking_dates.date_ordered = calcworkdays.date_first
  AND pro_order_tracking_dates.date_declared_stockout  = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_stockout_declared = calcworkdays.workdays
WHERE date_declared_stockout is not null ;

#workdays to create tracking code
SELECT  'Update workdays_to_create_tracking_code',now();
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
 ON date(pro_order_tracking_dates.date_po_confirmed) = calcworkdays.date_first
  AND date(pro_order_tracking_dates.date_shipped)  = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_create_tracking_code = calcworkdays.workdays
WHERE date_po_confirmed is not null AND  date_shipped  is not null ;

#workdays_total_1st_attempt
SELECT  'Update workdays_total_1st_attempt',now();
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
	ON pro_order_tracking_dates.date_ordered	= calcworkdays.date_first
		AND pro_order_tracking_dates.date_1st_attempt 		= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_total_1st_attempt 		=	calcworkdays.workdays
WHERE date_1st_attempt IS NOT NULL;

SELECT  'Update workdays_total_1st_attempt',now();
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
	ON pro_order_tracking_dates.date_ordered	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_total_1st_attempt 	=	calcworkdays.workdays
WHERE date_ordered IS NOT NULL
AND date_1st_attempt IS NULL;

#workdays_total_delivery
SELECT  'Update workdays_total_delivery',now();
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
	ON pro_order_tracking_dates.date_ordered	= calcworkdays.date_first
		AND pro_order_tracking_dates.date_delivered 		= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_total_delivery 		=	calcworkdays.workdays
WHERE date_delivered IS NOT NULL;

SELECT  'Update workdays_total_delivery',now();
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
	ON pro_order_tracking_dates.date_ordered	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_total_delivery 	=	calcworkdays.workdays
WHERE date_ordered IS NOT NULL
AND date_delivered IS NULL;

#workdays_total_shipment
UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
	ON pro_order_tracking_dates.date_ordered	= calcworkdays.date_first
		AND pro_order_tracking_dates.date_shipped = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_total_shipment 		=	calcworkdays.workdays
WHERE date_shipped IS NOT NULL;

UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
	ON pro_order_tracking_dates.date_ordered	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_total_shipment 	=	calcworkdays.workdays
WHERE date_shipped IS NULL
AND date_ordered IS NOT NULL;

UPDATE operations_pe.pro_order_tracking_dates
INNER JOIN operations_pe.calcworkdays
	ON pro_order_tracking_dates.date_exported	= calcworkdays.date_first
		AND date_declared_backorder 	= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_backorder_declared 	=	calcworkdays.workdays
WHERE date_exported IS NOT NULL
AND date_declared_backorder IS NOT NULL;

SELECT  'Update Week y Month',now();
UPDATE operations_pe.pro_order_tracking_dates
SET 
 week_ordered = operations_pe.week_iso (date_ordered),
 week_procured_promised = operations_pe.week_iso (date_procured_promised),
 week_r2s_promised = operations_pe.week_iso (date_ready_to_ship_promised),
 week_delivered_promised = operations_pe.week_iso (date_delivered_promised),
 week_shipped = operations_pe.week_iso (date_shipped),
 month_procured_promised = date_format(date_procured_promised,"%Y-%m"),
 month_delivered_promised = date_format(date_delivered_promised,"%Y-%m"),
 month_r2s_promised = date_format(date_ready_to_ship_promised,"%Y-%m");

SELECT  'Update r2s_workday',now();
UPDATE        operations_pe.pro_order_tracking_dates
   INNER JOIN operations_pe.calcworkdays
           ON     pro_order_tracking_dates.date_ready_to_pick = calcworkdays.date_first
              AND calcworkdays.date_last = curdate() 
SET 
   r2s_workday_0  = IF( workdays_to_ready  = 0 AND workdays >= 0, 1 , r2s_workday_0 ),
   r2s_workday_1  = IF( workdays_to_ready <= 1 AND workdays >= 1, 1 , r2s_workday_1 ),
   r2s_workday_2  = IF( workdays_to_ready <= 2 AND workdays >= 2, 1 , r2s_workday_2 ),
   r2s_workday_3  = IF( workdays_to_ready <= 3 AND workdays >= 3, 1 , r2s_workday_3 ),
   r2s_workday_4  = IF( workdays_to_ready <= 4 AND workdays >= 4, 1 , r2s_workday_4 ),
   r2s_workday_5  = IF( workdays_to_ready <= 5 AND workdays >= 5, 1 , r2s_workday_5 ),
   r2s_workday_6  = IF( workdays_to_ready <= 6 AND workdays >= 6, 1 , r2s_workday_6 ), 
   r2s_workday_7  = IF( workdays_to_ready <= 7 AND workdays >= 7, 1 , r2s_workday_7 ), 
   r2s_workday_8  = IF( workdays_to_ready <= 8 AND workdays >= 8, 1 , r2s_workday_8 ),  
   r2s_workday_9  = IF( workdays_to_ready <= 9 AND workdays >= 9, 1 , r2s_workday_9 ),  
   r2s_workday_10 = IF( workdays_to_ready  >10 AND workdays >=10, 1 , r2s_workday_10);

SELECT  'Update check_dates',now();
UPDATE operations_pe.pro_order_tracking_dates
SET check_dates =
								 IF (date_ordered > date_exported,"Date ordered > date exported",
								 IF (date_exported > date_procured,"Date exported > Date procured",
								 IF (date_procured > date_ready_to_ship,"Date procured > Date ready to ship",
								 IF (date_ready_to_ship > date_shipped,"Date ready to ship > Date shipped",
								 IF (date_shipped > date_1st_attempt,"Date shipped > Date 1st attempt",
								 IF (date_1st_attempt > date_delivered,"Date 1st atteempt> Date delivered",
																 "Correct"))))));

SELECT  'Update checks',now();
UPDATE operations_pe.pro_order_tracking_dates
SET
check_date_exported = CASE
												WHEN date_exported IS NULL
												THEN (CASE
																WHEN date_procured IS NULL
																AND date_ready_to_ship IS NULL
																AND date_shipped IS NULL
																AND date_1st_attempt IS NULL
																AND date_delivered IS NULL
																THEN 0
																ELSE 1
															END )
												ELSE (CASE
															 WHEN date_exported > date_procured
															 THEN 1
															 ELSE 0
															END)
												END,
check_date_ordered = CASE
											WHEN date_ordered IS NULL
											THEN (  CASE
															 WHEN date_exported IS NULL
															 AND date_procured IS NULL
															 AND date_ready_to_ship IS NULL
															 AND date_shipped IS NULL
															 AND date_1st_attempt IS NULL
															 AND date_delivered IS NULL
															 THEN 0
															 ELSE 1
															 END )
											ELSE (  CASE
															 WHEN date_ordered > date_exported
															 THEN 1
															 ELSE 0
															 END )
															END,
check_date_procured = CASE
												WHEN date_procured IS NULL
												THEN (  CASE
																 WHEN date_ready_to_ship IS NULL
																 AND date_shipped IS NULL
																 AND date_1st_attempt IS NULL
																 AND date_delivered IS NULL
																 THEN 0
																 ELSE 1
																 END )
												ELSE (  CASE
																 WHEN date_procured > date_ready_to_ship
																 THEN 1
																 ELSE 0
																 END )
																END,
check_date_ready_to_ship = CASE
														 WHEN date_ready_to_ship IS NULL
														 THEN (  CASE
																			 WHEN date_shipped IS NULL
																			 AND date_1st_attempt IS NULL
																			 AND date_delivered IS NULL
																			 THEN 0
																			 ELSE 1
																			 END )
														 ELSE (    CASE
																				WHEN date_ready_to_ship > date_shipped
																				THEN 1
																				ELSE 0
																				END)
														 END,
check_date_shipped = CASE
											WHEN date_shipped IS NULL
											THEN (  CASE
																											WHEN date_1st_attempt IS NULL
																											AND date_delivered IS NULL
																											THEN 0
																											ELSE 1
																											END )
											ELSE (    CASE
																											WHEN date_shipped > date_1st_attempt
																											THEN 1
																											ELSE 0
																											END)
											END,
check_date_1st_attempt = CASE
													WHEN date_1st_attempt IS NULL
													THEN (  CASE
																												 WHEN date_delivered IS NULL
																												 THEN 0
																												 ELSE 1
																												 END )
													ELSE (    CASE
																												 WHEN date_1st_attempt > date_delivered
																												 THEN 1
																												 ELSE 0
																												 END )
													END;

SELECT  'Update check_date_stockout_over_bo',now();
UPDATE operations_pe.pro_order_tracking_dates
SET
 check_date_stockout_over_bo = IF(min_delivery_time>=max_delivery_time,1,0);

SELECT  'Update times',now();
UPDATE operations_pe.pro_order_tracking_dates
SET 
pro_order_tracking_dates.time_to_po = If(datetime_exported is Null,null,If(datetime_po_created is Null, TIMEDIFF(CURDATE(),datetime_exported), TIMEDIFF(datetime_po_created,datetime_exported))),
pro_order_tracking_dates.time_to_confirm = If(datetime_po_issued is Null,null,If(datetime_po_confirmed is Null, TIMEDIFF(CURDATE(),datetime_po_issued), TIMEDIFF(datetime_po_confirmed,datetime_po_issued))),
pro_order_tracking_dates.time_to_send_po = If(datetime_po_created is Null,null,If(datetime_po_issued is Null, TIMEDIFF(CURDATE(),datetime_po_created), TIMEDIFF(datetime_po_issued,datetime_po_created))),
pro_order_tracking_dates.time_to_procure = If(datetime_exported is Null,null,If(datetime_procured is Null, TIMEDIFF(CURDATE(),datetime_exported), TIMEDIFF(datetime_procured,datetime_exported))),
pro_order_tracking_dates.time_to_export = If(datetime_ordered is Null,null,If(datetime_exported is Null, TIMEDIFF(CURDATE(),datetime_ordered), TIMEDIFF(datetime_exported,datetime_ordered))),
pro_order_tracking_dates.time_to_ready = If(datetime_ready_to_pick is Null,null,If(datetime_ready_to_ship is Null, TIMEDIFF(CURDATE(),datetime_ready_to_pick), TIMEDIFF(datetime_ready_to_ship,datetime_ready_to_pick))),
pro_order_tracking_dates.time_to_ship = If(datetime_ready_to_ship is Null,null,If(datetime_shipped is Null, TIMEDIFF(CURDATE(),datetime_ready_to_ship), TIMEDIFF(datetime_shipped,datetime_ready_to_ship))),
pro_order_tracking_dates.time_to_1st_attempt = If(datetime_shipped is Null,null,If(date_1st_attempt is Null, TIMEDIFF(CURDATE(),datetime_shipped), TIMEDIFF(date_1st_attempt,datetime_shipped))),
pro_order_tracking_dates.time_to_deliver = If(datetime_shipped is Null,null,If(date_delivered is Null, TIMEDIFF(CURDATE(),datetime_shipped), TIMEDIFF(date_delivered,datetime_shipped))),
pro_order_tracking_dates.time_to_request_recollection = If(datetime_shipped is Null,null,If(date_delivered is Null, TIMEDIFF(CURDATE(),datetime_shipped), TIMEDIFF(date_delivered,datetime_shipped))),
pro_order_tracking_dates.time_to_stockout_declared = If(datetime_ordered is Null,null,If(date_declared_stockout is Null, TIMEDIFF(CURDATE(),datetime_ordered), TIMEDIFF(date_declared_stockout,datetime_ordered))),
pro_order_tracking_dates.time_to_createPO = If(datetime_exported is Null,null,If(datetime_po_created is Null, TIMEDIFF(CURDATE(),datetime_exported), TIMEDIFF(datetime_po_created,datetime_exported))),
pro_order_tracking_dates.time_to_create_tracking_code = If(datetime_po_confirmed is Null,null,If(datetime_shipped is Null, TIMEDIFF(CURDATE(),datetime_po_confirmed), TIMEDIFF(datetime_shipped,datetime_po_confirmed))),
pro_order_tracking_dates.time_total_1st_attempt = If(datetime_ordered is Null,null,If(date_1st_attempt is Null, TIMEDIFF(CURDATE(),datetime_ordered), TIMEDIFF(date_1st_attempt,datetime_ordered))),
pro_order_tracking_dates.time_total_delivery = If(datetime_ordered is Null,null,If(date_delivered is Null, TIMEDIFF(CURDATE(),datetime_ordered), TIMEDIFF(date_delivered,datetime_ordered))),
pro_order_tracking_dates.time_to_backorder = If(datetime_exported is Null,null,If(datetime_backorder is Null, TIMESTAMPDIFF(SECOND,datetime_exported, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_exported,datetime_backorder)/3600)),
pro_order_tracking_dates.time_to_backorder_tratada = If(datetime_backorder is Null,null,If(datetime_backorder_tratada is Null, TIMESTAMPDIFF(SECOND,datetime_backorder, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_backorder,datetime_backorder_tratada)/3600)),
pro_order_tracking_dates.time_to_backorder_accepted = If(datetime_backorder is Null,null,If(datetime_backorder_accepted is Null, TIMESTAMPDIFF(SECOND,datetime_backorder, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_backorder,datetime_backorder_accepted)/3600)),
pro_order_tracking_dates.time_to_declared_stockout = If(datetime_exported is Null,null,If(datetime_declared_stockout is Null, TIMESTAMPDIFF(SECOND,datetime_exported, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_exported,datetime_declared_stockout)/3600)),
pro_order_tracking_dates.time_to_declared_backorder = If(datetime_exported is Null,null,If(datetime_declared_backorder is Null, TIMESTAMPDIFF(SECOND,datetime_exported, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_exported,datetime_declared_backorder)/3600)),
pro_order_tracking_dates.time_total_shipment = If(datetime_ordered is Null,null,If(datetime_shipped is Null, TIMEDIFF(CURDATE(),datetime_ordered), TIMEDIFF(datetime_shipped,datetime_ordered)));


SELECT  'Update on_times',now();
UPDATE operations_pe.pro_order_tracking_dates
SET 
#ontime_to_po = If(date(date_po_created)<=date_po_created_promised,1,0),
#on_time_to_confirm = If(date(date_po_confirmed)<=date_po_confirmed_promised,1,0),
#on_time_to_send_po = If(date(date_po_issued)<=date_po_issued_promised,1,0),
on_time_to_procure = If(date(date_procured)<=date_procured_promised,1,0),
#ontime_to_export = If(date(date_exported)<=date_exported_promised,1,0),
on_time_to_ready = if(date_ready_to_ship<=date_ready_to_ship_promised,1,0),
on_time_to_ship = if(date_shipped<=date_ready_to_ship_promised,1,0),
on_time_to_1st_attempt = if(date_1st_attempt <=date_delivered_promised,1,0),
on_time_to_deliver = if(date_delivered<=date_delivered_promised,1,0),
#on_time_to_request_recollection = If(date_first_attempt<=promised_delivery_date,1,0),
#on_time_to_stockout_declared = If(date_shipped is not null,(if(date_shipped<=date_ready_to_ship_promised,1,0)),(if(date_ready_to_ship_promised<=curdate(),1,0))),
#on_time_to_create_po = If(date_shipped is not null,(if(date_shipped<=date_ready_to_ship_promised,1,0)),(if(date_ready_to_ship_promised<=curdate(),1,0))),
#on_time_to_create_tracking_code = If(date_first_attempt<=promised_delivery_date,1,0),
#on_time_total_1st_attempt = If(date_delivered<=date_delivered_promised,1,0),
on_time_total_delivery = If(date_delivered<=date_delivered_promised,1,0);

SELECT  'Update days',now();
UPDATE operations_pe.pro_order_tracking_dates
SET 
pro_order_tracking_dates.days_to_po = If(date_exported is Null,null,If(date_po_created is Null, DATEDIFF(CURDATE(),date_exported), DATEDIFF(date_po_created,date_exported))),
pro_order_tracking_dates.days_to_confirm = If(date_po_issued is Null,null,If(date_po_confirmed is Null, DATEDIFF(CURDATE(),date_po_issued), DATEDIFF(date_po_confirmed,date_po_issued))),
pro_order_tracking_dates.days_to_send_po = If(date_po_created is Null,null,If(date_po_issued is Null, DATEDIFF(CURDATE(),date_po_created), DATEDIFF(date_po_issued,date_po_created))),
pro_order_tracking_dates.days_to_procure = If(date_exported is Null,null,If(date_procured is Null, DATEDIFF(CURDATE(),date_exported), DATEDIFF(date_procured,date_exported))),
pro_order_tracking_dates.days_to_export = If(date_ordered is Null,null,If(date_exported is Null, DATEDIFF(CURDATE(),date_ordered), DATEDIFF(date_exported,date_ordered))),
pro_order_tracking_dates.days_to_ready = If(date_ready_to_pick is Null,null,If(date_ready_to_ship is Null, DATEDIFF(CURDATE(),date_ready_to_pick), DATEDIFF(date_ready_to_ship,date_ready_to_pick))),
pro_order_tracking_dates.days_to_ship = If(date_ready_to_ship is Null,null,If(date_shipped is Null, DATEDIFF(CURDATE(),date_ready_to_ship), DATEDIFF(date_shipped,date_ready_to_ship))),
pro_order_tracking_dates.days_to_1st_attempt = If(date_shipped is Null,null,If(date_1st_attempt is Null, DATEDIFF(CURDATE(),date_shipped), DATEDIFF(date_1st_attempt,date_shipped))),
pro_order_tracking_dates.days_to_deliver = If(date_shipped is Null,null,If(date_delivered is Null, DATEDIFF(CURDATE(),date_shipped), DATEDIFF(date_delivered,date_shipped))),
pro_order_tracking_dates.days_to_request_recollection = If(date_shipped is Null,null,If(date_delivered is Null, DATEDIFF(CURDATE(),date_shipped), DATEDIFF(date_delivered,date_shipped))),
pro_order_tracking_dates.days_to_stockout_declared = If(date_ordered is Null,null,If(date_declared_stockout is Null,null, DATEDIFF(date_declared_stockout,date_ordered))),
pro_order_tracking_dates.days_to_createPO = If(date_exported is Null,null,If(date_po_created is Null, DATEDIFF(CURDATE(),date_exported), DATEDIFF(date_po_created,date_exported))),
pro_order_tracking_dates.days_to_create_tracking_code = If(date_po_confirmed is Null,null,If(date_shipped is Null, DATEDIFF(CURDATE(),date_po_confirmed), DATEDIFF(date_shipped,date_po_confirmed))),
pro_order_tracking_dates.days_total_1st_attempt = If(date_ordered is Null,null,If(date_1st_attempt is Null, DATEDIFF(CURDATE(),date_ordered), DATEDIFF(date_1st_attempt,date_ordered))),
pro_order_tracking_dates.days_total_delivery = If(date_ordered is Null,null,If(date_delivered is Null, DATEDIFF(CURDATE(),date_ordered), DATEDIFF(date_delivered,date_ordered))),
pro_order_tracking_dates.days_to_backorder_declared = If(date_exported is Null,null,If(date_declared_backorder is Null, null, DATEDIFF(date_declared_backorder,date_exported))),
pro_order_tracking_dates.days_total_shipment = If(date_ordered is Null,null,If(date_shipped is Null, DATEDIFF(CURDATE(),date_ordered), DATEDIFF(date_shipped,date_ordered)));


UPDATE operations_pe.pro_order_tracking_dates
SET 
days_to_procure = NULL,
days_to_ready = NULL,
days_to_ship = days_total_shipment-days_to_export,
workdays_to_procure = NULL,
workdays_to_ready = NULL,
workdays_to_ship = workdays_total_shipment-workdays_to_export,
time_to_procure = NULL,
time_to_ready = NULL,
time_to_ship = time_total_shipment-time_to_export
WHERE fulfillment_type_real = 'Dropshipping';

SELECT  'Update shipped_same_day_as_procured',now();
UPDATE operations_pe.pro_order_tracking_dates
SET 
pro_order_tracking_dates.shipped_same_day_as_procured = If (date_procured = date_shipped,1,0), 
pro_order_tracking_dates.shipped_same_day_as_order = If (date_ordered = date_shipped ,1,0);

SELECT  'Update 24hr_shipments',now();
UPDATE operations_pe.pro_order_tracking_dates
SET 
24hr_shipments_Cust_persp = If(date_ordered is Null,null,If(date_shipped is Null,if( Datediff(CURDATE(),date_ordered)<= 1,1,0), if( Datediff(date_shipped,date_ordered)<=1,1,0))),
24hr_shipmnets_WH_persp = If(date_ready_to_pick is Null,null,If(date_shipped is Null, if( Datediff(CURDATE(),date_ready_to_pick)<= 1,1,0), if( Datediff(date_shipped,date_ready_to_pick)<=1,1,0)));

DROP TEMPORARY TABLE IF EXISTS operations_pe.tmp_usuarios_wms;
CREATE TEMPORARY TABLE operations_pe.tmp_usuarios_wms(INDEX (item_id))
SELECT item_id,nome FROM (SELECT itens_venda_id,nome,status
			FROM wmsprod_pe.status_itens_venda AS a
			INNER JOIN wmsprod_pe.usuarios AS b
            ON a.usuarios_id=b.usuarios_id ORDER BY data DESC) AS t1
			INNER JOIN wmsprod_pe.itens_venda AS c
			ON (c.itens_venda_id=t1.itens_venda_id
			AND c.status=t1.status);

UPDATE operations_pe.out_order_tracking_sample AS a
INNER JOIN operations_pe.tmp_usuarios_wms AS b
ON b.item_id = a.item_id
SET a.user_status_wms = b.nome;

DROP TEMPORARY TABLE IF EXISTS operations_pe.tmp_usuarios_bob;
CREATE TEMPORARY TABLE operations_pe.tmp_usuarios_bob(INDEX (item_id))
SELECT item_id,username FROM (SELECT d.fk_sales_order_item,username
			FROM bob_live_pe.sales_order_item_status_history AS d
			INNER JOIN bob_live_pe.sales_order_item_status AS e
			ON d.fk_sales_order_item_status=e.id_sales_order_item_status
			LEFT JOIN bob_live_pe.acl_user AS f
			ON d.fk_acl_user=f.id_acl_user
			ORDER BY d.id_sales_order_item_status_history DESC) AS t2
			INNER JOIN operations_pe.out_order_tracking AS g
			ON g.item_id=t2.fk_sales_order_item;

UPDATE operations_pe.out_order_tracking_sample AS a
INNER JOIN operations_pe.tmp_usuarios_bob AS b
ON b.item_id = a.item_id
SET a.user_status_bob = b.username;

SELECT  'Update out_order_tracking_sample',now();
UPDATE operations_pe.out_order_tracking_sample a
INNER JOIN operations_pe.pro_order_tracking_dates AS b
ON a.item_id = b.item_id
SET
a.sku_simple = b.sku_simple,
a.status_wms = b.status_wms,
a.fulfillment_type_bob = b.fulfillment_type_bob,
a.supplier_leadtime = b.supplier_leadtime,
a.supplier_id = b.supplier_id,
a.min_delivery_time = b.min_delivery_time,
a.max_delivery_time = b.max_delivery_time,
a.date_ordered = b.date_ordered,
a.date_exported = b.date_exported,
a.date_backorder = b.date_backorder,
a.date_backorder_tratada = b.date_backorder_tratada,
a.date_backorder_accepted = b.date_backorder_accepted,
a.date_po_created = b.date_po_created,
a.date_po_updated = b.date_po_updated,
a.date_po_issued = b.date_po_issued,
a.date_po_confirmed = b.date_po_confirmed,
a.date_shipped = b.date_shipped,
a.date_procured = b.date_procured,
a.date_assigned_to_stock = b.date_assigned_to_stock,
a.date_ready_to_pick = b.date_ready_to_pick,
a.date_ready_to_ship = b.date_ready_to_ship,
a.date_shipped = b.date_shipped,
a.date_1st_attempt = b.date_1st_attempt,
a.date_delivered = b.date_delivered,
a.date_failed_delivery = b.date_failed_delivery,
a.date_returned = b.date_returned,
a.date_procured_promised = b.date_procured_promised,
a.date_ready_to_ship_promised = b.date_ready_to_ship_promised,
a.date_delivered_promised = b.date_delivered_promised,
a.status_value_chain = b.status_value_chain,
a.date_status_value_chain = b.date_status_value_chain,
a.date_stock_updated = b.date_stock_updated,
a.date_declared_stockout = b.date_declared_stockout,
a.date_declared_backorder = b.date_declared_backorder,
a.datetime_ordered = b.datetime_ordered,
a.datetime_exported = b.datetime_exported,
a.datetime_po_created = b.datetime_po_created,
a.datetime_po_updated = b.datetime_po_updated,
a.datetime_po_issued = b.datetime_po_issued,
a.datetime_po_confirmed = b.datetime_po_confirmed,
a.datetime_shipped = b.datetime_shipped,
a.datetime_procured = b.datetime_procured,
a.datetime_assigned_to_stock = b.datetime_assigned_to_stock,
a.datetime_ready_to_pick = b.datetime_ready_to_pick,
a.datetime_ready_to_ship = b.datetime_ready_to_ship,
a.datetime_shipped = b.datetime_shipped,
a.datetime_backorder = b.datetime_backorder,
a.datetime_backorder_tratada = b.datetime_backorder_tratada,
a.datetime_backorder_accepted = b.datetime_backorder_accepted,
a.datetime_declared_stockout = b.datetime_declared_stockout,
a.datetime_declared_backorder = b.datetime_declared_backorder,
a.week_ordered = b.week_ordered,
a.week_procured_promised = b.week_procured_promised,
a.week_delivered_promised = b.week_delivered_promised,
a.week_r2s_promised = b.week_r2s_promised,
a.week_shipped = b.week_shipped,
a.month_procured_promised = b.month_procured_promised,
a.month_delivered_promised = b.month_delivered_promised,
a.month_r2s_promised = b.month_r2s_promised,
a.check_dates = b.check_dates,
a.check_date_exported = b.check_date_exported,
a.check_date_ordered = b.check_date_ordered,
a.check_date_procured = b.check_date_procured,
a.check_date_ready_to_ship = b.check_date_ready_to_ship,
a.check_date_shipped = b.check_date_shipped,
a.check_date_1st_attempt = b.check_date_1st_attempt,
a.check_date_stockout_over_bo = b.check_date_stockout_over_bo,
a.check_min_max = b.check_min_max,
a.workdays_to_po = b.workdays_to_po,
a.workdays_to_confirm = b.workdays_to_confirm,
a.workdays_to_send_po = b.workdays_to_send_po,
a.workdays_to_procure = b.workdays_to_procure,
a.workdays_to_export = b.workdays_to_export,
a.workdays_to_ready = b.workdays_to_ready,
a.workdays_to_ship = b.workdays_to_ship,
a.workdays_to_1st_attempt = b.workdays_to_1st_attempt,
a.workdays_to_deliver = b.workdays_to_deliver,
a.workdays_to_request_recollection = b.workdays_to_request_recollection,
a.workdays_to_stockout_declared = b.workdays_to_stockout_declared,
a.workdays_to_createPO = b.workdays_to_createPO,
a.workdays_to_create_tracking_code = b.workdays_to_create_tracking_code,
a.workdays_total_1st_attempt = b.workdays_total_1st_attempt,
a.workdays_total_delivery = b.workdays_total_delivery,
a.is_backorder = b.is_backorder,
a.workdays_to_backlog = b.workdays_to_backlog,
a.days_to_po = b.days_to_po,
a.days_to_confirm = b.days_to_confirm,
a.days_to_send_po = b.days_to_send_po, 
a.days_to_procure = b.days_to_procure,
a.days_to_export = b.days_to_export, 
a.days_to_ready = b.days_to_ready, 
a.days_to_ship = b.days_to_ship, 
a.days_to_1st_attempt = b.days_to_1st_attempt,
a.days_to_deliver = b.days_to_deliver,
a.days_to_request_recollection = b.days_to_request_recollection,
a.days_to_stockout_declared = b.days_to_stockout_declared,
a.days_to_createPO = b.days_to_createPO, 
a.days_to_create_tracking_code = b.days_to_create_tracking_code,
a.days_total_1st_attempt = b.days_total_1st_attempt,
a.days_total_delivery = b.days_total_delivery,
#a.days_to_backlog = b.days_to_backlog, 
a.days_since_stock_updated = b.days_since_stock_updated, 
a.time_to_po = b.time_to_po,
a.time_to_confirm = b.time_to_confirm, 
a.time_to_send_po = b.time_to_send_po,
a.time_to_procure = b.time_to_procure, 
a.time_to_export = b.time_to_export,
a.time_to_ready = b.time_to_ready, 
a.time_to_ship = b.time_to_ship,
a.time_to_1st_attempt = b.time_to_1st_attempt, 
a.time_to_deliver = b.time_to_deliver,
a.time_to_request_recollection = b.time_to_request_recollection, 
a.time_to_stockout_declared = b.time_to_stockout_declared,
a.time_to_createPO = b.time_to_createPO,
a.time_to_create_tracking_code = b.time_to_create_tracking_code, 
a.time_total_1st_attempt = b.time_total_1st_attempt, 
a.time_total_delivery = b.time_total_delivery,
a.time_to_backorder = b.time_to_backorder,
a.time_to_backorder_tratada = b.time_to_backorder_tratada,
a.time_to_backorder_accepted = b.time_to_backorder_accepted,
a.time_to_declared_stockout = b.time_to_declared_stockout,
a.time_to_declared_backorder = b.time_to_declared_backorder,
a.user_cs_bo = b.user_cs_bo,
a.user_cs_oos = b.user_cs_bo,
#a.time_to_backlog = b.time_to_backlog, 
a.on_time_to_po = b.on_time_to_po,
a.on_time_to_confirm = b.on_time_to_confirm,
a.on_time_to_send_po = b.on_time_to_send_po, 
a.on_time_to_procure = b.on_time_to_procure, 
a.on_time_to_export = b.on_time_to_export, 
a.on_time_to_ready = b.on_time_to_ready, 
a.on_time_to_ship = b.on_time_to_ship, 
a.on_time_to_1st_attempt = b.on_time_to_1st_attempt,
a.on_time_to_deliver = b.on_time_to_deliver, 
a.on_time_to_request_recollection = b.on_time_to_request_recollection,
a.on_time_to_stockout_declared = b.on_time_to_stockout_declared, 
a.on_time_to_create_po = b.on_time_to_create_po, 
a.on_time_to_create_tracking_code = b.on_time_to_create_tracking_code, 
#a.on_time_total_1st_attempt = b.on_time_total_1st_attempt, 
a.on_time_total_delivery = b.on_time_total_delivery, 
a.r2s_workday_0 = b.r2s_workday_0,
a.r2s_workday_1 = b.r2s_workday_1, 
a.r2s_workday_2 = b.r2s_workday_2, 
a.r2s_workday_3 = b.r2s_workday_3, 
a.r2s_workday_4 = b.r2s_workday_4, 
a.r2s_workday_5 = b.r2s_workday_5,
a.r2s_workday_6 = b.r2s_workday_6, 
a.r2s_workday_7 = b.r2s_workday_7, 
a.r2s_workday_8 = b.r2s_workday_8, 
a.r2s_workday_9 = b.r2s_workday_9, 
a.r2s_workday_10 = b.r2s_workday_10,
a.shipped_same_day_as_procured = b.shipped_same_day_as_procured, 
a.shipped_same_day_as_order = b.shipped_same_day_as_order, 
a.24hr_shipments_Cust_persp = b.24hr_shipments_Cust_persp,
a.24hr_shipmnets_WH_persp = b.24hr_shipmnets_WH_persp,
a.workdays_total_shipment = b.workdays_total_shipment, 
a.time_total_shipment = b.time_total_shipment, 
a.workdays_to_backorder_declared = b.workdays_to_backorder_declared,
a.days_to_backorder_declared = b.days_to_backorder_declared,
a.days_total_shipment = b.days_total_shipment;

########## Queries Dates

UPDATE operations_pe.out_order_tracking_sample
SET out_order_tracking_sample.carrier_time = max_delivery_time - wh_time - supplier_leadtime
WHERE fulfillment_type_bob not like 'Dropshipp%'
AND carrier_time = 0
AND max_delivery_time > (wh_time + supplier_leadtime);

SELECT  'Update effective_1st_attempt',now();
UPDATE operations_pe.out_order_tracking_sample
SET effective_1st_attempt =1
WHERE date_delivered = date_1st_attempt;

SELECT  'Update early_to_procure',now();
UPDATE operations_pe.out_order_tracking_sample
SET out_order_tracking_sample.early_to_procure = CASE
WHEN (
CASE
                WHEN date_procured IS NULL
                THEN datediff(date_procured_promised,curdate())
ELSE datediff(date_procured_promised,date_procured)
END) > 0
THEN 1
ELSE 0
END,
out_order_tracking_sample.early_to_1st_attempt = CASE
WHEN (
CASE
  WHEN date_1st_attempt IS NULL
                THEN datediff(date_delivered_promised,curdate())
ELSE datediff(date_delivered_promised,date_1st_attempt)
END) > 0
THEN 1
ELSE 0
END;



SELECT  'Update tms_tracks',now();
UPDATE operations_pe.out_order_tracking_sample
INNER JOIN operations_pe.tbl_bi_ops_guiasbyitem2
	ON operations_pe.out_order_tracking_sample.item_id = operations_pe.tbl_bi_ops_guiasbyitem2.item_id
SET operations_pe.out_order_tracking_sample.shipping_carrier_tracking_code = operations_pe.tbl_bi_ops_guiasbyitem2.total_guias;

#### Insert para A_Master
INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  step,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Peru',
  'out_order_tracking',
  'shipping_carrier_tracking_code',
  NOW(),
  max(datetime_ordered),
  count(*),
  count(item_counter)
FROM
  operations_pe.out_order_tracking;


SELECT  'Update days_since_stock_updated',now();
UPDATE operations_pe.out_order_tracking_sample AS a
SET a.days_since_stock_updated =
IF(a.date_stock_updated IS NULL,NULL,TIMESTAMPDIFF(DAY, date(a.date_stock_updated), CURDATE()));

SELECT  'Update status_bob',now();
UPDATE operations_pe.out_order_tracking_sample AS a
INNER JOIN bob_live_pe.sales_order_item AS b
	ON a.item_id = b.id_sales_order_item
INNER JOIN bob_live_pe.sales_order_item_status AS c
	ON b.fk_sales_order_item_status = c.id_sales_order_item_status
SET a.status_bob = c. NAME;

SELECT  'Update status_match_bob_wms',now();
UPDATE operations_pe.out_order_tracking_sample
INNER JOIN operations_pe.status_match_bob_wms 
	ON out_order_tracking_sample.status_wms = status_match_bob_wms.status_wms
	AND out_order_tracking_sample.status_bob = status_match_bob_wms.status_bob
SET 
	out_order_tracking_sample.status_match_bob_wms = status_match_bob_wms.correct;

SELECT  'Update catalog_shipment_type',now();
UPDATE operations_pe.out_order_tracking_sample AS a
INNER JOIN bob_live_pe.sales_order_item AS b
	ON a.item_id = b.id_sales_order_item
INNER JOIN bob_live_pe.catalog_shipment_type AS c
	ON b.fk_catalog_shipment_type = c.id_catalog_shipment_type
SET a.fulfillment_type_bob = c.NAME;



SELECT  'Update is_stockout',now();
UPDATE operations_pe.out_order_tracking_sample
SET
is_stockout = If(status_wms  IN(	'Quebrado',
																	'quebra tratada',
																	'backorder_tratada'),1,0),
is_backorder = if(date_backorder is not null,1,0),
is_ready_to_pick = If(date_ready_to_pick is not null,1,0),
is_ready_to_ship = If(date_ready_to_ship is not null,1,0),
is_procured = If(date_procured is not null,1,0),
is_shipped = If(date_shipped is not null,1,0),
is_first_attempt = If(date_1st_attempt is not null,1,0),
is_delivered = If(date_delivered is not null,1,0),
is_cancelled = If(status_wms in ('Cancelado','quebra tratada','Quebrado'),1,0),
still_to_procure = if(date_procured is null AND	status_wms not in ("quebrado", "quebra tratada") AND fulfillment_type_real = 'Crossdocking',1,0);

UPDATE operations_pe.out_order_tracking_sample d
INNER JOIN operations_pe.tmp_stockout_seller_center t
                ON d.item_id = t.item_id
SET d.is_stockout = 1 ;

SELECT  'Update is_presale',now();
UPDATE operations_pe.out_order_tracking_sample a
INNER JOIN bob_live_pe.catalog_config b
	ON a.sku_config = b.sku
SET 
	a.is_presale = IF(b. NAME LIKE "%preventa%",1,0),
	a.is_gift_card = IF(a.supplier_name like '%E-Gift Card%',1,0);

#Workdays to procure
SELECT  'Update workdays_to_procure',now();
UPDATE operations_pe.out_order_tracking_sample
INNER JOIN operations_pe.calcworkdays
	ON out_order_tracking_sample.date_exported 			= calcworkdays.date_first
		AND out_order_tracking_sample.date_procured 	= calcworkdays.date_last
SET out_order_tracking_sample.workdays_to_procure 	=	calcworkdays.workdays
WHERE is_stockout = 0
AND fulfillment_type_real not like 'Dropshipping';

SELECT  'Update workdays_to_procure',now();
UPDATE operations_pe.out_order_tracking_sample
INNER JOIN operations_pe.calcworkdays
	ON out_order_tracking_sample.date_exported 			= calcworkdays.date_first
		AND curdate() 																= calcworkdays.date_last
SET out_order_tracking_sample.workdays_to_procure =	calcworkdays.workdays
WHERE is_stockout = 0
AND date_procured IS NULL
AND fulfillment_type_real not like 'Dropshipping';

SELECT  'Update workdays_to_procure',now();
UPDATE operations_pe.pro_order_tracking_dates a
INNER JOIN operations_pe.out_order_tracking_sample b
	ON a.item_id = b.item_id
SET a.workdays_to_procure = b.workdays_to_procure;

SELECT  'Update stockout_order_nr',now();
UPDATE operations_pe.out_order_tracking_sample AS a
INNER JOIN bob_live_pe.sales_order_item_status_history AS b
ON a.item_id=b.fk_sales_order_item
SET
a.stockout_order_nr = 
 (SUBSTR(b.note,(SELECT INSTR(b.note, '200')),9))
WHERE a.is_stockout=1
AND b.fk_sales_order_item_status=9;

SELECT  'Update delays',now();
UPDATE operations_pe.out_order_tracking_sample
SET 
	delay_to_procure = CASE
													WHEN date_procured IS NULL 
													THEN(	CASE
																	WHEN curdate() > date_procured_promised 
																	AND fulfillment_type_real NOT IN ('Dropshipping')
																	THEN 1
																	ELSE 0
																END)
													ELSE(	CASE
																	WHEN date_procured > date_procured_promised 
																	THEN 1
																	ELSE 0
																END)
												END,
	delay_to_ready = 	CASE
											WHEN workdays_to_ready > 1 
											THEN 1
											ELSE 0
										END,
 delay_to_ship = 	CASE
										WHEN workdays_to_ship > 1 
										THEN 1
										ELSE 0
									END,
 delay_to_1st_attempt = CASE
													WHEN workdays_to_1st_attempt > 2 
													THEN 1
													ELSE 0
												END,
 delay_to_delivery = CASE
											WHEN workdays_to_deliver > 2 
											THEN 1
											ELSE 0
										END,
 delay_total_1st_attempt = CASE
														WHEN date_1st_attempt IS NULL 
														THEN	(	CASE
																			WHEN curdate() > date_delivered_promised 
																			THEN 1
																			ELSE 0
																		END)
														ELSE	( CASE
																			WHEN date_1st_attempt > date_delivered_promised 
																			THEN 1
																			ELSE 0
																		END)
														END,
delay_total_delivery = CASE
	WHEN date_delivered IS NULL 
		THEN	(	CASE
					WHEN curdate() > date_delivered_promised 
						THEN 1
						ELSE 0
				END)
		ELSE	(	CASE
					WHEN date_delivered > date_delivered_promised 
						THEN 1
						ELSE 0
				END)
END,
 workdays_delay_to_procure = CASE
															WHEN workdays_to_procure - supplier_leadtime < 0 
															THEN dias_habiles_negativos(date_procured_promised, CURDATE())
															ELSE workdays_to_procure - supplier_leadtime
														END,
 workdays_delay_to_ready = 	CASE
															WHEN workdays_to_ready - 1 < 0 
															THEN 0
															ELSE workdays_to_ready - 1
														END,
 workdays_delay_to_ship = CASE
														WHEN workdays_to_ship - 1 < 0 
														THEN 0
														ELSE workdays_to_ship - 1
													END,
 workdays_delay_to_1st_attempt = 	CASE
																		WHEN workdays_to_1st_attempt - 3 < 0 
																		THEN 0
																		ELSE workdays_to_1st_attempt - 3
																	END,
 workdays_delay_to_delivery = 	CASE
																WHEN workdays_to_deliver - 3 < 0 
																THEN 0
																ELSE workdays_to_deliver - 3
															END,
 on_time_to_procure = CASE
												WHEN date_procured <= date_procured_promised 
												THEN 1
												ELSE 0
											END,
 on_time_total_1st_attempt = 	CASE
																WHEN date_1st_attempt <= date_delivered_promised 
																THEN 1
																ELSE 0
															END,
 on_time_total_delivery = CASE
														WHEN date_delivered <= date_delivered_promised 
														THEN 1
														WHEN date_delivered IS NULL 
														THEN 0
														ELSE 0
													END;

SELECT  'Update backlog_delivery',now();
UPDATE operations_pe.out_order_tracking_sample
SET
backlog_delivery = If((status_wms='Expedido' AND delay_total_delivery = 1 AND date_delivered is null),1,0),
backlog_procurement = If((date_procured is null AND status_wms in ('Aguardando estoque','Analisando quebra') AND delay_to_procure=1 ),1,0);

SELECT  'Update stockout_recovered',now();
UPDATE operations_pe.out_order_tracking_sample AS a
INNER JOIN development_pe.A_Master AS b
ON a.stockout_order_nr=b.OrderNum
SET
a.stockout_recovered = 1
WHERE (stockout_order_nr <> '')
and status_wms in ('Quebrado','quebra tratada')
AND b.OrderAfterCan=1;

SELECT  'Update stockout_real',now();
UPDATE operations_pe.out_order_tracking_sample AS a
SET
a.stockout_real = 1
WHERE a.is_stockout=1
AND a.stockout_recovered is null;

#UPDATE operations_pe.out_order_tracking_sample AS a
#INNER JOIN tbl_orders_reo_rep_inv AS b
#ON a.item_id=b.item_original
#SET
#a.stockout_recovered_voucher = b.coupon_code,
#a.stockout_item_id = b.item_nueva
#WHERE b.coupon_code like 'REO%';

SELECT  'Update shipping_carrier_tracking_code_inverse',now();
UPDATE operations_pe.out_order_tracking_sample AS a
INNER JOIN wmsprod_pe.inverselogistics_agendamiento AS b
	ON a.item_id=b.item_id
SET a.shipping_carrier_tracking_code_inverse = b.carrier_tracking_code;

# C�mo ligar PO de PE
/* Dise�o Regional
UPDATE operations_pe.out_order_tracking_sample 
INNER JOIN procurement_live_pe.wms_imported_dropship_orders
	ON out_order_tracking_sample.item_id = wms_imported_dropship_orders.fk_sales_order_item
INNER JOIN procurement_live_pe.procurement_order_item_dropshipping 
	ON wms_imported_dropship_orders.id_wms_imported_dropship_order = procurement_order_item_dropshipping.fk_wms_imported_dropship_order
INNER JOIN procurement_live_pe.procurement_order_item
	ON procurement_order_item_dropshipping.fk_procurement_order_item = procurement_order_item.id_procurement_order_item
INNER JOIN procurement_live_pe.procurement_order
	ON procurement_order_item.fk_procurement_order = procurement_order.id_procurement_order
SET 
 out_order_tracking_sample.date_po_created = procurement_order.created_at,
 out_order_tracking_sample.date_po_issued = procurement_order.sent_at
WHERE operations_pe.out_order_tracking_sample.fulfillment_type_real = "dropshipping";*/

SELECT  'Update date_po_created',now();
UPDATE operations_pe.out_order_tracking_sample
INNER JOIN wmsprod_pe.itens_venda
	ON out_order_tracking_sample.item_id = itens_venda.item_id
INNER JOIN procurement_live_pe.wms_received_item
	ON itens_venda.itens_venda_id = wms_received_item.id_wms
INNER JOIN procurement_live_pe.procurement_order
	ON wms_received_item.fk_procurement_order = procurement_order.id_procurement_order
SET 
 out_order_tracking_sample.date_po_created = procurement_order.created_at,
 out_order_tracking_sample.date_po_issued = procurement_order.sent_at;

SELECT  'Update purchase_order',now();
UPDATE operations_pe.out_order_tracking_sample a
INNER JOIN wmsprod_pe.itens_venda b
ON a.item_id=b.item_id
INNER JOIN wmsprod_pe.estoque c
ON b.estoque_id=c.estoque_id
INNER JOIN wmsprod_pe.itens_recebimento d
ON c.itens_recebimento_id=d.itens_recebimento_id
INNER JOIN wmsprod_pe.recebimento e
ON d.recebimento_id=e.recebimento_id
SET a.purchase_order = e.inbound_document_identificator
WHERE e.inbound_document_type_id = 4;

SELECT  'Update delivery_fullfilment',now();
UPDATE operations_pe.out_order_tracking_sample a
INNER JOIN wmsprod_pe.entrega b
ON a.wms_tracking_code = b.cod_rastreamento
SET a.delivery_fullfilment = b.delivery_fulfillment;

#Fecha
SELECT  'Truncate pro_1st_attempt',now();
TRUNCATE	operations_pe.pro_1st_attempt;

SELECT  'Insert pro_1st_attempt',now();
INSERT INTO operations_pe.pro_1st_attempt (
	order_item_id, 
	min_of_date) 
SELECT
	out_order_tracking_sample.item_id,
	min(tms_status_delivery.date) AS min_of_date
FROM operations_pe.out_order_tracking_sample
INNER JOIN wmsprod_pe.vw_itens_venda_entrega 
	ON out_order_tracking_sample.item_id = vw_itens_venda_entrega.item_id
INNER JOIN wmsprod_pe.tms_status_delivery 
	ON vw_itens_venda_entrega.cod_rastreamento = tms_status_delivery.cod_rastreamento
GROUP BY
	out_order_tracking_sample.item_id,
	tms_status_delivery.id_status
HAVING
	tms_status_delivery.id_status = 5;

SELECT  'Update date_1st_attempt',now();
UPDATE operations_pe.out_order_tracking_sample
INNER JOIN operations_pe.pro_1st_attempt 
	ON out_order_tracking_sample.item_id = pro_1st_attempt.order_item_id
SET out_order_tracking_sample.date_1st_attempt = pro_1st_attempt.min_of_date;

SELECT  'Update date_1st_attempt',now();
UPDATE operations_pe.out_order_tracking_sample
INNER JOIN wmsprod_pe.vw_itens_venda_entrega vw 
	ON out_order_tracking_sample.item_id = vw.item_id
INNER JOIN wmsprod_pe.tms_status_delivery del 
	ON vw.cod_rastreamento = del.cod_rastreamento
SET out_order_tracking_sample.date_1st_attempt = (
	SELECT
		max(tms.date)
	FROM
		wmsprod_pe.tms_status_delivery tms,
		wmsprod_pe.vw_itens_venda_entrega itens
	WHERE
		del.cod_rastreamento = tms.cod_rastreamento
	AND vw.item_id = itens.item_id
	AND tms.id_status = 4
)
WHERE
#Fecha
	out_order_tracking_sample.date_1st_attempt IS NULL
OR out_order_tracking_sample.date_1st_attempt < '2011-05-01';

/*
SELECT  'Update date_ready_to_pick',now();	
UPDATE operations_pe.out_order_tracking_sample
#Fecha
SET out_order_tracking_sample.date_ready_to_pick = 	CASE
	WHEN dayofweek(date_procured) = 1 
	THEN operations_pe.workday (date_procured, 1)
	WHEN dayofweek(date_procured) = 7 
	THEN operations_pe.workday (date_procured, 1)
	ELSE date_procured
END;

SELECT  'Update datetime_ready_to_pick',now();	
UPDATE operations_pe.out_order_tracking_sample
SET out_order_tracking_sample.datetime_ready_to_pick = 	CASE
	WHEN dayofweek(datetime_procured) = 1 
	THEN operations_pe.workday (datetime_procured, 1)
	WHEN dayofweek(datetime_procured) = 7 
	THEN operations_pe.workday (datetime_procured, 1)
	ELSE datetime_procured
END;
*/


#Fecha pendientes 
SELECT  'Update ship_to_zip',now();	
UPDATE operations_pe.out_order_tracking_sample
INNER JOIN bob_live_pe.sales_order 
	ON out_order_tracking_sample.order_number = sales_order.order_nr
INNER JOIN bob_live_pe.sales_order_address 
	ON sales_order.fk_sales_order_address_shipping = sales_order_address.id_sales_order_address
SET 
	out_order_tracking_sample.ship_to_zip = postcode,
	out_order_tracking_sample.ship_to_zip2 = mid(postcode, 3, 2),
	out_order_tracking_sample.ship_to_zip3 = RIGHT (postcode, 3),
	out_order_tracking_sample.ship_to_zip4 = LEFT (postcode, 4);

# Pendiente agregar para CO
SELECT  'Update ship_to_zip2',now();	
UPDATE operations_pe.out_order_tracking_sample
SET ship_to_met_area = CASE
WHEN ship_to_zip BETWEEN 01000
AND 16999
OR ship_to_zip BETWEEN 53000
AND 53970
OR ship_to_zip BETWEEN 54000
AND 54198
OR ship_to_zip BETWEEN 54600
AND 54658
OR ship_to_zip BETWEEN 54700
AND 54769
OR ship_to_zip BETWEEN 54900
AND 54959
OR ship_to_zip BETWEEN 54960
AND 54990
OR ship_to_zip BETWEEN 52760
AND 52799
OR ship_to_zip BETWEEN 52900
AND 52998
OR ship_to_zip BETWEEN 55000
AND 55549
OR ship_to_zip BETWEEN 55700
AND 55739
OR ship_to_zip BETWEEN 57000
AND 57950 THEN
	1
ELSE
	0
END;

SELECT  'Update payment_method',now();	
UPDATE operations_pe.out_order_tracking_sample a
INNER JOIN wmsprod_pe.pedidos_venda b
ON a.order_number = b.numero_pedido
SET 
a.payment_method = b.metodo_de_pagamento,
a.ship_to_region = b.estado_cliente,
a.ship_to_city = b.cidade_cliente;

SELECT  'Update calendar',now();	
UPDATE operations_pe.out_order_tracking_sample AS a
INNER JOIN operations_pe.calendar AS b
ON DATE(a.date_shipped)=b.dt
AND b.isweekend=1
SET a.shipped_is_weekend=1;

SELECT  'Update note_tracking',now();	
UPDATE operations_pe.out_order_tracking_sample AS a
LEFT JOIN wmsprod_pe.itens_venda AS b
ON a.item_id=b.item_id
SET
a.note_tracking = b.obs;

#UPDATE operations_pe.out_order_tracking_sample AS a
#SET
#a.tracking_stockout= 
#CASE 
#   WHEN INSTR(note_tracking,'OOS')!=0 THEN "OOS" 
#   WHEN INSTR(note_tracking,'OOT')!=0 THEN "OOT" 
#   WHEN INSTR(note_tracking,'PNP')!=0 THEN "PNP" 
#   WHEN INSTR(note_tracking,'DSE')!=0 THEN "DSE" 
#   WHEN INSTR(note_tracking,'POL')!=0 THEN "POL"  
#   WHEN INSTR(note_tracking,'DTE')!=0 THEN "DTE"  
#   WHEN INSTR(note_tracking,'SOE')!=0 THEN "SOE"  
#   WHEN INSTR(note_tracking,'CBC')!=0 THEN "CBC"  
#   WHEN INSTR(note_tracking,'DTL')!=0 THEN "DTL"
#   WHEN INSTR(note_tracking,'OSW')!=0 THEN "OSW"
#   WHEN INSTR(note_tracking,'UNS')!=0 THEN "UNS"
#   WHEN INSTR(note_tracking,'OSM')!=0 THEN "OSM"
#   WHEN INSTR(note_tracking,'SWC')!=0 THEN "SWC"
#   WHEN INSTR(note_tracking,'OOA')!=0 THEN "OOA"
#   WHEN INSTR(note_tracking,'IWL')!=0 THEN "IWL"
#   WHEN INSTR(note_tracking,'ETR')!=0 THEN "ETR"
#   WHEN INSTR(note_tracking,'CSW')!=0 THEN "CSW"
#   WHEN INSTR(note_tracking,'STE')!=0 THEN "STE"
#   ELSE "NA"
#END;

SELECT  'Create tmp_last_quebra_tratada_date',now();
DROP TEMPORARY TABLE IF EXISTS operations_pe.tmp_last_quebra_tratada_date;
 
CREATE TEMPORARY TABLE operations_pe.tmp_last_quebra_tratada_date (INDEX (item_id))
SELECT
 a.itens_venda_id,
  a.numero_order,
  a.item_id,
  b.date,
  c.nome
FROM
  wmsprod_pe.itens_venda a
INNER JOIN ( SELECT
               itens_venda_id,
               max(DATA) date,
               usuarios_id
              FROM
               wmsprod_pe.status_itens_venda
              WHERE
               STATUS = 'quebra tratada'
              GROUP BY
               itens_venda_id) b
 ON a.itens_venda_id = b.itens_venda_id
INNER JOIN wmsprod_pe.usuarios c
 ON b.usuarios_id = c.usuarios_id
;



UPDATE operations_pe.out_order_tracking_sample AS d
INNER JOIN operations_pe.tmp_last_quebra_tratada_date t
 ON d.item_id = t.item_id
SET d.date_processed_stockout = t.date,
d.datetime_processed_stockout = t.date,
d.time_to_processed_stockout = If(datetime_declared_stockout is Null,null,If(datetime_processed_stockout is Null, TIMESTAMPDIFF(SECOND,datetime_declared_stockout, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_declared_stockout,datetime_processed_stockout)/3600)),
d.user_cs_oos = t.nome;

SELECT  'Update is_quebra_tratada',now();
UPDATE operations_pe.out_order_tracking_sample AS d
SET d.is_quebra_tratada = 1
WHERE d.date_declared_stockout IS NOT NULL
AND d.date_processed_stockout IS NOT NULL
AND d.date_declared_stockout < d.date_processed_stockout;

SELECT  'Update dias_habiles_negativos',now();
UPDATE operations_pe.out_order_tracking_sample
SET sac_backlog = dias_habiles_negativos(curdate(), date_declared_stockout)
WHERE date_processed_stockout IS NULL
AND date_declared_stockout IS NOT NULL;

SELECT  'Update min_delivery_date',now();
UPDATE operations_pe.out_order_tracking_sample AS a
INNER JOIN calcworkdays AS b
ON date(a.date_exported) = b.date_last 
 AND b.workdays =a.min_delivery_time
 AND b.isweekday = 1 
    AND b.isholiday = 0
SET 
 a.min_delivery_date = b.date_first ;
 
 UPDATE operations_pe.out_order_tracking_sample AS a
SET 
a.date_processed_backorder = if(a.date_backorder_accepted is null, a.date_backorder_tratada, a.date_backorder_accepted),
a.datetime_processed_backorder = if(a.datetime_backorder_accepted is null, a.datetime_backorder_tratada, a.datetime_backorder_accepted),
a.time_to_processed_backorder = If(datetime_backorder is Null,null,If(datetime_processed_backorder is Null, TIMESTAMPDIFF(SECOND,datetime_backorder, CURDATE())/3600, TIMESTAMPDIFF( SECOND ,datetime_backorder,datetime_processed_backorder)/3600));



SELECT  'Update customer_backlog',now();
UPDATE operations_pe.out_order_tracking_sample
SET customer_backlog = dias_habiles_negativos(curdate(),date_delivered_promised)
WHERE date_processed_stockout IS NULL
AND date_declared_stockout IS NOT NULL;

SELECT  'Update procured_promised_last_30',now();
UPDATE operations_pe.out_order_tracking_sample AS a
SET a.procured_promised_last_30 = CASE
WHEN datediff(curdate(),date_procured_promised) <= 30
AND datediff(curdate(),date_procured_promised) > 0 
THEN 1
ELSE 0
END,
a.shipped_last_30 = CASE
WHEN datediff(curdate(), date_shipped) <= 30
AND datediff(curdate(), date_shipped) > 0 
THEN 1
ELSE 0
END,
a.delivered_promised_last_30 = CASE
WHEN datediff(curdate(),date_delivered_promised) <= 30
AND datediff(curdate(),date_delivered_promised) > 0 
THEN 1
ELSE 0
END;

SELECT  'Update quebra_sugerido',now();
UPDATE operations_pe.out_order_tracking_sample AS a
SET
a.quebra_sugerido = IF(a.note_tracking regexp 'LACOL|SUGERIR', 'SI', 'NO')
WHERE a.date_declared_stockout IS NOT NULL;

SELECT  'Update quebra_sugerido',now();
UPDATE operations_pe.out_order_tracking_sample AS a
SET
a.quebra_sugerido = IF(a.note_tracking NOT regexp 'LACOL' = 1, 'NO', 'SI')
WHERE a.date_declared_stockout IS NOT NULL;




SELECT  'Update A_Master',now();
UPDATE operations_pe.out_order_tracking_sample AS a
INNER JOIN development_pe.A_Master AS b
ON a.item_id=b.ItemID
SET
a.tax_percent = b.Tax,
a.cost = b.Cost,
a.cost_w_o_vat = b.CostAfterTax,
a.price = b.Price,
a.price_w_o_vat = b.PriceAfterTax,
a.is_marketplace = b.isMPlace,
a.corporate_sale = if(b.CouponCode LIKE 'VC%',1,0),
a.coupon_value = if(b.CouponCode like 'REP%',b.CouponValue,0), 
a.customer_first_name = b.FirstName,
a.customer_last_name = b.LastName,
a.payment_method = b.PaymentMethod,
a.supplier_type = b.SupplierType;

UPDATE operations_pe.out_order_tracking_sample as a 
INNER JOIN wmsprod_pe.inverselogistics_devolucion as b
ON a.item_id = b.item_id 
SET a.is_returned = 1;

UPDATE operations_pe.out_order_tracking_sample
SET out_order_tracking_sample.date_ready_to_pick_bono = date_ready_to_pick
WHERE
datetime_ready_to_pick <= TIMESTAMP(date_ready_to_pick)+ INTERVAL 16 HOUR 
AND package_measure_new in ('small', 'medium','large');

UPDATE operations_pe.out_order_tracking_sample
SET out_order_tracking_sample.date_ready_to_pick_bono = date_ready_to_pick + INTERVAL 1 DAY 
WHERE
datetime_ready_to_pick <= TIMESTAMP(date_ready_to_pick)+ INTERVAL 16 HOUR 
AND package_measure_new = 'oversized';


UPDATE operations_pe.out_order_tracking_sample
SET out_order_tracking_sample.date_ready_to_pick_bono = date_ready_to_pick + INTERVAL 1 DAY 
WHERE datetime_ready_to_pick > TIMESTAMP(date_ready_to_pick)+ INTERVAL 16 HOUR
AND package_measure_new in ('small', 'medium','large');

UPDATE operations_pe.out_order_tracking_sample
SET out_order_tracking_sample.date_ready_to_pick_bono = date_ready_to_pick + INTERVAL 2 DAY 
WHERE datetime_ready_to_pick > TIMESTAMP(date_ready_to_pick)+ INTERVAL 16 HOUR
AND package_measure_new = 'oversized';

UPDATE operations_pe.out_order_tracking_sample AS a
INNER JOIN operations_pe.calcworkdays AS c
ON  date(a.date_ready_to_pick_bono ) = c.date_first 
              AND c.workdays = 1
              AND c.isholiday_first = 1
SET a.date_ready_to_pick_bono = c.date_last;

UPDATE operations_pe.out_order_tracking_sample AS a
INNER JOIN operations_pe.calcworkdays AS c
ON  date(a.date_ready_to_pick_bono ) = c.date_first 
              AND c.workdays = 1
              AND c.isweekend_first = 1
SET a.date_ready_to_pick_bono = c.date_last;

UPDATE operations_pe.out_order_tracking_sample
SET out_order_tracking_sample.is_bono = 1
Where date_shipped <= date_ready_to_pick_bono ;

UPDATE operations_pe.out_order_tracking_sample
SET out_order_tracking_sample.procured_3days = 1 
WHERE out_order_tracking_sample.workdays_to_procure <= 3;
	

UPDATE operations_pe.out_order_tracking_sample
SET out_order_tracking_sample.procured_5days = 1 
WHERE out_order_tracking_sample.workdays_to_procure <= 5;
	

UPDATE operations_pe.out_order_tracking_sample
SET out_order_tracking_sample.procured_10days = 1 
WHERE out_order_tracking_sample.workdays_to_procure <= 10;



UPDATE operations_pe.out_order_tracking_sample
SET out_order_tracking_sample.procured_more10days = 1 
WHERE out_order_tracking_sample.workdays_to_procure > 10;

# num_items_per_order
SELECT  'Truncate pro_order_tracking_num_items_per_order',now();
TRUNCATE  operations_pe.pro_order_tracking_num_items_per_order;

SELECT  'Insert pro_order_tracking_num_items_per_order',now();
INSERT INTO operations_pe.pro_order_tracking_num_items_per_order (
order_number,
count_of_order_item_id
) SELECT
out_order_tracking_sample.order_number,
count(*) AS countoforder_item_id
FROM
operations_pe.out_order_tracking_sample
GROUP BY
out_order_tracking_sample.order_number;

SELECT  'Update num_items_per_order',now();
UPDATE operations_pe.out_order_tracking_sample
INNER JOIN operations_pe.pro_order_tracking_num_items_per_order
                ON out_order_tracking_sample.order_number = pro_order_tracking_num_items_per_order.order_number
SET out_order_tracking_sample.num_items_per_order = 1 / pro_order_tracking_num_items_per_order.count_of_order_item_id;

UPDATE operations_pe.out_order_tracking_sample
INNER JOIN customer_service.NPSDataPE
ON out_order_tracking_sample.item_id = NPSDataPE.ItemID
SET
out_order_tracking_sample.loyalty = NPSDataPE.Loyalty,
has_nps_score = '1' ;

SELECT  'Update item_counter',now();
UPDATE operations_pe.out_order_tracking_sample
SET item_counter = 1;

UPDATE operations_pe.out_order_tracking_sample
SET out_order_tracking_sample.warehouse_delay_counter = if(date_ready_to_ship_promised > curdate(),1,0);

##### Termina Rutina #####