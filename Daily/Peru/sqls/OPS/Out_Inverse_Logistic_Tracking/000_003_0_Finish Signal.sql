INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
	updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Peru', 
  'out_inverse_logistics_tracking',
  'finish',
  NOW(),
  max(date_ordered),
  count(*),
  count(item_counter)
FROM
  operations_pe.out_inverse_logistics_tracking;