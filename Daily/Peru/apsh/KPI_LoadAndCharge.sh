v_user=$1
v_pwd=$2
v_host=$3
v_country=$4
v_date=`date +"%Y%m%d"`

for v_folder in KPI
do
    echo "Loading $v_folder ..."
    jars="./jars/${v_folder}"
    for i in $jars/*.jar
    do
		dateTime=`date`
		echo "($dateTime):: ${i}--Start Load."
		java -jar ${i}
		dateTime=`date`
		echo "($dateTime):: ${i}--End Load."
    done
    
done
