v_user=$1
v_pwd=$2
v_host=$3
v_activity=Channel_Report
v_date=`date +"%Y%m%d"`
v_country=Peru
echo    "$v_country Daily Info..."

v_country=Peru
echo    "$v_country Daily Info..."
v_event=A_Master
v_step=finish
v_reply=8000
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
. $lib/ToolKit/monitoring.sh

v_event=SEM.transaction_id_pe
v_column=date
v_step=finish
v_reply=8000
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
. $lib/ToolKit/monitoring_appends.sh

v_event=SEM.transaction_id_fashion_pe
v_column=date
v_step=finish
v_reply=8000
v_dependecy=1 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
. $lib/ToolKit/monitoring_appends.sh

for v_folder in $v_activity
do
    echo "Loading $v_folder ..."

    sqls="./sqls/MKT/${v_activity}"
    for i in $sqls/*.sql
    do
       dateTime=`date`
	   echo $dateTime $i 	   
       mysql -u $v_user -p${v_pwd}   -h ${v_host} -b marketing_report < $i
       error=`echo $?`
       if [ $error -gt 0 ]; then
           exit 1
       fi		
	   
    done
    dateTime=`date`
    echo " ${dateTime} End Load ..."
done