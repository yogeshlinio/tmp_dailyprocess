v_user=$1
v_pwd=$2
v_host=$3
v_country=$4
v_date=`date +"%Y%m%d"`
v_country=Peru


v_event=wmsprod_pe.itens_venda
v_reply=120
v_dependecy=0 #If 0: If the log it is not ready It continue; If 1 --> If the log is not ready It don't continue
#. /home/cmondragon/production/ToolKit/monitoring.sh
. $lib/ToolKit/monitoring.sh


for v_folder in OPS_Part2
do
    echo "Loading $v_folder ..."
    sqls="./sqls/${v_folder}"
    for i in $sqls/*.sql
    do
		dateTime=`date`
		echo "Start--($dateTime):: $i" 
		mysql -u ${v_user} -p${v_pwd}   -h ${v_host} -b production < $i
		error=`echo $?`
		if [ $error -gt 0 ]; then
			exit 1
		fi
		dateTime=`date`
		echo "($dateTime)::End Load ..."
	done
done
