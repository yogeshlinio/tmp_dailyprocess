#!/bin/bash
v_databases=$1
#Module to load data from BOB/UMS/OMS into dwh
#usage:
#     load.sh <country> <data_source>

echo  "Starting processing synchronize"

#absolute path to the linio-git repository
path=/var/lib/jenkins/git_repository

#parse config.ini
. $path/scripts/parser_config.sh
read_ini $path/config.ini synchronize

#get the var names according the parameter
server_dwh="INI__synchronize__DWH_ADDRESS"
username_dwh="INI__synchronize__DWH_USERNAME"
password_dwh="INI__synchronize__DWH_PASSWORD"

server="INI__synchronize__DB_ADDRESS"
username="INI__synchronize__DB_USERNAME"
password="INI__synchronize__DB_PASSWORD"

#shell double referencing
server_dwh=`eval echo \\$\$server_dwh`
username_dwh=`eval echo \\$\$username_dwh`
password_dwh=`eval echo \\$\$password_dwh`

server=`eval echo \\$\$server`
username=`eval echo \\$\$username`
password=`eval echo \\$\$password`

DATA_SOURCE=('synchronize');
FOUND=`echo ${DATA_SOURCE[*]} | grep "$2"`

#source $path/load/synchronize/DATABASES.inc

for v_database in  $v_databases
do
   source $path/load/synchronize/${v_database}.inc
   if [ "${FOUND}" != "" ]; then
    echo "Synchronizing of - $v_database "   
    echo "Importing synchronize $2 ..."
	
    # dumping the tables, parsing and importing on the dwh side
    #NOTE: Due BOB, OMS and UMS haves equal constraint names we will have problems transporting the full tables to the dwh due
    #Uniqueness constraint names in a database. For this we need to parse the sql dump, removing all constraint declarations
	
	#mysqldump --single-transaction --add-drop-table -R --host=${server_dwh}  --user=${username_dwh} -p${password_dwh}  ${v_database} ${tables} | php $path/code/parser.php | mysql --host=${server} -u ${username_dwh} -p${password} ${v_database}
	mysqldump --single-transaction --add-drop-table -R --host=${server_dwh}  --user=${username_dwh} -p${password_dwh}  ${v_database} ${tables} | mysql --host=${server} -u ${username_dwh} -p${password} ${v_database}
    ERR=$(( ${PIPESTATUS[0]} + ${PIPESTATUS[1]} ))	
	
    #ERR=$(( ${PIPESTATUS[0]} + ${PIPESTATUS[1]} + ${PIPESTATUS[2]}))

	echo "El valor es: $ERR"
    if [ $ERR -gt 0 ]; then
        exit 1
    fi
   else
      echo "$2 is not a valid data source"
      exit
   fi
done 