#!/bin/bash

#absolute path to the linio-git repository
path=/var/lib/jenkins/git_repository

#read and parse the config.ini file
#to call this shell script is:
. $path/scripts/parser_config.sh
read_ini $path/config.ini $1

#get the var names according the parameter
server="INI__${1}__DWH_DATABASE_SERVER"
username="INI__${1}__DWH_USERNAME"
password="INI__${1}__DWH_PASSWORD"
database="INI__${1}__DWH_DATABASE"

echo  "Starting processing logic for ${1}"

#shell double referencing
server=`eval echo \\$\$server`
username=`eval echo \\$\$username`
password=`eval echo \\$\$password`
database=`eval echo \\$\$database`

#for all *.sql files in sql/<country>
#please rename files in order to maintain running order
ERROR=0
for sql in $(ls ${path}/sql/${2}/${3}/${4}/*.sql)
do
    #execute sql script
    echo "Running ${sql}"
    time mysql --host=${server} -u${username} -p${password} $database < ${sql}

    #collect any possible errors during the script execution
    if [ $? -gt 0 ]
    then
        (( ERROR ++ ))
    fi
done

#if an error occurred in one of the scripts then jenkins job will be
#classified as failed
if [ $ERROR -gt 0 ]; then
    exit 1
fi
