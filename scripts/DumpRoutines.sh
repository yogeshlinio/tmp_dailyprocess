#!/bin/bash
v_databases=$1
#Module to load data from BOB/UMS/OMS into dwh
#usage:
#     load.sh <country> <data_source>

echo  "Starting processing DumpRoutines"

#absolute path to the linio-git repository
path=/var/lib/jenkins/git_repository
folder=/var/lib/jenkins/git_repository/Functions

#parse config.ini
. $path/scripts/parser_config.sh
read_ini $path/config.ini DumpRoutines

#get the var names according the parameter
server_dwh="INI__DumpRoutines__DWH_ADDRESS"
username_dwh="INI__DumpRoutines__DWH_USERNAME"
password_dwh="INI__DumpRoutines__DWH_PASSWORD"

server="INI__DumpRoutines__DB_ADDRESS"
username="INI__DumpRoutines__DB_USERNAME"
password="INI__DumpRoutines__DB_PASSWORD"

#shell double referencing
server_dwh=`eval echo \\$\$server_dwh`
username_dwh=`eval echo \\$\$username_dwh`
password_dwh=`eval echo \\$\$password_dwh`

server=`eval echo \\$\$server`
username=`eval echo \\$\$username`
password=`eval echo \\$\$password`

DATA_SOURCE=('DumpRoutines');
FOUND=`echo ${DATA_SOURCE[*]} | grep "$2"`

source $path/load/synchronize/DATABASES.inc

	git stash
	git clean -f #Borra todos los archivos que no estan en repo (untracked files)
	git checkout master
	
	if [ $? -ne 0 ]; then
		exit 1
	fi
	
for v_database in  $v_databases
do
   
    echo "Dumps Routines of - $v_database "
    echo "Importing DumpRoutines $2 ..."
	
    # dumping the routines
	mysqldump --routines --no-create-info --no-data --no-create-db --skip-opt --force --host=${server_dwh}  --user=${username_dwh} -p${password_dwh}  ${v_database} > $folder/$v_database.sql
	
	dateTime=`date`
	echo " ${dateTime} End MysqlDump ..."
	
done
	
	git add $folder/*
	git commit -m "Dumps Routines MySQL"
	git stash
	git pull origin master
	git push origin master

	dateTime=`date`
	echo " ${dateTime} End Load ..."