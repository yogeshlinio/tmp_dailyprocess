#!/bin/bash

#Module to load data from BOB/UMS/OMS into dwh
#usage:
#     load.sh <country> <data_source>

echo  "Starting processing ${1}"

#absolute path to the linio-git repository
path=/var/lib/jenkins/git_repository

#parse config.ini
. $path/scripts/parser_config.sh
read_ini $path/config.ini $1

#get the var names according the parameter
server_dwh="INI__${1}__DWH_DATABASE_SERVER"
username_dwh="INI__${1}__DWH_USERNAME"
password_dwh="INI__${1}__DWH_PASSWORD"
database_dwh="INI__${1}__DWH_DATABASE"

server="INI__${1}__${2}_DB_ADDRESS"
username="INI__${1}__${2}_DB_USERNAME"
password="INI__${1}__${2}_DB_PASSWORD"
database="INI__${1}__${2}_DB_DATABASE"
database_usage="INI__${1}__${2}_DB_DATABASE_USAGE"

#shell double referencing
server_dwh=`eval echo \\$\$server_dwh`
username_dwh=`eval echo \\$\$username_dwh`
password_dwh=`eval echo \\$\$password_dwh`
database_dwh=`eval echo \\$\$database_dwh`
server=`eval echo \\$\$server`
username=`eval echo \\$\$username`
password=`eval echo \\$\$password`
database=`eval echo \\$\$database`
database_usage=`eval echo \\$\$database_usage`
[ ! -z "$database_usage" ] && database=`echo \$database_usage`

DATA_SOURCE=('BOB' 'UMS' 'OMS' 'WMS' 'FACEBOOK' 'PRODUCTION' 'SEM' 'GLOBAL' 'MARKETING' 'SINCRONIZATION' 'CS');
FOUND=`echo ${DATA_SOURCE[*]} | grep "$2"`
source $path/load/${1}/$2.inc

if [ "${FOUND}" != "" ]; then
    echo "Importing $1 $2..."
    # dumping the tables, parsing and importing on the dwh side
    #NOTE: Due BOB, OMS and UMS haves equal constraint names we will have problems transporting the full tables to the dwh due
    #Uniqueness constraint names in a database. For this we need to parse the sql dump, removing all constraint declarations
    mysqldump --single-transaction --add-drop-table --host=${server}  -P3306 --user=${username} -p${password}  ${database} ${tables} | php $path/code/parser.php | mysql --host=${server_dwh} -u ${username_dwh} -p${password_dwh} ${database_dwh}
    ERR=$(( ${PIPESTATUS[0]} + ${PIPESTATUS[1]} + ${PIPESTATUS[2]}))
    if [ $ERR -gt 0 ]; then
        exit 1
    fi
else
  echo $2 is not a valid data source
  exit 1
fi
