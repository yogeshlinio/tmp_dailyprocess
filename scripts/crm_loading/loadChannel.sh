#!/bin/bash

#Module to load data from BOB/UMS/OMS into dwh
#usage:
#     load.sh <country> <data_source>

echo  "Starting processing ${1}"
COUNTRY=${2}


#absolute path to the linio-git repository
path="/var/lib/jenkins/git_repository/"

#parse config.ini
. $path/scripts/parser_config.sh
read_ini $path/configChannels.ini $1

#get the var names according the parameter
channel_dir="INI__${1}__DIRECTORY"
channel_file="INI__${1}__LOADFILE"

DB_HOST="INI__${1}__DB_HOST"
DB_DATABASE="INI__${1}__DB_DATABASE"


DB_TABLE="INI__${1}__DB_TABLE"
DB_USERNAME="INI__${1}__DB_USERNAME"
DB_PASSWORD="INI__${1}__DB_PASSWORD"

#shell double referencing
channel_dir=`eval echo \\$\$channel_dir `
channel_file=`eval echo \\$\$channel_file`
DB_TABLE=`eval echo \\$\$DB_TABLE`
DB_HOST=`eval echo \\$\$DB_HOST`
DB_DB=`eval echo \\$\$DB_DATABASE`

DB_USERNAME=`eval echo \\$\$DB_USERNAME`
DB_PASSWORD=`eval echo \\$\$DB_PASSWORD`
channel_dir=`eval echo \$channel_dir `
channel_file=`eval echo \$channel_file`
DB_TABLE=`eval echo \$DB_TABLE`
DB_HOST=`eval echo \$DB_HOST`
DB_DB=`eval echo \$DB_DB`
DB_USERNAME=`eval echo \$DB_USERNAME`
DB_PASSWORD=`eval echo \$DB_PASSWORD`

echo $channel_dir
echo $channel_file
echo $DB_TABLE
echo $DB_HOST
echo $DB_DB
echo $DB_USERNAME
echo $DB_PASSWORD

rm -f $path/sql/crm/tmp*
cat $path/sql/crm/loadData.sql | sed s%@v_dir@%"${channel_dir}"%g>$path/sql/crm/tmp
cat $path/sql/crm/tmp  | sed s/@v_file@/${channel_file}/g>$path/sql/crm/tmp1
cat $path/sql/crm/tmp1 | sed s/@v_host@/${DB_HOST}/g>$path/sql/crm/tmp2
cat $path/sql/crm/tmp2 | sed s/@v_db@/${DB_DB}/g>$path/sql/crm/tmp3
cat $path/sql/crm/tmp3 | sed s/@v_tables@/${DB_TABLE}/g>$path/sql/crm/tmp4

mysql -u$DB_USERNAME  -p$DB_PASSWORD -h $DB_HOST -b $DB_DB <$path/sql/crm/tmp4
rm $path/sql/crm/tmp* 
exit
