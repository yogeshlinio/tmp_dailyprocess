country=$1
v_date=`date +"%Y%m%d"`

system="/var/lib/jenkins/git_repository/"
data="/var/tmp/CRM/${country}"
cd $data
for i in *.gz
do
   echo $i
   gunzip $i
   v_type=`echo $i | awk -F "."  '{print $2}'  `
   v_config=`echo $i | awk -F "."  '{print $3}'  `
   v_file=`echo $i | awk -F "."  '{print $1"."$2"."$3"."$4}'`
   v_inputFile=`echo $i | awk -F "."  '{print $2"."$3"."$4}'`
   [ $v_type = "interface"  ] && v_prefix="i"
   [ $v_type = "campaign"   ] && v_prefix="c"
 
   echo $v_inputFile
   mv $v_file $v_inputFile 
   
   $system/scripts/crm_loading/loadChannel.sh ${v_prefix}${v_config} $country
   mv $v_inputFile $v_file
   gzip $v_file
done
cd ..
pwd
tar -cvzf Historical_${country}_${v_date}.tar.gz ./${country}
mv Historical_${country}_${v_date}.tar.gz ./Historical
#rm -f $data/*
cd $system