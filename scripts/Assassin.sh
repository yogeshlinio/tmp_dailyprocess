#!/bin/bash

#Module to load data from BOB/UMS/OMS into dwh
#usage:
#     load.sh <country> <data_source>

echo  "Starting processing Assassin"

#absolute path to the linio-git repository
path=/var/lib/jenkins/git_repository

#parse config.ini
. $path/scripts/parser_config.sh
read_ini $path/config.ini Assassin

#get the var names according the parameter
server_dwh="INI__Assassin__DWH_ADDRESS"
username_dwh="INI__Assassin__DWH_USERNAME"
password_dwh="INI__Assassin__DWH_PASSWORD"

#shell double referencing
server_dwh=`eval echo \\$\$server_dwh`
username_dwh=`eval echo \\$\$username_dwh`
password_dwh=`eval echo \\$\$password_dwh`


DATA_SOURCE=('Assassin');
FOUND=`echo ${DATA_SOURCE[*]} | grep "$2"`

#source $path/load/synchronize/BI_Users.inc

v_users="AND USER NOT IN ('adrien.c', 'alejandros', 'raul.delgado', 'carlos.mondragon', 'charles', 'daniel.h', 'daniel.palacios', 'eduardo.martinez', 'javier.guana', 'marketing', 'julian.buitrago', 'laura.forero', 'lorena.ramirez', 'luis.ochoa', 'auto.reports', 'mario.monroy', 'natali.serrano', 'juan.zinser', 'paula.mendoza', 'saul.martinez', 'sthip.blas', 'vikram.deswal', 'system user', 'root', 'bi_co', 'ops_co', 'cs_co', 'commercial_co', 'commercial_ve', 'commercial_pe', 'commercial_mx', 'read_only', 'ops_mx','ops_co','ops_ve','ops_pe','ODS', 'mkt_co', 'mkt_mx', 'mkt_pe', 'mkt_ve')"
	
    
	var=$(mysql --skip-column-names --host=${server_dwh}  --user=${username_dwh} -p${password_dwh} -e "SELECT id FROM information_schema.PROCESSLIST WHERE time > 700 $v_users")
	send=$(mysql --skip-column-names --host=${server_dwh}  --user=${username_dwh} -p${password_dwh} -e "SELECT CONCAT_WS(' ','DB: ',DB,'\nUser: ',USER,'\nTime: ', TIME, '\nIP: ', HOST,'\nQuery:', INFO) FROM information_schema.PROCESSLIST WHERE time > 700 $v_users")
	
	for i in $var
	do echo "kill $i" | mysql  --host=${server_dwh}  --user=${username_dwh} -p${password_dwh} 
	done
	
	echo "Slow $send"
	if [ -n "$send" ]; then 
		echo -e $send | mailx -s "Slow Query" julian.buitrago@linio.com vikram.deswal@linio.com
	fi
	#echo $send
	#exit=`mysql $kill` 
	#echo $exit 
	
	error=`echo $?`
	if [ $error -gt 0 ]; then
		exit 1
	fi
   
	dateTime=`date`
	echo " ${dateTime} End ..."	
