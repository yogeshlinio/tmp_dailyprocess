#!/bin/bash
v_enviroment=$1
destiny=$2
#Module to load data from BOB/UMS/OMS into dwh
#usage:
#     load.sh <country> <data_source>

echo  "Starting processing synchronize"

#absolute path to the linio-git repository
path=/var/lib/jenkins/git_repository

#parse config.ini
. $path/scripts/parser_config.sh
read_ini $path/config.ini $v_enviroment

#get the var names according the parameter
server_dwh="INI__${v_enviroment}__DWH_ADDRESS"
username_dwh="INI__${v_enviroment}__DWH_USERNAME"
password_dwh="INI__${v_enviroment}__DWH_PASSWORD"

server="INI__${v_enviroment}__DB_ADDRESS"
username="INI__${v_enviroment}__DB_USERNAME"
password="INI__${v_enviroment}__DB_PASSWORD"


#shell double referencing
server_dwh=`eval echo \\$\$server_dwh`
username_dwh=`eval echo \\$\$username_dwh`
password_dwh=`eval echo \\$\$password_dwh`

server=`eval echo \\$\$server`
username=`eval echo \\$\$username`
password=`eval echo \\$\$password`


DATA_SOURCE=('synchronize' 'customer_service_co');
FOUND=`echo ${DATA_SOURCE[*]} | grep "$2"`

#source $path/load/synchronize/DATABASES.inc
for i in `ls $path/load/${v_enviroment}/*.inc`
do
    echo "Dump over -- $i "
    v_database=`echo $i | awk -F "/" '{print $8}' | awk -F "." '{print $1}'`
   source $path/load/${v_enviroment}/${v_database}.inc

   #if [ "${FOUND}" != "" ]; then
    echo "Synchronizing of - $v_database "   
    echo "Importing synchronize $2 ..."
	
    # dumping the tables, parsing and importing on the dwh side
    #NOTE: Due BOB, OMS and UMS haves equal constraint names we will have problems transporting the full tables to the dwh due
    #Uniqueness constraint names in a database. For this we need to parse the sql dump, removing all constraint declarations
echo	"mysqldump --single-transaction --add-drop-table -R --host=${server_dwh}  --user=${username_dwh} -p${password_dwh}  ${v_database} ${tables} | mysql --host=${server} -u ${username} -p${password} ${destiny}"

	
	#mysqldump --single-transaction --add-drop-table -R --host=${server_dwh}  --user=${username_dwh} -p${password_dwh}  ${v_database} ${tables} | php $path/code/parser.php | mysql --host=${server} -u ${username_dwh} -p${password} ${v_database}
	mysqldump --single-transaction --add-drop-table -R --host=${server_dwh}  --user=${username_dwh} -p${password_dwh}  ${v_database} ${tables} | mysql --host=${server} -u ${username} -p${password} ${destiny}

    #ERR=$(( ${PIPESTATUS[0]} + ${PIPESTATUS[1]} ))	
	ERR=0

	echo "El valor es: $ERR"
    if [ $ERR -gt 0 ]; then
        exit 1
    fi
  #else
  #    echo "$2 is not a valid data source"
  #    exit
  #fi
done 
cat $path/load/${v_enviroment}/log.sql | mysql --host=${server} -u ${username} -p${password} 