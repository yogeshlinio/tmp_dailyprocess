. $lib/parser_config.sh
read_ini $home/config.ini 
cd $global

v_prefix="INI__${v_environment}__PREFIX"
v_prefix=`eval echo \\$\$v_prefix`

echo "Loading $v_environment ..."
sqls="./sqls/${v_prefix}/${v_environment}"

dir=$(ls -d ${sqls}_Testing/*)
for k in $dir
do
echo $k
    var=$(ls -d $k/*)
    for i in $var
    do
       dateTime=`date`
	   v_external=1
	   echo $i | grep "005_External_References"
	   v_external=$?
       if [ -d $i ]
       then
          echo "Executing $i subroutine"
	      for j in $i/*.sql
		  do
	        echo -e "\t $dateTime $j"
	   	    v_file=$j
            . $lib/ToolKit/polymorphism.sh	 	
            . $lib/ToolKit/customize.sh	 
            if [ $v_external = 0 ]
            then
               . $lib/ToolKit/executeQuery.sh 	 			
            else	
               . $lib/ToolKit/executeQuery.sh	 			
            fi			
		  done
       else
	      echo -e "\t $dateTime $i "
		  v_file=$i
          . $lib/ToolKit/polymorphism.sh	 	
          . $lib/ToolKit/customize.sh	 
            if [ $v_external = 0 ]
            then
               . $lib/ToolKit/executeQuery.sh 	 			
            else	
               . $lib/ToolKit/executeQuery.sh	 			
            fi			
       fi		
    done
done
dateTime=`date`
echo " ${dateTime} End Load ..."
