v_userMonitor="monitor"
v_pwdMonitor="monitor"

#v_date=$1
#v_country=$2
#v_column=$3
#v_event=$3
#v_reply=$4
#v_dependecy=$5

echo -e "Checking $v_event done in $v_country..."
query=`echo "SELECT COUNT(*) from ${v_event} where date_format( ${v_column} , '%Y%m%d'  ) >= '$v_date' - INTERVAL 1 DAY "`

a="0"
error=0
times=0
while [ $a -eq 0  ] 
do
  a=`echo $query |  mysql -u $v_userMonitor -p${v_pwdMonitor} -N   -h 172.17.12.191 -b production `

  if [ $a -ge 1 ]
  then
    echo "Event: $v_event - $v_country Ready..."
  else
    time=`date`
    echo "Event: $v_event - $v_country not ready at:" $time
    echo "Waiting 30s...."
  fi

  sleep 30
  times=$(( $times + 1 )) 
  if [ $times -eq $v_reply   ]
  then
    a=1
    echo "Event $v_event - $v_country incorrect..."
    error=1
  fi 
done
  
if [ $v_dependecy -eq 1 ] && [ $error = 1   ]
then
   echo "Fatal error We can not continue with the process..."
   exit
else
   echo "Continuing with the process..."
fi


