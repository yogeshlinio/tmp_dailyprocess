#v_date=$1
#v_country=$2
#v_event=$3
#v_reply=$4
#v_dependecy=$5
echo -e "Checking $v_event done in $v_country..."
a="1"
error=0
times=0
while [ $a -ne 0  ]
do
  ping -c1 -i 10 172.18.0.8
  a=`echo $?`
  if [ $a -eq 0 ]
  then
    echo "Event: $v_event - $v_country Ready..."
  else
    time=`date`
    echo "Event: $v_event - $v_country not ready at:" $time
    echo "Waiting 30s...."
  fi
  sleep 30
  times=$(( $times + 1 ))
  if [ $times -eq $v_reply   ]
  then
    a=0
    echo "Event $v_event - $v_country incorrect..."
    error=1
  fi
done

if [ $v_dependecy -eq 1 ] && [ $error = 1   ]
then
   echo "Fatal error We can not continue with the process..."
   exit
else
   echo "Continuing with the process..."
fi