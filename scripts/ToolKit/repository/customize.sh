v_timestamp_file=$( date +%N )
toolKit_Repository="${lib}/ToolKit/repository"
v_customFile=${toolKit_Repository}/${v_environment}_${v_timestamp_file}.sql
v_customFileAux=${toolKit_Repository}/${v_environment}_${v_timestamp_file}_aux.sql

cat $v_file > $v_customFile
v_aux=""
v_parameterValue=""

for v_parameter in parameters
do

   v_aux="INI__${v_environment}__${v_parameter}"
   v_parameterValue=`eval echo \\$\$v_aux`
   echo $v_parameter $v_parameterValue
   cat $v_customFileAux | sed s/"@${v_parameter}@"/"${$v_parameterValue}"/g > $v_customFile
   cp  -f $v_customFile $v_customFileAux
done
cat $v_customFile