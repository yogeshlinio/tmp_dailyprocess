v_timestamp_file=$( date +%N )
toolKit_Repository="${lib}/ToolKit/repository"
v_customFile=${toolKit_Repository}/${v_environment}_${v_timestamp_file}.sql
v_customFileAux=${toolKit_Repository}/${v_environment}_${v_timestamp_file}_aux.sql

#get the var names according the parameter
source ./resources/${v_environment}/parameters.inc

cat $v_file > $v_customFile
cp  -f $v_customFile $v_customFileAux
v_aux=""
v_parameterValue=""

for v_parameter in $parameters
do
   v_aux="INI__${v_environment}__${v_parameter}"
   v_parameterValue=`eval echo \\$\$v_aux`
   v_parameterValue=$( eval echo $v_parameterValue )

   
   cat $v_customFileAux | sed s/"@${v_parameter}@"/"$v_parameterValue"/g > $v_customFile
   cp  -f $v_customFile $v_customFileAux
done

[ $v_verbose -eq 1 ] && cat $v_customFile