-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: facebook
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'facebook'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 FUNCTION `week_iso`(edate date) RETURNS varchar(20) CHARSET latin1
begin
 declare sdate varchar(20);
 if year(edate) = 2012 then set sdate = concat(year(edate), '-',(week(edate, 5))+1); else set sdate = date_format(edate, "%x-%v"); end if;
      return sdate;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `facebook_campaign`()
begin

delete from facebook.facebook_campaign;

insert into facebook.facebook_campaign
select date, 
		campaign_name,
		impressions,
		clicks,
		spend,
		'' channel,
		null fk_channel,
		country
from marketing.facebook_campaign;


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `facebook_optimization`()
begin

select  'Facebook Optimization Table: start',now();

-- Mexico
-- Facebook Optimization Region

set @days:=30;

delete from facebook.facebook_campaign;

insert into facebook.facebook_campaign select * from facebook.facebook_campaign_mx union select * from facebook.facebook_campaign_co union select * from facebook.facebook_campaign_pe;  

update facebook.facebook_campaign set campaign = replace(campaign, '_', '.');

update facebook.ga_facebook_ads_region_mx set campaign = replace(campaign, '_', '.');

update facebook.ga_facebook_transaction_id_region_mx set campaign = replace(campaign, '_', '.');

delete from facebook.facebook_optimization_region_mx where datediff(curdate(), date)<@days;

insert into facebook.facebook_optimization_region_mx (date, campaign, country, region, city, obc_transactions, visits, gross_revenue, carts) select date, campaign, country, region, city, transactions, visits, transaction_revenue, goal2starts from facebook.ga_facebook_ads_region_mx where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=facebook.week_iso(date) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx f set f.oac_transactions = (select count(distinct z.transaction_id) from production.tbl_order_detail a, facebook.ga_facebook_transaction_id_region_mx z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and a.oac=1) where datediff(curdate(), date)<@days;

create table facebook.temporary as select r.date, r.campaign, r.country, r.region, r.city, (select t.custid from production.tbl_order_detail t where t.order_nr=r.transaction_id and oac=1 group by t.custid) as custid, r.transaction_id, (select sum(t.actual_paid_price) from production.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as actual_paid_price, (select sum(ifnull(t.actual_paid_price_after_tax,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost_per_sku,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) from production.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as PC2, (select sum(new_customers) from production.tbl_order_detail z where z.order_nr=r.transaction_id and z.oac=1 group by z.order_nr) as new_customers from facebook.ga_facebook_transaction_id_region_mx r where datediff(curdate(), date)<@days group by transaction_id;

-- Back Cohort

create table facebook.temporary_back_cohort(
date date,
campaign varchar(255),
country varchar(255),
region varchar(255),
city varchar(255),
custid int,
transaction_id varchar(255),
oac_transactions_30_cohort float,
oac_transactions_60_cohort float,
actual_paid_price_30_cohort float,
actual_paid_price_60_cohort float,
PC2_30_cohort float,
PC2_60_cohort float,
repeated int
);


create index custid on facebook.temporary_back_cohort(custid);

create index all_index on facebook.temporary_back_cohort(date, campaign, country, city);

insert into facebook.temporary_back_cohort (date, campaign, country, region, city, custid, transaction_id) select date, campaign, country, region, city, custid, transaction_id from facebook.temporary where new_customers is not null and datediff(curdate(), date)<@days;

create table facebook.backup_temporary_back_cohort as select * from facebook.temporary_back_cohort;

update facebook.temporary_back_cohort a set repeated = (select count(*) as total from facebook.backup_temporary_back_cohort b where a.date=b.date and a.campaign=b.campaign and a.country=b.country and a.region=b.region and a.city=b.city  group by date, campaign, country, region, city, custid having count(*)>1);

update facebook.temporary_back_cohort a set repeated = 1 where repeated is null;

drop table facebook.backup_temporary_back_cohort;

update facebook.temporary_back_cohort b set oac_transactions_30_cohort = (select count(distinct t.order_nr) as oac_transactions from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set actual_paid_price_30_cohort = (select sum(t.actual_paid_price) as actual_paid_price from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set PC2_30_cohort = (select sum(ifnull(t.actual_paid_price_after_tax,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost_per_sku,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set oac_transactions_60_cohort = (select count(distinct t.order_nr) as oac_transactions from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set actual_paid_price_60_cohort = (select sum(t.actual_paid_price) as actual_paid_price from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set PC2_60_cohort = (select sum(ifnull(t.actual_paid_price_after_tax,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost_per_sku,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort set oac_transactions_30_cohort=oac_transactions_30_cohort/repeated, actual_paid_price_30_cohort=actual_paid_price_30_cohort/repeated, PC2_30_cohort=PC2_30_cohort/repeated, oac_transactions_60_cohort=oac_transactions_60_cohort/repeated, actual_paid_price_60_cohort=actual_paid_price_60_cohort/repeated, PC2_60_cohort=PC2_60_cohort/repeated where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx f set oac_transactions_30_cohort = (select sum(oac_transactions_30_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx f set cum_net_rev_30_cohort = (select sum(actual_paid_price_30_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx f set PC2_30_cohort = (select sum(PC2_30_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx f set oac_transactions_60_cohort = (select sum(oac_transactions_60_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx f set cum_net_rev_60_cohort = (select sum(actual_paid_price_60_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx f set PC2_60_cohort = (select sum(PC2_60_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

drop table facebook.temporary_back_cohort;

update facebook.facebook_optimization_region_mx set oac_transactions_30_cohort = 0 where oac_transactions_30_cohort is null;

update facebook.facebook_optimization_region_mx set oac_transactions_60_cohort = 0 where oac_transactions_60_cohort is null;

update facebook.facebook_optimization_region_mx set cum_net_rev_30_cohort = 0 where cum_net_rev_30_cohort is null;

update facebook.facebook_optimization_region_mx set cum_net_rev_60_cohort = 0 where cum_net_rev_60_cohort is null;

-- Back Cohort

alter table facebook.temporary add column avg_discount float;

update facebook.temporary r set avg_discount = (select avg(1-(i.unit_price/i.original_unit_price)) from bob_live_mx.sales_order o inner join bob_live_mx.sales_order_item i on i.fk_sales_order = o.id_sales_order where r.transaction_id=o.order_nr group by o.order_nr) where datediff(curdate(), date)<@days; 

create index temporary on facebook.temporary(campaign, date, country, city);

update facebook.facebook_optimization_region_mx f set net_revenue= (select sum(actual_paid_price) from facebook.temporary z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx f set new_customers= (select sum(z.new_customers) from facebook.temporary z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx f set PC2_absolute= (select sum(PC2) from facebook.temporary z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx f set `avg_discount(%)`= (select (avg(avg_discount))*100 from facebook.temporary z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city) where datediff(curdate(), date)<@days;

drop table facebook.temporary;

update facebook.facebook_optimization_region_mx set oac_transactions = 0 where oac_transactions is null;

update facebook.facebook_optimization_region_mx set net_revenue = 0 where net_revenue is null;

update facebook.facebook_optimization_region_mx set new_customers = 0 where new_customers is null;

create table facebook.optimization as select date, campaign, sum(visits)as visits from facebook.facebook_optimization_region_mx group by date, campaign;

create index optimization on facebook.optimization(date, campaign);

create table facebook.cost_campaign as select date, campaign, sum(spent) as spent, sum(clicks) as clicks, sum(social_impressions) as impressions from facebook.facebook_campaign where datediff(curdate(), date)<@days group by date, campaign;

create index cost_campaign on facebook.cost_campaign(date, campaign);

create table facebook.final as select a.date, a.campaign, a.visits, b.spent, b.clicks, b.impressions from facebook.optimization a, facebook.cost_campaign b where a.date=b.date and a.campaign=b.campaign;

drop table facebook.optimization;

drop table facebook.cost_campaign;

create index final on facebook.final(date, campaign);

update facebook.facebook_optimization_region_mx f inner join facebook.final final on f.date=final.date and f.campaign=final.campaign set f.cost=(f.visits/(final.visits))*final.spent, f.clicks=(f.visits/(final.visits))*final.clicks, f.impressions=(f.visits/(final.visits))*final.impressions where datediff(curdate(), f.date)<@days;

drop table facebook.final;

update facebook.facebook_optimization_region_mx set cost = 0 where cost is null;

update facebook.facebook_optimization_region_mx set clicks = 0 where clicks is null;

update facebook.facebook_optimization_region_mx set impressions = 0 where impressions is null;

update facebook.facebook_optimization_region_mx set category = substr(substr(campaign, locate('.',campaign)+1), 1, locate('.', substr(campaign, locate('.',campaign)+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx set gender = substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1) ,1 , locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx set age = substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,1 ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx set segmentation = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) ,1 ,locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx set extra_field = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) , locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))+1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, PC2_absolute=PC2_absolute/xr, cum_net_rev_30_cohort=cum_net_rev_30_cohort/xr, cum_net_rev_60_cohort=cum_net_rev_60_cohort/xr, PC2_30_cohort=PC2_30_cohort/xr, PC2_60_cohort=PC2_60_cohort/xr where r.country='MEX' and datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx set oac_transactions=0, net_revenue=0, new_customers=0, oac_transactions_30_cohort=0, oac_transactions_60_cohort=0, cum_net_rev_30_cohort=0, cum_net_rev_60_cohort=0 where obc_transactions=0 and datediff(curdate(), date)<@days;

-- Colombia
-- Facebook Optimization Region

update facebook.facebook_campaign set campaign = replace(campaign, '_', '.');

update facebook.ga_facebook_ads_region_co set campaign = replace(campaign, '_', '.');

update facebook.ga_facebook_transaction_id_region_co set campaign = replace(campaign, '_', '.');

delete from facebook.facebook_optimization_region_co where datediff(curdate(), date)<@days;

insert into facebook.facebook_optimization_region_co (date, campaign, country, region, city, obc_transactions, visits, gross_revenue, carts) select date, campaign, country, region, city, transactions, visits, transaction_revenue, goal2starts from facebook.ga_facebook_ads_region_co where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=facebook.week_iso(date) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co f set f.oac_transactions = (select count(distinct z.transaction_id) from production_co.tbl_order_detail a, facebook.ga_facebook_transaction_id_region_co z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and a.oac=1) where datediff(curdate(), date)<@days;

create table facebook.temporary as select r.date, r.campaign, r.country, r.region, r.city, (select t.custid from production_co.tbl_order_detail t where t.order_nr=r.transaction_id and oac=1 group by t.custid) as custid, r.transaction_id, (select sum(t.paid_price) from production_co.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as paid_price, (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) from production_co.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as PC2, (select sum(new_customers) from production_co.tbl_order_detail z where z.order_nr=r.transaction_id and z.oac=1 group by z.order_nr) as new_customers from facebook.ga_facebook_transaction_id_region_co r where datediff(curdate(), date)<@days group by transaction_id;

-- Back Cohort

create table facebook.temporary_back_cohort(
date date,
campaign varchar(255),
country varchar(255),
region varchar(255),
city varchar(255),
custid int,
transaction_id varchar(255),
oac_transactions_30_cohort float,
oac_transactions_60_cohort float,
paid_price_30_cohort float,
paid_price_60_cohort float,
PC2_30_cohort float,
PC2_60_cohort float,
repeated int
);


create index custid on facebook.temporary_back_cohort(custid);

create index all_index on facebook.temporary_back_cohort(date, campaign, country, city);

insert into facebook.temporary_back_cohort (date, campaign, country, region, city, custid, transaction_id) select date, campaign, country, region, city, custid, transaction_id from facebook.temporary where new_customers !=0 and datediff(curdate(), date)<@days;

create table facebook.backup_temporary_back_cohort as select * from facebook.temporary_back_cohort;

update facebook.temporary_back_cohort a set repeated = (select count(*) as total from facebook.backup_temporary_back_cohort b where a.date=b.date and a.campaign=b.campaign and a.country=b.country and a.region=b.region and a.city=b.city  group by date, campaign, country, region, city, custid having count(*)>1);

update facebook.temporary_back_cohort a set repeated = 1 where repeated is null;

drop table facebook.backup_temporary_back_cohort;

update facebook.temporary_back_cohort b set oac_transactions_30_cohort = (select count(distinct t.order_nr) as oac_transactions from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers = 0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set paid_price_30_cohort = (select sum(t.paid_price) as paid_price from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers = 0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set PC2_30_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers = 0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set oac_transactions_60_cohort = (select count(distinct t.order_nr) as oac_transactions from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers = 0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set paid_price_60_cohort = (select sum(t.paid_price) as paid_price from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers = 0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set PC2_60_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers = 0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort set oac_transactions_30_cohort=oac_transactions_30_cohort/repeated, paid_price_30_cohort=paid_price_30_cohort/repeated, PC2_30_cohort=PC2_30_cohort/repeated, oac_transactions_60_cohort=oac_transactions_60_cohort/repeated, paid_price_60_cohort=paid_price_60_cohort/repeated, PC2_60_cohort=PC2_60_cohort/repeated where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co f set oac_transactions_30_cohort = (select sum(oac_transactions_30_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co f set cum_net_rev_30_cohort = (select sum(paid_price_30_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co f set PC2_30_cohort = (select sum(PC2_30_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co f set oac_transactions_60_cohort = (select sum(oac_transactions_60_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co f set cum_net_rev_60_cohort = (select sum(paid_price_60_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co f set PC2_60_cohort = (select sum(PC2_60_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

drop table facebook.temporary_back_cohort;

update facebook.facebook_optimization_region_co set oac_transactions_30_cohort = 0 where oac_transactions_30_cohort is null;

update facebook.facebook_optimization_region_co set oac_transactions_60_cohort = 0 where oac_transactions_60_cohort is null;

update facebook.facebook_optimization_region_co set cum_net_rev_30_cohort = 0 where cum_net_rev_30_cohort is null;

update facebook.facebook_optimization_region_co set cum_net_rev_60_cohort = 0 where cum_net_rev_60_cohort is null;

-- Back Cohort

alter table facebook.temporary add column avg_discount float;

update facebook.temporary r set avg_discount = (select avg(1-(i.unit_price/i.original_unit_price)) from bob_live_co.sales_order o inner join bob_live_co.sales_order_item i on i.fk_sales_order = o.id_sales_order where r.transaction_id=o.order_nr group by o.order_nr) where datediff(curdate(), date)<@days; 

create index temporary on facebook.temporary(campaign, date, country, city);

update facebook.facebook_optimization_region_co f set net_revenue= (select sum(paid_price) from facebook.temporary z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co f set new_customers= (select sum(z.new_customers) from facebook.temporary z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co f set PC2_absolute= (select sum(PC2) from facebook.temporary z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co f set `avg_discount(%)`= (select (avg(avg_discount))*100 from facebook.temporary z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city) where datediff(curdate(), date)<@days;

drop table facebook.temporary;

update facebook.facebook_optimization_region_co set oac_transactions = 0 where oac_transactions is null;

update facebook.facebook_optimization_region_co set net_revenue = 0 where net_revenue is null;

update facebook.facebook_optimization_region_co set new_customers = 0 where new_customers is null;

create table facebook.optimization as select date, campaign, sum(visits)as visits from facebook.facebook_optimization_region_co where datediff(curdate(), date)<@days group by date, campaign;

create index optimization on facebook.optimization(date, campaign);

create table facebook.cost_campaign as select date, campaign, sum(spent) as spent, sum(clicks) as clicks, sum(social_impressions) as impressions from facebook.facebook_campaign where datediff(curdate(), date)<@days group by date, campaign;

create index cost_campaign on facebook.cost_campaign(date, campaign);

create table facebook.final as select a.date, a.campaign, a.visits, b.spent, b.clicks, b.impressions from facebook.optimization a, facebook.cost_campaign b where a.date=b.date and a.campaign=b.campaign;

drop table facebook.optimization;

drop table facebook.cost_campaign;

create index final on facebook.final(date, campaign);

update facebook.facebook_optimization_region_co f inner join facebook.final final on f.date=final.date and f.campaign=final.campaign set f.cost=(f.visits/(final.visits))*final.spent, f.clicks=(f.visits/(final.visits))*final.clicks, f.impressions=(f.visits/(final.visits))*final.impressions where datediff(curdate(), f.date)<@days;

drop table facebook.final;

update facebook.facebook_optimization_region_co set cost = 0 where cost is null;

update facebook.facebook_optimization_region_co set clicks = 0 where clicks is null;

update facebook.facebook_optimization_region_co set impressions = 0 where impressions is null;

update facebook.facebook_optimization_region_co set category = substr(substr(campaign, locate('.',campaign)+1), 1, locate('.', substr(campaign, locate('.',campaign)+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co set gender = substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1) ,1 , locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co set age = substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,1 ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co set segmentation = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) ,1 ,locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co set extra_field = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) , locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))+1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, PC2_absolute=PC2_absolute/xr, cum_net_rev_30_cohort=cum_net_rev_30_cohort/xr, cum_net_rev_60_cohort=cum_net_rev_60_cohort/xr, PC2_30_cohort=PC2_30_cohort/xr, PC2_60_cohort=PC2_60_cohort/xr where r.country='COL' and datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co set cost=cost/1.23 where date >= '2013-05-01' and datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co set oac_transactions=0, net_revenue=0, new_customers=0, oac_transactions_30_cohort=0, oac_transactions_60_cohort=0, cum_net_rev_30_cohort=0, cum_net_rev_60_cohort=0 where obc_transactions=0 and datediff(curdate(), date)<@days;

-- Peru
-- Facebook Optimization Region

update facebook.facebook_campaign set campaign = replace(campaign, '_', '.');

update facebook.ga_facebook_ads_region_pe set campaign = replace(campaign, '_', '.');

update facebook.ga_facebook_transaction_id_region_pe set campaign = replace(campaign, '_', '.');

delete from facebook.facebook_optimization_region_pe where datediff(curdate(), date)<@days;

insert into facebook.facebook_optimization_region_pe (date, campaign, country, region, city, obc_transactions, visits, gross_revenue, carts) select date, campaign, country, region, city, transactions, visits, transaction_revenue, goal2starts from facebook.ga_facebook_ads_region_pe where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=facebook.week_iso(date) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe f set f.oac_transactions = (select count(distinct z.transaction_id) from production_pe.tbl_order_detail a, facebook.ga_facebook_transaction_id_region_pe z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and a.oac=1) where datediff(curdate(), date)<@days;

create table facebook.temporary as select r.date, r.campaign, r.country, r.region, r.city, (select t.custid from production_pe.tbl_order_detail t where t.order_nr=r.transaction_id and oac=1 group by t.custid) as custid, r.transaction_id, (select sum(t.paid_price) from production_pe.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as paid_price, (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) from production_pe.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as PC2, (select sum(new_customers) from production_pe.tbl_order_detail z where z.order_nr=r.transaction_id and z.oac=1 group by z.order_nr) as new_customers from facebook.ga_facebook_transaction_id_region_pe r where datediff(curdate(), date)<@days group by transaction_id;

-- Back Cohort

create table facebook.temporary_back_cohort(
date date,
campaign varchar(255),
country varchar(255),
region varchar(255),
city varchar(255),
custid int,
transaction_id varchar(255),
oac_transactions_30_cohort float,
oac_transactions_60_cohort float,
paid_price_30_cohort float,
paid_price_60_cohort float,
PC2_30_cohort float,
PC2_60_cohort float,
repeated int
);


create index custid on facebook.temporary_back_cohort(custid);

create index all_index on facebook.temporary_back_cohort(date, campaign, country, city);

insert into facebook.temporary_back_cohort (date, campaign, country, region, city, custid, transaction_id) select date, campaign, country, region, city, custid, transaction_id from facebook.temporary where new_customers is not null and datediff(curdate(), date)<@days;

create table facebook.backup_temporary_back_cohort as select * from facebook.temporary_back_cohort;

update facebook.temporary_back_cohort a set repeated = (select count(*) as total from facebook.backup_temporary_back_cohort b where a.date=b.date and a.campaign=b.campaign and a.country=b.country and a.region=b.region and a.city=b.city  group by date, campaign, country, region, city, custid having count(*)>1) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort a set repeated = 1 where repeated is null;

drop table facebook.backup_temporary_back_cohort;

update facebook.temporary_back_cohort b set oac_transactions_30_cohort = (select count(distinct t.order_nr) as oac_transactions from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set paid_price_30_cohort = (select sum(t.paid_price) as paid_price from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set PC2_30_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set oac_transactions_60_cohort = (select count(distinct t.order_nr) as oac_transactions from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set paid_price_60_cohort = (select sum(t.paid_price) as paid_price from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set PC2_60_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort set oac_transactions_30_cohort=oac_transactions_30_cohort/repeated, paid_price_30_cohort=paid_price_30_cohort/repeated, PC2_30_cohort=PC2_30_cohort/repeated, oac_transactions_60_cohort=oac_transactions_60_cohort/repeated, paid_price_60_cohort=paid_price_60_cohort/repeated, PC2_60_cohort=PC2_60_cohort/repeated where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe f set oac_transactions_30_cohort = (select sum(oac_transactions_30_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe f set cum_net_rev_30_cohort = (select sum(paid_price_30_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe f set PC2_30_cohort = (select sum(PC2_30_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe f set oac_transactions_60_cohort = (select sum(oac_transactions_60_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe f set cum_net_rev_60_cohort = (select sum(paid_price_60_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe f set PC2_60_cohort = (select sum(PC2_60_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

drop table facebook.temporary_back_cohort;

update facebook.facebook_optimization_region_pe set oac_transactions_30_cohort = 0 where oac_transactions_30_cohort is null;

update facebook.facebook_optimization_region_pe set oac_transactions_60_cohort = 0 where oac_transactions_60_cohort is null;

update facebook.facebook_optimization_region_pe set cum_net_rev_30_cohort = 0 where cum_net_rev_30_cohort is null;

update facebook.facebook_optimization_region_pe set cum_net_rev_60_cohort = 0 where cum_net_rev_60_cohort is null;

-- Back Cohort

alter table facebook.temporary add column avg_discount float;

update facebook.temporary r set avg_discount = (select avg(1-(i.unit_price/i.original_unit_price)) from bob_live_pe.sales_order o inner join bob_live_pe.sales_order_item i on i.fk_sales_order = o.id_sales_order where r.transaction_id=o.order_nr group by o.order_nr) where datediff(curdate(), date)<@days; 

create index temporary on facebook.temporary(campaign, date, country, city);

update facebook.facebook_optimization_region_pe f set net_revenue= (select sum(paid_price) from facebook.temporary z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe f set new_customers= (select sum(z.new_customers) from facebook.temporary z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe f set PC2_absolute= (select sum(PC2) from facebook.temporary z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe f set `avg_discount(%)`= (select (avg(avg_discount))*100 from facebook.temporary z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city) where datediff(curdate(), date)<@days;

drop table facebook.temporary;

update facebook.facebook_optimization_region_pe set oac_transactions = 0 where oac_transactions is null;

update facebook.facebook_optimization_region_pe set net_revenue = 0 where net_revenue is null;

update facebook.facebook_optimization_region_pe set new_customers = 0 where new_customers is null;

create table facebook.optimization as select date, campaign, sum(visits)as visits from facebook.facebook_optimization_region_pe where datediff(curdate(), date)<@days group by date, campaign;

create index optimization on facebook.optimization(date, campaign);

create table facebook.cost_campaign as select date, campaign, sum(spent) as spent, sum(clicks) as clicks, sum(social_impressions) as impressions from facebook.facebook_campaign where datediff(curdate(), date)<@days group by date, campaign;

create index cost_campaign on facebook.cost_campaign(date, campaign);

create table facebook.final as select a.date, a.campaign, a.visits, b.spent, b.clicks, b.impressions from facebook.optimization a, facebook.cost_campaign b where a.date=b.date and a.campaign=b.campaign;

drop table facebook.optimization;

drop table facebook.cost_campaign;

create index final on facebook.final(date, campaign);

update facebook.facebook_optimization_region_pe f inner join facebook.final final on f.date=final.date and f.campaign=final.campaign set f.cost=(f.visits/(final.visits))*final.spent, f.clicks=(f.visits/(final.visits))*final.clicks, f.impressions=(f.visits/(final.visits))*final.impressions where datediff(curdate(), f.date)<@days;

drop table facebook.final;

update facebook.facebook_optimization_region_pe set cost = 0 where cost is null;

update facebook.facebook_optimization_region_pe set clicks = 0 where clicks is null;

update facebook.facebook_optimization_region_pe set impressions = 0 where impressions is null;

update facebook.facebook_optimization_region_pe set category = substr(substr(campaign, locate('.',campaign)+1), 1, locate('.', substr(campaign, locate('.',campaign)+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe set gender = substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1) ,1 , locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe set age = substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,1 ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe set segmentation = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) ,1 ,locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe set extra_field = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) , locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))+1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, PC2_absolute=PC2_absolute/xr, cum_net_rev_30_cohort=cum_net_rev_30_cohort/xr, cum_net_rev_60_cohort=cum_net_rev_60_cohort/xr, PC2_30_cohort=PC2_30_cohort/xr, PC2_60_cohort=PC2_60_cohort/xr where r.country='PER' and datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe set oac_transactions=0, net_revenue=0, new_customers=0, oac_transactions_30_cohort=0, oac_transactions_60_cohort=0, cum_net_rev_30_cohort=0, cum_net_rev_60_cohort=0 where obc_transactions=0 and datediff(curdate(), date)<@days;

call facebook.fanpage_optimization;

select  'Facebook Optimization Table: end',now();

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `facebook_optimization_co`()
begin

-- Colombia
-- Facebook Optimization Region

set @days:=30;

update facebook.ga_facebook_ads_region_co set campaign = replace(campaign, '_', '.');

update facebook.ga_facebook_transaction_id_region_co set campaign = replace(campaign, '_', '.');

delete from facebook.facebook_optimization_region_co where datediff(curdate(), date)<@days;

insert into facebook.facebook_optimization_region_co (date, campaign, country, region, city, obc_transactions, visits, gross_revenue, carts) select date, campaign, country, region, city, transactions, visits, transaction_revenue, goal2starts from facebook.ga_facebook_ads_region_co where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=facebook.week_iso(date) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co f set f.oac_transactions = (select count(distinct z.transaction_id) from production_co.tbl_order_detail a, facebook.ga_facebook_transaction_id_region_co z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and a.oac=1) where datediff(curdate(), date)<@days;

create table facebook.temporary_co as select r.date, r.campaign, r.country, r.region, r.city, (select t.custid from production_co.tbl_order_detail t where t.order_nr=r.transaction_id and oac=1 group by t.custid) as custid, r.transaction_id, (select sum(t.paid_price) from production_co.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as paid_price, (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) from production_co.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as PC2, (select sum(new_customers) from production_co.tbl_order_detail z where z.order_nr=r.transaction_id and z.oac=1 group by z.order_nr) as new_customers from facebook.ga_facebook_transaction_id_region_co r where datediff(curdate(), date)<@days group by transaction_id;

-- Back Cohort

create table facebook.temporary_back_cohort_co(
date date,
campaign varchar(255),
country varchar(255),
region varchar(255),
city varchar(255),
custid int,
transaction_id varchar(255),
oac_transactions_30_cohort float,
oac_transactions_60_cohort float,
paid_price_30_cohort float,
paid_price_60_cohort float,
PC2_30_cohort float,
PC2_60_cohort float,
repeated int
);


create index custid on facebook.temporary_back_cohort_co(custid);

create index all_index on facebook.temporary_back_cohort_co(date, campaign, country, city);

insert into facebook.temporary_back_cohort_co (date, campaign, country, region, city, custid, transaction_id) select date, campaign, country, region, city, custid, transaction_id from facebook.temporary_co where new_customers !=0 and datediff(curdate(), date)<@days;

create table facebook.backup_temporary_back_cohort_co as select * from facebook.temporary_back_cohort_co;

update facebook.temporary_back_cohort_co a set repeated = (select count(*) as total from facebook.backup_temporary_back_cohort_co b where a.date=b.date and a.campaign=b.campaign and a.country=b.country and a.region=b.region and a.city=b.city  group by date, campaign, country, region, city, custid having count(*)>1);

update facebook.temporary_back_cohort_co a set repeated = 1 where repeated is null;

drop table facebook.backup_temporary_back_cohort_co;

update facebook.temporary_back_cohort_co b set oac_transactions_30_cohort = (select count(distinct t.order_nr) as oac_transactions from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers = 0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_co b set paid_price_30_cohort = (select sum(t.paid_price) as paid_price from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers = 0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_co b set PC2_30_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers = 0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_co b set oac_transactions_60_cohort = (select count(distinct t.order_nr) as oac_transactions from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers = 0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_co b set paid_price_60_cohort = (select sum(t.paid_price) as paid_price from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers = 0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_co b set PC2_60_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers = 0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_co set oac_transactions_30_cohort=oac_transactions_30_cohort/repeated, paid_price_30_cohort=paid_price_30_cohort/repeated, PC2_30_cohort=PC2_30_cohort/repeated, oac_transactions_60_cohort=oac_transactions_60_cohort/repeated, paid_price_60_cohort=paid_price_60_cohort/repeated, PC2_60_cohort=PC2_60_cohort/repeated where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co f set oac_transactions_30_cohort = (select sum(oac_transactions_30_cohort) from facebook.temporary_back_cohort_co b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co f set cum_net_rev_30_cohort = (select sum(paid_price_30_cohort) from facebook.temporary_back_cohort_co b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co f set PC2_30_cohort = (select sum(PC2_30_cohort) from facebook.temporary_back_cohort_co b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co f set oac_transactions_60_cohort = (select sum(oac_transactions_60_cohort) from facebook.temporary_back_cohort_co b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co f set cum_net_rev_60_cohort = (select sum(paid_price_60_cohort) from facebook.temporary_back_cohort_co b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co f set PC2_60_cohort = (select sum(PC2_60_cohort) from facebook.temporary_back_cohort_co b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

drop table facebook.temporary_back_cohort_co;

update facebook.facebook_optimization_region_co set oac_transactions_30_cohort = 0 where oac_transactions_30_cohort is null;

update facebook.facebook_optimization_region_co set oac_transactions_60_cohort = 0 where oac_transactions_60_cohort is null;

update facebook.facebook_optimization_region_co set cum_net_rev_30_cohort = 0 where cum_net_rev_30_cohort is null;

update facebook.facebook_optimization_region_co set cum_net_rev_60_cohort = 0 where cum_net_rev_60_cohort is null;

-- Back Cohort

alter table facebook.temporary_co add column avg_discount float;

update facebook.temporary_co r set avg_discount = (select avg(1-(i.unit_price/i.original_unit_price)) from bob_live_co.sales_order o inner join bob_live_co.sales_order_item i on i.fk_sales_order = o.id_sales_order where r.transaction_id=o.order_nr group by o.order_nr) where datediff(curdate(), date)<@days; 

create index temporary_co on facebook.temporary_co(campaign, date, country, city);

update facebook.facebook_optimization_region_co f set net_revenue= (select sum(paid_price) from facebook.temporary_co z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co f set new_customers= (select sum(z.new_customers) from facebook.temporary_co z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co f set PC2_absolute= (select sum(PC2) from facebook.temporary_co z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co f set `avg_discount(%)`= (select (avg(avg_discount))*100 from facebook.temporary_co z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city) where datediff(curdate(), date)<@days;

drop table facebook.temporary_co;

update facebook.facebook_optimization_region_co set oac_transactions = 0 where oac_transactions is null;

update facebook.facebook_optimization_region_co set net_revenue = 0 where net_revenue is null;

update facebook.facebook_optimization_region_co set new_customers = 0 where new_customers is null;

create table facebook.optimization_co as select date, campaign, sum(visits)as visits from facebook.facebook_optimization_region_co where datediff(curdate(), date)<@days group by date, campaign;

create index optimization_co on facebook.optimization_co(date, campaign);

create table facebook.cost_campaign_co as select date, campaign, sum(spent) as spent, sum(clicks) as clicks, sum(social_impressions) as impressions from facebook.facebook_campaign where datediff(curdate(), date)<@days group by date, campaign;

create index cost_campaign_co on facebook.cost_campaign_co(date, campaign);

create table facebook.final_co as select a.date, a.campaign, a.visits, b.spent, b.clicks, b.impressions from facebook.optimization_co a, facebook.cost_campaign_co b where a.date=b.date and a.campaign=b.campaign;

drop table facebook.optimization_co;

drop table facebook.cost_campaign_co;

create index final_co on facebook.final_co(date, campaign);

update facebook.facebook_optimization_region_co f inner join facebook.final_co final_co on f.date=final_co.date and f.campaign=final_co.campaign set f.cost=(f.visits/(final_co.visits))*final_co.spent, f.clicks=(f.visits/(final_co.visits))*final_co.clicks, f.impressions=(f.visits/(final_co.visits))*final_co.impressions where datediff(curdate(), f.date)<@days;

drop table facebook.final_co;

update facebook.facebook_optimization_region_co set cost = 0 where cost is null;

update facebook.facebook_optimization_region_co set clicks = 0 where clicks is null;

update facebook.facebook_optimization_region_co set impressions = 0 where impressions is null;

update facebook.facebook_optimization_region_co set category = substr(substr(campaign, locate('.',campaign)+1), 1, locate('.', substr(campaign, locate('.',campaign)+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co set gender = substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1) ,1 , locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co set age = substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,1 ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co set segmentation = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) ,1 ,locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co set extra_field = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) , locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))+1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, PC2_absolute=PC2_absolute/xr, cum_net_rev_30_cohort=cum_net_rev_30_cohort/xr, cum_net_rev_60_cohort=cum_net_rev_60_cohort/xr, PC2_30_cohort=PC2_30_cohort/xr, PC2_60_cohort=PC2_60_cohort/xr where r.country='COL' and datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co set cost=cost/1.23 where date >= '2013-05-01' and datediff(curdate(), date)<@days;

##update facebook.facebook_optimization_region_co set oac_transactions=0, net_revenue=0, new_customers=0, oac_transactions_30_cohort=0, oac_transactions_60_cohort=0, cum_net_rev_30_cohort=0, cum_net_rev_60_cohort=0 where obc_transactions=0 and datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_co set obc_transactions=oac_transactions, gross_revenue=net_revenue where obc_transactions=0 and oac_transactions!=0 and datediff(curdate(), date)<@days;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `facebook_optimization_keyword_co`()
begin

-- Colombia
-- Facebook Optimization Region

set @days:=30;

update facebook.ga_facebook_ads_keyword_co set campaign = replace(campaign, '_', '.');

update facebook.ga_facebook_transaction_id_keyword_co set campaign = replace(campaign, '_', '.');

delete from facebook.facebook_optimization_keyword_co where datediff(curdate(), date)<@days;

insert into facebook.facebook_optimization_keyword_co (date, campaign, keyword, country, region, city, obc_transactions, visits, gross_revenue, carts) select date, campaign, keyword, country, region, city, transactions, visits, transaction_revenue, goal2starts from facebook.ga_facebook_ads_keyword_co where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_co set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=facebook.week_iso(date) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_co set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_co f set f.oac_transactions = (select count(distinct z.transaction_id) from production_co.tbl_order_detail a, facebook.ga_facebook_transaction_id_keyword_co z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.keyword=z.keyword and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and a.oac=1) where datediff(curdate(), date)<@days;

create table facebook.temporary_co as select r.date, r.campaign, r.keyword, r.country, r.region, r.city, (select t.custid from production_co.tbl_order_detail t where t.order_nr=r.transaction_id and oac=1 group by t.custid) as custid, r.transaction_id, (select sum(t.paid_price) from production_co.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as paid_price, (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) from production_co.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as PC2, (select sum(new_customers) from production_co.tbl_order_detail z where z.order_nr=r.transaction_id and z.oac=1 group by z.order_nr) as new_customers from facebook.ga_facebook_transaction_id_keyword_co r where datediff(curdate(), date)<@days group by transaction_id;

-- Back Cohort

create table facebook.temporary_back_cohort_co(
date date,
campaign varchar(255),
keyword varchar(255),
country varchar(255),
region varchar(255),
city varchar(255),
custid int,
transaction_id varchar(255),
oac_transactions_30_cohort float,
oac_transactions_60_cohort float,
paid_price_30_cohort float,
paid_price_60_cohort float,
PC2_30_cohort float,
PC2_60_cohort float,
repeated int
);


create index custid on facebook.temporary_back_cohort_co(custid);

create index all_index on facebook.temporary_back_cohort_co(date, campaign, country, city);

insert into facebook.temporary_back_cohort_co (date, campaign, keyword, country, region, city, custid, transaction_id) select date, campaign, keyword, country, region, city, custid, transaction_id from facebook.temporary_co where new_customers !=0 and datediff(curdate(), date)<@days;

create table facebook.backup_temporary_back_cohort_co as select * from facebook.temporary_back_cohort_co;

update facebook.temporary_back_cohort_co a set repeated = (select count(*) as total from facebook.backup_temporary_back_cohort_co b where a.date=b.date and a.campaign=b.campaign and a.country=b.country and a.region=b.region and a.city=b.city and a.keyword=b.keyword  group by date, campaign, keyword, country, region, city, custid having count(*)>1);

update facebook.temporary_back_cohort_co a set repeated = 1 where repeated is null;

drop table facebook.backup_temporary_back_cohort_co;

update facebook.temporary_back_cohort_co b set oac_transactions_30_cohort = (select count(distinct t.order_nr) as oac_transactions from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers = 0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_co b set paid_price_30_cohort = (select sum(t.paid_price) as paid_price from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers = 0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_co b set PC2_30_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers = 0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_co b set oac_transactions_60_cohort = (select count(distinct t.order_nr) as oac_transactions from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers = 0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_co b set paid_price_60_cohort = (select sum(t.paid_price) as paid_price from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers = 0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_co b set PC2_60_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers = 0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_co set oac_transactions_30_cohort=oac_transactions_30_cohort/repeated, paid_price_30_cohort=paid_price_30_cohort/repeated, PC2_30_cohort=PC2_30_cohort/repeated, oac_transactions_60_cohort=oac_transactions_60_cohort/repeated, paid_price_60_cohort=paid_price_60_cohort/repeated, PC2_60_cohort=PC2_60_cohort/repeated where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_co f set oac_transactions_30_cohort = (select sum(oac_transactions_30_cohort) from facebook.temporary_back_cohort_co b where f.date=b.date and f.campaign=b.campaign and f.keyword=b.keyword and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_co f set cum_net_rev_30_cohort = (select sum(paid_price_30_cohort) from facebook.temporary_back_cohort_co b where f.date=b.date and f.campaign=b.campaign and f.keyword=b.keyword and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_co f set PC2_30_cohort = (select sum(PC2_30_cohort) from facebook.temporary_back_cohort_co b where f.date=b.date and f.campaign=b.campaign and f.keyword=b.keyword and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_co f set oac_transactions_60_cohort = (select sum(oac_transactions_60_cohort) from facebook.temporary_back_cohort_co b where f.date=b.date and f.campaign=b.campaign and f.keyword=b.keyword and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_co f set cum_net_rev_60_cohort = (select sum(paid_price_60_cohort) from facebook.temporary_back_cohort_co b where f.date=b.date and f.campaign=b.campaign and f.keyword=b.keyword and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_co f set PC2_60_cohort = (select sum(PC2_60_cohort) from facebook.temporary_back_cohort_co b where f.date=b.date and f.campaign=b.campaign and f.keyword=b.keyword and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

drop table facebook.temporary_back_cohort_co;

update facebook.facebook_optimization_keyword_co set oac_transactions_30_cohort = 0 where oac_transactions_30_cohort is null;

update facebook.facebook_optimization_keyword_co set oac_transactions_60_cohort = 0 where oac_transactions_60_cohort is null;

update facebook.facebook_optimization_keyword_co set cum_net_rev_30_cohort = 0 where cum_net_rev_30_cohort is null;

update facebook.facebook_optimization_keyword_co set cum_net_rev_60_cohort = 0 where cum_net_rev_60_cohort is null;

-- Back Cohort

alter table facebook.temporary_co add column avg_discount float;

update facebook.temporary_co r set avg_discount = (select avg(1-(i.unit_price/i.original_unit_price)) from bob_live_co.sales_order o inner join bob_live_co.sales_order_item i on i.fk_sales_order = o.id_sales_order where r.transaction_id=o.order_nr group by o.order_nr) where datediff(curdate(), date)<@days; 

create index temporary_co on facebook.temporary_co(campaign, date, country, city);

update facebook.facebook_optimization_keyword_co f set net_revenue= (select sum(paid_price) from facebook.temporary_co z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and f.keyword=z.keyword) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_co f set new_customers= (select sum(z.new_customers) from facebook.temporary_co z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and f.keyword=z.keyword) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_co f set PC2_absolute= (select sum(PC2) from facebook.temporary_co z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and f.keyword=z.keyword) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_co f set `avg_discount(%)`= (select (avg(avg_discount))*100 from facebook.temporary_co z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and f.keyword=z.keyword) where datediff(curdate(), date)<@days;

drop table facebook.temporary_co;

update facebook.facebook_optimization_keyword_co set oac_transactions = 0 where oac_transactions is null;

update facebook.facebook_optimization_keyword_co set net_revenue = 0 where net_revenue is null;

update facebook.facebook_optimization_keyword_co set new_customers = 0 where new_customers is null;

create table facebook.optimization_co as select date, campaign, sum(visits)as visits from facebook.facebook_optimization_keyword_co where datediff(curdate(), date)<@days group by date, campaign;

create index optimization_co on facebook.optimization_co(date, campaign);

create table facebook.cost_campaign_co as select date, campaign, sum(spent) as spent, sum(clicks) as clicks, sum(social_impressions) as impressions from facebook.facebook_campaign where datediff(curdate(), date)<@days group by date, campaign;

create index cost_campaign_co on facebook.cost_campaign_co(date, campaign);

create table facebook.final_co as select a.date, a.campaign, a.visits, b.spent, b.clicks, b.impressions from facebook.optimization_co a, facebook.cost_campaign_co b where a.date=b.date and a.campaign=b.campaign;

drop table facebook.optimization_co;

drop table facebook.cost_campaign_co;

create index final_co on facebook.final_co(date, campaign);

update facebook.facebook_optimization_keyword_co f inner join facebook.final_co final_co on f.date=final_co.date and f.campaign=final_co.campaign set f.cost=(f.visits/(final_co.visits))*final_co.spent, f.clicks=(f.visits/(final_co.visits))*final_co.clicks, f.impressions=(f.visits/(final_co.visits))*final_co.impressions where datediff(curdate(), f.date)<@days;

drop table facebook.final_co;

update facebook.facebook_optimization_keyword_co set cost = 0 where cost is null;

update facebook.facebook_optimization_keyword_co set clicks = 0 where clicks is null;

update facebook.facebook_optimization_keyword_co set impressions = 0 where impressions is null;

update facebook.facebook_optimization_keyword_co set category = substr(substr(campaign, locate('.',campaign)+1), 1, locate('.', substr(campaign, locate('.',campaign)+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_co set gender = substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1) ,1 , locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_co set age = substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,1 ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_co set segmentation = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) ,1 ,locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_co set extra_field = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) , locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))+1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_co s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, PC2_absolute=PC2_absolute/xr, cum_net_rev_30_cohort=cum_net_rev_30_cohort/xr, cum_net_rev_60_cohort=cum_net_rev_60_cohort/xr, PC2_30_cohort=PC2_30_cohort/xr, PC2_60_cohort=PC2_60_cohort/xr where r.country='COL' and datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_co set cost=cost/1.23 where date >= '2013-05-01' and datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_co set obc_transactions=oac_transactions, gross_revenue=net_revenue where obc_transactions=0 and oac_transactions!=0 and datediff(curdate(), date)<@days;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `facebook_optimization_keyword_mx`()
begin

-- Mexico
-- Facebook Optimization Region

set @days:=30;

update facebook.ga_facebook_ads_keyword_mx set campaign = replace(campaign, '_', '.');

update facebook.ga_facebook_transaction_id_keyword_mx set campaign = replace(campaign, '_', '.');

delete from facebook.facebook_optimization_keyword_mx where datediff(curdate(), date)<@days;

insert into facebook.facebook_optimization_keyword_mx (date, campaign, keyword, country, region, city, obc_transactions, visits, gross_revenue, carts) select date, campaign, keyword, country, region, city, transactions, visits, transaction_revenue, goal2starts from facebook.ga_facebook_ads_keyword_mx where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_mx set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=facebook.week_iso(date) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_mx set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_mx f set f.oac_transactions = (select count(distinct z.transaction_id) from production.tbl_order_detail a, facebook.ga_facebook_transaction_id_keyword_mx z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.keyword=z.keyword and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and a.oac=1) where datediff(curdate(), date)<@days;

create table facebook.temporary_mx as select r.date, r.campaign, r.keyword, r.country, r.region, r.city, (select t.custid from production.tbl_order_detail t where t.order_nr=r.transaction_id and oac=1 group by t.custid) as custid, r.transaction_id, (select sum(t.paid_price) from production.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as paid_price, (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) from production.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as PC2, (select sum(new_customers) from production.tbl_order_detail z where z.order_nr=r.transaction_id and z.oac=1 group by z.order_nr) as new_customers from facebook.ga_facebook_transaction_id_keyword_mx r where datediff(curdate(), date)<@days group by transaction_id;

-- Back Cohort

create table facebook.temporary_back_cohort_mx(
date date,
campaign varchar(255),
keyword varchar(255),
country varchar(255),
region varchar(255),
city varchar(255),
custid int,
transaction_id varchar(255),
oac_transactions_30_cohort float,
oac_transactions_60_cohort float,
paid_price_30_cohort float,
paid_price_60_cohort float,
PC2_30_cohort float,
PC2_60_cohort float,
repeated int
);


create index custid on facebook.temporary_back_cohort_mx(custid);

create index all_index on facebook.temporary_back_cohort_mx(date, campaign, country, city);

insert into facebook.temporary_back_cohort_mx (date, campaign, keyword, country, region, city, custid, transaction_id) select date, campaign, keyword, country, region, city, custid, transaction_id from facebook.temporary_mx where new_customers is not null and datediff(curdate(), date)<@days;

create table facebook.backup_temporary_back_cohort_mx as select * from facebook.temporary_back_cohort_mx;

update facebook.temporary_back_cohort_mx a set repeated = (select count(*) as total from facebook.backup_temporary_back_cohort_mx b where a.date=b.date and a.campaign=b.campaign and a.country=b.country and a.region=b.region and a.city=b.city and a.keyword=b.keyword  group by date, campaign, keyword, country, region, city, custid having count(*)>1);

update facebook.temporary_back_cohort_mx a set repeated = 1 where repeated is null;

drop table facebook.backup_temporary_back_cohort_mx;

update facebook.temporary_back_cohort_mx b set oac_transactions_30_cohort = (select count(distinct t.order_nr) as oac_transactions from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_mx b set paid_price_30_cohort = (select sum(t.paid_price) as paid_price from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_mx b set PC2_30_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_mx b set oac_transactions_60_cohort = (select count(distinct t.order_nr) as oac_transactions from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_mx b set paid_price_60_cohort = (select sum(t.paid_price) as paid_price from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_mx b set PC2_60_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_mx set oac_transactions_30_cohort=oac_transactions_30_cohort/repeated, paid_price_30_cohort=paid_price_30_cohort/repeated, PC2_30_cohort=PC2_30_cohort/repeated, oac_transactions_60_cohort=oac_transactions_60_cohort/repeated, paid_price_60_cohort=paid_price_60_cohort/repeated, PC2_60_cohort=PC2_60_cohort/repeated where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_mx f set oac_transactions_30_cohort = (select sum(oac_transactions_30_cohort) from facebook.temporary_back_cohort_mx b where f.date=b.date and f.campaign=b.campaign and f.keyword=b.keyword and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_mx f set cum_net_rev_30_cohort = (select sum(paid_price_30_cohort) from facebook.temporary_back_cohort_mx b where f.date=b.date and f.campaign=b.campaign and f.keyword=b.keyword and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_mx f set PC2_30_cohort = (select sum(PC2_30_cohort) from facebook.temporary_back_cohort_mx b where f.date=b.date and f.campaign=b.campaign and f.keyword=b.keyword and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_mx f set oac_transactions_60_cohort = (select sum(oac_transactions_60_cohort) from facebook.temporary_back_cohort_mx b where f.date=b.date and f.campaign=b.campaign and f.keyword=b.keyword and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_mx f set cum_net_rev_60_cohort = (select sum(paid_price_60_cohort) from facebook.temporary_back_cohort_mx b where f.date=b.date and f.campaign=b.campaign and f.keyword=b.keyword and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_mx f set PC2_60_cohort = (select sum(PC2_60_cohort) from facebook.temporary_back_cohort_mx b where f.date=b.date and f.campaign=b.campaign and f.keyword=b.keyword and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

drop table facebook.temporary_back_cohort_mx;

update facebook.facebook_optimization_keyword_mx set oac_transactions_30_cohort = 0 where oac_transactions_30_cohort is null;

update facebook.facebook_optimization_keyword_mx set oac_transactions_60_cohort = 0 where oac_transactions_60_cohort is null;

update facebook.facebook_optimization_keyword_mx set cum_net_rev_30_cohort = 0 where cum_net_rev_30_cohort is null;

update facebook.facebook_optimization_keyword_mx set cum_net_rev_60_cohort = 0 where cum_net_rev_60_cohort is null;

-- Back Cohort

alter table facebook.temporary_mx add column avg_discount float;

update facebook.temporary_mx r set avg_discount = (select avg(1-(i.unit_price/i.original_unit_price)) from bob_live_co.sales_order o inner join bob_live_co.sales_order_item i on i.fk_sales_order = o.id_sales_order where r.transaction_id=o.order_nr group by o.order_nr) where datediff(curdate(), date)<@days; 

create index temporary_mx on facebook.temporary_mx(campaign, date, country, city);

update facebook.facebook_optimization_keyword_mx f set net_revenue= (select sum(paid_price) from facebook.temporary_mx z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and f.keyword=z.keyword) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_mx f set new_customers= (select sum(z.new_customers) from facebook.temporary_mx z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and f.keyword=z.keyword) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_mx f set PC2_absolute= (select sum(PC2) from facebook.temporary_mx z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and f.keyword=z.keyword) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_mx f set `avg_discount(%)`= (select (avg(avg_discount))*100 from facebook.temporary_mx z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and f.keyword=z.keyword) where datediff(curdate(), date)<@days;

drop table facebook.temporary_mx;

update facebook.facebook_optimization_keyword_mx set oac_transactions = 0 where oac_transactions is null;

update facebook.facebook_optimization_keyword_mx set net_revenue = 0 where net_revenue is null;

update facebook.facebook_optimization_keyword_mx set new_customers = 0 where new_customers is null;

create table facebook.optimization_mx as select date, campaign, sum(visits)as visits from facebook.facebook_optimization_keyword_mx where datediff(curdate(), date)<@days group by date, campaign;

create index optimization_mx on facebook.optimization_mx(date, campaign);

create table facebook.cost_campaign_mx as select date, campaign, sum(spent) as spent, sum(clicks) as clicks, sum(social_impressions) as impressions from facebook.facebook_campaign where datediff(curdate(), date)<@days group by date, campaign;

create index cost_campaign_mx on facebook.cost_campaign_mx(date, campaign);

create table facebook.final_mx as select a.date, a.campaign, a.visits, b.spent, b.clicks, b.impressions from facebook.optimization_mx a, facebook.cost_campaign_mx b where a.date=b.date and a.campaign=b.campaign;

drop table facebook.optimization_mx;

drop table facebook.cost_campaign_mx;

create index final_mx on facebook.final_mx(date, campaign);

update facebook.facebook_optimization_keyword_mx f inner join facebook.final_mx final_mx on f.date=final_mx.date and f.campaign=final_mx.campaign set f.cost=(f.visits/(final_mx.visits))*final_mx.spent, f.clicks=(f.visits/(final_mx.visits))*final_mx.clicks, f.impressions=(f.visits/(final_mx.visits))*final_mx.impressions where datediff(curdate(), f.date)<@days;

drop table facebook.final_mx;

update facebook.facebook_optimization_keyword_mx set cost = 0 where cost is null;

update facebook.facebook_optimization_keyword_mx set clicks = 0 where clicks is null;

update facebook.facebook_optimization_keyword_mx set impressions = 0 where impressions is null;

update facebook.facebook_optimization_keyword_mx set category = substr(substr(campaign, locate('.',campaign)+1), 1, locate('.', substr(campaign, locate('.',campaign)+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_mx set gender = substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1) ,1 , locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_mx set age = substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,1 ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_mx set segmentation = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) ,1 ,locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_mx set extra_field = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) , locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))+1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_mx s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, PC2_absolute=PC2_absolute/xr, cum_net_rev_30_cohort=cum_net_rev_30_cohort/xr, cum_net_rev_60_cohort=cum_net_rev_60_cohort/xr, PC2_30_cohort=PC2_30_cohort/xr, PC2_60_cohort=PC2_60_cohort/xr where r.country='MEX' and datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_mx set obc_transactions=oac_transactions, gross_revenue=net_revenue where obc_transactions=0 and oac_transactions!=0 and datediff(curdate(), date)<@days;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `facebook_optimization_keyword_pe`()
begin

-- Peru
-- Facebook Optimization Region

set @days:=30;

update facebook.ga_facebook_ads_keyword_pe set campaign = replace(campaign, '_', '.');

update facebook.ga_facebook_transaction_id_keyword_pe set campaign = replace(campaign, '_', '.');

delete from facebook.facebook_optimization_keyword_pe where datediff(curdate(), date)<@days;

insert into facebook.facebook_optimization_keyword_pe (date, campaign, keyword, country, region, city, obc_transactions, visits, gross_revenue, carts) select date, campaign, keyword, country, region, city, transactions, visits, transaction_revenue, goal2starts from facebook.ga_facebook_ads_keyword_pe where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=facebook.week_iso(date) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe f set f.oac_transactions = (select count(distinct z.transaction_id) from production_pe.tbl_order_detail a, facebook.ga_facebook_transaction_id_keyword_pe z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.keyword=z.keyword and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and a.oac=1) where datediff(curdate(), date)<@days;

create table facebook.temporary_pe as select r.date, r.campaign, r.keyword, r.country, r.region, r.city, (select t.custid from production_pe.tbl_order_detail t where t.order_nr=r.transaction_id and oac=1 group by t.custid) as custid, r.transaction_id, (select sum(t.paid_price) from production_pe.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as paid_price, (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) from production_pe.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as PC2, (select sum(new_customers) from production_pe.tbl_order_detail z where z.order_nr=r.transaction_id and z.oac=1 group by z.order_nr) as new_customers from facebook.ga_facebook_transaction_id_keyword_pe r where datediff(curdate(), date)<@days group by transaction_id;

-- Back Cohort

create table facebook.temporary_back_cohort_pe(
date date,
campaign varchar(255),
keyword varchar(255),
country varchar(255),
region varchar(255),
city varchar(255),
custid int,
transaction_id varchar(255),
oac_transactions_30_cohort float,
oac_transactions_60_cohort float,
paid_price_30_cohort float,
paid_price_60_cohort float,
PC2_30_cohort float,
PC2_60_cohort float,
repeated int
);


create index custid on facebook.temporary_back_cohort_pe(custid);

create index all_index on facebook.temporary_back_cohort_pe(date, campaign, country, city);

insert into facebook.temporary_back_cohort_pe (date, campaign, keyword, country, region, city, custid, transaction_id) select date, campaign, keyword, country, region, city, custid, transaction_id from facebook.temporary_pe where new_customers is not null and datediff(curdate(), date)<@days;

create table facebook.backup_temporary_back_cohort_pe as select * from facebook.temporary_back_cohort_pe;

update facebook.temporary_back_cohort_pe a set repeated = (select count(*) as total from facebook.backup_temporary_back_cohort_pe b where a.date=b.date and a.campaign=b.campaign and a.country=b.country and a.region=b.region and a.city=b.city and a.keyword=b.keyword  group by date, campaign, keyword, country, region, city, custid having count(*)>1);

update facebook.temporary_back_cohort_pe a set repeated = 1 where repeated is null;

drop table facebook.backup_temporary_back_cohort_pe;

update facebook.temporary_back_cohort_pe b set oac_transactions_30_cohort = (select count(distinct t.order_nr) as oac_transactions from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe b set paid_price_30_cohort = (select sum(t.paid_price) as paid_price from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe b set PC2_30_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe b set oac_transactions_60_cohort = (select count(distinct t.order_nr) as oac_transactions from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe b set paid_price_60_cohort = (select sum(t.paid_price) as paid_price from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe b set PC2_60_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe set oac_transactions_30_cohort=oac_transactions_30_cohort/repeated, paid_price_30_cohort=paid_price_30_cohort/repeated, PC2_30_cohort=PC2_30_cohort/repeated, oac_transactions_60_cohort=oac_transactions_60_cohort/repeated, paid_price_60_cohort=paid_price_60_cohort/repeated, PC2_60_cohort=PC2_60_cohort/repeated where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe f set oac_transactions_30_cohort = (select sum(oac_transactions_30_cohort) from facebook.temporary_back_cohort_pe b where f.date=b.date and f.campaign=b.campaign and f.keyword=b.keyword and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe f set cum_net_rev_30_cohort = (select sum(paid_price_30_cohort) from facebook.temporary_back_cohort_pe b where f.date=b.date and f.campaign=b.campaign and f.keyword=b.keyword and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe f set PC2_30_cohort = (select sum(PC2_30_cohort) from facebook.temporary_back_cohort_pe b where f.date=b.date and f.campaign=b.campaign and f.keyword=b.keyword and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe f set oac_transactions_60_cohort = (select sum(oac_transactions_60_cohort) from facebook.temporary_back_cohort_pe b where f.date=b.date and f.campaign=b.campaign and f.keyword=b.keyword and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe f set cum_net_rev_60_cohort = (select sum(paid_price_60_cohort) from facebook.temporary_back_cohort_pe b where f.date=b.date and f.campaign=b.campaign and f.keyword=b.keyword and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe f set PC2_60_cohort = (select sum(PC2_60_cohort) from facebook.temporary_back_cohort_pe b where f.date=b.date and f.campaign=b.campaign and f.keyword=b.keyword and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

drop table facebook.temporary_back_cohort_pe;

update facebook.facebook_optimization_keyword_pe set oac_transactions_30_cohort = 0 where oac_transactions_30_cohort is null;

update facebook.facebook_optimization_keyword_pe set oac_transactions_60_cohort = 0 where oac_transactions_60_cohort is null;

update facebook.facebook_optimization_keyword_pe set cum_net_rev_30_cohort = 0 where cum_net_rev_30_cohort is null;

update facebook.facebook_optimization_keyword_pe set cum_net_rev_60_cohort = 0 where cum_net_rev_60_cohort is null;

-- Back Cohort

alter table facebook.temporary_pe add column avg_discount float;

update facebook.temporary_pe r set avg_discount = (select avg(1-(i.unit_price/i.original_unit_price)) from bob_live_co.sales_order o inner join bob_live_co.sales_order_item i on i.fk_sales_order = o.id_sales_order where r.transaction_id=o.order_nr group by o.order_nr) where datediff(curdate(), date)<@days; 

create index temporary_pe on facebook.temporary_pe(campaign, date, country, city);

update facebook.facebook_optimization_keyword_pe f set net_revenue= (select sum(paid_price) from facebook.temporary_pe z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and f.keyword=z.keyword) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe f set new_customers= (select sum(z.new_customers) from facebook.temporary_pe z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and f.keyword=z.keyword) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe f set PC2_absolute= (select sum(PC2) from facebook.temporary_pe z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and f.keyword=z.keyword) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe f set `avg_discount(%)`= (select (avg(avg_discount))*100 from facebook.temporary_pe z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and f.keyword=z.keyword) where datediff(curdate(), date)<@days;

drop table facebook.temporary_pe;

update facebook.facebook_optimization_keyword_pe set oac_transactions = 0 where oac_transactions is null;

update facebook.facebook_optimization_keyword_pe set net_revenue = 0 where net_revenue is null;

update facebook.facebook_optimization_keyword_pe set new_customers = 0 where new_customers is null;

create table facebook.optimization_pe as select date, campaign, sum(visits)as visits from facebook.facebook_optimization_keyword_pe where datediff(curdate(), date)<@days group by date, campaign;

create index optimization_pe on facebook.optimization_pe(date, campaign);

create table facebook.cost_campaign_pe as select date, campaign, sum(spent) as spent, sum(clicks) as clicks, sum(social_impressions) as impressions from facebook.facebook_campaign where datediff(curdate(), date)<@days group by date, campaign;

create index cost_campaign_pe on facebook.cost_campaign_pe(date, campaign);

create table facebook.final_pe as select a.date, a.campaign, a.visits, b.spent, b.clicks, b.impressions from facebook.optimization_pe a, facebook.cost_campaign_pe b where a.date=b.date and a.campaign=b.campaign;

drop table facebook.optimization_pe;

drop table facebook.cost_campaign_pe;

create index final_pe on facebook.final_pe(date, campaign);

update facebook.facebook_optimization_keyword_pe f inner join facebook.final_pe final_pe on f.date=final_pe.date and f.campaign=final_pe.campaign set f.cost=(f.visits/(final_pe.visits))*final_pe.spent, f.clicks=(f.visits/(final_pe.visits))*final_pe.clicks, f.impressions=(f.visits/(final_pe.visits))*final_pe.impressions where datediff(curdate(), f.date)<@days;

drop table facebook.final_pe;

update facebook.facebook_optimization_keyword_pe set cost = 0 where cost is null;

update facebook.facebook_optimization_keyword_pe set clicks = 0 where clicks is null;

update facebook.facebook_optimization_keyword_pe set impressions = 0 where impressions is null;

update facebook.facebook_optimization_keyword_pe set category = substr(substr(campaign, locate('.',campaign)+1), 1, locate('.', substr(campaign, locate('.',campaign)+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe set gender = substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1) ,1 , locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe set age = substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,1 ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe set segmentation = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) ,1 ,locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe set extra_field = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) , locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))+1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, PC2_absolute=PC2_absolute/xr, cum_net_rev_30_cohort=cum_net_rev_30_cohort/xr, cum_net_rev_60_cohort=cum_net_rev_60_cohort/xr, PC2_30_cohort=PC2_30_cohort/xr, PC2_60_cohort=PC2_60_cohort/xr where r.country='PER' and datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe set cost=cost/1.23 where date >= '2014-01-01' and datediff(curdate(), date)<@days;

update facebook.facebook_optimization_keyword_pe set obc_transactions=oac_transactions, gross_revenue=net_revenue where obc_transactions=0 and oac_transactions!=0 and datediff(curdate(), date)<@days;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `facebook_optimization_mx`()
begin

-- Mexico
-- Facebook Optimization Region

set @days:=30;

update facebook.ga_facebook_ads_region_mx set campaign = replace(campaign, '_', '.');

update facebook.ga_facebook_transaction_id_region_mx set campaign = replace(campaign, '_', '.');

delete from facebook.facebook_optimization_region_mx where datediff(curdate(), date)<@days;

insert into facebook.facebook_optimization_region_mx (date, campaign, country, region, city, obc_transactions, visits, gross_revenue, carts) select date, campaign, country, region, city, transactions, visits, transaction_revenue, goal2starts from facebook.ga_facebook_ads_region_mx where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=facebook.week_iso(date) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx f set f.oac_transactions = (select count(distinct z.transaction_id) from production.tbl_order_detail a, facebook.ga_facebook_transaction_id_region_mx z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and a.oac=1) where datediff(curdate(), date)<@days;

create table facebook.temporary_mx as select r.date, r.campaign, r.country, r.region, r.city, (select t.custid from production.tbl_order_detail t where t.order_nr=r.transaction_id and oac=1 group by t.custid) as custid, r.transaction_id, (select sum(t.actual_paid_price) from production.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as actual_paid_price, (select sum(ifnull(t.actual_paid_price_after_tax,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost_per_sku,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) from production.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as PC2, (select sum(new_customers) from production.tbl_order_detail z where z.order_nr=r.transaction_id and z.oac=1 group by z.order_nr) as new_customers from facebook.ga_facebook_transaction_id_region_mx r where datediff(curdate(), date)<@days group by transaction_id;

-- Back Cohort

create table facebook.temporary_back_cohort_mx(
date date,
campaign varchar(255),
country varchar(255),
region varchar(255),
city varchar(255),
custid int,
transaction_id varchar(255),
oac_transactions_30_cohort float,
oac_transactions_60_cohort float,
actual_paid_price_30_cohort float,
actual_paid_price_60_cohort float,
PC2_30_cohort float,
PC2_60_cohort float,
repeated int
);


create index custid on facebook.temporary_back_cohort_mx(custid);

create index all_index on facebook.temporary_back_cohort_mx(date, campaign, country, city);

insert into facebook.temporary_back_cohort_mx (date, campaign, country, region, city, custid, transaction_id) select date, campaign, country, region, city, custid, transaction_id from facebook.temporary_mx where new_customers is not null and datediff(curdate(), date)<@days;

create table facebook.backup_temporary_back_cohort_mx as select * from facebook.temporary_back_cohort_mx;

update facebook.temporary_back_cohort_mx a set repeated = (select count(*) as total from facebook.backup_temporary_back_cohort_mx b where a.date=b.date and a.campaign=b.campaign and a.country=b.country and a.region=b.region and a.city=b.city  group by date, campaign, country, region, city, custid having count(*)>1);

update facebook.temporary_back_cohort_mx a set repeated = 1 where repeated is null;

drop table facebook.backup_temporary_back_cohort_mx;

update facebook.temporary_back_cohort_mx b set oac_transactions_30_cohort = (select count(distinct t.order_nr) as oac_transactions from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_mx b set actual_paid_price_30_cohort = (select sum(t.actual_paid_price) as actual_paid_price from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_mx b set PC2_30_cohort = (select sum(ifnull(t.actual_paid_price_after_tax,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost_per_sku,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_mx b set oac_transactions_60_cohort = (select count(distinct t.order_nr) as oac_transactions from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_mx b set actual_paid_price_60_cohort = (select sum(t.actual_paid_price) as actual_paid_price from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_mx b set PC2_60_cohort = (select sum(ifnull(t.actual_paid_price_after_tax,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost_per_sku,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_mx set oac_transactions_30_cohort=oac_transactions_30_cohort/repeated, actual_paid_price_30_cohort=actual_paid_price_30_cohort/repeated, PC2_30_cohort=PC2_30_cohort/repeated, oac_transactions_60_cohort=oac_transactions_60_cohort/repeated, actual_paid_price_60_cohort=actual_paid_price_60_cohort/repeated, PC2_60_cohort=PC2_60_cohort/repeated where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx f set oac_transactions_30_cohort = (select sum(oac_transactions_30_cohort) from facebook.temporary_back_cohort_mx b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx f set cum_net_rev_30_cohort = (select sum(actual_paid_price_30_cohort) from facebook.temporary_back_cohort_mx b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx f set PC2_30_cohort = (select sum(PC2_30_cohort) from facebook.temporary_back_cohort_mx b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx f set oac_transactions_60_cohort = (select sum(oac_transactions_60_cohort) from facebook.temporary_back_cohort_mx b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx f set cum_net_rev_60_cohort = (select sum(actual_paid_price_60_cohort) from facebook.temporary_back_cohort_mx b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx f set PC2_60_cohort = (select sum(PC2_60_cohort) from facebook.temporary_back_cohort_mx b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

drop table facebook.temporary_back_cohort_mx;

update facebook.facebook_optimization_region_mx set oac_transactions_30_cohort = 0 where oac_transactions_30_cohort is null;

update facebook.facebook_optimization_region_mx set oac_transactions_60_cohort = 0 where oac_transactions_60_cohort is null;

update facebook.facebook_optimization_region_mx set cum_net_rev_30_cohort = 0 where cum_net_rev_30_cohort is null;

update facebook.facebook_optimization_region_mx set cum_net_rev_60_cohort = 0 where cum_net_rev_60_cohort is null;

-- Back Cohort

alter table facebook.temporary_mx add column avg_discount float;

update facebook.temporary_mx r set avg_discount = (select avg(1-(i.unit_price/i.original_unit_price)) from bob_live_mx.sales_order o inner join bob_live_mx.sales_order_item i on i.fk_sales_order = o.id_sales_order where r.transaction_id=o.order_nr group by o.order_nr) where datediff(curdate(), date)<@days; 

create index temporary_mx on facebook.temporary_mx(campaign, date, country, city);

update facebook.facebook_optimization_region_mx f set net_revenue= (select sum(actual_paid_price) from facebook.temporary_mx z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx f set new_customers= (select sum(z.new_customers) from facebook.temporary_mx z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx f set PC2_absolute= (select sum(PC2) from facebook.temporary_mx z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx f set `avg_discount(%)`= (select (avg(avg_discount))*100 from facebook.temporary_mx z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city) where datediff(curdate(), date)<@days;

drop table facebook.temporary_mx;

update facebook.facebook_optimization_region_mx set oac_transactions = 0 where oac_transactions is null;

update facebook.facebook_optimization_region_mx set net_revenue = 0 where net_revenue is null;

update facebook.facebook_optimization_region_mx set new_customers = 0 where new_customers is null;

create table facebook.optimization_mx as select date, campaign, sum(visits)as visits from facebook.facebook_optimization_region_mx group by date, campaign;

create index optimization_mx on facebook.optimization_mx(date, campaign);

create table facebook.cost_campaign_mx as select date, campaign, sum(spent) as spent, sum(clicks) as clicks, sum(social_impressions) as impressions from facebook.facebook_campaign where datediff(curdate(), date)<@days group by date, campaign;

create index cost_campaign_mx on facebook.cost_campaign_mx(date, campaign);

create table facebook.final_mx as select a.date, a.campaign, a.visits, b.spent, b.clicks, b.impressions from facebook.optimization_mx a, facebook.cost_campaign_mx b where a.date=b.date and a.campaign=b.campaign;

drop table facebook.optimization_mx;

drop table facebook.cost_campaign_mx;

create index final_mx on facebook.final_mx(date, campaign);

update facebook.facebook_optimization_region_mx f inner join facebook.final_mx final_mx on f.date=final_mx.date and f.campaign=final_mx.campaign set f.cost=(f.visits/(final_mx.visits))*final_mx.spent, f.clicks=(f.visits/(final_mx.visits))*final_mx.clicks, f.impressions=(f.visits/(final_mx.visits))*final_mx.impressions where datediff(curdate(), f.date)<@days;

drop table facebook.final_mx;

update facebook.facebook_optimization_region_mx set cost = 0 where cost is null;

update facebook.facebook_optimization_region_mx set clicks = 0 where clicks is null;

update facebook.facebook_optimization_region_mx set impressions = 0 where impressions is null;

update facebook.facebook_optimization_region_mx set category = substr(substr(campaign, locate('.',campaign)+1), 1, locate('.', substr(campaign, locate('.',campaign)+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx set gender = substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1) ,1 , locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx set age = substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,1 ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx set segmentation = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) ,1 ,locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx set extra_field = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) , locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))+1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, PC2_absolute=PC2_absolute/xr, cum_net_rev_30_cohort=cum_net_rev_30_cohort/xr, cum_net_rev_60_cohort=cum_net_rev_60_cohort/xr, PC2_30_cohort=PC2_30_cohort/xr, PC2_60_cohort=PC2_60_cohort/xr where r.country='MEX' and datediff(curdate(), date)<@days;

##update facebook.facebook_optimization_region_mx set oac_transactions=0, net_revenue=0, new_customers=0, oac_transactions_30_cohort=0, oac_transactions_60_cohort=0, cum_net_rev_30_cohort=0, cum_net_rev_60_cohort=0 where obc_transactions=0 and datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_mx set obc_transactions=oac_transactions, gross_revenue=net_revenue where obc_transactions=0 and oac_transactions!=0 and datediff(curdate(), date)<@days;


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `facebook_optimization_pe`()
begin

-- Peru
-- Facebook Optimization Region

set @days:=30;

update facebook.ga_facebook_ads_region_pe set campaign = replace(campaign, '_', '.');

update facebook.ga_facebook_transaction_id_region_pe set campaign = replace(campaign, '_', '.');

delete from facebook.facebook_optimization_region_pe where datediff(curdate(), date)<@days;

insert into facebook.facebook_optimization_region_pe (date, campaign, country, region, city, obc_transactions, visits, gross_revenue, carts) select date, campaign, country, region, city, transactions, visits, transaction_revenue, goal2starts from facebook.ga_facebook_ads_region_pe where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=facebook.week_iso(date) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe f set f.oac_transactions = (select count(distinct z.transaction_id) from production_pe.tbl_order_detail a, facebook.ga_facebook_transaction_id_region_pe z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city and a.oac=1) where datediff(curdate(), date)<@days;

create table facebook.temporary_pe as select r.date, r.campaign, r.country, r.region, r.city, (select t.custid from production_pe.tbl_order_detail t where t.order_nr=r.transaction_id and oac=1 group by t.custid) as custid, r.transaction_id, (select sum(t.paid_price) from production_pe.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as paid_price, (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) from production_pe.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as PC2, (select sum(new_customers) from production_pe.tbl_order_detail z where z.order_nr=r.transaction_id and z.oac=1 group by z.order_nr) as new_customers from facebook.ga_facebook_transaction_id_region_pe r where datediff(curdate(), date)<@days group by transaction_id;

-- Back Cohort

create table facebook.temporary_back_cohort_pe(
date date,
campaign varchar(255),
country varchar(255),
region varchar(255),
city varchar(255),
custid int,
transaction_id varchar(255),
oac_transactions_30_cohort float,
oac_transactions_60_cohort float,
paid_price_30_cohort float,
paid_price_60_cohort float,
PC2_30_cohort float,
PC2_60_cohort float,
repeated int
);


create index custid on facebook.temporary_back_cohort_pe(custid);

create index all_index on facebook.temporary_back_cohort_pe(date, campaign, country, city);

insert into facebook.temporary_back_cohort_pe (date, campaign, country, region, city, custid, transaction_id) select date, campaign, country, region, city, custid, transaction_id from facebook.temporary_pe where new_customers is not null and datediff(curdate(), date)<@days;

create table facebook.backup_temporary_back_cohort_pe as select * from facebook.temporary_back_cohort_pe;

update facebook.temporary_back_cohort_pe a set repeated = (select count(*) as total from facebook.backup_temporary_back_cohort_pe b where a.date=b.date and a.campaign=b.campaign and a.country=b.country and a.region=b.region and a.city=b.city  group by date, campaign, country, region, city, custid having count(*)>1) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe a set repeated = 1 where repeated is null;

drop table facebook.backup_temporary_back_cohort_pe;

update facebook.temporary_back_cohort_pe b set oac_transactions_30_cohort = (select count(distinct t.order_nr) as oac_transactions from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe b set paid_price_30_cohort = (select sum(t.paid_price) as paid_price from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe b set PC2_30_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe b set oac_transactions_60_cohort = (select count(distinct t.order_nr) as oac_transactions from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe b set paid_price_60_cohort = (select sum(t.paid_price) as paid_price from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe b set PC2_60_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe set oac_transactions_30_cohort=oac_transactions_30_cohort/repeated, paid_price_30_cohort=paid_price_30_cohort/repeated, PC2_30_cohort=PC2_30_cohort/repeated, oac_transactions_60_cohort=oac_transactions_60_cohort/repeated, paid_price_60_cohort=paid_price_60_cohort/repeated, PC2_60_cohort=PC2_60_cohort/repeated where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe f set oac_transactions_30_cohort = (select sum(oac_transactions_30_cohort) from facebook.temporary_back_cohort_pe b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe f set cum_net_rev_30_cohort = (select sum(paid_price_30_cohort) from facebook.temporary_back_cohort_pe b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe f set PC2_30_cohort = (select sum(PC2_30_cohort) from facebook.temporary_back_cohort_pe b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe f set oac_transactions_60_cohort = (select sum(oac_transactions_60_cohort) from facebook.temporary_back_cohort_pe b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe f set cum_net_rev_60_cohort = (select sum(paid_price_60_cohort) from facebook.temporary_back_cohort_pe b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe f set PC2_60_cohort = (select sum(PC2_60_cohort) from facebook.temporary_back_cohort_pe b where f.date=b.date and f.campaign=b.campaign and f.country=b.country and f.region=b.region and f.city=b.city) where datediff(curdate(), date)<@days;

drop table facebook.temporary_back_cohort_pe;

update facebook.facebook_optimization_region_pe set oac_transactions_30_cohort = 0 where oac_transactions_30_cohort is null;

update facebook.facebook_optimization_region_pe set oac_transactions_60_cohort = 0 where oac_transactions_60_cohort is null;

update facebook.facebook_optimization_region_pe set cum_net_rev_30_cohort = 0 where cum_net_rev_30_cohort is null;

update facebook.facebook_optimization_region_pe set cum_net_rev_60_cohort = 0 where cum_net_rev_60_cohort is null;

-- Back Cohort

alter table facebook.temporary_pe add column avg_discount float;

update facebook.temporary_pe r set avg_discount = (select avg(1-(i.unit_price/i.original_unit_price)) from bob_live_pe.sales_order o inner join bob_live_pe.sales_order_item i on i.fk_sales_order = o.id_sales_order where r.transaction_id=o.order_nr group by o.order_nr) where datediff(curdate(), date)<@days; 

create index temporary_pe on facebook.temporary_pe(campaign, date, country, city);

update facebook.facebook_optimization_region_pe f set net_revenue= (select sum(paid_price) from facebook.temporary_pe z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe f set new_customers= (select sum(z.new_customers) from facebook.temporary_pe z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe f set PC2_absolute= (select sum(PC2) from facebook.temporary_pe z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe f set `avg_discount(%)`= (select (avg(avg_discount))*100 from facebook.temporary_pe z where f.campaign=z.campaign and f.date=z.date and f.country=z.country and f.region=z.region and f.city=z.city) where datediff(curdate(), date)<@days;

drop table facebook.temporary_pe;

update facebook.facebook_optimization_region_pe set oac_transactions = 0 where oac_transactions is null;

update facebook.facebook_optimization_region_pe set net_revenue = 0 where net_revenue is null;

update facebook.facebook_optimization_region_pe set new_customers = 0 where new_customers is null;

create table facebook.optimization_pe as select date, campaign, sum(visits)as visits from facebook.facebook_optimization_region_pe where datediff(curdate(), date)<@days group by date, campaign;

create index optimization_pe on facebook.optimization_pe(date, campaign);

create table facebook.cost_campaign_pe as select date, campaign, sum(spent) as spent, sum(clicks) as clicks, sum(social_impressions) as impressions from facebook.facebook_campaign where datediff(curdate(), date)<@days group by date, campaign;

create index cost_campaign_pe on facebook.cost_campaign_pe(date, campaign);

create table facebook.final_pe as select a.date, a.campaign, a.visits, b.spent, b.clicks, b.impressions from facebook.optimization_pe a, facebook.cost_campaign_pe b where a.date=b.date and a.campaign=b.campaign;

drop table facebook.optimization_pe;

drop table facebook.cost_campaign_pe;

create index final_pe on facebook.final_pe(date, campaign);

update facebook.facebook_optimization_region_pe f inner join facebook.final_pe final_pe on f.date=final_pe.date and f.campaign=final_pe.campaign set f.cost=(f.visits/(final_pe.visits))*final_pe.spent, f.clicks=(f.visits/(final_pe.visits))*final_pe.clicks, f.impressions=(f.visits/(final_pe.visits))*final_pe.impressions where datediff(curdate(), f.date)<@days;

drop table facebook.final_pe;

update facebook.facebook_optimization_region_pe set cost = 0 where cost is null;

update facebook.facebook_optimization_region_pe set clicks = 0 where clicks is null;

update facebook.facebook_optimization_region_pe set impressions = 0 where impressions is null;

update facebook.facebook_optimization_region_pe set category = substr(substr(campaign, locate('.',campaign)+1), 1, locate('.', substr(campaign, locate('.',campaign)+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe set gender = substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1) ,1 , locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe set age = substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,1 ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe set segmentation = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) ,1 ,locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe set extra_field = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) , locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))+1) where datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, PC2_absolute=PC2_absolute/xr, cum_net_rev_30_cohort=cum_net_rev_30_cohort/xr, cum_net_rev_60_cohort=cum_net_rev_60_cohort/xr, PC2_30_cohort=PC2_30_cohort/xr, PC2_60_cohort=PC2_60_cohort/xr where r.country='PER' and datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe set cost=cost/1.23 where date >= '2014-01-01' and datediff(curdate(), date)<@days;

##update facebook.facebook_optimization_region_pe set oac_transactions=0, net_revenue=0, new_customers=0, oac_transactions_30_cohort=0, oac_transactions_60_cohort=0, cum_net_rev_30_cohort=0, cum_net_rev_60_cohort=0 where obc_transactions=0 and datediff(curdate(), date)<@days;

update facebook.facebook_optimization_region_pe set obc_transactions=oac_transactions, gross_revenue=net_revenue where obc_transactions=0 and oac_transactions!=0 and datediff(curdate(), date)<@days;


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `fanpage_optimization`()
begin

select  'Fanpage Optimization Table: start',now();

-- Mexico
-- Fanpage Optimization

set @days:=30;

delete from facebook.facebook_campaign;

insert into facebook.facebook_campaign select * from facebook.facebook_campaign_mx union select * from facebook.facebook_campaign_co union select * from facebook.facebook_campaign_pe union select * from facebook.facebook_campaign_ve union select * from facebook.facebook_campaign_fashion_co;  

update facebook.facebook_campaign set campaign = replace(campaign, '_', '.');

delete from facebook.fanpage_campaign_mx where datediff(curdate(), date)<@days;

delete from facebook.fanpage_transaction_id_mx where datediff(curdate(), date)<@days;

insert into facebook.fanpage_campaign_mx(date, campaign, source, medium, impressions, clicks, visits, ad_cost, bounce, cart) select distinct date, campaign, source, medium, impressions, clicks, visits, ad_cost, bounce, cart from SEM.campaign_ad_group_mx where campaign like '%fanpage%' and (campaign not like '%IMA-%' and campaign not like '%MAI-%');

insert into facebook.fanpage_transaction_id_mx select * from SEM.transaction_id_mx where campaign like '%fanpage%' and (campaign not like '%IMA-%' and campaign not like '%MAI-%');

update facebook.fanpage_campaign_mx set campaign = replace(campaign, '_', '.');

update facebook.fanpage_transaction_id_mx set campaign = replace(campaign, '_', '.');

delete from facebook.fanpage_optimization_mx where datediff(curdate(), date)<@days;

insert into facebook.fanpage_optimization_mx (date, campaign, visits, carts) select date, campaign, visits, cart from facebook.fanpage_campaign_mx where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=facebook.week_iso(date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx f set f.obc_transactions = (select count(distinct z.transaction_id) from production.tbl_order_detail a, facebook.fanpage_transaction_id_mx z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.date=z.date and a.obc=1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx f set f.gross_revenue = (select sum(total_value) from facebook.fanpage_transaction_id_mx z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx f set f.oac_transactions = (select count(distinct z.transaction_id) from production.tbl_order_detail a, facebook.fanpage_transaction_id_mx z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.date=z.date and a.oac=1) where datediff(curdate(), date)<@days;

create table facebook.temporary as select r.date, r.campaign, (select t.custid from production.tbl_order_detail t where t.order_nr=r.transaction_id and oac=1 group by t.custid) as custid, r.transaction_id, (select sum(t.actual_paid_price) from production.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as actual_paid_price, (select sum(ifnull(t.actual_paid_price_after_tax,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost_per_sku,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) from production.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as PC2, (select sum(new_customers) from production.tbl_order_detail z where z.order_nr=r.transaction_id and z.oac=1 group by z.order_nr) as new_customers from facebook.fanpage_transaction_id_mx r where datediff(curdate(), date)<@days group by transaction_id;

-- Back Cohort

create table facebook.temporary_back_cohort(
date date,
campaign varchar(255),
custid int,
transaction_id varchar(255),
oac_transactions_30_cohort float,
oac_transactions_60_cohort float,
actual_paid_price_30_cohort float,
actual_paid_price_60_cohort float,
PC2_30_cohort float,
PC2_60_cohort float,
repeated int
);


create index custid on facebook.temporary_back_cohort(custid);

create index all_index on facebook.temporary_back_cohort(date, campaign);

insert into facebook.temporary_back_cohort (date, campaign, custid, transaction_id) select date, campaign, custid, transaction_id from facebook.temporary where new_customers is not null and datediff(curdate(), date)<@days;

create table facebook.backup_temporary_back_cohort as select * from facebook.temporary_back_cohort;

update facebook.temporary_back_cohort a set repeated = (select count(*) as total from facebook.backup_temporary_back_cohort b where a.date=b.date and a.campaign=b.campaign group by date, campaign, custid having count(*)>1);

update facebook.temporary_back_cohort a set repeated = 1 where repeated is null;

drop table facebook.backup_temporary_back_cohort;

update facebook.temporary_back_cohort b set oac_transactions_30_cohort = (select count(distinct t.order_nr) as oac_transactions from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set actual_paid_price_30_cohort = (select sum(t.actual_paid_price) as actual_paid_price from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set PC2_30_cohort = (select sum(ifnull(t.actual_paid_price_after_tax,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost_per_sku,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set oac_transactions_60_cohort = (select count(distinct t.order_nr) as oac_transactions from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set actual_paid_price_60_cohort = (select sum(t.actual_paid_price) as actual_paid_price from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set PC2_60_cohort = (select sum(ifnull(t.actual_paid_price_after_tax,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost_per_sku,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort set oac_transactions_30_cohort=oac_transactions_30_cohort/repeated, actual_paid_price_30_cohort=actual_paid_price_30_cohort/repeated, PC2_30_cohort=PC2_30_cohort/repeated, oac_transactions_60_cohort=oac_transactions_60_cohort/repeated, actual_paid_price_60_cohort=actual_paid_price_60_cohort/repeated, PC2_60_cohort=PC2_60_cohort/repeated where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx f set oac_transactions_30_cohort = (select sum(oac_transactions_30_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx f set cum_net_rev_30_cohort = (select sum(actual_paid_price_30_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx f set PC2_30_cohort = (select sum(PC2_30_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign);

update facebook.fanpage_optimization_mx f set oac_transactions_60_cohort = (select sum(oac_transactions_60_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx f set cum_net_rev_60_cohort = (select sum(actual_paid_price_60_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx f set PC2_60_cohort = (select sum(PC2_60_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

drop table facebook.temporary_back_cohort;

update facebook.fanpage_optimization_mx set oac_transactions_30_cohort = 0 where oac_transactions_30_cohort is null;

update facebook.fanpage_optimization_mx set oac_transactions_60_cohort = 0 where oac_transactions_60_cohort is null;

update facebook.fanpage_optimization_mx set cum_net_rev_30_cohort = 0 where cum_net_rev_30_cohort is null;

update facebook.fanpage_optimization_mx set cum_net_rev_60_cohort = 0 where cum_net_rev_60_cohort is null;

-- Back Cohort

alter table facebook.temporary add column avg_discount float;

update facebook.temporary r set avg_discount = (select avg(1-(i.unit_price/i.original_unit_price)) from bob_live_mx.sales_order o inner join bob_live_mx.sales_order_item i on i.fk_sales_order = o.id_sales_order where r.transaction_id=o.order_nr group by o.order_nr) where datediff(curdate(), date)<@days; 

create index temporary on facebook.temporary(campaign, date);

update facebook.fanpage_optimization_mx f set net_revenue= (select sum(actual_paid_price) from facebook.temporary z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx f set new_customers= (select sum(z.new_customers) from facebook.temporary z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx f set PC2_absolute= (select sum(PC2) from facebook.temporary z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx f set `avg_discount(%)`= (select (avg(avg_discount))*100 from facebook.temporary z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

drop table facebook.temporary;

update facebook.fanpage_optimization_mx set oac_transactions = 0 where oac_transactions is null;

update facebook.fanpage_optimization_mx set net_revenue = 0 where net_revenue is null;

update facebook.fanpage_optimization_mx set new_customers = 0 where new_customers is null;

create table facebook.optimization as select date, campaign, sum(visits)as visits from facebook.fanpage_optimization_mx where datediff(curdate(), date)<@days group by date, campaign;

create index optimization on facebook.optimization(date, campaign);

create table facebook.cost_campaign as select date, campaign, sum(spent) as spent, sum(clicks) as clicks, sum(social_impressions) as impressions from facebook.facebook_campaign where datediff(curdate(), date)<@days group by date, campaign;

create index cost_campaign on facebook.cost_campaign(date, campaign);

create table facebook.final as select a.date, a.campaign, a.visits, b.spent, b.clicks, b.impressions from facebook.optimization a, facebook.cost_campaign b where a.date=b.date and a.campaign=b.campaign;

drop table facebook.optimization;

drop table facebook.cost_campaign;

create index final on facebook.final(date, campaign);

update facebook.fanpage_optimization_mx f inner join facebook.final final on f.date=final.date and f.campaign=final.campaign set f.cost=(f.visits/(final.visits))*final.spent, f.clicks=(f.visits/(final.visits))*final.clicks, f.impressions=(f.visits/(final.visits))*final.impressions where datediff(curdate(), f.date)<@days;

drop table facebook.final;

update facebook.fanpage_optimization_mx set cost = 0 where cost is null;

update facebook.fanpage_optimization_mx set clicks = 0 where clicks is null;

update facebook.fanpage_optimization_mx set impressions = 0 where impressions is null;

update facebook.fanpage_optimization_mx set category = substr(substr(campaign, locate('.',campaign)+1), 1, locate('.', substr(campaign, locate('.',campaign)+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx set gender = substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1) ,1 , locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx set age = substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,1 ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx set segmentation = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) ,1 ,locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx set extra_field = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) , locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))+1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, PC2_absolute=PC2_absolute/xr, cum_net_rev_30_cohort=cum_net_rev_30_cohort/xr, cum_net_rev_60_cohort=cum_net_rev_60_cohort/xr, PC2_30_cohort=PC2_30_cohort/xr, PC2_60_cohort=PC2_60_cohort/xr where r.country='MEX' and datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx set oac_transactions=0, gross_revenue=0, net_revenue=0, new_customers=0, oac_transactions_30_cohort=0, oac_transactions_60_cohort=0, cum_net_rev_30_cohort=0, cum_net_rev_60_cohort=0 where obc_transactions=0 and datediff(curdate(), date)<@days;

-- Colombia
-- Fanpage Optimization

delete from facebook.fanpage_campaign_co where datediff(curdate(), date)<@days;

delete from facebook.fanpage_transaction_id_co where datediff(curdate(), date)<@days;

insert into facebook.fanpage_campaign_co(date, campaign, source, medium, impressions, clicks, visits, ad_cost, bounce, cart) select distinct date, campaign, source, medium, impressions, clicks, visits, ad_cost, bounce, cart from SEM.campaign_ad_group_co where campaign like '%fanpage%' and (campaign not like '%IMA-%' and campaign not like '%MAI-%');

insert into facebook.fanpage_transaction_id_co select * from SEM.transaction_id_co where campaign like '%fanpage%' and (campaign not like '%IMA-%' and campaign not like '%MAI-%');

update facebook.fanpage_campaign_co set campaign = replace(campaign, '_', '.');

update facebook.fanpage_transaction_id_co set campaign = replace(campaign, '_', '.');

delete from facebook.fanpage_optimization_co where datediff(curdate(), date)<@days;

insert into facebook.fanpage_optimization_co (date, campaign, visits, carts) select date, campaign, visits, cart from facebook.fanpage_campaign_co where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=facebook.week_iso(date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set f.obc_transactions = (select count(distinct z.transaction_id) from production_co.tbl_order_detail a, facebook.fanpage_transaction_id_co z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.date=z.date and a.obc=1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set f.gross_revenue = (select sum(total_value) from facebook.fanpage_transaction_id_co z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set f.oac_transactions = (select count(distinct z.transaction_id) from production_co.tbl_order_detail a, facebook.fanpage_transaction_id_co z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.date=z.date and a.oac=1) where datediff(curdate(), date)<@days;

create table facebook.temporary as select r.date, r.campaign, (select t.custid from production_co.tbl_order_detail t where t.order_nr=r.transaction_id and oac=1 group by t.custid) as custid, r.transaction_id, (select sum(t.paid_price) from production_co.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as paid_price, (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) from production_co.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as PC2, (select sum(new_customers) from production_co.tbl_order_detail z where z.order_nr=r.transaction_id and z.oac=1 group by z.order_nr) as new_customers from facebook.fanpage_transaction_id_co r where datediff(curdate(), date)<@days group by transaction_id;

-- Back Cohort

create table facebook.temporary_back_cohort(
date date,
campaign varchar(255),
custid int,
transaction_id varchar(255),
oac_transactions_30_cohort float,
oac_transactions_60_cohort float,
paid_price_30_cohort float,
paid_price_60_cohort float,
PC2_30_cohort float,
PC2_60_cohort float,
repeated int
);


create index custid on facebook.temporary_back_cohort(custid);

create index all_index on facebook.temporary_back_cohort(date, campaign);

insert into facebook.temporary_back_cohort (date, campaign, custid, transaction_id) select date, campaign, custid, transaction_id from facebook.temporary where new_customers !=0 and datediff(curdate(), date)<@days;

create table facebook.backup_temporary_back_cohort as select * from facebook.temporary_back_cohort;

update facebook.temporary_back_cohort a set repeated = (select count(*) as total from facebook.backup_temporary_back_cohort b where a.date=b.date and a.campaign=b.campaign group by date, campaign, custid having count(*)>1);

update facebook.temporary_back_cohort a set repeated = 1 where repeated is null;

drop table facebook.backup_temporary_back_cohort;

update facebook.temporary_back_cohort b set oac_transactions_30_cohort = (select count(distinct t.order_nr) as oac_transactions from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers =0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set paid_price_30_cohort = (select sum(t.paid_price) as paid_price from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers =0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set PC2_30_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers =0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set oac_transactions_60_cohort = (select count(distinct t.order_nr) as oac_transactions from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers =0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set paid_price_60_cohort = (select sum(t.paid_price) as paid_price from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers =0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set PC2_60_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers =0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort set oac_transactions_30_cohort=oac_transactions_30_cohort/repeated, paid_price_30_cohort=paid_price_30_cohort/repeated, PC2_30_cohort=PC2_30_cohort/repeated, oac_transactions_60_cohort=oac_transactions_60_cohort/repeated, paid_price_60_cohort=paid_price_60_cohort/repeated, PC2_60_cohort=PC2_60_cohort/repeated where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set oac_transactions_30_cohort = (select sum(oac_transactions_30_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set cum_net_rev_30_cohort = (select sum(paid_price_30_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set PC2_30_cohort = (select sum(PC2_30_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set oac_transactions_60_cohort = (select sum(oac_transactions_60_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set cum_net_rev_60_cohort = (select sum(paid_price_60_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set PC2_60_cohort = (select sum(PC2_60_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

drop table facebook.temporary_back_cohort;

update facebook.fanpage_optimization_co set oac_transactions_30_cohort = 0 where oac_transactions_30_cohort is null;

update facebook.fanpage_optimization_co set oac_transactions_60_cohort = 0 where oac_transactions_60_cohort is null;

update facebook.fanpage_optimization_co set cum_net_rev_30_cohort = 0 where cum_net_rev_30_cohort is null;

update facebook.fanpage_optimization_co set cum_net_rev_60_cohort = 0 where cum_net_rev_60_cohort is null;

-- Back Cohort

alter table facebook.temporary add column avg_discount float;

update facebook.temporary r set avg_discount = (select avg(1-(i.unit_price/i.original_unit_price)) from bob_live_co.sales_order o inner join bob_live_co.sales_order_item i on i.fk_sales_order = o.id_sales_order where r.transaction_id=o.order_nr group by o.order_nr) where datediff(curdate(), date)<@days; 

create index temporary on facebook.temporary(campaign, date);

update facebook.fanpage_optimization_co f set net_revenue= (select sum(paid_price) from facebook.temporary z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set new_customers= (select sum(z.new_customers) from facebook.temporary z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set PC2_absolute= (select sum(PC2) from facebook.temporary z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set `avg_discount(%)`= (select (avg(avg_discount))*100 from facebook.temporary z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

drop table facebook.temporary;

update facebook.fanpage_optimization_co set oac_transactions = 0 where oac_transactions is null;

update facebook.fanpage_optimization_co set net_revenue = 0 where net_revenue is null;

update facebook.fanpage_optimization_co set new_customers = 0 where new_customers is null;

create table facebook.optimization as select date, campaign, sum(visits)as visits from facebook.fanpage_optimization_co where datediff(curdate(), date)<@days group by date, campaign;

create index optimization on facebook.optimization(date, campaign);

create table facebook.cost_campaign as select date, campaign, sum(spent) as spent, sum(clicks) as clicks, sum(social_impressions) as impressions from facebook.facebook_campaign where datediff(curdate(), date)<@days group by date, campaign;

create index cost_campaign on facebook.cost_campaign(date, campaign);

create table facebook.final as select a.date, a.campaign, a.visits, b.spent, b.clicks, b.impressions from facebook.optimization a, facebook.cost_campaign b where a.date=b.date and a.campaign=b.campaign;

drop table facebook.optimization;

drop table facebook.cost_campaign;

create index final on facebook.final(date, campaign);

update facebook.fanpage_optimization_co f inner join facebook.final final on f.date=final.date and f.campaign=final.campaign set f.cost=(f.visits/(final.visits))*final.spent, f.clicks=(f.visits/(final.visits))*final.clicks, f.impressions=(f.visits/(final.visits))*final.impressions where datediff(curdate(), f.date)<@days;

drop table facebook.final;

update facebook.fanpage_optimization_co set cost = 0 where cost is null;

update facebook.fanpage_optimization_co set clicks = 0 where clicks is null;

update facebook.fanpage_optimization_co set impressions = 0 where impressions is null;

update facebook.fanpage_optimization_co set category = substr(substr(campaign, locate('.',campaign)+1), 1, locate('.', substr(campaign, locate('.',campaign)+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co set gender = substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1) ,1 , locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))-1) ;

update facebook.fanpage_optimization_co set age = substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,1 ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co set segmentation = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) ,1 ,locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co set extra_field = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) , locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))+1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, PC2_absolute=PC2_absolute/xr, cum_net_rev_30_cohort=cum_net_rev_30_cohort/xr, cum_net_rev_60_cohort=cum_net_rev_60_cohort/xr, PC2_30_cohort=PC2_30_cohort/xr, PC2_60_cohort=PC2_60_cohort/xr where r.country='COL' and datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co set cost=cost/1.23 where date >= '2013-05-01' and datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co set oac_transactions=0, gross_revenue=0, net_revenue=0, new_customers=0, oac_transactions_30_cohort=0, oac_transactions_60_cohort=0, cum_net_rev_30_cohort=0, cum_net_rev_60_cohort=0 where obc_transactions=0 and datediff(curdate(), date)<@days;

-- Peru
-- Fanpage Optimization

delete from facebook.fanpage_campaign_pe where datediff(curdate(), date)<@days;

delete from facebook.fanpage_transaction_id_pe where datediff(curdate(), date)<@days;

insert into facebook.fanpage_campaign_pe(date, campaign, source, medium, impressions, clicks, visits, ad_cost, bounce, cart) select distinct date, campaign, source, medium, impressions, clicks, visits, ad_cost, bounce, cart from SEM.campaign_ad_group_pe where campaign like '%fanpage%' and (campaign not like '%IMA-%' and campaign not like '%MAI-%');

insert into facebook.fanpage_transaction_id_pe select * from SEM.transaction_id_pe where campaign like '%fanpage%' and (campaign not like '%IMA-%' and campaign not like '%MAI-%');

update facebook.fanpage_campaign_pe set campaign = replace(campaign, '_', '.');

update facebook.fanpage_transaction_id_pe set campaign = replace(campaign, '_', '.');

delete from facebook.fanpage_optimization_pe where datediff(curdate(), date)<@days;

insert into facebook.fanpage_optimization_pe (date, campaign, visits, carts) select date, campaign, visits, cart from facebook.fanpage_campaign_pe where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=facebook.week_iso(date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe f set f.obc_transactions = (select count(distinct z.transaction_id) from production_pe.tbl_order_detail a, facebook.fanpage_transaction_id_pe z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.date=z.date and a.obc=1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe f set f.gross_revenue = (select sum(total_value) from facebook.fanpage_transaction_id_pe z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe f set f.oac_transactions = (select count(distinct z.transaction_id) from production_pe.tbl_order_detail a, facebook.fanpage_transaction_id_pe z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.date=z.date and a.oac=1) where datediff(curdate(), date)<@days;

create table facebook.temporary as select r.date, r.campaign, (select t.custid from production_pe.tbl_order_detail t where t.order_nr=r.transaction_id and oac=1 group by t.custid) as custid, r.transaction_id, (select sum(t.paid_price) from production_pe.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as paid_price, (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) from production_pe.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as PC2, (select sum(new_customers) from production_pe.tbl_order_detail z where z.order_nr=r.transaction_id and z.oac=1 group by z.order_nr) as new_customers from facebook.fanpage_transaction_id_pe r where datediff(curdate(), date)<@days group by transaction_id;

-- Back Cohort

create table facebook.temporary_back_cohort(
date date,
campaign varchar(255),
custid int,
transaction_id varchar(255),
oac_transactions_30_cohort float,
oac_transactions_60_cohort float,
paid_price_30_cohort float,
paid_price_60_cohort float,
PC2_30_cohort float,
PC2_60_cohort float,
repeated int
);


create index custid on facebook.temporary_back_cohort(custid);

create index all_index on facebook.temporary_back_cohort(date, campaign);

insert into facebook.temporary_back_cohort (date, campaign, custid, transaction_id) select date, campaign, custid, transaction_id from facebook.temporary where new_customers is not null;

create table facebook.backup_temporary_back_cohort as select * from facebook.temporary_back_cohort;

update facebook.temporary_back_cohort a set repeated = (select count(*) as total from facebook.backup_temporary_back_cohort b where a.date=b.date and a.campaign=b.campaign group by date, campaign, custid having count(*)>1) and datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort a set repeated = 1 where repeated is null;

drop table facebook.backup_temporary_back_cohort;

update facebook.temporary_back_cohort b set oac_transactions_30_cohort = (select count(distinct t.order_nr) as oac_transactions from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set paid_price_30_cohort = (select sum(t.paid_price) as paid_price from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set PC2_30_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set oac_transactions_60_cohort = (select count(distinct t.order_nr) as oac_transactions from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set paid_price_60_cohort = (select sum(t.paid_price) as paid_price from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set PC2_60_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort set oac_transactions_30_cohort=oac_transactions_30_cohort/repeated, paid_price_30_cohort=paid_price_30_cohort/repeated, PC2_30_cohort=PC2_30_cohort/repeated, oac_transactions_60_cohort=oac_transactions_60_cohort/repeated, paid_price_60_cohort=paid_price_60_cohort/repeated, PC2_60_cohort=PC2_60_cohort/repeated where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe f set oac_transactions_30_cohort = (select sum(oac_transactions_30_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe f set cum_net_rev_30_cohort = (select sum(paid_price_30_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe f set PC2_30_cohort = (select sum(PC2_30_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe f set oac_transactions_60_cohort = (select sum(oac_transactions_60_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe f set cum_net_rev_60_cohort = (select sum(paid_price_60_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe f set PC2_60_cohort = (select sum(PC2_60_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

drop table facebook.temporary_back_cohort;

update facebook.fanpage_optimization_pe set oac_transactions_30_cohort = 0 where oac_transactions_30_cohort is null;

update facebook.fanpage_optimization_pe set oac_transactions_60_cohort = 0 where oac_transactions_60_cohort is null;

update facebook.fanpage_optimization_pe set cum_net_rev_30_cohort = 0 where cum_net_rev_30_cohort is null;

update facebook.fanpage_optimization_pe set cum_net_rev_60_cohort = 0 where cum_net_rev_60_cohort is null;

-- Back Cohort

alter table facebook.temporary add column avg_discount float;

update facebook.temporary r set avg_discount = (select avg(1-(i.unit_price/i.original_unit_price)) from bob_live_pe.sales_order o inner join bob_live_pe.sales_order_item i on i.fk_sales_order = o.id_sales_order where r.transaction_id=o.order_nr group by o.order_nr) where datediff(curdate(), date)<@days; 

create index temporary on facebook.temporary(campaign, date);

update facebook.fanpage_optimization_pe f set net_revenue= (select sum(paid_price) from facebook.temporary z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe f set new_customers= (select sum(z.new_customers) from facebook.temporary z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe f set PC2_absolute= (select sum(PC2) from facebook.temporary z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe f set `avg_discount(%)`= (select (avg(avg_discount))*100 from facebook.temporary z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

drop table facebook.temporary;

update facebook.fanpage_optimization_pe set oac_transactions = 0 where oac_transactions is null;

update facebook.fanpage_optimization_pe set net_revenue = 0 where net_revenue is null;

update facebook.fanpage_optimization_pe set new_customers = 0 where new_customers is null;

create table facebook.optimization as select date, campaign, sum(visits)as visits from facebook.fanpage_optimization_pe where datediff(curdate(), date)<@days group by date, campaign;

create index optimization on facebook.optimization(date, campaign);

create table facebook.cost_campaign as select date, campaign, sum(spent) as spent, sum(clicks) as clicks, sum(social_impressions) as impressions from facebook.facebook_campaign where datediff(curdate(), date)<@days group by date, campaign;

create index cost_campaign on facebook.cost_campaign(date, campaign);

create table facebook.final as select a.date, a.campaign, a.visits, b.spent, b.clicks, b.impressions from facebook.optimization a, facebook.cost_campaign b where a.date=b.date and a.campaign=b.campaign;

drop table facebook.optimization;

drop table facebook.cost_campaign;

create index final on facebook.final(date, campaign);

update facebook.fanpage_optimization_pe f inner join facebook.final final on f.date=final.date and f.campaign=final.campaign set f.cost=(f.visits/(final.visits))*final.spent, f.clicks=(f.visits/(final.visits))*final.clicks, f.impressions=(f.visits/(final.visits))*final.impressions where datediff(curdate(), f.date)<@days;

drop table facebook.final;

update facebook.fanpage_optimization_pe set cost = 0 where cost is null;

update facebook.fanpage_optimization_pe set clicks = 0 where clicks is null;

update facebook.fanpage_optimization_pe set impressions = 0 where impressions is null;

update facebook.fanpage_optimization_pe set category = substr(substr(campaign, locate('.',campaign)+1), 1, locate('.', substr(campaign, locate('.',campaign)+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe set gender = substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1) ,1 , locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe set age = substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,1 ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe set segmentation = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) ,1 ,locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe set extra_field = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) , locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))+1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, PC2_absolute=PC2_absolute/xr, cum_net_rev_30_cohort=cum_net_rev_30_cohort/xr, cum_net_rev_60_cohort=cum_net_rev_60_cohort/xr, PC2_30_cohort=PC2_30_cohort/xr, PC2_60_cohort=PC2_60_cohort/xr where r.country='PER' and datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe set oac_transactions=0, gross_revenue=0, net_revenue=0, new_customers=0, oac_transactions_30_cohort=0, oac_transactions_60_cohort=0, cum_net_rev_30_cohort=0, cum_net_rev_60_cohort=0 where obc_transactions=0 and datediff(curdate(), date)<@days;

-- Venezuela
-- Fanpage Optimization

delete from facebook.fanpage_campaign_ve where datediff(curdate(), date)<@days;

delete from facebook.fanpage_transaction_id_ve where datediff(curdate(), date)<@days;

insert into facebook.fanpage_campaign_ve(date, campaign, source, medium, impressions, clicks, visits, ad_cost, bounce, cart) select distinct date, campaign, source, medium, impressions, clicks, visits, ad_cost, bounce, cart from SEM.campaign_ad_group_ve where campaign like '%fanpage%' and (campaign not like '%IMA-%' and campaign not like '%MAI-%');

insert into facebook.fanpage_transaction_id_ve select * from SEM.transaction_id_ve where campaign like '%fanpage%' and (campaign not like '%IMA-%' and campaign not like '%MAI-%');

update facebook.fanpage_campaign_ve set campaign = replace(campaign, '_', '.');

update facebook.fanpage_transaction_id_ve set campaign = replace(campaign, '_', '.');

delete from facebook.fanpage_optimization_ve where datediff(curdate(), date)<@days;

insert into facebook.fanpage_optimization_ve (date, campaign, visits, carts) select date, campaign, visits, cart from facebook.fanpage_campaign_ve where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=facebook.week_iso(date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve f set f.obc_transactions = (select count(distinct z.transaction_id) from production_ve.tbl_order_detail a, facebook.fanpage_transaction_id_ve z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.date=z.date and a.obc=1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve f set f.gross_revenue = (select sum(total_value) from facebook.fanpage_transaction_id_ve z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve f set f.oac_transactions = (select count(distinct z.transaction_id) from production_ve.tbl_order_detail a, facebook.fanpage_transaction_id_ve z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.date=z.date and a.oac=1) where datediff(curdate(), date)<@days;

create table facebook.temporary as select r.date, r.campaign, (select t.custid from production_ve.tbl_order_detail t where t.order_nr=r.transaction_id and oac=1 group by t.custid) as custid, r.transaction_id, (select sum(t.paid_price) from production_ve.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as paid_price, (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) from production_ve.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as PC2, (select sum(new_customers) from production_ve.tbl_order_detail z where z.order_nr=r.transaction_id and z.oac=1 group by z.order_nr) as new_customers from facebook.fanpage_transaction_id_ve r where datediff(curdate(), date)<@days group by transaction_id;

-- Back Cohort

create table facebook.temporary_back_cohort(
date date,
campaign varchar(255),
custid int,
transaction_id varchar(255),
oac_transactions_30_cohort float,
oac_transactions_60_cohort float,
paid_price_30_cohort float,
paid_price_60_cohort float,
PC2_30_cohort float,
PC2_60_cohort float,
repeated int
);


create index custid on facebook.temporary_back_cohort(custid);

create index all_index on facebook.temporary_back_cohort(date, campaign);

insert into facebook.temporary_back_cohort (date, campaign, custid, transaction_id) select date, campaign, custid, transaction_id from facebook.temporary where new_customers is not null and datediff(curdate(), date)<@days;

create table facebook.backup_temporary_back_cohort as select * from facebook.temporary_back_cohort;

update facebook.temporary_back_cohort a set repeated = (select count(*) as total from facebook.backup_temporary_back_cohort b where a.date=b.date and a.campaign=b.campaign group by date, campaign, custid having count(*)>1);

update facebook.temporary_back_cohort a set repeated = 1 where repeated is null;

drop table facebook.backup_temporary_back_cohort;

update facebook.temporary_back_cohort b set oac_transactions_30_cohort = (select count(distinct t.order_nr) as oac_transactions from production_ve.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set paid_price_30_cohort = (select sum(t.paid_price) as paid_price from production_ve.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set PC2_30_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_ve.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set oac_transactions_60_cohort = (select count(distinct t.order_nr) as oac_transactions from production_ve.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set paid_price_60_cohort = (select sum(t.paid_price) as paid_price from production_ve.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort b set PC2_60_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_ve.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort set oac_transactions_30_cohort=oac_transactions_30_cohort/repeated, paid_price_30_cohort=paid_price_30_cohort/repeated, PC2_30_cohort=PC2_30_cohort/repeated, oac_transactions_60_cohort=oac_transactions_60_cohort/repeated, paid_price_60_cohort=paid_price_60_cohort/repeated, PC2_60_cohort=PC2_60_cohort/repeated where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve f set oac_transactions_30_cohort = (select sum(oac_transactions_30_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve f set cum_net_rev_30_cohort = (select sum(paid_price_30_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve f set PC2_30_cohort = (select sum(PC2_30_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve f set oac_transactions_60_cohort = (select sum(oac_transactions_60_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve f set cum_net_rev_60_cohort = (select sum(paid_price_60_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve f set PC2_60_cohort = (select sum(PC2_60_cohort) from facebook.temporary_back_cohort b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

drop table facebook.temporary_back_cohort;

update facebook.fanpage_optimization_ve set oac_transactions_30_cohort = 0 where oac_transactions_30_cohort is null;

update facebook.fanpage_optimization_ve set oac_transactions_60_cohort = 0 where oac_transactions_60_cohort is null;

update facebook.fanpage_optimization_ve set cum_net_rev_30_cohort = 0 where cum_net_rev_30_cohort is null;

update facebook.fanpage_optimization_ve set cum_net_rev_60_cohort = 0 where cum_net_rev_60_cohort is null;

-- Back Cohort

alter table facebook.temporary add column avg_discount float;

update facebook.temporary r set avg_discount = (select avg(1-(i.unit_price/i.original_unit_price)) from bob_live_ve.sales_order o inner join bob_live_ve.sales_order_item i on i.fk_sales_order = o.id_sales_order where r.transaction_id=o.order_nr group by o.order_nr) where datediff(curdate(), date)<@days; 

create index temporary on facebook.temporary(campaign, date);

update facebook.fanpage_optimization_ve f set net_revenue= (select sum(paid_price) from facebook.temporary z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve f set new_customers= (select sum(z.new_customers) from facebook.temporary z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve f set PC2_absolute= (select sum(PC2) from facebook.temporary z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve f set `avg_discount(%)`= (select (avg(avg_discount))*100 from facebook.temporary z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

drop table facebook.temporary;

update facebook.fanpage_optimization_ve set oac_transactions = 0 where oac_transactions is null;

update facebook.fanpage_optimization_ve set net_revenue = 0 where net_revenue is null;

update facebook.fanpage_optimization_ve set new_customers = 0 where new_customers is null;

create table facebook.optimization as select date, campaign, sum(visits)as visits from facebook.fanpage_optimization_ve where datediff(curdate(), date)<@days group by date, campaign;

create index optimization on facebook.optimization(date, campaign);

create table facebook.cost_campaign as select date, campaign, sum(spent) as spent, sum(clicks) as clicks, sum(social_impressions) as impressions from facebook.facebook_campaign where datediff(curdate(), date)<@days group by date, campaign;

create index cost_campaign on facebook.cost_campaign(date, campaign);

create table facebook.final as select a.date, a.campaign, a.visits, b.spent, b.clicks, b.impressions from facebook.optimization a, facebook.cost_campaign b where a.date=b.date and a.campaign=b.campaign;

drop table facebook.optimization;

drop table facebook.cost_campaign;

create index final on facebook.final(date, campaign);

update facebook.fanpage_optimization_ve f inner join facebook.final final on f.date=final.date and f.campaign=final.campaign set f.cost=(f.visits/(final.visits))*final.spent, f.clicks=(f.visits/(final.visits))*final.clicks, f.impressions=(f.visits/(final.visits))*final.impressions where datediff(curdate(), f.date)<@days;

drop table facebook.final;

update facebook.fanpage_optimization_ve set cost = 0 where cost is null;

update facebook.fanpage_optimization_ve set clicks = 0 where clicks is null;

update facebook.fanpage_optimization_ve set impressions = 0 where impressions is null;

update facebook.fanpage_optimization_ve set category = substr(substr(campaign, locate('.',campaign)+1), 1, locate('.', substr(campaign, locate('.',campaign)+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve set gender = substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1) ,1 , locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve set age = substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,1 ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve set segmentation = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) ,1 ,locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve set extra_field = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) , locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))+1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, PC2_absolute=PC2_absolute/xr, cum_net_rev_30_cohort=cum_net_rev_30_cohort/xr, cum_net_rev_60_cohort=cum_net_rev_60_cohort/xr, PC2_30_cohort=PC2_30_cohort/xr, PC2_60_cohort=PC2_60_cohort/xr where r.country='VEN' and datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve set oac_transactions=0, gross_revenue=0, net_revenue=0, new_customers=0, oac_transactions_30_cohort=0, oac_transactions_60_cohort=0, cum_net_rev_30_cohort=0, cum_net_rev_60_cohort=0 where obc_transactions=0 and datediff(curdate(), date)<@days;

select  'Fanpage Optimization Table: end',now();

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `fanpage_optimization_co`()
begin

-- Colombia
-- Fanpage Optimization

set @days:=30;

delete from facebook.fanpage_campaign_co where datediff(curdate(), date)<@days;

delete from facebook.fanpage_transaction_id_co where datediff(curdate(), date)<@days;

insert into facebook.fanpage_campaign_co(date, campaign, source, medium, impressions, clicks, visits, ad_cost, bounce, cart) select distinct date, campaign, source, medium, impressions, clicks, visits, ad_cost, bounce, cart from SEM.campaign_ad_group_co where campaign like '%fanpage%' and campaign like '%!_WCA%' escape '!'
 and (campaign not like '%IMA-%' and campaign not like '%MAI-%');

insert into facebook.fanpage_transaction_id_co select * from SEM.transaction_id_co where campaign like '%fanpage%' and campaign like '%!_WCA%' escape '!'
and (campaign not like '%IMA-%' and campaign not like '%MAI-%');

update facebook.fanpage_campaign_co set campaign = replace(campaign, '_', '.');

update facebook.fanpage_transaction_id_co set campaign = replace(campaign, '_', '.');

delete from facebook.fanpage_optimization_co where datediff(curdate(), date)<@days;

insert into facebook.fanpage_optimization_co (date, campaign, visits, carts) select date, campaign, visits, cart from facebook.fanpage_campaign_co where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=facebook.week_iso(date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set f.obc_transactions = (select count(distinct z.transaction_id) from production_co.tbl_order_detail a, facebook.fanpage_transaction_id_co z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.date=z.date and a.obc=1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set f.gross_revenue = (select sum(total_value) from facebook.fanpage_transaction_id_co z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set f.oac_transactions = (select count(distinct z.transaction_id) from production_co.tbl_order_detail a, facebook.fanpage_transaction_id_co z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.date=z.date and a.oac=1) where datediff(curdate(), date)<@days;

create table facebook.temporary_co as select r.date, r.campaign, (select t.custid from production_co.tbl_order_detail t where t.order_nr=r.transaction_id and oac=1 group by t.custid) as custid, r.transaction_id, (select sum(t.paid_price) from production_co.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as paid_price, (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) from production_co.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as PC2, (select sum(new_customers) from production_co.tbl_order_detail z where z.order_nr=r.transaction_id and z.oac=1 group by z.order_nr) as new_customers from facebook.fanpage_transaction_id_co r where datediff(curdate(), date)<@days group by transaction_id;

-- Back Cohort

create table facebook.temporary_back_cohort_co(
date date,
campaign varchar(255),
custid int,
transaction_id varchar(255),
oac_transactions_30_cohort float,
oac_transactions_60_cohort float,
paid_price_30_cohort float,
paid_price_60_cohort float,
PC2_30_cohort float,
PC2_60_cohort float,
repeated int
);


create index custid on facebook.temporary_back_cohort_co(custid);

create index all_index on facebook.temporary_back_cohort_co(date, campaign);

insert into facebook.temporary_back_cohort_co (date, campaign, custid, transaction_id) select date, campaign, custid, transaction_id from facebook.temporary_co where new_customers !=0 and datediff(curdate(), date)<@days;

create table facebook.backup_temporary_back_cohort_co as select * from facebook.temporary_back_cohort_co;

update facebook.temporary_back_cohort_co a set repeated = (select count(*) as total from facebook.backup_temporary_back_cohort_co b where a.date=b.date and a.campaign=b.campaign group by date, campaign, custid having count(*)>1);

update facebook.temporary_back_cohort_co a set repeated = 1 where repeated is null;

drop table facebook.backup_temporary_back_cohort_co;

update facebook.temporary_back_cohort_co b set oac_transactions_30_cohort = (select count(distinct t.order_nr) as oac_transactions from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers =0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_co b set paid_price_30_cohort = (select sum(t.paid_price) as paid_price from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers =0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_co b set PC2_30_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers =0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_co b set oac_transactions_60_cohort = (select count(distinct t.order_nr) as oac_transactions from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers =0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_co b set paid_price_60_cohort = (select sum(t.paid_price) as paid_price from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers =0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_co b set PC2_60_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers =0 group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_co set oac_transactions_30_cohort=oac_transactions_30_cohort/repeated, paid_price_30_cohort=paid_price_30_cohort/repeated, PC2_30_cohort=PC2_30_cohort/repeated, oac_transactions_60_cohort=oac_transactions_60_cohort/repeated, paid_price_60_cohort=paid_price_60_cohort/repeated, PC2_60_cohort=PC2_60_cohort/repeated where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set oac_transactions_30_cohort = (select sum(oac_transactions_30_cohort) from facebook.temporary_back_cohort_co b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set cum_net_rev_30_cohort = (select sum(paid_price_30_cohort) from facebook.temporary_back_cohort_co b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set PC2_30_cohort = (select sum(PC2_30_cohort) from facebook.temporary_back_cohort_co b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set oac_transactions_60_cohort = (select sum(oac_transactions_60_cohort) from facebook.temporary_back_cohort_co b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set cum_net_rev_60_cohort = (select sum(paid_price_60_cohort) from facebook.temporary_back_cohort_co b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set PC2_60_cohort = (select sum(PC2_60_cohort) from facebook.temporary_back_cohort_co b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

drop table facebook.temporary_back_cohort_co;

update facebook.fanpage_optimization_co set oac_transactions_30_cohort = 0 where oac_transactions_30_cohort is null;

update facebook.fanpage_optimization_co set oac_transactions_60_cohort = 0 where oac_transactions_60_cohort is null;

update facebook.fanpage_optimization_co set cum_net_rev_30_cohort = 0 where cum_net_rev_30_cohort is null;

update facebook.fanpage_optimization_co set cum_net_rev_60_cohort = 0 where cum_net_rev_60_cohort is null;

-- Back Cohort

alter table facebook.temporary_co add column avg_discount float;

update facebook.temporary_co r set avg_discount = (select avg(1-(i.unit_price/i.original_unit_price)) from bob_live_co.sales_order o inner join bob_live_co.sales_order_item i on i.fk_sales_order = o.id_sales_order where r.transaction_id=o.order_nr group by o.order_nr) where datediff(curdate(), date)<@days; 

create index temporary_co on facebook.temporary_co(campaign, date);

update facebook.fanpage_optimization_co f set net_revenue= (select sum(paid_price) from facebook.temporary_co z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set new_customers= (select sum(z.new_customers) from facebook.temporary_co z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set PC2_absolute= (select sum(PC2) from facebook.temporary_co z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co f set `avg_discount(%)`= (select (avg(avg_discount))*100 from facebook.temporary_co z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

drop table facebook.temporary_co;

update facebook.fanpage_optimization_co set oac_transactions = 0 where oac_transactions is null;

update facebook.fanpage_optimization_co set net_revenue = 0 where net_revenue is null;

update facebook.fanpage_optimization_co set new_customers = 0 where new_customers is null;

create table facebook.optimization_co as select date, campaign, sum(visits)as visits from facebook.fanpage_optimization_co where datediff(curdate(), date)<@days group by date, campaign;

create index optimization_co on facebook.optimization_co(date, campaign);

create table facebook.cost_campaign_co as select date, campaign, sum(spent) as spent, sum(clicks) as clicks, sum(social_impressions) as impressions from facebook.facebook_campaign where datediff(curdate(), date)<@days group by date, campaign;

create index cost_campaign_co on facebook.cost_campaign_co(date, campaign);

create table facebook.final_co as select a.date, a.campaign, a.visits, b.spent, b.clicks, b.impressions from facebook.optimization_co a, facebook.cost_campaign_co b where a.date=b.date and a.campaign=b.campaign;

drop table facebook.optimization_co;

drop table facebook.cost_campaign_co;

create index final_co on facebook.final_co(date, campaign);

update facebook.fanpage_optimization_co f inner join facebook.final_co final_co on f.date=final_co.date and f.campaign=final_co.campaign set f.cost=(f.visits/(final_co.visits))*final_co.spent, f.clicks=(f.visits/(final_co.visits))*final_co.clicks, f.impressions=(f.visits/(final_co.visits))*final_co.impressions where datediff(curdate(), f.date)<@days;

drop table facebook.final_co;

update facebook.fanpage_optimization_co set cost = 0 where cost is null;

update facebook.fanpage_optimization_co set clicks = 0 where clicks is null;

update facebook.fanpage_optimization_co set impressions = 0 where impressions is null;

update facebook.fanpage_optimization_co set category = substr(substr(campaign, locate('.',campaign)+1), 1, locate('.', substr(campaign, locate('.',campaign)+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co set gender = substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1) ,1 , locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))-1) ;

update facebook.fanpage_optimization_co set age = substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,1 ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co set segmentation = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) ,1 ,locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co set extra_field = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) , locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))+1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, PC2_absolute=PC2_absolute/xr, cum_net_rev_30_cohort=cum_net_rev_30_cohort/xr, cum_net_rev_60_cohort=cum_net_rev_60_cohort/xr, PC2_30_cohort=PC2_30_cohort/xr, PC2_60_cohort=PC2_60_cohort/xr where r.country='COL' and datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co set cost=cost/1.23 where date >= '2013-05-01' and datediff(curdate(), date)<@days;

##update facebook.fanpage_optimization_co set oac_transactions=0, gross_revenue=0, net_revenue=0, new_customers=0, oac_transactions_30_cohort=0, oac_transactions_60_cohort=0, cum_net_rev_30_cohort=0, cum_net_rev_60_cohort=0 where obc_transactions=0 and datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_co set obc_transactions=oac_transactions, gross_revenue=net_revenue where obc_transactions=0 and oac_transactions!=0 and datediff(curdate(), date)<@days;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `fanpage_optimization_mx`()
begin

-- Mexico
-- Fanpage Optimization

set @days:=30;

delete from facebook.fanpage_campaign_mx where datediff(curdate(), date)<@days;

delete from facebook.fanpage_transaction_id_mx where datediff(curdate(), date)<@days;

insert into facebook.fanpage_campaign_mx(date, campaign, source, medium, impressions, clicks, visits, ad_cost, bounce, cart) select distinct date, campaign, source, medium, impressions, clicks, visits, ad_cost, bounce, cart from SEM.campaign_ad_group_mx where campaign like '%fanpage%' and campaign like '%!_WCA%' escape '!'
and (campaign not like '%IMA-%' and campaign not like '%MAI-%');

insert into facebook.fanpage_transaction_id_mx 
select * from SEM.transaction_id_mx
where campaign like '%fanpage%' 
and campaign like '%!_WCA%' escape '!'
and (campaign not like '%IMA-%' and campaign not like '%MAI-%');

update facebook.fanpage_campaign_mx set campaign = replace(campaign, '_', '.');

update facebook.fanpage_transaction_id_mx set campaign = replace(campaign, '_', '.');

delete from facebook.fanpage_optimization_mx where datediff(curdate(), date)<@days;

insert into facebook.fanpage_optimization_mx (date, campaign, visits, carts) select date, campaign, visits, cart from facebook.fanpage_campaign_mx where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=facebook.week_iso(date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx f set f.obc_transactions = (select count(distinct z.transaction_id) from production.tbl_order_detail a, facebook.fanpage_transaction_id_mx z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.date=z.date and a.obc=1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx f set f.gross_revenue = (select sum(total_value) from facebook.fanpage_transaction_id_mx z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx f set f.oac_transactions = (select count(distinct z.transaction_id) from production.tbl_order_detail a, facebook.fanpage_transaction_id_mx z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.date=z.date and a.oac=1) where datediff(curdate(), date)<@days;

create table facebook.temporary_mx as select r.date, r.campaign, (select t.custid from production.tbl_order_detail t where t.order_nr=r.transaction_id and oac=1 group by t.custid) as custid, r.transaction_id, (select sum(t.actual_paid_price) from production.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as actual_paid_price, (select sum(ifnull(t.actual_paid_price_after_tax,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost_per_sku,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) from production.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as PC2, (select sum(new_customers) from production.tbl_order_detail z where z.order_nr=r.transaction_id and z.oac=1 group by z.order_nr) as new_customers from facebook.fanpage_transaction_id_mx r where datediff(curdate(), date)<@days group by transaction_id;

-- Back Cohort

create table facebook.temporary_back_cohort_mx(
date date,
campaign varchar(255),
custid int,
transaction_id varchar(255),
oac_transactions_30_cohort float,
oac_transactions_60_cohort float,
actual_paid_price_30_cohort float,
actual_paid_price_60_cohort float,
PC2_30_cohort float,
PC2_60_cohort float,
repeated int
);


create index custid on facebook.temporary_back_cohort_mx(custid);

create index all_index on facebook.temporary_back_cohort_mx(date, campaign);

insert into facebook.temporary_back_cohort_mx (date, campaign, custid, transaction_id) select date, campaign, custid, transaction_id from facebook.temporary_mx where new_customers is not null and datediff(curdate(), date)<@days;

create table facebook.backup_temporary_back_cohort_mx as select * from facebook.temporary_back_cohort_mx;

update facebook.temporary_back_cohort_mx a set repeated = (select count(*) as total from facebook.backup_temporary_back_cohort_mx b where a.date=b.date and a.campaign=b.campaign group by date, campaign, custid having count(*)>1);

update facebook.temporary_back_cohort_mx a set repeated = 1 where repeated is null;

drop table facebook.backup_temporary_back_cohort_mx;

update facebook.temporary_back_cohort_mx b set oac_transactions_30_cohort = (select count(distinct t.order_nr) as oac_transactions from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_mx b set actual_paid_price_30_cohort = (select sum(t.actual_paid_price) as actual_paid_price from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_mx b set PC2_30_cohort = (select sum(ifnull(t.actual_paid_price_after_tax,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost_per_sku,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_mx b set oac_transactions_60_cohort = (select count(distinct t.order_nr) as oac_transactions from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_mx b set actual_paid_price_60_cohort = (select sum(t.actual_paid_price) as actual_paid_price from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_mx b set PC2_60_cohort = (select sum(ifnull(t.actual_paid_price_after_tax,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost_per_sku,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_mx set oac_transactions_30_cohort=oac_transactions_30_cohort/repeated, actual_paid_price_30_cohort=actual_paid_price_30_cohort/repeated, PC2_30_cohort=PC2_30_cohort/repeated, oac_transactions_60_cohort=oac_transactions_60_cohort/repeated, actual_paid_price_60_cohort=actual_paid_price_60_cohort/repeated, PC2_60_cohort=PC2_60_cohort/repeated where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx f set oac_transactions_30_cohort = (select sum(oac_transactions_30_cohort) from facebook.temporary_back_cohort_mx b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx f set cum_net_rev_30_cohort = (select sum(actual_paid_price_30_cohort) from facebook.temporary_back_cohort_mx b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx f set PC2_30_cohort = (select sum(PC2_30_cohort) from facebook.temporary_back_cohort_mx b where f.date=b.date and f.campaign=b.campaign);

update facebook.fanpage_optimization_mx f set oac_transactions_60_cohort = (select sum(oac_transactions_60_cohort) from facebook.temporary_back_cohort_mx b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx f set cum_net_rev_60_cohort = (select sum(actual_paid_price_60_cohort) from facebook.temporary_back_cohort_mx b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx f set PC2_60_cohort = (select sum(PC2_60_cohort) from facebook.temporary_back_cohort_mx b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

drop table facebook.temporary_back_cohort_mx;

update facebook.fanpage_optimization_mx set oac_transactions_30_cohort = 0 where oac_transactions_30_cohort is null;

update facebook.fanpage_optimization_mx set oac_transactions_60_cohort = 0 where oac_transactions_60_cohort is null;

update facebook.fanpage_optimization_mx set cum_net_rev_30_cohort = 0 where cum_net_rev_30_cohort is null;

update facebook.fanpage_optimization_mx set cum_net_rev_60_cohort = 0 where cum_net_rev_60_cohort is null;

-- Back Cohort

alter table facebook.temporary_mx add column avg_discount float;

update facebook.temporary_mx r set avg_discount = (select avg(1-(i.unit_price/i.original_unit_price)) from bob_live_mx.sales_order o inner join bob_live_mx.sales_order_item i on i.fk_sales_order = o.id_sales_order where r.transaction_id=o.order_nr group by o.order_nr) where datediff(curdate(), date)<@days; 

create index temporary_mx on facebook.temporary_mx(campaign, date);

update facebook.fanpage_optimization_mx f set net_revenue= (select sum(actual_paid_price) from facebook.temporary_mx z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx f set new_customers= (select sum(z.new_customers) from facebook.temporary_mx z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx f set PC2_absolute= (select sum(PC2) from facebook.temporary_mx z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx f set `avg_discount(%)`= (select (avg(avg_discount))*100 from facebook.temporary_mx z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

drop table facebook.temporary_mx;

update facebook.fanpage_optimization_mx set oac_transactions = 0 where oac_transactions is null;

update facebook.fanpage_optimization_mx set net_revenue = 0 where net_revenue is null;

update facebook.fanpage_optimization_mx set new_customers = 0 where new_customers is null;

create table facebook.optimization_mx as select date, campaign, sum(visits)as visits from facebook.fanpage_optimization_mx where datediff(curdate(), date)<@days group by date, campaign;

create index optimization_mx on facebook.optimization_mx(date, campaign);

create table facebook.cost_campaign_mx as select date, campaign, sum(spent) as spent, sum(clicks) as clicks, sum(social_impressions) as impressions from facebook.facebook_campaign where datediff(curdate(), date)<@days group by date, campaign;

create index cost_campaign_mx on facebook.cost_campaign_mx(date, campaign);

create table facebook.final_mx as select a.date, a.campaign, a.visits, b.spent, b.clicks, b.impressions from facebook.optimization_mx a, facebook.cost_campaign_mx b where a.date=b.date and a.campaign=b.campaign;

drop table facebook.optimization_mx;

drop table facebook.cost_campaign_mx;

create index final_mx on facebook.final_mx(date, campaign);

update facebook.fanpage_optimization_mx f inner join facebook.final_mx final_mx on f.date=final_mx.date and f.campaign=final_mx.campaign set f.cost=(f.visits/(final_mx.visits))*final_mx.spent, f.clicks=(f.visits/(final_mx.visits))*final_mx.clicks, f.impressions=(f.visits/(final_mx.visits))*final_mx.impressions where datediff(curdate(), f.date)<@days;

drop table facebook.final_mx;

update facebook.fanpage_optimization_mx set cost = 0 where cost is null;

update facebook.fanpage_optimization_mx set clicks = 0 where clicks is null;

update facebook.fanpage_optimization_mx set impressions = 0 where impressions is null;

update facebook.fanpage_optimization_mx set category = substr(substr(campaign, locate('.',campaign)+1), 1, locate('.', substr(campaign, locate('.',campaign)+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx set gender = substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1) ,1 , locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx set age = substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,1 ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx set segmentation = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) ,1 ,locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx set extra_field = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) , locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))+1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, PC2_absolute=PC2_absolute/xr, cum_net_rev_30_cohort=cum_net_rev_30_cohort/xr, cum_net_rev_60_cohort=cum_net_rev_60_cohort/xr, PC2_30_cohort=PC2_30_cohort/xr, PC2_60_cohort=PC2_60_cohort/xr where r.country='MEX' and datediff(curdate(), date)<@days;

##update facebook.fanpage_optimization_mx set oac_transactions=0, gross_revenue=0, net_revenue=0, new_customers=0, oac_transactions_30_cohort=0, oac_transactions_60_cohort=0, cum_net_rev_30_cohort=0, cum_net_rev_60_cohort=0 where obc_transactions=0 and datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_mx set obc_transactions=oac_transactions, gross_revenue=net_revenue where obc_transactions=0 and oac_transactions!=0 and datediff(curdate(), date)<@days;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `fanpage_optimization_pe`()
begin

-- Peru
-- Fanpage Optimization

set @days:=30;

delete from facebook.fanpage_campaign_pe where datediff(curdate(), date)<@days;

delete from facebook.fanpage_transaction_id_pe where datediff(curdate(), date)<@days;

insert into facebook.fanpage_campaign_pe(date, campaign, source, medium, impressions, clicks, visits, ad_cost, bounce, cart) select distinct date, campaign, source, medium, impressions, clicks, visits, ad_cost, bounce, cart from SEM.campaign_ad_group_pe where campaign like '%fanpage%' and campaign like '%!_WCA%' escape '!' 
and (campaign not like '%IMA-%' and campaign not like '%MAI-%');

insert into facebook.fanpage_transaction_id_pe select * from SEM.transaction_id_pe where campaign like '%fanpage%' and campaign like '%!_WCA%' escape '!'
and (campaign not like '%IMA-%' and campaign not like '%MAI-%');

update facebook.fanpage_campaign_pe set campaign = replace(campaign, '_', '.');

update facebook.fanpage_transaction_id_pe set campaign = replace(campaign, '_', '.');

delete from facebook.fanpage_optimization_pe where datediff(curdate(), date)<@days;

insert into facebook.fanpage_optimization_pe (date, campaign, visits, carts) select date, campaign, visits, cart from facebook.fanpage_campaign_pe where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=facebook.week_iso(date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe f set f.obc_transactions = (select count(distinct z.transaction_id) from production_pe.tbl_order_detail a, facebook.fanpage_transaction_id_pe z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.date=z.date and a.obc=1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe f set f.gross_revenue = (select sum(total_value) from facebook.fanpage_transaction_id_pe z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe f set f.oac_transactions = (select count(distinct z.transaction_id) from production_pe.tbl_order_detail a, facebook.fanpage_transaction_id_pe z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.date=z.date and a.oac=1) where datediff(curdate(), date)<@days;

create table facebook.temporary_pe as select r.date, r.campaign, (select t.custid from production_pe.tbl_order_detail t where t.order_nr=r.transaction_id and oac=1 group by t.custid) as custid, r.transaction_id, (select sum(t.paid_price) from production_pe.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as paid_price, (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) from production_pe.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as PC2, (select sum(new_customers) from production_pe.tbl_order_detail z where z.order_nr=r.transaction_id and z.oac=1 group by z.order_nr) as new_customers from facebook.fanpage_transaction_id_pe r where datediff(curdate(), date)<@days group by transaction_id;

-- Back Cohort

create table facebook.temporary_back_cohort_pe(
date date,
campaign varchar(255),
custid int,
transaction_id varchar(255),
oac_transactions_30_cohort float,
oac_transactions_60_cohort float,
paid_price_30_cohort float,
paid_price_60_cohort float,
PC2_30_cohort float,
PC2_60_cohort float,
repeated int
);


create index custid on facebook.temporary_back_cohort_pe(custid);

create index all_index on facebook.temporary_back_cohort_pe(date, campaign);

insert into facebook.temporary_back_cohort_pe (date, campaign, custid, transaction_id) select date, campaign, custid, transaction_id from facebook.temporary_pe where new_customers is not null;

create table facebook.backup_temporary_back_cohort_pe as select * from facebook.temporary_back_cohort_pe;

update facebook.temporary_back_cohort_pe a set repeated = (select count(*) as total from facebook.backup_temporary_back_cohort_pe b where a.date=b.date and a.campaign=b.campaign group by date, campaign, custid having count(*)>1) and datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe a set repeated = 1 where repeated is null;

drop table facebook.backup_temporary_back_cohort_pe;

update facebook.temporary_back_cohort_pe b set oac_transactions_30_cohort = (select count(distinct t.order_nr) as oac_transactions from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe b set paid_price_30_cohort = (select sum(t.paid_price) as paid_price from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe b set PC2_30_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe b set oac_transactions_60_cohort = (select count(distinct t.order_nr) as oac_transactions from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe b set paid_price_60_cohort = (select sum(t.paid_price) as paid_price from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe b set PC2_60_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_pe set oac_transactions_30_cohort=oac_transactions_30_cohort/repeated, paid_price_30_cohort=paid_price_30_cohort/repeated, PC2_30_cohort=PC2_30_cohort/repeated, oac_transactions_60_cohort=oac_transactions_60_cohort/repeated, paid_price_60_cohort=paid_price_60_cohort/repeated, PC2_60_cohort=PC2_60_cohort/repeated where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe f set oac_transactions_30_cohort = (select sum(oac_transactions_30_cohort) from facebook.temporary_back_cohort_pe b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe f set cum_net_rev_30_cohort = (select sum(paid_price_30_cohort) from facebook.temporary_back_cohort_pe b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe f set PC2_30_cohort = (select sum(PC2_30_cohort) from facebook.temporary_back_cohort_pe b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe f set oac_transactions_60_cohort = (select sum(oac_transactions_60_cohort) from facebook.temporary_back_cohort_pe b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe f set cum_net_rev_60_cohort = (select sum(paid_price_60_cohort) from facebook.temporary_back_cohort_pe b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe f set PC2_60_cohort = (select sum(PC2_60_cohort) from facebook.temporary_back_cohort_pe b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

drop table facebook.temporary_back_cohort_pe;

update facebook.fanpage_optimization_pe set oac_transactions_30_cohort = 0 where oac_transactions_30_cohort is null;

update facebook.fanpage_optimization_pe set oac_transactions_60_cohort = 0 where oac_transactions_60_cohort is null;

update facebook.fanpage_optimization_pe set cum_net_rev_30_cohort = 0 where cum_net_rev_30_cohort is null;

update facebook.fanpage_optimization_pe set cum_net_rev_60_cohort = 0 where cum_net_rev_60_cohort is null;

-- Back Cohort

alter table facebook.temporary_pe add column avg_discount float;

update facebook.temporary_pe r set avg_discount = (select avg(1-(i.unit_price/i.original_unit_price)) from bob_live_pe.sales_order o inner join bob_live_pe.sales_order_item i on i.fk_sales_order = o.id_sales_order where r.transaction_id=o.order_nr group by o.order_nr) where datediff(curdate(), date)<@days; 

create index temporary_pe on facebook.temporary_pe(campaign, date);

update facebook.fanpage_optimization_pe f set net_revenue= (select sum(paid_price) from facebook.temporary_pe z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe f set new_customers= (select sum(z.new_customers) from facebook.temporary_pe z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe f set PC2_absolute= (select sum(PC2) from facebook.temporary_pe z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe f set `avg_discount(%)`= (select (avg(avg_discount))*100 from facebook.temporary_pe z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

drop table facebook.temporary_pe;

update facebook.fanpage_optimization_pe set oac_transactions = 0 where oac_transactions is null;

update facebook.fanpage_optimization_pe set net_revenue = 0 where net_revenue is null;

update facebook.fanpage_optimization_pe set new_customers = 0 where new_customers is null;

create table facebook.optimization_pe as select date, campaign, sum(visits)as visits from facebook.fanpage_optimization_pe where datediff(curdate(), date)<@days group by date, campaign;

create index optimization_pe on facebook.optimization_pe(date, campaign);

create table facebook.cost_campaign_pe as select date, campaign, sum(spent) as spent, sum(clicks) as clicks, sum(social_impressions) as impressions from facebook.facebook_campaign where datediff(curdate(), date)<@days group by date, campaign;

create index cost_campaign_pe on facebook.cost_campaign_pe(date, campaign);

create table facebook.final_pe as select a.date, a.campaign, a.visits, b.spent, b.clicks, b.impressions from facebook.optimization_pe a, facebook.cost_campaign_pe b where a.date=b.date and a.campaign=b.campaign;

drop table facebook.optimization_pe;

drop table facebook.cost_campaign_pe;

create index final_pe on facebook.final_pe(date, campaign);

update facebook.fanpage_optimization_pe f inner join facebook.final_pe final_pe on f.date=final_pe.date and f.campaign=final_pe.campaign set f.cost=(f.visits/(final_pe.visits))*final_pe.spent, f.clicks=(f.visits/(final_pe.visits))*final_pe.clicks, f.impressions=(f.visits/(final_pe.visits))*final_pe.impressions where datediff(curdate(), f.date)<@days;

drop table facebook.final_pe;

update facebook.fanpage_optimization_pe set cost = 0 where cost is null;

update facebook.fanpage_optimization_pe set clicks = 0 where clicks is null;

update facebook.fanpage_optimization_pe set impressions = 0 where impressions is null;

update facebook.fanpage_optimization_pe set category = substr(substr(campaign, locate('.',campaign)+1), 1, locate('.', substr(campaign, locate('.',campaign)+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe set gender = substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1) ,1 , locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe set age = substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,1 ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe set segmentation = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) ,1 ,locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe set extra_field = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) , locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))+1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, PC2_absolute=PC2_absolute/xr, cum_net_rev_30_cohort=cum_net_rev_30_cohort/xr, cum_net_rev_60_cohort=cum_net_rev_60_cohort/xr, PC2_30_cohort=PC2_30_cohort/xr, PC2_60_cohort=PC2_60_cohort/xr where r.country='PER' and datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe set cost=cost/1.23 where date >= '2014-01-01' and datediff(curdate(), date)<@days;

##update facebook.fanpage_optimization_pe set oac_transactions=0, gross_revenue=0, net_revenue=0, new_customers=0, oac_transactions_30_cohort=0, oac_transactions_60_cohort=0, cum_net_rev_30_cohort=0, cum_net_rev_60_cohort=0 where obc_transactions=0 and datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_pe set obc_transactions=oac_transactions, gross_revenue=net_revenue where obc_transactions=0 and oac_transactions!=0 and datediff(curdate(), date)<@days;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `fanpage_optimization_ve`()
begin

-- Venezuela
-- Fanpage Optimization

set @days:=30;

delete from facebook.fanpage_campaign_ve where datediff(curdate(), date)<@days;

delete from facebook.fanpage_transaction_id_ve where datediff(curdate(), date)<@days;

insert into facebook.fanpage_campaign_ve(date, campaign, source, medium, impressions, clicks, visits, ad_cost, bounce, cart) 
select distinct date, campaign, source, medium, impressions, clicks, visits, ad_cost, bounce, cart from SEM.campaign_ad_group_ve
 where campaign like '%fanpage%' and  campaign like '%!_WCA%' escape '!'
and (campaign not like '%IMA-%' and campaign not like '%MAI-%');

insert into facebook.fanpage_transaction_id_ve select * from SEM.transaction_id_ve where campaign like '%fanpage%' and campaign like '%!_WCA%' escape '!' and (campaign not like '%IMA-%' and campaign not like '%MAI-%');

update facebook.fanpage_campaign_ve set campaign = replace(campaign, '_', '.');

update facebook.fanpage_transaction_id_ve set campaign = replace(campaign, '_', '.');

delete from facebook.fanpage_optimization_ve where datediff(curdate(), date)<@days;

insert into facebook.fanpage_optimization_ve (date, campaign, visits, carts) select date, campaign, visits, cart from facebook.fanpage_campaign_ve where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=facebook.week_iso(date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve f set f.obc_transactions = (select count(distinct z.transaction_id) from production_ve.tbl_order_detail a, facebook.fanpage_transaction_id_ve z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.date=z.date and a.obc=1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve f set f.gross_revenue = (select sum(total_value) from facebook.fanpage_transaction_id_ve z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve f set f.oac_transactions = (select count(distinct z.transaction_id) from production_ve.tbl_order_detail a, facebook.fanpage_transaction_id_ve z where a.order_nr=z.transaction_id and f.campaign=z.campaign and f.date=z.date and a.oac=1) where datediff(curdate(), date)<@days;

create table facebook.temporary_ve as select r.date, r.campaign, (select t.custid from production_ve.tbl_order_detail t where t.order_nr=r.transaction_id and oac=1 group by t.custid) as custid, r.transaction_id, (select sum(t.paid_price) from production_ve.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as paid_price, (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) from production_ve.tbl_order_detail t where t.order_nr=r.transaction_id and t.oac=1 group by t.order_nr) as PC2, (select sum(new_customers) from production_ve.tbl_order_detail z where z.order_nr=r.transaction_id and z.oac=1 group by z.order_nr) as new_customers from facebook.fanpage_transaction_id_ve r where datediff(curdate(), date)<@days group by transaction_id;

-- Back Cohort

create table facebook.temporary_back_cohort_ve(
date date,
campaign varchar(255),
custid int,
transaction_id varchar(255),
oac_transactions_30_cohort float,
oac_transactions_60_cohort float,
paid_price_30_cohort float,
paid_price_60_cohort float,
PC2_30_cohort float,
PC2_60_cohort float,
repeated int
);


create index custid on facebook.temporary_back_cohort_ve(custid);

create index all_index on facebook.temporary_back_cohort_ve(date, campaign);

insert into facebook.temporary_back_cohort_ve (date, campaign, custid, transaction_id) select date, campaign, custid, transaction_id from facebook.temporary_ve where new_customers is not null and datediff(curdate(), date)<@days;

create table facebook.backup_temporary_back_cohort_ve as select * from facebook.temporary_back_cohort_ve;

update facebook.temporary_back_cohort_ve a set repeated = (select count(*) as total from facebook.backup_temporary_back_cohort_ve b where a.date=b.date and a.campaign=b.campaign group by date, campaign, custid having count(*)>1);

update facebook.temporary_back_cohort_ve a set repeated = 1 where repeated is null;

drop table facebook.backup_temporary_back_cohort_ve;

update facebook.temporary_back_cohort_ve b set oac_transactions_30_cohort = (select count(distinct t.order_nr) as oac_transactions from production_ve.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_ve b set paid_price_30_cohort = (select sum(t.paid_price) as paid_price from production_ve.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_ve b set PC2_30_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_ve.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_ve b set oac_transactions_60_cohort = (select count(distinct t.order_nr) as oac_transactions from production_ve.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_ve b set paid_price_60_cohort = (select sum(t.paid_price) as paid_price from production_ve.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_ve b set PC2_60_cohort = (select sum(ifnull(t.paid_price_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)-ifnull(t.costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(t.shipping_cost,0)-ifnull(t.payment_cost,0)-ifnull(t.wh,0)-ifnull(t.cs,0)) as PC2 from production_ve.tbl_order_detail t where t.custid=b.custid and t.oac=1 and  t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.transaction_id, b.custid order by b.custid) where datediff(curdate(), date)<@days;

update facebook.temporary_back_cohort_ve set oac_transactions_30_cohort=oac_transactions_30_cohort/repeated, paid_price_30_cohort=paid_price_30_cohort/repeated, PC2_30_cohort=PC2_30_cohort/repeated, oac_transactions_60_cohort=oac_transactions_60_cohort/repeated, paid_price_60_cohort=paid_price_60_cohort/repeated, PC2_60_cohort=PC2_60_cohort/repeated where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve f set oac_transactions_30_cohort = (select sum(oac_transactions_30_cohort) from facebook.temporary_back_cohort_ve b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve f set cum_net_rev_30_cohort = (select sum(paid_price_30_cohort) from facebook.temporary_back_cohort_ve b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve f set PC2_30_cohort = (select sum(PC2_30_cohort) from facebook.temporary_back_cohort_ve b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve f set oac_transactions_60_cohort = (select sum(oac_transactions_60_cohort) from facebook.temporary_back_cohort_ve b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve f set cum_net_rev_60_cohort = (select sum(paid_price_60_cohort) from facebook.temporary_back_cohort_ve b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve f set PC2_60_cohort = (select sum(PC2_60_cohort) from facebook.temporary_back_cohort_ve b where f.date=b.date and f.campaign=b.campaign) where datediff(curdate(), date)<@days;

drop table facebook.temporary_back_cohort_ve;

update facebook.fanpage_optimization_ve set oac_transactions_30_cohort = 0 where oac_transactions_30_cohort is null;

update facebook.fanpage_optimization_ve set oac_transactions_60_cohort = 0 where oac_transactions_60_cohort is null;

update facebook.fanpage_optimization_ve set cum_net_rev_30_cohort = 0 where cum_net_rev_30_cohort is null;

update facebook.fanpage_optimization_ve set cum_net_rev_60_cohort = 0 where cum_net_rev_60_cohort is null;

-- Back Cohort

alter table facebook.temporary_ve add column avg_discount float;

update facebook.temporary_ve r set avg_discount = (select avg(1-(i.unit_price/i.original_unit_price)) from bob_live_ve.sales_order o inner join bob_live_ve.sales_order_item i on i.fk_sales_order = o.id_sales_order where r.transaction_id=o.order_nr group by o.order_nr) where datediff(curdate(), date)<@days; 

create index temporary_ve on facebook.temporary_ve(campaign, date);

update facebook.fanpage_optimization_ve f set net_revenue= (select sum(paid_price) from facebook.temporary_ve z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve f set new_customers= (select sum(z.new_customers) from facebook.temporary_ve z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve f set PC2_absolute= (select sum(PC2) from facebook.temporary_ve z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve f set `avg_discount(%)`= (select (avg(avg_discount))*100 from facebook.temporary_ve z where f.campaign=z.campaign and f.date=z.date) where datediff(curdate(), date)<@days;

drop table facebook.temporary_ve;

update facebook.fanpage_optimization_ve set oac_transactions = 0 where oac_transactions is null;

update facebook.fanpage_optimization_ve set net_revenue = 0 where net_revenue is null;

update facebook.fanpage_optimization_ve set new_customers = 0 where new_customers is null;

create table facebook.optimization_ve as select date, campaign, sum(visits)as visits from facebook.fanpage_optimization_ve where datediff(curdate(), date)<@days group by date, campaign;

create index optimization_ve on facebook.optimization_ve(date, campaign);

create table facebook.cost_campaign_ve as select date, campaign, sum(spent) as spent, sum(clicks) as clicks, sum(social_impressions) as impressions from facebook.facebook_campaign where datediff(curdate(), date)<@days group by date, campaign;

create index cost_campaign_ve on facebook.cost_campaign_ve(date, campaign);

create table facebook.final_ve as select a.date, a.campaign, a.visits, b.spent, b.clicks, b.impressions from facebook.optimization_ve a, facebook.cost_campaign_ve b where a.date=b.date and a.campaign=b.campaign;

drop table facebook.optimization_ve;

drop table facebook.cost_campaign_ve;

create index final_ve on facebook.final_ve(date, campaign);

update facebook.fanpage_optimization_ve f inner join facebook.final_ve final_ve on f.date=final_ve.date and f.campaign=final_ve.campaign set f.cost=(f.visits/(final_ve.visits))*final_ve.spent, f.clicks=(f.visits/(final_ve.visits))*final_ve.clicks, f.impressions=(f.visits/(final_ve.visits))*final_ve.impressions where datediff(curdate(), f.date)<@days;

drop table facebook.final_ve;

update facebook.fanpage_optimization_ve set cost = 0 where cost is null;

update facebook.fanpage_optimization_ve set clicks = 0 where clicks is null;

update facebook.fanpage_optimization_ve set impressions = 0 where impressions is null;

update facebook.fanpage_optimization_ve set category = substr(substr(campaign, locate('.',campaign)+1), 1, locate('.', substr(campaign, locate('.',campaign)+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve set gender = substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1) ,1 , locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve set age = substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,1 ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve set segmentation = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) ,1 ,locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))-1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve set extra_field = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) , locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))+1) where datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, PC2_absolute=PC2_absolute/xr, cum_net_rev_30_cohort=cum_net_rev_30_cohort/xr, cum_net_rev_60_cohort=cum_net_rev_60_cohort/xr, PC2_30_cohort=PC2_30_cohort/xr, PC2_60_cohort=PC2_60_cohort/xr where r.country='VEN' and datediff(curdate(), date)<@days;

##update facebook.fanpage_optimization_ve set oac_transactions=0, gross_revenue=0, net_revenue=0, new_customers=0, oac_transactions_30_cohort=0, oac_transactions_60_cohort=0, cum_net_rev_30_cohort=0, cum_net_rev_60_cohort=0 where obc_transactions=0 and datediff(curdate(), date)<@days;

update facebook.fanpage_optimization_ve set obc_transactions=oac_transactions, gross_revenue=net_revenue where obc_transactions=0 and oac_transactions!=0 and datediff(curdate(), date)<@days;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:03:59
