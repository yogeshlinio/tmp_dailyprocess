-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: test
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'test'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`leonardo.fraile`@`%`*/ /*!50003 PROCEDURE `1`()
BEGIN
DECLARE fecha_reporte DATE;

#SET fecha_reporte = "2014-03-30";
SET fecha_reporte = subdate(CURDATE(), 1);

INSERT INTO finance_ve.Reporte_Contenido 
SELECT
	fecha_reporte,
	IFNULL(SUBSTR(amc.Cat_BP,5),"(en blanco)") AS Cat_BP,
	count(DISTINCT(amc.sku_config)) as Cantidad,
	"V" as Tipo
FROM
	development_ve.A_Master_Catalog AS amc
WHERE
	amc.isVisible = 1
GROUP BY
	amc.Cat_BP;

INSERT INTO finance_ve.Reporte_Contenido
SELECT
	fecha_reporte,
	IFNULL(SUBSTR(osh.category_bp,5),"(en blanco)") AS Cat_BP,
	count(DISTINCT(osh.sku_config)) as Cantidad, 
	"NV" as Tipo
FROM
	operations_ve.out_stock_hist AS osh
WHERE
	osh.visible = 0
AND osh.in_stock = 1
AND osh.reserved = 0
GROUP BY
	osh.category_bp;

INSERT INTO finance_ve.Reporte_Contenido
SELECT
	fecha_reporte,
	IFNULL(SUBSTR(osh.category_bp,5),"(en blanco)") AS Cat_BP,
	count(DISTINCT(osh.sku_config)) AS Cantidad,
	"NVFC" AS Tipo
FROM
	operations_ve.out_stock_hist AS osh
JOIN (
	SELECT
		bcc.sku
	FROM
		bob_live_ve.catalog_config AS bcc
	WHERE
		bcc.`status` <> "deleted"
	AND bcc.pet_status <> "creation,edited,images"
) AS skufc ON osh.sku_config = skufc.sku
WHERE
	osh.visible = 0
AND osh.in_stock = 1
AND osh.reserved = 0
GROUP BY
	osh.category_bp;

INSERT INTO finance_ve.Reporte_Contenido
SELECT
	fecha_reporte,
	IFNULL(SUBSTR(amc.Cat_BP,5),"(en blanco)") AS Cat_BP,
	count(DISTINCT(amc.sku_config)) AS Cantidad,
"FC" as Tipo
FROM
	development_ve.A_Master_Catalog AS amc
JOIN (
	SELECT
		bcc.sku
	FROM
		bob_live_ve.catalog_config AS bcc
	WHERE
		bcc.`status` <> "deleted"
	AND bcc.pet_status <> "creation,edited,images"
) as skufc on amc.sku_config = skufc.sku
GROUP BY
	amc.Cat_BP;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `CAC`()
begin

delete from test.CAC;

-- Colombia

create table test.temporary_first_order as select CustomerNum, FirstOrderNum
from development_co.A_Master
where OrderAfterCan = 1
group by CustomerNum;

create index customernum on test.temporary_first_order(customernum);
create index FirstOrderNum on test.temporary_first_order(FirstOrderNum);

create table test.temporary_cac as select CustomerNum, CouponCode, Date, OrderNum, PrefixCode
from development_co.A_Master
where CouponCode like '%cac%'
 and OrderAfterCan = 1;

create index customernum on test.temporary_cac(customernum);
create index OrderNum on test.temporary_cac(OrderNum);

insert into test.CAC select 'Colombia', a.CustomerNum, a.FirstOrderNum, b.date, if(c.CouponCode is not null, 1, 0), c.Date, c.OrderNum, c.CouponCode, c.PrefixCode from test.temporary_first_order a inner join development_co.Out_SalesReportOrder b on a.FirstOrderNum = b.OrderNum LEFT JOIN test.temporary_cac c on a.CustomerNum = c.CustomerNum; 

drop table test.temporary_first_order;

drop table test.temporary_cac;

-- Mexico

create table test.temporary_first_order as select CustomerNum, FirstOrderNum
from development_mx.A_Master
where OrderAfterCan = 1
group by CustomerNum;

create index customernum on test.temporary_first_order(customernum);
create index FirstOrderNum on test.temporary_first_order(FirstOrderNum);

create table test.temporary_cac as select CustomerNum, CouponCode, Date, OrderNum, PrefixCode
from development_mx.A_Master
where CouponCode like '%cac%'
 and OrderAfterCan = 1;

create index customernum on test.temporary_cac(customernum);
create index OrderNum on test.temporary_cac(OrderNum);

insert into test.CAC select 'Mexico', a.CustomerNum, a.FirstOrderNum, b.date, if(c.CouponCode is not null, 1, 0), c.Date, c.OrderNum, c.CouponCode, c.PrefixCode from test.temporary_first_order a inner join development_mx.Out_SalesReportOrder b on a.FirstOrderNum = b.OrderNum LEFT JOIN test.temporary_cac c on a.CustomerNum = c.CustomerNum; 

drop table test.temporary_first_order;

drop table test.temporary_cac;

-- Peru

create table test.temporary_first_order as select CustomerNum, FirstOrderNum
from development_pe.A_Master
where OrderAfterCan = 1
group by CustomerNum;

create index customernum on test.temporary_first_order(customernum);
create index FirstOrderNum on test.temporary_first_order(FirstOrderNum);

create table test.temporary_cac as select CustomerNum, CouponCode, Date, OrderNum, PrefixCode
from development_pe.A_Master
where CouponCode like '%cac%'
 and OrderAfterCan = 1;

create index customernum on test.temporary_cac(customernum);
create index OrderNum on test.temporary_cac(OrderNum);

insert into test.CAC select 'Peru', a.CustomerNum, a.FirstOrderNum, b.date, if(c.CouponCode is not null, 1, 0), c.Date, c.OrderNum, c.CouponCode, c.PrefixCode from test.temporary_first_order a inner join development_pe.Out_SalesReportOrder b on a.FirstOrderNum = b.OrderNum LEFT JOIN test.temporary_cac c on a.CustomerNum = c.CustomerNum; 

drop table test.temporary_first_order;

drop table test.temporary_cac;

-- Venezuela

create table test.temporary_first_order as select CustomerNum, FirstOrderNum
from development_ve.A_Master
where OrderAfterCan = 1
group by CustomerNum;

create index customernum on test.temporary_first_order(customernum);
create index FirstOrderNum on test.temporary_first_order(FirstOrderNum);

create table test.temporary_cac as select CustomerNum, CouponCode, Date, OrderNum, PrefixCode
from development_ve.A_Master
where CouponCode like '%cac%'
 and OrderAfterCan = 1;

create index customernum on test.temporary_cac(customernum);
create index OrderNum on test.temporary_cac(OrderNum);

insert into test.CAC select 'Venezuela', a.CustomerNum, a.FirstOrderNum, b.date, if(c.CouponCode is not null, 1, 0), c.Date, c.OrderNum, c.CouponCode, c.PrefixCode from test.temporary_first_order a inner join development_ve.Out_SalesReportOrder b on a.FirstOrderNum = b.OrderNum LEFT JOIN test.temporary_cac c on a.CustomerNum = c.CustomerNum; 

drop table test.temporary_first_order;

drop table test.temporary_cac;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `calcworkdays`()
begin

set @pasado:=120;
set @futuro:=60;

set @days:=@pasado;
set @days2:=@pasado+@futuro;

drop table if exists calcworkdays;

create table calcworkdays(
date_ordered date,
date_delivered date,
workdays int
);
create index date on calcworkdays(date_ordered, date_delivered);

set @date_ordered:=date_sub(curdate(), interval @days day);
set @date_delivered:=date_sub(curdate(), interval @days day);

set @x:=1;
while @x<=@days do
	set @y:=1;
	while @y<=@days2 do
		insert into calcworkdays(date_ordered, date_delivered) select @date_ordered, @date_delivered;
		set @date_delivered:=@date_delivered + interval 1 day;
		set @y:=@y+1; 
	end while;
set @days2:=@days2-1;
set @date_ordered:=@date_ordered + interval 1 day;
set @date_delivered:=@date_ordered;
set @x:=@x+1; 
end while;

update calcworkdays set workdays = production.calcworkdays(date_ordered, date_delivered);

alter ignore table calcworkdays add primary key(date_ordered, date_delivered);

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`leonardo.fraile`@`%`*/ /*!50003 PROCEDURE `Reporte_Contenido`()
BEGIN
DECLARE fecha_reporte DATE;

#SET fecha_reporte = "2014-03-30";
SET fecha_reporte = subdate(CURDATE(), 1);

INSERT INTO finance_ve.Reporte_Contenido 
SELECT
	fecha_reporte,
	IFNULL(SUBSTR(amc.Cat_BP,5),"(en blanco)") AS Cat_BP,
	count(DISTINCT(amc.sku_config)) as Cantidad,
	"V" as Tipo
FROM
	development_ve.A_Master_Catalog AS amc
WHERE
	amc.isVisible = 1
GROUP BY
	amc.Cat_BP;

INSERT INTO finance_ve.Reporte_Contenido
SELECT
	fecha_reporte,
	IFNULL(SUBSTR(osh.category_bp,5),"(en blanco)") AS Cat_BP,
	count(DISTINCT(osh.sku_config)) as Cantidad, 
	"NV" as Tipo
FROM
	operations_ve.out_stock_hist AS osh
WHERE
	osh.visible = 0
AND osh.in_stock = 1
AND osh.reserved = 0
GROUP BY
	osh.category_bp;

INSERT INTO finance_ve.Reporte_Contenido
SELECT
	fecha_reporte,
	IFNULL(SUBSTR(osh.category_bp,5),"(en blanco)") AS Cat_BP,
	count(DISTINCT(osh.sku_config)) AS Cantidad,
	"NVFC" AS Tipo
FROM
	operations_ve.out_stock_hist AS osh
JOIN (
	SELECT
		bcc.sku
	FROM
		bob_live_ve.catalog_config AS bcc
	WHERE
		bcc.`status` <> "deleted"
	AND bcc.pet_status <> "creation,edited,images"
) AS skufc ON osh.sku_config = skufc.sku
WHERE
	osh.visible = 0
AND osh.in_stock = 1
AND osh.reserved = 0
GROUP BY
	osh.category_bp;

INSERT INTO finance_ve.Reporte_Contenido
SELECT
	fecha_reporte,
	IFNULL(SUBSTR(amc.Cat_BP,5),"(en blanco)") AS Cat_BP,
	count(DISTINCT(amc.sku_config)) AS Cantidad,
"FC" as Tipo
FROM
	development_ve.A_Master_Catalog AS amc
JOIN (
	SELECT
		bcc.sku
	FROM
		bob_live_ve.catalog_config AS bcc
	WHERE
		bcc.`status` <> "deleted"
	AND bcc.pet_status <> "creation,edited,images"
) as skufc on amc.sku_config = skufc.sku
GROUP BY
	amc.Cat_BP;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `tele_sales`()
begin



delete from test.tele_sales_mx;

set @a:=0;

create table test.test_cost as select @a:=@a+1 as id, monthnum, first_day, last_day, datediff(last_day, first_day) as diff from (select distinct monthnum, cast(substring(monthnum, 1, 4) as char) as year, cast(substring(monthnum, 5, 6) as char) as month, concat(cast(substring(monthnum, 1, 4) as char), '-', cast(substring(monthnum, 5, 6) as char), '-01') as first_day, last_day(concat(cast(substring(monthnum, 1, 4) as char), '-', cast(substring(monthnum, 5, 6) as char), '-01'))  as last_day from test.M_Costs where country='MEX' and typecost='SalesForce')a;

set @x:=1;
while @x<=(select max(id) from test.test_cost) do
	set @y:=1;
	set @first_day=(select first_day from test.test_cost where id=@x);
	insert into test.tele_sales_mx(date) select @first_day:=@first_day;
	while @y<=(select diff from test.test_cost where id=@x) do
	insert into test.tele_sales_mx(date) select @first_day:=@first_day + interval 1 day;
	set @y:=@y+1;
	end while;
set @x:=@x+1;
end while;

update test.tele_sales_mx inner join test.test_cost c on c.monthnum=concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) inner join test.M_Costs m on m.monthnum=c.monthnum set cost=value/(diff+1) where m.country='MEX' and typecost='SalesForce';

update test.tele_sales_mx s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=concat(year(s.date),if(month(s.date)<10,concat(0,month(s.date)),month(s.date))) set s.cost = s.cost/xr where r.country='MEX';

drop table test.test_cost;



delete from test.tele_sales_co;

set @a:=0;

create table test.test_cost as select @a:=@a+1 as id, monthnum, first_day, last_day, datediff(last_day, first_day) as diff from (select distinct monthnum, cast(substring(monthnum, 1, 4) as char) as year, cast(substring(monthnum, 5, 6) as char) as month, concat(cast(substring(monthnum, 1, 4) as char), '-', cast(substring(monthnum, 5, 6) as char), '-01') as first_day, last_day(concat(cast(substring(monthnum, 1, 4) as char), '-', cast(substring(monthnum, 5, 6) as char), '-01'))  as last_day from test.M_Costs where country='COL' and typecost='SalesForce')a;

set @x:=1;
while @x<=(select max(id) from test.test_cost) do
	set @y:=1;
	set @first_day=(select first_day from test.test_cost where id=@x);
	insert into test.tele_sales_co(date) select @first_day:=@first_day;
	while @y<=(select diff from test.test_cost where id=@x) do
	insert into test.tele_sales_co(date) select @first_day:=@first_day + interval 1 day;
	set @y:=@y+1;
	end while;
set @x:=@x+1;
end while;

update test.tele_sales_co inner join test.test_cost c on c.monthnum=concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) inner join test.M_Costs m on m.monthnum=c.monthnum set cost=value/(diff+1) where m.country='COL' and typecost='SalesForce';

update test.tele_sales_co s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=concat(year(s.date),if(month(s.date)<10,concat(0,month(s.date)),month(s.date))) set s.cost = s.cost/xr where r.country='COL';

drop table test.test_cost;



delete from test.tele_sales_pe;

set @a:=0;

create table test.test_cost as select @a:=@a+1 as id, monthnum, first_day, last_day, datediff(last_day, first_day) as diff from (select distinct monthnum, cast(substring(monthnum, 1, 4) as char) as year, cast(substring(monthnum, 5, 6) as char) as month, concat(cast(substring(monthnum, 1, 4) as char), '-', cast(substring(monthnum, 5, 6) as char), '-01') as first_day, last_day(concat(cast(substring(monthnum, 1, 4) as char), '-', cast(substring(monthnum, 5, 6) as char), '-01'))  as last_day from test.M_Costs where country='PER' and typecost='SalesForce')a;

set @x:=1;
while @x<=(select max(id) from test.test_cost) do
	set @y:=1;
	set @first_day=(select first_day from test.test_cost where id=@x);
	insert into test.tele_sales_pe(date) select @first_day:=@first_day;
	while @y<=(select diff from test.test_cost where id=@x) do
	insert into test.tele_sales_pe(date) select @first_day:=@first_day + interval 1 day;
	set @y:=@y+1;
	end while;
set @x:=@x+1;
end while;

update test.tele_sales_pe inner join test.test_cost c on c.monthnum=concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) inner join test.M_Costs m on m.monthnum=c.monthnum set cost=value/(diff+1) where m.country='PER' and typecost='SalesForce';

update test.tele_sales_pe s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=concat(year(s.date),if(month(s.date)<10,concat(0,month(s.date)),month(s.date))) set s.cost = s.cost/xr where r.country='PER';

drop table test.test_cost;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `test`()
begin

create table marketing_report.temporary_targets(
yrmonth int,
channel_group varchar(255),
count int
);

insert into marketing_report.temporary_targets select yrmonth, channel_group, count(channel) from test.global_report where country='Colombia' and datediff(curdate(), date)<@days group by yrmonth, channel_group;

update test.global_report g inner join marketing_report.targets t on g.yrmonth=t.yrmonth and g.country=t.country and (g.channel_group=t.channel_group or g.channel=t.channel_group) inner join marketing_report.temporary_targets s on s.yrmonth=g.yrmonth and s.channel_group=g.channel_group set g.`PC1.5_target`=t.`PC1.5`, g.net_revenue_target=t.net_revenue/s.count, g.cac_target=t.cac/s.count where datediff(curdate(), g.date)<@days;

drop table marketing_report.temporary_targets;

create table marketing_report.temporary_targets(
yrmonth int,
date date,
days_in_month int,
current_day int
);

delete from marketing_report.temporary_targets;

set @date='2014-02-01';
while @date<=curdate() do
insert into marketing_report.temporary_targets(date) select @date;
set @date:=@date + interval 1 day;
end while;

update marketing_report.temporary_targets set yrmonth=concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), days_in_month=day(last_day(date)), current_day=day(date);

create table marketing_report.temporary_cummulative as select a.yrmonth, a.date, a.channel_group, sum(b.net_revenue) as net_revenue, sum(b.net_revenue_target) as net_revenue_target from (select country, yrmonth, date, channel_group, sum(net_revenue) as net_revenue, sum(net_revenue_target) as net_revenue_target from test.global_report where yrmonth>=201402 and country='Colombia' group by channel_group, date)b, (select country, yrmonth, date, channel_group, sum(net_revenue) as net_revenue, sum(net_revenue_target) as net_revenue_target from test.global_report where yrmonth>=201402 and country='Colombia' group by channel_group, date)a where a.date>=b.date and a.channel_group=b.channel_group and a.yrmonth=b.yrmonth and a.country=b.country group by a.date, a.channel_group order by a.channel_group, a.date;

update test.global_report g inner join marketing_report.temporary_targets t on t.date=g.date inner join marketing_report.temporary_cummulative c on g.date=c.date and g.channel_group=c.channel_group set net_revenue_deviation = (c.net_revenue/((c.net_revenue_target/t.days_in_month)*t.current_day))-1 where country='Colombia'; 

drop table marketing_report.temporary_cummulative;

drop table marketing_report.temporary_targets;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `test09`()
begin

-- Estimated

-- Moving Avg

drop table if exists marketing_report.temporary_retargeting_report;
create table marketing_report.temporary_retargeting_report(
date date,
source varchar(255),
medium varchar(255),
campaign varchar(255),
gross_transactions int not null default 0,
net_transactions int not null default 0,
e_transactions float not null default 0,
ee_transactions float not null default 0,
gross_revenue float not null default 0,
net_revenue float not null default 0,
e_revenue float not null default 0,
ee_revenue float not null default 0,
new_customers_gross int not null default 0,
new_customers int not null default 0,
e_new_customers float not null default 0,
ee_new_customers float not null default 0,
cancel float,
returns float,
voucher float
);

create index temporary_mkt on marketing_report.temporary_retargeting_report(date, source, medium, campaign);

insert into marketing_report.temporary_retargeting_report(date, source, medium, campaign, gross_transactions, net_transactions, gross_revenue, net_revenue, new_customers_gross, new_customers) select date, source, medium, campaign, gross_transactions, net_transactions, gross_revenue, net_revenue, new_customers_gross, new_customers from marketing_report.retargeting_report where country='Mexico' and datediff(curdate(), date)<90;

-- Orders

set @date=date_sub(curdate(), interval 90 day);

while @date!=curdate() do
update marketing_report.temporary_retargeting_report t set e_transactions = (select (avg(g.net_transactions/g.gross_transactions)) from marketing_report.retargeting_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Mexico') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_retargeting_report t set e_transactions = gross_transactions*e_transactions where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_transactions = case when e_transactions>net_transactions then e_transactions else net_transactions end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set cancel = (select count(distinct order_nr) from production.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.source_medium and t.campaign=d.campaign and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set returns = (select count(distinct order_nr) from production.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.source_medium and t.campaign=d.campaign and t.date=d.date and d.returned=1) where datediff(curdate(), date)<90; 

update marketing_report.temporary_retargeting_report t set cancel = cancel/gross_transactions, returns = returns/gross_transactions where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set ee_transactions = gross_transactions*(1-ifnull(cancel,0)-ifnull(returns,0)) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_transactions = case when e_transactions>ee_transactions then ee_transactions else e_transactions end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_transactions = case when e_transactions>net_transactions then e_transactions else net_transactions end where datediff(curdate(), date)<90;

-- Revenue

set @date=date_sub(curdate(), interval 90 day);

while @date!=curdate() do
update marketing_report.temporary_retargeting_report t set e_revenue = (select (avg(g.net_revenue/g.gross_revenue)) from marketing_report.retargeting_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Mexico') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_retargeting_report t set e_revenue = gross_revenue*e_revenue where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_revenue = case when e_revenue>net_revenue then e_revenue else net_revenue end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set cancel = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.source_medium and t.campaign=d.campaign and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set returns = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.source_medium and t.campaign=d.campaign and t.date=d.date and d.returned=1) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set voucher = (select sum(coupon_money_after_vat) from production.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.source_medium and t.campaign=d.campaign and t.date=d.date and d.coupon_code is not null and d.oac=1) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set cancel = cancel/gross_revenue, returns = returns/gross_revenue, voucher=voucher/gross_revenue where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set ee_revenue = gross_revenue*(1-ifnull(cancel,0)-ifnull(returns,0)-ifnull(voucher,0)) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_revenue = case when e_revenue>ee_revenue then ee_revenue else e_revenue end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_revenue = case when e_revenue>net_revenue then e_revenue else net_revenue end where datediff(curdate(), date)<90;

-- New Customers

set @date=date_sub(curdate(), interval 90 day);

while @date!=curdate() do
update marketing_report.temporary_retargeting_report t set e_new_customers = (select (avg(g.new_customers/g.new_customers_gross)) from marketing_report.retargeting_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Mexico') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_retargeting_report t set e_new_customers = new_customers_gross*e_new_customers where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_new_customers = case when e_new_customers>new_customers then e_new_customers else new_customers end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set cancel = (select sum(new_customers_gross) from production.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.source_medium and t.campaign=d.campaign and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set returns = (select sum(new_customers_gross) from production.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.source_medium and t.campaign=d.campaign and t.date=d.date and d.returned=1) where datediff(curdate(), date)<90; 

update marketing_report.temporary_retargeting_report t set cancel = cancel/new_customers_gross, returns = returns/new_customers_gross where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set ee_new_customers = new_customers_gross*(1-ifnull(cancel,0)-ifnull(returns,0)) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_new_customers = case when e_new_customers>ee_new_customers then ee_new_customers else e_new_customers end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_new_customers = case when e_new_customers>new_customers then e_new_customers else new_customers end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t inner join marketing_report.retargeting_report g on t.date=g.date and t.source=g.source and t.medium=g.medium and t.campaign=g.campaign set g.e_transactions=t.e_transactions, g.e_revenue=t.e_revenue, g.e_new_customers=t.e_new_customers where country = 'Mexico' and datediff(curdate(), t.date)<90;

drop table marketing_report.temporary_retargeting_report;

-- Estimated

-- Moving Avg

drop table if exists marketing_report.temporary_retargeting_report;
create table marketing_report.temporary_retargeting_report(
date date,
source varchar(255),
medium varchar(255),
campaign varchar(255),
gross_transactions int not null default 0,
net_transactions int not null default 0,
e_transactions float not null default 0,
ee_transactions float not null default 0,
gross_revenue float not null default 0,
net_revenue float not null default 0,
e_revenue float not null default 0,
ee_revenue float not null default 0,
new_customers_gross int not null default 0,
new_customers int not null default 0,
e_new_customers float not null default 0,
ee_new_customers float not null default 0,
cancel float,
returns float,
voucher float
);

create index temporary_mkt on marketing_report.temporary_retargeting_report(date, source, medium, campaign);

insert into marketing_report.temporary_retargeting_report(date, source, medium, campaign, gross_transactions, net_transactions, gross_revenue, net_revenue, new_customers_gross, new_customers) select date, source, medium, campaign, gross_transactions, net_transactions, gross_revenue, net_revenue, new_customers_gross, new_customers from marketing_report.retargeting_report where country='Colombia' and datediff(curdate(), date)<90;

-- Orders

set @date=date_sub(curdate(), interval 90 day);

while @date!=curdate() do
update marketing_report.temporary_retargeting_report t set e_transactions = (select (avg(g.net_transactions/g.gross_transactions)) from marketing_report.retargeting_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Colombia') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_retargeting_report t set e_transactions = gross_transactions*e_transactions where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_transactions = case when e_transactions>net_transactions then e_transactions else net_transactions end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set cancel = (select count(distinct order_nr) from production_co.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.`source/medium` and t.campaign=d.campaign and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set returns = (select count(distinct order_nr) from production_co.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.`source/medium` and t.campaign=d.campaign and t.date=d.date and d.returned=1) where datediff(curdate(), date)<90; 

update marketing_report.temporary_retargeting_report t set cancel = cancel/gross_transactions, returns = returns/gross_transactions where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set ee_transactions = gross_transactions*(1-ifnull(cancel,0)-ifnull(returns,0)) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_transactions = case when e_transactions>ee_transactions then ee_transactions else e_transactions end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_transactions = case when e_transactions>net_transactions then e_transactions else net_transactions end where datediff(curdate(), date)<90;

-- Revenue

set @date=date_sub(curdate(), interval 90 day);

while @date!=curdate() do
update marketing_report.temporary_retargeting_report t set e_revenue = (select (avg(g.net_revenue/g.gross_revenue)) from marketing_report.retargeting_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Colombia') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_retargeting_report t set e_revenue = gross_revenue*e_revenue where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_revenue = case when e_revenue>net_revenue then e_revenue else net_revenue end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set cancel = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production_co.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.`source/medium` and t.campaign=d.campaign and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set returns = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production_co.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.`source/medium` and t.campaign=d.campaign and t.date=d.date and d.returned=1) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set voucher = (select sum(coupon_money_after_vat) from production_co.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.`source/medium` and t.campaign=d.campaign and t.date=d.date and d.coupon_code is not null and d.oac=1) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set cancel = cancel/gross_revenue, returns = returns/gross_revenue, voucher=voucher/gross_revenue where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set ee_revenue = gross_revenue*(1-ifnull(cancel,0)-ifnull(returns,0)-ifnull(voucher,0)) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_revenue = case when e_revenue>ee_revenue then ee_revenue else e_revenue end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_revenue = case when e_revenue>net_revenue then e_revenue else net_revenue end where datediff(curdate(), date)<90;

-- New Customers

set @date=date_sub(curdate(), interval 90 day);

while @date!=curdate() do
update marketing_report.temporary_retargeting_report t set e_new_customers = (select (avg(g.new_customers/g.new_customers_gross)) from marketing_report.retargeting_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Colombia') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_retargeting_report t set e_new_customers = new_customers_gross*e_new_customers where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_new_customers = case when e_new_customers>new_customers then e_new_customers else new_customers end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set cancel = (select sum(new_customers_gross) from production_co.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.`source/medium` and t.campaign=d.campaign and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set returns = (select sum(new_customers_gross) from production_co.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.`source/medium` and t.campaign=d.campaign and t.date=d.date and d.returned=1) where datediff(curdate(), date)<90; 

update marketing_report.temporary_retargeting_report t set cancel = cancel/new_customers_gross, returns = returns/new_customers_gross where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set ee_new_customers = new_customers_gross*(1-ifnull(cancel,0)-ifnull(returns,0)) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_new_customers = case when e_new_customers>ee_new_customers then ee_new_customers else e_new_customers end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_new_customers = case when e_new_customers>new_customers then e_new_customers else new_customers end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t inner join marketing_report.retargeting_report g on t.date=g.date and t.source=g.source and t.medium=g.medium and t.campaign=g.campaign set g.e_transactions=t.e_transactions, g.e_revenue=t.e_revenue, g.e_new_customers=t.e_new_customers where country = 'Colombia' and datediff(curdate(), t.date)<90;

drop table marketing_report.temporary_retargeting_report;

-- Estimated

-- Moving Avg

drop table if exists marketing_report.temporary_retargeting_report;
create table marketing_report.temporary_retargeting_report(
date date,
source varchar(255),
medium varchar(255),
campaign varchar(255),
gross_transactions int not null default 0,
net_transactions int not null default 0,
e_transactions float not null default 0,
ee_transactions float not null default 0,
gross_revenue float not null default 0,
net_revenue float not null default 0,
e_revenue float not null default 0,
ee_revenue float not null default 0,
new_customers_gross int not null default 0,
new_customers int not null default 0,
e_new_customers float not null default 0,
ee_new_customers float not null default 0,
cancel float,
returns float,
voucher float
);

create index temporary_mkt on marketing_report.temporary_retargeting_report(date, source, medium, campaign);

insert into marketing_report.temporary_retargeting_report(date, source, medium, campaign, gross_transactions, net_transactions, gross_revenue, net_revenue, new_customers_gross, new_customers) select date, source, medium, campaign, gross_transactions, net_transactions, gross_revenue, net_revenue, new_customers_gross, new_customers from marketing_report.retargeting_report where country='Peru' and datediff(curdate(), date)<90;

-- Orders

set @date=date_sub(curdate(), interval 90 day);

while @date!=curdate() do
update marketing_report.temporary_retargeting_report t set e_transactions = (select (avg(g.net_transactions/g.gross_transactions)) from marketing_report.retargeting_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Peru') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_retargeting_report t set e_transactions = gross_transactions*e_transactions where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_transactions = case when e_transactions>net_transactions then e_transactions else net_transactions end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set cancel = (select count(distinct order_nr) from production_pe.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.source_medium and t.campaign=d.campaign and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set returns = (select count(distinct order_nr) from production_pe.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.source_medium and t.campaign=d.campaign and t.date=d.date and d.returned=1) where datediff(curdate(), date)<90; 

update marketing_report.temporary_retargeting_report t set cancel = cancel/gross_transactions, returns = returns/gross_transactions where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set ee_transactions = gross_transactions*(1-ifnull(cancel,0)-ifnull(returns,0)) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_transactions = case when e_transactions>ee_transactions then ee_transactions else e_transactions end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_transactions = case when e_transactions>net_transactions then e_transactions else net_transactions end where datediff(curdate(), date)<90;

-- Revenue

set @date=date_sub(curdate(), interval 90 day);

while @date!=curdate() do
update marketing_report.temporary_retargeting_report t set e_revenue = (select (avg(g.net_revenue/g.gross_revenue)) from marketing_report.retargeting_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Peru') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_retargeting_report t set e_revenue = gross_revenue*e_revenue where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_revenue = case when e_revenue>net_revenue then e_revenue else net_revenue end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set cancel = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production_pe.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.source_medium and t.campaign=d.campaign and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set returns = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production_pe.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.source_medium and t.campaign=d.campaign and t.date=d.date and d.returned=1) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set voucher = (select sum(coupon_money_after_vat) from production_pe.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.source_medium and t.campaign=d.campaign and t.date=d.date and d.coupon_code is not null and d.oac=1) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set cancel = cancel/gross_revenue, returns = returns/gross_revenue, voucher=voucher/gross_revenue where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set ee_revenue = gross_revenue*(1-ifnull(cancel,0)-ifnull(returns,0)-ifnull(voucher,0)) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_revenue = case when e_revenue>ee_revenue then ee_revenue else e_revenue end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_revenue = case when e_revenue>net_revenue then e_revenue else net_revenue end where datediff(curdate(), date)<90;

-- New Customers

set @date=date_sub(curdate(), interval 90 day);

while @date!=curdate() do
update marketing_report.temporary_retargeting_report t set e_new_customers = (select (avg(g.new_customers/g.new_customers_gross)) from marketing_report.retargeting_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Peru') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_retargeting_report t set e_new_customers = new_customers_gross*e_new_customers where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_new_customers = case when e_new_customers>new_customers then e_new_customers else new_customers end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set cancel = (select sum(new_customers_gross) from production_pe.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.source_medium and t.campaign=d.campaign and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set returns = (select sum(new_customers_gross) from production_pe.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.source_medium and t.campaign=d.campaign and t.date=d.date and d.returned=1) where datediff(curdate(), date)<90; 

update marketing_report.temporary_retargeting_report t set cancel = cancel/new_customers_gross, returns = returns/new_customers_gross where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set ee_new_customers = new_customers_gross*(1-ifnull(cancel,0)-ifnull(returns,0)) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_new_customers = case when e_new_customers>ee_new_customers then ee_new_customers else e_new_customers end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_new_customers = case when e_new_customers>new_customers then e_new_customers else new_customers end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t inner join marketing_report.retargeting_report g on t.date=g.date and t.source=g.source and t.medium=g.medium and t.campaign=g.campaign set g.e_transactions=t.e_transactions, g.e_revenue=t.e_revenue, g.e_new_customers=t.e_new_customers where country = 'Peru' and datediff(curdate(), t.date)<90;

drop table marketing_report.temporary_retargeting_report;

-- Estimated

-- Moving Avg

drop table if exists marketing_report.temporary_retargeting_report;
create table marketing_report.temporary_retargeting_report(
date date,
source varchar(255),
medium varchar(255),
campaign varchar(255),
gross_transactions int not null default 0,
net_transactions int not null default 0,
e_transactions float not null default 0,
ee_transactions float not null default 0,
gross_revenue float not null default 0,
net_revenue float not null default 0,
e_revenue float not null default 0,
ee_revenue float not null default 0,
new_customers_gross int not null default 0,
new_customers int not null default 0,
e_new_customers float not null default 0,
ee_new_customers float not null default 0,
cancel float,
returns float,
voucher float
);

create index temporary_mkt on marketing_report.temporary_retargeting_report(date, source, medium, campaign);

insert into marketing_report.temporary_retargeting_report(date, source, medium, campaign, gross_transactions, net_transactions, gross_revenue, net_revenue, new_customers_gross, new_customers) select date, source, medium, campaign, gross_transactions, net_transactions, gross_revenue, net_revenue, new_customers_gross, new_customers from marketing_report.retargeting_report where country='Venezuela' and datediff(curdate(), date)<90;

-- Orders

set @date=date_sub(curdate(), interval 90 day);

while @date!=curdate() do
update marketing_report.temporary_retargeting_report t set e_transactions = (select (avg(g.net_transactions/g.gross_transactions)) from marketing_report.retargeting_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Venezuela') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_retargeting_report t set e_transactions = gross_transactions*e_transactions where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_transactions = case when e_transactions>net_transactions then e_transactions else net_transactions end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set cancel = (select count(distinct order_nr) from production_ve.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.source_medium and t.campaign=d.campaign and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set returns = (select count(distinct order_nr) from production_ve.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.source_medium and t.campaign=d.campaign and t.date=d.date and d.returned=1) where datediff(curdate(), date)<90; 

update marketing_report.temporary_retargeting_report t set cancel = cancel/gross_transactions, returns = returns/gross_transactions where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set ee_transactions = gross_transactions*(1-ifnull(cancel,0)-ifnull(returns,0)) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_transactions = case when e_transactions>ee_transactions then ee_transactions else e_transactions end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_transactions = case when e_transactions>net_transactions then e_transactions else net_transactions end where datediff(curdate(), date)<90;

-- Revenue

set @date=date_sub(curdate(), interval 90 day);

while @date!=curdate() do
update marketing_report.temporary_retargeting_report t set e_revenue = (select (avg(g.net_revenue/g.gross_revenue)) from marketing_report.retargeting_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Venezuela') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_retargeting_report t set e_revenue = gross_revenue*e_revenue where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_revenue = case when e_revenue>net_revenue then e_revenue else net_revenue end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set cancel = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production_ve.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.source_medium and t.campaign=d.campaign and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set returns = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production_ve.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.source_medium and t.campaign=d.campaign and t.date=d.date and d.returned=1) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set voucher = (select sum(coupon_money_after_vat) from production_ve.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.source_medium and t.campaign=d.campaign and t.date=d.date and d.coupon_code is not null and d.oac=1) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set cancel = cancel/gross_revenue, returns = returns/gross_revenue, voucher=voucher/gross_revenue where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set ee_revenue = gross_revenue*(1-ifnull(cancel,0)-ifnull(returns,0)-ifnull(voucher,0)) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_revenue = case when e_revenue>ee_revenue then ee_revenue else e_revenue end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_revenue = case when e_revenue>net_revenue then e_revenue else net_revenue end where datediff(curdate(), date)<90;

-- New Customers

set @date=date_sub(curdate(), interval 90 day);

while @date!=curdate() do
update marketing_report.temporary_retargeting_report t set e_new_customers = (select (avg(g.new_customers/g.new_customers_gross)) from marketing_report.retargeting_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Venezuela') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_retargeting_report t set e_new_customers = new_customers_gross*e_new_customers where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_new_customers = case when e_new_customers>new_customers then e_new_customers else new_customers end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set cancel = (select sum(new_customers_gross) from production_ve.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.source_medium and t.campaign=d.campaign and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set returns = (select sum(new_customers_gross) from production_ve.tbl_order_detail d where concat(t.source, ' / ', t.medium)=d.source_medium and t.campaign=d.campaign and t.date=d.date and d.returned=1) where datediff(curdate(), date)<90; 

update marketing_report.temporary_retargeting_report t set cancel = cancel/new_customers_gross, returns = returns/new_customers_gross where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set ee_new_customers = new_customers_gross*(1-ifnull(cancel,0)-ifnull(returns,0)) where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_new_customers = case when e_new_customers>ee_new_customers then ee_new_customers else e_new_customers end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t set e_new_customers = case when e_new_customers>new_customers then e_new_customers else new_customers end where datediff(curdate(), date)<90;

update marketing_report.temporary_retargeting_report t inner join marketing_report.retargeting_report g on t.date=g.date and t.source=g.source and t.medium=g.medium and t.campaign=g.campaign set g.e_transactions=t.e_transactions, g.e_revenue=t.e_revenue, g.e_new_customers=t.e_new_customers where country = 'Venezuela' and datediff(curdate(), t.date)<90;

drop table marketing_report.temporary_retargeting_report;

update marketing_report.retargeting_report s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set e_revenue=e_revenue/xr where s.country = 'Mexico' and r.country='MEX' and datediff(curdate(), s.date)<90;

update marketing_report.retargeting_report s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set e_revenue=e_revenue/xr where s.country = 'Colombia' and r.country='COL' and datediff(curdate(), s.date)<90;

update marketing_report.retargeting_report s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set e_revenue=e_revenue/xr where s.country = 'Peru' and r.country='PER' and datediff(curdate(), s.date)<90;

update marketing_report.retargeting_report s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set e_revenue=e_revenue/xr where s.country = 'Venezuela' and r.country='VEN' and datediff(curdate(), s.date)<90;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `test19`()
begin

set @days=180;

-- Attribution Compensation

create table attribution_testing.compensation_attribution_co as select date, channel, 0 as flag from attribution_testing.attribution_model_co group by date, channel;

create index date on attribution_testing.compensation_attribution_co(date);
create index channel on attribution_testing.compensation_attribution_co(channel);

create table attribution_testing.compensation_global_co as select date, channel_group as channel from marketing_report.global_report where country='Colombia' group by date, channel_group;

create index date on attribution_testing.compensation_global_co(date);
create index channel on attribution_testing.compensation_global_co(channel);

update attribution_testing.compensation_attribution_co a inner join attribution_testing.compensation_global_co g on a.date=g.date and a.channel=g.channel set a.flag=1;

insert into marketing_report.global_report(country, yrmonth, week, date, weekday, channel_group, channel) select 'Colombia', concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), production.week_iso(date) as week, date, case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end, channel, channel from attribution_testing.compensation_attribution_co where flag=0 and datediff(curdate(), date)<@days; 

drop table attribution_testing.compensation_attribution_co;

drop table attribution_testing.compensation_global_co;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `test39`()
begin

delete from attribution_testing.output_marketing_campaign_tool;

insert into attribution_testing.output_marketing_campaign_tool(date, product, channel, campaign, add_to_cart, product_views, clicks, impressions, cost, unit_sold) select c.date, c.product, c.channel, c.campaign, c.add_to_cart, c.product_views, c.clicks, c.impressions, c.cost, c.unit_sold from attribution_testing.marketing_campaign_tool c, attribution_testing.input_marketing_campaign_tool l where c.date between l.start_date and l.end_date and c.product=l.product group by c.date, c.product, c.channel, c.campaign;

-- Product Views

create table attribution_testing.temporary_date_range(
date date,
product varchar(255),
product_views int,
last_period_add_to_cart float,
last_period_product_views float,
last_period_clicks float,
last_period_unit_sold float
);

create index date on attribution_testing.temporary_date_range(date);
create index product on attribution_testing.temporary_date_range(product);

insert into attribution_testing.temporary_date_range(date, product, product_views) select c.date, c.product, sum(c.product_views) as product_views from attribution_testing.marketing_campaign_tool c, attribution_testing.input_marketing_campaign_tool l where c.product=l.product group by c.date, c.product;

set @id=1;

while @id<= (select max(id) from attribution_testing.input_marketing_campaign_tool) do

set @start_date:= (select start_date from attribution_testing.input_marketing_campaign_tool where id=@id);
set @end_date:=(select end_date from attribution_testing.input_marketing_campaign_tool where id=@id);
set @days:=(select days from attribution_testing.input_marketing_campaign_tool where id=@id);
set @product:=(select product from attribution_testing.input_marketing_campaign_tool where id=@id);

##while @start_date!=adddate(@end_date, interval 1 day) do

if @start_date!=@end_date then
while @start_date!=@end_date do
update attribution_testing.temporary_date_range t set last_period_product_views = (select sum(product_views) from attribution_testing.marketing_campaign_tool g where g.date = date_sub(@start_date, interval @days day) and t.product=g.product) where t.date=@start_date;
set @start_date:=adddate(@start_date, interval 1 day);
end while;
end if;

/*
if @days=1 then 
update attribution_testing.temporary_date_range t set last_period_product_views = (select sum(product_views) from attribution_testing.marketing_campaign_tool g where g.date = date_sub(@start_date, interval @days day) and t.product=g.product and g.product=@product) where t.date=@start_date;
end if;*/

set @id=@id+1;

end while;

delete from attribution_testing.temporary_date_range where last_period_product_views is null;

##drop table attribution_testing.temporary_date_range;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `test59`()
BEGIN

update marketing_report.channel_report_ve t inner join SEM.transaction_id_ve c on t.ordernum=c.transaction_id set t.source_medium= concat(c.source,' / ',c.medium),t.campaign=c.campaign where source_medium is null;

#Channel Report---VENEZUELA

#Define channel---
##UPDATE marketing_report.channel_report_ve SET source='',channel='', channel_group='';
#Voucher is priority statemnt
#If voucher is empty use GA source
UPDATE marketing_report.channel_report_ve SET channel='' where channel is null;
UPDATE marketing_report.channel_report_ve SET channel_group='' where channel_group is null;
UPDATE marketing_report.channel_report_ve SET source='' where source is null;
UPDATE marketing_report.channel_report_ve SET campaign='' where campaign is null;
UPDATE marketing_report.channel_report_ve SET source=source_medium;
UPDATE marketing_report.channel_report_ve SET source=coupon_code where source_medium is null;
UPDATE marketing_report.channel_report_ve SET source=source_medium where channel_group='Non Identified';

#NEW REGISTERS
UPDATE marketing_report.channel_report_ve SET channel='New Register' 
WHERE (source like 'NL%' OR source like 'NR%' OR source like
'COM%' OR source like 'BNR%') AND source_medium is null;
UPDATE marketing_report.channel_report_ve SET source=source_medium
WHERE (coupon_code like 'NL%' OR coupon_code='SINIVA' OR coupon_code like 'COM%' 
OR coupon_code like 'NR%' OR coupon_code like 'BNR%')
 AND source_medium is not null;
#BLOG
-- UPDATE marketing_report.channel_report_ve SET channel='Blog Linio' WHERE source like 'blog%' 
-- OR source='blog.linio.com.ve / referral' or source like 'BL%';

#REFERRAL
UPDATE marketing_report.channel_report_ve SET channel='Referral Sites' WHERE source like '%referral%' 
AND source<>'facebook.com / referral'
AND source<>'m.facebook.com / referral'
AND source<>'linio.com.co / referral'
AND source<>'linio.com / referral'
AND source<>'linio.com.mx / referral'
AND source<>'google.co.ve / referral'
AND source<>'linio.com.ve / referral'
AND source<>'linio.com.pe / referral'
AND source<>'www-staging.linio.com.ve / referral'
AND source<>'google.com / referral'
AND source<>'.com.ve / referral'
AND source<>'blog.linio.com.ve / referral' AND source<>'t.co / referral'
AND source not like '%google%' AND source not like '%blog%';

UPDATE marketing_report.channel_report_ve SET channel='Other Affiliates' where source like '%affiliate%' or source like '%afiliate%';

UPDATE marketing_report.channel_report_ve set channel='SoloCPM' where source like '%solocpm%';

UPDATE marketing_report.channel_report_ve set channel='PromoDescuentos' where source like '%promodescuentos%';

UPDATE marketing_report.channel_report_ve SET channel='Buscape' WHERE source='Buscape / Price Comparison';
#FB ADS
UPDATE marketing_report.channel_report_ve SET channel='Facebook Ads' WHERE source='facebook / socialmediaads' or source='facebook / socialmediapaid' or source='facebook / (not set)';

UPDATE marketing_report.channel_report_ve set channel= 'Dark Post' where campaign like '%darkpost%';

#SEM - GDN
UPDATE marketing_report.channel_report_ve SET channel='Bing' where source like '%bing%';
UPDATE marketing_report.channel_report_ve SET channel='SEM Branded' WHERE (source='google / cpc' or source like '%bing%') AND campaign like '%linio%';
UPDATE marketing_report.channel_report_ve SET channel='SEM' WHERE source='google / cpc' AND campaign not like '%linio%';
UPDATE marketing_report.channel_report_ve SET channel='Other Branded' WHERE source = '201.151.86.164' or source ='10.0.0.9' or source = 'www-staging.linio.com.mx' or source='blog.linio.com.mx' or source='213.229.186.15';


#GDN
UPDATE marketing_report.channel_report_ve SET channel='Google Display Network' WHERE source='google / cpc' AND 
(campaign like 'r.%' or campaign like '[D%' or campaign like '%d.%');

#RETARGETING
UPDATE marketing_report.channel_report_ve set channel='Other Retargeting' where source like '%retargeting%';
UPDATE marketing_report.channel_report_ve SET channel='Sociomantic' WHERE source='sociomantic / (not set)' OR source='sociomantic / retargeting' or source='facebook / retargeting';
UPDATE marketing_report.channel_report_ve SET channel='Vizury' WHERE source='vizury / retargeting' OR source='vizury.com / referral';
UPDATE marketing_report.channel_report_ve SET channel='VEInteractive' WHERE source like '%VEInteractive%';
UPDATE marketing_report.channel_report_ve SET channel='Main ADV' where source in ('retargeting / mainadv', 'mainadv / retargeting') or source like '%solocpm%';
UPDATE marketing_report.channel_report_ve SET channel='AdRoll' where source like '%adroll%';

#RETARGETING
#UPDATE marketing_report.channel_report_ve SET channel='Facebook R.' WHERE source='facebook / retargeting';
#Criteo
UPDATE marketing_report.channel_report_ve set channel='Criteo' where source like '%criteo%';
#PAMPA
UPDATE marketing_report.channel_report_ve set channel='Pampa Network' where source like '%pampa%';
#SOCIAL MEDIA
UPDATE marketing_report.channel_report_ve SET channel='Facebook Referral' WHERE 
(source='facebook.com / referral' OR source='m.facebook.com / referral') AND source not like '%socialmediaads%';
UPDATE marketing_report.channel_report_ve SET channel='Twitter Referral' WHERE source='t.co / referral' OR source='twitter / socialmedia';
UPDATE marketing_report.channel_report_ve SET channel='FB Posts' 
WHERE source='facebook / socialmedia' OR source='SocialMedia / FacebookVoucher' or source='twitter / socialmedia';
#SERVICIO AL CLIENTE
UPDATE marketing_report.channel_report_ve set channel='Other Tele Sales' where source = 'CS /%' or source = '%/ CS' or source like 'telesales /%' or source like '%/ telesales'; 
UPDATE marketing_report.channel_report_ve SET channel='Inbound' WHERE source like 'Tele%' or source like '%inbound%' or source like '%outbound%';
UPDATE marketing_report.channel_report_ve SET channel='OutBound' WHERE source like 'REC%';
UPDATE marketing_report.channel_report_ve SET channel='CS Generic Voucher' WHERE source like 'CCemp%';

UPDATE marketing_report.channel_report_ve SET channel='Search Organic' WHERE source like '%organic' OR source='google.com.co / referral';

UPDATE marketing_report.channel_report_ve SET channel='Search Organic' where source like 'google.%' and source like '%referral';

UPDATE marketing_report.channel_report_ve SET channel='Yahoo Answers' where source like '%answers.yahoo.com%';

#NEWSLETTER
UPDATE marketing_report.channel_report_ve set Channel='Newsletter' WHERE source like '%CRM' AND source<>'TeleSales / CRM' or source like '%email%';
UPDATE marketing_report.channel_report_ve SET channel='MKT Bs. 50' WHERE source='MKT02kVLM';
UPDATE marketing_report.channel_report_ve SET channel='MKT 15%' WHERE source='MKT048S8J';
UPDATE marketing_report.channel_report_ve SET channel='MKT Bs. 50' WHERE source='MKT04GPXB';
UPDATE marketing_report.channel_report_ve SET channel='MKT 15% hasta 31 de diciembre' WHERE source='MKT06Ru';
UPDATE marketing_report.channel_report_ve SET channel='MKT Bs. 50' WHERE source='MKT07kiKz';
UPDATE marketing_report.channel_report_ve SET channel='MKT 15%' WHERE source='MKT0a3YVI';
UPDATE marketing_report.channel_report_ve SET channel='MKT 50Bs  hasta 31 de diciembre' WHERE source='MKT0FTh';
UPDATE marketing_report.channel_report_ve SET channel='Tramontina Utilita Sacarcorcho' WHERE source='MKT0hAb7W';
UPDATE marketing_report.channel_report_ve SET channel='Victorinox Memoria USB Slim 4GB Verde' WHERE source='MKT0iuXXU';
UPDATE marketing_report.channel_report_ve SET channel='HP FlashDrive Pen 4GB Usb V195B Azul' WHERE source='MKT0j2Efo';
UPDATE marketing_report.channel_report_ve SET channel='GBTech Audífono GBTech X5 GBX5 Amarillo' WHERE source='MKT0oShGI';
UPDATE marketing_report.channel_report_ve SET channel='Perfect Choice Audifono Diadema PC-110309 Con Banda Para Cuello' WHERE source='MKT0P2Bsq';
UPDATE marketing_report.channel_report_ve SET channel='MKT Bs. 50' WHERE source='MKT0qCcAP';
UPDATE marketing_report.channel_report_ve SET channel='Decocar Combo Tropical Azul' WHERE source='MKT0rjeet';
UPDATE marketing_report.channel_report_ve SET channel='MKT 15%' WHERE source='MKT0WFMCm';
UPDATE marketing_report.channel_report_ve SET channel='Ocean Vaso Corto Studio Set 6' WHERE source='MKT0yj5mw';
UPDATE marketing_report.channel_report_ve SET channel='MKT Bs. 50' WHERE source='MKT0Z8YYn';
UPDATE marketing_report.channel_report_ve SET channel='X-mini Altavoz Capsule v1.1 Rojo' WHERE source='MKT10hQfI';
UPDATE marketing_report.channel_report_ve SET channel='HP P-FD4GBHP165-GEL Pendrive USB' WHERE source='MKT11vw0x';
UPDATE marketing_report.channel_report_ve SET channel='SABA Set de 2 Cuchillos Colores' WHERE source='MKT12jfHL';
UPDATE marketing_report.channel_report_ve SET channel='MKT Bs. 50' WHERE source='MKT14zmPN';
UPDATE marketing_report.channel_report_ve SET channel='Kingston DT101G2/4GB PenDrive USB' WHERE source='MKT16mWW7';
UPDATE marketing_report.channel_report_ve SET channel='Ifrogz Audífono EarPollution Plugz Azul Cielo' WHERE source='MKT18qw1j';
UPDATE marketing_report.channel_report_ve SET channel='MKT 30%' WHERE source='MKT1DcqIV';
UPDATE marketing_report.channel_report_ve SET channel='GENIUS Speakers 31731006103 Blanco' WHERE source='MKT1Hn1B5';
UPDATE marketing_report.channel_report_ve SET channel='Bajo Techo Pizarra Autoadhesiva Tipo Vinil' WHERE source='MKT1HWmFm';
UPDATE marketing_report.channel_report_ve SET channel='MKT 15%' WHERE source='MKT1i3zA8';
UPDATE marketing_report.channel_report_ve SET channel='MKT Bs. 50' WHERE source='MKT1ktP10';
UPDATE marketing_report.channel_report_ve SET channel='Desarmador trompo con matraca con 12 puntas' WHERE source='MKT1ORfvU';
UPDATE marketing_report.channel_report_ve SET channel='SABA – Taza de Porcelana Rayas (Facebook)' WHERE source='MKT1P4NbB';
UPDATE marketing_report.channel_report_ve SET channel='MKT 15%' WHERE source='MKT1rTCoJ';
UPDATE marketing_report.channel_report_ve SET channel='MKT Bs. 50' WHERE source='MKT1xLsRR';
UPDATE marketing_report.channel_report_ve SET channel='Botella de Alumino para Agua No soy de plástico Gaiam-Plata' WHERE source='MKT1Z54Lh';
UPDATE marketing_report.channel_report_ve SET channel='MKT 15% hasta 31 de diciembre' WHERE source='MKT1ZtV';
UPDATE marketing_report.channel_report_ve SET channel='SABA – Taza de Porcelana Rayas ' WHERE source='MKT2ww6wJ';
UPDATE marketing_report.channel_report_ve SET channel='SABA – Taza de Porcelana Rayas ' WHERE source='MKT7XzxhT';
UPDATE marketing_report.channel_report_ve SET channel='Cupón de reemplazo a cupón de Mkt' WHERE source='MKTAeF';
UPDATE marketing_report.channel_report_ve SET channel='MKT 15%' WHERE source='MKTbS3gmp';
UPDATE marketing_report.channel_report_ve SET channel='MKT 15%' WHERE source='MKTcwB9vv';
UPDATE marketing_report.channel_report_ve SET channel='SABA Porta Botellas Neopreno Negro' WHERE source='MKTdtlbpI';
UPDATE marketing_report.channel_report_ve SET channel='ILUV Audífonos in ear Bubble Gum Rojo IEP205-R' WHERE source='MKTECOjZv';
UPDATE marketing_report.channel_report_ve SET channel='Reemplazo cupón de Marketing' WHERE source='MKTenQ';
UPDATE marketing_report.channel_report_ve SET channel='Tostador Proctor Silex 24605Y 4 Rebanadas-Blanco' WHERE source='MKTf4A6Sk';
UPDATE marketing_report.channel_report_ve SET channel='HP P-FD4GBHP165-GEL Pendrive USB' WHERE source='MKTf8VV4U';
UPDATE marketing_report.channel_report_ve SET channel='Morrocoy Silla de Director' WHERE source='MKTGOhXfE';
UPDATE marketing_report.channel_report_ve SET channel='Kalosh Almohada 95% Pluma y 5% Plumón Blanca' WHERE source='MKTkZ047R';
UPDATE marketing_report.channel_report_ve SET channel='MKT Bs. 50' WHERE source='MKTLf9Mdg';
UPDATE marketing_report.channel_report_ve SET channel='Kingston Tarjeta de memoria SDC4/4GB-1ADP' WHERE source='MKTljJ3QL';
UPDATE marketing_report.channel_report_ve SET channel='PRO CHEF Parrillera Hamburguer' WHERE source='MKTmCN60a';
UPDATE marketing_report.channel_report_ve SET channel='Credito por bono no funcionando 2' WHERE source='MKTMKT';
UPDATE marketing_report.channel_report_ve SET channel='Credito por bono no funcionando 3' WHERE source='MKTMKT250';
UPDATE marketing_report.channel_report_ve SET channel='Contigo West loop Vaso Térmico con autosello 16 Oz/ 473 ml Rojo' WHERE source='MKToVzJ1d';
UPDATE marketing_report.channel_report_ve SET channel='MKT 50Bs  hasta 31 de diciembre' WHERE source='MKTQaQk';
UPDATE marketing_report.channel_report_ve SET channel='MKT 15%' WHERE source='MKTr1jvkH';
UPDATE marketing_report.channel_report_ve SET channel='ARGOM Teclado multimedia en español ARG-KB-7805 Negro' WHERE source='MKTr2DzaC';
UPDATE marketing_report.channel_report_ve SET channel='GBTech MP4 GBTech GB1106 Azul 4 GB' WHERE source='MKTRp7vye';
UPDATE marketing_report.channel_report_ve SET channel='SABA Set de 3 utensilios para Parrilla' WHERE source='MKTsd9V9B';
UPDATE marketing_report.channel_report_ve SET channel='Decocar Cava Americana 11,4Lt Roja' WHERE source='MKTthEw98';
UPDATE marketing_report.channel_report_ve SET channel='MKT Bs. 50' WHERE source='MKTuWtOQ3';
UPDATE marketing_report.channel_report_ve SET channel='Marketing en ferias' WHERE source='MKTVngFxL';
UPDATE marketing_report.channel_report_ve SET channel='SABA - Dispensador de Aceite y Vinagre 2 en 1 / SABA-511 / Transparente' WHERE source='MKTvpeCte';
UPDATE marketing_report.channel_report_ve SET channel='MKT 30%' WHERE source='MKTy6v5rt';
UPDATE marketing_report.channel_report_ve SET channel='Easy Line Mouse Retráctil EL-993346 Negro' WHERE source='MKTzBMwc3';
UPDATE marketing_report.channel_report_ve SET channel='Reactivation campaign' WHERE source='MKTzOcbUq';



#OTHER
UPDATE marketing_report.channel_report_ve SET channel='Exchange' WHERE source like 'CCE%';
UPDATE marketing_report.channel_report_ve SET channel='Content Mistake' WHERE source like 'CON%';
UPDATE marketing_report.channel_report_ve SET channel='Procurement Mistake' WHERE source like 'PRC%';
UPDATE marketing_report.channel_report_ve SET channel='Employee Vouchers' 
WHERE (source like 'TE%' AND source<>'TeleSales / CRM') OR source like 'EA%';


#MERCADO LIBRE
UPDATE marketing_report.channel_report_ve SET channel='Mercado Libre Voucher' WHERE source like 'ML%';

#PERIODISTAS

UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKT02kER0';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKT08jq2i';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKT0arRZF';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKT0hoq3F';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKT0JgIhn';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKT0LeO2h';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKT0Olwjk';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKT1d8pkJ';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKT1fspLY';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKT1h2uie';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKT1jzjKP';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKT1llem9';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKT1Pk3Ap';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKT1u1JSo';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKT1v5kLS';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKT1Yz6yi';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKT4rXdCS';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKTA0aIrT';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKTac0eLL';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKTCcHdRX';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKThMerN5';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKThNplZF';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKTiE46NZ';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKTJy4dxm';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKTlzVdEY';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKTmvE0F7';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKTn7VdJO';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKTna93IX';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKTpYM3yV';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKTQdcuzY';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKTrI3ds7';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKTS55mkn';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKTSR8lJR';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKTt1kyoi';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKTTRutlc';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKTuUrfbj';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKTvkHUlg';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKTvsJVH4';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKTwdi2uO';
UPDATE marketing_report.channel_report_ve SET channel='Cupón para periodistas' WHERE source='MKTXVRpth';





#PARTNERSHIPS
UPDATE marketing_report.channel_report_ve SET channel='Other Partnerships' where source like '%PARTNERSHIP%' or coupon_code like '%PARTNERSHIP%';

#DIRECT
UPDATE marketing_report.channel_report_ve SET channel='Linio.com Referral' WHERE  
source='linio.com.co / referral' OR
source='linio.com / referral' OR
source='linio.com.mx / referral' OR
source='linio.com.ve / referral' OR
source='linio.com.pe / referral' OR
source='www-staging.linio.com.ve / referral';
UPDATE marketing_report.channel_report_ve SET channel='Directo / Typed' WHERE  source='(direct) / (none)';



#CORPORATE SALES
UPDATE marketing_report.channel_report_ve SET channel='Corporate Sales' WHERE source like 'CDEAL%';

UPDATE marketing_report.channel_report_ve set channel='Corporate Sales' where coupon_code LIKE 'CDEAL%' or source_medium like '%CORPORATESALES%';


#OFFLINE
UPDATE marketing_report.channel_report_ve SET channel='SMS' where coupon_code in ('VIERNES10',
'NOCTURNO',
'USB01',
'ELECTROLINIO',
'TODOLINIO',
'LINIO',
'VIERNESLOCO');

UPDATE marketing_report.channel_report_ve set channel='Valla' where coupon_code in ('GUAIRITA',
'PUENTE',
'HIERRO',
'TRINIDAD',
'BANDERA',
'BRISAS'
);

UPDATE marketing_report.channel_report_ve set channel='Publitruck' where coupon_code = 'CARACAS'; 

UPDATE marketing_report.channel_report_ve set channel='Paradas Bus' where coupon_code in ('LIBERTADOR',
'MERCEDES',
'CHACAITO',
'PALOS',
'BOLEITA',
'ALEGRE',
'CAFETAL',
'GALLEGOS',
'CAURIMARE',
'PAULA',
'INTERCOMUNAL',
'VALLE',
'CARICUAO',
'BOLIVAR',
'BELLO',
'MEDINA',
'MONTE',
'REDOMA',
'TAHONA',
'MANZANARES',
'ILUSTRES',
'MARTIN',
'ANTIMANO',
'PAEZ',
'CASANOVA'
);


#N/A
UPDATE marketing_report.channel_report_ve SET channel='Unknown-No voucher' WHERE source='';
UPDATE marketing_report.channel_report_ve SET channel='Unknown-Voucher' WHERE channel='' AND source<>'';

#CAC Vouchers
##UPDATE marketing_report.channel_report_ve set channel = 'CAC Deals' where (coupon_code like 'CAC%');
UPDATE marketing_report.channel_report_ve set channel='FB Ads CAC' WHERE (coupon_code like 'CAC%') and source_medium='facebook / socialmediaads';
UPDATE marketing_report.channel_report_ve set channel='FB CAC' WHERE (coupon_code like 'CAC%') and source_medium='facebook / socialmedia';
UPDATE marketing_report.channel_report_ve set channel='NL CAC' WHERE coupon_code like 'CAC%' and source_medium='postal / crm';

#Channel Report Extra Stuff to define Channel---
#Venta corporativa 

#Curebit
UPDATE marketing_report.channel_report_ve set channel = 'Curebit' where coupon_code like '%cbit%' or source_medium like '%curebit%';

UPDATE marketing_report.channel_report_ve t inner join production_ve.mobileapp_transaction_id m on t.ordernum=m.transaction_id set channel = 'Mobile App';

#Partnerships Channel Extra


#Mercado libre Orders 


#DEFINE CHANNEL GROUP

UPDATE marketing_report.channel_report_ve SET channel_group='Mobile App' WHERE channel='Mobile App';

##UPDATE marketing_report.channel_report_ve set channel_group ='Corporate Sales' where channel='Corporate Sales';

#Facebook Ads Group
UPDATE marketing_report.channel_report_ve SET channel_group='Facebook Ads' WHERE channel='Facebook Ads' or channel = 'Dark Post' or channel='Facebook Referral' or channel='FB Ads CAC';

#OTHER
UPDATE marketing_report.channel_report_ve SET channel_group='Mercado Libre ' WHERE channel='Mercado Libre Voucher';

#SEM Group
UPDATE marketing_report.channel_report_ve SET channel_group='Search Engine Marketing' WHERE channel='SEM' or channel='Bing' or channel='Other Branded';

#Social Media Group
UPDATE marketing_report.channel_report_ve SET channel_group='Social Media' 
WHERE channel='Twitter Referral' OR
channel='FB Posts' or channel='Youtube' or channel='FB CAC';
UPDATE marketing_report.channel_report_ve SET channel='Youtube' where source='youtube / socialmedia';

#Referal Platform
UPDATE marketing_report.channel_report_ve set channel_group = 'Referal Platform' where channel='Curebit';

#Retargeting Group
UPDATE marketing_report.channel_report_ve SET channel_group='Retargeting' WHERE channel='Sociomantic' OR channel like '%retargeting'
OR channel='Facebook R.' OR channel='Vizury' OR channel = 'VEInteractive' OR channel = 'GDN Retargeting' OR channel = 'Criteo' or channel='Main ADV' or channel='AdRoll' or channel='Other Retargeting';

#GDN Group
UPDATE marketing_report.channel_report_ve SET channel_group='Google Display Network' WHERE channel like 'GDN' or channel='Yeei'; 

#NewsLetter Group
UPDATE marketing_report.channel_report_ve SET channel_group='NewsLetter' WHERE channel='Newsletter' or channel='NL CAC';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='MKT 15% hasta 31 de diciembre';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='MKT 50Bs  hasta 31 de diciembre';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='Tramontina Utilita Sacarcorcho';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='Victorinox Memoria USB Slim 4GB Verde';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='HP FlashDrive Pen 4GB Usb V195B Azul';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='GBTech Audífono GBTech X5 GBX5 Amarillo';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='Perfect Choice Audifono Diadema PC-110309 Con Banda Para Cuello';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='Decocar Combo Tropical Azul';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='Ocean Vaso Corto Studio Set 6';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='X-mini Altavoz Capsule v1.1 Rojo';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='HP P-FD4GBHP165-GEL Pendrive USB';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='SABA Set de 2 Cuchillos Colores';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='Kingston DT101G2/4GB PenDrive USB';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='Ifrogz Audífono EarPollution Plugz Azul Cielo';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='MKT 30%';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='GENIUS Speakers 31731006103 Blanco';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='Bajo Techo Pizarra Autoadhesiva Tipo Vinil';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='Desarmador trompo con matraca con 12 puntas';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='SABA – Taza de Porcelana Rayas (Facebook)';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='Botella de Alumino para Agua No soy de plástico Gaiam-Plata';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='MKT 15% hasta 31 de diciembre';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='SABA – Taza de Porcelana Rayas ';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='SABA – Taza de Porcelana Rayas ';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='Cupón de reemplazo a cupón de Mkt';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='SABA Porta Botellas Neopreno Negro';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='ILUV Audífonos in ear Bubble Gum Rojo IEP205-R';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='Reemplazo cupón de Marketing';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='Tostador Proctor Silex 24605Y 4 Rebanadas-Blanco';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='HP P-FD4GBHP165-GEL Pendrive USB';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='Morrocoy Silla de Director';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='Kalosh Almohada 95% Pluma y 5% Plumón Blanca';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='Kingston Tarjeta de memoria SDC4/4GB-1ADP';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='PRO CHEF Parrillera Hamburguer';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='Credito por bono no funcionando 2';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='Credito por bono no funcionando 3';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='Contigo West loop Vaso Térmico con autosello 16 Oz/ 473 ml Rojo';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='MKT 50Bs  hasta 31 de diciembre';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='ARGOM Teclado multimedia en español ARG-KB-7805 Negro';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='GBTech MP4 GBTech GB1106 Azul 4 GB';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='SABA Set de 3 utensilios para Parrilla';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='Decocar Cava Americana 11,4Lt Roja';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='Marketing en ferias';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='SABA - Dispensador de Aceite y Vinagre 2 en 1 / SABA-511 / Transparente';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='MKT 30%';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='Easy Line Mouse Retráctil EL-993346 Negro';
UPDATE marketing_report.channel_report_ve SET channel_group='Newsletter' WHERE channel='Reactivation campaign';

#CAC Deals Group
##UPDATE marketing_report.channel_report_ve set channel_group='CAC Deals' where channel='CAC Deals' or channel='FB Ads CAC' or channel='FB CAC' or channel='NL CAC';

#SEO Group
UPDATE marketing_report.channel_report_ve SET channel_group='Search Engine Optimization' WHERE channel='Search Organic' or channel='Yahoo Answers';

#Partnerships Group

#CORPORATE SALES

UPDATE marketing_report.channel_report_ve SET channel_group='Other (identified)' WHERE channel='Corporate Sales';

#Corporate Sales Group
UPDATE marketing_report.channel_report_ve SET channel_group='Other (identified)' WHERE channel='Corporate Sales';

#Offline Group
UPDATE marketing_report.channel_report_ve SET channel_group='Offline Marketing' where channel='SMS' or channel='Buscape' or channel='Valla' or channel='Publitruck' or channel='Paradas Bus';

#Tele Sales Group
UPDATE marketing_report.channel_report_ve SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales' OR channel='CS Generic Voucher' or channel='Other Tele Sales';

#Branded Group
UPDATE marketing_report.channel_report_ve SET channel_group='Branded' 
WHERE channel='Linio.com Referral' OR channel='Directo / Typed' OR
channel='SEM Branded';

#ML Group
UPDATE marketing_report.channel_report_ve SET channel_group='Mercado Libre' 
WHERE channel='Mercado Libre Referral' OR channel='Mercado Libre Voucher' 
OR channel='Otros - Mercado Libre';

#Blog Group
-- UPDATE marketing_report.channel_report_ve SET channel_group='Blogs'
-- WHERE channel='Blog Linio';

#Affiliates Group
UPDATE marketing_report.channel_report_ve SET channel_group='Affiliates' 
WHERE channel='Pampa Network' or channel='Other Affiliates' or channel='SoloCPM' or channel='PromoDescuentos';


#Other (identified) Group
UPDATE marketing_report.channel_report_ve SET channel_group='Other (identified)' 
WHERE channel='Referral Sites' OR channel='Reactivation Letters' 
OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' OR
channel='Exchange' OR
channel='Content Mistake' OR
channel='Procurement Mistake' OR
channel='Employee Voucher' OR channel='Cupón para periodistas';

#Extra Channel Group
update marketing_report.channel_report_ve set channel_group='Google Display Network' where source_medium = 'Noticias24 / banner5A';
update marketing_report.channel_report_ve set channel_group='Google Display Network' where source_medium = 'Noticias24 / intersticial';
update marketing_report.channel_report_ve set channel_group='Google Display Network' where source_medium = 'Noticias24 / Display';
update marketing_report.channel_report_ve set channel_group='Other (identified)' where source_medium = 'googleads.g.doubleclick.net / referral';
update marketing_report.channel_report_ve set channel_group='Google Display Network' where source_medium = 'LaPatilla / banner650';
update marketing_report.channel_report_ve set channel_group='Tele Sales' where source_medium = 'CS / Inbound';
update marketing_report.channel_report_ve set channel_group='Tele Sales' where source_medium = 'CS / Outbound';
update marketing_report.channel_report_ve set channel_group='Google Display Network' where source_medium = 'LaPatilla / Display';
update marketing_report.channel_report_ve set channel_group='Google Display Network' where source_medium = 'Noticias24 / banner';
update marketing_report.channel_report_ve set channel_group='Other (identified)' where source_medium = 'google.co.ve / referral';

#Unknown Group
UPDATE marketing_report.channel_report_ve SET channel_group='Non identified' 
WHERE channel='Unknown-No voucher' OR channel='Unknown-Voucher' or (channel=1 and channel_group='') or channel_group='';

#Define Channel Type---
#TO BE DONE

#Ownership

UPDATE marketing_report.channel_report_ve set Ownership='' where channel_group='Social Media';
UPDATE marketing_report.channel_report_ve set Ownership='' where channel_group='NewsLetter';
UPDATE marketing_report.channel_report_ve set Ownership='' where channel_group='Search Engine Optimization';
UPDATE marketing_report.channel_report_ve set Ownership='' where channel_group='Blogs';
UPDATE marketing_report.channel_report_ve set Ownership='' where channel_group='Offline Marketing';
UPDATE marketing_report.channel_report_ve set Ownership='' where channel_group='Partnerships';
UPDATE marketing_report.channel_report_ve set Ownership='' where channel_group='Mercado Libre';
UPDATE marketing_report.channel_report_ve set Ownership='' where channel_group='Tele Sales';

-- SELECT 'Start: channel_report_no_voucher ', now();
-- call channel_report_no_voucher();

-- SELECT 'Start: visits_costs_channel ', now();
-- call visits_costs_channel();

-- call daily_marketing();
-- call monthly_marketing();

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `test69`()
begin

set @itemid:= (select max(item) from production_co.tbl_order_detail); 

insert into production_co.tbl_order_detail(yrmonth, date, custid, order_nr, item, unit_price_after_vat, coupon_money_after_vat, shipping_fee_after_vat, costo_after_vat, delivery_cost_supplier, payment_cost, shipping_cost, wh, cs, oac, obc, returned, pending, cancel, category_bp, channel_group, channel, new_cat1, new_cat2, new_cat3, payment_method, product_name, buyer, brand, unit_price, coupon_money_value, sku, sku_config, coupon_code, coupon_code_description, is_marketplace) select monthnum, date, customernum, ordernum, itemid, priceaftertax, couponvalueaftertax, shippingfee, costaftertax, deliverycostsupplier, paymentfees, shippingcost, flwhcost, flcscost, orderaftercan, orderbeforecan, returns, pending, cancellations, CatBP, channelgroup, channel, cat1, cat2, cat3, paymentmethod, skuname, buyer, brand, price, couponvalue, skusimple, skuconfig, couponcode, couponcodedescription, isMPlace from development_co_project.A_Master where itemid>@itemid;

-- New Customers

create table marketing_report.new_customers_freeze select itemid, ordernum, newreturning, count(*) as items, 0 as total, 0.0 as percent from development_co_project.A_Master where newreturning='NEW' group by itemid;

create index itemid on marketing_report.new_customers_freeze(itemid, ordernum);

update marketing_report.new_customers_freeze f set total = (select count(*) from development_co_project.A_Master a where a.ordernum=f.ordernum);

update marketing_report.new_customers_freeze set percent=items/total;

update production_co.tbl_order_detail t inner join marketing_report.new_customers_freeze f on t.item=f.itemid set t.new_customers=f.percent;

drop table marketing_report.new_customers_freeze;

-- New Customers Gross
/*
create table marketing_report.new_customers_freeze select itemid, ordernum, newreturning, count(*) as items, 0 as total, 0.0 as percent from development_co_project.A_Master where newreturning='NEW' group by itemid;

create index itemid on marketing_report.new_customers_freeze(itemid, ordernum);

update marketing_report.new_customers_freeze f set total = (select count(*) from development_co_project.A_Master a where a.ordernum=f.ordernum);

update marketing_report.new_customers_freeze set percent=items/total;

update production_co.tbl_order_detail t inner join marketing_report.new_customers_freeze f on t.item=f.itemid set t.new_customers=f.percent;

drop table marketing_report.new_customers_freeze;*/

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `test89`()
begin

set @start_date='2014-01-01';

/*
create table test.workdays_item_testing(
yrmonth_ordered int,
week_ordered varchar(255),
date_ordered date,
order_nr varchar(255),
sku_simple varchar(255),
item_id varchar(255),
date_exportable date,
status_itens_venda varchar(255),
date_shipped date,
delivery_time_supplier int,
date_delivered date,
workdays_total_delivery int,
workdays_export int,
delivered int,
shipped int
);

create index item_id on test.workdays_item_testing(item_id);
create index itens_venda_id on test.workdays_item_testing(itens_venda_id);
create index sku_simple on test.workdays_item_testing(sku_simple);*/

delete from test.workdays_item_testing;

insert into test.workdays_item_testing(item_id, date_ordered) select id_sales_order_item, date(created_at) from bob_live_mx.sales_order_item where date(created_at)>=@start_date; 

update test.workdays_item_testing set yrmonth_ordered = concat(year(date_ordered),if(month(date_ordered)<10,concat(0,month(date_ordered)),month(date_ordered))), week_ordered=production.week_iso(date_ordered);

update test.workdays_item_testing a inner join wmsprod.itens_venda v on a.item_id=v.item_id set a.sku_simple=v.sku, a.date_exported=v.data_exportable, a.order_nr=v.numero_order, a.itens_venda_id=v.itens_venda_id, a.status_wms=v.status; 

update test.workdays_item_testing set sku_config = left(sku_simple, 15);

update test.workdays_item_testing a set date_shipped = (select max(data) from wmsprod.status_itens_venda v where a.itens_venda_id=v.itens_venda_id and v.status='exportado');

update test.workdays_item_testing a inner join wmsprod.itens_venda v on a.item_id=v.item_id set a.delivery_time_supplier=v.tempo_entrega_fornecedor;

update test.workdays_item_testing a inner join bob_live_mx.sales_order_item s on a.item_id=s.id_sales_order_item set a.delivery_time_supplier=s.delivery_time_supplier where a.delivery_time_supplier=0;

update test.workdays_item_testing a inner join bob_live_mx.catalog_config c on a.sku_config=c.sku set a.delivery_time_supplier=c.supplier_lead_time where a.delivery_time_supplier=0;

update test.workdays_item_testing a set date_delivered = (select max(date) from wmsprod.vw_itens_venda_entrega v, wmsprod.tms_status_delivery t where v.cod_rastreamento=t.cod_rastreamento and a.itens_venda_id=v.itens_venda_id ); 

update test.workdays_item_testing a inner join operations_mx.out_order_tracking o on a.item_id=o.item_id set a.fulfillment_type_real=o.fulfillment_type_real;

update test.workdays_item_testing a inner join development_mx.A_Master m on a.item_id=m.itemid set a.obc=m.OrderBeforeCan, a.oac=m.OrderAfterCan;

update test.workdays_item_testing a inner join development_mx.A_Master_Catalog m on a.sku_simple=m.sku_simple inner join bob_live_mx.supplier s on s.id_supplier=m.id_supplier set a.is_marketplace=m.isMarketPlace, a.supplier_type=s.type;

update test.workdays_item_testing set workdays_exported=production.calcworkdays(date_ordered, date_exported);

update test.workdays_item_testing set workdays_shipped=production.calcworkdays(date_exported, date_shipped);

update test.workdays_item_testing set workdays_delivered=production.calcworkdays(date_shipped, date_delivered);

update test.workdays_item_testing set workdays_total_delivery=production.calcworkdays(date_ordered, date_delivered);

update test.workdays_item_testing set exported=1 where date_exported is not null;

update test.workdays_item_testing set shipped=1 where date_shipped is not null;

update test.workdays_item_testing set delivered=1 where date_delivered is not null;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `test99`()
begin

set @currency:='USD';

set @days:=50;

set @freeze:=60;

set @channel_marketing:=1;

select  'Marketing Report: start',now();

delete from test.global_report where datediff(curdate(), date)<@days;

-- Colombia

update test.tbl_order_detail t inner join development_co.A_Master o on t.item=o.itemID set t.unit_price_after_vat=o.priceaftertax, t.coupon_money_after_vat=o.couponvalueaftertax, t.shipping_fee_after_vat=o.shippingfee, t.costo_after_vat=o.costaftertax, t.cost_oms=o.CostAfterTax, t.delivery_cost_supplier=o.deliverycostsupplier, t.payment_cost=o.paymentfees, t.shipping_cost=o.shippingcost, t.wh=o.flwhcost, t.cs=o.flcscost, t.oac=o.orderaftercan, t.obc=o.orderbeforecan, t.returned=o.returns, t.pending=o.pending, t.cancel=o.cancellations, t.category_bp=o.CatBP where datediff(curdate(), t.date)<@freeze;

insert into test.global_report(country, yrmonth, week, date, weekday, channel_group, channel, gross_orders, net_orders, gross_revenue, net_revenue, PC1, `PC1.5`, PC2, new_customers_gross, new_customers) select 'Colombia', gross.yrmonth, gross.week, gross.date, gross.weekday, gross.channel_group, gross.channel, gross.gross_orders, net.net_orders, gross.gross_revenue, net.net_revenue, net.PC1, net.`PC1.5`, net.PC2, gross.new_customers_gross, net.new_customers from (select yrmonth, production_co.week_iso(date) as week, date, case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end as weekday,  channel_group, channel, count(distinct order_nr) as gross_orders, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as gross_revenue, sum(new_customers_gross) as new_customers_gross from test.tbl_order_detail where obc=1 group by yrmonth, production_co.week_iso(date), date, channel_group, channel)gross left join (select yrmonth, production_co.week_iso(date) as week, date, channel_group, channel, count(distinct order_nr) as net_orders, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(cost_oms,0)-ifnull(delivery_cost_supplier,0)) as PC1, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(cost_oms,0)-ifnull(delivery_cost_supplier,0)-ifnull(payment_cost,0)-ifnull(shipping_cost,0)) as `PC1.5`, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(cost_oms,0)-ifnull(delivery_cost_supplier,0)-ifnull(payment_cost,0)-ifnull(shipping_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2, sum(new_customers) as new_customers from test.tbl_order_detail where oac=1 and returned = 0 and rejected = 0 group by yrmonth, production_co.week_iso(date), date, channel_group, channel)net on gross.date=net.date and gross.channel=net.channel and gross.channel_group=net.channel_group where datediff(curdate(), gross.date)<@days order by date desc;

-- Avg Basket Size

create table marketing_report.temporary_basket as select date, channel_group, channel, avg(basket) as avg_basket_size from (select date, channel_group, channel, order_nr, count(*) as basket from test.tbl_order_detail where oac=1 and datediff(curdate(), date)<@days group by date, channel_group, channel, order_nr)a group by date, channel_group, channel;

create index channel on marketing_report.temporary_basket(date, channel_group, channel);

update test.global_report g inner join marketing_report.temporary_basket t on g.date=t.date and g.channel_group=t.channel_group and g.channel=t.channel set g.avg_basket_size=t.avg_basket_size where g.country='Colombia' and datediff(curdate(), g.date)<@days;

drop table marketing_report.temporary_basket;

-- Out of Stock

create table marketing_report.temporary_oos as select date, channel_group, channel, count(*) as stock_out from test.tbl_order_detail t, wmsprod_co.itens_venda i where t.item = i.item_id and t.obc=1 and (i.status='quebra tratada' or i.status='quebrado') and datediff(curdate(), t.date)<@days group by date, channel_group, channel;

create index channel on marketing_report.temporary_oos(date, channel_group, channel);

update test.global_report g inner join marketing_report.temporary_oos o on g.date=o.date and g.channel_group=o.channel_group and g.channel=o.channel set g.stock_out=o.stock_out where country='Colombia' and datediff(curdate(), g.date)<@days;

drop table marketing_report.temporary_oos;

-- Is MarketPlace

create table marketing_report.temporary_marketplace as select t.date, t.channel_group, t.channel, sum(t.is_marketplace) as is_marketplace_items from test.tbl_order_detail t where t.oac=1 and datediff(curdate(), t.date)<@days group by t.date, t.channel_group, t.channel;

create index channel on marketing_report.temporary_marketplace(date, channel_group, channel);

update test.global_report g inner join marketing_report.temporary_marketplace m on g.date=m.date and g.channel_group=m.channel_group and g.channel=m.channel set g.is_marketplace_items=m.is_marketplace_items where country='Colombia' and datediff(curdate(), g.date)<@days;

drop table marketing_report.temporary_marketplace;

create table marketing_report.temporary_all_items as select t.date, t.channel_group, t.channel, count(*) as all_items from test.tbl_order_detail t where t.oac=1 and datediff(curdate(), t.date)<@days group by t.date, t.channel_group, t.channel;

create index channel on marketing_report.temporary_all_items(date, channel_group, channel);

update test.global_report g inner join marketing_report.temporary_all_items m on g.date=m.date and g.channel_group=m.channel_group and g.channel=m.channel set g.all_items=m.all_items where country='Colombia' and datediff(curdate(), g.date)<@days;

drop table marketing_report.temporary_all_items;

create table marketing_report.temporary_marketplace as select t.date, t.channel_group, t.channel, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as marketplace_revenue from test.tbl_order_detail t where t.oac=1 and t.is_marketplace=1 and datediff(curdate(), t.date)<@days group by t.date, t.channel_group, t.channel;

create index channel on marketing_report.temporary_marketplace(date, channel_group, channel);

update test.global_report g inner join marketing_report.temporary_marketplace m on g.date=m.date and g.channel_group=m.channel_group and g.channel=m.channel set g.marketplace_revenue=m.marketplace_revenue where country='Colombia' and datediff(curdate(), g.date)<@days;

drop table marketing_report.temporary_marketplace;

-- PC2 New Customers / Old Customers

create table marketing_report.temporary_new_customers as select t.date, t.channel_group, t.channel, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(cost_oms,0)-ifnull(delivery_cost_supplier,0)-ifnull(payment_cost,0)-ifnull(shipping_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as new_customers_PC2 from test.tbl_order_detail t where new_customers !=0 and t.oac=1 and datediff(curdate(), t.date)<@days group by t.date, t.channel_group, t.channel;

create index channel on marketing_report.temporary_new_customers(date, channel_group, channel);

update test.global_report g inner join marketing_report.temporary_new_customers m on g.date=m.date and g.channel_group=m.channel_group and g.channel=m.channel set g.new_customers_PC2=m.new_customers_PC2 where country='Colombia' and datediff(curdate(), g.date)<@days;

drop table marketing_report.temporary_new_customers;

create table marketing_report.temporary_new_customers as select t.date, t.channel_group, t.channel, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(payment_cost,0)-ifnull(shipping_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as old_customers_PC2 from test.tbl_order_detail t where new_customers =0 and t.oac=1 and datediff(curdate(), t.date)<@days group by t.date, t.channel_group, t.channel;

create index channel on marketing_report.temporary_new_customers(date, channel_group, channel);

update test.global_report g inner join marketing_report.temporary_new_customers m on g.date=m.date and g.channel_group=m.channel_group and g.channel=m.channel set g.old_customers_PC2=m.old_customers_PC2 where country='Colombia' and datediff(curdate(), g.date)<@days;

drop table marketing_report.temporary_new_customers;

-- Returning Customers

create table marketing_report.temporary_returning_customers as select t.date, t.channel_group, t.channel, count(distinct custid) as returning_customers from test.tbl_order_detail t where new_customers =0 and t.oac=1 and datediff(curdate(), t.date)<@days group by t.date, t.channel_group, t.channel;

create index channel on marketing_report.temporary_returning_customers(date, channel_group, channel);

update test.global_report g inner join marketing_report.temporary_returning_customers m on g.date=m.date and g.channel_group=m.channel_group and g.channel=m.channel set g.returning_customers=m.returning_customers where country='Colombia' and datediff(curdate(), g.date)<@days;

drop table marketing_report.temporary_returning_customers;

-- Pending Rev

update test.global_report g set pending_revenue = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from test.tbl_order_detail t where t.pending=1 and g.date=t.date and g.channel_group=t.channel_group and g.channel=t.channel) where country='Colombia' and datediff(curdate(), date)<@days;

-- Canceled Rev

update test.global_report g set canceled_revenue = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from test.tbl_order_detail t where t.cancel=1 and g.date=t.date and g.channel_group=t.channel_group and g.channel=t.channel) where country='Colombia' and datediff(curdate(), date)<@days;

-- MoM

create table marketing_report.temporary_mom as select a.yrmonth, a.date, a.channel_group, a.net_revenue/b.net_revenue as mom from (select a.yrmonth, a.date, a.channel_group, sum(b.net_revenue) as net_revenue from (select yrmonth, date, channel_group, sum(net_revenue) as net_revenue from test.global_report where country='Colombia' group by channel_group, date)b, (select yrmonth, date, channel_group, sum(net_revenue) as net_revenue from test.global_report where country='Colombia' group by channel_group, date)a where a.date>=b.date and a.channel_group=b.channel_group and a.yrmonth=b.yrmonth group by a.date, a.channel_group order by a.channel_group, a.date)a, (select a.yrmonth, a.date, a.channel_group, sum(b.net_revenue) as net_revenue from (select yrmonth, date, channel_group, sum(net_revenue) as net_revenue from test.global_report where country='Colombia' group by channel_group, date)b, (select yrmonth, date, channel_group, sum(net_revenue) as net_revenue from test.global_report where country='Colombia' group by channel_group, date)a where a.date>=b.date and a.channel_group=b.channel_group and a.yrmonth=b.yrmonth group by a.date, a.channel_group order by a.channel_group, a.date)b where a.channel_group=b.channel_group and a.date - interval 1 month = b.date order by a.channel_group, a.date desc;

create index channel_group on marketing_report.temporary_mom(date, channel_group);

update test.global_report g inner join marketing_report.temporary_mom t on g.date=t.date and g.channel_group=t.channel_group set MoM_net_revenue=t.mom where country='Colombia'; 

update test.global_report set MoM_net_revenue=MoM_net_revenue-1 where country='Colombia';

drop table marketing_report.temporary_mom;

-- Visits

if @channel_marketing=1 then update SEM.campaign_ad_group_co set source_medium=concat(source,' / ',medium);
update SEM.campaign_ad_group_co set channel=null, channel_group=null;
call production_co.channel_marketing;
end if;

create table marketing_report.temporary(
date date,
channel varchar(255),
channel_group varchar(255),
include int
);

insert into marketing_report.temporary(date, channel_group, channel) select distinct date, channel_group, channel from SEM.campaign_ad_group_co where datediff(curdate(), date)<@days;

update marketing_report.temporary t inner join test.global_report g on t.channel_group=g.channel_group and t.channel=g.channel and t.date=g.date set include = 1 where country='Colombia' and datediff(curdate(), g.date)<@days;

insert into test.global_report(country, date, channel_group, channel) select 'Colombia', date, channel_group, channel from marketing_report.temporary where include is null;

update test.global_report g set visits = (select sum(visits) from SEM.campaign_ad_group_co c where c.channel_group=g.channel_group and c.channel=g.channel and c.date=g.date) where country='Colombia' and datediff(curdate(), g.date)<@days;

update test.global_report g set visits = (select sum(visits) from production_co.mobileapp_campaign c where c.date=g.date) where channel_group='Mobile App' and channel='Mobile App' and country='Colombia' and datediff(curdate(), g.date)<@days;

-- Cost

##update test.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_co r where channel='Criteo' and g.date=r.date) where channel='Criteo' and country='Colombia' and datediff(curdate(), g.date)<@days; 

update test.global_report g set cost = (select sum(cost) from marketing_report.criteo_co r where g.date=r.date) where channel='Criteo' and country='Colombia' and datediff(curdate(), g.date)<@days; 

update test.global_report g set cost = (select sum(cost) from facebook.facebook_optimization_region_co r where g.date=r.date and campaign not like 'COL_darkpost%' and campaign not like '%.CAC%' and campaign not like '%_CAC') where channel='Facebook Ads' and country='Colombia' and datediff(curdate(), g.date)<@days; 

update test.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_co r where channel='GLG' and g.date=r.date) where channel='Glg' and country='Colombia' and datediff(curdate(), g.date)<@days; 

update test.global_report g set cost = (select sum(cost) from SEM.google_optimization_ad_group_co r where channel='GDN' and g.date=r.date) where channel='Google Display Network' and country='Colombia' and datediff(curdate(), g.date)<@days;

##update test.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_co r where channel='Pampa Network' and g.date=r.date) where channel='Pampa Network' and country='Colombia' and datediff(curdate(), g.date)<@days;

##update test.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_co r where channel='Main ADV' and g.date=r.date) where channel='Main ADV' and country='Colombia' and datediff(curdate(), g.date)<@days;   

##update test.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_co r where channel='My Things' and g.date=r.date) where channel='My Things' and country='Colombia' and datediff(curdate(), g.date)<@days;


update test.global_report g set cost = (select sum(commission_total) from marketing_report.affiliates_co r where g.date=r.date and network='TradeTracker') where channel='Trade Tracker' and country='Colombia' and datediff(curdate(), g.date)<@days; 

update test.global_report g set cost = (select sum(commission_total) from marketing_report.affiliates_co r where g.date=r.date and network='PampaNetwork') where channel='Pampa Network' and country='Colombia' and datediff(curdate(), g.date)<@days; 

update test.global_report g set cost = (select sum(commission_total) from marketing_report.affiliates_co r where g.date=r.date and network='Netslave') where channel='Netslave' and country='Colombia' and datediff(curdate(), g.date)<@days;

##update test.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_co r where channel='Trade Tracker' and g.date=r.date) where channel='Trade Tracker' and country='Colombia' and datediff(curdate(), g.date)<@days;     

##update test.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_co r where channel='FBX' and g.date=r.date) where channel='Facebook Retargeting' and country='Colombia' and datediff(curdate(), g.date)<@days;

##update test.global_report g set cost = (select sum(l.cost)+sum(r.cost) from marketing_report.sociomantic_fbx_loyalty_co l, marketing_report.sociomantic_fbx_co r where l.date=r.date and g.date=r.date) where channel='Facebook Retargeting' and country='Colombia' and datediff(curdate(), g.date)<@days; 

##update test.global_report g set retention_cost = (select sum(l.cost) from marketing_report.sociomantic_fbx_loyalty_co l where l.date=g.date) where channel='Facebook Retargeting' and country='Colombia' and datediff(curdate(), g.date)<@days; 

##update test.global_report g set acquisition_cost = (select sum(r.cost) from marketing_report.sociomantic_fbx_co r where g.date=r.date) where channel='Facebook Retargeting' and country='Colombia' and datediff(curdate(), g.date)<@days;  

update test.global_report g set cost = (select sum(cost) from SEM.google_optimization_ad_group_co r where channel='SEM' and g.date=r.date) where channel='SEM (unbranded)' and country='Colombia' and datediff(curdate(), g.date)<@days;

update test.global_report g set cost = (select sum(ad_cost) from SEM.campaign_ad_group_co r where source = 'google' and medium = 'CPC' and campaign like 'brandb%' and g.date=r.date) where channel='SEM Branded' and country='Colombia' and datediff(curdate(), g.date)<@days;

##update test.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_co r where channel='Sociomantic' and g.date=r.date) where channel='Sociomantic' and country='Colombia' and datediff(curdate(), g.date)<@days;

##update test.global_report g set cost = (select sum(l.cost)+sum(r.cost) from marketing_report.sociomantic_loyalty_co l, marketing_report.sociomantic_basket_co r where l.date=r.date and g.date=r.date) where channel='Sociomantic' and country='Colombia' and datediff(curdate(), g.date)<@days;

update test.global_report g set cost = (select sum(l.cost)+sum(r.cost)+sum(m.cost)+sum(n.cost) from marketing_report.sociomantic_loyalty_co l, marketing_report.sociomantic_basket_co r, marketing_report.sociomantic_fbx_loyalty_co m, marketing_report.sociomantic_fbx_co n where m.date=n.date and m.date=r.date and l.date=r.date and g.date=r.date) where channel='Sociomantic' and country='Colombia' and datediff(curdate(), g.date)<@days;

update test.global_report g set retention_cost = (select sum(l.cost)+sum(m.cost) from marketing_report.sociomantic_loyalty_co l, marketing_report.sociomantic_fbx_loyalty_co m where l.date=g.date and l.date=m.date) where channel='Sociomantic' and country='Colombia' and datediff(curdate(), g.date)<@days;

update test.global_report g set acquisition_cost = (select sum(r.cost)+sum(n.cost) from marketing_report.sociomantic_basket_co r, marketing_report.sociomantic_fbx_co n where g.date=r.date and g.date=n.date) where channel='Sociomantic' and country='Colombia' and datediff(curdate(), g.date)<@days;

update test.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_co r where channel='Veinteractive' and g.date=r.date) where channel='Newsletter' and country='Colombia' and datediff(curdate(), g.date)<@days;

##update test.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_co r where channel='Vizury' and g.date=r.date) where channel='Vizury' and country='Colombia' and datediff(curdate(), g.date)<@days and g.date>='2014-01-09';

update test.global_report g set cost = (select sum(cost) from facebook.fanpage_optimization_co r where g.date=r.date and r.campaign not like '%CAC.%' and r.campaign not like '%_CAC' and r.campaign not like '%CAC-%') where channel='Linio Fan Page' and country='Colombia' and datediff(curdate(), g.date)<@days;

update test.global_report g set cost = (select sum(spent) from facebook.facebook_campaign r where g.date=r.date and campaign like 'COL_fanpagefashion%' and r.campaign not like '%CAC.%' and r.campaign not like '%_CAC') where channel='Fashion Fan Page' and country='Colombia' and datediff(curdate(), g.date)<@days;

update test.global_report g set cost = (select sum(spent) from facebook.facebook_campaign r where g.date=r.date and campaign like 'COL_darkpost%' and campaign not like '%.CAC%' and campaign not like '%_CAC') where channel='Dark Post' and country='Colombia' and datediff(curdate(), g.date)<@days;

update test.global_report g set cost = (select sum(cost) from facebook.facebook_optimization_region_co r where g.date=r.date and (campaign like '%.CAC%' or campaign like '%_CAC')) where channel_group='CAC Deals' and  channel='FB Ads CAC' and country='Colombia' and datediff(curdate(), g.date)<@days;

update test.global_report g set cost = (select sum(cost) from facebook.fanpage_optimization_co r where g.date=r.date and (campaign like '%CAC.%' or campaign like '%_CAC' or campaign like '%CAC-%')) where channel_group='CAC Deals' and  channel='FB CAC' and country='Colombia' and datediff(curdate(), g.date)<@days;

update test.global_report g set cost = (select sum(spent) from facebook.facebook_campaign r where g.date=r.date and campaign like 'COL_fanpagefashion%' and (campaign like '%CAC.%' or campaign like '%_CAC')) where channel_group='CAC Deals' and  channel='FBF CAC' and country='Colombia' and datediff(curdate(), g.date)<@days;

update test.global_report g set cost = (select sum(clicks)*0.2 from marketing_report.triggit_co r where g.date=r.report_start_date) where channel='Triggit' and country='Colombia' and datediff(curdate(), g.date)<@days;

update test.global_report g set cost = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0))*0.012 from test.tbl_order_detail r where g.date=r.date and r.obc=1 and r.channel='Vizury') where channel='Vizury' and country='Colombia' and datediff(curdate(), g.date)<@days;

create table marketing_report.temporary_offline(
date date,
count int
);

insert into marketing_report.temporary_offline select date, count(distinct channel) from test.global_report where channel_group='Offline Marketing' and country='Colombia' and datediff(curdate(), date)<@days group by date;

update test.global_report g set cost = (select sum(cost)/count from marketing_report.offline_co t, marketing_report.temporary_offline r where g.date=r.date and t.date=r.date) where channel_group='Offline Marketing' and country='Colombia' and datediff(curdate(), date)<@days;

drop table marketing_report.temporary_offline;

create table marketing_report.temporary_tele_sales(
date date,
count int
);

insert into marketing_report.temporary_tele_sales select date, count(distinct channel) from test.global_report where channel_group='Tele Sales' and country='Colombia' and datediff(curdate(), date)<@days group by date;

update test.global_report g set cost = (select sum(cost)/count from marketing_report.tele_sales_co t, marketing_report.temporary_tele_sales r where g.date=r.date and t.date=r.date) where channel_group='Tele Sales' and country='Colombia' and datediff(curdate(), date)<@days;

drop table marketing_report.temporary_tele_sales;

-- PC3

update test.global_report set PC3 = PC2-cost where country='Colombia';

-- Moving Avg

create table marketing_report.temporary_global_report(
date date,
channel_group varchar(255),
channel varchar(255),
gross_orders int not null default 0,
net_orders int not null default 0,
e_orders float not null default 0,
ee_orders float not null default 0,
gross_revenue float not null default 0,
net_revenue float not null default 0,
e_revenue float not null default 0,
ee_revenue float not null default 0,
new_customers_gross int not null default 0,
new_customers int not null default 0,
e_new_customers float not null default 0,
ee_new_customers float not null default 0,
cancel float,
returns float,
voucher float
);

create index temporary_mkt on marketing_report.temporary_global_report(date, channel_group, channel);

insert into marketing_report.temporary_global_report(date, channel_group, channel, gross_orders, net_orders, gross_revenue, net_revenue, new_customers_gross, new_customers) select date, channel_group, channel, gross_orders, net_orders, gross_revenue, net_revenue, new_customers_gross, new_customers from test.global_report where country='Colombia' and datediff(curdate(), date)<@days;

-- Orders

set @date=date_sub(curdate(), interval @days day);

while @date!=curdate() do
update marketing_report.temporary_global_report t set e_orders = (select (avg(g.net_orders/g.gross_orders)) from test.global_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Colombia') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;


update marketing_report.temporary_global_report t set e_orders = gross_orders*e_orders where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_orders = case when e_orders>net_orders then e_orders else net_orders end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set cancel = (select count(distinct order_nr) from test.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set returns = (select count(distinct order_nr) from test.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.returned=1) where datediff(curdate(), date)<@days; 

update marketing_report.temporary_global_report t set cancel = cancel/gross_orders, returns = returns/gross_orders where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set ee_orders = gross_orders*(1-ifnull(cancel,0)-ifnull(returns,0)) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_orders = case when e_orders>ee_orders then ee_orders else e_orders end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_orders = case when e_orders>net_orders then e_orders else net_orders end where datediff(curdate(), date)<@days;

-- Revenue

set @date=date_sub(curdate(), interval @days day);

while @date!=curdate() do
update marketing_report.temporary_global_report t set e_revenue = (select (avg(g.net_revenue/g.gross_revenue)) from test.global_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Colombia') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_global_report t set e_revenue = gross_revenue*e_revenue where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_revenue = case when e_revenue>net_revenue then e_revenue else net_revenue end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set cancel = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from test.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set returns = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from test.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.returned=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set voucher = (select sum(coupon_money_after_vat) from test.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.coupon_code is not null and d.oac=1) where datediff(curdate(), date)<@days; 

update marketing_report.temporary_global_report t set cancel = cancel/gross_revenue, returns = returns/gross_revenue, voucher=voucher/gross_revenue where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set ee_revenue = gross_revenue*(1-ifnull(cancel,0)-ifnull(returns,0)-ifnull(voucher,0)) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_revenue = case when e_revenue>ee_revenue then ee_revenue else e_revenue end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_revenue = case when e_revenue>net_revenue then e_revenue else net_revenue end where datediff(curdate(), date)<@days;

-- New Customers

set @date=date_sub(curdate(), interval @days day);

while @date!=curdate() do
update marketing_report.temporary_global_report t set e_new_customers = (select (avg(g.new_customers/g.new_customers_gross)) from test.global_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Colombia') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_global_report t set e_new_customers = new_customers_gross*e_new_customers where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_new_customers = case when e_new_customers>new_customers then e_new_customers else new_customers end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set cancel = (select sum(new_customers_gross) from test.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set returns = (select sum(new_customers_gross) from test.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.returned=1) where datediff(curdate(), date)<@days; 

update marketing_report.temporary_global_report t set cancel = cancel/new_customers_gross, returns = returns/new_customers_gross where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set ee_new_customers = new_customers_gross*(1-ifnull(cancel,0)-ifnull(returns,0)) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_new_customers = case when e_new_customers>ee_new_customers then ee_new_customers else e_new_customers end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_new_customers = case when e_new_customers>new_customers then e_new_customers else new_customers end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t inner join test.global_report g on t.date=g.date and t.channel_group=g.channel_group and t.channel=g.channel set g.e_orders=t.e_orders, g.e_revenue=t.e_revenue, g.e_new_customers=t.e_new_customers where country = 'Colombia' and datediff(curdate(), g.date)<@days;

-- Exchange Rate

update test.global_report set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=production.week_iso(date), weekday=case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

if @currency='EUR' then update test.global_report s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, e_revenue=e_revenue/xr, a_revenue=a_revenue/xr, PC1=PC1/xr, `PC1.5`=`PC1.5`/xr, PC2=PC2/xr, PC3=PC3/xr, pending_revenue=pending_revenue/xr, canceled_revenue=canceled_revenue/xr, marketplace_revenue=marketplace_revenue/xr, new_customers_PC2=new_customers_PC2/xr, old_customers_PC2=old_customers_PC2/xr where s.country = 'Colombia' and r.country='COL' and datediff(curdate(), s.date)<@days;  

update test.global_report set cost = cost/16.35 where channel= 'SEM Branded' and country = 'Colombia' and yrmonth <= 201310 and datediff(curdate(), date)<@days;

update test.global_report set cost = cost/2450 where channel= 'SEM Branded' and country = 'Colombia' and yrmonth >= 201311 and datediff(curdate(), date)<@days;

update test.global_report s inner join development_mx.A_E_BI_ExchangeRate_EUR r on r.month_num=s.yrmonth set cost = cost/xr where channel in ('Criteo', 'Triggit') and s.country = 'Colombia' and datediff(curdate(), date)<@days;

update test.global_report s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set cost = cost/xr where channel in ('Netslave', 'Vizury') and s.country='Colombia' and r.country='COL' and datediff(curdate(), date)<@days;

update test.global_report s inner join development_mx.A_E_BI_ExchangeRate_EUR r on r.month_num=s.yrmonth set cost = cost/xr where channel= 'Pampa Network' and s.country = 'Colombia' and datediff(curdate(), date)<@days;

end if;

if @currency='USD' then update test.global_report s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, e_revenue=e_revenue/xr, a_revenue=a_revenue/xr, PC1=PC1/xr, `PC1.5`=`PC1.5`/xr, PC2=PC2/xr, PC3=PC3/xr, pending_revenue=pending_revenue/xr, canceled_revenue=canceled_revenue/xr, marketplace_revenue=marketplace_revenue/xr, new_customers_PC2=new_customers_PC2/xr, old_customers_PC2=old_customers_PC2/xr where s.country = 'Colombia' and r.country='COL' and datediff(curdate(), s.date)<@days;   

update test.global_report s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set cost=cost*xr where s.country = 'Colombia' and r.country='COL' and channel not in ('Criteo', 'SEM Branded', 'Pampa Network', 'Netslave', 'Vizury', 'Triggit') and datediff(curdate(), s.date)<@days; 

update test.global_report s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set cost=cost/xr where s.country = 'Colombia' and r.country='COL' and channel not in ('Criteo', 'SEM Branded', 'Pampa Network', 'Triggit') and datediff(curdate(), s.date)<@days; 

##update test.global_report s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set cost=cost*xr where s.country = 'Colombia' and r.country='COL' and channel not in ('Criteo', 'SEM Branded') and datediff(curdate(), s.date)<@days; 

##update test.global_report s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set cost=cost/xr where s.country = 'Colombia' and r.country='COL' and channel not in ('Criteo', 'SEM Branded') and datediff(curdate(), s.date)<@days; 

update test.global_report s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set cost=cost/xr where s.country = 'Colombia' and r.country='MEX' and channel = 'SEM Branded' and yrmonth <= 201310 and datediff(curdate(), s.date)<@days; 

update test.global_report s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set cost=cost/xr where s.country = 'Colombia' and r.country='COL' and channel = 'SEM Branded' and yrmonth >= 201311 and datediff(curdate(), s.date)<@days; 

end if;

-- Targets

create table marketing_report.temporary_targets(
yrmonth int,
channel_group varchar(255),
count int
);

insert into marketing_report.temporary_targets select yrmonth, channel_group, count(channel) from test.global_report where country='Colombia' and datediff(curdate(), date)<@days group by yrmonth, channel_group;

update test.global_report g inner join marketing_report.targets t on g.yrmonth=t.yrmonth and g.country=t.country and (g.channel_group=t.channel_group or g.channel=t.channel_group) inner join marketing_report.temporary_targets s on s.yrmonth=g.yrmonth and s.channel_group=g.channel_group set g.`PC1.5_target`=t.`PC1.5`, g.net_revenue_target=t.net_revenue/s.count, g.cac_target=t.cac/s.count where datediff(curdate(), g.date)<@days;

drop table marketing_report.temporary_targets;

create table marketing_report.temporary_targets(
yrmonth int,
date date,
days_in_month int,
current_day int
);

delete from marketing_report.temporary_targets;

set @date='2014-02-01';
while @date<=curdate() do
insert into marketing_report.temporary_targets(date) select @date;
set @date:=@date + interval 1 day;
end while;

update marketing_report.temporary_targets set yrmonth=concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), days_in_month=day(last_day(date)), current_day=day(date);

create table marketing_report.temporary_cummulative as select a.yrmonth, a.date, a.channel_group, sum(b.net_revenue) as net_revenue, sum(b.net_revenue_target) as net_revenue_target from (select country, yrmonth, date, channel_group, sum(net_revenue) as net_revenue, sum(net_revenue_target) as net_revenue_target from test.global_report where yrmonth>=201402 and country='Colombia' group by channel_group, date)b, (select country, yrmonth, date, channel_group, sum(net_revenue) as net_revenue, sum(net_revenue_target) as net_revenue_target from test.global_report where yrmonth>=201402 and country='Colombia' group by channel_group, date)a where a.date>=b.date and a.channel_group=b.channel_group and a.yrmonth=b.yrmonth and a.country=b.country group by a.date, a.channel_group order by a.channel_group, a.date;

update test.global_report g inner join marketing_report.temporary_targets t on t.date=g.date inner join marketing_report.temporary_cummulative c on g.date=c.date and g.channel_group=c.channel_group set net_revenue_deviation = (c.net_revenue/((c.net_revenue_target/t.days_in_month)*t.current_day))-1 where country='Colombia'; 

drop table marketing_report.temporary_cummulative;

drop table marketing_report.temporary_targets;

drop table marketing_report.temporary;

drop table marketing_report.temporary_global_report;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `testmp`()
begin

-- Temporary Colombia

insert into marketplace.product_views(country, yrmonth, week, date, sku_config, product_views) select 'Colombia', concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), production.week_iso(date), date, product, sum(quantity) from attribution_testing.getfullbasket_co_local where date>='2014-01-01' and stat=2 group by date, product;

update marketplace.product_views p set unit_sold = (select count(*) from development_co_project.A_Master a where a.orderaftercan=1 and a.isMPlace=1 and a.skuconfig=p.sku_config and a.date=p.date) where country='Colombia';

update marketplace.product_views p set net_revenue = (select sum(rev) from development_co_project.A_Master a where a.orderaftercan=1 and a.isMPlace=1 and a.skuconfig=p.sku_config and a.date=p.date) where country='Colombia';

update marketplace.product_views p set `PC1.5` = (select sum(pconepfive) from development_co_project.A_Master a where a.orderaftercan=1 and a.isMPlace=1 and a.skuconfig=p.sku_config and a.date=p.date) where country='Colombia';

update marketplace.product_views p inner join development_co_project.A_Master_Catalog c on p.sku_config=c.sku_config set p.cat1=c.Cat1, p.cat2=c.Cat2, p.cat3=c.Cat3, p.price=c.price, p.cat_bp=c.cat_bp, p.product_name=c.sku_name ,p.is_marketplace=c.ismarketplace where country='Colombia'; 

create table marketplace.temporary_product_views(
sku_config varchar(255),
cat1 varchar(255),
cat2 varchar(255),
price float,
price_range varchar(255)
);

create index sku_config on marketplace.temporary_product_views(sku_config);
create index cat1 on marketplace.temporary_product_views(cat1);
create index cat2 on marketplace.temporary_product_views(cat2);

insert into marketplace.temporary_product_views(sku_config, cat1, cat2, price) select sku_config, cat1, cat2, price from marketplace.product_views where country='Colombia' group by sku_config;

create table marketplace.temporary_cat_std_dev(
cat1 varchar(255),
cat2 varchar(255),
std_dev_minus float,
std_dev_plus float
);

create index cat1 on marketplace.temporary_cat_std_dev(cat1);
create index cat2 on marketplace.temporary_cat_std_dev(cat2);


insert into marketplace.temporary_cat_std_dev(cat1, cat2) select distinct cat1, cat2 from marketplace.temporary_product_views;

/*
update marketplace.temporary_cat_std_dev d set std_dev_minus = (select avg(price)-std(price) from marketplace.temporary_product_views p where p.cat1=d.cat1);

update marketplace.temporary_cat_std_dev d set std_dev_plus = (select avg(price)+std(price) from marketplace.temporary_product_views p where p.cat1=d.cat1);
*/

create table marketplace.temporary_cat as
select cat1, cat2, avg(price) as price from marketplace.temporary_product_views group by cat1, cat2;

create index cat1 on marketplace.temporary_cat(cat1);
create index cat2 on marketplace.temporary_cat(cat2);

update marketplace.temporary_cat_std_dev d set std_dev_minus = (select price-(price*0.15) from marketplace.temporary_cat p where p.cat1=d.cat1 and p.cat2=d.cat2);

update marketplace.temporary_cat_std_dev d set std_dev_plus = (select price+(price*0.15) from marketplace.temporary_cat p where p.cat1=d.cat1 and p.cat2=d.cat2);

update marketplace.temporary_product_views p inner join marketplace.temporary_cat_std_dev d on p.cat1=d.cat1 and p.cat2=d.cat2 set p.price_range='Cheap' where p.price between 0 and d.std_dev_minus;

update marketplace.temporary_product_views p inner join marketplace.temporary_cat_std_dev d on p.cat1=d.cat1 and p.cat2=d.cat2 set p.price_range='Medium' where p.price between d.std_dev_minus and d.std_dev_plus;

update marketplace.temporary_product_views p inner join marketplace.temporary_cat_std_dev d on p.cat1=d.cat1 and p.cat2=d.cat2 set p.price_range='Expensive' where p.price>=d.std_dev_plus;

update marketplace.product_views p inner join marketplace.temporary_product_views t on p.sku_config=t.sku_config set p.price_range_cat=t.price_range where country='Colombia';

update marketplace.product_views p set p.price_range_total='Cheap' where p.price between 0 and 700 and country='Colombia';

update marketplace.product_views p set p.price_range_total='Medium' where p.price between 701 and 4000 and country='Colombia';

update marketplace.product_views p set p.price_range_total='Expensive' where p.price>4000 and country='Colombia';

drop table marketplace.temporary_cat;

drop table marketplace.temporary_product_views;

drop table marketplace.temporary_cat_std_dev;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:04:24
