-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: marketplace
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'marketplace'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 FUNCTION `calcworkdays`(sdate varchar(50), edate varchar(50)) RETURNS int(11)
begin
 declare wdays, tdiff, counter, thisday smallint;
 declare newdate date;
 set newdate := sdate;
 set wdays = 0;
 if sdate is null then return 0; end if;
 if edate is null then return 0; end if;
 if edate like '1%' then return 0; end if;
 if edate like '0%' then return 0; end if;
 if sdate like '1%' then return 0; end if;
 if sdate like '0%' then return 0; end if;
    if datediff(edate, sdate) <= 0 then return 0; end if;
         label1: loop
          set thisday = dayofweek(newdate);
            if thisday in(1,2,3,4,5) then set wdays := wdays + 1; end if;
             set newdate = date_add(newdate, interval 1 day);
            if datediff(edate, newdate) <= 0 then leave label1; end if;
       end loop label1;
      return wdays;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 FUNCTION `maximum`(procure integer, ready integer, to_ship integer,  attempt integer ) RETURNS int(11)
begin
declare compare integer; 
		set compare = procure;
		if compare < ready then set compare = ready; end if;
		if compare < to_ship then set compare = to_ship; end if;
		if compare < attempt then set compare = attempt; end if;
 return compare;
 end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 FUNCTION `week_iso`(edate date) RETURNS varchar(20) CHARSET latin1
begin
 declare sdate varchar(20);
 if year(edate) = 2012 then set sdate = concat(year(edate), '-',(week(edate, 5))+1); else set sdate = date_format(edate, "%x-%v"); end if;
      return sdate;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 FUNCTION `workday`(edate date, workdays integer ) RETURNS date
begin
 declare wdays, thisday, thisday0, timer smallint;
 declare newdate, middate date;
 set wdays = 0;
 set newdate :=  edate;
 if edate is null then set newdate = '2012-05-01'; end if;
 if workdays is null then return '2012-05-01'; end if;
 set thisday0 = dayofweek(edate);
 if workdays in (0) and thisday0 between 2 and 6 then return newdate; end if;
 if (workdays in (0) and thisday0 in (7)) then return date(adddate(newdate, 2)); end if;
 if (workdays in (0) and thisday0 in (1)) then return date(adddate(newdate, 1)); end if;
	label2: loop
		set newdate = date(adddate(newdate, 1));
        set thisday = dayofweek(newdate);
		if thisday between 2 and 6 then 
			set wdays = wdays + 1;
			if wdays >= workdays then 
				leave label2; 
			end if;
		else 
			set wdays = wdays;
		end if;
	end loop label2;
return newdate;
 end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `category_tree`()
begin

delete from marketplace.category_tree;

-- Mexico

create table marketplace.temporary_category_tree as select node.id_catalog_category, node.name, count(parent.name) - 1 as depth from sc_live_mx_reporting.catalog_category node, sc_live_mx_reporting.catalog_category parent where node.lft between parent.lft and parent.rgt group by node.id_catalog_category having (count(parent.name) - 1)=1  order by depth;

create index id_catalog_category on marketplace.temporary_category_tree(id_catalog_category);

insert into marketplace.category_tree select distinct 'Mexico', a1.name as cat1, a2.name as cat2, a3.name as cat3, a4.name as cat4 from sc_live_mx_reporting.catalog_category a1 inner join marketplace.temporary_category_tree t on a1.id_catalog_category=t.id_catalog_category left join sc_live_mx_reporting.catalog_category a2 on a2.lft>a1.lft and a2.rgt<a1.rgt left join sc_live_mx_reporting.catalog_category a3 on a3.lft>a2.lft and a3.rgt<a2.rgt left join sc_live_mx_reporting.catalog_category a4 on a4.lft>a3.lft and a4.rgt<a3.rgt where a1.name !='Root Category' /*and a2.name is not null and a3.name is not null*/;

drop table marketplace.temporary_category_tree;

-- Colombia

create table marketplace.temporary_category_tree as select node.id_catalog_category, node.name, count(parent.name) - 1 as depth from sc_live_co_reporting.catalog_category node, sc_live_co_reporting.catalog_category parent where node.lft between parent.lft and parent.rgt group by node.id_catalog_category having (count(parent.name) - 1)=1  order by depth;

create index id_catalog_category on marketplace.temporary_category_tree(id_catalog_category);

insert into marketplace.category_tree select distinct 'Colombia', a1.name as cat1, a2.name as cat2, a3.name as cat3, a4.name as cat4 from sc_live_co_reporting.catalog_category a1 inner join marketplace.temporary_category_tree t on a1.id_catalog_category=t.id_catalog_category left join sc_live_co_reporting.catalog_category a2 on a2.lft>a1.lft and a2.rgt<a1.rgt left join sc_live_co_reporting.catalog_category a3 on a3.lft>a2.lft and a3.rgt<a2.rgt left join sc_live_co_reporting.catalog_category a4 on a4.lft>a3.lft and a4.rgt<a3.rgt where a1.name !='Root Category' /*and a2.name is not null and a3.name is not null*/;

drop table marketplace.temporary_category_tree;

-- Peru

create table marketplace.temporary_category_tree as select node.id_catalog_category, node.name, count(parent.name) - 1 as depth from sc_live_pe_reporting.catalog_category node, sc_live_pe_reporting.catalog_category parent where node.lft between parent.lft and parent.rgt group by node.id_catalog_category having (count(parent.name) - 1)=1  order by depth;

create index id_catalog_category on marketplace.temporary_category_tree(id_catalog_category);

insert into marketplace.category_tree select distinct 'Peru', a1.name as cat1, a2.name as cat2, a3.name as cat3, a4.name as cat4 from sc_live_pe_reporting.catalog_category a1 inner join marketplace.temporary_category_tree t on a1.id_catalog_category=t.id_catalog_category left join sc_live_pe_reporting.catalog_category a2 on a2.lft>a1.lft and a2.rgt<a1.rgt left join sc_live_pe_reporting.catalog_category a3 on a3.lft>a2.lft and a3.rgt<a2.rgt left join sc_live_pe_reporting.catalog_category a4 on a4.lft>a3.lft and a4.rgt<a3.rgt where a1.name !='Root Category' /*and a2.name is not null and a3.name is not null*/;

drop table marketplace.temporary_category_tree;

-- Venezuela

create table marketplace.temporary_category_tree as select node.id_catalog_category, node.name, count(parent.name) - 1 as depth from sc_live_ve_reporting.catalog_category node, sc_live_ve_reporting.catalog_category parent where node.lft between parent.lft and parent.rgt group by node.id_catalog_category having (count(parent.name) - 1)=1  order by depth;

create index id_catalog_category on marketplace.temporary_category_tree(id_catalog_category);

insert into marketplace.category_tree select distinct 'Venezuela', a1.name as cat1, a2.name as cat2, a3.name as cat3, a4.name as cat4 from sc_live_ve_reporting.catalog_category a1 inner join marketplace.temporary_category_tree t on a1.id_catalog_category=t.id_catalog_category left join sc_live_ve_reporting.catalog_category a2 on a2.lft>a1.lft and a2.rgt<a1.rgt left join sc_live_ve_reporting.catalog_category a3 on a3.lft>a2.lft and a3.rgt<a2.rgt left join sc_live_ve_reporting.catalog_category a4 on a4.lft>a3.lft and a4.rgt<a3.rgt where a1.name !='Root Category' /*and a2.name is not null and a3.name is not null*/;

drop table marketplace.temporary_category_tree;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `daily_sc`()
begin

set @days:=130;

delete from marketplace.daily_sc;

-- Mexico

insert into marketplace.daily_sc(country, date, marketplace_rev) select 'Mexico', date, sum(rev) from development_mx.A_Master where isMPlace=1 and OrderAfterCan=1 and datediff(curdate(), date)<=@days group by date;

create table marketplace.temporary_daily_sc as select date, sum(rev) as total_rev from development_mx.A_Master where OrderAfterCan=1 and datediff(curdate(), date)<=@days group by date;

create index date on marketplace.temporary_daily_sc(date);

update marketplace.daily_sc m inner join marketplace.temporary_daily_sc t on m.date=t.date set m.marketplace_contribution=m.marketplace_rev/t.total_rev where country='Mexico';

drop table marketplace.temporary_daily_sc;

-- Colombia

insert into marketplace.daily_sc(country, date, marketplace_rev) select 'Colombia', date, sum(rev) from development_co_project.A_Master where isMPlace=1 and OrderAfterCan=1 and datediff(curdate(), date)<=@days group by date;

create table marketplace.temporary_daily_sc as select date, sum(rev) as total_rev from development_co_project.A_Master where OrderAfterCan=1 and datediff(curdate(), date)<=@days group by date;

create index date on marketplace.temporary_daily_sc(date);

update marketplace.daily_sc m inner join marketplace.temporary_daily_sc t on m.date=t.date set m.marketplace_contribution=m.marketplace_rev/t.total_rev where country='Colombia';

drop table marketplace.temporary_daily_sc;

-- Peru

insert into marketplace.daily_sc(country, date, marketplace_rev) select 'Peru', date, sum(rev) from development_pe.A_Master where isMPlace=1 and OrderAfterCan=1 and datediff(curdate(), date)<=@days group by date;

create table marketplace.temporary_daily_sc as select date, sum(rev) as total_rev from development_pe.A_Master where OrderAfterCan=1 and datediff(curdate(), date)<=@days group by date;

create index date on marketplace.temporary_daily_sc(date);

update marketplace.daily_sc m inner join marketplace.temporary_daily_sc t on m.date=t.date set m.marketplace_contribution=m.marketplace_rev/t.total_rev where country='Peru';

drop table marketplace.temporary_daily_sc;

-- Venezuela

insert into marketplace.daily_sc(country, date, marketplace_rev) select 'Venezuela', date, sum(rev) from development_ve.A_Master where isMPlace=1 and OrderAfterCan=1 and datediff(curdate(), date)<=@days group by date;

create table marketplace.temporary_daily_sc as select date, sum(rev) as total_rev from development_ve.A_Master where OrderAfterCan=1 and datediff(curdate(), date)<=@days group by date;

create index date on marketplace.temporary_daily_sc(date);

update marketplace.daily_sc m inner join marketplace.temporary_daily_sc t on m.date=t.date set m.marketplace_contribution=m.marketplace_rev/t.total_rev where country='Venezuela';

drop table marketplace.temporary_daily_sc;

-- Exchange Rate (EUR)

update marketplace.daily_sc s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=concat(year(s.date),if(month(s.date)<10,concat(0,month(s.date)),month(s.date))) set marketplace_rev=marketplace_rev/xr where s.country = 'Mexico' and r.country='MEX';

update marketplace.daily_sc s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=concat(year(s.date),if(month(s.date)<10,concat(0,month(s.date)),month(s.date))) set marketplace_rev=marketplace_rev/xr where s.country = 'Colombia' and r.country='COL';

update marketplace.daily_sc s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=concat(year(s.date),if(month(s.date)<10,concat(0,month(s.date)),month(s.date))) set marketplace_rev=marketplace_rev/xr where s.country = 'Peru' and r.country='PER';

update marketplace.daily_sc s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=concat(year(s.date),if(month(s.date)<10,concat(0,month(s.date)),month(s.date))) set marketplace_rev=marketplace_rev/xr where s.country = 'Venezuela' and r.country='VEN';

delete from marketplace.daily_sc where date=curdate();

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `marketing_key_account_manager`()
begin

set @yrmonth=201401;

delete from marketplace.marketing_key_account_manager;

-- Mexico

insert into marketplace.marketing_key_account_manager(country, yrmonth, week, date, channel_group, channel, marketplace_rev) select 'Mexico', monthnum, production.week_iso(date), date, channelgroup, channel, sum(rev) from development_mx.A_Master where orderaftercan=1 and ismplace=1 and monthnum>=@yrmonth group by date, channelgroup, channel;

create table marketplace.temporary_marketing_key_account_manager as select monthnum, production.week_iso(date) as week, date, channelgroup, channel, sum(rev) as total_rev from development_mx.A_Master where orderaftercan=1 and monthnum>=@yrmonth group by date, channelgroup, channel;

create index date on marketplace.temporary_marketing_key_account_manager(date);
create index channelgroup on marketplace.temporary_marketing_key_account_manager(channelgroup);
create index channel on marketplace.temporary_marketing_key_account_manager(channel);

update marketplace.marketing_key_account_manager m inner join marketplace.temporary_marketing_key_account_manager t on m.date=t.date and m.channel_group=t.channelgroup and m.channel=t.channel set m.marketplace_contribution=m.marketplace_rev/t.total_rev where country='Mexico';

drop table marketplace.temporary_marketing_key_account_manager;

-- Colombia

insert into marketplace.marketing_key_account_manager(country, yrmonth, week, date, channel_group, channel, marketplace_rev) select 'Colombia', monthnum, production.week_iso(date), date, channelgroup, channel, sum(rev) from development_co_project.A_Master where orderaftercan=1 and ismplace=1 and monthnum>=@yrmonth group by date, channelgroup, channel;

create table marketplace.temporary_marketing_key_account_manager as select monthnum, production.week_iso(date) as week, date, channelgroup, channel, sum(rev) as total_rev from development_co_project.A_Master where orderaftercan=1 and monthnum>=@yrmonth group by date, channelgroup, channel;

create index date on marketplace.temporary_marketing_key_account_manager(date);
create index channelgroup on marketplace.temporary_marketing_key_account_manager(channelgroup);
create index channel on marketplace.temporary_marketing_key_account_manager(channel);

update marketplace.marketing_key_account_manager m inner join marketplace.temporary_marketing_key_account_manager t on m.date=t.date and m.channel_group=t.channelgroup and m.channel=t.channel set m.marketplace_contribution=m.marketplace_rev/t.total_rev where country='Colombia';

drop table marketplace.temporary_marketing_key_account_manager;

-- Peru

insert into marketplace.marketing_key_account_manager(country, yrmonth, week, date, channel_group, channel, marketplace_rev) select 'Peru', monthnum, production.week_iso(date), date, channelgroup, channel, sum(rev) from development_pe.A_Master where orderaftercan=1 and ismplace=1 and monthnum>=@yrmonth group by date, channelgroup, channel;

create table marketplace.temporary_marketing_key_account_manager as select monthnum, production.week_iso(date) as week, date, channelgroup, channel, sum(rev) as total_rev from development_pe.A_Master where orderaftercan=1 and monthnum>=@yrmonth group by date, channelgroup, channel;

create index date on marketplace.temporary_marketing_key_account_manager(date);
create index channelgroup on marketplace.temporary_marketing_key_account_manager(channelgroup);
create index channel on marketplace.temporary_marketing_key_account_manager(channel);

update marketplace.marketing_key_account_manager m inner join marketplace.temporary_marketing_key_account_manager t on m.date=t.date and m.channel_group=t.channelgroup and m.channel=t.channel set m.marketplace_contribution=m.marketplace_rev/t.total_rev where country='Peru';

drop table marketplace.temporary_marketing_key_account_manager;

-- Venezuela

insert into marketplace.marketing_key_account_manager(country, yrmonth, week, date, channel_group, channel, marketplace_rev) select 'Venezuela', monthnum, production.week_iso(date), date, channelgroup, channel, sum(rev) from development_ve.A_Master where orderaftercan=1 and ismplace=1 and monthnum>=@yrmonth group by date, channelgroup, channel;

create table marketplace.temporary_marketing_key_account_manager as select monthnum, production.week_iso(date) as week, date, channelgroup, channel, sum(rev) as total_rev from development_ve.A_Master where orderaftercan=1 and monthnum>=@yrmonth group by date, channelgroup, channel;

create index date on marketplace.temporary_marketing_key_account_manager(date);
create index channelgroup on marketplace.temporary_marketing_key_account_manager(channelgroup);
create index channel on marketplace.temporary_marketing_key_account_manager(channel);

update marketplace.marketing_key_account_manager m inner join marketplace.temporary_marketing_key_account_manager t on m.date=t.date and m.channel_group=t.channelgroup and m.channel=t.channel set m.marketplace_contribution=m.marketplace_rev/t.total_rev where country='Venezuela';

drop table marketplace.temporary_marketing_key_account_manager;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `marketing_report`()
begin

set @days=40;

delete from marketplace.marketing_report where datediff(curdate(), date)<@days;

-- Mexico

insert into marketplace.marketing_report(country, yrmonth, week, date, weekday, channel_group, channel, cat_bp, cat1, cat2, cat3, gross_orders, net_orders, gross_revenue, net_revenue, PC1, `PC1.5`, PC2) select 'Mexico', gross.monthnum, gross.week, gross.date, gross.weekday, gross.channelgroup, gross.channel, gross.catbp, gross.cat1, gross.cat2, gross.cat3, gross.orders, net.orders, gross.revenue, net.revenue, net.PC1, net.`PC1.5`, net.PC2 from (select MonthNum, production.week_iso(date) as week, date, case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end as weekday,  channelgroup, channel, CatBP, Cat1, Cat2, Cat3, count(distinct ordernum) as orders, sum(rev) as revenue from development_mx.A_Master where orderbeforecan=1 and isMPlace=1 group by date, channelgroup, channel, CatBP, Cat1, Cat2, Cat3)gross left join (select date, channelgroup, channel, CatBP, Cat1, Cat2, Cat3, count(distinct ordernum) as orders, sum(rev) as revenue, sum(pcone) as PC1, sum(pconepfive) as `PC1.5`, sum(pctwo) as PC2 from development_mx.A_Master where orderaftercan=1 and isMPlace=1 group by date, channelgroup, channel, CatBP, Cat1, Cat2, Cat3)net on gross.date=net.date and gross.channelgroup=net.channelgroup and gross.channel=net.channel and gross.catbp=net.catbp and gross.cat1=net.cat1 and gross.cat2=net.cat2 and gross.cat3=net.cat3 where datediff(curdate(), gross.date)<@days;

update marketplace.marketing_report m set new_customers = (select count(distinct ordernum) from development_mx.A_Master a where a.orderaftercan=1 and a.isMPlace=1 and a.newreturning='NEW' and m.date=a.date and m.channel_group=a.channelgroup and m.channel=a.channel and m.cat_bp=a.catbp and m.cat1=a.cat1 and m.cat2=a.cat2 and m.cat3=a.cat3) where country='Mexico' and datediff(curdate(), m.date)<@days;

update marketplace.marketing_report m set new_customers_gross = (select count(distinct ordernum) from development_mx.A_Master a where a.orderbeforecan=1 and a.isMPlace=1 and a.newreturninggross='NEW' and m.date=a.date and m.channel_group=a.channelgroup and m.channel=a.channel and m.cat_bp=a.catbp and m.cat1=a.cat1 and m.cat2=a.cat2 and m.cat3=a.cat3) where country='Mexico' and datediff(curdate(), m.date)<@days;

update marketplace.marketing_report m set returning_customers = (select count(distinct ordernum) from development_mx.A_Master a where a.orderaftercan=1 and a.isMPlace=1 and a.newreturning='RETURNING' and m.date=a.date and m.channel_group=a.channelgroup and m.channel=a.channel and m.cat_bp=a.catbp and m.cat1=a.cat1 and m.cat2=a.cat2 and m.cat3=a.cat3) where country='Mexico' and datediff(curdate(), m.date)<@days;

update marketplace.marketing_report m set pending_revenue = (select sum(rev) from development_mx.A_Master a where a.pending=1 and a.isMPlace=1 and m.date=a.date and m.channel_group=a.channelgroup and m.channel=a.channel and m.cat_bp=a.catbp and m.cat1=a.cat1 and m.cat2=a.cat2 and m.cat3=a.cat3) where country='Mexico' and datediff(curdate(), m.date)<@days;

update marketplace.marketing_report m set canceled_revenue = (select sum(rev) from development_mx.A_Master a where a.Cancellations=1 and a.isMPlace=1 and m.date=a.date and m.channel_group=a.channelgroup and m.channel=a.channel and m.cat_bp=a.catbp and m.cat1=a.cat1 and m.cat2=a.cat2 and m.cat3=a.cat3) where country='Mexico' and datediff(curdate(), m.date)<@days;

update marketplace.marketing_report m set all_items = (select count(*) from development_mx.A_Master a where a.orderaftercan=1 and a.isMPlace=1 and m.date=a.date and m.channel_group=a.channelgroup and m.channel=a.channel and m.cat_bp=a.catbp and m.cat1=a.cat1 and m.cat2=a.cat2 and m.cat3=a.cat3) where country='Mexico' and datediff(curdate(), m.date)<@days;

create table marketplace.temporary_oos as select date, channelgroup as channel_group, channel, catbp as cat_bp, cat1, cat2, cat3, count(*) as stock_out from development_mx.A_Master a, wmsprod.itens_venda i where a.itemid = i.item_id and a.OrderBeforeCan=1 and (i.status='quebra tratada' or i.status='quebrado') and datediff(curdate(), a.date)<@days group by date, channelgroup, channel, catbp, cat1, cat2, cat3;

create index channel on marketplace.temporary_oos(date, channel_group, channel);
create index cat on marketplace.temporary_oos(cat1, cat2, cat3);

update marketplace.marketing_report m inner join marketplace.temporary_oos o on m.date=o.date and m.channel_group=o.channel_group and m.channel=o.channel and m.cat_bp=o.cat_bp and m.cat1=o.cat1 and m.cat2=o.cat2 and m.cat3=o.cat3 set m.stock_out=o.stock_out where country='Mexico' and datediff(curdate(), m.date)<@days;

drop table marketplace.temporary_oos;

create table marketplace.temporary_marketing_report as select date, channelgroup as channel_group, channel, catbp as cat_bp, cat1, cat2, cat3,  sum(rev) as total_rev from development_mx.A_Master where orderaftercan=1 and datediff(curdate(), date)<@days group by date, channel_group, channel, cat_bp, cat1, cat2, cat3;

create index date on marketplace.temporary_marketing_report(date);
create index channel_group on marketplace.temporary_marketing_report(channel_group);
create index channel on marketplace.temporary_marketing_report(channel);
create index cat_bp on marketplace.temporary_marketing_report(cat_bp);
create index cat1 on marketplace.temporary_marketing_report(cat1);
create index cat2 on marketplace.temporary_marketing_report(cat2);
create index cat3 on marketplace.temporary_marketing_report(cat3);

update marketplace.marketing_report m inner join marketplace.temporary_marketing_report t on m.date=t.date and m.channel_group=t.channel_group and m.channel=t.channel and m.cat_bp=t.cat_bp and m.cat1=t.cat1 and m.cat2=t.cat2 and m.cat3=t.cat3 set m.contribution=m.net_revenue/t.total_rev where country='Mexico' and datediff(curdate(), m.date)<@days;

drop table marketplace.temporary_marketing_report;

update marketplace.marketing_report s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, pending_revenue=pending_revenue/xr, canceled_revenue=canceled_revenue/xr, PC1=PC1/xr, `PC1.5`=`PC1.5`/xr, PC2=PC2/xr, PC3=PC3/xr where s.country = 'Mexico' and r.country='MEX' and datediff(curdate(), s.date)<@days;

-- Colombia

insert into marketplace.marketing_report(country, yrmonth, week, date, weekday, channel_group, channel, cat_bp, cat1, cat2, cat3, gross_orders, net_orders, gross_revenue, net_revenue, PC1, `PC1.5`, PC2) select 'Colombia', gross.monthnum, gross.week, gross.date, gross.weekday, gross.channelgroup, gross.channel, gross.catbp, gross.cat1, gross.cat2, gross.cat3, gross.orders, net.orders, gross.revenue, net.revenue, net.PC1, net.`PC1.5`, net.PC2 from (select MonthNum, production.week_iso(date) as week, date, case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end as weekday,  channelgroup, channel, CatBP, Cat1, Cat2, Cat3, count(distinct ordernum) as orders, sum(rev) as revenue from development_co_project.A_Master where orderbeforecan=1 and isMPlace=1 group by date, channelgroup, channel, CatBP, Cat1, Cat2, Cat3)gross left join (select date, channelgroup, channel, CatBP, Cat1, Cat2, Cat3, count(distinct ordernum) as orders, sum(rev) as revenue, sum(pcone) as PC1, sum(pconepfive) as `PC1.5`, sum(pctwo) as PC2 from development_co_project.A_Master where orderaftercan=1 and isMPlace=1 group by date, channelgroup, channel, CatBP, Cat1, Cat2, Cat3)net on gross.date=net.date and gross.channelgroup=net.channelgroup and gross.channel=net.channel and gross.catbp=net.catbp and gross.cat1=net.cat1 and gross.cat2=net.cat2 and gross.cat3=net.cat3 where datediff(curdate(), gross.date)<@days;

update marketplace.marketing_report m set new_customers = (select count(distinct ordernum) from development_co_project.A_Master a where a.orderaftercan=1 and a.isMPlace=1 and a.newreturning='NEW' and m.date=a.date and m.channel_group=a.channelgroup and m.channel=a.channel and m.cat_bp=a.catbp and m.cat1=a.cat1 and m.cat2=a.cat2 and m.cat3=a.cat3) where country='Colombia' and datediff(curdate(), m.date)<@days;

update marketplace.marketing_report m set new_customers_gross = (select count(distinct ordernum) from development_co_project.A_Master a where a.orderbeforecan=1 and a.isMPlace=1 and a.newreturninggross='NEW' and m.date=a.date and m.channel_group=a.channelgroup and m.channel=a.channel and m.cat_bp=a.catbp and m.cat1=a.cat1 and m.cat2=a.cat2 and m.cat3=a.cat3) where country='Colombia' and datediff(curdate(), m.date)<@days;

update marketplace.marketing_report m set returning_customers = (select count(distinct ordernum) from development_co_project.A_Master a where a.orderaftercan=1 and a.isMPlace=1 and a.newreturning='RETURNING' and m.date=a.date and m.channel_group=a.channelgroup and m.channel=a.channel and m.cat_bp=a.catbp and m.cat1=a.cat1 and m.cat2=a.cat2 and m.cat3=a.cat3) where country='Colombia' and datediff(curdate(), m.date)<@days;

update marketplace.marketing_report m set pending_revenue = (select sum(rev) from development_co_project.A_Master a where a.pending=1 and a.isMPlace=1 and m.date=a.date and m.channel_group=a.channelgroup and m.channel=a.channel and m.cat_bp=a.catbp and m.cat1=a.cat1 and m.cat2=a.cat2 and m.cat3=a.cat3) where country='Colombia' and datediff(curdate(), m.date)<@days;

update marketplace.marketing_report m set canceled_revenue = (select sum(rev) from development_co_project.A_Master a where a.Cancellations=1 and a.isMPlace=1 and m.date=a.date and m.channel_group=a.channelgroup and m.channel=a.channel and m.cat_bp=a.catbp and m.cat1=a.cat1 and m.cat2=a.cat2 and m.cat3=a.cat3) where country='Colombia' and datediff(curdate(), m.date)<@days;

update marketplace.marketing_report m set all_items = (select count(*) from development_co_project.A_Master a where a.orderaftercan=1 and a.isMPlace=1 and m.date=a.date and m.channel_group=a.channelgroup and m.channel=a.channel and m.cat_bp=a.catbp and m.cat1=a.cat1 and m.cat2=a.cat2 and m.cat3=a.cat3) where country='Colombia' and datediff(curdate(), m.date)<@days;

create table marketplace.temporary_oos as select date, channelgroup as channel_group, channel, catbp as cat_bp, cat1, cat2, cat3, count(*) as stock_out from development_co_project.A_Master a, wmsprod_co.itens_venda i where a.itemid = i.item_id and a.OrderBeforeCan=1 and (i.status='quebra tratada' or i.status='quebrado') and datediff(curdate(), a.date)<@days group by date, channelgroup, channel, catbp, cat1, cat2, cat3;

create index channel on marketplace.temporary_oos(date, channel_group, channel);
create index cat on marketplace.temporary_oos(cat1, cat2, cat3);

update marketplace.marketing_report m inner join marketplace.temporary_oos o on m.date=o.date and m.channel_group=o.channel_group and m.channel=o.channel and m.cat_bp=o.cat_bp and m.cat1=o.cat1 and m.cat2=o.cat2 and m.cat3=o.cat3 set m.stock_out=o.stock_out where country='Colombia' and datediff(curdate(), m.date)<@days;

drop table marketplace.temporary_oos;

create table marketplace.temporary_marketing_report as select date, channelgroup as channel_group, channel, catbp as cat_bp, cat1, cat2, cat3,  sum(rev) as total_rev from development_co_project.A_Master where orderaftercan=1 and datediff(curdate(), date)<@days group by date, channel_group, channel, cat_bp, cat1, cat2, cat3;

create index date on marketplace.temporary_marketing_report(date);
create index channel_group on marketplace.temporary_marketing_report(channel_group);
create index channel on marketplace.temporary_marketing_report(channel);
create index cat_bp on marketplace.temporary_marketing_report(cat_bp);
create index cat1 on marketplace.temporary_marketing_report(cat1);
create index cat2 on marketplace.temporary_marketing_report(cat2);
create index cat3 on marketplace.temporary_marketing_report(cat3);

update marketplace.marketing_report m inner join marketplace.temporary_marketing_report t on m.date=t.date and m.channel_group=t.channel_group and m.channel=t.channel and m.cat_bp=t.cat_bp and m.cat1=t.cat1 and m.cat2=t.cat2 and m.cat3=t.cat3 set m.contribution=m.net_revenue/t.total_rev where country='Colombia' and datediff(curdate(), m.date)<@days;

drop table marketplace.temporary_marketing_report;

update marketplace.marketing_report s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, pending_revenue=pending_revenue/xr, canceled_revenue=canceled_revenue/xr, PC1=PC1/xr, `PC1.5`=`PC1.5`/xr, PC2=PC2/xr, PC3=PC3/xr where s.country = 'Colombia' and r.country='COL' and datediff(curdate(), s.date)<@days;

-- Peru

insert into marketplace.marketing_report(country, yrmonth, week, date, weekday, channel_group, channel, cat_bp, cat1, cat2, cat3, gross_orders, net_orders, gross_revenue, net_revenue, PC1, `PC1.5`, PC2) select 'Peru', gross.monthnum, gross.week, gross.date, gross.weekday, gross.channelgroup, gross.channel, gross.catbp, gross.cat1, gross.cat2, gross.cat3, gross.orders, net.orders, gross.revenue, net.revenue, net.PC1, net.`PC1.5`, net.PC2 from (select MonthNum, production.week_iso(date) as week, date, case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end as weekday,  channelgroup, channel, CatBP, Cat1, Cat2, Cat3, count(distinct ordernum) as orders, sum(rev) as revenue from development_pe.A_Master where orderbeforecan=1 and isMPlace=1 group by date, channelgroup, channel, CatBP, Cat1, Cat2, Cat3)gross left join (select date, channelgroup, channel, CatBP, Cat1, Cat2, Cat3, count(distinct ordernum) as orders, sum(rev) as revenue, sum(pcone) as PC1, sum(pconepfive) as `PC1.5`, sum(pctwo) as PC2 from development_pe.A_Master where orderaftercan=1 and isMPlace=1 group by date, channelgroup, channel, CatBP, Cat1, Cat2, Cat3)net on gross.date=net.date and gross.channelgroup=net.channelgroup and gross.channel=net.channel and gross.catbp=net.catbp and gross.cat1=net.cat1 and gross.cat2=net.cat2 and gross.cat3=net.cat3 where datediff(curdate(), gross.date)<@days;

update marketplace.marketing_report m set new_customers = (select count(distinct ordernum) from development_pe.A_Master a where a.orderaftercan=1 and a.isMPlace=1 and a.newreturning='NEW' and m.date=a.date and m.channel_group=a.channelgroup and m.channel=a.channel and m.cat_bp=a.catbp and m.cat1=a.cat1 and m.cat2=a.cat2 and m.cat3=a.cat3) where country='Peru' and datediff(curdate(), m.date)<@days;

update marketplace.marketing_report m set new_customers_gross = (select count(distinct ordernum) from development_pe.A_Master a where a.orderbeforecan=1 and a.isMPlace=1 and a.newreturninggross='NEW' and m.date=a.date and m.channel_group=a.channelgroup and m.channel=a.channel and m.cat_bp=a.catbp and m.cat1=a.cat1 and m.cat2=a.cat2 and m.cat3=a.cat3) where country='Peru' and datediff(curdate(), m.date)<@days;

update marketplace.marketing_report m set returning_customers = (select count(distinct ordernum) from development_pe.A_Master a where a.orderaftercan=1 and a.isMPlace=1 and a.newreturning='RETURNING' and m.date=a.date and m.channel_group=a.channelgroup and m.channel=a.channel and m.cat_bp=a.catbp and m.cat1=a.cat1 and m.cat2=a.cat2 and m.cat3=a.cat3) where country='Peru' and datediff(curdate(), m.date)<@days;

update marketplace.marketing_report m set pending_revenue = (select sum(rev) from development_pe.A_Master a where a.pending=1 and a.isMPlace=1 and m.date=a.date and m.channel_group=a.channelgroup and m.channel=a.channel and m.cat_bp=a.catbp and m.cat1=a.cat1 and m.cat2=a.cat2 and m.cat3=a.cat3) where country='Peru' and datediff(curdate(), m.date)<@days;

update marketplace.marketing_report m set canceled_revenue = (select sum(rev) from development_pe.A_Master a where a.Cancellations=1 and a.isMPlace=1 and m.date=a.date and m.channel_group=a.channelgroup and m.channel=a.channel and m.cat_bp=a.catbp and m.cat1=a.cat1 and m.cat2=a.cat2 and m.cat3=a.cat3) where country='Peru' and datediff(curdate(), m.date)<@days;

update marketplace.marketing_report m set all_items = (select count(*) from development_pe.A_Master a where a.orderaftercan=1 and a.isMPlace=1 and m.date=a.date and m.channel_group=a.channelgroup and m.channel=a.channel and m.cat_bp=a.catbp and m.cat1=a.cat1 and m.cat2=a.cat2 and m.cat3=a.cat3) where country='Peru' and datediff(curdate(), m.date)<@days;

create table marketplace.temporary_oos as select date, channelgroup as channel_group, channel, catbp as cat_bp, cat1, cat2, cat3, count(*) as stock_out from development_pe.A_Master a, wmsprod_pe.itens_venda i where a.itemid = i.item_id and a.OrderBeforeCan=1 and (i.status='quebra tratada' or i.status='quebrado') and datediff(curdate(), a.date)<@days group by date, channelgroup, channel, catbp, cat1, cat2, cat3;

create index channel on marketplace.temporary_oos(date, channel_group, channel);
create index cat on marketplace.temporary_oos(cat1, cat2, cat3);

update marketplace.marketing_report m inner join marketplace.temporary_oos o on m.date=o.date and m.channel_group=o.channel_group and m.channel=o.channel and m.cat_bp=o.cat_bp and m.cat1=o.cat1 and m.cat2=o.cat2 and m.cat3=o.cat3 set m.stock_out=o.stock_out where country='Peru' and datediff(curdate(), m.date)<@days;

drop table marketplace.temporary_oos;

create table marketplace.temporary_marketing_report as select date, channelgroup as channel_group, channel, catbp as cat_bp, cat1, cat2, cat3,  sum(rev) as total_rev from development_pe.A_Master where orderaftercan=1 and datediff(curdate(), date)<@days group by date, channel_group, channel, cat_bp, cat1, cat2, cat3;

create index date on marketplace.temporary_marketing_report(date);
create index channel_group on marketplace.temporary_marketing_report(channel_group);
create index channel on marketplace.temporary_marketing_report(channel);
create index cat_bp on marketplace.temporary_marketing_report(cat_bp);
create index cat1 on marketplace.temporary_marketing_report(cat1);
create index cat2 on marketplace.temporary_marketing_report(cat2);
create index cat3 on marketplace.temporary_marketing_report(cat3);

update marketplace.marketing_report m inner join marketplace.temporary_marketing_report t on m.date=t.date and m.channel_group=t.channel_group and m.channel=t.channel and m.cat_bp=t.cat_bp and m.cat1=t.cat1 and m.cat2=t.cat2 and m.cat3=t.cat3 set m.contribution=m.net_revenue/t.total_rev where country='Peru' and datediff(curdate(), m.date)<@days;

drop table marketplace.temporary_marketing_report;

update marketplace.marketing_report s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, pending_revenue=pending_revenue/xr, canceled_revenue=canceled_revenue/xr, PC1=PC1/xr, `PC1.5`=`PC1.5`/xr, PC2=PC2/xr, PC3=PC3/xr where s.country = 'Peru' and r.country='PER' and datediff(curdate(), s.date)<@days;

-- Venezuela

insert into marketplace.marketing_report(country, yrmonth, week, date, weekday, channel_group, channel, cat_bp, cat1, cat2, cat3, gross_orders, net_orders, gross_revenue, net_revenue, PC1, `PC1.5`, PC2) select 'Venezuela', gross.monthnum, gross.week, gross.date, gross.weekday, gross.channelgroup, gross.channel, gross.catbp, gross.cat1, gross.cat2, gross.cat3, gross.orders, net.orders, gross.revenue, net.revenue, net.PC1, net.`PC1.5`, net.PC2 from (select MonthNum, production.week_iso(date) as week, date, case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end as weekday,  channelgroup, channel, CatBP, Cat1, Cat2, Cat3, count(distinct ordernum) as orders, sum(rev) as revenue from development_ve.A_Master where orderbeforecan=1 and isMPlace=1 group by date, channelgroup, channel, CatBP, Cat1, Cat2, Cat3)gross left join (select date, channelgroup, channel, CatBP, Cat1, Cat2, Cat3, count(distinct ordernum) as orders, sum(rev) as revenue, sum(pcone) as PC1, sum(pconepfive) as `PC1.5`, sum(pctwo) as PC2 from development_ve.A_Master where orderaftercan=1 and isMPlace=1 group by date, channelgroup, channel, CatBP, Cat1, Cat2, Cat3)net on gross.date=net.date and gross.channelgroup=net.channelgroup and gross.channel=net.channel and gross.catbp=net.catbp and gross.cat1=net.cat1 and gross.cat2=net.cat2 and gross.cat3=net.cat3 where datediff(curdate(), gross.date)<@days;

update marketplace.marketing_report m set new_customers = (select count(distinct ordernum) from development_ve.A_Master a where a.orderaftercan=1 and a.isMPlace=1 and a.newreturning='NEW' and m.date=a.date and m.channel_group=a.channelgroup and m.channel=a.channel and m.cat_bp=a.catbp and m.cat1=a.cat1 and m.cat2=a.cat2 and m.cat3=a.cat3) where country='Venezuela' and datediff(curdate(), m.date)<@days;

update marketplace.marketing_report m set new_customers_gross = (select count(distinct ordernum) from development_ve.A_Master a where a.orderbeforecan=1 and a.isMPlace=1 and a.newreturninggross='NEW' and m.date=a.date and m.channel_group=a.channelgroup and m.channel=a.channel and m.cat_bp=a.catbp and m.cat1=a.cat1 and m.cat2=a.cat2 and m.cat3=a.cat3) where country='Venezuela' and datediff(curdate(), m.date)<@days;

update marketplace.marketing_report m set returning_customers = (select count(distinct ordernum) from development_ve.A_Master a where a.orderaftercan=1 and a.isMPlace=1 and a.newreturning='RETURNING' and m.date=a.date and m.channel_group=a.channelgroup and m.channel=a.channel and m.cat_bp=a.catbp and m.cat1=a.cat1 and m.cat2=a.cat2 and m.cat3=a.cat3) where country='Venezuela' and datediff(curdate(), m.date)<@days;

update marketplace.marketing_report m set pending_revenue = (select sum(rev) from development_ve.A_Master a where a.pending=1 and a.isMPlace=1 and m.date=a.date and m.channel_group=a.channelgroup and m.channel=a.channel and m.cat_bp=a.catbp and m.cat1=a.cat1 and m.cat2=a.cat2 and m.cat3=a.cat3) where country='Venezuela' and datediff(curdate(), m.date)<@days;

update marketplace.marketing_report m set canceled_revenue = (select sum(rev) from development_ve.A_Master a where a.Cancellations=1 and a.isMPlace=1 and m.date=a.date and m.channel_group=a.channelgroup and m.channel=a.channel and m.cat_bp=a.catbp and m.cat1=a.cat1 and m.cat2=a.cat2 and m.cat3=a.cat3) where country='Venezuela' and datediff(curdate(), m.date)<@days;

update marketplace.marketing_report m set all_items = (select count(*) from development_ve.A_Master a where a.orderaftercan=1 and a.isMPlace=1 and m.date=a.date and m.channel_group=a.channelgroup and m.channel=a.channel and m.cat_bp=a.catbp and m.cat1=a.cat1 and m.cat2=a.cat2 and m.cat3=a.cat3) where country='Venezuela' and datediff(curdate(), m.date)<@days;

create table marketplace.temporary_oos as select date, channelgroup as channel_group, channel, catbp as cat_bp, cat1, cat2, cat3, count(*) as stock_out from development_ve.A_Master a, wmsprod_ve.itens_venda i where a.itemid = i.item_id and a.OrderBeforeCan=1 and (i.status='quebra tratada' or i.status='quebrado') and datediff(curdate(), a.date)<@days group by date, channelgroup, channel, catbp, cat1, cat2, cat3;

create index channel on marketplace.temporary_oos(date, channel_group, channel);
create index cat on marketplace.temporary_oos(cat1, cat2, cat3);

update marketplace.marketing_report m inner join marketplace.temporary_oos o on m.date=o.date and m.channel_group=o.channel_group and m.channel=o.channel and m.cat_bp=o.cat_bp and m.cat1=o.cat1 and m.cat2=o.cat2 and m.cat3=o.cat3 set m.stock_out=o.stock_out where country='Venezuela' and datediff(curdate(), m.date)<@days;

drop table marketplace.temporary_oos;

create table marketplace.temporary_marketing_report as select date, channelgroup as channel_group, channel, catbp as cat_bp, cat1, cat2, cat3,  sum(rev) as total_rev from development_ve.A_Master where orderaftercan=1 and datediff(curdate(), date)<@days group by date, channel_group, channel, cat_bp, cat1, cat2, cat3;

create index date on marketplace.temporary_marketing_report(date);
create index channel_group on marketplace.temporary_marketing_report(channel_group);
create index channel on marketplace.temporary_marketing_report(channel);
create index cat_bp on marketplace.temporary_marketing_report(cat_bp);
create index cat1 on marketplace.temporary_marketing_report(cat1);
create index cat2 on marketplace.temporary_marketing_report(cat2);
create index cat3 on marketplace.temporary_marketing_report(cat3);

update marketplace.marketing_report m inner join marketplace.temporary_marketing_report t on m.date=t.date and m.channel_group=t.channel_group and m.channel=t.channel and m.cat_bp=t.cat_bp and m.cat1=t.cat1 and m.cat2=t.cat2 and m.cat3=t.cat3 set m.contribution=m.net_revenue/t.total_rev where country='Venezuela' and datediff(curdate(), m.date)<@days;

drop table marketplace.temporary_marketing_report;

update marketplace.marketing_report s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, pending_revenue=pending_revenue/xr, canceled_revenue=canceled_revenue/xr, PC1=PC1/xr, `PC1.5`=`PC1.5`/xr, PC2=PC2/xr, PC3=PC3/xr where s.country = 'Venezuela' and r.country='VEN' and datediff(curdate(), s.date)<@days;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `marketplace`()
begin

call marketplace.daily_sc;

call marketplace.weekly_sc;

call marketplace.yearly_sc;

call marketplace.marketing_key_account_manager;

call marketplace.sku_history;

call marketplace.sku_history_cat_bp;

call marketplace.sellers_trend;

call marketplace.ops_dashboard;

call marketplace.regional_report;

call marketplace.news_report;

insert into production.table_monitoring_log ( country, table_name, updated_at, key_date, total_rows, total_rows_check, step) select 'Regional', 'marketplace', NOW(), NOW(), 1, 1, 'finish';

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `news_report`()
begin

delete from marketplace.news_report;

alter table marketplace.news_report AUTO_INCREMENT = 1;

-- Mexico

insert into marketplace.news_report(country, yrmonth, week, type, net_revenue) select 'Mexico', yrmonth, week, type, net_revenue from (select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, production.week_iso(date) as week, 'Marketplace' as type, sum(rev) net_revenue from development_mx.A_Master where OrderAfterCan=1 and isMPlace=1 and MonthNum>=201401 group by production.week_iso(date)
union
select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, production.week_iso(date) as week, 'All' as type, sum(rev) as net_revenue from development_mx.A_Master where OrderAfterCan=1 and MonthNum>=201401 group by production.week_iso(date))a order by week, type;

create table marketplace.temporary_news_report as select week, net_revenue from marketplace.news_report where type='All' and country='Mexico';

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n inner join marketplace.temporary_news_report t on n.week=t.week set `% share` = n.net_revenue/t.net_revenue where n.country='Mexico';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select id_news_report, week, net_revenue from marketplace.news_report where country='Mexico';

create index id_news_report on marketplace.temporary_news_report(id_news_report);

update marketplace.news_report n inner join marketplace.temporary_news_report t on n.id_news_report=t.id_news_report+2 set WoW_net_revenue = (n.net_revenue/t.net_revenue)-1 where country='Mexico';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select production.week_iso(curdate()) as week, count(distinct supplier) as supplier from development_mx.A_Master_Catalog c where c.ismarketplace=1 and c.isactive_skuconfig=1 group by production.week_iso(curdate());

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n set sellers_active = (select sum(supplier) from marketplace.temporary_news_report c where n.week=c.week) where country='Mexico' and type='Marketplace';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select production.week_iso(curdate()) as week, count(distinct supplier) as supplier from development_mx.A_Master_Catalog c where c.isactive_skuconfig=1 group by production.week_iso(curdate());

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n set sellers_active = (select sum(supplier) from marketplace.temporary_news_report c where n.week=c.week) where country='Mexico' and type='All';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select production.week_iso(curdate()) as week, count(distinct supplier) as supplier from development_mx.A_Master_Catalog c where c.ismarketplace=1 and c.isvisible=1 group by production.week_iso(curdate());

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n set sellers_visible = (select sum(supplier) from marketplace.temporary_news_report c where n.week=c.week) where country='Mexico' and type='Marketplace';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select production.week_iso(curdate()) as week, count(distinct supplier) as supplier from development_mx.A_Master_Catalog c where c.isvisible=1 group by production.week_iso(curdate());

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n set sellers_visible = (select sum(supplier) from marketplace.temporary_news_report c where n.week=c.week) where country='Mexico' and type='All';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select production.week_iso(curdate()) as week, count(distinct supplier) as supplier from development_mx.A_Master a where a.orderaftercan=1 and a.ismplace=1 group by production.week_iso(curdate());

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n set sellers_sales = (select sum(supplier) from marketplace.temporary_news_report c where n.week=c.week) where country='Mexico' and type='Marketplace';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select production.week_iso(curdate()) as week, count(distinct supplier) as supplier from development_mx.A_Master a where a.orderaftercan=1 group by production.week_iso(curdate());

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n set sellers_sales = (select sum(supplier) from marketplace.temporary_news_report c where n.week=c.week) where country='Mexico' and type='All';

drop table marketplace.temporary_news_report;

-- Colombia

insert into marketplace.news_report(country, yrmonth, week, type, net_revenue) select 'Colombia', yrmonth, week, type, net_revenue from (select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, production.week_iso(date) as week, 'Marketplace' as type, sum(rev) net_revenue from development_co_project.A_Master where OrderAfterCan=1 and isMPlace=1 and MonthNum>=201401 group by production.week_iso(date)
union
select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, production.week_iso(date) as week, 'All' as type, sum(rev) as net_revenue from development_co_project.A_Master where OrderAfterCan=1 and MonthNum>=201401 group by production.week_iso(date))a order by week, type;

create table marketplace.temporary_news_report as select week, net_revenue from marketplace.news_report where type='All' and country='Colombia';

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n inner join marketplace.temporary_news_report t on n.week=t.week set `% share` = n.net_revenue/t.net_revenue where n.country='Colombia';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select id_news_report, week, net_revenue from marketplace.news_report where country='Colombia';

create index id_news_report on marketplace.temporary_news_report(id_news_report);

update marketplace.news_report n inner join marketplace.temporary_news_report t on n.id_news_report=t.id_news_report+2 set WoW_net_revenue = (n.net_revenue/t.net_revenue)-1 where country='Colombia';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select production.week_iso(curdate()) as week, count(distinct supplier) as supplier from development_co_project.A_Master_Catalog c where c.ismarketplace=1 and c.isactive_skuconfig=1 group by production.week_iso(curdate());

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n set sellers_active = (select sum(supplier) from marketplace.temporary_news_report c where n.week=c.week) where country='Colombia' and type='Marketplace';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select production.week_iso(curdate()) as week, count(distinct supplier) as supplier from development_co_project.A_Master_Catalog c where c.isactive_skuconfig=1 group by production.week_iso(curdate());

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n set sellers_active = (select sum(supplier) from marketplace.temporary_news_report c where n.week=c.week) where country='Colombia' and type='All';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select production.week_iso(curdate()) as week, count(distinct supplier) as supplier from development_co_project.A_Master_Catalog c where c.ismarketplace=1 and c.isvisible=1 group by production.week_iso(curdate());

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n set sellers_visible = (select sum(supplier) from marketplace.temporary_news_report c where n.week=c.week) where country='Colombia' and type='Marketplace';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select production.week_iso(curdate()) as week, count(distinct supplier) as supplier from development_co_project.A_Master_Catalog c where c.isvisible=1 group by production.week_iso(curdate());

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n set sellers_visible = (select sum(supplier) from marketplace.temporary_news_report c where n.week=c.week) where country='Colombia' and type='All';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select production.week_iso(curdate()) as week, count(distinct supplier) as supplier from development_co_project.A_Master a where a.orderaftercan=1 and a.ismplace=1 group by production.week_iso(curdate());

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n set sellers_sales = (select sum(supplier) from marketplace.temporary_news_report c where n.week=c.week) where country='Colombia' and type='Marketplace';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select production.week_iso(curdate()) as week, count(distinct supplier) as supplier from development_co_project.A_Master a where a.orderaftercan=1 group by production.week_iso(curdate());

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n set sellers_sales = (select sum(supplier) from marketplace.temporary_news_report c where n.week=c.week) where country='Colombia' and type='All';

drop table marketplace.temporary_news_report;

-- Peru

insert into marketplace.news_report(country, yrmonth, week, type, net_revenue) select 'Peru', yrmonth, week, type, net_revenue from (select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, production.week_iso(date) as week, 'Marketplace' as type, sum(rev) net_revenue from development_pe.A_Master where OrderAfterCan=1 and isMPlace=1 and MonthNum>=201401 group by production.week_iso(date)
union
select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, production.week_iso(date) as week, 'All' as type, sum(rev) as net_revenue from development_pe.A_Master where OrderAfterCan=1 and MonthNum>=201401 group by production.week_iso(date))a order by week, type;

create table marketplace.temporary_news_report as select week, net_revenue from marketplace.news_report where type='All' and country='Peru';

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n inner join marketplace.temporary_news_report t on n.week=t.week set `% share` = n.net_revenue/t.net_revenue where n.country='Peru';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select id_news_report, week, net_revenue from marketplace.news_report where country='Peru';

create index id_news_report on marketplace.temporary_news_report(id_news_report);

update marketplace.news_report n inner join marketplace.temporary_news_report t on n.id_news_report=t.id_news_report+2 set WoW_net_revenue = (n.net_revenue/t.net_revenue)-1 where country='Peru';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select production.week_iso(curdate()) as week, count(distinct supplier) as supplier from development_pe.A_Master_Catalog c where c.ismarketplace=1 and c.isactive_skuconfig=1 group by production.week_iso(curdate());

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n set sellers_active = (select sum(supplier) from marketplace.temporary_news_report c where n.week=c.week) where country='Peru' and type='Marketplace';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select production.week_iso(curdate()) as week, count(distinct supplier) as supplier from development_pe.A_Master_Catalog c where c.isactive_skuconfig=1 group by production.week_iso(curdate());

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n set sellers_active = (select sum(supplier) from marketplace.temporary_news_report c where n.week=c.week) where country='Peru' and type='All';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select production.week_iso(curdate()) as week, count(distinct supplier) as supplier from development_pe.A_Master_Catalog c where c.ismarketplace=1 and c.isvisible=1 group by production.week_iso(curdate());

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n set sellers_visible = (select sum(supplier) from marketplace.temporary_news_report c where n.week=c.week) where country='Peru' and type='Marketplace';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select production.week_iso(curdate()) as week, count(distinct supplier) as supplier from development_pe.A_Master_Catalog c where c.isvisible=1 group by production.week_iso(curdate());

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n set sellers_visible = (select sum(supplier) from marketplace.temporary_news_report c where n.week=c.week) where country='Peru' and type='All';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select production.week_iso(curdate()) as week, count(distinct supplier) as supplier from development_pe.A_Master a where a.orderaftercan=1 and a.ismplace=1 group by production.week_iso(curdate());

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n set sellers_sales = (select sum(supplier) from marketplace.temporary_news_report c where n.week=c.week) where country='Peru' and type='Marketplace';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select production.week_iso(curdate()) as week, count(distinct supplier) as supplier from development_pe.A_Master a where a.orderaftercan=1 group by production.week_iso(curdate());

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n set sellers_sales = (select sum(supplier) from marketplace.temporary_news_report c where n.week=c.week) where country='Peru' and type='All';

drop table marketplace.temporary_news_report;

-- Venezuela

insert into marketplace.news_report(country, yrmonth, week, type, net_revenue) select 'Venezuela', yrmonth, week, type, net_revenue from (select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, production.week_iso(date) as week, 'Marketplace' as type, sum(rev) net_revenue from development_ve.A_Master where OrderAfterCan=1 and isMPlace=1 and MonthNum>=201401 group by production.week_iso(date)
union
select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, production.week_iso(date) as week, 'All' as type, sum(rev) as net_revenue from development_ve.A_Master where OrderAfterCan=1 and MonthNum>=201401 group by production.week_iso(date))a order by week, type;

create table marketplace.temporary_news_report as select week, net_revenue from marketplace.news_report where type='All' and country='Venezuela';

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n inner join marketplace.temporary_news_report t on n.week=t.week set `% share` = n.net_revenue/t.net_revenue where n.country='Venezuela';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select id_news_report, week, net_revenue from marketplace.news_report where country='Venezuela';

create index id_news_report on marketplace.temporary_news_report(id_news_report);

update marketplace.news_report n inner join marketplace.temporary_news_report t on n.id_news_report=t.id_news_report+2 set WoW_net_revenue = (n.net_revenue/t.net_revenue)-1 where country='Venezuela';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select production.week_iso(curdate()) as week, count(distinct supplier) as supplier from development_ve.A_Master_Catalog c where c.ismarketplace=1 and c.isactive_skuconfig=1 group by production.week_iso(curdate());

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n set sellers_active = (select sum(supplier) from marketplace.temporary_news_report c where n.week=c.week) where country='Venezuela' and type='Marketplace';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select production.week_iso(curdate()) as week, count(distinct supplier) as supplier from development_ve.A_Master_Catalog c where c.isactive_skuconfig=1 group by production.week_iso(curdate());

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n set sellers_active = (select sum(supplier) from marketplace.temporary_news_report c where n.week=c.week) where country='Venezuela' and type='All';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select production.week_iso(curdate()) as week, count(distinct supplier) as supplier from development_ve.A_Master_Catalog c where c.ismarketplace=1 and c.isvisible=1 group by production.week_iso(curdate());

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n set sellers_visible = (select sum(supplier) from marketplace.temporary_news_report c where n.week=c.week) where country='Venezuela' and type='Marketplace';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select production.week_iso(curdate()) as week, count(distinct supplier) as supplier from development_ve.A_Master_Catalog c where c.isvisible=1 group by production.week_iso(curdate());

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n set sellers_visible = (select sum(supplier) from marketplace.temporary_news_report c where n.week=c.week) where country='Venezuela' and type='All';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select production.week_iso(curdate()) as week, count(distinct supplier) as supplier from development_ve.A_Master a where a.orderaftercan=1 and a.ismplace=1 group by production.week_iso(curdate());

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n set sellers_sales = (select sum(supplier) from marketplace.temporary_news_report c where n.week=c.week) where country='Venezuela' and type='Marketplace';

drop table marketplace.temporary_news_report;

create table marketplace.temporary_news_report as select production.week_iso(curdate()) as week, count(distinct supplier) as supplier from development_ve.A_Master a where a.orderaftercan=1 group by production.week_iso(curdate());

create index week on marketplace.temporary_news_report(week);

update marketplace.news_report n set sellers_sales = (select sum(supplier) from marketplace.temporary_news_report c where n.week=c.week) where country='Venezuela' and type='All';

drop table marketplace.temporary_news_report;

-- Exchange Rate

update marketplace.news_report s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr where s.country = 'Mexico' and r.country='MEX';

update marketplace.news_report s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr where s.country = 'Colombia' and r.country='COL';

update marketplace.news_report s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr where s.country = 'Peru' and r.country='PER';

update marketplace.news_report s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr where s.country = 'Venezuela' and r.country='VEN';

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `new_sellers`()
begin

delete from marketplace.new_sellers;

insert into marketplace.new_sellers 
(select
'Mexico',
Cat_BP,
monthnum,
COUNT(DISTINCT supplier) from(SELECT
Cat_BP,
Supplier,
skuconfig,
monthnum,
IF( supplier != @supplier , @i := 1, @i := @i + 1 ) as ID, @supplier := supplier from (SELECT
Cat_BP,
Supplier,
concat(year(DATE(s.created_at)),if(month(DATE(s.created_at))<10,concat(0,month(DATE(s.created_at))),month(DATE(s.created_at)))) as Monthnum,
COUNT(DISTINCT sku_config) as skuconfig
from development_mx.A_Master_Catalog am
INNER JOIN bob_live_mx.supplier s on am.supplier=s.`name`
WHERE isMarketPlace=1 and s.created_at>"2014-04-01"
GROUP BY Supplier,Cat_BP,monthnum
ORDER BY Supplier, skuconfig DESC)a)a where id=1
GROUP BY cat_bp,monthnum)
union
(select
'Colombia',
Cat_BP,
monthnum,
COUNT(DISTINCT supplier) from(SELECT
Cat_BP,
Supplier,
skuconfig,
monthnum,
IF( supplier != @supplier , @i := 1, @i := @i + 1 ) as ID, @supplier := supplier from (SELECT
Cat_BP,
Supplier,
concat(year(DATE(s.created_at)),if(month(DATE(s.created_at))<10,concat(0,month(DATE(s.created_at))),month(DATE(s.created_at)))) as Monthnum,
COUNT(DISTINCT sku_config) as skuconfig
from development_co_project.A_Master_Catalog am
INNER JOIN bob_live_co.supplier s on am.supplier=s.`name`
WHERE isMarketPlace=1 and s.created_at>"2014-04-01"
GROUP BY Supplier,Cat_BP,monthnum
ORDER BY Supplier, skuconfig DESC)a)a where id=1
GROUP BY cat_bp,monthnum)
union
(select
'Peru',
Cat_BP,
monthnum,
COUNT(DISTINCT supplier) from(SELECT
Cat_BP,
Supplier,
skuconfig,
monthnum,
IF( supplier != @supplier , @i := 1, @i := @i + 1 ) as ID, @supplier := supplier from (SELECT
Cat_BP,
Supplier,
concat(year(DATE(s.created_at)),if(month(DATE(s.created_at))<10,concat(0,month(DATE(s.created_at))),month(DATE(s.created_at)))) as Monthnum,
COUNT(DISTINCT sku_config) as skuconfig
from development_pe.A_Master_Catalog am
INNER JOIN bob_live_pe.supplier s on am.supplier=s.`name`
WHERE isMarketPlace=1 and s.created_at>"2014-04-01"
GROUP BY Supplier,Cat_BP,monthnum
ORDER BY Supplier, skuconfig DESC)a)a where id=1
GROUP BY cat_bp,monthnum)
union
(select
'Venezuela',
Cat_BP,
monthnum,
COUNT(DISTINCT supplier) from(SELECT
Cat_BP,
Supplier,
skuconfig,
monthnum,
IF( supplier != @supplier , @i := 1, @i := @i + 1 ) as ID, @supplier := supplier from (SELECT
Cat_BP,
Supplier,
concat(year(DATE(s.created_at)),if(month(DATE(s.created_at))<10,concat(0,month(DATE(s.created_at))),month(DATE(s.created_at)))) as Monthnum,
COUNT(DISTINCT sku_config) as skuconfig
from development_ve.A_Master_Catalog am
INNER JOIN bob_live_ve.supplier s on am.supplier=s.`name`
WHERE isMarketPlace=1 and s.created_at>"2014-04-01"
GROUP BY Supplier,Cat_BP,monthnum
ORDER BY Supplier, skuconfig DESC)a)a where id=1
GROUP BY cat_bp,monthnum);

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `ops_dashboard`()
begin

delete from marketplace.ops_dashboard;

-- Mexico

insert into marketplace.ops_dashboard(country, type, is_marketplace, workdays_to_export) select 'Mexico', s.type, o.is_marketplace, avg(o.workdays_to_export) from operations_mx.out_order_tracking o, development_mx.A_Master_Catalog c, bob_live_mx.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate()))) group by s.type, o.is_marketplace; 

update marketplace.ops_dashboard d set workdays_to_ship = (select avg(o.workdays_to_ship) from operations_mx.out_order_tracking o, development_mx.A_Master_Catalog c, bob_live_mx.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Mexico'; 

update marketplace.ops_dashboard d set workdays_total_1st_attempt = (select avg(o.workdays_total_1st_attempt) from operations_mx.out_order_tracking o, development_mx.A_Master_Catalog c, bob_live_mx.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Mexico'; 

update marketplace.ops_dashboard d set workdays_total_delivery = (select avg(o.workdays_total_delivery) from operations_mx.out_order_tracking o, development_mx.A_Master_Catalog c, bob_live_mx.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Mexico'; 

update marketplace.ops_dashboard d set days_to_export = (select avg(o.days_to_export) from operations_mx.out_order_tracking o, development_mx.A_Master_Catalog c, bob_live_mx.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Mexico'; 

update marketplace.ops_dashboard d set days_to_ship = (select avg(o.days_to_ship) from operations_mx.out_order_tracking o, development_mx.A_Master_Catalog c, bob_live_mx.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Mexico'; 

update marketplace.ops_dashboard d set days_total_1st_attempt = (select avg(o.days_total_1st_attempt) from operations_mx.out_order_tracking o, development_mx.A_Master_Catalog c, bob_live_mx.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Mexico'; 

update marketplace.ops_dashboard d set days_total_delivery = (select avg(o.days_total_delivery) from operations_mx.out_order_tracking o, development_mx.A_Master_Catalog c, bob_live_mx.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Mexico'; 

update marketplace.ops_dashboard d set `#_stockout` = (select sum(o.is_stockout) from operations_mx.out_order_tracking o, development_mx.A_Master_Catalog c, bob_live_mx.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Mexico';

create table marketplace.temporary_ops_dashboard_items as select s.type, o.is_marketplace, count(*) as items from operations_mx.out_order_tracking o, development_mx.A_Master_Catalog c, bob_live_mx.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate()))) and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') group by s.type, o.is_marketplace;

create index type on marketplace.temporary_ops_dashboard_items(type);

create index is_marketplace on marketplace.temporary_ops_dashboard_items(is_marketplace);

create table marketplace.temporary_ops_dashboard_stockout as select s.type, o.is_marketplace, sum(is_stockout) as stockout from operations_mx.out_order_tracking o, development_mx.A_Master_Catalog c, bob_live_mx.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate()))) group by s.type, o.is_marketplace;

create index type on marketplace.temporary_ops_dashboard_stockout(type);

create index is_marketplace on marketplace.temporary_ops_dashboard_stockout(is_marketplace);

update marketplace.ops_dashboard d inner join marketplace.temporary_ops_dashboard_items i on d.is_marketplace=i.is_marketplace and d.type=i.type inner join marketplace.temporary_ops_dashboard_stockout s on d.is_marketplace=s.is_marketplace and d.type=s.type set `%_stockout` = s.stockout/i.items where country='Mexico';  

drop table marketplace.temporary_ops_dashboard_items;

drop table marketplace.temporary_ops_dashboard_stockout;

update marketplace.ops_dashboard d set on_time_to_1st_attempt = (select avg(o.on_time_to_1st_attempt) from operations_mx.out_order_tracking o, development_mx.A_Master_Catalog c, bob_live_mx.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Mexico'; 

update marketplace.ops_dashboard d set on_time_to_deliver = (select avg(o.on_time_to_deliver) from operations_mx.out_order_tracking o, development_mx.A_Master_Catalog c, bob_live_mx.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Mexico'; 

update marketplace.ops_dashboard d set on_time_to_1st_attempt = (select avg(o.on_time_to_1st_attempt) from operations_mx.out_order_tracking o, development_mx.A_Master_Catalog c, bob_live_mx.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Mexico'; 

update marketplace.ops_dashboard d set `#_order` = (select count(*) from operations_mx.out_order_tracking o, development_mx.A_Master_Catalog c, bob_live_mx.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Mexico'; 

update marketplace.ops_dashboard d set `%_order` = 1 where country='Mexico'; 

update marketplace.ops_dashboard d set `#_export` = (select sum(If(o.date_exported is not null,1,0)) from operations_mx.out_order_tracking o, development_mx.A_Master_Catalog c, bob_live_mx.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Mexico'; 

update marketplace.ops_dashboard d set `%_export` = `#_export`/`#_order` where country='Mexico'; 

update marketplace.ops_dashboard d set `#_ship` = (select sum(o.is_shipped) from operations_mx.out_order_tracking o, development_mx.A_Master_Catalog c, bob_live_mx.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Mexico'; 

update marketplace.ops_dashboard d set `%_ship` = `#_ship`/`#_order` where country='Mexico'; 

update marketplace.ops_dashboard d set `#_1st_attempt` = (select sum(o.is_first_attempt) from operations_mx.out_order_tracking o, development_mx.A_Master_Catalog c, bob_live_mx.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Mexico'; 

update marketplace.ops_dashboard d set `%_1st_attempt` = `#_1st_attempt`/`#_order` where country='Mexico'; 

update marketplace.ops_dashboard d set `#_deliver` = (select sum(o.is_delivered) from operations_mx.out_order_tracking o, development_mx.A_Master_Catalog c, bob_live_mx.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Mexico'; 

update marketplace.ops_dashboard d set `%_deliver` = `#_deliver`/`#_order` where country='Mexico'; 

-- Colombia

insert into marketplace.ops_dashboard(country, type, is_marketplace, workdays_to_export) select 'Colombia', s.type, o.is_marketplace, avg(o.workdays_to_export) from operations_co.out_order_tracking o, development_co_project.A_Master_Catalog c, bob_live_co.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate()))) group by s.type, o.is_marketplace; 

update marketplace.ops_dashboard d set workdays_to_ship = (select avg(o.workdays_to_ship) from operations_co.out_order_tracking o, development_co_project.A_Master_Catalog c, bob_live_co.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Colombia'; 

update marketplace.ops_dashboard d set workdays_total_1st_attempt = (select avg(o.workdays_total_1st_attempt) from operations_co.out_order_tracking o, development_co_project.A_Master_Catalog c, bob_live_co.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Colombia'; 

update marketplace.ops_dashboard d set workdays_total_delivery = (select avg(o.workdays_total_delivery) from operations_co.out_order_tracking o, development_co_project.A_Master_Catalog c, bob_live_co.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Colombia'; 

update marketplace.ops_dashboard d set days_to_export = (select avg(o.days_to_export) from operations_co.out_order_tracking o, development_co_project.A_Master_Catalog c, bob_live_co.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Colombia'; 

update marketplace.ops_dashboard d set days_to_ship = (select avg(o.days_to_ship) from operations_co.out_order_tracking o, development_co_project.A_Master_Catalog c, bob_live_co.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Colombia'; 

update marketplace.ops_dashboard d set days_total_1st_attempt = (select avg(o.days_total_1st_attempt) from operations_co.out_order_tracking o, development_co_project.A_Master_Catalog c, bob_live_co.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Colombia'; 

update marketplace.ops_dashboard d set days_total_delivery = (select avg(o.days_total_delivery) from operations_co.out_order_tracking o, development_co_project.A_Master_Catalog c, bob_live_co.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Colombia'; 

update marketplace.ops_dashboard d set `#_stockout` = (select sum(o.is_stockout) from operations_co.out_order_tracking o, development_co_project.A_Master_Catalog c, bob_live_co.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Colombia';

create table marketplace.temporary_ops_dashboard_items as select s.type, o.is_marketplace, count(*) as items from operations_co.out_order_tracking o, development_co_project.A_Master_Catalog c, bob_live_co.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate()))) and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') group by s.type, o.is_marketplace;

create index type on marketplace.temporary_ops_dashboard_items(type);

create index is_marketplace on marketplace.temporary_ops_dashboard_items(is_marketplace);

create table marketplace.temporary_ops_dashboard_stockout as select s.type, o.is_marketplace, sum(is_stockout) as stockout from operations_co.out_order_tracking o, development_co_project.A_Master_Catalog c, bob_live_co.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate()))) group by s.type, o.is_marketplace;

create index type on marketplace.temporary_ops_dashboard_stockout(type);

create index is_marketplace on marketplace.temporary_ops_dashboard_stockout(is_marketplace);

update marketplace.ops_dashboard d inner join marketplace.temporary_ops_dashboard_items i on d.is_marketplace=i.is_marketplace and d.type=i.type inner join marketplace.temporary_ops_dashboard_stockout s on d.is_marketplace=s.is_marketplace and d.type=s.type set `%_stockout` = s.stockout/i.items where country='Colombia';  

drop table marketplace.temporary_ops_dashboard_items;

drop table marketplace.temporary_ops_dashboard_stockout;

update marketplace.ops_dashboard d set on_time_to_1st_attempt = (select avg(o.on_time_to_1st_attempt) from operations_co.out_order_tracking o, development_co_project.A_Master_Catalog c, bob_live_co.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Colombia'; 

update marketplace.ops_dashboard d set on_time_to_deliver = (select avg(o.on_time_to_deliver) from operations_co.out_order_tracking o, development_co_project.A_Master_Catalog c, bob_live_co.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Colombia'; 

update marketplace.ops_dashboard d set on_time_to_1st_attempt = (select avg(o.on_time_to_1st_attempt) from operations_co.out_order_tracking o, development_co_project.A_Master_Catalog c, bob_live_co.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Colombia'; 

update marketplace.ops_dashboard d set `#_order` = (select count(*) from operations_co.out_order_tracking o, development_co_project.A_Master_Catalog c, bob_live_co.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Colombia'; 

update marketplace.ops_dashboard d set `%_order` = 1 where country='Colombia'; 

update marketplace.ops_dashboard d set `#_export` = (select sum(If(o.date_exported is not null,1,0)) from operations_co.out_order_tracking o, development_co_project.A_Master_Catalog c, bob_live_co.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Colombia'; 

update marketplace.ops_dashboard d set `%_export` = `#_export`/`#_order` where country='Colombia'; 

update marketplace.ops_dashboard d set `#_ship` = (select sum(o.is_shipped) from operations_co.out_order_tracking o, development_co_project.A_Master_Catalog c, bob_live_co.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Colombia'; 

update marketplace.ops_dashboard d set `%_ship` = `#_ship`/`#_order` where country='Colombia'; 

update marketplace.ops_dashboard d set `#_1st_attempt` = (select sum(o.is_first_attempt) from operations_co.out_order_tracking o, development_co_project.A_Master_Catalog c, bob_live_co.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Colombia'; 

update marketplace.ops_dashboard d set `%_1st_attempt` = `#_1st_attempt`/`#_order` where country='Colombia'; 

update marketplace.ops_dashboard d set `#_deliver` = (select sum(o.is_delivered) from operations_co.out_order_tracking o, development_co_project.A_Master_Catalog c, bob_live_co.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Colombia'; 

update marketplace.ops_dashboard d set `%_deliver` = `#_deliver`/`#_order` where country='Colombia'; 

-- Peru

insert into marketplace.ops_dashboard(country, type, is_marketplace, workdays_to_export) select 'Peru', s.type, o.is_marketplace, avg(o.workdays_to_export) from operations_pe.out_order_tracking o, development_pe.A_Master_Catalog c, bob_live_pe.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate()))) group by s.type, o.is_marketplace; 

update marketplace.ops_dashboard d set workdays_to_ship = (select avg(o.workdays_to_ship) from operations_pe.out_order_tracking o, development_pe.A_Master_Catalog c, bob_live_pe.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Peru'; 

update marketplace.ops_dashboard d set workdays_total_1st_attempt = (select avg(o.workdays_total_1st_attempt) from operations_pe.out_order_tracking o, development_pe.A_Master_Catalog c, bob_live_pe.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Peru'; 

update marketplace.ops_dashboard d set workdays_total_delivery = (select avg(o.workdays_total_delivery) from operations_pe.out_order_tracking o, development_pe.A_Master_Catalog c, bob_live_pe.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Peru'; 

update marketplace.ops_dashboard d set days_to_export = (select avg(o.days_to_export) from operations_pe.out_order_tracking o, development_pe.A_Master_Catalog c, bob_live_pe.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Peru'; 

update marketplace.ops_dashboard d set days_to_ship = (select avg(o.days_to_ship) from operations_pe.out_order_tracking o, development_pe.A_Master_Catalog c, bob_live_pe.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Peru'; 

update marketplace.ops_dashboard d set days_total_1st_attempt = (select avg(o.days_total_1st_attempt) from operations_pe.out_order_tracking o, development_pe.A_Master_Catalog c, bob_live_pe.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Peru'; 

update marketplace.ops_dashboard d set days_total_delivery = (select avg(o.days_total_delivery) from operations_pe.out_order_tracking o, development_pe.A_Master_Catalog c, bob_live_pe.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Peru'; 

update marketplace.ops_dashboard d set `#_stockout` = (select sum(o.is_stockout) from operations_pe.out_order_tracking o, development_pe.A_Master_Catalog c, bob_live_pe.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Peru';

create table marketplace.temporary_ops_dashboard_items as select s.type, o.is_marketplace, count(*) as items from operations_pe.out_order_tracking o, development_pe.A_Master_Catalog c, bob_live_pe.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate()))) and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') group by s.type, o.is_marketplace;

create index type on marketplace.temporary_ops_dashboard_items(type);

create index is_marketplace on marketplace.temporary_ops_dashboard_items(is_marketplace);

create table marketplace.temporary_ops_dashboard_stockout as select s.type, o.is_marketplace, sum(is_stockout) as stockout from operations_pe.out_order_tracking o, development_pe.A_Master_Catalog c, bob_live_pe.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate()))) group by s.type, o.is_marketplace;

create index type on marketplace.temporary_ops_dashboard_stockout(type);

create index is_marketplace on marketplace.temporary_ops_dashboard_stockout(is_marketplace);

update marketplace.ops_dashboard d inner join marketplace.temporary_ops_dashboard_items i on d.is_marketplace=i.is_marketplace and d.type=i.type inner join marketplace.temporary_ops_dashboard_stockout s on d.is_marketplace=s.is_marketplace and d.type=s.type set `%_stockout` = s.stockout/i.items where country='Peru';  

drop table marketplace.temporary_ops_dashboard_items;

drop table marketplace.temporary_ops_dashboard_stockout;

update marketplace.ops_dashboard d set on_time_to_1st_attempt = (select avg(o.on_time_to_1st_attempt) from operations_pe.out_order_tracking o, development_pe.A_Master_Catalog c, bob_live_pe.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Peru'; 

update marketplace.ops_dashboard d set on_time_to_deliver = (select avg(o.on_time_to_deliver) from operations_pe.out_order_tracking o, development_pe.A_Master_Catalog c, bob_live_pe.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Peru'; 

update marketplace.ops_dashboard d set on_time_to_1st_attempt = (select avg(o.on_time_to_1st_attempt) from operations_pe.out_order_tracking o, development_pe.A_Master_Catalog c, bob_live_pe.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Peru'; 

update marketplace.ops_dashboard d set `#_order` = (select count(*) from operations_pe.out_order_tracking o, development_pe.A_Master_Catalog c, bob_live_pe.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Peru'; 

update marketplace.ops_dashboard d set `%_order` = 1 where country='Peru'; 

update marketplace.ops_dashboard d set `#_export` = (select sum(If(o.date_exported is not null,1,0)) from operations_pe.out_order_tracking o, development_pe.A_Master_Catalog c, bob_live_pe.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Peru'; 

update marketplace.ops_dashboard d set `%_export` = `#_export`/`#_order` where country='Peru'; 

update marketplace.ops_dashboard d set `#_ship` = (select sum(o.is_shipped) from operations_pe.out_order_tracking o, development_pe.A_Master_Catalog c, bob_live_pe.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Peru'; 

update marketplace.ops_dashboard d set `%_ship` = `#_ship`/`#_order` where country='Peru'; 

update marketplace.ops_dashboard d set `#_1st_attempt` = (select sum(o.is_first_attempt) from operations_pe.out_order_tracking o, development_pe.A_Master_Catalog c, bob_live_pe.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Peru'; 

update marketplace.ops_dashboard d set `%_1st_attempt` = `#_1st_attempt`/`#_order` where country='Peru'; 

update marketplace.ops_dashboard d set `#_deliver` = (select sum(o.is_delivered) from operations_pe.out_order_tracking o, development_pe.A_Master_Catalog c, bob_live_pe.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Peru'; 

update marketplace.ops_dashboard d set `%_deliver` = `#_deliver`/`#_order` where country='Peru'; 

-- Venezuela

insert into marketplace.ops_dashboard(country, type, is_marketplace, workdays_to_export) select 'Venezuela', s.type, o.is_marketplace, avg(o.workdays_to_export) from operations_ve.out_order_tracking o, development_ve.A_Master_Catalog c, bob_live_ve.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate()))) group by s.type, o.is_marketplace; 

update marketplace.ops_dashboard d set workdays_to_ship = (select avg(o.workdays_to_ship) from operations_ve.out_order_tracking o, development_ve.A_Master_Catalog c, bob_live_ve.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Venezuela'; 

update marketplace.ops_dashboard d set workdays_total_1st_attempt = (select avg(o.workdays_total_1st_attempt) from operations_ve.out_order_tracking o, development_ve.A_Master_Catalog c, bob_live_ve.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Venezuela'; 

update marketplace.ops_dashboard d set workdays_total_delivery = (select avg(o.workdays_total_delivery) from operations_ve.out_order_tracking o, development_ve.A_Master_Catalog c, bob_live_ve.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Venezuela'; 

update marketplace.ops_dashboard d set days_to_export = (select avg(o.days_to_export) from operations_ve.out_order_tracking o, development_ve.A_Master_Catalog c, bob_live_ve.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Venezuela'; 

update marketplace.ops_dashboard d set days_to_ship = (select avg(o.days_to_ship) from operations_ve.out_order_tracking o, development_ve.A_Master_Catalog c, bob_live_ve.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Venezuela'; 

update marketplace.ops_dashboard d set days_total_1st_attempt = (select avg(o.days_total_1st_attempt) from operations_ve.out_order_tracking o, development_ve.A_Master_Catalog c, bob_live_ve.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Venezuela'; 

update marketplace.ops_dashboard d set days_total_delivery = (select avg(o.days_total_delivery) from operations_ve.out_order_tracking o, development_ve.A_Master_Catalog c, bob_live_ve.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Venezuela'; 

update marketplace.ops_dashboard d set `#_stockout` = (select sum(o.is_stockout) from operations_ve.out_order_tracking o, development_ve.A_Master_Catalog c, bob_live_ve.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Venezuela';

create table marketplace.temporary_ops_dashboard_items as select s.type, o.is_marketplace, count(*) as items from operations_ve.out_order_tracking o, development_ve.A_Master_Catalog c, bob_live_ve.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate()))) and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') group by s.type, o.is_marketplace;

create index type on marketplace.temporary_ops_dashboard_items(type);

create index is_marketplace on marketplace.temporary_ops_dashboard_items(is_marketplace);

create table marketplace.temporary_ops_dashboard_stockout as select s.type, o.is_marketplace, sum(is_stockout) as stockout from operations_ve.out_order_tracking o, development_ve.A_Master_Catalog c, bob_live_ve.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate()))) group by s.type, o.is_marketplace;

create index type on marketplace.temporary_ops_dashboard_stockout(type);

create index is_marketplace on marketplace.temporary_ops_dashboard_stockout(is_marketplace);

update marketplace.ops_dashboard d inner join marketplace.temporary_ops_dashboard_items i on d.is_marketplace=i.is_marketplace and d.type=i.type inner join marketplace.temporary_ops_dashboard_stockout s on d.is_marketplace=s.is_marketplace and d.type=s.type set `%_stockout` = s.stockout/i.items where country='Venezuela';  

drop table marketplace.temporary_ops_dashboard_items;

drop table marketplace.temporary_ops_dashboard_stockout;

update marketplace.ops_dashboard d set on_time_to_1st_attempt = (select avg(o.on_time_to_1st_attempt) from operations_ve.out_order_tracking o, development_ve.A_Master_Catalog c, bob_live_ve.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Venezuela'; 

update marketplace.ops_dashboard d set on_time_to_deliver = (select avg(o.on_time_to_deliver) from operations_ve.out_order_tracking o, development_ve.A_Master_Catalog c, bob_live_ve.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Venezuela'; 

update marketplace.ops_dashboard d set on_time_to_1st_attempt = (select avg(o.on_time_to_1st_attempt) from operations_ve.out_order_tracking o, development_ve.A_Master_Catalog c, bob_live_ve.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Venezuela'; 

update marketplace.ops_dashboard d set `#_order` = (select count(*) from operations_ve.out_order_tracking o, development_ve.A_Master_Catalog c, bob_live_ve.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Venezuela'; 

update marketplace.ops_dashboard d set `%_order` = 1 where country='Venezuela'; 

update marketplace.ops_dashboard d set `#_export` = (select sum(If(o.date_exported is not null,1,0)) from operations_ve.out_order_tracking o, development_ve.A_Master_Catalog c, bob_live_ve.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Venezuela'; 

update marketplace.ops_dashboard d set `%_export` = `#_export`/`#_order` where country='Venezuela'; 

update marketplace.ops_dashboard d set `#_ship` = (select sum(o.is_shipped) from operations_ve.out_order_tracking o, development_ve.A_Master_Catalog c, bob_live_ve.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Venezuela'; 

update marketplace.ops_dashboard d set `%_ship` = `#_ship`/`#_order` where country='Venezuela'; 

update marketplace.ops_dashboard d set `#_1st_attempt` = (select sum(o.is_first_attempt) from operations_ve.out_order_tracking o, development_ve.A_Master_Catalog c, bob_live_ve.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Venezuela'; 

update marketplace.ops_dashboard d set `%_1st_attempt` = `#_1st_attempt`/`#_order` where country='Venezuela'; 

update marketplace.ops_dashboard d set `#_deliver` = (select sum(o.is_delivered) from operations_ve.out_order_tracking o, development_ve.A_Master_Catalog c, bob_live_ve.supplier s where o.sku_simple=c.sku_simple and s.id_supplier=c.id_supplier and d.type=s.type and d.is_marketplace=o.is_marketplace and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and concat(year(o.date_ordered),if(month(o.date_ordered)<10,concat(0,month(o.date_ordered)),month(o.date_ordered)))=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))) where country='Venezuela'; 

update marketplace.ops_dashboard d set `%_deliver` = `#_deliver`/`#_order` where country='Venezuela'; 

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `product_views`()
begin

/*
create table marketplace.product_views(
country varchar(255),
yrmonth int,
week varchar(255),
date date,
product_name varchar(255),
sku_config varchar(255),
cat1 varchar(255),
cat2 varchar(255),
cat3 varchar(255),
channel varchar(255),
campaign varchar(255),
price float,
price_range_cat varchar(255),
price_range_total varchar(255),
add_to_cart int,
product_views float,
clicks float,
unit_sold float,
net_revenue  float,
`PC1.5` float,
is_marketplace int 
);

create index date on marketplace.product_views(date);
create index sku_config on marketplace.product_views(sku_config);

*/

set @days:=300;

##delete from marketplace.product_views where datediff(curdate(), date)<@days;

-- Mexico

set @last_date:=(select max(date) from marketplace.product_views where country='Mexico');

##set @last_date:='2013-12-31';

/*
insert into marketplace.product_views(country, yrmonth, week, date, sku_config, channel, campaign, add_to_cart, product_views, clicks, unit_sold, net_revenue, `PC1.5`) select 'Mexico', concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), production.week_iso(date), date, product, channel, campaign, add_to_cart, product_views, clicks, unit_sold, net_revenue, `PC1.5` from attribution_testing.marketing_campaign_tool where date>@last_date;*/

insert into marketplace.product_views(country, yrmonth, week, date, sku_config, product_views) select 'Mexico', concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), production.week_iso(date), date, product, sum(quantity) from attribution_testing.getfullbasket_mx where date>@last_date and stat=2 group by date, product;

create table marketplace.temporary_product_views select date, product as sku_config, sum(quantity) as add_to_cart from attribution_testing.getfullbasket_mx where date>@last_date and stat=0 group by date, product;

create index sku_config on marketplace.temporary_product_views(sku_config);
create index date on marketplace.temporary_product_views(date);

update marketplace.product_views p set add_to_cart = (select sum(add_to_cart) from marketplace.temporary_product_views a where a.sku_config=p.sku_config and a.date=p.date) where country='Mexico' and p.date>@last_date;

drop table marketplace.temporary_product_views;

create table marketplace.temporary_product_views select date, skuconfig as sku_config, count(*) as unit_sold, sum(rev) as net_revenue, sum(pconepfive) as `PC1.5` from development_mx.A_Master where datediff(curdate(), date)<@days and orderaftercan=1 group by skuconfig, date;

create index sku_config on marketplace.temporary_product_views(sku_config);
create index date on marketplace.temporary_product_views(date);

update marketplace.product_views p set unit_sold = (select sum(unit_sold) from marketplace.temporary_product_views a where a.sku_config=p.sku_config and a.date=p.date) where country='Mexico' and datediff(curdate(), p.date)<@days;

update marketplace.product_views p set net_revenue = (select sum(net_revenue) from marketplace.temporary_product_views a where a.sku_config=p.sku_config and a.date=p.date) where country='Mexico' and datediff(curdate(), p.date)<@days;

update marketplace.product_views p set `PC1.5` = (select sum(`PC1.5`) from marketplace.temporary_product_views a where a.sku_config=p.sku_config and a.date=p.date) where country='Mexico' and datediff(curdate(), p.date)<@days;

drop table marketplace.temporary_product_views;

create table marketplace.temporary_product_views select date, skuconfig as sku_config, count(*) as gross_unit_sold from development_mx.A_Master where datediff(curdate(), date)<@days and orderbeforecan=1 group by skuconfig, date;

create index sku_config on marketplace.temporary_product_views(sku_config);
create index date on marketplace.temporary_product_views(date);

update marketplace.product_views p set gross_unit_sold = (select sum(gross_unit_sold) from marketplace.temporary_product_views a where a.sku_config=p.sku_config and a.date=p.date) where country='Mexico' and datediff(curdate(), p.date)<@days;

drop table marketplace.temporary_product_views;

update marketplace.product_views p inner join development_mx.A_Master_Catalog c on p.sku_config=c.sku_config set p.cat1=c.Cat1, p.cat2=c.Cat2, p.cat3=c.Cat3, p.price=c.price, p.cat_bp=c.cat_bp, p.product_name=c.sku_name ,p.is_marketplace=c.ismarketplace where p.country='Mexico'; 

create table marketplace.temporary_product_views select date, skuconfig from development_mx.A_Master where isMPlace=1 and datediff(curdate(), date)<@days;

create index date on marketplace.temporary_product_views(date);
create index skuconfig on marketplace.temporary_product_views(skuconfig);

update marketplace.product_views p inner join marketplace.temporary_product_views a on p.sku_config=a.skuconfig and p.date=a.date set p.is_marketplace=1 where p.country='Mexico' and datediff(curdate(), p.date)<@days;

drop table marketplace.temporary_product_views;

create table marketplace.temporary_product_views(
sku_config varchar(255),
cat1 varchar(255),
cat2 varchar(255),
price float,
price_range varchar(255)
);

create index sku_config on marketplace.temporary_product_views(sku_config);
create index cat1 on marketplace.temporary_product_views(cat1);
create index cat2 on marketplace.temporary_product_views(cat2);

insert into marketplace.temporary_product_views(sku_config, cat1, cat2, price) select sku_config, cat1, cat2, price from marketplace.product_views where country='Mexico' group by sku_config;

create table marketplace.temporary_cat_std_dev(
cat1 varchar(255),
cat2 varchar(255),
std_dev_minus float,
std_dev_plus float
);

create index cat1 on marketplace.temporary_cat_std_dev(cat1);
create index cat2 on marketplace.temporary_cat_std_dev(cat2);


insert into marketplace.temporary_cat_std_dev(cat1, cat2) select distinct cat1, cat2 from marketplace.temporary_product_views;

/*
update marketplace.temporary_cat_std_dev d set std_dev_minus = (select avg(price)-std(price) from marketplace.temporary_product_views p where p.cat1=d.cat1);

update marketplace.temporary_cat_std_dev d set std_dev_plus = (select avg(price)+std(price) from marketplace.temporary_product_views p where p.cat1=d.cat1);
*/

create table marketplace.temporary_cat as
select cat1, cat2, avg(price) as price from marketplace.temporary_product_views group by cat1, cat2;

create index cat1 on marketplace.temporary_cat(cat1);
create index cat2 on marketplace.temporary_cat(cat2);

update marketplace.temporary_cat_std_dev d set std_dev_minus = (select price-(price*0.15) from marketplace.temporary_cat p where p.cat1=d.cat1 and p.cat2=d.cat2);

update marketplace.temporary_cat_std_dev d set std_dev_plus = (select price+(price*0.15) from marketplace.temporary_cat p where p.cat1=d.cat1 and p.cat2=d.cat2);

update marketplace.temporary_product_views p inner join marketplace.temporary_cat_std_dev d on p.cat1=d.cat1 and p.cat2=d.cat2 set p.price_range='Cheap' where p.price between 0 and d.std_dev_minus;

update marketplace.temporary_product_views p inner join marketplace.temporary_cat_std_dev d on p.cat1=d.cat1 and p.cat2=d.cat2 set p.price_range='Medium' where p.price between d.std_dev_minus and d.std_dev_plus;

update marketplace.temporary_product_views p inner join marketplace.temporary_cat_std_dev d on p.cat1=d.cat1 and p.cat2=d.cat2 set p.price_range='Expensive' where p.price>=d.std_dev_plus;

update marketplace.product_views p inner join marketplace.temporary_product_views t on p.sku_config=t.sku_config set p.price_range_cat=t.price_range where country='Mexico';

update marketplace.product_views p set p.price_range_total='Cheap' where p.price between 0 and 700 and country='Mexico';

update marketplace.product_views p set p.price_range_total='Medium' where p.price between 701 and 4000 and country='Mexico';

update marketplace.product_views p set p.price_range_total='Expensive' where p.price>4000 and country='Mexico';

drop table marketplace.temporary_cat;

drop table marketplace.temporary_product_views;

drop table marketplace.temporary_cat_std_dev;

update marketplace.sc_missing_cat set sku_config=substr(sku_config, 1,15);

update marketplace.product_views p inner join marketplace.sc_missing_cat s on p.sku_config=s.sku_config set p.cat1=s.cat1, p.cat2=s.cat2, p.cat_bp=s.cat_bp where p.cat1='DefaultSC' and p.country='Mexico';

update marketplace.product_views s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr, `PC1.5`=`PC1.5`/xr where s.country = 'Mexico' and r.country='MEX' and datediff(curdate(), s.date)<@days;

create table marketplace.temporary_product_views as select date_ordered as date, sku_config, sum(is_stockout) as oos from operations_mx.out_order_tracking where date_ordered>='2014-01-01' and datediff(curdate(), date_ordered)<@days group by date_ordered, sku_config;

create index date on marketplace.temporary_product_views(date); 
create index sku_config on marketplace.temporary_product_views(sku_config);

update marketplace.product_views p inner join marketplace.temporary_product_views t on p.date=t.date and p.sku_config=t.sku_config set p.oos=t.oos where p.country='Mexico' and datediff(curdate(), p.date)<@days;

drop table marketplace.temporary_product_views;

create table marketplace.temporary_product_views as select date, skuconfig as sku_config, sum(cancelled) as canceled, sum(returns) as returned from development_mx.A_Master where date>='2014-01-01' and datediff(curdate(), date)<@days group by date, sku_config; 

create index date on marketplace.temporary_product_views(date); 
create index sku_config on marketplace.temporary_product_views(sku_config);

update marketplace.product_views p inner join marketplace.temporary_product_views t on p.date=t.date and p.sku_config=t.sku_config set p.canceled=t.canceled, p.returned=t.returned where p.country='Mexico' and datediff(curdate(), p.date)<@days;

drop table marketplace.temporary_product_views;

-- Colombia

set @last_date:=(select max(date) from marketplace.product_views where country='Colombia');

insert into marketplace.product_views(country, yrmonth, week, date, sku_config, product_views) select 'Colombia', concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), production.week_iso(date), date, product, sum(quantity) from attribution_testing.getfullbasket_co where date>@last_date and stat=2 group by date, product;

create table marketplace.temporary_product_views select date, product as sku_config, sum(quantity) as add_to_cart from attribution_testing.getfullbasket_co where date>@last_date and stat=0 group by date, product;

create index sku_config on marketplace.temporary_product_views(sku_config);
create index date on marketplace.temporary_product_views(date);

update marketplace.product_views p set add_to_cart = (select sum(add_to_cart) from marketplace.temporary_product_views a where a.sku_config=p.sku_config and a.date=p.date) where country='Colombia' and p.date>@last_date;

drop table marketplace.temporary_product_views;

create table marketplace.temporary_product_views select date, skuconfig as sku_config, count(*) as unit_sold, sum(rev) as net_revenue, sum(pconepfive) as `PC1.5` from development_co_project.A_Master where datediff(curdate(), date)<@days and orderaftercan=1 group by skuconfig, date;

create index sku_config on marketplace.temporary_product_views(sku_config);
create index date on marketplace.temporary_product_views(date);

update marketplace.product_views p set unit_sold = (select sum(unit_sold) from marketplace.temporary_product_views a where a.sku_config=p.sku_config and a.date=p.date) where country='Colombia' and datediff(curdate(), p.date)<@days;

update marketplace.product_views p set net_revenue = (select sum(net_revenue) from marketplace.temporary_product_views a where a.sku_config=p.sku_config and a.date=p.date) where country='Colombia' and datediff(curdate(), p.date)<@days;

update marketplace.product_views p set `PC1.5` = (select sum(`PC1.5`) from marketplace.temporary_product_views a where a.sku_config=p.sku_config and a.date=p.date) where country='Colombia' and datediff(curdate(), p.date)<@days;

drop table marketplace.temporary_product_views;

create table marketplace.temporary_product_views select date, skuconfig as sku_config, count(*) as gross_unit_sold from development_co_project.A_Master where datediff(curdate(), date)<@days and orderbeforecan=1 group by skuconfig, date;

create index sku_config on marketplace.temporary_product_views(sku_config);
create index date on marketplace.temporary_product_views(date);

update marketplace.product_views p set gross_unit_sold = (select sum(gross_unit_sold) from marketplace.temporary_product_views a where a.sku_config=p.sku_config and a.date=p.date) where country='Colombia' and datediff(curdate(), p.date)<@days;

drop table marketplace.temporary_product_views;

update marketplace.product_views p inner join development_co_project.A_Master_Catalog c on p.sku_config=c.sku_config set p.cat1=c.Cat1, p.cat2=c.Cat2, p.cat3=c.Cat3, p.price=c.price, p.cat_bp=c.cat_bp, p.product_name=c.sku_name ,p.is_marketplace=c.ismarketplace where p.country='Colombia'; 

create table marketplace.temporary_product_views select date, skuconfig from development_co_project.A_Master where isMPlace=1 and datediff(curdate(), date)<@days;

create index date on marketplace.temporary_product_views(date);
create index skuconfig on marketplace.temporary_product_views(skuconfig);

update marketplace.product_views p inner join marketplace.temporary_product_views a on p.sku_config=a.skuconfig and p.date=a.date set p.is_marketplace=1 where p.country='Colombia' and datediff(curdate(), p.date)<@days;

drop table marketplace.temporary_product_views;

create table marketplace.temporary_product_views(
sku_config varchar(255),
cat1 varchar(255),
cat2 varchar(255),
price float,
price_range varchar(255)
);

create index sku_config on marketplace.temporary_product_views(sku_config);
create index cat1 on marketplace.temporary_product_views(cat1);
create index cat2 on marketplace.temporary_product_views(cat2);

insert into marketplace.temporary_product_views(sku_config, cat1, cat2, price) select sku_config, cat1, cat2, price from marketplace.product_views where country='Colombia' group by sku_config;

create table marketplace.temporary_cat_std_dev(
cat1 varchar(255),
cat2 varchar(255),
std_dev_minus float,
std_dev_plus float
);

create index cat1 on marketplace.temporary_cat_std_dev(cat1);
create index cat2 on marketplace.temporary_cat_std_dev(cat2);


insert into marketplace.temporary_cat_std_dev(cat1, cat2) select distinct cat1, cat2 from marketplace.temporary_product_views;

/*
update marketplace.temporary_cat_std_dev d set std_dev_minus = (select avg(price)-std(price) from marketplace.temporary_product_views p where p.cat1=d.cat1);

update marketplace.temporary_cat_std_dev d set std_dev_plus = (select avg(price)+std(price) from marketplace.temporary_product_views p where p.cat1=d.cat1);
*/

create table marketplace.temporary_cat as
select cat1, cat2, avg(price) as price from marketplace.temporary_product_views group by cat1, cat2;

create index cat1 on marketplace.temporary_cat(cat1);
create index cat2 on marketplace.temporary_cat(cat2);

update marketplace.temporary_cat_std_dev d set std_dev_minus = (select price-(price*0.15) from marketplace.temporary_cat p where p.cat1=d.cat1 and p.cat2=d.cat2);

update marketplace.temporary_cat_std_dev d set std_dev_plus = (select price+(price*0.15) from marketplace.temporary_cat p where p.cat1=d.cat1 and p.cat2=d.cat2);

update marketplace.temporary_product_views p inner join marketplace.temporary_cat_std_dev d on p.cat1=d.cat1 and p.cat2=d.cat2 set p.price_range='Cheap' where p.price between 0 and d.std_dev_minus;

update marketplace.temporary_product_views p inner join marketplace.temporary_cat_std_dev d on p.cat1=d.cat1 and p.cat2=d.cat2 set p.price_range='Medium' where p.price between d.std_dev_minus and d.std_dev_plus;

update marketplace.temporary_product_views p inner join marketplace.temporary_cat_std_dev d on p.cat1=d.cat1 and p.cat2=d.cat2 set p.price_range='Expensive' where p.price>=d.std_dev_plus;

update marketplace.product_views p inner join marketplace.temporary_product_views t on p.sku_config=t.sku_config set p.price_range_cat=t.price_range where country='Colombia';

/*
update marketplace.product_views p set p.price_range_total='Cheap' where p.price between 0 and 700 and country='Colombia';

update marketplace.product_views p set p.price_range_total='Medium' where p.price between 701 and 4000 and country='Colombia';

update marketplace.product_views p set p.price_range_total='Expensive' where p.price>4000 and country='Colombia';*/

drop table marketplace.temporary_cat;

drop table marketplace.temporary_product_views;

drop table marketplace.temporary_cat_std_dev;

update marketplace.product_views s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr, `PC1.5`=`PC1.5`/xr where s.country = 'Colombia' and r.country='COL' and datediff(curdate(), s.date)<@days;

create table marketplace.temporary_product_views as select date_ordered as date, sku_config, sum(is_stockout) as oos from operations_co.out_order_tracking where date_ordered>='2014-01-01' and datediff(curdate(), date_ordered)<@days group by date_ordered, sku_config;

create index date on marketplace.temporary_product_views(date); 
create index sku_config on marketplace.temporary_product_views(sku_config);

update marketplace.product_views p inner join marketplace.temporary_product_views t on p.date=t.date and p.sku_config=t.sku_config set p.oos=t.oos where p.country='Colombia' and datediff(curdate(), p.date)<@days;

drop table marketplace.temporary_product_views;

create table marketplace.temporary_product_views as select date, skuconfig as sku_config, sum(cancelled) as canceled, sum(returns) as returned from development_co_project.A_Master where date>='2014-01-01' and datediff(curdate(), date)<@days group by date, sku_config; 

create index date on marketplace.temporary_product_views(date); 
create index sku_config on marketplace.temporary_product_views(sku_config);

update marketplace.product_views p inner join marketplace.temporary_product_views t on p.date=t.date and p.sku_config=t.sku_config set p.canceled=t.canceled, p.returned=t.returned where p.country='Colombia' and datediff(curdate(), p.date)<@days;

drop table marketplace.temporary_product_views;

-- Peru

set @last_date:=(select max(date) from marketplace.product_views where country='Peru');

insert into marketplace.product_views(country, yrmonth, week, date, sku_config, product_views) select 'Peru', concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), production.week_iso(date), date, product, sum(quantity) from attribution_testing.getfullbasket_pe where date>@last_date and stat=2 group by date, product;

create table marketplace.temporary_product_views select date, product as sku_config, sum(quantity) as add_to_cart from attribution_testing.getfullbasket_pe where date>@last_date and stat=0 group by date, product;

create index sku_config on marketplace.temporary_product_views(sku_config);
create index date on marketplace.temporary_product_views(date);

update marketplace.product_views p set add_to_cart = (select sum(add_to_cart) from marketplace.temporary_product_views a where a.sku_config=p.sku_config and a.date=p.date) where country='Peru' and p.date>@last_date;

drop table marketplace.temporary_product_views;

create table marketplace.temporary_product_views select date, skuconfig as sku_config, count(*) as unit_sold, sum(rev) as net_revenue, sum(pconepfive) as `PC1.5` from development_pe.A_Master where datediff(curdate(), date)<@days and orderaftercan=1 group by skuconfig, date;

create index sku_config on marketplace.temporary_product_views(sku_config);
create index date on marketplace.temporary_product_views(date);

update marketplace.product_views p set unit_sold = (select sum(unit_sold) from marketplace.temporary_product_views a where a.sku_config=p.sku_config and a.date=p.date) where country='Peru' and datediff(curdate(), p.date)<@days;

update marketplace.product_views p set net_revenue = (select sum(net_revenue) from marketplace.temporary_product_views a where a.sku_config=p.sku_config and a.date=p.date) where country='Peru' and datediff(curdate(), p.date)<@days;

update marketplace.product_views p set `PC1.5` = (select sum(`PC1.5`) from marketplace.temporary_product_views a where a.sku_config=p.sku_config and a.date=p.date) where country='Peru' and datediff(curdate(), p.date)<@days;

drop table marketplace.temporary_product_views;

create table marketplace.temporary_product_views select date, skuconfig as sku_config, count(*) as gross_unit_sold from development_pe.A_Master where datediff(curdate(), date)<@days and orderbeforecan=1 group by skuconfig, date;

create index sku_config on marketplace.temporary_product_views(sku_config);
create index date on marketplace.temporary_product_views(date);

update marketplace.product_views p set gross_unit_sold = (select sum(gross_unit_sold) from marketplace.temporary_product_views a where a.sku_config=p.sku_config and a.date=p.date) where country='Peru' and datediff(curdate(), p.date)<@days;

drop table marketplace.temporary_product_views;

update marketplace.product_views p inner join development_pe.A_Master_Catalog c on p.sku_config=c.sku_config set p.cat1=c.Cat1, p.cat2=c.Cat2, p.cat3=c.Cat3, p.price=c.price, p.cat_bp=c.cat_bp, p.product_name=c.sku_name ,p.is_marketplace=c.ismarketplace where p.country='Peru'; 

create table marketplace.temporary_product_views select date, skuconfig from development_pe.A_Master where isMPlace=1 and datediff(curdate(), date)<@days;

create index date on marketplace.temporary_product_views(date);
create index skuconfig on marketplace.temporary_product_views(skuconfig);

update marketplace.product_views p inner join marketplace.temporary_product_views a on p.sku_config=a.skuconfig and p.date=a.date set p.is_marketplace=1 where p.country='Peru' and datediff(curdate(), p.date)<@days;

drop table marketplace.temporary_product_views;

create table marketplace.temporary_product_views(
sku_config varchar(255),
cat1 varchar(255),
cat2 varchar(255),
price float,
price_range varchar(255)
);

create index sku_config on marketplace.temporary_product_views(sku_config);
create index cat1 on marketplace.temporary_product_views(cat1);
create index cat2 on marketplace.temporary_product_views(cat2);

insert into marketplace.temporary_product_views(sku_config, cat1, cat2, price) select sku_config, cat1, cat2, price from marketplace.product_views where country='Peru' group by sku_config;

create table marketplace.temporary_cat_std_dev(
cat1 varchar(255),
cat2 varchar(255),
std_dev_minus float,
std_dev_plus float
);

create index cat1 on marketplace.temporary_cat_std_dev(cat1);
create index cat2 on marketplace.temporary_cat_std_dev(cat2);


insert into marketplace.temporary_cat_std_dev(cat1, cat2) select distinct cat1, cat2 from marketplace.temporary_product_views;

/*
update marketplace.temporary_cat_std_dev d set std_dev_minus = (select avg(price)-std(price) from marketplace.temporary_product_views p where p.cat1=d.cat1);

update marketplace.temporary_cat_std_dev d set std_dev_plus = (select avg(price)+std(price) from marketplace.temporary_product_views p where p.cat1=d.cat1);
*/

create table marketplace.temporary_cat as
select cat1, cat2, avg(price) as price from marketplace.temporary_product_views group by cat1, cat2;

create index cat1 on marketplace.temporary_cat(cat1);
create index cat2 on marketplace.temporary_cat(cat2);

update marketplace.temporary_cat_std_dev d set std_dev_minus = (select price-(price*0.15) from marketplace.temporary_cat p where p.cat1=d.cat1 and p.cat2=d.cat2);

update marketplace.temporary_cat_std_dev d set std_dev_plus = (select price+(price*0.15) from marketplace.temporary_cat p where p.cat1=d.cat1 and p.cat2=d.cat2);

update marketplace.temporary_product_views p inner join marketplace.temporary_cat_std_dev d on p.cat1=d.cat1 and p.cat2=d.cat2 set p.price_range='Cheap' where p.price between 0 and d.std_dev_minus;

update marketplace.temporary_product_views p inner join marketplace.temporary_cat_std_dev d on p.cat1=d.cat1 and p.cat2=d.cat2 set p.price_range='Medium' where p.price between d.std_dev_minus and d.std_dev_plus;

update marketplace.temporary_product_views p inner join marketplace.temporary_cat_std_dev d on p.cat1=d.cat1 and p.cat2=d.cat2 set p.price_range='Expensive' where p.price>=d.std_dev_plus;

update marketplace.product_views p inner join marketplace.temporary_product_views t on p.sku_config=t.sku_config set p.price_range_cat=t.price_range where country='Peru';

/*
update marketplace.product_views p set p.price_range_total='Cheap' where p.price between 0 and 700 and country='Peru';

update marketplace.product_views p set p.price_range_total='Medium' where p.price between 701 and 4000 and country='Peru';

update marketplace.product_views p set p.price_range_total='Expensive' where p.price>4000 and country='Peru';*/

drop table marketplace.temporary_cat;

drop table marketplace.temporary_product_views;

drop table marketplace.temporary_cat_std_dev;

update marketplace.product_views s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr, `PC1.5`=`PC1.5`/xr where s.country = 'Peru' and r.country='PER' and datediff(curdate(), s.date)<@days;

create table marketplace.temporary_product_views as select date_ordered as date, sku_config, sum(is_stockout) as oos from operations_pe.out_order_tracking where date_ordered>='2014-01-01' and datediff(curdate(), date_ordered)<@days group by date_ordered, sku_config;

create index date on marketplace.temporary_product_views(date); 
create index sku_config on marketplace.temporary_product_views(sku_config);

update marketplace.product_views p inner join marketplace.temporary_product_views t on p.date=t.date and p.sku_config=t.sku_config set p.oos=t.oos where p.country='Peru' and datediff(curdate(), p.date)<@days;

drop table marketplace.temporary_product_views;

create table marketplace.temporary_product_views as select date, skuconfig as sku_config, sum(cancelled) as canceled, sum(returns) as returned from development_pe.A_Master where date>='2014-01-01' and datediff(curdate(), date)<@days group by date, sku_config; 

create index date on marketplace.temporary_product_views(date); 
create index sku_config on marketplace.temporary_product_views(sku_config);

update marketplace.product_views p inner join marketplace.temporary_product_views t on p.date=t.date and p.sku_config=t.sku_config set p.canceled=t.canceled, p.returned=t.returned where p.country='Peru' and datediff(curdate(), p.date)<@days;

drop table marketplace.temporary_product_views;

-- Venezuela

set @last_date:=(select max(date) from marketplace.product_views where country='Venezuela');

insert into marketplace.product_views(country, yrmonth, week, date, sku_config, product_views) select 'Venezuela', concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), production.week_iso(date), date, product, sum(quantity) from attribution_testing.getfullbasket_ve where date>@last_date and stat=2 group by date, product;

create table marketplace.temporary_product_views select date, product as sku_config, sum(quantity) as add_to_cart from attribution_testing.getfullbasket_ve where date>@last_date and stat=0 group by date, product;

create index sku_config on marketplace.temporary_product_views(sku_config);
create index date on marketplace.temporary_product_views(date);

update marketplace.product_views p set add_to_cart = (select sum(add_to_cart) from marketplace.temporary_product_views a where a.sku_config=p.sku_config and a.date=p.date) where country='Venezuela' and p.date>@last_date;

drop table marketplace.temporary_product_views;

create table marketplace.temporary_product_views select date, skuconfig as sku_config, count(*) as unit_sold, sum(rev) as net_revenue, sum(pconepfive) as `PC1.5` from development_ve.A_Master where datediff(curdate(), date)<@days and orderaftercan=1 group by skuconfig, date;

create index sku_config on marketplace.temporary_product_views(sku_config);
create index date on marketplace.temporary_product_views(date);

update marketplace.product_views p set unit_sold = (select sum(unit_sold) from marketplace.temporary_product_views a where a.sku_config=p.sku_config and a.date=p.date) where country='Venezuela' and datediff(curdate(), p.date)<@days;

update marketplace.product_views p set net_revenue = (select sum(net_revenue) from marketplace.temporary_product_views a where a.sku_config=p.sku_config and a.date=p.date) where country='Venezuela' and datediff(curdate(), p.date)<@days;

update marketplace.product_views p set `PC1.5` = (select sum(`PC1.5`) from marketplace.temporary_product_views a where a.sku_config=p.sku_config and a.date=p.date) where country='Venezuela' and datediff(curdate(), p.date)<@days;

drop table marketplace.temporary_product_views;

create table marketplace.temporary_product_views select date, skuconfig as sku_config, count(*) as gross_unit_sold from development_ve.A_Master where datediff(curdate(), date)<@days and orderbeforecan=1 group by skuconfig, date;

create index sku_config on marketplace.temporary_product_views(sku_config);
create index date on marketplace.temporary_product_views(date);

update marketplace.product_views p set gross_unit_sold = (select sum(gross_unit_sold) from marketplace.temporary_product_views a where a.sku_config=p.sku_config and a.date=p.date) where country='Venezuela' and datediff(curdate(), p.date)<@days;

drop table marketplace.temporary_product_views;

update marketplace.product_views p inner join development_ve.A_Master_Catalog c on p.sku_config=c.sku_config set p.cat1=c.Cat1, p.cat2=c.Cat2, p.cat3=c.Cat3, p.price=c.price, p.cat_bp=c.cat_bp, p.product_name=c.sku_name, p.is_marketplace=c.ismarketplace where p.country='Venezuela'; 

create table marketplace.temporary_product_views select date, skuconfig from development_ve.A_Master where isMPlace=1 and datediff(curdate(), date)<@days;

create index date on marketplace.temporary_product_views(date);
create index skuconfig on marketplace.temporary_product_views(skuconfig);

update marketplace.product_views p inner join marketplace.temporary_product_views a on p.sku_config=a.skuconfig and p.date=a.date set p.is_marketplace=1 where p.country='Venezuela' and datediff(curdate(), p.date)<@days;

drop table marketplace.temporary_product_views;

create table marketplace.temporary_product_views(
sku_config varchar(255),
cat1 varchar(255),
cat2 varchar(255),
price float,
price_range varchar(255)
);

create index sku_config on marketplace.temporary_product_views(sku_config);
create index cat1 on marketplace.temporary_product_views(cat1);
create index cat2 on marketplace.temporary_product_views(cat2);

insert into marketplace.temporary_product_views(sku_config, cat1, cat2, price) select sku_config, cat1, cat2, price from marketplace.product_views where country='Venezuela' group by sku_config;

create table marketplace.temporary_cat_std_dev(
cat1 varchar(255),
cat2 varchar(255),
std_dev_minus float,
std_dev_plus float
);

create index cat1 on marketplace.temporary_cat_std_dev(cat1);
create index cat2 on marketplace.temporary_cat_std_dev(cat2);


insert into marketplace.temporary_cat_std_dev(cat1, cat2) select distinct cat1, cat2 from marketplace.temporary_product_views;

/*
update marketplace.temporary_cat_std_dev d set std_dev_minus = (select avg(price)-std(price) from marketplace.temporary_product_views p where p.cat1=d.cat1);

update marketplace.temporary_cat_std_dev d set std_dev_plus = (select avg(price)+std(price) from marketplace.temporary_product_views p where p.cat1=d.cat1);
*/

create table marketplace.temporary_cat as
select cat1, cat2, avg(price) as price from marketplace.temporary_product_views group by cat1, cat2;

create index cat1 on marketplace.temporary_cat(cat1);
create index cat2 on marketplace.temporary_cat(cat2);

update marketplace.temporary_cat_std_dev d set std_dev_minus = (select price-(price*0.15) from marketplace.temporary_cat p where p.cat1=d.cat1 and p.cat2=d.cat2);

update marketplace.temporary_cat_std_dev d set std_dev_plus = (select price+(price*0.15) from marketplace.temporary_cat p where p.cat1=d.cat1 and p.cat2=d.cat2);

update marketplace.temporary_product_views p inner join marketplace.temporary_cat_std_dev d on p.cat1=d.cat1 and p.cat2=d.cat2 set p.price_range='Cheap' where p.price between 0 and d.std_dev_minus;

update marketplace.temporary_product_views p inner join marketplace.temporary_cat_std_dev d on p.cat1=d.cat1 and p.cat2=d.cat2 set p.price_range='Medium' where p.price between d.std_dev_minus and d.std_dev_plus;

update marketplace.temporary_product_views p inner join marketplace.temporary_cat_std_dev d on p.cat1=d.cat1 and p.cat2=d.cat2 set p.price_range='Expensive' where p.price>=d.std_dev_plus;

update marketplace.product_views p inner join marketplace.temporary_product_views t on p.sku_config=t.sku_config set p.price_range_cat=t.price_range where country='Venezuela';

/*
update marketplace.product_views p set p.price_range_total='Cheap' where p.price between 0 and 700 and country='Venezuela';

update marketplace.product_views p set p.price_range_total='Medium' where p.price between 701 and 4000 and country='Venezuela';

update marketplace.product_views p set p.price_range_total='Expensive' where p.price>4000 and country='Venezuela';*/

drop table marketplace.temporary_cat;

drop table marketplace.temporary_product_views;

drop table marketplace.temporary_cat_std_dev;

update marketplace.product_views s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr, `PC1.5`=`PC1.5`/xr where s.country = 'Venezuela' and r.country='VEN' and datediff(curdate(), s.date)<@days;

create table marketplace.temporary_product_views as select date_ordered as date, sku_config, sum(is_stockout) as oos from operations_ve.out_order_tracking where date_ordered>='2014-01-01' and datediff(curdate(), date_ordered)<@days group by date_ordered, sku_config;

create index date on marketplace.temporary_product_views(date); 
create index sku_config on marketplace.temporary_product_views(sku_config);

update marketplace.product_views p inner join marketplace.temporary_product_views t on p.date=t.date and p.sku_config=t.sku_config set p.oos=t.oos where p.country='Venezuela' and datediff(curdate(), p.date)<@days;

drop table marketplace.temporary_product_views;

create table marketplace.temporary_product_views as select date, skuconfig as sku_config, sum(cancelled) as canceled, sum(returns) as returned from development_ve.A_Master where date>='2014-01-01' and datediff(curdate(), date)<@days group by date, sku_config; 

create index date on marketplace.temporary_product_views(date); 
create index sku_config on marketplace.temporary_product_views(sku_config);

update marketplace.product_views p inner join marketplace.temporary_product_views t on p.date=t.date and p.sku_config=t.sku_config set p.canceled=t.canceled, p.returned=t.returned where p.country='Venezuela' and datediff(curdate(), p.date)<@days;

drop table marketplace.temporary_product_views;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `regional_report`()
begin

 /* Index

create index created_at on sc_live_mx_reporting.sales_order_item(created_at);
create index created_at on sc_live_co_reporting.sales_order_item(created_at);
create index created_at on sc_live_pe_reporting.sales_order_item(created_at);
create index created_at on sc_live_ve_reporting.sales_order_item(created_at);

*/

-- Daily

set @days=300;

set @yrmonth=201401;

delete from marketplace.daily_regional_report where datediff(curdate(), date)<@days;

-- Mexico

insert into marketplace.daily_regional_report(country, yrmonth, week, date, type, net_revenue, shipping_fee, items_sold, unique_items_sold, orders, PC1, `PC1.5`, PC2) select 'Mexico', yrmonth, week, date, type, net_revenue, shipping_fee, items_sold, unique_items_sold, orders, PC1, `PC1.5`, PC2 from (select monthnum as yrmonth, production.week_iso(date) as week, date, 'Marketplace' as type, sum(rev) as net_revenue, sum(shippingfee) as shipping_fee, count(*) as items_sold, count(distinct skusimple) as unique_items_sold, count(distinct ordernum) as orders, sum(pcone) as PC1, sum(pconepfive) as `PC1.5`, sum(pctwo) as PC2 from development_mx.A_Master m where orderaftercan=1 and isMPlace=1 and monthnum>=201401 and datediff(curdate(), date)<@days group by date
union 
select monthnum as yrmonth, production.week_iso(date) as week, date, 'All' as type, sum(rev) as net_revenue, sum(shippingfee) as shipping_fee, count(*) as items_sold, count(distinct skusimple) as unique_items_sold, count(distinct ordernum) as orders, sum(pcone) as PC1, sum(pconepfive) as `PC1.5`, sum(pctwo) as PC2 from development_mx.A_Master m where orderaftercan=1 and monthnum>=201401 and datediff(curdate(), date)<@days group by date)a; 

update marketplace.daily_regional_report r set dropshipping = (select count(*) from operations_mx.out_order_tracking o where o.date_ordered=r.date and o.is_marketplace=1 and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Dropshipping') where country='Mexico' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set own_warehouse = (select count(*) from operations_mx.out_order_tracking o where o.date_ordered=r.date and o.is_marketplace=1 and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Own Warehouse') where country='Mexico' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set crossdocking = (select count(*) from operations_mx.out_order_tracking o where o.date_ordered=r.date and o.is_marketplace=1 and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Crossdocking') where country='Mexico' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set consignment = (select count(*) from operations_mx.out_order_tracking o where o.date_ordered=r.date and o.is_marketplace=1 and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Consignment') where country='Mexico' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set unshipped = (select count(*) from operations_mx.out_order_tracking o where o.date_ordered=r.date and o.is_marketplace=1 and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.date_shipped is null) where country='Mexico' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set wms_items = (select count(*) from operations_mx.out_order_tracking o where o.date_ordered=r.date and o.is_marketplace=1 and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping')) where country='Mexico' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set dropshipping = (select count(*) from operations_mx.out_order_tracking o where o.date_ordered=r.date and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Dropshipping') where country='Mexico' and type='All' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set own_warehouse = (select count(*) from operations_mx.out_order_tracking o where o.date_ordered=r.date and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Own Warehouse') where country='Mexico' and type='All' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set crossdocking = (select count(*) from operations_mx.out_order_tracking o where o.date_ordered=r.date and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Crossdocking') where country='Mexico' and type='All' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set consignment = (select count(*) from operations_mx.out_order_tracking o where o.date_ordered=r.date and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Consignment') where country='Mexico' and type='All' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set unshipped = (select count(*) from operations_mx.out_order_tracking o where o.date_ordered=r.date and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.date_shipped is null) where country='Mexico' and type='All' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set wms_items = (select count(*) from operations_mx.out_order_tracking o where o.date_ordered=r.date and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping')) where country='Mexico' and type='All' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set net_commission = (select sum(a.price*if(a.mplacefee<0, 0, a.mplacefee)) from development_mx.A_Master a where a.orderaftercan=1 and a.date=r.date and a.ismplace=1) where country='Mexico' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set sellers_with_sales = (select count(distinct supplier) from development_mx.A_Master a where a.orderaftercan=1 and r.date=a.date and ismplace=1) where country='Mexico' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set sellers_with_sales = (select count(distinct supplier) from development_mx.A_Master a where a.orderaftercan=1 and r.date=a.date) where country='Mexico' and type='All' and datediff(curdate(), r.date)<@days;

-- Exchange Rate

update marketplace.daily_regional_report s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr, net_commission=net_commission/xr, shipping_fee=shipping_fee/xr, PC1=PC1/xr, `PC1.5`=`PC1.5`/xr, PC2=PC2/xr where s.country = 'Mexico' and r.country='MEX' and datediff(curdate(), s.date)<@days;

-- Colombia

insert into marketplace.daily_regional_report(country, yrmonth, week, date, type, net_revenue, shipping_fee, items_sold, unique_items_sold, orders, PC1, `PC1.5`, PC2) select 'Colombia', yrmonth, week, date, type, net_revenue, shipping_fee, items_sold, unique_items_sold, orders, PC1, `PC1.5`, PC2 from (select monthnum as yrmonth, production.week_iso(date) as week, date, 'Marketplace' as type, sum(rev) as net_revenue, sum(shippingfee) as shipping_fee, count(*) as items_sold, count(distinct skusimple) as unique_items_sold, count(distinct ordernum) as orders, sum(pcone) as PC1, sum(pconepfive) as `PC1.5`, sum(pctwo) as PC2 from development_co_project.A_Master m where orderaftercan=1 and isMPlace=1 and monthnum>=201401 and datediff(curdate(), date)<@days group by date
union 
select monthnum as yrmonth, production.week_iso(date) as week, date, 'All' as type, sum(rev) as net_revenue, sum(shippingfee) as shipping_fee, count(*) as items_sold, count(distinct skusimple) as unique_items_sold, count(distinct ordernum) as orders, sum(pcone) as PC1, sum(pconepfive) as `PC1.5`, sum(pctwo) as PC2 from development_co_project.A_Master m where orderaftercan=1 and monthnum>=201401 and datediff(curdate(), date)<@days group by date)a; 

update marketplace.daily_regional_report r set dropshipping = (select count(*) from operations_co.out_order_tracking o where o.date_ordered=r.date and o.is_marketplace=1 and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Dropshipping') where country='Colombia' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set own_warehouse = (select count(*) from operations_co.out_order_tracking o where o.date_ordered=r.date and o.is_marketplace=1 and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Own Warehouse') where country='Colombia' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set crossdocking = (select count(*) from operations_co.out_order_tracking o where o.date_ordered=r.date and o.is_marketplace=1 and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Crossdocking') where country='Colombia' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set consignment = (select count(*) from operations_co.out_order_tracking o where o.date_ordered=r.date and o.is_marketplace=1 and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Consignment') where country='Colombia' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set unshipped = (select count(*) from operations_co.out_order_tracking o where o.date_ordered=r.date and o.is_marketplace=1 and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.date_shipped is null) where country='Colombia' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set wms_items = (select count(*) from operations_co.out_order_tracking o where o.date_ordered=r.date and o.is_marketplace=1 and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping')) where country='Colombia' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set dropshipping = (select count(*) from operations_co.out_order_tracking o where o.date_ordered=r.date and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Dropshipping') where country='Colombia' and type='All' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set own_warehouse = (select count(*) from operations_co.out_order_tracking o where o.date_ordered=r.date and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Own Warehouse') where country='Colombia' and type='All' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set crossdocking = (select count(*) from operations_co.out_order_tracking o where o.date_ordered=r.date and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Crossdocking') where country='Colombia' and type='All' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set consignment = (select count(*) from operations_co.out_order_tracking o where o.date_ordered=r.date and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Consignment') where country='Colombia' and type='All' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set unshipped = (select count(*) from operations_co.out_order_tracking o where o.date_ordered=r.date and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.date_shipped is null) where country='Colombia' and type='All' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set wms_items = (select count(*) from operations_co.out_order_tracking o where o.date_ordered=r.date and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping')) where country='Colombia' and type='All' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set net_commission = (select sum(a.price-a.cost) from development_co_project.A_Master a where a.orderaftercan=1 and a.date=r.date and a.ismplace=1) where country='Colombia' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set sellers_with_sales = (select count(distinct supplier) from development_co_project.A_Master a where a.orderaftercan=1 and r.date=a.date and ismplace=1) where country='Colombia' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set sellers_with_sales = (select count(distinct supplier) from development_co_project.A_Master a where a.orderaftercan=1 and r.date=a.date) where country='Colombia' and type='All' and datediff(curdate(), r.date)<@days;

-- Exchange Rate

update marketplace.daily_regional_report s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr, net_commission=net_commission/xr, shipping_fee=shipping_fee/xr, PC1=PC1/xr, `PC1.5`=`PC1.5`/xr, PC2=PC2/xr where s.country = 'Colombia' and r.country='COL' and datediff(curdate(), s.date)<@days;

-- Peru

insert into marketplace.daily_regional_report(country, yrmonth, week, date, type, net_revenue, shipping_fee, items_sold, unique_items_sold, orders, PC1, `PC1.5`, PC2) select 'Peru', yrmonth, week, date, type, net_revenue, shipping_fee, items_sold, unique_items_sold, orders, PC1, `PC1.5`, PC2 from (select monthnum as yrmonth, production.week_iso(date) as week, date, 'Marketplace' as type, sum(rev) as net_revenue, sum(shippingfee) as shipping_fee, count(*) as items_sold, count(distinct skusimple) as unique_items_sold, count(distinct ordernum) as orders, sum(pcone) as PC1, sum(pconepfive) as `PC1.5`, sum(pctwo) as PC2 from development_pe.A_Master m where orderaftercan=1 and isMPlace=1 and monthnum>=201401 and datediff(curdate(), date)<@days group by date
union 
select monthnum as yrmonth, production.week_iso(date) as week, date, 'All' as type, sum(rev) as net_revenue, sum(shippingfee) as shipping_fee, count(*) as items_sold, count(distinct skusimple) as unique_items_sold, count(distinct ordernum) as orders, sum(pcone) as PC1, sum(pconepfive) as `PC1.5`, sum(pctwo) as PC2 from development_pe.A_Master m where orderaftercan=1 and monthnum>=201401 and datediff(curdate(), date)<@days group by date)a; 

update marketplace.daily_regional_report r set dropshipping = (select count(*) from operations_pe.out_order_tracking o where o.date_ordered=r.date and o.is_marketplace=1 and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Dropshipping') where country='Peru' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set own_warehouse = (select count(*) from operations_pe.out_order_tracking o where o.date_ordered=r.date and o.is_marketplace=1 and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Own Warehouse') where country='Peru' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set crossdocking = (select count(*) from operations_pe.out_order_tracking o where o.date_ordered=r.date and o.is_marketplace=1 and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Crossdocking') where country='Peru' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set consignment = (select count(*) from operations_pe.out_order_tracking o where o.date_ordered=r.date and o.is_marketplace=1 and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Consignment') where country='Peru' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set unshipped = (select count(*) from operations_pe.out_order_tracking o where o.date_ordered=r.date and o.is_marketplace=1 and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.date_shipped is null) where country='Peru' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set wms_items = (select count(*) from operations_pe.out_order_tracking o where o.date_ordered=r.date and o.is_marketplace=1 and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping')) where country='Peru' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set dropshipping = (select count(*) from operations_pe.out_order_tracking o where o.date_ordered=r.date and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Dropshipping') where country='Peru' and type='All' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set own_warehouse = (select count(*) from operations_pe.out_order_tracking o where o.date_ordered=r.date and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Own Warehouse') where country='Peru' and type='All' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set crossdocking = (select count(*) from operations_pe.out_order_tracking o where o.date_ordered=r.date and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Crossdocking') where country='Peru' and type='All' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set consignment = (select count(*) from operations_pe.out_order_tracking o where o.date_ordered=r.date and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Consignment') where country='Peru' and type='All' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set unshipped = (select count(*) from operations_pe.out_order_tracking o where o.date_ordered=r.date and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.date_shipped is null) where country='Peru' and type='All' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set wms_items = (select count(*) from operations_pe.out_order_tracking o where o.date_ordered=r.date and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping')) where country='Peru' and type='All' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set net_commission = (select sum(a.price*if(a.mplacefee<0, 0, a.mplacefee)) from development_pe.A_Master a where a.orderaftercan=1 and a.date=r.date and a.ismplace=1) where country='Peru' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set sellers_with_sales = (select count(distinct supplier) from development_pe.A_Master a where a.orderaftercan=1 and r.date=a.date and ismplace=1) where country='Peru' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set sellers_with_sales = (select count(distinct supplier) from development_pe.A_Master a where a.orderaftercan=1 and r.date=a.date) where country='Peru' and type='All' and datediff(curdate(), r.date)<@days;

-- Exchange Rate

update marketplace.daily_regional_report s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr, net_commission=net_commission/xr, shipping_fee=shipping_fee/xr, PC1=PC1/xr, `PC1.5`=`PC1.5`/xr, PC2=PC2/xr where s.country = 'Peru' and r.country='PER' and datediff(curdate(), s.date)<@days;

-- Venezuela

insert into marketplace.daily_regional_report(country, yrmonth, week, date, type, net_revenue, shipping_fee, items_sold, unique_items_sold, orders, PC1, `PC1.5`, PC2) select 'Venezuela', yrmonth, week, date, type, net_revenue, shipping_fee, items_sold, unique_items_sold, orders, PC1, `PC1.5`, PC2 from (select monthnum as yrmonth, production.week_iso(date) as week, date, 'Marketplace' as type, sum(rev) as net_revenue, sum(shippingfee) as shipping_fee, count(*) as items_sold, count(distinct skusimple) as unique_items_sold, count(distinct ordernum) as orders, sum(pcone) as PC1, sum(pconepfive) as `PC1.5`, sum(pctwo) as PC2 from development_ve.A_Master m where orderaftercan=1 and isMPlace=1 and monthnum>=201401 and datediff(curdate(), date)<@days group by date
union 
select monthnum as yrmonth, production.week_iso(date) as week, date, 'All' as type, sum(rev) as net_revenue, sum(shippingfee) as shipping_fee, count(*) as items_sold, count(distinct skusimple) as unique_items_sold, count(distinct ordernum) as orders, sum(pcone) as PC1, sum(pconepfive) as `PC1.5`, sum(pctwo) as PC2 from development_ve.A_Master m where orderaftercan=1 and monthnum>=201401 and datediff(curdate(), date)<@days group by date)a; 

update marketplace.daily_regional_report r set dropshipping = (select count(*) from operations_ve.out_order_tracking o where o.date_ordered=r.date and o.is_marketplace=1 and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Dropshipping') where country='Venezuela' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set own_warehouse = (select count(*) from operations_ve.out_order_tracking o where o.date_ordered=r.date and o.is_marketplace=1 and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Own Warehouse') where country='Venezuela' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set crossdocking = (select count(*) from operations_ve.out_order_tracking o where o.date_ordered=r.date and o.is_marketplace=1 and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Crossdocking') where country='Venezuela' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set consignment = (select count(*) from operations_ve.out_order_tracking o where o.date_ordered=r.date and o.is_marketplace=1 and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Consignment') where country='Venezuela' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set unshipped = (select count(*) from operations_ve.out_order_tracking o where o.date_ordered=r.date and o.is_marketplace=1 and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.date_shipped is null) where country='Venezuela' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set wms_items = (select count(*) from operations_ve.out_order_tracking o where o.date_ordered=r.date and o.is_marketplace=1 and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping')) where country='Venezuela' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set dropshipping = (select count(*) from operations_ve.out_order_tracking o where o.date_ordered=r.date and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Dropshipping') where country='Venezuela' and type='All' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set own_warehouse = (select count(*) from operations_ve.out_order_tracking o where o.date_ordered=r.date and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Own Warehouse') where country='Venezuela' and type='All' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set crossdocking = (select count(*) from operations_ve.out_order_tracking o where o.date_ordered=r.date and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Crossdocking') where country='Venezuela' and type='All' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set consignment = (select count(*) from operations_ve.out_order_tracking o where o.date_ordered=r.date and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.fulfillment_type_real ='Consignment') where country='Venezuela' and type='All' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set unshipped = (select count(*) from operations_ve.out_order_tracking o where o.date_ordered=r.date and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping') and o.date_shipped is null) where country='Venezuela' and type='All' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set wms_items = (select count(*) from operations_ve.out_order_tracking o where o.date_ordered=r.date and o.status_wms in ('Analisando quebra', 'dropshipping notified', 'DS estoque reservado', 'Expedido', 'Faturado', 'handled_by_marketplace', 'Electronic good', 'Aguardando expedicao', 'Aguardando separacao', 'Aguardando estoque', 'Waiting dropshipping')) where country='Venezuela' and type='All' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set net_commission = (select sum(a.price*if(a.mplacefee<0, 0, a.mplacefee)) from development_ve.A_Master a where a.orderaftercan=1 and a.date=r.date and a.ismplace=1) where country='Venezuela' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set sellers_with_sales = (select count(distinct supplier) from development_ve.A_Master a where a.orderaftercan=1 and r.date=a.date and ismplace=1) where country='Venezuela' and type='Marketplace' and datediff(curdate(), r.date)<@days;

update marketplace.daily_regional_report r set sellers_with_sales = (select count(distinct supplier) from development_ve.A_Master a where a.orderaftercan=1 and r.date=a.date) where country='Venezuela' and type='All' and datediff(curdate(), r.date)<@days;

-- Exchange Rate

update marketplace.daily_regional_report s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr, net_commission=net_commission/xr, shipping_fee=shipping_fee/xr, PC1=PC1/xr, `PC1.5`=`PC1.5`/xr, PC2=PC2/xr where s.country = 'Venezuela' and r.country='VEN' and datediff(curdate(), s.date)<@days;

-- Marketplace Share

create table marketplace.temporary_daily_regional_report as select country, date, net_revenue from marketplace.daily_regional_report where type='All';

create index country on marketplace.temporary_daily_regional_report(country);

create index date on marketplace.temporary_daily_regional_report(date);

update marketplace.daily_regional_report r inner join marketplace.temporary_daily_regional_report t on r.country=t.country and r.date=t.date set r.`% share` = r.net_revenue/t.net_revenue;

drop table marketplace.temporary_daily_regional_report;

delete from marketplace.daily_regional_report where date=curdate();

-- Monthly

delete from marketplace.monthly_regional_report;

insert into marketplace.monthly_regional_report(country, yrmonth, type, net_revenue, shipping_fee, dropshipping, crossdocking, own_warehouse, consignment, unshipped, wms_items, net_commission, orders, items_sold, unique_items_sold, PC1, `PC1.5`, PC2, month_days) select country, yrmonth, type, sum(net_revenue), sum(shipping_fee), sum(dropshipping), sum(crossdocking), sum(own_warehouse), sum(consignment), sum(unshipped), sum(wms_items), sum(net_commission), sum(orders), sum(items_sold), sum(unique_items_sold), sum(PC1), sum(`PC1.5`), sum(PC2), day(last_day(date)) from marketplace.daily_regional_report where yrmonth>=@yrmonth group by country, yrmonth, type;  

-- Marketplace Share

create table marketplace.temporary_monthly_regional_report as select country, yrmonth, net_revenue from marketplace.monthly_regional_report where type='All';

create index country on marketplace.temporary_monthly_regional_report(country);

create index yrmonth on marketplace.temporary_monthly_regional_report(yrmonth);

update marketplace.monthly_regional_report r inner join marketplace.temporary_monthly_regional_report t on r.country=t.country and r.yrmonth=t.yrmonth set r.`% share` = r.net_revenue/t.net_revenue;

drop table marketplace.temporary_monthly_regional_report;

-- Sellers with Sales

update marketplace.monthly_regional_report r set sellers_with_sales = (select count(distinct supplier) from development_mx.A_Master a where a.orderaftercan=1 and r.yrmonth=a.monthnum and ismplace=1) where country='Mexico' and type='Marketplace' and yrmonth>=@yrmonth;

update marketplace.monthly_regional_report r set sellers_with_sales = (select count(distinct supplier) from development_mx.A_Master a where a.orderaftercan=1 and r.yrmonth=a.monthnum) where country='Mexico' and type='All' and yrmonth>=@yrmonth;

update marketplace.monthly_regional_report r set sellers_with_sales = (select count(distinct supplier) from development_co_project.A_Master a where a.orderaftercan=1 and r.yrmonth=a.monthnum and ismplace=1) where country='Colombia' and type='Marketplace' and yrmonth>=@yrmonth;

update marketplace.monthly_regional_report r set sellers_with_sales = (select count(distinct supplier) from development_co_project.A_Master a where a.orderaftercan=1 and r.yrmonth=a.monthnum) where country='Colombia' and type='All' and yrmonth>=@yrmonth;

update marketplace.monthly_regional_report r set sellers_with_sales = (select count(distinct supplier) from development_pe.A_Master a where a.orderaftercan=1 and r.yrmonth=a.monthnum and ismplace=1) where country='Peru' and type='Marketplace' and yrmonth>=@yrmonth;

update marketplace.monthly_regional_report r set sellers_with_sales = (select count(distinct supplier) from development_pe.A_Master a where a.orderaftercan=1 and r.yrmonth=a.monthnum) where country='Peru' and type='All' and yrmonth>=@yrmonth;

update marketplace.monthly_regional_report r set sellers_with_sales = (select count(distinct supplier) from development_ve.A_Master a where a.orderaftercan=1 and r.yrmonth=a.monthnum and ismplace=1) where country='Venezuela' and type='Marketplace' and yrmonth>=@yrmonth;

update marketplace.monthly_regional_report r set sellers_with_sales = (select count(distinct supplier) from development_ve.A_Master a where a.orderaftercan=1 and r.yrmonth=a.monthnum) where country='Venezuela' and type='All' and yrmonth>=@yrmonth;

-- Last 4 Weeks

update marketplace.monthly_regional_report r set sellers_with_sales = (select count(distinct supplier) from development_mx.A_Master a where a.orderaftercan=1 and datediff(curdate(), a.date)<=28 and ismplace=1) where country='Mexico' and type='Marketplace' and substring(yrmonth, 1, 4)=year(curdate()) and substring(yrmonth, 5, 6)=month(curdate());

update marketplace.monthly_regional_report r set sellers_with_sales = (select count(distinct supplier) from development_mx.A_Master a where a.orderaftercan=1 and datediff(curdate(), a.date)<=28) where country='Mexico' and type='All' and substring(yrmonth, 1, 4)=year(curdate()) and substring(yrmonth, 5, 6)=month(curdate());

update marketplace.monthly_regional_report r set sellers_with_sales = (select count(distinct supplier) from development_co_project.A_Master a where a.orderaftercan=1 and datediff(curdate(), a.date)<=28 and ismplace=1) where country='Colombia' and type='Marketplace' and substring(yrmonth, 1, 4)=year(curdate()) and substring(yrmonth, 5, 6)=month(curdate());

update marketplace.monthly_regional_report r set sellers_with_sales = (select count(distinct supplier) from development_co_project.A_Master a where a.orderaftercan=1 and datediff(curdate(), a.date)<=28) where country='Colombia' and type='All' and substring(yrmonth, 1, 4)=year(curdate()) and substring(yrmonth, 5, 6)=month(curdate());

update marketplace.monthly_regional_report r set sellers_with_sales = (select count(distinct supplier) from development_pe.A_Master a where a.orderaftercan=1 and datediff(curdate(), a.date)<=28 and ismplace=1) where country='Peru' and type='Marketplace' and substring(yrmonth, 1, 4)=year(curdate()) and substring(yrmonth, 5, 6)=month(curdate());

update marketplace.monthly_regional_report r set sellers_with_sales = (select count(distinct supplier) from development_pe.A_Master a where a.orderaftercan=1 and datediff(curdate(), a.date)<=28) where country='Peru' and type='All' and substring(yrmonth, 1, 4)=year(curdate()) and substring(yrmonth, 5, 6)=month(curdate());

update marketplace.monthly_regional_report r set sellers_with_sales = (select count(distinct supplier) from development_ve.A_Master a where a.orderaftercan=1 and datediff(curdate(), a.date)<=28 and ismplace=1) where country='Venezuela' and type='Marketplace' and substring(yrmonth, 1, 4)=year(curdate()) and substring(yrmonth, 5, 6)=month(curdate());

update marketplace.monthly_regional_report r set sellers_with_sales = (select count(distinct supplier) from development_ve.A_Master a where a.orderaftercan=1 and datediff(curdate(), a.date)<=28) where country='Venezuela' and type='All' and substring(yrmonth, 1, 4)=year(curdate()) and substring(yrmonth, 5, 6)=month(curdate());

-- Targets
/*
update marketplace.monthly_regional_report r inner join development_mx.M_Targets t on r.yrmonth=t.monthnum inner join development_mx.A_E_BI_ExchangeRate_USD s on s.Month_Num=r.yrmonth and t.country=s.country set r.target_monthly=t.value*s.xr where r.country='Mexico' and t.country='MEX' and t.type='NMV' and r.type='ALL';

update marketplace.monthly_regional_report r inner join development_mx.M_Targets t on r.yrmonth=t.monthnum inner join development_mx.A_E_BI_ExchangeRate_USD s on s.Month_Num=r.yrmonth and t.country=s.country set r.target_monthly=t.value*s.xr where r.country='Colombia' and t.country='COL' and t.type='NMV' and r.type='ALL';

update marketplace.monthly_regional_report r inner join development_mx.M_Targets t on r.yrmonth=t.monthnum inner join development_mx.A_E_BI_ExchangeRate_USD s on s.Month_Num=r.yrmonth and t.country=s.country set r.target_monthly=t.value*s.xr where r.country='Peru' and t.country='PER' and t.type='NMV' and r.type='ALL';

update marketplace.monthly_regional_report r inner join development_mx.M_Targets t on r.yrmonth=t.monthnum inner join development_mx.A_E_BI_ExchangeRate_USD s on s.Month_Num=r.yrmonth and t.country=s.country set r.target_monthly=t.value*s.xr where r.country='Venezuela' and t.country='VEN' and t.type='NMV' and r.type='ALL';

update marketplace.monthly_regional_report r inner join development_mx.A_E_BI_ExchangeRate s on s.Month_Num=r.yrmonth set r.target_monthly=r.target_monthly/s.xr where r.country='Mexico' and s.country='MEX' and r.type='ALL';

update marketplace.monthly_regional_report r inner join development_mx.A_E_BI_ExchangeRate s on s.Month_Num=r.yrmonth set r.target_monthly=r.target_monthly/s.xr where r.country='Colombia' and s.country='COL' and r.type='ALL';

update marketplace.monthly_regional_report r inner join development_mx.A_E_BI_ExchangeRate s on s.Month_Num=r.yrmonth set r.target_monthly=r.target_monthly/s.xr where r.country='Peru' and s.country='PER' and r.type='ALL';

update marketplace.monthly_regional_report r inner join development_mx.A_E_BI_ExchangeRate s on s.Month_Num=r.yrmonth set r.target_monthly=r.target_monthly/s.xr where r.country='Venezuela' and s.country='VEN' and r.type='ALL';*/

update marketplace.monthly_regional_report r inner join development_mx.M_Targets t on r.yrmonth=t.monthnum set r.target_monthly=t.value where r.country='Mexico' and t.country='MEX' and t.type='NMV' and r.type='ALL';

update marketplace.monthly_regional_report r inner join development_mx.M_Targets t on r.yrmonth=t.monthnum set r.target_monthly=t.value where r.country='Colombia' and t.country='COL' and t.type='NMV' and r.type='ALL';

update marketplace.monthly_regional_report r inner join development_mx.M_Targets t on r.yrmonth=t.monthnum set r.target_monthly=t.value where r.country='Peru' and t.country='PER' and t.type='NMV' and r.type='ALL';

update marketplace.monthly_regional_report r inner join development_mx.M_Targets t on r.yrmonth=t.monthnum set r.target_monthly=t.value where r.country='Venezuela' and t.country='VEN' and t.type='NMV' and r.type='ALL';

create table marketplace.temporary_monthly_regional_report as select country, yrmonth, target_monthly from marketplace.monthly_regional_report where type='All';

create index country on marketplace.temporary_monthly_regional_report(country);

create index yrmonth on marketplace.temporary_monthly_regional_report(yrmonth);

update marketplace.monthly_regional_report r inner join marketplace.temporary_monthly_regional_report t on r.country=t.country and r.yrmonth=t.yrmonth inner join development_mx.M_Targets s on s.MonthNum=r.yrmonth and s.type=r.type set r.target_monthly = t.target_monthly*s.value where s.type='Marketplace' and r.type='Marketplace' and r.country='Mexico' and s.country='MEX';

update marketplace.monthly_regional_report r inner join marketplace.temporary_monthly_regional_report t on r.country=t.country and r.yrmonth=t.yrmonth inner join development_mx.M_Targets s on s.MonthNum=r.yrmonth and s.type=r.type set r.target_monthly = t.target_monthly*s.value where s.type='Marketplace' and r.type='Marketplace' and r.country='Colombia' and s.country='COL';

update marketplace.monthly_regional_report r inner join marketplace.temporary_monthly_regional_report t on r.country=t.country and r.yrmonth=t.yrmonth inner join development_mx.M_Targets s on s.MonthNum=r.yrmonth and s.type=r.type set r.target_monthly = t.target_monthly*s.value where s.type='Marketplace' and r.type='Marketplace' and r.country='Peru' and s.country='PER';

update marketplace.monthly_regional_report r inner join marketplace.temporary_monthly_regional_report t on r.country=t.country and r.yrmonth=t.yrmonth inner join development_mx.M_Targets s on s.MonthNum=r.yrmonth and s.type=r.type set r.target_monthly = t.target_monthly*s.value where s.type='Marketplace' and r.type='Marketplace' and r.country='Venezuela' and s.country='VEN';

drop table marketplace.temporary_monthly_regional_report;

update marketplace.monthly_regional_report set target_daily=target_monthly/month_days; 

update marketplace.monthly_regional_report set `% runrate` = net_revenue/target_monthly;

update marketplace.monthly_regional_report set `% deviation` = net_revenue/target_monthly;

update marketplace.monthly_regional_report r set `% deviation` = net_revenue/(target_daily*day(date_sub(curdate(), interval 1 day))) where r.yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())));

update marketplace.daily_regional_report d inner join marketplace.monthly_regional_report m on m.country=d.country and m.type=d.type and m.yrmonth=d.yrmonth set d.target_daily=m.target_daily; 

-- MoM

-- Mexico

-- Net Revenue

create table marketplace.temporary_mom as select a.yrmonth, a.date, a.type, a.net_revenue/b.net_revenue as mom from (select a.yrmonth, a.date, a.type, sum(b.net_revenue) as net_revenue from (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.daily_regional_report where country='Mexico' group by type, date)b, (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.daily_regional_report where country='Mexico' group by type, date)a where a.type=b.type and a.yrmonth=b.yrmonth group by a.date, a.type order by a.type, a.date)a, (select a.yrmonth, a.date, a.type, sum(b.net_revenue) as net_revenue from (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.daily_regional_report where country='Mexico' group by type, date)b, (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.daily_regional_report where country='Mexico' group by type, date)a where a.type=b.type and a.yrmonth=b.yrmonth group by a.date, a.type order by a.type, a.date)b where a.type=b.type and a.date - interval 1 month = b.date order by a.type, a.date desc;

create index type on marketplace.temporary_mom(yrmonth, type);

update marketplace.monthly_regional_report g inner join marketplace.temporary_mom t on g.yrmonth=t.yrmonth and g.type=t.type set g.MoM=t.mom where country='Mexico'; 

update marketplace.monthly_regional_report set MoM=MoM-1 where country='Mexico';

drop table marketplace.temporary_mom;

-- MoM - Last Month

create table marketplace.temporary_daily_regional_report as select * from marketplace.daily_regional_report where country='Mexico' and yrmonth between concat(year((curdate() - interval 1 month)),if(month((curdate() - interval 1 month))<10,concat(0,month((curdate()) - interval 1 month)),month((curdate()) - interval 1 month))) and concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())));

delete from marketplace.temporary_daily_regional_report where date between curdate() - interval 1 month and (concat(year(curdate()),'-',if(month(curdate())<10,concat(0,month(curdate())),month(curdate())),'-01') - interval 1 day);

-- Net Revenue

create table marketplace.temporary_daily_regional_report_2 as select a.yrmonth, a.date, a.type, a.net_revenue/b.net_revenue as mom from (select a.yrmonth, a.date, a.type, sum(b.net_revenue) as net_revenue from (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.temporary_daily_regional_report where country='Mexico' group by type, date)b, (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.temporary_daily_regional_report where country='Mexico' group by type, date)a where a.type=b.type and a.yrmonth=b.yrmonth group by a.date, a.type order by a.type, a.date)a, (select a.yrmonth, a.date, a.type, sum(b.net_revenue) as net_revenue from (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.temporary_daily_regional_report where country='Mexico' group by type, date)b, (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.temporary_daily_regional_report where country='Mexico' group by type, date)a where a.type=b.type and a.yrmonth=b.yrmonth group by a.date, a.type order by a.type, a.date)b where a.type=b.type and a.date - interval 1 month = b.date order by a.type, a.date desc;

create index type on marketplace.temporary_daily_regional_report_2(date, type);

update marketplace.monthly_regional_report g inner join marketplace.temporary_daily_regional_report_2 t on g.yrmonth=t.yrmonth and g.type=t.type set g.MoM=t.mom where country='Mexico' and concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))=g.yrmonth; 

update marketplace.monthly_regional_report set MoM=MoM-1 where country='Mexico' and concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))=yrmonth;

drop table marketplace.temporary_daily_regional_report;

drop table marketplace.temporary_daily_regional_report_2;

-- Colombia

-- Net Revenue

create table marketplace.temporary_mom as select a.yrmonth, a.date, a.type, a.net_revenue/b.net_revenue as mom from (select a.yrmonth, a.date, a.type, sum(b.net_revenue) as net_revenue from (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.daily_regional_report where country='Colombia' group by type, date)b, (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.daily_regional_report where country='Colombia' group by type, date)a where a.type=b.type and a.yrmonth=b.yrmonth group by a.date, a.type order by a.type, a.date)a, (select a.yrmonth, a.date, a.type, sum(b.net_revenue) as net_revenue from (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.daily_regional_report where country='Colombia' group by type, date)b, (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.daily_regional_report where country='Colombia' group by type, date)a where a.type=b.type and a.yrmonth=b.yrmonth group by a.date, a.type order by a.type, a.date)b where a.type=b.type and a.date - interval 1 month = b.date order by a.type, a.date desc;

create index type on marketplace.temporary_mom(yrmonth, type);

update marketplace.monthly_regional_report g inner join marketplace.temporary_mom t on g.yrmonth=t.yrmonth and g.type=t.type set g.MoM=t.mom where country='Colombia'; 

update marketplace.monthly_regional_report set MoM=MoM-1 where country='Colombia';

drop table marketplace.temporary_mom;

-- MoM - Last Month

create table marketplace.temporary_daily_regional_report as select * from marketplace.daily_regional_report where country='Colombia' and yrmonth between concat(year((curdate() - interval 1 month)),if(month((curdate() - interval 1 month))<10,concat(0,month((curdate()) - interval 1 month)),month((curdate()) - interval 1 month))) and concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())));

delete from marketplace.temporary_daily_regional_report where date between curdate() - interval 1 month and (concat(year(curdate()),'-',if(month(curdate())<10,concat(0,month(curdate())),month(curdate())),'-01') - interval 1 day);

-- Net Revenue

create table marketplace.temporary_daily_regional_report_2 as select a.yrmonth, a.date, a.type, a.net_revenue/b.net_revenue as mom from (select a.yrmonth, a.date, a.type, sum(b.net_revenue) as net_revenue from (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.temporary_daily_regional_report where country='Colombia' group by type, date)b, (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.temporary_daily_regional_report where country='Colombia' group by type, date)a where a.type=b.type and a.yrmonth=b.yrmonth group by a.date, a.type order by a.type, a.date)a, (select a.yrmonth, a.date, a.type, sum(b.net_revenue) as net_revenue from (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.temporary_daily_regional_report where country='Colombia' group by type, date)b, (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.temporary_daily_regional_report where country='Colombia' group by type, date)a where a.type=b.type and a.yrmonth=b.yrmonth group by a.date, a.type order by a.type, a.date)b where a.type=b.type and a.date - interval 1 month = b.date order by a.type, a.date desc;

create index type on marketplace.temporary_daily_regional_report_2(date, type);

update marketplace.monthly_regional_report g inner join marketplace.temporary_daily_regional_report_2 t on g.yrmonth=t.yrmonth and g.type=t.type set g.MoM=t.mom where country='Colombia' and concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))=g.yrmonth; 

update marketplace.monthly_regional_report set MoM=MoM-1 where country='Colombia' and concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))=yrmonth;

drop table marketplace.temporary_daily_regional_report;

drop table marketplace.temporary_daily_regional_report_2;

-- Peru

-- Net Revenue

create table marketplace.temporary_mom as select a.yrmonth, a.date, a.type, a.net_revenue/b.net_revenue as mom from (select a.yrmonth, a.date, a.type, sum(b.net_revenue) as net_revenue from (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.daily_regional_report where country='Peru' group by type, date)b, (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.daily_regional_report where country='Peru' group by type, date)a where a.type=b.type and a.yrmonth=b.yrmonth group by a.date, a.type order by a.type, a.date)a, (select a.yrmonth, a.date, a.type, sum(b.net_revenue) as net_revenue from (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.daily_regional_report where country='Peru' group by type, date)b, (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.daily_regional_report where country='Peru' group by type, date)a where a.type=b.type and a.yrmonth=b.yrmonth group by a.date, a.type order by a.type, a.date)b where a.type=b.type and a.date - interval 1 month = b.date order by a.type, a.date desc;

create index type on marketplace.temporary_mom(yrmonth, type);

update marketplace.monthly_regional_report g inner join marketplace.temporary_mom t on g.yrmonth=t.yrmonth and g.type=t.type set g.MoM=t.mom where country='Peru'; 

update marketplace.monthly_regional_report set MoM=MoM-1 where country='Peru';

drop table marketplace.temporary_mom;

-- MoM - Last Month

create table marketplace.temporary_daily_regional_report as select * from marketplace.daily_regional_report where country='Peru' and yrmonth between concat(year((curdate() - interval 1 month)),if(month((curdate() - interval 1 month))<10,concat(0,month((curdate()) - interval 1 month)),month((curdate()) - interval 1 month))) and concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())));

delete from marketplace.temporary_daily_regional_report where date between curdate() - interval 1 month and (concat(year(curdate()),'-',if(month(curdate())<10,concat(0,month(curdate())),month(curdate())),'-01') - interval 1 day);

-- Net Revenue

create table marketplace.temporary_daily_regional_report_2 as select a.yrmonth, a.date, a.type, a.net_revenue/b.net_revenue as mom from (select a.yrmonth, a.date, a.type, sum(b.net_revenue) as net_revenue from (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.temporary_daily_regional_report where country='Peru' group by type, date)b, (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.temporary_daily_regional_report where country='Peru' group by type, date)a where a.type=b.type and a.yrmonth=b.yrmonth group by a.date, a.type order by a.type, a.date)a, (select a.yrmonth, a.date, a.type, sum(b.net_revenue) as net_revenue from (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.temporary_daily_regional_report where country='Peru' group by type, date)b, (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.temporary_daily_regional_report where country='Peru' group by type, date)a where a.type=b.type and a.yrmonth=b.yrmonth group by a.date, a.type order by a.type, a.date)b where a.type=b.type and a.date - interval 1 month = b.date order by a.type, a.date desc;

create index type on marketplace.temporary_daily_regional_report_2(date, type);

update marketplace.monthly_regional_report g inner join marketplace.temporary_daily_regional_report_2 t on g.yrmonth=t.yrmonth and g.type=t.type set g.MoM=t.mom where country='Peru' and concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))=g.yrmonth; 

update marketplace.monthly_regional_report set MoM=MoM-1 where country='Peru' and concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))=yrmonth;

drop table marketplace.temporary_daily_regional_report;

drop table marketplace.temporary_daily_regional_report_2;

-- Venezuela

-- Net Revenue

create table marketplace.temporary_mom as select a.yrmonth, a.date, a.type, a.net_revenue/b.net_revenue as mom from (select a.yrmonth, a.date, a.type, sum(b.net_revenue) as net_revenue from (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.daily_regional_report where country='Venezuela' group by type, date)b, (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.daily_regional_report where country='Venezuela' group by type, date)a where a.type=b.type and a.yrmonth=b.yrmonth group by a.date, a.type order by a.type, a.date)a, (select a.yrmonth, a.date, a.type, sum(b.net_revenue) as net_revenue from (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.daily_regional_report where country='Venezuela' group by type, date)b, (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.daily_regional_report where country='Venezuela' group by type, date)a where a.type=b.type and a.yrmonth=b.yrmonth group by a.date, a.type order by a.type, a.date)b where a.type=b.type and a.date - interval 1 month = b.date order by a.type, a.date desc;

create index type on marketplace.temporary_mom(yrmonth, type);

update marketplace.monthly_regional_report g inner join marketplace.temporary_mom t on g.yrmonth=t.yrmonth and g.type=t.type set g.MoM=t.mom where country='Venezuela'; 

update marketplace.monthly_regional_report set MoM=MoM-1 where country='Venezuela';

drop table marketplace.temporary_mom;

-- MoM - Last Month

create table marketplace.temporary_daily_regional_report as select * from marketplace.daily_regional_report where country='Venezuela' and yrmonth between concat(year((curdate() - interval 1 month)),if(month((curdate() - interval 1 month))<10,concat(0,month((curdate()) - interval 1 month)),month((curdate()) - interval 1 month))) and concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())));

delete from marketplace.temporary_daily_regional_report where date between curdate() - interval 1 month and (concat(year(curdate()),'-',if(month(curdate())<10,concat(0,month(curdate())),month(curdate())),'-01') - interval 1 day);

-- Net Revenue

create table marketplace.temporary_daily_regional_report_2 as select a.yrmonth, a.date, a.type, a.net_revenue/b.net_revenue as mom from (select a.yrmonth, a.date, a.type, sum(b.net_revenue) as net_revenue from (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.temporary_daily_regional_report where country='Venezuela' group by type, date)b, (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.temporary_daily_regional_report where country='Venezuela' group by type, date)a where a.type=b.type and a.yrmonth=b.yrmonth group by a.date, a.type order by a.type, a.date)a, (select a.yrmonth, a.date, a.type, sum(b.net_revenue) as net_revenue from (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.temporary_daily_regional_report where country='Venezuela' group by type, date)b, (select yrmonth, date, type, sum(net_revenue) as net_revenue from marketplace.temporary_daily_regional_report where country='Venezuela' group by type, date)a where a.type=b.type and a.yrmonth=b.yrmonth group by a.date, a.type order by a.type, a.date)b where a.type=b.type and a.date - interval 1 month = b.date order by a.type, a.date desc;

create index type on marketplace.temporary_daily_regional_report_2(date, type);

update marketplace.monthly_regional_report g inner join marketplace.temporary_daily_regional_report_2 t on g.yrmonth=t.yrmonth and g.type=t.type set g.MoM=t.mom where country='Venezuela' and concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))=g.yrmonth; 

update marketplace.monthly_regional_report set MoM=MoM-1 where country='Venezuela' and concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))=yrmonth;

drop table marketplace.temporary_daily_regional_report;

drop table marketplace.temporary_daily_regional_report_2;

-- Active / Visible

update marketplace.monthly_regional_report m set sku_config_active = (select sum(sku_config_active) from marketplace.sku_visible h where h.country=m.country and h.yrmonth=m.yrmonth and h.is_marketplace=1) where type = 'Marketplace'; 

update marketplace.monthly_regional_report m set sku_config_active = (select sum(sku_config_active) from marketplace.sku_visible h where h.country=m.country and h.yrmonth=m.yrmonth) where type = 'All'; 

update marketplace.monthly_regional_report m set sku_simple_active = (select sum(sku_simple_active) from marketplace.sku_visible h where h.country=m.country and h.yrmonth=m.yrmonth and h.is_marketplace=1) where type = 'Marketplace'; 

update marketplace.monthly_regional_report m set sku_simple_active = (select sum(sku_simple_active) from marketplace.sku_visible h where h.country=m.country and h.yrmonth=m.yrmonth) where type = 'All'; 

update marketplace.monthly_regional_report m set supplier_active = (select sum(supplier_active) from marketplace.sku_visible h where h.country=m.country and h.yrmonth=m.yrmonth and h.is_marketplace=1) where type = 'Marketplace'; 

update marketplace.monthly_regional_report m set supplier_active = (select sum(supplier_active) from marketplace.sku_visible h where h.country=m.country and h.yrmonth=m.yrmonth) where type = 'All'; 

update marketplace.monthly_regional_report m set sku_config_visible = (select sum(sku_config_visible) from marketplace.sku_visible h where h.country=m.country and h.yrmonth=m.yrmonth and h.is_marketplace=1) where type = 'Marketplace'; 

update marketplace.monthly_regional_report m set sku_config_visible = (select sum(sku_config_visible) from marketplace.sku_visible h where h.country=m.country and h.yrmonth=m.yrmonth) where type = 'All'; 

update marketplace.monthly_regional_report m set sku_simple_visible = (select sum(sku_simple_visible) from marketplace.sku_visible h where h.country=m.country and h.yrmonth=m.yrmonth and h.is_marketplace=1) where type = 'Marketplace'; 

update marketplace.monthly_regional_report m set sku_simple_visible = (select sum(sku_simple_visible) from marketplace.sku_visible h where h.country=m.country and h.yrmonth=m.yrmonth) where type = 'All'; 

update marketplace.monthly_regional_report m set supplier_visible = (select sum(supplier_visible) from marketplace.sku_visible h where h.country=m.country and h.yrmonth=m.yrmonth and h.is_marketplace=1) where type = 'Marketplace'; 

update marketplace.monthly_regional_report m set supplier_visible = (select sum(supplier_visible) from marketplace.sku_visible h where h.country=m.country and h.yrmonth=m.yrmonth) where type = 'All'; 

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `sellers_trend`()
begin

delete from marketplace.sellers_trend where yrmonth>=201404;

-- Supplier

insert into marketplace.sellers_trend(country, yrmonth, type, supplier) select country, yrmonth, type, sum(supplier_created) from marketplace.sku_visible where is_marketplace=1 and yrmonth>=201404 group by country, yrmonth, type;

-- Supplier With Sales

update marketplace.sellers_trend t set seller_with_sales = (select count(distinct a.supplier) from development_mx.A_Master a, bob_live_mx.supplier s where t.yrmonth=a.monthnum and a.supplier=s.name and t.type=s.type and a.orderaftercan=1 and a.ismplace=1) where country='Mexico'; 

update marketplace.sellers_trend t set seller_with_sales = (select count(distinct a.supplier) from development_co_project.A_Master a, bob_live_co.supplier s where t.yrmonth=a.monthnum and a.supplier=s.name and t.type=s.type and a.orderaftercan=1 and a.ismplace=1) where country='Colombia'; 

update marketplace.sellers_trend t set seller_with_sales = (select count(distinct a.supplier) from development_pe.A_Master a, bob_live_pe.supplier s where t.yrmonth=a.monthnum and a.supplier=s.name and t.type=s.type and a.orderaftercan=1 and a.ismplace=1) where country='Peru'; 

update marketplace.sellers_trend t set seller_with_sales = (select count(distinct a.supplier) from development_ve.A_Master a, bob_live_ve.supplier s where t.yrmonth=a.monthnum and a.supplier=s.name and t.type=s.type and a.orderaftercan=1 and a.ismplace=1) where country='Venezuela'; 

-- Last 4 Weeks

update marketplace.sellers_trend t set seller_with_sales = (select count(distinct a.supplier) from development_mx.A_Master a, bob_live_mx.supplier s where datediff(curdate(), a.date)<=28 and a.supplier=s.name and t.type=s.type and a.orderaftercan=1 and a.ismplace=1) where country='Mexico' and substring(yrmonth, 1, 4)=year(curdate()) and substring(yrmonth, 5, 6)=month(curdate()); 

update marketplace.sellers_trend t set seller_with_sales = (select count(distinct a.supplier) from development_co_project.A_Master a, bob_live_co.supplier s where datediff(curdate(), a.date)<=28 and a.supplier=s.name and t.type=s.type and a.orderaftercan=1 and a.ismplace=1) where country='Colombia' and substring(yrmonth, 1, 4)=year(curdate()) and substring(yrmonth, 5, 6)=month(curdate()); 

update marketplace.sellers_trend t set seller_with_sales = (select count(distinct a.supplier) from development_pe.A_Master a, bob_live_pe.supplier s where datediff(curdate(), a.date)<=28 and a.supplier=s.name and t.type=s.type and a.orderaftercan=1 and a.ismplace=1) where country='Peru' and substring(yrmonth, 1, 4)=year(curdate()) and substring(yrmonth, 5, 6)=month(curdate()); 

update marketplace.sellers_trend t set seller_with_sales = (select count(distinct a.supplier) from development_ve.A_Master a, bob_live_ve.supplier s where datediff(curdate(), a.date)<=28 and a.supplier=s.name and t.type=s.type and a.orderaftercan=1 and a.ismplace=1) where country='Venezuela' and substring(yrmonth, 1, 4)=year(curdate()) and substring(yrmonth, 5, 6)=month(curdate()); 

-- Corrective

update marketplace.sellers_trend t set supplier=seller_with_sales where seller_with_sales>supplier;

-- Month Name

update marketplace.sellers_trend set month_name = case when substr(yrmonth, 5) = 01 then 'January' when substr(yrmonth, 5) = 02 then 'February' when substr(yrmonth, 5) = 03 then 'March' when substr(yrmonth, 5) = 04 then 'April' when substr(yrmonth, 5) = 05 then 'May' when substr(yrmonth, 5) = 06 then 'June' when substr(yrmonth, 5) = 07 then 'July' when substr(yrmonth, 5) = 08 then 'August' when substr(yrmonth, 5) = 09 then 'September' when substr(yrmonth, 5) = 10 then 'October' when substr(yrmonth, 5) = 11 then 'November' when substr(yrmonth, 5) = 12 then 'December' end; 

-- MoM

create table marketplace.sellers_trend_lookup as select * from marketplace.sellers_trend order by country, yrmonth, type;

delete from marketplace.sellers_trend;

insert into marketplace.sellers_trend select * from marketplace.sellers_trend_lookup;

alter table marketplace.sellers_trend drop id_sellers_trend;

alter table marketplace.sellers_trend add id_sellers_trend int auto_increment primary key;

alter table marketplace.sellers_trend AUTO_INCREMENT = 1;

drop table marketplace.sellers_trend_lookup;

update marketplace.sellers_trend set MoM_supplier=0 where yrmonth=201308;

-- Mexico

create table marketplace.temporary_sellers_trend as select id_sellers_trend, yrmonth, supplier from marketplace.sellers_trend where country='Mexico';

create index id_sellers_trend on marketplace.temporary_sellers_trend(id_sellers_trend);

update marketplace.sellers_trend n inner join marketplace.temporary_sellers_trend t on n.id_sellers_trend=t.id_sellers_trend+2 set MoM_supplier = (n.supplier/t.supplier)-1 where country='Mexico';

drop table marketplace.temporary_sellers_trend;

-- Colombia

create table marketplace.temporary_sellers_trend as select id_sellers_trend, yrmonth, supplier from marketplace.sellers_trend where country='Colombia';

create index id_sellers_trend on marketplace.temporary_sellers_trend(id_sellers_trend);

update marketplace.sellers_trend n inner join marketplace.temporary_sellers_trend t on n.id_sellers_trend=t.id_sellers_trend+2 set MoM_supplier = (n.supplier/t.supplier)-1 where country='Colombia';

drop table marketplace.temporary_sellers_trend;

-- Peru

create table marketplace.temporary_sellers_trend as select id_sellers_trend, yrmonth, supplier from marketplace.sellers_trend where country='Peru';

create index id_sellers_trend on marketplace.temporary_sellers_trend(id_sellers_trend);

update marketplace.sellers_trend n inner join marketplace.temporary_sellers_trend t on n.id_sellers_trend=t.id_sellers_trend+2 set MoM_supplier = (n.supplier/t.supplier)-1 where country='Peru';

drop table marketplace.temporary_sellers_trend;

-- Venezuela

create table marketplace.temporary_sellers_trend as select id_sellers_trend, yrmonth, supplier from marketplace.sellers_trend where country='Venezuela';

create index id_sellers_trend on marketplace.temporary_sellers_trend(id_sellers_trend);

update marketplace.sellers_trend n inner join marketplace.temporary_sellers_trend t on n.id_sellers_trend=t.id_sellers_trend+2 set MoM_supplier = (n.supplier/t.supplier)-1 where country='Venezuela';

drop table marketplace.temporary_sellers_trend;

update marketplace.sellers_trend set MoM_supplier = 0 where MoM_supplier is null; 

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `sku_equivalence`()
begin

delete from marketplace.sku_equivalence;

-- Mexico

insert into marketplace.sku_equivalence(country, sku_supplier_source) select distinct 'Mexico', b.sku_supplier_source from sc_live_mx_reporting.catalog_product a inner join bob_live_mx.catalog_source b on b.sku_supplier_source=a.sku_seller;

create table marketplace.temporary_sku_equivalence as select c.sku_supplier_source, s.sku from bob_live_mx.catalog_source c, bob_live_mx.catalog_simple s where s.id_catalog_simple=c.fk_catalog_simple; 

create index sku_supplier_source on marketplace.temporary_sku_equivalence(sku_supplier_source);

update marketplace.sku_equivalence e inner join marketplace.temporary_sku_equivalence t on e.sku_supplier_source=t.sku_supplier_source set e.bob_sku_simple=t.sku where e.country='Mexico';

drop table marketplace.temporary_sku_equivalence;

create table marketplace.temporary_sku_equivalence as select s.sku, c.name from bob_live_mx.catalog_config c, bob_live_mx.catalog_simple s where c.id_catalog_config=s.fk_catalog_config;

create index sku on marketplace.temporary_sku_equivalence(sku);

update marketplace.sku_equivalence e inner join marketplace.temporary_sku_equivalence t on e.bob_sku_simple=t.sku set e.bob_product_name=t.name where e.country='Mexico';

drop table marketplace.temporary_sku_equivalence;

create table marketplace.temporary_sku_equivalence as select sku_simple, sum(in_stock) as stock from operations_mx.out_stock_hist where reserved = 0 and in_stock = 1 group by sku_simple;

create index sku_simple on marketplace.temporary_sku_equivalence(sku_simple);

update marketplace.sku_equivalence e inner join marketplace.temporary_sku_equivalence t on e.bob_sku_simple=t.sku_simple set e.wms_stock=t.stock where e.country='Mexico';

drop table marketplace.temporary_sku_equivalence;

update marketplace.sku_equivalence e inner join sc_live_mx_reporting.catalog_product p on e.sku_supplier_source=p.sku_seller set e.sc_sku_simple=p.sku, e.sc_product_name=p.name where e.country='Mexico';

update marketplace.sku_equivalence e inner join sc_live_mx_reporting.catalog_product p on e.sku_supplier_source=p.sku_seller inner join sc_live_mx_reporting.catalog_stock s on p.id_catalog_product=s.fk_catalog_product set e.sc_stock=s.quantity where e.country='Mexico';

-- Colombia

insert into marketplace.sku_equivalence(country, sku_supplier_source) select distinct 'Colombia', b.sku_supplier_source from sc_live_co_reporting.catalog_product a inner join bob_live_co.catalog_source b on b.sku_supplier_source=a.sku_seller;

create table marketplace.temporary_sku_equivalence as select c.sku_supplier_source, s.sku from bob_live_co.catalog_source c, bob_live_co.catalog_simple s where s.id_catalog_simple=c.fk_catalog_simple; 

create index sku_supplier_source on marketplace.temporary_sku_equivalence(sku_supplier_source);

update marketplace.sku_equivalence e inner join marketplace.temporary_sku_equivalence t on e.sku_supplier_source=t.sku_supplier_source set e.bob_sku_simple=t.sku where e.country='Colombia';

drop table marketplace.temporary_sku_equivalence;

create table marketplace.temporary_sku_equivalence as select s.sku, c.name from bob_live_co.catalog_config c, bob_live_co.catalog_simple s where c.id_catalog_config=s.fk_catalog_config;

create index sku on marketplace.temporary_sku_equivalence(sku);

update marketplace.sku_equivalence e inner join marketplace.temporary_sku_equivalence t on e.bob_sku_simple=t.sku set e.bob_product_name=t.name where e.country='Colombia';

drop table marketplace.temporary_sku_equivalence;

create table marketplace.temporary_sku_equivalence as select sku_simple, sum(in_stock) as stock from operations_co.out_stock_hist where reserved = 0 and in_stock = 1 group by sku_simple;

create index sku_simple on marketplace.temporary_sku_equivalence(sku_simple);

update marketplace.sku_equivalence e inner join marketplace.temporary_sku_equivalence t on e.bob_sku_simple=t.sku_simple set e.wms_stock=t.stock where e.country='Colombia';

drop table marketplace.temporary_sku_equivalence;

update marketplace.sku_equivalence e inner join sc_live_co_reporting.catalog_product p on e.sku_supplier_source=p.sku_seller set e.sc_sku_simple=p.sku, e.sc_product_name=p.name where e.country='Colombia';

update marketplace.sku_equivalence e inner join sc_live_co_reporting.catalog_product p on e.sku_supplier_source=p.sku_seller inner join sc_live_co_reporting.catalog_stock s on p.id_catalog_product=s.fk_catalog_product set e.sc_stock=s.quantity where e.country='Colombia';

-- Peru

insert into marketplace.sku_equivalence(country, sku_supplier_source) select distinct 'Peru', b.sku_supplier_source from sc_live_pe_reporting.catalog_product a inner join bob_live_pe.catalog_source b on b.sku_supplier_source=a.sku_seller;

create table marketplace.temporary_sku_equivalence as select c.sku_supplier_source, s.sku from bob_live_pe.catalog_source c, bob_live_pe.catalog_simple s where s.id_catalog_simple=c.fk_catalog_simple; 

create index sku_supplier_source on marketplace.temporary_sku_equivalence(sku_supplier_source);

update marketplace.sku_equivalence e inner join marketplace.temporary_sku_equivalence t on e.sku_supplier_source=t.sku_supplier_source set e.bob_sku_simple=t.sku where e.country='Peru';

drop table marketplace.temporary_sku_equivalence;

create table marketplace.temporary_sku_equivalence as select s.sku, c.name from bob_live_pe.catalog_config c, bob_live_pe.catalog_simple s where c.id_catalog_config=s.fk_catalog_config;

create index sku on marketplace.temporary_sku_equivalence(sku);

update marketplace.sku_equivalence e inner join marketplace.temporary_sku_equivalence t on e.bob_sku_simple=t.sku set e.bob_product_name=t.name where e.country='Peru';

drop table marketplace.temporary_sku_equivalence;

create table marketplace.temporary_sku_equivalence as select sku_simple, sum(in_stock) as stock from operations_pe.out_stock_hist where reserved = 0 and in_stock = 1 group by sku_simple;

create index sku_simple on marketplace.temporary_sku_equivalence(sku_simple);

update marketplace.sku_equivalence e inner join marketplace.temporary_sku_equivalence t on e.bob_sku_simple=t.sku_simple set e.wms_stock=t.stock where e.country='Peru';

drop table marketplace.temporary_sku_equivalence;

update marketplace.sku_equivalence e inner join sc_live_pe_reporting.catalog_product p on e.sku_supplier_source=p.sku_seller set e.sc_sku_simple=p.sku, e.sc_product_name=p.name where e.country='Peru';

update marketplace.sku_equivalence e inner join sc_live_pe_reporting.catalog_product p on e.sku_supplier_source=p.sku_seller inner join sc_live_pe_reporting.catalog_stock s on p.id_catalog_product=s.fk_catalog_product set e.sc_stock=s.quantity where e.country='Peru';

-- Venezuela

insert into marketplace.sku_equivalence(country, sku_supplier_source) select distinct 'Venezuela', b.sku_supplier_source from sc_live_ve_reporting.catalog_product a inner join bob_live_ve.catalog_source b on b.sku_supplier_source=a.sku_seller;

create table marketplace.temporary_sku_equivalence as select c.sku_supplier_source, s.sku from bob_live_ve.catalog_source c, bob_live_ve.catalog_simple s where s.id_catalog_simple=c.fk_catalog_simple; 

create index sku_supplier_source on marketplace.temporary_sku_equivalence(sku_supplier_source);

update marketplace.sku_equivalence e inner join marketplace.temporary_sku_equivalence t on e.sku_supplier_source=t.sku_supplier_source set e.bob_sku_simple=t.sku where e.country='Venezuela';

drop table marketplace.temporary_sku_equivalence;

create table marketplace.temporary_sku_equivalence as select s.sku, c.name from bob_live_ve.catalog_config c, bob_live_ve.catalog_simple s where c.id_catalog_config=s.fk_catalog_config;

create index sku on marketplace.temporary_sku_equivalence(sku);

update marketplace.sku_equivalence e inner join marketplace.temporary_sku_equivalence t on e.bob_sku_simple=t.sku set e.bob_product_name=t.name where e.country='Venezuela';

drop table marketplace.temporary_sku_equivalence;

create table marketplace.temporary_sku_equivalence as select sku_simple, sum(in_stock) as stock from operations_ve.out_stock_hist where reserved = 0 and in_stock = 1 group by sku_simple;

create index sku_simple on marketplace.temporary_sku_equivalence(sku_simple);

update marketplace.sku_equivalence e inner join marketplace.temporary_sku_equivalence t on e.bob_sku_simple=t.sku_simple set e.wms_stock=t.stock where e.country='Venezuela';

drop table marketplace.temporary_sku_equivalence;

update marketplace.sku_equivalence e inner join sc_live_ve_reporting.catalog_product p on e.sku_supplier_source=p.sku_seller set e.sc_sku_simple=p.sku, e.sc_product_name=p.name where e.country='Venezuela';

update marketplace.sku_equivalence e inner join sc_live_ve_reporting.catalog_product p on e.sku_supplier_source=p.sku_seller inner join sc_live_ve_reporting.catalog_stock s on p.id_catalog_product=s.fk_catalog_product set e.sc_stock=s.quantity where e.country='Venezuela';

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `sku_history`()
begin

##delete from marketplace.sku_history;

-- Mexico

insert into marketplace.sku_history(country, yrmonth, week, date, type, is_marketplace, supplier_created, sku_config_created, sku_simple_created) select 'Mexico', concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate()))), production.week_iso(curdate()), curdate(), s.type, c.isMarketPlace, count(distinct c.supplier), count(distinct c.sku_config), count(distinct c.sku_simple) from development_mx.A_Master_Catalog c, bob_live_mx.supplier s where c.id_supplier=s.id_supplier group by s.type, c.isMarketPlace;

update marketplace.sku_history h set supplier_active = (select count(distinct c.supplier) from development_mx.A_Master_Catalog c, bob_live_mx.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and c.isactive_skuconfig=1) where country = 'Mexico' and date=curdate();

update marketplace.sku_history h set sku_config_active = (select count(distinct c.sku_config) from development_mx.A_Master_Catalog c, bob_live_mx.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and c.isactive_skuconfig=1) where country = 'Mexico' and date=curdate();

update marketplace.sku_history h set sku_simple_active = (select count(distinct c.sku_simple) from development_mx.A_Master_Catalog c, bob_live_mx.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and c.isactive_skusimple=1) where country = 'Mexico' and date=curdate();

update marketplace.sku_history h set supplier_visible = (select count(distinct c.supplier) from development_mx.A_Master_Catalog c, bob_live_mx.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and c.isvisible=1) where country = 'Mexico' and date=curdate();

update marketplace.sku_history h set sku_config_visible = (select count(distinct c.sku_config) from development_mx.A_Master_Catalog c, bob_live_mx.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and c.isvisible=1) where country = 'Mexico' and date=curdate();

update marketplace.sku_history h set sku_simple_visible = (select count(distinct c.sku_simple) from development_mx.A_Master_Catalog c, bob_live_mx.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and c.isvisible=1) where country = 'Mexico' and date=curdate();

-- Colombia

insert into marketplace.sku_history(country, yrmonth, week, date, type, is_marketplace, supplier_created, sku_config_created, sku_simple_created) select 'Colombia', concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate()))), production.week_iso(curdate()), curdate(), s.type, c.isMarketPlace, count(distinct c.supplier), count(distinct c.sku_config), count(distinct c.sku_simple) from development_co_project.A_Master_Catalog c, bob_live_co.supplier s where c.id_supplier=s.id_supplier group by s.type, c.isMarketPlace;

update marketplace.sku_history h set supplier_active = (select count(distinct c.supplier) from development_co_project.A_Master_Catalog c, bob_live_co.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and c.isactive_skuconfig=1) where country = 'Colombia' and date=curdate();

update marketplace.sku_history h set sku_config_active = (select count(distinct c.sku_config) from development_co_project.A_Master_Catalog c, bob_live_co.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and c.isactive_skuconfig=1) where country = 'Colombia' and date=curdate();

update marketplace.sku_history h set sku_simple_active = (select count(distinct c.sku_simple) from development_co_project.A_Master_Catalog c, bob_live_co.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and c.isactive_skusimple=1) where country = 'Colombia' and date=curdate();

update marketplace.sku_history h set supplier_visible = (select count(distinct c.supplier) from development_co_project.A_Master_Catalog c, bob_live_co.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and c.isvisible=1) where country = 'Colombia' and date=curdate();

update marketplace.sku_history h set sku_config_visible = (select count(distinct c.sku_config) from development_co_project.A_Master_Catalog c, bob_live_co.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and c.isvisible=1) where country = 'Colombia' and date=curdate();

update marketplace.sku_history h set sku_simple_visible = (select count(distinct c.sku_simple) from development_co_project.A_Master_Catalog c, bob_live_co.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and c.isvisible=1) where country = 'Colombia' and date=curdate();

-- Peru

insert into marketplace.sku_history(country, yrmonth, week, date, type, is_marketplace, supplier_created, sku_config_created, sku_simple_created) select 'Peru', concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate()))), production.week_iso(curdate()), curdate(), s.type, c.isMarketPlace, count(distinct c.supplier), count(distinct c.sku_config), count(distinct c.sku_simple) from development_pe.A_Master_Catalog c, bob_live_pe.supplier s where c.id_supplier=s.id_supplier group by s.type, c.isMarketPlace;

update marketplace.sku_history h set supplier_active = (select count(distinct c.supplier) from development_pe.A_Master_Catalog c, bob_live_pe.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and c.isactive_skuconfig=1) where country = 'Peru' and date=curdate();

update marketplace.sku_history h set sku_config_active = (select count(distinct c.sku_config) from development_pe.A_Master_Catalog c, bob_live_pe.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and c.isactive_skuconfig=1) where country = 'Peru' and date=curdate();

update marketplace.sku_history h set sku_simple_active = (select count(distinct c.sku_simple) from development_pe.A_Master_Catalog c, bob_live_pe.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and c.isactive_skusimple=1) where country = 'Peru' and date=curdate();

update marketplace.sku_history h set supplier_visible = (select count(distinct c.supplier) from development_pe.A_Master_Catalog c, bob_live_pe.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and c.isvisible=1) where country = 'Peru' and date=curdate();

update marketplace.sku_history h set sku_config_visible = (select count(distinct c.sku_config) from development_pe.A_Master_Catalog c, bob_live_pe.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and c.isvisible=1) where country = 'Peru' and date=curdate();

update marketplace.sku_history h set sku_simple_visible = (select count(distinct c.sku_simple) from development_pe.A_Master_Catalog c, bob_live_pe.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and c.isvisible=1) where country = 'Peru' and date=curdate();

-- Venezuela

insert into marketplace.sku_history(country, yrmonth, week, date, type, is_marketplace, supplier_created, sku_config_created, sku_simple_created) select 'Venezuela', concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate()))), production.week_iso(curdate()), curdate(), s.type, c.isMarketPlace, count(distinct c.supplier), count(distinct c.sku_config), count(distinct c.sku_simple) from development_ve.A_Master_Catalog c, bob_live_ve.supplier s where c.id_supplier=s.id_supplier group by s.type, c.isMarketPlace;

update marketplace.sku_history h set supplier_active = (select count(distinct c.supplier) from development_ve.A_Master_Catalog c, bob_live_ve.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and c.isactive_skuconfig=1) where country = 'Venezuela' and date=curdate();

update marketplace.sku_history h set sku_config_active = (select count(distinct c.sku_config) from development_ve.A_Master_Catalog c, bob_live_ve.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and c.isactive_skuconfig=1) where country = 'Venezuela' and date=curdate();

update marketplace.sku_history h set sku_simple_active = (select count(distinct c.sku_simple) from development_ve.A_Master_Catalog c, bob_live_ve.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and c.isactive_skusimple=1) where country = 'Venezuela' and date=curdate();

update marketplace.sku_history h set supplier_visible = (select count(distinct c.supplier) from development_ve.A_Master_Catalog c, bob_live_ve.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and c.isvisible=1) where country = 'Venezuela' and date=curdate();

update marketplace.sku_history h set sku_config_visible = (select count(distinct c.sku_config) from development_ve.A_Master_Catalog c, bob_live_ve.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and c.isvisible=1) where country = 'Venezuela' and date=curdate();

update marketplace.sku_history h set sku_simple_visible = (select count(distinct c.sku_simple) from development_ve.A_Master_Catalog c, bob_live_ve.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and c.isvisible=1) where country = 'Venezuela' and date=curdate();

delete from marketplace.sku_visible;

insert into marketplace.sku_visible select * from marketplace.sku_history s where date in (select max(date) from marketplace.sku_history t where t.yrmonth=s.yrmonth) group by country, yrmonth, type, is_marketplace;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `sku_history_cat_bp`()
begin

##delete from marketplace.sku_history_cat_bp;

-- Mexico

insert into marketplace.sku_history_cat_bp(country, yrmonth, week, date, type, is_marketplace, cat_bp, supplier_created, sku_config_created, sku_simple_created) select 'Mexico', concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate()))), production.week_iso(curdate()), curdate(), s.type, c.isMarketPlace, c.cat_bp, count(distinct c.supplier), count(distinct c.sku_config), count(distinct c.sku_simple) from development_mx.A_Master_Catalog c, bob_live_mx.supplier s where c.id_supplier=s.id_supplier group by s.type, c.isMarketPlace, c.cat_bp;

update marketplace.sku_history_cat_bp h set supplier_active = (select count(distinct c.supplier) from development_mx.A_Master_Catalog c, bob_live_mx.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and h.cat_bp=c.cat_bp and c.isactive_skuconfig=1) where country = 'Mexico' and date=curdate();

update marketplace.sku_history_cat_bp h set sku_config_active = (select count(distinct c.sku_config) from development_mx.A_Master_Catalog c, bob_live_mx.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and h.cat_bp=c.cat_bp and c.isactive_skuconfig=1) where country = 'Mexico' and date=curdate();

update marketplace.sku_history_cat_bp h set sku_simple_active = (select count(distinct c.sku_simple) from development_mx.A_Master_Catalog c, bob_live_mx.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and h.cat_bp=c.cat_bp and c.isactive_skusimple=1) where country = 'Mexico' and date=curdate();

update marketplace.sku_history_cat_bp h set supplier_visible = (select count(distinct c.supplier) from development_mx.A_Master_Catalog c, bob_live_mx.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and h.cat_bp=c.cat_bp and c.isvisible=1) where country = 'Mexico' and date=curdate();

update marketplace.sku_history_cat_bp h set sku_config_visible = (select count(distinct c.sku_config) from development_mx.A_Master_Catalog c, bob_live_mx.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and h.cat_bp=c.cat_bp and c.isvisible=1) where country = 'Mexico' and date=curdate();

update marketplace.sku_history_cat_bp h set sku_simple_visible = (select count(distinct c.sku_simple) from development_mx.A_Master_Catalog c, bob_live_mx.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and h.cat_bp=c.cat_bp and c.isvisible=1) where country = 'Mexico' and date=curdate();

-- Colombia

insert into marketplace.sku_history_cat_bp(country, yrmonth, week, date, type, is_marketplace, cat_bp, supplier_created, sku_config_created, sku_simple_created) select 'Colombia', concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate()))), production.week_iso(curdate()), curdate(), s.type, c.isMarketPlace, c.cat_bp, count(distinct c.supplier), count(distinct c.sku_config), count(distinct c.sku_simple) from development_co_project.A_Master_Catalog c, bob_live_co.supplier s where c.id_supplier=s.id_supplier group by s.type, c.isMarketPlace, c.cat_bp;

update marketplace.sku_history_cat_bp h set supplier_active = (select count(distinct c.supplier) from development_co_project.A_Master_Catalog c, bob_live_co.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and h.cat_bp=c.cat_bp and c.isactive_skuconfig=1) where country = 'Colombia' and date=curdate();

update marketplace.sku_history_cat_bp h set sku_config_active = (select count(distinct c.sku_config) from development_co_project.A_Master_Catalog c, bob_live_co.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and h.cat_bp=c.cat_bp and c.isactive_skuconfig=1) where country = 'Colombia' and date=curdate();

update marketplace.sku_history_cat_bp h set sku_simple_active = (select count(distinct c.sku_simple) from development_co_project.A_Master_Catalog c, bob_live_co.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and h.cat_bp=c.cat_bp and c.isactive_skusimple=1) where country = 'Colombia' and date=curdate();

update marketplace.sku_history_cat_bp h set supplier_visible = (select count(distinct c.supplier) from development_co_project.A_Master_Catalog c, bob_live_co.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and h.cat_bp=c.cat_bp and c.isvisible=1) where country = 'Colombia' and date=curdate();

update marketplace.sku_history_cat_bp h set sku_config_visible = (select count(distinct c.sku_config) from development_co_project.A_Master_Catalog c, bob_live_co.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and h.cat_bp=c.cat_bp and c.isvisible=1) where country = 'Colombia' and date=curdate();

update marketplace.sku_history_cat_bp h set sku_simple_visible = (select count(distinct c.sku_simple) from development_co_project.A_Master_Catalog c, bob_live_co.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and h.cat_bp=c.cat_bp and c.isvisible=1) where country = 'Colombia' and date=curdate();

-- Peru

insert into marketplace.sku_history_cat_bp(country, yrmonth, week, date, type, is_marketplace, cat_bp, supplier_created, sku_config_created, sku_simple_created) select 'Peru', concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate()))), production.week_iso(curdate()), curdate(), s.type, c.isMarketPlace, c.cat_bp, count(distinct c.supplier), count(distinct c.sku_config), count(distinct c.sku_simple) from development_pe.A_Master_Catalog c, bob_live_pe.supplier s where c.id_supplier=s.id_supplier group by s.type, c.isMarketPlace, c.cat_bp;

update marketplace.sku_history_cat_bp h set supplier_active = (select count(distinct c.supplier) from development_pe.A_Master_Catalog c, bob_live_pe.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and h.cat_bp=c.cat_bp and c.isactive_skuconfig=1) where country = 'Peru' and date=curdate();

update marketplace.sku_history_cat_bp h set sku_config_active = (select count(distinct c.sku_config) from development_pe.A_Master_Catalog c, bob_live_pe.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and h.cat_bp=c.cat_bp  and c.isactive_skuconfig=1) where country = 'Peru' and date=curdate();

update marketplace.sku_history_cat_bp h set sku_simple_active = (select count(distinct c.sku_simple) from development_pe.A_Master_Catalog c, bob_live_pe.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and h.cat_bp=c.cat_bp and c.isactive_skusimple=1) where country = 'Peru' and date=curdate();

update marketplace.sku_history_cat_bp h set supplier_visible = (select count(distinct c.supplier) from development_pe.A_Master_Catalog c, bob_live_pe.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and h.cat_bp=c.cat_bp and c.isvisible=1) where country = 'Peru' and date=curdate();

update marketplace.sku_history_cat_bp h set sku_config_visible = (select count(distinct c.sku_config) from development_pe.A_Master_Catalog c, bob_live_pe.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and h.cat_bp=c.cat_bp and c.isvisible=1) where country = 'Peru' and date=curdate();

update marketplace.sku_history_cat_bp h set sku_simple_visible = (select count(distinct c.sku_simple) from development_pe.A_Master_Catalog c, bob_live_pe.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and h.cat_bp=c.cat_bp and c.isvisible=1) where country = 'Peru' and date=curdate();

-- Venezuela

insert into marketplace.sku_history_cat_bp(country, yrmonth, week, date, type, is_marketplace, cat_bp, supplier_created, sku_config_created, sku_simple_created) select 'Venezuela', concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate()))), production.week_iso(curdate()), curdate(), s.type, c.isMarketPlace, c.cat_bp, count(distinct c.supplier), count(distinct c.sku_config), count(distinct c.sku_simple) from development_ve.A_Master_Catalog c, bob_live_ve.supplier s where c.id_supplier=s.id_supplier group by s.type, c.isMarketPlace, c.cat_bp;

update marketplace.sku_history_cat_bp h set supplier_active = (select count(distinct c.supplier) from development_ve.A_Master_Catalog c, bob_live_ve.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and h.cat_bp=c.cat_bp and c.isactive_skuconfig=1) where country = 'Venezuela' and date=curdate();

update marketplace.sku_history_cat_bp h set sku_config_active = (select count(distinct c.sku_config) from development_ve.A_Master_Catalog c, bob_live_ve.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and h.cat_bp=c.cat_bp and c.isactive_skuconfig=1) where country = 'Venezuela' and date=curdate();

update marketplace.sku_history_cat_bp h set sku_simple_active = (select count(distinct c.sku_simple) from development_ve.A_Master_Catalog c, bob_live_ve.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and h.cat_bp=c.cat_bp and c.isactive_skusimple=1) where country = 'Venezuela' and date=curdate();

update marketplace.sku_history_cat_bp h set supplier_visible = (select count(distinct c.supplier) from development_ve.A_Master_Catalog c, bob_live_ve.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and h.cat_bp=c.cat_bp and c.isvisible=1) where country = 'Venezuela' and date=curdate();

update marketplace.sku_history_cat_bp h set sku_config_visible = (select count(distinct c.sku_config) from development_ve.A_Master_Catalog c, bob_live_ve.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and h.cat_bp=c.cat_bp and c.isvisible=1) where country = 'Venezuela' and date=curdate();

update marketplace.sku_history_cat_bp h set sku_simple_visible = (select count(distinct c.sku_simple) from development_ve.A_Master_Catalog c, bob_live_ve.supplier s where c.id_supplier=s.id_supplier and h.type=s.type and h.is_marketplace=c.isMarketPlace and h.cat_bp=c.cat_bp and c.isvisible=1) where country = 'Venezuela' and date=curdate();

delete from marketplace.sku_visible_cat_bp;

insert into marketplace.sku_visible_cat_bp select * from marketplace.sku_history_cat_bp s where date in (select max(date) from marketplace.sku_history_cat_bp t where t.yrmonth=s.yrmonth) group by country, yrmonth, type, is_marketplace, cat_bp;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `sku_marketplace`()
begin

delete from marketplace.sku_marketplace;

/*
create table marketplace.temporary_sku_marketplace(
id_supplier int,
cat_temporary varchar(255),
cat1 int,
cat2 int,
cat3 int
);

create index cat_temporary on marketplace.temporary_sku_marketplace(cat_temporary);

insert into marketplace.temporary_sku_marketplace(id_supplier, cat_temporary) select distinct b.src_id, c.name as cat_temporary from sc_live_mx_reporting.seller_commission a inner join sc_live_mx_reporting.seller b on a.fk_seller=b.id_seller inner join sc_live_mx_reporting.catalog_category as c on a.fk_catalog_category=c.id_catalog_category;

update marketplace.temporary_sku_marketplace t inner join marketplace.category_tree c on t.cat_temporary=c.cat1 set t.cat1=1;

update marketplace.temporary_sku_marketplace t inner join marketplace.category_tree c on t.cat_temporary=c.cat2 set t.cat2=1;

update marketplace.temporary_sku_marketplace t inner join marketplace.category_tree c on t.cat_temporary=c.cat3 set t.cat3=1;

update marketplace.temporary_sku_marketplace t set cat2=null where cat1 is not null;

update marketplace.temporary_sku_marketplace t set cat3=null where cat1 is not null;

update marketplace.temporary_sku_marketplace t set cat3=null where cat2 is not null;

insert into marketplace.sku_marketplace(country, id_supplier, sc_supplier_name, cat1, fee, type) select distinct 'Mexico', b.src_id, b.name, c.name, a.percentage/100, 'seller center' from sc_live_mx_reporting.seller_commission a inner join sc_live_mx_reporting.seller b on a.fk_seller=b.id_seller inner join sc_live_mx_reporting.catalog_category as c on a.fk_catalog_category=c.id_catalog_category inner join marketplace.temporary_sku_marketplace t on t.id_supplier=b.src_id and t.cat_temporary=c.name where t.cat1=1;

insert into marketplace.sku_marketplace(country, id_supplier, sc_supplier_name, cat2, fee, type) select distinct 'Mexico', b.src_id, b.name, c.name, a.percentage/100, 'seller center' from sc_live_mx_reporting.seller_commission a inner join sc_live_mx_reporting.seller b on a.fk_seller=b.id_seller inner join sc_live_mx_reporting.catalog_category as c on a.fk_catalog_category=c.id_catalog_category inner join marketplace.temporary_sku_marketplace t on t.id_supplier=b.src_id and t.cat_temporary=c.name where t.cat2=1;

insert into marketplace.sku_marketplace(country, id_supplier, sc_supplier_name, cat3, fee, type) select distinct 'Mexico', b.src_id, b.name, c.name, a.percentage/100, 'seller center' from sc_live_mx_reporting.seller_commission a inner join sc_live_mx_reporting.seller b on a.fk_seller=b.id_seller inner join sc_live_mx_reporting.catalog_category as c on a.fk_catalog_category=c.id_catalog_category inner join marketplace.temporary_sku_marketplace t on t.id_supplier=b.src_id and t.cat_temporary=c.name where t.cat3=1;

insert into marketplace.sku_marketplace(country, id_supplier, sc_supplier_name, fee, type) select distinct 'Mexico', b.src_id, b.name, a.percentage/100, 'seller center' from sc_live_mx_reporting.seller_commission a inner join sc_live_mx_reporting.seller b on a.fk_seller=b.id_seller inner join sc_live_mx_reporting.catalog_category as c on a.fk_catalog_category=c.id_catalog_category inner join marketplace.temporary_sku_marketplace t on t.id_supplier=b.src_id and t.cat_temporary=c.name where t.cat1 is null and t.cat2 is null and t.cat3 is null;

drop table marketplace.temporary_sku_marketplace;*/

/*
create table marketplace.temporary_sku_marketplace as select distinct cat_bp, cat1, cat2, cat3 from development_mx.A_Master_Catalog;

create index cat1 on marketplace.temporary_sku_marketplace(cat1); 
create index cat2 on marketplace.temporary_sku_marketplace(cat2);
create index cat3 on marketplace.temporary_sku_marketplace(cat3);

update marketplace.sku_marketplace m inner join marketplace.temporary_sku_marketplace t on m.cat2=t.cat2 set m.cat1=t.cat1;

update marketplace.sku_marketplace m inner join marketplace.temporary_sku_marketplace t on m.cat3=t.cat3 set m.cat1=t.cat1, m.cat2=t.cat2; 

update marketplace.sku_marketplace m inner join marketplace.temporary_sku_marketplace t on m.cat1=t.cat1 set m.cat_bp=t.cat_bp;

update marketplace.sku_marketplace m inner join marketplace.temporary_sku_marketplace t on m.cat2=t.cat2 set m.cat_bp=t.cat_bp;

update marketplace.sku_marketplace m inner join marketplace.temporary_sku_marketplace t on m.cat1=t.cat1 and m.cat2=t.cat2 set m.cat_bp=t.cat_bp; 

drop table marketplace.temporary_sku_marketplace;*/

insert into marketplace.sku_marketplace(country, id_supplier, sc_supplier_name, id_catalog_category, fee, type) select distinct 'Mexico', b.src_id, b.name, c.id_catalog_category, a.percentage/100, 'seller center' from sc_live_mx_reporting.seller_commission a inner join sc_live_mx_reporting.seller b on a.fk_seller=b.id_seller inner join sc_live_mx_reporting.catalog_category as c on a.fk_catalog_category=c.id_catalog_category;

update marketplace.sku_marketplace m inner join marketplace.sc_to_bob_category t on m.id_catalog_category=t.id_catalog_category set m.cat_bp=t.cat_bp, m.cat1=t.cat1, m.cat2=t.cat2;

update marketplace.sku_marketplace m inner join bob_live_mx.supplier s on m.id_supplier=s.id_supplier set m.bob_supplier_name=s.name where country='Mexico';

create table marketplace.temporary_sku_marketplace as select distinct id_supplier from marketplace.sku_marketplace;

create index id_supplier on marketplace.temporary_sku_marketplace(id_supplier);

insert into marketplace.sku_marketplace(country, id_supplier, sc_supplier_name, bob_supplier_name, cat_bp, cat1, cat2, cat3, fee, type, sku_simple, since) select distinct 'Mexico', o.id_supplier, o.supplier, o.bob_supplier_name, o.category, o.subcategory, o.subsubcategory, o.subsubsubcategory, o.fee, 'bob', o.persku, o.since from marketplace.old_sku_marketplace_mx o where o.id_supplier not in (select t.id_supplier from marketplace.temporary_sku_marketplace t); 

drop table marketplace.temporary_sku_marketplace;

update marketplace.sku_marketplace set cat1='' where cat1 = 'Root Category';

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `supplier_fulfillment`()
begin
/*
drop table if exists marketplace.supplier_fulfillment;

create table marketplace.supplier_fulfillment(
country varchar(255),
supplier varchar(255),
sku_simple varchar(255),
product_name varchar(255),
cat1 varchar(255),
cat2 varchar(255),
fulfillment_type_real varchar(255),
fulfillment_type_actual varchar(255),
fulfillment_type_duplicate int,
package_height float,
package_width float,
package_length float,
package_weight float,
package_measure varchar(255),
is_marketplace int,
distinct_sku_simple float
);

create index supplier on marketplace.supplier_fulfillment(supplier);
create index sku_simple on marketplace.supplier_fulfillment(sku_simple);*/

delete from marketplace.supplier_fulfillment;

-- Mexico

insert into marketplace.supplier_fulfillment(country, sku_simple, product_name, fulfillment_type_real) select 'Mexico', o.sku_simple, o.sku_name, o.fulfillment_type_real from operations_mx.out_order_tracking o, development_mx.A_Master_Catalog c where o.sku_simple=c.sku_simple and c.isactive_skuconfig=1 group by o.sku_simple, o.fulfillment_type_real;

update marketplace.supplier_fulfillment s inner join operations_mx.out_order_tracking o on s.sku_simple=o.sku_simple set s.cat1=o.cat_1, s.cat2=o.cat_2 where s.country='Mexico';

update marketplace.supplier_fulfillment s inner join development_mx.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.cat1=c.cat1 where (s.cat1 is null or s.cat1 = '') and s.country='Mexico'; 

update marketplace.supplier_fulfillment s inner join development_mx.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.cat2=c.cat2 where (s.cat2 is null or s.cat2 = '') and s.country='Mexico'; 

update marketplace.supplier_fulfillment s inner join development_mx.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.package_height=c.package_height, s.package_width=c.package_width, s.package_length=c.package_length, s.package_weight=c.package_weight where s.country='Mexico'; 

update marketplace.supplier_fulfillment s inner join development_mx.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.supplier=c.supplier, s.is_marketplace=c.isMarketPlace where s.country='Mexico'; 

update marketplace.supplier_fulfillment s inner join development_mx.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.fulfillment_type_real=c.fulfillment_type where (s.fulfillment_type_real is null or s.fulfillment_type_real = '') and s.country='Mexico';

update marketplace.supplier_fulfillment s inner join development_mx.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.fulfillment_type_actual=c.fulfillment_type where s.country='Mexico';

update marketplace.supplier_fulfillment s inner join development_mx.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.package_height=c.Package_Height where s.package_height is null and s.country='Mexico';

-- Inserting Missing

create table marketplace.temporary_supplier_fulfillment select supplier, sku_simple, sku_name, cat1, cat2, fulfillment_type, package_height, package_width, package_length, ismarketplace, 0 as flag from development_mx.A_Master_Catalog where isactive_skuconfig=1 group by sku_simple;

create index sku_simple on marketplace.temporary_supplier_fulfillment(sku_simple);

update marketplace.temporary_supplier_fulfillment t inner join marketplace.supplier_fulfillment s on t.sku_simple=s.sku_simple set t.flag=1 where country='Mexico';

insert into marketplace.supplier_fulfillment(country, supplier, sku_simple, product_name, cat1, cat2, fulfillment_type_real, fulfillment_type_actual, package_height, package_width, package_length, is_marketplace) select 'Mexico', supplier, sku_simple, sku_name, cat1, cat2, fulfillment_type, fulfillment_type,  package_height, package_width, package_length, ismarketplace from marketplace.temporary_supplier_fulfillment where flag=0 group by sku_simple;

drop table marketplace.temporary_supplier_fulfillment;

--

alter ignore table marketplace.supplier_fulfillment add primary key (sku_simple, fulfillment_type_real);

create table marketplace.temporary_supplier_fulfillment as select sku_simple, count(*) as count from marketplace.supplier_fulfillment where country='Mexico' group by sku_simple;

create index sku_simple on marketplace.temporary_supplier_fulfillment(sku_simple);

update marketplace.supplier_fulfillment s inner join marketplace.temporary_supplier_fulfillment t on s.sku_simple=t.sku_simple set s.fulfillment_type_duplicate=1 where count>1 and country='Mexico';

update marketplace.supplier_fulfillment s set s.fulfillment_type_duplicate=0 where fulfillment_type_duplicate is null and country='Mexico';

drop table marketplace.temporary_supplier_fulfillment;

create table marketplace.temporary_supplier_fulfillment as select cat1, avg(package_height) as package_height from marketplace.supplier_fulfillment where country='Mexico' group by cat1;

create index sku_simple on marketplace.temporary_supplier_fulfillment(cat1);

update marketplace.supplier_fulfillment s inner join marketplace.temporary_supplier_fulfillment t on s.cat1=t.cat1 set s.package_height=t.package_height where s.package_height is null and country='Mexico'; 

drop table marketplace.temporary_supplier_fulfillment;

alter table marketplace.supplier_fulfillment drop primary key;

-- Colombia

insert into marketplace.supplier_fulfillment(country, sku_simple, product_name, fulfillment_type_real) select 'Colombia', o.sku_simple, o.sku_name, o.fulfillment_type_real from operations_co.out_order_tracking o, development_co_project.A_Master_Catalog c where o.sku_simple=c.sku_simple and c.isactive_skuconfig=1 group by o.sku_simple, o.fulfillment_type_real;

update marketplace.supplier_fulfillment s inner join operations_co.out_order_tracking o on s.sku_simple=o.sku_simple set s.cat1=o.cat_1, s.cat2=o.cat_2 where s.country='Colombia';

update marketplace.supplier_fulfillment s inner join development_co_project.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.cat1=c.cat1 where (s.cat1 is null or s.cat1 = '') and s.country='Colombia'; 

update marketplace.supplier_fulfillment s inner join development_co_project.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.cat2=c.cat2 where (s.cat2 is null or s.cat2 = '') and s.country='Colombia'; 

update marketplace.supplier_fulfillment s inner join development_co_project.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.package_height=c.package_height, s.package_width=c.package_width, s.package_length=c.package_length, s.package_weight=c.package_weight where s.country='Colombia';

update marketplace.supplier_fulfillment s inner join development_co_project.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.supplier=c.supplier, s.is_marketplace=c.isMarketPlace where s.country='Colombia'; 

update marketplace.supplier_fulfillment s inner join development_co_project.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.fulfillment_type_real=c.fulfillment_type where (s.fulfillment_type_real is null or s.fulfillment_type_real = '') and s.country='Colombia';

update marketplace.supplier_fulfillment s inner join development_co_project.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.fulfillment_type_actual=c.fulfillment_type where s.country='Colombia';

update marketplace.supplier_fulfillment s inner join development_co_project.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.package_height=c.Package_Height where s.package_height is null and s.country='Colombia';

-- Inserting Missing

create table marketplace.temporary_supplier_fulfillment select supplier, sku_simple, sku_name, cat1, cat2, fulfillment_type, package_height, package_width, package_length, ismarketplace, 0 as flag from development_co_project.A_Master_Catalog where isactive_skuconfig=1 group by sku_simple;

create index sku_simple on marketplace.temporary_supplier_fulfillment(sku_simple);

update marketplace.temporary_supplier_fulfillment t inner join marketplace.supplier_fulfillment s on t.sku_simple=s.sku_simple set t.flag=1 where country='Colombia';

insert into marketplace.supplier_fulfillment(country, supplier, sku_simple, product_name, cat1, cat2, fulfillment_type_real, fulfillment_type_actual, package_height, package_width, package_length, is_marketplace) select 'Colombia', supplier, sku_simple, sku_name, cat1, cat2, fulfillment_type, fulfillment_type,  package_height, package_width, package_length, ismarketplace from marketplace.temporary_supplier_fulfillment where flag=0 group by sku_simple;

drop table marketplace.temporary_supplier_fulfillment;

--

alter ignore table marketplace.supplier_fulfillment add primary key (sku_simple, fulfillment_type_real);

create table marketplace.temporary_supplier_fulfillment as select sku_simple, count(*) as count from marketplace.supplier_fulfillment where country='Colombia' group by sku_simple;

create index sku_simple on marketplace.temporary_supplier_fulfillment(sku_simple);

update marketplace.supplier_fulfillment s inner join marketplace.temporary_supplier_fulfillment t on s.sku_simple=t.sku_simple set s.fulfillment_type_duplicate=1 where count>1 and country='Colombia';

update marketplace.supplier_fulfillment s set s.fulfillment_type_duplicate=0 where fulfillment_type_duplicate is null and country='Colombia';

drop table marketplace.temporary_supplier_fulfillment;

create table marketplace.temporary_supplier_fulfillment as select cat1, avg(package_height) as package_height from marketplace.supplier_fulfillment where country='Colombia' group by cat1;

create index sku_simple on marketplace.temporary_supplier_fulfillment(cat1);

update marketplace.supplier_fulfillment s inner join marketplace.temporary_supplier_fulfillment t on s.cat1=t.cat1 set s.package_height=t.package_height where s.package_height is null and country='Colombia'; 

drop table marketplace.temporary_supplier_fulfillment;

alter table marketplace.supplier_fulfillment drop primary key;

-- Peru

insert into marketplace.supplier_fulfillment(country, sku_simple, product_name, fulfillment_type_real) select 'Peru', o.sku_simple, o.sku_name, o.fulfillment_type_real from operations_pe.out_order_tracking o, development_pe.A_Master_Catalog c where o.sku_simple=c.sku_simple and c.isactive_skuconfig=1 group by o.sku_simple, o.fulfillment_type_real;

update marketplace.supplier_fulfillment s inner join operations_pe.out_order_tracking o on s.sku_simple=o.sku_simple set s.cat1=o.cat_1, s.cat2=o.cat_2 where s.country='Peru';

update marketplace.supplier_fulfillment s inner join development_pe.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.cat1=c.cat1 where (s.cat1 is null or s.cat1 = '') and s.country='Peru'; 

update marketplace.supplier_fulfillment s inner join development_pe.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.cat2=c.cat2 where (s.cat2 is null or s.cat2 = '') and s.country='Peru'; 

update marketplace.supplier_fulfillment s inner join development_pe.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.package_height=c.package_height, s.package_width=c.package_width, s.package_length=c.package_length, s.package_weight=c.package_weight where s.country='Peru';

update marketplace.supplier_fulfillment s inner join development_pe.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.supplier=c.supplier, s.is_marketplace=c.isMarketPlace where s.country='Peru'; 

update marketplace.supplier_fulfillment s inner join development_pe.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.fulfillment_type_real=c.fulfillment_type where (s.fulfillment_type_real is null or s.fulfillment_type_real = '') and s.country='Peru';

update marketplace.supplier_fulfillment s inner join development_pe.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.fulfillment_type_actual=c.fulfillment_type where s.country='Peru';

update marketplace.supplier_fulfillment s inner join development_pe.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.package_height=c.Package_Height where s.package_height is null and s.country='Peru';

-- Inserting Missing

create table marketplace.temporary_supplier_fulfillment select supplier, sku_simple, sku_name, cat1, cat2, fulfillment_type, package_height, package_width, package_length, ismarketplace, 0 as flag from development_pe.A_Master_Catalog where isactive_skuconfig=1 group by sku_simple;

create index sku_simple on marketplace.temporary_supplier_fulfillment(sku_simple);

update marketplace.temporary_supplier_fulfillment t inner join marketplace.supplier_fulfillment s on t.sku_simple=s.sku_simple set t.flag=1 where country='Peru';

insert into marketplace.supplier_fulfillment(country, supplier, sku_simple, product_name, cat1, cat2, fulfillment_type_real, fulfillment_type_actual, package_height, package_width, package_length, is_marketplace) select 'Peru', supplier, sku_simple, sku_name, cat1, cat2, fulfillment_type, fulfillment_type,  package_height, package_width, package_length, ismarketplace from marketplace.temporary_supplier_fulfillment where flag=0 group by sku_simple;

drop table marketplace.temporary_supplier_fulfillment;

--

alter ignore table marketplace.supplier_fulfillment add primary key (sku_simple, fulfillment_type_real);

create table marketplace.temporary_supplier_fulfillment as select sku_simple, count(*) as count from marketplace.supplier_fulfillment where country='Peru' group by sku_simple;

create index sku_simple on marketplace.temporary_supplier_fulfillment(sku_simple);

update marketplace.supplier_fulfillment s inner join marketplace.temporary_supplier_fulfillment t on s.sku_simple=t.sku_simple set s.fulfillment_type_duplicate=1 where count>1 and country='Peru';

update marketplace.supplier_fulfillment s set s.fulfillment_type_duplicate=0 where fulfillment_type_duplicate is null and country='Peru';

drop table marketplace.temporary_supplier_fulfillment;

create table marketplace.temporary_supplier_fulfillment as select cat1, avg(package_height) as package_height from marketplace.supplier_fulfillment where country='Peru' group by cat1;

create index sku_simple on marketplace.temporary_supplier_fulfillment(cat1);

update marketplace.supplier_fulfillment s inner join marketplace.temporary_supplier_fulfillment t on s.cat1=t.cat1 set s.package_height=t.package_height where s.package_height is null and country='Peru'; 

drop table marketplace.temporary_supplier_fulfillment;

alter table marketplace.supplier_fulfillment drop primary key;

-- Venezuela

insert into marketplace.supplier_fulfillment(country, sku_simple, product_name, fulfillment_type_real) select 'Venezuela', o.sku_simple, o.sku_name, o.fulfillment_type_real from operations_ve.out_order_tracking o, development_ve.A_Master_Catalog c where o.sku_simple=c.sku_simple and c.isactive_skuconfig=1 group by o.sku_simple, o.fulfillment_type_real;

update marketplace.supplier_fulfillment s inner join operations_ve.out_order_tracking o on s.sku_simple=o.sku_simple set s.cat1=o.cat_1, s.cat2=o.cat_2 where country='Venezuela';

update marketplace.supplier_fulfillment s inner join development_ve.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.cat1=c.cat1 where (s.cat1 is null or s.cat1 = '') and s.country='Venezuela'; 

update marketplace.supplier_fulfillment s inner join development_ve.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.cat2=c.cat2 where (s.cat2 is null or s.cat2 = '') and s.country='Venezuela'; 

update marketplace.supplier_fulfillment s inner join development_ve.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.package_height=c.package_height, s.package_width=c.package_width, s.package_length=c.package_length, s.package_weight=c.package_weight where s.country='Venezuela';

update marketplace.supplier_fulfillment s inner join development_ve.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.supplier=c.supplier, s.is_marketplace=c.isMarketPlace where s.country='Venezuela'; 

update marketplace.supplier_fulfillment s inner join development_ve.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.fulfillment_type_real=c.fulfillment_type where (s.fulfillment_type_real is null or s.fulfillment_type_real = '') and s.country='Venezuela';

update marketplace.supplier_fulfillment s inner join development_ve.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.fulfillment_type_actual=c.fulfillment_type where s.country='Venezuela';

update marketplace.supplier_fulfillment s inner join development_ve.A_Master_Catalog c on s.sku_simple=c.sku_simple set s.package_height=c.Package_Height where s.package_height is null and s.country='Venezuela';

-- Inserting Missing

create table marketplace.temporary_supplier_fulfillment select supplier, sku_simple, sku_name, cat1, cat2, fulfillment_type, package_height, package_width, package_length, ismarketplace, 0 as flag from development_ve.A_Master_Catalog where isactive_skuconfig=1 group by sku_simple;

create index sku_simple on marketplace.temporary_supplier_fulfillment(sku_simple);

update marketplace.temporary_supplier_fulfillment t inner join marketplace.supplier_fulfillment s on t.sku_simple=s.sku_simple set t.flag=1 where country='Venezuela';

insert into marketplace.supplier_fulfillment(country, supplier, sku_simple, product_name, cat1, cat2, fulfillment_type_real, fulfillment_type_actual, package_height, package_width, package_length, is_marketplace) select 'Venezuela', supplier, sku_simple, sku_name, cat1, cat2, fulfillment_type, fulfillment_type,  package_height, package_width, package_length, ismarketplace from marketplace.temporary_supplier_fulfillment where flag=0 group by sku_simple;

drop table marketplace.temporary_supplier_fulfillment;

--

alter ignore table marketplace.supplier_fulfillment add primary key (sku_simple, fulfillment_type_real);

create table marketplace.temporary_supplier_fulfillment as select sku_simple, count(*) as count from marketplace.supplier_fulfillment where country='Venezuela' group by sku_simple;

create index sku_simple on marketplace.temporary_supplier_fulfillment(sku_simple);

update marketplace.supplier_fulfillment s inner join marketplace.temporary_supplier_fulfillment t on s.sku_simple=t.sku_simple set s.fulfillment_type_duplicate=1 where count>1 and country='Venezuela';

update marketplace.supplier_fulfillment s set s.fulfillment_type_duplicate=0 where fulfillment_type_duplicate is null and country='Venezuela';

drop table marketplace.temporary_supplier_fulfillment;

create table marketplace.temporary_supplier_fulfillment as select cat1, avg(package_height) as package_height from marketplace.supplier_fulfillment where country='Venezuela' group by cat1;

create index sku_simple on marketplace.temporary_supplier_fulfillment(cat1);

update marketplace.supplier_fulfillment s inner join marketplace.temporary_supplier_fulfillment t on s.cat1=t.cat1 set s.package_height=t.package_height where s.package_height is null and country='Venezuela'; 

drop table marketplace.temporary_supplier_fulfillment;

alter table marketplace.supplier_fulfillment drop primary key;

-- Cleaning

delete from marketplace.supplier_fulfillment where fulfillment_type_real='' and fulfillment_type_duplicate=1;

update marketplace.supplier_fulfillment set fulfillment_type_duplicate=null;

create table marketplace.temporary_supplier_fulfillment as select country, sku_simple, count(*) as count from marketplace.supplier_fulfillment group by country, sku_simple;

create index sku_simple on marketplace.temporary_supplier_fulfillment(sku_simple);
create index country on marketplace.temporary_supplier_fulfillment(country);

update marketplace.supplier_fulfillment s inner join marketplace.temporary_supplier_fulfillment t on s.sku_simple=t.sku_simple and s.country=t.country set s.fulfillment_type_duplicate=1 where count>1;

update marketplace.supplier_fulfillment s set s.fulfillment_type_duplicate=0 where fulfillment_type_duplicate is null;

drop table marketplace.temporary_supplier_fulfillment;

-- Count Distinct SKU Simple

create table marketplace.temporary_supplier_fulfillment as select country, sku_simple, count(*) as count from marketplace.supplier_fulfillment group by country, sku_simple; 

create index sku_simple on marketplace.temporary_supplier_fulfillment(sku_simple);
create index country on marketplace.temporary_supplier_fulfillment(country);

update marketplace.supplier_fulfillment s inner join marketplace.temporary_supplier_fulfillment t on s.sku_simple=t.sku_simple and s.country=t.country set s.distinct_sku_simple=1/t.count;

drop table marketplace.temporary_supplier_fulfillment;

-- Package Measure

create table marketplace.temporary_measures as select country, sku_simple, greatest(cast(replace(if(isnull(s.package_width),1,s.package_width),',','.') as decimal(6,2)),1.0) as package_width, greatest(cast(replace(if(isnull(s.package_length),1,s.package_length),',','.') as decimal(6,2)),1.0) as package_length, greatest(cast(replace(if(isnull(s.package_height),1,s.package_height),',','.') as decimal(6,2)),1.0) as package_height, greatest(cast(replace(if(isnull(s.package_weight),1,s.package_weight),',','.') as decimal(6,2)),1.0) as package_weight, 0.0 as volume_weight, 0.0 as maximum_volume, 'default value' as package_measure from marketplace.supplier_fulfillment s; 

update marketplace.temporary_measures set volume_weight = package_width*package_length*package_height/5000;

update marketplace.temporary_measures set maximum_volume = CASE when volume_weight > package_weight then volume_weight else package_weight end;

update marketplace.temporary_measures set package_measure = case when maximum_volume > 35 then 'oversized' else ( case when maximum_volume > 5 then 'large' else ( case when maximum_volume > 2 then 'medium' else 'small' end) end) end;

update marketplace.supplier_fulfillment s inner join marketplace.temporary_measures m on s.sku_simple=m.sku_simple and s.country=m.country set s.package_measure=m.package_measure;

drop table marketplace.temporary_measures;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `table_monitoring`()
begin

delete from marketplace.table_monitoring;

insert into marketplace.table_monitoring(table_name, Mexico) select 'BOB', created_at from bob_live_mx.sales_order order by created_at desc limit 1;	

update marketplace.table_monitoring set Colombia = (select created_at from bob_live_co.sales_order order by created_at desc limit 1 ) where table_name = 'BOB';	

update marketplace.table_monitoring set Peru = (select created_at from bob_live_pe.sales_order order by created_at desc limit 1 ) where table_name = 'BOB';	

update marketplace.table_monitoring set Venezuela = (select created_at from bob_live_ve.sales_order order by created_at desc limit 1 ) where table_name = 'BOB';

insert into marketplace.table_monitoring(table_name, Mexico) select 'WMS', data_criacao from wmsprod.itens_venda order by data_criacao desc limit 1;	

update marketplace.table_monitoring set Colombia = (select data_criacao from wmsprod_co.itens_venda order by data_criacao desc limit 1 ) where table_name = 'WMS';	

update marketplace.table_monitoring set Peru = (select data_criacao from wmsprod_pe.itens_venda order by data_criacao desc limit 1 ) where table_name = 'WMS';	

update marketplace.table_monitoring set Venezuela = (select data_criacao from wmsprod_ve.itens_venda order by data_criacao desc limit 1 ) where table_name = 'WMS';	

insert into marketplace.table_monitoring(table_name, Mexico) select 'OMS', updated_at from procurement_live_mx.procurement_order_item order by updated_at desc limit 1;	

update marketplace.table_monitoring set Colombia = (select updated_at from procurement_live_co.procurement_order_item order by updated_at desc limit 1 ) where table_name = 'OMS';	

update marketplace.table_monitoring set Peru = (select updated_at from procurement_live_pe.procurement_order_item order by updated_at desc limit 1 ) where table_name = 'OMS';	

update marketplace.table_monitoring set Venezuela = (select updated_at from procurement_live_ve.procurement_order_item order by updated_at desc limit 1 ) where table_name = 'OMS';

insert into marketplace.table_monitoring(table_name, Mexico) select 'Global GA', date from SEM.campaign_ad_group_mx order by date desc limit 1;	

update marketplace.table_monitoring set Colombia = (select date from SEM.campaign_ad_group_co order by date desc limit 1 ) where table_name = 'Global GA';	

update marketplace.table_monitoring set Peru = (select date from SEM.campaign_ad_group_pe order by date desc limit 1 ) where table_name = 'Global GA';

update marketplace.table_monitoring set Venezuela = (select date from SEM.campaign_ad_group_ve order by date desc limit 1 ) where table_name = 'Global GA';

--

insert into marketplace.table_monitoring(table_name, Mexico) select 'Keyword GA', date from SEM.campaign_keyword_mx order by date desc limit 1;	

update marketplace.table_monitoring set Colombia = (select date from SEM.campaign_keyword_co order by date desc limit 1 ) where table_name = 'Keyword GA';	

update marketplace.table_monitoring set Peru = (select date from SEM.campaign_keyword_pe order by date desc limit 1 ) where table_name = 'Keyword GA';

update marketplace.table_monitoring set Venezuela = (select date from SEM.campaign_keyword_ve order by date desc limit 1 ) where table_name = 'Keyword GA';

--

insert into marketplace.table_monitoring(table_name, Colombia) select 'Global Fashion GA', date from SEM.campaign_ad_group_fashion_co order by date desc limit 1;	

insert into marketplace.table_monitoring(table_name, Mexico) select 'Facebook GA', date from facebook.ga_facebook_ads_region_mx order by date desc limit 1;	

update marketplace.table_monitoring set Colombia = (select date from facebook.ga_facebook_ads_region_co order by date desc limit 1 ) where table_name = 'Facebook GA';	

update marketplace.table_monitoring set Peru = (select date from facebook.ga_facebook_ads_region_pe order by date desc limit 1 ) where table_name = 'Facebook GA';

insert into marketplace.table_monitoring(table_name, Mexico) select 'Mobile App GA', date from production.mobileapp_transaction_id order by date desc limit 1;

update marketplace.table_monitoring set Colombia = (select date from production_co.mobileapp_transaction_id order by date desc limit 1 ) where table_name = 'Mobile App GA';

update marketplace.table_monitoring set Peru = (select date from production_pe.mobileapp_transaction_id order by date desc limit 1 ) where table_name = 'Mobile App GA';

update marketplace.table_monitoring set Venezuela = (select date from production_ve.mobileapp_transaction_id order by date desc limit 1 ) where table_name = 'Mobile App GA';

--

insert into marketplace.table_monitoring(table_name, Mexico) select 'Facebook Keyword GA', date from facebook.ga_facebook_ads_keyword_mx order by date desc limit 1;	

update marketplace.table_monitoring set Colombia = (select date from facebook.ga_facebook_ads_keyword_co order by date desc limit 1 ) where table_name = 'Facebook Keyword GA';	

update marketplace.table_monitoring set Peru = (select date from facebook.ga_facebook_ads_keyword_pe order by date desc limit 1 ) where table_name = 'Facebook Keyword GA';

--

insert into marketplace.table_monitoring(table_name, Mexico) select 'Facebook Cost', date from facebook.facebook_campaign_mx order by date desc limit 1;	

update marketplace.table_monitoring set Colombia = (select date from facebook.facebook_campaign_co order by date desc limit 1 ) where table_name = 'Facebook Cost';	

update marketplace.table_monitoring set Peru = (select date from facebook.facebook_campaign_pe order by date desc limit 1 ) where table_name = 'Facebook Cost';

update marketplace.table_monitoring set Venezuela = (select date from facebook.facebook_campaign_ve order by date desc limit 1 ) where table_name = 'Facebook Cost';

insert into marketplace.table_monitoring(table_name, Colombia) select 'Facebook Fashion Cost', date from facebook.facebook_campaign_fashion_co order by date desc limit 1;	

--

insert into marketplace.table_monitoring(table_name, Mexico) select 'Criteo Cost', date from marketing_report.criteo where country='MEX' order by date desc limit 1;	

update marketplace.table_monitoring set Colombia = (select date from marketing_report.criteo where country='COL' order by date desc limit 1 ) where table_name = 'Criteo Cost';	

--

insert into marketplace.table_monitoring(table_name, Mexico) select 'Sociomantic Cost', date from marketing_report.sociomantic where country='MEX' order by date desc limit 1;	

update marketplace.table_monitoring set Colombia = (select date from marketing_report.sociomantic where country='COL' order by date desc limit 1 ) where table_name = 'Sociomantic Cost';	

update marketplace.table_monitoring set Peru = (select date from marketing_report.sociomantic where country='PER' order by date desc limit 1 ) where table_name = 'Sociomantic Cost';	

update marketplace.table_monitoring set Venezuela = (select date from marketing_report.sociomantic where country='VEN' order by date desc limit 1 ) where table_name = 'Sociomantic Cost';	

--

insert into marketplace.table_monitoring(table_name, Mexico) select 'Triggit Cost', report_start_date from marketing_report.triggit_mx order by report_start_date desc limit 1;	

update marketplace.table_monitoring set Colombia = (select report_start_date from marketing_report.triggit_co order by report_start_date desc limit 1 ) where table_name = 'Triggit Cost';	

update marketplace.table_monitoring set Peru = (select report_start_date from marketing_report.triggit_pe order by report_start_date desc limit 1 ) where table_name = 'Triggit Cost';	

update marketplace.table_monitoring set Venezuela = (select report_start_date from marketing_report.triggit_ve order by report_start_date desc limit 1 ) where table_name = 'Triggit Cost';	

--

insert into marketplace.table_monitoring(table_name, Mexico) select 'Vizury Cost', date from marketing_report.vizury_mx order by date desc limit 1;	

update marketplace.table_monitoring set Colombia = (select date from marketing_report.vizury_co order by date desc limit 1 ) where table_name = 'Vizury Cost';

update marketplace.table_monitoring set Peru = (select date from marketing_report.vizury_pe order by date desc limit 1 ) where table_name = 'Vizury Cost';

update marketplace.table_monitoring set Venezuela = (select date from marketing_report.vizury_ve order by date desc limit 1 ) where table_name = 'Vizury Cost';

--

insert into marketplace.table_monitoring(table_name, Mexico) select 'Trade Tracker Cost', date from marketing_report.affiliates_mx where network='TradeTracker' order by date desc limit 1;	

update marketplace.table_monitoring set Colombia = (select date from marketing_report.affiliates_co where network='TradeTracker' order by date desc limit 1 ) where table_name = 'Trade Tracker Cost';

update marketplace.table_monitoring set Peru = (select date from marketing_report.affiliates_pe where network='TradeTracker' order by date desc limit 1 ) where table_name = 'Trade Tracker Cost';

update marketplace.table_monitoring set Venezuela = (select date from marketing_report.affiliates_ve where network='TradeTracker' order by date desc limit 1 ) where table_name = 'Trade Tracker Cost';

--

insert into marketplace.table_monitoring(table_name, Mexico) select 'GLG Cost', date from marketing_report.affiliates_mx where network='GlobalLeadsGroup' order by date desc limit 1;	

update marketplace.table_monitoring set Colombia = (select date from marketing_report.affiliates_co where network='GlobalLeadsGroup' order by date desc limit 1 ) where table_name = 'GLG Cost';

update marketplace.table_monitoring set Peru = (select date from marketing_report.affiliates_pe where network='GlobalLeadsGroup' order by date desc limit 1 ) where table_name = 'GLG Cost';

update marketplace.table_monitoring set Venezuela = (select date from marketing_report.affiliates_ve where network='GlobalLeadsGroup' order by date desc limit 1 ) where table_name = 'GLG Cost';

--

insert into marketplace.table_monitoring(table_name, Mexico) select 'Pampa Network Cost', date from marketing_report.affiliates_mx where network='PampaNetwork' order by date desc limit 1;	

update marketplace.table_monitoring set Colombia = (select date from marketing_report.affiliates_co where network='PampaNetwork' order by date desc limit 1 ) where table_name = 'Pampa Network Cost';

update marketplace.table_monitoring set Peru = (select date from marketing_report.affiliates_pe where network='PampaNetwork' order by date desc limit 1 ) where table_name = 'Pampa Network Cost';

update marketplace.table_monitoring set Venezuela = (select date from marketing_report.affiliates_ve where network='PampaNetwork' order by date desc limit 1 ) where table_name = 'Pampa Network Cost';

--

insert into marketplace.table_monitoring(table_name, Mexico) select 'Experian Cost', date from development_mx.M_Other_Cost where country='MEX' and type='experian' order by date desc limit 1;	

update marketplace.table_monitoring set Colombia = (select date from development_mx.M_Other_Cost where country='COL' and type='experian' order by date desc limit 1 ) where table_name = 'Experian Cost';

update marketplace.table_monitoring set Peru = (select date from development_mx.M_Other_Cost where country='PER' and type='experian' order by date desc limit 1 ) where table_name = 'Experian Cost';

update marketplace.table_monitoring set Venezuela = (select date from development_mx.M_Other_Cost where country='VEN' and type='experian' order by date desc limit 1 ) where table_name = 'Experian Cost';

--

insert into marketplace.table_monitoring(table_name, Colombia) select 'ICCK Cost', date from marketing_report.icck where country='Colombia' order by date desc limit 1;	

--

insert into marketplace.table_monitoring(table_name, Colombia) select 'ExoClick Cost', date from marketing_report.exoclick where country='Colombia' order by date desc limit 1;	

--

insert into marketplace.table_monitoring(table_name, Mexico) select 'tbl_order_detail', date from production.tbl_order_detail order by date desc limit 1;	

update marketplace.table_monitoring set Colombia = (select date from production_co.tbl_order_detail order by date desc limit 1 ) where table_name = 'tbl_order_detail';	

update marketplace.table_monitoring set Peru = (select date from production_pe.tbl_order_detail order by date desc limit 1 ) where table_name = 'tbl_order_detail';	

update marketplace.table_monitoring set Venezuela = (select date from production_ve.tbl_order_detail order by date desc limit 1 ) where table_name = 'tbl_order_detail';

--

insert into marketplace.table_monitoring(table_name, Mexico) select 'A_Master', concat(date,' ', time)  from development_mx.A_Master order by date desc, time desc limit 1;	

update marketplace.table_monitoring set Colombia = (select concat(date,' ', time) from development_co_project.A_Master order by date desc, time desc limit 1 ) where table_name = 'A_Master';	

update marketplace.table_monitoring set Peru = (select concat(date,' ', time) from development_pe.A_Master order by date desc, time desc limit 1 ) where table_name = 'A_Master';	

update marketplace.table_monitoring set Venezuela = (select concat(date,' ', time) from development_ve.A_Master order by date desc, time desc limit 1 ) where table_name = 'A_Master';

--

insert into marketplace.table_monitoring(table_name, Mexico) select 'global_report', date  from marketing_report.global_report where country='Mexico' order by date desc limit 1;	

update marketplace.table_monitoring set Colombia = (select date  from marketing_report.global_report where country='Colombia' order by date desc limit 1 ) where table_name = 'global_report';	

update marketplace.table_monitoring set Peru = (select date  from marketing_report.global_report where country='Peru' order by date desc limit 1 ) where table_name = 'global_report';	

update marketplace.table_monitoring set Venezuela = (select date  from marketing_report.global_report where country='Venezuela' order by date desc limit 1 ) where table_name = 'global_report';	

--

insert into marketplace.table_monitoring(table_name, Mexico) select 'Attribution Campaign', date  from attribution_testing.getfullcampaign_mx order by date desc limit 1;	

update marketplace.table_monitoring set Colombia = (select date from attribution_testing.getfullcampaign_co order by date  desc limit 1 ) where table_name = 'Attribution Campaign';	

update marketplace.table_monitoring set Peru = (select date from attribution_testing.getfullcampaign_pe order by date  desc limit 1 ) where table_name = 'Attribution Campaign';	

update marketplace.table_monitoring set Venezuela = (select date from attribution_testing.getfullcampaign_ve order by date  desc limit 1 ) where table_name = 'Attribution Campaign';	

insert into marketplace.table_monitoring(table_name, Mexico) select 'Attribution Campaign Config', date  from attribution_testing.campaigns_config_mx order by date desc limit 1;	

update marketplace.table_monitoring set Colombia = (select date from attribution_testing.campaigns_config_co order by date  desc limit 1 ) where table_name = 'Attribution Campaign Config';	

update marketplace.table_monitoring set Peru = (select date from attribution_testing.campaigns_config_pe order by date  desc limit 1 ) where table_name = 'Attribution Campaign Config';	

update marketplace.table_monitoring set Venezuela = (select date from attribution_testing.campaigns_config_ve order by date  desc limit 1 ) where table_name = 'Attribution Campaign Config';	

insert into marketplace.table_monitoring(table_name, Mexico) select 'Attribution Orders', date  from attribution_testing.getfullorders_mx order by date desc limit 1;	

update marketplace.table_monitoring set Colombia = (select date from attribution_testing.getfullorders_co order by date  desc limit 1 ) where table_name = 'Attribution Orders';

update marketplace.table_monitoring set Peru = (select date from attribution_testing.getfullorders_pe order by date  desc limit 1 ) where table_name = 'Attribution Orders';

update marketplace.table_monitoring set Venezuela = (select date from attribution_testing.getfullorders_ve order by date  desc limit 1 ) where table_name = 'Attribution Orders';

insert into marketplace.table_monitoring(table_name, Mexico) select 'Attribution Visitors', date  from attribution_testing.getfullvisitors_mx order by date desc limit 1;	

update marketplace.table_monitoring set Colombia = (select date from attribution_testing.getfullvisitors_co order by date  desc limit 1 ) where table_name = 'Attribution Visitors';

update marketplace.table_monitoring set Peru = (select date from attribution_testing.getfullvisitors_pe order by date  desc limit 1 ) where table_name = 'Attribution Visitors';

update marketplace.table_monitoring set Venezuela = (select date from attribution_testing.getfullvisitors_ve order by date  desc limit 1 ) where table_name = 'Attribution Visitors';

--

insert into marketplace.table_monitoring(table_name, Mexico) select 'Attribution Basket', date  from attribution_testing.getfullbasket_mx order by date desc limit 1;

update marketplace.table_monitoring set Colombia = (select date from attribution_testing.getfullbasket_co order by date  desc limit 1 ) where table_name = 'Attribution Basket';

update marketplace.table_monitoring set Peru = (select date from attribution_testing.getfullbasket_pe order by date  desc limit 1 ) where table_name = 'Attribution Basket';

update marketplace.table_monitoring set Venezuela = (select date from attribution_testing.getfullbasket_ve order by date  desc limit 1 ) where table_name = 'Attribution Basket';

--

insert into marketplace.table_monitoring(table_name, Mexico) select 'Attribution Clicks', date  from attribution_testing.getfullclicks_mx order by date desc limit 1;

update marketplace.table_monitoring set Colombia = (select date from attribution_testing.getfullclicks_co order by date  desc limit 1 ) where table_name = 'Attribution Clicks';

update marketplace.table_monitoring set Peru = (select date from attribution_testing.getfullclicks_pe order by date  desc limit 1 ) where table_name = 'Attribution Clicks';

update marketplace.table_monitoring set Venezuela = (select date from attribution_testing.getfullclicks_ve order by date  desc limit 1 ) where table_name = 'Attribution Clicks';


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `weekly_sc`()
begin

set @days:=60;

delete from marketplace.weekly_sc;

-- Mexico

insert into marketplace.weekly_sc(Country, MonthNum, Week, Date, ItemID, OrderNum, Supplier, CatBP, SKUConfig, SKUSimple, isMPlace, MPlaceFee, Price, Cost, CouponValue, PaymentMethod, NetItemsInOrder, ShipmentType, OrderBeforeCan, OrderAfterCan, Cancellations, PCOne, PCOnePFive, Rev, ShippingFee, ShipmentCost, PaymentFees, FLWHCost, FLCSCost ) 
SELECT
'Mexico' as Country,
development_mx.A_Master.MonthNum,
production.week_iso(  Date ) as Week,
development_mx.A_Master.Date,
development_mx.A_Master.ItemID,
development_mx.A_Master.OrderNum,
development_mx.A_Master.Supplier,
development_mx.A_Master.CatBP,
development_mx.A_Master.SKUConfig,
development_mx.A_Master.SKUSimple,
development_mx.A_Master.isMPlace,
development_mx.A_Master.MPlaceFee,
development_mx.A_Master.Price,
development_mx.A_Master.Cost,
development_mx.A_Master.CouponValue,
development_mx.A_Master.PaymentMethod,
development_mx.A_Master.NetItemsInOrder,
development_mx.A_Master.ShipmentType,
development_mx.A_Master.OrderBeforeCan,
development_mx.A_Master.OrderAfterCan,
development_mx.A_Master.Cancellations,
development_mx.A_Master.PCOne,
development_mx.A_Master.PCOnePFive,
development_mx.A_Master.Rev,
development_mx.A_Master.ShippingFee,
development_mx.A_Master.ShipmentCost,
development_mx.A_Master.PaymentFees,
development_mx.A_Master.FLWHCost,
development_mx.A_Master.FLCSCost
FROM development_mx.A_Master 
WHERE datediff(curdate(),date)<=@days;

update marketplace.weekly_sc inner join operations_mx.out_order_tracking on weekly_sc.itemid=out_order_tracking.item_id 
set weekly_sc.fulfillment_type_real =  operations_mx.out_order_tracking.fulfillment_type_real, 
weekly_sc.fulfillment_type_bob = operations_mx.out_order_tracking.fulfillment_type_bob,
weekly_sc.date_delivered = operations_mx.out_order_tracking.date_delivered,
weekly_sc.date_delivered_promised = operations_mx.out_order_tracking.date_delivered_promised,
weekly_sc.week_ordered = operations_mx.out_order_tracking.week_ordered,
weekly_sc.on_time_total_1st_attempt = operations_mx.out_order_tracking.on_time_total_1st_attempt,
weekly_sc.on_time_total_delivery = operations_mx.out_order_tracking.on_time_total_delivery,
weekly_sc.status_bob = operations_mx.out_order_tracking.status_bob,
weekly_sc.status_wms = operations_mx.out_order_tracking.status_wms where Country='Mexico';

-- Colombia

insert into marketplace.weekly_sc(Country, MonthNum, Week, Date, ItemID, OrderNum, Supplier, CatBP, SKUConfig, SKUSimple, isMPlace, MPlaceFee, Price, Cost, CouponValue, PaymentMethod, NetItemsInOrder, ShipmentType, OrderBeforeCan, OrderAfterCan, Cancellations, PCOne, PCOnePFive, Rev, ShippingFee, ShipmentCost, PaymentFees, FLWHCost, FLCSCost ) 
SELECT
'Colombia' as Country,
development_co_project.A_Master.MonthNum,
production.week_iso(  Date ) as Week,
development_co_project.A_Master.Date,
development_co_project.A_Master.ItemID,
development_co_project.A_Master.OrderNum,
development_co_project.A_Master.Supplier,
development_co_project.A_Master.CatBP,
development_co_project.A_Master.SKUConfig,
development_co_project.A_Master.SKUSimple,
development_co_project.A_Master.isMPlace,
development_co_project.A_Master.MPlaceFee,
development_co_project.A_Master.Price,
development_co_project.A_Master.Cost,
development_co_project.A_Master.CouponValue,
development_co_project.A_Master.PaymentMethod,
development_co_project.A_Master.NetItemsInOrder,
development_co_project.A_Master.ShipmentType,
development_co_project.A_Master.OrderBeforeCan,
development_co_project.A_Master.OrderAfterCan,
development_co_project.A_Master.Cancellations,
development_co_project.A_Master.PCOne,
development_co_project.A_Master.PCOnePFive,
development_co_project.A_Master.Rev,
development_co_project.A_Master.ShippingFee,
development_co_project.A_Master.ShipmentCost,
development_co_project.A_Master.PaymentFees,
development_co_project.A_Master.FLWHCost,
development_co_project.A_Master.FLCSCost
FROM development_co_project.A_Master 
WHERE datediff(curdate(),date)<=@days;

update marketplace.weekly_sc inner join operations_co.out_order_tracking on weekly_sc.itemid=out_order_tracking.item_id 
set weekly_sc.fulfillment_type_real =  operations_co.out_order_tracking.fulfillment_type_real, 
weekly_sc.fulfillment_type_bob = operations_co.out_order_tracking.fulfillment_type_bob,
weekly_sc.date_delivered = operations_co.out_order_tracking.date_delivered,
weekly_sc.date_delivered_promised = operations_co.out_order_tracking.date_delivered_promised,
weekly_sc.week_ordered = operations_co.out_order_tracking.week_ordered,
weekly_sc.on_time_total_1st_attempt = operations_co.out_order_tracking.on_time_total_1st_attempt,
weekly_sc.on_time_total_delivery = operations_co.out_order_tracking.on_time_total_delivery,
weekly_sc.status_bob = operations_co.out_order_tracking.status_bob,
weekly_sc.status_wms = operations_co.out_order_tracking.status_wms where Country='Colombia';

-- Peru

insert into marketplace.weekly_sc(Country, MonthNum, Week, Date, ItemID, OrderNum, Supplier, CatBP, SKUConfig, SKUSimple, isMPlace, MPlaceFee, Price, Cost, CouponValue, PaymentMethod, NetItemsInOrder, ShipmentType, OrderBeforeCan, OrderAfterCan, Cancellations, PCOne, PCOnePFive, Rev, ShippingFee, ShipmentCost, PaymentFees, FLWHCost, FLCSCost ) 
SELECT
'Peru' as Country,
development_pe.A_Master.MonthNum,
production.week_iso(  Date ) as Week,
development_pe.A_Master.Date,
development_pe.A_Master.ItemID,
development_pe.A_Master.OrderNum,
development_pe.A_Master.Supplier,
development_pe.A_Master.CatBP,
development_pe.A_Master.SKUConfig,
development_pe.A_Master.SKUSimple,
development_pe.A_Master.isMPlace,
development_pe.A_Master.MPlaceFee,
development_pe.A_Master.Price,
development_pe.A_Master.Cost,
development_pe.A_Master.CouponValue,
development_pe.A_Master.PaymentMethod,
development_pe.A_Master.NetItemsInOrder,
development_pe.A_Master.ShipmentType,
development_pe.A_Master.OrderBeforeCan,
development_pe.A_Master.OrderAfterCan,
development_pe.A_Master.Cancellations,
development_pe.A_Master.PCOne,
development_pe.A_Master.PCOnePFive,
development_pe.A_Master.Rev,
development_pe.A_Master.ShippingFee,
development_pe.A_Master.ShipmentCost,
development_pe.A_Master.PaymentFees,
development_pe.A_Master.FLWHCost,
development_pe.A_Master.FLCSCost
FROM development_pe.A_Master 
WHERE datediff(curdate(),date)<=@days;

update marketplace.weekly_sc inner join operations_pe.out_order_tracking on weekly_sc.itemid=out_order_tracking.item_id 
set weekly_sc.fulfillment_type_real =  operations_pe.out_order_tracking.fulfillment_type_real, 
weekly_sc.fulfillment_type_bob = operations_pe.out_order_tracking.fulfillment_type_bob,
weekly_sc.date_delivered = operations_pe.out_order_tracking.date_delivered,
weekly_sc.date_delivered_promised = operations_pe.out_order_tracking.date_delivered_promised,
weekly_sc.week_ordered = operations_pe.out_order_tracking.week_ordered,
weekly_sc.on_time_total_1st_attempt = operations_pe.out_order_tracking.on_time_total_1st_attempt,
weekly_sc.on_time_total_delivery = operations_pe.out_order_tracking.on_time_total_delivery,
weekly_sc.status_bob = operations_pe.out_order_tracking.status_bob,
weekly_sc.status_wms = operations_pe.out_order_tracking.status_wms where Country='Peru';

-- Venezuela

insert into marketplace.weekly_sc(Country, MonthNum, Week, Date, ItemID, OrderNum, Supplier, CatBP, SKUConfig, SKUSimple, isMPlace, MPlaceFee, Price, Cost, CouponValue, PaymentMethod, NetItemsInOrder, ShipmentType, OrderBeforeCan, OrderAfterCan, Cancellations, PCOne, PCOnePFive, Rev, ShippingFee, ShipmentCost, PaymentFees, FLWHCost, FLCSCost ) 
SELECT
'Venezuela' as Country,
development_ve.A_Master.MonthNum,
production.week_iso(  Date ) as Week,
development_ve.A_Master.Date,
development_ve.A_Master.ItemID,
development_ve.A_Master.OrderNum,
development_ve.A_Master.Supplier,
development_ve.A_Master.CatBP,
development_ve.A_Master.SKUConfig,
development_ve.A_Master.SKUSimple,
development_ve.A_Master.isMPlace,
development_ve.A_Master.MPlaceFee,
development_ve.A_Master.Price,
development_ve.A_Master.Cost,
development_ve.A_Master.CouponValue,
development_ve.A_Master.PaymentMethod,
development_ve.A_Master.NetItemsInOrder,
development_ve.A_Master.ShipmentType,
development_ve.A_Master.OrderBeforeCan,
development_ve.A_Master.OrderAfterCan,
development_ve.A_Master.Cancellations,
development_ve.A_Master.PCOne,
development_ve.A_Master.PCOnePFive,
development_ve.A_Master.Rev,
development_ve.A_Master.ShippingFee,
development_ve.A_Master.ShipmentCost,
development_ve.A_Master.PaymentFees,
development_ve.A_Master.FLWHCost,
development_ve.A_Master.FLCSCost
FROM development_ve.A_Master 
WHERE datediff(curdate(),date)<=@days;

update marketplace.weekly_sc inner join operations_ve.out_order_tracking on weekly_sc.itemid=out_order_tracking.item_id 
set weekly_sc.fulfillment_type_real =  operations_ve.out_order_tracking.fulfillment_type_real, 
weekly_sc.fulfillment_type_bob = operations_ve.out_order_tracking.fulfillment_type_bob,
weekly_sc.date_delivered = operations_ve.out_order_tracking.date_delivered,
weekly_sc.date_delivered_promised = operations_ve.out_order_tracking.date_delivered_promised,
weekly_sc.week_ordered = operations_ve.out_order_tracking.week_ordered,
weekly_sc.on_time_total_1st_attempt = operations_ve.out_order_tracking.on_time_total_1st_attempt,
weekly_sc.on_time_total_delivery = operations_ve.out_order_tracking.on_time_total_delivery,
weekly_sc.status_bob = operations_ve.out_order_tracking.status_bob,
weekly_sc.status_wms = operations_ve.out_order_tracking.status_wms where Country='Venezuela';

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `yearly_sc`()
begin

set @year=2014;

delete from marketplace.monthly_sc;

-- Mexico

insert into marketplace.monthly_sc select 'Mexico', year(date), monthnum, 'Linio General', sum(rev), count(*) from development_mx.A_Master where OrderAfterCan=1 group by monthnum order by monthnum desc;

insert into marketplace.monthly_sc select 'Mexico', year(date), monthnum, 'Marketplace', sum(rev), count(*) from development_mx.A_Master where OrderAfterCan=1 and isMPlace=1 group by monthnum order by monthnum desc;

-- Colombia

insert into marketplace.monthly_sc select 'Colombia', year(date), monthnum, 'Linio General', sum(rev), count(*) from development_co_project.A_Master where OrderAfterCan=1 group by monthnum order by monthnum desc;

insert into marketplace.monthly_sc select 'Colombia', year(date), monthnum, 'Marketplace', sum(rev), count(*) from development_co_project.A_Master where OrderAfterCan=1 and isMPlace=1 group by monthnum order by monthnum desc;

-- Peru

insert into marketplace.monthly_sc select 'Peru', year(date), monthnum, 'Linio General', sum(rev), count(*) from development_pe.A_Master where OrderAfterCan=1 group by monthnum order by monthnum desc;

insert into marketplace.monthly_sc select 'Peru', year(date), monthnum, 'Marketplace', sum(rev), count(*) from development_pe.A_Master where OrderAfterCan=1 and isMPlace=1 group by monthnum order by monthnum desc;

-- Venezuela

insert into marketplace.monthly_sc select 'Venezuela', year(date), monthnum, 'Linio General', sum(rev), count(*) from development_ve.A_Master where OrderAfterCan=1 group by monthnum order by monthnum desc;

insert into marketplace.monthly_sc select 'Venezuela', year(date), monthnum, 'Marketplace', sum(rev), count(*) from development_ve.A_Master where OrderAfterCan=1 and isMPlace=1 group by monthnum order by monthnum desc;

-- Exchange Rate (EUR)

update marketplace.monthly_sc s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr where s.country = 'Mexico' and r.country='MEX';

update marketplace.monthly_sc s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr where s.country = 'Colombia' and r.country='COL';

update marketplace.monthly_sc s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr where s.country = 'Peru' and r.country='PER';

update marketplace.monthly_sc s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr where s.country = 'Venezuela' and r.country='VEN';

delete from marketplace.yearly_sc;

insert into marketplace.yearly_sc select country, year, type, sum(net_revenue), sum(items) from marketplace.monthly_sc where year>=@year group by country, year, type;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:04:05
