-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: production_pe
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'production_pe'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 FUNCTION `calcworkdays`(sdate varchar(50), edate varchar(50)) RETURNS int(11)
begin
 declare wdays, tdiff, counter, thisday smallint;
 declare newdate date;
 set newdate := sdate;
 set wdays = 0;
 if sdate is null then return 0; end if;
 if edate is null then return 0; end if;
 if edate like '1%' then return 0; end if;
 if edate like '0%' then return 0; end if;
 if sdate like '1%' then return 0; end if;
 if sdate like '0%' then return 0; end if;
    if datediff(edate, sdate) <= 0 then return 0; end if;
         label1: loop
          set thisday = dayofweek(newdate);
            if thisday in(1,2,3,4,5) then set wdays := wdays + 1; end if;
             set newdate = date_add(newdate, interval 1 day);
            if datediff(edate, newdate) <= 0 then leave label1; end if;
       end loop label1;
      return wdays;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 FUNCTION `maximum`(procure integer, ready integer, to_ship integer,  attempt integer ) RETURNS int(11)
begin
declare compare integer; 
		set compare = procure;
		if compare < ready then set compare = ready; end if;
		if compare < to_ship then set compare = to_ship; end if;
		if compare < attempt then set compare = attempt; end if;
 return compare;
 end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 FUNCTION `week_exit`(edate date) RETURNS varchar(20) CHARSET latin1
begin
 declare sdate varchar(20);
 if year(edate) = 2012 then set sdate = week(edate, 5)+1; else set sdate = date_format(edate, "%v"); end if;
      return sdate;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 FUNCTION `week_iso`(edate date) RETURNS varchar(20) CHARSET latin1
begin
 declare sdate varchar(20);
 if year(edate) = 2012 then set sdate = concat(year(edate), '-',(week(edate, 5))+1); else set sdate = date_format(edate, "%x-%v"); end if;
      return sdate;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 FUNCTION `workday`(edate date, workdays integer ) RETURNS date
begin
 declare wdays, thisday, thisday0, timer smallint;
 declare newdate, middate date;
 set wdays = 0;
 set newdate :=  edate;
 if edate is null then set newdate = '2012-05-01'; end if;
 if workdays is null then return '2012-05-01'; end if;
 set thisday0 = dayofweek(edate);
 if workdays in (0) and thisday0 between 2 and 6 then return newdate; end if;
 if (workdays in (0) and thisday0 in (7)) then return date(adddate(newdate, 2)); end if;
 if (workdays in (0) and thisday0 in (1)) then return date(adddate(newdate, 1)); end if;
	label2: loop
		set newdate = date(adddate(newdate, 1));
        set thisday = dayofweek(newdate);
		if thisday between 2 and 6 then 
			set wdays = wdays + 1;
			if wdays >= workdays then 
				leave label2; 
			end if;
		else 
			set wdays = wdays;
		end if;
	end loop label2;
return newdate;
 end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `catalog_creation`()
begin

delete from production_pe.catalog_creation;

insert into production_pe.catalog_creation
select z.cat1, a201205.number as '201205', a201206.number as '201206', a201207.number as '201207', a201208.number as '201208', a201209.number as '201209', a201210.number as '201210', a201211.number as '201211', a201212.number as '201212', a201301.number as '201301', a201302.number as '201302', a201303.number as '201303', a201304.number as '201304' from (select cat1 from production_pe.tbl_catalog_product_v2 c where cat1 is not null group by cat1)z left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_pe.tbl_catalog_product_v2 c, bob_live_pe.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) = 201205 group by cat1)a201205 on z.cat1=a201205.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_pe.tbl_catalog_product_v2 c, bob_live_pe.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201206 group by cat1)a201206 on z.cat1=a201206.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_pe.tbl_catalog_product_v2 c, bob_live_pe.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201207 group by cat1)a201207 on z.cat1=a201207.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_pe.tbl_catalog_product_v2 c, bob_live_pe.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201208 group by cat1)a201208 on z.cat1=a201208.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_pe.tbl_catalog_product_v2 c, bob_live_pe.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201209 group by cat1)a201209 on z.cat1=a201209.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_pe.tbl_catalog_product_v2 c, bob_live_pe.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201210 group by cat1)a201210 on z.cat1=a201210.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_pe.tbl_catalog_product_v2 c, bob_live_pe.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201211 group by cat1)a201211 on z.cat1=a201211.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_pe.tbl_catalog_product_v2 c, bob_live_pe.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201212 group by cat1)a201212 on z.cat1=a201212.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_pe.tbl_catalog_product_v2 c, bob_live_pe.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201301 group by cat1)a201301 on z.cat1=a201301.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_pe.tbl_catalog_product_v2 c, bob_live_pe.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201302 group by cat1)a201302 on z.cat1=a201302.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_pe.tbl_catalog_product_v2 c, bob_live_pe.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201303 group by cat1)a201303 on z.cat1=a201303.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_pe.tbl_catalog_product_v2 c, bob_live_pe.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201304 group by cat1)a201304 on z.cat1=a201304.cat1 order by cat1;

update production_pe.catalog_creation set `201205` = 0  where `201205` is null;

update production_pe.catalog_creation set `201206` = 0  where `201206` is null;

update production_pe.catalog_creation set `201207` = 0  where `201207` is null;

update production_pe.catalog_creation set `201208` = 0  where `201208` is null;

update production_pe.catalog_creation set `201209` = 0  where `201209` is null;

update production_pe.catalog_creation set `201210` = 0  where `201210` is null;

update production_pe.catalog_creation set `201211` = 0  where `201211` is null;

update production_pe.catalog_creation set `201212` = 0  where `201212` is null;

update production_pe.catalog_creation set `201301` = 0  where `201301` is null;

update production_pe.catalog_creation set `201302` = 0  where `201302` is null;

update production_pe.catalog_creation set `201303` = 0  where `201303` is null;

update production_pe.catalog_creation set `201304` = 0  where `201304` is null;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `catalog_history`()
begin

call production_pe.catalog_visible;

insert into production_pe.catalog_history(date, sku_config, sku_simple, product_name, status_config, status_simple, quantity, price) 
select curdate(), catalog_config.sku as sku_config, catalog_simple.sku as sku_simple, catalog_config.name, catalog_config.status, catalog_simple.status, catalog_stock.quantity, catalog_simple.price
from (bob_live_pe.catalog_config inner join bob_live_pe.catalog_simple on catalog_config.id_catalog_config = catalog_simple.fk_catalog_config) left join bob_live_pe.catalog_stock on catalog_simple.id_catalog_simple = catalog_stock.fk_catalog_simple;

update production_pe.catalog_history set quantity = 0  where quantity is null and date = curdate();

update production_pe.catalog_history c inner join production_pe.catalog_visible v on c.sku_simple=v.sku_simple set visible = 1 where date = curdate();

update production_pe.catalog_history set visible = 0  where visible is null and date = curdate();

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `catalog_visible`()
begin

delete from production_pe.catalog_visible;

insert into production_pe.catalog_visible(sku_config, sku_simple, pet_status, pet_approved, status_config, status_simple, name, display_if_out_of_stock, updated_at, activated_at, price) 
select catalog_config.sku as sku_config, catalog_simple.sku as sku_simple, catalog_config.pet_status, catalog_config.pet_approved, catalog_config.status as status_config, catalog_simple.status as status_simple, catalog_config.name, catalog_config.display_if_out_of_stock, catalog_config.updated_at, catalog_config.activated_at, catalog_simple.price
from (bob_live_pe.catalog_config inner join bob_live_pe.catalog_simple on catalog_config.id_catalog_config = catalog_simple.fk_catalog_config)
where (((catalog_config.pet_status)="creation,edited,images") and ((catalog_config.pet_approved)=1) and ((catalog_config.status)="active") and ((catalog_simple.status)="active") and ((catalog_config.display_if_out_of_stock)=0) and ((catalog_simple.price)>0)) or (((catalog_config.pet_status)="creation,edited,images") and ((catalog_config.pet_approved)=1) and ((catalog_config.status)="active") and ((catalog_simple.status)="active") and ((catalog_config.display_if_out_of_stock)=1) and ((catalog_simple.price)>0));

create table production_pe.temporary_visible(
sku varchar(255),
catalog_warehouse_stock int,
catalog_supplier_stock int,
item_reserveed int,
stock_available int
);
create index sku on production_pe.temporary_visible(sku);

insert into production_pe.temporary_visible
SELECT `catalog_simple`.`sku`, IFNULL((SELECT CAST(quantity AS SIGNED INT) FROM bob_live_pe.catalog_warehouse_stock WHERE catalog_warehouse_stock.fk_catalog_simple = catalog_simple.id_catalog_simple),0) AS `catalog_warehouse_stock`, IFNULL((SELECT CAST(quantity AS SIGNED INT) FROM bob_live_pe.catalog_supplier_stock WHERE catalog_supplier_stock.fk_catalog_simple = catalog_simple.id_catalog_simple),0) AS `catalog_supplier_stock`, ( (SELECT COUNT(*) FROM bob_live_pe.sales_order_item JOIN bob_live_pe.sales_order ON fk_sales_order = id_sales_order WHERE fk_sales_order_process IN (33,5,27,26,31,8,23,28,32,34,29,9,30) AND is_reserved = 1 AND sales_order_item.sku = catalog_simple.sku) + IFNULL((SELECT SUM(catalog_stock_shared.reserved) FROM bob_live_pe.catalog_stock_shared WHERE catalog_stock_shared.sku = catalog_simple.sku GROUP BY catalog_stock_shared.sku), 0)) AS `item_reserved`, GREATEST((IFNULL((SELECT CAST(quantity AS SIGNED INT) FROM bob_live_pe.catalog_stock WHERE catalog_stock.fk_catalog_simple = catalog_simple.id_catalog_simple),0) - ( (SELECT COUNT(*) FROM bob_live_pe.sales_order_item JOIN bob_live_pe.sales_order ON fk_sales_order = id_sales_order WHERE fk_sales_order_process IN (33,5,27,26,31,8,23,28,32,34,29,9,30) AND is_reserved = 1 AND sales_order_item.sku = catalog_simple.sku) + IFNULL((SELECT SUM(catalog_stock_shared.reserved) FROM bob_live_pe.catalog_stock_shared WHERE catalog_stock_shared.sku = catalog_simple.sku GROUP BY catalog_stock_shared.sku), 0))),0) AS `stock_available` FROM bob_live_pe.`catalog_simple` ORDER BY `catalog_simple`.`id_catalog_simple` DESC;

update production_pe.catalog_visible v inner join production_pe.temporary_visible t on t.sku=v.sku_simple set v.quantity=t.stock_available;

delete from production_pe.catalog_visible where quantity=0 or quantity is null;

drop table production_pe.temporary_visible;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `channel_marketing`()
BEGIN
#Channel Report PERU---

#Define channel---
#Voucher is priority statemnt
#If voucher is empty use GA source_medium
UPDATE SEM.campaign_ad_group_pe SET source_medium=source_medium;

#BLOG
-- UPDATE SEM.campaign_ad_group_pe SET channel='Blog Linio' WHERE source_medium like 'blog%' 
-- OR source_medium='blog.linio.com.pe / referral' or source_medium='blogs.peru21.pe / referral';

#REFERRAL
UPDATE SEM.campaign_ad_group_pe SET channel='Referral Sites' WHERE source_medium like '%referral%' 
AND source_medium<>'facebook.com / referral'
AND source_medium<>'m.facebook.com / referral'
AND source_medium<>'linio.com.co / referral'
AND source_medium<>'linio.com / referral'
AND source_medium<>'linio.com.mx / referral'
AND source_medium<>'google.co.ve / referral'
AND source_medium<>'linio.com.ve / referral'
AND source_medium<>'linio.com.pe / referral'
AND source_medium<>'www-staging.linio.com.ve / referral'
AND source_medium<>'google.com / referral'
AND source_medium<>'.com.ve / referral'
AND source_medium<>'blog.linio.com.ve / referral' AND source_medium<>'t.co / referral'
AND source_medium not like '%google%' AND source_medium not like '%blog%';


UPDATE SEM.campaign_ad_group_pe SET channel='Buscape' 
WHERE source_medium='Buscape / Price Comparison' OR source_medium like '%buscape%';

#FB ADS
UPDATE SEM.campaign_ad_group_pe SET channel='Facebook Ads' WHERE source_medium='facebook / socialmediaads' OR source_medium 
like '%facebook%ads%';

#Criteo
UPDATE SEM.campaign_ad_group_pe set channel='Criteo' where source_medium like '%criteo%';

#PAMPA
UPDATE SEM.campaign_ad_group_pe set channel='Pampa Network' where source_medium like '%pampa%';

#SEM - GDN

UPDATE SEM.campaign_ad_group_pe SET channel='SEM' WHERE source_medium='google / cpc' AND 
(campaign not like 'brandb%' and campaign not like 'er.%' and campaign not like '[D%');
UPDATE SEM.campaign_ad_group_pe SET channel='SEM Branded' WHERE source_medium='google / cpc' AND campaign like 'brandb%';

#GDN
UPDATE SEM.campaign_ad_group_pe SET channel='Google Display Network' WHERE source_medium='google / cpc' AND 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%');

#RETARGETING
UPDATE SEM.campaign_ad_group_pe SET channel='Sociomantic' WHERE source_medium='sociomantic / (not set)' OR source_medium='sociomantic / retargeting';
UPDATE SEM.campaign_ad_group_pe SET channel='Cart Recovery' WHERE source_medium='Cart_Recovery / CRM';
UPDATE SEM.campaign_ad_group_pe SET channel='Vizury' WHERE source_medium='vizury / retargeting' OR source_medium like '%vizury%';
UPDATE SEM.campaign_ad_group_pe SET channel='Facebook R.' WHERE source_medium='facebook / retargeting';
UPDATE SEM.campaign_ad_group_pe SET channel='VEInteractive' WHERE source_medium like 'VEInteractive%';
UPDATE SEM.campaign_ad_group_pe SET channel='Triggit' WHERE source_medium like '%Triggit / Retargeting%';


#SOCIAL MEDIA
UPDATE SEM.campaign_ad_group_pe SET channel='Facebook Referral' WHERE (source_medium='facebook.com / referral' OR source_medium='m.facebook.com / referral' OR source_medium like '%facebook%referral%') 
AND source_medium not like '%ads%';
UPDATE SEM.campaign_ad_group_pe SET channel='Twitter Referral' WHERE source_medium='t.co / referral' OR source_medium='twitter / socialmedia';
UPDATE SEM.campaign_ad_group_pe SET channel='FB Posts' WHERE source_medium='facebook / socialmedia' OR (source_medium like
'%social%media%' and source_medium<>'facebook / socialmediaads');
#SERVICIO AL CLIENTE
UPDATE SEM.campaign_ad_group_pe SET channel='Inbound' WHERE source_medium like 'Tele%' or source_medium like '%inbound%';
UPDATE SEM.campaign_ad_group_pe SET channel='OutBound' WHERE source_medium like 'REC%';
#SEO
UPDATE SEM.campaign_ad_group_pe SET channel='Search Organic' WHERE source_medium like '%organic' OR source_medium='google.com.co / referral'
OR source_medium like 'google%referral%';
#NEWSLETTER
UPDATE SEM.campaign_ad_group_pe set Channel='Newsletter' WHERE (source_medium like '%CRM' OR source_medium like '%email') AND 
(source_medium<>'TeleSales / CRM' AND source_medium not like 'CDEAL%' AND source_medium<>'Cart_Recovery / CRM' and source_medium not like 'VE%');
update SEM.campaign_ad_group_pe
set channel = 'NL CAC'
where source_medium like '%CRM'
and campaign like '%cac%';

UPDATE tbl_order_detail 
set channel='FB MP', channel_group = 'Social Media'
WHERE campaign like '%_!MP' escape '!' and source_medium='facebook / socialmedia';

UPDATE tbl_order_detail 
set channel='FBF MP' , channel_group = 'Social Media'
WHERE campaign like '%_!MP' escape '!' and source_medium='facebook / socialmedia_fashion';

UPDATE tbl_order_detail 
set channel='FB Ads MP' , channel_group = 'Facebook Ads MP'
WHERE campaign like '%_!MP' escape '!' and source_medium='facebook / socialmediaads';

UPDATE tbl_order_detail 
set channel='NL MP' , channel_group = 'NL MP'
WHERE campaign like '%_!MP' escape '!' and source_medium='postal / CRM';

UPDATE tbl_order_detail 
set channel='SEM Branded MP' , channel_group = 'SEM Branded MP'
WHERE campaign like 'brandb.%_!MP' escape '!' and source_medium='google / cpc';
#OTHER
UPDATE SEM.campaign_ad_group_pe SET channel='Exchange' WHERE source_medium like 'CCE%';
UPDATE SEM.campaign_ad_group_pe SET channel='Content Mistake' WHERE source_medium like 'CON%';
UPDATE SEM.campaign_ad_group_pe SET channel='Procurement Mistake' WHERE source_medium like 'PRC%';
UPDATE SEM.campaign_ad_group_pe SET channel='Employee Vouchers' WHERE (source_medium like 'TE%' AND source_medium
<>'TeleSales / CRM') OR source_medium like 'EA%' OR (source_medium like 'LIN%' AND source_medium not like 'linio.com%');

#PARTNERSHIPS
UPDATE SEM.campaign_ad_group_pe SET channel='Seguros Pacifico' WHERE source_medium like 'CDEALpac%';
UPDATE SEM.campaign_ad_group_pe SET channel='Romero' WHERE source_medium like 'CDEALrom%';
UPDATE SEM.campaign_ad_group_pe SET channel='Belcorp' WHERE source_medium like 'CDEALbc%';
UPDATE SEM.campaign_ad_group_pe SET channel='Email Deal' WHERE source_medium like 'CDEAL / email';
UPDATE SEM.campaign_ad_group_pe SET channel='Edifica' WHERE source_medium like 'CDEALed%';
UPDATE SEM.campaign_ad_group_pe SET channel='Tracking Villa María' WHERE source_medium='CDEALCDEALVMM' OR source_medium='CDEALVMM';
UPDATE SEM.campaign_ad_group_pe SET channel='BBVA' WHERE (source_medium like 'CDEALbb%' OR source_medium='MKT3M0' OR
source_medium='MKT09h' OR
source_medium='MKT0dV' OR
source_medium='MKT0hG' OR
source_medium='MKT0mf' OR
source_medium='MKT0Nh' OR
source_medium='MKT0ni' OR
source_medium='MKT0Q8' OR
source_medium='MKT0rg' OR
source_medium='MKT0t5' OR
source_medium='MKT0VM' OR
source_medium='MKT12' OR
source_medium='MKT17H' OR
source_medium='MKT17u' OR
source_medium='MKT18i' OR
source_medium='MKT1CU' OR
source_medium='MKT1DD' OR
source_medium='MKT1L9' OR
source_medium='MKT1n7' OR
source_medium='MKT1NG' OR
source_medium='MKT1Ql' OR
source_medium='MKT1TF' OR
source_medium='MKT1uA' OR
source_medium='MKT35s' OR
source_medium='MKT6Fy' OR
source_medium='MKT7FG' OR
source_medium='MKT8PF' OR
source_medium='MKT8pI' OR
source_medium='MKT9Ki' OR
source_medium='MKT9xQ' OR
source_medium='MKTApv' OR
source_medium='MKTauE' OR
source_medium='MKTBBVAvm4' OR
source_medium='MKTbKL' OR
source_medium='MKTEhO' OR
source_medium='MKTFEv' OR
source_medium='MKTFww' OR
source_medium='MKTiQS' OR
source_medium='MKTm1V' OR
source_medium='MKTmbS' OR
source_medium='MKTMpH' OR
source_medium='MKTtest' OR
source_medium='MKTtFM' OR
source_medium='MKTUvb' OR
source_medium='MKTuWQ' OR
source_medium='MKTzmJ') AND channel='';
UPDATE SEM.campaign_ad_group_pe SET channel='Email CDEAL' WHERE source_medium like 'CDEAL / mailing';

#DIRECT
UPDATE SEM.campaign_ad_group_pe SET channel='Linio.com Referral' WHERE  
source_medium='linio.com.co / referral' OR
source_medium='linio.com / referral' OR
source_medium='linio.com.mx / referral' OR
source_medium='linio.com.ve / referral' OR
source_medium='linio.com.pe / referral' OR
source_medium='www-staging.linio.com.ve / referral' OR
source_medium like '%linio%referal%';

UPDATE SEM.campaign_ad_group_pe SET channel='Directo / Typed' WHERE source_medium='(direct) / (none)' OR 
source_medium like '%direct%none%';

#OFFLINE
UPDATE SEM.campaign_ad_group_pe SET channel='PR vouchers' WHERE source_medium like 'PR%';

#CORPORATE SALES

#OFFLINE

#N/A
UPDATE 
SEM.campaign_ad_group_pe SET channel='Unknown-No voucher' 
WHERE channel is null;
UPDATE SEM.campaign_ad_group_pe SET 
channel='Unknown-Voucher' WHERE channel='' AND source_medium<>'';

#Channel Report Extra Stuff to define Channel---
#Venta corporativa 



#Partnerships Channel Extra


#Mercado libre Orders 


#DEFINE CHANNEL GROUP

#Facebook Ads Group
UPDATE SEM.campaign_ad_group_pe SET channel_group='Facebook Ads' WHERE channel='Facebook Ads';

#SEM Group
UPDATE SEM.campaign_ad_group_pe SET channel_group='Search Engine Marketing' WHERE channel='SEM';

#Social Media Group
UPDATE SEM.campaign_ad_group_pe SET 
channel_group='Social Media' WHERE channel='Facebook Referral' 
OR channel='Twitter Referral' or channel='FB Posts';

#Retargeting Group
UPDATE SEM.campaign_ad_group_pe SET channel_group='Retargeting' WHERE channel='Sociomantic' or source_medium like '%retargeting' OR 
channel='Cart Recovery' OR channel='Vizury' OR channel='Facebook R.'  OR channel = 'VEInteractive' OR channel = 'GDN Retargeting' OR channel = 'Criteo' or channel='Triggit';

#GDN group
UPDATE SEM.campaign_ad_group_pe SET channel_group='Google Display Network' WHERE channel like 'Google Display Network'; 

#NewsLetter Group
UPDATE SEM.campaign_ad_group_pe SET channel_group='NewsLetter' WHERE channel='Newsletter'
or channel = 'NL CAC';

#SEO Group
UPDATE SEM.campaign_ad_group_pe SET channel_group='Search Engine Optimization' WHERE channel='Search Organic';

#Partnerships Group

UPDATE SEM.campaign_ad_group_pe SET channel_group='Partnerships' WHERE channel='Seguros Pacifico' OR
channel='Romero' OR channel='Email Deal' OR channel='CDEAL' OR channel='BBVA' OR channel='Belcorp' or
channel='Email CDEAL';

#Corporate Sales Group
UPDATE SEM.campaign_ad_group_pe SET channel_group='Other (identified)' WHERE channel='Corporate Sales';

#Offline Group
UPDATE SEM.campaign_ad_group_pe SET channel_group='Offline Marketing' WHERE channel='PR vouchers' or
channel='Publimetro';

#Tele Sales Group
UPDATE SEM.campaign_ad_group_pe SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales';

#Branded Group
UPDATE SEM.campaign_ad_group_pe SET channel_group='Branded' 
WHERE channel='Linio.com Referral' OR channel='Directo / Typed' OR
channel='SEM Branded';

#ML Group
UPDATE SEM.campaign_ad_group_pe SET channel_group='Mercado Libre' WHERE channel='Mercado Libre Referral' OR channel='Mercado Libre Voucher' OR channel='Otros - Mercado Libre';

#Blog Group
-- UPDATE SEM.campaign_ad_group_pe SET channel_group='Blogs' WHERE channel='Blog Linio';

#Affiliates Group
UPDATE SEM.campaign_ad_group_pe SET channel_group='Affiliates' WHERE channel='Buscape' or channel='Pampa Network';


#Other (identified) Group
UPDATE SEM.campaign_ad_group_pe SET channel_group='Other (identified)' 
WHERE channel='Referral Sites' OR channel='Reactivation Letters' 
OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' OR
channel='Exchange' OR
channel='Content Mistake' OR
channel='Procurement Mistake' OR
channel='Employee Voucher';

#Extra Channel Group
update SEM.campaign_ad_group_pe set channel_group='Other (identified)' where source_medium = 'Banner+Interno / Banner SKY';
update SEM.campaign_ad_group_pe set channel_group='Other (identified)' where source_medium = 'LA+REPUBLICA / BANNER-DESPLEGABLE';
update SEM.campaign_ad_group_pe set channel_group='Other (identified)' where source_medium = 'mail.google.com / referral';
update SEM.campaign_ad_group_pe set channel_group='Tele Sales' where source_medium = 'CS / Inbound';

#Unknown Group
UPDATE SEM.campaign_ad_group_pe SET channel_group='Non identified' WHERE channel='Unknown-No voucher' OR channel='Unknown-Voucher';

#Define Channel Type---
#TO BE DONE


-- call monthly_marketing();
-- call daily_marketing();



END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `channel_report`()
BEGIN

update production_pe.tbl_order_detail t inner join SEM.transaction_id_pe c on t.order_nr=c.transaction_id set t.source_medium= concat(c.source,' / ',c.medium),t.campaign=c.campaign where source_medium is null;

#Channel Report PERU---

#Define channel---
##UPDATE tbl_order_detail SET source='',channel='', channel_group='';
#Voucher is priority statemnt
#If voucher is empty use GA source
UPDATE tbl_order_detail SET source='' where source is null;
UPDATE tbl_order_detail SET campaign='' where campaign is null;
UPDATE tbl_order_detail SET source=source_medium;
UPDATE tbl_order_detail SET source=coupon_code where source_medium is null;
UPDATE tbl_order_detail SET source=source_medium where channel_group='Non Identified';

#NEW REGISTERS
UPDATE tbl_order_detail SET channel='New Register' 
WHERE (source like 'NL%' OR source like 'NR%' OR source like
'COM%' OR source like 'BNR%') AND source_medium is null;
UPDATE tbl_order_detail SET source=source_medium
WHERE (coupon_code like 'NL%' OR coupon_code='SINIVA' OR coupon_code like 'COM%' 
OR coupon_code like 'NR%' OR coupon_code like 'BNR%' OR coupon_code like 'MKT%')
 AND source_medium is not null;
#BLOG
-- UPDATE tbl_order_detail SET channel='Blog Linio' WHERE source like 'blog%' 
-- OR source='blog.linio.com.pe / referral' or source='blogs.peru21.pe / referral';

#REFERRAL
UPDATE tbl_order_detail SET channel='Referral Sites' WHERE source like '%referral%' 
AND source<>'facebook.com / referral'
AND source<>'m.facebook.com / referral'
AND source<>'linio.com.co / referral'
AND source<>'linio.com / referral'
AND source<>'linio.com.mx / referral'
AND source<>'google.co.ve / referral'
AND source<>'linio.com.ve / referral'
AND source<>'linio.com.pe / referral'
AND source<>'www-staging.linio.com.ve / referral'
AND source<>'google.com / referral'
AND source<>'.com.ve / referral'
AND source<>'blog.linio.com.ve / referral' AND source<>'t.co / referral'
AND source not like '%google%' AND source not like '%blog%';

UPDATE tbl_order_detail SET channel='Other Affiliates' where source like '%affiliate%' or source like '%afiliate%';
UPDATE tbl_order_detail SET channel='Trade Tracker' where source like '%tradetracker%';

UPDATE tbl_order_detail SET channel='Buscape' 
WHERE source='Buscape / Price Comparison' OR source like '%buscape%';

UPDATE tbl_order_detail set channel='SoloCPM' where source like '%solocpm%';

#FB ADS
UPDATE tbl_order_detail SET channel='Facebook Ads' WHERE source='facebook / socialmediaads' OR source 
like '%facebook%ads%' or source='facebook / (not set)';

UPDATE tbl_order_detail set channel= 'Dark Post' where campaign like '%darkpost%';

#Criteo
UPDATE tbl_order_detail set channel='Criteo' where source like '%criteo%';

#PAMPA
UPDATE tbl_order_detail set channel='Pampa Network' where source like '%pampa%';

UPDATE tbl_order_detail set channel='PromoDescuentos' where source like '%promodescuentos%';

#SEM - GDN

UPDATE tbl_order_detail SET channel='Bing' where source like '%bing%';
UPDATE tbl_order_detail SET channel='SEM' WHERE source='google / cpc' AND 
(campaign not like 'brandb%' and campaign not like 'er.%' and campaign not like '[D%');
UPDATE tbl_order_detail SET channel='SEM Branded' WHERE (source='google / cpc' or source like '%bing%') AND campaign like 'brandb%';
UPDATE tbl_order_detail SET channel='Other Branded' WHERE source = '201.151.86.164' or source ='10.0.0.9' or source = 'www-staging.linio.com.mx' or source='blog.linio.com.mx' or source='213.229.186.15';

#GDN
UPDATE tbl_order_detail SET channel='Google Display Network' WHERE source='google / cpc' AND 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%');

#RETARGETING
UPDATE tbl_order_detail set channel='Other Retargeting' where source like '%retargeting%';
UPDATE tbl_order_detail SET channel='Sociomantic' WHERE source='sociomantic / (not set)' OR source='sociomantic / retargeting' or source='facebook / retargeting';
UPDATE tbl_order_detail SET channel='Cart Recovery' WHERE source='Cart_Recovery / CRM';
UPDATE tbl_order_detail SET channel='Vizury' WHERE source='vizury / retargeting' OR source like '%vizury%';
#UPDATE tbl_order_detail SET channel='Facebook R.' WHERE source='facebook / retargeting';
UPDATE tbl_order_detail SET channel='VEInteractive' WHERE source like 'VEInteractive%';
UPDATE tbl_order_detail SET channel='Main ADV' where source = 'retargeting / mainadv' or source like '%mainadv%' or source like '%solocpm%';
UPDATE tbl_order_detail SET channel='AdRoll' where source like '%adroll%';
UPDATE tbl_order_detail SET channel='Triggit' where source like '%triggit / retargeting%';


#SOCIAL MEDIA
UPDATE tbl_order_detail SET channel='Facebook Referral' WHERE (source='facebook.com / referral' OR source='m.facebook.com / referral' OR source like '%facebook%referral%') 
AND source not like '%ads%';
UPDATE tbl_order_detail SET channel='Twitter Referral' WHERE source='t.co / referral';
UPDATE tbl_order_detail SET channel='FB Posts' WHERE source='facebook / socialmedia' OR (source like
'%social%media%' and source<>'facebook / socialmediaads') or source='twitter / socialmedia';
UPDATE tbl_order_detail SET channel='Youtube' where source='youtube / socialmedia';
#SERVICIO AL CLIENTE
UPDATE tbl_order_detail set channel='Other Tele Sales' where source = 'CS /%' or source = '%/ CS' or source like 'telesales /%' or source like '%/ telesales'; 
UPDATE tbl_order_detail SET channel='Inbound' WHERE source like 'Tele%' or source like '%inbound%';
UPDATE tbl_order_detail SET channel='OutBound' WHERE source like 'REC%';
#SEO
UPDATE tbl_order_detail SET channel='Search Organic' WHERE source like '%organic' OR source='google.com.co / referral'
OR source like 'google%referral%';

UPDATE tbl_order_detail SET channel='Search Organic' where source like 'google.%' and source like '%referral';

UPDATE tbl_order_detail SET channel='Yahoo Answers' where source like '%answers.yahoo.com%';

#NEWSLETTER
UPDATE tbl_order_detail set Channel='Newsletter' WHERE (source like '%CRM' OR source like '%email') AND 
(source<>'TeleSales / CRM' AND source not like 'CDEAL%' AND source<>'Cart_Recovery / CRM' and source not like 'VE%');

#OTHER
UPDATE tbl_order_detail SET channel='Exchange' WHERE source like 'CCE%';
UPDATE tbl_order_detail SET channel='Content Mistake' WHERE source like 'CON%';
UPDATE tbl_order_detail SET channel='Procurement Mistake' WHERE source like 'PRC%';
UPDATE tbl_order_detail SET channel='Employee Vouchers' WHERE (source like 'TE%' AND source
<>'TeleSales / CRM') OR source like 'EA%' OR (source like 'LIN%' AND source not like 'linio.com%');

#PARTNERSHIPS
UPDATE tbl_order_detail SET channel='Other Partnerships' where source like '%PARTNERSHIP%' or coupon_code like '%PARTNERSHIP%';

UPDATE tbl_order_detail SET channel='Seguros Pacifico' WHERE source like 'CDEALpac%';
UPDATE tbl_order_detail SET channel='Romero' WHERE source like 'CDEALrom%';
UPDATE tbl_order_detail SET channel='Belcorp' WHERE source like 'CDEALbc%';
UPDATE tbl_order_detail SET channel='Email Deal' WHERE source like 'CDEAL / email';
UPDATE tbl_order_detail SET channel='Edifica' WHERE source like 'CDEALed%';
UPDATE tbl_order_detail SET channel='Tracking Villa María' WHERE source='CDEALCDEALVMM' OR source='CDEALVMM';
UPDATE tbl_order_detail SET channel='BBVA' WHERE (source like 'CDEALbb%' OR source='MKT3M0' OR
source='MKT09h' OR
source='MKT0dV' OR
source='MKT0hG' OR
source='MKT0mf' OR
source='MKT0Nh' OR
source='MKT0ni' OR
source='MKT0Q8' OR
source='MKT0rg' OR
source='MKT0t5' OR
source='MKT0VM' OR
source='MKT12' OR
source='MKT17H' OR
source='MKT17u' OR
source='MKT18i' OR
source='MKT1CU' OR
source='MKT1DD' OR
source='MKT1L9' OR
source='MKT1n7' OR
source='MKT1NG' OR
source='MKT1Ql' OR
source='MKT1TF' OR
source='MKT1uA' OR
source='MKT35s' OR
source='MKT6Fy' OR
source='MKT7FG' OR
source='MKT8PF' OR
source='MKT8pI' OR
source='MKT9Ki' OR
source='MKT9xQ' OR
source='MKTApv' OR
source='MKTauE' OR
source='MKTBBVAvm4' OR
source='MKTbKL' OR
source='MKTEhO' OR
source='MKTFEv' OR
source='MKTFww' OR
source='MKTiQS' OR
source='MKTm1V' OR
source='MKTmbS' OR
source='MKTMpH' OR
source='MKTtest' OR
source='MKTtFM' OR
source='MKTUvb' OR
source='MKTuWQ' OR
source='MKTzmJ') AND channel='';
UPDATE tbl_order_detail SET channel='Email CDEAL' WHERE source like 'CDEAL / mailing';

#DIRECT
UPDATE tbl_order_detail SET channel='Linio.com Referral' WHERE  
source='linio.com.co / referral' OR
source='linio.com / referral' OR
source='linio.com.mx / referral' OR
source='linio.com.ve / referral' OR
source='linio.com.pe / referral' OR
source='www-staging.linio.com.ve / referral' OR
source like '%linio%referal%';

UPDATE tbl_order_detail SET channel='Directo / Typed' WHERE source='(direct) / (none)' OR 
source like '%direct%none%';

#OFFLINE
UPDATE tbl_order_detail SET channel='PR vouchers' WHERE source like 'PR%';
UPDATE tbl_order_detail SET channel='Publimetro' WHERE description_voucher like '%publimetro%';
UPDATE tbl_order_detail SET channel='TV' WHERE coupon_code = 'enemigos';

#CORPORATE SALES
UPDATE tbl_order_detail set channel='Corporate Sales' where coupon_code LIKE 'CDEAL%' or source_medium like '%CORPORATESALES%';

#OFFLINE

#N/A
UPDATE 
tbl_order_detail SET channel='Unknown-No voucher' 
WHERE source='';
UPDATE tbl_order_detail SET 
channel='Unknown-Voucher' WHERE channel='' AND source<>'';

#CAC Vouchers
##UPDATE tbl_order_detail set channel = 'CAC Deals' where (coupon_code like 'CAC%') and campaign not in ('PER_fanpage_m_22-50_fashion_20130624ZapatillasCocaCola__', 'PER_fanpage__22-40_fashion_20130617zapatillascocacola__');

UPDATE tbl_order_detail set channel='FB Ads CAC' WHERE (coupon_code like 'CAC%') and source_medium='facebook / socialmediaads';
UPDATE tbl_order_detail set channel='FB CAC' WHERE (coupon_code like 'CAC%') and source_medium='facebook / socialmedia';
UPDATE tbl_order_detail set channel='NL CAC' WHERE coupon_code like 'CAC%' and source_medium='postal / crm';


UPDATE tbl_order_detail 
set channel='FB MP', channel_group = 'Social Media'
WHERE campaign like '%_!MP' escape '!' and source_medium='facebook / socialmedia';

UPDATE tbl_order_detail 
set channel='FBF MP' , channel_group = 'Social Media'
WHERE campaign like '%_!MP' escape '!' and source_medium='facebook / socialmedia_fashion';

UPDATE tbl_order_detail 
set channel='FB Ads MP' , channel_group = 'Facebook Ads MP'
WHERE campaign like '%_!MP' escape '!' and source_medium='facebook / socialmediaads';

UPDATE tbl_order_detail 
set channel='NL MP' , channel_group = 'NL MP'
WHERE campaign like '%_!MP' escape '!' and source_medium='postal / CRM';

UPDATE tbl_order_detail 
set channel='SEM Branded MP' , channel_group = 'SEM Branded MP'
WHERE campaign like 'brandb.%_!MP' escape '!' and source_medium='google / cpc';

#Channel Report Extra Stuff to define Channel---
#Venta corporativa 

#Curebit
UPDATE tbl_order_detail set channel = 'Curebit' where coupon_code like '%cbit%' or source_medium like '%curebit%' or description_voucher like 'curebit%';

UPDATE tbl_order_detail t inner join mobileapp_transaction_id m on t.order_nr=m.transaction_id set channel = 'Mobile App';

#Partnerships Channel Extra


#Mercado libre Orders 


#DEFINE CHANNEL GROUP

UPDATE tbl_order_detail SET channel_group='Mobile App' WHERE channel='Mobile App';

##UPDATE tbl_order_detail set channel_group ='Corporate Sales' where channel='Corporate Sales';

#Facebook Ads Group
UPDATE tbl_order_detail SET channel_group='Facebook Ads' WHERE channel='Facebook Ads' or channel = 'Dark Post' or channel='Facebook Referral' or channel='FB Ads CAC';

#SEM Group
UPDATE tbl_order_detail SET channel_group='Search Engine Marketing' WHERE channel='SEM' or channel='Bing' or channel='Other Branded';

#Social Media Group
UPDATE tbl_order_detail SET 
channel_group='Social Media' WHERE channel='Twitter Referral' or channel='FB Posts' or channel='Youtube' or channel='FB CAC';

#Referal Platform
UPDATE tbl_order_detail set channel_group = 'Referal Platform' where channel='Curebit';

#Retargeting Group
UPDATE tbl_order_detail SET channel_group='Retargeting' WHERE channel='Sociomantic' or source like '%retargeting' OR 
channel='Cart Recovery' OR channel='Vizury' OR channel='Facebook R.'  OR channel = 'VEInteractive' OR channel = 'GDN Retargeting' OR channel = 'Criteo' or channel='Main ADV' or channel='AdRoll' or channel='Other Retargeting' or channel='Triggit';

#GDN group
UPDATE tbl_order_detail SET channel_group='Google Display Network' WHERE channel like 'Google Display Network'; 

#NewsLetter Group
UPDATE tbl_order_detail SET channel_group='NewsLetter' WHERE channel='Newsletter' or channel='NL CAC';

#CAC Deals Group
##UPDATE tbl_order_detail set channel_group='CAC Deals' where channel='CAC Deals' or channel='FB Ads CAC' or channel='FB CAC' or channel='NL CAC';

#SEO Group
UPDATE tbl_order_detail SET channel_group='Search Engine Optimization' WHERE channel='Search Organic' or channel='Yahoo Answers';

#Partnerships Group

UPDATE tbl_order_detail SET channel_group='Other (identified)' WHERE channel='Seguros Pacifico' OR
channel='Romero' OR channel='Email Deal' OR channel='CDEAL' OR channel='BBVA' OR channel='Belcorp' or
channel='Email CDEAL' or channel='Corporate Sales';

#Corporate Sales Group
UPDATE tbl_order_detail SET channel_group='Other (identified)' WHERE channel='Corporate Sales';

#Offline Group
UPDATE tbl_order_detail SET channel_group='Offline Marketing' WHERE channel='PR vouchers' or
channel='Publimetro' or channel='TV';

#Tele Sales Group
UPDATE tbl_order_detail SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales' or channel='Other Tele Sales';

#Branded Group
UPDATE tbl_order_detail SET channel_group='Branded' 
WHERE channel='Linio.com Referral' OR channel='Directo / Typed' OR
channel='SEM Branded';

#ML Group
UPDATE tbl_order_detail SET channel_group='Mercado Libre' WHERE channel='Mercado Libre Referral' OR channel='Mercado Libre Voucher' OR channel='Otros - Mercado Libre';

#Blog Group
-- UPDATE tbl_order_detail SET channel_group='Blogs' WHERE channel='Blog Linio';

#Affiliates Group
UPDATE tbl_order_detail SET channel_group='Affiliates' WHERE channel='Buscape' or channel='Pampa Network' or channel='Other Affiliates' or channel='SoloCPM' or channel='PromoDescuentos' or channel='Trade Tracker';


#Other (identified) Group
UPDATE tbl_order_detail SET channel_group='Other (identified)' 
WHERE channel='Referral Sites' OR channel='Reactivation Letters' 
OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' OR
channel='Exchange' OR
channel='Content Mistake' OR
channel='Procurement Mistake' OR
channel='Employee Voucher';

#Extra Channel Group
update tbl_order_detail set channel_group='Other (identified)' where source_medium = 'Banner+Interno / Banner SKY';
update tbl_order_detail set channel_group='Other (identified)' where source_medium = 'LA+REPUBLICA / BANNER-DESPLEGABLE';
update tbl_order_detail set channel_group='Other (identified)' where source_medium = 'mail.google.com / referral';
update tbl_order_detail set channel_group='Tele Sales' where source_medium = 'CS / Inbound';

#Unknown Group
UPDATE tbl_order_detail SET channel_group='Non identified' WHERE channel='Unknown-No voucher' OR channel='Unknown-Voucher';

#Define Channel Type---
#TO BE DONE

#Ownership

UPDATE tbl_order_detail set Ownership='' where channel_group='Social Media';
UPDATE tbl_order_detail set Ownership='' where channel_group='NewsLetter';
UPDATE tbl_order_detail set Ownership='' where channel_group='Search Engine Optimization';
UPDATE tbl_order_detail set Ownership='' where channel_group='Blogs';
UPDATE tbl_order_detail set Ownership='' where channel_group='Offline Marketing';
UPDATE tbl_order_detail set Ownership='' where channel_group='Partnerships';
UPDATE tbl_order_detail set Ownership='' where channel_group='Mercado Libre';
UPDATE tbl_order_detail set Ownership='' where channel_group='Tele Sales';

-- call channel_report_no_voucher();
-- call visits_costs_channel();
-- call monthly_marketing();
-- call daily_marketing();



END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `channel_report_no_voucher`()
BEGIN
#Channel Report PERU---

#ga_cost_Campaign
select @last_date:=max(date) from ga_cost_campaign where source = 'vizury';

insert into ga_cost_campaign(date, source, medium, campaign, adCost)
SELECT date, "Vizury" as source, "Retargeting" as medium, "(not set)" as campaign, (sum(paid_price_after_vat) +sum(shipping_fee_after_vat) )*0.08 as cost
from tbl_order_detail
where source_medium like 'vizury /%' and obc = 1 and date > @last_date
group by date
order by date desc;

select @last_date:=max(date) from ga_cost_campaign where source like 'VEInteractive%';

insert into ga_cost_campaign(date, source, medium, campaign, adCost)
SELECT date, "VEInteractive" as source, "Retargeting" as medium, "(not set)" as campaign, (sum(paid_price_after_vat) +sum(shipping_fee_after_vat) )*0.085 as cost
from tbl_order_detail
where `source_medium` like 'VEInteractive%' and obc = 1 and date > @last_date
group by date
order by date desc;

#ga_visits_cost_source_medium

#Define channel---
update ga_visits_cost_source_medium set source_medium = concat( source, ' / ', medium) where source_medium is null;

#NEW REGISTERS
UPDATE ga_visits_cost_source_medium SET channel='New Register' 
WHERE (source_medium like 'NL%' OR source_medium like 'NR%' OR source_medium like
'COM%' OR source_medium like 'BNR%') AND source_medium is null;
#BLOG
UPDATE ga_visits_cost_source_medium SET channel='Blog Linio' WHERE source_medium like 'blog%' 
OR source_medium='blog.linio.com.pe / referral' or source_medium='blogs.peru21.pe / referral';

#REFERRAL
UPDATE ga_visits_cost_source_medium SET channel='Referral Sites' WHERE source_medium like '%referral' 
AND source_medium<>'facebook.com / referral'
AND source_medium<>'m.facebook.com / referral'
AND source_medium<>'linio.com.co / referral'
AND source_medium<>'linio.com / referral'
AND source_medium<>'linio.com.mx / referral'
AND source_medium<>'google.co.ve / referral'
AND source_medium<>'linio.com.ve / referral'
AND source_medium<>'linio.com.pe / referral'
AND source_medium<>'www-staging.linio.com.ve / referral'
AND source_medium<>'google.com / referral'
AND source_medium<>'.com.ve / referral'
AND source_medium<>'blog.linio.com.ve / referral' AND source_medium<>'t.co / referral'
AND source_medium not like '%google%' AND source_medium not like '%blog%';


UPDATE ga_visits_cost_source_medium SET channel='Buscape' 
WHERE source_medium='Buscape / Price Comparison';

#FB ADS
UPDATE ga_visits_cost_source_medium SET channel='Facebook Ads' WHERE source_medium='facebook / socialmediaads';

#RETARGETING
UPDATE ga_visits_cost_source_medium SET channel='Sociomantic' WHERE source_medium='sociomantic / (not set)' OR source_medium='sociomantic / retargeting';
UPDATE ga_visits_cost_source_medium SET channel='Cart Recovery' WHERE source_medium='Cart_Recovery / CRM';
UPDATE ga_visits_cost_source_medium SET channel='Vizury' WHERE source_medium='vizury / retargeting' OR source_medium like '%vizury%';
UPDATE ga_visits_cost_source_medium SET channel = 'Facebook R.' WHERE (source  like 'face%' or source like '%book') and medium like 'retargeting%';
UPDATE ga_visits_cost_source_medium SET channel = 'Facebook R.' WHERE (source  = 'RetargetingRM');
UPDATE ga_visits_cost_source_medium SET channel='VEInteractive' WHERE source like '%VEInteractive%';
UPDATE ga_visits_cost_source_medium SET channel='GDN Retargeting' WHERE source_medium='google / cpc' AND campaign like '[D[R%';

#SOCIAL MEDIA
UPDATE ga_visits_cost_source_medium SET channel='Facebook Referral' WHERE 
(source_medium='facebook.com / referral' OR source_medium='m.facebook.com / referral') AND source_medium not like '%socialmediaads%';
UPDATE ga_visits_cost_source_medium SET channel='Twitter Referral' WHERE source_medium='t.co / referral' OR source_medium='twitter / socialmedia';
UPDATE ga_visits_cost_source_medium SET channel='FB Posts' WHERE source_medium='facebook / socialmedia';
update ga_visits_cost_source_medium set channel = 'FB Posts' where (source like 'face%' or source like '%book') and medium like 'social%media';
UPDATE ga_visits_cost_source_medium SET channel = 'FB Posts' WHERE source = 'SocialMedia' and medium = 'FacebookVoucher';
#SERVICIO AL CLIENTE
UPDATE ga_visits_cost_source_medium SET channel='Inbound' WHERE source_medium like 'Tele%';
UPDATE ga_visits_cost_source_medium SET channel='OutBound' WHERE source_medium like 'REC%';
#SEO
#SEO
UPDATE ga_visits_cost_source_medium SET channel='Search Organic' WHERE source_medium like '%organic' OR source_medium='google.com.co / referral'
OR source_medium like 'google.%referral%';
UPDATE ga_visits_cost_source_medium SET channel='Search Organic' WHERE source like '%google%com%' and medium like 'referral%';
update ga_visits_cost_source_medium set channel = 'Search Organic' where medium like 'organic%';

#SEM - GDN

UPDATE ga_visits_cost_source_medium SET channel='SEM' WHERE source_medium='google / cpc' 
AND campaign not like 'brand%';
UPDATE ga_visits_cost_source_medium SET channel='SEM Branded' WHERE source_medium='google / cpc' 
AND campaign like 'brand%' ;
UPDATE ga_visits_cost_source_medium SET channel='GDN' WHERE source_medium='google / cpc' AND campaign not like 'b.%' AND campaign like 'r.%';
UPDATE ga_visits_cost_source_medium SET channel='GDN' WHERE source like '%.doubleclick.net%';
UPDATE ga_visits_cost_source_medium SET channel='GDN' WHERE source_medium='google / cpc' AND campaign like '[D[D%';
#NEWSLETTER,
UPDATE ga_visits_cost_source_medium set Channel='Newsletter' WHERE (source_medium like '%CRM' OR source_medium like '%email') AND 
(source<>'TeleSales / CRM' AND source_medium<>'CDEAL / email' AND source_medium<>'Cart_Recovery / CRM');

#OTHER
UPDATE ga_visits_cost_source_medium SET channel='Exchange' WHERE source_medium like 'CCE%';
UPDATE ga_visits_cost_source_medium SET channel='Content Mistake' WHERE source_medium like 'CON%';
UPDATE ga_visits_cost_source_medium SET channel='Procurement Mistake' WHERE source_medium like 'PRC%';
UPDATE ga_visits_cost_source_medium SET channel='Employee Vouchers' WHERE source_medium like 'TE%' OR source_medium like 'EA%'
 OR (source_medium like 'LIN%' AND source_medium not like '%linio%');

#PARTNERSHIPS
UPDATE ga_visits_cost_source_medium SET channel='Seguros Pacifico' WHERE source_medium like 'CDEALpac%';
UPDATE ga_visits_cost_source_medium SET channel='Romero' WHERE source_medium like 'CDEALrom%';
UPDATE ga_visits_cost_source_medium SET channel='Belcorp' WHERE source_medium like 'CDEALbc%';
UPDATE ga_visits_cost_source_medium SET channel='Email Deal' WHERE source_medium like 'CDEAL / email';
update ga_visits_cost_source_medium set channel = source where medium like 'Partne%';

update ga_visits_cost_source_medium set channel = 'BBVA' where source like 'CDEAL%';
UPDATE ga_visits_cost_source_medium SET channel='Tracking Villa María' WHERE source_medium='CDEALCDEALVMM' OR source_medium='CDEALVMM';
UPDATE ga_visits_cost_source_medium SET channel='BBVA' WHERE source like '%BBVA%' or campaign like '%BBVA%' or medium like '%BBVA%';
UPDATE ga_visits_cost_source_medium SET channel='BBVA' WHERE (source_medium like 'CDEAL%' OR source_medium='MKT3M0' OR
source_medium='MKT09h' OR
source_medium='MKT0dV' OR
source_medium='MKT0hG' OR
source_medium='MKT0mf' OR
source_medium='MKT0Nh' OR
source_medium='MKT0ni' OR
source_medium='MKT0Q8' OR
source_medium='MKT0rg' OR
source_medium='MKT0t5' OR
source_medium='MKT0VM' OR
source_medium='MKT12' OR
source_medium='MKT17H' OR
source_medium='MKT17u' OR
source_medium='MKT18i' OR
source_medium='MKT1CU' OR
source_medium='MKT1DD' OR
source_medium='MKT1L9' OR
source_medium='MKT1n7' OR
source_medium='MKT1NG' OR
source_medium='MKT1Ql' OR
source_medium='MKT1TF' OR
source_medium='MKT1uA' OR
source_medium='MKT35s' OR
source_medium='MKT6Fy' OR
source_medium='MKT7FG' OR
source_medium='MKT8PF' OR
source_medium='MKT8pI' OR
source_medium='MKT9Ki' OR
source_medium='MKT9xQ' OR
source_medium='MKTApv' OR
source_medium='MKTauE' OR
source_medium='MKTBBVAvm4' OR
source_medium='MKTbKL' OR
source_medium='MKTEhO' OR
source_medium='MKTFEv' OR
source_medium='MKTFww' OR
source_medium='MKTiQS' OR
source_medium='MKTm1V' OR
source_medium='MKTmbS' OR
source_medium='MKTMpH' OR
source_medium='MKTtest' OR
source_medium='MKTtFM' OR
source_medium='MKTUvb' OR
source_medium='MKTuWQ' OR
source_medium='MKTzmJ') AND channel='';
UPDATE ga_visits_cost_source_medium SET channel='Edifica' WHERE source_medium like 'CDEALed%';
#DIRECT
UPDATE ga_visits_cost_source_medium SET channel='Linio.com Referral' WHERE  
source_medium='linio.com.co / referral' OR
source_medium='linio.com / referral' OR
source_medium='linio.com.mx / referral' OR
source_medium='linio.com.ve / referral' OR
source_medium='linio.com.pe / referral' OR
source_medium='www-staging.linio.com.ve / referral';

UPDATE ga_visits_cost_source_medium SET channel='Directo / Typed' WHERE source_medium='(direct) / (none)' OR 
source_medium like '%direct%none%';

UPDATE ga_visits_cost_source_medium SET channel='Directo / Typed' WHERE source like 'Linio%' and medium like 'banner%' ;

#OFFLINE
UPDATE ga_visits_cost_source_medium SET channel='PR vouchers' WHERE channel like 'PR%';

#CORPORATE SALES

#OFFLINE

#N/A
UPDATE 
ga_visits_cost_source_medium SET channel='Unknown-No voucher' 
WHERE source_medium='';
UPDATE ga_visits_cost_source_medium SET 
channel='Unknown-Voucher' WHERE channel='' AND source<>'';

#Channel Report Extra Stuff to define Channel---
#Venta corporativa 



#Partnerships Channel Extra


#Mercado libre Orders 


#DEFINE CHANNEL GROUP

#Facebook Ads Group
UPDATE ga_visits_cost_source_medium SET channel_group='Facebook Ads' WHERE channel='Facebook Ads';

#SEM Group
UPDATE ga_visits_cost_source_medium SET channel_group='Search Engine Marketing' WHERE channel='SEM';

#Social Media Group
UPDATE ga_visits_cost_source_medium SET 
channel_group='Social Media' WHERE channel='Facebook Referral' 
OR channel='Twitter Referral' or channel='FB Posts';

#Retargeting Group
UPDATE ga_visits_cost_source_medium SET channel_group='Retargeting' WHERE channel='Sociomantic' or source_medium like '%retargeting' OR 
channel='Cart Recovery' OR channel='Vizury' or channel = 'Facebook R.'  OR channel = 'VEInteractive' OR channel = 'GDN Retargeting';

#GDN group
UPDATE ga_visits_cost_source_medium SET channel_group='Google Display Network' WHERE channel like 'GDN'; 

#NewsLetter Group
UPDATE ga_visits_cost_source_medium SET channel_group='NewsLetter' WHERE channel='Newsletter';

#SEO Group
UPDATE ga_visits_cost_source_medium SET channel_group='Search Engine Optimization' WHERE channel='Search Organic';

#Partnerships Group

UPDATE ga_visits_cost_source_medium SET channel_group='Partnerships' WHERE channel='Seguros Pacifico' OR
channel='Romero' OR channel='Email Deal' OR channel='CDEAL' OR channel='BBVA' OR channel='Belcorp';

#Corporate Sales Group
UPDATE ga_visits_cost_source_medium SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';

#Offline Group
UPDATE ga_visits_cost_source_medium SET channel_group='Offline Marketing' WHERE channel='PR vouchers';

#Tele Sales Group
UPDATE ga_visits_cost_source_medium SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales';

#Branded Group
UPDATE ga_visits_cost_source_medium SET channel_group='Branded' 
WHERE channel='Linio.com Referral' OR channel='Directo / Typed' OR
channel='SEM Branded';

#ML Group
UPDATE ga_visits_cost_source_medium SET channel_group='Mercado Libre' WHERE channel='Mercado Libre Referral' OR channel='Mercado Libre Voucher' OR channel='Otros - Mercado Libre';

#Blog Group
UPDATE ga_visits_cost_source_medium SET channel_group='Blogs' WHERE channel='Blog Linio';

#Affiliates Group
UPDATE ga_visits_cost_source_medium SET channel_group='Affiliates' WHERE channel='Buscape';


#Other (identified) Group
UPDATE ga_visits_cost_source_medium SET channel_group='Other (identified)' 
WHERE channel='Referral Sites' OR channel='Reactivation Letters' 
OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' OR
channel='Exchange' OR
channel='Content Mistake' OR
channel='Procurement Mistake' OR
channel='Employee Voucher';

#Unknown Group
UPDATE ga_visits_cost_source_medium SET channel_group='Non identified' WHERE channel='Unknown-No voucher' OR channel='Unknown-Voucher';





#Define Channel Type---
#TO BE DONE

#ga_cost_campaign

#Define channel---
update ga_cost_campaign set source_medium = concat( source, ' / ', medium) where source_medium is null;

#NEW REGISTERS
UPDATE ga_cost_campaign SET channel='New Register' 
WHERE (source_medium like 'NL%' OR source_medium like 'NR%' OR source_medium like
'COM%' OR source_medium like 'BNR%') AND source_medium is null;
#BLOG
UPDATE ga_cost_campaign SET channel='Blog Linio' WHERE source_medium like 'blog%' 
OR source_medium='blog.linio.com.pe / referral' or source_medium='blogs.peru21.pe / referral';

#REFERRAL
UPDATE ga_cost_campaign SET channel='Referral Sites' WHERE source_medium like '%referral' 
AND source_medium<>'facebook.com / referral'
AND source_medium<>'m.facebook.com / referral'
AND source_medium<>'linio.com.co / referral'
AND source_medium<>'linio.com / referral'
AND source_medium<>'linio.com.mx / referral'
AND source_medium<>'google.co.ve / referral'
AND source_medium<>'linio.com.ve / referral'
AND source_medium<>'linio.com.pe / referral'
AND source_medium<>'www-staging.linio.com.ve / referral'
AND source_medium<>'google.com / referral'
AND source_medium<>'.com.ve / referral'
AND source_medium<>'blog.linio.com.ve / referral' AND source_medium<>'t.co / referral'
AND source_medium not like '%google%' AND source_medium not like '%blog%';


UPDATE ga_cost_campaign SET channel='Buscape' 
WHERE source_medium='Buscape / Price Comparison';

#FB ADS
UPDATE ga_cost_campaign SET channel='Facebook Ads' WHERE source_medium='facebook / socialmediaads';

#RETARGETING
UPDATE ga_cost_campaign SET channel='Sociomantic' WHERE source_medium='sociomantic / (not set)' OR source_medium='sociomantic / retargeting';
UPDATE ga_cost_campaign SET channel='Cart Recovery' WHERE source_medium='Cart_Recovery / CRM';
UPDATE ga_cost_campaign SET channel='Vizury' WHERE source_medium='vizury / retargeting' OR source_medium like '%vizury%';
UPDATE ga_cost_campaign SET channel = 'Facebook R.' WHERE (source  like 'face%' or source like '%book') and medium like 'retargeting%';
UPDATE ga_cost_campaign SET channel = 'Facebook R.' WHERE (source  = 'RetargetingRM');
UPDATE ga_cost_campaign SET channel='VEInteractive' WHERE source like '%VEInteractive%';
UPDATE ga_cost_campaign SET channel='GDN Retargeting' WHERE source_medium='google / cpc' AND campaign like '[D[R%';

#SOCIAL MEDIA
UPDATE ga_cost_campaign SET channel='Facebook Referral' WHERE 
(source_medium='facebook.com / referral' OR source_medium='m.facebook.com / referral') AND source_medium not like '%socialmediaads%';
UPDATE ga_cost_campaign SET channel='Twitter Referral' WHERE source_medium='t.co / referral' OR source_medium='twitter / socialmedia';
UPDATE ga_cost_campaign SET channel='FB Posts' WHERE source_medium='facebook / socialmedia';
update ga_cost_campaign set channel = 'FB Posts' where (source like 'face%' or source like '%book') and medium like 'social%media';
UPDATE ga_cost_campaign SET channel = 'FB Posts' WHERE source = 'SocialMedia' and medium = 'FacebookVoucher';
#SERVICIO AL CLIENTE
UPDATE ga_cost_campaign SET channel='Inbound' WHERE source_medium like 'Tele%';
UPDATE ga_cost_campaign SET channel='OutBound' WHERE source_medium like 'REC%';
#SEO
#SEO
UPDATE ga_cost_campaign SET channel='Search Organic' WHERE source_medium like '%organic' OR source_medium='google.com.co / referral'
OR source_medium like 'google.%referral%';
UPDATE ga_cost_campaign SET channel='Search Organic' WHERE source like '%google%com%' and medium like 'referral%';
update ga_cost_campaign set channel = 'Search Organic' where medium like 'organic%';

#SEM - GDN
UPDATE ga_cost_campaign SET channel='SEM Branded' WHERE source_medium='google / cpc' AND (campaign like 'brand%');
UPDATE ga_cost_campaign SET channel='SEM' WHERE source_medium='google / cpc' AND (campaign not like 'brand%');
UPDATE ga_cost_campaign SET channel='GDN' WHERE source_medium='google / cpc' AND campaign not like 'b.%' AND campaign like 'r.%';
UPDATE ga_cost_campaign SET channel='GDN' WHERE source like '%.doubleclick.net%';
UPDATE ga_cost_campaign SET channel='GDN' WHERE source_medium='google / cpc' AND campaign like '[D[D%';
#NEWSLETTER,
UPDATE ga_cost_campaign set Channel='Newsletter' WHERE (source_medium like '%CRM' OR source_medium like '%email') AND 
(source<>'TeleSales / CRM' AND source_medium<>'CDEAL / email' AND source_medium<>'Cart_Recovery / CRM');

#OTHER
UPDATE ga_cost_campaign SET channel='Exchange' WHERE source_medium like 'CCE%';
UPDATE ga_cost_campaign SET channel='Content Mistake' WHERE source_medium like 'CON%';
UPDATE ga_cost_campaign SET channel='Procurement Mistake' WHERE source_medium like 'PRC%';
UPDATE ga_cost_campaign SET channel='Employee Vouchers' WHERE source_medium like 'TE%' OR source_medium like 'EA%'
 OR (source_medium like 'LIN%' AND source_medium not like '%linio%');

#PARTNERSHIPS
UPDATE ga_cost_campaign SET channel='Seguros Pacifico' WHERE source_medium like 'CDEALpac%';
UPDATE ga_cost_campaign SET channel='Romero' WHERE source_medium like 'CDEALrom%';
UPDATE ga_cost_campaign SET channel='Belcorp' WHERE source_medium like 'CDEALbc%';
UPDATE ga_cost_campaign SET channel='Email Deal' WHERE source_medium like 'CDEAL / email';
update ga_cost_campaign set channel = source where medium like 'Partne%';

update ga_cost_campaign set channel = 'BBVA' where source like 'CDEAL%';
UPDATE ga_cost_campaign SET channel='Tracking Villa María' WHERE source_medium='CDEALCDEALVMM' OR source_medium='CDEALVMM';
UPDATE ga_cost_campaign SET channel='BBVA' WHERE source like '%BBVA%' or campaign like '%BBVA%' or medium like '%BBVA%';
UPDATE ga_cost_campaign SET channel='BBVA' WHERE (source_medium like 'CDEAL%' OR source_medium='MKT3M0' OR
source_medium='MKT09h' OR
source_medium='MKT0dV' OR
source_medium='MKT0hG' OR
source_medium='MKT0mf' OR
source_medium='MKT0Nh' OR
source_medium='MKT0ni' OR
source_medium='MKT0Q8' OR
source_medium='MKT0rg' OR
source_medium='MKT0t5' OR
source_medium='MKT0VM' OR
source_medium='MKT12' OR
source_medium='MKT17H' OR
source_medium='MKT17u' OR
source_medium='MKT18i' OR
source_medium='MKT1CU' OR
source_medium='MKT1DD' OR
source_medium='MKT1L9' OR
source_medium='MKT1n7' OR
source_medium='MKT1NG' OR
source_medium='MKT1Ql' OR
source_medium='MKT1TF' OR
source_medium='MKT1uA' OR
source_medium='MKT35s' OR
source_medium='MKT6Fy' OR
source_medium='MKT7FG' OR
source_medium='MKT8PF' OR
source_medium='MKT8pI' OR
source_medium='MKT9Ki' OR
source_medium='MKT9xQ' OR
source_medium='MKTApv' OR
source_medium='MKTauE' OR
source_medium='MKTBBVAvm4' OR
source_medium='MKTbKL' OR
source_medium='MKTEhO' OR
source_medium='MKTFEv' OR
source_medium='MKTFww' OR
source_medium='MKTiQS' OR
source_medium='MKTm1V' OR
source_medium='MKTmbS' OR
source_medium='MKTMpH' OR
source_medium='MKTtest' OR
source_medium='MKTtFM' OR
source_medium='MKTUvb' OR
source_medium='MKTuWQ' OR
source_medium='MKTzmJ') AND channel='';
UPDATE ga_cost_campaign SET channel='Edifica' WHERE source_medium like 'CDEALed%';
#DIRECT
UPDATE ga_cost_campaign SET channel='Linio.com Referral' WHERE  
source_medium='linio.com.co / referral' OR
source_medium='linio.com / referral' OR
source_medium='linio.com.mx / referral' OR
source_medium='linio.com.ve / referral' OR
source_medium='linio.com.pe / referral' OR
source_medium='www-staging.linio.com.ve / referral';

UPDATE ga_cost_campaign SET channel='Directo / Typed' WHERE source_medium='(direct) / (none)' OR 
source_medium like '%direct%none%';

UPDATE ga_cost_campaign SET channel='Directo / Typed' WHERE source like 'Linio%' and medium like 'banner%' ;

#OFFLINE
UPDATE ga_cost_campaign SET channel='PR vouchers' WHERE channel like 'PR%';

#CORPORATE SALES

#OFFLINE

#N/A
UPDATE 
ga_cost_campaign SET channel='Unknown-No voucher' 
WHERE source_medium='';
UPDATE ga_cost_campaign SET 
channel='Unknown-Voucher' WHERE channel='' AND source<>'';

#Channel Report Extra Stuff to define Channel---
#Venta corporativa 



#Partnerships Channel Extra


#Mercado libre Orders 


#DEFINE CHANNEL GROUP

#Facebook Ads Group
UPDATE ga_cost_campaign SET channel_group='Facebook Ads' WHERE channel='Facebook Ads';

#SEM Group
UPDATE ga_cost_campaign SET channel_group='Search Engine Marketing' WHERE channel='SEM';

#Social Media Group
UPDATE ga_cost_campaign SET 
channel_group='Social Media' WHERE channel='Facebook Referral' 
OR channel='Twitter Referral' or channel='FB Posts';

#Retargeting Group
UPDATE ga_cost_campaign SET channel_group='Retargeting' WHERE channel='Sociomantic' or source_medium like '%retargeting' OR 
channel='Cart Recovery' OR channel='Vizury' or channel = 'Facebook R.'  OR channel = 'VEInteractive' OR channel = 'GDN Retargeting';

#GDN Group
UPDATE ga_cost_campaign SET channel_group='Google Display Network' WHERE channel like 'GDN'; 

#NewsLetter Group
UPDATE ga_cost_campaign SET channel_group='NewsLetter' WHERE channel='Newsletter';

#SEO Group
UPDATE ga_cost_campaign SET channel_group='Search Engine Optimization' WHERE channel='Search Organic';

#Partnerships Group

UPDATE ga_cost_campaign SET channel_group='Partnerships' WHERE channel='Seguros Pacifico' OR
channel='Romero' OR channel='Email Deal' OR channel='CDEAL' OR channel='BBVA' OR channel='Belcorp';

#Corporate Sales Group
UPDATE ga_cost_campaign SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';

#Offline Group
UPDATE ga_cost_campaign SET channel_group='Offline Marketing' WHERE channel='PR vouchers';

#Tele Sales Group
UPDATE ga_cost_campaign SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales';

#Branded Group
UPDATE ga_cost_campaign SET channel_group='Branded' 
WHERE channel='Linio.com Referral' OR channel='Directo / Typed' OR
channel='SEM Branded';

#ML Group
UPDATE ga_cost_campaign SET channel_group='Mercado Libre' WHERE channel='Mercado Libre Referral' OR channel='Mercado Libre Voucher' OR channel='Otros - Mercado Libre';

#Blog Group
UPDATE ga_cost_campaign SET channel_group='Blogs' WHERE channel='Blog Linio';

#Affiliates Group
UPDATE ga_cost_campaign SET channel_group='Affiliates' WHERE channel='Buscape';


#Other (identified) Group
UPDATE ga_cost_campaign SET channel_group='Other (identified)' 
WHERE channel='Referral Sites' OR channel='Reactivation Letters' 
OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' OR
channel='Exchange' OR
channel='Content Mistake' OR
channel='Procurement Mistake' OR
channel='Employee Voucher';

#Unknown Group
UPDATE ga_cost_campaign SET channel_group='Non identified' WHERE channel='Unknown-No voucher' OR channel='Unknown-Voucher';




END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `cheating_list`()
begin

select  'Cheating List: start',now();

delete from production_pe.cheating_list;

insert into production_pe.cheating_list(date, custid, first_name, last_name, email, order_nr, coupon_code, address1, address2, street_number, city, postcode, phone_number) select t.date, t.custid, c.first_name, c.last_name, c.email, t.order_nr, t.coupon_code, a.address1, a.address2, a.street_number, a.city, a.postcode, a.phone from production_pe.tbl_order_detail t, bob_live_pe.customer c, bob_live_pe.customer_address a where t.custid=c.id_customer and t.custid=a.fk_customer and t.obc=1 and t.coupon_code like 'CAC%' group by order_nr, phone, address1, address2, street_number, city order by custid;

update production_pe.cheating_list c set actual_paid_price = (select sum(paid_price) from production_pe.tbl_order_detail t where t.order_nr=c.order_nr);

update production_pe.cheating_list c inner join bob_live_pe.sales_order o on c.order_nr=o.order_nr set c.ip_address=o.ip;

update production_pe.cheating_list c inner join bob_live_pe.sales_order o on c.order_nr=o.order_nr inner join bob_live_pe.sales_order_accertify_log g on o.id_sales_order=g.fk_sales_order set credit_card=ExtractValue(g.message,'/transaction/paymentInformation/cardNumber');

-- IP Address

create table production_pe.temporary_cheat(
ip_address varchar(255),
custid int
);

create index temporary_cheat on production_pe.temporary_cheat(ip_address, custid);

insert into production_pe.temporary_cheat select ip, fk_customer from bob_live_pe.sales_order;

update production_pe.cheating_list c inner join production_pe.temporary_cheat o on c.ip_address=o.ip_address set c.ip_address_cheat = 1 where c.custid!=o.custid; 

drop table production_pe.temporary_cheat;

-- Address

create table production_pe.temporary_cheat(
address1 varchar(255),
address2 varchar(255),
street_number varchar(255),
city varchar(255),
custid int
);

create index temporary_cheat on production_pe.temporary_cheat(address1, address2, city, custid);

insert into production_pe.temporary_cheat select address1, address2, street_number, city, fk_customer from bob_live_pe.customer_address;

update production_pe.cheating_list c inner join production_pe.temporary_cheat o on c.address1=o.address1 and c.address2=o.address2 and c.street_number=o.street_number and c.city=o.city set c.address_cheat = 1 where c.custid!=o.custid;

drop table production_pe.temporary_cheat;

-- Phone Number

create table production_pe.temporary_cheat(
phone_number varchar(255),
custid int
);

create index temporary_cheat on production_pe.temporary_cheat(phone_number, custid);

insert into production_pe.temporary_cheat select phone, fk_customer from bob_live_pe.customer_address;

update production_pe.cheating_list c inner join production_pe.temporary_cheat o on c.phone_number=o.phone_number set c.phone_number_cheat = 1 where c.custid!=o.custid; 

drop table production_pe.temporary_cheat;

-- Same Name

create table production_pe.temporary_cheat(
first_name varchar(255),
last_name varchar(255),
postcode int,
custid int
);

create index temporary_cheat on production_pe.temporary_cheat(first_name, last_name, custid, postcode);

insert into production_pe.temporary_cheat select c.first_name, c.last_name, a.postcode, c.id_customer from bob_live_pe.customer c, bob_live_pe.customer_address a where c.id_customer=a.fk_customer;

update production_pe.cheating_list c inner join production_pe.temporary_cheat o on c.first_name=o.first_name and c.last_name=o.last_name and c.postcode=o.postcode set c.same_name_cheat = 1 where c.custid!=o.custid; 

drop table production_pe.temporary_cheat;

-- Credit Card

create table production_pe.temporary_cheat(
credit_card varchar(255),
custid int
);

create index temporary_cheat on production_pe.temporary_cheat(credit_card, custid);

insert into production_pe.temporary_cheat select ExtractValue(g.message,'/transaction/paymentInformation/cardNumber'), fk_customer from bob_live_pe.sales_order o, bob_live_pe.sales_order_accertify_log g where o.id_sales_order=g.fk_sales_order;

update production_pe.cheating_list c inner join production_pe.temporary_cheat o on c.credit_card=o.credit_card set c.credit_card_cheat = 1 where c.custid!=o.custid; 

drop table production_pe.temporary_cheat;

-- Linio Mail

update production_pe.cheating_list set linio_mail_cheat = 1 where email like '%@linio%';

-- delete from production_pe.cheating_list where ip_address_cheat is null and address_cheat is null and phone_number_cheat is null and same_name_cheat is null and credit_card_cheat is null and linio_mail_cheat is null;

update production_pe.cheating_list set ip_address_cheat=0 where ip_address_cheat is null;

update production_pe.cheating_list set address_cheat=0 where address_cheat is null;

update production_pe.cheating_list set phone_number_cheat=0 where phone_number_cheat is null;

update production_pe.cheating_list set same_name_cheat=0 where same_name_cheat is null;

update production_pe.cheating_list set credit_card_cheat=0 where credit_card_cheat is null;

update production_pe.cheating_list set linio_mail_cheat=0 where linio_mail_cheat is null;

select  'Cheating List: stop',now();

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `clv_monthly`()
begin

delete from production_pe.clv_monthly;

insert into production_pe.clv_monthly(custid, yrmonth, date, voucher, net_revenue, PC2, nr_items) select customer_num, yrmonth, date, coupon_code, sum(ifnull(price_after_tax,0)-ifnull(after_vat_coupon,0)+ifnull(shipping_fee_charged,0)), sum(ifnull(price_after_tax,0)-ifnull(after_vat_coupon,0)+ifnull(shipping_fee_charged,0)-ifnull(cost_after_tax,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_fees,0)-ifnull(wh_cost_per_item,0)-ifnull(cs_cost_per_item,0)) as PC2, count(orderNum) from production_pe.out_sales_report_item z where oac=1 and new_returning ='New' group by customer_num;

update production_pe.clv_monthly b set nr_unique_repurcharse_same_month = (select case when count(distinct t.orderNum)>=1 then 1 else 0 end from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and b.yrmonth=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set transactions_same_month = (select count(distinct t.orderNum) from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and b.yrmonth=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set nr_items_same_month = (select count(t.orderNum) from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and b.yrmonth=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set net_revenue_same_month = (select sum(ifnull(t.price_after_tax,0)-ifnull(t.after_vat_coupon,0)+ifnull(t.shipping_fee_charged,0)) from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and b.yrmonth=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set PC2_same_month = (select sum(ifnull(price_after_tax,0)-ifnull(after_vat_coupon,0)+ifnull(shipping_fee_charged,0)-ifnull(cost_after_tax,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_fees,0)-ifnull(wh_cost_per_item,0)-ifnull(cs_cost_per_item,0)) as PC2 from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and b.yrmonth=t.yrmonth group by b.custid);

-- 30 Days

update production_pe.clv_monthly b set nr_unique_repurcharse_30 = (select case when count(distinct t.orderNum)>=1 then 1 else 0 end from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 1 month)),if(month(date_add(b.date, interval 1 month))<10,concat(0,month(date_add(b.date, interval 1 month))),month(date_add(b.date, interval 1 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set transactions_30 = (select count(distinct t.orderNum) from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 1 month)),if(month(date_add(b.date, interval 1 month))<10,concat(0,month(date_add(b.date, interval 1 month))),month(date_add(b.date, interval 1 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set nr_items_30 = (select count(t.orderNum) from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 1 month)),if(month(date_add(b.date, interval 1 month))<10,concat(0,month(date_add(b.date, interval 1 month))),month(date_add(b.date, interval 1 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set net_revenue_30 = (select sum(ifnull(t.price_after_tax,0)-ifnull(t.after_vat_coupon,0)+ifnull(t.shipping_fee_charged,0)) from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 1 month)),if(month(date_add(b.date, interval 1 month))<10,concat(0,month(date_add(b.date, interval 1 month))),month(date_add(b.date, interval 1 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set PC2_30 = (select sum(ifnull(price_after_tax,0)-ifnull(after_vat_coupon,0)+ifnull(shipping_fee_charged,0)-ifnull(cost_after_tax,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_fees,0)-ifnull(wh_cost_per_item,0)-ifnull(cs_cost_per_item,0)) as PC2 from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 1 month)),if(month(date_add(b.date, interval 1 month))<10,concat(0,month(date_add(b.date, interval 1 month))),month(date_add(b.date, interval 1 month))))>=t.yrmonth group by b.custid);

-- 60 Days

update production_pe.clv_monthly b set nr_unique_repurcharse_60 = (select case when count(distinct t.orderNum)>=1 then 1 else 0 end from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 2 month)),if(month(date_add(b.date, interval 2 month))<10,concat(0,month(date_add(b.date, interval 2 month))),month(date_add(b.date, interval 2 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set transactions_60 = (select count(distinct t.orderNum) from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 2 month)),if(month(date_add(b.date, interval 2 month))<10,concat(0,month(date_add(b.date, interval 2 month))),month(date_add(b.date, interval 2 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set nr_items_60 = (select count(t.orderNum) from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 2 month)),if(month(date_add(b.date, interval 2 month))<10,concat(0,month(date_add(b.date, interval 2 month))),month(date_add(b.date, interval 2 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set net_revenue_60 = (select sum(ifnull(t.price_after_tax,0)-ifnull(t.after_vat_coupon,0)+ifnull(t.shipping_fee_charged,0)) from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 2 month)),if(month(date_add(b.date, interval 2 month))<10,concat(0,month(date_add(b.date, interval 2 month))),month(date_add(b.date, interval 2 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set PC2_60 = (select sum(ifnull(price_after_tax,0)-ifnull(after_vat_coupon,0)+ifnull(shipping_fee_charged,0)-ifnull(cost_after_tax,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_fees,0)-ifnull(wh_cost_per_item,0)-ifnull(cs_cost_per_item,0)) as PC2 from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 2 month)),if(month(date_add(b.date, interval 2 month))<10,concat(0,month(date_add(b.date, interval 2 month))),month(date_add(b.date, interval 2 month))))>=t.yrmonth group by b.custid);

-- 90 Days

update production_pe.clv_monthly b set nr_unique_repurcharse_90 = (select case when count(distinct t.orderNum)>=1 then 1 else 0 end from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 3 month)),if(month(date_add(b.date, interval 3 month))<10,concat(0,month(date_add(b.date, interval 3 month))),month(date_add(b.date, interval 3 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set transactions_90 = (select count(distinct t.orderNum) from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 3 month)),if(month(date_add(b.date, interval 3 month))<10,concat(0,month(date_add(b.date, interval 3 month))),month(date_add(b.date, interval 3 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set nr_items_90 = (select count(t.orderNum) from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 3 month)),if(month(date_add(b.date, interval 3 month))<10,concat(0,month(date_add(b.date, interval 3 month))),month(date_add(b.date, interval 3 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set net_revenue_90 = (select sum(ifnull(t.price_after_tax,0)-ifnull(t.after_vat_coupon,0)+ifnull(t.shipping_fee_charged,0)) from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 3 month)),if(month(date_add(b.date, interval 3 month))<10,concat(0,month(date_add(b.date, interval 3 month))),month(date_add(b.date, interval 3 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set PC2_90 = (select sum(ifnull(price_after_tax,0)-ifnull(after_vat_coupon,0)+ifnull(shipping_fee_charged,0)-ifnull(cost_after_tax,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_fees,0)-ifnull(wh_cost_per_item,0)-ifnull(cs_cost_per_item,0)) as PC2 from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 3 month)),if(month(date_add(b.date, interval 3 month))<10,concat(0,month(date_add(b.date, interval 3 month))),month(date_add(b.date, interval 3 month))))>=t.yrmonth group by b.custid);

-- 120 Days

update production_pe.clv_monthly b set nr_unique_repurcharse_120 = (select case when count(distinct t.orderNum)>=1 then 1 else 0 end from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 4 month)),if(month(date_add(b.date, interval 4 month))<10,concat(0,month(date_add(b.date, interval 4 month))),month(date_add(b.date, interval 4 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set transactions_120 = (select count(distinct t.orderNum) from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 4 month)),if(month(date_add(b.date, interval 4 month))<10,concat(0,month(date_add(b.date, interval 4 month))),month(date_add(b.date, interval 4 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set nr_items_120 = (select count(t.orderNum) from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 4 month)),if(month(date_add(b.date, interval 4 month))<10,concat(0,month(date_add(b.date, interval 4 month))),month(date_add(b.date, interval 4 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set net_revenue_120 = (select sum(ifnull(t.price_after_tax,0)-ifnull(t.after_vat_coupon,0)+ifnull(t.shipping_fee_charged,0)) from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 4 month)),if(month(date_add(b.date, interval 4 month))<10,concat(0,month(date_add(b.date, interval 4 month))),month(date_add(b.date, interval 4 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set PC2_120 = (select sum(ifnull(price_after_tax,0)-ifnull(after_vat_coupon,0)+ifnull(shipping_fee_charged,0)-ifnull(cost_after_tax,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_fees,0)-ifnull(wh_cost_per_item,0)-ifnull(cs_cost_per_item,0)) as PC2 from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 4 month)),if(month(date_add(b.date, interval 4 month))<10,concat(0,month(date_add(b.date, interval 4 month))),month(date_add(b.date, interval 4 month))))>=t.yrmonth group by b.custid);

-- 150 Days

update production_pe.clv_monthly b set nr_unique_repurcharse_150 = (select case when count(distinct t.orderNum)>=1 then 1 else 0 end from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 5 month)),if(month(date_add(b.date, interval 5 month))<10,concat(0,month(date_add(b.date, interval 5 month))),month(date_add(b.date, interval 5 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set transactions_150 = (select count(distinct t.orderNum) from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 5 month)),if(month(date_add(b.date, interval 5 month))<10,concat(0,month(date_add(b.date, interval 5 month))),month(date_add(b.date, interval 5 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set nr_items_150 = (select count(t.orderNum) from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 5 month)),if(month(date_add(b.date, interval 5 month))<10,concat(0,month(date_add(b.date, interval 5 month))),month(date_add(b.date, interval 5 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set net_revenue_150 = (select sum(ifnull(t.price_after_tax,0)-ifnull(t.after_vat_coupon,0)+ifnull(t.shipping_fee_charged,0)) from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 5 month)),if(month(date_add(b.date, interval 5 month))<10,concat(0,month(date_add(b.date, interval 5 month))),month(date_add(b.date, interval 5 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set PC2_150 = (select sum(ifnull(price_after_tax,0)-ifnull(after_vat_coupon,0)+ifnull(shipping_fee_charged,0)-ifnull(cost_after_tax,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_fees,0)-ifnull(wh_cost_per_item,0)-ifnull(cs_cost_per_item,0)) as PC2 from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 5 month)),if(month(date_add(b.date, interval 5 month))<10,concat(0,month(date_add(b.date, interval 5 month))),month(date_add(b.date, interval 5 month))))>=t.yrmonth group by b.custid);

-- 180 Days

update production_pe.clv_monthly b set nr_unique_repurcharse_180 = (select case when count(distinct t.orderNum)>=1 then 1 else 0 end from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 6 month)),if(month(date_add(b.date, interval 6 month))<10,concat(0,month(date_add(b.date, interval 6 month))),month(date_add(b.date, interval 6 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set transactions_180 = (select count(distinct t.orderNum) from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 6 month)),if(month(date_add(b.date, interval 6 month))<10,concat(0,month(date_add(b.date, interval 6 month))),month(date_add(b.date, interval 6 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set nr_items_180 = (select count(t.orderNum) from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 6 month)),if(month(date_add(b.date, interval 6 month))<10,concat(0,month(date_add(b.date, interval 6 month))),month(date_add(b.date, interval 6 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set net_revenue_180 = (select sum(ifnull(t.price_after_tax,0)-ifnull(t.after_vat_coupon,0)+ifnull(t.shipping_fee_charged,0)) from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 6 month)),if(month(date_add(b.date, interval 6 month))<10,concat(0,month(date_add(b.date, interval 6 month))),month(date_add(b.date, interval 6 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly b set PC2_180 = (select sum(ifnull(price_after_tax,0)-ifnull(after_vat_coupon,0)+ifnull(shipping_fee_charged,0)-ifnull(cost_after_tax,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_fees,0)-ifnull(wh_cost_per_item,0)-ifnull(cs_cost_per_item,0)) as PC2 from production_pe.out_sales_report_item t where t.customer_num=b.custid and t.oac=1 and t.new_returning != 'New' and concat(year(date_add(b.date, interval 6 month)),if(month(date_add(b.date, interval 6 month))<10,concat(0,month(date_add(b.date, interval 6 month))),month(date_add(b.date, interval 6 month))))>=t.yrmonth group by b.custid);

update production_pe.clv_monthly set net_revenue=net_revenue/3.3, net_revenue_same_month=net_revenue_same_month/3.3, net_revenue_30=net_revenue_30/3.3, net_revenue_60=net_revenue_60/3.3, net_revenue_90=net_revenue_90/3.3, net_revenue_120=net_revenue_120/3.3, net_revenue_150=net_revenue_150/3.3, net_revenue_180=net_revenue_180/3.3, PC2=PC2/3.3, PC2_same_month=PC2_same_month/3.3, PC2_30=PC2_30/3.3, PC2_60=PC2_60/3.3, PC2_90=PC2_90/3.3, PC2_120=PC2_120/3.3, PC2_150=PC2_150/3.3, PC2_180=PC2_180/3.3;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `cohort_channel_category`()
begin

delete from production_pe.cohort_channel_category;

insert into production_pe.cohort_channel_category(country, custid, yrmonth, date, channel_group, channel, category, voucher, net_revenue, PC15, nr_items, `% discount`, region) select 'Peru', custid, yrmonth, date, channel_group, channel,(select n1 from production_pe.tbl_order_detail x where paid_price = (select max(paid_price) from production_pe.tbl_order_detail y where x.order_nr=y.order_nr group by order_nr) and z.order_nr=x.order_nr group by order_nr), coupon_code, sum(paid_price), sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)) as PC15, count(order_nr), (select (1-(avg(i.unit_price)/avg(i.original_unit_price))) from bob_live_pe.sales_order o inner join bob_live_pe.sales_order_item i on i.fk_sales_order = o.id_sales_order where z.order_nr=o.order_nr group by o.order_nr), region from production_pe.tbl_order_detail z where oac=1 and new_customers is not null group by custid;

update production_pe.cohort_channel_category z inner join bob_live_pe.customer c on z.custid=c.id_customer set z.age = year(curdate()) - year(c.birthday), z.sex = c.gender;

-- Same Month

update production_pe.cohort_channel_category b set nr_unique_repurcharse_same_month = (select case when count(distinct t.order_nr)>=1 then 1 else 0 end from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and b.yrmonth=t.yrmonth and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set transactions_same_month = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and b.yrmonth=t.yrmonth and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set nr_items_same_month = (select count(t.order_nr) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and b.yrmonth=t.yrmonth and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set net_revenue_same_month = (select sum(t.paid_price) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and b.yrmonth=t.yrmonth and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set PC15_same_month = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and b.yrmonth=t.yrmonth and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set nr_dif_categories_same_month = (select count(distinct t.n1) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and b.yrmonth=t.yrmonth and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set nr_dif_sub_categories_same_month = (select count(distinct t.n2) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and b.yrmonth=t.yrmonth and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category set nr_unique_repurcharse_same_month=0 where nr_unique_repurcharse_same_month is null;

-- 30 days

update production_pe.cohort_channel_category b set nr_unique_repurcharse_30 = (select case when count(distinct t.order_nr)>=1 and nr_unique_repurcharse_same_month=0 then 1 else 0 end from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set transactions_30 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set nr_items_30 = (select count(t.order_nr) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set net_revenue_30 = (select sum(t.paid_price) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set PC15_30 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set nr_dif_categories_30 = (select count(distinct t.n1) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set nr_dif_sub_categories_30 = (select count(distinct t.n2) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category set nr_unique_repurcharse_30=0 where nr_unique_repurcharse_30 is null;

-- 60 days

update production_pe.cohort_channel_category b set nr_unique_repurcharse_60 = (select case when count(distinct t.order_nr)>=1 and nr_unique_repurcharse_same_month=0 and nr_unique_repurcharse_30=0 then 1 else 0 end from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set transactions_60 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set nr_items_60 = (select count(t.order_nr) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set net_revenue_60 = (select sum(t.paid_price) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set PC15_60 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set nr_dif_categories_60 = (select count(distinct t.n1) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set nr_dif_sub_categories_60 = (select count(distinct t.n2) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category set nr_unique_repurcharse_60=0 where nr_unique_repurcharse_60 is null;

-- 90 days

update production_pe.cohort_channel_category b set nr_unique_repurcharse_90 = (select case when count(distinct t.order_nr)>=1 and nr_unique_repurcharse_same_month=0 and nr_unique_repurcharse_30=0 and nr_unique_repurcharse_60=0 then 1 else 0 end from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 90 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set transactions_90 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 90 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set nr_items_90 = (select count(t.order_nr) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 90 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set net_revenue_90 = (select sum(t.paid_price) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 90 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set PC15_90 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 90 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set nr_dif_categories_90 = (select count(distinct t.n1) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 90 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set nr_dif_sub_categories_90 = (select count(distinct t.n2) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 90 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category set nr_unique_repurcharse_90=0 where nr_unique_repurcharse_90 is null;

-- 120 days

update production_pe.cohort_channel_category b set nr_unique_repurcharse_120 = (select case when count(distinct t.order_nr)>=1 and nr_unique_repurcharse_same_month=0 and nr_unique_repurcharse_30=0 and nr_unique_repurcharse_60=0 and nr_unique_repurcharse_90=0 then 1 else 0 end from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 120 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set transactions_120 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 120 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set nr_items_120 = (select count(t.order_nr) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 120 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set net_revenue_120 = (select sum(t.paid_price) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 120 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set PC15_120 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 120 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set nr_dif_categories_120 = (select count(distinct t.n1) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 120 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set nr_dif_sub_categories_120 = (select count(distinct t.n2) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 120 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category set nr_unique_repurcharse_120=0 where nr_unique_repurcharse_120 is null;

-- 150 days

update production_pe.cohort_channel_category b set nr_unique_repurcharse_150 = (select case when count(distinct t.order_nr)>=1 and nr_unique_repurcharse_same_month=0 and nr_unique_repurcharse_30=0 and nr_unique_repurcharse_60=0 and nr_unique_repurcharse_90=0 and nr_unique_repurcharse_120=0 then 1 else 0 end from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 150 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set transactions_150 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 150 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set nr_items_150 = (select count(t.order_nr) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 150 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set net_revenue_150 = (select sum(t.paid_price) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 150 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set PC15_150 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 150 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set nr_dif_categories_150 = (select count(distinct t.n1) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 150 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set nr_dif_sub_categories_150 = (select count(distinct t.n2) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 150 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category set nr_unique_repurcharse_150=0 where nr_unique_repurcharse_150 is null;

-- 180 days

update production_pe.cohort_channel_category b set nr_unique_repurcharse_180 = (select case when count(distinct t.order_nr)>=1 and nr_unique_repurcharse_same_month=0 and nr_unique_repurcharse_30=0 and nr_unique_repurcharse_60=0 and nr_unique_repurcharse_90=0 and nr_unique_repurcharse_120=0 and nr_unique_repurcharse_150=0 then 1 else 0 end from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 180 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set transactions_180 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 180 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set nr_items_180 = (select count(t.order_nr) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 180 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set net_revenue_180 = (select sum(t.paid_price) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 180 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set PC15_180 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 180 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set nr_dif_categories_180 = (select count(distinct t.n1) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 180 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category b set nr_dif_sub_categories_180 = (select count(distinct t.n2) from production_pe.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 180 day) and t.new_customers is null group by b.custid);

update production_pe.cohort_channel_category set nr_unique_repurcharse_180=0 where nr_unique_repurcharse_180 is null;

update production_pe.cohort_channel_category c inner join development_mx.A_E_BI_ExchangeRate_USD u on c.yrmonth=u.Month_Num set net_revenue=net_revenue/xr, net_revenue_same_month=net_revenue_same_month/xr, net_revenue_30=net_revenue_30/xr, net_revenue_60=net_revenue_60/xr, net_revenue_90=net_revenue_90/xr, net_revenue_120=net_revenue_120/xr, net_revenue_150=net_revenue_150/xr, net_revenue_180=net_revenue_180/xr, PC15=PC15/xr, PC15_same_month=PC15_same_month/xr, PC15_30=PC15_30/xr, PC15_60=PC15_60/xr, PC15_90=PC15_90/xr, PC15_120=PC15_120/xr, PC15_150=PC15_150/xr, PC15_180=PC15_180/xr where u.country='PER';


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 PROCEDURE `daily_execution_ops`()
begin

select  'daily execution ops: start',now();

-- Operaciones
-- Macro A

-- A200
delete production_pe.pro_sku_con_categorias.* from production_pe.pro_sku_con_categorias;

-- A201
insert into production_pe.pro_sku_con_categorias ( sku, catalog_config_name, catalog_category_name, lft, rgt )
select catalog_config.sku, catalog_config.name, catalog_category.name, catalog_category.lft, catalog_category.rgt
from (bob_live_pe.catalog_config inner join bob_live_pe.catalog_config_has_catalog_category on catalog_config.id_catalog_config = catalog_config_has_catalog_category.fk_catalog_config) inner join bob_live_pe.catalog_category on catalog_config_has_catalog_category.fk_catalog_category = catalog_category.id_catalog_category
where catalog_category.lft<>1;

-- A202
update production_pe.pro_sku_con_categorias set pro_sku_con_categorias.dif = rgt-lft;

-- A211
delete production_pe.pro_category_tree.* from production_pe.pro_category_tree;

delete from production_pe.cat1_step_1_test;
delete from production_pe.cat1_final_test;
delete from production_pe.cat2_step_1_test;
delete from production_pe.cat2_step_2_test;
delete from production_pe.cat2_final_test;
delete from production_pe.cat3_step_1_test;
delete from production_pe.cat3_step_2_test;
delete from production_pe.cat3_final_test;

insert into production_pe.cat1_step_1_test (sku, catalog_config_name, maxofdif) select pro_sku_con_categorias.sku, pro_sku_con_categorias.catalog_config_name, max(pro_sku_con_categorias.dif) as maxofdif from production_pe.pro_sku_con_categorias group by pro_sku_con_categorias.sku, pro_sku_con_categorias.catalog_config_name;

insert into production_pe.cat1_final_test (sku, catalog_config_name, catalog_category_name) select cat1_step_1_test.sku, cat1_step_1_test.catalog_config_name, pro_sku_con_categorias.catalog_category_name
from production_pe.cat1_step_1_test inner join production_pe.pro_sku_con_categorias on (cat1_step_1_test.maxofdif = pro_sku_con_categorias.dif) and (cat1_step_1_test.sku = pro_sku_con_categorias.sku);


insert into production_pe.cat2_step_1_test (sku, catalog_config_name, catalog_category_name, dif) select pro_sku_con_categorias.sku, pro_sku_con_categorias.catalog_config_name, pro_sku_con_categorias.catalog_category_name, pro_sku_con_categorias.dif from production_pe.pro_sku_con_categorias inner join production_pe.cat1_step_1_test on pro_sku_con_categorias.sku = cat1_step_1_test.sku where (((pro_sku_con_categorias.dif)<maxofdif));

insert into production_pe.cat2_step_2_test (sku, catalog_config_name, maxofdif) select cat2_step_1_test.sku, cat2_step_1_test.catalog_config_name, max(cat2_step_1_test.dif) as maxofdif from production_pe.cat2_step_1_test group by cat2_step_1_test.sku, cat2_step_1_test.catalog_config_name;

insert into production_pe.cat2_final_test (sku, catalog_config_name, catalog_category_name) select cat2_step_2_test.sku, cat2_step_2_test.catalog_config_name, pro_sku_con_categorias.catalog_category_name
from production_pe.cat2_step_2_test inner join production_pe.pro_sku_con_categorias on (cat2_step_2_test.sku = pro_sku_con_categorias.sku) and (cat2_step_2_test.maxofdif = pro_sku_con_categorias.dif);


insert into production_pe.cat3_step_1_test (sku, catalog_config_name, catalog_category_name, dif) select pro_sku_con_categorias.sku, pro_sku_con_categorias.catalog_config_name, pro_sku_con_categorias.catalog_category_name, pro_sku_con_categorias.dif
from production_pe.pro_sku_con_categorias inner join production_pe.cat2_step_2_test on pro_sku_con_categorias.sku = cat2_step_2_test.sku
where (((pro_sku_con_categorias.dif)<maxofdif));

insert into production_pe.cat3_step_2_test (sku, catalog_config_name, maxofdif) select cat3_step_1_test.sku, cat3_step_1_test.catalog_config_name, max(cat3_step_1_test.dif) as maxofdif
from production_pe.cat3_step_1_test
group by cat3_step_1_test.sku, cat3_step_1_test.catalog_config_name;

insert into production_pe.cat3_final_test (sku, catalog_config_name, catalog_category_name) select pro_sku_con_categorias.sku, pro_sku_con_categorias.catalog_config_name,  pro_sku_con_categorias.catalog_category_name
from production_pe.cat3_step_2_test inner join production_pe.pro_sku_con_categorias on (cat3_step_2_test.sku = pro_sku_con_categorias.sku) and (cat3_step_2_test.maxofdif = pro_sku_con_categorias.dif);

-- A212
insert into production_pe.pro_category_tree ( sku, catalog_config_name, cat1, cat2, cat3 )
select cat1_final_test.sku, cat1_final_test.catalog_config_name, cat1_final_test.catalog_category_name as cat1, cat2_final_test.catalog_category_name as cat2, cat3_final_test.catalog_category_name as cat3 from (production_pe.cat1_final_test left join production_pe.cat2_final_test on cat1_final_test.sku = cat2_final_test.sku) left join production_pe.cat3_final_test on cat2_final_test.sku = cat3_final_test.sku;

-- Operaciones
-- Macro B

-- B100 A
delete production_pe.out_order_tracking.* from production_pe.out_order_tracking;

-- B101 A
insert into production_pe.out_order_tracking ( order_item_id, order_number, sku_simple ) select itens_venda.item_id, itens_venda.numero_order, itens_venda.sku from wmsprod_pe.itens_venda;

-- B102 U
update ((bob_live_pe.sales_order_address right join (production_pe.out_order_tracking inner join bob_live_pe.sales_order on out_order_tracking.order_number = sales_order.order_nr) on sales_order_address.id_sales_order_address = sales_order.fk_sales_order_address_billing) left join bob_live_pe.customer_address_region on sales_order_address.fk_customer_address_region = customer_address_region.id_customer_address_region) inner join bob_live_pe.sales_order_item on out_order_tracking.order_item_id = sales_order_item.id_sales_order_item set out_order_tracking.date_ordered = date(sales_order.created_at), date_time_ordered = sales_order.created_at,
out_order_tracking.payment_method = sales_order.payment_method, out_order_tracking.ship_to_state = customer_address_region.code, out_order_tracking.min_delivery_time = sales_order_item.min_delivery_time, out_order_tracking.max_delivery_time = sales_order_item.max_delivery_time, out_order_tracking.supplier_leadtime = sales_order_item.delivery_time_supplier;

-- B103 U
update production_pe.out_order_tracking inner join bob_live_pe.catalog_simple on out_order_tracking.sku_simple = catalog_simple.sku inner join bob_live_pe.catalog_config on catalog_simple.fk_catalog_config = catalog_config.id_catalog_config inner join bob_live_pe.supplier on catalog_config.fk_supplier = supplier.id_supplier set out_order_tracking.sku_config = catalog_config.sku, out_order_tracking.sku_name = catalog_config.name, out_order_tracking.supplier_id = supplier.id_supplier, out_order_tracking.supplier_name = supplier.name, out_order_tracking.min_delivery_time = case when out_order_tracking.min_delivery_time is null then catalog_simple.min_delivery_time else out_order_tracking.min_delivery_time end, out_order_tracking.max_delivery_time = case when out_order_tracking.max_delivery_time is null then catalog_simple.max_delivery_time else out_order_tracking.max_delivery_time end, out_order_tracking.package_height = catalog_config.package_height, out_order_tracking.package_length = catalog_config.package_length, out_order_tracking.package_width = catalog_config.package_width, out_order_tracking.package_weight = catalog_config.package_weight;

-- B103b U
update production_pe.out_order_tracking inner join bob_live_pe.catalog_simple on out_order_tracking.sku_simple = catalog_simple.sku inner join bob_live_pe.catalog_config on catalog_simple.fk_catalog_config = catalog_config.id_catalog_config set out_order_tracking.supplier_leadtime = case when out_order_tracking.supplier_leadtime is null then 0 else out_order_tracking.supplier_leadtime end;

-- B103c U extra
update production_pe.out_order_tracking set out_order_tracking.fullfilment_type_real = "inventory";

-- B104 U
update production_pe.out_order_tracking inner join (wmsprod_pe.itens_venda inner join wmsprod_pe.status_itens_venda on itens_venda.itens_venda_id = status_itens_venda.itens_venda_id) on out_order_tracking.order_item_id = itens_venda.item_id set out_order_tracking.fullfilment_type_real = "crossdock" where status_itens_venda.status="analisando quebra" or status_itens_venda.status="aguardando estoque";

-- B104 U Extra
update (production_pe.out_order_tracking inner join wmsprod_pe.itens_venda on out_order_tracking.order_item_id = itens_venda.item_id) inner join wmsprod_pe.status_itens_venda on itens_venda.itens_venda_id = status_itens_venda.itens_venda_id set out_order_tracking.fullfilment_type_real = "dropshipping"
where (((status_itens_venda.status)="ds estoque reservado"));

-- B105 U
update production_pe.out_order_tracking inner join production_pe.pro_category_tree on out_order_tracking.sku_config = pro_category_tree.sku set out_order_tracking.category = cat1, out_order_tracking.cat2 = pro_category_tree.cat2;

-- B105a U
update production_pe.out_order_tracking inner join production_pe.exceptions_supplier_categories on out_order_tracking.supplier_name = exceptions_supplier_categories.supplier set out_order_tracking.category = "ropa, calzado y accesorios"
where out_order_tracking.category="deportes";

-- B105b U
update production_pe.out_order_tracking set out_order_tracking.category = "salud y cuidado personal" where out_order_tracking.category="ropa, calzado y accesorios" and out_order_tracking.cat2="salud y cuidado personal";

-- B106 U
update production_pe.out_order_tracking inner join production_pe.buyer_category on out_order_tracking.category = buyer_category.category set out_order_tracking.category_english = buyer_category.category_english;

-- B110 D
delete production_pe.pro_min_date_exported.* from production_pe.pro_min_date_exported;

-- B111 A
insert into production_pe.pro_min_date_exported ( fk_sales_order_item, name, min_of_created_at )
select sales_order_item_status_history.fk_sales_order_item, sales_order_item_status.name, min(sales_order_item_status_history.created_at) as min_of_created_at from bob_live_pe.sales_order_item_status inner join bob_live_pe.sales_order_item_status_history on sales_order_item_status.id_sales_order_item_status = sales_order_item_status_history.fk_sales_order_item_status group by sales_order_item_status_history.fk_sales_order_item, sales_order_item_status.name having (((sales_order_item_status.name)="exported"));

-- B112 U
update production_pe.out_order_tracking inner join production_pe.pro_min_date_exported on out_order_tracking.order_item_id = pro_min_date_exported.fk_sales_order_item set out_order_tracking.date_exported = date(min_of_created_at), out_order_tracking.date_time_exported = min_of_created_at;

-- B120 U
update production_pe.out_order_tracking inner join wmsprod_pe.itens_venda on out_order_tracking.order_item_id = itens_venda.item_id inner join wmsprod_pe.status_itens_venda on itens_venda.itens_venda_id = status_itens_venda.itens_venda_id set out_order_tracking.date_procured = date(data), out_order_tracking.date_time_procured = data where status_itens_venda.status="estoque reservado";

-- B120b U
update production_pe.out_order_tracking set out_order_tracking.date_ready_to_pick = case when dayofweek(date_procured)=1 then production_pe.workday(date_procured,1) else (case when dayofweek(date_procured)=7 then production_pe.workday(date_procured,1) else date_procured end)end;

-- B120b2 U
update production_pe.out_order_tracking set out_order_tracking.date_time_ready_to_pick = case when dayofweek(date_time_procured)=1 then production_pe.workday(date_time_procured,1) else (case when dayofweek(date_time_procured)=7 then production_pe.workday(date_time_procured,1) else date_procured end)end;

-- B120c U
update production_pe.out_order_tracking set out_order_tracking.date_ready_to_pick = case when date_ready_to_pick in('2012-11-19','2012-12-25','2013-1-1','2013-2-4','2013-3-18','2013-05-01') then production_pe.workday(date_ready_to_pick,1) when date_ready_to_pick = '2013-3-28' then production_pe.workday(date_ready_to_pick,2) else date_ready_to_pick end,
out_order_tracking.date_time_ready_to_pick = case when date_ready_to_pick in('2012-11-19','2012-12-25','2013-1-1','2013-2-4','2013-3-18','2013-05-01') then production_pe.workday(date_time_ready_to_pick,1) when date_ready_to_pick = '2013-3-28' then production_pe.workday(date_time_ready_to_pick,2) else date_time_ready_to_pick end;

-- B121a D
delete production_pe.pro_max_date_ready_to_ship.* from production_pe.pro_max_date_ready_to_ship;

-- B121b A
insert into production_pe.pro_max_date_ready_to_ship ( date_ready, order_item_id )
select min(date(data)) as date_ready, out_order_tracking.order_item_id
from production_pe.out_order_tracking inner join wmsprod_pe.itens_venda on out_order_tracking.order_item_id = itens_venda.item_id inner join wmsprod_pe.status_itens_venda on itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
where status_itens_venda.status="faturado"
group by out_order_tracking.order_item_id;

-- B121c U
update production_pe.out_order_tracking inner join production_pe.pro_max_date_ready_to_ship on out_order_tracking.order_item_id = pro_max_date_ready_to_ship.order_item_id set out_order_tracking.date_ready_to_ship = pro_max_date_ready_to_ship.date_ready;

-- B122 U
update production_pe.out_order_tracking inner join (wmsprod_pe.itens_venda b inner join wmsprod_pe.status_itens_venda d on b.itens_venda_id = d.itens_venda_id) on out_order_tracking.order_item_id = b.item_id set out_order_tracking.date_shipped = (select max(date(data)) from wmsprod_pe.itens_venda c, wmsprod_pe.status_itens_venda e where e.status="expedido" and b.itens_venda_id = c.itens_venda_id and e.itens_venda_id = d.itens_venda_id);

-- B123 U
update production_pe.out_order_tracking inner join wmsprod_pe.vw_itens_venda_entrega vw on out_order_tracking.order_item_id = vw.item_id inner join wmsprod_pe.tms_status_delivery del on vw.cod_rastreamento = del.cod_rastreamento set out_order_tracking.date_delivered = (select case when tms.date < '2012-05-01' then null else max(tms.date) end from wmsprod_pe.vw_itens_venda_entrega itens, wmsprod_pe.tms_status_delivery tms where itens.cod_rastreamento = vw.cod_rastreamento and tms.cod_rastreamento = del.cod_rastreamento and tms.id_status = 4);

-- B124 U
update production_pe.out_order_tracking set out_order_tracking.date_procured_promised = production_pe.workday(date_exported, supplier_leadtime), out_order_tracking.date_delivered_promised = production_pe.workday(date_exported,max_delivery_time)
where out_order_tracking.date_exported is not null;

-- B124a U
update bob_live_pe.sales_order_item inner join production_pe.out_order_tracking on sales_order_item.id_sales_order_item = out_order_tracking.order_item_id set out_order_tracking.is_linio_promise = sales_order_item.is_linio_promise;

-- B124b U
update production_pe.out_order_tracking set is_linio_promise = 0 where is_linio_promise is null;

-- B124c U
update production_pe.out_order_tracking set date_delivered_promised = production_pe.workday(date_exported,2) where is_linio_promise = 1;

-- B124d1 U
update production_pe.out_order_tracking set out_order_tracking.date_procured_promised = production_pe.workday(out_order_tracking.date_procured_promised,1)
where out_order_tracking.date_procured_promised>='2013-1-1' and out_order_tracking.date_exported<'2013-1-1';

-- B124d2 U
update production_pe.out_order_tracking set out_order_tracking.date_delivered_promised = production_pe.workday(out_order_tracking.date_delivered_promised,1) where out_order_tracking.date_delivered_promised>='2013-1-1' and out_order_tracking.date_exported<'2013-1-1';

-- B124d4 U
update production_pe.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_pe.workday(out_order_tracking.date_ready_to_ship_promised,1) where out_order_tracking.date_ready_to_ship_promised>='2013-1-1' and out_order_tracking.date_exported<'2013-1-1';

-- B124e1 U
update production_pe.out_order_tracking set out_order_tracking.date_procured_promised = production_pe.workday(out_order_tracking.date_procured_promised,1)
where out_order_tracking.date_procured_promised>='2013-03-28' and out_order_tracking.date_exported<'2013-03-28';

-- B124e2 U
update production_pe.out_order_tracking set out_order_tracking.date_delivered_promised = production_pe.workday(out_order_tracking.date_delivered_promised,1) where out_order_tracking.date_delivered_promised>='2013-03-28' and out_order_tracking.date_exported<'2013-03-28';

-- B124e4 U
update production_pe.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_pe.workday(out_order_tracking.date_ready_to_ship_promised,1) where out_order_tracking.date_ready_to_ship_promised>='2013-03-28' and out_order_tracking.date_exported<'2013-03-28';

-- B124h1 U
update production_pe.out_order_tracking set out_order_tracking.date_procured_promised = production_pe.workday(out_order_tracking.date_procured_promised,1)
where out_order_tracking.date_procured_promised>='2013-3-29' and out_order_tracking.date_exported<'2013-3-29';

-- B124h2 U
update production_pe.out_order_tracking set out_order_tracking.date_delivered_promised = production_pe.workday(out_order_tracking.date_delivered_promised,1)
where out_order_tracking.date_delivered_promised>='2013-3-29' and out_order_tracking.date_exported<'2013-3-29';

-- B124h4 U
update production_pe.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_pe.workday(out_order_tracking.date_ready_to_ship_promised,1) where out_order_tracking.date_ready_to_ship_promised>='2013-3-29' and out_order_tracking.date_exported<'2013-3-29';

-- B124i1 U
update production_pe.out_order_tracking set out_order_tracking.date_procured_promised = production_pe.workday(out_order_tracking.date_procured_promised,1)
where out_order_tracking.date_procured_promised>='2013-05-01' and out_order_tracking.date_exported<'2013-05-01';

-- B124i2 U
update production_pe.out_order_tracking set out_order_tracking.date_delivered_promised = production_pe.workday(out_order_tracking.date_delivered_promised,1)
where out_order_tracking.date_delivered_promised>='2013-05-01' and out_order_tracking.date_exported<'2013-05-01';

-- B124i4 U
update production_pe.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_pe.workday(out_order_tracking.date_ready_to_ship_promised,1) where out_order_tracking.date_ready_to_ship_promised>='2013-05-01' and out_order_tracking.date_exported<'2013-05-01';

-- B124j1 U
update production_pe.out_order_tracking set out_order_tracking.date_procured_promised = production_pe.workday(out_order_tracking.date_procured_promised,1)
where out_order_tracking.date_procured_promised>='2013-06-29' and out_order_tracking.date_exported<'2013-06-29';

-- B124j2 U
update production_pe.out_order_tracking set out_order_tracking.date_delivered_promised = production_pe.workday(out_order_tracking.date_delivered_promised,1)
where out_order_tracking.date_delivered_promised>='2013-06-29' and out_order_tracking.date_exported<'2013-06-29';

-- B124j4 U
update production_pe.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_pe.workday(out_order_tracking.date_ready_to_ship_promised,1) where out_order_tracking.date_ready_to_ship_promised>='2013-06-29' and out_order_tracking.date_exported<'2013-06-29';

-- B124f U
update production_pe.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_pe.workday(date_procured_promised, wh_time);

-- B124f1 U Extra
update production_pe.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_pe.workday(out_order_tracking.date_ready_to_ship_promised,1)
where (((out_order_tracking.date_ready_to_ship_promised)>='2013-01-01') and ((out_order_tracking.date_procured_promised)<'2013-01-01'));

-- B124f2 U Extra
update production_pe.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_pe.workday(out_order_tracking.date_ready_to_ship_promised,1)
where (((out_order_tracking.date_ready_to_ship_promised)>='2013-03-28') and ((out_order_tracking.date_procured_promised)<'2013-03-28'));

-- B124f3 U Extra
update production_pe.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_pe.workday(out_order_tracking.date_ready_to_ship_promised,1)
where (((out_order_tracking.date_ready_to_ship_promised)>='2013-03-29') and ((out_order_tracking.date_procured_promised)<'2013-03-29'));

-- B124f4 U Extra
update production_pe.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_pe.workday(out_order_tracking.date_ready_to_ship_promised,1)
where (((out_order_tracking.date_ready_to_ship_promised)>='2013-05-01') and ((out_order_tracking.date_procured_promised)<'2013-05-01'));

-- B124f5 U Extra
update production_pe.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_pe.workday(out_order_tracking.date_ready_to_ship_promised,1)
where (((out_order_tracking.date_ready_to_ship_promised)>='2013-06-29') and ((out_order_tracking.date_procured_promised)<'2013-06-29'));

-- B124j U
update production_pe.out_order_tracking inner join wmsprod_pe.itens_venda on out_order_tracking.order_item_id = itens_venda.item_id set out_order_tracking.wh_time = tempo_armazem where tempo_armazem >1;

-- B125 U
update production_pe.out_order_tracking inner join bob_live_pe.sales_order_item on out_order_tracking.sku_simple = sales_order_item.sku inner join bob_live_pe.catalog_shipment_type on sales_order_item.fk_catalog_shipment_type = catalog_shipment_type.id_catalog_shipment_type set out_order_tracking.fullfilment_type_bob = catalog_shipment_type.name;

-- B130 D
delete production_pe.pro_1st_attempt.* from production_pe.pro_1st_attempt;

-- B131 A
insert into production_pe.pro_1st_attempt ( order_item_id, min_of_date )
select out_order_tracking.order_item_id, min(tms_status_delivery.date) as min_of_date from (production_pe.out_order_tracking inner join wmsprod_pe.
vw_itens_venda_entrega on out_order_tracking.order_item_id = vw_itens_venda_entrega.item_id) inner join wmsprod_pe.
tms_status_delivery on vw_itens_venda_entrega.cod_rastreamento = tms_status_delivery.cod_rastreamento group by out_order_tracking.order_item_id, tms_status_delivery.id_status
having tms_status_delivery.id_status=5;

-- B132 U
update production_pe.out_order_tracking inner join production_pe.pro_1st_attempt on out_order_tracking.order_item_id = pro_1st_attempt.order_item_id set out_order_tracking.date_1st_attempt = min_of_date;

-- B133 U
update production_pe.out_order_tracking inner join wmsprod_pe.vw_itens_venda_entrega vw on out_order_tracking.order_item_id = vw.item_id inner join wmsprod_pe.tms_status_delivery del on vw.cod_rastreamento = del.cod_rastreamento set out_order_tracking.date_1st_attempt = (select max(tms.date) from wmsprod_pe.tms_status_delivery tms, wmsprod_pe.vw_itens_venda_entrega itens where del.cod_rastreamento = tms.cod_rastreamento and vw.item_id = itens.item_id and tms.id_status=4) where out_order_tracking.date_1st_attempt is null or out_order_tracking.date_1st_attempt < '2011-05-01';

-- B140 U
update production_pe.out_order_tracking set out_order_tracking.week_exported = production_pe.week_iso(date_exported), out_order_tracking.week_procured_promised = production_pe.week_iso(date_procured_promised), out_order_tracking.week_delivered_promised = production_pe.week_iso(date_delivered_promised), out_order_tracking.month_num_delivered_promised = date_format(date_delivered_promised, "%x-%m"), out_order_tracking.month_num_procured_promised = date_format(date_procured_promised, "%x-%m"), out_order_tracking.week_ready_to_ship_promised = production_pe.week_iso(date_ready_to_ship_promised), out_order_tracking.week_ready_to_pick = production_pe.week_iso(date_ready_to_pick), production_pe.out_order_tracking.week_ordered = production_pe.week_iso(date_ordered),month_num_ready_to_pick = date_format(date_ready_to_pick, "%x-%m"), out_order_tracking.week_shipped = production_pe.week_iso(date_shipped);

-- B141a U
update production_pe.out_order_tracking set out_order_tracking.month_procured_promised = monthname(date_procured_promised);

-- B141a1 U
update production_pe.out_order_tracking set out_order_tracking.month_num_procured_promised = date_format(date_procured_promised, "%x-%m");

-- B141b U
update production_pe.out_order_tracking set out_order_tracking.month_delivered_promised = monthname(date_delivered_promised);

-- B141b1 U
update production_pe.out_order_tracking set out_order_tracking.month_num_delivered_promised = date_format(date_delivered_promised, "%x-%m");

-- B141c U
update production_pe.out_order_tracking set month_num_procured_promised = '2012-12' where date_procured_promised = '2012-12-31';
 
-- B141c2 U
update production_pe.out_order_tracking set month_num_delivered_promised = '2012-12' where date_delivered_promised = '2012-12-31';

-- B150 U
update production_pe.out_order_tracking inner join wmsprod_pe.itens_venda on out_order_tracking.order_item_id = itens_venda.item_id inner join wmsprod_pe.romaneio on itens_venda.romaneio_id = romaneio.romaneio_id inner join wmsprod_pe.transportadoras on romaneio.transportadora_id = transportadoras.transportadoras_id set out_order_tracking.shipping_carrier = nome_transportadora;

-- B150a U
update production_pe.out_order_tracking ot inner join bob_live_pe.sales_order_item soi on ot.order_item_id = soi.id_sales_order_item inner join bob_live_pe.shipment_carrier sc on soi.fk_shipment_carrier = sc.id_shipment_carrier set ot.shipping_carrier_srt = sc.name;

-- B151 U
update production_pe.out_order_tracking set out_order_tracking.year_procured_promised = date_format(date_procured_promised, "%x");

-- B152 U
update production_pe.out_order_tracking set out_order_tracking.year_delivered_promised = date_format(date_delivered_promised,"%x");

-- B153 U
update production_pe.out_order_tracking set out_order_tracking.sl0 = 1
where (((out_order_tracking.supplier_leadtime)=0));

-- B190 U
update wmsprod_pe.itens_venda inner join production_pe.out_order_tracking on itens_venda.item_id = out_order_tracking.order_item_id set out_order_tracking.status_wms = status;

-- B191 U
update production_pe.out_order_tracking set out_order_tracking.stockout = "1"
where out_order_tracking.status_wms="quebra tratada" or out_order_tracking.status_wms="quebrado";

-- B201 U
update production_pe.out_order_tracking set out_order_tracking.workdays_to_export = case when date_ordered is null then null else production_pe.calcworkdays(date_ordered,case when date_exported is null then curdate() else date_exported end)end, out_order_tracking.workdays_to_procure = case when stockout = 1 then null else (case when date_exported is null then null else production_pe.calcworkdays(date_exported,case when date_procured is null then curdate() else date_procured end)end)end, out_order_tracking.workdays_to_ready = case when date_ready_to_pick is null then null else production_pe.calcworkdays(date_ready_to_pick, case when date_ready_to_ship is null then curdate() else date_ready_to_ship end)end, out_order_tracking.workdays_to_ship = case when date_ready_to_ship is null then null else production_pe.calcworkdays(date_ready_to_ship, case when date_shipped is null then curdate() else date_shipped end)end, out_order_tracking.workdays_to_1st_attempt = case when date_shipped is null then null else production_pe.calcworkdays(date_shipped, case when date_1st_attempt is null then curdate() else date_1st_attempt end)end, out_order_tracking.workdays_to_deliver = case when date_shipped is null then null else production_pe.calcworkdays(date_shipped,case when date_delivered is null then curdate() else date_delivered end)end, out_order_tracking.workdays_total_1st_attempt = case when date_shipped is null then null else production_pe.calcworkdays(date_exported, case when date_1st_attempt is null then curdate() else date_1st_attempt end)end, out_order_tracking.workdays_total_delivery = case when date_shipped is null then null else production_pe.calcworkdays(date_exported, case when date_delivered is null then curdate() else date_delivered end)end;

-- B201a U
update production_pe.out_order_tracking set days_to_export = case when date_ordered is null then null else (case when date_exported is null then datediff(curdate(),date_ordered) else datediff(date_exported, date_ordered) end)end, days_to_procure = case when stockout = 1 then null else (case when date_procured < date_exported then 0 else (case when date_procured is null then datediff(curdate(),date_exported) else datediff(date_procured, date_exported) end)end) end, days_to_ready = case when date_ready_to_pick is null then null else (case when date_ready_to_ship is null then datediff(curdate(),date_ready_to_pick) else datediff(date_ready_to_ship, date_ready_to_pick) end)end, days_to_ship  = case when date_ready_to_ship is null then null else (case when date_shipped is null then datediff(curdate(),date_ready_to_ship) else datediff(date_shipped, date_ready_to_ship) end)end, days_to_1st_attempt = case when date_shipped is null then null else (case when date_1st_attempt is null then datediff(curdate(),date_shipped) else datediff(date_1st_attempt,date_shipped) end)end, out_order_tracking.days_total_delivery = case when date_shipped is null then null else (case when date_delivered is null then datediff(curdate(),date_ordered) else datediff(date_delivered,date_ordered) end)end;

-- B201b U
update production_pe.out_order_tracking set date_delivered_errors = case when days_total_delivery < 0 then 1  else 0 end;

-- B202 U
update production_pe.out_order_tracking set out_order_tracking.delay_to_procure = case when date_procured is null then (case when curdate()>date_procured_promised then 1 else 0 end) else (case when date_procured>date_procured_promised then 1 else 0 end)end, out_order_tracking.delay_to_ready = case when workdays_to_ready>1 then 1 else 0 end, out_order_tracking.delay_to_ship = case when workdays_to_ship>1 then 1 else 0 end, out_order_tracking.delay_to_1st_attempt = case when workdays_to_1st_attempt>2 then 1 else 0 end, out_order_tracking.delay_to_deliver = case when workdays_to_deliver> 2 then 1 else 0 end, out_order_tracking.delay_total_1st_attempt = case when date_1st_attempt is null then (case when curdate()>date_delivered_promised then 1 else 0 end) else (case when date_1st_attempt>date_delivered_promised then 1 else 0 end)end, out_order_tracking.delay_total_delivery = case when date_delivered is null then (case when curdate()>date_delivered_promised then 1 else 0 end) else (case when date_delivered>date_delivered_promised then 1 else 0 end)end, out_order_tracking.workdays_delay_to_procure = case when workdays_to_procure-supplier_leadtime<0 then 0 else workdays_to_procure-supplier_leadtime end, out_order_tracking.workdays_delay_to_ready = case when workdays_to_ready-1<0 then 0 else workdays_to_ready-1 end, out_order_tracking.workdays_delay_to_ship = case when workdays_to_ship-1<0 then 0 else workdays_to_ship-1 end, out_order_tracking.workdays_delay_to_1st_attempt = case when workdays_to_1st_attempt-3<0 then 0 else workdays_to_1st_attempt-3 end, out_order_tracking.workdays_delay_to_deliver = case when workdays_to_deliver-3<0 then 0 else workdays_to_deliver-3 end, out_order_tracking.on_time_to_procure = case when date_procured<=date_procured_promised then 1 else 0 end, out_order_tracking.on_time_total_1st_attempt = case when date_1st_attempt<=date_delivered_promised then 1 else 0 end, out_order_tracking.on_time_total_delivery = case when date_delivered<=date_delivered_promised then 1 when date_delivered is null then 0 else 0 end;

-- B203 U
update production_pe.out_order_tracking inner join bob_live_pe.catalog_config on out_order_tracking.sku_config = catalog_config.sku set out_order_tracking.presale = "1" where catalog_config.name like "preventa%";

-- B204 U
update production_pe.out_order_tracking inner join bob_live_pe.catalog_config on out_order_tracking.sku_config = catalog_config.sku inner join bob_live_pe.catalog_attribute_option_global_buyer on catalog_attribute_option_global_buyer.id_catalog_attribute_option_global_buyer = catalog_config.fk_catalog_attribute_option_global_buyer set out_order_tracking.buyer = catalog_attribute_option_global_buyer.name;

-- B206 U
update production_pe.out_order_tracking inner join bob_live_pe.sales_order_item on out_order_tracking.order_item_id = sales_order_item.id_sales_order_item inner join bob_live_pe.sales_order_item_status on sales_order_item.fk_sales_order_item_status = sales_order_item_status.id_sales_order_item_status set out_order_tracking.status_bob = sales_order_item_status.name;

-- B207 U
update production_pe.out_order_tracking inner join wmsprod_pe.vw_itens_venda_entrega on out_order_tracking.order_item_id = vw_itens_venda_entrega.item_id set out_order_tracking.wms_tracking_code = cod_rastreamento;

-- B208 U
update production_pe.out_order_tracking inner join wmsprod_pe.tms_tracks on out_order_tracking.wms_tracking_code = tms_tracks.cod_rastreamento set out_order_tracking.shipping_carrier_tracking_code = track;

-- B210 U
update production_pe.out_order_tracking inner join production_pe.status_match_bob_wms on out_order_tracking.status_wms = status_match_bob_wms.status_wms and out_order_tracking.status_bob = status_match_bob_wms.status_bob set out_order_tracking.status_match_bob_wms = status_match_bob_wms.correct;

-- B211 U
update production_pe.out_order_tracking set out_order_tracking.check_dates = case when date_ordered>date_exported then "date ordered > date exported" else (case when date_exported>date_procured then "date exported > date procured" else (case when date_procured>date_ready_to_ship then "date procured > date ready to ship" else (case when date_ready_to_ship>date_shipped then "date ready to ship > date shipped" else (case when date_shipped>date_1st_attempt then "date shipped > date 1st attempt" else (case when date_1st_attempt>date_delivered then "date shipped > date 1st attempt" else "correct" end)end)end)end)end)end;

-- B211a U
update production_pe.out_order_tracking set out_order_tracking.check_date_1st_attempt = case when date_1st_attempt is null then (case when date_delivered is null then 0 else 1 end) else (case when date_1st_attempt>date_delivered then 1 else 0 end)end;

-- B211b U
update production_pe.out_order_tracking set out_order_tracking.check_date_shipped = case when date_shipped is null then (case when date_1st_attempt is null and date_delivered is null then 0 else 1 end) else (case when date_shipped>date_1st_attempt then 1 else 0 end)end;

-- B211c U
update production_pe.out_order_tracking set out_order_tracking.check_date_ready_to_ship = case when date_ready_to_ship is null then (case when date_shipped is null and date_1st_attempt is null and date_delivered is null then 0 else 1 end) else (case when date_ready_to_ship>date_shipped then 1 else 0 end)end;

-- B211d U
update production_pe.out_order_tracking set out_order_tracking.check_date_procured = case when date_procured is null then (case when date_ready_to_ship is null and date_shipped is null and date_1st_attempt is null and date_delivered is null then 0 else 1 end) else (case when date_procured>date_ready_to_ship then 1 else 0 end)end;

-- B211e U
update production_pe.out_order_tracking set out_order_tracking.check_date_exported = case when date_exported is null then (case when date_procured is null and date_ready_to_ship is null and date_shipped is null and date_1st_attempt is null and date_delivered is null then 0 else 1 end) else (case when date_exported>date_procured then 1 else 0 end)end;

-- B211f U
update production_pe.out_order_tracking set out_order_tracking.check_date_ordered = case when date_ordered is null then (case when date_exported is null and date_procured is null and date_ready_to_ship is null and date_shipped is null and date_1st_attempt is null and date_delivered is null then 0 else 1 end) else (case when date_ordered>date_exported then 1 else 0 end)end;

-- B214 U
update production_pe.out_order_tracking set out_order_tracking.date_procured = null
where out_order_tracking.status_wms="aguardando estoque" or out_order_tracking.status_wms="analisando quebra";

-- B216 U
update production_pe.out_order_tracking set out_order_tracking.still_to_procure = 1
where out_order_tracking.date_procured is null;

-- B216b U
update production_pe.out_order_tracking set out_order_tracking.still_to_procure = 0
where out_order_tracking.status_wms="quebrado" or out_order_tracking.status_wms="quebra tratada";

-- B218 U
update production_pe.out_order_tracking set out_order_tracking.vol_weight = package_height*package_length*package_width/5000;

-- B219 U
update production_pe.out_order_tracking set out_order_tracking.max_vol_w_vs_w= case when vol_weight>package_weight then vol_weight else package_weight end;

-- B220 U
update production_pe.out_order_tracking set out_order_tracking.package_measure = case when max_vol_w_vs_w>45.486 then "large" else (case when max_vol_w_vs_w>2.277 then "medium" else "small" end)end;

-- B221 U
update production_pe.out_order_tracking inner join bob_live_pe.sales_order on out_order_tracking.order_number = sales_order.order_nr inner join bob_live_pe.sales_order_address on sales_order.fk_sales_order_address_billing = sales_order_address.id_sales_order_address set out_order_tracking.ship_to_zip = postcode;

-- B222 U
update production_pe.out_order_tracking set out_order_tracking.delay_carrier_shipment = case when package_measure="large" then (case when workdays_to_1st_attempt-5>0 then 1 else 0 end) else (case when workdays_to_1st_attempt-3>0 then 1 else 0 end)end;

-- B223 U
update production_pe.out_order_tracking set out_order_tracking.days_left_to_procure = case when date_procured is null then (case when datediff(date_procured_promised,curdate())<0 then 0 else datediff(date_procured_promised,curdate()) end) else 555 end;

-- B224 U
update production_pe.out_order_tracking set out_order_tracking.deliv_within_3_days_of_order = case when datediff(date_delivered,date_ordered)<=5 then (case when production_pe.calcworkdays(date_ordered,date_delivered)<=3 then 1 else 0 end) else 0 end, out_order_tracking.first_att_within_3_days_of_order = case when datediff(date_1st_attempt,date_ordered)<=5 then (case when production_pe.calcworkdays(date_ordered,date_1st_attempt)<=3 then 1 else 0 end) else 0 end;

-- B225 U
update production_pe.out_order_tracking set out_order_tracking.shipped_same_day_as_order = case when date_ordered=date_shipped then 1 else 0 end;

-- B226 U
update production_pe.out_order_tracking set out_order_tracking.shipped_same_day_as_procured = case when date_procured=date_shipped then 1 else 0 end;

-- B227 U extra
update production_pe.out_order_tracking set out_order_tracking.reason_for_delay = "on time 1st attempt";

-- B227 U
update production_pe.out_order_tracking set out_order_tracking.reason_for_delay = "procurement" where out_order_tracking.delay_to_procure=1 and out_order_tracking.delay_total_1st_attempt=1;

-- B228 U
update production_pe.out_order_tracking set out_order_tracking.reason_for_delay = "preparing item for shipping" where out_order_tracking.reason_for_delay="on time 1st attempt" and out_order_tracking.delay_to_ready=1 and out_order_tracking.delay_total_1st_attempt=1;

-- B229 U
update production_pe.out_order_tracking set out_order_tracking.reason_for_delay = "preparing item for shipping"
where out_order_tracking.reason_for_delay="on time 1st attempt" and out_order_tracking.delay_to_ready=1 and out_order_tracking.delay_total_1st_attempt=1;

-- B230 U
update production_pe.out_order_tracking set out_order_tracking.reason_for_delay = "carrier late delivering" where out_order_tracking.reason_for_delay="on time 1st attempt" and out_order_tracking.delay_total_1st_attempt=1;

-- B231 U
update production_pe.out_order_tracking set out_order_tracking.early_to_procure = case when (case when date_procured is null then datediff(date_procured_promised,curdate()) else datediff(date_procured_promised,date_procured) end)>0 then 1 else 0 end, out_order_tracking.early_to_1st_attempt = case when (case when date_1st_attempt is null then datediff(date_delivered_promised,curdate()) else datediff(date_delivered_promised,date_1st_attempt) end)>0 then 1 else 0 end;

-- B232 U
update production_pe.out_order_tracking set out_order_tracking.delay_reason_maximum_value = production_pe.maximum(workdays_delay_to_procure,workdays_delay_to_ready,workdays_delay_to_ship,workdays_delay_to_1st_attempt);

-- B233 U
update production_pe.out_order_tracking set out_order_tracking.delay_reason = case when delay_total_1st_attempt=1 then (case when delay_reason_maximum_value=workdays_delay_to_procure then "procurement" else (case when delay_reason_maximum_value=workdays_delay_to_ready then "preparing item for shipping" else (case when delay_reason_maximum_value=workdays_delay_to_ship then "carrier late to pick up item" else (case when delay_reason_maximum_value=workdays_delay_to_1st_attempt then "carrier late delivering" else "on time 1st attempt" end)end)end)end) else "on time 1st attempt" end;

-- B234 U
update production_pe.out_order_tracking set out_order_tracking.on_time_to_ship = case when date_shipped<= date_ready_to_ship_promised then 1 else 0 end, out_order_tracking.on_time_r2s = case when date_ready_to_ship<=date_ready_to_ship_promised then 1 else 0 end;

-- B235 D
delete production_pe.pro_order_tracking_num_items_per_order.* from production_pe.pro_order_tracking_num_items_per_order;

-- B236 A
insert into production_pe.pro_order_tracking_num_items_per_order ( order_number, count_of_order_item_id ) select out_order_tracking.order_number, count(out_order_tracking.order_item_id) as countoforder_item_id from production_pe.out_order_tracking group by out_order_tracking.order_number;

-- B237 U
update production_pe.out_order_tracking inner join production_pe.pro_order_tracking_num_items_per_order on out_order_tracking.order_number = pro_order_tracking_num_items_per_order.order_number set out_order_tracking.num_items_per_order = 1/pro_order_tracking_num_items_per_order.count_of_order_item_id;

-- B238 U
update production_pe.out_order_tracking set out_order_tracking.procurement_actual_time = case when date_exported is null then null else (case when date_procured is null then production_pe.calcworkdays(date_exported,curdate()) else production_pe.calcworkdays(date_exported,date_procured)end)end;

-- B239 U
update production_pe.out_order_tracking set out_order_tracking.procurement_delay= case when date_exported>=curdate() then 0 else (case when date_sub(procurement_actual_time, interval supplier_leadtime day)>0 then date_sub(procurement_actual_time, interval supplier_leadtime day) else 0 end)end;

-- B240 U
update production_pe.out_order_tracking set out_order_tracking.procurement_delay_counter = 1
where out_order_tracking.procurement_delay>0;

-- B241 U
update production_pe.out_order_tracking set out_order_tracking.warehouse_actual_time = case when date_ready_to_pick is null then null else (case when date_ready_to_ship is null then production_pe.calcworkdays(date_ready_to_pick,curdate()) else production_pe.calcworkdays(date_ready_to_pick,date_ready_to_ship)end)end;

-- B242 U
update production_pe.out_order_tracking set out_order_tracking.warehouse_delay = case when date_ready_to_ship_promised>curdate() then 0 else (case when date_ready_to_ship is null then production_pe.calcworkdays(date_ready_to_ship_promised,curdate()) else production_pe.calcworkdays(date_ready_to_ship_promised,date_ready_to_ship)end)end;

-- B243 U
update production_pe.out_order_tracking set out_order_tracking.warehouse_delay_counter = 1
where out_order_tracking.warehouse_delay>0;

-- B244 U
update production_pe.out_order_tracking set out_order_tracking.carrier_time = max_delivery_time-wh_time-supplier_leadtime;

-- B245 U
update production_pe.out_order_tracking set out_order_tracking.actual_carrier_time = case when date_ready_to_ship is null then null else (case when date_delivered is null then production_pe.calcworkdays(date_ready_to_ship,curdate()) else production_pe.calcworkdays(date_ready_to_ship,date_delivered)end)end;

-- B246 U
update production_pe.out_order_tracking set out_order_tracking.delivery_delay = case when date_ready_to_ship>=curdate() then 0 else (case when actual_carrier_time>carrier_time then actual_carrier_time-carrier_time else 0 end)end;

-- B247 U
update production_pe.out_order_tracking set out_order_tracking.delivery_delay_counter = 1
where out_order_tracking.delivery_delay>0 and out_order_tracking.date_delivered_promised<=curdate();

-- B248 U
update production_pe.out_order_tracking set out_order_tracking.delay_exceptions = case when out_order_tracking.supplier_leadtime>=out_order_tracking.max_delivery_time then 1 else 0 end;

-- B250 U 
update production_pe.out_order_tracking set out_order_tracking.ready_to_ship = 1 where out_order_tracking.date_ready_to_ship is not null;

-- B251 U
update production_pe.out_order_tracking set out_order_tracking.wh_workdays_to_r2s = case when date_ready_to_ship is null then null else (case when date_ready_to_pick is null then null else production_pe.calcworkdays(date_ready_to_pick, date_ready_to_ship)end)end;

-- B251b U
update production_pe.out_order_tracking set out_order_tracking.week_r2s_promised = production_pe.week_iso(date_ready_to_ship_promised), out_order_tracking.month_num_r2s_promised = date_format(date_ready_to_ship_promised, "%x-%m");

-- B252 U
update production_pe.out_order_tracking set out_order_tracking.r2s_workday_0 = case when production_pe.calcworkdays(date_ready_to_pick,curdate())<0 then 0 else (case when wh_workdays_to_r2s=0 then 1 else 0 end)end, out_order_tracking.r2s_workday_1 = case when production_pe.calcworkdays(date_ready_to_pick,curdate())<1 then 0 else (case when wh_workdays_to_r2s<=1 then 1 else 0 end)end, out_order_tracking.r2s_workday_2 = case when production_pe.calcworkdays(date_ready_to_pick,curdate())<2 then 0 else (case when wh_workdays_to_r2s<=2 then 1 else 0 end)end, out_order_tracking.r2s_workday_3 = case when production_pe.calcworkdays(date_ready_to_pick,curdate())<3 then 0 else (case when wh_workdays_to_r2s<=3 then 1 else 0 end)end, out_order_tracking.r2s_workday_4 = case when production_pe.calcworkdays(date_ready_to_pick,curdate())<4 then 0 else (case when wh_workdays_to_r2s<=4 then 1 else 0 end)end, out_order_tracking.r2s_workday_5 = case when production_pe.calcworkdays(date_ready_to_pick,curdate())<5 then 0 else (case when wh_workdays_to_r2s<=5 then 1 else 0 end)end, out_order_tracking.r2s_workday_6 = case when production_pe.calcworkdays(date_ready_to_pick,curdate())<6 then 0 else (case when wh_workdays_to_r2s<=6 then 1 else 0 end)end, out_order_tracking.r2s_workday_7 = case when production_pe.calcworkdays(date_ready_to_pick,curdate())<7 then 0 else (case when wh_workdays_to_r2s<=7 then 1 else 0 end)end, out_order_tracking.r2s_workday_8 = case when production_pe.calcworkdays(date_ready_to_pick,curdate())<8 then 0 else (case when wh_workdays_to_r2s<=8 then 1 else 0 end)end, out_order_tracking.r2s_workday_9 = case when production_pe.calcworkdays(date_ready_to_pick,curdate())<9 then 0 else (case when wh_workdays_to_r2s<=9 then 1 else 0 end)end, out_order_tracking.r2s_workday_10 = case when production_pe.calcworkdays(date_ready_to_pick,curdate())<10 then 0 else (case when wh_workdays_to_r2s<=10 then 1 else 0 end)end, out_order_tracking.r2s_workday_superior_10 = case when production_pe.calcworkdays(date_ready_to_pick,curdate())<10 then 0 else (case when wh_workdays_to_r2s>10 then 1 else 0 end)end;

-- B253 U
update production_pe.out_order_tracking set out_order_tracking.ready_to_pick = 1
where out_order_tracking.date_ready_to_pick is not null;
 
-- B256 U
update production_pe.out_order_tracking set out_order_tracking.date_payable_promised = date_procured
where out_order_tracking.oms_payment_event="entrega";
 
-- B257 U
update production_pe.out_order_tracking set out_order_tracking.date_payable_promised = date_procured
where out_order_tracking.oms_payment_event="pedido";
 
-- B258 U
update production_pe.out_order_tracking set out_order_tracking.date_payable_promised = date_procured-supplier_leadtime
where out_order_tracking.oms_payment_event="factura";

-- B259 U
update production_pe.out_order_tracking set out_order_tracking.delay_linio_promise = case when is_linio_promise = 1 and workdays_to_1st_attempt > 2 then 1 else 0 end;

-- B260
update production_pe.out_order_tracking inner join wmsprod_pe.entrega on out_order_tracking.wms_tracking_code = entrega.cod_rastreamento set out_order_tracking.delivery_fullfilment = entrega.delivery_fulfillment;
 
-- B261
update production_pe.out_order_tracking set out_order_tracking.split_order = case when delivery_fullfilment = 'partial' then 1 else 0 end;

-- B262
update production_pe.out_order_tracking set out_order_tracking.effective_1st_attempt = case when date_delivered = date_1st_attempt then 1 else 0 end;

-- B263
delete from production_pe.pro_value_from_order;

-- B264
insert into production_pe.pro_value_from_order
select order_number, 1/count(order_item_id) as value_from_order from production_pe.out_order_tracking t group by order_number order by date_ordered desc;

-- B265
update production_pe.out_order_tracking inner join production_pe.pro_value_from_order on out_order_tracking.order_number=pro_value_from_order.order_number set out_order_tracking.value_from_order=pro_value_from_order.value_from_order;

-- B267
update production_pe.out_order_tracking set out_order_tracking.shipped = case when date_shipped is not null then 1 else 0 end;

-- B268
update production_pe.out_order_tracking set out_order_tracking.delivered = case when date_delivered is not null then 1 else 0 end;
 
-- B269 
update production_pe.out_order_tracking set out_order_tracking.first_attempt = 1 where date_1st_attempt is not null;

-- B270 U
update production_pe.out_stock_hist set out_stock_hist.entrance_last_30= case when datediff(curdate(),date_entrance)<30 then 1 else 0 end, out_stock_hist.oms_po_last_30= case when datediff(curdate(),date_procurement_order)<=30 and datediff(curdate(),date_procurement_order)>0 then 1 else 0 end;
 
-- B271 U
update production_pe.out_order_tracking set out_order_tracking.procured_promised_last_30 = case when datediff(curdate(),date_procured_promised)<=30 and datediff(curdate(),date_procured_promised)>0 then 1 else 0 end;
 
-- B272 U
update production_pe.out_order_tracking set out_order_tracking.shipped_last_30 = case when datediff(curdate(),date_shipped)<=30 and datediff(curdate(),date_shipped)>0 then 1 else 0 end;
 
-- B273 U
update production_pe.out_order_tracking set out_order_tracking.delivered_promised_last_30 = case when datediff(curdate(),date_delivered_promised)<=30 and datediff(curdate(),date_delivered_promised)>0 then 1 else 0 end;

-- B276 U
update production_pe.out_order_tracking o inner join production_pe.tbl_order_detail t on o.order_item_id=t.item set o.actual_paid_price_after_tax=t.paid_price_after_vat, o.costo_after_tax=t.costo_after_vat;

-- B277 U
update production_pe.out_order_tracking a inner join bob_live_pe.catalog_config b on a.sku_config = b.sku inner join bob_live_pe.catalog_attribute_option_global_category c on b.fk_catalog_attribute_option_global_category = c.id_catalog_attribute_option_global_category inner join bob_live_pe.catalog_attribute_option_global_sub_category d on b.fk_catalog_attribute_option_global_sub_category = d.id_catalog_attribute_option_global_sub_category inner join bob_live_pe.catalog_attribute_option_global_sub_sub_category e on b.fk_catalog_attribute_option_global_sub_sub_category = e.id_catalog_attribute_option_global_sub_sub_category set a.category_com_main = c.name, a.category_com_sub = d.name, a.category_com_sub_sub = e.name;

-- B278 U
update production_pe.out_order_tracking 
inner join bob_live_pe.shipment_zone_mapping szm on szm.area_code = out_order_tracking.ship_to_zip 
set dep =  (if(szm.area_1 is null, 0,szm.area_1 ));

-- B279 U
update production_pe.out_order_tracking 
inner join bob_live_pe.shipment_zone_mapping szm on szm.area_code = out_order_tracking.ship_to_zip 
set prov=  (if(szm.area_2 is null, 0,szm.area_2 ));

-- B280 U
update production_pe.out_order_tracking 
inner join bob_live_pe.shipment_zone_mapping szm on szm.area_code = out_order_tracking.ship_to_zip 
set dist=  (if(szm.area_3 is null, 0,szm.area_3 ));

-- B281 U
update production_pe.out_order_tracking set ship_to_zip2 = left(ship_to_zip,2), ship_to_zip3 = left(ship_to_zip,3);

-- B282 U
update production_pe.out_order_tracking a inner join production_pe.tbl_order_detail b on a.order_number = b.order_nr set a.coupon_code = b.coupon_code;

-- B283 U
update production_pe.out_order_tracking set is_corporate_sale = 1 where coupon_code like'CDEAL%';

-- B284
update production_pe.out_order_tracking a inner join development_mx.A_E_M1_New_CategoryBP b on a.category_com_sub = b.cat2 set a.category_bp = b.CatBP;

-- B285 
update production_pe.out_order_tracking a inner join development_mx.A_E_M1_New_CategoryBP b on a.category_com_main = b.cat1 set a.category_bp = b.CatBP where a.category_bp is null;

-- B286
update production_pe.out_order_tracking set fullfilment_type_bp =
case when fullfilment_type_real = 'crossdock' then 'crossdocking'
when fullfilment_type_real = 'inventory' and fullfilment_type_bob = 'consignment' then 'consignment'
when fullfilment_type_real = 'inventory' and fullfilment_type_bob <> 'consignment' then 'outright buying'
when fullfilment_type_real = 'dropshipping' then 'other' end;

-- Operaciones
-- Macro C

-- C001 D
delete production_pe.out_stock_hist.* from production_pe.out_stock_hist;

-- C002 A
insert into production_pe.out_stock_hist ( stock_item_id, barcode_wms, date_entrance, barcode_bob_duplicated, in_stock, wh_location )
select estoque.estoque_id, estoque.cod_barras, case when data_criacao is null then null else date(data_criacao) end as expr1, estoque.minucioso, posicoes.participa_estoque, estoque.endereco from (wmsprod_pe.estoque left join wmsprod_pe.itens_recebimento on estoque.itens_recebimento_id = itens_recebimento.itens_recebimento_id) left join wmsprod_pe.posicoes on estoque.endereco = posicoes.posicao;

-- C002 a
update production_pe.out_stock_hist inner join wmsprod_pe.movimentacoes on out_stock_hist.stock_item_id = movimentacoes.estoque_id set out_stock_hist.date_entrance = date(movimentacoes.data_criacao)
where movimentacoes.de_endereco='retorno de cancelamento' or movimentacoes.de_endereco='vendidos';

-- C003 D
delete production_pe.pro_unique_ean_bob.* from production_pe.pro_unique_ean_bob;

-- C004 A
insert into production_pe.pro_unique_ean_bob ( ean ) select produtos.ean as ean from bob_live_pe.catalog_simple inner join wmsprod_pe.produtos on catalog_simple.sku = produtos.sku where catalog_simple.status not like "deleted"
group by produtos.ean
having count(produtos.ean=1);

-- C102 U
update production_pe.out_stock_hist inner join wmsprod_pe.traducciones_producto on out_stock_hist.barcode_wms = traducciones_producto.identificador set out_stock_hist.sku_simple = sku;

-- C104 U
update production_pe.out_stock_hist inner join wmsprod_pe.estoque on out_stock_hist.stock_item_id = estoque.estoque_id set out_stock_hist.date_exit = date(data_ultima_movimentacao), out_stock_hist.exit_type = "sold" where out_stock_hist.wh_location="vendidos";

-- C105 U
update production_pe.out_stock_hist inner join wmsprod_pe.estoque on out_stock_hist.stock_item_id = estoque.estoque_id set out_stock_hist.date_exit = date(data_ultima_movimentacao), out_stock_hist.exit_type = "error" where out_stock_hist.wh_location="error";

-- C106 U
update (((production_pe.out_stock_hist inner join bob_live_pe.
catalog_simple on out_stock_hist.sku_simple = catalog_simple.sku) inner join bob_live_pe.
catalog_config on catalog_simple.fk_catalog_config = catalog_config.id_catalog_config) inner join bob_live_pe.
catalog_shipment_type on catalog_simple.fk_catalog_shipment_type = catalog_shipment_type.id_catalog_shipment_type) inner join bob_live_pe.supplier on catalog_config.fk_supplier = supplier.id_supplier set out_stock_hist.barcode_bob = barcode_ean, out_stock_hist.sku_config = catalog_config.sku, out_stock_hist.sku_name = catalog_config.name, out_stock_hist.sku_supplier_config = catalog_config.sku_supplier_config, out_stock_hist.model = catalog_config.model, out_stock_hist.cost = catalog_simple.cost, out_stock_hist.supplier_name = supplier.name, out_stock_hist.supplier_id = supplier.id_supplier, out_stock_hist.fullfilment_type_bob = catalog_shipment_type.name, out_stock_hist.price = catalog_simple.price;

-- C107 U
update production_pe.out_stock_hist inner join production_pe.pro_category_tree on out_stock_hist.sku_config = pro_category_tree.sku set out_stock_hist.category = cat1, out_stock_hist.cat2 = pro_category_tree.cat2;

-- C107a U
update production_pe.out_stock_hist inner join production_pe.exceptions_supplier_categories on out_stock_hist.supplier_name = exceptions_supplier_categories.supplier set out_stock_hist.category = "ropa, calzado y accesorios"
where out_stock_hist.category="deportes";

-- C107b U
update production_pe.out_stock_hist set out_stock_hist.category = "salud y cuidado personal"
where out_stock_hist.category="ropa, calzado y accesorios" and out_stock_hist.cat2="salud y cuidado personal";

-- C107c U
update production_pe.out_stock_hist inner join bob_live_pe.catalog_config on out_stock_hist.sku_config = catalog_config.sku inner join bob_live_pe.catalog_attribute_option_global_buyer on catalog_attribute_option_global_buyer.id_catalog_attribute_option_global_buyer = catalog_config.fk_catalog_attribute_option_global_buyer set out_stock_hist.buyer = catalog_attribute_option_global_buyer.name;

-- C107d U
update production_pe.out_stock_hist inner join production_pe.buyer_category on out_stock_hist.category = buyer_category.category set out_stock_hist.category_english = buyer_category.category_english;

-- C108 U
update production_pe.out_stock_hist set out_stock_hist.sold_last_30= case when datediff(curdate(),date_exit)<30 then 1 else 0 end, out_stock_hist.sold_yesterday = case when datediff(curdate(),(date_sub(date_exit, interval 1 day)))=0 then 1 else 0 end, out_stock_hist.sold_last_10 = case when datediff(curdate(),date_exit)<10 then 1 else 0 end, out_stock_hist.sold_last_7 = case when datediff(curdate(),date_exit)<7 then 1 else 0 end where out_stock_hist.exit_type="sold";

-- C108a U
update production_pe.out_stock_hist set out_stock_hist.entrance_last_30= case when datediff(curdate(),date_entrance)<30 then 1 else 0 end, out_stock_hist.oms_po_last_30= case when datediff(curdate(),date_procurement_order)<30 then 1 else 0 end;

-- C109 U
update production_pe.out_stock_hist set out_stock_hist.days_in_stock = case when date_exit is null then datediff(curdate(),date_entrance) else datediff(date_exit,date_entrance) end;

-- Extra
update production_pe.out_stock_hist set out_stock_hist.fullfilment_type_real = "inventory";

-- C110 U
update production_pe.out_stock_hist inner join (wmsprod_pe.itens_venda inner join wmsprod_pe.status_itens_venda on itens_venda.itens_venda_id = status_itens_venda.itens_venda_id) on out_stock_hist.stock_item_id = itens_venda.estoque_id set out_stock_hist.fullfilment_type_real = "crossdock"
where status_itens_venda.status="analisando quebra" or status_itens_venda.status="aguardando estoque";

-- C110 U Extra
update (production_pe.out_stock_hist inner join wmsprod_pe.itens_venda on out_stock_hist.stock_item_id = itens_venda.estoque_id) inner join wmsprod_pe.status_itens_venda on itens_venda.itens_venda_id = status_itens_venda.itens_venda_id set out_stock_hist.fullfilment_type_real = "dropshipping"
where (((status_itens_venda.status)="ds estoque reservado"));

-- C110 c
update production_pe.out_stock_hist inner join wmsprod_pe.movimentacoes on out_stock_hist.stock_item_id = movimentacoes.estoque_id set out_stock_hist.fullfilment_type_real = 'inventory'
where movimentacoes.de_endereco='retorno de cancelamento' or movimentacoes.de_endereco='vendidos';

-- C111 U
update production_pe.out_stock_hist inner join bob_live_pe.catalog_simple on out_stock_hist.sku_simple = catalog_simple.sku inner join bob_live_pe.catalog_tax_class on catalog_simple.fk_catalog_tax_class = catalog_tax_class.id_catalog_tax_class set out_stock_hist.tax_percentage = tax_percent, out_stock_hist.cost_w_o_vat = out_stock_hist.cost/(1+tax_percent/100);

-- C111b U
update production_pe.out_stock_hist set out_stock_hist.in_stock_cost_w_o_vat = out_stock_hist.cost_w_o_vat where out_stock_hist.in_stock=1;

-- C111c U
update production_pe.out_stock_hist set out_stock_hist.sold_last_30_cost_w_o_vat = out_stock_hist.cost_w_o_vat, out_stock_hist.sold_last_30_price = out_stock_hist.price
where out_stock_hist.sold_last_30=1;

-- C112 D
delete production_pe.pro_stock_hist_sku_count.* from production_pe.pro_stock_hist_sku_count;

-- C113 A
insert into production_pe.pro_stock_hist_sku_count ( sku_simple, sku_count )
select out_stock_hist.sku_simple, sum(out_stock_hist.item_counter) as sumofitem_counter from production_pe.out_stock_hist
group by out_stock_hist.sku_simple, out_stock_hist.in_stock having out_stock_hist.in_stock=1;

-- C114 U
update production_pe.out_stock_hist inner join production_pe.pro_stock_hist_sku_count on out_stock_hist.sku_simple = pro_stock_hist_sku_count.sku_simple set out_stock_hist.sku_counter= 1/pro_stock_hist_sku_count.sku_count;

-- C114b U
update production_pe.out_stock_hist set out_stock_hist.reserved = '0';

-- C115 U
update production_pe.out_stock_hist inner join wmsprod_pe.estoque on out_stock_hist.stock_item_id = estoque.estoque_id set out_stock_hist.reserved = "1"
where (((estoque.almoxarifado)="separando" or (estoque.almoxarifado)="estoque reservado" or (estoque.almoxarifado)="aguardando separacao"));

-- C116 U
update production_pe.out_stock_hist inner join bob_live_pe.catalog_simple on out_stock_hist.sku_simple = catalog_simple.sku set out_stock_hist.supplier_leadtime = catalog_simple.delivery_time_supplier;

-- C117 U
update production_pe.out_stock_hist inner join ((bob_live_pe.catalog_config inner join bob_live_pe.catalog_simple on catalog_config.id_catalog_config = catalog_simple.fk_catalog_config) inner join bob_live_pe.catalog_stock on catalog_simple.id_catalog_simple = catalog_stock.fk_catalog_simple) on out_stock_hist.sku_config = catalog_config.sku set out_stock_hist.visible = 1
where (((catalog_config.pet_status)="creation,edited,images") and ((catalog_config.pet_approved)=1) and ((catalog_config.status)="active") and ((catalog_simple.status)="active") and ((catalog_simple.price)>0) and ((catalog_config.display_if_out_of_stock)=0) and ((catalog_stock.quantity)>0) and ((catalog_simple.cost)>0)) or (((catalog_config.pet_status)="creation,edited,images") and ((catalog_config.pet_approved)=1) and ((catalog_config.status)="active") and ((catalog_simple.status)="active") and ((catalog_simple.price)>0) and ((catalog_config.display_if_out_of_stock)=1) and ((catalog_stock.quantity)>0) and ((catalog_simple.cost)>0));

-- C118 U
update production_pe.out_stock_hist set out_stock_hist.week_exit = production_pe.week_iso(date_exit);

-- C119a D
delete production_pe.pro_stock_hist_sold_last_30_count.* from production_pe.pro_stock_hist_sold_last_30_count;

-- C119b A
insert into production_pe.pro_stock_hist_sold_last_30_count ( sold_last_30, sku_simple, count_of_item_counter )
select out_stock_hist.sold_last_30, out_stock_hist.sku_simple, count(out_stock_hist.item_counter) as count_of_item_counter
from production_pe.out_stock_hist group by out_stock_hist.sold_last_30, out_stock_hist.sku_simple having out_stock_hist.sold_last_30=1;

-- C119c U
update production_pe.out_stock_hist inner join production_pe.pro_stock_hist_sold_last_30_count on out_stock_hist.sku_simple = pro_stock_hist_sold_last_30_count.sku_simple set out_stock_hist.sold_last_30_counter = 1/pro_stock_hist_sold_last_30_count.count_of_item_counter;

-- C120 U
update production_pe.out_stock_hist set out_stock_hist.sku_simple_blank = 1
where out_stock_hist.sku_simple is null;

-- C121 U
update (wmsprod_pe.itens_recebimento inner join wmsprod_pe.estoque on itens_recebimento.itens_recebimento_id = estoque.itens_recebimento_id) inner join production_pe.out_stock_hist on estoque.estoque_id = out_stock_hist.stock_item_id set out_stock_hist.quarantine = 1 where itens_recebimento.endereco="cuarentena" or itens_recebimento.endereco="cuarenta2";

-- C124 D
delete production_pe.pro_sum_last_5_out_of_6_wks.* from production_pe.pro_sum_last_5_out_of_6_wks;

-- C125 A
insert into production_pe.pro_sum_last_5_out_of_6_wks ( sku_simple, expr1 )
select max_last_6_wks.sku_simple, sumofcountofdate_exit-maxofcountofdate_exit as expr1 from production_pe.max_last_6_wks
group by max_last_6_wks.sku_simple, sumofcountofdate_exit-maxofcountofdate_exit;

-- C126 U
update production_pe.out_stock_hist inner join production_pe.pro_sum_last_5_out_of_6_wks on out_stock_hist.sku_simple = pro_sum_last_5_out_of_6_wks.sku_simple set out_stock_hist.sum_sold_last_5_out_of_6_wks = pro_sum_last_5_out_of_6_wks.expr1;

-- C127 U
update production_pe.out_stock_hist inner join bob_live_pe.catalog_config on out_stock_hist.sku_config = catalog_config.sku set out_stock_hist.package_height = catalog_config.package_height, out_stock_hist.package_length = catalog_config.package_length, out_stock_hist.package_width = catalog_config.package_width;

-- C128 U
update production_pe.out_stock_hist set out_stock_hist.vol_weight = package_height*package_length*package_width/5000;

-- C129 U
update production_pe.out_stock_hist inner join bob_live_pe.catalog_config on out_stock_hist.sku_config = catalog_config.sku set out_stock_hist.package_weight = catalog_config.package_weight;

-- C130 U
update production_pe.out_stock_hist set out_stock_hist.max_vol_w_vs_w = case when vol_weight>package_weight then vol_weight else package_weight end;

-- C131 U
update production_pe.out_stock_hist set out_stock_hist.package_measure = case when max_vol_w_vs_w>45.486 then "large" else (case when max_vol_w_vs_w>2.277 then "medium" else "small" end)end;

-- C132 U
update production_pe.out_stock_hist inner join wmsprod_pe.itens_inventario on out_stock_hist.barcode_wms = itens_inventario.cod_barras set out_stock_hist.sub_position = sub_endereco;

-- C133 U
update wmsprod_pe.posicoes inner join production_pe.out_stock_hist on posicoes.posicao = out_stock_hist.wh_location set out_stock_hist.position_type= posicoes.tipo_posicao;

-- C135 U
update production_pe.out_stock_hist set out_stock_hist.position_item_size_type = case when position_type="safe" then "small" else (case when position_type="mezanine" then "small" else (case when position_type="muebles" then "large" else "tbd" end)end)end;

-- C136 D
delete from production_pe.items_procured_in_transit;

-- C136 A
insert into production_pe.items_procured_in_transit ( sku_simple, number_ordered, unit_price )
select catalog_simple.sku, count(procurement_order_item.id_procurement_order_item) as countofid_procurement_order_item, avg(procurement_order_item.unit_price) as avgofunit_price
from bob_live_pe.catalog_simple inner join (procurement_live_pe.procurement_order_item inner join procurement_live_pe.procurement_order on procurement_order_item.fk_procurement_order = procurement_order.id_procurement_order) on catalog_simple.id_catalog_simple = procurement_order_item.fk_catalog_simple
where (((procurement_order_item.is_deleted)=0) and ((procurement_order.is_cancelled)=0) and ((procurement_order_item.sku_received)=0))
group by catalog_simple.sku;

-- C137 U
update production_pe.out_stock_hist inner join production_pe.items_procured_in_transit on out_stock_hist.sku_simple = items_procured_in_transit.sku_simple set out_stock_hist.items_procured_in_transit = number_ordered, out_stock_hist.procurement_price = unit_price;

-- C138 U
update (procurement_live_pe.procurement_order inner join ((bob_live_pe.catalog_simple inner join production_pe.out_stock_hist on catalog_simple.sku = out_stock_hist.sku_simple) inner join procurement_live_pe.procurement_order_item on catalog_simple.id_catalog_simple = procurement_order_item.fk_catalog_simple) on procurement_order.id_procurement_order = procurement_order_item.fk_procurement_order) set out_stock_hist.date_procurement_order = procurement_order.created_at;

-- C139 U
update production_pe.out_stock_hist set out_stock_hist.date_payable_promised = adddate(date_procurement_order, interval oms_payment_terms day) where out_stock_hist.oms_payment_event in("factura","pedido");

-- C140 U
update production_pe.out_stock_hist set out_stock_hist.date_payable_promised = adddate(date_procurement_order, interval oms_payment_terms + supplier_leadtime day)
where out_stock_hist.oms_payment_event="entrega";
 
-- C142 U
update production_pe.out_stock_hist set out_stock_hist.sold = 1
where out_stock_hist.exit_type="sold";

-- C143 D
delete from production_pe.sell_rate_simple;
 
-- C144 A
insert into production_pe.sell_rate_simple(sku_simple,num_items,num_sales,average_days_in_stock)
select sku_simple, sum(item_counter),sum(sold),avg(days_in_stock)from production_pe.out_stock_hist group by sku_simple;
 
-- C144b U
update production_pe.sell_rate_simple srs set srs.num_items_available = ( select sum(in_stock) from production_pe.out_stock_hist osh where reserved = '0' and srs.sku_simple = osh.sku_simple group by sku_simple);
 
-- C145 U
update production_pe.sell_rate_simple set sell_rate_simple.average_sell_rate = case when average_days_in_stock=0 then 0 else num_sales/average_days_in_stock end;
 
-- C145b U
update production_pe.sell_rate_simple set sell_rate_simple.remaining_days = case when average_sell_rate=0 then 0 else num_items_available/average_sell_rate end;
 
-- C155 D
		DELETE
		FROM
			production_pe.sell_rate_config ; 

-- C156 A
			INSERT INTO production_pe.sell_rate_config (
				sku_config,
				num_items,
				num_sales
			) SELECT
				sku_config,
				sum(item_counter),
				sum(sold)
			FROM
				production_pe.out_stock_hist
			WHERE
				datediff(curdate(), date_exit) <= 42
			GROUP BY
				sku_config ; 

-- C157 U
				UPDATE production_pe.sell_rate_config a
			INNER JOIN (
				SELECT
					sku_config,
					avg(days_in_stock) avg_days
				FROM
					production_pe.out_stock_hist
				WHERE
					in_stock = 1
				AND reserved = 0
				GROUP BY
					sku_config
			) b ON a.sku_config = b.sku_config
			SET a.average_days_in_stock = CASE
			WHEN b.avg_days >= 42 THEN
				42
			ELSE
				b.avg_days
			END ; 

-- C158 U
			UPDATE production_pe.sell_rate_config a
			SET a.num_items_available = (
				SELECT
					sum(b.in_stock)
				FROM
					production_pe.out_stock_hist b
				WHERE
					b.reserved = '0'
				AND a.sku_config = b.sku_config
				GROUP BY
					sku_config
			) ; 

-- C159 U
			UPDATE production_pe.sell_rate_config a
			SET a.average_sell_rate = CASE
			WHEN a.average_days_in_stock = 0 THEN
				0
			ELSE
				a.num_sales / a.average_days_in_stock
			END ; 

-- C160 U
			UPDATE production_pe.sell_rate_config
			SET sell_rate_config.remaining_days = CASE
			WHEN average_sell_rate = 0 THEN
				0
			ELSE
				num_items_available / average_sell_rate
			END ; 

-- C161 U
			UPDATE production_pe.out_stock_hist a
			INNER JOIN production_pe.sell_rate_config b ON a.sku_config = b.sku_config
			SET a.average_remaining_days = b.remaining_days ;


-- C146 D
delete from production_pe.pro_max_days_in_stock; 

-- C163 A
				INSERT INTO production_pe.pro_max_days_in_stock (
					sku_config,
					max_days_in_stock,
					min_days_in_stock
				) SELECT
					sku_config,
					max(days_in_stock),
					min(days_in_stock)
				FROM
					production_pe.out_stock_hist
				WHERE
					in_stock = 1
				AND reserved = 0
				GROUP BY
					sku_config ; 

-- C164 U 
					UPDATE production_pe.out_stock_hist
				INNER JOIN production_pe.pro_max_days_in_stock ON out_stock_hist.sku_config = pro_max_days_in_stock.sku_config
				SET out_stock_hist.max_days_in_stock = pro_max_days_in_stock.max_days_in_stock,
				out_stock_hist.min_days_in_stock = pro_max_days_in_stock.min_days_in_stock ;

-- C149 U
update production_pe.out_stock_hist set out_stock_hist.week_payable_promised = production_pe.week_iso(date_payable_promised);

-- C150 U
update production_pe.out_stock_hist set out_stock_hist. week_procurement_order = production_pe.week_iso(date_procurement_order);

-- C151 U
update production_pe.out_stock_hist inner join bob_live_pe.catalog_simple on out_stock_hist.sku_simple = catalog_simple.sku set out_stock_hist.delivery_cost_supplier = catalog_simple.delivery_cost_supplier;

-- C152 U
update production_pe.out_stock_hist set cogs = cost_w_o_vat + delivery_cost_supplier;

-- C153 U
update production_pe.out_stock_hist set out_stock_hist.days_payable_since_entrance = case when date_entrance is null then null else (case when oms_payment_event in('factura','pedido') then oms_payment_terms - supplier_leadtime else (case when oms_payment_event = 'entrega' then oms_payment_terms end)end)end;

-- C154 U
update production_pe.out_stock_hist a inner join bob_live_pe.catalog_config b on a.sku_config = b.sku inner join bob_live_pe.catalog_attribute_option_global_category c on b.fk_catalog_attribute_option_global_category = c.id_catalog_attribute_option_global_category inner join bob_live_pe.catalog_attribute_option_global_sub_category d on b.fk_catalog_attribute_option_global_sub_category = d.id_catalog_attribute_option_global_sub_category inner join bob_live_pe.catalog_attribute_option_global_sub_sub_category e on b.fk_catalog_attribute_option_global_sub_sub_category = e.id_catalog_attribute_option_global_sub_sub_category set a.category_com_main = c.name, a.category_com_sub = d.name, a.category_com_sub_sub = e.name;

-- C173 U
		UPDATE production_pe.out_stock_hist
		SET fullfilment_type_bp = CASE
		WHEN fullfilment_type_real = 'crossdock' THEN
			'crossdocking'
		WHEN fullfilment_type_real = 'inventory'
		AND fullfilment_type_bob = 'consignment' THEN
			'consignment'
		WHEN fullfilment_type_real = 'inventory'
		AND fullfilment_type_bob <> 'consignment' THEN
			'outright buying'
		WHEN fullfilment_type_real = 'dropshipping' THEN
			'other'
		END ;

-- C174 D
SET @i = 0 ; 
DROP TEMPORARY TABLE IF EXISTS TMP ; 
CREATE TEMPORARY TABLE TMP 
SELECT
	sku_config,
	Count(*) AS items_in_stock,
	sum(cost_w_o_vat) AS cost_w_o_vat_in_stock
FROM
	production_pe.out_stock_hist
WHERE
	in_stock = 1
AND reserved = 0
AND (
			fullfilment_type_bob <> 'consignment'
			OR fullfilment_type_bob IS NOT NULL
		)
GROUP BY
	sku_config;

DROP TABLE IF EXISTS production_pe.top_skus_sell_list ; 
CREATE TABLE production_pe.top_skus_sell_list 
SELECT
	sku_config,
	items_in_stock,
	cost_w_o_vat_in_stock,
	@i := @i + 1 AS rkn
FROM
	TMP
ORDER BY
	cost_w_o_vat_in_stock DESC ;

update production_pe.out_order_tracking A set A.date_delivered='2013-02-26' where A.order_item_id='20673';
update production_pe.out_order_tracking A set A.date_delivered='2013-02-26' where A.order_item_id='20674';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-15' where A.order_item_id='51320';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-14' where A.order_item_id='51916';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-15' where A.order_item_id='51851';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-15' where A.order_item_id='51852';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='52063';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='51894';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-14' where A.order_item_id='51880';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='52150';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-14' where A.order_item_id='52143';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-15' where A.order_item_id='52393';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='52378';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-15' where A.order_item_id='52305';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-15' where A.order_item_id='52093';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-17' where A.order_item_id='51909';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-24' where A.order_item_id='52239';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='52031';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-16' where A.order_item_id='52038';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-16' where A.order_item_id='52039';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-16' where A.order_item_id='52767';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-16' where A.order_item_id='52211';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-16' where A.order_item_id='52790';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-16' where A.order_item_id='52791';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-16' where A.order_item_id='52792';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-16' where A.order_item_id='52645';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='52828';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='52829';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='51919';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='51920';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='52867';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='52868';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='52869';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='52685';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='52969';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='52970';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='52971';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-17' where A.order_item_id='52887';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='52743';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53010';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53041';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53118';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53146';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53068';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-17' where A.order_item_id='52694';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-17' where A.order_item_id='52852';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53016';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-17' where A.order_item_id='53218';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53234';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-17' where A.order_item_id='53264';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53253';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53254';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53255';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-23' where A.order_item_id='53272';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='52918';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53478';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53471';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53089';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53526';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53202';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53416';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53131';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53132';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53133';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53387';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53402';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53622';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53666';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53413';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53669';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53670';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53671';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53672';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53673';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53354';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53747';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53757';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53758';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53714';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53715';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53383';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53425';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53699';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53786';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53787';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53686';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53687';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53698';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53700';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53800';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53801';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53735';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53737';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53763';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='52902';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53592';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54089';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54095';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54053';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53824';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53845';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53881';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53882';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53861';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53869';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53906';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54105';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53990';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53989';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53983';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='54137';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='54138';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53963';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53967';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53898';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53974';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54166';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54161';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54157';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53853';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53807';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54203';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53575';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54193';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54191';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53767';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54131';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54130';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54211';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53877';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54000';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53828';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53829';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53889';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53891';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='54223';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54182';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='54180';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54088';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54052';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54251';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='54249';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-23' where A.order_item_id='54204';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='54301';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='54290';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53918';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='54369';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='54368';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53932';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='54266';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='54382';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53994';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='54378';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='54392';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='54019';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='54354';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='54275';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='54225';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-24' where A.order_item_id='54255';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-24' where A.order_item_id='54256';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-24' where A.order_item_id='54257';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-24' where A.order_item_id='54258';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-24' where A.order_item_id='54259';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='54362';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='54530';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='54537';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='54426';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53774';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53775';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53776';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53777';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53778';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53779';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53780';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53781';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53782';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53783';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53822';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53823';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='54471';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='54477';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-23' where A.order_item_id='54743';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='54845';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-23' where A.order_item_id='54980';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-24' where A.order_item_id='55073';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='55217';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-24' where A.order_item_id='55339';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='55474';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='55563';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='55642';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-28' where A.order_item_id='55834';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='54282';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='54283';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='55488';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-28' where A.order_item_id='55521';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='55854';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='55878';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='55879';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='55880';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='55958';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='55845';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='56355';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='56343';
update production_pe.out_order_tracking A set A.date_delivered='2013-06-1' where A.order_item_id='56195';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-28' where A.order_item_id='56261';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='55938';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-28' where A.order_item_id='55988';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-28' where A.order_item_id='55996';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-28' where A.order_item_id='56468';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-28' where A.order_item_id='56506';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='56542';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='56490';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='56547';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='56142';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='56153';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='56189';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='56816';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='56949';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='56956';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='57057';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='56096';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='56999';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='56215';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='57073';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='57018';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='57270';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='57271';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='57272';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='57273';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='57274';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='57275';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='57148';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='57345';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='57205';
update production_pe.out_order_tracking A set A.date_delivered='2013-06-1' where A.order_item_id='56446';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='57449';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='56233';
update production_pe.out_order_tracking A set A.date_delivered='2013-06-1' where A.order_item_id='56979';
update production_pe.out_order_tracking A set A.date_delivered='2013-06-1' where A.order_item_id='56980';
update production_pe.out_order_tracking A set A.date_delivered='2013-06-1' where A.order_item_id='56981';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='56758';
update production_pe.out_order_tracking A set A.date_delivered='2013-06-1' where A.order_item_id='57669';
update production_pe.out_order_tracking A set A.date_delivered='2013-06-1' where A.order_item_id='57764';
update production_pe.out_order_tracking A set A.date_delivered='2013-06-1' where A.order_item_id='57807';
update production_pe.out_order_tracking A set A.date_delivered='2013-06-1' where A.order_item_id='57784';
update production_pe.out_order_tracking A set A.date_delivered='2013-06-1' where A.order_item_id='57794';
update production_pe.out_order_tracking A set A.date_delivered='2013-06-1' where A.order_item_id='57911';
update production_pe.out_order_tracking A set A.date_delivered='2013-06-1' where A.order_item_id='57929';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-5' where A.order_item_id='64680';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-5' where A.order_item_id='68475';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-5' where A.order_item_id='68474';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='69225';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='69249';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='69250';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='69251';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='69252';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='69253';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='69254';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='69330';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='69459';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='69460';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='69526';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='69649';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-05-24' where A.order_item_id='54980';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-05-24' where A.order_item_id='55073';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-02-19' where A.order_item_id='20673';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-02-19' where A.order_item_id='20674';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-20' where A.order_item_id='22453';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='27612';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='27613';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='27614';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='27615';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='29255';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='29538';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='29509';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='29510';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='29724';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='29682';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='30700';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='30303';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='30575';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='30826';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='31030';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='31686';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='31878';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='32160';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='30031';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='30032';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='32573';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='32479';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='32046';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='32692';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='32693';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='32674';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-03-21' where A.order_item_id='32672';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='69758';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='69492';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='69493';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-10' where A.order_item_id='69747';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-9' where A.order_item_id='69777';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='69793';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='69798';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-9' where A.order_item_id='69844';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='69876';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='69877';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='69878';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='69879';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='69888';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='69962';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='69963';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='69975';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70045';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='70170';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='69981';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='69931';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='70154';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70163';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='70239';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-10' where A.order_item_id='70124';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='70065';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='70066';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='70067';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='70068';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='70235';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='70237';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='70367';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='70279';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='70473';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70511';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='70512';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='69881';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='69955';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-10' where A.order_item_id='70033';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='70044';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='70537';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70542';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='70152';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-10' where A.order_item_id='70156';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='70637';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='70593';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='70582';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70662';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70663';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70664';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70665';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='70620';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='70676';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70147';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70148';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70639';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70640';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70658';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='70741';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='70788';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70790';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70793';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70800';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70801';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70802';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70803';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='70807';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='70810';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='70811';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='70813';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70685';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70686';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70687';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='70816';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='70822';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='70823';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='70840';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='70830';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='70831';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='70834';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='70863';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70875';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='70903';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='70904';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='70890';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='70891';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='70910';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='70913';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70915';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='70916';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='70949';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='70937';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='70960';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='71012';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='70968';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='70965';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='70984';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='71016';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='71017';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71021';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71025';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='71022';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='70882';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='70883';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='70417';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='70562';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='70826';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='71023';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='71024';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='70854';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='71051';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='71097';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71100';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='71116';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='71117';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='71130';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='71135';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-11' where A.order_item_id='71136';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71146';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71152';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70633';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='70634';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71163';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71245';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71259';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71188';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71271';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71272';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71273';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71274';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71275';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71192';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71205';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71206';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71207';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71213';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71214';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71236';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71220';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71221';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='71253';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='71269';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71279';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71286';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71306';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71335';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71307';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71246';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='71341';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71317';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71318';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71330';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71338';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71339';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71340';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71344';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71345';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71346';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71347';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71354';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71355';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71356';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71357';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71348';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71368';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='71371';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71374';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71383';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='71379';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71391';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71393';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71403';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71427';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71319';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71235';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71437';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71439';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='71440';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71458';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71469';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='71471';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71402';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71478';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71244';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-12' where A.order_item_id='71075';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='71658';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71667';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71636';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71676';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71677';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71512';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71685';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71514';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71689';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='71538';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71574';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71584';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='71594';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71712';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71713';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71715';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71720';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71407';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='71734';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='71737';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='71751';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='71782';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='71783';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71799';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='71817';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='71823';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71832';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71833';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-13' where A.order_item_id='71834';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='71838';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='71839';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='71846';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='71847';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='71873';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='71882';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='71885';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='71895';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='71919';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='71922';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='71926';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='71927';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='71931';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='71935';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='71940';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='71958';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='71989';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='71992';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='71993';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='71995';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72008';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72013';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72016';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72017';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72023';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72024';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72025';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72026';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72027';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72028';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72045';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72047';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72048';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72056';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72057';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72058';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72104';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='72122';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='72127';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72135';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72144';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72145';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72149';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72150';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72175';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72182';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72183';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72184';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72193';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72196';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72214';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72221';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72224';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72225';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72234';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72250';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72251';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72257';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72259';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72260';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72261';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72271';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72272';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72273';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72300';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72301';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72302';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72314';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72317';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72326';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72332';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72334';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72335';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72336';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72338';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72340';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72363';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72364';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72365';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72374';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72378';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72379';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72381';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72389';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='71581';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72390';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72395';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72402';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72420';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72421';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72433';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72434';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72451';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72452';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72453';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72454';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72455';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72471';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72474';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72479';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72484';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72487';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72488';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72497';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72499';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72500';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72506';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72510';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72513';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72514';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72519';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72541';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72543';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72548';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72563';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72564';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72570';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72571';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72572';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='72578';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72580';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-16' where A.order_item_id='72586';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72599';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72606';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72612';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72632';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72633';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72639';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72668';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72672';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72679';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72691';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72692';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72693';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72695';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72701';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72711';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72717';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72731';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72737';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72742';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72746';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72763';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72764';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72769';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72772';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72785';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72787';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72799';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72800';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72806';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72810';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72817';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72835';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72840';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72843';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72854';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72856';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72859';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72862';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72864';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72865';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72866';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='72872';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72879';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72880';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72881';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-17' where A.order_item_id='72882';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='72897';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='72904';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='72906';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='72912';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='72915';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='72968';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='73347';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='73407';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='73504';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='73548';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='73575';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='73666';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='73708';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='73709';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='73710';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='73731';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='73776';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='74131';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='74145';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='74278';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='74279';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='74355';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='74356';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='74366';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='74367';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-18' where A.order_item_id='74368';
update production_pe.out_order_tracking A set A.date_1st_attempt='2013-07-19' where A.order_item_id='74873';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='54282';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53592';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='55488';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53686';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='54477';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54354';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53526';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53671';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-16' where A.order_item_id='52211';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54161';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53068';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-16' where A.order_item_id='52038';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='55880';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='54283';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54105';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-17' where A.order_item_id='53218';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-15' where A.order_item_id='52093';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-15' where A.order_item_id='51851';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='56758';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-23' where A.order_item_id='54980';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53781';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53757';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53669';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-16' where A.order_item_id='52039';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-15' where A.order_item_id='51320';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53402';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53253';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='57807';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='57148';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='56490';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-28' where A.order_item_id='56468';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='55854';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53782';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54362';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54088';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54211';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54193';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54157';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53989';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53800';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53758';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53747';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53670';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53016';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-15' where A.order_item_id='52393';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-15' where A.order_item_id='51852';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-14' where A.order_item_id='51916';
update production_pe.out_order_tracking A set A.date_delivered='2013-02-18' where A.order_item_id='20673';
update production_pe.out_order_tracking A set A.date_delivered='2013-02-18' where A.order_item_id='20674';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='56956';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='55834';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53823';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53779';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53829';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53666';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53133';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53234';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-16' where A.order_item_id='52791';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='57784';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='55339';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='56215';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54203';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53118';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-16' where A.order_item_id='52792';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-17' where A.order_item_id='51909';
update production_pe.out_order_tracking A set A.date_delivered='2013-03-20' where A.order_item_id='41353';
update production_pe.out_order_tracking A set A.date_delivered='2013-03-20' where A.order_item_id='41353';
update production_pe.out_order_tracking A set A.date_delivered='2013-03-20' where A.order_item_id='41353';
update production_pe.out_order_tracking A set A.date_delivered='2013-03-20' where A.order_item_id='41353';
update production_pe.out_order_tracking A set A.date_delivered='2013-03-20' where A.order_item_id='41353';
update production_pe.out_order_tracking A set A.date_delivered='2013-03-20' where A.order_item_id='41353';
update production_pe.out_order_tracking A set A.date_delivered='2013-03-20' where A.order_item_id='41353';
update production_pe.out_order_tracking A set A.date_delivered='2013-03-20' where A.order_item_id='41353';
update production_pe.out_order_tracking A set A.date_delivered='2013-03-20' where A.order_item_id='41353';
update production_pe.out_order_tracking A set A.date_delivered='2013-03-20' where A.order_item_id='41353';
update production_pe.out_order_tracking A set A.date_delivered='2013-03-20' where A.order_item_id='41353';
update production_pe.out_order_tracking A set A.date_delivered='2013-03-20' where A.order_item_id='41353';
update production_pe.out_order_tracking A set A.date_delivered='2013-03-20' where A.order_item_id='41353';
update production_pe.out_order_tracking A set A.date_delivered='2013-03-20' where A.order_item_id='41353';
update production_pe.out_order_tracking A set A.date_delivered='2013-03-20' where A.order_item_id='41353';
update production_pe.out_order_tracking A set A.date_delivered='2013-03-20' where A.order_item_id='41353';
update production_pe.out_order_tracking A set A.date_delivered='2013-03-20' where A.order_item_id='41353';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53801';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-28' where A.order_item_id='56816';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-16' where A.order_item_id='52645';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-24' where A.order_item_id='55474';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='55217';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53413';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53780';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53932';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53575';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='57764';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-28' where A.order_item_id='56189';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='57929';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='56195';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='54743';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54259';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54382';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54290';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54182';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53807';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53735';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53202';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='52918';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='52970';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='52869';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='57057';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53254';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53146';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53010';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-14' where A.order_item_id='51880';
update production_pe.out_order_tracking A set A.date_delivered='2013-03-20' where A.order_item_id='41353';
update production_pe.out_order_tracking A set A.date_delivered='2013-03-20' where A.order_item_id='41353';
update production_pe.out_order_tracking A set A.date_delivered='2013-03-20' where A.order_item_id='41353';
update production_pe.out_order_tracking A set A.date_delivered='2013-03-20' where A.order_item_id='41353';
update production_pe.out_order_tracking A set A.date_delivered='2013-03-20' where A.order_item_id='41353';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='57911';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='56979';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='57205';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='57345';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='57273';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='56096';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='56142';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='56547';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='56542';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='55996';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='55521';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-24' where A.order_item_id='55642';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53777';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54537';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54256';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54378';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53918';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54301';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54251';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53889';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53767';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53853';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53974';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53898';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54137';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53882';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54095';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53698';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53714';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53672';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53131';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='53272';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-17' where A.order_item_id='53264';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='52867';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='51919';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='52828';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-16' where A.order_item_id='52767';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-17' where A.order_item_id='52852';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54225';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-23' where A.order_item_id='41417';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='51894';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='52150';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-14' where A.order_item_id='52143';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='57018';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53869';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53906';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54255';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='56261';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-15' where A.order_item_id='52305';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54000';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53425';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53983';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53622';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-24' where A.order_item_id='55563';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53783';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54052';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54166';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-17' where A.order_item_id='52887';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='57449';
update production_pe.out_order_tracking A set A.date_delivered='2013-03-20' where A.order_item_id='41353';
update production_pe.out_order_tracking A set A.date_delivered='2013-03-20' where A.order_item_id='41353';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='56506';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54275';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53471';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='52743';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53737';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53786';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53089';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-17' where A.order_item_id='52694';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='57271';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='57073';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53775';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54369';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='57270';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53877';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53845';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='52378';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='57275';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54530';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='55988';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='52971';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54089';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53774';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53994';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53861';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='56981';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54426';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54180';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53699';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='57794';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53687';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='56343';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='56355';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='55878';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54257';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54249';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53891';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53763';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53715';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='52829';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='57669';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='56980';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='56233';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='57274';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='56999';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='56949';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='56153';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='55938';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='55845';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='55958';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-27' where A.order_item_id='55879';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='54845';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='54471';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53822';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53778';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54258';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54019';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54266';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53828';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54130';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54191';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53967';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54138';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53824';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='52902';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53700';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53354';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53673';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53387';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53132';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53478';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-22' where A.order_item_id='52969';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='52868';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='51920';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-16' where A.order_item_id='52790';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='52031';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='52063';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-30' where A.order_item_id='56446';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-29' where A.order_item_id='57272';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53776';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54392';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54204';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54223';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54131';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53963';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54053';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53416';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='53255';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53041';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-18' where A.order_item_id='52685';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-24' where A.order_item_id='52239';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53990';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='54368';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53881';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53787';
update production_pe.out_order_tracking A set A.date_delivered='2013-05-21' where A.order_item_id='53383';



select  'daily execution ops: end',now();

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `daily_marketing`()
BEGIN
#date

DELETE FROM tbl_daily_marketing WHERE date=CAST(CURDATE()-1 as date);

INSERT INTO tbl_daily_marketing (date, channel) 
SELECT DISTINCT date, channel_group FROM tbl_order_detail WHERE date=CAST(CURDATE()-1 as date);

#VISITS	
UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, reporting_channel, sum(visits) visits
FROM daily_costs_visits_per_channel
GROUP BY date, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_daily_marketing.visits=daily_costs_visits_per_channel.visits;

#GROSS ORDERS
UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, channel_group, sum(gross_orders) gross_orders
FROM tbl_order_detail
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=tbl_order_detail.channel_group
SET tbl_daily_marketing.gross_orders=tbl_order_detail.gross_orders;

#NET ORDERS
UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, channel_group, sum(net_orders) net_orders
FROM tbl_order_detail
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=tbl_order_detail.channel_group
SET tbl_daily_marketing.net_orders=tbl_order_detail.net_orders;

#ORDERS NET EXPECTED

UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, reporting_channel, sum(net_orders_e) net_orders_e
FROM daily_costs_visits_per_channel
GROUP BY date, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_daily_marketing.orders_net_expected=daily_costs_visits_per_channel.net_orders_e;

#CONVERSION RATE
UPDATE tbl_daily_marketing SET conversion_rate=IF(gross_orders/visits IS NULL, 0, gross_orders/visits);

#PENDING REVENUE
UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, channel_group, 
(SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat))/3.3 pending_sales
FROM tbl_order_detail WHERE tbl_order_detail.pending=1 
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing.date 
SET tbl_daily_marketing.pending_revenue=tbl_order_detail.pending_sales
WHERE tbl_daily_marketing.channel=tbl_order_detail.channel_group;

#GROSS REVENUE
UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, channel_group, 
(SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat))/3.3 gross_sales
FROM tbl_order_detail WHERE tbl_order_detail.obc=1 
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing.date 
SET tbl_daily_marketing.gross_revenue=tbl_order_detail.gross_sales
WHERE tbl_daily_marketing.channel=tbl_order_detail.channel_group;

#NET REVENUE
UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, channel_group, 
(SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat))/3.3 net_sales
FROM tbl_order_detail WHERE tbl_order_detail.oac=1 AND tbl_order_detail.returned=0  
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing.date 
SET tbl_daily_marketing.net_revenue=tbl_order_detail.net_sales
WHERE tbl_daily_marketing.channel=tbl_order_detail.channel_group;

#NET REVENUE EXPECTED

UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, 
reporting_channel, sum(net_rev_e)/3.3 net_rev_e
FROM daily_costs_visits_per_channel
GROUP BY date, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_daily_marketing.net_revenue_expected=daily_costs_visits_per_channel.net_rev_e;
UPDATE tbl_daily_marketing SET net_revenue_expected = net_revenue WHERE net_revenue_expected=0;

#MONTHLY TARGET

UPDATE tbl_daily_marketing  
INNER JOIN (SELECT date, channel_group, sum(target_net_sales) daily_target
FROM daily_targets_per_channel
GROUP BY date, channel_group) daily_targets_per_channel
ON daily_targets_per_channel.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=daily_targets_per_channel.channel_group
SET tbl_daily_marketing.daily_target=daily_targets_per_channel.daily_target;

#COST VS REV
UPDATE tbl_daily_marketing SET target_vs_rev=(net_revenue_expected/daily_target)-1;

#AVG TICKET
UPDATE tbl_daily_marketing SET avg_ticket=net_revenue/net_orders;

#MARKETING COST

UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, 
reporting_channel, sum(cost_local)/3.3 cost
FROM daily_costs_visits_per_channel
GROUP BY date, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_daily_marketing.marketing_cost=daily_costs_visits_per_channel.cost;

#COST/REV
UPDATE tbl_daily_marketing SET cost_rev=IF(marketing_cost/net_revenue_expected IS NULL, 0, marketing_cost/net_revenue_expected);

#CPO
UPDATE tbl_daily_marketing SET cost_per_order=IF(marketing_cost/net_orders IS NULL, 0, marketing_cost/net_orders);



#CPO TARGET

UPDATE tbl_daily_marketing  
INNER JOIN (SELECT date, channel_group, MAX(target_cpo) target_cpo
FROM daily_targets_per_channel
GROUP BY date, channel_group) daily_targets_per_channel
ON daily_targets_per_channel.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=daily_targets_per_channel.channel_group
SET tbl_daily_marketing.cost_per_order_target=daily_targets_per_channel.target_cpo;

#CPO VS TARGET
UPDATE tbl_daily_marketing SET cpo_vs_target_cpo=(cost_per_order/cost_per_order_target)-1;

#NEW CLIENTS
UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, channel_group, sum(new_customers) nc
FROM tbl_order_detail
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=tbl_order_detail.channel_group
SET tbl_daily_marketing.new_clients=tbl_order_detail.nc;

#NEW CLIENTS EXPECTED
UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, 
reporting_channel, sum(new_customers_e) nc_e
FROM daily_costs_visits_per_channel
GROUP BY date, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_daily_marketing.new_clientes_expected=daily_costs_visits_per_channel.nc_e;
UPDATE tbl_daily_marketing SET new_clientes_expected=new_clients WHERE new_clientes_expected=0;

#CAC
UPDATE tbl_daily_marketing SET client_acquisition_cost=marketing_cost/new_clientes_expected;

#PC2_COST

UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, channel_group, 
(SUM(costo_after_vat)+SUM(delivery_cost_supplier)+SUM(wh)+SUM(cs)+SUM(payment_cost)+SUM(shipping_cost))/3.3 pc2_costs
FROM tbl_order_detail WHERE tbl_order_detail.oac=1 AND tbl_order_detail.returned=0  
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing.date 
SET tbl_daily_marketing.pc2_costs=tbl_order_detail.pc2_costs
WHERE tbl_daily_marketing.channel=tbl_order_detail.channel_group;

#PC2
UPDATE tbl_daily_marketing SET profit_contribution_2= 1-(pc2_costs/net_revenue);

#MOM
select 'Month over month: ', now();
CALL mom_net_sales_per_channel();

UPDATE tbl_daily_marketing INNER JOIN tbl_mom_net_sales_per_channel 
ON tbl_daily_marketing.channel = tbl_mom_net_sales_per_channel.channel_group
AND tbl_daily_marketing.date = tbl_mom_net_sales_per_channel.date
SET tbl_daily_marketing.month_over_month = tbl_mom_net_sales_per_channel.month_over_month;

INSERT IGNORE INTO tbl_daily_marketing (date, channel) 
VALUES(date_add(now(), interval -1 day), 'MTD');

UPDATE tbl_daily_marketing INNER JOIN tbl_mom_net_sales_per_channel 
ON tbl_daily_marketing.channel = tbl_mom_net_sales_per_channel.channel_group
AND tbl_daily_marketing.date = tbl_mom_net_sales_per_channel.date
SET tbl_daily_marketing.month_over_month = tbl_mom_net_sales_per_channel.month_over_month
where channel_group = 'MTD';

#Adjusted revenue
select 'Adjusted revenue: ', now();
#Net
update tbl_daily_marketing m left join 
(
select t1.*, t2.totalAbout, t1.totalChannel/t2.totalAbout as proporcion, t3.netSales, t1.totalChannel/t2.totalAbout*t3.netSales adjusted_net_revenue
from
(select t.date, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by t.date desc,l.about_linio, t.channel_group) t1 inner join
(select t.date, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by t.date desc,l.about_linio) t2 on t1.date = t2.date and t1.about_linio = t2.about_linio
inner join
(select date, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0
group by date desc, t.about_linio) t3 on t3.date = t2.date and t3.about_linio = t2.about_linio) t 
on m.date = t.date and m.channel = t.channel_group 
set m.adjusted_net_revenue = ifnull(m.net_revenue + t.adjusted_net_revenue/3.3, m.net_revenue)
where m.channel <> 'Tele Sales';

update tbl_daily_marketing m left join 
(
select t1.*, t2.totalAbout, t1.totalChannel/t2.totalAbout as proporcion, t3.netSales, t1.totalChannel/t2.totalAbout*t3.netSales adjusted_net_revenue
from
(select t.date, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by t.date desc,l.about_linio, t.channel_group) t1 inner join
(select t.date, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by t.date desc,l.about_linio) t2 on t1.date = t2.date and t1.about_linio = t2.about_linio
inner join
(select date, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0
group by date desc, t.about_linio) t3 on t3.date = t2.date and t3.about_linio = t2.about_linio) t 
on m.date = t.date and m.channel = t.channel_group set m.adjusted_net_revenue = ifnull(t.adjusted_net_revenue/3.3,0)
where m.channel = 'Tele Sales';

#Gross
update tbl_daily_marketing m left join 
(
select t1.*, t2.totalAbout, t1.totalChannel/t2.totalAbout as proporcion, t3.netSales, t1.totalChannel/t2.totalAbout*t3.netSales adjusted_revenue
from
(select t.date, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where obc  = 1
group by t.date desc,l.about_linio, t.channel_group) t1 inner join
(select t.date, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where obc  = 1
group by t.date desc,l.about_linio) t2 on t1.date = t2.date and t1.about_linio = t2.about_linio
inner join
(select date, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and obc  = 1
group by date desc, t.about_linio) t3 on t3.date = t2.date and t3.about_linio = t2.about_linio) t 
on m.date = t.date and m.channel = t.channel_group 
set m.adjusted_gross_revenue = ifnull(m.gross_revenue + t.adjusted_revenue/3.3,m.gross_revenue)
where m.channel <> 'Tele Sales';

update tbl_daily_marketing m left join 
(
select t1.*, t2.totalAbout, t1.totalChannel/t2.totalAbout as proporcion, t3.netSales, t1.totalChannel/t2.totalAbout*t3.netSales adjusted_revenue
from
(select t.date, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where obc  = 1
group by t.date desc,l.about_linio, t.channel_group) t1 inner join
(select t.date, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where obc  = 1
group by t.date desc,l.about_linio) t2 on t1.date = t2.date and t1.about_linio = t2.about_linio
inner join
(select date, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and obc  = 1
group by date desc, t.about_linio) t3 on t3.date = t2.date and t3.about_linio = t2.about_linio) t 
on m.date = t.date and m.channel = t.channel_group set m.adjusted_gross_revenue = ifnull(t.adjusted_revenue/3.3,0)
where m.channel = 'Tele Sales';

#Pending
update tbl_daily_marketing m left join 
(
select t1.*, t2.totalAbout, t1.totalChannel/t2.totalAbout as proporcion, t3.netSales, t1.totalChannel/t2.totalAbout*t3.netSales adjusted_revenue
from
(select t.date, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where pending  = 1
group by t.date desc,l.about_linio, t.channel_group) t1 inner join
(select t.date, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where pending  = 1
group by t.date desc,l.about_linio) t2 on t1.date = t2.date and t1.about_linio = t2.about_linio
inner join
(select date, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and pending  = 1
group by date desc, t.about_linio) t3 on t3.date = t2.date and t3.about_linio = t2.about_linio) t 
on m.date = t.date and m.channel = t.channel_group 
set m.adjusted_pending_revenue = ifnull(m.pending_revenue + t.adjusted_revenue/3.3, m.pending_revenue)
where m.channel <> 'Tele Sales';

update tbl_daily_marketing m left join 
(
select t1.*, t2.totalAbout, t1.totalChannel/t2.totalAbout as proporcion, t3.netSales, t1.totalChannel/t2.totalAbout*t3.netSales adjusted_revenue
from
(select t.date, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where pending  = 1
group by t.date desc,l.about_linio, t.channel_group) t1 inner join
(select t.date, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where pending  = 1
group by t.date desc,l.about_linio) t2 on t1.date = t2.date and t1.about_linio = t2.about_linio
inner join
(select date, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and pending  = 1
group by date desc, t.about_linio) t3 on t3.date = t2.date and t3.about_linio = t2.about_linio) t 
on m.date = t.date and m.channel = t.channel_group set m.adjusted_pending_revenue = ifnull(t.adjusted_revenue/3.3,0)
where m.channel = 'Tele Sales';





END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `daily_report`()
begin

truncate production_pe.tbl_dailyReport;
insert into production_pe.tbl_dailyReport (date) select dt from production_pe.calendar where dt>'2012-07-31' and dt<now();

update production_pe.tbl_dailyReport
set year = year(date) , month = month(date) , yrmonth = concat(year,if(month<10,concat(0,month),month));

#gross
update production_pe.tbl_dailyReport join 
(select date,sum(unit_price) as grossrev, count(distinct order_nr,oac,returned,pending) as grossorders, 
	sum(unit_price_after_vat) as grosssalesprecancel
            from production_pe.tbl_order_detail where   (obc = '1')
            group by date) as tbl_gross on tbl_gross.date=tbl_dailyReport.date
set tbl_dailyReport.grossrev=tbl_gross.grossrev , tbl_dailyReport.grossorders=tbl_gross.grossorders, 
tbl_dailyReport.grosssalesprecancel=tbl_gross.grosssalesprecancel;

#cancel
update production_pe.tbl_dailyReport join 
(select date,sum(unit_price_after_vat) as cancelrev, count(distinct order_nr) as cancelorders
from production_pe.tbl_order_detail 
where   (cancel = '1')
group by date) as tbl_cancel on tbl_cancel.date=tbl_dailyReport.date
set tbl_dailyReport.cancellations=tbl_cancel.cancelrev , tbl_dailyReport.cancelorders=tbl_cancel.cancelorders;


#pending
update production_pe.tbl_dailyReport join 
(select date,sum(unit_price_after_vat) as pendingrev, count(distinct order_nr) as pendingorders
from production_pe.tbl_order_detail 
where   (pending = '1')
group by date) as tbl_pending on tbl_pending.date=tbl_dailyReport.date
set tbl_dailyReport.pendingcop=tbl_pending.pendingrev , tbl_dailyReport.pendingorders=tbl_pending.pendingorders;


#cogs_gross_post_cancel
update production_pe.tbl_dailyReport join 
(select date,(sum(costo_after_vat) + sum(delivery_cost_supplier)) as cogs_gross_post_cancel
from production_pe.tbl_order_detail 
where   (oac = '1')
group by date) as tbl_cogs_post_cancel on tbl_cogs_post_cancel.date=tbl_dailyReport.date
set tbl_dailyReport.cogs_gross_post_cancel=tbl_cogs_post_cancel.cogs_gross_post_cancel;

#pending
update production_pe.tbl_dailyReport join 
(select date,sum(unit_price_after_vat) as returnrev, count(distinct order_nr) as returnorders
from production_pe.tbl_order_detail 
where   (returned = '1')
group by date) as tbl_returned on tbl_returned.date=tbl_dailyReport.date
set tbl_dailyReport.returnedcop=tbl_returned.returnrev , tbl_dailyReport.returnedorders=tbl_returned.returnorders;

#net
update production_pe.tbl_dailyReport join 
(select date,sum(unit_price_after_vat) as netsalesprevoucher, sum(coupon_money_after_vat) as vouchermktcost,
sum(paid_price_after_vat) as netsales,count(distinct order_nr) as netorders,
 (sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
count(order_nr)/count(distinct order_nr) as averageitemsperbasket,
count(distinct custid) as customers,
sum(shipping_fee) as shippingfee, 
sum(shipping_cost)as shippingcost, 
sum(payment_cost) as paymentcost
from production_pe.tbl_order_detail 
where   (returned = '0') and (oac = '1')
group by date) as tbl_net on tbl_net.date=tbl_dailyReport.date
set tbl_dailyReport.netsalesprevoucher=tbl_net.netsalesprevoucher , tbl_dailyReport.vouchermktcost=tbl_net.vouchermktcost,
tbl_dailyReport.netsales=tbl_net.netsales,tbl_dailyReport.netorders=tbl_net.netorders,
tbl_dailyReport.cogs_on_net_sales=tbl_net.cogs_on_net_sales,tbl_dailyReport.averageitemsperbasket=tbl_net.averageitemsperbasket,
tbl_dailyReport.customers=tbl_net.customers,
tbl_dailyReport.shippingfee=tbl_net.shippingfee/1.18,
tbl_dailyReport.shippingcost=tbl_net.shippingcost,tbl_dailyReport.paymentcost=tbl_net.paymentcost;

 
#categorías
#electrodomésticos
update production_pe.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production_pe.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 = 'electrodomésticos')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_appiliances=tbl_cat.netsales , tbl_dailyReport.cogs_appiliances=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_appliances=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_appliances=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_appliances=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_appliances=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_appliances=tbl_cat.payment_fee;

#cámaras y fotografía
update production_pe.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production_pe.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 = 'cámaras y fotografía')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_photography=tbl_cat.netsales , tbl_dailyReport.cogs_photography=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_photography=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_photography=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_photography=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_photography=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_photography=tbl_cat.payment_fee;


#tv, video y audio
update production_pe.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production_pe.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 ='tv, audio y video' or n1 = 'altavoces')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_tv_audio_video=tbl_cat.netsales , tbl_dailyReport.cogs_tv_audio_video=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_tv_audio_video=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_tv_audio_video=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_tv_audio_video=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_tv_audio_video=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_tv_audio_video=tbl_cat.payment_fee;

#libros
update production_pe.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production_pe.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 = 'libros')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_books=tbl_cat.netsales , tbl_dailyReport.cogs_books=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_books=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_books=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_books=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_books=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_books=tbl_cat.payment_fee;

#videojuegos
update production_pe.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production_pe.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 = 'videojuegos')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_videogames=tbl_cat.netsales , tbl_dailyReport.cogs_videogames=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_videogames=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_videogames=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_videogames=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_videogames=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_videogames=tbl_cat.payment_fee;

#fashion
update production_pe.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production_pe.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 = 'ropa, calzado y accesorios')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_fashion=tbl_cat.netsales , tbl_dailyReport.cogs_fashion=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_fashion=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_fashion=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_fashion=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_fashion=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_fashion=tbl_cat.payment_fee;


#cuidado personal
update production_pe.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production_pe.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 like '%cuidado personal%' or n1 = 'rizadoras' or n1 = 'cabello' or n1 = 'secadores')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_health_beauty=tbl_cat.netsales , tbl_dailyReport.cogs_health_beauty=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_health_beauty=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_health_beauty=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_health_beauty=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_health_beauty=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_health_beauty=tbl_cat.payment_fee;


#teléfonos y gps
update production_pe.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production_pe.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 = 'celulares, telefonía y gps')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_cellphones=tbl_cat.netsales , tbl_dailyReport.cogs_cellphones=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_cellphones=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_cellphones=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_cellphones=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_cellphones=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_cellphones=tbl_cat.payment_fee;


 
#computadores y tablets    
update production_pe.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production_pe.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 = 'computadoras')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_computing=tbl_cat.netsales , tbl_dailyReport.cogs_computing=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_computing=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_computing=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_computing=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_computing=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_computing=tbl_cat.payment_fee;


#hogar y muebles   
update production_pe.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production_pe.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 = 'hogar' or n1 = 'línea blanca')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_hogar=tbl_cat.netsales , tbl_dailyReport.cogs_hogar=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_hogar=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_hogar=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_hogar=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_hogar=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_hogar=tbl_cat.payment_fee;


#juguetes, niños y bebés 
update production_pe.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 ='niños y bebés')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_kids_babies=tbl_cat.netsales , tbl_dailyReport.cogs_kids_babies=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_kids_babies=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_kids_babies=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_kids_babies=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_kids_babies=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_kids_babies=tbl_cat.payment_fee;

#deportes 
update production_pe.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 ='deportes')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_fitness=tbl_cat.netsales , tbl_dailyReport.cogs_fitness=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_fitness=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_fitness=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_fitness=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_fitness=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_fitness=tbl_cat.payment_fee;

#Oficina y Utiles 
update production_pe.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 ='oficina y útiles')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_office_supplies=tbl_cat.netsales , tbl_dailyReport.cogs_office_supplies=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_office_supplies=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_office_supplies=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_office_supplies=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_office_supplies=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_office_supplies=tbl_cat.payment_fee;

#paidchannels
 update production_pe.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as net_sales_paid_channel,
sum(coupon_money_after_vat) as voucher_paid_channel
from production_pe.tbl_order_detail 
where  (oac = '1') and (returned = '0') 
and   ((source_medium like '%price comparison%')
  or (source_medium like '%socialmediaads%')
  or (source_medium like '%cpc%')
  or (source_medium like '%sociomantic%'))
group by date) as tbl_paid on tbl_paid.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_paid_channel=tbl_paid.net_sales_paid_channel
 , tbl_dailyReport.voucher_paid_channel=tbl_paid.voucher_paid_channel;

# new customers
update production_pe.tbl_dailyReport join  
(select firstorder as date,count(*) as newcustomers from production_pe.view_cohort 
group by firstorder) as tbl_nc on tbl_nc.date=tbl_dailyReport.date
set tbl_dailyReport.newcustomers=tbl_nc.newcustomers;


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `daily_report_EUR`()
BEGIN

truncate tbl_dailyReport_EUR;
insert into tbl_dailyReport_EUR (date) select dt from calendar where dt>'2012-07-31' and dt<now();

update tbl_dailyReport_EUR
set year = year(date) , month = month(date) , yrmonth = concat(year,if(month<10,concat(0,month),month));

#Visits
UPDATE tbl_dailyReport_EUR t INNER JOIN tbl_dailyReport_EUR g 
ON t.date = g.date
SET t.Visits = g.visits;

#Visits
UPDATE tbl_dailyReport_EUR t INNER JOIN tbl_dailyReport_EUR g 
ON t.date = g.date
SET t.UniqueVisits = g.UniqueVisits;

#Mobile Visits
UPDATE tbl_dailyReport_EUR t INNER JOIN tbl_dailyReport_EUR g 
ON t.date = g.date
SET t.MobileVisits = if(g.MobileVisits is null, 0, g.MobileVisits);

#EUR
update tbl_dailyReport_EUR e join 
tbl_dailyReport_local l on e.date = l.date
join tbl_exchange_rate 
on tbl_exchange_rate.yrmonth = cast(concat(year(e.date),if(month(e.date)<10, concat(0,month(e.date)),month(e.date))) as unsigned )
set e.GrossRev = l.GrossRev/rate,
e.GrossSalesPreCancel = l.GrossSalesPreCancel/rate,
e.Cancellations = l.Cancellations/rate,
e.Pending = l.PendingCOP/rate,
e.cogs_gross_post_cancel = l.cogs_gross_post_cancel/rate,
e.Returned = l.ReturnedCop/rate,
e.NetSales = l.NetSales/rate,
e.NetSalesPreVoucher = l.NetSalesPreVoucher/rate,
e.VoucherMktCost = l.VoucherMktCost/rate,
e.cogs_on_net_sales = l.cogs_on_net_sales/rate,
e.marketing_spend = l.marketing_spend/rate,
e.net_sales_paid_channel = l.net_sales_paid_channel/rate,
e.voucher_paid_channel = l.voucher_paid_channel/rate,
e.ShippingFee = l.ShippingFee/rate,
e.ShippingCost = l.ShippingCost/rate,
e.ShippingCostPerOrder = l.ShippingCostPerOrder/rate,
e.PaymentCost = l.PaymentCost/rate,
e.PaymentCostPerOrder = l.PaymentCostPerOrder/rate,
e.WHCostPerOrder = l.WHCostPerOrder/rate,
e.CSCostPerOrder = l.CSCostPerOrder,
e.net_sales_Appliances = l.net_sales_Appliances/rate,
e.costs_Appliances = l.costs_Appliances/rate,
e.inboundCosts_Appliances = l.inboundCosts_Appliances/rate,
e.cogs_Appliances = l.cogs_Appliances/rate,
e.ShippingFee_Appliances = l.ShippingFee_Appliances/rate,
e.ShippingCost_Appliances = l.ShippingCost_Appliances/rate,
e.WH_Cost_Appliances = l.WH_Cost_Appliances/rate,
e.CS_Cost_Appliances = l.CS_Cost_Appliances/rate,
e.Payment_Fees_Appliances = l.Payment_Fees_Appliances/rate,
e.net_sales_Photography = l.net_sales_Photography/rate,
e.costs_Photography = l.costs_Photography/rate,
e.inboundCosts_Photography = l.inboundCosts_Photography/rate,
e.cogs_Photography = l.cogs_Photography/rate,
e.ShippingFee_Photography = l.ShippingFee_Photography/rate,
e.ShippingCost_Photography = l.ShippingCost_Photography/rate,
e.WH_Cost_Photography = l.WH_Cost_Photography/rate,
e.CS_Cost_Photography = l.CS_Cost_Photography/rate,
e.Payment_Fees_Photography = l.Payment_Fees_Photography/rate,
e.net_sales_TV_Audio_Video = l.net_sales_TV_Audio_Video/rate,
e.costs_TV_Audio_Video = l.costs_TV_Audio_Video/rate,
e.inboundCosts_TV_Audio_Video = l.inboundCosts_TV_Audio_Video/rate,
e.cogs_TV_Audio_Video = l.cogs_TV_Audio_Video/rate,
e.ShippingFee_TV_Audio_Video = l.ShippingFee_TV_Audio_Video/rate,
e.ShippingCost_TV_Audio_Video = l.ShippingCost_TV_Audio_Video/rate,
e.WH_Cost_TV_Audio_Video = l.WH_Cost_TV_Audio_Video/rate,
e.CS_Cost_TV_Audio_Video = l.CS_Cost_TV_Audio_Video/rate,
e.Payment_Fees_TV_Audio_Video = l.Payment_Fees_TV_Audio_Video/rate,
e.net_sales_Books = l.net_sales_Books/rate,
e.costs_Books = l.costs_Books/rate,
e.inboundCosts_Books = l.inboundCosts_Books/rate,
e.cogs_Books = l.cogs_Books/rate,
e.ShippingFee_Books = l.ShippingFee_Books/rate,
e.ShippingCost_Books = l.ShippingCost_Books/rate,
e.WH_Cost_Books = l.WH_Cost_Books/rate,
e.CS_Cost_Books = l.CS_Cost_Books/rate,
e.Payment_Fees_Books = l.Payment_Fees_Books/rate,
e.net_sales_Videogames = l.net_sales_Videogames/rate,
e.costs_Videogames = l.costs_Videogames/rate,
e.inboundCosts_Videogames = l.inboundCosts_Videogames/rate,
e.cogs_Videogames = l.cogs_Videogames/rate,
e.ShippingFee_Videogames = l.ShippingFee_Videogames/rate,
e.ShippingCost_Videogames = l.ShippingCost_Videogames/rate,
e.WH_Cost_Videogames = l.WH_Cost_Videogames/rate,
e.CS_Cost_Videogames = l.CS_Cost_Videogames/rate,
e.Payment_Fees_Videogames = l.Payment_Fees_Videogames/rate,
e.net_sales_Fashion = l.net_sales_Fashion/rate,
e.costs_Fashion = l.costs_Fashion/rate,
e.inboundCosts_Fashion = l.inboundCosts_Fashion/rate,
e.cogs_Fashion = l.cogs_Fashion/rate,
e.ShippingFee_Fashion = l.ShippingFee_Fashion/rate,
e.ShippingCost_Fashion = l.ShippingCost_Fashion/rate,
e.WH_Cost_Fashion = l.WH_Cost_Fashion/rate,
e.CS_Cost_Fashion = l.CS_Cost_Fashion/rate,
e.Payment_Fees_Fashion = l.Payment_Fees_Fashion/rate,
e.net_sales_Health_Beauty = l.net_sales_Health_Beauty/rate,
e.costs_Health_Beauty = l.costs_Health_Beauty/rate,
e.inboundCosts_Health_Beauty = l.inboundCosts_Health_Beauty/rate,
e.cogs_Health_Beauty = l.cogs_Health_Beauty/rate,
e.ShippingFee_Health_Beauty = l.ShippingFee_Health_Beauty/rate,
e.ShippingCost_Health_Beauty = l.ShippingCost_Health_Beauty/rate,
e.WH_Cost_Health_Beauty = l.WH_Cost_Health_Beauty/rate,
e.CS_Cost_Health_Beauty = l.CS_Cost_Health_Beauty/rate,
e.Payment_Fees_Health_Beauty = l.Payment_Fees_Health_Beauty/rate,
e.net_sales_Cellphones = l.net_sales_Cellphones/rate,
e.costs_Cellphones = l.costs_Cellphones/rate,
e.inboundCosts_Cellphones = l.inboundCosts_Cellphones/rate,
e.cogs_Cellphones = l.cogs_Cellphones/rate,
e.ShippingFee_Cellphones = l.ShippingFee_Cellphones/rate,
e.ShippingCost_Cellphones = l.ShippingCost_Cellphones/rate,
e.WH_Cost_Cellphones = l.WH_Cost_Cellphones/rate,
e.CS_Cost_Cellphones = l.CS_Cost_Cellphones/rate,
e.Payment_Fees_Cellphones = l.Payment_Fees_Cellphones/rate,
e.net_sales_Home = l.net_sales_Home/rate,
e.costs_Home = l.costs_Home/rate,
e.inboundCosts_Home = l.inboundCosts_Home/rate,
e.cogs_Home = l.cogs_Home/rate,
e.ShippingFee_Home = l.ShippingFee_Home/rate,
e.ShippingCost_Home = l.ShippingCost_Home/rate,
e.WH_Cost_Home = l.WH_Cost_Home/rate,
e.CS_Cost_Home = l.CS_Cost_Home/rate,
e.Payment_Fees_Home = l.Payment_Fees_Home/rate,
e.net_sales_Kids_Babies = l.net_sales_Kids_Babies/rate,
e.costs_Kids_Babies = l.costs_Kids_Babies/rate,
e.inboundCosts_Kids_Babies = l.inboundCosts_Kids_Babies/rate,
e.cogs_Kids_Babies = l.cogs_Kids_Babies/rate,
e.ShippingFee_Kids_Babies = l.ShippingFee_Kids_Babies/rate,
e.ShippingCost_Kids_Babies = l.ShippingCost_Kids_Babies/rate,
e.WH_Cost_Kids_Babies = l.WH_Cost_Kids_Babies/rate,
e.CS_Cost_Kids_Babies = l.CS_Cost_Kids_Babies/rate,
e.Payment_Fees_Kids_Babies = l.Payment_Fees_Kids_Babies/rate,
e.net_sales_Sports = l.net_sales_Sports/rate,
e.costs_Sports = l.costs_Sports/rate,
e.inboundCosts_Sports = l.inboundCosts_Sports/rate,
e.cogs_Sports = l.cogs_Sports/rate,
e.ShippingFee_Sports = l.ShippingFee_Sports/rate,
e.ShippingCost_Sports = l.ShippingCost_Sports/rate,
e.WH_Cost_Sports = l.WH_Cost_Sports/rate,
e.CS_Cost_Sports = l.CS_Cost_Sports/rate,
e.Payment_Fees_Sports = l.Payment_Fees_Sports/rate,
e.GrossRev_Appliances = l.GrossRev_Appliances/rate,
e.GrossRev_Photography = l.GrossRev_Photography/rate,
e.GrossRev_TV_Audio_Video = l.GrossRev_TV_Audio_Video/rate,
e.GrossRev_Books = l.GrossRev_Books/rate,
e.GrossRev_Videogames = l.GrossRev_Videogames/rate,
e.GrossRev_Fashion = l.GrossRev_Fashion/rate,
e.GrossRev_Health_Beauty = l.GrossRev_Health_Beauty/rate,
e.GrossRev_Cellphones = l.GrossRev_Cellphones/rate,
e.GrossRev_Computing = l.GrossRev_Computing/rate,
e.GrossRev_Home = l.GrossRev_Home/rate,
e.GrossRev_Kids_Babies = l.GrossRev_Kids_Babies/rate,
e.GrossRev_Sports = l.GrossRev_Sports/rate;

update tbl_dailyReport_EUR e join 
tbl_dailyReport_local l on e.date = l.date
set e.Visits = l.Visits,
e.UniqueVisits = l.UniqueVisits,
e.MobileVisits = l.MobileVisits,
e.Visits = l.Visits,
e.GrossOrders = l.GrossOrders,
e.CancelOrders = l.CancelOrders,
e.PendingOrders = l.PendingOrders,
e.ReturnedOrders = l.ReturnedOrders,
e.NetOrders = l.NetOrders,
e.PromisedDeliveries = l.PromisedDeliveries,
e.DeliveriesOnTime = l.DeliveriesOnTime,
e.AverageItemsPerBasket = l.AverageItemsPerBasket,
e.WHIdleCapacity = l.WHIdleCapacity,
e.newCustomers = l.newCustomers,
e.itemsInStock = l.itemsInStock,
e.brandsInStock = l.brandsInStock;




END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `daily_report_local`()
BEGIN

truncate tbl_dailyReport_local;
insert into tbl_dailyReport_local (date) select dt from calendar where dt>'2012-07-31' and dt<now();

update tbl_dailyReport_local
set year = year(date) , month = month(date) , yrmonth = concat(year,if(month<10,concat(0,month),month));

#Visits
UPDATE tbl_dailyReport_local t INNER JOIN ga_daily_visits g 
ON t.date = g.date
SET t.Visits = g.visits;

#Visits
UPDATE tbl_dailyReport_local t INNER JOIN ga_daily_visits g 
ON t.date = g.date
SET t.UniqueVisits = g.unique_visits;

#Mobile Visits
UPDATE tbl_dailyReport_local t INNER JOIN ga_daily_visits g 
ON t.date = g.date
SET t.MobileVisits = if(g.mobile_visits is null, 0, g.mobile_visits);

#Gross
update tbl_dailyReport_local join 
(select date,sum(`unit_price`) as grossrev, count(distinct `order_nr`,`OAC`,`RETURNED`,`PENDING`) as grossorders, 
	sum(`unit_price_after_vat`) as GrossSalesPreCancel
            from `tbl_order_detail` where   (`OBC` = '1')
            group by `date`) AS tbl_gross on tbl_gross.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.GrossRev=tbl_gross.grossrev , tbl_dailyReport_local.GrossOrders=tbl_gross.grossorders, 
tbl_dailyReport_local.GrossSalesPreCancel=tbl_gross.GrossSalesPreCancel;

#Cancel
update tbl_dailyReport_local join 
(select date,sum(`unit_price_after_vat`) as cancelrev, count(distinct `order_nr`) as cancelorders
from `tbl_order_detail` 
where   (`CANCEL` = '1')
group by `date`) AS tbl_cancel on tbl_cancel.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.Cancellations=tbl_cancel.cancelrev , tbl_dailyReport_local.CancelOrders=tbl_cancel.cancelorders;


#Pending
update tbl_dailyReport_local join 
(select date,sum(if(`unit_price_after_vat` is null, 0, unit_price_after_vat))  as pendingrev, if( count(distinct `order_nr`) is null, 0,  count(distinct `order_nr`)) as pendingorders
from `tbl_order_detail` 
where   (`PENDING` = '1')
group by `date`) AS tbl_pending on tbl_pending.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.PendingCOP=tbl_pending.pendingrev , tbl_dailyReport_local.PendingOrders=tbl_pending.pendingorders;

#cogs_gross_post_cancel
update tbl_dailyReport_local join 
(select date,(sum(`costo_after_vat`) + sum(`delivery_cost_supplier`)) as cogs_gross_post_cancel
from `tbl_order_detail` 
where   (`OAC` = '1')
group by `date`) AS tbl_cogs_post_cancel on tbl_cogs_post_cancel.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.cogs_gross_post_cancel=tbl_cogs_post_cancel.cogs_gross_post_cancel;

#Returned
update tbl_dailyReport_local join 
(select date,sum(`unit_price_after_vat`) as returnrev, count(distinct `order_nr`) as returnorders
from `tbl_order_detail` 
where   (`RETURNED` = '1')
group by `date`) AS tbl_returned on tbl_returned.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.ReturnedCOP=tbl_returned.returnrev , tbl_dailyReport_local.ReturnedOrders=tbl_returned.returnorders;

#Net
update tbl_dailyReport_local join 
(select date,sum(`unit_price_after_vat`) as NetSalesPreVoucher, SUM(coupon_money_after_vat) as VoucherMktCost,
SUM(`paid_price_after_vat`) as NetSales,count(distinct `order_nr`) as NetOrders,
 (sum(`costo_after_vat`) + sum(`delivery_cost_supplier`))as cogs_on_net_sales,
count(`order_nr`)/count(distinct `order_nr`) as AverageItemsPerBasket,
count(distinct `CustID`) as customers,
SUM(`shipping_fee`) as ShippingFee, 
SUM(`shipping_cost`)as ShippingCost, 
SUM(`payment_cost`) as PaymentCost
from `tbl_order_detail` 
where   (`RETURNED` = '0') AND (`OAC` = '1')
group by `date`) AS tbl_net on tbl_net.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.NetSalesPreVoucher=tbl_net.NetSalesPreVoucher ,
tbl_dailyReport_local.VoucherMktCost=tbl_net.VoucherMktCost,
tbl_dailyReport_local.NetSales=tbl_net.NetSales,
tbl_dailyReport_local.NetOrders=tbl_net.NetOrders,
tbl_dailyReport_local.cogs_on_net_sales=tbl_net.cogs_on_net_sales,
tbl_dailyReport_local.AverageItemsPerBasket=tbl_net.AverageItemsPerBasket,
tbl_dailyReport_local.customers=tbl_net.customers,
tbl_dailyReport_local.ShippingFee=tbl_net.ShippingFee/1.18,
tbl_dailyReport_local.ShippingCost=tbl_net.ShippingCost,
tbl_dailyReport_local.PaymentCost=tbl_net.PaymentCost;

#Marketing spend
UPDATE tbl_dailyReport_local t
INNER JOIN (select date, sum(adCost) adCost from ga_cost_campaign group by date) g 
ON t.date = g.date SET t.marketing_spend = adCost;

# New Customers
update tbl_dailyReport_local join  
(select firstOrder as date,COUNT(*) as newCustomers from `view_cohort` 
group by firstOrder) as tbl_nc on tbl_nc.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.newcustomers=tbl_nc.newCustomers,
tbl_dailyReport_local.ShippingCostPerOrder=tbl_dailyReport_local.ShippingCost/tbl_nc.newCustomers,
tbl_dailyReport_local.PaymentCostPerOrder=tbl_dailyReport_local.PaymentCost/tbl_nc.newCustomers;

#PaidChannels
 update production_pe.tbl_dailyReport_local join 
(select date,sum(paid_price_after_vat) as net_sales_paid_channel,
sum(coupon_money_after_vat) as voucher_paid_channel
from production_pe.tbl_order_detail 
where  (oac = '1') and (returned = '0') 
and   ((source_medium like '%price comparison%')
  or (source_medium like '%socialmediaads%')
  or (source_medium like '%cpc%')
  or (source_medium like '%sociomantic%'))
group by date) as tbl_paid on tbl_paid.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_paid_channel=tbl_paid.net_sales_paid_channel
 , tbl_dailyReport_local.voucher_paid_channel=tbl_paid.voucher_paid_channel;

 
#Categorías
#Electrodomésticos
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` = 'electrodomésticos')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Appliances=tbl_cat.netsales , tbl_dailyReport_local.cogs_Appliances=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Appliances=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Appliances=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Appliances=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Appliances=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Appliances=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Appliances = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Appliances = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Appliances= tbl_cat.grossRev;

#Cámaras y Fotografía
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` = 'cámaras y fotografía')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Photography=tbl_cat.netsales , tbl_dailyReport_local.cogs_Photography=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Photography=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Photography=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Photography=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Photography=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Photography=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Photography = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Photography = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Photography= tbl_cat.grossRev ;


#TV, Video y Audio
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 ='tv, audio y video' or n1 = 'altavoces')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_TV_Audio_Video=tbl_cat.netsales , tbl_dailyReport_local.cogs_TV_Audio_Video=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_TV_Audio_Video=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_TV_Audio_Video=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_TV_Audio_Video=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_TV_Audio_Video=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_TV_Audio_Video=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_TV_Audio_Video = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_TV_Audio_Video = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_TV_Audio_Video= tbl_cat.grossRev ;

#Libros
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 = 'libros')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Books=tbl_cat.netsales , tbl_dailyReport_local.cogs_Books=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Books=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Books=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Books=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Books=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Books=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Books = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Books = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Books= tbl_cat.grossRev  ;
#Videojuegos
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` = 'videojuegos')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Videogames=tbl_cat.netsales , tbl_dailyReport_local.cogs_Videogames=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Videogames=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Videogames=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Videogames=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Videogames=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Videogames=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Videogames = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Videogames = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Videogames= tbl_cat.grossRev ;

#Fashion
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 = 'ropa, calzado y accesorios')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Fashion=tbl_cat.netsales , tbl_dailyReport_local.cogs_Fashion=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Fashion=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Fashion=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Fashion=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Fashion=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Fashion=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Fashion = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Fashion = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Fashion= tbl_cat.grossRev  ;


#Cuidado Personal
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 like '%cuidado personal%' or n1 = 'rizadoras' or n1 = 'cabello' or n1 = 'secadores')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Health_Beauty=tbl_cat.netsales , tbl_dailyReport_local.cogs_Health_Beauty=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Health_Beauty=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Health_Beauty=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Health_Beauty=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Health_Beauty=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Health_Beauty=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Health_Beauty = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Health_Beauty = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Health_Beauty= tbl_cat.grossRev  ;


#Teléfonos y GPS
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND( n1 = 'celulares, telefonía y gps')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Cellphones=tbl_cat.netsales , tbl_dailyReport_local.cogs_Cellphones=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Cellphones=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Cellphones=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Cellphones=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Cellphones=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Cellphones=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Cellphones = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Cellphones = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Cellphones= tbl_cat.grossRev   ;


 
#Computadores y Tablets    
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 = 'computadoras')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Computing=tbl_cat.netsales , tbl_dailyReport_local.cogs_Computing=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Computing=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Computing=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Computing=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Computing=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Computing=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Computing = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Computing = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Computing = tbl_cat.grossRev  ;


#Home y Muebles   
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 = 'hogar' or n1 = 'línea blanca')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Home=tbl_cat.netsales , 
tbl_dailyReport_local.cogs_Home=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Home=tbl_cat.shipping_fee,
 tbl_dailyReport_local.ShippingCost_Home=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Home=tbl_cat.WH_cost, 
tbl_dailyReport_local.CS_Cost_Home=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Home=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Home = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Home = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Home = tbl_cat.grossRev ;


#Juguetes, Niños y Bebés 
 update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 ='niños y bebés')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Kids_Babies=tbl_cat.netsales , tbl_dailyReport_local.cogs_Kids_Babies=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Kids_Babies=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Kids_Babies=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Kids_Babies=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Kids_Babies=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Kids_Babies=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Kids_Babies = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Kids_Babies = tbl_cat.inboundCosts , 
tbl_dailyReport_local.GrossRev_Kids_Babies = tbl_cat.grossRev ;

#Deportes
 update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.18) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` ='Deportes')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Sports=tbl_cat.netsales , 
tbl_dailyReport_local.cogs_Sports=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Sports=tbl_cat.shipping_fee, 
tbl_dailyReport_local.ShippingCost_Sports=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Sports=tbl_cat.WH_cost, 
tbl_dailyReport_local.CS_Cost_Sports=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Sports=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Sports = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Sports = tbl_cat.inboundCosts , 
tbl_dailyReport_local.GrossRev_Sports = tbl_cat.grossRev ;



END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `daily_routine`()
begin
select  'daily routine: start',now();

select  'daily routine: tbl_catalog_product start',now();
#table catalog_product_v2
drop table if exists production_pe.simple_variation;
create table production_pe.simple_variation as (select * from production_pe.simple_variation_view);
alter table production_pe.simple_variation add primary key (fk_catalog_simple) ;

#tbl_catalog_stock
truncate production_pe.tbl_catalog_product_stock;
insert into production_pe.tbl_catalog_product_stock(fk_catalog_simple,sku,reservedbob,stockbob,availablebob)
select catalog_simple.id_catalog_simple,catalog_simple.sku,
if(sum(is_reserved) is null, 0, sum(is_reserved))  as reservedbob ,catalog_stock.quantity as availablebob,
catalog_stock.quantity-if(sum(is_reserved) is null, 0, sum(is_reserved)) as disponible
from 
(bob_live_pe.catalog_simple join bob_live_pe.catalog_stock on catalog_simple.id_catalog_simple=catalog_stock.fk_catalog_simple) left join bob_live_pe.sales_order_item
 on sales_order_item.sku=catalog_simple.sku
group by catalog_simple.sku
order by catalog_stock.quantity desc;
drop table if exists tbl_catalog_product_v3;
create table production_pe.tbl_catalog_product_v3 as (select * from production_pe.catalog_product);
update production_pe.tbl_catalog_product_v3  set visible='no' where cat1 is null;

#actualiza lo recientamente reservado, desde el 01 de enero
update production_pe.tbl_catalog_product_stock inner join (select tbl_catalog_product_stock.fk_catalog_simple,
if(sum(is_reserved) is null, 0, sum(is_reserved))  as recently_reserved
from 
production_pe.tbl_catalog_product_stock inner join bob_live_pe.sales_order_item on tbl_catalog_product_stock.sku=sales_order_item.sku
where date(sales_order_item.created_at)>'2013-01-01' group by fk_catalog_simple) as a 
on  tbl_catalog_product_stock.fk_catalog_simple = a.fk_catalog_simple
set tbl_catalog_product_stock.recently_reservedbob = a.recently_reserved;


alter table production_pe.tbl_catalog_product_v3 add primary key (sku) ;

drop table if exists tbl_catalog_product_v2;
alter table production_pe.tbl_catalog_product_v3 rename to  production_pe.tbl_catalog_product_v2 ;

alter table production_pe.tbl_catalog_product_v2 
add index (sku_config ) ;

select  'daily routine: tbl_catalog_product ok',now();

#special cost
#call special_cost();
select  'daily routine: tbl_order_detail start',now();

call production_pe.update_tbl_order_detail();

select  'daily routine: tbl_order_detail end',now();


#ventas netas mes corrido
/*insert into production_pe.tbl_sales_growth_mom_cat
select date(now() - interval 1 day) date, tab.n1, ventas_netas_t, ventas_netas_t_1, 
cast(((ventas_netas_t/ventas_netas_t_1)-1) as decimal(30,10)) mom from (
select n1,sum(paid_price_after_vat) + sum(if(shipping_fee_after_vat is null, shipping_fee_after_vat,0)) ventas_netas_t
 from production_pe.tbl_order_detail where date>= date(now() - interval day(now()) day) and date <= now() 
 and month(date) = month(now() - interval 1 day) and oac = 1 and returned = 0 group by n1) tab inner join 
(select n1,sum(paid_price_after_vat) + sum(if(shipping_fee_after_vat is null, shipping_fee_after_vat,0)) ventas_netas_t_1
 from production_pe.tbl_order_detail where date<= date(now() - interval 1 month)
 and date >= date(now() - interval 1 month - interval day(now()) day ) 
and month(date) = month(now() - interval 1 day) - 1
 and oac = 1 and returned = 0 group by n1) tab2
on tab.n1 = tab2.n1;

insert into production_pe.tbl_sales_growth_mom_buyer
select date(now() - interval 1 day) date, tab.buyer, ventas_netas_t, ventas_netas_t_1, 
cast(((ventas_netas_t/ventas_netas_t_1)-1) as decimal(30,10)) mom from (
select buyer,sum(paid_price_after_vat) + sum(if(shipping_fee_after_vat is null, shipping_fee_after_vat,0)) ventas_netas_t
 from production_pe.tbl_order_detail where date>= date(now() - interval day(now()) day) and date <= now() 
 and month(date) = month(now() - interval 1 day) and oac = 1 and returned = 0 group by buyer) tab inner join 
(select buyer,sum(paid_price_after_vat) + sum(if(shipping_fee_after_vat is null, shipping_fee_after_vat,0)) ventas_netas_t_1
 from production_pe.tbl_order_detail where date<= date(now() - interval 1 month)
 and date >= date(now() - interval 1 month - interval day(now()) day ) 
and month(date) =  month(now() - interval 1 day) - 1
 and oac = 1 and returned = 0 group by buyer) tab2
on tab.buyer = tab2.buyer;*/

select  'bireporting: start',now();

#sales cube
-- call bireporting.etl_fact_sales_comercial();

select  'bireporting: ok',now();


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `extra_queries`()
BEGIN

#Status
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005966' AND c.sku = 'AL183BO98HVXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002196' AND c.sku = 'ED164BO25PUOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009166' AND c.sku = 'OS049EL19EGUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006396' AND c.sku = 'OS049EL18EGVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009296' AND c.sku = 'SO029EL96GNJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005296' AND c.sku = 'BA440TB34QNLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002996' AND c.sku = 'TA452EL71QPWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009996' AND c.sku = 'NI064EL51MFGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009996' AND c.sku = 'NI064EL50MFHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009996' AND c.sku = 'NI064EL55MFCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004596' AND c.sku = 'KI042EL56GDJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004596' AND c.sku = 'KI042EL56GDJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009166' AND c.sku = 'LG018EL78GGJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000232' AND c.sku = 'PR022EL80EPZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000352' AND c.sku = 'SO029EL04GNBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000412' AND c.sku = 'SA026EL76EILPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000212' AND c.sku = 'SA026EL76EILPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000612' AND c.sku = 'CA035EL52GDNPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000812' AND c.sku = 'NI047EL33EZMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000712' AND c.sku = 'FU038EL19GAYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000512' AND c.sku = 'CA035EL70GCVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000232' AND c.sku = 'NI047EL33EZMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000752' AND c.sku = 'SO029EL78GKFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000632' AND c.sku = 'NI047EL33EZMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007497' AND c.sku = 'AV162TB75HDOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007497' AND c.sku = 'AV162TB40HEXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007497' AND c.sku = 'AV162TB23KXSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002697' AND c.sku = 'WA453EL66QQBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001697' AND c.sku = 'HU445EL61ZFIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003167' AND c.sku = 'LG018EL91QLGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003178' AND c.sku = 'OS049EL18EGVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003958' AND c.sku = 'BL112EL05KMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001978' AND c.sku = 'PR022EL37BHEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003978' AND c.sku = 'PR022EL37BHEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004878' AND c.sku = 'PR022EL37BHEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002878' AND c.sku = 'PR022EL37BHEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006878' AND c.sku = 'PR022EL37BHEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004778' AND c.sku = 'PH121EL34BIFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008978' AND c.sku = 'PR022EL37BHEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003178' AND c.sku = 'IM013EL82OCBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001678' AND c.sku = 'GE116EL34EGFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001378' AND c.sku = 'LE060EL83XGMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007458' AND c.sku = 'BL112EL05KMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003458' AND c.sku = 'BL112EL05KMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007258' AND c.sku = 'DA007EL27QVKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006658' AND c.sku = 'AI031EL52LPVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005658' AND c.sku = 'SA026EL33WLIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002958' AND c.sku = 'BL112EL05KMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009447' AND c.sku = 'EL070EL91QPCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007778' AND c.sku = 'PH121EL34BIFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007478' AND c.sku = 'GE116EL17EGWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003776' AND c.sku = 'HO176TB84MXFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002688' AND c.sku = 'SA026EL39ZGEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002688' AND c.sku = 'SA026EL39ZGEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008988' AND c.sku = 'FI173TB78MXLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004788' AND c.sku = 'SA026EL35WLGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008788' AND c.sku = 'GA010EL88KRJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006588' AND c.sku = 'SO029EL04PZFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005978' AND c.sku = 'PH121EL36BIDPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003388' AND c.sku = 'LG018EL63AGEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004858' AND c.sku = 'TO066EL21LRAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006278' AND c.sku = 'GE116EL17EGWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005278' AND c.sku = 'GE116EL17EGWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001278' AND c.sku = 'GE116EL17EGWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003278' AND c.sku = 'GE116EL17EGWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004678' AND c.sku = 'LO103EL51DUAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002678' AND c.sku = 'LO103EL51DUAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006678' AND c.sku = 'LO103EL51DUAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007678' AND c.sku = 'KI042EL39LMMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004388' AND c.sku = 'AR444EL63QUAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004538' AND c.sku = 'LE060EL08BFJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009958' AND c.sku = 'SA026EL33WLIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003638' AND c.sku = 'NO125EL51ZFSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002938' AND c.sku = 'KI042EL55GDKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009838' AND c.sku = 'LE060EL08BFJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004738' AND c.sku = 'LG018EL63AGEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002738' AND c.sku = 'LG018EL63AGEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006738' AND c.sku = 'LG018EL63AGEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001118' AND c.sku = 'SA026EL39ZGEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005738' AND c.sku = 'NE553FA57VJMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001518' AND c.sku = 'BL112EL04KMXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002538' AND c.sku = 'EL070EL91QPCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001538' AND c.sku = 'MO115EL93ITEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003538' AND c.sku = 'AC446EL01QOSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005138' AND c.sku = 'TI536FA23TUMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005138' AND c.sku = 'TI536FA23TUMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001138' AND c.sku = 'TI536FA23TUMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001138' AND c.sku = 'TI536FA23TUMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003987' AND c.sku = 'KO448EL56QQLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009738' AND c.sku = 'BL112EL05KMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009918' AND c.sku = 'SA026EL38BIBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006858' AND c.sku = 'SA026EL19TQUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003858' AND c.sku = 'LE060EL83XGMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009758' AND c.sku = 'LE060EL83XGMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005758' AND c.sku = 'LE060EL08BJFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005558' AND c.sku = 'SA026EL33BIGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006158' AND c.sku = 'SA026EL33BIGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005158' AND c.sku = 'AR444EL63QUAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008318' AND c.sku = 'GA148TB20GQHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009218' AND c.sku = 'RA520TB63TDOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008338' AND c.sku = 'SA026EL34WLHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008918' AND c.sku = 'PH121EL34BIFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004818' AND c.sku = 'SA026EL21RLAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004718' AND c.sku = 'DA007EL27QVKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002718' AND c.sku = 'DA007EL27QVKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008718' AND c.sku = 'NO125EL43ZCEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003718' AND c.sku = 'WE266EL98LKFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003718' AND c.sku = 'KI042EL38LMNPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007518' AND c.sku = 'BL112EL04KMXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005418' AND c.sku = 'LE060EL83XGMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009525' AND c.sku = 'SA026EL44QQXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006965' AND c.sku = 'AN242EL16KMLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009625' AND c.sku = 'IM013EL68OCPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008625' AND c.sku = 'OS049EL02EHLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001625' AND c.sku = 'AP032EL74QEFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003825' AND c.sku = 'SA026EL35WLGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006725' AND c.sku = 'DA007EL21QVQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007725' AND c.sku = 'CA447EL95QOYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000632' AND c.sku = 'PR022EL80EPZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000746' AND c.sku = 'SA026EL85GRQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000746' AND c.sku = 'SA026EL85GRQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000746' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000426' AND c.sku = 'SA026EL63GGYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000626' AND c.sku = 'AO002ME72FRFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000112' AND c.sku = 'NI047EL33EZMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000782' AND c.sku = 'TO144BO83FYMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000892' AND c.sku = 'AV162TB05HCKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000792' AND c.sku = 'TO144BO83FYMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000592' AND c.sku = 'AV162TB81HDIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000192' AND c.sku = 'AO002ME72FRFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000392' AND c.sku = 'AO002ME72FRFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000482' AND c.sku = 'AO002ME72FRFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000282' AND c.sku = 'BR145BO07HGEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000152' AND c.sku = 'SO029EL04GNBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000982' AND c.sku = 'PL158BO25GXUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000552' AND c.sku = 'SO029EL78GKFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000382' AND c.sku = 'PL158BO64HADPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000382' AND c.sku = 'PL158BO64HADPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000272' AND c.sku = 'AO002ME74FRDPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000772' AND c.sku = 'SO029EL04GNBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000372' AND c.sku = 'HE058EL77GCOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000452' AND c.sku = 'HE058EL77GCOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000852' AND c.sku = 'SA026EL96GRFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000876' AND c.sku = 'NI047EL31EZOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000682' AND c.sku = 'BR145BO07HGEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000136' AND c.sku = 'WU146BO25GIKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000926' AND c.sku = 'FU038EL19GAYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000516' AND c.sku = 'RO120EL61EQSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000516' AND c.sku = 'RO120EL86FBHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000516' AND c.sku = 'MI046EL41IRIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000516' AND c.sku = 'MI046EL33IRQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000516' AND c.sku = 'HE058EL62IUJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000516' AND c.sku = 'BL131EL14EORPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000516' AND c.sku = 'TH052EL24IODPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000536' AND c.sku = 'SO029EL30GMBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000716' AND c.sku = 'MI046EL41IRIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000329' AND c.sku = 'SA026EL82GRTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000669' AND c.sku = 'KL124EL61GHAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000969' AND c.sku = 'MA019EL06ELDPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000999' AND c.sku = 'CA035EL54GDLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004452' AND c.sku = 'FI173TB88MXBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006372' AND c.sku = 'FI173TB88MXBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006372' AND c.sku = 'FI173TB88MXBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006372' AND c.sku = 'FI173TB75HPCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006372' AND c.sku = 'AV162TB08HCHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004452' AND c.sku = 'FI173TB75HPCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004452' AND c.sku = 'AV162TB49HEOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004452' AND c.sku = 'AV162TB08HCHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004626' AND c.sku = 'BL112EL05KMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004452' AND c.sku = 'FI173TB88MXBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003572' AND c.sku = 'EP057EL11LNOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008252' AND c.sku = 'SA026EL69MMGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004852' AND c.sku = 'PL158BO79MEEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009852' AND c.sku = 'PL158BO79MEEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004552' AND c.sku = 'SO029EL07PVGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001352' AND c.sku = 'BE247EL21MZQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009412' AND c.sku = 'HO176TB84MXFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007412' AND c.sku = 'SA315EL72NYPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004452' AND c.sku = 'FI173TB88MXBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002182' AND c.sku = 'NU168TB56JGDPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003582' AND c.sku = 'NU168TB60JFZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003582' AND c.sku = 'NU168TB56JGDPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003582' AND c.sku = 'SC294EL26MRTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004182' AND c.sku = 'NU168TB49JCOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004182' AND c.sku = 'NU168TB48JCPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004182' AND c.sku = 'NU168TB60JFZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004182' AND c.sku = 'NU168TB56JGDPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006372' AND c.sku = 'FI173TB88MXBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002182' AND c.sku = 'SC294EL26MRTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006372' AND c.sku = 'AV162TB49HEOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002182' AND c.sku = 'NU168TB48JCPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002182' AND c.sku = 'NU168TB49JCOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002182' AND c.sku = 'NU168TB60JFZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004972' AND c.sku = 'BE247EL22KARPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009972' AND c.sku = 'PH121EL89KFUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004772' AND c.sku = 'BL131EL20EOLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005572' AND c.sku = 'EP057EL11LNOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009612' AND c.sku = 'FI173TB17MVYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004182' AND c.sku = 'SC294EL26MRTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008346' AND c.sku = 'AO002EL49QMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003212' AND c.sku = 'PA020EL25NSSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004746' AND c.sku = 'SA026EL76EILPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002746' AND c.sku = 'SA026EL76EILPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006746' AND c.sku = 'SA026EL76EILPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009746' AND c.sku = 'SA026EL76EILPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008746' AND c.sku = 'SA026EL76EILPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007746' AND c.sku = 'SA026EL76EILPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002846' AND c.sku = 'BL112EL03ISUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006346' AND c.sku = 'VI068EL67IYAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007332' AND c.sku = 'TA119EL01DVYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006226' AND c.sku = 'AO002EL49QMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006226' AND c.sku = 'AP032EL23KQAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008226' AND c.sku = 'LO103EL51DUAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008226' AND c.sku = 'SA026EL71LPCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005226' AND c.sku = 'SA026EL21RLAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001226' AND c.sku = 'PH021EL46DUFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001226' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000021' AND c.sku = 'NI047EL33EZMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008546' AND c.sku = 'AR444EL64QTZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008812' AND c.sku = 'SO029EL40QBRPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001582' AND c.sku = 'NU168TB48JCPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009612' AND c.sku = 'NU168TB52JCLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009612' AND c.sku = 'PI301TB08NDZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005612' AND c.sku = 'AP032EL96KRBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004812' AND c.sku = 'RE024EL76EQDPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004812' AND c.sku = 'RE024EL76EQDPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006812' AND c.sku = 'IM013EL41ODQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006846' AND c.sku = 'SO029EL97GNIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008812' AND c.sku = 'SO029EL51QBGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004612' AND c.sku = 'PA020EL25NSSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004512' AND c.sku = 'LG018EL44KWXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001512' AND c.sku = 'AL183BO75MTSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008632' AND c.sku = 'BL112EL05KMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007632' AND c.sku = 'BL112EL05KMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005632' AND c.sku = 'BL112EL05KMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006732' AND c.sku = 'SO029EL08PVFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002132' AND c.sku = 'CA447EL93QPAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006132' AND c.sku = 'CA447EL93QPAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008812' AND c.sku = 'SO029EL34QBXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004322' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001322' AND c.sku = 'FI173TB44HQHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004322' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004322' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000799' AND c.sku = 'LG018EL72GGPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000279' AND c.sku = 'MA019EL81FNAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000459' AND c.sku = 'SO029EL22GMJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000259' AND c.sku = 'SO029EL22GMJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000316' AND c.sku = 'PH121EL84GCHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000156' AND c.sku = 'SE163EL08HJZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000362' AND c.sku = 'FU038EL22GAVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000176' AND c.sku = 'SA026EL64GGXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000376' AND c.sku = 'SA026EL64GGXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000456' AND c.sku = 'SA026EL64GGXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000256' AND c.sku = 'SA026EL64GGXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000656' AND c.sku = 'SA026EL64GGXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000956' AND c.sku = 'SA026EL64GGXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000516' AND c.sku = 'HE058EL62IUJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000556' AND c.sku = 'HE058EL62IUJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000586' AND c.sku = 'KI042EL73BJQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000716' AND c.sku = 'BL131EL14EORPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000716' AND c.sku = 'RO120EL61EQSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000716' AND c.sku = 'RO120EL86FBHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000716' AND c.sku = 'TH052EL24IODPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000716' AND c.sku = 'SA026EL65GGWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000716' AND c.sku = 'MI046EL33IRQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000716' AND c.sku = 'HE058EL62IUJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000716' AND c.sku = 'HE058EL62IUJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000756' AND c.sku = 'CA035EL52GDNPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000818' AND c.sku = 'LG018EL76GGLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000029' AND c.sku = 'AO002ME58FRTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000026' AND c.sku = 'GE116EL12EHBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000066' AND c.sku = 'PH021EL21DVEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000096' AND c.sku = 'BL112EL56EJFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000086' AND c.sku = 'MA019EL10EKZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000076' AND c.sku = 'PH021EL21DVEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000016' AND c.sku = 'PH021EL23DVCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000052' AND c.sku = 'JC117EL68EITPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000818' AND c.sku = 'BL112EL03ISUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000092' AND c.sku = 'GE116EL41EFYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000818' AND c.sku = 'LG018EL76GGLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000818' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000718' AND c.sku = 'LG018EL78GGJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000718' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000718' AND c.sku = 'LG018EL76GGLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000718' AND c.sku = 'LG018EL76GGLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000238' AND c.sku = 'AP032EL31KPSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000292' AND c.sku = 'GE116EL63EIYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000598' AND c.sku = 'LG018EL44KWXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000442' AND c.sku = 'NI047EL36EZJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000091' AND c.sku = 'GE116EL12EHBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000081' AND c.sku = 'GE116EL66EIVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000071' AND c.sku = 'GE116EL66EIVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000051' AND c.sku = 'NI047EL36EZJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000011' AND c.sku = 'SA026EL76EILPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000023' AND c.sku = 'CO072EL92GBZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000063' AND c.sku = 'PH021EL40DULPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000032' AND c.sku = 'MA019EL10EKZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000033' AND c.sku = 'CA035EL18GEVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000029' AND c.sku = 'LO103EL90DSNPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000942' AND c.sku = 'MO115EL73EIOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000542' AND c.sku = 'GE116EL16EGXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000142' AND c.sku = 'FO118BO32FLBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000001' AND c.sku = 'GE116EL92EHVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000042' AND c.sku = 'NI047EL36EZJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000062' AND c.sku = 'MA019EL49EJMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000092' AND c.sku = 'GE116EL41EFYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000092' AND c.sku = 'GE116EL41EFYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000053' AND c.sku = 'PH021EL21DVEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000122' AND c.sku = 'SA026EL76EILPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006842' AND c.sku = 'MA019EL53EJIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000065' AND c.sku = 'GE116EL66EIVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000095' AND c.sku = 'PH021EL47DUEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000075' AND c.sku = 'NI047EL36EZJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000222' AND c.sku = 'GE116EL11EHCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000622' AND c.sku = 'NI047EL36EZJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000922' AND c.sku = 'FU038EL21GAWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000017' AND c.sku = 'PH021EL47DUEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000722' AND c.sku = 'MO115EL74EINPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000057' AND c.sku = 'CO127BO62FNTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000322' AND c.sku = 'SA026EL76EILPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000462' AND c.sku = 'SA026EL76EILPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000662' AND c.sku = 'FU038EL22GAVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000962' AND c.sku = 'MA019EL24EKLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000962' AND c.sku = 'MA019EL07ELCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000962' AND c.sku = 'AO002ME74FRDPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000162' AND c.sku = 'BL112EL66EIVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000559' AND c.sku = 'BL112EL04KMXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000722' AND c.sku = 'AO002ME72FRFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000039' AND c.sku = 'BL112EL56EJFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000029' AND c.sku = 'GE116EL27EGMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000069' AND c.sku = 'GE116EL12EHBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000069' AND c.sku = 'GE116EL11EHCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000099' AND c.sku = 'GE116EL11EHCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000089' AND c.sku = 'GE116EL66EIVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000079' AND c.sku = 'GE116EL11EHCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000059' AND c.sku = 'GE116EL11EHCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000045' AND c.sku = 'PR022EL57BZQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000019' AND c.sku = 'BL112EL56EJFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000492' AND c.sku = 'GE116EL63EIYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000048' AND c.sku = 'BL112EL56EJFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000028' AND c.sku = 'PH021EL40DULPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000088' AND c.sku = 'BL112EL56EJFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000018' AND c.sku = 'LO103EL55DTWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000047' AND c.sku = 'PH021EL47DUEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000067' AND c.sku = 'MO115EL74EINPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000067' AND c.sku = 'MO115EL74EINPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000087' AND c.sku = 'CO127BO77FNEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000059' AND c.sku = 'NI047EL36EZJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000531' AND c.sku = 'SO029EL52GLFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000893' AND c.sku = 'NI047EL42EZDPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000751' AND c.sku = 'WU146BO10GIZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000751' AND c.sku = 'WU146BO10GIZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000751' AND c.sku = 'WU146BO26GIJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000211' AND c.sku = 'LE060EL06KIZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000911' AND c.sku = 'SE163EL94HKNPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000631' AND c.sku = 'MO115EL94ITDPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000851' AND c.sku = 'WU146BO26GIJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000731' AND c.sku = 'PH121EL51KDKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000851' AND c.sku = 'WU146BO26GIJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000643' AND c.sku = 'NU168TB33HMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000943' AND c.sku = 'NU168TB33HMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000423' AND c.sku = 'VA145EL00FEPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000963' AND c.sku = 'KI042EL76JBNPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000863' AND c.sku = 'KI042EL76JBNPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000293' AND c.sku = 'LG018EL41KXAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000693' AND c.sku = 'LG018EL41KXAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000661' AND c.sku = 'SA026EL76EILPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000831' AND c.sku = 'SO029EL52GLFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000651' AND c.sku = 'WU146BO10GIZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000559' AND c.sku = 'KE041EL49FZUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000661' AND c.sku = 'SO029EL60GKXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000661' AND c.sku = 'SA026EL76EILPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000861' AND c.sku = 'KL124EL89FMSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000491' AND c.sku = 'PH121EL51KDKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000991' AND c.sku = 'PQ176EL36GEDPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000571' AND c.sku = 'SE163EL03HKEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000751' AND c.sku = 'WU146BO26GIJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000651' AND c.sku = 'WU146BO26GIJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000783' AND c.sku = 'FA195TB43INKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000651' AND c.sku = 'WU146BO26GIJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000651' AND c.sku = 'WU146BO10GIZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000951' AND c.sku = 'WU146BO26GIJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000951' AND c.sku = 'WU146BO26GIJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000951' AND c.sku = 'WU146BO10GIZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000951' AND c.sku = 'WU146BO10GIZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000851' AND c.sku = 'WU146BO10GIZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000851' AND c.sku = 'WU146BO10GIZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000251' AND c.sku = 'IM013EL22LQZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003622' AND c.sku = 'TA182BO64IBBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000993' AND c.sku = 'LG018EL41KXAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005142' AND c.sku = 'KI042EL76JBNPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001142' AND c.sku = 'KE041EL52FZRPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003142' AND c.sku = 'KE041EL52FZRPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003342' AND c.sku = 'SE163EL10HJXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009422' AND c.sku = 'CA035EL45LEOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008222' AND c.sku = 'SI027EL68IQHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001542' AND c.sku = 'BL112EL04MSPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003622' AND c.sku = 'UN194BO42IYZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007542' AND c.sku = 'BL112EL04MSPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004922' AND c.sku = 'UN194BO42IYZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004922' AND c.sku = 'TA182BO64IBBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004922' AND c.sku = 'WU146BO18GIRPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004722' AND c.sku = 'AP032EL52KOXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005722' AND c.sku = 'KE041EL46FZXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009522' AND c.sku = 'BL112EL03ISUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008522' AND c.sku = 'KE041EL52FZRPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004176' AND c.sku = 'FI173TB82MXHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005622' AND c.sku = 'BL112EL03ISUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004942' AND c.sku = 'BL112EL04ISTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000583' AND c.sku = 'FA195TB43INKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000773' AND c.sku = 'SA026EL75EIMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000953' AND c.sku = 'SO029EL26HFLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000113' AND c.sku = 'SA026EL75EIMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000313' AND c.sku = 'FU038EL21GAWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007242' AND c.sku = 'BL112EL04KMXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005242' AND c.sku = 'BL112EL04KMXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004142' AND c.sku = 'BL112EL04MSPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003642' AND c.sku = 'AO002ME72FRFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000661' AND c.sku = 'FU038EL22GAVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002942' AND c.sku = 'PH121EL88KBZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005942' AND c.sku = 'CO088EL07KBGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005742' AND c.sku = 'AO002ME70FRHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001742' AND c.sku = 'LG018EL41KXAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003742' AND c.sku = 'LG018EL41KXAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004542' AND c.sku = 'MO115EL92ITFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002542' AND c.sku = 'MO115EL92ITFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009542' AND c.sku = 'MO115EL92ITFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001642' AND c.sku = 'AO002ME72FRFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000617' AND c.sku = 'CA035EL71GCUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000765' AND c.sku = 'PH121EL55KDGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000167' AND c.sku = 'IM013EL22LQZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000477' AND c.sku = 'NI047EL37EZIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000977' AND c.sku = 'SI027EL58IQRPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000377' AND c.sku = 'IM013EL22LQZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000457' AND c.sku = 'IM013EL22LQZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000457' AND c.sku = 'TO066EL44IRFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000427' AND c.sku = 'FI173TB34HQRPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000357' AND c.sku = 'IM013EL22LQZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000218' AND c.sku = 'MO115EL92ITFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000117' AND c.sku = 'CA035EL66GCZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000117' AND c.sku = 'AP032EL23KQAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000117' AND c.sku = 'AP032EL99KNCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000117' AND c.sku = 'CA035EL68GCXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000117' AND c.sku = 'AP032EL52KOXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000637' AND c.sku = 'AP032EL19KQEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000945' AND c.sku = 'GE116EL66EIVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000661' AND c.sku = 'VE126EL98ISZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000557' AND c.sku = 'PH121EL87KCAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000348' AND c.sku = 'SO029EL23GMIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000619' AND c.sku = 'WH030EL18EWFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000919' AND c.sku = 'WH030EL18EWFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000819' AND c.sku = 'WH030EL18EWFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000719' AND c.sku = 'WH030EL18EWFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000639' AND c.sku = 'IN202EL33IVMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000448' AND c.sku = 'SA026EL85GRQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000567' AND c.sku = 'IM013EL22LQZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000148' AND c.sku = 'SO029EL23GMIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000565' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000968' AND c.sku = 'AP032EL17KQGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000568' AND c.sku = 'PH121EL59KGYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000288' AND c.sku = 'AP032EL54KOVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009736' AND c.sku = 'CA447EL21TFEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005736' AND c.sku = 'CA447EL21TFEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003736' AND c.sku = 'CA447EL21TFEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200003966' AND c.sku = 'PI301TB25NDIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200003366' AND c.sku = 'SE163EL08HJZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200008296' AND c.sku = 'SO029EL42MFPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200006796' AND c.sku = 'NI047EL38EZHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200001396' AND c.sku = 'LE060EL68RUTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200003396' AND c.sku = 'NI047EL34EZLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200003656' AND c.sku = 'LE060EL70MMFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200006916' AND c.sku = 'LG018EL44KWXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200005116' AND c.sku = 'TA452EL67QQAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200003986' AND c.sku = 'CA447EL49SKWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200001886' AND c.sku = 'CA447EL49SKWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200004229' AND c.sku = 'CA036EL01LCKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200002899' AND c.sku = 'SO029EL61MEWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200003199' AND c.sku = 'AU478TB06SIRPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200007279' AND c.sku = 'FI173TB05MWKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200001389' AND c.sku = 'AV162TB31KXKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200009559' AND c.sku = 'HA418BO09PNMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200005619' AND c.sku = 'PH021EL32DUTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200001519' AND c.sku = 'FI173TB06MWJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200008439' AND c.sku = 'FI173TB39HQMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200002939' AND c.sku = 'BA265TB06LJXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200001539' AND c.sku = 'EL070EL88QPFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200007628' AND c.sku = 'AV162TB91HCYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200008928' AND c.sku = 'FI173TB66HPLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200007928' AND c.sku = 'PR143EL81FFIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200003258' AND c.sku = 'FI173TB43HQIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200005247' AND c.sku = 'SO029EL32WLJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200002867' AND c.sku = 'LG018EL91QLGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200008817' AND c.sku = 'SP645FA12BUPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200008817' AND c.sku = 'FI173TB76MXNPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200001517' AND c.sku = 'KO448EL56QQLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200001145' AND c.sku = 'HE309FA65BSOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200009495' AND c.sku = 'PI301TB09NHUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200006795' AND c.sku = 'NI047EL33EZMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200001685' AND c.sku = 'HE058EL73GCSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200001585' AND c.sku = 'AS282EL87TOEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200001585' AND c.sku = 'AS282EL87TOEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200005185' AND c.sku = 'BL112EL05KMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200009232' AND c.sku = 'AP032EL15KQIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200009232' AND c.sku = 'AP032EL15KQIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200009232' AND c.sku = 'AP032EL15KQIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200008946' AND c.sku = 'PI301TB08NDZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200001626' AND c.sku = 'SA026EL91GRKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200004326' AND c.sku = 'TO066EL03QWIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =0, PENDING =0, RETURNED = 1 WHERE order_nr = '200006632' AND c.sku = 'GA010EL92KRFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =0, PENDING =0, RETURNED = 1 WHERE order_nr = '200003726' AND c.sku = 'HU445EL62QUBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =0, PENDING =0, RETURNED = 1 WHERE order_nr = '200001126' AND c.sku = 'IN210EL85IXIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =0, PENDING =0, RETURNED = 1 WHERE order_nr = '200006266' AND c.sku = 'PI301TB28NDFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =0, PENDING =0, RETURNED = 1 WHERE order_nr = '200009666' AND c.sku = 'TR307TB86NQJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =0, PENDING =0, RETURNED = 1 WHERE order_nr = '200006558' AND c.sku = 'LE060EL08BJFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =0, PENDING =0, RETURNED = 1 WHERE order_nr = '200004218' AND c.sku = 'LE060EL83XGMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =0, PENDING =0, RETURNED = 1 WHERE order_nr = '200001557' AND c.sku = 'PI301TB11NDWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =0, PENDING =0, RETURNED = 1 WHERE order_nr = '200001557' AND c.sku = 'BA211TB87JEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =0, PENDING =0, RETURNED = 1 WHERE order_nr = '200004217' AND c.sku = 'SA026EL42WOVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005532' AND c.sku = 'BL112EL98NXPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006496' AND c.sku = 'SA026EL21RLAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009736' AND c.sku = 'CA447EL21TFEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006899' AND c.sku = 'FI173TB77HPAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006779' AND c.sku = 'HT487EL53SKSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008439' AND c.sku = 'FI173TB39HQMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008928' AND c.sku = 'FI173TB66HPLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002558' AND c.sku = 'LE060EL08BJFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001517' AND c.sku = 'KO448EL56QQLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005876' AND c.sku = 'LG018EL20RLBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008776' AND c.sku = 'SA026EL54MQRPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007776' AND c.sku = 'FI173TB62MYBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001776' AND c.sku = 'HO176TB84MXFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001776' AND c.sku = 'FI173TB82MXHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003776' AND c.sku = 'FI173TB82MXHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008476' AND c.sku = 'SO029EL98PVPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001586' AND c.sku = 'AP032EL96KRBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006396' AND c.sku = 'DA007EL21QVQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004786' AND c.sku = 'WU146BO26GIJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008786' AND c.sku = 'AO002ME66FRLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006586' AND c.sku = 'KI042EL37LMOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006586' AND c.sku = 'AP032EL96KRBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007586' AND c.sku = 'AP032EL96KRBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007586' AND c.sku = 'KI042EL37LMOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007186' AND c.sku = 'AP032EL96KRBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005586' AND c.sku = 'KI042EL37LMOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003886' AND c.sku = 'WU146BO26GIJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001586' AND c.sku = 'KI042EL37LMOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004186' AND c.sku = 'KI042EL37LMOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004186' AND c.sku = 'AP032EL96KRBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002186' AND c.sku = 'AP032EL96KRBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002186' AND c.sku = 'KI042EL37LMOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006186' AND c.sku = 'AP032EL96KRBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006186' AND c.sku = 'KI042EL37LMOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008186' AND c.sku = 'AP032EL96KRBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005586' AND c.sku = 'AP032EL96KRBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005466' AND c.sku = 'FI173TB76HPBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005266' AND c.sku = 'SA026EL71EIQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006126' AND c.sku = 'SA026EL21RLAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008326' AND c.sku = 'DE455EL10RDTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008326' AND c.sku = 'TA119EL94DWFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008326' AND c.sku = 'LO103EL59DTSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007326' AND c.sku = 'TA119EL94DWFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007326' AND c.sku = 'LO103EL59DTSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004126' AND c.sku = 'SO029EL94GNLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005466' AND c.sku = 'FI173TB77HPAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005726' AND c.sku = 'SO029EL83QAAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005466' AND c.sku = 'BA211TB71KZSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001466' AND c.sku = 'EL008EL77EXUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004266' AND c.sku = 'EL008EL77EXUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007266' AND c.sku = 'TO066EL54DARPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007266' AND c.sku = 'MO115EL95ITCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007266' AND c.sku = 'SA026EL71EIQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005266' AND c.sku = 'TO066EL54DARPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006396' AND c.sku = 'AO002EL49QMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007326' AND c.sku = 'DE455EL10RDTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009826' AND c.sku = 'SA026EL71LPCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003626' AND c.sku = 'SO029EL96PVRPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002926' AND c.sku = 'AL183BO45HUCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008926' AND c.sku = 'AP032EL23KQAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008926' AND c.sku = 'AO002EL49QMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007926' AND c.sku = 'AO002EL49QMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007926' AND c.sku = 'AP032EL23KQAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001926' AND c.sku = 'AP032EL23KQAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002126' AND c.sku = 'SA026EL21RLAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009826' AND c.sku = 'LO103EL51DUAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001266' AND c.sku = 'SA026EL71EIQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008826' AND c.sku = 'BL112EL72EIPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005826' AND c.sku = 'SA026EL71LPCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005826' AND c.sku = 'LO103EL51DUAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001826' AND c.sku = 'LO103EL51DUAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001826' AND c.sku = 'SA026EL71LPCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003826' AND c.sku = 'AP032EL23KQAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003826' AND c.sku = 'AO002EL49QMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009726' AND c.sku = 'AP032EL23KQAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001926' AND c.sku = 'AO002EL49QMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003196' AND c.sku = 'AO002EL49QMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005266' AND c.sku = 'MO115EL95ITCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009196' AND c.sku = 'CA447EL98QOVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009196' AND c.sku = 'SO029EL03PVKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008196' AND c.sku = 'SO029EL03PVKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008196' AND c.sku = 'CA447EL98QOVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001196' AND c.sku = 'DA007EL21QVQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001196' AND c.sku = 'OS049EL18EGVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004196' AND c.sku = 'LE060EL69RUSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003196' AND c.sku = 'DA007EL21QVQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003596' AND c.sku = 'LE060EL69RUSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003196' AND c.sku = 'OS049EL18EGVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004396' AND c.sku = 'DA007EL21QVQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004396' AND c.sku = 'OS049EL18EGVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004396' AND c.sku = 'AO002EL49QMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002396' AND c.sku = 'DA007EL21QVQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002396' AND c.sku = 'OS049EL18EGVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002396' AND c.sku = 'AO002EL49QMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004122' AND c.sku = 'SA026EL68GGTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001196' AND c.sku = 'AO002EL49QMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009166' AND c.sku = 'DA007EL24QVNPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001266' AND c.sku = 'TO066EL54DARPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001266' AND c.sku = 'MO115EL95ITCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005966' AND c.sku = 'AL183BO27ICMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005966' AND c.sku = 'AL183BO64HXFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005966' AND c.sku = 'AL183BO35ICEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005966' AND c.sku = 'AL183BO97HVYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004322' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004322' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004322' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004322' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004322' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004322' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004322' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003582' AND c.sku = 'NU168TB48JCPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004322' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001322' AND c.sku = 'GA010EL53KSSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001122' AND c.sku = 'OX193EL75IMEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009122' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005592' AND c.sku = 'SO029EL27HFKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004322' AND c.sku = 'CO072EL07EWQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002492' AND c.sku = 'MA019EL26EKJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008492' AND c.sku = 'OX193EL12IKTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008292' AND c.sku = 'SO029EL04GNBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007292' AND c.sku = 'BL112EL90NXXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005292' AND c.sku = 'LG018EL41KHQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006692' AND c.sku = 'AP032EL95KNGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002862' AND c.sku = 'BA211TB98JENPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003892' AND c.sku = 'UN194BO96JATPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002862' AND c.sku = 'BA171TB35LBCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003592' AND c.sku = 'SA026EL59MMQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009192' AND c.sku = 'AO002ME72FRFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009392' AND c.sku = 'FU038EL22GAVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001582' AND c.sku = 'SC294EL26MRTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001582' AND c.sku = 'NU168TB56JGDPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001582' AND c.sku = 'NU168TB60JFZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001582' AND c.sku = 'NU168TB49JCOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006626' AND c.sku = 'CA035EL67GCYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007692' AND c.sku = 'LG018EL34MZDPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008262' AND c.sku = 'SA026EL69MMGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001322' AND c.sku = 'GA010EL60KSLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001322' AND c.sku = 'BO313TB68NUXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004462' AND c.sku = 'FI173TB44HQHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004462' AND c.sku = 'GA010EL53KSSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004462' AND c.sku = 'GA010EL60KSLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004462' AND c.sku = 'BO313TB68NUXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003462' AND c.sku = 'PH121EL51KDKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002492' AND c.sku = 'PA020EL34NSJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006262' AND c.sku = 'PH121EL51KDKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003582' AND c.sku = 'NU168TB49JCOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002662' AND c.sku = 'MA019EL82FMZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002662' AND c.sku = 'MA019EL24EKLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009662' AND c.sku = 'SA026EL79NYIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008662' AND c.sku = 'SA026EL79NYIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009962' AND c.sku = 'SO029EL94GNLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004862' AND c.sku = 'SA026EL79NYIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002862' AND c.sku = 'PI301TB37NCWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002862' AND c.sku = 'IP312TB52NRRPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002262' AND c.sku = 'PH121EL51KDKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003686' AND c.sku = 'RO192TB84MTJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001486' AND c.sku = 'AG185BO20HVBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001486' AND c.sku = 'AG185BO20HVBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001486' AND c.sku = 'AG185BO20HVBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003486' AND c.sku = 'SO029EL57RZAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002286' AND c.sku = 'LE060EL69RUSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005286' AND c.sku = 'IM013EL13OESPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003286' AND c.sku = 'LE060EL65RUWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003226' AND c.sku = 'BL112EL05KMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003686' AND c.sku = 'RO192TB35IJWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001486' AND c.sku = 'AG185BO20HVBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003686' AND c.sku = 'BA171TB19IGQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003686' AND c.sku = 'RO192TB28IKDPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003686' AND c.sku = 'TO302TB71NFKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002986' AND c.sku = 'LG018EL26QJXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006986' AND c.sku = 'LG018EL26QJXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009986' AND c.sku = 'LG018EL26QJXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005986' AND c.sku = 'LG018EL26QJXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003686' AND c.sku = 'BA171TB83MXGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001486' AND c.sku = 'AG185BO20HVBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008396' AND c.sku = 'LE060EL69RUSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008396' AND c.sku = 'LE060EL69RUSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008396' AND c.sku = 'LE060EL69RUSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008396' AND c.sku = 'LE060EL68RUTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008396' AND c.sku = 'LE060EL68RUTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005486' AND c.sku = 'LE060EL68RUTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001486' AND c.sku = 'AG185BO20HVBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001486' AND c.sku = 'AG185BO20HVBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001486' AND c.sku = 'AG185BO20HVBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001486' AND c.sku = 'AG185BO20HVBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001486' AND c.sku = 'SA026EL64GGXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001486' AND c.sku = 'AG185BO20HVBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001486' AND c.sku = 'AG185BO20HVBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001486' AND c.sku = 'AG185BO20HVBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001486' AND c.sku = 'GE011EL52ENFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001486' AND c.sku = 'AG185BO20HVBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001486' AND c.sku = 'AG185BO20HVBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004786' AND c.sku = 'WU146BO10GIZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001486' AND c.sku = 'AG185BO20HVBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007476' AND c.sku = 'DR320EL74OGFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003886' AND c.sku = 'WU146BO10GIZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001186' AND c.sku = 'AP032EL96KRBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003186' AND c.sku = 'AP032EL96KRBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002386' AND c.sku = 'KI042EL37LMOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002386' AND c.sku = 'AP032EL96KRBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006476' AND c.sku = 'LG018EL76GGLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008476' AND c.sku = 'DR320EL74OGFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007186' AND c.sku = 'KI042EL37LMOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008476' AND c.sku = 'CA447EL49SKWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008186' AND c.sku = 'KI042EL37LMOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007476' AND c.sku = 'CA447EL49SKWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007476' AND c.sku = 'SO029EL98PVPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000688' AND c.sku = 'AP032EL54KOVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000658' AND c.sku = 'BA211TB98JENPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000658' AND c.sku = 'FI173TB57HPUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000658' AND c.sku = 'NU168TB54JCJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000658' AND c.sku = 'BA211TB80JFFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000248' AND c.sku = 'SA026EL85GRQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000935' AND c.sku = 'TO066EL20LRBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000125' AND c.sku = 'PH121EL55KDGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000635' AND c.sku = 'TO066EL20LRBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000635' AND c.sku = 'TO066EL20LRBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000635' AND c.sku = 'TO066EL20LRBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000635' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000935' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000935' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000635' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000935' AND c.sku = 'TO066EL20LRBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000635' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000935' AND c.sku = 'TO066EL20LRBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000935' AND c.sku = 'TO066EL20LRBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000935' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000135' AND c.sku = 'SA026EL76EILPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000121' AND c.sku = 'IM013EL22LQZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000661' AND c.sku = 'VE126EL98ISZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000661' AND c.sku = 'PR022EL91EPOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000661' AND c.sku = 'GE116EL16EGXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000935' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000475' AND c.sku = 'SA026EL16HNNPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000365' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000495' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000295' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000395' AND c.sku = 'AP032EL48KPBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000885' AND c.sku = 'JB040EL30GANPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000785' AND c.sku = 'JB040EL30GANPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000585' AND c.sku = 'JB040EL30GANPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000635' AND c.sku = 'TO066EL20LRBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000475' AND c.sku = 'KI042EL73JBQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000559' AND c.sku = 'AO002ME59FRSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000475' AND c.sku = 'KI042EL73JBQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000475' AND c.sku = 'ZO116EL69EISPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000475' AND c.sku = 'KI042EL73JBQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000115' AND c.sku = 'OX193EL02ILDPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000635' AND c.sku = 'TO066EL20LRBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000635' AND c.sku = 'SO029EL62MEVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000635' AND c.sku = 'SO029EL62MEVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000635' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200000185' AND c.sku = 'JB040EL30GANPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001427' AND c.sku = 'MO076EL20TQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003547' AND c.sku = 'SO029EL06VHPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008147' AND c.sku = 'NU168TB45JCSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008147' AND c.sku = 'PI301TB33NDAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004347' AND c.sku = 'MO076EL20TQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006347' AND c.sku = 'LG018EL79UDWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009427' AND c.sku = 'HE058EL73GCSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008427' AND c.sku = 'LE060EL08BJFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007488' AND c.sku = 'LG018EL63AGEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001427' AND c.sku = 'MO076EL20TQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001747' AND c.sku = 'SO029EL06VHPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002227' AND c.sku = 'SA026EL21RLAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007227' AND c.sku = 'SA026EL67GGUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001627' AND c.sku = 'SO029EL05VHQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003627' AND c.sku = 'SA026EL38QRDPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003927' AND c.sku = 'CA035EL45LEOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009527' AND c.sku = 'SO029EL05VHQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008527' AND c.sku = 'SO029EL05VHQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007427' AND c.sku = 'HU445EL62QUBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001947' AND c.sku = 'SO029EL12WPZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001447' AND c.sku = 'MA179TB79HSUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001447' AND c.sku = 'TO302TB38NGRPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003447' AND c.sku = 'IN423BO25PQSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004247' AND c.sku = 'RO542TB34TXXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006247' AND c.sku = 'SA026EL75EIMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003247' AND c.sku = 'SE163EL91HKQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003647' AND c.sku = 'SO029EL12WPZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008547' AND c.sku = 'BL112EL05KMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005947' AND c.sku = 'SO029EL12WPZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009547' AND c.sku = 'SO029EL06VHPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003947' AND c.sku = 'SO029EL12WPZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004847' AND c.sku = 'SO029EL12WPZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002847' AND c.sku = 'SO029EL12WPZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006847' AND c.sku = 'SO029EL12WPZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007847' AND c.sku = 'LE060EL08BFJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004747' AND c.sku = 'LG018EL64SODPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008747' AND c.sku = 'SA026EL67MMIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003267' AND c.sku = 'SA026EL05GQWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004947' AND c.sku = 'SO029EL12WPZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006397' AND c.sku = 'KO448EL56QQLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007327' AND c.sku = 'BL112EL05KMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004997' AND c.sku = 'HU445EL62ZFHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002997' AND c.sku = 'HU445EL62ZFHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006997' AND c.sku = 'HU445EL62ZFHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009997' AND c.sku = 'HU445EL62ZFHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009897' AND c.sku = 'GA010EL78KRTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003597' AND c.sku = 'KO448EL56QQLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003697' AND c.sku = 'HU445EL61ZFIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003197' AND c.sku = 'NI047EL33EZMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001697' AND c.sku = 'HU445EL62ZFHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006397' AND c.sku = 'KO448EL56QQLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009397' AND c.sku = 'KO448EL56QQLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003397' AND c.sku = 'SA026EL59MMQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009487' AND c.sku = 'KO448EL56QQLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008487' AND c.sku = 'KO448EL56QQLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007487' AND c.sku = 'KO448EL56QQLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005487' AND c.sku = 'KO448EL56QQLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009687' AND c.sku = 'OL048EL01QSOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008197' AND c.sku = 'KO448EL56QQLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009367' AND c.sku = 'LG018EL91QLGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004338' AND c.sku = 'LE060EL08BFJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008667' AND c.sku = 'SO029EL05VHQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007667' AND c.sku = 'AR444EL63QUAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004567' AND c.sku = 'SO029EL60GKXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002167' AND c.sku = 'LG018EL91QLGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009167' AND c.sku = 'LG018EL91QLGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007167' AND c.sku = 'LG018EL91QLGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003697' AND c.sku = 'HU445EL62ZFHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006367' AND c.sku = 'LG018EL91QLGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007267' AND c.sku = 'SA026EL39ZGEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007367' AND c.sku = 'BL112EL04KMXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008497' AND c.sku = 'SA026EL69MMGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007497' AND c.sku = 'AV162TB42HEVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003225' AND c.sku = 'IM617HL58AGJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006525' AND c.sku = 'AP032EL74QEFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001225' AND c.sku = 'SA026EL35WLGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001525' AND c.sku = 'SA026EL35WLGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002125' AND c.sku = 'PH121EL90KBXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001125' AND c.sku = 'CA035EL54GDLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002325' AND c.sku = 'SA026EL35WLGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002465' AND c.sku = 'SA026EL19TQUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002665' AND c.sku = 'BL112EL05KMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008665' AND c.sku = 'SO029EL97GNIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003245' AND c.sku = 'LG018EL53BWWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002525' AND c.sku = 'SO029EL40GLRPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009745' AND c.sku = 'SA026EL21RLAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007687' AND c.sku = 'PI301TB09NDYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007645' AND c.sku = 'LG018EL53BWWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001645' AND c.sku = 'LG018EL53BWWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007945' AND c.sku = 'CA036FA24CJNPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006845' AND c.sku = 'BE299TB90TOBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003845' AND c.sku = 'LE060EL08BJFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004745' AND c.sku = 'LE060EL08BJFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002625' AND c.sku = 'LG018EL78GGJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006745' AND c.sku = 'LE060EL08BJFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006965' AND c.sku = 'AN242EL27JHGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002545' AND c.sku = 'SA026EL39ZGEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006545' AND c.sku = 'BL112EL05KMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004145' AND c.sku = 'FI173TB76MXNPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006145' AND c.sku = 'AC446EL47BPKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007145' AND c.sku = 'SO029EL60GKXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009425' AND c.sku = 'SO029EL90PZTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005425' AND c.sku = 'SO029EL60GKXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005225' AND c.sku = 'IM617HL58AGJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002745' AND c.sku = 'LE060EL08BJFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004395' AND c.sku = 'NO125EL44ZCDPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001665' AND c.sku = 'BL112EL05KMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007995' AND c.sku = 'LG018EL53BWWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005995' AND c.sku = 'LG018EL53BWWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004595' AND c.sku = 'KR650TB29BXUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004595' AND c.sku = 'HE309FA58BSVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009595' AND c.sku = 'BL112EL05KMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007595' AND c.sku = 'BL112EL05KMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007695' AND c.sku = 'DR264TB10LVHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004395' AND c.sku = 'AL540FA53UMOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002695' AND c.sku = 'SW643FA45BPMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004395' AND c.sku = 'PH121EL74BOJPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004395' AND c.sku = 'NO125EL44ZCDPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004395' AND c.sku = 'RI658FA98CGRPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002395' AND c.sku = 'AL540FA53UMOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002395' AND c.sku = 'RI658FA98CGRPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002395' AND c.sku = 'NO125EL44ZCDPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002395' AND c.sku = 'NO125EL44ZCDPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005395' AND c.sku = 'BL112EL05KMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005195' AND c.sku = 'KO448EL56QQLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004765' AND c.sku = 'SA026EL44QQXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009965' AND c.sku = 'SO029EL90PVXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009965' AND c.sku = 'TO066EL14YZLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007965' AND c.sku = 'AP032EL74QEFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005965' AND c.sku = 'AC446EL47BPKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004865' AND c.sku = 'SA026EL44QQXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004865' AND c.sku = 'SA026EL03GQYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008865' AND c.sku = 'SA026EL44QQXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007695' AND c.sku = 'DR264TB71LWUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003865' AND c.sku = 'LG018EL53BWWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007245' AND c.sku = 'LG018EL53BWWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007765' AND c.sku = 'LG018EL20RLBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005765' AND c.sku = 'LG018EL20RLBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009565' AND c.sku = 'SA026EL44QQXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009565' AND c.sku = 'SA026EL03GQYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004165' AND c.sku = 'TA452EL68QPZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008365' AND c.sku = 'AP032EL74QEFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006295' AND c.sku = 'SA026EL35WLGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008295' AND c.sku = 'CA035EL54GDLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005865' AND c.sku = 'SA026EL44QQXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001377' AND c.sku = 'CA035EL67GCYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005757' AND c.sku = 'KO448EL56QQLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003877' AND c.sku = 'BL112EL05KMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008777' AND c.sku = 'KO448EL56QQLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005777' AND c.sku = 'AC446EL47BPKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001777' AND c.sku = 'AC446EL47BPKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003777' AND c.sku = 'AC446EL47BPKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009577' AND c.sku = 'AC446EL47BPKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004977' AND c.sku = 'OL048EL01QSOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005377' AND c.sku = 'CA035EL67GCYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004387' AND c.sku = 'BO004EL11SMIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008457' AND c.sku = 'CA035EL67GCYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007457' AND c.sku = 'CA036EL75LDKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003457' AND c.sku = 'AP032EL74QEFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009257' AND c.sku = 'CA036EL87LCYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001657' AND c.sku = 'SO029EL05VHQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004957' AND c.sku = 'BL112EL96NXRPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001857' AND c.sku = 'KO448EL56QQLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004645' AND c.sku = 'LG018EL53BWWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007377' AND c.sku = 'CA035EL67GCYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009187' AND c.sku = 'BO004EL11SMIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007398' AND c.sku = 'SA026EL69MMGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006887' AND c.sku = 'RE025EL70OZPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008887' AND c.sku = 'HE058EL74GCRPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002587' AND c.sku = 'KO448EL87QPGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001587' AND c.sku = 'FU038EL11GBGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003587' AND c.sku = 'FU038EL11GBGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004187' AND c.sku = 'FU038EL11GBGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006977' AND c.sku = 'BL112EL05KMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006187' AND c.sku = 'BO004EL11SMIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002557' AND c.sku = 'ED566BO05XNIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009187' AND c.sku = 'BO004EL11SMIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008187' AND c.sku = 'BO004EL11SMIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008187' AND c.sku = 'BO004EL11SMIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007187' AND c.sku = 'BO004EL11SMIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007187' AND c.sku = 'BO004EL11SMIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005187' AND c.sku = 'BO004EL11SMIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005187' AND c.sku = 'BO004EL11SMIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004387' AND c.sku = 'BO004EL11SMIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006187' AND c.sku = 'BO004EL11SMIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004137' AND c.sku = 'LG018EL53BWWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004757' AND c.sku = 'SO029EL05VHQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005937' AND c.sku = 'LG018EL53BWWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003937' AND c.sku = 'SA026EL42WOVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009837' AND c.sku = 'LG018EL53BWWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009737' AND c.sku = 'AU478TB06SIRPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002537' AND c.sku = 'LG018EL53BWWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008537' AND c.sku = 'LG018EL53BWWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004937' AND c.sku = 'AI031EL52LPVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003537' AND c.sku = 'LG018EL53BWWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004937' AND c.sku = 'TI536FA15TUUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002137' AND c.sku = 'BE646FA88BVNPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007137' AND c.sku = 'LG018EL53BWWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005137' AND c.sku = 'SO029EL95PZOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005137' AND c.sku = 'SO029EL43MFOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001137' AND c.sku = 'SO029EL95PZOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001137' AND c.sku = 'SO029EL43MFOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006445' AND c.sku = 'SA026EL44QQXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002245' AND c.sku = 'LG018EL53BWWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005537' AND c.sku = 'LG018EL53BWWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004917' AND c.sku = 'CA036FA40VGHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006557' AND c.sku = 'ED566BO05XNIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006157' AND c.sku = 'AP032EL74QEFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007157' AND c.sku = 'LG018EL26QJXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005157' AND c.sku = 'LG018EL26QJXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002417' AND c.sku = 'TO066EL14YZLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005417' AND c.sku = 'NI047EL33EZMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004617' AND c.sku = 'UB660EL63CEEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009937' AND c.sku = 'LG018EL53BWWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001617' AND c.sku = 'LG018EL26QJXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006987' AND c.sku = 'SE163EL12HJVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007917' AND c.sku = 'CA036FA40VGHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003917' AND c.sku = 'LG018EL26QJXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001717' AND c.sku = 'SA026EL42WOVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004117' AND c.sku = 'AC446EL47BPKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009117' AND c.sku = 'SA026EL44QQXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003437' AND c.sku = 'AV661EL01CKKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004237' AND c.sku = 'AV661EL01CKKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006637' AND c.sku = 'GA113EL43EJSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007617' AND c.sku = 'LG018EL26QJXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006969' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006669' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009669' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008669' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007669' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005669' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001669' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003669' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003288' AND c.sku = 'HO176TB00HRZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002969' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003269' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009969' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008969' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007969' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005969' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001969' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003969' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004869' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004969' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004269' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001129' AND c.sku = 'OX193EL98ILHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003329' AND c.sku = 'CA447EL21TFEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004469' AND c.sku = 'CA447EL21TFEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002469' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009469' AND c.sku = 'CA447EL21TFEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007469' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005469' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002669' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003469' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004669' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002269' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006269' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009269' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008269' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007269' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005269' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001269' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009869' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001469' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007169' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002869' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005569' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001569' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003569' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004169' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002169' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006169' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008569' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008169' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009569' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005169' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001169' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003169' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004369' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002369' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006369' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009369' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008369' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009169' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009769' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008129' AND c.sku = 'SA026EL00GRBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008869' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007869' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005869' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001869' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003869' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004769' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007569' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006769' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006869' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008769' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007769' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005769' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001769' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003769' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004569' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002569' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006569' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002769' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009316' AND c.sku = 'CA447EL21TFEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001836' AND c.sku = 'SO029EL05PZEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009916' AND c.sku = 'SA026EL60MMPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007816' AND c.sku = 'SA026EL60MMPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004716' AND c.sku = 'SO029EL30QCBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004716' AND c.sku = 'CA447EL93QPAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002716' AND c.sku = 'CA447EL93QPAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002716' AND c.sku = 'SO029EL30QCBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007416' AND c.sku = 'SA026EL60MMPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002116' AND c.sku = 'SO029EL03PVKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008416' AND c.sku = 'SA026EL60MMPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008316' AND c.sku = 'CA447EL21TFEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007316' AND c.sku = 'CA447EL21TFEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008436' AND c.sku = 'LG018EL44KWXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007436' AND c.sku = 'LG018EL44KWXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005436' AND c.sku = 'LG018EL44KWXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006636' AND c.sku = 'CA447EL21TFEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009636' AND c.sku = 'CA447EL21TFEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005129' AND c.sku = 'TA119EL84DWPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001516' AND c.sku = 'CA447EL49SKWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001756' AND c.sku = 'SA026EL60MMPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001395' AND c.sku = 'BL112EL05KMWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006256' AND c.sku = 'SA026EL35QRGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004656' AND c.sku = 'KI042EL77JBMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007656' AND c.sku = 'FI173TB74HPDPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007656' AND c.sku = 'FI173TB74HPDPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002956' AND c.sku = 'BE247EL20MZRPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009756' AND c.sku = 'SA026EL60MMPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009216' AND c.sku = 'SA026EL60MMPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005756' AND c.sku = 'SA026EL60MMPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006736' AND c.sku = 'AC446EL00QOTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004556' AND c.sku = 'SA026EL60MMPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009556' AND c.sku = 'CA447EL93QPAPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009556' AND c.sku = 'SO029EL30QCBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007556' AND c.sku = 'SA026EL60MMPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005556' AND c.sku = 'SA026EL60MMPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001556' AND c.sku = 'SA026EL60MMPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002416' AND c.sku = 'SA026EL60MMPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006416' AND c.sku = 'SA026EL60MMPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007756' AND c.sku = 'SA026EL60MMPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003149' AND c.sku = 'SA026EL59MMQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003636' AND c.sku = 'SA026EL60MMPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003949' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004849' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002849' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006849' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009849' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007849' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005949' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007149' AND c.sku = 'GA113FA49QQSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007949' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004429' AND c.sku = 'BE247EL23MZOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009629' AND c.sku = 'SO029EL01PVMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005829' AND c.sku = 'AL540FA87USYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005829' AND c.sku = 'BL112EL97MSWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001829' AND c.sku = 'AL540FA87USYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001829' AND c.sku = 'BL112EL97MSWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006729' AND c.sku = 'AP032EL18KQFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001369' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006149' AND c.sku = 'CA036EL75LDKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008649' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002536' AND c.sku = 'SA026EL67MMIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006536' AND c.sku = 'SA026EL69MMGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009536' AND c.sku = 'SA026EL66GGVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008336' AND c.sku = 'FI173TB75HPCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008336' AND c.sku = 'BA306TB71NQYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007336' AND c.sku = 'BA306TB71NQYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007336' AND c.sku = 'FI173TB75HPCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001949' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003449' AND c.sku = 'SA026EL08LRNPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007129' AND c.sku = 'SA026EL00GRBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007649' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005649' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001649' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003649' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004949' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006949' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009949' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008949' AND c.sku = 'SO029EL95GNKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001336' AND c.sku = 'LG018EL77QLUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005928' AND c.sku = 'CA036EL03LCIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005128' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006428' AND c.sku = 'OX193EL07IKYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004228' AND c.sku = 'SA026EL42WOVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009228' AND c.sku = 'SO029EL06VHPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007228' AND c.sku = 'OX193EL07IKYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002628' AND c.sku = 'OX193EL07IKYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009628' AND c.sku = 'OX193EL07IKYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007148' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005928' AND c.sku = 'CA036EL03LCIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007848' AND c.sku = 'PH121EL90KBXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005928' AND c.sku = 'CA036EL03LCIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004828' AND c.sku = 'LE060EL83XGMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003728' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004528' AND c.sku = 'SA026EL44QQXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008528' AND c.sku = 'KI042EL41LMKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004128' AND c.sku = 'PR022EL37BHEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008128' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004739' AND c.sku = 'SA026EL07QWEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002928' AND c.sku = 'PH121EL90KBXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005648' AND c.sku = 'PH121EL90KBXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007369' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009739' AND c.sku = 'AV162TB43HEUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007139' AND c.sku = 'BE247EL21MZQPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003139' AND c.sku = 'RE024EL73EQGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004339' AND c.sku = 'SO029EL04GNBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001339' AND c.sku = 'SO029EL06VHPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004448' AND c.sku = 'SO029EL06VHPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005348' AND c.sku = 'PH121EL90KBXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002648' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001128' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002948' AND c.sku = 'PH121EL90KBXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006948' AND c.sku = 'PH121EL90KBXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008948' AND c.sku = 'PH121EL90KBXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008948' AND c.sku = 'SA026EL44QQXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008948' AND c.sku = 'SA026EL44QQXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003948' AND c.sku = 'PH121EL90KBXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002848' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009848' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009448' AND c.sku = 'PH121EL90KBXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003698' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005128' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002768' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006768' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005568' AND c.sku = 'OX193EL07IKYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003368' AND c.sku = 'SA026EL44QQXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009298' AND c.sku = 'OX193EL07IKYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008298' AND c.sku = 'OX193EL07IKYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005868' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001698' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008868' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004998' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009998' AND c.sku = 'SA026EL44QQXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003998' AND c.sku = 'LE060EL67JBWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002898' AND c.sku = 'BL112EL72EIPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006898' AND c.sku = 'PI314EL64NVBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009598' AND c.sku = 'GA010EL79VQIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003198' AND c.sku = 'SA026EL44QQXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009398' AND c.sku = 'GA010EL79VQIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007298' AND c.sku = 'OX193EL07IKYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009668' AND c.sku = 'PH121EL90KBXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003128' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004328' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002328' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007328' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002268' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009268' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008268' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004768' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003268' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003839' AND c.sku = 'FO539FA47UYIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001668' AND c.sku = 'SA026EL44QQXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008968' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007968' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005968' AND c.sku = 'OX193EL07IKYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003968' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004868' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006868' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009868' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001268' AND c.sku = 'GA010EL79VQIPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004199' AND c.sku = 'PH121EL04HNZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001489' AND c.sku = 'HA324TB96OJFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006699' AND c.sku = 'CA447EL21TFEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004999' AND c.sku = 'BE299TB39MYYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008999' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007999' AND c.sku = 'CA447EL21TFEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005899' AND c.sku = 'ME495TB60SOHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005899' AND c.sku = 'ME495TB34SPHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003299' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009599' AND c.sku = 'CA447EL21TFEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001299' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004199' AND c.sku = 'PH121EL04HNZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006199' AND c.sku = 'BE247EL22KARPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009199' AND c.sku = 'BE247EL22KARPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008199' AND c.sku = 'BE247EL22KARPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007199' AND c.sku = 'PI301TB08NDZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005199' AND c.sku = 'PI301TB08NDZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008399' AND c.sku = 'SE163EL94HKNPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009739' AND c.sku = 'DR264TB97LVUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009799' AND c.sku = 'SA026EL76TOPPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001499' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004176' AND c.sku = 'HO176TB84MXFPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003369' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004499' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002499' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006499' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009499' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008499' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004699' AND c.sku = 'KE494FA83SRGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005499' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001489' AND c.sku = 'TO302TB37NGSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003499' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004299' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002299' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006299' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009299' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008299' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007299' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005299' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007499' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007439' AND c.sku = 'OS049EL17TQWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001399' AND c.sku = 'LG018EL79UDWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001179' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003179' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005959' AND c.sku = 'KO448EL56QQLPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003959' AND c.sku = 'NE553FA52VJRPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007219' AND c.sku = 'BA265TB06LJXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006719' AND c.sku = 'GA010EL71MMEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005879' AND c.sku = 'PH121EL13QVYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004319' AND c.sku = 'FI173TB02MWNPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007979' AND c.sku = 'AG185BO63HXGPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007239' AND c.sku = 'WA453EL66QQBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005239' AND c.sku = 'WA453EL66QQBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006639' AND c.sku = 'WA453EL66QQBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009639' AND c.sku = 'WA453EL66QQBPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001639' AND c.sku = 'LG018EL79UDWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008939' AND c.sku = 'SA026EL42WOVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008839' AND c.sku = 'SA026EL07QWEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005839' AND c.sku = 'TA452EL70QPXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003119' AND c.sku = 'FI173TB02MWNPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008189' AND c.sku = 'SI027EL61IQOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001689' AND c.sku = 'LI535BO10TRDPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004989' AND c.sku = 'CA447EL49SKWPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009889' AND c.sku = 'CA447EL94QOZPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001889' AND c.sku = 'SA026EL66GGVPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200008789' AND c.sku = 'DR264TB10LVHPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006589' AND c.sku = 'LG018EL64SODPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200003589' AND c.sku = 'AP032EL31KPSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005179' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009189' AND c.sku = 'SI027EL61IQOPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200005369' AND c.sku = 'SO029EL59MEYPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002389' AND c.sku = 'SA026EL01LRUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002389' AND c.sku = 'SA026EL01LRUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006479' AND c.sku = 'GR225BO41KTEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004679' AND c.sku = 'RO192TB83MTKPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004679' AND c.sku = 'GF311TB67NRCPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006679' AND c.sku = 'DA007EL43QUUPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007979' AND c.sku = 'KE269EL54LLXPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200007979' AND c.sku = 'PH121EL49KDMPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004189' AND c.sku = 'AP032EL31KPSPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200001636' AND c.sku = 'CA447EL21TFEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200004316' AND c.sku = 'CA447EL21TFEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006436' AND c.sku = 'CA447EL21TFEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200001436' AND c.sku = 'CA447EL21TFEPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200002936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200006936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009936' AND c.sku = 'PR022EL60EQTPEAMZ';
UPDATE tbl_order_detail t inner join bob_live_pe.catalog_simple s on s.sku = t.sku inner join bob_live_pe.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200009936' AND c.sku = 'PR022EL60EQTPEAMZ';

#Costos
UPDATE tbl_order_detail t SET cost_pet = 467, costo_after_vat=395 WHERE order_nr = '200000036' AND sku = 'MA019EL08ELBPEAMZ-3004';
UPDATE tbl_order_detail t SET cost_pet = 126, costo_after_vat=107 WHERE order_nr = '200000058' AND sku = 'AO002ME57FRUPEAMZ-3856';
UPDATE tbl_order_detail t SET cost_pet = 23, costo_after_vat=20 WHERE order_nr = '200000058' AND sku = 'MP128BO42FONPEAMZ-3771';
UPDATE tbl_order_detail t SET cost_pet = 51, costo_after_vat=43 WHERE order_nr = '200000083' AND sku = 'TA119EL02DVXPEAMZ-2610';
UPDATE tbl_order_detail t SET cost_pet = 50, costo_after_vat=43 WHERE order_nr = '200000085' AND sku = 'PH021EL41DUKPEAMZ-2571';
UPDATE tbl_order_detail t SET cost_pet = 16, costo_after_vat=14 WHERE order_nr = '200000111' AND sku = 'KI042EL56GDJPEAMZ-4155';
UPDATE tbl_order_detail t SET cost_pet = 64, costo_after_vat=54 WHERE order_nr = '200000111' AND sku = 'SE163EL08HJZPEAMZ-5005';
UPDATE tbl_order_detail t SET cost_pet = 29, costo_after_vat=25 WHERE order_nr = '200000116' AND sku = 'KI042EL76JBNPEAMZ-6137';
UPDATE tbl_order_detail t SET cost_pet = 55, costo_after_vat=47 WHERE order_nr = '200000118' AND sku = 'ES133BO35GXKPEAMZ-4678';
UPDATE tbl_order_detail t SET cost_pet = 19, costo_after_vat=16 WHERE order_nr = '200000123' AND sku = 'AV162TB85HDEPEAMZ-4828';
UPDATE tbl_order_detail t SET cost_pet = 19, costo_after_vat=16 WHERE order_nr = '200000123' AND sku = 'FI173TB57HPUPEAMZ-5156';
UPDATE tbl_order_detail t SET cost_pet = 143, costo_after_vat=121 WHERE order_nr = '200000123' AND sku = 'GA010EL71MMEPEAMZ-8453';
UPDATE tbl_order_detail t SET cost_pet = 52, costo_after_vat=44 WHERE order_nr = '200000127' AND sku = 'FI173TB62HPPPEAMZ-5151';
UPDATE tbl_order_detail t SET cost_pet = 2519, costo_after_vat=2135 WHERE order_nr = '200000128' AND sku = 'WH030EL17EWGPEAMZ-3296';
UPDATE tbl_order_detail t SET cost_pet = 18, costo_after_vat=15 WHERE order_nr = '200000131' AND sku = 'GE116EL34EGFPEAMZ-2878';
UPDATE tbl_order_detail t SET cost_pet = 14, costo_after_vat=12 WHERE order_nr = '200000131' AND sku = 'GE116EL83EEIPEAMZ-2829';
UPDATE tbl_order_detail t SET cost_pet = 940, costo_after_vat=796 WHERE order_nr = '200000131' AND sku = 'HE058EL04LJZPEAMZ-7719';
UPDATE tbl_order_detail t SET cost_pet = 343, costo_after_vat=291 WHERE order_nr = '200000138' AND sku = 'PH121EL87KCAPEAMZ-6826';
UPDATE tbl_order_detail t SET cost_pet = 20, costo_after_vat=17 WHERE order_nr = '200000139' AND sku = 'SA051EL56IUPPEAMZ-5957';
UPDATE tbl_order_detail t SET cost_pet = 65, costo_after_vat=55 WHERE order_nr = '200000141' AND sku = 'TA119EL64DXJPEAMZ-2648';
UPDATE tbl_order_detail t SET cost_pet = 86, costo_after_vat=72 WHERE order_nr = '200000143' AND sku = 'PR022EL90EPPPEAMZ-3122';
UPDATE tbl_order_detail t SET cost_pet = 118, costo_after_vat=100 WHERE order_nr = '200000147' AND sku = 'MO115EL15KAYPEAMZ-6798';
UPDATE tbl_order_detail t SET cost_pet = 7, costo_after_vat=6 WHERE order_nr = '200000151' AND sku = 'PH121EL04HNZPEAMZ-5109';
UPDATE tbl_order_detail t SET cost_pet = 19, costo_after_vat=16 WHERE order_nr = '200000151' AND sku = 'PQ176EL31GEIPEAMZ-4180';
UPDATE tbl_order_detail t SET cost_pet = 13, costo_after_vat=11 WHERE order_nr = '200000158' AND sku = 'KI042EL74JBPPEAMZ-6139';
UPDATE tbl_order_detail t SET cost_pet = 19, costo_after_vat=16 WHERE order_nr = '200000161' AND sku = 'SC294EL31MROPEAMZ-8593';
UPDATE tbl_order_detail t SET cost_pet = 59, costo_after_vat=50 WHERE order_nr = '200000168' AND sku = 'PH121EL59KGYPEAMZ-6954';
UPDATE tbl_order_detail t SET cost_pet = 13, costo_after_vat=11 WHERE order_nr = '200000171' AND sku = 'MA175TB25HRAPEAMZ-5188';
UPDATE tbl_order_detail t SET cost_pet = 2112, costo_after_vat=1790 WHERE order_nr = '200000175' AND sku = 'AP032EL19KQEPEAMZ-7196';
UPDATE tbl_order_detail t SET cost_pet = 212, costo_after_vat=180 WHERE order_nr = '200000175' AND sku = 'AP032EL59KOQPEAMZ-7158';
UPDATE tbl_order_detail t SET cost_pet = 248, costo_after_vat=210 WHERE order_nr = '200000177' AND sku = 'SE163EL95HKMPEAMZ-5018';
UPDATE tbl_order_detail t SET cost_pet = 12, costo_after_vat=10 WHERE order_nr = '200000178' AND sku = 'NU168TB54JCJPEAMZ-6159';
UPDATE tbl_order_detail t SET cost_pet = 60, costo_after_vat=51 WHERE order_nr = '200000178' AND sku = 'RE024EL66EQNPEAMZ-3146';
UPDATE tbl_order_detail t SET cost_pet = 17, costo_after_vat=14 WHERE order_nr = '200000181' AND sku = 'BA171TB29IGGPEAMZ-5584';
UPDATE tbl_order_detail t SET cost_pet = 24, costo_after_vat=20 WHERE order_nr = '200000181' AND sku = 'FA195TB38INPPEAMZ-5775';
UPDATE tbl_order_detail t SET cost_pet = 30, costo_after_vat=25 WHERE order_nr = '200000181' AND sku = 'FA195TB40INNPEAMZ-5773';
UPDATE tbl_order_detail t SET cost_pet = 1816, costo_after_vat=1539 WHERE order_nr = '200000187' AND sku = 'AP032EL14KQJPEAMZ-7201';
UPDATE tbl_order_detail t SET cost_pet = 65, costo_after_vat=55 WHERE order_nr = '200000188' AND sku = 'TA119EL64DXJPEAMZ-2648';
UPDATE tbl_order_detail t SET cost_pet = 2269, costo_after_vat=1923 WHERE order_nr = '200000191' AND sku = 'LE060EL60JCDPEAMZ-6153';
UPDATE tbl_order_detail t SET cost_pet = 26, costo_after_vat=22 WHERE order_nr = '200000193' AND sku = 'MP128BO03FQAPEAMZ-3810';
UPDATE tbl_order_detail t SET cost_pet = 26, costo_after_vat=22 WHERE order_nr = '200000193' AND sku = 'MP128BO12FPRPEAMZ-3801';
UPDATE tbl_order_detail t SET cost_pet = 23, costo_after_vat=20 WHERE order_nr = '200000193' AND sku = 'MP128BO21FPIPEAMZ-3792';
UPDATE tbl_order_detail t SET cost_pet = 26, costo_after_vat=22 WHERE order_nr = '200000193' AND sku = 'MP128BO22FPHPEAMZ-3791';
UPDATE tbl_order_detail t SET cost_pet = 352, costo_after_vat=298 WHERE order_nr = '200000195' AND sku = 'VA145EL03FEMPEAMZ-3508';
UPDATE tbl_order_detail t SET cost_pet = 467, costo_after_vat=396 WHERE order_nr = '200000197' AND sku = 'NI047EL36EZJPEAMZ-3375';
UPDATE tbl_order_detail t SET cost_pet = 1540, costo_after_vat=1305 WHERE order_nr = '200000198' AND sku = 'LG018EL44KWXPEAMZ-7378';
UPDATE tbl_order_detail t SET cost_pet = 65, costo_after_vat=55 WHERE order_nr = '200000199' AND sku = 'MA175TB18HRHPEAMZ-5195';
UPDATE tbl_order_detail t SET cost_pet = 20, costo_after_vat=17 WHERE order_nr = '200000215' AND sku = 'FO118BO03ESYPEAMZ-3210';
UPDATE tbl_order_detail t SET cost_pet = 42, costo_after_vat=35 WHERE order_nr = '200000216' AND sku = 'HE058EL62IUJPEAMZ-5951';
UPDATE tbl_order_detail t SET cost_pet = 179, costo_after_vat=152 WHERE order_nr = '200000217' AND sku = 'IM013EL22LQZPEAMZ-7901';
UPDATE tbl_order_detail t SET cost_pet = 36, costo_after_vat=30 WHERE order_nr = '200000219' AND sku = 'PH021EL47DUEPEAMZ-2565';
UPDATE tbl_order_detail t SET cost_pet = 442, costo_after_vat=374 WHERE order_nr = '200000223' AND sku = 'VA145EL00FEPPEAMZ-3511';
UPDATE tbl_order_detail t SET cost_pet = 46, costo_after_vat=39 WHERE order_nr = '200000226' AND sku = 'TE160BO22HBTPEAMZ-4791';
UPDATE tbl_order_detail t SET cost_pet = 41, costo_after_vat=35 WHERE order_nr = '200000226' AND sku = 'WA096EL99GFOPEAMZ-4214';
UPDATE tbl_order_detail t SET cost_pet = 444, costo_after_vat=376 WHERE order_nr = '200000227' AND sku = 'PA020EL50KHHPEAMZ-6963';
UPDATE tbl_order_detail t SET cost_pet = 1699, costo_after_vat=1440 WHERE order_nr = '200000227' AND sku = 'SA026EL85GRQPEAMZ-4528';
UPDATE tbl_order_detail t SET cost_pet = 1013, costo_after_vat=859 WHERE order_nr = '200000227' AND sku = 'TH052EL26IOBPEAMZ-5787';
UPDATE tbl_order_detail t SET cost_pet = 158, costo_after_vat=134 WHERE order_nr = '200000228' AND sku = 'AP032EL39KPKPEAMZ-7177';
UPDATE tbl_order_detail t SET cost_pet = 98, costo_after_vat=83 WHERE order_nr = '200000231' AND sku = 'SI027EL85ITMPEAMZ-5928';
UPDATE tbl_order_detail t SET cost_pet = 26, costo_after_vat=22 WHERE order_nr = '200000235' AND sku = 'OS049EL86EIBPEAMZ-2927';
UPDATE tbl_order_detail t SET cost_pet = 25, costo_after_vat=21 WHERE order_nr = '200000236' AND sku = 'AV162TB31HFGPEAMZ-4882';
UPDATE tbl_order_detail t SET cost_pet = 33, costo_after_vat=28 WHERE order_nr = '200000236' AND sku = 'AV162TB39HEYPEAMZ-4874';
UPDATE tbl_order_detail t SET cost_pet = 19, costo_after_vat=16 WHERE order_nr = '200000236' AND sku = 'AV162TB85HDEPEAMZ-4828';
UPDATE tbl_order_detail t SET cost_pet = 55, costo_after_vat=46 WHERE order_nr = '200000236' AND sku = 'CR181EL52HTVPEAMZ-5261';
UPDATE tbl_order_detail t SET cost_pet = 1928, costo_after_vat=1634 WHERE order_nr = '200000237' AND sku = 'DE073EL79IXOPEAMZ-6034';
UPDATE tbl_order_detail t SET cost_pet = 86, costo_after_vat=72 WHERE order_nr = '200000239' AND sku = 'PR022EL90EPPPEAMZ-3122';
UPDATE tbl_order_detail t SET cost_pet = 60, costo_after_vat=51 WHERE order_nr = '200000241' AND sku = 'SI027EL64IQLPEAMZ-5849';
UPDATE tbl_order_detail t SET cost_pet = 1251, costo_after_vat=1060 WHERE order_nr = '200000245' AND sku = 'CA035EL36GEDPEAMZ-4177';
UPDATE tbl_order_detail t SET cost_pet = 37, costo_after_vat=31 WHERE order_nr = '200000245' AND sku = 'KI042EL50GDPPEAMZ-4161';
UPDATE tbl_order_detail t SET cost_pet = 16, costo_after_vat=13 WHERE order_nr = '200000255' AND sku = 'CY142EL08EWPPEAMZ-3303';
UPDATE tbl_order_detail t SET cost_pet = 29, costo_after_vat=25 WHERE order_nr = '200000255' AND sku = 'KI042EL78JBLPEAMZ-6135';
UPDATE tbl_order_detail t SET cost_pet = 25, costo_after_vat=21 WHERE order_nr = '200000255' AND sku = 'TA119EL61DXMPEAMZ-2651';
UPDATE tbl_order_detail t SET cost_pet = 19, costo_after_vat=16 WHERE order_nr = '200000257' AND sku = 'AV162TB84HDFPEAMZ-4829';
UPDATE tbl_order_detail t SET cost_pet = 10, costo_after_vat=8 WHERE order_nr = '200000257' AND sku = 'NU168TB60JFZPEAMZ-6253';
UPDATE tbl_order_detail t SET cost_pet = 12, costo_after_vat=10 WHERE order_nr = '200000263' AND sku = 'NU168TB54JCJPEAMZ-6159';
UPDATE tbl_order_detail t SET cost_pet = 14, costo_after_vat=12 WHERE order_nr = '200000263' AND sku = 'RO192TB24IKHPEAMZ-5689';
UPDATE tbl_order_detail t SET cost_pet = 12, costo_after_vat=10 WHERE order_nr = '200000263' AND sku = 'RO192TB31IKAPEAMZ-5682';
UPDATE tbl_order_detail t SET cost_pet = 24, costo_after_vat=21 WHERE order_nr = '200000263' AND sku = 'SC294EL27MRSPEAMZ-8597';
UPDATE tbl_order_detail t SET cost_pet = 101, costo_after_vat=86 WHERE order_nr = '200000265' AND sku = 'SE163EL07HKAPEAMZ-5006';
UPDATE tbl_order_detail t SET cost_pet = 343, costo_after_vat=291 WHERE order_nr = '200000267' AND sku = 'PH121EL88KBZPEAMZ-6825';
UPDATE tbl_order_detail t SET cost_pet = 343, costo_after_vat=291 WHERE order_nr = '200000275' AND sku = 'SO029EL76GKHPEAMZ-4337';
UPDATE tbl_order_detail t SET cost_pet = 149, costo_after_vat=126 WHERE order_nr = '200000275' AND sku = 'VA145EL95FEUPEAMZ-3516';
UPDATE tbl_order_detail t SET cost_pet = 3360, costo_after_vat=2847 WHERE order_nr = '200000276' AND sku = 'SA026EL63GGYPEAMZ-4250';
UPDATE tbl_order_detail t SET cost_pet = 179, costo_after_vat=152 WHERE order_nr = '200000281' AND sku = 'IM013EL22LQZPEAMZ-7901';
UPDATE tbl_order_detail t SET cost_pet = 76, costo_after_vat=65 WHERE order_nr = '200000283' AND sku = 'MO076EL80EPZPEAMZ-3133';
UPDATE tbl_order_detail t SET cost_pet = 40, costo_after_vat=34 WHERE order_nr = '200000285' AND sku = 'GE116EL85EICPEAMZ-2927';
UPDATE tbl_order_detail t SET cost_pet = 16, costo_after_vat=14 WHERE order_nr = '200000285' AND sku = 'KI042EL56GDJPEAMZ-4155';
UPDATE tbl_order_detail t SET cost_pet = 13, costo_after_vat=11 WHERE order_nr = '200000285' AND sku = 'KI042EL74JBPPEAMZ-6139';
UPDATE tbl_order_detail t SET cost_pet = 26, costo_after_vat=22 WHERE order_nr = '200000291' AND sku = 'FI173TB75HPCPEAMZ-5138';
UPDATE tbl_order_detail t SET cost_pet = 95, costo_after_vat=81 WHERE order_nr = '200000297' AND sku = 'GA010EL55KSQPEAMZ-7264';
UPDATE tbl_order_detail t SET cost_pet = 1699, costo_after_vat=1440 WHERE order_nr = '200000299' AND sku = 'SA026EL85GRQPEAMZ-4528';
UPDATE tbl_order_detail t SET cost_pet = 59, costo_after_vat=50 WHERE order_nr = '200000311' AND sku = 'PH121EL82KGBPEAMZ-6931';
UPDATE tbl_order_detail t SET cost_pet = 41, costo_after_vat=35 WHERE order_nr = '200000312' AND sku = 'WA096EL99GFOPEAMZ-4214';
UPDATE tbl_order_detail t SET cost_pet = 236, costo_after_vat=200 WHERE order_nr = '200000315' AND sku = 'OS049EL24EOHPEAMZ-3089';
UPDATE tbl_order_detail t SET cost_pet = 1219, costo_after_vat=1033 WHERE order_nr = '200000317' AND sku = 'AP032EL23KQAPEAMZ-7192';
UPDATE tbl_order_detail t SET cost_pet = 4160, costo_after_vat=3525 WHERE order_nr = '200000317' AND sku = 'AP032EL52KOXPEAMZ-7215';
UPDATE tbl_order_detail t SET cost_pet = 3723, costo_after_vat=3155 WHERE order_nr = '200000317' AND sku = 'AP032EL99KNCPEAMZ-7118';
UPDATE tbl_order_detail t SET cost_pet = 5345, costo_after_vat=4530 WHERE order_nr = '200000317' AND sku = 'CA035EL66GCZPEAMZ-4147';
UPDATE tbl_order_detail t SET cost_pet = 29, costo_after_vat=25 WHERE order_nr = '200000318' AND sku = 'KI042EL55GDKPEAMZ-4156';
UPDATE tbl_order_detail t SET cost_pet = 85, costo_after_vat=72 WHERE order_nr = '200000319' AND sku = 'PH121EL95KFOPEAMZ-6918';
UPDATE tbl_order_detail t SET cost_pet = 16, costo_after_vat=14 WHERE order_nr = '200000323' AND sku = 'BA171TB36LBBPEAMZ-7487';
UPDATE tbl_order_detail t SET cost_pet = 7, costo_after_vat=6 WHERE order_nr = '200000323' AND sku = 'BA211TB90KYZPEAMZ-7432';
UPDATE tbl_order_detail t SET cost_pet = 774, costo_after_vat=656 WHERE order_nr = '200000325' AND sku = 'GA148TB19GQIPEAMZ-4494';
UPDATE tbl_order_detail t SET cost_pet = 21, costo_after_vat=18 WHERE order_nr = '200000325' AND sku = 'GA148TB86LSJPEAMZ-7938';
UPDATE tbl_order_detail t SET cost_pet = 38, costo_after_vat=32 WHERE order_nr = '200000327' AND sku = 'BA171TB18IGRPEAMZ-5595';
UPDATE tbl_order_detail t SET cost_pet = 20, costo_after_vat=17 WHERE order_nr = '200000327' AND sku = 'RO192TB39IJSPEAMZ-5674';
UPDATE tbl_order_detail t SET cost_pet = 20, costo_after_vat=17 WHERE order_nr = '200000327' AND sku = 'RO192TB41IJQPEAMZ-5672';
UPDATE tbl_order_detail t SET cost_pet = 50, costo_after_vat=43 WHERE order_nr = '200000328' AND sku = 'GE116EL59EFGPEAMZ-2853';
UPDATE tbl_order_detail t SET cost_pet = 103, costo_after_vat=87 WHERE order_nr = '200000337' AND sku = 'IN202EL45IVAPEAMZ-5968';
UPDATE tbl_order_detail t SET cost_pet = 125, costo_after_vat=106 WHERE order_nr = '200000338' AND sku = 'OX193EL96ILJPEAMZ-5717';
UPDATE tbl_order_detail t SET cost_pet = 84, costo_after_vat=72 WHERE order_nr = '200000345' AND sku = 'MA175TB10HRPPEAMZ-5203';
UPDATE tbl_order_detail t SET cost_pet = 49, costo_after_vat=42 WHERE order_nr = '200000347' AND sku = 'MI046EL37IRMPEAMZ-5876';
UPDATE tbl_order_detail t SET cost_pet = 330, costo_after_vat=280 WHERE order_nr = '200000349' AND sku = 'OS049EL53ENEPEAMZ-3060';
UPDATE tbl_order_detail t SET cost_pet = 16, costo_after_vat=14 WHERE order_nr = '200000351' AND sku = 'KI042EL56GDJPEAMZ-4155';
UPDATE tbl_order_detail t SET cost_pet = 9, costo_after_vat=8 WHERE order_nr = '200000351' AND sku = 'KI042EL75JBOPEAMZ-6138';
UPDATE tbl_order_detail t SET cost_pet = 36, costo_after_vat=31 WHERE order_nr = '200000351' AND sku = 'LO103EL70DTHPEAMZ-2542';
UPDATE tbl_order_detail t SET cost_pet = 343, costo_after_vat=291 WHERE order_nr = '200000355' AND sku = 'PH121EL88KBZPEAMZ-6825';
UPDATE tbl_order_detail t SET cost_pet = 46, costo_after_vat=39 WHERE order_nr = '200000356' AND sku = 'TE160BO22HBTPEAMZ-4791';
UPDATE tbl_order_detail t SET cost_pet = 18, costo_after_vat=15 WHERE order_nr = '200000358' AND sku = 'GE116EL33EGGPEAMZ-2879';
UPDATE tbl_order_detail t SET cost_pet = 16, costo_after_vat=14 WHERE order_nr = '200000361' AND sku = 'KI042EL56GDJPEAMZ-4155';
UPDATE tbl_order_detail t SET cost_pet = 85, costo_after_vat=72 WHERE order_nr = '200000361' AND sku = 'PH121EL75KCMPEAMZ-6838';
UPDATE tbl_order_detail t SET cost_pet = 88, costo_after_vat=74 WHERE order_nr = '200000363' AND sku = 'DE219BO66MUBPEAMZ-8661';
UPDATE tbl_order_detail t SET cost_pet = 67, costo_after_vat=57 WHERE order_nr = '200000367' AND sku = 'CO127BO60FNVPEAMZ-3753';
UPDATE tbl_order_detail t SET cost_pet = 20, costo_after_vat=17 WHERE order_nr = '200000367' AND sku = 'PL158BO99GYUPEAMZ-4714';
UPDATE tbl_order_detail t SET cost_pet = 32, costo_after_vat=27 WHERE order_nr = '200000371' AND sku = 'FI173TB55HPWPEAMZ-5158';
UPDATE tbl_order_detail t SET cost_pet = 14, costo_after_vat=12 WHERE order_nr = '200000371' AND sku = 'NU168TB17JDUPEAMZ-6196';
UPDATE tbl_order_detail t SET cost_pet = 79, costo_after_vat=67 WHERE order_nr = '200000375' AND sku = 'BL112EL21HNIPEAMZ-5092';
UPDATE tbl_order_detail t SET cost_pet = 9, costo_after_vat=8 WHERE order_nr = '200000378' AND sku = 'RO192TB38IJTPEAMZ-5675';
UPDATE tbl_order_detail t SET cost_pet = 19, costo_after_vat=16 WHERE order_nr = '200000378' AND sku = 'RO192TB55IJCPEAMZ-5658';
UPDATE tbl_order_detail t SET cost_pet = 19, costo_after_vat=16 WHERE order_nr = '200000378' AND sku = 'RO192TB57IJAPEAMZ-5656';
UPDATE tbl_order_detail t SET cost_pet = 30, costo_after_vat=25 WHERE order_nr = '200000381' AND sku = 'BL112EL57EJEPEAMZ-2956';
UPDATE tbl_order_detail t SET cost_pet = 186, costo_after_vat=157 WHERE order_nr = '200000385' AND sku = 'JB040EL30GANPEAMZ-4083';
UPDATE tbl_order_detail t SET cost_pet = 3360, costo_after_vat=2847 WHERE order_nr = '200000386' AND sku = 'SA026EL63GGYPEAMZ-4250';
UPDATE tbl_order_detail t SET cost_pet = 179, costo_after_vat=152 WHERE order_nr = '200000387' AND sku = 'IM013EL22LQZPEAMZ-7901';
UPDATE tbl_order_detail t SET cost_pet = 24, costo_after_vat=20 WHERE order_nr = '200000391' AND sku = 'SC294EL41MREPEAMZ-8583';
UPDATE tbl_order_detail t SET cost_pet = 343, costo_after_vat=291 WHERE order_nr = '200000393' AND sku = 'SO029EL76GKHPEAMZ-4337';
UPDATE tbl_order_detail t SET cost_pet = 93, costo_after_vat=79 WHERE order_nr = '200000397' AND sku = 'MI046EL13ISKPEAMZ-5900';
UPDATE tbl_order_detail t SET cost_pet = 29, costo_after_vat=25 WHERE order_nr = '200000415' AND sku = 'KI042EL55GDKPEAMZ-4156';
UPDATE tbl_order_detail t SET cost_pet = 29, costo_after_vat=25 WHERE order_nr = '200000415' AND sku = 'KI042EL76JBNPEAMZ-6137';
UPDATE tbl_order_detail t SET cost_pet = 42, costo_after_vat=35 WHERE order_nr = '200000416' AND sku = 'HE058EL62IUJPEAMZ-5951';
UPDATE tbl_order_detail t SET cost_pet = 47, costo_after_vat=40 WHERE order_nr = '200000417' AND sku = 'GA010EL51KSUPEAMZ-7268';
UPDATE tbl_order_detail t SET cost_pet = 108, costo_after_vat=91 WHERE order_nr = '200000418' AND sku = 'PH121EL08HNVPEAMZ-5105';
UPDATE tbl_order_detail t SET cost_pet = 16, costo_after_vat=14 WHERE order_nr = '200000421' AND sku = 'DR264TB90LWBPEAMZ-8034';
UPDATE tbl_order_detail t SET cost_pet = 23, costo_after_vat=20 WHERE order_nr = '200000421' AND sku = 'DR264TB93LVYPEAMZ-8031';
UPDATE tbl_order_detail t SET cost_pet = 55, costo_after_vat=47 WHERE order_nr = '200000428' AND sku = 'IW204EL29IVQPEAMZ-5984';
UPDATE tbl_order_detail t SET cost_pet = 307, costo_after_vat=260 WHERE order_nr = '200000431' AND sku = 'OX193EL09IKWPEAMZ-5704';
UPDATE tbl_order_detail t SET cost_pet = 41, costo_after_vat=35 WHERE order_nr = '200000432' AND sku = 'WA096EL99GFOPEAMZ-4214';
UPDATE tbl_order_detail t SET cost_pet = 343, costo_after_vat=291 WHERE order_nr = '200000435' AND sku = 'PH121EL88KBZPEAMZ-6825';
UPDATE tbl_order_detail t SET cost_pet = 79, costo_after_vat=67 WHERE order_nr = '200000438' AND sku = 'BL112EL21HNIPEAMZ-5092';
UPDATE tbl_order_detail t SET cost_pet = 199, costo_after_vat=169 WHERE order_nr = '200000439' AND sku = 'RE024EL78AERPEAMZ-121';
UPDATE tbl_order_detail t SET cost_pet = 19, costo_after_vat=16 WHERE order_nr = '200000441' AND sku = 'CE262BO47LTWPEAMZ-7977';
UPDATE tbl_order_detail t SET cost_pet = 7, costo_after_vat=6 WHERE order_nr = '200000443' AND sku = 'BA211TB72JFNPEAMZ-6241';
UPDATE tbl_order_detail t SET cost_pet = 12, costo_after_vat=10 WHERE order_nr = '200000443' AND sku = 'NU168TB16JDVPEAMZ-6197';
UPDATE tbl_order_detail t SET cost_pet = 8, costo_after_vat=7 WHERE order_nr = '200000443' AND sku = 'NU168TB19MKIPEAMZ-8405';
UPDATE tbl_order_detail t SET cost_pet = 10, costo_after_vat=8 WHERE order_nr = '200000443' AND sku = 'NU168TB56JGDPEAMZ-6257';
UPDATE tbl_order_detail t SET cost_pet = 50, costo_after_vat=42 WHERE order_nr = '200000445' AND sku = 'MI046EL21ISCPEAMZ-5892';
UPDATE tbl_order_detail t SET cost_pet = 1, costo_after_vat=1 WHERE order_nr = '200000447' AND sku = 'SA026EL87GROPEAMZ-4526';
UPDATE tbl_order_detail t SET cost_pet = 687, costo_after_vat=582 WHERE order_nr = '200000455' AND sku = 'PH121EL15KEUPEAMZ-6898';
UPDATE tbl_order_detail t SET cost_pet = 26, costo_after_vat=22 WHERE order_nr = '200000461' AND sku = 'PR022EL86EPTPEAMZ-3126';
UPDATE tbl_order_detail t SET cost_pet = 52, costo_after_vat=44 WHERE order_nr = '200000461' AND sku = 'PR022EL88EPRPEAMZ-3124';
UPDATE tbl_order_detail t SET cost_pet = 21, costo_after_vat=18 WHERE order_nr = '200000465' AND sku = 'DI153BO54HMBPEAMZ-5059';
UPDATE tbl_order_detail t SET cost_pet = 70, costo_after_vat=59 WHERE order_nr = '200000466' AND sku = 'LO099EL77GRYPEAMZ-4536';
UPDATE tbl_order_detail t SET cost_pet = 18, costo_after_vat=15 WHERE order_nr = '200000466' AND sku = 'TO144BO83FYMPEAMZ-4030';
UPDATE tbl_order_detail t SET cost_pet = 5, costo_after_vat=4 WHERE order_nr = '200000469' AND sku = 'SA026EL82GRTPEAMZ-4531';
UPDATE tbl_order_detail t SET cost_pet = 125, costo_after_vat=106 WHERE order_nr = '200000471' AND sku = 'OX193EL96ILJPEAMZ-5717';
UPDATE tbl_order_detail t SET cost_pet = 103, costo_after_vat=87 WHERE order_nr = '200000478' AND sku = 'ED123BO20FDVPEAMZ-3493';
UPDATE tbl_order_detail t SET cost_pet = 62, costo_after_vat=52 WHERE order_nr = '200000478' AND sku = 'ED123BO70FBXPEAMZ-3443';
UPDATE tbl_order_detail t SET cost_pet = 34, costo_after_vat=29 WHERE order_nr = '200000481' AND sku = 'OX193EL82ILXPEAMZ-5731';
UPDATE tbl_order_detail t SET cost_pet = 194, costo_after_vat=164 WHERE order_nr = '200000481' AND sku = 'OX193EL85ILUPEAMZ-5728';
UPDATE tbl_order_detail t SET cost_pet = 29, costo_after_vat=25 WHERE order_nr = '200000485' AND sku = 'KI042EL55GDKPEAMZ-4156';
UPDATE tbl_order_detail t SET cost_pet = 36, costo_after_vat=30 WHERE order_nr = '200000485' AND sku = 'PH021EL21DVEPEAMZ-2591';
UPDATE tbl_order_detail t SET cost_pet = 159, costo_after_vat=135 WHERE order_nr = '200000486' AND sku = 'RE024EL43ACEPEAMZ-56';
UPDATE tbl_order_detail t SET cost_pet = 158, costo_after_vat=134 WHERE order_nr = '200000487' AND sku = 'AP032EL39KPKPEAMZ-7177';
UPDATE tbl_order_detail t SET cost_pet = 104, costo_after_vat=88 WHERE order_nr = '200000497' AND sku = 'SA026EL52EJJPEAMZ-2961';
UPDATE tbl_order_detail t SET cost_pet = 36, costo_after_vat=31 WHERE order_nr = '200000515' AND sku = 'PH021EL43DUIPEAMZ-2569';
UPDATE tbl_order_detail t SET cost_pet = 28, costo_after_vat=24 WHERE order_nr = '200000515' AND sku = 'WA096EL01GFMPEAMZ-4212';
UPDATE tbl_order_detail t SET cost_pet = 16, costo_after_vat=14 WHERE order_nr = '200000517' AND sku = 'KI042EL56GDJPEAMZ-4155';
UPDATE tbl_order_detail t SET cost_pet = 61, costo_after_vat=52 WHERE order_nr = '200000517' AND sku = 'PR022EL91EPOPEAMZ-3121';
UPDATE tbl_order_detail t SET cost_pet = 42, costo_after_vat=35 WHERE order_nr = '200000525' AND sku = 'HE058EL61IUKPEAMZ-5952';
UPDATE tbl_order_detail t SET cost_pet = 30, costo_after_vat=26 WHERE order_nr = '200000525' AND sku = 'TA100EL93KBUPEAMZ-6820';
UPDATE tbl_order_detail t SET cost_pet = 143, costo_after_vat=121 WHERE order_nr = '200000527' AND sku = 'FU038EL21GAWPEAMZ-4092';
UPDATE tbl_order_detail t SET cost_pet = 32, costo_after_vat=27 WHERE order_nr = '200000535' AND sku = 'FI173TB51HQAPEAMZ-5162';
UPDATE tbl_order_detail t SET cost_pet = 32, costo_after_vat=27 WHERE order_nr = '200000535' AND sku = 'FI173TB55HPWPEAMZ-5158';
UPDATE tbl_order_detail t SET cost_pet = 19, costo_after_vat=16 WHERE order_nr = '200000535' AND sku = 'FI173TB57HPUPEAMZ-5156';
UPDATE tbl_order_detail t SET cost_pet = 214, costo_after_vat=182 WHERE order_nr = '200000539' AND sku = 'MO115EL74EINPEAMZ-2939';
UPDATE tbl_order_detail t SET cost_pet = 47, costo_after_vat=40 WHERE order_nr = '200000541' AND sku = 'KL124EL62GGZPEAMZ-4251';
UPDATE tbl_order_detail t SET cost_pet = 11, costo_after_vat=9 WHERE order_nr = '200000543' AND sku = 'CR181EL48HTZPEAMZ-5265';
UPDATE tbl_order_detail t SET cost_pet = 111, costo_after_vat=94 WHERE order_nr = '200000545' AND sku = 'PH121EL82KCFPEAMZ-6831';
UPDATE tbl_order_detail t SET cost_pet = 343, costo_after_vat=291 WHERE order_nr = '200000547' AND sku = 'PH121EL41KDUPEAMZ-6872';
UPDATE tbl_order_detail t SET cost_pet = 50, costo_after_vat=42 WHERE order_nr = '200000551' AND sku = 'WU146BO10GIZPEAMZ-4303';
UPDATE tbl_order_detail t SET cost_pet = 120, costo_after_vat=102 WHERE order_nr = '200000551' AND sku = 'WU146BO26GIJPEAMZ-4287';
UPDATE tbl_order_detail t SET cost_pet = 17, costo_after_vat=14 WHERE order_nr = '200000555' AND sku = 'PL158BO70HLLPEAMZ-5043';
UPDATE tbl_order_detail t SET cost_pet = 70, costo_after_vat=59 WHERE order_nr = '200000558' AND sku = 'PL234BO52JZNPEAMZ-6761';
UPDATE tbl_order_detail t SET cost_pet = 343, costo_after_vat=291 WHERE order_nr = '200000561' AND sku = 'PH121EL88KBZPEAMZ-6825';
UPDATE tbl_order_detail t SET cost_pet = 19, costo_after_vat=16 WHERE order_nr = '200000563' AND sku = 'AV162TB34HFDPEAMZ-4879';
UPDATE tbl_order_detail t SET cost_pet = 29, costo_after_vat=25 WHERE order_nr = '200000563' AND sku = 'AV162TB38HEZPEAMZ-4875';
UPDATE tbl_order_detail t SET cost_pet = 46, costo_after_vat=39 WHERE order_nr = '200000575' AND sku = 'BL131EL24EOHPEAMZ-3088';
UPDATE tbl_order_detail t SET cost_pet = 188, costo_after_vat=160 WHERE order_nr = '200000575' AND sku = 'OS049EL27EOEPEAMZ-3086';
UPDATE tbl_order_detail t SET cost_pet = 74, costo_after_vat=63 WHERE order_nr = '200000575' AND sku = 'RE024EL72EQHPEAMZ-3140';
UPDATE tbl_order_detail t SET cost_pet = 101, costo_after_vat=86 WHERE order_nr = '200000576' AND sku = 'SE163EL07HKAPEAMZ-5006';
UPDATE tbl_order_detail t SET cost_pet = 374, costo_after_vat=317 WHERE order_nr = '200000576' AND sku = 'SE163EL99HKIPEAMZ-5014';
UPDATE tbl_order_detail t SET cost_pet = 1517, costo_after_vat=1286 WHERE order_nr = '200000578' AND sku = 'AP032EL15KQIPEAMZ-7200';
UPDATE tbl_order_detail t SET cost_pet = 14, costo_after_vat=12 WHERE order_nr = '200000579' AND sku = 'RO192TB24IKHPEAMZ-5689';
UPDATE tbl_order_detail t SET cost_pet = 28, costo_after_vat=24 WHERE order_nr = '200000587' AND sku = 'PH021EL29DUWPEAMZ-2583';
UPDATE tbl_order_detail t SET cost_pet = 29, costo_after_vat=25 WHERE order_nr = '200000588' AND sku = 'KI042EL76JBNPEAMZ-6137';
UPDATE tbl_order_detail t SET cost_pet = 58, costo_after_vat=50 WHERE order_nr = '200000593' AND sku = 'FI173TB73HPEPEAMZ-5140';
UPDATE tbl_order_detail t SET cost_pet = 2415, costo_after_vat=2047 WHERE order_nr = '200000595' AND sku = 'SA026EL64GGXPEAMZ-4249';
UPDATE tbl_order_detail t SET cost_pet = 223, costo_after_vat=189 WHERE order_nr = '200000599' AND sku = 'SO029EL56GLBPEAMZ-4357';
UPDATE tbl_order_detail t SET cost_pet = 19, costo_after_vat=16 WHERE order_nr = '200000611' AND sku = 'SC294EL31MROPEAMZ-8593';
UPDATE tbl_order_detail t SET cost_pet = 49, costo_after_vat=42 WHERE order_nr = '200000615' AND sku = 'LO099EL76GRZPEAMZ-4537';
UPDATE tbl_order_detail t SET cost_pet = 479, costo_after_vat=406 WHERE order_nr = '200000618' AND sku = 'AP032EL02KQVPEAMZ-7213';
UPDATE tbl_order_detail t SET cost_pet = 20, costo_after_vat=17 WHERE order_nr = '200000621' AND sku = 'AV162TB44HETPEAMZ-4869';
UPDATE tbl_order_detail t SET cost_pet = 40, costo_after_vat=34 WHERE order_nr = '200000621' AND sku = 'ED123BO75FBSPEAMZ-3438';
UPDATE tbl_order_detail t SET cost_pet = 26, costo_after_vat=22 WHERE order_nr = '200000621' AND sku = 'FI173TB75HPCPEAMZ-5138';
UPDATE tbl_order_detail t SET cost_pet = 32, costo_after_vat=27 WHERE order_nr = '200000623' AND sku = 'DR264TB12LVFPEAMZ-8012';
UPDATE tbl_order_detail t SET cost_pet = 17, costo_after_vat=15 WHERE order_nr = '200000623' AND sku = 'DR264TB74LWRPEAMZ-8050';
UPDATE tbl_order_detail t SET cost_pet = 108, costo_after_vat=91 WHERE order_nr = '200000625' AND sku = 'PH121EL09HNUPEAMZ-5104';
UPDATE tbl_order_detail t SET cost_pet = 16, costo_after_vat=13 WHERE order_nr = '200000627' AND sku = 'AV162TB00HCPPEAMZ-4813';
UPDATE tbl_order_detail t SET cost_pet = 22, costo_after_vat=19 WHERE order_nr = '200000627' AND sku = 'FI173TB34HQRPEAMZ-5179';
UPDATE tbl_order_detail t SET cost_pet = 26, costo_after_vat=22 WHERE order_nr = '200000627' AND sku = 'FI173TB56HPVPEAMZ-5157';
UPDATE tbl_order_detail t SET cost_pet = 12, costo_after_vat=10 WHERE order_nr = '200000627' AND sku = 'RO192TB30IKBPEAMZ-5683';
UPDATE tbl_order_detail t SET cost_pet = 774, costo_after_vat=656 WHERE order_nr = '200000636' AND sku = 'GA148TB19GQIPEAMZ-4494';
UPDATE tbl_order_detail t SET cost_pet = 430, costo_after_vat=364 WHERE order_nr = '200000638' AND sku = 'SA026EL76EILPEAMZ-2937';
UPDATE tbl_order_detail t SET cost_pet = 13, costo_after_vat=11 WHERE order_nr = '200000641' AND sku = 'AL241EL17JHQPEAMZ-6296';
UPDATE tbl_order_detail t SET cost_pet = 17, costo_after_vat=15 WHERE order_nr = '200000641' AND sku = 'AR252EL33KLUPEAMZ-7080';
UPDATE tbl_order_detail t SET cost_pet = 8, costo_after_vat=6 WHERE order_nr = '200000641' AND sku = 'CA036EL67LDSPEAMZ-7556';
UPDATE tbl_order_detail t SET cost_pet = 32, costo_after_vat=27 WHERE order_nr = '200000645' AND sku = 'DE219BO58JNTPEAMZ-6455';
UPDATE tbl_order_detail t SET cost_pet = 34, costo_after_vat=29 WHERE order_nr = '200000647' AND sku = 'OX193EL82ILXPEAMZ-5731';
UPDATE tbl_order_detail t SET cost_pet = 26, costo_after_vat=22 WHERE order_nr = '200000648' AND sku = 'FI173TB54HPXPEAMZ-5159';
UPDATE tbl_order_detail t SET cost_pet = 26, costo_after_vat=22 WHERE order_nr = '200000648' AND sku = 'FI173TB75HPCPEAMZ-5138';
UPDATE tbl_order_detail t SET cost_pet = 425, costo_after_vat=360 WHERE order_nr = '200000655' AND sku = 'TF119EL07EOYPEAMZ-3106';
UPDATE tbl_order_detail t SET cost_pet = 61, costo_after_vat=52 WHERE order_nr = '200000657' AND sku = 'PR022EL91EPOPEAMZ-3121';
UPDATE tbl_order_detail t SET cost_pet = 60, costo_after_vat=51 WHERE order_nr = '200000663' AND sku = 'KL124EL59GHCPEAMZ-4254';
UPDATE tbl_order_detail t SET cost_pet = 85, costo_after_vat=72 WHERE order_nr = '200000665' AND sku = 'PH121EL81KGCPEAMZ-6932';
UPDATE tbl_order_detail t SET cost_pet = 3, costo_after_vat=2 WHERE order_nr = '200000666' AND sku = 'SA026EL84GRRPEAMZ-4529';
UPDATE tbl_order_detail t SET cost_pet = 4587, costo_after_vat=3887 WHERE order_nr = '200000667' AND sku = 'AP032EL53KOWPEAMZ-7216';
UPDATE tbl_order_detail t SET cost_pet = 35, costo_after_vat=30 WHERE order_nr = '200000668' AND sku = 'CO215BO12JLRPEAMZ-6401';
UPDATE tbl_order_detail t SET cost_pet = 8, costo_after_vat=7 WHERE order_nr = '200000668' AND sku = 'RO192TB50IJHPEAMZ-5663';
UPDATE tbl_order_detail t SET cost_pet = 18, costo_after_vat=15 WHERE order_nr = '200000668' AND sku = 'TO144BO83FYMPEAMZ-4030';
UPDATE tbl_order_detail t SET cost_pet = 317, costo_after_vat=268 WHERE order_nr = '200000671' AND sku = 'AV162TB95HCUPEAMZ-4818';
UPDATE tbl_order_detail t SET cost_pet = 16, costo_after_vat=14 WHERE order_nr = '200000677' AND sku = 'KI042EL56GDJPEAMZ-4155';
UPDATE tbl_order_detail t SET cost_pet = 7970, costo_after_vat=6754 WHERE order_nr = '200000678' AND sku = 'LG018EL41KXAPEAMZ-7381';
UPDATE tbl_order_detail t SET cost_pet = 194, costo_after_vat=164 WHERE order_nr = '200000681' AND sku = 'OX193EL85ILUPEAMZ-5728';
UPDATE tbl_order_detail t SET cost_pet = 46, costo_after_vat=39 WHERE order_nr = '200000685' AND sku = 'TE160BO22HBTPEAMZ-4791';
UPDATE tbl_order_detail t SET cost_pet = 12, costo_after_vat=10 WHERE order_nr = '200000686' AND sku = 'FO118BO66FVHPEAMZ-3947';
UPDATE tbl_order_detail t SET cost_pet = 5, costo_after_vat=4 WHERE order_nr = '200000686' AND sku = 'FO118BO88FULPEAMZ-3925';
UPDATE tbl_order_detail t SET cost_pet = 7, costo_after_vat=6 WHERE order_nr = '200000697' AND sku = 'SA026EL81GRUPEAMZ-4532';
UPDATE tbl_order_detail t SET cost_pet = 54, costo_after_vat=46 WHERE order_nr = '200000699' AND sku = 'OS049EL19EGUPEAMZ-2894';
UPDATE tbl_order_detail t SET cost_pet = 601, costo_after_vat=509 WHERE order_nr = '200000711' AND sku = 'SO029EL70GKNPEAMZ-4343';
UPDATE tbl_order_detail t SET cost_pet = 118, costo_after_vat=100 WHERE order_nr = '200000715' AND sku = 'IL226BO75JUUPEAMZ-6638';
UPDATE tbl_order_detail t SET cost_pet = 1251, costo_after_vat=1060 WHERE order_nr = '200000717' AND sku = 'CA035EL36GEDPEAMZ-4177';
UPDATE tbl_order_detail t SET cost_pet = 29, costo_after_vat=25 WHERE order_nr = '200000721' AND sku = 'PL158BO66MERPEAMZ-8258';
UPDATE tbl_order_detail t SET cost_pet = 128, costo_after_vat=108 WHERE order_nr = '200000723' AND sku = 'BL131EL14EORPEAMZ-3098';
UPDATE tbl_order_detail t SET cost_pet = 34, costo_after_vat=28 WHERE order_nr = '200000728' AND sku = 'TA119EL69DXEPEAMZ-2643';
UPDATE tbl_order_detail t SET cost_pet = 29, costo_after_vat=25 WHERE order_nr = '200000735' AND sku = 'KI042EL78JBLPEAMZ-6135';
UPDATE tbl_order_detail t SET cost_pet = 307, costo_after_vat=260 WHERE order_nr = '200000737' AND sku = 'AP032EL27KPWPEAMZ-7189';
UPDATE tbl_order_detail t SET cost_pet = 16, costo_after_vat=14 WHERE order_nr = '200000738' AND sku = 'KI042EL51GDOPEAMZ-4160';
UPDATE tbl_order_detail t SET cost_pet = 29, costo_after_vat=25 WHERE order_nr = '200000738' AND sku = 'KI042EL55GDKPEAMZ-4156';
UPDATE tbl_order_detail t SET cost_pet = 16, costo_after_vat=14 WHERE order_nr = '200000738' AND sku = 'KI042EL56GDJPEAMZ-4155';
UPDATE tbl_order_detail t SET cost_pet = 29, costo_after_vat=24 WHERE order_nr = '200000738' AND sku = 'KI042EL73JBQPEAMZ-6140';
UPDATE tbl_order_detail t SET cost_pet = 11, costo_after_vat=9 WHERE order_nr = '200000739' AND sku = 'CR181EL48HTZPEAMZ-5265';
UPDATE tbl_order_detail t SET cost_pet = 7, costo_after_vat=6 WHERE order_nr = '200000739' AND sku = 'CR181EL55HTSPEAMZ-5258';
UPDATE tbl_order_detail t SET cost_pet = 33, costo_after_vat=28 WHERE order_nr = '200000739' AND sku = 'SE143BO43FWEPEAMZ-3970';
UPDATE tbl_order_detail t SET cost_pet = 343, costo_after_vat=291 WHERE order_nr = '200000741' AND sku = 'PH121EL88KBZPEAMZ-6825';
UPDATE tbl_order_detail t SET cost_pet = 54, costo_after_vat=46 WHERE order_nr = '200000743' AND sku = 'GE116EL56EFJPEAMZ-2856';
UPDATE tbl_order_detail t SET cost_pet = 29, costo_after_vat=24 WHERE order_nr = '200000743' AND sku = 'KI042EL73JBQPEAMZ-6140';
UPDATE tbl_order_detail t SET cost_pet = 110, costo_after_vat=93 WHERE order_nr = '200000743' AND sku = 'SA026EL16HNNPEAMZ-5097';
UPDATE tbl_order_detail t SET cost_pet = 210, costo_after_vat=178 WHERE order_nr = '200000743' AND sku = 'ZO116EL69EISPEAMZ-2944';
UPDATE tbl_order_detail t SET cost_pet = 98, costo_after_vat=83 WHERE order_nr = '200000745' AND sku = 'MI046EL35IROPEAMZ-5878';
UPDATE tbl_order_detail t SET cost_pet = 11, costo_after_vat=9 WHERE order_nr = '200000755' AND sku = 'CR181EL48HTZPEAMZ-5265';
UPDATE tbl_order_detail t SET cost_pet = 14, costo_after_vat=12 WHERE order_nr = '200000755' AND sku = 'RO192TB24IKHPEAMZ-5689';
UPDATE tbl_order_detail t SET cost_pet = 29, costo_after_vat=25 WHERE order_nr = '200000758' AND sku = 'KI042EL55GDKPEAMZ-4156';
UPDATE tbl_order_detail t SET cost_pet = 13, costo_after_vat=11 WHERE order_nr = '200000758' AND sku = 'KI042EL74JBPPEAMZ-6139';
UPDATE tbl_order_detail t SET cost_pet = 50, costo_after_vat=42 WHERE order_nr = '200000761' AND sku = 'KL124EL89FMSPEAMZ-3724';
UPDATE tbl_order_detail t SET cost_pet = 2415, costo_after_vat=2047 WHERE order_nr = '200000763' AND sku = 'SA026EL64GGXPEAMZ-4249';
UPDATE tbl_order_detail t SET cost_pet = 1079, costo_after_vat=914 WHERE order_nr = '200000763' AND sku = 'SO029EL94GNLPEAMZ-4419';
UPDATE tbl_order_detail t SET cost_pet = 120, costo_after_vat=101 WHERE order_nr = '200000767' AND sku = 'PH121EL57KHAPEAMZ-6956';
UPDATE tbl_order_detail t SET cost_pet = 165, costo_after_vat=140 WHERE order_nr = '200000769' AND sku = 'PH121EL90KBXPEAMZ-6823';
UPDATE tbl_order_detail t SET cost_pet = 7, costo_after_vat=6 WHERE order_nr = '200000771' AND sku = 'BA211TB89JEWPEAMZ-6224';
UPDATE tbl_order_detail t SET cost_pet = 39, costo_after_vat=33 WHERE order_nr = '200000771' AND sku = 'FI173TB48HQDPEAMZ-5165';
UPDATE tbl_order_detail t SET cost_pet = 26, costo_after_vat=22 WHERE order_nr = '200000771' AND sku = 'FI173TB56HPVPEAMZ-5157';
UPDATE tbl_order_detail t SET cost_pet = 84, costo_after_vat=71 WHERE order_nr = '200000776' AND sku = 'FI173TB72HPFPEAMZ-5141';
UPDATE tbl_order_detail t SET cost_pet = 32, costo_after_vat=27 WHERE order_nr = '200000777' AND sku = 'DE219BO58JNTPEAMZ-6455';
UPDATE tbl_order_detail t SET cost_pet = 24, costo_after_vat=21 WHERE order_nr = '200000777' AND sku = 'DE219BO91JMMPEAMZ-6422';
UPDATE tbl_order_detail t SET cost_pet = 91, costo_after_vat=77 WHERE order_nr = '200000779' AND sku = 'FI173TB38HQNPEAMZ-5175';
UPDATE tbl_order_detail t SET cost_pet = 39, costo_after_vat=33 WHERE order_nr = '200000779' AND sku = 'FI173TB50HQBPEAMZ-5163';
UPDATE tbl_order_detail t SET cost_pet = 129, costo_after_vat=109 WHERE order_nr = '200000781' AND sku = 'EL008EL81EXQPEAMZ-3330';
UPDATE tbl_order_detail t SET cost_pet = 14, costo_after_vat=12 WHERE order_nr = '200000787' AND sku = 'PR143EL74FFPPEAMZ-3537';
UPDATE tbl_order_detail t SET cost_pet = 44, costo_after_vat=38 WHERE order_nr = '200000787' AND sku = 'VA145EL14FEBPEAMZ-3497';
UPDATE tbl_order_detail t SET cost_pet = 60, costo_after_vat=50 WHERE order_nr = '200000788' AND sku = 'EV222BO13JTIPEAMZ-6600';
UPDATE tbl_order_detail t SET cost_pet = 284, costo_after_vat=240 WHERE order_nr = '200000791' AND sku = 'MO115EL03MSQPEAMZ-8621';
UPDATE tbl_order_detail t SET cost_pet = 7, costo_after_vat=6 WHERE order_nr = '200000793' AND sku = 'BA211TB85JFAPEAMZ-6228';
UPDATE tbl_order_detail t SET cost_pet = 27, costo_after_vat=23 WHERE order_nr = '200000793' AND sku = 'OX193EL62IMRPEAMZ-5751';
UPDATE tbl_order_detail t SET cost_pet = 40, costo_after_vat=34 WHERE order_nr = '200000793' AND sku = 'SC294EL25MRUPEAMZ-8599';
UPDATE tbl_order_detail t SET cost_pet = 7, costo_after_vat=6 WHERE order_nr = '200000795' AND sku = 'BA211TB85JFAPEAMZ-6228';
UPDATE tbl_order_detail t SET cost_pet = 13, costo_after_vat=11 WHERE order_nr = '200000795' AND sku = 'KI042EL74JBPPEAMZ-6139';
UPDATE tbl_order_detail t SET cost_pet = 143, costo_after_vat=121 WHERE order_nr = '200000797' AND sku = 'FU038EL21GAWPEAMZ-4092';
UPDATE tbl_order_detail t SET cost_pet = 30, costo_after_vat=26 WHERE order_nr = '200000811' AND sku = 'TA100EL93KBUPEAMZ-6820';
UPDATE tbl_order_detail t SET cost_pet = 187, costo_after_vat=158 WHERE order_nr = '200000815' AND sku = 'KE269EL58LLTPEAMZ-7765';
UPDATE tbl_order_detail t SET cost_pet = 159, costo_after_vat=135 WHERE order_nr = '200000825' AND sku = 'VE126EL98ISZPEAMZ-5915';
UPDATE tbl_order_detail t SET cost_pet = 1860, costo_after_vat=1576 WHERE order_nr = '200000827' AND sku = 'AP032EL54KOVPEAMZ-7163';
UPDATE tbl_order_detail t SET cost_pet = 29, costo_after_vat=25 WHERE order_nr = '200000828' AND sku = 'KI042EL55GDKPEAMZ-4156';
UPDATE tbl_order_detail t SET cost_pet = 29, costo_after_vat=24 WHERE order_nr = '200000828' AND sku = 'KI042EL73JBQPEAMZ-6140';
UPDATE tbl_order_detail t SET cost_pet = 29, costo_after_vat=25 WHERE order_nr = '200000828' AND sku = 'KI042EL76JBNPEAMZ-6137';
UPDATE tbl_order_detail t SET cost_pet = 1251, costo_after_vat=1060 WHERE order_nr = '200000836' AND sku = 'CA035EL36GEDPEAMZ-4177';
UPDATE tbl_order_detail t SET cost_pet = 128, costo_after_vat=109 WHERE order_nr = '200000837' AND sku = 'PH121EL83KGAPEAMZ-6930';
UPDATE tbl_order_detail t SET cost_pet = 58, costo_after_vat=50 WHERE order_nr = '200000838' AND sku = 'MA175TB12HRNPEAMZ-5201';
UPDATE tbl_order_detail t SET cost_pet = 32, costo_after_vat=27 WHERE order_nr = '200000841' AND sku = 'FI173TB51HQAPEAMZ-5162';
UPDATE tbl_order_detail t SET cost_pet = 26, costo_after_vat=22 WHERE order_nr = '200000841' AND sku = 'FI173TB52HPZPEAMZ-5161';
UPDATE tbl_order_detail t SET cost_pet = 26, costo_after_vat=22 WHERE order_nr = '200000841' AND sku = 'FI173TB56HPVPEAMZ-5157';
UPDATE tbl_order_detail t SET cost_pet = 3, costo_after_vat=2 WHERE order_nr = '200000845' AND sku = 'SA026EL84GRRPEAMZ-4529';
UPDATE tbl_order_detail t SET cost_pet = 7, costo_after_vat=6 WHERE order_nr = '200000847' AND sku = 'BA211TB05JEGPEAMZ-6208';
UPDATE tbl_order_detail t SET cost_pet = 7, costo_after_vat=6 WHERE order_nr = '200000847' AND sku = 'BA211TB80JFFPEAMZ-6233';
UPDATE tbl_order_detail t SET cost_pet = 39, costo_after_vat=33 WHERE order_nr = '200000847' AND sku = 'FI173TB50HQBPEAMZ-5163';
UPDATE tbl_order_detail t SET cost_pet = 179, costo_after_vat=152 WHERE order_nr = '200000855' AND sku = 'IM013EL22LQZPEAMZ-7901';
UPDATE tbl_order_detail t SET cost_pet = 603, costo_after_vat=511 WHERE order_nr = '200000856' AND sku = 'SE163EL00HKHPEAMZ-5013';
UPDATE tbl_order_detail t SET cost_pet = 57, costo_after_vat=49 WHERE order_nr = '200000857' AND sku = 'AP032EL81KNUPEAMZ-7136';
UPDATE tbl_order_detail t SET cost_pet = 41, costo_after_vat=35 WHERE order_nr = '200000869' AND sku = 'DE219BO62JRLPEAMZ-6551';
UPDATE tbl_order_detail t SET cost_pet = 307, costo_after_vat=260 WHERE order_nr = '200000871' AND sku = 'OX193EL09IKWPEAMZ-5704';
UPDATE tbl_order_detail t SET cost_pet = 1699, costo_after_vat=1440 WHERE order_nr = '200000878' AND sku = 'SA026EL85GRQPEAMZ-4528';
UPDATE tbl_order_detail t SET cost_pet = 101, costo_after_vat=85 WHERE order_nr = '200000881' AND sku = 'TH052EL33INUPEAMZ-5780';
UPDATE tbl_order_detail t SET cost_pet = 14, costo_after_vat=12 WHERE order_nr = '200000887' AND sku = 'RO192TB26IKFPEAMZ-5687';
UPDATE tbl_order_detail t SET cost_pet = 19, costo_after_vat=16 WHERE order_nr = '200000887' AND sku = 'RO192TB57IJAPEAMZ-5656';
UPDATE tbl_order_detail t SET cost_pet = 57, costo_after_vat=49 WHERE order_nr = '200000895' AND sku = 'AP032EL81KNUPEAMZ-7136';
UPDATE tbl_order_detail t SET cost_pet = 3723, costo_after_vat=3155 WHERE order_nr = '200000897' AND sku = 'AP032EL99KNCPEAMZ-7118';
UPDATE tbl_order_detail t SET cost_pet = 55, costo_after_vat=47 WHERE order_nr = '200000899' AND sku = 'CO216BO08JLVPEAMZ-6405';
UPDATE tbl_order_detail t SET cost_pet = 32, costo_after_vat=28 WHERE order_nr = '200000912' AND sku = 'FO118BO35EVOPEAMZ-3278';
UPDATE tbl_order_detail t SET cost_pet = 27, costo_after_vat=23 WHERE order_nr = '200000915' AND sku = 'ED154BO40GXFPEAMZ-4673';
UPDATE tbl_order_detail t SET cost_pet = 2738, costo_after_vat=2320 WHERE order_nr = '200000917' AND sku = 'CA035EL71GCUPEAMZ-4142';
UPDATE tbl_order_detail t SET cost_pet = 150, costo_after_vat=127 WHERE order_nr = '200000918' AND sku = 'SE163EL89HKSPEAMZ-5024';
UPDATE tbl_order_detail t SET cost_pet = 52, costo_after_vat=44 WHERE order_nr = '200000921' AND sku = 'TH052EL95IPGPEAMZ-5818';
UPDATE tbl_order_detail t SET cost_pet = 32, costo_after_vat=27 WHERE order_nr = '200000928' AND sku = 'GE116EL71EEUPEAMZ-2841';
UPDATE tbl_order_detail t SET cost_pet = 13, costo_after_vat=11 WHERE order_nr = '200000928' AND sku = 'MA175TB23HRCPEAMZ-5190';
UPDATE tbl_order_detail t SET cost_pet = 16, costo_after_vat=14 WHERE order_nr = '200000931' AND sku = 'DR264TB90LWBPEAMZ-8034';
UPDATE tbl_order_detail t SET cost_pet = 23, costo_after_vat=20 WHERE order_nr = '200000931' AND sku = 'DR264TB93LVYPEAMZ-8031';
UPDATE tbl_order_detail t SET cost_pet = 10, costo_after_vat=8 WHERE order_nr = '200000931' AND sku = 'NU168TB59JGAPEAMZ-6254';
UPDATE tbl_order_detail t SET cost_pet = 1540, costo_after_vat=1305 WHERE order_nr = '200000938' AND sku = 'LG018EL44KWXPEAMZ-7378';
UPDATE tbl_order_detail t SET cost_pet = 214, costo_after_vat=182 WHERE order_nr = '200000941' AND sku = 'MO115EL74EINPEAMZ-2939';
UPDATE tbl_order_detail t SET cost_pet = 111, costo_after_vat=94 WHERE order_nr = '200000947' AND sku = 'MI046EL15ISIPEAMZ-5898';
UPDATE tbl_order_detail t SET cost_pet = 344, costo_after_vat=292 WHERE order_nr = '200000955' AND sku = 'MI046EL38IRLPEAMZ-5875';
UPDATE tbl_order_detail t SET cost_pet = 343, costo_after_vat=291 WHERE order_nr = '200000957' AND sku = 'PH121EL88KBZPEAMZ-6825';
UPDATE tbl_order_detail t SET cost_pet = 7, costo_after_vat=6 WHERE order_nr = '200000958' AND sku = 'BA211TB80JFFPEAMZ-6233';
UPDATE tbl_order_detail t SET cost_pet = 7, costo_after_vat=6 WHERE order_nr = '200000958' AND sku = 'BA211TB98JENPEAMZ-6215';
UPDATE tbl_order_detail t SET cost_pet = 19, costo_after_vat=16 WHERE order_nr = '200000958' AND sku = 'FI173TB57HPUPEAMZ-5156';
UPDATE tbl_order_detail t SET cost_pet = 12, costo_after_vat=10 WHERE order_nr = '200000958' AND sku = 'NU168TB54JCJPEAMZ-6159';
UPDATE tbl_order_detail t SET cost_pet = 48, costo_after_vat=40 WHERE order_nr = '200000965' AND sku = 'CR181EL57HTQPEAMZ-5256';
UPDATE tbl_order_detail t SET cost_pet = 65, costo_after_vat=55 WHERE order_nr = '200000965' AND sku = 'MA175TB18HRHPEAMZ-5195';
UPDATE tbl_order_detail t SET cost_pet = 12, costo_after_vat=10 WHERE order_nr = '200000965' AND sku = 'RO192TB28IKDPEAMZ-5685';
UPDATE tbl_order_detail t SET cost_pet = 12, costo_after_vat=10 WHERE order_nr = '200000965' AND sku = 'RO192TB31IKAPEAMZ-5682';
UPDATE tbl_order_detail t SET cost_pet = 27, costo_after_vat=23 WHERE order_nr = '200000971' AND sku = 'PL158BO74GZTPEAMZ-4739';
UPDATE tbl_order_detail t SET cost_pet = 23, costo_after_vat=20 WHERE order_nr = '200000975' AND sku = 'MP128BO08FPVPEAMZ-3805';
UPDATE tbl_order_detail t SET cost_pet = 14, costo_after_vat=12 WHERE order_nr = '200000978' AND sku = 'RO192TB25IKGPEAMZ-5688';
UPDATE tbl_order_detail t SET cost_pet = 12, costo_after_vat=10 WHERE order_nr = '200000978' AND sku = 'RO192TB29IKCPEAMZ-5684';
UPDATE tbl_order_detail t SET cost_pet = 12, costo_after_vat=10 WHERE order_nr = '200000978' AND sku = 'RO192TB31IKAPEAMZ-5682';
UPDATE tbl_order_detail t SET cost_pet = 200, costo_after_vat=170 WHERE order_nr = '200000981' AND sku = 'SE163EL96HKLPEAMZ-5017';
UPDATE tbl_order_detail t SET cost_pet = 99, costo_after_vat=84 WHERE order_nr = '200000983' AND sku = 'TA119EL89DWKPEAMZ-2623';
UPDATE tbl_order_detail t SET cost_pet = 103, costo_after_vat=87 WHERE order_nr = '200000985' AND sku = 'AL208EL98IWVPEAMZ-6015';
UPDATE tbl_order_detail t SET cost_pet = 50, costo_after_vat=42 WHERE order_nr = '200000986' AND sku = 'WU146BO10GIZPEAMZ-4303';
UPDATE tbl_order_detail t SET cost_pet = 50, costo_after_vat=42 WHERE order_nr = '200000986' AND sku = 'WU146BO25GIKPEAMZ-4288';
UPDATE tbl_order_detail t SET cost_pet = 2048, costo_after_vat=1736 WHERE order_nr = '200000987' AND sku = 'LE060EL65JBYPEAMZ-6148';
UPDATE tbl_order_detail t SET cost_pet = 944, costo_after_vat=800 WHERE order_nr = '200000988' AND sku = 'AP032EL47KPCPEAMZ-7169';
UPDATE tbl_order_detail t SET cost_pet = 19, costo_after_vat=16 WHERE order_nr = '200000995' AND sku = 'FI173TB57HPUPEAMZ-5156';
UPDATE tbl_order_detail t SET cost_pet = 26, costo_after_vat=22 WHERE order_nr = '200000995' AND sku = 'FI173TB76HPBPEAMZ-5137';
UPDATE tbl_order_detail t SET cost_pet = 3360, costo_after_vat=2847 WHERE order_nr = '200000997' AND sku = 'SA026EL63GGYPEAMZ-4250';
UPDATE tbl_order_detail t SET cost_pet = 109, costo_after_vat=93 WHERE order_nr = '200000998' AND sku = 'RE250EL03KJCPEAMZ-7010';
UPDATE tbl_order_detail t SET cost_pet = 765, costo_after_vat=649 WHERE order_nr = '200001158' AND sku = 'SO029EL05VHQPEAMZ-15522';
UPDATE tbl_order_detail t SET cost_pet = 395, costo_after_vat=335 WHERE order_nr = '200001187' AND sku = 'HT487EL53SKSPEAMZ-12489';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200001195' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 1129, costo_after_vat=957 WHERE order_nr = '200001237' AND sku = 'TO066EL14YZLPEAMZ-18343';
UPDATE tbl_order_detail t SET cost_pet = 654, costo_after_vat=554 WHERE order_nr = '200001247' AND sku = 'AP032EL48KPBPEAMZ-7168';
UPDATE tbl_order_detail t SET cost_pet = 18, costo_after_vat=15 WHERE order_nr = '200001258' AND sku = 'GE116EL34EGFPEAMZ-2878';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200001275' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200001297' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 29, costo_after_vat=25 WHERE order_nr = '200001338' AND sku = 'KI042EL55GDKPEAMZ-4156';
UPDATE tbl_order_detail t SET cost_pet = 17, costo_after_vat=14 WHERE order_nr = '200001387' AND sku = 'KI042EL77JBMPEAMZ-6136';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200001547' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 22, costo_after_vat=18 WHERE order_nr = '200001638' AND sku = 'PH021EL31DUUPEAMZ-2581';
UPDATE tbl_order_detail t SET cost_pet = 7, costo_after_vat=6 WHERE order_nr = '200001638' AND sku = 'PH121EL04HNZPEAMZ-5109';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200001675' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 17, costo_after_vat=14 WHERE order_nr = '200001679' AND sku = 'KI042EL77JBMPEAMZ-6136';
UPDATE tbl_order_detail t SET cost_pet = 22, costo_after_vat=18 WHERE order_nr = '200001699' AND sku = 'PH021EL31DUUPEAMZ-2581';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200001718' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 107, costo_after_vat=91 WHERE order_nr = '200001736' AND sku = 'TA119EL08DVRPEAMZ-2604';
UPDATE tbl_order_detail t SET cost_pet = 2112, costo_after_vat=1790 WHERE order_nr = '200001789' AND sku = 'AP032EL19KQEPEAMZ-7196';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200001795' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 2209, costo_after_vat=1872 WHERE order_nr = '200001799' AND sku = 'SA026EL01LRUPEAMZ-7923';
UPDATE tbl_order_detail t SET cost_pet = 13, costo_after_vat=11 WHERE order_nr = '200001938' AND sku = 'KI042EL74JBPPEAMZ-6139';
UPDATE tbl_order_detail t SET cost_pet = 776, costo_after_vat=658 WHERE order_nr = '200001965' AND sku = 'AP032EL74QEFPEAMZ-10953';
UPDATE tbl_order_detail t SET cost_pet = 1670, costo_after_vat=1415 WHERE order_nr = '200001966' AND sku = 'SA026EL21RLAPEAMZ-11817';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200001995' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 13, costo_after_vat=11 WHERE order_nr = '200002165' AND sku = 'AO002ME61FRQPEAMZ-3852';
UPDATE tbl_order_detail t SET cost_pet = 776, costo_after_vat=658 WHERE order_nr = '200002175' AND sku = 'AP032EL74QEFPEAMZ-10953';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200002185' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 20, costo_after_vat=17 WHERE order_nr = '200002222' AND sku = 'PI301TB09NDYPEAMZ-8918';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200002225' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 158, costo_after_vat=134 WHERE order_nr = '200002236' AND sku = 'AP032EL31KPSPEAMZ-7185';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200002257' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200002277' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200002318' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 966, costo_after_vat=819 WHERE order_nr = '200002397' AND sku = 'TO066EL72TOTPEAMZ-13649';
UPDATE tbl_order_detail t SET cost_pet = 395, costo_after_vat=335 WHERE order_nr = '200002445' AND sku = 'HT487EL53SKSPEAMZ-12489';
UPDATE tbl_order_detail t SET cost_pet = 254, costo_after_vat=215 WHERE order_nr = '200002466' AND sku = 'LG018EL20RLBPEAMZ-11818';
UPDATE tbl_order_detail t SET cost_pet = 765, costo_after_vat=649 WHERE order_nr = '200002497' AND sku = 'SO029EL05VHQPEAMZ-15522';
UPDATE tbl_order_detail t SET cost_pet = 1313, costo_after_vat=1113 WHERE order_nr = '200002517' AND sku = 'LE060EL43DEYPEAMZ-2168';
UPDATE tbl_order_detail t SET cost_pet = 36, costo_after_vat=30 WHERE order_nr = '200002559' AND sku = 'LO103EL69DTIPEAMZ-2543';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200002577' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200002585' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 113, costo_after_vat=96 WHERE order_nr = '200002679' AND sku = 'LO103EL00DSDPEAMZ-2512';
UPDATE tbl_order_detail t SET cost_pet = 1670, costo_after_vat=1415 WHERE order_nr = '200002766' AND sku = 'SA026EL21RLAPEAMZ-11817';
UPDATE tbl_order_detail t SET cost_pet = 14, costo_after_vat=12 WHERE order_nr = '200002836' AND sku = 'GE116EL07EHGPEAMZ-2905';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200002858' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 1670, costo_after_vat=1415 WHERE order_nr = '200002966' AND sku = 'SA026EL21RLAPEAMZ-11817';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200002975' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 50, costo_after_vat=43 WHERE order_nr = '200003116' AND sku = 'GE116EL59EFGPEAMZ-2853';
UPDATE tbl_order_detail t SET cost_pet = 1066, costo_after_vat=903 WHERE order_nr = '200003137' AND sku = 'CO072EL22CYZPEAMZ-22776';
UPDATE tbl_order_detail t SET cost_pet = 17, costo_after_vat=14 WHERE order_nr = '200003228' AND sku = 'KI042EL77JBMPEAMZ-6136';
UPDATE tbl_order_detail t SET cost_pet = 7, costo_after_vat=6 WHERE order_nr = '200003228' AND sku = 'PH121EL04HNZPEAMZ-5109';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200003367' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 2659, costo_after_vat=2253 WHERE order_nr = '200003389' AND sku = 'LE060EL68JBVPEAMZ-6145';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200003418' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200003438' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200003485' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200003527' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200003575' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200003585' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200003665' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 395, costo_after_vat=335 WHERE order_nr = '200003745' AND sku = 'HT487EL53SKSPEAMZ-12489';
UPDATE tbl_order_detail t SET cost_pet = 884, costo_after_vat=749 WHERE order_nr = '200003773' AND sku = 'GE011EL89DAGPEAMZ-22808';
UPDATE tbl_order_detail t SET cost_pet = 1670, costo_after_vat=1415 WHERE order_nr = '200003849' AND sku = 'SA026EL21RLAPEAMZ-11817';
UPDATE tbl_order_detail t SET cost_pet = 307, costo_after_vat=260 WHERE order_nr = '200003965' AND sku = 'AP032EL27KPWPEAMZ-7189';
UPDATE tbl_order_detail t SET cost_pet = 99, costo_after_vat=84 WHERE order_nr = '200004129' AND sku = 'TA119EL77DWWPEAMZ-2635';
UPDATE tbl_order_detail t SET cost_pet = 265, costo_after_vat=224 WHERE order_nr = '200004249' AND sku = 'SA026EL08LRNPEAMZ-7916';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200004358' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 99, costo_after_vat=84 WHERE order_nr = '200004375' AND sku = 'TA119EL77DWWPEAMZ-2635';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200004418' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200004467' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 13, costo_after_vat=11 WHERE order_nr = '200004475' AND sku = 'KI042EL74JBPPEAMZ-6139';
UPDATE tbl_order_detail t SET cost_pet = 1012, costo_after_vat=858 WHERE order_nr = '200004659' AND sku = 'TO066EL44IRFPEAMZ-5869';
UPDATE tbl_order_detail t SET cost_pet = 765, costo_after_vat=649 WHERE order_nr = '200004688' AND sku = 'SO029EL05VHQPEAMZ-15522';
UPDATE tbl_order_detail t SET cost_pet = 13, costo_after_vat=11 WHERE order_nr = '200004749' AND sku = 'KI042EL74JBPPEAMZ-6139';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200004758' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 776, costo_after_vat=658 WHERE order_nr = '200004925' AND sku = 'AP032EL74QEFPEAMZ-10953';
UPDATE tbl_order_detail t SET cost_pet = 395, costo_after_vat=335 WHERE order_nr = '200004945' AND sku = 'HT487EL53SKSPEAMZ-12489';
UPDATE tbl_order_detail t SET cost_pet = 395, costo_after_vat=335 WHERE order_nr = '200005125' AND sku = 'HT487EL53SKSPEAMZ-12489';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200005165' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 13, costo_after_vat=11 WHERE order_nr = '200005219' AND sku = 'KI042EL74JBPPEAMZ-6139';
UPDATE tbl_order_detail t SET cost_pet = 9, costo_after_vat=8 WHERE order_nr = '200005219' AND sku = 'KI042EL75JBOPEAMZ-6138';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200005267' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200005295' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 29, costo_after_vat=25 WHERE order_nr = '200005317' AND sku = 'KI042EL76JBNPEAMZ-6137';
UPDATE tbl_order_detail t SET cost_pet = 52, costo_after_vat=44 WHERE order_nr = '200005326' AND sku = 'DE455EL10RDTPEAMZ-11627';
UPDATE tbl_order_detail t SET cost_pet = 2112, costo_after_vat=1790 WHERE order_nr = '200005339' AND sku = 'AP032EL19KQEPEAMZ-7196';
UPDATE tbl_order_detail t SET cost_pet = 52, costo_after_vat=44 WHERE order_nr = '200005447' AND sku = 'DE455EL10RDTPEAMZ-11627';
UPDATE tbl_order_detail t SET cost_pet = 776, costo_after_vat=658 WHERE order_nr = '200005485' AND sku = 'AP032EL74QEFPEAMZ-10953';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200005518' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 395, costo_after_vat=335 WHERE order_nr = '200005547' AND sku = 'HT487EL53SKSPEAMZ-12489';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200005595' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 36, costo_after_vat=30 WHERE order_nr = '200005619' AND sku = 'PH021EL32DUTPEAMZ-2580';
UPDATE tbl_order_detail t SET cost_pet = 52, costo_after_vat=44 WHERE order_nr = '200005639' AND sku = 'DE455EL10RDTPEAMZ-11627';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200005657' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 765, costo_after_vat=649 WHERE order_nr = '200005685' AND sku = 'SO029EL05VHQPEAMZ-15522';
UPDATE tbl_order_detail t SET cost_pet = 395, costo_after_vat=335 WHERE order_nr = '200005695' AND sku = 'HT487EL53SKSPEAMZ-12489';
UPDATE tbl_order_detail t SET cost_pet = 1407, costo_after_vat=1192 WHERE order_nr = '200005727' AND sku = 'TO066EL03QWIPEAMZ-11434';
UPDATE tbl_order_detail t SET cost_pet = 162, costo_after_vat=137 WHERE order_nr = '200005739' AND sku = 'HE058EL74GCRPEAMZ-4137';
UPDATE tbl_order_detail t SET cost_pet = 29, costo_after_vat=25 WHERE order_nr = '200005767' AND sku = 'KI042EL55GDKPEAMZ-4156';
UPDATE tbl_order_detail t SET cost_pet = 1565, costo_after_vat=1326 WHERE order_nr = '200005787' AND sku = 'LE060EL67CEAPEAMZ-21558';
UPDATE tbl_order_detail t SET cost_pet = 13, costo_after_vat=11 WHERE order_nr = '200005857' AND sku = 'KI042EL74JBPPEAMZ-6139';
UPDATE tbl_order_detail t SET cost_pet = 1670, costo_after_vat=1415 WHERE order_nr = '200005866' AND sku = 'SA026EL21RLAPEAMZ-11817';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200005925' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 29, costo_after_vat=24 WHERE order_nr = '200005975' AND sku = 'KI042EL73JBQPEAMZ-6140';
UPDATE tbl_order_detail t SET cost_pet = 86, costo_after_vat=73 WHERE order_nr = '200005985' AND sku = 'AP032EL67KOIPEAMZ-7150';
UPDATE tbl_order_detail t SET cost_pet = 540, costo_after_vat=458 WHERE order_nr = '200005999' AND sku = 'EP057EL18TQVPEAMZ-13719';
UPDATE tbl_order_detail t SET cost_pet = 32, costo_after_vat=27 WHERE order_nr = '200006188' AND sku = 'GE116EL09EHEPEAMZ-2903';
UPDATE tbl_order_detail t SET cost_pet = 17, costo_after_vat=14 WHERE order_nr = '200006227' AND sku = 'KI042EL77JBMPEAMZ-6136';
UPDATE tbl_order_detail t SET cost_pet = 395, costo_after_vat=335 WHERE order_nr = '200006288' AND sku = 'HT487EL53SKSPEAMZ-12489';
UPDATE tbl_order_detail t SET cost_pet = 776, costo_after_vat=658 WHERE order_nr = '200006345' AND sku = 'AP032EL74QEFPEAMZ-10953';
UPDATE tbl_order_detail t SET cost_pet = 776, costo_after_vat=658 WHERE order_nr = '200006417' AND sku = 'AP032EL74QEFPEAMZ-10953';
UPDATE tbl_order_detail t SET cost_pet = 267, costo_after_vat=226 WHERE order_nr = '200006439' AND sku = 'LG018EL54SKRPEAMZ-12488';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200006457' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 765, costo_after_vat=649 WHERE order_nr = '200006495' AND sku = 'SO029EL05VHQPEAMZ-15522';
UPDATE tbl_order_detail t SET cost_pet = 2102, costo_after_vat=1781 WHERE order_nr = '200006689' AND sku = 'TO066EL70TOVPEAMZ-13651';
UPDATE tbl_order_detail t SET cost_pet = 765, costo_after_vat=649 WHERE order_nr = '200006757' AND sku = 'SO029EL05VHQPEAMZ-15522';
UPDATE tbl_order_detail t SET cost_pet = 1407, costo_after_vat=1192 WHERE order_nr = '200006787' AND sku = 'TO066EL03QWIPEAMZ-11434';
UPDATE tbl_order_detail t SET cost_pet = 1670, costo_after_vat=1415 WHERE order_nr = '200006826' AND sku = 'SA026EL21RLAPEAMZ-11817';
UPDATE tbl_order_detail t SET cost_pet = 2145, costo_after_vat=1818 WHERE order_nr = '200006829' AND sku = 'AC089EL16MKLPEAMZ-8408';
UPDATE tbl_order_detail t SET cost_pet = 776, costo_after_vat=658 WHERE order_nr = '200006895' AND sku = 'AP032EL74QEFPEAMZ-10953';
UPDATE tbl_order_detail t SET cost_pet = 776, costo_after_vat=658 WHERE order_nr = '200006917' AND sku = 'AP032EL74QEFPEAMZ-10953';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200006918' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 776, costo_after_vat=658 WHERE order_nr = '200006945' AND sku = 'AP032EL74QEFPEAMZ-10953';
UPDATE tbl_order_detail t SET cost_pet = 29, costo_after_vat=25 WHERE order_nr = '200007188' AND sku = 'KI042EL55GDKPEAMZ-4156';
UPDATE tbl_order_detail t SET cost_pet = 1825, costo_after_vat=1547 WHERE order_nr = '200007247' AND sku = 'AP032EL75QEEPEAMZ-10952';
UPDATE tbl_order_detail t SET cost_pet = 395, costo_after_vat=335 WHERE order_nr = '200007275' AND sku = 'HT487EL53SKSPEAMZ-12489';
UPDATE tbl_order_detail t SET cost_pet = 767, costo_after_vat=650 WHERE order_nr = '200007338' AND sku = 'AS282EL87TOEPEAMZ-13634';
UPDATE tbl_order_detail t SET cost_pet = 776, costo_after_vat=658 WHERE order_nr = '200007365' AND sku = 'AP032EL74QEFPEAMZ-10953';
UPDATE tbl_order_detail t SET cost_pet = 395, costo_after_vat=335 WHERE order_nr = '200007378' AND sku = 'HT487EL53SKSPEAMZ-12489';
UPDATE tbl_order_detail t SET cost_pet = 1129, costo_after_vat=957 WHERE order_nr = '200007417' AND sku = 'TO066EL14YZLPEAMZ-18343';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200007418' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200007438' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 148, costo_after_vat=126 WHERE order_nr = '200007485' AND sku = 'AP032EL94KNHPEAMZ-7121';
UPDATE tbl_order_detail t SET cost_pet = 21, costo_after_vat=18 WHERE order_nr = '200007532' AND sku = 'AN242EL25KMCPEAMZ-7088';
UPDATE tbl_order_detail t SET cost_pet = 21, costo_after_vat=18 WHERE order_nr = '200007639' AND sku = 'GE116EL17EGWPEAMZ-2895';
UPDATE tbl_order_detail t SET cost_pet = 7, costo_after_vat=6 WHERE order_nr = '200007639' AND sku = 'PH121EL04HNZPEAMZ-5109';
UPDATE tbl_order_detail t SET cost_pet = 75, costo_after_vat=64 WHERE order_nr = '200007716' AND sku = 'DE455EL22RDHPEAMZ-11615';
UPDATE tbl_order_detail t SET cost_pet = 1670, costo_after_vat=1415 WHERE order_nr = '200007749' AND sku = 'SA026EL21RLAPEAMZ-11817';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200007758' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 776, costo_after_vat=658 WHERE order_nr = '200007775' AND sku = 'AP032EL74QEFPEAMZ-10953';
UPDATE tbl_order_detail t SET cost_pet = 36, costo_after_vat=30 WHERE order_nr = '200007799' AND sku = 'PH021EL21DVEPEAMZ-2591';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200007827' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 1670, costo_after_vat=1415 WHERE order_nr = '200007866' AND sku = 'SA026EL21RLAPEAMZ-11817';
UPDATE tbl_order_detail t SET cost_pet = 1670, costo_after_vat=1415 WHERE order_nr = '200007919' AND sku = 'SA026EL19TQUPEAMZ-13703';
UPDATE tbl_order_detail t SET cost_pet = 9, costo_after_vat=8 WHERE order_nr = '200008228' AND sku = 'KI042EL75JBOPEAMZ-6138';
UPDATE tbl_order_detail t SET cost_pet = 28, costo_after_vat=24 WHERE order_nr = '200008238' AND sku = 'GE116EL27EGMPEAMZ-2885';
UPDATE tbl_order_detail t SET cost_pet = 1816, costo_after_vat=1539 WHERE order_nr = '200008247' AND sku = 'AP032EL20KQDPEAMZ-7195';
UPDATE tbl_order_detail t SET cost_pet = 52, costo_after_vat=44 WHERE order_nr = '200008257' AND sku = 'DE455EL10RDTPEAMZ-11627';
UPDATE tbl_order_detail t SET cost_pet = 1670, costo_after_vat=1415 WHERE order_nr = '200008266' AND sku = 'SA026EL21RLAPEAMZ-11817';
UPDATE tbl_order_detail t SET cost_pet = 1313, costo_after_vat=1113 WHERE order_nr = '200008357' AND sku = 'LE060EL43DEYPEAMZ-2168';
UPDATE tbl_order_detail t SET cost_pet = 1816, costo_after_vat=1539 WHERE order_nr = '200008358' AND sku = 'AP032EL20KQDPEAMZ-7195';
UPDATE tbl_order_detail t SET cost_pet = 176, costo_after_vat=150 WHERE order_nr = '200008368' AND sku = 'AL295EL72MMDPEAMZ-8452';
UPDATE tbl_order_detail t SET cost_pet = 395, costo_after_vat=335 WHERE order_nr = '200008458' AND sku = 'HT487EL53SKSPEAMZ-12489';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200008518' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 765, costo_after_vat=649 WHERE order_nr = '200008545' AND sku = 'SO029EL05VHQPEAMZ-15522';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200008557' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200008558' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200008585' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 29, costo_after_vat=25 WHERE order_nr = '200008588' AND sku = 'KI042EL55GDKPEAMZ-4156';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200008725' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200008738' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200008757' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 395, costo_after_vat=335 WHERE order_nr = '200008797' AND sku = 'HT487EL53SKSPEAMZ-12489';
UPDATE tbl_order_detail t SET cost_pet = 29, costo_after_vat=25 WHERE order_nr = '200008859' AND sku = 'KI042EL76JBNPEAMZ-6137';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200008945' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 395, costo_after_vat=335 WHERE order_nr = '200008989' AND sku = 'HT487EL53SKSPEAMZ-12489';
UPDATE tbl_order_detail t SET cost_pet = 9, costo_after_vat=8 WHERE order_nr = '200009118' AND sku = 'KI042EL75JBOPEAMZ-6138';
UPDATE tbl_order_detail t SET cost_pet = 1670, costo_after_vat=1415 WHERE order_nr = '200009126' AND sku = 'SA026EL21RLAPEAMZ-11817';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200009127' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 1488, costo_after_vat=1261 WHERE order_nr = '200009158' AND sku = 'LE060EL08BJFPEAMZ-19953';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200009177' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200009178' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200009195' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200009255' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 1670, costo_after_vat=1415 WHERE order_nr = '200009266' AND sku = 'SA026EL21RLAPEAMZ-11817';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200009295' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 13, costo_after_vat=11 WHERE order_nr = '200009327' AND sku = 'KI042EL74JBPPEAMZ-6139';
UPDATE tbl_order_detail t SET cost_pet = 1488, costo_after_vat=1261 WHERE order_nr = '200009458' AND sku = 'LE060EL08BJFPEAMZ-19953';
UPDATE tbl_order_detail t SET cost_pet = 1417, costo_after_vat=1201 WHERE order_nr = '200009473' AND sku = 'TO066EL78WJPPEAMZ-16579';
UPDATE tbl_order_detail t SET cost_pet = 395, costo_after_vat=335 WHERE order_nr = '200009497' AND sku = 'HT487EL53SKSPEAMZ-12489';
UPDATE tbl_order_detail t SET cost_pet = 2102, costo_after_vat=1781 WHERE order_nr = '200009619' AND sku = 'TO066EL70TOVPEAMZ-13651';
UPDATE tbl_order_detail t SET cost_pet = 1565, costo_after_vat=1326 WHERE order_nr = '200009785' AND sku = 'LE060EL67CEAPEAMZ-21558';
UPDATE tbl_order_detail t SET cost_pet = 13, costo_after_vat=11 WHERE order_nr = '200009789' AND sku = 'KI042EL74JBPPEAMZ-6139';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200009795' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 765, costo_after_vat=649 WHERE order_nr = '200009828' AND sku = 'SO029EL05VHQPEAMZ-15522';
UPDATE tbl_order_detail t SET cost_pet = 1670, costo_after_vat=1415 WHERE order_nr = '200009866' AND sku = 'SA026EL21RLAPEAMZ-11817';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200009978' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 505, costo_after_vat=428 WHERE order_nr = '200009985' AND sku = 'BL112EL05KMWPEAMZ-7112';
UPDATE tbl_order_detail t SET cost_pet = 1129, costo_after_vat=957 WHERE order_nr = '200009988' AND sku = 'TO066EL14YZLPEAMZ-18343';


/*  DELETE DE MYSQL para tener data de access  */



END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `mom_net_sales_per_channel`()
BEGIN

#Fecha inicio mes actual
select @start_current_month:=cast(date_add(now(), interval - day(now())+1 day) as date);

#Fecha fin mes actual
select @end_current_month:=cast(date_add(now(), interval - 1 day) as date);

#Fecha inicio mes anterior
select @start_previous_month:=date_add(cast(date_add(now(), interval - day(now())+1 day) as date), interval - 1 month);

#Fecha fin mes anterior
select @end_previous_month:=date_add(cast(date_add(now(), interval - 1 day) as date), interval - 1 month);

delete from tbl_mom_net_sales_per_channel;

insert into tbl_mom_net_sales_per_channel(date, channel_group, netSalesPreviousMonth, netSalesCurrentMonth,month_over_month)
select @end_current_month, currentMonth.channel_group, sum(NetSalesPreviousMonth) NetSalesPreviousMonth, sum(NetSalesCurrentMonth) NetSalesCurrentMonth, 
if((sum(NetSalesCurrentMonth)/sum(NetSalesPreviousMonth))-1 is null, 0, (sum(NetSalesCurrentMonth)/sum(NetSalesPreviousMonth))-1)
from
(select channel_group, sum(paid_price_after_vat) +sum(if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat)) as NetSalesCurrentMonth
from tbl_order_detail
where oac = 1 and returned = 0 and date >= @start_current_month and date <= @end_current_month
group by channel_group) currentMonth left join
(select channel_group, sum(paid_price_after_vat) +sum(if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat)) as NetSalesPreviousMonth
from tbl_order_detail
where oac = 1 and returned = 0 and date >= @start_previous_month and date <= @end_previous_month
group by channel_group) previousMonth on
currentMonth.channel_group = previousMonth.channel_group
group by month(@start_current_month), currentMonth.channel_group;

insert into tbl_mom_net_sales_per_channel(date, channel_group, netSalesPreviousMonth, netSalesCurrentMonth,month_over_month)
select date, 'MTD',sum(NetSalesPreviousMonth), sum(NetSalesCurrentMonth), (sum(NetSalesCurrentMonth)/sum(NetSalesPreviousMonth))-1
from (
select @end_current_month date, sum(NetSalesPreviousMonth) NetSalesPreviousMonth, sum(NetSalesCurrentMonth) NetSalesCurrentMonth, 
(sum(NetSalesCurrentMonth)/sum(NetSalesPreviousMonth))-1
from
(select channel_group, sum(paid_price_after_vat) +sum(if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat)) as NetSalesCurrentMonth
from tbl_order_detail
where oac = 1 and returned = 0 and date >= @start_current_month and date <= @end_current_month
group by channel_group) currentMonth inner join
(select channel_group, sum(paid_price_after_vat) +sum(if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat)) as NetSalesPreviousMonth
from tbl_order_detail
where oac = 1 and returned = 0 and date >= @start_previous_month and date <= @end_previous_month
group by channel_group) previousMonth on
currentMonth.channel_group = previousMonth.channel_group
group by month(@start_current_month), currentMonth.channel_group) p group by date;

update tbl_mom_net_sales_per_channel
set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) where date =  @end_previous_month;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `monthly_marketing`()
BEGIN

#MONTH

DELETE FROM tbl_monthly_marketing WHERE MONTH=MONTH(NOW());

INSERT INTO tbl_monthly_marketing (month,channel) 
SELECT DISTINCT MONTH(NOW()), channel_group FROM tbl_order_detail WHERE MONTH(DATE)=MONTH(NOW());

#VISITS	

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT MONTH(date) month, reporting_channel, sum(visits) visits
FROM daily_costs_visits_per_channel
GROUP BY MONTH(date), reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.month=tbl_monthly_marketing.month 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.visits=daily_costs_visits_per_channel.visits;

#GROSS ORDERS

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT MONTH(date) month, channel_group, sum(gross_orders) gross_orders
FROM tbl_order_detail WHERE OBC = 1
GROUP BY MONTH(date), channel_group) tbl_order_detail
ON tbl_order_detail.month=tbl_monthly_marketing.month 
AND tbl_monthly_marketing.channel=tbl_order_detail.channel_group
SET tbl_monthly_marketing.gross_orders=tbl_order_detail.gross_orders;

#NET ORDERS

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT MONTH(date) month, channel_group, sum(net_orders) net_orders
FROM tbl_order_detail WHERE oac = 1 AND returned = 0
GROUP BY MONTH(date), channel_group) tbl_order_detail
ON tbl_order_detail.month=tbl_monthly_marketing.month 
AND tbl_monthly_marketing.channel=tbl_order_detail.channel_group
SET tbl_monthly_marketing.net_orders=tbl_order_detail.net_orders;

#ORDERS NET EXPECTED

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT MONTH(date) month, reporting_channel, sum(net_orders_e) net_orders_e
FROM daily_costs_visits_per_channel
GROUP BY MONTH(date), reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.month=tbl_monthly_marketing.month 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.orders_net_expected=daily_costs_visits_per_channel.net_orders_e;

#CONVERSION RATE
UPDATE tbl_monthly_marketing SET conversion_rate=IF(gross_orders/visits IS NULL, 0, gross_orders/visits);
#PENDING REVENUE

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT MONTH(date) month, channel_group, 
(SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat))/3.3 pending_sales
FROM tbl_order_detail WHERE tbl_order_detail.pending=1 
GROUP BY MONTH(date), channel_group) tbl_order_detail
ON tbl_order_detail.month=tbl_monthly_marketing.month 
SET tbl_monthly_marketing.pending_revenue=tbl_order_detail.pending_sales
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group;

#GROSS REVENUE
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT MONTH(date) month, channel_group, 
(SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat))/3.3 gross_sales
FROM tbl_order_detail WHERE tbl_order_detail.obc=1 
GROUP BY MONTH(date), channel_group) tbl_order_detail
ON tbl_order_detail.month=tbl_monthly_marketing.month 
SET tbl_monthly_marketing.gross_revenue=tbl_order_detail.gross_sales
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group;

#NET REVENUE
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT MONTH(date) month, channel_group, 
(SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat))/3.3 net_sales
FROM tbl_order_detail WHERE tbl_order_detail.oac=1 AND tbl_order_detail.returned=0  
GROUP BY MONTH(date), channel_group) tbl_order_detail
ON tbl_order_detail.month=tbl_monthly_marketing.month 
SET tbl_monthly_marketing.net_revenue=tbl_order_detail.net_sales
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group;

#NET REVENUE EXPECTED

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT MONTH(date) month, 
reporting_channel, sum(net_rev_e)/3.3 net_rev_e
FROM daily_costs_visits_per_channel
GROUP BY MONTH(date), reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.month=tbl_monthly_marketing.month 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.net_revenue_expected=daily_costs_visits_per_channel.net_rev_e;
UPDATE tbl_monthly_marketing SET net_revenue_expected = net_revenue WHERE net_revenue_expected=0;

#MONTHLY TARGET

UPDATE tbl_monthly_marketing  
INNER JOIN (SELECT MONTH(date) month, channel_group, sum(target_net_sales) monthly_target
FROM daily_targets_per_channel
GROUP BY MONTH(date), channel_group) daily_targets_per_channel
ON daily_targets_per_channel.month=tbl_monthly_marketing.month 
AND tbl_monthly_marketing.channel=daily_targets_per_channel.channel_group
SET tbl_monthly_marketing.monthly_target=daily_targets_per_channel.monthly_target;

#MONTHLY DEVIATION
UPDATE tbl_monthly_marketing 
SET 
deviation=if((net_revenue_expected/(monthly_target/(SELECT DAYOFMONTH(LAST_DAY(NOW())))*(SELECT DAY(NOW())-1))-1) IS NULL,
0,(net_revenue_expected/(monthly_target/(SELECT DAYOFMONTH(LAST_DAY(NOW())))*(SELECT DAY(NOW())-1))-1));

#AVG TICKET
UPDATE tbl_monthly_marketing SET avg_ticket=net_revenue/net_orders;

#MARKETING COST

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT MONTH(date) month, 
reporting_channel, sum(cost_local)/3.3 cost
FROM daily_costs_visits_per_channel
GROUP BY MONTH(date), reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.month=tbl_monthly_marketing.month 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.marketing_cost=daily_costs_visits_per_channel.cost;

#COST/REV
UPDATE tbl_monthly_marketing SET cost_rev=IF(marketing_cost/net_revenue_expected IS NULL, 0, marketing_cost/net_revenue_expected);

#CPO
UPDATE tbl_monthly_marketing SET cost_per_order=IF(marketing_cost/net_orders IS NULL, 0, marketing_cost/net_orders);

#CPO TARGET

UPDATE tbl_monthly_marketing  
INNER JOIN (SELECT MONTH(date) month, channel_group, MAX(target_cpo) target_cpo
FROM daily_targets_per_channel
GROUP BY MONTH(date), channel_group) daily_targets_per_channel
ON daily_targets_per_channel.month=tbl_monthly_marketing.month 
AND tbl_monthly_marketing.channel=daily_targets_per_channel.channel_group
SET tbl_monthly_marketing.cost_per_order_target=daily_targets_per_channel.target_cpo;

#CPO VS TARGET
UPDATE tbl_monthly_marketing SET cpo_vs_target_cpo=(cost_per_order/cost_per_order_target)-1;

#NEW CLIENTS
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT MONTH(date) month, channel_group, sum(new_customers) nc
FROM tbl_order_detail
GROUP BY MONTH(date), channel_group) tbl_order_detail
ON tbl_order_detail.month=tbl_monthly_marketing.month 
AND tbl_monthly_marketing.channel=tbl_order_detail.channel_group
SET tbl_monthly_marketing.new_clients=tbl_order_detail.nc;

#NEW CLIENTS EXPECTED
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT MONTH(date) month, 
reporting_channel, sum(new_customers_e) nc_e
FROM daily_costs_visits_per_channel
GROUP BY MONTH(date), reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.month=tbl_monthly_marketing.month 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.new_clientes_expected=daily_costs_visits_per_channel.nc_e;
UPDATE tbl_monthly_marketing SET new_clientes_expected=new_clients WHERE new_clientes_expected=0;

#CAC
UPDATE tbl_monthly_marketing SET client_acquisition_cost=marketing_cost/new_clientes_expected;

#PC2_COST

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT MONTH(date) month, channel_group, 
(SUM(costo_after_vat)+SUM(delivery_cost_supplier)+SUM(wh)+SUM(cs)+SUM(payment_cost)+SUM(shipping_cost))/3.3 pc2_costs
FROM tbl_order_detail WHERE tbl_order_detail.oac=1 AND tbl_order_detail.returned=0  
GROUP BY MONTH(date), channel_group) tbl_order_detail
ON tbl_order_detail.month=tbl_monthly_marketing.month 
SET tbl_monthly_marketing.pc2_costs=tbl_order_detail.pc2_costs
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group;

#PC2
UPDATE tbl_monthly_marketing SET profit_contribution_2=1-(pc2_costs/net_revenue);


#MOM
CALL mom_net_sales_per_channel();

UPDATE tbl_monthly_marketing INNER JOIN tbl_mom_net_sales_per_channel 
ON tbl_monthly_marketing.channel = tbl_mom_net_sales_per_channel.channel_group
AND tbl_monthly_marketing.month = month(tbl_mom_net_sales_per_channel.date)
SET tbl_monthly_marketing.month_over_month = tbl_mom_net_sales_per_channel.month_over_month;

INSERT IGNORE INTO tbl_monthly_marketing (month, channel) 
VALUES(month(date_add(now(), interval -1 day)), 'MTD');

UPDATE tbl_monthly_marketing INNER JOIN tbl_mom_net_sales_per_channel 
ON tbl_monthly_marketing.channel = tbl_mom_net_sales_per_channel.channel_group
AND tbl_monthly_marketing.month = month(tbl_mom_net_sales_per_channel.date)
SET tbl_monthly_marketing.month_over_month = tbl_mom_net_sales_per_channel.month_over_month
where channel_group = 'MTD';


#Adjusted revenue
#Net
update tbl_monthly_marketing m left join 
(
select t1.month, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select month(t.date) month, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by month(t.date) desc,l.about_linio, t.channel_group) t1 inner join
(select month(t.date) month, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by month(t.date) desc,l.about_linio) t2 on t1.month = t2.month and t1.about_linio = t2.about_linio
inner join
(select month(date) month, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 
group by month(date) desc, t.about_linio) t3 on t3.month = t2.month and t3.about_linio = t2.about_linio
group by t1.month, t1.channel_group) t 
on m.month = t.month and m.channel = t.channel_group set m.adjusted_net_revenue = ifnull(m.net_revenue + t.adjusted_net_revenue/3.3, m.net_revenue)
where m.channel <> 'Tele Sales' ;

update tbl_monthly_marketing m left join 
(
select t1.month, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select month(t.date) month, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by month(t.date) desc,l.about_linio, t.channel_group) t1 inner join
(select month(t.date) month, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by month(t.date) desc,l.about_linio) t2 on t1.month = t2.month and t1.about_linio = t2.about_linio
inner join
(select month(date) month, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 
group by month(date) desc, t.about_linio) t3 on t3.month = t2.month and t3.about_linio = t2.about_linio
group by t1.month, t1.channel_group) t 
on m.month = t.month and m.channel = t.channel_group set m.adjusted_net_revenue = ifnull( t.adjusted_net_revenue/3.3,0)
where m.channel = 'Tele Sales' ;

#Gross
update tbl_monthly_marketing m left join 
(
select t1.month, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select month(t.date) month, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by month(t.date) desc,l.about_linio, t.channel_group) t1 inner join
(select month(t.date) month, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by month(t.date) desc,l.about_linio) t2 on t1.month = t2.month and t1.about_linio = t2.about_linio
inner join
(select month(date) month, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and obc = 1 
group by month(date) desc, t.about_linio) t3 on t3.month = t2.month and t3.about_linio = t2.about_linio
group by t1.month, t1.channel_group) t 
on m.month = t.month and m.channel = t.channel_group set m.adjusted_gross_revenue = ifnull(m.gross_revenue + t.adjusted_net_revenue/3.3, m.gross_revenue)
where m.channel <> 'Tele Sales' ;

update tbl_monthly_marketing m left join 
(
select t1.month, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select month(t.date) month, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by month(t.date) desc,l.about_linio, t.channel_group) t1 inner join
(select month(t.date) month, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by month(t.date) desc,l.about_linio) t2 on t1.month = t2.month and t1.about_linio = t2.about_linio
inner join
(select month(date) month, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and obc = 1 
group by month(date) desc, t.about_linio) t3 on t3.month = t2.month and t3.about_linio = t2.about_linio
group by t1.month, t1.channel_group) t 
on m.month = t.month and m.channel = t.channel_group set m.adjusted_gross_revenue = ifnull( t.adjusted_net_revenue/3.3,0)
where m.channel = 'Tele Sales' ;

#Pending
update tbl_monthly_marketing m left join 
(
select t1.month, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select month(t.date) month, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by month(t.date) desc,l.about_linio, t.channel_group) t1 inner join
(select month(t.date) month, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by month(t.date) desc,l.about_linio) t2 on t1.month = t2.month and t1.about_linio = t2.about_linio
inner join
(select month(date) month, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and pending = 1 
group by month(date) desc, t.about_linio) t3 on t3.month = t2.month and t3.about_linio = t2.about_linio
group by t1.month, t1.channel_group) t 
on m.month = t.month and m.channel = t.channel_group set m.adjusted_pending_revenue = ifnull(m.pending_revenue + t.adjusted_net_revenue/3.3, m.pending_revenue)
where m.channel <> 'Tele Sales' ;

update tbl_monthly_marketing m left join 
(
select t1.month, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select month(t.date) month, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by month(t.date) desc,l.about_linio, t.channel_group) t1 inner join
(select month(t.date) month, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by month(t.date) desc,l.about_linio) t2 on t1.month = t2.month and t1.about_linio = t2.about_linio
inner join
(select month(date) month, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and pending = 1 
group by month(date) desc, t.about_linio) t3 on t3.month = t2.month and t3.about_linio = t2.about_linio
group by t1.month, t1.channel_group) t 
on m.month = t.month and m.channel = t.channel_group set m.adjusted_pending_revenue = ifnull( t.adjusted_net_revenue/3.3,0)
where m.channel = 'Tele Sales' ;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `monthly_marketing_2`()
BEGIN

#PE
#yrmonth

select @yrmonth := concat(year(curdate()-2),if(month(curdate()-2)<10,concat(0,month(curdate()-2)),month(curdate()-2)));

DELETE FROM tbl_monthly_marketing WHERE yrmonth=@yrmonth;

INSERT IGNORE INTO tbl_monthly_marketing (month,channel) 
SELECT DISTINCT month(now()), channel_group FROM tbl_order_detail WHERE channel_group is not null and yrmonth=@yrmonth;

update tbl_monthly_marketing
set yrmonth = concat(year(curdate()-2),if(month(curdate()-2)<10,concat(0,month(curdate()-2)),month(curdate()-2)))
where month = month (curdate()-2);

#VISITS	

UPDATE tbl_monthly_marketing  left JOIN (SELECT yrmonth yrmonth, reporting_channel, sum(visits) visits
FROM daily_costs_visits_per_channel where daily_costs_visits_per_channel.date < cast(curdate() as date)
GROUP BY yrmonth, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.visits=daily_costs_visits_per_channel.visits 
WHERE tbl_monthly_marketing.yrmonth = @yrmonth
;

#GROSS ORDERS

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, sum(gross_orders) gross_orders
FROM tbl_order_detail WHERE OBC = 1 and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=tbl_order_detail.channel_group
SET tbl_monthly_marketing.gross_orders=tbl_order_detail.gross_orders
WHERE tbl_monthly_marketing.yrmonth = @yrmonth
;
#NET ORDERS

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, sum(net_orders) net_orders
FROM tbl_order_detail WHERE oac = 1 and returned = 0 and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=tbl_order_detail.channel_group
SET tbl_monthly_marketing.net_orders=tbl_order_detail.net_orders
WHERE tbl_monthly_marketing.yrmonth = @yrmonth
;

#ORDERS NET EXPECTED

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, reporting_channel, sum(net_orders_e) net_orders_e
FROM daily_costs_visits_per_channel where daily_costs_visits_per_channel.date < cast(curdate() as date)
GROUP BY yrmonth, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.orders_net_expected=daily_costs_visits_per_channel.net_orders_e
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#CONVERSION RATE
UPDATE tbl_monthly_marketing SET conversion_rate=IF(gross_orders/visits IS NULL, 0, gross_orders/visits);
#PENDING REVENUE

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, 
(SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat))/3.3 pending_sales
FROM tbl_order_detail WHERE tbl_order_detail.pending=1  and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
SET tbl_monthly_marketing.pending_revenue=tbl_order_detail.pending_sales
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group and tbl_monthly_marketing.yrmonth = @yrmonth;

#GROSS REVENUE
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, 
(SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat))/3.3 gross_sales
FROM tbl_order_detail WHERE tbl_order_detail.obc=1  and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
SET tbl_monthly_marketing.gross_revenue=tbl_order_detail.gross_sales
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group and tbl_monthly_marketing.yrmonth = @yrmonth;

#NET REVENUE
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth, channel_group, 
(SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat))/3.3 net_sales
FROM tbl_order_detail WHERE tbl_order_detail.oac=1 AND tbl_order_detail.returned=0  and tbl_order_detail.date < cast(curdate() as date) 
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
SET tbl_monthly_marketing.net_revenue=tbl_order_detail.net_sales
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group and tbl_monthly_marketing.yrmonth = @yrmonth
;

#NET REVENUE EXPECTED

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, 
reporting_channel, sum(net_rev_e)/3.3 net_rev_e
FROM daily_costs_visits_per_channel where daily_costs_visits_per_channel.date < cast(curdate() as date)
GROUP BY yrmonth, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.net_revenue_expected=daily_costs_visits_per_channel.net_rev_e and tbl_monthly_marketing.yrmonth = @yrmonth
;

UPDATE tbl_monthly_marketing SET net_revenue_expected = net_revenue WHERE net_revenue_expected=0;
UPDATE tbl_monthly_marketing SET net_revenue_expected = net_revenue WHERE net_revenue_expected<net_revenue;

#MONTHLY TARGET

UPDATE tbl_monthly_marketing  
INNER JOIN (SELECT yrmonth yrmonth, channel_group, sum(target_net_sales) monthly_target
FROM daily_targets_per_channel
GROUP BY yrmonth, channel_group) daily_targets_per_channel
ON daily_targets_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_targets_per_channel.channel_group
SET tbl_monthly_marketing.monthly_target=daily_targets_per_channel.monthly_target and tbl_monthly_marketing.yrmonth = @yrmonth;

#MONTHLY DEVIATION
UPDATE tbl_monthly_marketing 
SET 
deviation=if((net_revenue_expected/(monthly_target/(SELECT DAYOFMONTH(LAST_DAY(NOW())))*(SELECT DAY(NOW())-1))-1) IS NULL,
0,(net_revenue_expected/(monthly_target/(SELECT DAYOFMONTH(LAST_DAY(NOW())))*(SELECT DAY(NOW())-1))-1))
where tbl_monthly_marketing.yrmonth = @yrmonth;

#AVG TICKET
UPDATE tbl_monthly_marketing SET avg_ticket=net_revenue/net_orders;

#MARKETING COST

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, 
reporting_channel, sum(cost_local)/3.3 cost
FROM daily_costs_visits_per_channel where daily_costs_visits_per_channel.date < cast(curdate() as date)
GROUP BY yrmonth, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.marketing_cost=daily_costs_visits_per_channel.cost
WHERE tbl_monthly_marketing.yrmonth = @yrmonth
;

#COST/REV
UPDATE tbl_monthly_marketing SET cost_rev=IF(marketing_cost/net_revenue_expected IS NULL, 0, marketing_cost/net_revenue_expected)
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#CPO
UPDATE tbl_monthly_marketing SET cost_per_order=IF(marketing_cost/net_orders IS NULL, 0, marketing_cost/net_orders)
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#CPO TARGET

UPDATE tbl_monthly_marketing  
INNER JOIN (SELECT yrmonth yrmonth, channel_group, MAX(target_cpo) target_cpo
FROM daily_targets_per_channel
GROUP BY yrmonth, channel_group) daily_targets_per_channel
ON daily_targets_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_targets_per_channel.channel_group
SET tbl_monthly_marketing.cost_per_order_target=daily_targets_per_channel.target_cpo
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#CPO VS TARGET
UPDATE tbl_monthly_marketing SET cpo_vs_target_cpo=(cost_per_order/cost_per_order_target)-1 WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#NEW CLIENTS
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, sum(new_customers) nc
FROM tbl_order_detail where oac = 1 and returned = 0 and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=tbl_order_detail.channel_group
SET tbl_monthly_marketing.new_clients=tbl_order_detail.nc WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#NEW CLIENTS EXPECTED
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, 
reporting_channel, sum(new_customers_e) nc_e
FROM daily_costs_visits_per_channel 
where daily_costs_visits_per_channel.date < cast(curdate() as date)
GROUP BY yrmonth, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.new_clientes_expected=daily_costs_visits_per_channel.nc_e WHERE tbl_monthly_marketing.yrmonth = @yrmonth;



UPDATE tbl_monthly_marketing SET new_clientes_expected=new_clients WHERE new_clientes_expected=0 AND tbl_monthly_marketing.yrmonth = @yrmonth;

#CAC
UPDATE tbl_monthly_marketing SET client_acquisition_cost=marketing_cost/new_clientes_expected WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#PC2_COST

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, 
(SUM(costo_after_vat)+SUM(delivery_cost_supplier)+SUM(wh)+SUM(cs)+SUM(payment_cost)+SUM(shipping_cost))/3.3 pc2_costs
FROM tbl_order_detail WHERE tbl_order_detail.oac=1 AND tbl_order_detail.returned=0  and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
SET tbl_monthly_marketing.pc2_costs=tbl_order_detail.pc2_costs
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group AND tbl_monthly_marketing.yrmonth = @yrmonth;

#PC2
UPDATE tbl_monthly_marketing SET profit_contribution_2=1-(pc2_costs/net_revenue) WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#MOM

UPDATE tbl_monthly_marketing INNER JOIN tbl_mom_net_sales_per_channel 
ON tbl_monthly_marketing.channel = tbl_mom_net_sales_per_channel.channel_group
AND tbl_monthly_marketing.yrmonth = tbl_mom_net_sales_per_channel.yrmonth
SET tbl_monthly_marketing.month_over_month = tbl_mom_net_sales_per_channel.month_over_month WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

INSERT IGNORE INTO tbl_monthly_marketing (month, channel) 
VALUES(month(date_add(now(), interval -1 day)), 'MTD');

update tbl_monthly_marketing
set yrmonth = concat(year(curdate()-1),if(month(curdate()-1)<10,concat(0,month(curdate()-1)),month(curdate()-1)))
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

UPDATE tbl_monthly_marketing INNER JOIN tbl_mom_net_sales_per_channel 
ON tbl_monthly_marketing.channel = tbl_mom_net_sales_per_channel.channel_group
AND tbl_monthly_marketing.yrmonth = tbl_mom_net_sales_per_channel.yrmonth
SET tbl_monthly_marketing.month_over_month = tbl_mom_net_sales_per_channel.month_over_month
where channel_group = 'MTD' AND tbl_monthly_marketing.yrmonth = @yrmonth;

#Adjusted revenue
#Net
update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion,
 sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_net_revenue = ifnull(m.net_revenue + t.adjusted_net_revenue/3.3, m.net_revenue)
where m.channel <> 'Tele Sales' AND m.yrmonth = @yrmonth;

update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_net_revenue = ifnull( t.adjusted_net_revenue/3.3,0)
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth ;

#Gross
update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and obc = 1 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_gross_revenue = ifnull(m.gross_revenue + t.adjusted_net_revenue/3.3, m.gross_revenue)
where m.channel <> 'Tele Sales'  AND m.yrmonth = @yrmonth;

update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and obc = 1 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_gross_revenue = ifnull( t.adjusted_net_revenue/3.3,0)
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth;

#Pending
update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and pending = 1 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_pending_revenue = ifnull(m.pending_revenue + t.adjusted_net_revenue/3.3, m.pending_revenue)
where m.channel <> 'Tele Sales' AND m.yrmonth = @yrmonth;

update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and pending = 1 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_pending_revenue = ifnull( t.adjusted_net_revenue/3.3,0)
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth;

#Net ORders
update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, 
sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  count(distinct orderID) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, count(distinct orderID) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, count(distinct orderID) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_net_orders = ifnull(m.net_orders + t.adjusted_net_revenue, m.net_orders)
where m.channel <> 'Tele Sales' AND m.yrmonth = @yrmonth;

update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, 
sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  count(distinct orderID) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, count(distinct orderID) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, count(distinct orderID) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_net_orders = t.adjusted_net_revenue
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth;

#New clients
update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, 
sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(new_customers) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio,  sum(new_customers) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(new_customers) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_new_clients = ifnull(m.new_clients + t.adjusted_net_revenue, m.new_clients)
where m.channel <> 'Tele Sales' AND m.yrmonth = @yrmonth;

update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, 
sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group, sum(new_customers) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio,  sum(new_customers) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(new_customers) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_new_clients = t.adjusted_net_revenue
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `product_pricing_history`()
begin

delete from production_pe.product_pricing_history;

insert into production_pe.product_pricing_history(date, sku_config, sku_simple, product_name, price) select date, sku_config, sku_simple, product_name, price from production_pe.catalog_history where date>= '2013-07-22' and visible=1;

update production_pe.product_pricing_history p inner join bob_live_pe.catalog_config conf on p.sku_config=conf.sku inner join bob_live_pe.catalog_attribute_option_global_category c on c.id_catalog_attribute_option_global_category=conf.fk_catalog_attribute_option_global_category set p.category = c.name;

update production_pe.product_pricing_history p inner join bob_live_pe.catalog_config conf on p.sku_config=conf.sku inner join bob_live_pe.catalog_attribute_option_global_sub_category c on c.id_catalog_attribute_option_global_sub_category=conf.fk_catalog_attribute_option_global_sub_category set p.sub_category = c.name;

update production_pe.product_pricing_history p inner join bob_live_pe.catalog_config conf on p.sku_config=conf.sku inner join bob_live_pe.catalog_attribute_option_global_sub_sub_category c on c.id_catalog_attribute_option_global_sub_sub_category=conf.fk_catalog_attribute_option_global_sub_sub_category set p.sub_sub_category = c.name;

update production_pe.product_pricing_history p inner join bob_live_pe.catalog_config conf on p.sku_config=conf.sku inner join bob_live_pe.catalog_attribute_option_global_buyer c on c.id_catalog_attribute_option_global_buyer=conf.fk_catalog_attribute_option_global_buyer set p.buyer = c.name;

update production_pe.product_pricing_history p set PC1 = (select sum(PC1) from production_pe.tbl_order_detail t where oac=1 and p.sku_simple=t.sku and t.date=p.date group by t.sku);

update production_pe.product_pricing_history p set PC2 = (select sum(PC2) from production_pe.tbl_order_detail t where oac=1 and p.sku_simple=t.sku and t.date=p.date group by t.sku);

update production_pe.product_pricing_history p set number_sales = (select count(sku) from production_pe.tbl_order_detail t where oac=1 and p.sku_simple=t.sku and t.date=p.date group by t.sku); 

update production_pe.product_pricing_history p inner join bob_live_pe.catalog_config c on c.sku=p.sku_config inner join bob_live_pe.catalog_brand b on b.id_catalog_brand=c.fk_catalog_brand set p.brand=b.name, p.date_created=c.created_at;

update production_pe.product_pricing_history p set net_revenue = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production_pe.tbl_order_detail t where oac=1 and p.sku_simple=t.sku and t.date=p.date group by t.sku);

update production_pe.product_pricing_history  set `% PC1` = PC1/net_revenue, `% PC2` = PC2/net_revenue;

update test_linio.webtrekk_pe w inner join production_pe.product_pricing_history p on w.date=p.date and w.sku_config=p.sku_config set p.sku_visits=w.sku_visits;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `top_20`()
begin

delete from production_pe.top_20_weekly;

insert into production_pe.top_20_weekly(Category_English, Category, Sku, Product_name, Freq, AVGPrice, PC2)
(select *
from (select 'Appliances', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_pe.tbl_order_detail t
where production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and oac = 1 and n1 = 'electrodomésticos'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Photography', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_pe.tbl_order_detail t
where production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and oac = 1 and n1 = 'cámaras y fotografía'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Audio Video', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_pe.tbl_order_detail t
where production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and oac = 1 and (n1 = 'tv, audio y video' or n1 = 'altavoces')
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Books', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_pe.tbl_order_detail t
where production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and oac = 1 and n1 like 'libros%'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Video Games', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_pe.tbl_order_detail t
where production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and oac = 1 and n1 = 'videojuegos'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Fashion', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_pe.tbl_order_detail t
where production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and oac = 1 and n1 = 'ropa, calzado y accesorios'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Health and Beauty', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_pe.tbl_order_detail t
where production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and oac = 1 and (n1 like '%cuidado personal%' or n1 = 'rizadoras' or n1 = 'cabello' or n1 = 'secadores')
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Cellphones', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_pe.tbl_order_detail t
where production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and oac = 1 and n1 = 'celulares, telefonía y gps'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Computing', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_pe.tbl_order_detail t
where production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and oac = 1 and n1 = 'computadoras'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Home', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_pe.tbl_order_detail t
where production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and oac = 1 and (n1 = 'hogar' or n1 = 'línea blanca')
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Kids and Babies', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_pe.tbl_order_detail t
where production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and oac = 1 and n1 ='niños y bebés'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Fitness', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_pe.tbl_order_detail t
where production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and oac = 1 and n1 ='deportes'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Office Supplies', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_pe.tbl_order_detail t
where production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and oac = 1 and n1 ='oficina y útiles'
group by t.sku) n
order by Freq desc limit 20);

update  production_pe.top_20_weekly set AVGPrice = AVGPrice/3.3, PC2 = (PC2/3.3)/Freq;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `update_tbl_order_detail`()
begin
declare itemid integer;
select @last_date:=max(date) from production_pe.tbl_order_detail;
select @last_date_fa:=max(date) from production_pe.ga_cost_campaign where source = 'facebook';
select @last_date_socio:=max(date) from production_pe.ga_cost_campaign where source = 'Sociomantic';

select  'update tbl_order_detail: start',now();

#update status
update production_pe.tbl_order_detail join bob_live_pe.sales_order_item on id_sales_order_item=item join bob_live_pe.sales_order_item_status on fk_sales_order_item_status=id_sales_order_item_status
set status_item= sales_order_item_status.name;

#update obc, oac, pending, cancel, returned
update production_pe.tbl_order_detail join production_pe.status_bob 
on tbl_order_detail.payment_method=status_bob.payment_method 
and tbl_order_detail.status_item=status_bob.status_bob
set tbl_order_detail.obc=status_bob.obc, 
tbl_order_detail.oac=status_bob.oac,
tbl_order_detail.pending=status_bob.pending,
tbl_order_detail.cancel=status_bob.cancel,
tbl_order_detail.returned=status_bob.returned;

#update categories
update production_pe.tbl_order_detail join production_pe.tbl_catalog_product_v2 
on tbl_order_detail.sku=tbl_catalog_product_v2.sku 
set n1=cat1,n2=cat2,n3=cat3;

#insert facebookads and sociomantic
insert into production_pe.ga_cost_campaign(date, source, medium, impressions, adclicks, adcost) 
select date(date_value), 'facebook', 
'socialmedia', sum(if(impressions is null, 0, impressions)) impressions, sum(if(clicks is null, 0, clicks)) clicks, sum(if(cost_mxn is null, 0.,cost_mxn)) cost from production_pe.mkt_facebook_ad_cost where date(date_value) > @last_date_fa
and category = 'Social Media' group by date_value, channel;

insert into production_pe.ga_cost_campaign(date, source, medium, impressions, adclicks, adcost) 
select date(date_value), 'facebook', 
'socialmediaads', sum(if(impressions is null, 0, impressions)) impressions, sum(if(clicks is null, 0, clicks)) clicks, sum(if(cost_mxn is null, 0.,cost_mxn)) cost from production_pe.mkt_facebook_ad_cost where date(date_value) > @last_date_fa
and category <> 'Social Media' group by date_value, channel;

insert into production_pe.ga_cost_campaign(date, source, medium, campaign,impressions, adclicks, adcost) 
select date(date_value), 'Sociomantic', 
'Retargeting', '(not set)', sum(if(impressions is null, 0, impressions)) impressions,
 sum(if(clicks is null, 0, clicks)) clicks, sum(if(cost_mxn is null, 0.,cost_mxn)) cost 
from production_pe.mkt_channel_sociomantic where date(date_value) > @last_date_socio
group by date(date_value);


#insert new data
set itemid = (select item from production_pe.tbl_order_detail order by item desc limit 1);


insert into production_pe.tbl_order_detail (custid,orderid,order_nr,payment_method,unit_price,paid_price,
coupon_money_value,coupon_code,date,hour,sku,item,
status_item,obc,pending,cancel,oac,returned,n1,n2,n3,
tax_percent,unit_price_after_vat,
paid_price_after_vat,coupon_money_after_vat,cost_pet,costo_oferta,costo_after_vat,delivery_cost_supplier,
buyer,brand,product_name,peso,proveedor,ciudad,region,ordershippingfee, about_linio)
(select sales_order.fk_customer as custid,sales_order.id_sales_order as orderid,sales_order.order_nr as order_nr,sales_order.payment_method as payment_method,
sales_order_item.unit_price as unit_price,sales_order_item.paid_price as paid_price,sales_order_item.coupon_money_value as coupon_money_value,sales_order.coupon_code as coupon_code,
cast(sales_order.created_at as date) as date,concat(hour(sales_order.created_at),':',minute(sales_order.created_at),':',second(sales_order.created_at)) as hour,sales_order_item.sku as sku,
sales_order_item.id_sales_order_item as item,
sales_order_item_status.name as status_item,
(select status_bob.obc from production_pe.status_bob 
where ((status_bob.payment_method = sales_order.payment_method) and (sales_order_item_status.name = status_bob.status_bob)) limit 1) as obc,
(select status_bob.pending from production_pe.status_bob where ((status_bob.payment_method = sales_order.payment_method) and 
(sales_order_item_status.name = status_bob.status_bob)) limit 1) as pending,
(select status_bob.cancel from production_pe.status_bob where ((status_bob.payment_method = sales_order.payment_method) and (sales_order_item_status.name = status_bob.status_bob)) limit 1) as cancel,
(select status_bob.oac from production_pe.status_bob where ((status_bob.payment_method = sales_order.payment_method) and (sales_order_item_status.name = status_bob.status_bob)) limit 1) as oac,
(select status_bob.returned from production_pe.status_bob where ((status_bob.payment_method = sales_order.payment_method)
and (sales_order_item_status.name = status_bob.status_bob)) limit 1) as returned,
tbl_catalog_product_v2.cat1 as n1,
tbl_catalog_product_v2.cat2 as n2,
tbl_catalog_product_v2.cat3 as n3,
tbl_catalog_product_v2.tax_percent as tax_percent,
(sales_order_item.unit_price / (1 + (tbl_catalog_product_v2.tax_percent / 100))) as unit_price_after_vat,
(sales_order_item.paid_price / (1 + (tbl_catalog_product_v2.tax_percent / 100))) as paid_price_after_vat,
(sales_order_item.coupon_money_value / (1 + (tbl_catalog_product_v2.tax_percent / 100))) as coupon_money_after_vat,
tbl_catalog_product_v2.cost as cost_pet,
if(tbl_catalog_product_v2.cost<sales_order_item.cost,sales_order_item.cost,0) as costo_oferta,
 (sales_order_item.cost/1.18) as costo_after_vat,
if(isnull(sales_order_item.delivery_cost_supplier),tbl_catalog_product_v2.inbound,
sales_order_item.delivery_cost_supplier) as delivery_cost_supplier,
tbl_catalog_product_v2.buyer as buyer,
tbl_catalog_product_v2.brand as brand,
tbl_catalog_product_v2.product_name as product_name,
tbl_catalog_product_v2.product_weight as peso,
tbl_catalog_product_v2.supplier as proveedor,
if(isnull(sales_order_address.municipality),sales_order_address.city,sales_order_address.municipality) 
as ciudad,
sales_order_address.region as region,
sales_order.shipping_amount as ordershippingfee,
sales_order.about_linio as about_linio
from ((((bob_live_pe.sales_order_item join bob_live_pe.sales_order_item_status) join bob_live_pe.sales_order) 
join production_pe.tbl_catalog_product_v2) join bob_live_pe.sales_order_address) 
where
((sales_order_item.fk_sales_order_item_status = sales_order_item_status.id_sales_order_item_status) 
and (sales_order_item.id_sales_order_item > itemid) and 
(sales_order.id_sales_order = sales_order_item.fk_sales_order) 
and (sales_order.fk_sales_order_address_shipping = sales_order_address.id_sales_order_address)
and (sales_order_item.sku = tbl_catalog_product_v2.sku)
));


update production_pe.tbl_order_detail set sku_config = left(sku, 17);

update production_pe.tbl_order_detail o set 
o.package_weight = 
(select catalog_config.package_weight from bob_live_pe.catalog_config where o.sku_config = catalog_config.sku);

update production_pe.tbl_order_detail set costo_after_vat = cost_pet/1.18 
where costo_after_vat is null;

update tbl_order_detail 
inner join bob_live_pe.sales_order s on s.order_nr = tbl_order_detail.order_nr
inner join bob_live_pe.sales_order_item sa on s.id_sales_order = sa.fk_sales_order and 
sa.id_sales_order_item = tbl_order_detail.item set costo_oferta = (if(sa.cost is null, 0,sa.cost));


update production_pe.tbl_order_detail
set year = year(date) , month = month(date) , yrmonth = concat(year,if(month<10,concat(0,month),month))
where date >=@last_date;
#update source_medium
update production_pe.tbl_order_detail t inner join SEM.transaction_id_pe c on t.order_nr=c.transaction_id set t.source_medium= concat(c.source,' / ',c.medium),t.campaign=c.campaign where t.item>=itemid;

-- Category BP

update production_pe.tbl_order_detail t inner join development_pe.A_E_M1_New_CategoryBP m on m.Cat1=t.new_cat1 set t.category_bp=m.catbp;

update production_pe.tbl_order_detail t inner join development_pe.A_E_M1_New_CategoryBP m on m.Cat2=t.new_cat2 set t.category_bp=m.catbp where (t.new_cat1   =  "Entretenimiento" or t.new_cat1 like "Electr%nicos");

update production_pe.tbl_order_detail
set year = year(date) , month = month(date) , yrmonth = concat(year,if(month<10,concat(0,month),month))
where date >=@last_date;

update tbl_order_detail 
inner join bob_live_pe.catalog_config cc on cc.sku = tbl_order_detail.sku_config
inner join bob_live_pe.catalog_attribute_option_global_category caogc on 
caogc.id_catalog_attribute_option_global_category = cc.fk_catalog_attribute_option_global_category
set new_cat1 = (if(caogc.name is null, 0,caogc.name));

update tbl_order_detail 
inner join bob_live_pe.catalog_config cc on cc.sku = tbl_order_detail.sku_config
inner join bob_live_pe.catalog_attribute_option_global_sub_category caogsc on 
caogsc.id_catalog_attribute_option_global_sub_category = cc.fk_catalog_attribute_option_global_sub_category
set new_cat2 = (if(caogsc.name is null, 0,caogsc.name));

update tbl_order_detail 
inner join bob_live_pe.catalog_config cc on cc.sku = tbl_order_detail.sku_config
inner join bob_live_pe.catalog_attribute_option_global_sub_sub_category caogssc on 
caogssc.id_catalog_attribute_option_global_sub_sub_category = cc.fk_catalog_attribute_option_global_sub_sub_category
set new_cat3 = (if(caogssc.name is null, 0,caogssc.name));

update production_pe.tbl_order_detail inner join bob_live_pe.sales_rule on code = coupon_code
inner join bob_live_pe.sales_rule_set on id_sales_rule_set = fk_sales_rule_set
set description_voucher = description 
where coupon_code is not null;

call extra_queries();
-- call extra_queries_2013();

select  'start channel_report: start',now();

call production_pe.channel_report();

select  'start channel_report: end',now();

select  'start channel_report_no_voucher: start',now();

call production_pe.channel_report_no_voucher();

select  'start channel_report_no_voucher: end',now();

#Cupones de devolución

UPDATE tbl_order_detail t SET paid_price = unit_price, paid_price_after_vat = unit_price/1.18
WHERE coupon_code like 'CCE%' 
or coupon_code like 'PRCcre%' 
or coupon_code like 'OPScre%'
or coupon_Code like 'DEP%'
or coupon_Code like 'DEV%';

#incluir nuevos cupones de descuento


#pc2 
select 'start pc2',now();
truncate production_pe.tbl_group_order_detail;
insert into production_pe.tbl_group_order_detail (select orderid, count(order_nr) as items,
sum(greatest(cast(replace(if(isnull(peso),1,peso),',','.') as decimal(21,6)),1.0))as pesototal,
sum(paid_price) as grandtotal
from production_pe.tbl_order_detail where  oac='1' and returned='0' group by order_nr);	

truncate production_pe.tbl_group_order_detail_gross;
insert into production_pe.tbl_group_order_detail_gross (select orderid, count(order_nr) as items,
sum(greatest(cast(replace(if(isnull(peso),1,peso),',','.') as decimal(21,6)),1.0))as pesototal,
sum(paid_price) as grandtotal
from production_pe.tbl_order_detail where  obc='1' group by order_nr);	


update   production_pe.tbl_order_detail as t 
left join production_pe.tbl_group_order_detail as e 
on e.orderid = t.orderid 
set t.orderpeso = e.pesototal , t.nr_items=e.items , t.ordertotal=e.grandtotal
where t.oac='1' and t.returned='0';

update   production_pe.tbl_order_detail as t 
left join production_pe.tbl_group_order_detail_gross as e 
on e.orderid = t.orderid 
set t.orderpeso_gross = e.pesototal , t.gross_items=e.items , t.ordertotal_gross=e.grandtotal
where t.obc='1';

update production_pe.tbl_order_detail as t
set gross_orders = if(gross_items is null,0,1/gross_items) where obc = '1';

##update production_pe.tbl_order_detail as t
##set net_orders = if(nr_items is null, 0,1/nr_items) where oac = '1 'and returned = '0';

-- New Net Order

/*
create table production_pe.temporary_net_orders as select order_nr, count(*) as items from production_pe.tbl_order_detail where oac=1 group by order_nr;

create index order_nr on production_pe.temporary_net_orders(order_nr);

update production_pe.tbl_order_detail t inner join production_pe.temporary_net_orders n on t.order_nr=n.order_nr set t.net_orders=1/n.items;

drop table production_pe.temporary_net_orders;*/

#pc2: shipping_fee
update production_pe.tbl_order_detail set ordershippingfee=0.0 where ordershippingfee is null;
update production_pe.tbl_order_detail set shipping_fee=ordershippingfee/cast(nr_items as decimal) where oac='1' and returned='0';
update tbl_order_detail set shipping_fee_2=ordershippingfee/cast(nr_items as decimal);

update production_pe.tbl_order_detail set shipping_fee_after_vat=if(shipping_fee is null, 0, shipping_fee/1.18);
update tbl_order_detail set shipping_fee_after_vat_2=if(shipping_fee_2 is null, 0, shipping_fee_2/1.18);
update tbl_order_detail set grand_total_after_vat=shipping_fee_after_vat_2+paid_price_after_vat;
update tbl_order_detail set gross_grand_total_after_vat=shipping_fee_after_vat_2+paid_price_after_vat where obc = '1';
update tbl_order_detail set net_grand_total_after_vat=shipping_fee_after_vat+paid_price_after_vat 
where (oac = 1 and returned = 0);



UPDATE production_pe.tbl_order_detail A, bob_live_pe.customer B
   SET A.first_name = B.first_name,
    A.last_name = B.last_name,
    A.gender = B.gender,
    A.birthday = B.birthday,
    A.customer_age = DATEDIFF(CURRENT_DATE, STR_TO_DATE(B.birthday, '%Y-%m-%d'))/365 
WHERE A.custid=B.id_customer;




UPDATE production_pe.tbl_order_detail A
   set A.age_group = case when A.customer_age<15 then "0<x<14"
        when A.customer_age<20 then "15<x<19"
        when A.customer_age<25 then "20<x<24"
        when A.customer_age<31 then "25<x<30"
        when A.customer_age<41 then "31<x<40"
        when A.customer_age<51 then "41<x<50"
        when A.customer_age<61 then "51<x<60"
      else "x>61" end ;



#pc2: cs and wh
#update production_pe.tbl_order_detail set cs=9.9/cast(nr_items as decimal),wh=14.619/cast(nr_items as decimal) 
#where date>='2012-05-01' and oac='1' and returned='0';


#update production_pe.tbl_order_detail set cs=6.402/cast(nr_items as decimal),wh=10.527/cast(nr_items as decimal) 
#where date>='2013-02-01' and oac='1' and returned='0';

#CS and WH Cost_ 

UPDATE tbl_order_detail INNER JOIN tbl_OPS_WH_CS ON tbl_order_detail.month
 = tbl_OPS_WH_CS.month SET tbl_order_detail.wh = tbl_OPS_WH_CS.wh/
tbl_order_detail.nr_items, tbl_order_detail.cs = 
tbl_OPS_WH_CS.cs/tbl_order_detail.nr_items;


/* with Daily Report INFO
update production_pe.tbl_order_detail set cs= 15.74/cast(nr_items as decimal) where date>='2012-05-01' and oac='1' and returned='0';
update production_pe.tbl_order_detail set cs= 15.44/cast(nr_items as decimal) where date>='2012-07-01' and oac='1' and returned='0';
update production_pe.tbl_order_detail set cs= 9.24/cast(nr_items as decimal) where date>='2012-08-01' and oac='1' and returned='0';
update production_pe.tbl_order_detail set cs= 9.90/cast(nr_items as decimal) where date>='2012-09-01' and oac='1' and returned='0';
update production_pe.tbl_order_detail set cs= 8.21/cast(nr_items as decimal) where date>='2013-02-01' and oac='1' and returned='0';
update production_pe.tbl_order_detail set cs= 8.98/cast(nr_items as decimal) where date>='2013-03-01' and oac='1' and returned='0';
update production_pe.tbl_order_detail set cs= 8.98/cast(nr_items as decimal) where date>='2013-05-01' and oac='1' and returned='0';

update production_pe.tbl_order_detail set wh=14.619/cast(nr_items as decimal)where date>='2012-05-01' and oac='1' and returned='0';
update production_pe.tbl_order_detail set wh=12.87/cast(nr_items as decimal)where date>='2012-08-01' and oac='1' and returned='0';
update production_pe.tbl_order_detail set wh=14.619/cast(nr_items as decimal)where date>='2012-09-01' and oac='1' and returned='0';
update production_pe.tbl_order_detail set wh=12.64/cast(nr_items as decimal)where date>='2013-02-01' and oac='1' and returned='0';
update production_pe.tbl_order_detail set wh=6.5/cast(nr_items as decimal)where date>='2013-03-01' and oac='1' and returned='0';
update production_pe.tbl_order_detail set wh=5.247/cast(nr_items as decimal)where date>='2013-04-01' and oac='1' and returned='0';
update production_pe.tbl_order_detail set wh=5.412/cast(nr_items as decimal)where date>='2013-05-01' and oac='1' and returned='0';
*/
/*shipping_cost_new */
/*
UPDATE production_pe.tbl_order_detail LEFT JOIN production_pe.tbl_shipping_cost_per_Kg ON 
round(production_pe.tbl_order_detail.orderpeso) = production_pe.tbl_shipping_cost_per_Kg.kg 
LEFT JOIN production_pe.order_managment ON 
production_pe.tbl_order_detail.item = order_managment.item_id 
SET production_pe.tbl_order_detail.shipping_cost = 
if(production_pe.order_managment.shipping_cost is null,
production_pe.tbl_shipping_cost_per_Kg.shipping_cost,production_pe.order_managment.shipping_cost);
*/

UPDATE production_pe.tbl_order_detail LEFT JOIN production_pe.order_managment ON 
production_pe.tbl_order_detail.item = order_managment.item_id 
SET production_pe.tbl_order_detail.shipping_cost = order_managment.shipping_cost
where production_pe.tbl_order_detail.date <= '2013-01-31';

#UPDATE production_pe.tbl_order_detail LEFT JOIN production_pe.1tbl_shipping_cost_per_kg ON 
#round(production_pe.tbl_order_detail.orderpeso) = production_pe.1tbl_shipping_cost_per_kg.kg 
#LEFT JOIN production_pe.order_managment ON 
#production_pe.tbl_order_managment.item_id = order_managment.item_id 
#SET production_pe.tbl_order_detail.shipping_cost = 
#if(IsNull(order_managment.shipping_cost),
#production_pe.1tbl_shipping_cost_per_kg.shipping_cost,production_pe.order_managment.shipping_cost)
#where production_pe.tbl_order_detail.date >= '2013-01-31';


/* update production_pe.tbl_order_detail inner join production_pe.tbl_catalog_product_v2 
on tbl_order_detail.sku = tbl_catalog_product_v2.sku set shipping_cost = 3.22 where 
product_weight <= 1 and date>='2013-02-01' and date <='2013-02-28';

update production_pe.tbl_order_detail inner join production_pe.tbl_catalog_product_v2 
on tbl_order_detail.sku = tbl_catalog_product_v2.sku set shipping_cost = 3.22*product_weight 
where product_weight > 1 and date>='2013-02-01' and date <='2013-02-28';*/

/*update production_pe.tbl_order_detail inner join production_pe.tbl_catalog_product_v2 
on tbl_order_detail.sku = tbl_catalog_product_v2.sku set shipping_cost = 3.22 where 
date > '2013-02-01' and product_weight <= 1;

update production_pe.tbl_order_detail inner join production_pe.tbl_catalog_product_v2 
on tbl_order_detail.sku = tbl_catalog_product_v2.sku set shipping_cost = 3.22*product_weight 
where product_weight > 1 and date > '2013-02-01' ; */
/*
update production_pe.tbl_order_detail set shipping_cost= 8.872155092/cast(nr_items as decimal) 
where date>='2013-02-01' and oac='1' and returned='0';

update production_pe.tbl_order_detail set shipping_cost= 18.24550717/cast(nr_items as decimal) 
where date>='2013-03-01' and oac='1' and returned='0';

update production_pe.tbl_order_detail set shipping_cost= 12.64688839/cast(nr_items as decimal) 
where date>='2013-04-01' and oac='1' and returned='0';

update production_pe.tbl_order_detail set shipping_cost= 14.21703354/cast(nr_items as decimal) 
where date>='2013-05-01' and oac='1' and returned='0';
*/
#pc2: payment_cost
update production_pe.tbl_order_detail set payment_cost=(ordertotal+ordershippingfee)*0.035/cast(nr_items as decimal) 
where payment_method ='safetypay_payment' and oac='1' and returned='0';

update production_pe.tbl_order_detail set payment_cost=(ordertotal+ordershippingfee)*0.0214/cast(nr_items as decimal)
 where payment_method ='Adyen_HostedPaymentPage' and oac='1' and returned='0';

update production_pe.tbl_order_detail set payment_cost=(ordertotal+ordershippingfee)*0.045/cast(nr_items as decimal)
 where payment_method ='CreditCardOnDelivery_Payment' and oac='1' and returned='0';

update production_pe.tbl_order_detail set payment_cost=(2.8)/cast(nr_items as decimal)
 where payment_method ='AgenciaBCP_Payment' and oac='1' and returned='0';

update production_pe.tbl_order_detail set payment_cost=(2.5)/cast(nr_items as decimal)
 where payment_method ='AgenciaBBVA_Payment' and oac='1' and returned='0';

update production_pe.tbl_order_detail set payment_cost=(ordertotal+ordershippingfee)*0.005/cast(nr_items as decimal)
 where payment_method ='CashOnDelivery_Payment' and oac='1' and returned='0';

update production_pe.tbl_order_detail set payment_cost=(ordertotal+ordershippingfee)*0.0035/cast(nr_items as decimal)
 where payment_method ='VisaNet_HostedPaymentPage' and oac='1' and returned='0';

update production_pe.tbl_order_detail set payment_cost=(ordertotal+ordershippingfee)*0.0375/cast(nr_items as decimal)
 where payment_method ='Pagosonline_LAP' and oac='1' and returned='0';



#wh shipping_cost para drop shipping
update production_pe.tbl_order_detail set wh =0.0, shipping_cost=0.0 
where sku in (select sku from bob_live_pe.catalog_simple where fk_catalog_shipment_type=2);



#null fiedls
update production_pe.tbl_order_detail set shipping_fee=0.0 where shipping_fee is null;
update production_pe.tbl_order_detail set delivery_cost_supplier=0.0 where delivery_cost_supplier is null;
update production_pe.tbl_order_detail set wh=0.0 where wh is null;
update production_pe.tbl_order_detail set cs=0.0 where cs is null;
update production_pe.tbl_order_detail set payment_cost=0.0 where payment_cost is null;
update production_pe.tbl_order_detail set shipping_cost=0.0 where shipping_cost is null;



#daily report
call daily_report();


#tbl_monthly_cohort_sin_groupon
truncate production_pe.tbl_monthly_cohort_sin_groupon;
insert into tbl_monthly_cohort_sin_groupon(custid, netsales, orders, firstorder, lastorder, idfirstorder, idlastorder, frecuencia,
coupon_code,  channel, channel_group)
select t1.*, t2.coupon_code, t2.channel,t2.channel_group from
(select 
        tbl_order_detail.custid as custid,
        sum(tbl_order_detail.paid_price_after_vat) as netsales,
        count(distinct tbl_order_detail.order_nr) as orders,
        min(tbl_order_detail.date) as firstorder,
        max(tbl_order_detail.date) as lastorder,
        min(tbl_order_detail.orderid) as idfirstorder,
        max(tbl_order_detail.orderid) as idlastorder,
        count(distinct tbl_order_detail.date) as frecuencia
    from
        production_pe.tbl_order_detail
    where
        ((tbl_order_detail.oac = '1')
            and (tbl_order_detail.returned = '0'))
    group by tbl_order_detail.custid
    order by sum(tbl_order_detail.paid_price_after_vat) desc) as t1
inner join (select  orderid,coupon_code,channel,channel_group from production_pe.tbl_order_detail where oac = '1' and returned = '0'and coupon_code not like 'gr%')
as t2 on t1.idfirstorder = t2.orderid
group by t1.custid, t2.coupon_code
order by sum(t1.netsales) desc;

update production_pe.tbl_order_detail inner join  
(select firstOrder as date,custID as newCustomer, idFirstOrder from production_pe.view_cohort ) as tbl_nc on tbl_nc.date=tbl_order_detail.date and tbl_nc.newCustomer = tbl_order_detail.custID and tbl_nc.idFirstOrder = orderID
set tbl_order_detail.new_customers=1/nr_items;


#tbl_monthly_cohort
truncate production_pe.tbl_monthly_cohort;
insert into tbl_monthly_cohort(custid,cohort,idfirstorder,idlastorder)
select custid,
cast(concat(year(firstorder),if(length(month(firstorder)) < 2,concat('0', month(firstorder)),month(firstorder)))as signed) as cohort,
idfirstorder,idlastorder from production_pe.view_cohort;

update production_pe.tbl_monthly_cohort inner join production_pe.tbl_order_detail on idFirstOrder = orderID and tbl_monthly_cohort.CustID = tbl_order_detail.CustID
set tbl_monthly_cohort.coupon_code = tbl_order_detail.coupon_code;

update production_pe.tbl_monthly_cohort set coupon_code = '' where coupon_code is null;

select  'update tbl_order_detail: end',now();

#New_customers
update production_pe.tbl_order_detail inner join  
(select firstOrder as date,custID as newCustomer, idFirstOrder from production_pe.view_cohort_gross ) as tbl_nc on tbl_nc.date=tbl_order_detail.date and tbl_nc.newCustomer = tbl_order_detail.custID and tbl_nc.idFirstOrder = orderID
set tbl_order_detail.new_customers_gross=1/gross_items;

update production_pe.tbl_order_detail set PC1 = ifnull(unit_price_after_vat,0)-ifnull(coupon_money_value,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0);

update production_pe.tbl_order_detail set PC2 = PC1-ifnull(payment_cost,0)-ifnull(shipping_cost,0)-ifnull(wh,0)-ifnull(cs,0);

#channel clv
truncate production_pe.tbl_monthly_cohort_channel;
insert into production_pe.tbl_monthly_cohort_channel (fk_customer,cohort,idfirstorder,channel_group)
select tbl_monthly_cohort.custid as fk_customer,cohort,idfirstorder,tbl_order_detail.channel_group as channel
from production_pe.tbl_monthly_cohort left join production_pe.tbl_order_detail on orderid=idfirstorder
group by tbl_monthly_cohort.custid;

truncate production_pe.cat_new_category;
Insert Into production_pe.cat_new_category
select A.id_catalog_config ,GROUP_CONCAT(B.fk_catalog_category) fk_catalog_category, GROUP_CONCAT(C.name) name
from bob_live_pe.catalog_config A
left join bob_live_pe.catalog_config_has_catalog_category B on A.id_catalog_config=B.fk_catalog_config
left join bob_live_pe.catalog_category C on C.id_catalog_category=B.fk_catalog_category
group by A.id_catalog_config;

truncate production_pe.cat_old_category;
Insert into production_pe.cat_old_category
select  A.id_catalog_config,
cast(concat(COALESCE(nullif(A.fk_catalog_attribute_option_global_category,''),''),',',
COALESCE(nullif(A.fk_catalog_attribute_option_global_sub_category,''),''),',',
COALESCE(nullif(A.fk_catalog_attribute_option_global_sub_sub_category,''),'')) AS CHAR(10000)) old_cat_number,
concat(B.name,',',C.name,',',D.name) old_cat_name
from bob_live_pe.catalog_config A 
left join bob_live_pe.catalog_attribute_option_global_category B 
on A.fk_catalog_attribute_option_global_category = B.id_catalog_attribute_option_global_category
left join bob_live_pe.catalog_attribute_option_global_sub_category  C 
on A.fk_catalog_attribute_option_global_sub_category= C.id_catalog_attribute_option_global_sub_category
left join bob_live_pe.catalog_attribute_option_global_sub_sub_category  D 
on A.fk_catalog_attribute_option_global_sub_sub_category = D.id_catalog_attribute_option_global_sub_sub_category;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Peru', 
  'tbl_order_detail',
  NOW(),
  max(date),
  count(*),
  count(*)
FROM
  production_pe.tbl_order_detail;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `visits_costs_channel`()
begin

delete from production_pe.vw_visits_costs_channel;
delete from production_pe.daily_costs_visits_per_channel;

insert into production_pe.vw_visits_costs_channel (date, channel_group, sum_visits, sum_adcost) 
select v.*, if(c.adCost is null, 0, c.adCost) adCost from (
select date, channel_group, sum(if(adCost is null, 0, adCost)) adCost from production_pe.ga_cost_campaign
group by date , channel_group) c right join (
select date, channel_group, sum(visits) visits from production_pe.ga_visits_cost_source_medium
group by date , channel_group) v on c.channel_group = v.channel_group and c.date = v.date
order by c.date desc;

insert into production_pe.daily_costs_visits_per_channel (date, cost_local, reporting_channel, visits) select date, sum_adcost, channel_group, sum_visits from production_pe.vw_visits_costs_channel;

update production_pe.daily_costs_visits_per_channel
set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));

update production_pe.daily_costs_visits_per_channel set month = month(date);

update production_pe.daily_costs_visits_per_channel set reporting_channel = 'Non identified' where reporting_channel is null;

delete from production_pe.media_rev_orders;

insert into production_pe.media_rev_orders(date, channel_group, net_rev, gross_rev, net_orders, gross_orders, new_customers_gross, new_customers) select date, channel_group, sum(net_grand_total_after_vat), sum(gross_grand_total_after_vat), sum(net_orders), sum(gross_orders), sum(new_customers_gross), sum(new_customers) from production_pe.tbl_order_detail group by date, channel_group;

update production_pe.daily_costs_visits_per_channel a inner join production_pe.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.gross_rev = b.gross_rev;

update production_pe.daily_costs_visits_per_channel a inner join production_pe.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.gross_orders = b.gross_orders;

update production_pe.daily_costs_visits_per_channel a inner join production_pe.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.new_customers_gross = b.new_customers_gross;

update production_pe.daily_costs_visits_per_channel a set net_rev_e = (select (avg(net_rev/gross_rev)) from production_pe.media_rev_orders b where b.date between date_sub(a.date, interval 21 day) and date_sub(a.date,interval 7 day) and a.reporting_channel=b.channel_group group by reporting_channel);

update production_pe.daily_costs_visits_per_channel a set net_orders_e = (select (avg(net_orders/gross_orders)) from production_pe.media_rev_orders b where b.date between date_sub(a.date, interval 21 day) and date_sub(a.date,interval 7 day) and a.reporting_channel=b.channel_group group by reporting_channel);

update production_pe.daily_costs_visits_per_channel a set new_customers_e = (select (avg(new_customers/new_customers_gross)) from production_pe.media_rev_orders b where b.date between date_sub(a.date, interval 21 day) and date_sub(a.date,interval 7 day) and a.reporting_channel=b.channel_group group by reporting_channel);

update production_pe.daily_costs_visits_per_channel set net_rev_e = net_rev_e*gross_rev, net_orders_e = net_orders_e*gross_orders, new_customers_e = new_customers_e*new_customers_gross;

update production_pe.daily_costs_visits_per_channel a inner join production_pe.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.net_rev_e = b.net_rev, a.net_orders_e = b.net_orders, a.new_customers_e = b.new_customers where a.date < date_sub(curdate(), interval 14 day);

update production_pe.daily_costs_visits_per_channel a inner join production_pe.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.net_rev_e = case when b.net_rev>a.net_rev_e then b.net_rev else a.net_rev_e end, a.net_orders_e = case when b.net_orders>a.net_orders_e then b.net_orders else a.net_orders_e end, a.new_customers_e = case when b.new_customers>a.new_customers_e then b.new_customers else a.new_customers_e end;

update production_pe.daily_costs_visits_per_channel a inner join production_pe.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.net_rev_e = case when a.net_rev_e is null then b.net_rev else a.net_rev_e end, a.net_orders_e = case when a.net_orders_e is null then b.net_orders else a.net_orders_e end, a.new_customers_e = case when a.new_customers_e is null then b.new_customers else a.new_customers_e end;

update production_pe.daily_costs_visits_per_channel a inner join production_pe.daily_targets_per_channel b 
on a.reporting_channel=b.channel_group and a.date = b.date set a.target_net_sales = b.target_net_sales, a.target_cpo = b.target_cpo;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:04:18
