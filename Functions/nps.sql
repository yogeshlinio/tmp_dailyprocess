-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: nps
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'nps'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`edgar.karam`@`%`*/ /*!50003 PROCEDURE `LLenatablas`()
BEGIN
drop  table if exists MailsNO_TMP_CO;
create  table MailsNO_TMP_CO (key(email)) 
(select distinct uniqueKey as email from CRM_CO.campaing_clicks where `Campaign-ID` = 106653 and datediff(Now(),`dispatch-start`)<=4);

drop  table if exists MailsSI_TMP_CO;
create  table MailsSI_TMP_CO (key(email)) 
(select distinct uniqueKey as email from CRM_CO.campaing_messages where `Campaign-ID` = 106653 and datediff(Now(),`dispatch-start`)<=4);

drop  table if exists MailsNO_TMP_CO2;
create  table MailsNO_TMP_CO2 (key(email)) 
(select distinct uniqueKey as email from CRM_CO.campaing_clicks where `Campaign-ID` in (106653, 106654, 83932) and datediff(Now(),`dispatch-start`)<=90);

drop  table if exists MailsNO_TMP_MX;
create  table MailsNO_TMP_MX (key(email)) 
(select distinct uniqueKey as email from CRM_MX.campaing_clicks where `Campaign-ID` = 106651 and datediff(Now(),`dispatch-start`)<=4);

drop  table if exists MailsSI_TMP_MX;
create  table MailsSI_TMP_MX (key(email)) 
(select distinct uniqueKey as email from CRM_MX.campaing_messages where `Campaign-ID` = 106651 and datediff(Now(),`dispatch-start`)<=4);

drop  table if exists MailsNO_TMP_MX2;
create  table MailsNO_TMP_MX2 (key(email)) 
(select distinct uniqueKey as email from CRM_MX.campaing_clicks where `Campaign-ID` in (106651, 106652, 83930) and datediff(Now(),`dispatch-start`)<=90);

drop  table if exists MailsNO_TMP_PE;
create  table MailsNO_TMP_PE (key(email)) 
(select distinct uniqueKey as email from CRM_PE.campaing_clicks where `Campaign-ID` = 106655 and datediff(Now(),`dispatch-start`)<=4);

drop  table if exists MailsSI_TMP_PE;
create  table MailsSI_TMP_PE (key(email)) 
(select distinct uniqueKey as email from CRM_PE.campaing_messages where `Campaign-ID` = 106655 and datediff(Now(),`dispatch-start`)<=4);

drop  table if exists MailsNO_TMP_PE2;
create  table MailsNO_TMP_PE2 (key(email)) 
(select distinct uniqueKey as email from CRM_PE.campaing_clicks where `Campaign-ID` in (106655, 106656, 83936) and datediff(Now(),`dispatch-start`)<=90);

drop  table if exists MailsNO_TMP_VE;
create  table MailsNO_TMP_VE (key(email)) 
(select distinct uniqueKey as email from CRM_VE.campaing_clicks where `Campaign-ID` = 106657 and datediff(Now(),`dispatch-start`)<=4);

drop  table if exists MailsSI_TMP_VE;
create  table MailsSI_TMP_VE (key(email)) 
(select distinct uniqueKey as email from CRM_VE.campaing_messages where `Campaign-ID` = 106657 and datediff(Now(),`dispatch-start`)<=4);

drop  table if exists MailsNO_TMP_VE2;
create  table MailsNO_TMP_VE2 (key(email)) 
(select distinct uniqueKey as email from CRM_VE.campaing_clicks where `Campaign-ID` in (106657, 106658, 83933) and datediff(Now(),`dispatch-start`)<=90);

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`laura.forero`@`%`*/ /*!50003 PROCEDURE `supplier_ranking_mx`()
BEGIN

DROP TABLE IF EXISTS nps.supplier_ranking_mx;

CREATE TABLE nps.supplier_ranking_mx (INDEX(item_id,sku_config,sku_simple,order_number))
SELECT 
a.item_id,
a.order_number,
a.fulfillment_type_real,
a.sku_config,
a.sku_simple,
a.sku_name,
a.cat_1,
a.cat_2,
a.package_measure_new,
a.supplier_id,
a.supplier_name,
a.supplier_leadtime,
a.max_delivery_time,
a.date_ordered,
a.date_exported,
a.date_shipped,
a.date_delivered,
a.date_procured,
a.date_po_created,
a.date_po_issued,
a.date_procured_promised,
a.date_delivered_promised,
year(a.date_ordered) AS year_ordered,
month(a.date_ordered) AS month_ordered,
a.status_wms,
a.workdays_to_1st_attempt,
a.workdays_to_deliver,
a.workdays_to_ship,
a.workdays_total_1st_attempt - a.workdays_to_export AS workdays_exported_to_1st,
a.workdays_total_delivery - a.workdays_to_export AS workdays_exported_to_delivery,
a.days_since_stock_updated,
a.on_time_to_procure,
a.on_time_to_ship,
a.on_time_to_1st_attempt,	
a.on_time_to_deliver,
if(a.date_procured>a.date_procured_promised AND a.fulfillment_type_real = 'Crossdocking',1,0) AS late_procured,
c.is_returned,
a.is_stockout,
a.stockout_real,
a.is_backorder,
c.is_fail_delivery,
a.has_nps_score,
a.loyalty,
a.is_presale,
b.rev AS revenue,
b.SupplierType,
if(workdays_to_ship <= 1,1,0) AS shipped_within_1wd,
if(workdays_to_ship <= 2,1,0) AS shipped_within_2wd,
if(workdays_to_stockout_declared <=3,1,0) AS declared_oos_within_3wd,
if(workdays_to_backorder_declared <=3,1,0) AS declared_bo_within_3wd,
if(workdays_to_procure <=3,1,0) AS procured_within_3wd,
if(workdays_to_procure <=7,1,0) AS procured_within_7wd,
a.check_dates,
000 AS workdays_to_good_receipt,
0 AS nps_monoitem,
null as Calls_to_CC
FROM operations_mx.out_order_tracking a
INNER JOIN development_mx.A_Master b
ON item_id = itemid
INNER JOIN operations_mx.out_inverse_logistics_tracking c
ON a.item_id = c.item_id
WHERE YEAR(a.date_ordered) = 2014
AND (b.OrderAfterCan = 1 OR a.is_backorder = 1 OR a.is_stockout = 1 OR c.is_returned = 1)
AND a.is_presale = 0;

/*Update nps.supplier_ranking_mx mx
inner join development_mx.A_Master am
set revenue = Rev
where mx.item_id = am.itemid;

Update nps.supplier_ranking_mx mx
inner join operations_mx.out_order_tracking am
set mx.workdays_exported_to_1st = am.workdays_total_1st_attempt - am.workdays_to_export,
mx.workdays_exported_to_delivery = am.workdays_total_delivery - am.workdays_to_export
where mx.item_id = am.item_id;

Update nps.supplier_ranking_mx
set year_purchased = year(date_ordered),
month_purchased = month(date_ordered),
day_purchased = day(date_ordered);*/

Update nps.supplier_ranking_mx mx
inner join (select order_number, count(1) items
from operations_mx.out_order_tracking
group by order_number) r
on mx.order_number = r.order_number
set nps_monoitem = 1
where has_nps_score = 1 and r.items = 1;

UPDATE nps.supplier_ranking_mx
INNER JOIN operations_mx.calcworkdays
	ON supplier_ranking_mx.date_po_created = calcworkdays.date_first
		AND supplier_ranking_mx.date_procured = calcworkdays.date_last
SET supplier_ranking_mx.workdays_to_good_receipt =	calcworkdays.workdays;

drop TEMPORARY  TABLE if EXISTS A_Maste_sample;
CREATE TEMPORARY TABLE A_Maste_sample (KEY(OrderNum))
SELECT * FROM development_mx.A_Master WHERE YEAR(date)='2014';

DROP temporary table IF exists tmp_zendesk ;
create temporary table tmp_zendesk (KEY(Numero_del_pedido,SKU))


SELECT Numero_del_pedido,SKU,c FROM

(
select Numero_del_pedido,SKU_1 as SKU,count(SKU_1) as c from customer_service.tbl_zendesk 
WHERE LENGTH(Numero_del_pedido)=9  AND SKU_1<>'-' AND Group_ <>'Pre - Venta '
group by Numero_del_pedido
)b
INNER JOIN 
A_Maste_sample a
ON b.Numero_del_pedido=a.OrderNum;



update nps.supplier_ranking_mx a inner join 
tmp_zendesk  as z
on z.Numero_del_pedido= a.order_number 
and a.sku_simple= z.SKU
set
Calls_to_CC=c;
update nps.supplier_ranking_mx a inner join 
tmp_zendesk  as z
on z.Numero_del_pedido= a.order_number 
and a.sku_config= z.SKU
set
Calls_to_CC=c;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:04:05
