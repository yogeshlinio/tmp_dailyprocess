-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: GlobalConfig
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`daniel.palacios`@`%`*/ /*!50003 TRIGGER insert_Cost
     AFTER INSERT ON M_Costs
     FOR EACH ROW
		 BEGIN
			INSERT INTO M_Costs_History (MonthNum,Country,TypeCost,`value`,valueNew,updateAt,updateBy) 
			VALUES (NEW.MonthNum, NEW.Country, NEW.TypeCost,null,NEW.`value`, NOW(),USER()); 					
		 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`daniel.palacios`@`%`*/ /*!50003 TRIGGER update_Cost
     AFTER UPDATE ON M_Costs
     FOR EACH ROW
		 BEGIN
			INSERT INTO M_Costs_History (MonthNum,Country,TypeCost,`value`,valueNew,updateAt,updateBy) 
			VALUES (NEW.MonthNum, NEW.Country, NEW.TypeCost,OLD.`value`,NEW.`value`, NOW(),USER()); 					
		 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`daniel.palacios`@`%`*/ /*!50003 TRIGGER delete_Cost
     AFTER DELETE ON M_Costs
     FOR EACH ROW
		 BEGIN
			INSERT INTO M_Costs_History (MonthNum,Country,TypeCost,`value`,valueNew,updateAt,updateBy) 
			VALUES (OLD.MonthNum, OLD.Country, OLD.TypeCost,OLD.`value`,NULL, NOW(),USER()); 					
		 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`daniel.palacios`@`%`*/ /*!50003 TRIGGER insert_CostDaily
     AFTER INSERT ON M_CostsDaily
     FOR EACH ROW
		 BEGIN
			INSERT INTO M_CostsDaily_History(Date,Country,TypeCost,`value`,valueNew,updatedAt,updatedBy) 
			VALUES (NEW.Date, NEW.Country, NEW.TypeCost,NULL, NEW.`value`,NOW(),USER());
		 end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`daniel.palacios`@`%`*/ /*!50003 TRIGGER update_CostsDaily	
     AFTER UPDATE ON M_CostsDaily
     FOR EACH ROW
		 BEGIN
			INSERT INTO M_CostsDaily_History(Date,Country,TypeCost,`value`,valueNew,updatedAt,updatedBy) 
			VALUES (NEW.Date, NEW.Country, NEW.TypeCost,OLD.`value`, NEW.`value`,NOW(),USER());
		 end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`daniel.palacios`@`%`*/ /*!50003 TRIGGER delete_CostDaily
     AFTER DELETE ON M_CostsDaily
     FOR EACH ROW
		 BEGIN
			INSERT INTO M_CostsDaily_History (Date,Country,TypeCost,`value`,valueNew,updatedAt,updatedBy) 
			VALUES (OLD.Date, OLD.Country, OLD.TypeCost,OLD.`value`,null,NOW(),USER());
		 end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`GlobalConfig`@`%`*/ /*!50003 TRIGGER `insert_CreditNotes` AFTER INSERT ON `M_CreditNotes` FOR EACH ROW BEGIN
			INSERT INTO M_CreditNotes_History(id_credit_note,supplier,supplier_new,month_num,month_num_new,category_BP,category_BP_new,reference,reference_new,cost,cost_new,cost_after_tax,cost_after_tax_new,updetedAt,updetedBy)
			VALUES (NEW.id_creditnote,NULL,NEW.supplier,NULL,NEW.month_num,NULL,NEW.category_BP,NULL,NEW.reference,NULL,NEW.cost,NULL,NEW.cost_after_tax,NOW(),USER());
		 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`GlobalConfig`@`%`*/ /*!50003 TRIGGER `update_CreditNotes` AFTER UPDATE ON `M_CreditNotes` FOR EACH ROW BEGIN
			INSERT INTO M_CreditNotes_History(id_credit_note,supplier,supplier_new,month_num,month_num_new,category_BP,category_BP_new,reference,reference_new,cost,cost_new,cost_after_tax,cost_after_tax_new,updetedAt,updetedBy)
			VALUES (NEW.id_creditnote,OLD.supplier,NEW.supplier,OLD.month_num,NEW.month_num,OLD.category_BP,NEW.category_BP,OLD.reference,NEW.reference,OLD.cost,NEW.cost,OLD.cost_after_tax,NEW.cost_after_tax,NOW(),USER());
		 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`GlobalConfig`@`%`*/ /*!50003 TRIGGER `delete_CreditNotes` AFTER DELETE ON `M_CreditNotes` FOR EACH ROW BEGIN
			INSERT INTO M_CreditNotes_History(id_credit_note,supplier,supplier_new,month_num,month_num_new,category_BP,category_BP_new,reference,reference_new,cost,cost_new,cost_after_tax,cost_after_tax_new,updetedAt,updetedBy)
			VALUES (OLD.id_creditnote,OLD.supplier,NULL,OLD.month_num,NULL,OLD.category_BP,NULL,OLD.reference,NULL,OLD.cost,NULL,OLD.cost_after_tax,NULL,NOW(),USER());
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`daniel.palacios`@`%`*/ /*!50003 TRIGGER insert_ExchageRate
     AFTER INSERT ON `M_ExchangeRate` FOR EACH ROW
		 BEGIN
			INSERT INTO M_ExchangeRate_History(Month_Num,Country,XR,XRNew,comments,updatedAt,updatedBy)
			VALUES(NEW.Month_Num,NEW.Country,NULL,NEW.XR,NEW.comments,NOW(),USER());
		 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`daniel.palacios`@`%`*/ /*!50003 TRIGGER update_ExchageRate
     AFTER UPDATE ON `M_ExchangeRate` FOR EACH ROW
		 BEGIN
			INSERT INTO M_ExchangeRate_History(Month_Num,Country,XR,XRNew,comments,updatedAt,updatedBy)
			VALUES(NEW.Month_Num,NEW.Country,OLD.XR,NEW.XR,NEW.comments,NOW(),USER());
		 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`daniel.palacios`@`%`*/ /*!50003 TRIGGER delete_ExchageRate
     AFTER DELETE ON `M_ExchangeRate` FOR EACH ROW
		 BEGIN
			INSERT INTO M_ExchangeRate_History(Month_Num,Country,XR,XRNew,comments,updatedAt,updatedBy)
			VALUES(OLD.Month_Num,OLD.Country,OLD.XR,null,OLD.comments,NOW(),USER());
		 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`daniel.palacios`@`%`*/ /*!50003 TRIGGER insert_Offline
     AFTER INSERT ON M_Offline
     FOR EACH ROW
		 BEGIN
			INSERT INTO M_Offline_History (Date,Country,TypeCost,`value`,valueNew,updatedAt,updatedBy) 
			VALUES (NEW.Date,NEW.Country,NEW.TypeCost,null,NEW.`value`,NOW(),USER()); 					
		 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`daniel.palacios`@`%`*/ /*!50003 TRIGGER `update_Offline` AFTER UPDATE ON `M_Offline`
FOR EACH ROW BEGIN
			INSERT INTO M_Offline_History (Date,Country,TypeCost,`value`,valueNew,updatedAt,updatedBy) 
			VALUES (NEW.Date,NEW.Country,NEW.TypeCost,OLD.`value`,NEW.`value`,NOW(),USER()); 					
		 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`daniel.palacios`@`%`*/ /*!50003 TRIGGER delete_Offline
     AFTER DELETE ON M_Offline
     FOR EACH ROW
		 BEGIN
			INSERT INTO M_Offline_History (Date,Country,TypeCost,`value`,valueNew,updatedAt,updatedBy) 
			VALUES (OLD.Date,OLD.Country,OLD.TypeCost,OLD.`value`,null,NOW(),USER()); 					
		 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`daniel.palacios`@`%`*/ /*!50003 trigger insert_other_revenue
     after insert on M_Other_Revenue
     for each row
		 begin
			insert into M_Other_Revenue_History (date,country,type,`value`,valueNew,invoice,updatedat,updatedby)
			values (new.date,new.country,new.type,null,new.`value`,new.invoice,now(),user()); 					
		 end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`daniel.palacios`@`%`*/ /*!50003 TRIGGER update_Other_Revenue
     AFTER UPDATE ON M_Other_Revenue
     FOR EACH ROW
		 BEGIN
			insert into M_Other_Revenue_History (date,country,type,`value`,valueNew,invoice,updatedat,updatedby)
			values (new.date,new.country,new.type,OLD.`value`,new.`value`,new.invoice,now(),user()); 					
		 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`daniel.palacios`@`%`*/ /*!50003 TRIGGER delete_Other_Revenue
     AFTER DELETE ON M_Other_Revenue
     FOR EACH ROW
		 BEGIN
			insert into M_Other_Revenue_History (date,country,type,`value`,valueNew,invoice,updatedat,updatedby)
			values (OLD.date,OLD.country,OLD.type,OLD.`value`,NULL,OLD.invoice,now(),user()); 					
		 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`daniel.palacios`@`%`*/ /*!50003 TRIGGER insert_Targets
     AFTER INSERT ON M_Targets
     FOR EACH ROW
		 BEGIN
			INSERT INTO  M_Targets_History (MonthNum,Country,Type,`value`,valuenNew,updatedAt,updatedBy)
			VALUES ( NEW.MonthNum, NEW.Country, NEW.Type,NULL, NEW.`value`, NOW(),USER());
		 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`daniel.palacios`@`%`*/ /*!50003 TRIGGER update_Targets
     AFTER UPDATE ON M_Targets
     FOR EACH ROW
		 BEGIN
			INSERT INTO  M_Targets_History (MonthNum,Country,Type,`value`,valuenNew,updatedAt,updatedBy)
			VALUES ( NEW.MonthNum, NEW.Country, NEW.Type, OLD.`value`, NEW.`value`, NOW(),USER());
		 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`daniel.palacios`@`%`*/ /*!50003 TRIGGER delete_Targets
     AFTER DELETE ON M_Targets
     FOR EACH ROW
		 BEGIN
			INSERT INTO  M_Targets_History (MonthNum,Country,Type,`value`,valuenNew,updatedAt,updatedBy)
			VALUES (OLD.MonthNum, OLD.Country, OLD.Type, OLD.`value`, null, NOW(),USER());
		 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping routines for database 'GlobalConfig'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:03:20
