-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: attribution_testing
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'attribution_testing'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `attribution_model_co`()
begin

select 'Attribution Co: ' as step, 'Start' as state, now() as time;

-- Campaigns Config

update attribution_testing.campaigns_config_co set data_source_value=substring_index(data_source_value, '\'', -1), region=substring_index(region, '\'', 1) where date is null;

update attribution_testing.campaigns_config_co set date=date(time_start) where date is null;

-- BOB/GA

delete from attribution_testing.bob_ga_co;

insert into attribution_testing.bob_ga_co(date, order_nr, channel, campaign, voucher_code, about_linio, new_customers) 
 select a.date, a.OrderNum, t.channel_group, t.campaign, t.coupon_code, sales_order.about_linio, 
if(NewReturning = 'New', 1, 0)
 from development_co_project.A_Master a
inner join marketing_report.channel_report_co t
on orderID = idSalesORder
inner join bob_live_co.sales_order
on sales_order.order_nr = a.OrderNum where a.date>=  '2013-11-01' group by t.OrderNum
order by a.date desc;

update attribution_testing.bob_ga_co b inner join SEM.transaction_id_co t on b.order_nr=t.transaction_id set b.ad_group=t.ad_group;

update attribution_testing.bob_ga_co set new_customers = 0 where new_customers is null;

-- Webtrekk

delete from attribution_testing.webtrekk_co;

insert into attribution_testing.webtrekk_co(date, time, order_nr, campaign) select distinct c.date, c.time, o.order_id, c.campaign from attribution_testing.getfullcampaign_co c, attribution_testing.getfullorders_co o where o.sid=c.sid group by c.date, c.campaign, o.order_id;

update attribution_testing.webtrekk_co w inner join attribution_testing.campaigns_config_co c on w.campaign=c.data_source_value set w.channel=c.category_2;

update attribution_testing.webtrekk_co w inner join attribution_testing.campaigns_config_co c on w.campaign=c.data_source_value set w.ad_group=c.category_4;

update attribution_testing.webtrekk_co set channel='Retargeting' where campaign like '%retargeting%' and channel is null;

update attribution_testing.webtrekk_co set channel='Social Media' where campaign like '%socialmedia%' and channel is null;

update attribution_testing.webtrekk_co set channel='Newsletter' where (campaign like '%Postal%' or campaign like '%Newsletter%') and channel is null;

update attribution_testing.webtrekk_co set ad_group= '' where ad_group is null;

update attribution_testing.webtrekk_co set channel= '' where channel is null;

update attribution_testing.webtrekk_co set campaign=substr(campaign, locate('=', campaign)+1);

set @attribution := 0;
set @var_order_nr := space(20);

create table attribution_testing.temporary_webtrekk1(
date date,
time datetime,
order_nr varchar(255),
campaign varchar(255),
ad_group varchar(255),
`range` int
);
create index temp on attribution_testing.temporary_webtrekk1(time, order_nr, campaign);

insert into attribution_testing.temporary_webtrekk1(date, time, order_nr, campaign, ad_group, `range`) select date, time, order_nr, campaign, ad_group, position from (select date, time, order_nr, campaign, ad_group, IF( order_nr != @var_order_nr , @attribution := 1, @attribution := @attribution + 1 ) AS position, @var_order_nr := order_nr from (select date, time, order_nr, campaign, ad_group from attribution_testing.webtrekk_co)a order by order_nr asc, time asc)a;

update attribution_testing.webtrekk_co w set position = (select `range` from attribution_testing.temporary_webtrekk1 t where t.order_nr=w.order_nr and t.campaign=w.campaign and t.time=w.time and t.ad_group=w.ad_group group by t.order_nr, t.campaign, t.ad_group, t.time);

drop table attribution_testing.temporary_webtrekk1;

-- One Row Customer Service

create table attribution_testing.`temporary_webtrekk1.1` select date, time, order_nr, channel, campaign, ad_group, 1 as position from attribution_testing.webtrekk_co where order_nr in (select order_nr from attribution_testing.webtrekk_co where channel='Customer Service');

create table attribution_testing.`temporary_webtrekk1.2` select order_nr, max(time) as time from attribution_testing.webtrekk_co where channel='Customer Service' group by order_nr; 

delete from attribution_testing.webtrekk_co where order_nr in (select order_nr from attribution_testing.`temporary_webtrekk1.2`);

insert into attribution_testing.webtrekk_co select a.* from attribution_testing.`temporary_webtrekk1.1` a, attribution_testing.`temporary_webtrekk1.2` b where a.order_nr=b.order_nr and a.time=b.time and a.channel='Customer Service';

drop table attribution_testing.`temporary_webtrekk1.1`;

drop table attribution_testing.`temporary_webtrekk1.2`;

-- No Branded Webtrekk

create table attribution_testing.`temporary_webtrekk1.5`(
time datetime,
order_nr varchar(255),
channel varchar(255),
campaign varchar(255),
highest_number int
);
create index order_nr on attribution_testing.`temporary_webtrekk1.5`(time, order_nr);

insert into attribution_testing.`temporary_webtrekk1.5` select max(time), order_nr, channel, campaign, max(position) from attribution_testing.webtrekk_co group by order_nr;

update attribution_testing.`temporary_webtrekk1.5` t set t.channel= (select w.channel from attribution_testing.webtrekk_co w where w.channel='SEO Brand' and w.order_nr=t.order_nr and w.time=t.time and t.highest_number !=1);

update attribution_testing.`temporary_webtrekk1.5` t set t.channel= (select w.channel from attribution_testing.webtrekk_co w where w.channel='SEM Brand' and w.order_nr=t.order_nr and w.time=t.time and t.highest_number !=1) where channel is null;

update attribution_testing.`temporary_webtrekk1.5` t set t.channel= (select w.channel from attribution_testing.webtrekk_co w where w.channel='Direct' and w.order_nr=t.order_nr and w.time=t.time and t.highest_number !=1) where channel is null;

delete attribution_testing.x.* from attribution_testing.webtrekk_co x inner join attribution_testing.`temporary_webtrekk1.5` t on t.order_nr=x.order_nr and t.time=x.time where x.channel in ('SEO Brand', 'SEM Brand', 'Direct');

drop table attribution_testing.`temporary_webtrekk1.5`;

-- Middle Recollecting Process

delete from attribution_testing.middle_recollecting_process_co;

create table attribution_testing.temporary_webtrekk2(
order_nr varchar(255),
in_webtrekk int
);
create index order_nr on attribution_testing.temporary_webtrekk2(order_nr);

insert into attribution_testing.temporary_webtrekk2(order_nr) select distinct t.order_nr from attribution_testing.bob_ga_co t;

update attribution_testing.temporary_webtrekk2 t inner join attribution_testing.webtrekk_co w on t.order_nr=w.order_nr set in_webtrekk=1;

update attribution_testing.temporary_webtrekk2 set in_webtrekk=0 where in_webtrekk is null;

insert into attribution_testing.middle_recollecting_process_co(date, order_nr, channel_webtrekk, campaign_webtrekk, ad_group_webtrekk, channel_google_analytics, campaign_google_analytics, ad_group_google_analytics, position, about_linio, voucher_code, new_customers) select w.date, w.order_nr, w.channel, w.campaign, w.ad_group, b.channel, b.campaign, b.ad_group, w.position, b.about_linio, b.voucher_code, b.new_customers from attribution_testing.webtrekk_co w, attribution_testing.temporary_webtrekk2 t, attribution_testing.bob_ga_co b where t.order_nr=w.order_nr and w.order_nr=b.order_nr and t.in_webtrekk=1;

insert into attribution_testing.middle_recollecting_process_co(date, order_nr, channel_google_analytics, campaign_google_analytics, ad_group_google_analytics, about_linio, voucher_code, new_customers) select b.date, b.order_nr, b.channel, b.campaign, b.ad_group, b.about_linio, b.voucher_code, b.new_customers from attribution_testing.temporary_webtrekk2 t, attribution_testing.bob_ga_co b where t.order_nr=b.order_nr and t.in_webtrekk=0;

drop table attribution_testing.temporary_webtrekk2;

update attribution_testing.middle_recollecting_process_co set position=1 where position is null;

create table attribution_testing.`temporary_webtrekk2.5` as select order_nr, count(*) as total_clicks from attribution_testing.middle_recollecting_process_co group by order_nr; 

create index order_nr on attribution_testing.`temporary_webtrekk2.5`(order_nr);

update attribution_testing.middle_recollecting_process_co m inner join attribution_testing.`temporary_webtrekk2.5` t on m.order_nr=t.order_nr set m.one_click=t.total_clicks;

update attribution_testing.middle_recollecting_process_co set one_click = 0 where one_click>1;

update attribution_testing.middle_recollecting_process_co set position = 1 where one_click=1; 

drop table attribution_testing.`temporary_webtrekk2.5`;

-- Target Channels

update attribution_testing.middle_recollecting_process_co m inner join attribution_testing.channel_name_campaign_co c on m.campaign_google_analytics=c.source_campaign or m.campaign_webtrekk=c.source_campaign set m.target_channel=c.target_channel;

update attribution_testing.middle_recollecting_process_co m inner join attribution_testing.webtrekk_fixes_co f on f.webtrekk=m.channel_webtrekk and f.google_analytics=m.channel_google_analytics set target_channel=channel_webtrekk, target_campaign=campaign_webtrekk, target_ad_group=ad_group_webtrekk where ga_include=1 and wt_include=1 and target_analytics='wt' and m.one_click=f.one_click;

update attribution_testing.middle_recollecting_process_co m inner join attribution_testing.webtrekk_fixes_co f on f.webtrekk!=m.channel_webtrekk and f.google_analytics=m.channel_google_analytics set target_channel=channel_webtrekk, target_campaign=campaign_webtrekk, target_ad_group=ad_group_webtrekk where ga_include=1 and wt_include=0 and target_analytics='wt' and m.one_click=f.one_click;

update attribution_testing.middle_recollecting_process_co m inner join attribution_testing.webtrekk_fixes_co f on f.webtrekk=m.channel_webtrekk and f.google_analytics!=m.channel_google_analytics set target_channel=channel_webtrekk, target_campaign=campaign_webtrekk, target_ad_group=ad_group_webtrekk where ga_include=0 and wt_include=1 and target_analytics='wt' and m.one_click=f.one_click;

update attribution_testing.middle_recollecting_process_co m inner join attribution_testing.webtrekk_fixes_co f on f.webtrekk=m.channel_webtrekk and f.google_analytics=m.channel_google_analytics set target_channel=channel_google_analytics, target_campaign=campaign_google_analytics, target_ad_group=ad_group_google_analytics where ga_include=1 and wt_include=1 and target_analytics='ga' and m.one_click=f.one_click;

update attribution_testing.middle_recollecting_process_co m inner join attribution_testing.webtrekk_fixes_co f on f.webtrekk!=m.channel_webtrekk and f.google_analytics=m.channel_google_analytics set target_channel=channel_google_analytics, target_campaign=campaign_google_analytics, target_ad_group=ad_group_google_analytics where ga_include=1 and wt_include=0 and target_analytics='ga' and m.one_click=f.one_click;

update attribution_testing.middle_recollecting_process_co m inner join attribution_testing.webtrekk_fixes_co f on f.webtrekk=m.channel_webtrekk and f.google_analytics!=m.channel_google_analytics set target_channel=channel_google_analytics, target_campaign=campaign_google_analytics, target_ad_group=ad_group_google_analytics where ga_include=0 and wt_include=1 and target_analytics='ga' and m.one_click=f.one_click;

update attribution_testing.middle_recollecting_process_co set target_channel=channel_webtrekk, target_campaign=campaign_webtrekk, target_ad_group=ad_group_webtrekk where channel_webtrekk is not null and target_channel is null;

update attribution_testing.middle_recollecting_process_co set target_channel=channel_google_analytics, target_campaign=campaign_google_analytics, target_ad_group=ad_group_google_analytics where channel_webtrekk is null and target_channel is null;

update attribution_testing.middle_recollecting_process_co set target_channel = about_linio where (target_channel='Customer Service' or target_channel='Tele Sales') and about_linio is not null;

update attribution_testing.middle_recollecting_process_co set target_channel='Non Identified' where target_channel is null;

-- Temporary Target

update attribution_testing.middle_recollecting_process_co set target_channel='Branded' where channel_webtrekk='SEM NonBrand' and channel_google_analytics='Branded' and campaign_google_analytics like 'brandb.%';

-- Voucher Last Click

create table attribution_testing.temporary_webtrekk3(
date date,
order_nr varchar(255),
channel varchar(255),
campaign varchar(255),
voucher_code varchar(255),
new_customers int,
highest_number int,
new_number int
);
create index voucher_code on attribution_testing.temporary_webtrekk3(order_nr, voucher_code);

insert into attribution_testing.temporary_webtrekk3(date, order_nr, channel, campaign, voucher_code, new_customers, highest_number) select max(date), order_nr, v.channel, v.campaign, voucher_code, new_customers, max(position) from attribution_testing.middle_recollecting_process_co m, attribution_testing.voucher_description_co v where m.voucher_code like concat(v.prefix ,'%') and v.overwrite=0 and voucher_code is not null group by order_nr;

update attribution_testing.temporary_webtrekk3 set new_number=highest_number+1; 

insert into attribution_testing.middle_recollecting_process_co(date, order_nr, target_channel, target_campaign, voucher_code, position, new_customers) select t.date, t.order_nr, t.channel, t.campaign, t.voucher_code, t.new_number, t.new_customers from attribution_testing.temporary_webtrekk3 t;

delete from attribution_testing.temporary_webtrekk3;

insert into attribution_testing.temporary_webtrekk3(date, order_nr, channel, campaign, voucher_code, new_customers, new_number) select date, order_nr, v.channel, v.campaign, voucher_code, new_customers, 1 from attribution_testing.middle_recollecting_process_co, attribution_testing.voucher_description_co v where voucher_code like concat(v.prefix ,'%') and v.overwrite=1 group by order_nr;

delete from attribution_testing.middle_recollecting_process_co where order_nr in (select order_nr from attribution_testing.temporary_webtrekk3);

insert into attribution_testing.middle_recollecting_process_co(date, order_nr, target_channel, target_campaign, voucher_code, new_customers, position) select date, order_nr, channel, campaign, voucher_code, new_customers, new_number from attribution_testing.temporary_webtrekk3; 

update attribution_testing.middle_recollecting_process_co set one_click=1 where one_click is null;

drop table attribution_testing.temporary_webtrekk3;

-- Partnerships One Row

create table attribution_testing.`temporary_webtrekk3.5` select date, order_nr, channel_webtrekk, channel_google_analytics, target_channel, campaign_webtrekk, campaign_google_analytics, target_campaign, ad_group_webtrekk, ad_group_google_analytics, target_ad_group, 1 as position, about_linio, voucher_code, new_customers, 1 as one_click from attribution_testing.middle_recollecting_process_co where target_channel='Partnerships' group by order_nr;

create index order_nr on attribution_testing.`temporary_webtrekk3.5`(order_nr);

delete from attribution_testing.middle_recollecting_process_co where order_nr in (select order_nr from attribution_testing.`temporary_webtrekk3.5`);

insert into attribution_testing.middle_recollecting_process_co select * from attribution_testing.`temporary_webtrekk3.5`;

drop table attribution_testing.`temporary_webtrekk3.5`;

-- Final Attribution Table

delete from attribution_testing.attribution_model_co;

insert into attribution_testing.attribution_model_co(date, order_nr, channel, campaign, ad_group, position, new_customers) select date, order_nr, target_channel, target_campaign, target_ad_group, position, new_customers from attribution_testing.middle_recollecting_process_co;

create table attribution_testing.temporary_webtrekk4(
order_nr varchar(255),
highest_number int
);
create index order_nr on attribution_testing.temporary_webtrekk4(order_nr, highest_number);

insert into attribution_testing.temporary_webtrekk4 select order_nr, max(position) from attribution_testing.attribution_model_co group by order_nr;

update attribution_testing.attribution_model_co a inner join attribution_testing.temporary_webtrekk4 w on a.order_nr=w.order_nr inner join attribution_testing.percent_attribution x on x.clicks=w.highest_number and x.numbers=a.position and x.new_customers=a.new_customers set a.percent=x.result;

drop table attribution_testing.temporary_webtrekk4;

update attribution_testing.attribution_model_co set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=SEM.week_iso(date);

-- Dictionary

update attribution_testing.attribution_model_co a inner join attribution_testing.dictionary d on a.channel=d.source_channel set a.channel=d.target_channel;

select 'Attribution Co: ' as step, 'Stop' as state, now() as time;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `attribution_model_mx`()
begin

select 'Attribution Mx: ' as step, 'Start' as state, now() as time;

-- Campaigns Config

update attribution_testing.campaigns_config_mx set data_source_value=substring_index(data_source_value, '\'', -1), region=substring_index(region, '\'', 1) where date is null;

update attribution_testing.campaigns_config_mx set date=date(time_start) where date is null;

-- BOB/GA

delete from attribution_testing.bob_ga_mx;

##insert into attribution_testing.webtrekk_mx(date, time, order_nr, campaign) select distinct c.date, c.time, v.eid, o.order_id, c.campaign from attribution_testing.getfullcampaign_mx c, attribution_testing.getfullorders_mx o, attribution_testing.getfullvisitors_mx v where o.sid=c.sid and o.sid=v.sid group by c.date, c.campaign, o.order_id, v.eid;

insert into attribution_testing.bob_ga_mx(date, order_nr, channel, campaign, voucher_code, about_linio, new_customers) select t.date, t.order_nr, t.channel_group, t.campaign, t.coupon_code, t.about_linio, case when t.new_customers != 0 then 1 else 0 end from production.tbl_order_detail t where t.date>=  '2013-11-01' group by t.order_nr;

update attribution_testing.bob_ga_mx b inner join SEM.transaction_id_mx t on b.order_nr=t.transaction_id set b.ad_group=t.ad_group;

update attribution_testing.bob_ga_mx set new_customers = 0 where new_customers is null;

-- Webtrekk

delete from attribution_testing.webtrekk_mx;

insert into attribution_testing.webtrekk_mx(date, time, order_nr, campaign) select distinct c.date, c.time, o.order_id, c.campaign from attribution_testing.getfullcampaign_mx c, attribution_testing.getfullorders_mx o where o.sid=c.sid group by c.date, c.campaign, o.order_id;

update attribution_testing.webtrekk_mx w inner join attribution_testing.campaigns_config_mx c on w.campaign=c.data_source_value set w.channel=c.category_2;

update attribution_testing.webtrekk_mx w inner join attribution_testing.campaigns_config_mx c on w.campaign=c.data_source_value set w.ad_group=c.category_4;

update attribution_testing.webtrekk_mx set channel='Retargeting' where campaign like '%retargeting%' and channel is null;

update attribution_testing.webtrekk_mx set channel='Social Media' where campaign like '%socialmedia%' and channel is null;

update attribution_testing.webtrekk_mx set channel='Newsletter' where (campaign like '%Postal%' or campaign like '%Newsletter%') and channel is null;

update attribution_testing.webtrekk_mx set ad_group= '' where ad_group is null;

update attribution_testing.webtrekk_mx set channel= '' where channel is null;

update attribution_testing.webtrekk_mx set campaign=substr(campaign, locate('=', campaign)+1);

set @attribution := 0;
set @var_order_nr := space(20);

create table attribution_testing.temporary_webtrekk1(
date date,
time datetime,
order_nr varchar(255),
campaign varchar(255),
ad_group varchar(255),
`range` int
);
create index temp on attribution_testing.temporary_webtrekk1(time, order_nr, campaign);

insert into attribution_testing.temporary_webtrekk1(date, time, order_nr, campaign, ad_group, `range`) select date, time, order_nr, campaign, ad_group, position from (select date, time, order_nr, campaign, ad_group, IF( order_nr != @var_order_nr , @attribution := 1, @attribution := @attribution + 1 ) AS position, @var_order_nr := order_nr from (select date, time, order_nr, campaign, ad_group from attribution_testing.webtrekk_mx)a order by order_nr asc, time asc)a;

update attribution_testing.webtrekk_mx w set position = (select `range` from attribution_testing.temporary_webtrekk1 t where t.order_nr=w.order_nr and t.campaign=w.campaign and t.time=w.time and t.ad_group=w.ad_group group by t.order_nr, t.campaign, t.ad_group, t.time);

drop table attribution_testing.temporary_webtrekk1;

-- One Row Customer Service

create table attribution_testing.`temporary_webtrekk1.1` select date, time, order_nr, channel, campaign, ad_group, 1 as position from attribution_testing.webtrekk_mx where order_nr in (select order_nr from attribution_testing.webtrekk_mx where channel='Customer Service');

create table attribution_testing.`temporary_webtrekk1.2` select order_nr, max(time) as time from attribution_testing.webtrekk_mx where channel='Customer Service' group by order_nr; 

delete from attribution_testing.webtrekk_mx where order_nr in (select order_nr from attribution_testing.`temporary_webtrekk1.2`);

insert into attribution_testing.webtrekk_mx select a.* from attribution_testing.`temporary_webtrekk1.1` a, attribution_testing.`temporary_webtrekk1.2` b where a.order_nr=b.order_nr and a.time=b.time and a.channel='Customer Service';

drop table attribution_testing.`temporary_webtrekk1.1`;

drop table attribution_testing.`temporary_webtrekk1.2`;

-- No Branded Webtrekk

create table attribution_testing.`temporary_webtrekk1.5`(
time datetime,
order_nr varchar(255),
channel varchar(255),
campaign varchar(255),
highest_number int
);
create index order_nr on attribution_testing.`temporary_webtrekk1.5`(time, order_nr);

insert into attribution_testing.`temporary_webtrekk1.5` select max(time), order_nr, channel, campaign, max(position) from attribution_testing.webtrekk_mx group by order_nr;

update attribution_testing.`temporary_webtrekk1.5` t set t.channel= (select w.channel from attribution_testing.webtrekk_mx w where w.channel='SEO Brand' and w.order_nr=t.order_nr and w.time=t.time and t.highest_number !=1);

update attribution_testing.`temporary_webtrekk1.5` t set t.channel= (select w.channel from attribution_testing.webtrekk_mx w where w.channel='SEM Brand' and w.order_nr=t.order_nr and w.time=t.time and t.highest_number !=1) where channel is null;

update attribution_testing.`temporary_webtrekk1.5` t set t.channel= (select w.channel from attribution_testing.webtrekk_mx w where w.channel='Direct' and w.order_nr=t.order_nr and w.time=t.time and t.highest_number !=1) where channel is null;

delete attribution_testing.x.* from attribution_testing.webtrekk_mx x inner join attribution_testing.`temporary_webtrekk1.5` t on t.order_nr=x.order_nr and t.time=x.time where x.channel in ('SEO Brand', 'SEM Brand', 'Direct');

drop table attribution_testing.`temporary_webtrekk1.5`;

-- Middle Recollecting Process

delete from attribution_testing.middle_recollecting_process_mx;

create table attribution_testing.temporary_webtrekk2(
order_nr varchar(255),
in_webtrekk int
);
create index order_nr on attribution_testing.temporary_webtrekk2(order_nr);

insert into attribution_testing.temporary_webtrekk2(order_nr) select distinct t.order_nr from attribution_testing.bob_ga_mx t;

update attribution_testing.temporary_webtrekk2 t inner join attribution_testing.webtrekk_mx w on t.order_nr=w.order_nr set in_webtrekk=1;

update attribution_testing.temporary_webtrekk2 set in_webtrekk=0 where in_webtrekk is null;

insert into attribution_testing.middle_recollecting_process_mx(date, order_nr, channel_webtrekk, campaign_webtrekk, ad_group_webtrekk, channel_google_analytics, campaign_google_analytics, ad_group_google_analytics, position, about_linio, voucher_code, new_customers) select w.date, w.order_nr, w.channel, w.campaign, w.ad_group, b.channel, b.campaign, b.ad_group, w.position, b.about_linio, b.voucher_code, b.new_customers from attribution_testing.webtrekk_mx w, attribution_testing.temporary_webtrekk2 t, attribution_testing.bob_ga_mx b where t.order_nr=w.order_nr and w.order_nr=b.order_nr and t.in_webtrekk=1;

insert into attribution_testing.middle_recollecting_process_mx(date, order_nr, channel_google_analytics, campaign_google_analytics, ad_group_google_analytics, about_linio, voucher_code, new_customers) select b.date, b.order_nr, b.channel, b.campaign, b.ad_group, b.about_linio, b.voucher_code, b.new_customers from attribution_testing.temporary_webtrekk2 t, attribution_testing.bob_ga_mx b where t.order_nr=b.order_nr and t.in_webtrekk=0;

drop table attribution_testing.temporary_webtrekk2;

update attribution_testing.middle_recollecting_process_mx set position=1 where position is null;

create table attribution_testing.`temporary_webtrekk2.5` as select order_nr, count(*) as total_clicks from attribution_testing.middle_recollecting_process_mx group by order_nr; 

create index order_nr on attribution_testing.`temporary_webtrekk2.5`(order_nr);

update attribution_testing.middle_recollecting_process_mx m inner join attribution_testing.`temporary_webtrekk2.5` t on m.order_nr=t.order_nr set m.one_click=t.total_clicks;

update attribution_testing.middle_recollecting_process_mx set one_click = 0 where one_click>1;

update attribution_testing.middle_recollecting_process_mx set position = 1 where one_click=1; 

drop table attribution_testing.`temporary_webtrekk2.5`;

-- Target Channels

update attribution_testing.middle_recollecting_process_mx m inner join attribution_testing.channel_name_campaign_mx c on m.campaign_google_analytics=c.source_campaign or m.campaign_webtrekk=c.source_campaign set m.target_channel=c.target_channel;

update attribution_testing.middle_recollecting_process_mx m inner join attribution_testing.webtrekk_fixes_mx f on f.webtrekk=m.channel_webtrekk and f.google_analytics=m.channel_google_analytics set target_channel=channel_webtrekk, target_campaign=campaign_webtrekk, target_ad_group=ad_group_webtrekk where ga_include=1 and wt_include=1 and target_analytics='wt' and m.one_click=f.one_click;

update attribution_testing.middle_recollecting_process_mx m inner join attribution_testing.webtrekk_fixes_mx f on f.webtrekk!=m.channel_webtrekk and f.google_analytics=m.channel_google_analytics set target_channel=channel_webtrekk, target_campaign=campaign_webtrekk, target_ad_group=ad_group_webtrekk where ga_include=1 and wt_include=0 and target_analytics='wt' and m.one_click=f.one_click;

update attribution_testing.middle_recollecting_process_mx m inner join attribution_testing.webtrekk_fixes_mx f on f.webtrekk=m.channel_webtrekk and f.google_analytics!=m.channel_google_analytics set target_channel=channel_webtrekk, target_campaign=campaign_webtrekk, target_ad_group=ad_group_webtrekk where ga_include=0 and wt_include=1 and target_analytics='wt' and m.one_click=f.one_click;

update attribution_testing.middle_recollecting_process_mx m inner join attribution_testing.webtrekk_fixes_mx f on f.webtrekk=m.channel_webtrekk and f.google_analytics=m.channel_google_analytics set target_channel=channel_google_analytics, target_campaign=campaign_google_analytics, target_ad_group=ad_group_google_analytics where ga_include=1 and wt_include=1 and target_analytics='ga' and m.one_click=f.one_click;

update attribution_testing.middle_recollecting_process_mx m inner join attribution_testing.webtrekk_fixes_mx f on f.webtrekk!=m.channel_webtrekk and f.google_analytics=m.channel_google_analytics set target_channel=channel_google_analytics, target_campaign=campaign_google_analytics, target_ad_group=ad_group_google_analytics where ga_include=1 and wt_include=0 and target_analytics='ga' and m.one_click=f.one_click;

update attribution_testing.middle_recollecting_process_mx m inner join attribution_testing.webtrekk_fixes_mx f on f.webtrekk=m.channel_webtrekk and f.google_analytics!=m.channel_google_analytics set target_channel=channel_google_analytics, target_campaign=campaign_google_analytics, target_ad_group=ad_group_google_analytics where ga_include=0 and wt_include=1 and target_analytics='ga' and m.one_click=f.one_click;

update attribution_testing.middle_recollecting_process_mx set target_channel=channel_webtrekk, target_campaign=campaign_webtrekk, target_ad_group=ad_group_webtrekk where channel_webtrekk is not null and target_channel is null;

update attribution_testing.middle_recollecting_process_mx set target_channel=channel_google_analytics, target_campaign=campaign_google_analytics, target_ad_group=ad_group_google_analytics where channel_webtrekk is null and target_channel is null;

update attribution_testing.middle_recollecting_process_mx set target_channel = about_linio where (target_channel='Customer Service' or target_channel='Tele Sales') and about_linio is not null;

update attribution_testing.middle_recollecting_process_mx set target_channel='Non Identified' where target_channel is null;

-- Temporary Target

update attribution_testing.middle_recollecting_process_mx set target_channel='Branded' where channel_webtrekk='SEM NonBrand' and channel_google_analytics='Branded' and campaign_google_analytics like 'brandb.%';

-- Voucher Last Click

create table attribution_testing.temporary_webtrekk3(
date date,
order_nr varchar(255),
channel varchar(255),
campaign varchar(255),
voucher_code varchar(255),
new_customers int,
highest_number int,
new_number int
);
create index voucher_code on attribution_testing.temporary_webtrekk3(order_nr, voucher_code);

insert into attribution_testing.temporary_webtrekk3(date, order_nr, channel, campaign, voucher_code, new_customers, highest_number) select max(date), order_nr, v.channel, v.campaign, voucher_code, new_customers, max(position) from attribution_testing.middle_recollecting_process_mx m, attribution_testing.voucher_description_mx v where m.voucher_code like concat(v.prefix ,'%') and v.overwrite=0 and voucher_code is not null group by order_nr;

update attribution_testing.temporary_webtrekk3 set new_number=highest_number+1; 

insert into attribution_testing.middle_recollecting_process_mx(date, order_nr, target_channel, target_campaign, voucher_code, position, new_customers) select t.date, t.order_nr, t.channel, t.campaign, t.voucher_code, t.new_number, t.new_customers from attribution_testing.temporary_webtrekk3 t;

delete from attribution_testing.temporary_webtrekk3;

insert into attribution_testing.temporary_webtrekk3(date, order_nr, channel, campaign, voucher_code, new_customers, new_number) select date, order_nr, v.channel, v.campaign, voucher_code, new_customers, 1 from attribution_testing.middle_recollecting_process_mx, attribution_testing.voucher_description_mx v where voucher_code like concat(v.prefix ,'%') and v.overwrite=1 group by order_nr;

delete from attribution_testing.middle_recollecting_process_mx where order_nr in (select order_nr from attribution_testing.temporary_webtrekk3);

insert into attribution_testing.middle_recollecting_process_mx(date, order_nr, target_channel, target_campaign, voucher_code, new_customers, position) select date, order_nr, channel, campaign, voucher_code, new_customers, new_number from attribution_testing.temporary_webtrekk3; 

update attribution_testing.middle_recollecting_process_mx set one_click=1 where one_click is null;

drop table attribution_testing.temporary_webtrekk3;

-- Partnerships One Row

create table attribution_testing.`temporary_webtrekk3.5` select date, order_nr, channel_webtrekk, channel_google_analytics, target_channel, campaign_webtrekk, campaign_google_analytics, target_campaign, ad_group_webtrekk, ad_group_google_analytics, target_ad_group, 1 as position, about_linio, voucher_code, new_customers, 1 as one_click from attribution_testing.middle_recollecting_process_mx where target_channel='Partnerships' group by order_nr;

create index order_nr on attribution_testing.`temporary_webtrekk3.5`(order_nr);

delete from attribution_testing.middle_recollecting_process_mx where order_nr in (select order_nr from attribution_testing.`temporary_webtrekk3.5`);

insert into attribution_testing.middle_recollecting_process_mx select * from attribution_testing.`temporary_webtrekk3.5`;

drop table attribution_testing.`temporary_webtrekk3.5`;

-- Final Attribution Table

delete from attribution_testing.attribution_model_mx;

insert into attribution_testing.attribution_model_mx(date, order_nr, channel, campaign, ad_group, position, new_customers) select date, order_nr, target_channel, target_campaign, target_ad_group, position, new_customers from attribution_testing.middle_recollecting_process_mx;

create table attribution_testing.temporary_webtrekk4(
order_nr varchar(255),
highest_number int
);
create index order_nr on attribution_testing.temporary_webtrekk4(order_nr, highest_number);

insert into attribution_testing.temporary_webtrekk4 select order_nr, max(position) from attribution_testing.attribution_model_mx group by order_nr;

update attribution_testing.attribution_model_mx a inner join attribution_testing.temporary_webtrekk4 w on a.order_nr=w.order_nr inner join attribution_testing.percent_attribution x on x.clicks=w.highest_number and x.numbers=a.position and x.new_customers=a.new_customers set a.percent=x.result;

drop table attribution_testing.temporary_webtrekk4;

update attribution_testing.attribution_model_mx set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=SEM.week_iso(date);

-- Dictionary

update attribution_testing.attribution_model_mx a inner join attribution_testing.dictionary d on a.channel=d.source_channel set a.channel=d.target_channel;

select 'Attribution Mx: ' as step, 'Stop' as state, now() as time;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `attribution_model_pe`()
begin

select 'Attribution Pe: ' as step, 'Start' as state, now() as time;

-- Campaigns Config

update attribution_testing.campaigns_config_pe set data_source_value=substring_index(data_source_value, '\'', -1), region=substring_index(region, '\'', 1) where date is null;

update attribution_testing.campaigns_config_pe set date=date(time_start) where date is null;

-- BOB/GA

delete from attribution_testing.bob_ga_pe;

insert into attribution_testing.bob_ga_pe(date, order_nr, channel, campaign, voucher_code, about_linio, new_customers) select t.date, t.order_nr, t.channel_group, t.campaign, t.coupon_code, t.about_linio, case when t.new_customers != 0 then 1 else 0 end from production_pe.tbl_order_detail t where t.date>=  '2013-11-01' group by t.order_nr;

update attribution_testing.bob_ga_pe b inner join SEM.transaction_id_pe t on b.order_nr=t.transaction_id set b.ad_group=t.ad_group;

update attribution_testing.bob_ga_pe set new_customers = 0 where new_customers is null;

-- Webtrekk

delete from attribution_testing.webtrekk_pe;

insert into attribution_testing.webtrekk_pe(date, time, order_nr, campaign) select distinct c.date, c.time, o.order_id, c.campaign from attribution_testing.getfullcampaign_pe c, attribution_testing.getfullorders_pe o where o.sid=c.sid group by c.date, c.campaign, o.order_id;

update attribution_testing.webtrekk_pe w inner join attribution_testing.campaigns_config_pe c on w.campaign=c.data_source_value set w.channel=c.category_2;

update attribution_testing.webtrekk_pe w inner join attribution_testing.campaigns_config_pe c on w.campaign=c.data_source_value set w.ad_group=c.category_4;

update attribution_testing.webtrekk_pe set channel='Retargeting' where campaign like '%retargeting%' and channel is null;

update attribution_testing.webtrekk_pe set channel='Social Media' where campaign like '%socialmedia%' and channel is null;

update attribution_testing.webtrekk_pe set channel='Newsletter' where (campaign like '%Postal%' or campaign like '%Newsletter%') and channel is null;

update attribution_testing.webtrekk_pe set ad_group= '' where ad_group is null;

update attribution_testing.webtrekk_pe set channel= '' where channel is null;

update attribution_testing.webtrekk_pe set campaign=substr(campaign, locate('=', campaign)+1);

set @attribution := 0;
set @var_order_nr := space(20);

create table attribution_testing.temporary_webtrekk1(
date date,
time datetime,
order_nr varchar(255),
campaign varchar(255),
ad_group varchar(255),
`range` int
);
create index temp on attribution_testing.temporary_webtrekk1(time, order_nr, campaign);

insert into attribution_testing.temporary_webtrekk1(date, time, order_nr, campaign, ad_group, `range`) select date, time, order_nr, campaign, ad_group, position from (select date, time, order_nr, campaign, ad_group, IF( order_nr != @var_order_nr , @attribution := 1, @attribution := @attribution + 1 ) AS position, @var_order_nr := order_nr from (select date, time, order_nr, campaign, ad_group from attribution_testing.webtrekk_pe)a order by order_nr asc, time asc)a;

update attribution_testing.webtrekk_pe w set position = (select `range` from attribution_testing.temporary_webtrekk1 t where t.order_nr=w.order_nr and t.campaign=w.campaign and t.time=w.time and t.ad_group=w.ad_group group by t.order_nr, t.campaign, t.ad_group, t.time);

drop table attribution_testing.temporary_webtrekk1;

-- One Row Customer Service

create table attribution_testing.`temporary_webtrekk1.1` select date, time, order_nr, channel, campaign, ad_group, 1 as position from attribution_testing.webtrekk_pe where order_nr in (select order_nr from attribution_testing.webtrekk_pe where channel='Customer Service');

create table attribution_testing.`temporary_webtrekk1.2` select order_nr, max(time) as time from attribution_testing.webtrekk_pe where channel='Customer Service' group by order_nr; 

delete from attribution_testing.webtrekk_pe where order_nr in (select order_nr from attribution_testing.`temporary_webtrekk1.2`);

insert into attribution_testing.webtrekk_pe select a.* from attribution_testing.`temporary_webtrekk1.1` a, attribution_testing.`temporary_webtrekk1.2` b where a.order_nr=b.order_nr and a.time=b.time and a.channel='Customer Service';

drop table attribution_testing.`temporary_webtrekk1.1`;

drop table attribution_testing.`temporary_webtrekk1.2`;

-- No Branded Webtrekk

create table attribution_testing.`temporary_webtrekk1.5`(
time datetime,
order_nr varchar(255),
channel varchar(255),
campaign varchar(255),
highest_number int
);
create index order_nr on attribution_testing.`temporary_webtrekk1.5`(time, order_nr);

insert into attribution_testing.`temporary_webtrekk1.5` select max(time), order_nr, channel, campaign, max(position) from attribution_testing.webtrekk_pe group by order_nr;

update attribution_testing.`temporary_webtrekk1.5` t set t.channel= (select w.channel from attribution_testing.webtrekk_pe w where w.channel='SEO Brand' and w.order_nr=t.order_nr and w.time=t.time and t.highest_number !=1);

update attribution_testing.`temporary_webtrekk1.5` t set t.channel= (select w.channel from attribution_testing.webtrekk_pe w where w.channel='SEM Brand' and w.order_nr=t.order_nr and w.time=t.time and t.highest_number !=1) where channel is null;

update attribution_testing.`temporary_webtrekk1.5` t set t.channel= (select w.channel from attribution_testing.webtrekk_pe w where w.channel='Direct' and w.order_nr=t.order_nr and w.time=t.time and t.highest_number !=1) where channel is null;

delete attribution_testing.x.* from attribution_testing.webtrekk_pe x inner join attribution_testing.`temporary_webtrekk1.5` t on t.order_nr=x.order_nr and t.time=x.time where x.channel in ('SEO Brand', 'SEM Brand', 'Direct');

drop table attribution_testing.`temporary_webtrekk1.5`;

-- Middle Recollecting Process

delete from attribution_testing.middle_recollecting_process_pe;

create table attribution_testing.temporary_webtrekk2(
order_nr varchar(255),
in_webtrekk int
);
create index order_nr on attribution_testing.temporary_webtrekk2(order_nr);

insert into attribution_testing.temporary_webtrekk2(order_nr) select distinct t.order_nr from attribution_testing.bob_ga_pe t;

update attribution_testing.temporary_webtrekk2 t inner join attribution_testing.webtrekk_pe w on t.order_nr=w.order_nr set in_webtrekk=1;

update attribution_testing.temporary_webtrekk2 set in_webtrekk=0 where in_webtrekk is null;

insert into attribution_testing.middle_recollecting_process_pe(date, order_nr, channel_webtrekk, campaign_webtrekk, ad_group_webtrekk, channel_google_analytics, campaign_google_analytics, ad_group_google_analytics, position, about_linio, voucher_code, new_customers) select w.date, w.order_nr, w.channel, w.campaign, w.ad_group, b.channel, b.campaign, b.ad_group, w.position, b.about_linio, b.voucher_code, b.new_customers from attribution_testing.webtrekk_pe w, attribution_testing.temporary_webtrekk2 t, attribution_testing.bob_ga_pe b where t.order_nr=w.order_nr and w.order_nr=b.order_nr and t.in_webtrekk=1;

insert into attribution_testing.middle_recollecting_process_pe(date, order_nr, channel_google_analytics, campaign_google_analytics, ad_group_google_analytics, about_linio, voucher_code, new_customers) select b.date, b.order_nr, b.channel, b.campaign, b.ad_group, b.about_linio, b.voucher_code, b.new_customers from attribution_testing.temporary_webtrekk2 t, attribution_testing.bob_ga_pe b where t.order_nr=b.order_nr and t.in_webtrekk=0;

drop table attribution_testing.temporary_webtrekk2;

update attribution_testing.middle_recollecting_process_pe set position=1 where position is null;

create table attribution_testing.`temporary_webtrekk2.5` as select order_nr, count(*) as total_clicks from attribution_testing.middle_recollecting_process_pe group by order_nr; 

create index order_nr on attribution_testing.`temporary_webtrekk2.5`(order_nr);

update attribution_testing.middle_recollecting_process_pe m inner join attribution_testing.`temporary_webtrekk2.5` t on m.order_nr=t.order_nr set m.one_click=t.total_clicks;

update attribution_testing.middle_recollecting_process_pe set one_click = 0 where one_click>1;

update attribution_testing.middle_recollecting_process_pe set position = 1 where one_click=1; 

drop table attribution_testing.`temporary_webtrekk2.5`;

-- Target Channels

update attribution_testing.middle_recollecting_process_pe m inner join attribution_testing.channel_name_campaign_pe c on m.campaign_google_analytics=c.source_campaign or m.campaign_webtrekk=c.source_campaign set m.target_channel=c.target_channel;

update attribution_testing.middle_recollecting_process_pe m inner join attribution_testing.webtrekk_fixes_pe f on f.webtrekk=m.channel_webtrekk and f.google_analytics=m.channel_google_analytics set target_channel=channel_webtrekk, target_campaign=campaign_webtrekk, target_ad_group=ad_group_webtrekk where ga_include=1 and wt_include=1 and target_analytics='wt' and m.one_click=f.one_click;

update attribution_testing.middle_recollecting_process_pe m inner join attribution_testing.webtrekk_fixes_pe f on f.webtrekk!=m.channel_webtrekk and f.google_analytics=m.channel_google_analytics set target_channel=channel_webtrekk, target_campaign=campaign_webtrekk, target_ad_group=ad_group_webtrekk where ga_include=1 and wt_include=0 and target_analytics='wt' and m.one_click=f.one_click;

update attribution_testing.middle_recollecting_process_pe m inner join attribution_testing.webtrekk_fixes_pe f on f.webtrekk=m.channel_webtrekk and f.google_analytics!=m.channel_google_analytics set target_channel=channel_webtrekk, target_campaign=campaign_webtrekk, target_ad_group=ad_group_webtrekk where ga_include=0 and wt_include=1 and target_analytics='wt' and m.one_click=f.one_click;

update attribution_testing.middle_recollecting_process_pe m inner join attribution_testing.webtrekk_fixes_pe f on f.webtrekk=m.channel_webtrekk and f.google_analytics=m.channel_google_analytics set target_channel=channel_google_analytics, target_campaign=campaign_google_analytics, target_ad_group=ad_group_google_analytics where ga_include=1 and wt_include=1 and target_analytics='ga' and m.one_click=f.one_click;

update attribution_testing.middle_recollecting_process_pe m inner join attribution_testing.webtrekk_fixes_pe f on f.webtrekk!=m.channel_webtrekk and f.google_analytics=m.channel_google_analytics set target_channel=channel_google_analytics, target_campaign=campaign_google_analytics, target_ad_group=ad_group_google_analytics where ga_include=1 and wt_include=0 and target_analytics='ga' and m.one_click=f.one_click;

update attribution_testing.middle_recollecting_process_pe m inner join attribution_testing.webtrekk_fixes_pe f on f.webtrekk=m.channel_webtrekk and f.google_analytics!=m.channel_google_analytics set target_channel=channel_google_analytics, target_campaign=campaign_google_analytics, target_ad_group=ad_group_google_analytics where ga_include=0 and wt_include=1 and target_analytics='ga' and m.one_click=f.one_click;

update attribution_testing.middle_recollecting_process_pe set target_channel=channel_webtrekk, target_campaign=campaign_webtrekk, target_ad_group=ad_group_webtrekk where channel_webtrekk is not null and target_channel is null;

update attribution_testing.middle_recollecting_process_pe set target_channel=channel_google_analytics, target_campaign=campaign_google_analytics, target_ad_group=ad_group_google_analytics where channel_webtrekk is null and target_channel is null;

update attribution_testing.middle_recollecting_process_pe set target_channel = about_linio where (target_channel='Customer Service' or target_channel='Tele Sales') and about_linio is not null;

update attribution_testing.middle_recollecting_process_pe set target_channel='Non Identified' where target_channel is null;

-- Temporary Target

update attribution_testing.middle_recollecting_process_pe set target_channel='Branded' where channel_webtrekk='SEM NonBrand' and channel_google_analytics='Branded' and campaign_google_analytics like 'brandb.%';

-- Voucher Last Click

create table attribution_testing.temporary_webtrekk3(
date date,
order_nr varchar(255),
channel varchar(255),
campaign varchar(255),
voucher_code varchar(255),
new_customers int,
highest_number int,
new_number int
);
create index voucher_code on attribution_testing.temporary_webtrekk3(order_nr, voucher_code);

insert into attribution_testing.temporary_webtrekk3(date, order_nr, channel, campaign, voucher_code, new_customers, highest_number) select max(date), order_nr, v.channel, v.campaign, voucher_code, new_customers, max(position) from attribution_testing.middle_recollecting_process_pe m, attribution_testing.voucher_description_pe v where m.voucher_code like concat(v.prefix ,'%') and v.overwrite=0 and voucher_code is not null group by order_nr;

update attribution_testing.temporary_webtrekk3 set new_number=highest_number+1; 

insert into attribution_testing.middle_recollecting_process_pe(date, order_nr, target_channel, target_campaign, voucher_code, position, new_customers) select t.date, t.order_nr, t.channel, t.campaign, t.voucher_code, t.new_number, t.new_customers from attribution_testing.temporary_webtrekk3 t;

delete from attribution_testing.temporary_webtrekk3;

insert into attribution_testing.temporary_webtrekk3(date, order_nr, channel, campaign, voucher_code, new_customers, new_number) select date, order_nr, v.channel, v.campaign, voucher_code, new_customers, 1 from attribution_testing.middle_recollecting_process_pe, attribution_testing.voucher_description_pe v where voucher_code like concat(v.prefix ,'%') and v.overwrite=1 group by order_nr;

delete from attribution_testing.middle_recollecting_process_pe where order_nr in (select order_nr from attribution_testing.temporary_webtrekk3);

insert into attribution_testing.middle_recollecting_process_pe(date, order_nr, target_channel, target_campaign, voucher_code, new_customers, position) select date, order_nr, channel, campaign, voucher_code, new_customers, new_number from attribution_testing.temporary_webtrekk3; 

update attribution_testing.middle_recollecting_process_pe set one_click=1 where one_click is null;

drop table attribution_testing.temporary_webtrekk3;

-- Partnerships One Row

create table attribution_testing.`temporary_webtrekk3.5` select date, order_nr, channel_webtrekk, channel_google_analytics, target_channel, campaign_webtrekk, campaign_google_analytics, target_campaign, ad_group_webtrekk, ad_group_google_analytics, target_ad_group, 1 as position, about_linio, voucher_code, new_customers, 1 as one_click from attribution_testing.middle_recollecting_process_pe where target_channel='Partnerships' group by order_nr;

create index order_nr on attribution_testing.`temporary_webtrekk3.5`(order_nr);

delete from attribution_testing.middle_recollecting_process_pe where order_nr in (select order_nr from attribution_testing.`temporary_webtrekk3.5`);

insert into attribution_testing.middle_recollecting_process_pe select * from attribution_testing.`temporary_webtrekk3.5`;

drop table attribution_testing.`temporary_webtrekk3.5`;

-- Final Attribution Table

delete from attribution_testing.attribution_model_pe;

insert into attribution_testing.attribution_model_pe(date, order_nr, channel, campaign, ad_group, position, new_customers) select date, order_nr, target_channel, target_campaign, target_ad_group, position, new_customers from attribution_testing.middle_recollecting_process_pe;

create table attribution_testing.temporary_webtrekk4(
order_nr varchar(255),
highest_number int
);
create index order_nr on attribution_testing.temporary_webtrekk4(order_nr, highest_number);

insert into attribution_testing.temporary_webtrekk4 select order_nr, max(position) from attribution_testing.attribution_model_pe group by order_nr;

update attribution_testing.attribution_model_pe a inner join attribution_testing.temporary_webtrekk4 w on a.order_nr=w.order_nr inner join attribution_testing.percent_attribution x on x.clicks=w.highest_number and x.numbers=a.position and x.new_customers=a.new_customers set a.percent=x.result;

drop table attribution_testing.temporary_webtrekk4;

update attribution_testing.attribution_model_pe set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=SEM.week_iso(date);

-- Dictionary

update attribution_testing.attribution_model_pe a inner join attribution_testing.dictionary d on a.channel=d.source_channel set a.channel=d.target_channel;

select 'Attribution Pe: ' as step, 'Stop' as state, now() as time;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `attribution_model_ve`()
begin

select 'Attribution Ve: ' as step, 'Start' as state, now() as time;

-- Campaigns Config

update attribution_testing.campaigns_config_ve set data_source_value=substring_index(data_source_value, '\'', -1), region=substring_index(region, '\'', 1) where date is null;

update attribution_testing.campaigns_config_ve set date=date(time_start) where date is null;

-- BOB/GA

delete from attribution_testing.bob_ga_ve;

insert into attribution_testing.bob_ga_ve(date, order_nr, channel, campaign, voucher_code, about_linio, new_customers) select t.date, t.order_nr, t.channel_group, t.campaign, t.coupon_code, t.about_linio, case when t.new_customers != 0 then 1 else 0 end from production_ve.tbl_order_detail t where t.date>=  '2013-11-01' group by t.order_nr;

update attribution_testing.bob_ga_ve b inner join SEM.transaction_id_ve t on b.order_nr=t.transaction_id set b.ad_group=t.ad_group;

update attribution_testing.bob_ga_ve set new_customers = 0 where new_customers is null;

-- Webtrekk

delete from attribution_testing.webtrekk_ve;

insert into attribution_testing.webtrekk_ve(date, time, order_nr, campaign) select distinct c.date, c.time, o.order_id, c.campaign from attribution_testing.getfullcampaign_ve c, attribution_testing.getfullorders_ve o where o.sid=c.sid group by c.date, c.campaign, o.order_id;

update attribution_testing.webtrekk_ve w inner join attribution_testing.campaigns_config_ve c on w.campaign=c.data_source_value set w.channel=c.category_2;

update attribution_testing.webtrekk_ve w inner join attribution_testing.campaigns_config_ve c on w.campaign=c.data_source_value set w.ad_group=c.category_4;

update attribution_testing.webtrekk_ve set channel='Retargeting' where campaign like '%retargeting%' and channel is null;

update attribution_testing.webtrekk_ve set channel='Social Media' where campaign like '%socialmedia%' and channel is null;

update attribution_testing.webtrekk_ve set channel='Newsletter' where (campaign like '%Postal%' or campaign like '%Newsletter%') and channel is null;

update attribution_testing.webtrekk_ve set ad_group= '' where ad_group is null;

update attribution_testing.webtrekk_ve set channel= '' where channel is null;

update attribution_testing.webtrekk_ve set campaign=substr(campaign, locate('=', campaign)+1);

set @attribution := 0;
set @var_order_nr := space(20);

create table attribution_testing.temporary_webtrekk1(
date date,
time datetime,
order_nr varchar(255),
campaign varchar(255),
ad_group varchar(255),
`range` int
);
create index temp on attribution_testing.temporary_webtrekk1(time, order_nr, campaign);

insert into attribution_testing.temporary_webtrekk1(date, time, order_nr, campaign, ad_group, `range`) select date, time, order_nr, campaign, ad_group, position from (select date, time, order_nr, campaign, ad_group, IF( order_nr != @var_order_nr , @attribution := 1, @attribution := @attribution + 1 ) AS position, @var_order_nr := order_nr from (select date, time, order_nr, campaign, ad_group from attribution_testing.webtrekk_ve)a order by order_nr asc, time asc)a;

update attribution_testing.webtrekk_ve w set position = (select `range` from attribution_testing.temporary_webtrekk1 t where t.order_nr=w.order_nr and t.campaign=w.campaign and t.time=w.time and t.ad_group=w.ad_group group by t.order_nr, t.campaign, t.ad_group, t.time);

drop table attribution_testing.temporary_webtrekk1;

-- One Row Customer Service

create table attribution_testing.`temporary_webtrekk1.1` select date, time, order_nr, channel, campaign, ad_group, 1 as position from attribution_testing.webtrekk_ve where order_nr in (select order_nr from attribution_testing.webtrekk_ve where channel='Customer Service');

create table attribution_testing.`temporary_webtrekk1.2` select order_nr, max(time) as time from attribution_testing.webtrekk_ve where channel='Customer Service' group by order_nr; 

delete from attribution_testing.webtrekk_ve where order_nr in (select order_nr from attribution_testing.`temporary_webtrekk1.2`);

insert into attribution_testing.webtrekk_ve select a.* from attribution_testing.`temporary_webtrekk1.1` a, attribution_testing.`temporary_webtrekk1.2` b where a.order_nr=b.order_nr and a.time=b.time and a.channel='Customer Service';

drop table attribution_testing.`temporary_webtrekk1.1`;

drop table attribution_testing.`temporary_webtrekk1.2`;

-- No Branded Webtrekk

create table attribution_testing.`temporary_webtrekk1.5`(
time datetime,
order_nr varchar(255),
channel varchar(255),
campaign varchar(255),
highest_number int
);
create index order_nr on attribution_testing.`temporary_webtrekk1.5`(time, order_nr);

insert into attribution_testing.`temporary_webtrekk1.5` select max(time), order_nr, channel, campaign, max(position) from attribution_testing.webtrekk_ve group by order_nr;

update attribution_testing.`temporary_webtrekk1.5` t set t.channel= (select w.channel from attribution_testing.webtrekk_ve w where w.channel='SEO Brand' and w.order_nr=t.order_nr and w.time=t.time and t.highest_number !=1);

update attribution_testing.`temporary_webtrekk1.5` t set t.channel= (select w.channel from attribution_testing.webtrekk_ve w where w.channel='SEM Brand' and w.order_nr=t.order_nr and w.time=t.time and t.highest_number !=1) where channel is null;

update attribution_testing.`temporary_webtrekk1.5` t set t.channel= (select w.channel from attribution_testing.webtrekk_ve w where w.channel='Direct' and w.order_nr=t.order_nr and w.time=t.time and t.highest_number !=1) where channel is null;

delete attribution_testing.x.* from attribution_testing.webtrekk_ve x inner join attribution_testing.`temporary_webtrekk1.5` t on t.order_nr=x.order_nr and t.time=x.time where x.channel in ('SEO Brand', 'SEM Brand', 'Direct');

drop table attribution_testing.`temporary_webtrekk1.5`;

-- Middle Recollecting Process

delete from attribution_testing.middle_recollecting_process_ve;

create table attribution_testing.temporary_webtrekk2(
order_nr varchar(255),
in_webtrekk int
);
create index order_nr on attribution_testing.temporary_webtrekk2(order_nr);

insert into attribution_testing.temporary_webtrekk2(order_nr) select distinct t.order_nr from attribution_testing.bob_ga_ve t;

update attribution_testing.temporary_webtrekk2 t inner join attribution_testing.webtrekk_ve w on t.order_nr=w.order_nr set in_webtrekk=1;

update attribution_testing.temporary_webtrekk2 set in_webtrekk=0 where in_webtrekk is null;

insert into attribution_testing.middle_recollecting_process_ve(date, order_nr, channel_webtrekk, campaign_webtrekk, ad_group_webtrekk, channel_google_analytics, campaign_google_analytics, ad_group_google_analytics, position, about_linio, voucher_code, new_customers) select w.date, w.order_nr, w.channel, w.campaign, w.ad_group, b.channel, b.campaign, b.ad_group, w.position, b.about_linio, b.voucher_code, b.new_customers from attribution_testing.webtrekk_ve w, attribution_testing.temporary_webtrekk2 t, attribution_testing.bob_ga_ve b where t.order_nr=w.order_nr and w.order_nr=b.order_nr and t.in_webtrekk=1;

insert into attribution_testing.middle_recollecting_process_ve(date, order_nr, channel_google_analytics, campaign_google_analytics, ad_group_google_analytics, about_linio, voucher_code, new_customers) select b.date, b.order_nr, b.channel, b.campaign, b.ad_group, b.about_linio, b.voucher_code, b.new_customers from attribution_testing.temporary_webtrekk2 t, attribution_testing.bob_ga_ve b where t.order_nr=b.order_nr and t.in_webtrekk=0;

drop table attribution_testing.temporary_webtrekk2;

update attribution_testing.middle_recollecting_process_ve set position=1 where position is null;

create table attribution_testing.`temporary_webtrekk2.5` as select order_nr, count(*) as total_clicks from attribution_testing.middle_recollecting_process_ve group by order_nr; 

create index order_nr on attribution_testing.`temporary_webtrekk2.5`(order_nr);

update attribution_testing.middle_recollecting_process_ve m inner join attribution_testing.`temporary_webtrekk2.5` t on m.order_nr=t.order_nr set m.one_click=t.total_clicks;

update attribution_testing.middle_recollecting_process_ve set one_click = 0 where one_click>1;

update attribution_testing.middle_recollecting_process_ve set position = 1 where one_click=1; 

drop table attribution_testing.`temporary_webtrekk2.5`;

-- Target Channels

update attribution_testing.middle_recollecting_process_ve m inner join attribution_testing.channel_name_campaign_ve c on m.campaign_google_analytics=c.source_campaign or m.campaign_webtrekk=c.source_campaign set m.target_channel=c.target_channel;

update attribution_testing.middle_recollecting_process_ve m inner join attribution_testing.webtrekk_fixes_ve f on f.webtrekk=m.channel_webtrekk and f.google_analytics=m.channel_google_analytics set target_channel=channel_webtrekk, target_campaign=campaign_webtrekk, target_ad_group=ad_group_webtrekk where ga_include=1 and wt_include=1 and target_analytics='wt' and m.one_click=f.one_click;

update attribution_testing.middle_recollecting_process_ve m inner join attribution_testing.webtrekk_fixes_ve f on f.webtrekk!=m.channel_webtrekk and f.google_analytics=m.channel_google_analytics set target_channel=channel_webtrekk, target_campaign=campaign_webtrekk, target_ad_group=ad_group_webtrekk where ga_include=1 and wt_include=0 and target_analytics='wt' and m.one_click=f.one_click;

update attribution_testing.middle_recollecting_process_ve m inner join attribution_testing.webtrekk_fixes_ve f on f.webtrekk=m.channel_webtrekk and f.google_analytics!=m.channel_google_analytics set target_channel=channel_webtrekk, target_campaign=campaign_webtrekk, target_ad_group=ad_group_webtrekk where ga_include=0 and wt_include=1 and target_analytics='wt' and m.one_click=f.one_click;

update attribution_testing.middle_recollecting_process_ve m inner join attribution_testing.webtrekk_fixes_ve f on f.webtrekk=m.channel_webtrekk and f.google_analytics=m.channel_google_analytics set target_channel=channel_google_analytics, target_campaign=campaign_google_analytics, target_ad_group=ad_group_google_analytics where ga_include=1 and wt_include=1 and target_analytics='ga' and m.one_click=f.one_click;

update attribution_testing.middle_recollecting_process_ve m inner join attribution_testing.webtrekk_fixes_ve f on f.webtrekk!=m.channel_webtrekk and f.google_analytics=m.channel_google_analytics set target_channel=channel_google_analytics, target_campaign=campaign_google_analytics, target_ad_group=ad_group_google_analytics where ga_include=1 and wt_include=0 and target_analytics='ga' and m.one_click=f.one_click;

update attribution_testing.middle_recollecting_process_ve m inner join attribution_testing.webtrekk_fixes_ve f on f.webtrekk=m.channel_webtrekk and f.google_analytics!=m.channel_google_analytics set target_channel=channel_google_analytics, target_campaign=campaign_google_analytics, target_ad_group=ad_group_google_analytics where ga_include=0 and wt_include=1 and target_analytics='ga' and m.one_click=f.one_click;

update attribution_testing.middle_recollecting_process_ve set target_channel=channel_webtrekk, target_campaign=campaign_webtrekk, target_ad_group=ad_group_webtrekk where channel_webtrekk is not null and target_channel is null;

update attribution_testing.middle_recollecting_process_ve set target_channel=channel_google_analytics, target_campaign=campaign_google_analytics, target_ad_group=ad_group_google_analytics where channel_webtrekk is null and target_channel is null;

update attribution_testing.middle_recollecting_process_ve set target_channel = about_linio where (target_channel='Customer Service' or target_channel='Tele Sales') and about_linio is not null;

update attribution_testing.middle_recollecting_process_ve set target_channel='Non Identified' where target_channel is null;

-- Temporary Target

update attribution_testing.middle_recollecting_process_ve set target_channel='Branded' where channel_webtrekk='SEM NonBrand' and channel_google_analytics='Branded' and campaign_google_analytics like 'brandb.%';

-- Voucher Last Click

create table attribution_testing.temporary_webtrekk3(
date date,
order_nr varchar(255),
channel varchar(255),
campaign varchar(255),
voucher_code varchar(255),
new_customers int,
highest_number int,
new_number int
);
create index voucher_code on attribution_testing.temporary_webtrekk3(order_nr, voucher_code);

insert into attribution_testing.temporary_webtrekk3(date, order_nr, channel, campaign, voucher_code, new_customers, highest_number) select max(date), order_nr, v.channel, v.campaign, voucher_code, new_customers, max(position) from attribution_testing.middle_recollecting_process_ve m, attribution_testing.voucher_description_ve v where m.voucher_code like concat(v.prefix ,'%') and v.overwrite=0 and voucher_code is not null group by order_nr;

update attribution_testing.temporary_webtrekk3 set new_number=highest_number+1; 

insert into attribution_testing.middle_recollecting_process_ve(date, order_nr, target_channel, target_campaign, voucher_code, position, new_customers) select t.date, t.order_nr, t.channel, t.campaign, t.voucher_code, t.new_number, t.new_customers from attribution_testing.temporary_webtrekk3 t;

delete from attribution_testing.temporary_webtrekk3;

insert into attribution_testing.temporary_webtrekk3(date, order_nr, channel, campaign, voucher_code, new_customers, new_number) select date, order_nr, v.channel, v.campaign, voucher_code, new_customers, 1 from attribution_testing.middle_recollecting_process_ve, attribution_testing.voucher_description_ve v where voucher_code like concat(v.prefix ,'%') and v.overwrite=1 group by order_nr;

delete from attribution_testing.middle_recollecting_process_ve where order_nr in (select order_nr from attribution_testing.temporary_webtrekk3);

insert into attribution_testing.middle_recollecting_process_ve(date, order_nr, target_channel, target_campaign, voucher_code, new_customers, position) select date, order_nr, channel, campaign, voucher_code, new_customers, new_number from attribution_testing.temporary_webtrekk3; 

update attribution_testing.middle_recollecting_process_ve set one_click=1 where one_click is null;

drop table attribution_testing.temporary_webtrekk3;

-- Partnerships One Row

create table attribution_testing.`temporary_webtrekk3.5` select date, order_nr, channel_webtrekk, channel_google_analytics, target_channel, campaign_webtrekk, campaign_google_analytics, target_campaign, ad_group_webtrekk, ad_group_google_analytics, target_ad_group, 1 as position, about_linio, voucher_code, new_customers, 1 as one_click from attribution_testing.middle_recollecting_process_ve where target_channel='Partnerships' group by order_nr;

create index order_nr on attribution_testing.`temporary_webtrekk3.5`(order_nr);

delete from attribution_testing.middle_recollecting_process_ve where order_nr in (select order_nr from attribution_testing.`temporary_webtrekk3.5`);

insert into attribution_testing.middle_recollecting_process_ve select * from attribution_testing.`temporary_webtrekk3.5`;

drop table attribution_testing.`temporary_webtrekk3.5`;

-- Final Attribution Table

delete from attribution_testing.attribution_model_ve;

insert into attribution_testing.attribution_model_ve(date, order_nr, channel, campaign, ad_group, position, new_customers) select date, order_nr, target_channel, target_campaign, target_ad_group, position, new_customers from attribution_testing.middle_recollecting_process_ve;

create table attribution_testing.temporary_webtrekk4(
order_nr varchar(255),
highest_number int
);
create index order_nr on attribution_testing.temporary_webtrekk4(order_nr, highest_number);

insert into attribution_testing.temporary_webtrekk4 select order_nr, max(position) from attribution_testing.attribution_model_ve group by order_nr;

update attribution_testing.attribution_model_ve a inner join attribution_testing.temporary_webtrekk4 w on a.order_nr=w.order_nr inner join attribution_testing.percent_attribution x on x.clicks=w.highest_number and x.numbers=a.position and x.new_customers=a.new_customers set a.percent=x.result;

drop table attribution_testing.temporary_webtrekk4;

update attribution_testing.attribution_model_ve set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=SEM.week_iso(date);

-- Dictionary

update attribution_testing.attribution_model_ve a inner join attribution_testing.dictionary d on a.channel=d.source_channel set a.channel=d.target_channel;

select 'Attribution Ve: ' as step, 'Stop' as state, now() as time;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `attribution_report_co`()
begin

-- Colombia

set @days=180;

-- Attribution Compensation

drop table if exists attribution_testing.compensation_attribution_co;

create table attribution_testing.compensation_attribution_co as select date, channel, 0 as flag from attribution_testing.attribution_model_co group by date, channel;

create index date on attribution_testing.compensation_attribution_co(date);
create index channel on attribution_testing.compensation_attribution_co(channel);

create table attribution_testing.compensation_global_co as select date, channel_group as channel from marketing_report.global_report where country='Colombia' group by date, channel_group;

create index date on attribution_testing.compensation_global_co(date);
create index channel on attribution_testing.compensation_global_co(channel);

update attribution_testing.compensation_attribution_co a inner join attribution_testing.compensation_global_co g on a.date=g.date and a.channel=g.channel set a.flag=1;

insert into marketing_report.global_report(country, yrmonth, week, date, weekday, channel_group, channel) select 'Colombia', concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), production.week_iso(date) as week, date, case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end, channel, channel from attribution_testing.compensation_attribution_co where flag=0 and datediff(curdate(), date)<@days; 

drop table attribution_testing.compensation_attribution_co;

drop table attribution_testing.compensation_global_co;

-- Global Attribution

delete from attribution_testing.global_attribution;

insert into attribution_testing.global_attribution(country, date, channel_group, gross_orders, net_orders, gross_revenue, net_revenue, new_customers_gross, new_customers) select 'Colombia', gross.date, gross.channel, gross.orders, net.orders, gross.revenue, net.revenue, gross.new_customers, net.new_customers from (select t.date, a.channel, sum(t.gross_orders*a.percent) as orders, sum(ifnull(unit_price_after_vat*a.percent,0)-ifnull(coupon_money_after_vat*a.percent,0)+ifnull(shipping_fee_after_vat*a.percent,0)) as revenue, sum(new_customers_gross*a.percent) as new_customers from production_co.tbl_order_detail t, attribution_testing.attribution_model_co a where obc=1 and t.yrmonth>=201311 and a.order_nr=t.order_nr group by date, a.channel)gross left join (select t.date, a.channel, sum(t.net_orders*a.percent) as orders, sum(ifnull(unit_price_after_vat*a.percent,0)-ifnull(coupon_money_after_vat*a.percent,0)+ifnull(shipping_fee_after_vat*a.percent,0)) as revenue, sum(t.new_customers*a.percent) as new_customers from production_co.tbl_order_detail t, attribution_testing.attribution_model_co a where oac=1 and returned=0 and rejected=0 and t.yrmonth>=201311 and a.order_nr=t.order_nr group by date, a.channel)net on gross.date=net.date and gross.channel=net.channel;

update attribution_testing.global_attribution set week=production_co.week_iso(date), weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end, yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));

update attribution_testing.global_attribution s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr where s.country = 'Colombia' and r.country='COL';

update attribution_testing.global_attribution a inner join (select date, channel_group, sum(visits) as visits, sum(cost) as cost from marketing_report.global_report where country='Colombia' group by date, channel_group)g on a.date=g.date and a.channel_group=g.channel_group set a.visits=g.visits, a.cost=g.cost where a.country='Colombia';

-- No Tele Sales

create table attribution_testing.temporary_channel(
date date,
channel_group varchar(255),
gross_orders float not null default 0,
net_orders float not null default 0,
gross_revenue float not null default 0,
net_revenue float not null default 0,
`PC1.5` float not null default 0,
new_customers_gross float not null default 0,
new_customers float not null default 0,
cost float not null default 0,
visits float not null default 0
);

insert into attribution_testing.temporary_channel(date, channel_group) select date, channel_group from attribution_testing.global_attribution where channel_group!='Tele Sales';

create table attribution_testing.percent(
date date,
channel_group varchar(255),
visits int,
total_visits int,
percent float
);

insert into attribution_testing.percent(date, channel_group, visits) select date, channel_group, visits from attribution_testing.global_attribution where channel_group!='Tele Sales';

create table attribution_testing.total(
date date,
visits int
);

insert into attribution_testing.total select date, sum(visits) from attribution_testing.global_attribution where channel_group!='Tele Sales' group by date;

update attribution_testing.percent p inner join attribution_testing.total t on p.date=t.date set p.total_visits=t.visits where channel_group!='Tele Sales';

drop table attribution_testing.total;

update attribution_testing.percent set percent=visits/total_visits where channel_group!='Tele Sales';

create table attribution_testing.temporary_telesales(
date date,
gross_orders float not null default 0,
net_orders float not null default 0,
gross_revenue float not null default 0,
net_revenue float not null default 0,
`PC1.5` float not null default 0,
new_customers_gross float not null default 0,
new_customers float not null default 0,
cost float not null default 0,
visits int not null default 0
);

insert into attribution_testing.temporary_telesales select date, gross_orders, net_orders, gross_revenue, net_revenue, `PC1.5`, new_customers_gross, new_customers, cost, visits from attribution_testing.global_attribution where channel_group='Tele Sales';

update attribution_testing.temporary_telesales t inner join attribution_testing.temporary_channel c on t.date=c.date inner join attribution_testing.percent p on p.date=c.date and p.channel_group=c.channel_group set c.gross_orders=t.gross_orders*p.percent, c.net_orders=t.net_orders*p.percent, c.gross_revenue=t.gross_revenue*p.percent, c.net_revenue=t.net_revenue*p.percent, c.`PC1.5`=t.`PC1.5`*p.percent, c.new_customers_gross=t.new_customers_gross*p.percent, c.new_customers=t.new_customers*p.percent, c.cost=t.cost*p.percent, c.visits=t.visits*p.percent;

drop table attribution_testing.percent;

drop table attribution_testing.temporary_telesales;

delete from attribution_testing.global_attribution where channel_group='Tele Sales';

update attribution_testing.global_attribution g inner join attribution_testing.temporary_channel t on g.date=t.date and g.channel_group=t.channel_group set g.gross_orders=g.gross_orders+t.gross_orders, g.net_orders=g.net_orders+t.net_orders, g.gross_revenue=g.gross_revenue+t.gross_revenue, g.net_revenue=g.net_revenue+t.net_revenue, g.net_revenue=g.net_revenue+t.net_revenue, g.`PC1.5`=g.`PC1.5`+t.`PC1.5`, g.new_customers_gross=g.new_customers_gross+t.new_customers_gross, g.new_customers=g.new_customers+t.new_customers, g.cost=g.cost+t.cost, g.visits=g.visits+t.visits;

drop table attribution_testing.temporary_channel;

-- No Non Identified

create table attribution_testing.temporary_channel(
date date,
channel_group varchar(255),
gross_orders float not null default 0,
net_orders float not null default 0,
gross_revenue float not null default 0,
net_revenue float not null default 0,
`PC1.5` float not null default 0,
new_customers_gross float not null default 0,
new_customers float not null default 0,
cost float not null default 0,
visits float not null default 0
);

insert into attribution_testing.temporary_channel(date, channel_group) select date, channel_group from attribution_testing.global_attribution where channel_group!='Non Identified';

create table attribution_testing.percent(
date date,
channel_group varchar(255),
visits int,
total_visits int,
percent float
);

insert into attribution_testing.percent(date, channel_group, visits) select date, channel_group, visits from attribution_testing.global_attribution where channel_group!='Non Identified';

create table attribution_testing.total(
date date,
visits int
);

insert into attribution_testing.total select date, sum(visits) from attribution_testing.global_attribution where channel_group!='Non Identified' group by date;

update attribution_testing.percent p inner join attribution_testing.total t on p.date=t.date set p.total_visits=t.visits where channel_group!='Non Identified';

drop table attribution_testing.total;

update attribution_testing.percent set percent=visits/total_visits where channel_group!='Non Identified';

create table attribution_testing.temporary_telesales(
date date,
gross_orders float not null default 0,
net_orders float not null default 0,
gross_revenue float not null default 0,
net_revenue float not null default 0,
`PC1.5` float not null default 0,
new_customers_gross float not null default 0,
new_customers float not null default 0,
cost float not null default 0,
visits int not null default 0
);

insert into attribution_testing.temporary_telesales select date, gross_orders, net_orders, gross_revenue, net_revenue, `PC1.5`,new_customers_gross, new_customers, cost, visits from attribution_testing.global_attribution where channel_group='Non Identified';

update attribution_testing.temporary_telesales t inner join attribution_testing.temporary_channel c on t.date=c.date inner join attribution_testing.percent p on p.date=c.date and p.channel_group=c.channel_group set c.gross_orders=t.gross_orders*p.percent, c.net_orders=t.net_orders*p.percent, c.gross_revenue=t.gross_revenue*p.percent, c.net_revenue=t.net_revenue*p.percent, c.`PC1.5`=t.`PC1.5`*p.percent, c.new_customers_gross=t.new_customers_gross*p.percent, c.new_customers=t.new_customers*p.percent, c.cost=t.cost*p.percent, c.visits=t.visits*p.percent;

drop table attribution_testing.percent;

drop table attribution_testing.temporary_telesales;

delete from attribution_testing.global_attribution where channel_group='Non Identified';

update attribution_testing.global_attribution g inner join attribution_testing.temporary_channel t on g.date=t.date and g.channel_group=t.channel_group set g.gross_orders=g.gross_orders+t.gross_orders, g.net_orders=g.net_orders+t.net_orders, g.gross_revenue=g.gross_revenue+t.gross_revenue, g.net_revenue=g.net_revenue+t.net_revenue, g.net_revenue=g.net_revenue+t.net_revenue, g.`PC1.5`=g.`PC1.5`+t.`PC1.5`, g.new_customers_gross=g.new_customers_gross+t.new_customers_gross, g.new_customers=g.new_customers+t.new_customers, g.cost=g.cost+t.cost, g.visits=g.visits+t.visits;

drop table attribution_testing.temporary_channel;

-- Global Marketing Attribution

create table attribution_testing.temporary_report select date, channel_group, count(*) as channel_number, sum(net_orders+1) as net_orders, sum(net_revenue) as net_revenue, sum(new_customers+1) as new_customers from marketing_report.global_report where country='Colombia' group by date, channel_group order by date desc;

create index channel_group on attribution_testing.temporary_report(date, channel_group);

##update marketing_report.global_report g inner join attribution_testing.temporary_report t on g.date=t.date and g.channel_group=t.channel_group inner join attribution_testing.global_attribution a on a.date=g.date and g.channel_group=a.channel_group set g.a_orders=a.net_orders/channel_number, g.a_revenue=a.net_revenue/channel_number, g.a_new_customers=a.new_customers/channel_number, g.a_cost=a.cost/channel_number where g.country='Colombia' and a.country='Colombia';

update marketing_report.global_report g 
inner join attribution_testing.temporary_report t 
on g.date=t.date and g.channel_group=t.channel_group 
inner join attribution_testing.global_attribution a 
on a.date=g.date and g.channel_group=a.channel_group 
set g.a_orders=a.net_orders*((g.net_orders+1)/t.net_orders),
 g.a_revenue=a.net_revenue*((g.net_orders+1)/t.net_orders), 
g.a_new_customers=a.new_customers*((g.new_customers+1)/t.new_customers)
 where g.country='Colombia' and a.country='Colombia'; 

update marketing_report.global_report g set a_revenue=net_revenue where channel_group='Advertising' and country='Colombia';

drop table attribution_testing.temporary_report;

update marketing_report.global_report set a_orders=0 where a_orders is null;

update marketing_report.global_report set a_revenue=0 where a_revenue is null;

update marketing_report.global_report set a_new_customers=0 where a_new_customers is null;


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `attribution_report_mx`()
begin

-- Mexico

set @days=180;

-- Attribution Compensation

drop table if exists attribution_testing.compensation_attribution_mx;

create table attribution_testing.compensation_attribution_mx as select date, channel, 0 as flag from attribution_testing.attribution_model_mx group by date, channel;

create index date on attribution_testing.compensation_attribution_mx(date);
create index channel on attribution_testing.compensation_attribution_mx(channel);

create table attribution_testing.compensation_global_mx as select date, channel_group as channel from marketing_report.global_report where country='Mexico' group by date, channel_group;

create index date on attribution_testing.compensation_global_mx(date);
create index channel on attribution_testing.compensation_global_mx(channel);

update attribution_testing.compensation_attribution_mx a inner join attribution_testing.compensation_global_mx g on a.date=g.date and a.channel=g.channel set a.flag=1;

insert into marketing_report.global_report(country, yrmonth, week, date, weekday, channel_group, channel) select 'Mexico', concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), production.week_iso(date) as week, date, case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end, channel, channel from attribution_testing.compensation_attribution_mx where flag=0 and datediff(curdate(), date)<@days; 

drop table attribution_testing.compensation_attribution_mx;

drop table attribution_testing.compensation_global_mx;

-- Global Attribution

delete from attribution_testing.global_attribution;

insert into attribution_testing.global_attribution(country, date, channel_group, gross_orders, net_orders, gross_revenue, net_revenue, new_customers_gross, new_customers) select 'Mexico', gross.date, gross.channel, gross.orders, net.orders, gross.revenue, net.revenue, gross.new_customers, net.new_customers from (select t.date, a.channel, sum(t.gross_orders*a.percent) as orders, sum(ifnull(unit_price_after_vat*a.percent,0)-ifnull(coupon_money_after_vat*a.percent,0)+ifnull(shipping_fee_after_vat*a.percent,0)) as revenue, sum(new_customers_gross*a.percent) as new_customers from production.tbl_order_detail t, attribution_testing.attribution_model_mx a where obc=1 and t.yrmonth>=201311 and a.order_nr=t.order_nr group by date, a.channel)gross left join (select t.date, a.channel, sum(t.net_orders*a.percent) as orders, sum(ifnull(unit_price_after_vat*a.percent,0)-ifnull(coupon_money_after_vat*a.percent,0)+ifnull(shipping_fee_after_vat*a.percent,0)) as revenue, sum(t.new_customers*a.percent) as new_customers from production.tbl_order_detail t, attribution_testing.attribution_model_mx a where oac=1 and t.yrmonth>=201311 and a.order_nr=t.order_nr group by date, a.channel)net on gross.date=net.date and gross.channel=net.channel;

update attribution_testing.global_attribution set week=production.week_iso(date), weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end, yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));

update attribution_testing.global_attribution s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr where s.country = 'Mexico' and r.country='MEX';

update attribution_testing.global_attribution a inner join (select date, channel_group, sum(visits) as visits, sum(cost) as cost from marketing_report.global_report where country='Mexico' group by date, channel_group)g on a.date=g.date and a.channel_group=g.channel_group set a.visits=g.visits, a.cost=g.cost where a.country='Mexico';

-- No Tele Sales

create table attribution_testing.temporary_channel(
date date,
channel_group varchar(255),
gross_orders float not null default 0,
net_orders float not null default 0,
gross_revenue float not null default 0,
net_revenue float not null default 0,
`PC1.5` float not null default 0,
new_customers_gross float not null default 0,
new_customers float not null default 0,
cost float not null default 0,
visits float not null default 0
);

insert into attribution_testing.temporary_channel(date, channel_group) select date, channel_group from attribution_testing.global_attribution where channel_group!='Tele Sales';

create table attribution_testing.percent(
date date,
channel_group varchar(255),
visits int,
total_visits int,
percent float
);

insert into attribution_testing.percent(date, channel_group, visits) select date, channel_group, visits from attribution_testing.global_attribution where channel_group!='Tele Sales';

create table attribution_testing.total(
date date,
visits int
);

insert into attribution_testing.total select date, sum(visits) from attribution_testing.global_attribution where channel_group!='Tele Sales' group by date;

update attribution_testing.percent p inner join attribution_testing.total t on p.date=t.date set p.total_visits=t.visits where channel_group!='Tele Sales';

drop table attribution_testing.total;

update attribution_testing.percent set percent=visits/total_visits where channel_group!='Tele Sales';

create table attribution_testing.temporary_telesales(
date date,
gross_orders float not null default 0,
net_orders float not null default 0,
gross_revenue float not null default 0,
net_revenue float not null default 0,
`PC1.5` float not null default 0,
new_customers_gross float not null default 0,
new_customers float not null default 0,
cost float not null default 0,
visits int not null default 0
);

insert into attribution_testing.temporary_telesales select date, gross_orders, net_orders, gross_revenue, net_revenue, `PC1.5`, new_customers_gross, new_customers, cost, visits from attribution_testing.global_attribution where channel_group='Tele Sales';

update attribution_testing.temporary_telesales t inner join attribution_testing.temporary_channel c on t.date=c.date inner join attribution_testing.percent p on p.date=c.date and p.channel_group=c.channel_group set c.gross_orders=t.gross_orders*p.percent, c.net_orders=t.net_orders*p.percent, c.gross_revenue=t.gross_revenue*p.percent, c.net_revenue=t.net_revenue*p.percent, c.`PC1.5`=t.`PC1.5`*p.percent, c.new_customers_gross=t.new_customers_gross*p.percent, c.new_customers=t.new_customers*p.percent, c.cost=t.cost*p.percent, c.visits=t.visits*p.percent;

drop table attribution_testing.percent;

drop table attribution_testing.temporary_telesales;

delete from attribution_testing.global_attribution where channel_group='Tele Sales';

update attribution_testing.global_attribution g inner join attribution_testing.temporary_channel t on g.date=t.date and g.channel_group=t.channel_group set g.gross_orders=g.gross_orders+t.gross_orders, g.net_orders=g.net_orders+t.net_orders, g.gross_revenue=g.gross_revenue+t.gross_revenue, g.net_revenue=g.net_revenue+t.net_revenue, g.net_revenue=g.net_revenue+t.net_revenue, g.`PC1.5`=g.`PC1.5`+t.`PC1.5`, g.new_customers_gross=g.new_customers_gross+t.new_customers_gross, g.new_customers=g.new_customers+t.new_customers, g.cost=g.cost+t.cost, g.visits=g.visits+t.visits;

drop table attribution_testing.temporary_channel;

-- No Non Identified

create table attribution_testing.temporary_channel(
date date,
channel_group varchar(255),
gross_orders float not null default 0,
net_orders float not null default 0,
gross_revenue float not null default 0,
net_revenue float not null default 0,
`PC1.5` float not null default 0,
new_customers_gross float not null default 0,
new_customers float not null default 0,
cost float not null default 0,
visits float not null default 0
);

insert into attribution_testing.temporary_channel(date, channel_group) select date, channel_group from attribution_testing.global_attribution where channel_group!='Non Identified';

create table attribution_testing.percent(
date date,
channel_group varchar(255),
visits int,
total_visits int,
percent float
);

insert into attribution_testing.percent(date, channel_group, visits) select date, channel_group, visits from attribution_testing.global_attribution where channel_group!='Non Identified';

create table attribution_testing.total(
date date,
visits int
);

insert into attribution_testing.total select date, sum(visits) from attribution_testing.global_attribution where channel_group!='Non Identified' group by date;

update attribution_testing.percent p inner join attribution_testing.total t on p.date=t.date set p.total_visits=t.visits where channel_group!='Non Identified';

drop table attribution_testing.total;

update attribution_testing.percent set percent=visits/total_visits where channel_group!='Non Identified';

create table attribution_testing.temporary_telesales(
date date,
gross_orders float not null default 0,
net_orders float not null default 0,
gross_revenue float not null default 0,
net_revenue float not null default 0,
`PC1.5` float not null default 0,
new_customers_gross float not null default 0,
new_customers float not null default 0,
cost float not null default 0,
visits int not null default 0
);

insert into attribution_testing.temporary_telesales select date, gross_orders, net_orders, gross_revenue, net_revenue, `PC1.5`,new_customers_gross, new_customers, cost, visits from attribution_testing.global_attribution where channel_group='Non Identified';

update attribution_testing.temporary_telesales t inner join attribution_testing.temporary_channel c on t.date=c.date inner join attribution_testing.percent p on p.date=c.date and p.channel_group=c.channel_group set c.gross_orders=t.gross_orders*p.percent, c.net_orders=t.net_orders*p.percent, c.gross_revenue=t.gross_revenue*p.percent, c.net_revenue=t.net_revenue*p.percent, c.`PC1.5`=t.`PC1.5`*p.percent, c.new_customers_gross=t.new_customers_gross*p.percent, c.new_customers=t.new_customers*p.percent, c.cost=t.cost*p.percent, c.visits=t.visits*p.percent;

drop table attribution_testing.percent;

drop table attribution_testing.temporary_telesales;

delete from attribution_testing.global_attribution where channel_group='Non Identified';

update attribution_testing.global_attribution g inner join attribution_testing.temporary_channel t on g.date=t.date and g.channel_group=t.channel_group set g.gross_orders=g.gross_orders+t.gross_orders, g.net_orders=g.net_orders+t.net_orders, g.gross_revenue=g.gross_revenue+t.gross_revenue, g.net_revenue=g.net_revenue+t.net_revenue, g.net_revenue=g.net_revenue+t.net_revenue, g.`PC1.5`=g.`PC1.5`+t.`PC1.5`, g.new_customers_gross=g.new_customers_gross+t.new_customers_gross, g.new_customers=g.new_customers+t.new_customers, g.cost=g.cost+t.cost, g.visits=g.visits+t.visits;

drop table attribution_testing.temporary_channel;

-- Global Marketing Attribution

create table attribution_testing.temporary_report select date, channel_group, count(*) as channel_number, sum(net_orders+1) as net_orders, sum(net_revenue) as net_revenue, sum(new_customers+1) as new_customers from marketing_report.global_report where country='Mexico' group by date, channel_group order by date desc;

create index channel_group on attribution_testing.temporary_report(date, channel_group);

##update marketing_report.global_report g inner join attribution_testing.temporary_report t on g.date=t.date and g.channel_group=t.channel_group inner join attribution_testing.global_attribution a on a.date=g.date and g.channel_group=a.channel_group set g.a_orders=a.net_orders*(g.net_orders/t.net_orders), g.a_revenue=a.net_revenue*(g.net_revenue/t.net_revenue), g.a_new_customers=a.new_customers*(g.new_customers/t.new_customers) where g.country='Mexico' and a.country='Mexico'; 

update marketing_report.global_report g inner join attribution_testing.temporary_report t on g.date=t.date and g.channel_group=t.channel_group inner join attribution_testing.global_attribution a on a.date=g.date and g.channel_group=a.channel_group set g.a_orders=a.net_orders*((g.net_orders+1)/t.net_orders), g.a_revenue=a.net_revenue*((g.net_orders+1)/t.net_orders), g.a_new_customers=a.new_customers*((g.new_customers+1)/t.new_customers) where g.country='Mexico' and a.country='Mexico'; 

drop table attribution_testing.temporary_report;

update marketing_report.global_report set a_orders=0 where a_orders is null;

update marketing_report.global_report set a_revenue=0 where a_revenue is null;

update marketing_report.global_report set a_new_customers=0 where a_new_customers is null;


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `attribution_report_pe`()
begin

-- Peru

set @days=180;

-- Attribution Compensation

drop table if exists attribution_testing.compensation_attribution_pe;

create table attribution_testing.compensation_attribution_pe as select date, channel, 0 as flag from attribution_testing.attribution_model_pe group by date, channel;

create index date on attribution_testing.compensation_attribution_pe(date);
create index channel on attribution_testing.compensation_attribution_pe(channel);

create table attribution_testing.compensation_global_pe as select date, channel_group as channel from marketing_report.global_report where country='Peru' group by date, channel_group;

create index date on attribution_testing.compensation_global_pe(date);
create index channel on attribution_testing.compensation_global_pe(channel);

update attribution_testing.compensation_attribution_pe a inner join attribution_testing.compensation_global_pe g on a.date=g.date and a.channel=g.channel set a.flag=1;

insert into marketing_report.global_report(country, yrmonth, week, date, weekday, channel_group, channel) select 'Peru', concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), production.week_iso(date) as week, date, case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end, channel, channel from attribution_testing.compensation_attribution_pe where flag=0 and datediff(curdate(), date)<@days; 

drop table attribution_testing.compensation_attribution_pe;

drop table attribution_testing.compensation_global_pe;

-- Global Attribution

delete from attribution_testing.global_attribution;

insert into attribution_testing.global_attribution(country, date, channel_group, gross_orders, net_orders, gross_revenue, net_revenue, new_customers_gross, new_customers) select 'Peru', gross.date, gross.channel, gross.orders, net.orders, gross.revenue, net.revenue, gross.new_customers, net.new_customers from (select t.date, a.channel, sum(t.gross_orders*a.percent) as orders, sum(ifnull(unit_price_after_vat*a.percent,0)-ifnull(coupon_money_after_vat*a.percent,0)+ifnull(shipping_fee_after_vat*a.percent,0)) as revenue, sum(new_customers_gross*a.percent) as new_customers from production_pe.tbl_order_detail t, attribution_testing.attribution_model_pe a where obc=1 and t.yrmonth>=201311 and a.order_nr=t.order_nr group by date, a.channel)gross left join (select t.date, a.channel, sum(t.net_orders*a.percent) as orders, sum(ifnull(unit_price_after_vat*a.percent,0)-ifnull(coupon_money_after_vat*a.percent,0)+ifnull(shipping_fee_after_vat*a.percent,0)) as revenue, sum(t.new_customers*a.percent) as new_customers from production_pe.tbl_order_detail t, attribution_testing.attribution_model_pe a where oac=1 and t.yrmonth>=201311 and a.order_nr=t.order_nr group by date, a.channel)net on gross.date=net.date and gross.channel=net.channel;

update attribution_testing.global_attribution set week=production_pe.week_iso(date), weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end, yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));

update attribution_testing.global_attribution s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr where s.country = 'Peru' and r.country='PER';

update attribution_testing.global_attribution a inner join (select date, channel_group, sum(visits) as visits, sum(cost) as cost from marketing_report.global_report where country='Peru' group by date, channel_group)g on a.date=g.date and a.channel_group=g.channel_group set a.visits=g.visits, a.cost=g.cost where a.country='Peru';

-- No Tele Sales

create table attribution_testing.temporary_channel(
date date,
channel_group varchar(255),
gross_orders float not null default 0,
net_orders float not null default 0,
gross_revenue float not null default 0,
net_revenue float not null default 0,
`PC1.5` float not null default 0,
new_customers_gross float not null default 0,
new_customers float not null default 0,
cost float not null default 0,
visits float not null default 0
);

insert into attribution_testing.temporary_channel(date, channel_group) select date, channel_group from attribution_testing.global_attribution where channel_group!='Tele Sales';

create table attribution_testing.percent(
date date,
channel_group varchar(255),
visits int,
total_visits int,
percent float
);

insert into attribution_testing.percent(date, channel_group, visits) select date, channel_group, visits from attribution_testing.global_attribution where channel_group!='Tele Sales';

create table attribution_testing.total(
date date,
visits int
);

insert into attribution_testing.total select date, sum(visits) from attribution_testing.global_attribution where channel_group!='Tele Sales' group by date;

update attribution_testing.percent p inner join attribution_testing.total t on p.date=t.date set p.total_visits=t.visits where channel_group!='Tele Sales';

drop table attribution_testing.total;

update attribution_testing.percent set percent=visits/total_visits where channel_group!='Tele Sales';

create table attribution_testing.temporary_telesales(
date date,
gross_orders float not null default 0,
net_orders float not null default 0,
gross_revenue float not null default 0,
net_revenue float not null default 0,
`PC1.5` float not null default 0,
new_customers_gross float not null default 0,
new_customers float not null default 0,
cost float not null default 0,
visits int not null default 0
);

insert into attribution_testing.temporary_telesales select date, gross_orders, net_orders, gross_revenue, net_revenue, `PC1.5`, new_customers_gross, new_customers, cost, visits from attribution_testing.global_attribution where channel_group='Tele Sales';

update attribution_testing.temporary_telesales t inner join attribution_testing.temporary_channel c on t.date=c.date inner join attribution_testing.percent p on p.date=c.date and p.channel_group=c.channel_group set c.gross_orders=t.gross_orders*p.percent, c.net_orders=t.net_orders*p.percent, c.gross_revenue=t.gross_revenue*p.percent, c.net_revenue=t.net_revenue*p.percent, c.`PC1.5`=t.`PC1.5`*p.percent, c.new_customers_gross=t.new_customers_gross*p.percent, c.new_customers=t.new_customers*p.percent, c.cost=t.cost*p.percent, c.visits=t.visits*p.percent;

drop table attribution_testing.percent;

drop table attribution_testing.temporary_telesales;

delete from attribution_testing.global_attribution where channel_group='Tele Sales';

update attribution_testing.global_attribution g inner join attribution_testing.temporary_channel t on g.date=t.date and g.channel_group=t.channel_group set g.gross_orders=g.gross_orders+t.gross_orders, g.net_orders=g.net_orders+t.net_orders, g.gross_revenue=g.gross_revenue+t.gross_revenue, g.net_revenue=g.net_revenue+t.net_revenue, g.net_revenue=g.net_revenue+t.net_revenue, g.`PC1.5`=g.`PC1.5`+t.`PC1.5`, g.new_customers_gross=g.new_customers_gross+t.new_customers_gross, g.new_customers=g.new_customers+t.new_customers, g.cost=g.cost+t.cost, g.visits=g.visits+t.visits;

drop table attribution_testing.temporary_channel;

-- No Non Identified

create table attribution_testing.temporary_channel(
date date,
channel_group varchar(255),
gross_orders float not null default 0,
net_orders float not null default 0,
gross_revenue float not null default 0,
net_revenue float not null default 0,
`PC1.5` float not null default 0,
new_customers_gross float not null default 0,
new_customers float not null default 0,
cost float not null default 0,
visits float not null default 0
);

insert into attribution_testing.temporary_channel(date, channel_group) select date, channel_group from attribution_testing.global_attribution where channel_group!='Non Identified';

create table attribution_testing.percent(
date date,
channel_group varchar(255),
visits int,
total_visits int,
percent float
);

insert into attribution_testing.percent(date, channel_group, visits) select date, channel_group, visits from attribution_testing.global_attribution where channel_group!='Non Identified';

create table attribution_testing.total(
date date,
visits int
);

insert into attribution_testing.total select date, sum(visits) from attribution_testing.global_attribution where channel_group!='Non Identified' group by date;

update attribution_testing.percent p inner join attribution_testing.total t on p.date=t.date set p.total_visits=t.visits where channel_group!='Non Identified';

drop table attribution_testing.total;

update attribution_testing.percent set percent=visits/total_visits where channel_group!='Non Identified';

create table attribution_testing.temporary_telesales(
date date,
gross_orders float not null default 0,
net_orders float not null default 0,
gross_revenue float not null default 0,
net_revenue float not null default 0,
`PC1.5` float not null default 0,
new_customers_gross float not null default 0,
new_customers float not null default 0,
cost float not null default 0,
visits int not null default 0
);

insert into attribution_testing.temporary_telesales select date, gross_orders, net_orders, gross_revenue, net_revenue, `PC1.5`,new_customers_gross, new_customers, cost, visits from attribution_testing.global_attribution where channel_group='Non Identified';

update attribution_testing.temporary_telesales t inner join attribution_testing.temporary_channel c on t.date=c.date inner join attribution_testing.percent p on p.date=c.date and p.channel_group=c.channel_group set c.gross_orders=t.gross_orders*p.percent, c.net_orders=t.net_orders*p.percent, c.gross_revenue=t.gross_revenue*p.percent, c.net_revenue=t.net_revenue*p.percent, c.`PC1.5`=t.`PC1.5`*p.percent, c.new_customers_gross=t.new_customers_gross*p.percent, c.new_customers=t.new_customers*p.percent, c.cost=t.cost*p.percent, c.visits=t.visits*p.percent;

drop table attribution_testing.percent;

drop table attribution_testing.temporary_telesales;

delete from attribution_testing.global_attribution where channel_group='Non Identified';

update attribution_testing.global_attribution g inner join attribution_testing.temporary_channel t on g.date=t.date and g.channel_group=t.channel_group set g.gross_orders=g.gross_orders+t.gross_orders, g.net_orders=g.net_orders+t.net_orders, g.gross_revenue=g.gross_revenue+t.gross_revenue, g.net_revenue=g.net_revenue+t.net_revenue, g.net_revenue=g.net_revenue+t.net_revenue, g.`PC1.5`=g.`PC1.5`+t.`PC1.5`, g.new_customers_gross=g.new_customers_gross+t.new_customers_gross, g.new_customers=g.new_customers+t.new_customers, g.cost=g.cost+t.cost, g.visits=g.visits+t.visits;

drop table attribution_testing.temporary_channel;

-- Global Marketing Attribution

create table attribution_testing.temporary_report select date, channel_group, count(*) as channel_number, sum(net_orders+1) as net_orders, sum(net_revenue) as net_revenue, sum(new_customers+1) as new_customers from marketing_report.global_report where country='Peru' group by date, channel_group order by date desc;

create index channel_group on attribution_testing.temporary_report(date, channel_group);

##update marketing_report.global_report g inner join attribution_testing.temporary_report t on g.date=t.date and g.channel_group=t.channel_group inner join attribution_testing.global_attribution a on a.date=g.date and g.channel_group=a.channel_group set g.a_orders=a.net_orders*(g.net_orders/t.net_orders), g.a_revenue=a.net_revenue*(g.net_revenue/t.net_revenue), g.a_new_customers=a.new_customers*(g.new_customers/t.new_customers) where g.country='Peru' and a.country='Peru'; 

update marketing_report.global_report g inner join attribution_testing.temporary_report t on g.date=t.date and g.channel_group=t.channel_group inner join attribution_testing.global_attribution a on a.date=g.date and g.channel_group=a.channel_group set g.a_orders=a.net_orders*((g.net_orders+1)/t.net_orders), g.a_revenue=a.net_revenue*((g.net_orders+1)/t.net_orders), g.a_new_customers=a.new_customers*((g.new_customers+1)/t.new_customers) where g.country='Peru' and a.country='Peru'; 

drop table attribution_testing.temporary_report;

update marketing_report.global_report set a_orders=0 where a_orders is null;

update marketing_report.global_report set a_revenue=0 where a_revenue is null;

update marketing_report.global_report set a_new_customers=0 where a_new_customers is null;


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `attribution_report_ve`()
begin

-- Venezuela

set @days=180;

-- Attribution Compensation

drop table if exists attribution_testing.compensation_attribution_ve;

create table attribution_testing.compensation_attribution_ve as select date, channel, 0 as flag from attribution_testing.attribution_model_ve group by date, channel;

create index date on attribution_testing.compensation_attribution_ve(date);
create index channel on attribution_testing.compensation_attribution_ve(channel);

create table attribution_testing.compensation_global_ve as select date, channel_group as channel from marketing_report.global_report where country='Venezuela' group by date, channel_group;

create index date on attribution_testing.compensation_global_ve(date);
create index channel on attribution_testing.compensation_global_ve(channel);

update attribution_testing.compensation_attribution_ve a inner join attribution_testing.compensation_global_ve g on a.date=g.date and a.channel=g.channel set a.flag=1;

insert into marketing_report.global_report(country, yrmonth, week, date, weekday, channel_group, channel) select 'Venezuela', concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), production.week_iso(date) as week, date, case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end, channel, channel from attribution_testing.compensation_attribution_ve where flag=0 and datediff(curdate(), date)<@days; 

drop table attribution_testing.compensation_attribution_ve;

drop table attribution_testing.compensation_global_ve;

-- Global Attribution

delete from attribution_testing.global_attribution;

insert into attribution_testing.global_attribution(country, date, channel_group, gross_orders, net_orders, gross_revenue, net_revenue, new_customers_gross, new_customers) select 'Venezuela', gross.date, gross.channel, gross.orders, net.orders, gross.revenue, net.revenue, gross.new_customers, net.new_customers from (select t.date, a.channel, sum(t.gross_orders*a.percent) as orders, sum(ifnull(unit_price_after_vat*a.percent,0)-ifnull(coupon_money_after_vat*a.percent,0)+ifnull(shipping_fee_after_vat*a.percent,0)) as revenue, sum(new_customers_gross*a.percent) as new_customers from production_ve.tbl_order_detail t, attribution_testing.attribution_model_ve a where obc=1 and t.yrmonth>=201311 and a.order_nr=t.order_nr group by date, a.channel)gross left join (select t.date, a.channel, sum(t.net_orders*a.percent) as orders, sum(ifnull(unit_price_after_vat*a.percent,0)-ifnull(coupon_money_after_vat*a.percent,0)+ifnull(shipping_fee_after_vat*a.percent,0)) as revenue, sum(t.new_customers*a.percent) as new_customers from production_ve.tbl_order_detail t, attribution_testing.attribution_model_ve a where oac=1 and t.yrmonth>=201311 and a.order_nr=t.order_nr group by date, a.channel)net on gross.date=net.date and gross.channel=net.channel;

update attribution_testing.global_attribution set week=production_ve.week_iso(date), weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end, yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));

update attribution_testing.global_attribution s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr where s.country = 'Venezuela' and r.country='VEN';

update attribution_testing.global_attribution a inner join (select date, channel_group, sum(visits) as visits, sum(cost) as cost from marketing_report.global_report where country='Venezuela' group by date, channel_group)g on a.date=g.date and a.channel_group=g.channel_group set a.visits=g.visits, a.cost=g.cost where a.country='Venezuela';

-- No Tele Sales

create table attribution_testing.temporary_channel(
date date,
channel_group varchar(255),
gross_orders float not null default 0,
net_orders float not null default 0,
gross_revenue float not null default 0,
net_revenue float not null default 0,
`PC1.5` float not null default 0,
new_customers_gross float not null default 0,
new_customers float not null default 0,
cost float not null default 0,
visits float not null default 0
);

insert into attribution_testing.temporary_channel(date, channel_group) select date, channel_group from attribution_testing.global_attribution where channel_group!='Tele Sales';

create table attribution_testing.percent(
date date,
channel_group varchar(255),
visits int,
total_visits int,
percent float
);

insert into attribution_testing.percent(date, channel_group, visits) select date, channel_group, visits from attribution_testing.global_attribution where channel_group!='Tele Sales';

create table attribution_testing.total(
date date,
visits int
);

insert into attribution_testing.total select date, sum(visits) from attribution_testing.global_attribution where channel_group!='Tele Sales' group by date;

update attribution_testing.percent p inner join attribution_testing.total t on p.date=t.date set p.total_visits=t.visits where channel_group!='Tele Sales';

drop table attribution_testing.total;

update attribution_testing.percent set percent=visits/total_visits where channel_group!='Tele Sales';

create table attribution_testing.temporary_telesales(
date date,
gross_orders float not null default 0,
net_orders float not null default 0,
gross_revenue float not null default 0,
net_revenue float not null default 0,
`PC1.5` float not null default 0,
new_customers_gross float not null default 0,
new_customers float not null default 0,
cost float not null default 0,
visits int not null default 0
);

insert into attribution_testing.temporary_telesales select date, gross_orders, net_orders, gross_revenue, net_revenue, `PC1.5`, new_customers_gross, new_customers, cost, visits from attribution_testing.global_attribution where channel_group='Tele Sales';

update attribution_testing.temporary_telesales t inner join attribution_testing.temporary_channel c on t.date=c.date inner join attribution_testing.percent p on p.date=c.date and p.channel_group=c.channel_group set c.gross_orders=t.gross_orders*p.percent, c.net_orders=t.net_orders*p.percent, c.gross_revenue=t.gross_revenue*p.percent, c.net_revenue=t.net_revenue*p.percent, c.`PC1.5`=t.`PC1.5`*p.percent, c.new_customers_gross=t.new_customers_gross*p.percent, c.new_customers=t.new_customers*p.percent, c.cost=t.cost*p.percent, c.visits=t.visits*p.percent;

drop table attribution_testing.percent;

drop table attribution_testing.temporary_telesales;

delete from attribution_testing.global_attribution where channel_group='Tele Sales';

update attribution_testing.global_attribution g inner join attribution_testing.temporary_channel t on g.date=t.date and g.channel_group=t.channel_group set g.gross_orders=g.gross_orders+t.gross_orders, g.net_orders=g.net_orders+t.net_orders, g.gross_revenue=g.gross_revenue+t.gross_revenue, g.net_revenue=g.net_revenue+t.net_revenue, g.net_revenue=g.net_revenue+t.net_revenue, g.`PC1.5`=g.`PC1.5`+t.`PC1.5`, g.new_customers_gross=g.new_customers_gross+t.new_customers_gross, g.new_customers=g.new_customers+t.new_customers, g.cost=g.cost+t.cost, g.visits=g.visits+t.visits;

drop table attribution_testing.temporary_channel;

-- No Non Identified

create table attribution_testing.temporary_channel(
date date,
channel_group varchar(255),
gross_orders float not null default 0,
net_orders float not null default 0,
gross_revenue float not null default 0,
net_revenue float not null default 0,
`PC1.5` float not null default 0,
new_customers_gross float not null default 0,
new_customers float not null default 0,
cost float not null default 0,
visits float not null default 0
);

insert into attribution_testing.temporary_channel(date, channel_group) select date, channel_group from attribution_testing.global_attribution where channel_group!='Non Identified';

create table attribution_testing.percent(
date date,
channel_group varchar(255),
visits int,
total_visits int,
percent float
);

insert into attribution_testing.percent(date, channel_group, visits) select date, channel_group, visits from attribution_testing.global_attribution where channel_group!='Non Identified';

create table attribution_testing.total(
date date,
visits int
);

insert into attribution_testing.total select date, sum(visits) from attribution_testing.global_attribution where channel_group!='Non Identified' group by date;

update attribution_testing.percent p inner join attribution_testing.total t on p.date=t.date set p.total_visits=t.visits where channel_group!='Non Identified';

drop table attribution_testing.total;

update attribution_testing.percent set percent=visits/total_visits where channel_group!='Non Identified';

create table attribution_testing.temporary_telesales(
date date,
gross_orders float not null default 0,
net_orders float not null default 0,
gross_revenue float not null default 0,
net_revenue float not null default 0,
`PC1.5` float not null default 0,
new_customers_gross float not null default 0,
new_customers float not null default 0,
cost float not null default 0,
visits int not null default 0
);

insert into attribution_testing.temporary_telesales select date, gross_orders, net_orders, gross_revenue, net_revenue, `PC1.5`,new_customers_gross, new_customers, cost, visits from attribution_testing.global_attribution where channel_group='Non Identified';

update attribution_testing.temporary_telesales t inner join attribution_testing.temporary_channel c on t.date=c.date inner join attribution_testing.percent p on p.date=c.date and p.channel_group=c.channel_group set c.gross_orders=t.gross_orders*p.percent, c.net_orders=t.net_orders*p.percent, c.gross_revenue=t.gross_revenue*p.percent, c.net_revenue=t.net_revenue*p.percent, c.`PC1.5`=t.`PC1.5`*p.percent, c.new_customers_gross=t.new_customers_gross*p.percent, c.new_customers=t.new_customers*p.percent, c.cost=t.cost*p.percent, c.visits=t.visits*p.percent;

drop table attribution_testing.percent;

drop table attribution_testing.temporary_telesales;

delete from attribution_testing.global_attribution where channel_group='Non Identified';

update attribution_testing.global_attribution g inner join attribution_testing.temporary_channel t on g.date=t.date and g.channel_group=t.channel_group set g.gross_orders=g.gross_orders+t.gross_orders, g.net_orders=g.net_orders+t.net_orders, g.gross_revenue=g.gross_revenue+t.gross_revenue, g.net_revenue=g.net_revenue+t.net_revenue, g.net_revenue=g.net_revenue+t.net_revenue, g.`PC1.5`=g.`PC1.5`+t.`PC1.5`, g.new_customers_gross=g.new_customers_gross+t.new_customers_gross, g.new_customers=g.new_customers+t.new_customers, g.cost=g.cost+t.cost, g.visits=g.visits+t.visits;

drop table attribution_testing.temporary_channel;

-- Global Marketing Attribution

create table attribution_testing.temporary_report select date, channel_group, count(*) as channel_number, sum(net_orders+1) as net_orders, sum(net_revenue) as net_revenue, sum(new_customers+1) as new_customers from marketing_report.global_report where country='Venezuela' group by date, channel_group order by date desc;

create index channel_group on attribution_testing.temporary_report(date, channel_group);

##update marketing_report.global_report g inner join attribution_testing.temporary_report t on g.date=t.date and g.channel_group=t.channel_group inner join attribution_testing.global_attribution a on a.date=g.date and g.channel_group=a.channel_group set g.a_orders=a.net_orders*(g.net_orders/t.net_orders), g.a_revenue=a.net_revenue*(g.net_revenue/t.net_revenue), g.a_new_customers=a.new_customers*(g.new_customers/t.new_customers) where g.country='Venezuela' and a.country='Venezuela'; 

update marketing_report.global_report g inner join attribution_testing.temporary_report t on g.date=t.date and g.channel_group=t.channel_group inner join attribution_testing.global_attribution a on a.date=g.date and g.channel_group=a.channel_group set g.a_orders=a.net_orders*((g.net_orders+1)/t.net_orders), g.a_revenue=a.net_revenue*((g.net_orders+1)/t.net_orders), g.a_new_customers=a.new_customers*((g.new_customers+1)/t.new_customers) where g.country='Venezuela' and a.country='Venezuela'; 

drop table attribution_testing.temporary_report;

update marketing_report.global_report set a_orders=0 where a_orders is null;

update marketing_report.global_report set a_revenue=0 where a_revenue is null;

update marketing_report.global_report set a_new_customers=0 where a_new_customers is null;


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`lorena.ramirez`@`%`*/ /*!50003 PROCEDURE `estimated_new_customers`(in pais varchar(45))
BEGIN

-- New Customers
set @days:=10;
delete from marketing_report.temporary_global_report;

insert into marketing_report.temporary_global_report
(date, channel_group, channel, gross_orders, net_orders, gross_revenue, net_revenue, new_customers_gross, new_customers) 
select date, channel_group, channel, gross_orders, net_orders, gross_revenue, net_revenue,
 new_customers_gross, new_customers from marketing_report.global_report
 where country=pais and datediff(curdate(), date)<@days;

select pais, date, channel_group, channel, gross_orders, net_orders, gross_revenue, net_revenue,
 new_customers_gross, new_customers from marketing_report.global_report
 where country=pais and datediff(curdate(), date)<@days;

set @date=date_sub(curdate(), interval @days day);

while @date!=curdate() do
select @date;
update marketing_report.temporary_global_report t
 set e_new_customers = (select (avg(g.new_customers/g.new_customers_gross)) 
from marketing_report.global_report g
 where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) 
and country=pais) where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

select date,new_customers_gross, e_new_customers
from marketing_report.temporary_global_report;

update marketing_report.temporary_global_report t 
set e_new_customers = new_customers_gross*e_new_customers
 where datediff(curdate(), date)<@days;

select date,new_customers, e_new_customers
from marketing_report.temporary_global_report;

update marketing_report.temporary_global_report t
set e_new_customers = case when e_new_customers>new_customers 
then e_new_customers else new_customers end where datediff(curdate(), date)<@days;


update marketing_report.temporary_global_report t set cancel = (select sum(new_customers_gross) from production.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set returns = (select sum(new_customers_gross) from production.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.returned=1) where datediff(curdate(), date)<@days; 

update marketing_report.temporary_global_report t set cancel = cancel/new_customers_gross, returns = returns/new_customers_gross where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set ee_new_customers = new_customers_gross*(1-ifnull(cancel,0)-ifnull(returns,0)) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_new_customers = case when e_new_customers>ee_new_customers then ee_new_customers else e_new_customers end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_new_customers = case when e_new_customers>new_customers then e_new_customers else new_customers end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t inner join marketing_report.global_report g on t.date=g.date and t.channel_group=g.channel_group and t.channel=g.channel set g.e_orders=t.e_orders, g.e_revenue=t.e_revenue, g.e_new_customers=t.e_new_customers where country = 'Peru' and datediff(curdate(), g.date)<@days;





END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `marketing_campaign_tool`()
begin

create table attribution_testing.temporary_marketing_campaign_tool(
date date,
sid varchar(255),
product varchar(255),
channel varchar(255),
campaign varchar(255),
quantity int,
stat int
);

create index sid on attribution_testing.temporary_marketing_campaign_tool(sid);
create index campaign on attribution_testing.temporary_marketing_campaign_tool(campaign);
create index date on attribution_testing.temporary_marketing_campaign_tool(date);
create index product on attribution_testing.temporary_marketing_campaign_tool(product);
create index channel on attribution_testing.temporary_marketing_campaign_tool(channel);

##insert into attribution_testing.temporary_marketing_campaign_tool(date, sid, product, quantity, stat, campaign) select g.date, g.sid, g.product, g.quantity, g.stat, c.campaign from attribution_testing.getfullbasket_mx g, attribution_testing.getfullcampaign_mx c where c.sid=g.sid and g.date between '2014-01-01' and '2014-01-31';

insert into attribution_testing.temporary_marketing_campaign_tool(date, sid, product, quantity, stat) select date, sid, product, quantity, stat from attribution_testing.getfullbasket_mx where date between '2014-01-01' and '2014-03-12';

update attribution_testing.temporary_marketing_campaign_tool t inner join attribution_testing.getfullcampaign_mx c on t.sid=c.sid set t.campaign=c.campaign;

update attribution_testing.temporary_marketing_campaign_tool t inner join attribution_testing.campaigns_config_mx c on t.campaign=c.data_source_value set t.channel=c.category_2;

update attribution_testing.temporary_marketing_campaign_tool set channel='Retargeting' where channel is null and campaign like '%retargeting%';

update attribution_testing.temporary_marketing_campaign_tool set channel='Non Identified' where channel is null and campaign is null; 

update attribution_testing.temporary_marketing_campaign_tool set campaign='Non Identified' where campaign is null; 

delete from attribution_testing.marketing_campaign_tool;

insert into attribution_testing.marketing_campaign_tool(date, product, channel, campaign) select date, product, channel, campaign from attribution_testing.temporary_marketing_campaign_tool group by date, product, channel, campaign;

##update attribution_testing.marketing_campaign_tool c set add_to_cart = (select sum(quantity) from attribution_testing.temporary_marketing_campaign_tool t where stat=0 and t.date=c.date and t.product=c.product and t.channel=c.channel and t.campaign=c.campaign);

-- Marketing Campaign Tool

-- Add To Cart

create table attribution_testing.marketing_campaign_add_to_cart as select date, product, channel, campaign, sum(quantity) as quantity from attribution_testing.temporary_marketing_campaign_tool t where stat=0 group by date, product, channel, campaign;

create index product on attribution_testing.marketing_campaign_add_to_cart(product);
create index date on attribution_testing.marketing_campaign_add_to_cart(date);
create index channel on attribution_testing.marketing_campaign_add_to_cart(channel);
create index campaign on attribution_testing.marketing_campaign_add_to_cart(campaign);

update attribution_testing.marketing_campaign_tool c inner join attribution_testing.marketing_campaign_add_to_cart t on t.date=c.date and t.product=c.product and t.channel=c.channel and t.campaign=c.campaign set add_to_cart = quantity;

drop table attribution_testing.marketing_campaign_add_to_cart;

-- Product Views

create table attribution_testing.marketing_campaign_product_views as select date, product, channel, campaign, sum(quantity) as quantity from attribution_testing.temporary_marketing_campaign_tool t where stat=2 group by date, product, channel, campaign;

create index product on attribution_testing.marketing_campaign_product_views(product);
create index date on attribution_testing.marketing_campaign_product_views(date);
create index channel on attribution_testing.marketing_campaign_product_views(channel);
create index campaign on attribution_testing.marketing_campaign_product_views(campaign);

update attribution_testing.marketing_campaign_tool c inner join attribution_testing.marketing_campaign_product_views t on t.date=c.date and t.product=c.product and t.channel=c.channel and t.campaign=c.campaign set product_views = quantity;

drop table attribution_testing.marketing_campaign_product_views;

-- Clicks

create table attribution_testing.marketing_campaign_clicks select date, substring_index(substr(click, locate(':',click)+1), '.', 1) as product, count(*) as clicks from attribution_testing.getfullclicks_mx where date between '2014-01-01' and '2014-03-12' group by substring_index(substr(click, locate(':',click)+1), '.', 1), date;

create index product on attribution_testing.marketing_campaign_clicks(product);
create index date on attribution_testing.marketing_campaign_clicks(date);

create table attribution_testing.temporary_count select date, product, sum(product_views) as number from attribution_testing.marketing_campaign_tool group by date, product;

create index product on attribution_testing.temporary_count(product);
create index date on attribution_testing.temporary_count(date);

update attribution_testing.marketing_campaign_tool t inner join attribution_testing.temporary_count c on t.date=c.date and t.product=c.product set t.clicks=t.product_views/c.number;

drop table attribution_testing.temporary_count;

update attribution_testing.marketing_campaign_tool t inner join attribution_testing.marketing_campaign_clicks c on t.date=c.date and t.product=c.product set t.clicks=t.clicks*c.clicks, t.impressions=1;

update attribution_testing.marketing_campaign_tool set clicks = null where impressions is null;

update attribution_testing.marketing_campaign_tool set impressions = null;

drop table attribution_testing.marketing_campaign_clicks;

-- Unit Sold

create table attribution_testing.marketing_campaign_unit_sold select date, skuconfig as product, count(*) as unit_sold from development_mx.A_Master where date between '2014-01-01' and '2014-03-12' and orderaftercan=1 group by skuconfig, date;

create index product on attribution_testing.marketing_campaign_unit_sold(product);
create index date on attribution_testing.marketing_campaign_unit_sold(date);

create table attribution_testing.temporary_count select date, product, sum(product_views) as number from attribution_testing.marketing_campaign_tool group by date, product;

create index product on attribution_testing.temporary_count(product);
create index date on attribution_testing.temporary_count(date);

update attribution_testing.marketing_campaign_tool t inner join attribution_testing.temporary_count c on t.date=c.date and t.product=c.product set t.unit_sold=t.product_views/c.number;

drop table attribution_testing.temporary_count;

update attribution_testing.marketing_campaign_tool t inner join attribution_testing.marketing_campaign_unit_sold c on t.date=c.date and t.product=c.product set t.unit_sold=t.unit_sold*c.unit_sold, t.impressions=1;

update attribution_testing.marketing_campaign_tool set unit_sold = null where impressions is null;

update attribution_testing.marketing_campaign_tool set impressions = null;

drop table attribution_testing.marketing_campaign_unit_sold;

-- Net Revenue

create table attribution_testing.marketing_campaign_net_revenue select date, skuconfig as product, sum(rev) as net_revenue from development_mx.A_Master where date between '2014-01-01' and '2014-03-12' and orderaftercan=1 group by skuconfig, date;

create index product on attribution_testing.marketing_campaign_net_revenue(product);
create index date on attribution_testing.marketing_campaign_net_revenue(date);

create table attribution_testing.temporary_count select date, product, sum(product_views) as number from attribution_testing.marketing_campaign_tool group by date, product;

create index product on attribution_testing.temporary_count(product);
create index date on attribution_testing.temporary_count(date);

update attribution_testing.marketing_campaign_tool t inner join attribution_testing.temporary_count c on t.date=c.date and t.product=c.product set t.net_revenue=t.product_views/c.number;

drop table attribution_testing.temporary_count;

update attribution_testing.marketing_campaign_tool t inner join attribution_testing.marketing_campaign_net_revenue c on t.date=c.date and t.product=c.product set t.net_revenue=t.net_revenue*c.net_revenue, t.impressions=1;

update attribution_testing.marketing_campaign_tool set net_revenue = null where impressions is null;

update attribution_testing.marketing_campaign_tool set impressions = null;

drop table attribution_testing.marketing_campaign_net_revenue;

-- PC1.5

create table attribution_testing.`marketing_campaign_PC1.5` select date, skuconfig as product, sum(pconepfive) as `PC1.5` from development_mx.A_Master where date between '2014-01-01' and '2014-03-12' and orderaftercan=1 group by skuconfig, date;

create index product on attribution_testing.`marketing_campaign_PC1.5`(product);
create index date on attribution_testing.`marketing_campaign_PC1.5`(date);

create table attribution_testing.temporary_count select date, product, sum(product_views) as number from attribution_testing.marketing_campaign_tool group by date, product;

create index product on attribution_testing.temporary_count(product);
create index date on attribution_testing.temporary_count(date);

update attribution_testing.marketing_campaign_tool t inner join attribution_testing.temporary_count c on t.date=c.date and t.product=c.product set t.`PC1.5`=t.product_views/c.number;

drop table attribution_testing.temporary_count;

update attribution_testing.marketing_campaign_tool t inner join attribution_testing.`marketing_campaign_PC1.5` c on t.date=c.date and t.product=c.product set t.`PC1.5`=t.`PC1.5`*c.`PC1.5`, t.impressions=1;

update attribution_testing.marketing_campaign_tool set `PC1.5` = null where impressions is null;

update attribution_testing.marketing_campaign_tool set impressions = null;

drop table attribution_testing.`marketing_campaign_PC1.5`;

-- Last Perio Range Data

-- Days in Input Table

alter table attribution_testing.input_marketing_campaign_tool AUTO_INCREMENT = 1;

update attribution_testing.input_marketing_campaign_tool set days=datediff(end_date, start_date);

update attribution_testing.input_marketing_campaign_tool set days=days+1;

-- Final Output Table

delete from attribution_testing.output_marketing_campaign_tool;

insert into attribution_testing.output_marketing_campaign_tool(start_date_before, end_date_before, start_date, end_date, product, product_name, channel_target_1, campaign_target_1, channel_target_2, campaign_target_2, pushes, cat1, price, campaign_price, campaign_discount, `estimated_PC1.5`, week,  channel, campaign, add_to_cart, product_views, clicks, impressions, cost, unit_sold, net_revenue, `PC1.5`, days) select date_sub(l.start_date, interval days day), date_sub(l.end_date, interval days day), l.start_date, l.end_date, c.product, l.product_name, l.channel_1, l.campaign_1, l.channel_2, l.campaign_2, l.pushes, l.cat1, l.price, l.campaign_price, l.campaign_discount, l.`estimated_PC1.5`, l.week, c.channel, c.campaign, sum(c.add_to_cart), sum(c.product_views), sum(c.clicks), sum(c.impressions), sum(c.cost), sum(c.unit_sold), sum(net_revenue), sum(`PC1.5`), days from attribution_testing.marketing_campaign_tool c, attribution_testing.input_marketing_campaign_tool l where c.date between l.start_date and l.end_date and c.product=l.product group by c.product, c.channel, c.campaign, l.start_date, l.end_date;

-- Last Period Product Views

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.last_period_product_views=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set last_period_product_views = last_period_product_views*(select sum(product_views) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date_before and o.end_date_before and o.product=t.product), last_period_impressions=1;

update attribution_testing.output_marketing_campaign_tool set last_period_product_views = null where last_period_impressions is null;

update attribution_testing.output_marketing_campaign_tool set last_period_impressions = null;

drop table attribution_testing.temporary_proportion;

-- Clicks

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.last_period_clicks=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set last_period_clicks = last_period_clicks*(select sum(clicks) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date_before and o.end_date_before and o.product=t.product), last_period_impressions=1;

update attribution_testing.output_marketing_campaign_tool set last_period_clicks = null where last_period_impressions is null;

update attribution_testing.output_marketing_campaign_tool set last_period_impressions = null;

drop table attribution_testing.temporary_proportion;

-- Unit Sold

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.last_period_unit_sold=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set last_period_unit_sold = last_period_unit_sold*(select sum(unit_sold) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date_before and o.end_date_before and o.product=t.product), last_period_impressions=1;

update attribution_testing.output_marketing_campaign_tool set last_period_unit_sold = null where last_period_impressions is null;

update attribution_testing.output_marketing_campaign_tool set last_period_impressions = null;

drop table attribution_testing.temporary_proportion;

-- Add To Cart

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.last_period_add_to_cart=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set last_period_add_to_cart = last_period_add_to_cart*(select sum(add_to_cart) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date_before and o.end_date_before and o.product=t.product), last_period_impressions=1;

update attribution_testing.output_marketing_campaign_tool set last_period_add_to_cart = null where last_period_impressions is null;

update attribution_testing.output_marketing_campaign_tool set last_period_impressions = null;

drop table attribution_testing.temporary_proportion;

-- Net Revenue

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.last_period_net_revenue=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set last_period_net_revenue = last_period_net_revenue*(select sum(net_revenue) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date_before and o.end_date_before and o.product=t.product), last_period_impressions=1;

update attribution_testing.output_marketing_campaign_tool set last_period_net_revenue = null where last_period_impressions is null;

update attribution_testing.output_marketing_campaign_tool set last_period_impressions = null;

drop table attribution_testing.temporary_proportion;

-- PC1.5

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.`last_period_PC1.5`=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set `last_period_PC1.5` = `last_period_PC1.5`*(select sum(`PC1.5`) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date_before and o.end_date_before and o.product=t.product), last_period_impressions=1;

update attribution_testing.output_marketing_campaign_tool set `last_period_PC1.5` = null where last_period_impressions is null;

update attribution_testing.output_marketing_campaign_tool set last_period_impressions = null;

drop table attribution_testing.temporary_proportion;

-- Count SKU

create table attribution_testing.temporary_count as select week, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by week, product;

create index week on attribution_testing.temporary_count(week);
create index product on attribution_testing.temporary_count(product);

update attribution_testing.output_marketing_campaign_tool o inner join attribution_testing.temporary_count t on t.week=o.week and t.product=o.product set sku_count=1/number;

drop table attribution_testing.temporary_count; 

-- Missing SKU

create table attribution_testing.temporary_missing as select i.start_date, i.end_date, i.product from attribution_testing.input_marketing_campaign_tool i left join attribution_testing.output_marketing_campaign_tool o on o.start_date=i.start_date and o.end_date=i.end_date and o.product=i.product where sku_count is null group by start_date, end_date, product;

insert into attribution_testing.output_marketing_campaign_tool(start_date_before, end_date_before, start_date, end_date, product, product_name, channel_target_1, campaign_target_1, channel_target_2, campaign_target_2, cat1, price, campaign_price, campaign_discount, `estimated_PC1.5`, week) select date_sub(l.start_date, interval days day), date_sub(l.end_date, interval days day), l.start_date, l.end_date, l.product, l.product_name, l.channel_1, l.campaign_1, l.channel_2, l.campaign_2, l.cat1, l.price, l.campaign_price, l.campaign_discount, l.`estimated_PC1.5`, l.week from attribution_testing.input_marketing_campaign_tool l, attribution_testing.temporary_missing t where l.start_date=t.start_date and l.end_date=t.end_date and t.product=l.product group by l.product, l.start_date, l.end_date;

drop table attribution_testing.temporary_missing; 

-- Count SKU

create table attribution_testing.temporary_count as select week, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by week, product;

create index week on attribution_testing.temporary_count(week);
create index product on attribution_testing.temporary_count(product);

update attribution_testing.output_marketing_campaign_tool o inner join attribution_testing.temporary_count t on t.week=o.week and t.product=o.product set sku_count=1/number;

drop table attribution_testing.temporary_count; 

-- + 6 days

-- Product Views

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.plus_6_days_product_views=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set plus_6_days_product_views = plus_6_days_product_views*(select sum(product_views) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date and date_add(o.start_date, interval 5 day) and o.product=t.product), plus_6_days_impressions=1;

update attribution_testing.output_marketing_campaign_tool set plus_6_days_product_views = null where plus_6_days_impressions is null;

update attribution_testing.output_marketing_campaign_tool set plus_6_days_impressions = null;

drop table attribution_testing.temporary_proportion;

-- Add To Cart

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.plus_6_days_add_to_cart=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set plus_6_days_add_to_cart = plus_6_days_add_to_cart*(select sum(add_to_cart) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date and date_add(o.start_date, interval 5 day) and o.product=t.product), plus_6_days_impressions=1;

update attribution_testing.output_marketing_campaign_tool set plus_6_days_add_to_cart = null where plus_6_days_impressions is null;

update attribution_testing.output_marketing_campaign_tool set plus_6_days_impressions = null;

drop table attribution_testing.temporary_proportion;

-- Clicks

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.plus_6_days_clicks=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set plus_6_days_clicks = plus_6_days_clicks*(select sum(clicks) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date and date_add(o.start_date, interval 5 day) and o.product=t.product), plus_6_days_impressions=1;

update attribution_testing.output_marketing_campaign_tool set plus_6_days_clicks = null where plus_6_days_impressions is null;

update attribution_testing.output_marketing_campaign_tool set plus_6_days_impressions = null;

drop table attribution_testing.temporary_proportion;

-- Unit Sold

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.plus_6_days_unit_sold=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set plus_6_days_unit_sold = plus_6_days_unit_sold*(select sum(unit_sold) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date and date_add(o.start_date, interval 5 day) and o.product=t.product), plus_6_days_impressions=1;

update attribution_testing.output_marketing_campaign_tool set plus_6_days_unit_sold = null where plus_6_days_impressions is null;

update attribution_testing.output_marketing_campaign_tool set plus_6_days_impressions = null;

drop table attribution_testing.temporary_proportion;

-- Net Revenue

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.plus_6_days_net_revenue=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set plus_6_days_net_revenue = plus_6_days_net_revenue*(select sum(net_revenue) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date and date_add(o.start_date, interval 5 day) and o.product=t.product), plus_6_days_impressions=1;

update attribution_testing.output_marketing_campaign_tool set plus_6_days_net_revenue = null where plus_6_days_impressions is null;

update attribution_testing.output_marketing_campaign_tool set plus_6_days_impressions = null;

drop table attribution_testing.temporary_proportion;

-- PC1.5

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.`plus_6_days_PC1.5`=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set `plus_6_days_PC1.5` = `plus_6_days_PC1.5`*(select sum(`PC1.5`) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date and date_add(o.start_date, interval 5 day) and o.product=t.product), plus_6_days_impressions=1;

update attribution_testing.output_marketing_campaign_tool set `plus_6_days_PC1.5` = null where plus_6_days_impressions is null;

update attribution_testing.output_marketing_campaign_tool set plus_6_days_impressions = null;

drop table attribution_testing.temporary_proportion;

-- - 6 days

-- Product Views

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.minus_6_days_product_views=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set minus_6_days_product_views = minus_6_days_product_views*(select sum(product_views) from attribution_testing.marketing_campaign_tool t where t.date between date_sub(o.start_date, interval 6 day) and date_sub(o.start_date, interval 1 day) and o.product=t.product), minus_6_days_impressions=1;

update attribution_testing.output_marketing_campaign_tool set minus_6_days_product_views = null where minus_6_days_impressions is null;

update attribution_testing.output_marketing_campaign_tool set minus_6_days_impressions = null;

drop table attribution_testing.temporary_proportion;

-- Add To Cart

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.minus_6_days_add_to_cart=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set minus_6_days_add_to_cart = minus_6_days_add_to_cart*(select sum(add_to_cart) from attribution_testing.marketing_campaign_tool t where t.date between date_sub(o.start_date, interval 6 day) and date_sub(o.start_date, interval 1 day) and o.product=t.product), minus_6_days_impressions=1;

update attribution_testing.output_marketing_campaign_tool set minus_6_days_add_to_cart = null where minus_6_days_impressions is null;

update attribution_testing.output_marketing_campaign_tool set minus_6_days_impressions = null;

drop table attribution_testing.temporary_proportion;

-- Clicks

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.minus_6_days_clicks=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set minus_6_days_clicks = minus_6_days_clicks*(select sum(clicks) from attribution_testing.marketing_campaign_tool t where t.date between date_sub(o.start_date, interval 6 day) and date_sub(o.start_date, interval 1 day) and o.product=t.product), minus_6_days_impressions=1;

update attribution_testing.output_marketing_campaign_tool set minus_6_days_clicks = null where minus_6_days_impressions is null;

update attribution_testing.output_marketing_campaign_tool set minus_6_days_impressions = null;

drop table attribution_testing.temporary_proportion;

-- Unit Sold

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.minus_6_days_unit_sold=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set minus_6_days_unit_sold = minus_6_days_unit_sold*(select sum(unit_sold) from attribution_testing.marketing_campaign_tool t where t.date between date_sub(o.start_date, interval 6 day) and date_sub(o.start_date, interval 1 day) and o.product=t.product), minus_6_days_impressions=1;

update attribution_testing.output_marketing_campaign_tool set minus_6_days_unit_sold = null where minus_6_days_impressions is null;

update attribution_testing.output_marketing_campaign_tool set minus_6_days_impressions = null;

drop table attribution_testing.temporary_proportion;

-- Net Revenue

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.minus_6_days_net_revenue=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set minus_6_days_net_revenue = minus_6_days_net_revenue*(select sum(net_revenue) from attribution_testing.marketing_campaign_tool t where t.date between date_sub(o.start_date, interval 6 day) and date_sub(o.start_date, interval 1 day) and o.product=t.product), minus_6_days_impressions=1;

update attribution_testing.output_marketing_campaign_tool set minus_6_days_net_revenue = null where minus_6_days_impressions is null;

update attribution_testing.output_marketing_campaign_tool set minus_6_days_impressions = null;

drop table attribution_testing.temporary_proportion;

-- PC1.5

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.`minus_6_days_PC1.5`=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set `minus_6_days_PC1.5` = `minus_6_days_PC1.5`*(select sum(`PC1.5`) from attribution_testing.marketing_campaign_tool t where t.date between date_sub(o.start_date, interval 6 day) and date_sub(o.start_date, interval 1 day) and o.product=t.product), minus_6_days_impressions=1;

update attribution_testing.output_marketing_campaign_tool set `minus_6_days_PC1.5` = null where minus_6_days_impressions is null;

update attribution_testing.output_marketing_campaign_tool set minus_6_days_impressions = null;

drop table attribution_testing.temporary_proportion;

-- Stock Out

create table attribution_testing.temporary_count as select week, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by week, product;

create index week on attribution_testing.temporary_count(week);
create index product on attribution_testing.temporary_count(product);

create table attribution_testing.temporary_stock_out as select date_ordered as date, sku_config from production.out_order_tracking where stockout=1 and date_ordered between '2014-01-01' and '2014-03-12' group by date_ordered, sku_config order by date_ordered;

create index sku_config on attribution_testing.temporary_stock_out(sku_config);
create index date on attribution_testing.temporary_stock_out(date);

update attribution_testing.output_marketing_campaign_tool o inner join attribution_testing.temporary_count t on t.week=o.week and t.product=o.product set stock_out = (select count(*) from attribution_testing.temporary_stock_out v where v.date between o.start_date and o.end_date and o.product=v.sku_config)/number;

##update attribution_testing.output_marketing_campaign_tool t set stock_out = (select count(*) from attribution_testing.temporary_stock_out o where o.date between t.start_date and t.end_date and t.product=o.sku_config);

drop table attribution_testing.temporary_count;  

drop table attribution_testing.temporary_stock_out;

-- Week Marketing Campaign Tool

delete from attribution_testing.week_marketing_campaign_tool;

insert into attribution_testing.week_marketing_campaign_tool(yrmonth, week, product, channel, campaign, add_to_cart, product_views, clicks, impressions, unit_sold, net_revenue, `PC1.5`) select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), production.week_iso(date), product, channel, campaign, sum(add_to_cart), sum(product_views), sum(clicks), sum(impressions), sum(unit_sold), sum(net_revenue), sum(`PC1.5`) from attribution_testing.marketing_campaign_tool group by production.week_iso(date), campaign, product;  

update attribution_testing.week_marketing_campaign_tool w inner join development_mx.A_Master_Catalog c on w.product=c.sku_config set w.cat1=c.cat1;

-- drop table attribution_testing.temporary_marketing_campaign_tool;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `marketing_campaign_tool_bob`()
begin

-- Input Table Config

alter table attribution_testing.input_marketing_campaign_tool AUTO_INCREMENT = 1;

update attribution_testing.input_marketing_campaign_tool set days=datediff(end_date, start_date);

update attribution_testing.input_marketing_campaign_tool set days=days+1;

-- Final Output Table

delete from attribution_testing.output_marketing_campaign_tool;

insert into attribution_testing.output_marketing_campaign_tool(start_date_before, end_date_before, start_date, end_date, product, product_name, channel_target_1, campaign_target_1, channel_target_2, campaign_target_2, pushes, cat1, price, campaign_price, campaign_discount, `estimated_PC1.5`, week,  channel, campaign, add_to_cart, product_views, clicks, impressions, cost, unit_sold, net_revenue, `PC1.5`, days) select date_sub(l.start_date, interval days day), date_sub(l.end_date, interval days day), l.start_date, l.end_date, c.product, l.product_name, l.channel_1, l.campaign_1, l.channel_2, l.campaign_2, l.pushes, l.cat1, l.price, l.campaign_price, l.campaign_discount, l.`estimated_PC1.5`, l.week, c.channel, c.campaign, sum(c.add_to_cart), sum(c.product_views), sum(c.clicks), sum(c.impressions), sum(c.cost), sum(c.unit_sold), sum(net_revenue), sum(`PC1.5`), days from attribution_testing.marketing_campaign_tool c, attribution_testing.input_marketing_campaign_tool l where c.date between l.start_date and l.end_date and c.product=l.product group by c.product, c.channel, c.campaign, l.start_date, l.end_date;

set @first_date:=(select min(start_date) from attribution_testing.output_marketing_campaign_tool);
set @last_date:=(select max(start_date) from attribution_testing.output_marketing_campaign_tool);

-- Last Period 

-- Product Views

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.last_period_product_views=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set last_period_product_views = last_period_product_views*(select sum(product_views) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date_before and o.end_date_before and o.product=t.product), flag=1;

update attribution_testing.output_marketing_campaign_tool set last_period_product_views = null where flag is null;

update attribution_testing.output_marketing_campaign_tool set flag = null;

drop table attribution_testing.temporary_proportion;

-- Clicks

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.last_period_clicks=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set last_period_clicks = last_period_clicks*(select sum(clicks) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date_before and o.end_date_before and o.product=t.product), flag=1;

update attribution_testing.output_marketing_campaign_tool set last_period_clicks = null where flag is null;

update attribution_testing.output_marketing_campaign_tool set flag = null;

drop table attribution_testing.temporary_proportion;

-- Impressions

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.last_period_impressions=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set last_period_impressions = last_period_impressions*(select sum(impressions) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date_before and o.end_date_before and o.product=t.product), flag=1;

update attribution_testing.output_marketing_campaign_tool set last_period_impressions = null where flag is null;

update attribution_testing.output_marketing_campaign_tool set flag = null;

drop table attribution_testing.temporary_proportion;

-- Unit Sold

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.last_period_unit_sold=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set last_period_unit_sold = last_period_unit_sold*(select sum(unit_sold) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date_before and o.end_date_before and o.product=t.product), flag=1;

update attribution_testing.output_marketing_campaign_tool set last_period_unit_sold = null where flag is null;

update attribution_testing.output_marketing_campaign_tool set flag = null;

drop table attribution_testing.temporary_proportion;

-- Add To Cart

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.last_period_add_to_cart=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set last_period_add_to_cart = last_period_add_to_cart*(select sum(add_to_cart) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date_before and o.end_date_before and o.product=t.product), flag=1;

update attribution_testing.output_marketing_campaign_tool set last_period_add_to_cart = null where flag is null;

update attribution_testing.output_marketing_campaign_tool set flag = null;

drop table attribution_testing.temporary_proportion;

-- Net Revenue

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.last_period_net_revenue=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set last_period_net_revenue = last_period_net_revenue*(select sum(net_revenue) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date_before and o.end_date_before and o.product=t.product), flag=1;

update attribution_testing.output_marketing_campaign_tool set last_period_net_revenue = null where flag is null;

update attribution_testing.output_marketing_campaign_tool set flag = null;

drop table attribution_testing.temporary_proportion;

-- PC1.5

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.`last_period_PC1.5`=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set `last_period_PC1.5` = `last_period_PC1.5`*(select sum(`PC1.5`) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date_before and o.end_date_before and o.product=t.product), flag=1;

update attribution_testing.output_marketing_campaign_tool set `last_period_PC1.5` = null where flag is null;

update attribution_testing.output_marketing_campaign_tool set flag = null;

drop table attribution_testing.temporary_proportion;

-- Cost

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.last_period_cost=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set last_period_cost = last_period_cost*(select sum(cost) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date_before and o.end_date_before and o.product=t.product), flag=1;

update attribution_testing.output_marketing_campaign_tool set last_period_cost = null where flag is null;

update attribution_testing.output_marketing_campaign_tool set flag = null;

drop table attribution_testing.temporary_proportion;

-- Count SKU

create table attribution_testing.temporary_count as select week, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by week, product;

create index week on attribution_testing.temporary_count(week);
create index product on attribution_testing.temporary_count(product);

update attribution_testing.output_marketing_campaign_tool o inner join attribution_testing.temporary_count t on t.week=o.week and t.product=o.product set sku_count=1/number;

drop table attribution_testing.temporary_count; 

-- Missing SKU

create table attribution_testing.temporary_missing as select i.start_date, i.end_date, i.product from attribution_testing.input_marketing_campaign_tool i left join attribution_testing.output_marketing_campaign_tool o on o.start_date=i.start_date and o.end_date=i.end_date and o.product=i.product where sku_count is null group by start_date, end_date, product;

insert into attribution_testing.output_marketing_campaign_tool(start_date_before, end_date_before, start_date, end_date, product, product_name, channel_target_1, campaign_target_1, channel_target_2, campaign_target_2, cat1, price, campaign_price, campaign_discount, `estimated_PC1.5`, week) select date_sub(l.start_date, interval days day), date_sub(l.end_date, interval days day), l.start_date, l.end_date, l.product, l.product_name, l.channel_1, l.campaign_1, l.channel_2, l.campaign_2, l.cat1, l.price, l.campaign_price, l.campaign_discount, l.`estimated_PC1.5`, l.week from attribution_testing.input_marketing_campaign_tool l, attribution_testing.temporary_missing t where l.start_date=t.start_date and l.end_date=t.end_date and t.product=l.product group by l.product, l.start_date, l.end_date;

drop table attribution_testing.temporary_missing; 

-- Count SKU

create table attribution_testing.temporary_count as select week, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by week, product;

create index week on attribution_testing.temporary_count(week);
create index product on attribution_testing.temporary_count(product);

update attribution_testing.output_marketing_campaign_tool o inner join attribution_testing.temporary_count t on t.week=o.week and t.product=o.product set sku_count=1/number;

drop table attribution_testing.temporary_count; 

-- + 6 days

-- Product Views

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.plus_6_days_product_views=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set plus_6_days_product_views = plus_6_days_product_views*(select sum(product_views) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date and date_add(o.start_date, interval 5 day) and o.product=t.product), flag=1;

update attribution_testing.output_marketing_campaign_tool set plus_6_days_product_views = null where flag is null;

update attribution_testing.output_marketing_campaign_tool set flag = null;

drop table attribution_testing.temporary_proportion;

-- Add To Cart

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.plus_6_days_add_to_cart=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set plus_6_days_add_to_cart = plus_6_days_add_to_cart*(select sum(add_to_cart) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date and date_add(o.start_date, interval 5 day) and o.product=t.product), flag=1;

update attribution_testing.output_marketing_campaign_tool set plus_6_days_add_to_cart = null where flag is null;

update attribution_testing.output_marketing_campaign_tool set flag = null;

drop table attribution_testing.temporary_proportion;

-- Clicks

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.plus_6_days_clicks=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set plus_6_days_clicks = plus_6_days_clicks*(select sum(clicks) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date and date_add(o.start_date, interval 5 day) and o.product=t.product), flag=1;

update attribution_testing.output_marketing_campaign_tool set plus_6_days_clicks = null where flag is null;

update attribution_testing.output_marketing_campaign_tool set flag = null;

drop table attribution_testing.temporary_proportion;

-- Impressions

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.plus_6_days_impressions=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set plus_6_days_impressions = plus_6_days_impressions*(select sum(impressions) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date and date_add(o.start_date, interval 5 day) and o.product=t.product), flag=1;

update attribution_testing.output_marketing_campaign_tool set plus_6_days_impressions = null where flag is null;

update attribution_testing.output_marketing_campaign_tool set flag = null;

drop table attribution_testing.temporary_proportion;

-- Unit Sold

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.plus_6_days_unit_sold=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set plus_6_days_unit_sold = plus_6_days_unit_sold*(select sum(unit_sold) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date and date_add(o.start_date, interval 5 day) and o.product=t.product), flag=1;

update attribution_testing.output_marketing_campaign_tool set plus_6_days_unit_sold = null where flag is null;

update attribution_testing.output_marketing_campaign_tool set flag = null;

drop table attribution_testing.temporary_proportion;

-- Net Revenue

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.plus_6_days_net_revenue=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set plus_6_days_net_revenue = plus_6_days_net_revenue*(select sum(net_revenue) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date and date_add(o.start_date, interval 5 day) and o.product=t.product), flag=1;

update attribution_testing.output_marketing_campaign_tool set plus_6_days_net_revenue = null where flag is null;

update attribution_testing.output_marketing_campaign_tool set flag = null;

drop table attribution_testing.temporary_proportion;

-- PC1.5

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.`plus_6_days_PC1.5`=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set `plus_6_days_PC1.5` = `plus_6_days_PC1.5`*(select sum(`PC1.5`) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date and date_add(o.start_date, interval 5 day) and o.product=t.product), flag=1;

update attribution_testing.output_marketing_campaign_tool set `plus_6_days_PC1.5` = null where flag is null;

update attribution_testing.output_marketing_campaign_tool set flag = null;

drop table attribution_testing.temporary_proportion;

-- Cost

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.plus_6_days_cost=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set plus_6_days_cost = plus_6_days_cost*(select sum(cost) from attribution_testing.marketing_campaign_tool t where t.date between o.start_date and date_add(o.start_date, interval 5 day) and o.product=t.product), flag=1;

update attribution_testing.output_marketing_campaign_tool set plus_6_days_cost = null where flag is null;

update attribution_testing.output_marketing_campaign_tool set flag = null;

drop table attribution_testing.temporary_proportion;

-- - 6 days

-- Product Views

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.minus_6_days_product_views=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set minus_6_days_product_views = minus_6_days_product_views*(select sum(product_views) from attribution_testing.marketing_campaign_tool t where t.date between date_sub(o.start_date, interval 6 day) and date_sub(o.start_date, interval 1 day) and o.product=t.product), flag=1;

update attribution_testing.output_marketing_campaign_tool set minus_6_days_product_views = null where flag is null;

update attribution_testing.output_marketing_campaign_tool set flag = null;

drop table attribution_testing.temporary_proportion;

-- Add To Cart

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.minus_6_days_add_to_cart=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set minus_6_days_add_to_cart = minus_6_days_add_to_cart*(select sum(add_to_cart) from attribution_testing.marketing_campaign_tool t where t.date between date_sub(o.start_date, interval 6 day) and date_sub(o.start_date, interval 1 day) and o.product=t.product), flag=1;

update attribution_testing.output_marketing_campaign_tool set minus_6_days_add_to_cart = null where flag is null;

update attribution_testing.output_marketing_campaign_tool set flag = null;

drop table attribution_testing.temporary_proportion;

-- Clicks

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.minus_6_days_clicks=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set minus_6_days_clicks = minus_6_days_clicks*(select sum(clicks) from attribution_testing.marketing_campaign_tool t where t.date between date_sub(o.start_date, interval 6 day) and date_sub(o.start_date, interval 1 day) and o.product=t.product), flag=1;

update attribution_testing.output_marketing_campaign_tool set minus_6_days_clicks = null where flag is null;

update attribution_testing.output_marketing_campaign_tool set flag = null;

drop table attribution_testing.temporary_proportion;

-- Impressions

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.minus_6_days_impressions=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set minus_6_days_impressions = minus_6_days_impressions*(select sum(impressions) from attribution_testing.marketing_campaign_tool t where t.date between date_sub(o.start_date, interval 6 day) and date_sub(o.start_date, interval 1 day) and o.product=t.product), flag=1;

update attribution_testing.output_marketing_campaign_tool set minus_6_days_impressions = null where flag is null;

update attribution_testing.output_marketing_campaign_tool set flag = null;

drop table attribution_testing.temporary_proportion;

-- Unit Sold

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.minus_6_days_unit_sold=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set minus_6_days_unit_sold = minus_6_days_unit_sold*(select sum(unit_sold) from attribution_testing.marketing_campaign_tool t where t.date between date_sub(o.start_date, interval 6 day) and date_sub(o.start_date, interval 1 day) and o.product=t.product), flag=1;

update attribution_testing.output_marketing_campaign_tool set minus_6_days_unit_sold = null where flag is null;

update attribution_testing.output_marketing_campaign_tool set flag = null;

drop table attribution_testing.temporary_proportion;

-- Net Revenue

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.minus_6_days_net_revenue=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set minus_6_days_net_revenue = minus_6_days_net_revenue*(select sum(net_revenue) from attribution_testing.marketing_campaign_tool t where t.date between date_sub(o.start_date, interval 6 day) and date_sub(o.start_date, interval 1 day) and o.product=t.product), flag=1;

update attribution_testing.output_marketing_campaign_tool set minus_6_days_net_revenue = null where flag is null;

update attribution_testing.output_marketing_campaign_tool set flag = null;

drop table attribution_testing.temporary_proportion;

-- PC1.5

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.`minus_6_days_PC1.5`=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set `minus_6_days_PC1.5` = `minus_6_days_PC1.5`*(select sum(`PC1.5`) from attribution_testing.marketing_campaign_tool t where t.date between date_sub(o.start_date, interval 6 day) and date_sub(o.start_date, interval 1 day) and o.product=t.product), flag=1;

update attribution_testing.output_marketing_campaign_tool set `minus_6_days_PC1.5` = null where flag is null;

update attribution_testing.output_marketing_campaign_tool set flag = null;

drop table attribution_testing.temporary_proportion;

-- Cost

create table attribution_testing.temporary_proportion as select start_date, end_date, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by start_date, end_date, product;

create index start_date on attribution_testing.temporary_proportion(start_date);
create index end_date on attribution_testing.temporary_proportion(end_date);
create index product on attribution_testing.temporary_proportion(product);

update attribution_testing.output_marketing_campaign_tool t inner join attribution_testing.temporary_proportion c on t.start_date=c.start_date and t.end_date=c.end_date and t.product=c.product set t.minus_6_days_cost=1/c.number;

update attribution_testing.output_marketing_campaign_tool o set minus_6_days_cost = minus_6_days_cost*(select sum(cost) from attribution_testing.marketing_campaign_tool t where t.date between date_sub(o.start_date, interval 6 day) and date_sub(o.start_date, interval 1 day) and o.product=t.product), flag=1;

update attribution_testing.output_marketing_campaign_tool set minus_6_days_cost = null where flag is null;

update attribution_testing.output_marketing_campaign_tool set flag = null;

drop table attribution_testing.temporary_proportion;

-- Stock Out

create table attribution_testing.temporary_count as select week, product, count(*) as number from attribution_testing.output_marketing_campaign_tool group by week, product;

create index week on attribution_testing.temporary_count(week);
create index product on attribution_testing.temporary_count(product);

create table attribution_testing.temporary_stock_out as select date_ordered as date, sku_config from operations_mx.out_order_tracking where is_stockout=1 and date_ordered between @first_date and @last_date group by date_ordered, sku_config order by date_ordered;

create index sku_config on attribution_testing.temporary_stock_out(sku_config);
create index date on attribution_testing.temporary_stock_out(date);

update attribution_testing.output_marketing_campaign_tool o inner join attribution_testing.temporary_count t on t.week=o.week and t.product=o.product set stock_out = (select count(*) from attribution_testing.temporary_stock_out v where v.date between o.start_date and o.end_date and o.product=v.sku_config)/number;

drop table attribution_testing.temporary_count;  

drop table attribution_testing.temporary_stock_out;

-- Week Marketing Campaign Tool

delete from attribution_testing.week_marketing_campaign_tool;

insert into attribution_testing.week_marketing_campaign_tool(yrmonth, week, product, channel, campaign, add_to_cart, product_views, clicks, impressions, cost, unit_sold, net_revenue, `PC1.5`) select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), production.week_iso(date), product, channel, campaign, sum(add_to_cart), sum(product_views), sum(clicks), sum(impressions), sum(cost), sum(unit_sold), sum(net_revenue), sum(`PC1.5`) from attribution_testing.marketing_campaign_tool group by production.week_iso(date), campaign, product;  

update attribution_testing.week_marketing_campaign_tool w inner join development_mx.A_Master_Catalog c on w.product=c.sku_config set w.cat1=c.cat1;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `marketing_campaign_tool_webtrekk`()
begin

set @last_date:=(select max(date) from attribution_testing.channel_marketing_campaign_tool);

insert into attribution_testing.channel_marketing_campaign_tool(date, sid, product, quantity, stat) select date, sid, product, quantity, stat from attribution_testing.getfullbasket_mx where date>@last_date;

-- Channel / Campaign

update attribution_testing.channel_marketing_campaign_tool t inner join attribution_testing.getfullcampaign_mx c on t.sid=c.sid set t.campaign=c.campaign;

update attribution_testing.channel_marketing_campaign_tool t inner join attribution_testing.campaigns_config_mx c on t.campaign=c.data_source_value set t.channel=c.category_2;

update attribution_testing.channel_marketing_campaign_tool set channel='Retargeting' where channel is null and campaign like '%retargeting%';

update attribution_testing.channel_marketing_campaign_tool set channel='Non Identified' where channel is null and campaign is null; 

update attribution_testing.channel_marketing_campaign_tool set campaign='Non Identified' where campaign is null; 

delete from attribution_testing.marketing_campaign_tool;

insert into attribution_testing.marketing_campaign_tool(date, product, channel, campaign) select date, product, channel, campaign from attribution_testing.channel_marketing_campaign_tool group by date, product, channel, campaign;

set @first_date:=(select min(date) from attribution_testing.marketing_campaign_tool);
set @last_date:=(select max(date) from attribution_testing.marketing_campaign_tool);

-- Marketing Campaign Tool

-- Add To Cart

create table attribution_testing.marketing_campaign_add_to_cart as select date, product, channel, campaign, sum(quantity) as quantity from attribution_testing.channel_marketing_campaign_tool t where stat=0 group by date, product, channel, campaign;

create index product on attribution_testing.marketing_campaign_add_to_cart(product);
create index date on attribution_testing.marketing_campaign_add_to_cart(date);
create index channel on attribution_testing.marketing_campaign_add_to_cart(channel);
create index campaign on attribution_testing.marketing_campaign_add_to_cart(campaign);

update attribution_testing.marketing_campaign_tool c inner join attribution_testing.marketing_campaign_add_to_cart t on t.date=c.date and t.product=c.product and t.channel=c.channel and t.campaign=c.campaign set add_to_cart = quantity;

drop table attribution_testing.marketing_campaign_add_to_cart;

-- Product Views

create table attribution_testing.marketing_campaign_product_views as select date, product, channel, campaign, sum(quantity) as quantity from attribution_testing.channel_marketing_campaign_tool t where stat=2 group by date, product, channel, campaign;

create index product on attribution_testing.marketing_campaign_product_views(product);
create index date on attribution_testing.marketing_campaign_product_views(date);
create index channel on attribution_testing.marketing_campaign_product_views(channel);
create index campaign on attribution_testing.marketing_campaign_product_views(campaign);

update attribution_testing.marketing_campaign_tool c inner join attribution_testing.marketing_campaign_product_views t on t.date=c.date and t.product=c.product and t.channel=c.channel and t.campaign=c.campaign set product_views = quantity;

drop table attribution_testing.marketing_campaign_product_views;

-- Clicks

create table attribution_testing.marketing_campaign_clicks select date, substring_index(substr(click, locate(':',click)+1), '.', 1) as product, count(*) as clicks from attribution_testing.getfullclicks_mx where date between @first_date and @last_date group by substring_index(substr(click, locate(':',click)+1), '.', 1), date;

create index product on attribution_testing.marketing_campaign_clicks(product);
create index date on attribution_testing.marketing_campaign_clicks(date);

create table attribution_testing.temporary_count select date, product, sum(product_views) as number from attribution_testing.marketing_campaign_tool group by date, product;

create index product on attribution_testing.temporary_count(product);
create index date on attribution_testing.temporary_count(date);

update attribution_testing.marketing_campaign_tool t inner join attribution_testing.temporary_count c on t.date=c.date and t.product=c.product set t.clicks=t.product_views/c.number;

drop table attribution_testing.temporary_count;

update attribution_testing.marketing_campaign_tool t inner join attribution_testing.marketing_campaign_clicks c on t.date=c.date and t.product=c.product set t.clicks=t.clicks*c.clicks, t.flag=1;

update attribution_testing.marketing_campaign_tool set clicks = null where flag is null;

update attribution_testing.marketing_campaign_tool set flag = null;

drop table attribution_testing.marketing_campaign_clicks;

-- Unit Sold

create table attribution_testing.marketing_campaign_unit_sold select date, skuconfig as product, count(*) as unit_sold from development_mx.A_Master where date between @first_date and @last_date and orderaftercan=1 group by skuconfig, date;

create index product on attribution_testing.marketing_campaign_unit_sold(product);
create index date on attribution_testing.marketing_campaign_unit_sold(date);

create table attribution_testing.temporary_count select date, product, sum(product_views) as number from attribution_testing.marketing_campaign_tool group by date, product;

create index product on attribution_testing.temporary_count(product);
create index date on attribution_testing.temporary_count(date);

update attribution_testing.marketing_campaign_tool t inner join attribution_testing.temporary_count c on t.date=c.date and t.product=c.product set t.unit_sold=t.product_views/c.number;

drop table attribution_testing.temporary_count;

update attribution_testing.marketing_campaign_tool t inner join attribution_testing.marketing_campaign_unit_sold c on t.date=c.date and t.product=c.product set t.unit_sold=t.unit_sold*c.unit_sold, t.flag=1;

update attribution_testing.marketing_campaign_tool set unit_sold = null where flag is null;

update attribution_testing.marketing_campaign_tool set flag = null;

drop table attribution_testing.marketing_campaign_unit_sold;

-- Net Revenue

create table attribution_testing.marketing_campaign_net_revenue select date, skuconfig as product, sum(rev) as net_revenue from development_mx.A_Master where date between @first_date and @last_date and orderaftercan=1 group by skuconfig, date;

create index product on attribution_testing.marketing_campaign_net_revenue(product);
create index date on attribution_testing.marketing_campaign_net_revenue(date);

create table attribution_testing.temporary_count select date, product, sum(product_views) as number from attribution_testing.marketing_campaign_tool group by date, product;

create index product on attribution_testing.temporary_count(product);
create index date on attribution_testing.temporary_count(date);

update attribution_testing.marketing_campaign_tool t inner join attribution_testing.temporary_count c on t.date=c.date and t.product=c.product set t.net_revenue=t.product_views/c.number;

drop table attribution_testing.temporary_count;

update attribution_testing.marketing_campaign_tool t inner join attribution_testing.marketing_campaign_net_revenue c on t.date=c.date and t.product=c.product set t.net_revenue=t.net_revenue*c.net_revenue, t.flag=1;

update attribution_testing.marketing_campaign_tool set net_revenue = null where flag is null;

update attribution_testing.marketing_campaign_tool set flag = null;

drop table attribution_testing.marketing_campaign_net_revenue;

-- PC1.5

create table attribution_testing.`marketing_campaign_PC1.5` select date, skuconfig as product, sum(pconepfive) as `PC1.5` from development_mx.A_Master where date between @first_date and @last_date and orderaftercan=1 group by skuconfig, date;

create index product on attribution_testing.`marketing_campaign_PC1.5`(product);
create index date on attribution_testing.`marketing_campaign_PC1.5`(date);

create table attribution_testing.temporary_count select date, product, sum(product_views) as number from attribution_testing.marketing_campaign_tool group by date, product;

create index product on attribution_testing.temporary_count(product);
create index date on attribution_testing.temporary_count(date);

update attribution_testing.marketing_campaign_tool t inner join attribution_testing.temporary_count c on t.date=c.date and t.product=c.product set t.`PC1.5`=t.product_views/c.number;

drop table attribution_testing.temporary_count;

update attribution_testing.marketing_campaign_tool t inner join attribution_testing.`marketing_campaign_PC1.5` c on t.date=c.date and t.product=c.product set t.`PC1.5`=t.`PC1.5`*c.`PC1.5`, t.flag=1;

update attribution_testing.marketing_campaign_tool set `PC1.5` = null where flag is null;

update attribution_testing.marketing_campaign_tool set flag = null;

drop table attribution_testing.`marketing_campaign_PC1.5`;

-- Impressions TESTING

create table attribution_testing.marketing_campaign_impressions select date, sku_config as product, sum(impressions) as impressions from attribution_testing.getfullimpressions_mx where date between @first_date and @last_date group by sku_config, date;

create index product on attribution_testing.marketing_campaign_impressions(product);
create index date on attribution_testing.marketing_campaign_impressions(date);

create table attribution_testing.temporary_count select date, product, sum(product_views) as number from attribution_testing.marketing_campaign_tool group by date, product;

create index product on attribution_testing.temporary_count(product);
create index date on attribution_testing.temporary_count(date);

update attribution_testing.marketing_campaign_tool t inner join attribution_testing.temporary_count c on t.date=c.date and t.product=c.product set t.impressions=t.product_views/c.number;

drop table attribution_testing.temporary_count;

update attribution_testing.marketing_campaign_tool t inner join attribution_testing.marketing_campaign_impressions c on t.date=c.date and t.product=c.product set t.impressions=t.impressions*c.impressions, t.flag=1;

update attribution_testing.marketing_campaign_tool set impressions = null where flag is null;

update attribution_testing.marketing_campaign_tool set flag = null;

drop table attribution_testing.marketing_campaign_impressions;

-- Cost (Dollar)

create table attribution_testing.marketing_campaign_cost select date, channel_group, sum(cost) as cost from marketing_report.global_report where date between @first_date and @last_date and country='Mexico' group by channel_group, date;

create index channel_group on attribution_testing.marketing_campaign_cost(channel_group);
create index date on attribution_testing.marketing_campaign_cost(date);

create table attribution_testing.temporary_count select date, channel, sum(impressions) as number from attribution_testing.marketing_campaign_tool group by date, channel;

create index product on attribution_testing.temporary_count(channel);
create index date on attribution_testing.temporary_count(date);

update attribution_testing.marketing_campaign_tool t inner join attribution_testing.temporary_count c on t.date=c.date and t.channel=c.channel set t.cost=t.impressions/c.number;

drop table attribution_testing.temporary_count;

update attribution_testing.marketing_campaign_tool t inner join attribution_testing.marketing_campaign_cost c on t.date=c.date and t.channel=c.channel_group set t.cost=t.cost*c.cost, t.flag=1;

update attribution_testing.marketing_campaign_tool set cost = null where flag is null;

update attribution_testing.marketing_campaign_tool set flag = null;

drop table attribution_testing.marketing_campaign_cost;

-- Dictionary

update attribution_testing.marketing_campaign_tool m inner join attribution_testing.dictionary d on m.channel=d.source_channel set m.channel=d.target_channel;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `migration_table`()
begin

delete from attribution_testing.migration_table;

insert into attribution_testing.migration_table(date, order_nr, channel_google_analytics, channel_webtrekk, position, new_customers) select date, order_nr, channel_google_analytics, target_channel, position, new_customers from attribution_testing.middle_recollecting_process_mx;

update attribution_testing.migration_table m inner join production.tbl_order_detail t on t.order_nr=m.order_nr set m.obc=t.obc, m.oac=t.oac;

update attribution_testing.migration_table a inner join attribution_testing.dictionary d on a.channel_google_analytics=d.source_channel set a.channel_google_analytics=d.target_channel;

update attribution_testing.migration_table a inner join attribution_testing.dictionary d on a.channel_webtrekk=d.source_channel set a.channel_webtrekk=d.target_channel;

create table attribution_testing.temporary_webtrekk4(
order_nr varchar(255),
highest_number int
);
create index order_nr on attribution_testing.temporary_webtrekk4(order_nr, highest_number);

insert into attribution_testing.temporary_webtrekk4 select order_nr, max(position) from attribution_testing.migration_table group by order_nr;

update attribution_testing.migration_table a inner join attribution_testing.temporary_webtrekk4 w on a.order_nr=w.order_nr inner join attribution_testing.percent_attribution x on x.clicks=w.highest_number and x.numbers=a.position and x.new_customers=a.new_customers set a.percent=x.result;

update attribution_testing.migration_table set channel_webtrekk=channel_google_analytics where channel_webtrekk is null;

update attribution_testing.migration_table set channel_google_analytics=channel_webtrekk where channel_google_analytics is null;

drop table attribution_testing.temporary_webtrekk4;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `november_attribution`()
begin

/*delete from attribution_testing.november_orders;

insert into attribution_testing.november_orders select date, order_nr, 1, obc, oac, case when new_customers is not null then 1 else 0 end, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)), sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(payment_cost,0)-ifnull(shipping_cost_per_sku,0)) from production.tbl_order_detail where yrmonth=201311 and obc=1 and oac=0 group by order_nr;

insert into attribution_testing.november_orders select date, order_nr, 1, obc, oac, case when new_customers is not null then 1 else 0 end, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)), sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(payment_cost,0)-ifnull(shipping_cost_per_sku,0)) from production.tbl_order_detail where yrmonth=201311 and obc=1 and oac=1 group by order_nr;*/

delete from attribution_testing.global_attribution_mx;

insert into attribution_testing.global_attribution_mx(country, date, channel_group, gross_orders, net_orders, gross_revenue, net_revenue, new_customers_gross, new_customers) select 'Mexico', gross.date, gross.channel, gross.orders, net.orders, gross.revenue, net.revenue, gross.new_customers, net.new_customers from (select t.date, a.channel, sum(t.net_orders*a.percent) as orders, sum(ifnull(unit_price_after_vat*a.percent,0)-ifnull(coupon_money_after_vat*a.percent,0)+ifnull(shipping_fee_after_vat*a.percent,0)) as revenue, sum(new_customers_gross*a.percent) as new_customers from production.tbl_order_detail t, attribution_testing.attribution_model_mx a where obc=1 and t.yrmonth=201311 and a.order_nr=t.order_nr group by date, a.channel)gross left join (select t.date, a.channel, sum(t.net_orders*a.percent) as orders, sum(ifnull(unit_price_after_vat*a.percent,0)-ifnull(coupon_money_after_vat*a.percent,0)+ifnull(shipping_fee_after_vat*a.percent,0)) as revenue, sum(t.new_customers*a.percent) as new_customers from production.tbl_order_detail t, attribution_testing.attribution_model_mx a where oac=1 and t.yrmonth=201311 and a.order_nr=t.order_nr group by date, a.channel)net on gross.date=net.date and gross.channel=net.channel;

/*insert into attribution_testing.global_attribution_mx(country, date, channel_group, gross_orders, net_orders, gross_revenue, net_revenue, `PC1.5`, new_customers_gross, new_customers) select 'Mexico', gross.date, gross.channel, gross.orders, net.orders, gross.revenue/16.35, net.revenue/16.35, net.`PC1.5`/16.35, gross.new_customers, net.new_customers  from (select n.date, a.channel, sum(n.unit*a.percent) as orders, sum(n.revenue*a.percent) as revenue, sum(n.new_customers*a.percent)as new_customers from attribution_testing.november_orders n, attribution_testing.attribution_model_mx a where a.order_nr=n.order_nr and n.obc=1 group by n.date, a.channel)gross left join (select n.date, a.channel, sum(n.unit*a.percent) as orders, sum(n.revenue*a.percent) as revenue, sum(n.new_customers*a.percent) as new_customers, sum(`PC1.5`) as `PC1.5` from attribution_testing.november_orders n, attribution_testing.attribution_model_mx a where a.order_nr=n.order_nr and n.oac=1 group by n.date, a.channel)net on gross.date=net.date and gross.channel=net.channel;*/

update attribution_testing.global_attribution_mx set week=production.week_iso(date), weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end, yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));


update attribution_testing.global_attribution_mx a inner join (select date, channel_group, sum(visits) as visits, sum(cost) as cost from marketing_report.global_report where country='Mexico' group by date, channel_group)g on a.date=g.date and a.channel_group=g.channel_group set a.visits=g.visits, a.cost=g.cost where a.country='Mexico';

-- No Tele Sales

create table attribution_testing.temporary_channel(
date date,
channel_group varchar(255),
gross_orders float,
net_orders float,
gross_revenue float,
net_revenue float,
`PC1.5` float,
new_customers_gross float,
new_customers float,
cost float,
visits float
);

insert into attribution_testing.temporary_channel(date, channel_group) select date, channel_group from attribution_testing.global_attribution_mx where channel_group!='Tele Sales';

create table attribution_testing.percent(
date date,
channel_group varchar(255),
visits int,
total_visits int,
percent float
);

insert into attribution_testing.percent(date, channel_group, visits) select date, channel_group, visits from attribution_testing.global_attribution_mx where channel_group!='Tele Sales';

create table attribution_testing.total(
date date,
visits int
);

insert into attribution_testing.total select date, sum(visits) from attribution_testing.global_attribution_mx where channel_group!='Tele Sales' group by date;

update attribution_testing.percent p inner join attribution_testing.total t on p.date=t.date set p.total_visits=t.visits where channel_group!='Tele Sales';

drop table attribution_testing.total;

update attribution_testing.percent set percent=visits/total_visits where channel_group!='Tele Sales';

create table attribution_testing.temporary_telesales(
date date,
gross_orders int,
net_orders int,
gross_revenue float,
net_revenue float,
`PC1.5` float,
new_customers_gross int,
new_customers int,
cost float,
visits int
);

insert into attribution_testing.temporary_telesales select date, gross_orders, net_orders, gross_revenue, net_revenue, `PC1.5`, new_customers,new_customers_gross, cost, visits from attribution_testing.global_attribution_mx where channel_group='Tele Sales';

update attribution_testing.temporary_telesales t inner join attribution_testing.temporary_channel c on t.date=c.date inner join attribution_testing.percent p on p.date=c.date and p.channel_group=c.channel_group set c.gross_orders=t.gross_orders*p.percent, c.net_orders=t.net_orders*p.percent, c.gross_revenue=t.gross_revenue*p.percent, c.net_revenue=t.net_revenue*p.percent, c.`PC1.5`=t.`PC1.5`*p.percent, c.new_customers_gross=t.new_customers_gross*p.percent, c.new_customers=t.new_customers*p.percent, c.cost=t.cost*p.percent, c.visits=t.visits*p.percent;

drop table attribution_testing.percent;

drop table attribution_testing.temporary_telesales;

delete from attribution_testing.global_attribution_mx where channel_group='Tele Sales';

update attribution_testing.global_attribution_mx g inner join attribution_testing.temporary_channel t on g.date=t.date and g.channel_group=t.channel_group set g.gross_orders=g.gross_orders+t.gross_orders, g.net_orders=g.net_orders+t.net_orders, g.gross_revenue=g.gross_revenue+t.gross_revenue, g.net_revenue=g.net_revenue+t.net_revenue, g.net_revenue=g.net_revenue+t.net_revenue, g.`PC1.5`=g.`PC1.5`+t.`PC1.5`, g.new_customers_gross=g.new_customers_gross+t.new_customers_gross, g.new_customers=g.new_customers+t.new_customers, g.cost=g.cost+t.cost, g.visits=g.visits+t.visits;

update attribution_testing.global_attribution_mx s inner join development_mx.A_E_BI_ExchangeRate_USD r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr where s.country = 'Mexico' and r.country='MEX';

drop table attribution_testing.temporary_channel;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `percent_calculation`()
begin

delete from attribution_testing.percent_attribution;

-- Old Customers

set @k:=0;
set @i:=1;
while @i<=100 do
	set @j=1;
	while @j<=@i do
insert into attribution_testing.percent_attribution(new_customers, clicks, numbers) values (@k, @i, @j);
	set @j:=@j+1;
	end while;
	set @i:=@i+1;
end while;

update attribution_testing.percent_attribution set result = numbers/(clicks*(clicks+1)/2);

-- New Customers

set @k:=1;
set @i:=1;
while @i<=100 do
	set @j=1;
	while @j<=@i do
insert into attribution_testing.percent_attribution(new_customers, clicks, numbers) values (@k, @i, @j);
	set @j:=@j+1;
	end while;
	set @i:=@i+1;
end while;

update attribution_testing.percent_attribution set result=1 where clicks=1 and numbers=1 and new_customers=1;
update attribution_testing.percent_attribution set result=0.5 where clicks=2 and numbers=1 and new_customers=1;
update attribution_testing.percent_attribution set result=0.5 where clicks=2 and numbers=2 and new_customers=1;
update attribution_testing.percent_attribution set result=0.2/(clicks-2) where clicks>=3 and new_customers=1;
update attribution_testing.percent_attribution set result=0.4 where numbers=1 and clicks>=3 and new_customers=1;
update attribution_testing.percent_attribution set result=0.4 where clicks=numbers and clicks>=3 and new_customers=1;
 

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:03:26
