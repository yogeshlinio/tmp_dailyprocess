-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: operations_ve
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'operations_ve'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 FUNCTION `calcworkdays`(sdate varchar(50), edate varchar(50)) RETURNS int(11)
begin
 declare wdays, tdiff, counter, thisday smallint;
 declare newdate date;
 set newdate := sdate;
 set wdays = 0;
 if sdate is null then return 0; end if;
 if edate is null then return 0; end if;
 if edate like '1%' then return 0; end if;
 if edate like '0%' then return 0; end if;
 if sdate like '1%' then return 0; end if;
 if sdate like '0%' then return 0; end if;
    if datediff(edate, sdate) <= 0 then return 0; end if;
         label1: loop
          set thisday = dayofweek(newdate);
            if thisday in(1,2,3,4,5) then set wdays := wdays + 1; end if;
             set newdate = date_add(newdate, interval 1 day);
            if datediff(edate, newdate) <= 0 then leave label1; end if;
       end loop label1;
      return wdays;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 FUNCTION `dias_habiles_negativos`(fecha_min_delivery DATE, fecha_stc DATE) RETURNS int(11)
BEGIN	
	DECLARE dias_habiles_fixed INT;
	DECLARE fecha_inicio DATE;
	DECLARE fecha_final DATE;

	IF (fecha_min_delivery < fecha_stc) THEN SET fecha_final = fecha_stc;
	SET fecha_inicio = fecha_min_delivery;
	END IF;

	IF (fecha_min_delivery > fecha_stc) THEN SET fecha_inicio = fecha_stc;
	SET fecha_final = fecha_min_delivery;
	END IF;

	SET dias_habiles_fixed = (SELECT COUNT(*) FROM calendar WHERE (dt BETWEEN fecha_inicio AND fecha_final) AND isweekday=1 AND isholiday=0);
	IF (weekday(fecha_inicio)<>6 AND weekday(fecha_inicio)<>5)
		THEN SET dias_habiles_fixed = (dias_habiles_fixed-1);
	END IF;

	IF (fecha_min_delivery = fecha_final)
	THEN SET dias_habiles_fixed = dias_habiles_fixed *-1;
	END IF;
	
RETURN dias_habiles_fixed;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 FUNCTION `maximum`(procure integer, ready integer, to_ship integer,  attempt integer ) RETURNS int(11)
begin
declare compare integer; 
		set compare = procure;
		if compare < ready then set compare = ready; end if;
		if compare < to_ship then set compare = to_ship; end if;
		if compare < attempt then set compare = attempt; end if;
 return compare;
 end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 FUNCTION `week_iso`(edate date) RETURNS varchar(20) CHARSET latin1
begin
 declare sdate varchar(20);
 if year(edate) = 2012 then set sdate = concat(year(edate), '-',(week(edate, 5))+1); else set sdate = date_format(edate, "%x-%v"); end if;
      return sdate;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 FUNCTION `workday`(edate date, workdays integer) RETURNS date
begin
 declare wdays, thisday, thisday0, timer smallint;
 declare newdate, middate date;
 set wdays = 0;
 set newdate :=  edate;
 if edate is null then set newdate = '2012-05-01'; end if;
 if workdays is null then return '2012-05-01'; end if;
 set thisday0 = dayofweek(edate);
 if workdays in (0) and thisday0 between 2 and 6 then return newdate; end if;
 if (workdays in (0) and thisday0 in (7)) then return date(adddate(newdate, 2)); end if;
 if (workdays in (0) and thisday0 in (1)) then return date(adddate(newdate, 1)); end if;
	label2: loop
		set newdate = date(adddate(newdate, 1));
        set thisday = dayofweek(newdate);
		if thisday between 2 and 6 then 
			set wdays = wdays + 1;
			if wdays >= workdays then 
				leave label2; 
			end if;
		else 
			set wdays = wdays;
		end if;
	end loop label2;
return newdate;
 end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`ops_ve`@`%`*/ /*!50003 PROCEDURE `bi_ops_tracking_code`()
BEGIN

SELECT  'Insert item_id y coperations_ve.codigo de rastreamento',now();
TRUNCATE TABLE operations_ve.tbl_bi_ops_guiasbyitem2;

SELECT  'Insert item_id itens_venda',now();
INSERT INTO operations_ve.tbl_bi_ops_guiasbyitem2 (item_id,cod_rastreamento)
SELECT out_order_tracking_sample.item_id,out_order_tracking_sample.wms_tracking_code
FROM operations_ve.out_order_tracking_sample;

SELECT  'Actualizar tbl_bi_ops_tms_tracks2 segun tabla de wms tms_tracks',now();
#TRUNCATE TABLE operations_ve.tbl_bi_ops_tms_tracks2;

DROP TABLE IF EXISTS operations_ve.tbl_bi_ops_tms_tracks2;

CREATE TABLE operations_ve.tbl_bi_ops_tms_tracks2 LIKE wmsprod_ve.tms_tracks;

INSERT INTO operations_ve.tbl_bi_ops_tms_tracks2 SELECT * FROM  wmsprod_ve.tms_tracks;

SELECT  'GUIA 1',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2 b INNER JOIN operations_ve.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia1=a.track;

DELETE FROM operations_ve.tbl_bi_ops_tms_tracks2
WHERE track IN (SELECT guia1 FROM operations_ve.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 2',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2 b
INNER JOIN operations_ve.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia2=a.track;

DELETE FROM operations_ve.tbl_bi_ops_tms_tracks2
WHERE track IN (SELECT guia2 FROM operations_ve.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 3',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2 b
INNER JOIN operations_ve.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia3=a.track;

DELETE FROM operations_ve.tbl_bi_ops_tms_tracks2
WHERE track IN (SELECT guia3 FROM operations_ve.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 4',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2 b
INNER JOIN operations_ve.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia4=a.track;	

DELETE FROM operations_ve.tbl_bi_ops_tms_tracks2
WHERE track IN (SELECT guia4 FROM operations_ve.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 5',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2 b
INNER JOIN operations_ve.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia5=a.track;	

DELETE FROM operations_ve.tbl_bi_ops_tms_tracks2
WHERE track IN (SELECT guia5 FROM operations_ve.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 6',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2 b
inner join operations_ve.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia6=a.track;	

DELETE from operations_ve.tbl_bi_ops_tms_tracks2
where track in (select guia6 from operations_ve.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 7',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2 b
inner join operations_ve.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia7=a.track;

DELETE from operations_ve.tbl_bi_ops_tms_tracks2
where track in (select guia7 from operations_ve.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 8',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2 b
inner join operations_ve.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia8=a.track;

DELETE from operations_ve.tbl_bi_ops_tms_tracks2
where track in (select guia8 from operations_ve.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 9',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2 b
inner join operations_ve.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia9=a.track;

DELETE from operations_ve.tbl_bi_ops_tms_tracks2
where track in (select guia9 from operations_ve.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 10',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2 b
inner join operations_ve.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia10=a.track;

DELETE from operations_ve.tbl_bi_ops_tms_tracks2 where track in (select guia10 from operations_ve.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 11',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2 b inner join operations_ve.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia11=a.track;

DELETE from operations_ve.tbl_bi_ops_tms_tracks2 where track in (select guia11 from operations_ve.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 12',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2 b
inner join operations_ve.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia12=a.track;

DELETE from operations_ve.tbl_bi_ops_tms_tracks2 where track in (select guia12 from operations_ve.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 13',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2 b
inner join operations_ve.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia13=a.track;

DELETE from operations_ve.tbl_bi_ops_tms_tracks2 where track in (select guia13 from operations_ve.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 14',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2 b inner join operations_ve.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia14=a.track;

DELETE from operations_ve.tbl_bi_ops_tms_tracks2 where track in (select guia14 from operations_ve.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 15',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2 b inner join operations_ve.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia15=a.track;

DELETE from operations_ve.tbl_bi_ops_tms_tracks2 where track in (select guia15 from operations_ve.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 16',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2 b inner join operations_ve.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia16=a.track;

DELETE from operations_ve.tbl_bi_ops_tms_tracks2 where track in (select guia16 from operations_ve.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 17',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2 b inner join operations_ve.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia17=a.track;

DELETE from operations_ve.tbl_bi_ops_tms_tracks2 where track in (select guia17 from operations_ve.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 18',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2 b
inner join operations_ve.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia18=a.track;

DELETE from operations_ve.tbl_bi_ops_tms_tracks2 where track in (select guia18 from operations_ve.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 19',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2 b
inner join operations_ve.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia19=a.track;

DELETE from operations_ve.tbl_bi_ops_tms_tracks2 where track in (select guia19 from operations_ve.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 20',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2 b inner join operations_ve.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia20=a.track;

DELETE from operations_ve.tbl_bi_ops_tms_tracks2 where track in (select guia20 from operations_ve.tbl_bi_ops_guiasbyitem2);

SELECT  'concatenar',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET total_guias = NULL;

SELECT  'concatenar1',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET total_guias = guia1
WHERE guia1 is not null
AND guia2 is null;

SELECT  'concatenar2',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2)
WHERE guia2 is not null
AND guia3 is null;

SELECT  'concatenar3',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3)
WHERE guia3 is not null
AND guia4 is null;

SELECT  'concatenar4',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4)
WHERE guia4 is not null
AND guia5 is null;

SELECT  'concatenar5',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5)
WHERE guia5 is not null
AND guia6 is null;

SELECT  'concatenar6',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6)
WHERE guia6 is not null
AND guia7 is null;

SELECT  'concatenar7',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7)
WHERE guia7 is not null
AND guia8 is null;

SELECT  'concatenar8',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8)
WHERE guia8 is not null
AND guia9 is null;

SELECT  'concatenar9',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8,'-',guia9)
WHERE guia9 is not null
AND guia10 is null;

SELECT  'concatenar10',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8,'-',guia9,'-',guia10)
WHERE guia10 is not null
AND guia11 is null;

SELECT  'concatenar11',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8,'-',guia9,'-',guia10,'-',guia11)
WHERE guia11 is not null
AND guia12 is null;

SELECT  'concatenar12',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8,'-',guia9,'-',guia10,'-',guia11,'-',guia12)
WHERE guia12 is not null
AND guia13 is null;

SELECT  'concatenar13',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8,'-',guia9,'-',guia10,'-',guia11
,'-',guia12,'-',guia13)
WHERE guia13 is not null
AND guia14 is null;

SELECT  'concatenar14',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8,'-',guia9,'-',guia10,'-',guia11
,'-',guia12,'-',guia13,'-',guia14)
WHERE guia14 is not null
AND guia15 is null;

SELECT  'concatenar15',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8,'-',guia9,'-',guia10,'-',guia11
,'-',guia12,'-',guia13,'-',guia14,'-',guia15)
WHERE guia15 is not null
AND guia16 is null;

SELECT  'concatenar16',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8,'-',guia9,'-',guia10,'-',guia11
,'-',guia12,'-',guia13,'-',guia14,'-',guia15,'-',guia16)
WHERE guia16 is not null
AND guia17 is null;

SELECT  'concatenar17',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8,'-',guia9,'-',guia10,'-',guia11
,'-',guia12,'-',guia13,'-',guia14,'-',guia15,'-',guia16,'-',guia17)
WHERE guia17 is not null
AND guia18 is null;

SELECT  'concatenar18',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8,'-',guia9,'-',guia10,'-',guia11
,'-',guia12,'-',guia13,'-',guia14,'-',guia15,'-',guia16,'-',guia17,'-',guia18)
WHERE guia18 is not null
AND guia19 is null;

SELECT  'concatenar19',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8,'-',guia9,'-',guia10,'-',guia11
,'-',guia12,'-',guia13,'-',guia14,'-',guia15,'-',guia16,'-',guia17,'-',guia18,'-',guia19)
WHERE guia19 is not null
AND guia20 is null;

SELECT  'concatenar20',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8,'-',guia9,'-',guia10,'-',guia11
,'-',guia12,'-',guia13,'-',guia14,'-',guia15,'-',guia16,'-',guia17,'-',guia18,'-',guia19,'-',guia20)
WHERE guia20 is not null;

SELECT  'buscar y reemplazar --',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET total_guias = REPLACE(total_guias,'--','-');

SELECT  'guia1 -',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET
guia1 = if(instr(guia1,'-')=0,guia1,LEFT(total_guias,instr(total_guias,'-')-1)),
guia2 = if(instr(guia1,'-')=0,guia2,left(right(total_guias,length(total_guias)-instr(total_guias,'-')),instr(right(total_guias,length(total_guias)-instr(total_guias,'-')),'-')-1)),
guia3 = if(instr(guia1,'-')=0,guia3,left(right(total_guias,length(total_guias)-instr(total_guias,'-')-instr(total_guias,'-')),instr(right(total_guias,length(total_guias)-instr(total_guias,'-')),'-')-1));

SELECT  'buscar y reemplazar doble guion por un guion--',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET total_guias = REPLACE(total_guias,'--','-');

SELECT  'buscar y reemplazar espacios por vacios--',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
SET total_guias = REPLACE(total_guias,' ','');

SELECT  'validacion igual a total guias',now();
TRUNCATE TABLE operations_ve.validacion_track;

SELECT  'validacion igual a total guias',now();
INSERT INTO operations_ve.validacion_track SELECT item_id,total_guias FROM operations_ve.tbl_bi_ops_guiasbyitem2;

SELECT  'actualizar guia1',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia1 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia1',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia2',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia2 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia2',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia3',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia3 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia3',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia4',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia4 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia4',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia5',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia5 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia5',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia6',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia6 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia6',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia7',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia7 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia7',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia8',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia8 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia8',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia9',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia9 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia9',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia10',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia10 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia10',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia11',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia11 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia11',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia12',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia12 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia12',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia13',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia13 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia13',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia14',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia14 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia14',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia15',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia15 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia15',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia16',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia16 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia16',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia17',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia17 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia17',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia18',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia18 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia18',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia19',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia19 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia19',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia20',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia20 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia20',now();
UPDATE operations_ve.tbl_bi_ops_guiasbyitem2
inner join operations_ve.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'Rutina finalizada bi_ops_tracking_code',now();

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`ops_ve`@`%`*/ /*!50003 PROCEDURE `calcworkdays`()
begin

DECLARE bDone, x INT;
DECLARE pasado INT;
DECLARE futuro INT;

DECLARE vsDate date;


DECLARE curs_dt	date;
DECLARE curs_isweekday	bit(1);
DECLARE curs_isholiday	bit(1);
DECLARE curs_isweekend	bit(1);
DECLARE curs_yr	smallint(6);
DECLARE curs_qtr	tinyint(4);
DECLARE curs_mt	tinyint(4);
DECLARE curs_dy	tinyint(4);
DECLARE curs_dyw	tinyint(4);
DECLARE curs_wk	tinyint(4);

#SELECT 180,60  INTO pasado, futuro ;
DECLARE curs CURSOR FOR SELECT * FROM calendar WHERE dt BETWEEN dt - INTERVAL 180 DAY AND dt ORDER BY dt ASC;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET bDone = 1;

#drop table if exists calcworkdays;
create table if not exists  calcworkdays(
date_first date,
date_last date,
workdays int,

isweekday int,
isholiday int,
isweekend int,

isweekday_first int,
isholiday_first int,
isweekend_first int,

primary key ( date_first ,date_last ),
index date (date_first, date_last),
index date_first  ( date_first  ),
index date_last   ( date_last  ),
index date_last_wd  ( date_last  , workdays),
index date_first_wd ( date_first , workdays)
);


SET pasado=180;
SET futuro = 180;

OPEN curs;
SET bDone = 0;
REPEAT
FETCH curs INTO curs_dt,
                       curs_isweekday,
                       curs_isholiday,
                       curs_isweekend,
                       curs_yr,
                       curs_qtr,
                       curs_mt,
                       curs_dy,
                       curs_dyw,
                       curs_wk	
                       ;

#    IF whatever_filtering_desired
       -- here for whatever_transformation_may_be_desired
   SET x =0;
   SET vsDate = curs_dt;
   WHILE x<=pasado DO
 
    REPLACE INTO calcworkdays ( date_last , date_first , workdays )
    SELECT vsDate , curs_dt , sum( IF( dt != curs_dt , isweekday - if( isweekday = 1 , isholiday , 0 ), 0  ) ) 
    FROM calendar 
    where dt between curs_dt and vsDate;
		set x =x+1; 
		set vsDate = curs_dt - interval x day;
	end while;

   SET x =0;
   SET vsDate = curs_dt;
   WHILE x<=futuro DO
 
    REPLACE INTO calcworkdays ( date_last , date_first , workdays )
    SELECT vsDate , curs_dt , sum( IF( dt != curs_dt , isweekday - if( isweekday = 1 , isholiday , 0 ), 0  ) ) 
    FROM calendar 
    where dt between curs_dt and vsDate;
		set x =x+1; 
		set vsDate = curs_dt + interval x day;
	end while;


UNTIL bDone END REPEAT;
CLOSE curs;

UPDATE        calcworkdays 
   INNER JOIN calendar 
        ON calcworkdays.date_last = calendar.dt
SET
   calcworkdays.isweekday = calendar.isweekday,
   calcworkdays.isholiday = calendar.isholiday,
   calcworkdays.isweekend = calendar.isweekend
;

UPDATE        calcworkdays 
   INNER JOIN calendar 
        ON calcworkdays.date_first = calendar.dt
SET
   calcworkdays.isweekday_first = calendar.isweekday,
   calcworkdays.isholiday_first = calendar.isholiday,
   calcworkdays.isweekend_first = calendar.isweekend
;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 PROCEDURE `calcworkdays_backup`()
begin

DECLARE bDone, x INT;
DECLARE pasado INT;
DECLARE futuro INT;

DECLARE vsDate date;


DECLARE curs_dt	date;
DECLARE curs_isweekday	bit(1);
DECLARE curs_isholiday	bit(1);
DECLARE curs_isweekend	bit(1);
DECLARE curs_yr	smallint(6);
DECLARE curs_qtr	tinyint(4);
DECLARE curs_mt	tinyint(4);
DECLARE curs_dy	tinyint(4);
DECLARE curs_dyw	tinyint(4);
DECLARE curs_wk	tinyint(4);

#SELECT 180,60  INTO pasado, futuro ;
DECLARE curs CURSOR FOR SELECT * FROM calendar WHERE dt BETWEEN dt - INTERVAL 180 DAY AND dt ORDER BY dt ASC;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET bDone = 1;

#drop table if exists calcworkdays;
create table if not exists  calcworkdays(
date_first date,
date_last date,
workdays int,

isweekday int,
isholiday int,
isweekend int,

isweekday_first int,
isholiday_first int,
isweekend_first int,

primary key ( date_first ,date_last ),
index date (date_first, date_last),
index date_first  ( date_first  ),
index date_last   ( date_last  ),
index date_last_wd  ( date_last  , workdays),
index date_first_wd ( date_first , workdays)
);


SET pasado=180;
SET futuro = 180;

OPEN curs;
SET bDone = 0;
REPEAT
FETCH curs INTO curs_dt,
                       curs_isweekday,
                       curs_isholiday,
                       curs_isweekend,
                       curs_yr,
                       curs_qtr,
                       curs_mt,
                       curs_dy,
                       curs_dyw,
                       curs_wk	
                       ;

#    IF whatever_filtering_desired
       -- here for whatever_transformation_may_be_desired
   SET x =0;
   SET vsDate = curs_dt;
   WHILE x<=pasado DO
 
    REPLACE INTO calcworkdays ( date_last , date_first , workdays )
    SELECT vsDate , curs_dt , sum( IF( dt != curs_dt , isweekday - if( isweekday = 1 , isholiday , 0 ), 0  ) ) 
    FROM calendar 
    where dt between curs_dt and vsDate;
		set x =x+1; 
		set vsDate = curs_dt - interval x day;
	end while;

   SET x =0;
   SET vsDate = curs_dt;
   WHILE x<=futuro DO
 
    REPLACE INTO calcworkdays ( date_last , date_first , workdays )
    SELECT vsDate , curs_dt , sum( IF( dt != curs_dt , isweekday - if( isweekday = 1 , isholiday , 0 ), 0  ) ) 
    FROM calendar 
    where dt between curs_dt and vsDate;
		set x =x+1; 
		set vsDate = curs_dt + interval x day;
	end while;


UNTIL bDone END REPEAT;
CLOSE curs;

UPDATE        calcworkdays 
   INNER JOIN calendar 
        ON calcworkdays.date_last = calendar.dt
SET
   calcworkdays.isweekday = calendar.isweekday,
   calcworkdays.isholiday = calendar.isholiday,
   calcworkdays.isweekend = calendar.isweekend
;

UPDATE        calcworkdays 
   INNER JOIN calendar 
        ON calcworkdays.date_first = calendar.dt
SET
   calcworkdays.isweekday_first = calendar.isweekday,
   calcworkdays.isholiday_first = calendar.isholiday,
   calcworkdays.isweekend_first = calendar.isweekend
;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`ops_ve`@`%`*/ /*!50003 PROCEDURE `calcworkdays_inventory`()
begin

DECLARE bDone, x INT;
DECLARE pasado INT;
DECLARE futuro INT;

DECLARE vsDate date;


DECLARE curs_dt	date;
DECLARE curs_isweekday	bit(1);
DECLARE curs_isholiday	bit(1);
DECLARE curs_isweekend	bit(1);
DECLARE curs_yr	smallint(6);
DECLARE curs_qtr	tinyint(4);
DECLARE curs_mt	tinyint(4);
DECLARE curs_dy	tinyint(4);
DECLARE curs_dyw	tinyint(4);
DECLARE curs_wk	tinyint(4);

#SELECT 180,60  INTO pasado, futuro ;
DECLARE curs CURSOR FOR SELECT * FROM calendar WHERE dt BETWEEN dt - INTERVAL 180 DAY AND dt ORDER BY dt ASC;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET bDone = 1;

#drop table if exists calcworkdays;
create table if not exists  calcworkdays_inventory(
date_first date,
date_last date,
workdays int,

isweekday int,
isholiday int,
isweekend int,

isweekday_first int,
isholiday_first int,
isweekend_first int,

primary key ( date_first ,date_last ),
index date (date_first, date_last),
index date_first  ( date_first  ),
index date_last   ( date_last  ),
index date_last_wd  ( date_last  , workdays),
index date_first_wd ( date_first , workdays)
);


SET pasado=180;
SET futuro = 180;

OPEN curs;
SET bDone = 0;
REPEAT
FETCH curs INTO curs_dt,
                       curs_isweekday,
                       curs_isholiday,
                       curs_isweekend,
                       curs_yr,
                       curs_qtr,
                       curs_mt,
                       curs_dy,
                       curs_dyw,
                       curs_wk	
                       ;

#    IF whatever_filtering_desired
       -- here for whatever_transformation_may_be_desired
   SET x =0;
   SET vsDate = curs_dt;
   WHILE x<=pasado DO
 
    REPLACE INTO calcworkdays_inventory ( date_last , date_first , workdays )
    SELECT vsDate , curs_dt , sum(isweekday - if( isweekday = 1 , isholiday , 0 )) 
    FROM calendar 
    where dt between curs_dt and vsDate;
		set x =x+1; 
		set vsDate = curs_dt - interval x day;
	end while;

   SET x =0;
   SET vsDate = curs_dt;
   WHILE x<=futuro DO
 
    REPLACE INTO calcworkdays_inventory ( date_last , date_first , workdays )
    SELECT vsDate , curs_dt , sum(isweekday - if( isweekday = 1 , isholiday , 0 )) 
    FROM calendar 
    where dt between curs_dt and vsDate;
		set x =x+1; 
		set vsDate = curs_dt + interval x day;
	end while;


UNTIL bDone END REPEAT;
CLOSE curs;

UPDATE        calcworkdays_inventory 
   INNER JOIN calendar 
        ON calcworkdays_inventory.date_last = calendar.dt
SET
   calcworkdays_inventory.isweekday = calendar.isweekday,
   calcworkdays_inventory.isholiday = calendar.isholiday,
   calcworkdays_inventory.isweekend = calendar.isweekend
;

UPDATE        calcworkdays_inventory 
   INNER JOIN calendar 
        ON calcworkdays_inventory.date_first = calendar.dt
SET
   calcworkdays_inventory.isweekday_first = calendar.isweekday,
   calcworkdays_inventory.isholiday_first = calendar.isholiday,
   calcworkdays_inventory.isweekend_first = calendar.isweekend
;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 PROCEDURE `daily_execution_ops`()
    MODIFIES SQL DATA
BEGIN

CALL operations_ve.out_stock_hist;

CALL operations_ve.out_order_tracking;

#CALL operations_ve.out_procurement_tracking;

CALL operations_ve.out_inverse_logistics_tracking;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`ops_ve`@`%`*/ /*!50003 PROCEDURE `out_catalog_product_stock`()
BEGIN

SELECT 'Start: catalog_product_v2', now();

/*
# Simple Variation
DROP TABLE IF EXISTS operations_ve.simple_variation;

CREATE TABLE operations_ve.simple_variation AS
(
	SELECT
		`bob_live_ve`.`catalog_simple_books`.`fk_catalog_simple` AS `fk_catalog_simple`,
		`bob_live_ve`.`catalog_simple_books`.`variation` AS `variation`
	FROM
		`bob_live_ve`.`catalog_simple_books`
	WHERE	`bob_live_ve`.`catalog_simple_books`.`variation` IS NOT NULL
	AND		`bob_live_ve`.`catalog_simple_books`.`variation` <> '...'
	AND 	`bob_live_ve`.`catalog_simple_books`.`variation` <> '…'
	AND 	`bob_live_ve`.`catalog_simple_books`.`variation` <> '?'
	AND 	`bob_live_ve`.`catalog_simple_books`.`variation` <> ',,,'
)
UNION
	(
		SELECT
			`bob_live_ve`.`catalog_simple_electronics`.`fk_catalog_simple` AS `fk_catalog_simple`,
			`bob_live_ve`.`catalog_simple_electronics`.`variation` AS `variation`
		FROM
			`bob_live_ve`.`catalog_simple_electronics`
		WHERE	`bob_live_ve`.`catalog_simple_electronics`.`variation` IS NOT NULL
		AND 	`bob_live_ve`.`catalog_simple_electronics`.`variation` <> '...'
		AND 	`bob_live_ve`.`catalog_simple_electronics`.`variation` <> '…'
		AND 	`bob_live_ve`.`catalog_simple_electronics`.`variation` <> '?'
		AND 	`bob_live_ve`.`catalog_simple_electronics`.`variation` <> ',,,'
	)
UNION
	(
		SELECT
			`bob_live_ve`.`catalog_simple_toys_baby`.`fk_catalog_simple` AS `fk_catalog_simple`,
			`bob_live_ve`.`catalog_simple_toys_baby`.`variation` AS `variation`
		FROM
			`bob_live_ve`.`catalog_simple_toys_baby`
		WHERE	`bob_live_ve`.`catalog_simple_toys_baby`.`variation` IS NOT NULL
		AND		`bob_live_ve`.`catalog_simple_toys_baby`.`variation` <> '...'
		AND		`bob_live_ve`.`catalog_simple_toys_baby`.`variation` <> '…'
		AND		`bob_live_ve`.`catalog_simple_toys_baby`.`variation` <> '?'
		AND		`bob_live_ve`.`catalog_simple_toys_baby`.`variation` <> ',,,'
	)
UNION
	(
		SELECT
			`bob_live_ve`.`catalog_simple_health_beauty`.`fk_catalog_simple` AS `fk_catalog_simple`,
			`bob_live_ve`.`catalog_simple_health_beauty`.`variation` AS `variation`
		FROM
			`bob_live_ve`.`catalog_simple_health_beauty`
		WHERE	`bob_live_ve`.`catalog_simple_health_beauty`.`variation` IS NOT NULL
		AND		`bob_live_ve`.`catalog_simple_health_beauty`.`variation` <> '...'
		AND		`bob_live_ve`.`catalog_simple_health_beauty`.`variation` <> '…'
		AND		`bob_live_ve`.`catalog_simple_health_beauty`.`variation` <> '?'
		AND		`bob_live_ve`.`catalog_simple_health_beauty`.`variation` <> ',,,'
	)
UNION
	(
		SELECT
			`bob_live_ve`.`catalog_simple_home_living`.`fk_catalog_simple` AS `fk_catalog_simple`,
			`bob_live_ve`.`catalog_simple_home_living`.`variation` AS `variation`
		FROM
			`bob_live_ve`.`catalog_simple_home_living`
		WHERE	`bob_live_ve`.`catalog_simple_home_living`.`variation` IS NOT NULL
		AND		`bob_live_ve`.`catalog_simple_home_living`.`variation` <> '...'
		AND		`bob_live_ve`.`catalog_simple_home_living`.`variation` <> '…'
		AND		`bob_live_ve`.`catalog_simple_home_living`.`variation` <> '?'
		AND		`bob_live_ve`.`catalog_simple_home_living`.`variation` <> ',,,'
	)
UNION
	(
		SELECT
			`bob_live_ve`.`catalog_simple_media`.`fk_catalog_simple` AS `fk_catalog_simple`,
			`bob_live_ve`.`catalog_simple_media`.`variation` AS `variation`
		FROM
			`bob_live_ve`.`catalog_simple_media`
		WHERE	`bob_live_ve`.`catalog_simple_media`.`variation` IS NOT NULL
		AND		`bob_live_ve`.`catalog_simple_media`.`variation` <> '...'
		AND		`bob_live_ve`.`catalog_simple_media`.`variation` <> '…'
		AND		`bob_live_ve`.`catalog_simple_media`.`variation` <> '?'
		AND		`bob_live_ve`.`catalog_simple_media`.`variation` <> ',,,'
	)
UNION
	(
		SELECT
			`bob_live_ve`.`catalog_simple_other`.`fk_catalog_simple` AS `fk_catalog_simple`,
			`bob_live_ve`.`catalog_simple_other`.`variation` AS `variation`
		FROM
			`bob_live_ve`.`catalog_simple_other`
		WHERE	`bob_live_ve`.`catalog_simple_other`.`variation` IS NOT NULL
		AND		`bob_live_ve`.`catalog_simple_other`.`variation` <> '...'
		AND		`bob_live_ve`.`catalog_simple_other`.`variation` <> '…'
		AND		`bob_live_ve`.`catalog_simple_other`.`variation` <> '?'
		AND		`bob_live_ve`.`catalog_simple_other`.`variation` <> ',,,'
	)
UNION
	(
		SELECT
			`bob_live_ve`.`catalog_simple_sports`.`fk_catalog_simple` AS `fk_catalog_simple`,
			`bob_live_ve`.`catalog_simple_sports`.`variation` AS `variation`
		FROM
			`bob_live_ve`.`catalog_simple_sports`
		WHERE	`bob_live_ve`.`catalog_simple_sports`.`variation` IS NOT NULL
		AND		`bob_live_ve`.`catalog_simple_sports`.`variation` <> '...'
		AND		`bob_live_ve`.`catalog_simple_sports`.`variation` <> '…'
		AND		`bob_live_ve`.`catalog_simple_sports`.`variation` <> '?'
		AND		`bob_live_ve`.`catalog_simple_sports`.`variation` <> ',,,'
	)
UNION
	(
		SELECT
			`bob_live_ve`.`catalog_simple_fashion`.`fk_catalog_simple` AS `fk_catalog_simple`,
			`bob_live_ve`.`catalog_attribute_option_fashion_size`.`name` AS `name`
		FROM
			(
				`bob_live_ve`.`catalog_simple_fashion`
				JOIN `bob_live_ve`.`catalog_attribute_option_fashion_size`
			)
		WHERE `bob_live_ve`.`catalog_simple_fashion`.`fk_catalog_attribute_option_fashion_size` = `bob_live_ve`.`catalog_attribute_option_fashion_size`.`id_catalog_attribute_option_fashion_size`
		AND		`bob_live_ve`.`catalog_attribute_option_fashion_size`.`name` IS NOT NULL
	);

ALTER TABLE operations_ve.simple_variation ADD PRIMARY KEY (`fk_catalog_simple`) ;

*/

# out_catalog_product_stock
TRUNCATE operations_ve.out_catalog_product_stock;

INSERT INTO out_catalog_product_stock(
	fk_catalog_simple,
	sku,
	reservedbob,
	stockbob,
	availablebob)
SELECT 
	catalog_simple.id_catalog_simple,
	catalog_simple.sku,
	IF(sum(is_reserved) IS NULL, 0, sum(is_reserved))  AS reservedBOB,
	ifnull(catalog_stock.quantity,0) AS stockbob,
	ifnull(catalog_stock.quantity,0)-IF(sum(sales_order_item.is_reserved) IS NULL, 0, sum(is_reserved)) AS availablebob
FROM 
(bob_live_ve.catalog_simple
INNER JOIN bob_live_ve.catalog_source
 ON catalog_simple.id_catalog_simple = catalog_source.fk_catalog_simple
INNER JOIN bob_live_ve.catalog_stock
 ON catalog_source.id_catalog_source = catalog_stock.fk_catalog_source) 
	LEFT JOIN bob_live_ve.sales_order_item
		ON catalog_simple.sku = sales_order_item.sku
GROUP BY catalog_simple.sku
ORDER BY catalog_stock.quantity DESC;

UPDATE operations_ve.out_catalog_product_stock 
INNER JOIN bob_live_ve.catalog_warehouse_stock 
	ON out_catalog_product_stock.fk_catalog_simple = catalog_warehouse_stock.fk_catalog_simple
SET
	out_catalog_product_stock.ownstock = catalog_warehouse_stock.quantity;

UPDATE operations_ve.out_catalog_product_stock 
INNER JOIN bob_live_ve.catalog_supplier_stock 
	ON out_catalog_product_stock.fk_catalog_simple = catalog_supplier_stock.fk_catalog_simple
SET 
 out_catalog_product_stock.supplierstock = catalog_supplier_stock.quantity;

UPDATE operations_ve.out_catalog_product_stock 
SET availablebob = 0 
WHERE availablebob < 0;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`ops_ve`@`%`*/ /*!50003 PROCEDURE `out_inverse_logistics_tracking`()
BEGIN

SELECT  'Start operations_ve.out_inverse_logistics_tracking',now();

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Venezuela_Project', 
  'out_inverse_logistics_tracking',
  'start',
  NOW(),
  max(date_ordered),
  count(*),
  count(item_counter)
FROM
  production_ve.out_inverse_logistics_tracking;

SELECT  'Truncate out_inverse_logistics_tracking',now();
TRUNCATE	operations_ve.out_inverse_logistics_tracking;

SELECT  'Insert out_inverse_logistics_tracking',now();
INSERT INTO operations_ve.out_inverse_logistics_tracking (
	item_id,
	order_number,
	sku_config,
	sku_simple,
	sku_name,
	price_w_o_vat,
	cost_w_o_vat,
	supplier_id,
	supplier_name,
	buyer,
	head_buyer,
	package_height,
	package_length,
	package_width,
	package_weight,
	fulfillment_type_bob,
	fulfillment_type_real,
	shipping_carrier,
	date_ordered,
	date_exported,
	date_backorder,
	date_backorder_tratada,
	date_backorder_accepted,
	date_po_created,
	date_po_updated,
	date_po_issued,
	date_po_confirmed,
	#date_sent_tracking_code,
	date_procured,
	date_ready_to_pick,
	date_ready_to_ship,
	date_tracking_code_created,
	date_shipped,
	date_1st_attempt,
	date_delivered,
	date_failed_delivery,
	date_returned,
	date_procured_promised,
	date_ready_to_ship_promised,
	date_delivered_promised,
	date_stock_updated,
	date_declared_stockout,
	date_declared_backorder,
	date_assigned_to_stock,
	date_status_value_chain,
	datetime_ordered,
	datetime_exported,
	datetime_po_created,
	datetime_po_updated,
	datetime_po_updatetimed,
	datetime_po_issued,
	datetime_po_confirmed,
	#datetime_sent_tracking_code,
	datetime_procured,
	datetime_ready_to_pick,
	datetime_ready_to_ship,
	datetime_shipped,
	datetime_assigned_to_stock,
	week_procured_promised,
	week_delivered_promised,
	week_r2s_promised,
	week_shipped,
	month_procured_promised,
	month_delivered_promised,
	month_r2s_promised,
	on_time_to_po,
	on_time_to_confirm,
	on_time_to_send_po,
	on_time_to_procure,
	on_time_to_export,
	on_time_to_ready,
	on_time_to_ship,
	on_time_to_1st_attempt,
	on_time_to_deliver,
	on_time_to_request_recollection,
	on_time_to_stockout_declared,
	on_time_to_create_po,
	on_time_to_create_tracking_code,
	on_time_total_1st_attempt,
	on_time_total_delivery,
	is_presale,
	payment_method,
	wms_tracking_code,
	shipping_carrier_tracking_code,
	shipping_carrier_tracking_code_inverse,
	status_bob,
	status_wms,
	item_counter,
	package_measure_new,
	is_marketplace,
	is_returned,
	cat_1,
	cat_2,
	cat_3,
	category_bp,
  ship_to_met_area,
  ship_to_region,
  ship_to_city
) SELECT
	item_id,
	order_number,
	sku_config,
	sku_simple,
	sku_name,
	price_w_o_vat,
	cost_w_o_vat,
	supplier_id,
	supplier_name,
	buyer,
	head_buyer,
	package_height,
	package_length,
	package_width,
	package_weight,
	fulfillment_type_bob,
	fulfillment_type_real,
	shipping_carrier,
	date_ordered,
	date_exported,
	date_backorder,
	date_backorder_tratada,
	date_backorder_accepted,
	date_po_created,
	date_po_updated,
	date_po_issued,
	date_po_confirmed,
	#date_sent_tracking_code,
	date_procured,
	date_ready_to_pick,
	date_ready_to_ship,
	date_tracking_code_created,
	date_shipped,
	date_1st_attempt,
	date_delivered,
	date_failed_delivery,
	date_returned,
	date_procured_promised,
	date_ready_to_ship_promised,
	date_delivered_promised,
	date_stock_updated,
	date_declared_stockout,
	date_declared_backorder,
	date_assigned_to_stock,
	date_status_value_chain,
	datetime_ordered,
	datetime_exported,
	datetime_po_created,
	datetime_po_updated,
	datetime_po_updatetimed,
	datetime_po_issued,
	datetime_po_confirmed,
	#datetime_sent_tracking_code,
	datetime_procured,
	datetime_ready_to_pick,
	datetime_ready_to_ship,
	datetime_shipped,
	datetime_assigned_to_stock,
	week_procured_promised,
	week_delivered_promised,
	week_r2s_promised,
	week_shipped,
	month_procured_promised,
	month_delivered_promised,
	month_r2s_promised,
	on_time_to_po,
	on_time_to_confirm,
	on_time_to_send_po,
	on_time_to_procure,
	on_time_to_export,
	on_time_to_ready,
	on_time_to_ship,
	on_time_to_1st_attempt,
	on_time_to_deliver,
	on_time_to_request_recollection,
	on_time_to_stockout_declared,
	on_time_to_create_po,
	on_time_to_create_tracking_code,
	on_time_total_1st_attempt,
	on_time_total_delivery,
	is_presale,
	payment_method,
	wms_tracking_code,
	shipping_carrier_tracking_code,
	shipping_carrier_tracking_code_inverse,
	status_bob,
	status_wms,
	item_counter,
	package_measure_new,
	is_marketplace,
	is_returned,
	cat_1,
	cat_2,
	cat_3,
	category_bp,
  ship_to_met_area,
  ship_to_region,
  ship_to_city
FROM
	operations_ve.out_order_tracking;

SELECT  'Update out_inverse_logistics_tracking',now();
Update operations_ve.out_inverse_logistics_tracking as a 
INNER JOIN operations_ve.pro_inverse_logistics_docs as b
ON a.item_id = b.item_id
SET
a.date_inbound_il = b.date_entrance_ilwh,
a.shipping_carrier_tracking_code_inverse = b.dhl_tracking_code,
a.il_type = b.status,
a.reason_for_entrance_il = b.reason,
a.action_il = b.ilwh_1st_step,
a.damaged_item = b.damaged_item,
a.damaged_package = b.damaged_package,
a.comments_il = b.comments,
a.return_accepted = b.return_accepted,
a.position_il = b.ilwh_position,
a.date_exit_il = b.date_exit_ilwh,
a.date_canceled_il = b.date_cancelled,
a.analyst_il  = b.entered_to_ilwh_by,
a.comments = b.comments_main;

SELECT  'is_returned',now();
UPDATE operations_ve.out_inverse_logistics_tracking as a 
INNER JOIN wmsprod_ve.inverselogistics_devolucion as b
ON a.item_id = b.item_id 
SET a.is_returned = 1;

SELECT  'Update date_customer_request_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking AS a
INNER JOIN bob_live_ve.sales_order_item_status_history AS b
ON a.item_id=b.fk_sales_order_item
SET a.date_customer_request_il=b.created_at
WHERE b.fk_sales_order_item_status=111;

SELECT  'Update date_customer_request_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking AS a
INNER JOIN bob_live_ve.sales_order_item_status_history AS b
ON a.item_id=b.fk_sales_order_item
SET a.date_customer_request_il=b.created_at
WHERE b.fk_sales_order_item_status=112;

SELECT  'Drop operations_ve.TMP',now();
#se toma el segundo estado de aguardando retorno ya que el primero es el que hace el sistema automaticamente
#y el segundo es luego de aguardando agendamiento
DROP TEMPORARY TABLE IF EXISTS operations_ve.TMP ; 

SELECT  'Create operations_ve.TMP',now();
CREATE TEMPORARY TABLE operations_ve.TMP 
SELECT
		b.return_id,
		b. STATUS,
		min(b.changed_at) as cambiado ,
		c.item_id
	FROM
		wmsprod_ve.inverselogistics_status_history b
	JOIN wmsprod_ve.inverselogistics_devolucion c ON b.return_id = c.id
WHERE b.STATUS = 'aguardando_retorno'
Group by item_id;

SELECT  'create index operations_ve.TMP',now();
create index item_id on operations_ve.TMP(item_id);

SELECT  'date_first_track_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking a
INNER JOIN operations_ve.TMP
ON a.item_id = TMP.item_id
SET a.date_first_track_il = TMP.cambiado
WHERE
	TMP. STATUS = 'aguardando_retorno';

SELECT  'Drop operations_ve.TMP2',now();
DROP TEMPORARY TABLE IF EXISTS operations_ve.TMP2 ; 

SELECT  'Create operations_ve.TMP2',now();
CREATE TEMPORARY TABLE operations_ve.TMP2 
SELECT
		b.return_id,
		b. STATUS,
		max(b.changed_at) as cambiado ,
		c.item_id
	FROM
		wmsprod_ve.inverselogistics_status_history b
	JOIN wmsprod_ve.inverselogistics_devolucion c ON b.return_id = c.id
WHERE b.STATUS = 'aguardando_retorno'
Group by item_id;

SELECT  'create index operations_ve.TMP2',now();
create index item_id on operations_ve.TMP2(item_id);

SELECT  'TMP2. STATUS',now();
UPDATE operations_ve.out_inverse_logistics_tracking a
INNER JOIN operations_ve.TMP2
ON a.item_id = TMP2.item_id
SET a.date_last_track_il = TMP2.cambiado
WHERE
	TMP2. STATUS = 'aguardando_retorno';

SELECT  'calcworkdays',now();
CALL operations_ve.calcworkdays;

SELECT  'Update date_quality_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking AS a
INNER JOIN bob_live_ve.sales_order_item_status_history AS b
ON a.item_id=b.fk_sales_order_item
SET a.date_quality_il=b.created_at
WHERE b.fk_sales_order_item_status=104;

SELECT  'Update date_cash_refunded_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking AS a
INNER JOIN bob_live_ve.sales_order_item_status_history AS b
ON a.item_id=b.fk_sales_order_item
SET a.date_cash_refunded_il=b.created_at
WHERE b.fk_sales_order_item_status=113;

SELECT  'Update date_voucher_refunded_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking AS a
INNER JOIN bob_live_ve.sales_order_item_status_history AS b
ON a.item_id=b.fk_sales_order_item
SET a.date_voucher_refunded_il=b.created_at
WHERE b.fk_sales_order_item_status=102;

SELECT  'Update date_closed BOB',now();
UPDATE operations_ve.out_inverse_logistics_tracking AS a
INNER JOIN bob_live_ve.sales_order_item_status_history AS b
ON a.item_id=b.fk_sales_order_item
SET a.date_closed_il=b.created_at
WHERE b.fk_sales_order_item_status=6;

SELECT  'date_refunded_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking AS a
SET a.date_refunded_il= IF(a.date_cash_refunded_il IS NULL,a.date_voucher_refunded_il,a.date_cash_refunded_il);

SELECT  'Drop TMP3analyst',now();
DROP TEMPORARY TABLE IF EXISTS operations_ve.TMP3analyst;

SELECT  'Create TMP3analyst',now();
CREATE TEMPORARY TABLE operations_ve.TMP3analyst
SELECT d.*, e.nome
FROM
(SELECT
		b.user_id,
		b.return_id,
		b. STATUS,
		b.changed_at,
		c.item_id
	FROM
		wmsprod_ve.inverselogistics_status_history b
	JOIN wmsprod_ve.inverselogistics_devolucion c ON b.return_id = c.id) d 
INNER JOIN wmsprod_ve.usuarios as e ON d.user_id = e.usuarios_id
WHERE
	d. STATUS = 'retornado';

SELECT  'create index TMP3analyst',now();
create index item on operations_ve.TMP3analyst(item_id);

SELECT  'date_inbound_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking a
INNER JOIN operations_ve.TMP3analyst as d ON a.item_id = d.item_id
SET a.analyst_il = d.nome,
a.date_inbound_il  = d.changed_at
WHERE
	d. STATUS = 'retornado';


############## CALCULO DE DIAS  ##############
SELECT  'days_to_inbound_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_last_track_il) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking.date_inbound_il) = calcworkdays.date_last
                          AND calcworkdays.isweekday = 1 
	            AND calcworkdays.isholiday = 0
SET 
days_to_inbound_il = 
	IF(date_last_track_il IS NULL,
		NULL,
		DATEDIFF(date(date_inbound_il),date(date_last_track_il))),
workdays_to_inbound_il = 
	IF(date_last_track_il IS NULL,
		NULL,calcworkdays.workdays
		)
WHERE out_inverse_logistics_tracking.is_returned=1
AND out_inverse_logistics_tracking.date_inbound_il IS NOT NULL;

SELECT  'days_to_quality_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_inbound_il) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking.date_quality_il)= calcworkdays.date_last
                        AND calcworkdays.isweekday = 1 
	            AND calcworkdays.isholiday = 0
SET 
days_to_quality_il = 
	IF(date_inbound_il IS NULL,
		NULL,
		DATEDIFF(date(date_quality_il),date(date_inbound_il))),
workdays_to_quality_il = 
	IF((date_inbound_il IS NULL),
		NULL,calcworkdays.workdays)
WHERE operations_ve.out_inverse_logistics_tracking.is_returned =1
AND date_quality_il is not null;

SELECT  'days_to_customer_request_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking AS a
INNER JOIN operations_ve.calcworkdays
	ON date(date_delivered) = calcworkdays.date_first
		AND date(date_customer_request_il) = calcworkdays.date_last
                        AND calcworkdays.isweekday = 1 
	            AND calcworkdays.isholiday = 0
SET 
days_to_customer_request_il = 
IF(date_delivered IS NULL,
		NULL,
	DATEDIFF(date(date_customer_request_il),date(date_delivered))),
workdays_to_customer_request_il  = 
IF(date_customer_request_il IS NULL,
		NULL,calcworkdays.workdays)
WHERE is_returned = 1
AND date_customer_request_il IS NOT NULL;

SELECT  'days_to_track_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_customer_request_il) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking.date_first_track_il) = calcworkdays.date_last
    AND calcworkdays.isweekday = 1 
	  AND calcworkdays.isholiday = 0
SET 
days_to_track_il = 
	IF(date_customer_request_il IS NULL,
		NULL,
		DATEDIFF(date(date_first_track_il),date(date_customer_request_il))),
workdays_to_track_il = 
	IF(date_customer_request_il IS NULL,
		NULL,calcworkdays.workdays
		)
WHERE is_returned =1
AND date_first_track_il IS NOT NULL;

SELECT  'days_to_refunded_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_customer_request_il) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking.date_refunded_il)= calcworkdays.date_last
    AND calcworkdays.isweekday = 1 
	  AND calcworkdays.isholiday = 0
SET 
days_to_refunded_il = 
	IF((date_customer_request_il IS NULL),
		NULL,
		DATEDIFF(date(date_refunded_il),date(date_customer_request_il))),
workdays_to_refunded_il = 
	IF((date_customer_request_il IS NULL),
		NULL,calcworkdays.workdays)
WHERE is_returned =1
AND date_refunded_il IS NOT NULL;

SELECT  'days_to_closed_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking 
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_customer_request_il)= calcworkdays.date_first
		AND date(out_inverse_logistics_tracking.date_closed_il)= calcworkdays.date_last
  AND calcworkdays.isweekday = 1
AND calcworkdays.isholiday = 0
SET 
days_to_closed_il = 
	IF(date_customer_request_il IS NULL,
		NULL,
		DATEDIFF(date(date_closed_il),date(date_customer_request_il))),
workdays_to_closed_il = 
	IF(date_customer_request_il IS NULL,
		NULL,calcworkdays.workdays)
WHERE is_returned =1
AND date_closed_il IS NOT NULL;

SELECT  'Drop operations_ve.TMP2',now();
DROP TEMPORARY TABLE IF EXISTS operations_ve.TMP2 ; 

SELECT  'Create operations_ve.TMP2',now();
CREATE TEMPORARY TABLE operations_ve.TMP2 
SELECT c.*, d.description 
FROM wmsprod_ve.inverselogistics_devolucion as c INNER JOIN wmsprod_ve.inverselogistics_devolucion_razon as d on c.reason_id = d.reason_id;

SELECT  'create index operations_ve.TMP2',now();
create index item_id on operations_ve.TMP2(item_id);

SELECT  'id_ilwh',now();
UPDATE operations_ve.out_inverse_logistics_tracking as a
INNER JOIN operations_ve.TMP2 b ON a.item_id = b.item_id
SET a.status_wms_il = b.status,
a.id_ilwh = b.id,
a.reason_for_entrance_il = b.description,
date_canceled_il = b.created_at;

SELECT  'is_fail_delivery',now();
UPDATE operations_ve.out_inverse_logistics_tracking as a
INNER JOIN wmsprod_ve.tms_status_delivery b ON a.wms_tracking_code = b.cod_rastreamento
SET a.il_type = 'Fail delivery',
a.is_fail_delivery = 1, a.date_inbound_il = b.date WHERE b.id_status = 10;

SELECT  'is_fail_delivery',now();
UPDATE operations_ve.out_inverse_logistics_tracking a
INNER JOIN wmsprod_ve.ws_log_lista_embarque b ON b.cod_rastreamento = a.wms_tracking_code
SET a.reason_for_entrance_il = b.observaciones
WHERE
 a.is_fail_delivery = 1;

SELECT  'is_returned',now();
UPDATE operations_ve.out_inverse_logistics_tracking as a
SET a.il_type = 'Return'
WHERE a.is_returned = 1;

SELECT  'Drop operations_ve.TMP3 ',now();
DROP TEMPORARY TABLE IF EXISTS operations_ve.TMP3 ; 

SELECT  'Create operations_ve.TMP3 ',now();
CREATE TEMPORARY TABLE operations_ve.TMP3 
SELECT c.*, d.description 
FROM wmsprod_ve.inverselogistics_devolucion as c INNER JOIN wmsprod_ve.inverselogistics_devolucion_accion as d on c.action_id = d.action_id;

SELECT  'Create index operations_ve.TMP3 ',now();
create index item_id on operations_ve.TMP3(item_id);

SELECT  'Create index operations_ve.TMP3 ',now();
UPDATE operations_ve.out_inverse_logistics_tracking as a
INNER JOIN operations_ve.TMP3 b ON a.item_id = b.item_id
SET a.action_il = b.description;

SELECT  'date_exit_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking  as  a
INNER JOIN wmsprod_ve.inverselogistics_devolucion as b ON a.item_id = b.item_id
SET a.date_exit_il = CASE
WHEN a.status_wms_il = 'retornar_stock'
OR a.status_wms_il= 'enviar_cuarentena' 
OR a.status_wms_il= 'reenvio_cliente' THEN
	modified_at
ELSE
	NULL
END;

SELECT  'status_wms_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking a
SET a.return_accepted = CASE
WHEN a.status_wms_il = 'retornar_stock'
OR a.status_wms_il = 'enviar_cuarentena' THEN
	1
ELSE
	NULL
END;

SELECT  'is_exits_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking as a 
SET a.is_exits_il = 1 
WHERE date_exit_il is not null;

SELECT  'is_pending_return_wh_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking as a 
SET a.is_pending_return_wh_il = 1 
WHERE status_wms_il in ('aguardando_retorno','aguardando_agendamiento');

SELECT  'Drop TMP2analyst',now();
DROP TEMPORARY TABLE IF EXISTS operations_ve.TMP2analyst ; 

SELECT  'Create TMP2analyst',now();
CREATE TEMPORARY TABLE operations_ve.TMP2analyst
SELECT
		b.cod_rastreamento,
		b.id_status,
		b.id_user,
		c.nome
	FROM
		wmsprod_ve.tms_status_delivery b
	JOIN wmsprod_ve.usuarios c ON b.id_user = c.usuarios_id 
WHERE
	b.id_status = 10 ;

SELECT  'create index TMP2analyst',now();
create index cod on operations_ve.TMP2analyst(cod_rastreamento);

SELECT  'analyst_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking a
INNER JOIN operations_ve.TMP2analyst as  b ON b.cod_rastreamento = a.wms_tracking_code
SET a.analyst_il = b.nome
WHERE
	a.il_type = 'Fail delivery';

SELECT  'delivered_last_30_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking
SET out_inverse_logistics_tracking.delivered_last_30_il = CASE
WHEN datediff(curdate(), date_delivered) <= 45
AND datediff(curdate(), date_delivered) >= 15 THEN
	1
ELSE
	0
END;

SELECT  'date_first_track_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_customer_request_il)= calcworkdays.date_first
		AND date(curdate())= calcworkdays.date_last
SET 
backlog_track_il = 
	IF(calcworkdays.workdays >2,1,0)
WHERE date_first_track_il is null
AND date_inbound_il IS null
AND date_quality_il IS NULL
AND date_closed_il is null;

SELECT  'backlog_quality_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_inbound_il)= calcworkdays.date_first
		AND date(curdate())= calcworkdays.date_last
SET 
backlog_quality_il = 
	IF(calcworkdays.workdays >3,1,0)
WHERE date_quality_il is null
AND date_closed_il is null
AND is_returned=1;

/*
UPDATE operations_ve.out_inverse_logistics_tracking AS t1
INNER JOIN
(SELECT numero_guia
FROM outbound_co.tbl_outbound_daily_servientrega
GROUP BY numero_guia) as t2
ON t1.shipping_carrier_tracking_code_inverse = t2.numero_guia
SET t1.return_receivedserv = 1;


UPDATE operations_ve.out_inverse_logistics_tracking
SET 
backlog_currier_il = 
	IF(((return_receivedserv = 1) AND (date_inbound_il IS NULL)),1,0)
WHERE is_returned=1
AND date_quality_il IS NULL
AND date_closed_il is null;



UPDATE operations_ve.out_inverse_logistics_tracking
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_delivered)= calcworkdays.date_first
		AND date(curdate())= calcworkdays.date_last
SET 
backlog_refund_consignment_il = 
	IF(calcworkdays.workdays>15,1,0)
AND date_cash_refunded_il is null
AND date_voucher_refunded_il IS NULL
AND date_closed_il is null
AND date_quality_il IS NOT NULL
AND is_returned =1
AND action_il='Reembolso tarjeta o cuenta (10-20 días hábiles)';




UPDATE operations_ve.out_inverse_logistics_tracking
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_quality_il)= calcworkdays.date_first
		AND date(curdate())= calcworkdays.date_last
SET 
backlog_refund_effecty_il = 
	IF(calcworkdays.workdays>4,1,0)
AND date_cash_refunded_il is null
AND date_voucher_refunded_il IS NULL
AND date_closed_il is null
AND date_quality_il IS NOT NULL
AND is_returned =1
AND action_il='Reembolso en crédito Linio (2 días hábiles)';

*/

SELECT  'backlog_refund_reversion_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_quality_il)= calcworkdays.date_first
		AND date(curdate())= calcworkdays.date_last
SET 
backlog_refund_reversion_il = 
	IF(calcworkdays.workdays>20,1,0)
AND date_cash_refunded_il is null
AND date_voucher_refunded_il IS NULL
AND date_closed_il is null
AND date_quality_il IS NOT NULL
AND is_returned =1
AND action_il='Reembolso tarjeta o cuenta (10-20 días hábiles)';

SELECT  'backlog_refund_voucher_il',now();
UPDATE operations_ve.out_inverse_logistics_tracking
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_quality_il)= calcworkdays.date_first
		AND date(curdate())= calcworkdays.date_last
SET 
backlog_refund_voucher_il = 
	IF(calcworkdays.workdays>2,1,0)
AND date_cash_refunded_il is null
AND date_voucher_refunded_il IS NULL
AND date_closed_il is null
AND date_quality_il IS NOT NULL
AND is_returned =1
AND action_il='Reembolso en crédito Linio (2 días hábiles)';

SELECT  'production_ve',now();
DROP TABLE IF EXISTS production_ve.out_inverse_logistics_tracking;

CREATE TABLE production_ve.out_inverse_logistics_tracking LIKE operations_ve.out_inverse_logistics_tracking;

INSERT INTO production_ve.out_inverse_logistics_tracking SELECT * FROM operations_ve.out_inverse_logistics_tracking;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Venezuela_Project', 
  'out_inverse_logistics_tracking',
  'finish',
  NOW(),
  max(date_ordered),
  count(*),
  count(item_counter)
FROM
  production_ve.out_inverse_logistics_tracking;

SELECT  'End operations_ve.out_inverse_logistics_tracking',now();

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 PROCEDURE `out_inverse_logistics_tracking_backup`()
BEGIN
	
INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Venezuela', 
  'out_inverse_logistics_tracking',
  'start',
  NOW(),
  max(date_ordered),
  count(*),
  count(item_counter)
FROM
  production_ve.out_inverse_logistics_tracking;


DELETE operations_ve.pro_inverse_logistics_WMS.*
FROM
	operations_ve.pro_inverse_logistics_WMS;


INSERT INTO operations_ve.pro_inverse_logistics_WMS (
	id_inverse_logistics_docs,
	order_nr,
	item_id,
	sku_simple,
	shipping_carrier_return,
	dhl_tracking_code,
	reason,
	ilwh_1st_step,
	comments,
	ilwh_position,
	date_cancelled,
	comments_main
) SELECT
	id,
	order_number,
	item_id,
	sku,
	carrier,
	carrier_tracking_code,
	reason_id,
	action_id,
	COMMENT,
	STATUS,
	created_at,
	comments_il
FROM
	wmsprod_ve.inverselogistics_devolucion;


UPDATE operations_ve.pro_inverse_logistics_WMS a
INNER JOIN (
	SELECT
		b.return_id,
		b. STATUS,
		b.changed_at,
		c.item_id
	FROM
		wmsprod_ve.inverselogistics_status_history b
	JOIN wmsprod_ve.inverselogistics_devolucion c ON b.return_id = c.id
) d ON a.item_id = d.item_id
SET a.date_entrance_ilwh = d.changed_at
WHERE
	d. STATUS = 'retornado';


INSERT INTO operations_ve.pro_inverse_logistics_WMS (
	id_inverse_logistics_docs,
	wms_tracking_code,
	reason,
	date_cancelled
) SELECT
	id,
	cod_rastreamento,
	id_status,
	date
FROM
	wmsprod_ve.tms_status_delivery
WHERE
	tms_status_delivery.id_status = 10;


UPDATE operations_ve.pro_inverse_logistics_WMS a
SET a. STATUS = CASE
WHEN a.reason = 10 THEN
	'Entrega no Exitosa'
ELSE
	'Devolucion'
END;


UPDATE operations_ve.pro_inverse_logistics_WMS a
SET a.damaged_item = CASE
WHEN a.reason = 7 THEN
	1
ELSE
	NULL
END;


UPDATE operations_ve.pro_inverse_logistics_WMS a
SET a.damaged_package = CASE
WHEN a.reason = 1 THEN
	1
ELSE
	NULL
END;


UPDATE operations_ve.pro_inverse_logistics_WMS a
SET a.return_accepted = CASE
WHEN a.ilwh_position = 'retornar_stock'
OR a.ilwh_position = 'enviar_cuarentena' THEN
	1
ELSE
	NULL
END;


UPDATE operations_ve.pro_inverse_logistics_WMS a
SET a.create_new_order = CASE
WHEN a.ilwh_1st_step = 9
OR a.ilwh_1st_step = 10
AND a.return_accepted = 1 THEN
	1
ELSE
	NULL
END;


UPDATE operations_ve.pro_inverse_logistics_WMS a
INNER JOIN wmsprod_ve.inverselogistics_devolucion b ON a.item_id = b.item_id
SET a.date_exit_ilwh = CASE
WHEN b. STATUS = 'retornar_stock'
OR b. STATUS = 'enviar_cuarentena' THEN
	modified_at
ELSE
	NULL
END;


UPDATE operations_ve.pro_inverse_logistics_WMS a
INNER JOIN wmsprod_ve.inverselogistics_devolucion b ON a.item_id = b.item_id
SET a.ilwh_exit_to = CASE
WHEN b. STATUS = 'retornar_stock' THEN
	'A'
WHEN b. STATUS = 'enviar_cuarentena' THEN
	'V'
WHEN b. STATUS = 'reenvio_cliente' THEN
	'C'
ELSE
	NULL
END;


UPDATE operations_ve.pro_inverse_logistics_WMS a
INNER JOIN (
	SELECT
		b.user_id,
		b.return_id,
		b. STATUS,
		b.changed_at,
		c.item_id
	FROM
		wmsprod_ve.inverselogistics_status_history b
	JOIN wmsprod_ve.inverselogistics_devolucion c ON b.return_id = c.id
) d ON a.item_id = d.item_id
SET a.entered_to_ilwh_by = d.user_id
WHERE
	d. STATUS = 'retornado';


UPDATE operations_ve.pro_inverse_logistics_WMS a
INNER JOIN wmsprod_ve.tms_status_delivery b ON b.cod_rastreamento = a.wms_tracking_code
SET a.entered_to_ilwh_by = b.id_user
WHERE
	a.reason = 10;


UPDATE operations_ve.pro_inverse_logistics_WMS a
INNER JOIN wmsprod_ve.ws_log_lista_embarque b ON b.cod_rastreamento = a.wms_tracking_code
SET a.reason = b.observaciones
WHERE
 a.reason = 10;

UPDATE operations_ve.pro_inverse_logistics_WMS a
SET a.reason = CASE
WHEN a.reason = 1 THEN
 "D-Producto Dañado"
WHEN a.reason = 2 THEN
 "D-Producto Incorrecto"
WHEN a.reason = 3 THEN
 "D-Producto duplicado"
WHEN a.reason = 4 THEN
 "D-No se quiere el producto"
WHEN a.reason = 5 THEN
 "D-Piezas faltantes"
WHEN a.reason = 6 THEN
 "D.Retraso en entrega de pedido"
WHEN a.reason = 7 THEN
 "D-Producto usado"
WHEN a.reason = 8 THEN
 "D-Otro"
ELSE
a.reason
END;

UPDATE operations_ve.pro_inverse_logistics_WMS a
SET a.ilwh_2nd_step = a.ilwh_1st_step
WHERE
	a.ilwh_position = 'retornar_stock'
OR a.ilwh_position = 'enviar_cuarentena';


UPDATE operations_ve.pro_inverse_logistics_WMS a
SET a.ilwh_1st_step = CASE
WHEN a.ilwh_1st_step = 9 THEN
	"Otro producto igual"
WHEN a.ilwh_1st_step = 10 THEN
	"Otro producto diferente"
WHEN a.ilwh_1st_step = 11 THEN
	"Reembolso en crédito Linio"
WHEN a.ilwh_1st_step = 12 THEN
	"Rembolso a cuenta (2 -5 días)"
WHEN a.ilwh_1st_step = 13 THEN
	"Reverso a tarjeta (10-15 días)"
ELSE
	NULL
END;


UPDATE operations_ve.pro_inverse_logistics_WMS a
SET a.ilwh_2nd_step = CASE
WHEN a.ilwh_2nd_step = 9 THEN
	"Otro producto igual"
WHEN a.ilwh_2nd_step = 10 THEN
	"Otro producto diferente"
WHEN a.ilwh_2nd_step = 11 THEN
	"Reembolso en crédito Linio"
WHEN a.ilwh_1st_step = 12 THEN
	"Rembolso a cuenta (2 -5 días)"
WHEN a.ilwh_1st_step = 13 THEN
	"Reverso a tarjeta (10-15 días)"
ELSE
	NULL
END;

INSERT INTO operations_ve.pro_inverse_logistics_WMS (
	date_entrance_ilwh,
	order_nr,
	item_id,
	description,
	sku_simple,
	reason,
  ilwh_2nd_step,
  comments,
  STATUS
) SELECT
	date_entrance_ilwh,
	order_nr,
	item_id,
	description,
	sku_simple,
	reason,
  ilwh_2nd_step,
  comments,
  STATUS 
FROM
	(
		SELECT
			A.*
		FROM
			operations_ve.pro_inverse_logistics_docs A
		LEFT JOIN operations_ve.pro_inverse_logistics_WMS B ON A.item_id = B.item_id
		WHERE
			B.item_id IS NULL
	) C;





DELETE operations_ve.out_inverse_logistics_tracking.*
FROM
	operations_ve.out_inverse_logistics_tracking;


INSERT INTO operations_ve.out_inverse_logistics_tracking (
	order_item_id,
	order_number,
	sku_config,
	sku_simple,
	sku_name,
	supplier_id,
	supplier_name,
	buyer,
	head_buyer,
	package_height,
	package_length,
	package_width,
	package_weight,
	date_ordered,
	fullfilment_type_bob,
	fullfilment_type_real,
	shipping_carrier,
	date_shipped,
	date_delivered,
	date_delivered_promised,
	week_delivered_promised,
	month_num_delivered_promised,
	on_time_to_procure,
	presale,
	payment_method,
	ship_to_state,
	ship_to_zip,
	ship_to_zip2,
	ship_to_zip3,
	wms_tracking_code,
	shipping_carrier_tracking_code,
	status_bob,
	status_wms,
	item_counter,
	package_measure,
	shipped_last_30,
	delivered_promised_last_30,
	ship_to_met_area,
	category_com_main,
	category_com_sub,
	category_com_sub_sub,
	category_bp,
	fullfilment_type_bp
) SELECT
	order_item_id,
	order_number,
	sku_config,
	sku_simple,
	sku_name,
	supplier_id,
	supplier_name,
	buyer,
	head_buyer,
	package_height,
	package_length,
	package_width,
	package_weight,
	date_ordered,
	fullfilment_type_bob,
	fullfilment_type_real,
	shipping_carrier,
	date_shipped,
	date_delivered,
	date_delivered_promised,
	week_delivered_promised,
	month_num_delivered_promised,
	on_time_to_procure,
	presale,
	payment_method,
	ship_to_state,
	ship_to_zip,
	ship_to_zip2,
	ship_to_zip3,
	wms_tracking_code,
	shipping_carrier_tracking_code,
	status_bob,
	status_wms,
	item_counter,
	package_measure,
	shipped_last_30,
	delivered_promised_last_30,
	ship_to_met_area,
	category_com_main,
	category_com_sub,
	category_com_sub_sub,
	category_bp,
	fullfilment_type_bp
  
FROM
	operations_ve.out_order_tracking;


UPDATE operations_ve.out_inverse_logistics_tracking a
INNER JOIN operations_ve.pro_inverse_logistics_WMS b ON a.order_item_id = b.item_id
SET a.date_entrance_ilwh = date(b.date_entrance_ilwh),
 a.id_ilwh = b.id_inverse_logistics_docs,
 a.previous_ilwh_entrance = b.entrance_again,
 a.tracking_number_returns = b.dhl_tracking_code,
 a.reason_delivery_fail_return = b.reason,
 a.damaged_item = b.damaged_item,
 a.damaged_package = b.damaged_package,
 a.comments = b.comments_main,
 a.return_accepted = b.return_accepted,
 a.ilwh_position = b.ilwh_position,
 a.ilwh_cancellation_date = date(b.date_entrance_ilwh),
 a.ilwh_exit_date = date(b.date_exit_ilwh),
 a.entered_to_ilwh = b.entered_to_ilwh_by,
 a.payment_method_type = b.payment_method;


UPDATE operations_ve.out_inverse_logistics_tracking a
JOIN operations_ve.pro_inverse_logistics_WMS b ON a.wms_tracking_code = b.wms_tracking_code
SET a.ilwh_status = 'Delivery Fail',
 a.reason_delivery_fail_return = b.reason,
 a.id_ilwh = b.id_inverse_logistics_docs,
 a.ilwh_cancellation_date = b.date_cancelled,
 a.date_entrance_ilwh = b.date_cancelled
WHERE
 b. STATUS = 'Entrega no Exitosa';

UPDATE operations_ve.out_inverse_logistics_tracking a
INNER JOIN operations_ve.pro_inverse_logistics_WMS b ON a.order_item_id = b.item_id
SET a.ilwh_status = CASE
WHEN b. STATUS = 'Bloqueo Interno' THEN
	'Internal block'
WHEN b. STATUS = 'Entrega no Exitosa' THEN
	'Delivery Fail'
WHEN b. STATUS LIKE 'Devol%' THEN
	'Return'
ELSE
	NULL
END;


UPDATE operations_ve.out_inverse_logistics_tracking a
INNER JOIN operations_ve.pro_inverse_logistics_WMS b ON a.order_item_id = b.item_id
SET a.ilwh_1st_step = CASE
WHEN b.ilwh_1st_step = 'Cancelado' THEN
	'Cancelled'
WHEN b.ilwh_1st_step LIKE 'Re-Env%' THEN
	'Re-Delivery'
WHEN b.ilwh_1st_step LIKE 'Recolecci%' THEN
	'Split recolection'
ELSE
	b.ilwh_1st_step
END;


UPDATE operations_ve.out_inverse_logistics_tracking a
INNER JOIN operations_ve.pro_inverse_logistics_WMS b ON a.order_item_id = b.item_id
SET a.ilwh_2nd_step = CASE
WHEN b.ilwh_2nd_step LIKE 'Cancelado por L%' THEN
	'Cancelled by Logistics'
WHEN b.ilwh_2nd_step = 'Cancelado por el cliente' THEN
	'Cancelled by Client'
WHEN b.ilwh_2nd_step = 'Cancelado' THEN
	'Cancelled by Logistics'
WHEN b.ilwh_2nd_step LIKE 'Re-Env%' THEN
	'Re-Delivery'
WHEN b.ilwh_2nd_step LIKE 'cliente soli%' THEN
	'Re-Delivery'
ELSE
	b.ilwh_2nd_step
END;


UPDATE operations_ve.out_inverse_logistics_tracking a
INNER JOIN operations_ve.pro_inverse_logistics_WMS b ON a.order_item_id = b.item_id
SET a.ilwh_exit_to = CASE
WHEN b.ilwh_exit_to LIKE '%V%' THEN
	'Sale'
WHEN b.ilwh_exit_to LIKE 'A%' THEN
	'Stock'
WHEN b.ilwh_exit_to LIKE 'P%' THEN
	'Supplier'
WHEN b.ilwh_exit_to LIKE 'C%' THEN
	'Client'
ELSE
	NULL
END;


UPDATE operations_ve.out_inverse_logistics_tracking a
SET a.`RETURN` =
IF (a.ilwh_status = 'Return', 1, 0);


UPDATE operations_ve.out_inverse_logistics_tracking a
SET a.failed_delivery =
IF (
	a.ilwh_status = 'Delivery Fail',
	1,
	0
);


UPDATE operations_ve.out_inverse_logistics_tracking a
SET a.internal_block = 1
WHERE
	a.ilwh_status = 'Internal Block';


UPDATE operations_ve.out_inverse_logistics_tracking a
SET a.entered_to_ilwh = 1
WHERE
	a.id_ilwh IS NOT NULL;


UPDATE operations_ve.out_inverse_logistics_tracking a
SET a.1st_cancelled =
IF (
	ilwh_1st_step = 'Cancelled',
	1,
	0
),
 a.1st_partial_retrieval =
IF (
	ilwh_1st_step = 'Split Recolection',
	1,
	0
),
 a.1st_re_delivery =
IF (
	ilwh_1st_step = 'Re-Delivery',
	1,
	0
),
 a.1st_non_applicable =
IF (ilwh_1st_step IS NULL, 1, 0);


UPDATE operations_ve.out_inverse_logistics_tracking a
SET a.2nd_cancelled =
IF (
	ilwh_2nd_step = 'Cancelled',
	1,
	0
),
 a.2nd_cancelled_by_client =
IF (
	ilwh_2nd_step = 'Cancelled by Client',
	1,
	0
),
 a.2nd_cancelled_by_logistics =
IF (
	ilwh_2nd_step = 'Cancellled by Logistics',
	1,
	0
),
 a.2nd_re_delivery =
IF (
	ilwh_2nd_step = 'Re-Delivery',
	1,
	0
);


UPDATE operations_ve.out_inverse_logistics_tracking a
SET a.exit_to_client_failed_delivery =
IF (ilwh_exit_to = 'Client', 1, 0),
 a.exit_to_stock_failed_delivery =
IF (ilwh_exit_to = 'Stock', 1, 0),
 a.exit_to_supplier_failed_delivery =
IF (
	ilwh_exit_to = 'Supplier',
	1,
	0
),
 a.exit_to_resale_failed_delivery =
IF (ilwh_exit_to = 'Sale', 1, 0)
WHERE
	a.failed_delivery = 1;


UPDATE operations_ve.out_inverse_logistics_tracking a
SET a.exit_to_client_returns =
IF (ilwh_exit_to = 'Client', 1, 0),
 a.exit_to_stock_returns =
IF (ilwh_exit_to = 'Stock', 1, 0),
 a.exit_to_supplier_returns =
IF (
	ilwh_exit_to = 'Supplier',
	1,
	0
),
 a.exit_to_resale_returns =
IF (ilwh_exit_to = 'Sale', 1, 0)
WHERE
	a. `RETURN` = 1;


UPDATE operations_ve.out_inverse_logistics_tracking a
SET a.exit_to_client_internal_block =
IF (ilwh_exit_to = 'Client', 1, 0),
 a.exit_to_stock_internal_block =
IF (ilwh_exit_to = 'Stock', 1, 0),
 a.exit_to_supplier_internal_block =
IF (
	ilwh_exit_to = 'Supplier',
	1,
	0
),
 a.exit_to_resale_internal_block =
IF (ilwh_exit_to = 'Sale', 1, 0)
WHERE
	a.internal_block = 1;


UPDATE operations_ve.out_inverse_logistics_tracking a
SET a.reintegrate_to_stock =
IF (ilwh_position = 'IN' OR ilwh_position = 'retornar_stock', 1, 0);


UPDATE operations_ve.out_inverse_logistics_tracking a
SET a.succesfull_redelivery =
IF (ilwh_position = 'Client', 1, 0);


UPDATE operations_ve.out_inverse_logistics_tracking a
SET a.pending_exits =
IF (a.ilwh_exit_date IS NULL, 1, 0);


UPDATE operations_ve.out_inverse_logistics_tracking
SET out_inverse_logistics_tracking.last_30 = CASE
WHEN datediff(curdate(), date_entrance_ilwh) <= 45
AND datediff(curdate(), date_entrance_ilwh) >= 15 THEN
	1
ELSE
	0
END;

UPDATE operations_ve.out_inverse_logistics_tracking a
INNER JOIN bob_live_ve.sales_order_item_status_history as b
ON a.order_item_id=b.fk_sales_order_item
SET date_delivered_bob=b.created_at
WHERE fk_sales_order_item_status=52;



UPDATE operations_ve.out_inverse_logistics_tracking a
INNER JOIN bob_live_ve.sales_order_item_status_history as b
ON a.order_item_id=b.fk_sales_order_item
SET date_automatic_return_bob=b.created_at
WHERE fk_sales_order_item_status=111;

UPDATE operations_ve.out_inverse_logistics_tracking a
INNER JOIN bob_live_ve.sales_order_item_status_history as b
ON a.order_item_id=b.fk_sales_order_item
SET date_automatic_return_bob=b.created_at
WHERE fk_sales_order_item_status=112
AND date_automatic_return_bob IS NULL;


UPDATE operations_ve.out_inverse_logistics_tracking a
INNER JOIN bob_live_ve.sales_order_item_status_history as b
ON a.order_item_id=b.fk_sales_order_item
SET date_refund_needed_bob =b.created_at
WHERE fk_sales_order_item_status=104;


UPDATE operations_ve.out_inverse_logistics_tracking a
INNER JOIN bob_live_ve.sales_order_item_status_history as b
ON a.order_item_id=b.fk_sales_order_item
SET date_clarify_refund_needed_bob =b.created_at
WHERE fk_sales_order_item_status=114;



UPDATE operations_ve.out_inverse_logistics_tracking a
INNER JOIN bob_live_ve.sales_order_item_status_history as b
ON a.order_item_id=b.fk_sales_order_item
SET date_cash_refunded_bob =b.created_at
WHERE fk_sales_order_item_status=113;



UPDATE operations_ve.out_inverse_logistics_tracking a
INNER JOIN bob_live_ve.sales_order_item_status_history as b
ON a.order_item_id=b.fk_sales_order_item
SET date_store_credit_issued_bob =b.created_at
WHERE fk_sales_order_item_status=102;


UPDATE operations_ve.out_inverse_logistics_tracking a
INNER JOIN bob_live_ve.sales_order_item_status_history as b
ON a.order_item_id=b.fk_sales_order_item
SET date_closed_bob =b.created_at
WHERE fk_sales_order_item_status=6;


DROP TEMPORARY TABLE IF EXISTS operations_ve.TMP ; 
CREATE TEMPORARY TABLE operations_ve.TMP 
SELECT
		b.return_id,
		b. STATUS,
		min(b.changed_at) as cambiado ,
		c.item_id
	FROM
		wmsprod_ve.inverselogistics_status_history b
	JOIN wmsprod_ve.inverselogistics_devolucion c ON b.return_id = c.id
WHERE b.STATUS = 'aguardando_retorno'
Group by item_id;

create index item_id on operations_ve.TMP(item_id);

UPDATE operations_ve.out_inverse_logistics_tracking a
INNER JOIN operations_ve.TMP
ON a.order_item_id = TMP.item_id
SET a.date_track_return = TMP.cambiado
WHERE
	TMP. STATUS = 'aguardando_retorno';



DROP TEMPORARY TABLE IF EXISTS operations_ve.TMP2 ; 
CREATE TEMPORARY TABLE operations_ve.TMP2 
SELECT
		b.return_id,
		b. STATUS,
		max(b.changed_at) as cambiado ,
		c.item_id
	FROM
		wmsprod_ve.inverselogistics_status_history b
	JOIN wmsprod_ve.inverselogistics_devolucion c ON b.return_id = c.id
WHERE b.STATUS = 'aguardando_retorno'
Group by item_id;

create index item_id on operations_ve.TMP2(item_id);

UPDATE operations_ve.out_inverse_logistics_tracking a
INNER JOIN operations_ve.TMP2
ON a.order_item_id = TMP2.item_id
SET a.date_track_return2 = TMP2.cambiado
WHERE
	TMP2. STATUS = 'aguardando_retorno';

CALL operations_ve.calcworkdays;

UPDATE operations_ve.out_inverse_logistics_tracking
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_delivered_bob) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking.date_automatic_return_bob) = calcworkdays.date_last
SET 
nd_delivered_to_returnaskedautomatic = 
IF(date_delivered_bob IS NULL,
		NULL,
	DATEDIFF(date(date_automatic_return_bob),date(date_delivered_bob))),
wd_delivered_to_returnaskedautomatic  = 
IF(date_automatic_return_bob IS NULL,
		NULL,calcworkdays.workdays)
WHERE operations_ve.out_inverse_logistics_tracking.return=1
AND date_automatic_return_bob IS NOT NULL;



UPDATE operations_ve.out_inverse_logistics_tracking
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_automatic_return_bob) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking.date_track_return) = calcworkdays.date_last
SET 
nd_returnaskedautomatic_to_trackreturn = 
	IF(date_automatic_return_bob IS NULL,
		NULL,
		DATEDIFF(date(date_track_return),date(date_automatic_return_bob))),
wd_returnaskedautomatic_to_trackreturn = 
	IF(date_automatic_return_bob IS NULL,
		NULL,calcworkdays.workdays
		)
WHERE operations_ve.out_inverse_logistics_tracking.return=1
AND date_track_return IS NOT NULL;



UPDATE operations_ve.out_inverse_logistics_tracking
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_track_return2) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking.date_entrance_ilwh) = calcworkdays.date_last
SET 
nd_trackreturn_to_inboundwarehouse = 
	IF(date_track_return2 IS NULL,
		NULL,
		DATEDIFF(date(date_entrance_ilwh),date(date_track_return2))),
wd_trackreturn_to_inboundwarehouse = 
	IF(date_track_return2 IS NULL,
		NULL,calcworkdays.workdays
		)
WHERE operations_ve.out_inverse_logistics_tracking.return=1
AND date_entrance_ilwh IS NOT NULL;



UPDATE operations_ve.out_inverse_logistics_tracking
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_automatic_return_bob) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking.date_entrance_ilwh) = calcworkdays.date_last
SET 
nd_returnaskedautomatic_inboundwarehouse = 
	IF(date_automatic_return_bob IS NULL,
		NULL,
		DATEDIFF(date(date_entrance_ilwh),date(date_automatic_return_bob))),
wd_returnaskedautomatic_inboundwarehouse = 
	IF(date_automatic_return_bob IS NULL,
		NULL,
    calcworkdays.workdays)
WHERE operations_ve.out_inverse_logistics_tracking.return =1
AND date_entrance_ilwh is not null;


UPDATE operations_ve.out_inverse_logistics_tracking 
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_entrance_ilwh) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking.date_refund_needed_bob)= calcworkdays.date_last
SET 
nd_inboundwarehouse_refundneeded = 
	IF(date_entrance_ilwh IS NULL,
		NULL,
		DATEDIFF(date(date_refund_needed_bob),date(date_entrance_ilwh))),
wd_inboundwarehouse_refundneeded = 
	IF((date_entrance_ilwh IS NULL),
		NULL,calcworkdays.workdays)
WHERE operations_ve.out_inverse_logistics_tracking.return=1
AND date_refund_needed_bob is not null;



UPDATE operations_ve.out_inverse_logistics_tracking  
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_automatic_return_bob) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking.date_refund_needed_bob)= calcworkdays.date_last
SET 
nd_returnaskedautomatic_to_refundneeded = 
	IF(date_automatic_return_bob IS NULL,
		NULL,
		DATEDIFF(date(date_refund_needed_bob),date(date_automatic_return_bob))),
wd_returnaskedautomatic_to_refundneeded = 
	IF(date_automatic_return_bob IS NULL,
		NULL,calcworkdays.workdays)
WHERE operations_ve.out_inverse_logistics_tracking.return=1
AND date_refund_needed_bob IS NOT NULL;



UPDATE operations_ve.out_inverse_logistics_tracking
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_automatic_return_bob) = calcworkdays.date_first
		AND date(out_inverse_logistics_tracking.date_cash_refunded_bob)= calcworkdays.date_last
SET 
nd_returnaskedautomatic_cashrefunded = 
	IF((date_automatic_return_bob IS NULL),
		NULL,
		DATEDIFF(date(date_cash_refunded_bob),date(date_automatic_return_bob))),
wd_returnaskedautomatic_cashrefunded = 
	IF((date_automatic_return_bob IS NULL),
		NULL,calcworkdays.workdays)
WHERE operations_ve.out_inverse_logistics_tracking.return=1
AND date_cash_refunded_bob IS NOT NULL;



UPDATE operations_ve.out_inverse_logistics_tracking
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_refund_needed_bob)= calcworkdays.date_first
		AND date(out_inverse_logistics_tracking.date_cash_refunded_bob)= calcworkdays.date_last
SET 
nd_refundneeded_cashrefunded = 
	IF(date_refund_needed_bob IS NULL,
		NULL,
		DATEDIFF(date(date_cash_refunded_bob),date(date_refund_needed_bob))),
wd_refundneeded_cashrefunded = 
	IF(date_refund_needed_bob IS NULL,
		NULL,calcworkdays.workdays)
WHERE operations_ve.out_inverse_logistics_tracking.return=1
AND date_cash_refunded_bob IS NOT NULL;



UPDATE operations_ve.out_inverse_logistics_tracking
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_automatic_return_bob)= calcworkdays.date_first
		AND date(out_inverse_logistics_tracking.date_store_credit_issued_bob)= calcworkdays.date_last
SET 
nd_returnaskedautomatic_storecreditissued = 
	IF((date_automatic_return_bob IS NULL),
		NULL,
		DATEDIFF(date(date_store_credit_issued_bob),date(date_automatic_return_bob))),
wd_returnaskedautomatic_storecreditissued = 
	IF((date_automatic_return_bob IS NULL),
		NULL,calcworkdays.workdays)
WHERE operations_ve.out_inverse_logistics_tracking.return=1
AND date_store_credit_issued_bob IS NOT NULL;




UPDATE operations_ve.out_inverse_logistics_tracking
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_refund_needed_bob)= calcworkdays.date_first
		AND date(out_inverse_logistics_tracking.date_store_credit_issued_bob)= calcworkdays.date_last
SET 
nd_refundneeded_storecreditissued = 
	IF(date_refund_needed_bob IS NULL,
		NULL,
		DATEDIFF(date(date_store_credit_issued_bob),date(date_refund_needed_bob))),
wd_refundneeded_storecreditissued = 
	IF(date_refund_needed_bob IS NULL,
		NULL,calcworkdays.workdays)
WHERE operations_ve.out_inverse_logistics_tracking.return=1
AND date_store_credit_issued_bob IS NOT NULL;


UPDATE operations_ve.out_inverse_logistics_tracking
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_clarify_refund_needed_bob)= calcworkdays.date_first
		AND date(out_inverse_logistics_tracking.date_cash_refunded_bob)= calcworkdays.date_last
SET 
nd_clarifyrefundneeded_cashrefunded = 
	IF(date_clarify_refund_needed_bob IS NULL,
		NULL,
		DATEDIFF(date(date_cash_refunded_bob),date(date_clarify_refund_needed_bob))),
wd_clarifyrefundneeded_cashrefunded = 
	IF(date_clarify_refund_needed_bob IS NULL,
		NULL,calcworkdays.workdays)
WHERE operations_ve.out_inverse_logistics_tracking.return=1
AND date_cash_refunded_bob IS NOT NULL;



UPDATE operations_ve.out_inverse_logistics_tracking
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_clarify_refund_needed_bob)= calcworkdays.date_first
		AND date(out_inverse_logistics_tracking.date_store_credit_issued_bob)= calcworkdays.date_last
SET 
nd_clarifyrefundneeded_storecreditissued = 
	IF(date_clarify_refund_needed_bob IS NULL,
		NULL,
		DATEDIFF(date(date_store_credit_issued_bob),date(date_clarify_refund_needed_bob))),
wd_clarifyrefundneeded_storecreditissued = 
	IF(date_clarify_refund_needed_bob IS NULL,
		NULL,calcworkdays.workdays)
WHERE operations_ve.out_inverse_logistics_tracking.return=1
AND date_store_credit_issued_bob IS NOT NULL;


UPDATE operations_ve.out_inverse_logistics_tracking 
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_automatic_return_bob)= calcworkdays.date_first
		AND date(out_inverse_logistics_tracking.date_closed_bob)= calcworkdays.date_last
SET 
nd_returnaskedautomatic_closed = 
	IF(date_automatic_return_bob IS NULL,
		NULL,
		DATEDIFF(date(date_closed_bob),date(date_automatic_return_bob))),
wd_returnaskedautomatic_closed = 
	IF(date_automatic_return_bob IS NULL,
		NULL,calcworkdays.workdays)
WHERE operations_ve.out_inverse_logistics_tracking.return=1
AND date_closed_bob IS NOT NULL;


UPDATE operations_ve.out_inverse_logistics_tracking 
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_delivered_bob)= calcworkdays.date_first
		AND date(curdate())= calcworkdays.date_last
SET 
backlog_delivered_return = 
	IF(calcworkdays.workdays >5,1,0)
and operations_ve.out_inverse_logistics_tracking.return=1
AND date_closed_bob is null
AND date_automatic_return_bob IS NULL;


UPDATE operations_ve.out_inverse_logistics_tracking 
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_automatic_return_bob)= calcworkdays.date_first
		AND date(curdate())= calcworkdays.date_last
SET 
backlog_returns_track = 
	IF(calcworkdays.workdays >2,1,0)
WHERE date_track_return is null
AND date_entrance_ilwh IS null
AND date_refund_needed_bob IS NULL
AND date_closed_bob is null;


UPDATE operations_ve.out_inverse_logistics_tracking 
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_track_return)= calcworkdays.date_first
		AND date(curdate())= calcworkdays.date_last
SET 
backlog_returns_track_ops = 
	IF(calcworkdays.workdays >3,1,0)
WHERE date_entrance_ilwh is null
AND operations_ve.out_inverse_logistics_tracking.return=1
AND date_closed_bob is null;


UPDATE operations_ve.out_inverse_logistics_tracking 
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_entrance_ilwh)= calcworkdays.date_first
		AND date(curdate())= calcworkdays.date_last
SET 
backlog_returns_ops = 
	IF(calcworkdays.workdays >1,1,0)
WHERE date_refund_needed_bob is null
AND date_closed_bob is null
AND operations_ve.out_inverse_logistics_tracking.return=1;


UPDATE operations_ve.out_inverse_logistics_tracking 
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_refund_needed_bob) = calcworkdays.date_first
		AND date(curdate())= calcworkdays.date_last
SET 
backlog_returns_finance_acount = 
	IF(calcworkdays.workdays >15,1,0)
WHERE date_cash_refunded_bob is null
AND date_store_credit_issued_bob IS NULL
AND date_closed_bob is null
AND date_refund_needed_bob IS NOT NULL
AND operations_ve.out_inverse_logistics_tracking.return=1
AND ilwh_1st_step ='Reembolso tarjeta o cuenta (10-20 días hábiles)';




UPDATE operations_ve.out_inverse_logistics_tracking 
INNER JOIN operations_ve.calcworkdays
	ON date(out_inverse_logistics_tracking.date_refund_needed_bob) = calcworkdays.date_first
		AND date(curdate())= calcworkdays.date_last
SET 
backlog_returns_finance_linio = 
	IF(calcworkdays.workdays >1,1,0)
WHERE date_cash_refunded_bob is null
AND date_store_credit_issued_bob IS NULL
AND date_closed_bob is null
AND date_refund_needed_bob IS NOT NULL
AND operations_ve.out_inverse_logistics_tracking.return=1
AND ilwh_1st_step ='Reembolso en crédito Linio (2 días hábiles)';



UPDATE operations_ve.out_inverse_logistics_tracking
SET item_counter = 1;

DROP TABLE IF EXISTS production_ve.out_inverse_logistics_tracking;

CREATE TABLE production_ve.out_inverse_logistics_tracking LIKE operations_ve.out_inverse_logistics_tracking;

INSERT INTO production_ve.out_inverse_logistics_tracking SELECT * FROM operations_ve.out_inverse_logistics_tracking;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Venezuela', 
  'out_inverse_logistics_tracking',
  'finish',
  NOW(),
  max(date_ordered),
  count(*),
  count(item_counter)
FROM
  production_ve.out_inverse_logistics_tracking;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`natali.serrano`@`%`*/ /*!50003 PROCEDURE `out_order_tracking`()
    MODIFIES SQL DATA
BEGIN
DECLARE rows     INT DEFAULT 0;
DECLARE attempts INT DEFAULT 12;


SELECT  'Monitoring_log',now();

INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Peru_Project', 
  'out_order_tracking',
  'start',
  NOW(),
  max(datetime_ordered),
  count(*),
  count(item_counter)
FROM
  operations_ve.out_order_tracking;

/*
DROP TABLE IF EXISTS operations_ve.tbl_purchase_order;

CREATE TABLE operations_ve.tbl_purchase_order LIKE bazayaco.tbl_purchase_order;

INSERT INTO operations_ve.tbl_purchase_order SELECT * FROM bazayaco.tbl_purchase_order;
*/

SELECT  'Drop out_order_tracking_sample',now();
DROP TABLE IF EXISTS operations_ve.out_order_tracking_sample;

SELECT  'Create out_order_tracking_sample',now();
CREATE TABLE operations_ve.out_order_tracking_sample LIKE operations_ve.out_order_tracking;

SELECT  'Insert Itens_venda',now();
INSERT INTO operations_ve.out_order_tracking_sample (
	item_id,
	order_number,
	sku_simple,
	status_wms,
	order_id,
	min_delivery_time,
	max_delivery_time,
	date_exported,
	datetime_exported,
	#supplier_leadtime,
	is_linio_promise
) SELECT
	a.item_id,
	a.numero_order,
	a.sku,
	a.STATUS,
	a.order_id,
	a.tempo_de_entrega_minimo,
	a.tempo_de_entrega_maximo,
	date(a.data_exportable),
	a.data_exportable,
	#a.tempo_de_entrega_minimo - 2,
	if(a.linio_promise IS NULL, 0, a.linio_promise)
FROM
	wmsprod_ve.itens_venda AS a
WHERE DATE_FORMAT(a.data_criacao,'%Y%m') >= (DATE_FORMAT(curdate()- INTERVAL 3 MONTH,'%Y%m'));

SELECT  'Drop tmp_itens_venda_exported_items',now();
DROP TEMPORARY TABLE IF EXISTS operations_ve.tmp_itens_venda_exported_items;

SELECT  'Create tmp_itens_venda_exported_items',now();
CREATE TEMPORARY TABLE operations_ve.tmp_itens_venda_exported_items (INDEX (item_id))
SELECT
b.item_id,
a.date AS datetime_exported,
date(a.date) as date_exported
FROM
wmsprod_ve.comunication_itens_venda a
INNER JOIN wmsprod_ve.itens_venda b
                ON a.itens_venda_id = b.itens_venda_id
WHERE comunication = 'Comunicacion completa';

SELECT  'Update date_exported',now();
UPDATE operations_ve.out_order_tracking_sample AS a
INNER JOIN operations_ve.tmp_itens_venda_exported_items AS b ON a.item_id = b.item_id
SET a.date_exported = b.date_exported,
a.datetime_exported = b.datetime_exported;

SELECT  'Drop tmp_pro_min_date_exported',now();
DROP TEMPORARY TABLE IF EXISTS operations_ve.tmp_pro_min_date_exported;

SELECT  'Create tmp_pro_min_date_exported',now();
CREATE TEMPORARY TABLE operations_ve.tmp_pro_min_date_exported (INDEX (fk_sales_order_item))
SELECT
	sales_order_item_status_history.fk_sales_order_item,
	sales_order_item_status. NAME,
	min(	sales_order_item_status_history.created_at) AS min_of_created_at
FROM
	bob_live_ve.sales_order_item_status
INNER JOIN bob_live_ve.sales_order_item_status_history 
	ON sales_order_item_status.id_sales_order_item_status = sales_order_item_status_history.fk_sales_order_item_status
GROUP BY
	sales_order_item_status_history.fk_sales_order_item,
	sales_order_item_status. NAME
HAVING sales_order_item_status. NAME IN( 	"exported", 
	"exportable",
	"exported electronically");

SELECT  'Update date_exported',now();
UPDATE operations_ve.out_order_tracking_sample AS a
INNER JOIN operations_ve.tmp_pro_min_date_exported as b ON a.item_id = b.fk_sales_order_item
SET a.date_exported = date(min_of_created_at),
a.datetime_exported = min_of_created_at
WHERE date_exported IS NULL ;

SELECT  'Update transportadoras',now();
UPDATE operations_ve.out_order_tracking_sample AS a
INNER JOIN wmsprod_ve.itens_venda AS b
	ON a.item_id = b.item_id
INNER JOIN wmsprod_ve.romaneio AS c
	ON b.romaneio_id = c.romaneio_id
INNER JOIN wmsprod_ve.transportadoras AS d
	ON c.transportadora_id = d.transportadoras_id
SET 
 a.shipping_carrier = d.nome_transportadora,
 a.id_shipping_carrier = d.transportadoras_id;

#Pendiente reactivar
#CALL production.monitoring( "A_Master_Catalog" , "Peru",  240 );

SELECT  'Update A_Master_Catalog',now();
UPDATE operations_ve.out_order_tracking_sample AS a
INNER JOIN development_ve.A_Master_Catalog AS b 
 ON a.sku_simple = b.sku_simple
SET
	a.cat_1 = b.Cat1,
	a.cat_2 = b.Cat2,
	a.cat_3 = b.Cat3,
	a.category_bp = Cat_BP,
	a.category_kpi = Cat_KPI,
	a.head_buyer = b.Head_Buyer,
	a.buyer = b.Buyer,
	a.sku_config = b.sku_config,
	a.sku_name = b.sku_name,
	a.supplier_id = b.id_supplier,
 	a.supplier_name = b.Supplier,
 	a.package_height = b.package_height,
 	a.package_length = b.package_length,
 	a.package_width = b.package_width,
 	a.package_weight = b.package_weight;

#UPDATE operations_ve.out_order_tracking_sample as a
#INNER JOIN operations_ve.tbl_bi_ops_tracking_suppliers as b
#ON b.id = a.supplier_id
#SET a.procurement_analyst = b.tracker_name;

#vol_weight_carrier aplica solo para CO
SELECT  'Update vol_weight',now();	
UPDATE operations_ve.out_order_tracking_sample AS a
SET 
	a.vol_weight = a.package_height * a.package_length * a.package_width / 5000;
	#a.vol_weight_carrier= IF(shipping_carrier='DESPACHOS SERVIENTREGAS',
	#												(((a.package_width*a.package_length*a.package_height)*222)/1000000),
	#											IF(a.shipping_carrier='DESPACHO DESPRISA',
	#												(((a.package_width*a.package_length*a.package_height)*400)/1000000),
	#												(((a.package_width*a.package_length*a.package_height)*222)/1000000)));

SELECT  'Update max_vol_w_vs_w',now();	
UPDATE operations_ve.out_order_tracking_sample AS a
SET
a.max_vol_w_vs_w = 	CASE
											WHEN a.vol_weight > a.package_weight 
											THEN a.vol_weight
											ELSE a.package_weight
END;

SELECT  'Update package_measure_new',now();	
UPDATE operations_ve.out_order_tracking_sample AS a
SET
a.package_measure_new = CASE
													WHEN a.max_vol_w_vs_w > 35 
													THEN 'oversized'
													WHEN a.max_vol_w_vs_w > 5 
													THEN 'large'
													WHEN a.max_vol_w_vs_w > 2 
													THEN 'medium'
													ELSE 'small'
												END;
########## Fulfillment Type Real

SELECT  'Drop fulfillment_type_own_warehouse',now();
DROP TABLE IF EXISTS operations_ve.fulfillment_type_own_warehouse;
SELECT  'Create fulfillment_type_own_warehouse',now();
CREATE TABLE operations_ve.fulfillment_type_own_warehouse (INDEX (item_id))
SELECT
 b.item_id,
 "Own Warehouse" as fulfillment_type_real
FROM
wmsprod_ve.itens_venda b
INNER JOIN wmsprod_ve.status_itens_venda c
 ON b.itens_venda_id = c.itens_venda_id
WHERE c.STATUS ='Estoque reservado';

SELECT  'Update fulfillment_type_real',now();
UPDATE
 operations_ve.out_order_tracking_sample a
INNER JOIN operations_ve.fulfillment_type_own_warehouse b
 ON a.item_id = b.item_id
SET a.fulfillment_type_real = b.fulfillment_type_real;

SELECT  'Drop fulfillment_type_consignment',now();
DROP TABLE IF EXISTS operations_ve.fulfillment_type_consignment;

SELECT  'Create fulfillment_type_consignment',now();
CREATE TABLE operations_ve.fulfillment_type_consignment (INDEX (item_id))
SELECT
 b.item_id,
 'Consignment' AS fulfillment_type_real
FROM
wmsprod_ve.itens_venda b
INNER JOIN wmsprod_ve.estoque c
 ON b.estoque_id = c.estoque_id
INNER JOIN wmsprod_ve.itens_recebimento d
 ON c.itens_recebimento_id = d.itens_recebimento_id
INNER JOIN wmsprod_ve.recebimento e
 ON d.recebimento_id = e.recebimento_id
INNER JOIN wmsprod_ve.po_oms f
 ON e.inbound_document_identificator = f.name
INNER JOIN procurement_live_ve.procurement_order g
 ON f.po_oms_id = g.id_procurement_order
WHERE e.inbound_document_type_id = 4
AND g.fk_procurement_order_type = 7;

SELECT  'Update fulfillment_type_real',now();
UPDATE
 operations_ve.out_order_tracking_sample a
INNER JOIN operations_ve.fulfillment_type_consignment b
 ON a.item_id = b.item_id
SET a.fulfillment_type_real = b.fulfillment_type_real;

SELECT  'Drop fulfillment_type_dropshipping',now();
DROP TABLE IF EXISTS operations_ve.fulfillment_type_dropshipping;

SELECT  'Create fulfillment_type_dropshipping',now();
CREATE TABLE operations_ve.fulfillment_type_dropshipping (INDEX (item_id))
SELECT
 b.item_id,
 "Dropshipping" as fulfillment_type_real
FROM
wmsprod_ve.itens_venda b
INNER JOIN wmsprod_ve.status_itens_venda c
 ON b.itens_venda_id = c.itens_venda_id
WHERE c.STATUS IN (	'DS estoque reservado',
			'Waiting dropshipping',
			'Dropshipping notified',
			'awaiting_fulfillment',
			'Electronic good'
		);

SELECT  'Update fulfillment_type_real',now();
UPDATE
 operations_ve.out_order_tracking_sample a
INNER JOIN operations_ve.fulfillment_type_dropshipping b
 ON a.item_id = b.item_id
SET a.fulfillment_type_real = b.fulfillment_type_real;

SELECT  'Drop fulfillment_type_crossdocking',now();
DROP TABLE IF EXISTS operations_ve.fulfillment_type_crossdocking;
SELECT  'Create fulfillment_type_crossdocking',now();
CREATE TABLE operations_ve.fulfillment_type_crossdocking (INDEX (item_id))
SELECT
 b.item_id,
 "Crossdocking" as fulfillment_type_real
FROM
wmsprod_ve.itens_venda b
INNER JOIN wmsprod_ve.status_itens_venda c
 ON b.itens_venda_id = c.itens_venda_id
WHERE c.STATUS IN (	'analisando quebra',
			'aguardando estoque');

SELECT  'Update fulfillment_type_real',now();
UPDATE
 operations_ve.out_order_tracking_sample a
INNER JOIN operations_ve.fulfillment_type_crossdocking b
 ON a.item_id = b.item_id
SET a.fulfillment_type_real = if(a.fulfillment_type_bob = 'Dropshipping_cod', a.fulfillment_type_bob, b.fulfillment_type_real);

SELECT  'Update fulfillment_type_bp',now();
UPDATE operations_ve.out_order_tracking_sample
SET fulfillment_type_bp = CASE
				WHEN fulfillment_type_real IN( 'crossdocking', 'consignment')
				THEN fulfillment_type_real
				WHEN fulfillment_type_real = 'Own Warehouse'
				THEN 'Outright Buying'
				WHEN fulfillment_type_real = 'dropshipping' 
				THEN 'other'
END;

########## Queries Fechas
SELECT  'Call calcworkdays',now();
CALL operations_ve.calcworkdays;

SELECT  'Truncate pro_order_tracking_dates',now();
TRUNCATE operations_ve.pro_order_tracking_dates;

SELECT  'Insert pro_order_tracking_dates',now();
INSERT INTO operations_ve.pro_order_tracking_dates (
	item_id,
	order_number,
	order_id,
  sku_simple,
  status_wms,
  date_exported,
	datetime_exported,
  #supplier_leadtime,
  min_delivery_time,
	max_delivery_time,
  fulfillment_type_real
) SELECT
	a.item_id,
	a.order_number,
	a.order_id,
  a.sku_simple,
  a.status_wms,
  a.date_exported,
	a.datetime_exported,
  #a.supplier_leadtime,
  a.min_delivery_time,
	a.max_delivery_time,
  a.fulfillment_type_real
FROM
	operations_ve.out_order_tracking_sample a;

SELECT  'Update fulfillment_type_bob',now();
UPDATE operations_ve.pro_order_tracking_dates AS a
INNER JOIN bob_live_ve.sales_order_item AS b
	ON a.item_id = b.id_sales_order_item
INNER JOIN bob_live_ve.catalog_shipment_type AS c
	ON b.fk_catalog_shipment_type = c.id_catalog_shipment_type
SET a.fulfillment_type_bob = c.NAME,
a.supplier_leadtime=b.delivery_time_supplier;

UPDATE operations_ve.pro_order_tracking_dates AS a
INNER JOIN development_ve.A_Master_Catalog AS b 
 ON a.sku_simple = b.sku_simple
SET
 a.supplier_id = b.id_supplier;

SELECT  'Update date_ordered',now();
UPDATE 
((bob_live_ve.sales_order_address AS a
RIGHT JOIN (operations_ve.pro_order_tracking_dates AS b
				INNER JOIN bob_live_ve.sales_order AS c
				ON b.order_number = c.order_nr)
ON a.id_sales_order_address = c.fk_sales_order_address_billing)
LEFT JOIN bob_live_ve.customer_address_region AS d
ON a.fk_customer_address_region = d.id_customer_address_region)
INNER JOIN bob_live_ve.sales_order_item AS e
ON b.item_id = e.id_sales_order_item
SET 
 b.date_ordered = date(c.created_at),
 b.datetime_ordered = c.created_at;

SELECT  'Drop TMP_cod_rastreamento',now();
DROP TEMPORARY TABLE IF EXISTS operations_ve.TMP_cod_rastreamento;
SELECT  'Create TMP_cod_rastreamento',now();
CREATE TEMPORARY TABLE operations_ve.TMP_cod_rastreamento ( INDEX ( cod_rastreamento )  )
	SELECT
	
	itens.cod_rastreamento,
		CASE
	WHEN tms.date < '2012-05-01' THEN
		NULL
	ELSE
		max(tms.date)
	END as date_delivered
	FROM
		           wmsprod_ve.vw_itens_venda_entrega AS itens
		INNER JOIN wmsprod_ve.tms_status_delivery AS tms
	            ON tms.cod_rastreamento = itens.cod_rastreamento
    WHERE 		
	    tms.id_status = 4
GROUP BY itens.cod_rastreamento;

SELECT  'Drop TMP_cod_rastreamento_Item',now();
DROP TEMPORARY TABLE IF EXISTS operations_ve.TMP_cod_rastreamento_Item;
SELECT  'Create TMP_cod_rastreamento_Item',now();
CREATE TEMPORARY TABLE operations_ve.TMP_cod_rastreamento_Item ( INDEX ( item_id ) )  
SELECT
	a.cod_rastreamento,
   a.date_delivered,
   b.item_id
FROM
              operations_ve.TMP_cod_rastreamento AS a
   INNER JOIN wmsprod_ve.vw_itens_venda_entrega AS b
           ON b.cod_rastreamento = a.cod_rastreamento
;

SELECT  'Update date_delivered',now();
UPDATE            operations_ve.pro_order_tracking_dates  AS a
       INNER JOIN operations_ve.TMP_cod_rastreamento_Item AS b
            USING ( item_id )
SET 
    a.date_delivered =  b.date_delivered;
#Query repetido.
#UPDATE 
#((bob_live_ve.sales_order_address AS a
#RIGHT JOIN (operations_ve.pro_order_tracking_dates AS b
#				INNER JOIN bob_live_ve.sales_order AS c
#				ON b.order_number = c.order_nr)
#ON a.id_sales_order_address = c.fk_sales_order_address_billing)
#LEFT JOIN bob_live_ve.customer_address_region AS d
#ON a.fk_customer_address_region = d.id_customer_address_region)
#INNER JOIN bob_live_ve.sales_order_item AS e
#ON b.item_id = e.id_sales_order_item
#SET 
# b.date_ordered = date(c.created_at),
# b.datetime_ordered = c.created_at
#;
SELECT  'Update date_backorder',now();
UPDATE operations_ve.pro_order_tracking_dates AS a
	INNER JOIN wmsprod_ve.itens_venda AS b
	ON a.item_id=b.item_id
	INNER JOIN
	(SELECT itens_venda_id,max(data) date
	FROM wmsprod_ve.status_itens_venda WHERE status = 'backorder'
	GROUP BY itens_venda_id) AS c
ON b.itens_venda_id=c.itens_venda_id
SET a.date_backorder = c.date,
a.is_backorder = 1;

SELECT  'Update date_backorder_tratada',now();
UPDATE operations_ve.pro_order_tracking_dates AS a
	INNER JOIN wmsprod_ve.itens_venda AS b
	ON a.item_id=b.item_id
	INNER JOIN
	(SELECT itens_venda_id,max(data) date
	FROM wmsprod_ve.status_itens_venda WHERE status = 'backorder_tratada'
	GROUP BY itens_venda_id) AS c
ON b.itens_venda_id=c.itens_venda_id
SET a.date_backorder_tratada = c.date;

# date_backorder_accepted
SELECT  'Update date_backorder_accepted',now();
UPDATE operations_ve.pro_order_tracking_dates AS a
 INNER JOIN wmsprod_ve.itens_venda AS b
 ON a.item_id=b.item_id
 INNER JOIN
 (SELECT 
  itens_venda_id,
  max(data) date
 FROM wmsprod_ve.status_itens_venda 
  WHERE status in('Analisando quebra','Waiting dropshipping','dropshipping notified') 
 GROUP BY itens_venda_id) AS c
ON b.itens_venda_id=c.itens_venda_id
AND c.date > a.date_backorder
SET a.date_backorder_accepted = c.date;

#cambiar despues operations_ve_project
SELECT  'Update date oms',now();
UPDATE  operations_ve.pro_order_tracking_dates AS a
INNER JOIN wmsprod_ve.itens_venda AS b
ON a.item_id=b.item_id
INNER JOIN wmsprod_ve.estoque AS c
ON b.estoque_id=c.estoque_id
INNER JOIN wmsprod_ve.itens_recebimento AS d
ON c.itens_recebimento_id=d.itens_recebimento_id
INNER JOIN procurement_live_ve.wms_received_item AS e 
ON d.itens_recebimento_id = e.id_wms
INNER JOIN operations_ve_project.out_procurement_tracking AS f
ON e.fk_procurement_order_item = f.id_procurement_order_item
SET 
a.date_po_created = date(f.date_po_created),
a.datetime_po_created = f.date_po_created,
a.date_po_updated  = date(f.date_po_created),
a.datetime_po_updated = f.date_po_updated,
a.date_po_issued = date(f.date_po_issued),
a.datetime_po_issued = f.date_po_issued,
a.date_po_confirmed = date(f.date_po_confirmed),
a.datetime_po_confirmed = f.date_po_confirmed;

#UPDATE operations_ve.pro_order_tracking_dates AS a
#INNER JOIN operations_ve.tbl_purchase_order AS b
#ON a.item_id=b.itemid
#SET a.date_po_created = date(b.fecha_creacion),
#a.datetime_po_created = b.fecha_creacion;

#es igual que date_shipped
#UPDATE ((operations_ve.pro_order_tracking_dates AS a
#INNER JOIN wmsprod_ve.itens_venda AS b
#ON a.item_id = b.item_id)
#INNER JOIN wmsprod_ve.status_itens_venda AS c
#ON b.itens_venda_id = c.itens_venda_id) 
#SET a.date_shipped = date(c.data),
#a.date_shipped = c.data
#WHERE ((c.status)="Expedido");

SELECT  'Update date_procured',now();
UPDATE operations_ve.pro_order_tracking_dates AS a
INNER JOIN wmsprod_ve.itens_venda AS b
ON a.item_id = b.item_id
INNER JOIN wmsprod_ve.status_itens_venda AS c
ON b.itens_venda_id = c.itens_venda_id
SET 
a.date_procured = date(DATA),
a.datetime_procured = DATA,
a.date_assigned_to_stock  = date(DATA),
a.datetime_assigned_to_stock  = DATA
WHERE c.STATUS = "estoque reservado";

SELECT  'Update date_procured2',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN wmsprod_ve.itens_venda
                ON pro_order_tracking_dates.item_id = itens_venda.item_id
INNER JOIN wmsprod_ve.itens_recebimento
                ON itens_venda.itens_venda_id = itens_recebimento.itens_venda_id
SET
date_procured = date(itens_recebimento.data_criacao),
datetime_procured = itens_recebimento.data_criacao
WHERE itens_recebimento.data_criacao IS NOT NULL;

SELECT  'Update date_ready_to_pick',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN wmsprod_ve.itens_venda 
	ON pro_order_tracking_dates.item_id = itens_venda.item_id
INNER JOIN wmsprod_ve.status_itens_venda 
	ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
SET 
 pro_order_tracking_dates.date_ready_to_pick = date(DATA),
 pro_order_tracking_dates.datetime_ready_to_pick = DATA
WHERE
	status_itens_venda. STATUS = "Aguardando separacao";

SELECT  'Update date_ready_to_pick2',now();
UPDATE operations_ve.pro_order_tracking_dates
   INNER JOIN operations_ve.calcworkdays
           ON  date( date_ready_to_pick ) = calcworkdays.date_first 
              AND workdays = 1
              AND isweekday = 1 
              AND isholiday = 1
SET 
   date_ready_to_pick = calcworkdays.date_last;

SELECT  'Truncate pro_max_date_ready_to_ship',now();
TRUNCATE operations_ve.pro_max_date_ready_to_ship;

SELECT  'Insert pro_max_date_ready_to_ship',now();
INSERT INTO operations_ve.pro_max_date_ready_to_ship (date_ready, order_item_id) SELECT
	min(DATA) AS date_ready,
	pro_order_tracking_dates.item_id
FROM
	operations_ve.pro_order_tracking_dates
INNER JOIN wmsprod_ve.itens_venda ON pro_order_tracking_dates.item_id = itens_venda.item_id
INNER JOIN wmsprod_ve.status_itens_venda ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
WHERE
	status_itens_venda. STATUS = "faturado"
GROUP BY
	pro_order_tracking_dates.item_id;

SELECT  'Update datetime_ready_to_ship',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN operations_ve.pro_max_date_ready_to_ship ON pro_order_tracking_dates.item_id = pro_max_date_ready_to_ship.order_item_id
SET pro_order_tracking_dates.date_ready_to_ship = date(pro_max_date_ready_to_ship.date_ready),
 pro_order_tracking_dates.datetime_ready_to_ship = pro_max_date_ready_to_ship.date_ready;

SELECT  'Update date_shipping_list',now();
UPDATE ((operations_ve.pro_order_tracking_dates AS a
INNER JOIN wmsprod_ve.itens_venda AS b
ON a.item_id = b.item_id)
INNER JOIN wmsprod_ve.status_itens_venda AS c
ON b.itens_venda_id = c.itens_venda_id) 
SET a.date_shipping_list = date(data),
a.datetime_shipping_list = data
WHERE c.status="Aguardando expedicao";

SELECT  'Update date_shipped',now();
UPDATE operations_ve.pro_order_tracking_dates a
INNER JOIN wmsprod_ve.itens_venda b
	ON a.item_id = b.item_id
INNER JOIN wmsprod_ve.status_itens_venda d 
	ON b.itens_venda_id = d.itens_venda_id
SET
 a.date_shipped = (SELECT
												max(date(DATA))
											FROM
												wmsprod_ve.itens_venda c,
												wmsprod_ve.status_itens_venda e
											WHERE
												e. STATUS = "expedido"
											AND b.itens_venda_id = c.itens_venda_id
											AND e.itens_venda_id = d.itens_venda_id
										),
  a.datetime_shipped = (SELECT
													max(DATA)
													FROM
														wmsprod_ve.itens_venda c,
														wmsprod_ve.status_itens_venda e
													WHERE
														e. STATUS = "expedido"
													AND b.itens_venda_id = c.itens_venda_id
													AND e.itens_venda_id = d.itens_venda_id
										);

SELECT  'Truncate pro_1st_attempt',now();
TRUNCATE operations_ve.pro_1st_attempt;

SELECT  'Insert pro_1st_attempt',now();
INSERT INTO operations_ve.pro_1st_attempt (order_item_id, min_of_date) SELECT
	pro_order_tracking_dates.item_id,
	min(tms_status_delivery.date) AS min_of_date
FROM
	(
		operations_ve.pro_order_tracking_dates
		INNER JOIN wmsprod_ve.vw_itens_venda_entrega ON pro_order_tracking_dates.item_id = vw_itens_venda_entrega.item_id
	)
INNER JOIN wmsprod_ve.tms_status_delivery ON vw_itens_venda_entrega.cod_rastreamento = tms_status_delivery.cod_rastreamento
GROUP BY
	pro_order_tracking_dates.item_id,
	tms_status_delivery.id_status
HAVING
	tms_status_delivery.id_status = 5;

SELECT  'Update date_1st_attempt',now();
UPDATE operations_ve.pro_order_tracking_dates 
INNER JOIN operations_ve.pro_1st_attempt ON pro_order_tracking_dates.item_id = pro_1st_attempt.order_item_id
SET pro_order_tracking_dates.date_1st_attempt = pro_1st_attempt.min_of_date;

SELECT  'Update pro_order_tracking_dates',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN wmsprod_ve.vw_itens_venda_entrega vw 
	ON pro_order_tracking_dates.item_id = vw.item_id
INNER JOIN wmsprod_ve.tms_status_delivery del 
	ON vw.cod_rastreamento = del.cod_rastreamento
SET operations_ve.pro_order_tracking_dates.date_1st_attempt = (
	SELECT
		max(tms.date)
	FROM
		wmsprod_ve.tms_status_delivery tms,
		wmsprod_ve.vw_itens_venda_entrega itens
	WHERE
		del.cod_rastreamento = tms.cod_rastreamento
	AND vw.item_id = itens.item_id
	AND tms.id_status = 4
)
WHERE
	pro_order_tracking_dates.date_1st_attempt IS NULL
OR pro_order_tracking_dates.date_1st_attempt < '2011-05-01';

SELECT  'Update date_failed_delivery',now();
UPDATE operations_ve.pro_order_tracking_dates AS a
INNER JOIN wmsprod_ve.vw_itens_venda_entrega AS b
ON a.item_id = b.item_id
INNER JOIN wmsprod_ve.tms_status_delivery AS c
ON b.cod_rastreamento = c.cod_rastreamento
SET a.date_failed_delivery = c.date
WHERE c.id_status=10;

#no aplica
#UPDATE operations_ve.pro_order_tracking_dates as a
#INNER JOIN operations_ve.tbl_bi_ops_ingreso_bodega_devolucion as b
#ON a.item_id=b.item_id
#SET 
#a.date_returned=b.date_ingreso_bodega_devolucion;

SELECT  'Update date_procured_promised',now();
UPDATE        operations_ve.pro_order_tracking_dates 
   INNER JOIN operations_ve.calcworkdays
           ON     date( date_exported )           = calcworkdays.date_first 
              AND workdays = supplier_leadtime
              AND isweekday = 1 
              AND isholiday = 0
SET 
	date_procured_promised = calcworkdays.date_last
WHERE
	pro_order_tracking_dates.date_exported IS NOT NULL;

SELECT  'Update wh_time',now();
UPDATE operations_ve.pro_order_tracking_dates AS a
INNER JOIN operations_ve.out_order_tracking_sample b ON a.item_id = b.item_id
SET 
a.wh_time=if((b.package_measure_new='Small' OR b.package_measure_new='Medium'),1,2);

SELECT  'Update date_ready_to_ship_promised',now();
UPDATE operations_ve.pro_order_tracking_dates 
INNER JOIN operations_ve.calcworkdays
ON date(date_procured_promised) = calcworkdays.date_first 
AND workdays = wh_time
AND isweekday = 1
AND isholiday = 0
SET 
	date_ready_to_ship_promised = calcworkdays.date_last
WHERE
	operations_ve.pro_order_tracking_dates.date_procured_promised IS NOT NULL;

#No aplica para peru.
#UPDATE operations_ve.pro_order_tracking_dates AS a
#INNER JOIN 
#(SELECT sku_simple,max(date_update) as date_update FROM operations_ve.tbl_bi_ops_stock_update GROUP BY sku_simple) as t
#ON a.sku_simple=t.sku_simple
#SET a.date_stock_updated = t.date_update;

SELECT  'Update date_delivered_promised',now();
UPDATE  operations_ve.pro_order_tracking_dates 
   INNER JOIN operations_ve.calcworkdays
		ON     date( date_exported ) = calcworkdays.date_first 
			AND workdays = if((max_delivery_time is null or max_delivery_time =''  or max_delivery_time=0),7,max_delivery_time)
			AND isweekday = 1
			AND isholiday = 0
SET 
   date_delivered_promised = calcworkdays.date_last
WHERE
	pro_order_tracking_dates.date_exported IS NOT NULL;

SELECT  'Drop tmp_order_tracking_stockout_declared',now();
DROP TEMPORARY TABLE IF EXISTS operations_ve.tmp_order_tracking_stockout_declared;

SELECT  'Create tmp_order_tracking_stockout_declared',now();
CREATE TEMPORARY TABLE operations_ve.tmp_order_tracking_stockout_declared (INDEX (itens_venda_id))
SELECT
                a.itens_venda_id,
                a.numero_order,
                a.item_id,
                b.date
FROM
                wmsprod_ve.itens_venda a
INNER JOIN (  SELECT
								itens_venda_id,
								max(DATA) date
							FROM
								wmsprod_ve.status_itens_venda
							WHERE STATUS = 'Quebrado'
							GROUP BY itens_venda_id) b
ON  a.itens_venda_id = b.itens_venda_id;
 
SELECT  'Update date_declared_stockout',now();
UPDATE operations_ve.pro_order_tracking_dates d
INNER JOIN operations_ve.tmp_order_tracking_stockout_declared t
                ON d.item_id = t.item_id
SET d.date_declared_stockout = t.date;

SELECT  'Update date_declared_backorder',now();
UPDATE operations_ve.pro_order_tracking_dates d
INNER JOIN (
	SELECT
		a.item_id,
		a.tempo_de_entrega,
		a.tempo_entrega_fornecedor,
		a.data_criacao,
		a.estimated_delivery_date,
		a.estimated_delivery_days,
		a.estimated_days_on_wh,
		b.days_back,
		b.date_created
	FROM
		wmsprod_ve.itens_venda a
	JOIN wmsprod_ve.item_backorder b ON a.itens_venda_id = b.itens_venda_id
) c ON d.item_id = c.item_id
SET d.date_declared_backorder = c.date_created;

SELECT  'Update date_status_value_chain',now();
UPDATE operations_ve.pro_order_tracking_dates AS a
INNER JOIN
(SELECT  
	d.item_id,
	b.status,
	b.data 
FROM
(select 
	itens_venda_id,
	status, 
	max(data) as data
from
(SELECT 
	itens_venda_id,
	status,
	data
FROM wmsprod_ve.status_itens_venda
ORDER BY data desc) a
GROUP BY itens_venda_id) AS b
INNER JOIN wmsprod_ve.itens_venda AS d
ON b.itens_venda_id=d.itens_venda_id) AS t
ON a.item_id = t.item_id
SET 
a.date_status_value_chain = t.data,
a.status_value_chain=t.status;

SELECT  'Update date_1st_attempt',now();
UPDATE operations_ve.pro_order_tracking_dates AS a
SET a.date_status_value_chain=date_1st_attempt,
status_value_chain='Intentado'
WHERE status_wms='Expedido'
AND date_1st_attempt IS NOT NULL;

SELECT  'Update date_status_value_chain2',now();
UPDATE operations_ve.pro_order_tracking_dates AS a
SET a.date_status_value_chain=date_delivered,
status_value_chain='Entregado'
WHERE status_wms='Expedido'
AND date_delivered IS NOT NULL;

SELECT  'Update date_status_value_chain3',now();
UPDATE operations_ve.pro_order_tracking_dates AS a
INNER JOIN wmsprod_ve.inverselogistics_devolucion AS b
ON a.item_id=b.item_id
SET a.date_status_value_chain=b.modified_at,
status_value_chain=b.status;

#workdays to create po
SELECT  'Update workdays_to_po',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN operations_ve.calcworkdays
	ON pro_order_tracking_dates.date_exported = calcworkdays.date_first
		AND pro_order_tracking_dates.date_po_created = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_po =	calcworkdays.workdays;

SELECT  'Update workdays_to_po',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN operations_ve.calcworkdays
	ON pro_order_tracking_dates.date_exported = calcworkdays.date_first
		AND curdate() = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_po =	calcworkdays.workdays
WHERE date_exported IS NULL;

#Workdays to confirm
SELECT  'Update workdays_to_confirm',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN operations_ve.calcworkdays
	ON pro_order_tracking_dates.date_po_created = calcworkdays.date_first
		AND pro_order_tracking_dates.date_po_confirmed = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_confirm =	calcworkdays.workdays;

SELECT  'Update workdays_to_confirm2',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN operations_ve.calcworkdays
	ON pro_order_tracking_dates.date_po_created = calcworkdays.date_first
		AND curdate() = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_confirm =	calcworkdays.workdays
WHERE date_exported IS NULL;

#Workdays to send po
SELECT  'Update workdays_to_send_po',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN operations_ve.calcworkdays
	ON pro_order_tracking_dates.date_po_confirmed = calcworkdays.date_first
		AND pro_order_tracking_dates.date_po_issued = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_send_po =	calcworkdays.workdays;

SELECT  'Update workdays_to_send_po',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN operations_ve.calcworkdays
	ON pro_order_tracking_dates.date_po_confirmed = calcworkdays.date_first
		AND curdate() = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_send_po =	calcworkdays.workdays
WHERE date_exported IS NULL;

#Workdays to export
SELECT  'Update workdays_to_export',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN operations_ve.calcworkdays
	ON pro_order_tracking_dates.date_ordered = calcworkdays.date_first
		AND pro_order_tracking_dates.date_exported = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_export =	calcworkdays.workdays;

SELECT  'Update workdays_to_export2',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN operations_ve.calcworkdays
	ON pro_order_tracking_dates.date_ordered = calcworkdays.date_first
		AND curdate() = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_export =	calcworkdays.workdays
WHERE date_exported IS NULL;

#Workdays to ready
SELECT  'Update workdays_to_ready',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN operations_ve.calcworkdays
	ON pro_order_tracking_dates.date_ready_to_pick	= calcworkdays.date_first
		AND pro_order_tracking_dates.date_ready_to_ship 	= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_ready 	=	calcworkdays.workdays;

SELECT  'Update workdays_to_ready2',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN operations_ve.calcworkdays
	ON pro_order_tracking_dates.date_ready_to_pick	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_ready 	=	calcworkdays.workdays
WHERE date_ready_to_ship IS NULL;

#Workdays to ship
SELECT  'Update workdays_to_ship',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN operations_ve.calcworkdays
	ON pro_order_tracking_dates.date_ready_to_ship	= calcworkdays.date_first
		AND pro_order_tracking_dates.date_shipped 		= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_ship 		=	calcworkdays.workdays;

SELECT  'Update workdays_to_ship2',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN operations_ve.calcworkdays
	ON pro_order_tracking_dates.date_ready_to_ship	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_ship 	=	calcworkdays.workdays
WHERE date_shipped IS NULL;

#workdays_to_1st_attempt
SELECT  'Update workdays_to_1st_attempt',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN operations_ve.calcworkdays
	ON pro_order_tracking_dates.date_shipped	= calcworkdays.date_first
		AND pro_order_tracking_dates.date_1st_attempt 		= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_1st_attempt 		=	calcworkdays.workdays;

SELECT  'Update workdays_to_1st_attempt',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN operations_ve.calcworkdays
	ON pro_order_tracking_dates.date_shipped	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_1st_attempt 	=	calcworkdays.workdays
WHERE date_1st_attempt IS NULL;

#workdays_to_deliver
SELECT  'Update workdays_to_deliver',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN operations_ve.calcworkdays
	ON pro_order_tracking_dates.date_shipped	= calcworkdays.date_first
		AND pro_order_tracking_dates.date_delivered 		= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_deliver 		=	calcworkdays.workdays;

SELECT  'Update workdays_to_deliver2',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN operations_ve.calcworkdays
	ON pro_order_tracking_dates.date_shipped	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_deliver 	=	calcworkdays.workdays
WHERE date_delivered IS NULL;

#workdays to request recollection
SELECT  'Drop tmp_order_tracking_stockout_declared',now();
DROP TEMPORARY TABLE IF EXISTS operations_ve.tmp_order_tracking_stockout_declared;

SELECT  'Create tmp_order_tracking_stockout_declared',now();
CREATE TEMPORARY TABLE operations_ve.tmp_order_tracking_stockout_declared (INDEX (itens_venda_id))
SELECT
                a.itens_venda_id,
                a.numero_order,
                a.item_id,
                b.date
FROM
                wmsprod_ve.itens_venda a
INNER JOIN ( 	SELECT
								itens_venda_id,
								max(DATA) date
							FROM
								wmsprod_ve.status_itens_venda
							WHERE STATUS = 'Quebrado'
							GROUP BY itens_venda_id) b
ON  a.itens_venda_id = b.itens_venda_id;

SELECT  'Update date_declared_stockout',now();
UPDATE operations_ve.pro_order_tracking_dates d
INNER JOIN tmp_order_tracking_stockout_declared t
                ON d.item_id = t.item_id
SET d.date_declared_stockout = t.date;

#workdays to stockout declared
SELECT  'Update date_declared_stockout2',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN operations_ve.calcworkdays
 ON pro_order_tracking_dates.date_ordered = calcworkdays.date_first
  AND pro_order_tracking_dates.date_declared_stockout  = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_stockout_declared = calcworkdays.workdays
WHERE date_declared_stockout is not null ;

#workdays to create tracking code
SELECT  'Update workdays_to_create_tracking_code',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN operations_ve.calcworkdays
 ON date(pro_order_tracking_dates.date_po_confirmed) = calcworkdays.date_first
  AND date(pro_order_tracking_dates.date_shipped)  = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_create_tracking_code = calcworkdays.workdays
WHERE date_po_confirmed is not null AND  date_shipped  is not null ;

#workdays_total_1st_attempt
SELECT  'Update workdays_total_1st_attempt',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN operations_ve.calcworkdays
	ON pro_order_tracking_dates.date_exported	= calcworkdays.date_first
		AND pro_order_tracking_dates.date_1st_attempt 		= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_total_1st_attempt 		=	calcworkdays.workdays
WHERE date_shipped IS NOT NULL;

SELECT  'Update workdays_total_1st_attempt',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN operations_ve.calcworkdays
	ON pro_order_tracking_dates.date_exported	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_total_1st_attempt 	=	calcworkdays.workdays
WHERE date_shipped IS NOT NULL
AND date_1st_attempt IS NULL;

#workdays_total_delivery
SELECT  'Update workdays_total_delivery',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN operations_ve.calcworkdays
	ON pro_order_tracking_dates.date_exported	= calcworkdays.date_first
		AND pro_order_tracking_dates.date_delivered 		= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_total_delivery 		=	calcworkdays.workdays
WHERE date_shipped IS NOT NULL;

SELECT  'Update workdays_total_delivery',now();
UPDATE operations_ve.pro_order_tracking_dates
INNER JOIN operations_ve.calcworkdays
	ON pro_order_tracking_dates.date_exported	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_total_delivery 	=	calcworkdays.workdays
WHERE date_shipped IS NOT NULL
AND date_1st_attempt IS NULL;

SELECT  'Update Week y Month',now();
UPDATE operations_ve.pro_order_tracking_dates
SET 
 week_ordered = date_format(date_ordered,"%Y-%u"),
 week_procured_promised = date_format(date_procured_promised,"%Y-%u"),
 week_r2s_promised = date_format(date_ready_to_ship_promised,"%Y-%u"),
 week_delivered_promised = date_format(date_delivered_promised,"%Y-%u"),
 week_shipped = date_format(date_shipped,"%Y-%u"),
 month_procured_promised = date_format(date_procured_promised,"%Y-%m"),
 month_delivered_promised = date_format(date_delivered_promised,"%Y-%m"),
 month_r2s_promised = date_format(date_ready_to_ship_promised,"%Y-%m");

SELECT  'Update r2s_workday',now();
UPDATE        operations_ve.pro_order_tracking_dates
   INNER JOIN operations_ve.calcworkdays
           ON     pro_order_tracking_dates.date_ready_to_pick = calcworkdays.date_first
              AND calcworkdays.date_last = curdate() 
SET 
   r2s_workday_0  = IF( workdays_to_ready  = 0 AND workdays >= 0, 1 , r2s_workday_0 ),
   r2s_workday_1  = IF( workdays_to_ready <= 1 AND workdays >= 1, 1 , r2s_workday_1 ),
   r2s_workday_2  = IF( workdays_to_ready <= 2 AND workdays >= 2, 1 , r2s_workday_2 ),
   r2s_workday_3  = IF( workdays_to_ready <= 3 AND workdays >= 3, 1 , r2s_workday_3 ),
   r2s_workday_4  = IF( workdays_to_ready <= 4 AND workdays >= 4, 1 , r2s_workday_4 ),
   r2s_workday_5  = IF( workdays_to_ready <= 5 AND workdays >= 5, 1 , r2s_workday_5 ),
   r2s_workday_6  = IF( workdays_to_ready <= 6 AND workdays >= 6, 1 , r2s_workday_6 ), 
   r2s_workday_7  = IF( workdays_to_ready <= 7 AND workdays >= 7, 1 , r2s_workday_7 ), 
   r2s_workday_8  = IF( workdays_to_ready <= 8 AND workdays >= 8, 1 , r2s_workday_8 ),  
   r2s_workday_9  = IF( workdays_to_ready <= 9 AND workdays >= 9, 1 , r2s_workday_9 ),  
   r2s_workday_10 = IF( workdays_to_ready  >10 AND workdays >=10, 1 , r2s_workday_10);

SELECT  'Update check_dates',now();
UPDATE operations_ve.pro_order_tracking_dates
SET check_dates =
								 IF (date_ordered > date_exported,"Date ordered > date exported",
								 IF (date_exported > date_procured,"Date exported > Date procured",
								 IF (date_procured > date_ready_to_ship,"Date procured > Date ready to ship",
								 IF (date_ready_to_ship > date_shipped,"Date ready to ship > Date shipped",
								 IF (date_shipped > date_1st_attempt,"Date shipped > Date 1st attempt",
								 IF (date_1st_attempt > date_delivered,"Date 1st atteempt> Date delivered",
																 "Correct"))))));

SELECT  'Update checks',now();
UPDATE operations_ve.pro_order_tracking_dates
SET
check_date_exported = CASE
												WHEN date_exported IS NULL
												THEN (CASE
																WHEN date_procured IS NULL
																AND date_ready_to_ship IS NULL
																AND date_shipped IS NULL
																AND date_1st_attempt IS NULL
																AND date_delivered IS NULL
																THEN 0
																ELSE 1
															END )
												ELSE (CASE
															 WHEN date_exported > date_procured
															 THEN 1
															 ELSE 0
															END)
												END,
check_date_ordered = CASE
											WHEN date_ordered IS NULL
											THEN (  CASE
															 WHEN date_exported IS NULL
															 AND date_procured IS NULL
															 AND date_ready_to_ship IS NULL
															 AND date_shipped IS NULL
															 AND date_1st_attempt IS NULL
															 AND date_delivered IS NULL
															 THEN 0
															 ELSE 1
															 END )
											ELSE (  CASE
															 WHEN date_ordered > date_exported
															 THEN 1
															 ELSE 0
															 END )
															END,
check_date_procured = CASE
												WHEN date_procured IS NULL
												THEN (  CASE
																 WHEN date_ready_to_ship IS NULL
																 AND date_shipped IS NULL
																 AND date_1st_attempt IS NULL
																 AND date_delivered IS NULL
																 THEN 0
																 ELSE 1
																 END )
												ELSE (  CASE
																 WHEN date_procured > date_ready_to_ship
																 THEN 1
																 ELSE 0
																 END )
																END,
check_date_ready_to_ship = CASE
														 WHEN date_ready_to_ship IS NULL
														 THEN (  CASE
																			 WHEN date_shipped IS NULL
																			 AND date_1st_attempt IS NULL
																			 AND date_delivered IS NULL
																			 THEN 0
																			 ELSE 1
																			 END )
														 ELSE (    CASE
																				WHEN date_ready_to_ship > date_shipped
																				THEN 1
																				ELSE 0
																				END)
														 END,
check_date_shipped = CASE
											WHEN date_shipped IS NULL
											THEN (  CASE
																											WHEN date_1st_attempt IS NULL
																											AND date_delivered IS NULL
																											THEN 0
																											ELSE 1
																											END )
											ELSE (    CASE
																											WHEN date_shipped > date_1st_attempt
																											THEN 1
																											ELSE 0
																											END)
											END,
check_date_1st_attempt = CASE
													WHEN date_1st_attempt IS NULL
													THEN (  CASE
																												 WHEN date_delivered IS NULL
																												 THEN 0
																												 ELSE 1
																												 END )
													ELSE (    CASE
																												 WHEN date_1st_attempt > date_delivered
																												 THEN 1
																												 ELSE 0
																												 END )
													END;

SELECT  'Update check_date_stockout_over_bo',now();
UPDATE operations_ve.pro_order_tracking_dates
SET
 check_date_stockout_over_bo = IF(min_delivery_time>=max_delivery_time,1,0);

SELECT  'Update times',now();
UPDATE operations_ve.pro_order_tracking_dates
SET 
pro_order_tracking_dates.time_to_po = If(datetime_exported is Null,null,If(datetime_po_created is Null, TIMEDIFF(CURDATE(),datetime_exported), TIMEDIFF(datetime_po_created,datetime_exported))),
pro_order_tracking_dates.time_to_confirm = If(datetime_po_issued is Null,null,If(datetime_po_confirmed is Null, TIMEDIFF(CURDATE(),datetime_po_issued), TIMEDIFF(datetime_po_confirmed,datetime_po_issued))),
pro_order_tracking_dates.time_to_send_po = If(datetime_po_created is Null,null,If(datetime_po_issued is Null, TIMEDIFF(CURDATE(),datetime_po_created), TIMEDIFF(datetime_po_issued,datetime_po_created))),
pro_order_tracking_dates.time_to_procure = If(datetime_exported is Null,null,If(datetime_procured is Null, TIMEDIFF(CURDATE(),datetime_exported), TIMEDIFF(datetime_procured,datetime_exported))),
pro_order_tracking_dates.time_to_export = If(datetime_ordered is Null,null,If(datetime_exported is Null, TIMEDIFF(CURDATE(),datetime_ordered), TIMEDIFF(datetime_exported,datetime_ordered))),
pro_order_tracking_dates.time_to_ready = If(datetime_ready_to_pick is Null,null,If(datetime_ready_to_ship is Null, TIMEDIFF(CURDATE(),datetime_ready_to_pick), TIMEDIFF(datetime_ready_to_ship,datetime_ready_to_pick))),
pro_order_tracking_dates.time_to_ship = If(datetime_ready_to_ship is Null,null,If(datetime_shipped is Null, TIMEDIFF(CURDATE(),datetime_ready_to_ship), TIMEDIFF(datetime_shipped,datetime_ready_to_ship))),
pro_order_tracking_dates.time_to_1st_attempt = If(datetime_shipped is Null,null,If(date_1st_attempt is Null, TIMEDIFF(CURDATE(),datetime_shipped), TIMEDIFF(date_1st_attempt,datetime_shipped))),
pro_order_tracking_dates.time_to_deliver = If(datetime_shipped is Null,null,If(date_delivered is Null, TIMEDIFF(CURDATE(),datetime_shipped), TIMEDIFF(date_delivered,datetime_shipped))),
pro_order_tracking_dates.time_to_request_recollection = If(datetime_shipped is Null,null,If(date_delivered is Null, TIMEDIFF(CURDATE(),datetime_shipped), TIMEDIFF(date_delivered,datetime_shipped))),
pro_order_tracking_dates.time_to_stockout_declared = If(datetime_ordered is Null,null,If(date_declared_stockout is Null, TIMEDIFF(CURDATE(),datetime_ordered), TIMEDIFF(date_declared_stockout,datetime_ordered))),
pro_order_tracking_dates.time_to_createPO = If(datetime_exported is Null,null,If(datetime_po_created is Null, TIMEDIFF(CURDATE(),datetime_exported), TIMEDIFF(datetime_po_created,datetime_exported))),
pro_order_tracking_dates.time_to_create_tracking_code = If(datetime_po_confirmed is Null,null,If(datetime_shipped is Null, TIMEDIFF(CURDATE(),datetime_po_confirmed), TIMEDIFF(datetime_shipped,datetime_po_confirmed))),
pro_order_tracking_dates.time_total_1st_attempt = If(datetime_exported is Null,null,If(date_1st_attempt is Null, TIMEDIFF(CURDATE(),datetime_exported), TIMEDIFF(date_1st_attempt,datetime_exported))),
pro_order_tracking_dates.time_total_delivery = If(datetime_exported is Null,null,If(date_delivered is Null, TIMEDIFF(CURDATE(),datetime_exported), TIMEDIFF(date_delivered,datetime_exported)));

SELECT  'Update on_times',now();
UPDATE operations_ve.pro_order_tracking_dates
SET 
#ontime_to_po = If(date(date_po_created)<=date_po_created_promised,1,0),
#on_time_to_confirm = If(date(date_po_confirmed)<=date_po_confirmed_promised,1,0),
#on_time_to_send_po = If(date(date_po_issued)<=date_po_issued_promised,1,0),
on_time_to_procure = If(date(date_procured)<=date_procured_promised,1,0),
#ontime_to_export = If(date(date_exported)<=date_exported_promised,1,0),
on_time_to_ready = if(date_ready_to_ship<=date_ready_to_ship_promised,1,0),
on_time_to_ship = if(date_shipped<=date_ready_to_ship_promised,1,0),
on_time_to_1st_attempt = if(date_1st_attempt <=date_delivered_promised,1,0),
on_time_to_deliver = if(date_delivered<=date_delivered_promised,1,0),
#on_time_to_request_recollection = If(date_first_attempt<=promised_delivery_date,1,0),
#on_time_to_stockout_declared = If(date_shipped is not null,(if(date_shipped<=date_ready_to_ship_promised,1,0)),(if(date_ready_to_ship_promised<=curdate(),1,0))),
#on_time_to_create_po = If(date_shipped is not null,(if(date_shipped<=date_ready_to_ship_promised,1,0)),(if(date_ready_to_ship_promised<=curdate(),1,0))),
#on_time_to_create_tracking_code = If(date_first_attempt<=promised_delivery_date,1,0),
on_time_total_1st_attempt = If(date_delivered<=date_delivered_promised,1,0),
on_time_total_delivery = If(date_delivered<=date_delivered_promised,1,0);

SELECT  'Update days',now();
UPDATE operations_ve.pro_order_tracking_dates
SET 
pro_order_tracking_dates.days_to_po = If(date_exported is Null,null,If(date_po_created is Null, DATEDIFF(CURDATE(),date_exported), DATEDIFF(date_po_created,date_exported))),
pro_order_tracking_dates.days_to_confirm = If(date_po_issued is Null,null,If(date_po_confirmed is Null, DATEDIFF(CURDATE(),date_po_issued), DATEDIFF(date_po_confirmed,date_po_issued))),
pro_order_tracking_dates.days_to_send_po = If(date_po_created is Null,null,If(date_po_issued is Null, DATEDIFF(CURDATE(),date_po_created), DATEDIFF(date_po_issued,date_po_created))),
pro_order_tracking_dates.days_to_procure = If(date_exported is Null,null,If(date_procured is Null, DATEDIFF(CURDATE(),date_exported), DATEDIFF(date_procured,date_exported))),
pro_order_tracking_dates.days_to_export = If(date_ordered is Null,null,If(date_exported is Null, DATEDIFF(CURDATE(),date_ordered), DATEDIFF(date_exported,date_ordered))),
pro_order_tracking_dates.days_to_ready = If(date_ready_to_pick is Null,null,If(date_ready_to_ship is Null, DATEDIFF(CURDATE(),date_ready_to_pick), DATEDIFF(date_ready_to_ship,date_ready_to_pick))),
pro_order_tracking_dates.days_to_ship = If(date_ready_to_ship is Null,null,If(date_shipped is Null, DATEDIFF(CURDATE(),date_ready_to_ship), DATEDIFF(date_shipped,date_ready_to_ship))),
pro_order_tracking_dates.days_to_1st_attempt = If(date_shipped is Null,null,If(date_1st_attempt is Null, DATEDIFF(CURDATE(),date_shipped), DATEDIFF(date_1st_attempt,date_shipped))),
pro_order_tracking_dates.days_to_deliver = If(date_shipped is Null,null,If(date_delivered is Null, DATEDIFF(CURDATE(),date_shipped), DATEDIFF(date_delivered,date_shipped))),
pro_order_tracking_dates.days_to_request_recollection = If(date_shipped is Null,null,If(date_delivered is Null, DATEDIFF(CURDATE(),date_shipped), DATEDIFF(date_delivered,date_shipped))),
pro_order_tracking_dates.days_to_stockout_declared = If(date_ordered is Null,null,If(date_declared_stockout is Null, DATEDIFF(CURDATE(),date_ordered), DATEDIFF(date_declared_stockout,date_ordered))),
pro_order_tracking_dates.days_to_createPO = If(date_exported is Null,null,If(date_po_created is Null, DATEDIFF(CURDATE(),date_exported), DATEDIFF(date_po_created,date_exported))),
pro_order_tracking_dates.days_to_create_tracking_code = If(date_po_confirmed is Null,null,If(date_shipped is Null, DATEDIFF(CURDATE(),date_po_confirmed), DATEDIFF(date_shipped,date_po_confirmed))),
pro_order_tracking_dates.days_total_1st_attempt = If(date_exported is Null,null,If(date_1st_attempt is Null, DATEDIFF(CURDATE(),date_exported), DATEDIFF(date_1st_attempt,date_exported))),
pro_order_tracking_dates.days_total_delivery = If(date_exported is Null,null,If(date_delivered is Null, DATEDIFF(CURDATE(),date_exported), DATEDIFF(date_delivered,date_exported)));

SELECT  'Update shipped_same_day_as_procured',now();
UPDATE operations_ve.pro_order_tracking_dates
SET 
pro_order_tracking_dates.shipped_same_day_as_procured = If (date_procured = date_shipped,1,0), 
pro_order_tracking_dates.shipped_same_day_as_order = If (date_ordered = date_shipped ,1,0);

SELECT  'Update 24hr_shipments',now();
UPDATE operations_ve.pro_order_tracking_dates
SET 
24hr_shipments_Cust_persp = If(date_ordered is Null,null,If(date_shipped is Null,if( Datediff(CURDATE(),date_ordered)= 1,1,0), if( Datediff(date_shipped,date_ordered)=1,1,0))),
24hr_shipmnets_WH_persp = If(date_ready_to_pick is Null,null,If(date_shipped is Null, if( Datediff(CURDATE(),date_ready_to_pick)= 1,1,0), if( Datediff(date_shipped,date_ready_to_pick)=1,1,0)));

SELECT  'Update out_order_tracking_sample',now();
UPDATE operations_ve.out_order_tracking_sample a
INNER JOIN operations_ve.pro_order_tracking_dates AS b
ON a.item_id = b.item_id
SET
a.sku_simple = b.sku_simple,
a.status_wms = b.status_wms,
a.fulfillment_type_bob = b.fulfillment_type_bob,
a.supplier_leadtime = b.supplier_leadtime,
a.supplier_id = b.supplier_id,
a.min_delivery_time = b.min_delivery_time,
a.max_delivery_time = b.max_delivery_time,
a.wh_time = b.wh_time,
a.date_ordered = b.date_ordered,
a.date_exported = b.date_exported,
a.date_backorder = b.date_backorder,
a.date_backorder_tratada = b.date_backorder_tratada,
a.date_backorder_accepted = b.date_backorder_accepted,
a.date_po_created = b.date_po_created,
a.date_po_updated = b.date_po_updated,
a.date_po_issued = b.date_po_issued,
a.date_po_confirmed = b.date_po_confirmed,
a.date_shipped = b.date_shipped,
a.date_procured = b.date_procured,
a.date_assigned_to_stock = b.date_assigned_to_stock,
a.date_ready_to_pick = b.date_ready_to_pick,
a.date_ready_to_ship = b.date_ready_to_ship,
a.date_shipped = b.date_shipped,
a.date_1st_attempt = b.date_1st_attempt,
a.date_delivered = b.date_delivered,
a.date_failed_delivery = b.date_failed_delivery,
a.date_returned = b.date_returned,
a.date_procured_promised = b.date_procured_promised,
a.date_ready_to_ship_promised = b.date_ready_to_ship_promised,
a.date_delivered_promised = b.date_delivered_promised,
a.status_value_chain = b.status_value_chain,
a.date_status_value_chain = b.date_status_value_chain,
a.date_stock_updated = b.date_stock_updated,
a.date_declared_stockout = b.date_declared_stockout,
a.date_declared_backorder = b.date_declared_backorder,
a.datetime_ordered = b.datetime_ordered,
a.datetime_exported = b.datetime_exported,
a.datetime_po_created = b.datetime_po_created,
a.datetime_po_updated = b.datetime_po_updated,
a.datetime_po_issued = b.datetime_po_issued,
a.datetime_po_confirmed = b.datetime_po_confirmed,
a.datetime_shipped = b.datetime_shipped,
a.datetime_procured = b.datetime_procured,
a.datetime_assigned_to_stock = b.datetime_assigned_to_stock,
a.datetime_ready_to_pick = b.datetime_ready_to_pick,
a.datetime_ready_to_ship = b.datetime_ready_to_ship,
a.datetime_shipped = b.datetime_shipped,
a.week_ordered = b.week_ordered,
a.week_procured_promised = b.week_procured_promised,
a.week_delivered_promised = b.week_delivered_promised,
a.week_r2s_promised = b.week_r2s_promised,
a.week_shipped = b.week_shipped,
a.month_procured_promised = b.month_procured_promised,
a.month_delivered_promised = b.month_delivered_promised,
a.month_r2s_promised = b.month_r2s_promised,
a.check_dates = b.check_dates,
a.check_date_exported = b.check_date_exported,
a.check_date_ordered = b.check_date_ordered,
a.check_date_procured = b.check_date_procured,
a.check_date_ready_to_ship = b.check_date_ready_to_ship,
a.check_date_shipped = b.check_date_shipped,
a.check_date_1st_attempt = b.check_date_1st_attempt,
a.check_date_stockout_over_bo = b.check_date_stockout_over_bo,
a.check_min_max = b.check_min_max,
a.workdays_to_po = b.workdays_to_po,
a.workdays_to_confirm = b.workdays_to_confirm,
a.workdays_to_send_po = b.workdays_to_send_po,
a.workdays_to_procure = b.workdays_to_procure,
a.workdays_to_export = b.workdays_to_export,
a.workdays_to_ready = b.workdays_to_ready,
a.workdays_to_ship = b.workdays_to_ship,
a.workdays_to_1st_attempt = b.workdays_to_1st_attempt,
a.workdays_to_deliver = b.workdays_to_deliver,
a.workdays_to_request_recollection = b.workdays_to_request_recollection,
a.workdays_to_stockout_declared = b.workdays_to_stockout_declared,
a.workdays_to_createPO = b.workdays_to_createPO,
a.workdays_to_create_tracking_code = b.workdays_to_create_tracking_code,
a.workdays_total_1st_attempt = b.workdays_total_1st_attempt,
a.workdays_total_delivery = b.workdays_total_delivery,
a.is_backorder = b.is_backorder,
a.workdays_to_backlog = b.workdays_to_backlog,
a.days_to_po = b.days_to_po,
a.days_to_confirm = b.days_to_confirm,
a.days_to_send_po = b.days_to_send_po, 
a.days_to_procure = b.days_to_procure,
a.days_to_export = b.days_to_export, 
a.days_to_ready = b.days_to_ready, 
a.days_to_ship = b.days_to_ship, 
a.days_to_1st_attempt = b.days_to_1st_attempt,
a.days_to_deliver = b.days_to_deliver,
a.days_to_request_recollection = b.days_to_request_recollection,
a.days_to_stockout_declared = b.days_to_stockout_declared,
a.days_to_createPO = b.days_to_createPO, 
a.days_to_create_tracking_code = b.days_to_create_tracking_code,
a.days_total_1st_attempt = b.days_total_1st_attempt,
a.days_total_delivery = b.days_total_delivery,
#a.days_to_backlog = b.days_to_backlog, 
a.days_since_stock_updated = b.days_since_stock_updated, 
a.time_to_po = b.time_to_po,
a.time_to_confirm = b.time_to_confirm, 
a.time_to_send_po = b.time_to_send_po,
a.time_to_procure = b.time_to_procure, 
a.time_to_export = b.time_to_export,
a.time_to_ready = b.time_to_ready, 
a.time_to_ship = b.time_to_ship,
a.time_to_1st_attempt = b.time_to_1st_attempt, 
a.time_to_deliver = b.time_to_deliver,
a.time_to_request_recollection = b.time_to_request_recollection, 
a.time_to_stockout_declared = b.time_to_stockout_declared,
a.time_to_createPO = b.time_to_createPO,
a.time_to_create_tracking_code = b.time_to_create_tracking_code, 
a.time_total_1st_attempt = b.time_total_1st_attempt, 
a.time_total_delivery = b.time_total_delivery,
#a.time_to_backlog = b.time_to_backlog, 
a.on_time_to_po = b.on_time_to_po,
a.on_time_to_confirm = b.on_time_to_confirm,
a.on_time_to_send_po = b.on_time_to_send_po, 
a.on_time_to_procure = b.on_time_to_procure, 
a.on_time_to_export = b.on_time_to_export, 
a.on_time_to_ready = b.on_time_to_ready, 
a.on_time_to_ship = b.on_time_to_ship, 
a.on_time_to_1st_attempt = b.on_time_to_1st_attempt,
a.on_time_to_deliver = b.on_time_to_deliver, 
a.on_time_to_request_recollection = b.on_time_to_request_recollection,
a.on_time_to_stockout_declared = b.on_time_to_stockout_declared, 
a.on_time_to_create_po = b.on_time_to_create_po, 
a.on_time_to_create_tracking_code = b.on_time_to_create_tracking_code, 
a.on_time_total_1st_attempt = b.on_time_total_1st_attempt, 
a.on_time_total_delivery = b.on_time_total_delivery, 
a.r2s_workday_0 = b.r2s_workday_0,
a.r2s_workday_1 = b.r2s_workday_1, 
a.r2s_workday_2 = b.r2s_workday_2, 
a.r2s_workday_3 = b.r2s_workday_3, 
a.r2s_workday_4 = b.r2s_workday_4, 
a.r2s_workday_5 = b.r2s_workday_5,
a.r2s_workday_6 = b.r2s_workday_6, 
a.r2s_workday_7 = b.r2s_workday_7, 
a.r2s_workday_8 = b.r2s_workday_8, 
a.r2s_workday_9 = b.r2s_workday_9, 
a.r2s_workday_10 = b.r2s_workday_10,
a.shipped_same_day_as_procured = b.shipped_same_day_as_procured, 
a.shipped_same_day_as_order = b.shipped_same_day_as_order, 
a.24hr_shipments_Cust_persp = b.24hr_shipments_Cust_persp,
a.24hr_shipmnets_WH_persp = b.24hr_shipmnets_WH_persp;

########## Queries Dates
SELECT  'Update effective_1st_attempt',now();
UPDATE operations_ve.out_order_tracking
SET effective_1st_attempt =1
WHERE date_delivered = date_1st_attempt;

SELECT  'Update early_to_procure',now();
UPDATE operations_ve.out_order_tracking_sample
SET out_order_tracking_sample.early_to_procure = CASE
WHEN (
CASE
                WHEN date_procured IS NULL
                THEN datediff(date_procured_promised,curdate())
ELSE datediff(date_procured_promised,date_procured)
END) > 0
THEN 1
ELSE 0
END,
out_order_tracking_sample.early_to_1st_attempt = CASE
WHEN (
CASE
  WHEN date_1st_attempt IS NULL
                THEN datediff(date_delivered_promised,curdate())
ELSE datediff(date_delivered_promised,date_1st_attempt)
END) > 0
THEN 1
ELSE 0
END;

SELECT  'Update wms_tracking_code',now();
UPDATE operations_ve.out_order_tracking_sample AS a
INNER JOIN wmsprod_ve.vw_itens_venda_entrega AS b
ON a.item_id = b.item_id
SET a.wms_tracking_code = b.cod_rastreamento;

#SELECT  'CALL bi_ops_tracking_code',now();
#CALL operations_ve.bi_ops_tracking_code;
#2.
#SELECT  'Update tms_tracks',now();
#UPDATE operations_ve.out_order_tracking_sample
#INNER JOIN operations_ve.tbl_bi_ops_guiasbyitem2
#	ON operations_ve.out_order_tracking_sample.item_id = operations_ve.tbl_bi_ops_guiasbyitem2.item_id
#SET operations_ve.out_order_tracking_sample.shipping_carrier_tracking_code = operations_ve.tbl_bi_ops_guiasbyitem2.total_guias;
/*
#### Insert para A_Master
INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  step,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Peru_Project',
  'out_order_tracking',
  'shipping_carrier_tracking_code',
  NOW(),
  max(datetime_ordered),
  count(*),
  count(item_counter)
FROM
  operations_ve.out_order_tracking;
*/

SELECT  'Update days_since_stock_updated',now();
UPDATE operations_ve.out_order_tracking_sample AS a
SET a.days_since_stock_updated =
IF(a.date_stock_updated IS NULL,NULL,TIMESTAMPDIFF(DAY, date(a.date_stock_updated), CURDATE()));

SELECT  'Update status_bob',now();
UPDATE operations_ve.out_order_tracking_sample AS a
INNER JOIN bob_live_ve.sales_order_item AS b
	ON a.item_id = b.id_sales_order_item
INNER JOIN bob_live_ve.sales_order_item_status AS c
	ON b.fk_sales_order_item_status = c.id_sales_order_item_status
SET a.status_bob = c. NAME;

SELECT  'Update status_match_bob_wms',now();
UPDATE operations_ve.out_order_tracking_sample
INNER JOIN operations_ve.status_match_bob_wms 
	ON out_order_tracking_sample.status_wms = status_match_bob_wms.status_wms
	AND out_order_tracking_sample.status_bob = status_match_bob_wms.status_bob
SET 
	out_order_tracking_sample.status_match_bob_wms = status_match_bob_wms.correct;

SELECT  'Update catalog_shipment_type',now();
UPDATE operations_ve.out_order_tracking_sample AS a
INNER JOIN bob_live_ve.sales_order_item AS b
	ON a.item_id = b.id_sales_order_item
INNER JOIN bob_live_ve.catalog_shipment_type AS c
	ON b.fk_catalog_shipment_type = c.id_catalog_shipment_type
SET a.fulfillment_type_bob = c.NAME;

SELECT  'Calculo wh_time',now();
UPDATE operations_ve.out_order_tracking_sample AS a
SET 
a.wh_time=if((a.package_measure_new='Small' OR a.package_measure_new='Medium'),1,2);

/*
version para MX
UPDATE operations_ve.out_order_tracking_sample
INNER JOIN wmsprod_ve.itens_venda ON out_order_tracking_sample.item_id = itens_venda.item_id
SET out_order_tracking_sample.wh_time = tempo_armazem
WHERE
tempo_armazem > 1;
*/

SELECT  'Update is_stockout',now();
UPDATE operations_ve.out_order_tracking_sample
SET
is_stockout = If(status_wms  IN('Quebrado','quebra tratada','backorder_tratada'),1,0),
is_backorder = if(date_backorder is not null,1,0),
is_ready_to_pick = If(date_ready_to_pick is not null,1,0),
is_ready_to_ship = If(date_ready_to_ship is not null,1,0),
is_procured = If(date_procured is not null,1,0),
is_shipped = If(date_shipped is not null,1,0),
is_first_attempt = If(date_1st_attempt is not null,1,0),
is_delivered = If(date_delivered is not null,1,0),
is_cancelled = If(status_wms in ('Cancelado','quebra tratada','Quebrado') is not null,1,0),
backlog_procurement = If((date_procured is null AND status_wms in ('Aguardando estoque','Analisando quebra') AND delay_to_procure=1 ),1,0),
#backlog_delivery = If((status_wms='Expedido' AND delay_total_delivery = 1 AND date_delivered is null),1,0),
still_to_procure = if(date_procured is null AND	status_wms not in ("quebrado", "quebra tratada") AND fulfillment_type_real = 'Crossdocking',1,0);

SELECT  'Update is_presale',now();
UPDATE operations_ve.out_order_tracking_sample a
INNER JOIN bob_live_ve.catalog_config b
	ON a.sku_config = b.sku
SET 
	a.is_presale = IF(b. NAME LIKE "%preventa%",1,0),
	a.is_gift_card = IF(a.supplier_name like '%E-Gift Card%',1,0);

#Workdays to procure
SELECT  'Update workdays_to_procure',now();
UPDATE operations_ve.out_order_tracking_sample
INNER JOIN operations_ve.calcworkdays
	ON out_order_tracking_sample.date_exported 			= calcworkdays.date_first
		AND out_order_tracking_sample.date_procured 	= calcworkdays.date_last
SET out_order_tracking_sample.workdays_to_procure 	=	calcworkdays.workdays
WHERE is_stockout = 0
AND fulfillment_type_real not like 'Dropshipping';

SELECT  'Update workdays_to_procure',now();
UPDATE operations_ve.out_order_tracking_sample
INNER JOIN operations_ve.calcworkdays
	ON out_order_tracking_sample.date_exported 			= calcworkdays.date_first
		AND curdate() 																= calcworkdays.date_last
SET out_order_tracking_sample.workdays_to_procure =	calcworkdays.workdays
WHERE is_stockout = 0
AND date_procured IS NULL
AND fulfillment_type_real not like 'Dropshipping';

SELECT  'Update workdays_to_procure',now();
UPDATE operations_ve.pro_order_tracking_dates a
INNER JOIN operations_ve.out_order_tracking_sample b
	ON a.item_id = b.item_id
SET a.workdays_to_procure = b.workdays_to_procure;

SELECT  'Update stockout_order_nr',now();
UPDATE operations_ve.out_order_tracking_sample AS a
INNER JOIN bob_live_ve.sales_order_item_status_history AS b
ON a.item_id=b.fk_sales_order_item
SET
a.stockout_order_nr = 
 (SUBSTR(b.note,(SELECT INSTR(b.note, '200')),9))
WHERE a.is_stockout=1
AND b.fk_sales_order_item_status=9;

SELECT  'Update delays',now();
UPDATE operations_ve.out_order_tracking_sample
SET 
	delay_to_procure = CASE
													WHEN date_procured IS NULL 
													THEN(	CASE
																	WHEN curdate() > date_procured_promised 
																	THEN 1
																	ELSE 0
																END)
													ELSE(	CASE
																	WHEN date_procured > date_procured_promised 
																	THEN 1
																	ELSE 0
																END)
												END,
 delay_to_ready = 	CASE
											WHEN workdays_to_ready > 1 
											THEN 1
											ELSE 0
										END,
 delay_to_ship = 	CASE
										WHEN workdays_to_ship > 1 
										THEN 1
										ELSE 0
									END,
 delay_to_1st_attempt = CASE
													WHEN workdays_to_1st_attempt > 2 
													THEN 1
													ELSE 0
												END,
 delay_to_delivery = CASE
											WHEN workdays_to_deliver > 2 
											THEN 1
											ELSE 0
										END,
 delay_total_1st_attempt = CASE
														WHEN date_1st_attempt IS NULL 
														THEN	(	CASE
																			WHEN curdate() > date_delivered_promised 
																			THEN 1
																			ELSE 0
																		END)
														ELSE	( CASE
																			WHEN date_1st_attempt > date_delivered_promised 
																			THEN 1
																			ELSE 0
																		END)
														END,
delay_total_delivery = CASE
	WHEN date_delivered IS NULL 
		THEN	(	CASE
					WHEN curdate() > date_delivered_promised 
						THEN 1
						ELSE 0
				END)
		ELSE	(	CASE
					WHEN date_delivered > date_delivered_promised 
						THEN 1
						ELSE 0
				END)
END,
 workdays_delay_to_procure = CASE
															WHEN workdays_to_procure - supplier_leadtime < 0 
															THEN 0
															ELSE workdays_to_procure - supplier_leadtime
														END,
 workdays_delay_to_ready = 	CASE
															WHEN workdays_to_ready - 1 < 0 
															THEN 0
															ELSE workdays_to_ready - 1
														END,
 workdays_delay_to_ship = CASE
														WHEN workdays_to_ship - 1 < 0 
														THEN 0
														ELSE workdays_to_ship - 1
													END,
 workdays_delay_to_1st_attempt = 	CASE
																		WHEN workdays_to_1st_attempt - 3 < 0 
																		THEN 0
																		ELSE workdays_to_1st_attempt - 3
																	END,
 workdays_delay_to_delivery = 	CASE
																WHEN workdays_to_deliver - 3 < 0 
																THEN 0
																ELSE workdays_to_deliver - 3
															END,
 on_time_to_procure = CASE
												WHEN date_procured <= date_procured_promised 
												THEN 1
												ELSE 0
											END,
 on_time_total_1st_attempt = 	CASE
																WHEN date_1st_attempt <= date_delivered_promised 
																THEN 1
																ELSE 0
															END,
 on_time_total_delivery = CASE
														WHEN date_delivered <= date_delivered_promised 
														THEN 1
														WHEN date_delivered IS NULL 
														THEN 0
														ELSE 0
													END;

SELECT  'Update backlog_delivery',now();
UPDATE operations_ve.out_order_tracking_sample
SET
backlog_delivery = If((status_wms='Expedido' AND delay_total_delivery = 1 AND date_delivered is null),1,0);

SELECT  'Update stockout_recovered',now();
UPDATE operations_ve.out_order_tracking_sample AS a
INNER JOIN development_ve.A_Master AS b
ON a.stockout_order_nr=b.OrderNum
SET
a.stockout_recovered = 1
WHERE (stockout_order_nr <> '')
and status_wms in ('Quebrado','quebra tratada')
AND b.OrderAfterCan=1;

SELECT  'Update stockout_real',now();
UPDATE operations_ve.out_order_tracking_sample AS a
SET
a.stockout_real = 1
WHERE a.is_stockout=1
AND a.stockout_recovered is null;

#UPDATE operations_ve.out_order_tracking_sample AS a
#INNER JOIN tbl_orders_reo_rep_inv AS b
#ON a.item_id=b.item_original
#SET
#a.stockout_recovered_voucher = b.coupon_code,
#a.stockout_item_id = b.item_nueva
#WHERE b.coupon_code like 'REO%';

SELECT  'Update shipping_carrier_tracking_code_inverse',now();
UPDATE operations_ve.out_order_tracking_sample AS a
INNER JOIN wmsprod_ve.inverselogistics_agendamiento AS b
	ON a.item_id=b.item_id
SET a.shipping_carrier_tracking_code_inverse = b.carrier_tracking_code;

# Cómo ligar PO de PE
/* Diseño Regional
UPDATE operations_ve.out_order_tracking_sample 
INNER JOIN procurement_live_ve.wms_imported_dropship_orders
	ON out_order_tracking_sample.item_id = wms_imported_dropship_orders.fk_sales_order_item
INNER JOIN procurement_live_ve.procurement_order_item_dropshipping 
	ON wms_imported_dropship_orders.id_wms_imported_dropship_order = procurement_order_item_dropshipping.fk_wms_imported_dropship_order
INNER JOIN procurement_live_ve.procurement_order_item
	ON procurement_order_item_dropshipping.fk_procurement_order_item = procurement_order_item.id_procurement_order_item
INNER JOIN procurement_live_ve.procurement_order
	ON procurement_order_item.fk_procurement_order = procurement_order.id_procurement_order
SET 
 out_order_tracking_sample.date_po_created = procurement_order.created_at,
 out_order_tracking_sample.date_po_issued = procurement_order.sent_at
WHERE operations_ve.out_order_tracking_sample.fulfillment_type_real = "dropshipping";*/

SELECT  'Update date_po_created',now();
UPDATE operations_ve.out_order_tracking_sample
INNER JOIN wmsprod_ve.itens_venda
	ON out_order_tracking_sample.item_id = itens_venda.item_id
INNER JOIN procurement_live_ve.wms_received_item
	ON itens_venda.itens_venda_id = wms_received_item.id_wms
INNER JOIN procurement_live_ve.procurement_order
	ON wms_received_item.fk_procurement_order = procurement_order.id_procurement_order
SET 
 out_order_tracking_sample.date_po_created = procurement_order.created_at,
 out_order_tracking_sample.date_po_issued = procurement_order.sent_at;

SELECT  'Update purchase_order',now();
UPDATE operations_ve.out_order_tracking_sample a
INNER JOIN wmsprod_ve.itens_venda b
ON a.item_id=b.item_id
INNER JOIN wmsprod_ve.estoque c
ON b.estoque_id=c.estoque_id
INNER JOIN wmsprod_ve.itens_recebimento d
ON c.itens_recebimento_id=d.itens_recebimento_id
INNER JOIN wmsprod_ve.recebimento e
ON d.recebimento_id=e.recebimento_id
SET a.purchase_order = e.inbound_document_identificator
WHERE e.inbound_document_type_id = 4;

SELECT  'Update delivery_fullfilment',now();
UPDATE operations_ve.out_order_tracking_sample a
INNER JOIN wmsprod_ve.entrega b
ON a.wms_tracking_code = b.cod_rastreamento
SET a.delivery_fullfilment = b.delivery_fulfillment;

#Fecha
SELECT  'Truncate pro_1st_attempt',now();
TRUNCATE	operations_ve.pro_1st_attempt;

SELECT  'Insert pro_1st_attempt',now();
INSERT INTO operations_ve.pro_1st_attempt (
	order_item_id, 
	min_of_date) 
SELECT
	out_order_tracking_sample.item_id,
	min(tms_status_delivery.date) AS min_of_date
FROM operations_ve.out_order_tracking_sample
INNER JOIN wmsprod_ve.vw_itens_venda_entrega 
	ON out_order_tracking_sample.item_id = vw_itens_venda_entrega.item_id
INNER JOIN wmsprod_ve.tms_status_delivery 
	ON vw_itens_venda_entrega.cod_rastreamento = tms_status_delivery.cod_rastreamento
GROUP BY
	out_order_tracking_sample.item_id,
	tms_status_delivery.id_status
HAVING
	tms_status_delivery.id_status = 5;

SELECT  'Update date_1st_attempt',now();
UPDATE operations_ve.out_order_tracking_sample
INNER JOIN operations_ve.pro_1st_attempt 
	ON out_order_tracking_sample.item_id = pro_1st_attempt.order_item_id
SET out_order_tracking_sample.date_1st_attempt = pro_1st_attempt.min_of_date;

SELECT  'Update date_1st_attempt',now();
UPDATE operations_ve.out_order_tracking_sample
INNER JOIN wmsprod_ve.vw_itens_venda_entrega vw 
	ON out_order_tracking_sample.item_id = vw.item_id
INNER JOIN wmsprod_ve.tms_status_delivery del 
	ON vw.cod_rastreamento = del.cod_rastreamento
SET out_order_tracking_sample.date_1st_attempt = (
	SELECT
		max(tms.date)
	FROM
		wmsprod_ve.tms_status_delivery tms,
		wmsprod_ve.vw_itens_venda_entrega itens
	WHERE
		del.cod_rastreamento = tms.cod_rastreamento
	AND vw.item_id = itens.item_id
	AND tms.id_status = 4
)
WHERE
#Fecha
	out_order_tracking_sample.date_1st_attempt IS NULL
OR out_order_tracking_sample.date_1st_attempt < '2011-05-01';

SELECT  'Update date_ready_to_pick',now();	
UPDATE operations_ve.out_order_tracking_sample
#Fecha
SET out_order_tracking_sample.date_ready_to_pick = 	CASE
	WHEN dayofweek(date_procured) = 1 
	THEN operations_ve.workday (date_procured, 1)
	WHEN dayofweek(date_procured) = 7 
	THEN operations_ve.workday (date_procured, 1)
	ELSE date_procured
END;

SELECT  'Update datetime_ready_to_pick',now();	
UPDATE operations_ve.out_order_tracking_sample
SET out_order_tracking_sample.datetime_ready_to_pick = 	CASE
	WHEN dayofweek(datetime_procured) = 1 
	THEN operations_ve.workday (datetime_procured, 1)
	WHEN dayofweek(datetime_procured) = 7 
	THEN operations_ve.workday (datetime_procured, 1)
	ELSE datetime_procured
END;

#Fecha pendientes 
SELECT  'Update ship_to_zip',now();	
UPDATE operations_ve.out_order_tracking_sample
INNER JOIN bob_live_ve.sales_order 
	ON out_order_tracking_sample.order_number = sales_order.order_nr
INNER JOIN bob_live_ve.sales_order_address 
	ON sales_order.fk_sales_order_address_shipping = sales_order_address.id_sales_order_address
SET 
	out_order_tracking_sample.ship_to_zip = postcode,
	out_order_tracking_sample.ship_to_zip2 = mid(postcode, 3, 2),
	out_order_tracking_sample.ship_to_zip3 = RIGHT (postcode, 3),
	out_order_tracking_sample.ship_to_zip4 = LEFT (postcode, 4);

# Pendiente agregar para CO
SELECT  'Update ship_to_zip2',now();	
UPDATE operations_ve.out_order_tracking_sample
SET ship_to_met_area = CASE
WHEN ship_to_zip BETWEEN 01000
AND 16999
OR ship_to_zip BETWEEN 53000
AND 53970
OR ship_to_zip BETWEEN 54000
AND 54198
OR ship_to_zip BETWEEN 54600
AND 54658
OR ship_to_zip BETWEEN 54700
AND 54769
OR ship_to_zip BETWEEN 54900
AND 54959
OR ship_to_zip BETWEEN 54960
AND 54990
OR ship_to_zip BETWEEN 52760
AND 52799
OR ship_to_zip BETWEEN 52900
AND 52998
OR ship_to_zip BETWEEN 55000
AND 55549
OR ship_to_zip BETWEEN 55700
AND 55739
OR ship_to_zip BETWEEN 57000
AND 57950 THEN
	1
ELSE
	0
END;

SELECT  'Update payment_method',now();	
UPDATE operations_ve.out_order_tracking_sample a
INNER JOIN wmsprod_ve.pedidos_venda b
ON a.order_number = b.numero_pedido
SET 
a.payment_method = b.metodo_de_pagamento,
a.ship_to_region = b.estado_cliente,
a.ship_to_city = b.cidade_cliente;

SELECT  'Update calendar',now();	
UPDATE operations_ve.out_order_tracking_sample AS a
INNER JOIN operations_ve.calendar AS b
ON DATE(a.date_shipped)=b.dt
AND b.isweekend=1
SET a.shipped_is_weekend=1;

SELECT  'Update note_tracking',now();	
UPDATE operations_ve.out_order_tracking_sample AS a
LEFT JOIN wmsprod_ve.itens_venda AS b
ON a.item_id=b.item_id
SET
a.note_tracking = b.obs;

#UPDATE operations_ve.out_order_tracking_sample AS a
#SET
#a.tracking_stockout= 
#CASE 
#   WHEN INSTR(note_tracking,'OOS')!=0 THEN "OOS" 
#   WHEN INSTR(note_tracking,'OOT')!=0 THEN "OOT" 
#   WHEN INSTR(note_tracking,'PNP')!=0 THEN "PNP" 
#   WHEN INSTR(note_tracking,'DSE')!=0 THEN "DSE" 
#   WHEN INSTR(note_tracking,'POL')!=0 THEN "POL"  
#   WHEN INSTR(note_tracking,'DTE')!=0 THEN "DTE"  
#   WHEN INSTR(note_tracking,'SOE')!=0 THEN "SOE"  
#   WHEN INSTR(note_tracking,'CBC')!=0 THEN "CBC"  
#   WHEN INSTR(note_tracking,'DTL')!=0 THEN "DTL"
#   WHEN INSTR(note_tracking,'OSW')!=0 THEN "OSW"
#   WHEN INSTR(note_tracking,'UNS')!=0 THEN "UNS"
#   WHEN INSTR(note_tracking,'OSM')!=0 THEN "OSM"
#   WHEN INSTR(note_tracking,'SWC')!=0 THEN "SWC"
#   WHEN INSTR(note_tracking,'OOA')!=0 THEN "OOA"
#   WHEN INSTR(note_tracking,'IWL')!=0 THEN "IWL"
#   WHEN INSTR(note_tracking,'ETR')!=0 THEN "ETR"
#   WHEN INSTR(note_tracking,'CSW')!=0 THEN "CSW"
#   WHEN INSTR(note_tracking,'STE')!=0 THEN "STE"
#   ELSE "NA"
#END;

SELECT  'Update is_quebra_tratada',now();
UPDATE operations_ve.out_order_tracking_sample AS d
SET d.is_quebra_tratada = 1
WHERE d.date_declared_stockout IS NOT NULL
AND d.last_quebra_tratada_date IS NOT NULL
AND d.date_declared_stockout < d.last_quebra_tratada_date;

SELECT  'Update dias_habiles_negativos',now();
UPDATE operations_ve.out_order_tracking_sample
SET sac_backlog = dias_habiles_negativos(curdate(), date_declared_stockout)
WHERE last_quebra_tratada_date IS NULL
AND date_declared_stockout IS NOT NULL;

SELECT  'Update min_delivery_date',now();
UPDATE operations_ve.out_order_tracking_sample AS a
INNER JOIN calcworkdays AS b
ON date(a.date_exported) = b.date_last 
 AND b.workdays =a.min_delivery_time
 AND b.isweekday = 1 
    AND b.isholiday = 0
SET 
 a.min_delivery_date = b.date_first ;

SELECT  'Drop tmp_last_quebra_tratada_date',now();
DROP TEMPORARY TABLE IF EXISTS operations_ve.tmp_last_quebra_tratada_date;
 
SELECT  'Create tmp_last_quebra_tratada_date',now();
CREATE TEMPORARY TABLE operations_ve.tmp_last_quebra_tratada_date (INDEX (item_id))
SELECT
 a.itens_venda_id,
  a.numero_order,
  a.item_id,
  b.date
FROM
  wmsprod_ve.itens_venda a
INNER JOIN ( SELECT
               itens_venda_id,
               max(DATA) date
              FROM
               wmsprod_ve.status_itens_venda
              WHERE
               STATUS = 'quebra tratada'
              GROUP BY
               itens_venda_id) b
 ON a.itens_venda_id = b.itens_venda_id
INNER JOIN operations_ve.pro_order_tracking_dates c
 ON a.item_id = c.item_id;
 
SELECT  'Update last_quebra_tratada_date',now();
UPDATE operations_ve.out_order_tracking_sample AS d
INNER JOIN operations_ve.tmp_last_quebra_tratada_date t
ON d.item_id = t.item_id
SET d.last_quebra_tratada_date = t.date;

SELECT  'Update customer_backlog',now();
UPDATE operations_ve.out_order_tracking_sample
SET customer_backlog = dias_habiles_negativos(curdate(),date_delivered_promised)
WHERE last_quebra_tratada_date IS NULL
AND date_declared_stockout IS NOT NULL;

SELECT  'Update procured_promised_last_30',now();
UPDATE operations_ve.out_order_tracking_sample AS a
SET a.procured_promised_last_30 = CASE
WHEN datediff(curdate(),date_procured_promised) <= 30
AND datediff(curdate(),date_procured_promised) > 0 
THEN 1
ELSE 0
END,
a.shipped_last_30 = CASE
WHEN datediff(curdate(), date_shipped) <= 30
AND datediff(curdate(), date_shipped) > 0 
THEN 1
ELSE 0
END,
a.delivered_promised_last_30 = CASE
WHEN datediff(curdate(),date_delivered_promised) <= 30
AND datediff(curdate(),date_delivered_promised) > 0 
THEN 1
ELSE 0
END;

SELECT  'Update quebra_sugerido',now();
UPDATE operations_ve.out_order_tracking_sample AS a
SET
a.quebra_sugerido = IF(a.note_tracking regexp 'LACOL|SUGERIR', 'SI', 'NO')
WHERE a.date_declared_stockout IS NOT NULL;

SELECT  'Update quebra_sugerido',now();
UPDATE operations_ve.out_order_tracking_sample AS a
SET
a.quebra_sugerido = IF(a.note_tracking NOT regexp 'LACOL' = 1, 'NO', 'SI')
WHERE a.date_declared_stockout IS NOT NULL;

/*
SELECT  'A_Master',now();
CALL production.monitoring( "A_Master" , "Colombia_Project",  240 );
*/
SELECT  'Update A_Master',now();
UPDATE operations_ve.out_order_tracking_sample AS a
INNER JOIN development_ve.A_Master AS b
ON a.item_id=b.ItemID
SET
a.tax_percent = b.Tax,
a.cost = b.Cost,
a.cost_w_o_vat = b.CostAfterTax,
a.price = b.Price,
a.price_w_o_vat = b.PriceAfterTax,
a.is_marketplace = b.isMPlace,
a.corporate_sale = if(b.CouponCode LIKE 'VC%',1,0),
a.coupon_value = if(b.CouponCode like 'REP%',b.CouponValue,0), 
a.customer_first_name = b.FirstName,
a.customer_last_name = b.LastName,
a.payment_method = b.PaymentMethod;

# num_items_per_order
SELECT  'Truncate pro_order_tracking_num_items_per_order',now();
TRUNCATE  operations_ve.pro_order_tracking_num_items_per_order;

SELECT  'Insert pro_order_tracking_num_items_per_order',now();
INSERT INTO operations_ve.pro_order_tracking_num_items_per_order (
order_number,
count_of_order_item_id
) SELECT
out_order_tracking_sample.order_number,
count(*) AS countoforder_item_id
FROM
operations_ve.out_order_tracking_sample
GROUP BY
out_order_tracking_sample.order_number;

SELECT  'Update num_items_per_order',now();
UPDATE operations_ve.out_order_tracking_sample
INNER JOIN operations_ve.pro_order_tracking_num_items_per_order
                ON out_order_tracking_sample.order_number = pro_order_tracking_num_items_per_order.order_number
SET out_order_tracking_sample.num_items_per_order = 1 / pro_order_tracking_num_items_per_order.count_of_order_item_id;

SELECT  'Update item_counter',now();
UPDATE operations_ve.out_order_tracking_sample
SET item_counter = 1;

##### Termina Rutina #####

SELECT  'Replace out_order_tracking',now();
REPLACE operations_ve.out_order_tracking SELECT * FROM operations_ve.out_order_tracking_sample;

SELECT  'Replace pro_backlog_history',now();
REPLACE operations_ve.pro_backlog_history
SELECT
 curdate() as Date_reported,
 SUM( IF(     is_presale = 0
          AND status_wms IN ('aguardando estoque','analisando quebra')
                                      , delay_to_procure     , 0 ) ) as procurement_backlog,
 SUM( IF(     date_delivered IS NULL
          AND date_delivered_promised < curdate()
          AND status_wms = 'expedido' , delay_total_delivery , 0 ) ) as deliveries_backlog
FROM
 operations_ve.out_order_tracking_sample
WHERE 
(
         is_presale = 0
    AND status_wms IN ('aguardando estoque','analisando quebra')
)
OR 
(
        date_delivered IS NULL
    AND date_delivered_promised < curdate()
    AND status_wms = 'expedido'
);

### Status Match
SELECT  'Drop status_match_dates',now();
DROP TABLE IF EXISTS operations_ve.status_match_dates;

SELECT  'Create status_match_dates',now();
CREATE TABLE operations_ve.status_match_dates
SELECT
 a.item_id,
 a.status_bob,
 a.status_wms,
 a.status_match_bob_wms,
 a.status_value_chain,
 max(b.updated_at) AS bob_updated_at,
 max(c.data_ultima_alteracao) AS wms_updated_at,
 DATEDIFF(curdate(),max(b.updated_at)) AS days_since_last_bob_status,
 DATEDIFF(curdate(),max(c.data_ultima_alteracao)) AS days_since_last_wms_status,
 a.check_dates,
 a.check_date_ordered,
 a.check_date_exported,
 a.check_date_ready_to_ship,
 a.check_date_shipped,
 a.check_date_1st_attempt,
 a.payment_method,
 a.shipping_carrier
FROM operations_ve.out_order_tracking a
INNER JOIN bob_live_ve.sales_order_item b
	ON a.item_id = b.id_sales_order_item
INNER JOIN wmsprod_ve.itens_venda c
	ON a.item_id = c.item_id
GROUP BY a.item_id;

DROP TABLE IF EXISTS production_ve.out_order_tracking;

CREATE TABLE production_ve.out_order_tracking LIKE operations_ve.out_order_tracking; 

INSERT INTO production_ve.out_order_tracking SELECT * FROM operations_ve.out_order_tracking; 

SELECT  'End operations_ve.out_order_tracking',now();

INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check) 
SELECT 
  'Peru Project', 
  'out_order_tracking',
  'finish',
  NOW(),
  max(datetime_ordered),
  count(*),
  count(item_counter)
FROM
  operations_ve.out_order_tracking;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 PROCEDURE `out_order_tracking_backup`()
    MODIFIES SQL DATA
BEGIN

INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Venezuela', 
  'out_order_tracking',
  'start',
  NOW(),
  max(date_time_ordered),
  count(*),
  count(item_counter)
FROM
  production_ve.out_order_tracking;

CALL operations_ve.calcworkdays;

TRUNCATE operations_ve.out_order_tracking;


INSERT INTO operations_ve.out_order_tracking (
	order_item_id,
	order_number,
	sku_simple
) SELECT
	itens_venda.item_id,
	itens_venda.numero_order,
	itens_venda.sku
FROM
	wmsprod_ve.itens_venda
#WHERE DATE_FORMAT(itens_venda.data_criacao,'%Y%m') >= (DATE_FORMAT(curdate()- INTERVAL 4 MONTH,'%Y%m')) 
;


UPDATE (
	(
		bob_live_ve.sales_order_address
		RIGHT JOIN (
			operations_ve.out_order_tracking
			INNER JOIN bob_live_ve.sales_order ON out_order_tracking.order_number = sales_order.order_nr
		) ON sales_order_address.id_sales_order_address = sales_order.fk_sales_order_address_billing
	)
	LEFT JOIN bob_live_ve.customer_address_region ON sales_order_address.fk_customer_address_region = customer_address_region.id_customer_address_region
)
INNER JOIN bob_live_ve.sales_order_item ON out_order_tracking.order_item_id = sales_order_item.id_sales_order_item
SET out_order_tracking.date_ordered = date(sales_order.created_at),
 date_time_ordered = sales_order.created_at,
 out_order_tracking.payment_method = sales_order.payment_method,
 out_order_tracking.ship_to_state = customer_address_region. CODE,
 out_order_tracking.min_delivery_time = sales_order_item.min_delivery_time,
 out_order_tracking.max_delivery_time = sales_order_item.max_delivery_time,
 out_order_tracking.supplier_leadtime = sales_order_item.delivery_time_supplier,
 out_order_tracking.tax_percent = sales_order_item.tax_percent,
 out_order_tracking.price = sales_order_item.unit_price,
 out_order_tracking.cost = sales_order_item.cost
 ;


UPDATE operations_ve.out_order_tracking
INNER JOIN bob_live_ve.catalog_simple ON out_order_tracking.sku_simple = catalog_simple.sku
INNER JOIN bob_live_ve.catalog_config ON catalog_simple.fk_catalog_config = catalog_config.id_catalog_config
INNER JOIN bob_live_ve.supplier ON catalog_config.fk_supplier = supplier.id_supplier
SET out_order_tracking.sku_config = catalog_config.sku,
 out_order_tracking.sku_name = catalog_config. NAME,
 out_order_tracking.supplier_id = supplier.id_supplier,
 out_order_tracking.supplier_name = supplier. NAME,
 out_order_tracking.min_delivery_time = CASE
WHEN out_order_tracking.min_delivery_time IS NULL THEN
	catalog_simple.min_delivery_time
ELSE
	out_order_tracking.min_delivery_time
END,
 out_order_tracking.max_delivery_time = CASE
WHEN out_order_tracking.max_delivery_time IS NULL THEN
	catalog_simple.max_delivery_time
ELSE
	out_order_tracking.max_delivery_time
END,
 out_order_tracking.package_height = catalog_config.package_height,
 out_order_tracking.package_length = catalog_config.package_length,
 out_order_tracking.package_width = catalog_config.package_width,
 out_order_tracking.package_weight = catalog_config.package_weight;


UPDATE production_ve.out_order_tracking
INNER JOIN bob_live_ve.catalog_simple ON out_order_tracking.sku_simple = catalog_simple.sku
INNER JOIN bob_live_ve.catalog_config ON catalog_simple.fk_catalog_config = catalog_config.id_catalog_config
SET out_order_tracking.supplier_leadtime = CASE
WHEN out_order_tracking.supplier_leadtime IS NULL THEN
	0
ELSE
	out_order_tracking.supplier_leadtime
END;


UPDATE operations_ve.out_order_tracking
INNER JOIN wmsprod_ve.itens_venda
	ON out_order_tracking.order_item_id = itens_venda.item_id
INNER JOIN wmsprod_ve.status_itens_venda 
	ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
SET out_order_tracking.fullfilment_type_real = "Inventory"
WHERE
	status_itens_venda. STATUS = "Estoque reservado";

UPDATE (
	operations_ve.out_order_tracking
	INNER JOIN wmsprod_ve.itens_venda ON out_order_tracking.order_item_id = itens_venda.item_id
)
INNER JOIN wmsprod_ve.status_itens_venda ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
SET out_order_tracking.fullfilment_type_real = "dropshipping"
WHERE
	status_itens_venda. STATUS IN (
		"DS estoque reservado",
		'dropshipping notified',
		'Waiting dropshipping',
		'Electronic good');

UPDATE operations_ve.out_order_tracking
INNER JOIN (
	wmsprod_ve.itens_venda
	INNER JOIN wmsprod_ve.status_itens_venda ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
) ON out_order_tracking.order_item_id = itens_venda.item_id
SET out_order_tracking.fullfilment_type_real = "crossdock"
WHERE
	status_itens_venda. STATUS = "analisando quebra"
OR status_itens_venda. STATUS = "aguardando estoque";



DELETE operations_ve.pro_min_date_exported.*
FROM
	operations_ve.pro_min_date_exported;


INSERT INTO operations_ve.pro_min_date_exported (
	fk_sales_order_item,
	NAME,
	min_of_created_at
) SELECT
	sales_order_item_status_history.fk_sales_order_item,
	sales_order_item_status. NAME,
	min(
		sales_order_item_status_history.created_at
	) AS min_of_created_at
FROM
	bob_live_ve.sales_order_item_status
INNER JOIN bob_live_ve.sales_order_item_status_history ON sales_order_item_status.id_sales_order_item_status = sales_order_item_status_history.fk_sales_order_item_status
GROUP BY
	sales_order_item_status_history.fk_sales_order_item,
	sales_order_item_status. NAME
HAVING sales_order_item_status. NAME IN( 	"exported", 
																					"exportable",
																					"exported electronically")
;



UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.pro_min_date_exported ON out_order_tracking.order_item_id = pro_min_date_exported.fk_sales_order_item
SET out_order_tracking.date_exported = date(min_of_created_at),
 out_order_tracking.date_time_exported = min_of_created_at;


UPDATE operations_ve.out_order_tracking
INNER JOIN wmsprod_ve.itens_venda ON out_order_tracking.order_item_id = itens_venda.item_id
INNER JOIN wmsprod_ve.status_itens_venda ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
SET out_order_tracking.date_procured = date(DATA),
 out_order_tracking.date_time_procured = DATA
WHERE
	status_itens_venda. STATUS = "estoque reservado";


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.fullfilment_type_real = "crossdock"
WHERE
	out_order_tracking.fullfilment_type_real = "dropshipping"
AND date_procured IS NOT NULL;

UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_ready_to_pick = CASE
WHEN dayofweek(date_procured) = 1 THEN
	operations_ve.workday (date_procured, 1)
ELSE
	(
		CASE
		WHEN dayofweek(date_procured) = 7 THEN
			operations_ve.workday (date_procured, 1)
		ELSE
			date_procured
		END
	)
END;

UPDATE 
 operations_ve.out_order_tracking
INNER JOIN procurement_live_ve.procurement_order_item 
	ON out_order_tracking.order_item_id = procurement_order_item.fk_sales_order_item
INNER JOIN procurement_live_ve.procurement_order
	ON procurement_order_item.fk_procurement_order = procurement_order.id_procurement_order
SET 
 out_order_tracking.date_procurement_order = procurement_order.created_at;

UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_time_ready_to_pick = CASE
WHEN dayofweek(date_time_procured) = 1 THEN
	operations_ve.workday (date_time_procured, 1)
ELSE
	(
		CASE
		WHEN dayofweek(date_time_procured) = 7 THEN
			operations_ve.workday (date_time_procured, 1)
		ELSE
			date_procured
		END
	)
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_ready_to_pick = CASE
WHEN date_ready_to_pick IN (
	'2012-11-19',
	'2012-12-25',
	'2013-1-1',
	'2013-2-4',
	'2013-3-18',
	'2013-05-01',
  '2013-12-25',
  '2014-01-01'

) THEN
	operations_ve.workday (date_ready_to_pick, 1)
WHEN date_ready_to_pick IN ('2013-3-28',
														'2013-12-24','2013-12-31')
THEN
	operations_ve.workday (date_ready_to_pick, 2)
ELSE
	date_ready_to_pick
END,
 out_order_tracking.date_time_ready_to_pick = CASE
WHEN date_ready_to_pick IN (
	'2012-11-19',
	'2012-12-25',
	'2013-1-1',
	'2013-2-4',
	'2013-3-18',
	'2013-05-01',
  '2013-12-25',
  '2014-01-01'
) THEN
	operations_ve.workday (date_time_ready_to_pick, 1)
WHEN date_ready_to_pick IN ('2013-3-28',
														'2013-12-24','2013-12-31')
THEN
	operations_ve.workday (date_time_ready_to_pick, 2)
ELSE
	date_time_ready_to_pick
END;


DELETE operations_ve.pro_max_date_ready_to_ship.*
FROM
	operations_ve.pro_max_date_ready_to_ship;


INSERT INTO operations_ve.pro_max_date_ready_to_ship (date_ready, order_item_id) SELECT
	min(DATA) AS date_ready,
	out_order_tracking.order_item_id
FROM
	operations_ve.out_order_tracking
INNER JOIN wmsprod_ve.itens_venda ON out_order_tracking.order_item_id = itens_venda.item_id
INNER JOIN wmsprod_ve.status_itens_venda ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
WHERE
	status_itens_venda. STATUS = "faturado"
GROUP BY
	out_order_tracking.order_item_id;


UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.pro_max_date_ready_to_ship ON out_order_tracking.order_item_id = pro_max_date_ready_to_ship.order_item_id
SET 
 out_order_tracking.date_ready_to_ship = date(pro_max_date_ready_to_ship.date_ready),
 out_order_tracking.date_time_ready_to_ship = pro_max_date_ready_to_ship.date_ready;


UPDATE operations_ve.out_order_tracking
INNER JOIN (
	wmsprod_ve.itens_venda b
	INNER JOIN wmsprod_ve.status_itens_venda d ON b.itens_venda_id = d.itens_venda_id
) ON out_order_tracking.order_item_id = b.item_id
SET out_order_tracking.date_shipped = (
	SELECT
		max(date(DATA))
	FROM
		wmsprod_ve.itens_venda c,
		wmsprod_ve.status_itens_venda e
	WHERE
		e. STATUS = "expedido"
	AND b.itens_venda_id = c.itens_venda_id
	AND e.itens_venda_id = d.itens_venda_id
),
 out_order_tracking.date_time_shipped = (
	SELECT
		max(DATA)
	FROM
		wmsprod_ve.itens_venda c,
		wmsprod_ve.status_itens_venda e
	WHERE
		e. STATUS = "expedido"
	AND b.itens_venda_id = c.itens_venda_id
	AND e.itens_venda_id = d.itens_venda_id
);


UPDATE operations_ve.out_order_tracking
INNER JOIN wmsprod_ve.vw_itens_venda_entrega vw ON out_order_tracking.order_item_id = vw.item_id
INNER JOIN wmsprod_ve.tms_status_delivery del ON vw.cod_rastreamento = del.cod_rastreamento
SET out_order_tracking.date_delivered = (
	SELECT
		CASE
	WHEN tms.date < '2012-05-01' THEN
		NULL
	ELSE
		max(tms.date)
	END
	FROM
		wmsprod_ve.vw_itens_venda_entrega itens,
		wmsprod_ve.tms_status_delivery tms
	WHERE
		itens.cod_rastreamento = vw.cod_rastreamento
	AND tms.cod_rastreamento = del.cod_rastreamento
	AND tms.id_status = 4
);

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  updated_at
  )
SELECT 
  'Venezuela', 
  'out_order_tracking.date_delivered',
  NOW()
;

UPDATE operations_ve.out_order_tracking
INNER JOIN wmsprod_ve.itens_venda ON out_order_tracking.order_item_id = itens_venda.item_id
SET out_order_tracking.wh_time = tempo_armazem
WHERE
	tempo_armazem > 1;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_procured_promised = operations_ve.workday (
	date_exported,
	supplier_leadtime
),
 out_order_tracking.date_delivered_promised = operations_ve.workday (
	date_exported,
	max_delivery_time
)
WHERE
	out_order_tracking.date_exported IS NOT NULL;


UPDATE bob_live_ve.sales_order_item
INNER JOIN operations_ve.out_order_tracking ON sales_order_item.id_sales_order_item = out_order_tracking.order_item_id
SET out_order_tracking.is_linio_promise = sales_order_item.is_linio_promise;


UPDATE operations_ve.out_order_tracking
SET is_linio_promise = 0
WHERE
	is_linio_promise IS NULL;


UPDATE operations_ve.out_order_tracking
SET date_delivered_promised = operations_ve.workday (date_exported, 2)
WHERE
	is_linio_promise = 1;

UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_ready_to_ship_promised = operations_ve.workday (
	date_procured_promised,
	wh_time
);

UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_procured_promised = operations_ve.workday (
	out_order_tracking.date_procured_promised,
	1
)
WHERE
	out_order_tracking.date_procured_promised >= '2013-1-1'
AND out_order_tracking.date_exported < '2013-1-1';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_delivered_promised = operations_ve.workday (
	out_order_tracking.date_delivered_promised,
	1
)
WHERE
	out_order_tracking.date_delivered_promised >= '2013-1-1'
AND out_order_tracking.date_exported < '2013-1-1';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_ready_to_ship_promised = operations_ve.workday (
	out_order_tracking.date_ready_to_ship_promised,
	1
)
WHERE
	out_order_tracking.date_ready_to_ship_promised >= '2013-1-1'
AND out_order_tracking.date_exported < '2013-1-1';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_procured_promised = operations_ve.workday (
	out_order_tracking.date_procured_promised,
	1
)
WHERE
	out_order_tracking.date_procured_promised >= '2013-2-11'
AND out_order_tracking.date_exported < '2013-2-11';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_delivered_promised = operations_ve.workday (
	out_order_tracking.date_delivered_promised,
	1
)
WHERE
	out_order_tracking.date_delivered_promised >= '2013-2-11'
AND out_order_tracking.date_exported < '2013-2-11';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_ready_to_ship_promised = operations_ve.workday (
	out_order_tracking.date_ready_to_ship_promised,
	1
)
WHERE
	out_order_tracking.date_ready_to_ship_promised >= '2013-2-11'
AND out_order_tracking.date_exported < '2013-2-11';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_procured_promised = operations_ve.workday (
	out_order_tracking.date_procured_promised,
	1
)
WHERE
	out_order_tracking.date_procured_promised >= '2013-2-12'
AND out_order_tracking.date_exported < '2013-2-12';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_delivered_promised = operations_ve.workday (
	out_order_tracking.date_delivered_promised,
	1
)
WHERE
	out_order_tracking.date_delivered_promised >= '2013-2-12'
AND out_order_tracking.date_exported < '2013-2-12';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_ready_to_ship_promised = operations_ve.workday (
	out_order_tracking.date_ready_to_ship_promised,
	1
)
WHERE
	out_order_tracking.date_ready_to_ship_promised >= '2013-2-12'
AND out_order_tracking.date_exported < '2013-2-12';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_procured_promised = operations_ve.workday (
	out_order_tracking.date_procured_promised,
	1
)
WHERE
	out_order_tracking.date_procured_promised >= '2013-03-28'
AND out_order_tracking.date_exported < '2013-03-28';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_delivered_promised = operations_ve.workday (
	out_order_tracking.date_delivered_promised,
	1
)
WHERE
	out_order_tracking.date_delivered_promised >= '2013-03-28'
AND out_order_tracking.date_exported < '2013-03-28';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_ready_to_ship_promised = operations_ve.workday (
	out_order_tracking.date_ready_to_ship_promised,
	1
)
WHERE
	out_order_tracking.date_ready_to_ship_promised >= '2013-03-28'
AND out_order_tracking.date_exported < '2013-03-28';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_procured_promised = operations_ve.workday (
	out_order_tracking.date_procured_promised,
	1
)
WHERE
	out_order_tracking.date_procured_promised >= '2013-3-29'
AND out_order_tracking.date_exported < '2013-3-29';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_delivered_promised = operations_ve.workday (
	out_order_tracking.date_delivered_promised,
	1
)
WHERE
	out_order_tracking.date_delivered_promised >= '2013-3-29'
AND out_order_tracking.date_exported < '2013-3-29';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_ready_to_ship_promised = operations_ve.workday (
	out_order_tracking.date_ready_to_ship_promised,
	1
)
WHERE
	out_order_tracking.date_ready_to_ship_promised >= '2013-3-29'
AND out_order_tracking.date_exported < '2013-3-29';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_procured_promised = operations_ve.workday (
	out_order_tracking.date_procured_promised,
	1
)
WHERE
	out_order_tracking.date_procured_promised >= '2013-04-19'
AND out_order_tracking.date_exported < '2013-04-19';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_delivered_promised = operations_ve.workday (
	out_order_tracking.date_delivered_promised,
	1
)
WHERE
	out_order_tracking.date_delivered_promised >= '2013-04-19'
AND out_order_tracking.date_exported < '2013-04-19';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_ready_to_ship_promised = operations_ve.workday (
	out_order_tracking.date_ready_to_ship_promised,
	1
)
WHERE
	out_order_tracking.date_ready_to_ship_promised >= '2013-04-19'
AND out_order_tracking.date_exported < '2013-04-19';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_procured_promised = operations_ve.workday (
	out_order_tracking.date_procured_promised,
	1
)
WHERE
	out_order_tracking.date_procured_promised >= '2013-05-01'
AND out_order_tracking.date_exported < '2013-05-01';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_delivered_promised = operations_ve.workday (
	out_order_tracking.date_delivered_promised,
	1
)
WHERE
	out_order_tracking.date_delivered_promised >= '2013-05-01'
AND out_order_tracking.date_exported < '2013-05-01';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_ready_to_ship_promised = operations_ve.workday (
	out_order_tracking.date_ready_to_ship_promised,
	1
)
WHERE
	out_order_tracking.date_ready_to_ship_promised >= '2013-05-01'
AND out_order_tracking.date_exported < '2013-05-01';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_procured_promised = operations_ve.workday (
	out_order_tracking.date_procured_promised,
	1
)
WHERE
	out_order_tracking.date_procured_promised >= '2013-06-24'
AND out_order_tracking.date_exported < '2013-06-24';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_delivered_promised = operations_ve.workday (
	out_order_tracking.date_delivered_promised,
	1
)
WHERE
	out_order_tracking.date_delivered_promised >= '2013-06-24'
AND out_order_tracking.date_exported < '2013-06-24';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_ready_to_ship_promised = operations_ve.workday (
	out_order_tracking.date_ready_to_ship_promised,
	1
)
WHERE
	out_order_tracking.date_ready_to_ship_promised >= '2013-06-24'
AND out_order_tracking.date_exported < '2013-06-24';




UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_ready_to_ship_promised = operations_ve.workday (
	out_order_tracking.date_ready_to_ship_promised,
	1
)
WHERE
	(
		(
			(
				out_order_tracking.date_ready_to_ship_promised
			) >= '2013-01-01'
		)
		AND (
			(
				out_order_tracking.date_procured_promised
			) < '2013-01-01'
		)
	);


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_ready_to_ship_promised = operations_ve.workday (
	out_order_tracking.date_ready_to_ship_promised,
	1
)
WHERE
	(
		(
			(
				out_order_tracking.date_ready_to_ship_promised
			) >= '2013-02-11'
		)
		AND (
			(
				out_order_tracking.date_procured_promised
			) < '2013-02-11'
		)
	);


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_ready_to_ship_promised = operations_ve.workday (
	out_order_tracking.date_ready_to_ship_promised,
	1
)
WHERE
	(
		(
			(
				out_order_tracking.date_ready_to_ship_promised
			) >= '2013-02-12'
		)
		AND (
			(
				out_order_tracking.date_procured_promised
			) < '2013-02-12'
		)
	);


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_ready_to_ship_promised = operations_ve.workday (
	out_order_tracking.date_ready_to_ship_promised,
	1
)
WHERE
	(
		(
			(
				out_order_tracking.date_ready_to_ship_promised
			) >= '2013-03-28'
		)
		AND (
			(
				out_order_tracking.date_procured_promised
			) < '2013-03-28'
		)
	);


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_ready_to_ship_promised = operations_ve.workday (
	out_order_tracking.date_ready_to_ship_promised,
	1
)
WHERE
	(
		(
			(
				out_order_tracking.date_ready_to_ship_promised
			) >= '2013-03-29'
		)
		AND (
			(
				out_order_tracking.date_procured_promised
			) < '2013-03-29'
		)
	);


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_ready_to_ship_promised = operations_ve.workday (
	out_order_tracking.date_ready_to_ship_promised,
	1
)
WHERE
	(
		(
			(
				out_order_tracking.date_ready_to_ship_promised
			) >= '2013-04-19'
		)
		AND (
			(
				out_order_tracking.date_procured_promised
			) < '2013-04-19'
		)
	);


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_ready_to_ship_promised = operations_ve.workday (
	out_order_tracking.date_ready_to_ship_promised,
	1
)
WHERE
	(
		(
			(
				out_order_tracking.date_ready_to_ship_promised
			) >= '2013-05-01'
		)
		AND (
			(
				out_order_tracking.date_procured_promised
			) < '2013-05-01'
		)
	);


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_ready_to_ship_promised = operations_ve.workday (
	out_order_tracking.date_ready_to_ship_promised,
	1
)
WHERE
	(
		(
			(
				out_order_tracking.date_ready_to_ship_promised
			) >= '2013-06-24'
		)
		AND (
			(
				out_order_tracking.date_procured_promised
			) < '2013-06-24'
		)
	);

UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_procured_promised = operations_ve.workday (out_order_tracking.date_procured_promised,2)
WHERE
	out_order_tracking.date_procured_promised >= '2013-12-24'
AND out_order_tracking.date_exported < '2013-12-24';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_delivered_promised = operations_ve.workday (out_order_tracking.date_delivered_promised,2)
WHERE
	out_order_tracking.date_delivered_promised >= '2013-12-24'
AND out_order_tracking.date_exported < '2013-12-24';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_ready_to_ship_promised = operations_ve.workday (out_order_tracking.date_ready_to_ship_promised,2)
WHERE
	out_order_tracking.date_ready_to_ship_promised >= '2013-12-24'
AND out_order_tracking.date_exported < '2013-12-24';

UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_procured_promised = operations_ve.workday (out_order_tracking.date_procured_promised,1)
WHERE
	out_order_tracking.date_procured_promised >= '2013-12-25'
AND out_order_tracking.date_exported < '2013-12-25';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_delivered_promised = operations_ve.workday (out_order_tracking.date_delivered_promised,1)
WHERE
	out_order_tracking.date_delivered_promised >= '2013-12-25'
AND out_order_tracking.date_exported < '2013-12-25';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_ready_to_ship_promised = operations_ve.workday (out_order_tracking.date_ready_to_ship_promised,1)
WHERE
	out_order_tracking.date_ready_to_ship_promised >= '2013-12-25'
AND out_order_tracking.date_exported < '2013-12-25';

UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_procured_promised = operations_ve.workday (out_order_tracking.date_procured_promised,2)
WHERE
	out_order_tracking.date_procured_promised >= '2013-12-31'
AND out_order_tracking.date_exported < '2013-12-31';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_delivered_promised = operations_ve.workday (out_order_tracking.date_delivered_promised,2)
WHERE
	out_order_tracking.date_delivered_promised >= '2013-12-31'
AND out_order_tracking.date_exported < '2013-12-31';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_ready_to_ship_promised = operations_ve.workday (out_order_tracking.date_ready_to_ship_promised,2)
WHERE
	out_order_tracking.date_ready_to_ship_promised >= '2013-12-31'
AND out_order_tracking.date_exported < '2013-12-31';

UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_procured_promised = operations_ve.workday (out_order_tracking.date_procured_promised,1)
WHERE
	out_order_tracking.date_procured_promised >= '2014-01-01'
AND out_order_tracking.date_exported < '2014-01-01';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_delivered_promised = operations_ve.workday (out_order_tracking.date_delivered_promised,1)
WHERE
	out_order_tracking.date_delivered_promised >= '2014-01-01'
AND out_order_tracking.date_exported < '2014-01-01';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_ready_to_ship_promised = operations_ve.workday (out_order_tracking.date_ready_to_ship_promised,1)
WHERE
	out_order_tracking.date_ready_to_ship_promised >= '2014-01-01'
AND out_order_tracking.date_exported < '2014-01-01';

UPDATE operations_ve.out_order_tracking
INNER JOIN bob_live_ve.sales_order_item ON out_order_tracking.order_item_id = sales_order_item.id_sales_order_item
INNER JOIN bob_live_ve.catalog_shipment_type ON sales_order_item.fk_catalog_shipment_type = catalog_shipment_type.id_catalog_shipment_type
SET out_order_tracking.fullfilment_type_bob = catalog_shipment_type. NAME;


DELETE operations_ve.pro_1st_attempt.*
FROM
	operations_ve.pro_1st_attempt;


INSERT INTO operations_ve.pro_1st_attempt (order_item_id, min_of_date) SELECT
	out_order_tracking.order_item_id,
	min(tms_status_delivery.date) AS min_of_date
FROM
	(
		operations_ve.out_order_tracking
		INNER JOIN wmsprod_ve.vw_itens_venda_entrega ON out_order_tracking.order_item_id = vw_itens_venda_entrega.item_id
	)
INNER JOIN wmsprod_ve.tms_status_delivery ON vw_itens_venda_entrega.cod_rastreamento = tms_status_delivery.cod_rastreamento
GROUP BY
	out_order_tracking.order_item_id,
	tms_status_delivery.id_status
HAVING
	tms_status_delivery.id_status = 5;


UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.pro_1st_attempt ON out_order_tracking.order_item_id = pro_1st_attempt.order_item_id
SET out_order_tracking.date_1st_attempt = min_of_date;


UPDATE operations_ve.out_order_tracking
INNER JOIN wmsprod_ve.vw_itens_venda_entrega vw ON out_order_tracking.order_item_id = vw.item_id
INNER JOIN wmsprod_ve.tms_status_delivery del ON vw.cod_rastreamento = del.cod_rastreamento
SET out_order_tracking.date_1st_attempt = (
	SELECT
		max(tms.date)
	FROM
		wmsprod_ve.tms_status_delivery tms,
		wmsprod_ve.vw_itens_venda_entrega itens
	WHERE
		del.cod_rastreamento = tms.cod_rastreamento
	AND vw.item_id = itens.item_id
	AND tms.id_status = 4
)
WHERE
	out_order_tracking.date_1st_attempt IS NULL
OR out_order_tracking.date_1st_attempt < '2011-05-01';


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.week_exported = operations_ve.week_iso (date_exported),
 out_order_tracking.week_procured_promised = operations_ve.week_iso (date_procured_promised),
 out_order_tracking.week_delivered_promised = operations_ve.week_iso (date_delivered_promised),
 out_order_tracking.month_num_delivered_promised = date_format(date_delivered_promised,"%Y-%m"),
 out_order_tracking.month_num_procured_promised = date_format(date_procured_promised,"%Y-%m"),
 month_num_ready_to_pick = date_format(date_ready_to_pick, "%Y-%m"),
 month_num_ordered = date_format(date_ordered, "%Y-%m"), 
 out_order_tracking.week_ready_to_ship_promised = operations_ve.week_iso (date_ready_to_ship_promised),
 out_order_tracking.week_ready_to_pick = operations_ve.week_iso (date_ready_to_pick),
 operations_ve.out_order_tracking.week_ordered = operations_ve.week_iso (date_ordered),
 
 out_order_tracking.week_shipped = operations_ve.week_iso (date_shipped);


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.month_procured_promised = monthname(date_procured_promised);


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.month_num_procured_promised = date_format(
	date_procured_promised,
	"%Y-%m"
);


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.month_delivered_promised = monthname(date_delivered_promised);


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.month_num_delivered_promised = date_format(
	date_delivered_promised,
	"%Y-%m"
);


UPDATE operations_ve.out_order_tracking
SET month_num_procured_promised = '2012-12'
WHERE
	date_procured_promised = '2012-12-31';


UPDATE operations_ve.out_order_tracking
SET month_num_delivered_promised = '2012-12'
WHERE
	date_delivered_promised = '2012-12-31';


UPDATE operations_ve.out_order_tracking
INNER JOIN wmsprod_ve.itens_venda ON out_order_tracking.order_item_id = itens_venda.item_id
INNER JOIN wmsprod_ve.romaneio ON itens_venda.romaneio_id = romaneio.romaneio_id
INNER JOIN wmsprod_ve.transportadoras ON romaneio.transportadora_id = transportadoras.transportadoras_id
SET out_order_tracking.shipping_carrier = nome_transportadora;


UPDATE operations_ve.out_order_tracking ot
INNER JOIN bob_live_ve.sales_order_item soi ON ot.order_item_id = soi.id_sales_order_item
INNER JOIN bob_live_ve.shipment_carrier sc ON soi.fk_shipment_carrier = sc.id_shipment_carrier
SET ot.shipping_carrier_srt = sc. NAME;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.year_procured_promised = date_format(
	date_procured_promised,
	"%Y"
);


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.year_delivered_promised = date_format(
	date_delivered_promised,
	"%Y"
);


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.sl0 = 1
WHERE
	(
		(
			(
				out_order_tracking.supplier_leadtime
			) = 0
		)
	);


UPDATE wmsprod_ve.itens_venda
INNER JOIN operations_ve.out_order_tracking ON itens_venda.item_id = out_order_tracking.order_item_id
SET out_order_tracking.status_wms = STATUS;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.stockout = "1"
WHERE
	out_order_tracking.status_wms = "quebra tratada"
OR out_order_tracking.status_wms = "quebrado";


# Nuevo Calc WD

#Workdays to export
UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.calcworkdays
	ON out_order_tracking.date_ordered = calcworkdays.date_first
		AND out_order_tracking.date_exported = calcworkdays.date_last
SET out_order_tracking.workdays_to_export =	calcworkdays.workdays;

UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.calcworkdays
	ON out_order_tracking.date_ordered = calcworkdays.date_first
		AND curdate() = calcworkdays.date_last
SET out_order_tracking.workdays_to_export =	calcworkdays.workdays
WHERE date_exported IS NULL;

#Workdays to procure
UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.calcworkdays
	ON out_order_tracking.date_exported 			= calcworkdays.date_first
		AND out_order_tracking.date_procured 	= calcworkdays.date_last
SET out_order_tracking.workdays_to_procure 	=	calcworkdays.workdays
WHERE stockout = 0
AND fullfilment_type_real not like 'dropshipping';

UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.calcworkdays
	ON out_order_tracking.date_exported 			= calcworkdays.date_first
		AND curdate() 																= calcworkdays.date_last
SET out_order_tracking.workdays_to_procure =	calcworkdays.workdays
WHERE stockout = 0
AND date_procured IS NULL
AND fullfilment_type_real not like 'dropshipping';

#Workdays to ready
UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.calcworkdays
	ON out_order_tracking.date_ready_to_pick	= calcworkdays.date_first
		AND out_order_tracking.date_ready_to_ship 	= calcworkdays.date_last
SET out_order_tracking.workdays_to_ready 	=	calcworkdays.workdays
;

UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.calcworkdays
	ON out_order_tracking.date_ready_to_pick	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET out_order_tracking.workdays_to_ready 	=	calcworkdays.workdays
WHERE date_ready_to_ship IS NULL;

#Workdays to ship
UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.calcworkdays
	ON out_order_tracking.date_ready_to_ship	= calcworkdays.date_first
		AND out_order_tracking.date_shipped 		= calcworkdays.date_last
SET out_order_tracking.workdays_to_ship 		=	calcworkdays.workdays
;

UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.calcworkdays
	ON out_order_tracking.date_ready_to_ship	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET out_order_tracking.workdays_to_ship 	=	calcworkdays.workdays
WHERE date_shipped IS NULL;

#workdays_to_1st_attempt
UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.calcworkdays
	ON out_order_tracking.date_shipped	= calcworkdays.date_first
		AND out_order_tracking.date_1st_attempt 		= calcworkdays.date_last
SET out_order_tracking.workdays_to_1st_attempt 		=	calcworkdays.workdays
;

UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.calcworkdays
	ON out_order_tracking.date_shipped	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET out_order_tracking.workdays_to_1st_attempt 	=	calcworkdays.workdays
WHERE date_1st_attempt IS NULL;

#workdays_to_deliver
UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.calcworkdays
	ON out_order_tracking.date_shipped	= calcworkdays.date_first
		AND out_order_tracking.date_delivered 		= calcworkdays.date_last
SET out_order_tracking.workdays_to_deliver 		=	calcworkdays.workdays
;

UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.calcworkdays
	ON out_order_tracking.date_shipped	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET out_order_tracking.workdays_to_deliver 	=	calcworkdays.workdays
WHERE date_delivered IS NULL;

#workdays_total_1st_attempt
UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.calcworkdays
	ON out_order_tracking.date_exported	= calcworkdays.date_first
		AND out_order_tracking.date_1st_attempt 		= calcworkdays.date_last
SET out_order_tracking.workdays_total_1st_attempt 		=	calcworkdays.workdays
WHERE date_shipped IS NOT NULL;

UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.calcworkdays
	ON out_order_tracking.date_exported	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET out_order_tracking.workdays_total_1st_attempt 	=	calcworkdays.workdays
WHERE date_shipped IS NOT NULL
AND date_1st_attempt IS NULL;

#workdays_total_delivery
UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.calcworkdays
	ON out_order_tracking.date_exported	= calcworkdays.date_first
		AND out_order_tracking.date_delivered 		= calcworkdays.date_last
SET out_order_tracking.workdays_total_delivery 		=	calcworkdays.workdays
WHERE date_shipped IS NOT NULL;

UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.calcworkdays
	ON out_order_tracking.date_exported	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET out_order_tracking.workdays_total_delivery 	=	calcworkdays.workdays
WHERE date_shipped IS NOT NULL
AND date_1st_attempt IS NULL;

# Aquí acaba nuevo calcworkdays

UPDATE operations_ve.out_order_tracking
SET days_to_export = CASE
WHEN date_ordered IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN date_exported IS NULL THEN
			datediff(curdate(), date_ordered)
		ELSE
			datediff(date_exported, date_ordered)
		END
	)
END,
 days_to_procure = CASE
WHEN stockout = 1 OR fullfilment_type_real like 'dropshipping'  THEN
	NULL
ELSE
	(
		CASE
		WHEN date_procured < date_exported THEN
			0
		ELSE
			(
				CASE
				WHEN date_procured IS NULL THEN
					datediff(curdate(), date_exported)
				ELSE
					datediff(
						date_procured,
						date_exported
					)
				END
			)
		END
	)
END,
 days_to_ready = CASE
WHEN date_ready_to_pick IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN date_ready_to_ship IS NULL THEN
			datediff(
				curdate(),
				date_ready_to_pick
			)
		ELSE
			datediff(
				date_ready_to_ship,
				date_ready_to_pick
			)
		END
	)
END,
 days_to_ship = CASE
WHEN date_ready_to_ship IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN date_shipped IS NULL THEN
			datediff(
				curdate(),
				date_ready_to_ship
			)
		ELSE
			datediff(
				date_shipped,
				date_ready_to_ship
			)
		END
	)
END,
 days_to_1st_attempt = CASE
WHEN date_shipped IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN date_1st_attempt IS NULL THEN
			datediff(curdate(), date_shipped)
		ELSE
			datediff(
				date_1st_attempt,
				date_shipped
			)
		END
	)
END,
 out_order_tracking.days_total_delivery = CASE
WHEN date_shipped IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN date_delivered IS NULL THEN
			datediff(curdate(), date_ordered)
		ELSE
			datediff(
				date_delivered,
				date_ordered
			)
		END
	)
END;


UPDATE operations_ve.out_order_tracking
SET date_delivered_errors = CASE
WHEN days_total_delivery < 0 THEN
	1
ELSE
	0
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.delay_to_procure = CASE
WHEN date_procured IS NULL THEN
	(
		CASE
		WHEN curdate() > date_procured_promised THEN
			1
		ELSE
			0
		END
	)
ELSE
	(
		CASE
		WHEN date_procured > date_procured_promised THEN
			1
		ELSE
			0
		END
	)
END,
 out_order_tracking.delay_to_ready = CASE
WHEN workdays_to_ready > 1 THEN
	1
ELSE
	0
END,
 out_order_tracking.delay_to_ship = CASE
WHEN workdays_to_ship > 1 THEN
	1
ELSE
	0
END,
 out_order_tracking.delay_to_1st_attempt = CASE
WHEN workdays_to_1st_attempt > 2 THEN
	1
ELSE
	0
END,
 out_order_tracking.delay_to_deliver = CASE
WHEN workdays_to_deliver > 2 THEN
	1
ELSE
	0
END,
 out_order_tracking.delay_total_1st_attempt = CASE
WHEN date_1st_attempt IS NULL THEN
	(
		CASE
		WHEN curdate() > date_delivered_promised THEN
			1
		ELSE
			0
		END
	)
ELSE
	(
		CASE
		WHEN date_1st_attempt > date_delivered_promised THEN
			1
		ELSE
			0
		END
	)
END,
 out_order_tracking.delay_total_delivery = CASE
WHEN date_delivered IS NULL THEN
	(
		CASE
		WHEN curdate() > date_delivered_promised THEN
			1
		ELSE
			0
		END
	)
ELSE
	(
		CASE
		WHEN date_delivered > date_delivered_promised THEN
			1
		ELSE
			0
		END
	)
END,
 out_order_tracking.workdays_delay_to_procure = CASE
WHEN workdays_to_procure - supplier_leadtime < 0 THEN
	0
ELSE
	workdays_to_procure - supplier_leadtime
END,
 out_order_tracking.workdays_delay_to_ready = CASE
WHEN workdays_to_ready - 1 < 0 THEN
	0
ELSE
	workdays_to_ready - 1
END,
 out_order_tracking.workdays_delay_to_ship = CASE
WHEN workdays_to_ship - 1 < 0 THEN
	0
ELSE
	workdays_to_ship - 1
END,
 out_order_tracking.workdays_delay_to_1st_attempt = CASE
WHEN workdays_to_1st_attempt - 3 < 0 THEN
	0
ELSE
	workdays_to_1st_attempt - 3
END,
 out_order_tracking.workdays_delay_to_deliver = CASE
WHEN workdays_to_deliver - 3 < 0 THEN
	0
ELSE
	workdays_to_deliver - 3
END,
 out_order_tracking.on_time_to_procure = CASE
WHEN date_procured <= date_procured_promised THEN
	1
ELSE
	0
END,
 out_order_tracking.on_time_total_1st_attempt = CASE
WHEN date_1st_attempt <= date_delivered_promised THEN
	1
ELSE
	0
END,
 out_order_tracking.on_time_total_delivery = CASE
WHEN date_delivered <= date_delivered_promised THEN
	1
WHEN date_delivered IS NULL THEN
	0
ELSE
	0
END;


UPDATE operations_ve.out_order_tracking
INNER JOIN bob_live_ve.catalog_config ON out_order_tracking.sku_config = catalog_config.sku
SET out_order_tracking.presale = "1"
WHERE
	catalog_config. NAME LIKE "preventa%";


UPDATE operations_ve.out_order_tracking
INNER JOIN bob_live_ve.catalog_config ON out_order_tracking.sku_config = catalog_config.sku
INNER JOIN bob_live_ve.catalog_attribute_option_global_buyer ON catalog_attribute_option_global_buyer.id_catalog_attribute_option_global_buyer = catalog_config.fk_catalog_attribute_option_global_buyer
SET out_order_tracking.buyer = catalog_attribute_option_global_buyer. NAME;


UPDATE operations_ve.out_order_tracking
INNER JOIN bob_live_ve.sales_order_item ON out_order_tracking.order_item_id = sales_order_item.id_sales_order_item
INNER JOIN bob_live_ve.sales_order_item_status ON sales_order_item.fk_sales_order_item_status = sales_order_item_status.id_sales_order_item_status
SET out_order_tracking.status_bob = sales_order_item_status. NAME;


UPDATE operations_ve.out_order_tracking
INNER JOIN wmsprod_ve.vw_itens_venda_entrega ON out_order_tracking.order_item_id = vw_itens_venda_entrega.item_id
SET out_order_tracking.wms_tracking_code = cod_rastreamento;


UPDATE operations_ve.out_order_tracking
INNER JOIN wmsprod_ve.tms_tracks ON out_order_tracking.wms_tracking_code = tms_tracks.cod_rastreamento
SET out_order_tracking.shipping_carrier_tracking_code = track;


UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.status_match_bob_wms ON out_order_tracking.status_wms = status_match_bob_wms.status_wms
AND out_order_tracking.status_bob = status_match_bob_wms.status_bob
SET out_order_tracking.status_match_bob_wms = status_match_bob_wms.correct;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.check_dates = CASE
WHEN date_ordered > date_exported THEN
	"date ordered > date exported"
ELSE
	(
		CASE
		WHEN date_exported > date_procured THEN
			"date exported > date procured"
		ELSE
			(
				CASE
				WHEN date_procured > date_ready_to_ship THEN
					"date procured > date ready to ship"
				ELSE
					(
						CASE
						WHEN date_ready_to_ship > date_shipped THEN
							"date ready to ship > date shipped"
						ELSE
							(
								CASE
								WHEN date_shipped > date_1st_attempt THEN
									"date shipped > date 1st attempt"
								ELSE
									(
										CASE
										WHEN date_1st_attempt > date_delivered THEN
											"date shipped > date 1st attempt"
										ELSE
											"correct"
										END
									)
								END
							)
						END
					)
				END
			)
		END
	)
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.check_date_1st_attempt = CASE
WHEN date_1st_attempt IS NULL THEN
	(
		CASE
		WHEN date_delivered IS NULL THEN
			0
		ELSE
			1
		END
	)
ELSE
	(
		CASE
		WHEN date_1st_attempt > date_delivered THEN
			1
		ELSE
			0
		END
	)
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.check_date_shipped = CASE
WHEN date_shipped IS NULL THEN
	(
		CASE
		WHEN date_1st_attempt IS NULL
		AND date_delivered IS NULL THEN
			0
		ELSE
			1
		END
	)
ELSE
	(
		CASE
		WHEN date_shipped > date_1st_attempt THEN
			1
		ELSE
			0
		END
	)
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.check_date_ready_to_ship = CASE
WHEN date_ready_to_ship IS NULL THEN
	(
		CASE
		WHEN date_shipped IS NULL
		AND date_1st_attempt IS NULL
		AND date_delivered IS NULL THEN
			0
		ELSE
			1
		END
	)
ELSE
	(
		CASE
		WHEN date_ready_to_ship > date_shipped THEN
			1
		ELSE
			0
		END
	)
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.check_date_procured = CASE
WHEN date_procured IS NULL THEN
	(
		CASE
		WHEN date_ready_to_ship IS NULL
		AND date_shipped IS NULL
		AND date_1st_attempt IS NULL
		AND date_delivered IS NULL THEN
			0
		ELSE
			1
		END
	)
ELSE
	(
		CASE
		WHEN date_procured > date_ready_to_ship THEN
			1
		ELSE
			0
		END
	)
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.check_date_exported = CASE
WHEN date_exported IS NULL THEN
	(
		CASE
		WHEN date_procured IS NULL
		AND date_ready_to_ship IS NULL
		AND date_shipped IS NULL
		AND date_1st_attempt IS NULL
		AND date_delivered IS NULL THEN
			0
		ELSE
			1
		END
	)
ELSE
	(
		CASE
		WHEN date_exported > date_procured THEN
			1
		ELSE
			0
		END
	)
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.check_date_ordered = CASE
WHEN date_ordered IS NULL THEN
	(
		CASE
		WHEN date_exported IS NULL
		AND date_procured IS NULL
		AND date_ready_to_ship IS NULL
		AND date_shipped IS NULL
		AND date_1st_attempt IS NULL
		AND date_delivered IS NULL THEN
			0
		ELSE
			1
		END
	)
ELSE
	(
		CASE
		WHEN date_ordered > date_exported THEN
			1
		ELSE
			0
		END
	)
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_procured = NULL
WHERE
	out_order_tracking.status_wms = "aguardando estoque"
OR out_order_tracking.status_wms = "analisando quebra";


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.still_to_procure = 1
WHERE
	out_order_tracking.date_procured IS NULL;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.still_to_procure = 0
WHERE
	out_order_tracking.status_wms = "quebrado"
OR out_order_tracking.status_wms = "quebra tratada";


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.vol_weight = package_height * package_length * package_width / 5000;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.max_vol_w_vs_w = CASE
WHEN vol_weight > package_weight THEN
	vol_weight
ELSE
	package_weight
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.package_measure = CASE
WHEN max_vol_w_vs_w > 45.486 THEN
	"large"
ELSE
	(
		CASE
		WHEN max_vol_w_vs_w > 2.277 THEN
			"medium"
		ELSE
			"small"
		END
	)
END;

UPDATE operations_ve.out_order_tracking
SET out_order_tracking.package_measure_new = CASE
WHEN max_vol_w_vs_w > 35 THEN
	'oversized'
ELSE
	(
		CASE
		WHEN max_vol_w_vs_w > 5 THEN
			'large'
		ELSE
			(
				CASE
				WHEN max_vol_w_vs_w > 2 THEN
					'medium'
				ELSE
					'small'
				END
			)
		END
	)
END;


UPDATE operations_ve.out_order_tracking
INNER JOIN bob_live_ve.sales_order ON out_order_tracking.order_number = sales_order.order_nr
INNER JOIN bob_live_ve.sales_order_address ON sales_order.fk_sales_order_address_billing = sales_order_address.id_sales_order_address
SET out_order_tracking.ship_to_zip = postcode;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.delay_carrier_shipment = CASE
WHEN package_measure = "large" THEN
	(
		CASE
		WHEN workdays_to_1st_attempt - 5 > 0 THEN
			1
		ELSE
			0
		END
	)
ELSE
	(
		CASE
		WHEN workdays_to_1st_attempt - 3 > 0 THEN
			1
		ELSE
			0
		END
	)
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.days_left_to_procure = CASE
WHEN date_procured IS NULL THEN
	(
		CASE
		WHEN datediff(
			date_procured_promised,
			curdate()
		) < 0 THEN
			0
		ELSE
			datediff(
				date_procured_promised,
				curdate()
			)
		END
	)
ELSE
	555
END;

/*
UPDATE operations_ve.out_order_tracking
SET out_order_tracking.deliv_within_3_days_of_order = CASE
WHEN datediff(
	date_delivered,
	date_ordered
) <= 5 THEN
	(
		CASE
		WHEN operations_ve.calcworkdays (
			date_ordered,
			date_delivered
		) <= 3 THEN
			1
		ELSE
			0
		END
	)
ELSE
	0
END,
 out_order_tracking.first_att_within_3_days_of_order = CASE
WHEN datediff(
	date_1st_attempt,
	date_ordered
) <= 5 THEN
	(
		CASE
		WHEN operations_ve.calcworkdays (
			date_ordered,
			date_1st_attempt
		) <= 3 THEN
			1
		ELSE
			0
		END
	)
ELSE
	0
END;
*/

UPDATE operations_ve.out_order_tracking
SET out_order_tracking.shipped_same_day_as_order = CASE
WHEN date_ordered = date_shipped THEN
	1
ELSE
	0
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.shipped_same_day_as_procured = CASE
WHEN date_procured = date_shipped THEN
	1
ELSE
	0
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.reason_for_delay = "on time 1st attempt";


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.reason_for_delay = "procurement"
WHERE
	out_order_tracking.delay_to_procure = 1
AND out_order_tracking.delay_total_1st_attempt = 1;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.reason_for_delay = "preparing item for shipping"
WHERE
	out_order_tracking.reason_for_delay = "on time 1st attempt"
AND out_order_tracking.delay_to_ready = 1
AND out_order_tracking.delay_total_1st_attempt = 1;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.reason_for_delay = "preparing item for shipping"
WHERE
	out_order_tracking.reason_for_delay = "on time 1st attempt"
AND out_order_tracking.delay_to_ready = 1
AND out_order_tracking.delay_total_1st_attempt = 1;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.reason_for_delay = "carrier late delivering"
WHERE
	out_order_tracking.reason_for_delay = "on time 1st attempt"
AND out_order_tracking.delay_total_1st_attempt = 1;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.early_to_procure = CASE
WHEN (
	CASE
	WHEN date_procured IS NULL THEN
		datediff(
			date_procured_promised,
			curdate()
		)
	ELSE
		datediff(
			date_procured_promised,
			date_procured
		)
	END
) > 0 THEN
	1
ELSE
	0
END,
 out_order_tracking.early_to_1st_attempt = CASE
WHEN (
	CASE
	WHEN date_1st_attempt IS NULL THEN
		datediff(
			date_delivered_promised,
			curdate()
		)
	ELSE
		datediff(
			date_delivered_promised,
			date_1st_attempt
		)
	END
) > 0 THEN
	1
ELSE
	0
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.delay_reason_maximum_value = operations_ve.maximum (
	workdays_delay_to_procure,
	workdays_delay_to_ready,
	workdays_delay_to_ship,
	workdays_delay_to_1st_attempt
);


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.delay_reason = CASE
WHEN delay_total_1st_attempt = 1 THEN
	(
		CASE
		WHEN delay_reason_maximum_value = workdays_delay_to_procure THEN
			"procurement"
		ELSE
			(
				CASE
				WHEN delay_reason_maximum_value = workdays_delay_to_ready THEN
					"preparing item for shipping"
				ELSE
					(
						CASE
						WHEN delay_reason_maximum_value = workdays_delay_to_ship THEN
							"carrier late to pick up item"
						ELSE
							(
								CASE
								WHEN delay_reason_maximum_value = workdays_delay_to_1st_attempt THEN
									"carrier late delivering"
								ELSE
									"on time 1st attempt"
								END
							)
						END
					)
				END
			)
		END
	)
ELSE
	"on time 1st attempt"
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.on_time_to_ship = CASE
WHEN date_shipped <= date_ready_to_ship_promised THEN
	1
ELSE
	0
END,
 out_order_tracking.on_time_r2s = CASE
WHEN date_ready_to_ship <= date_ready_to_ship_promised THEN
	1
ELSE
	0
END;


DELETE operations_ve.pro_order_tracking_num_items_per_order.*
FROM
	operations_ve.pro_order_tracking_num_items_per_order;


INSERT INTO operations_ve.pro_order_tracking_num_items_per_order (
	order_number,
	count_of_order_item_id
) SELECT
	out_order_tracking.order_number,
	count(
		out_order_tracking.order_item_id
	) AS countoforder_item_id
FROM
	operations_ve.out_order_tracking
GROUP BY
	out_order_tracking.order_number;


UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.pro_order_tracking_num_items_per_order ON out_order_tracking.order_number = pro_order_tracking_num_items_per_order.order_number
SET out_order_tracking.num_items_per_order = 1 / pro_order_tracking_num_items_per_order.count_of_order_item_id;

/*
UPDATE operations_ve.out_order_tracking
SET out_order_tracking.procurement_actual_time = CASE
WHEN date_exported IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN date_procured IS NULL THEN
			operations_ve.calcworkdays (date_exported, curdate())
		ELSE
			operations_ve.calcworkdays (
				date_exported,
				date_procured
			)
		END
	)
END;
*/

UPDATE operations_ve.out_order_tracking
SET out_order_tracking.procurement_delay = CASE
WHEN date_exported >= curdate() THEN
	0
ELSE
	(
		CASE
		WHEN date_sub(
			workdays_to_procure,
			INTERVAL supplier_leadtime DAY
		) > 0 THEN
			date_sub(
				workdays_to_procure,
				INTERVAL supplier_leadtime DAY
			)
		ELSE
			0
		END
	)
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.procurement_delay_counter = 1
WHERE
	out_order_tracking.procurement_delay > 0;

/*
UPDATE operations_ve.out_order_tracking
SET out_order_tracking.warehouse_actual_time = CASE
WHEN date_ready_to_pick IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN date_ready_to_ship IS NULL THEN
			operations_ve.calcworkdays (
				date_ready_to_pick,
				curdate()
			)
		ELSE
			operations_ve.calcworkdays (
				date_ready_to_pick,
				date_ready_to_ship
			)
		END
	)
END;
*/

# Otro nuevo Calc Workdays
#Warehouse Delay
UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.calcworkdays
	ON out_order_tracking.date_ready_to_ship = calcworkdays.date_first
		AND out_order_tracking.date_ready_to_ship_promised = calcworkdays.date_last
SET 
	out_order_tracking.warehouse_delay  =	calcworkdays.workdays,
	out_order_tracking.warehouse_delay_counter = 1
WHERE date_ready_to_ship_promised <= curdate();

UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.calcworkdays
	ON out_order_tracking.date_ready_to_ship = calcworkdays.date_first
		AND curdate() = calcworkdays.date_last
SET 
	out_order_tracking.warehouse_delay  =	calcworkdays.workdays,
	out_order_tracking.warehouse_delay_counter = 1
WHERE date_ready_to_ship_promised <= curdate()
AND date_ready_to_ship IS NULL;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.carrier_time = max_delivery_time - wh_time - supplier_leadtime;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.actual_carrier_time = CASE
WHEN date_ready_to_ship IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN date_delivered IS NULL THEN
			operations_ve.calcworkdays (
				date_ready_to_ship,
				curdate()
			)
		ELSE
			operations_ve.calcworkdays (
				date_ready_to_ship,
				date_delivered
			)
		END
	)
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.delivery_delay = CASE
WHEN date_ready_to_ship >= curdate() THEN
	0
ELSE
	(
		CASE
		WHEN actual_carrier_time > carrier_time THEN
			actual_carrier_time - carrier_time
		ELSE
			0
		END
	)
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.delivery_delay_counter = 1
WHERE
	out_order_tracking.delivery_delay > 0
AND out_order_tracking.date_delivered_promised <= curdate();


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.delay_exceptions = CASE
WHEN out_order_tracking.supplier_leadtime >= out_order_tracking.max_delivery_time THEN
	1
ELSE
	0
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.ready_to_ship = 1
WHERE
	out_order_tracking.date_ready_to_ship IS NOT NULL;

/*
UPDATE operations_ve.out_order_tracking
SET out_order_tracking.wh_workdays_to_r2s = CASE
WHEN date_ready_to_ship IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN date_ready_to_pick IS NULL THEN
			NULL
		ELSE
			operations_ve.calcworkdays (
				date_ready_to_pick,
				date_ready_to_ship
			)
		END
	)
END;
*/

UPDATE operations_ve.out_order_tracking
SET 
 out_order_tracking.week_r2s_promised = operations_ve.week_iso (date_ready_to_ship_promised),
 out_order_tracking.month_num_r2s_promised = date_format(date_ready_to_ship_promised,"%Y-%m"),
 out_order_tracking.week_ordered = operations_ve.week_iso (date_ordered)
 ;

# Nuevo Calc WD

UPDATE        operations_ve.out_order_tracking
   INNER JOIN operations_ve.calcworkdays
           ON     out_order_tracking.date_ready_to_pick = calcworkdays.date_first
              AND calcworkdays.date_last = curdate() 
SET 
   r2s_workday_0  = IF( workdays_to_ready  = 0 AND workdays >= 0, 1 , r2s_workday_0 ),
   r2s_workday_1  = IF( workdays_to_ready <= 1 AND workdays >= 1, 1 , r2s_workday_1 ),
   r2s_workday_2  = IF( workdays_to_ready <= 2 AND workdays >= 2, 1 , r2s_workday_2 ),
   r2s_workday_3  = IF( workdays_to_ready <= 3 AND workdays >= 3, 1 , r2s_workday_3 ),
   r2s_workday_4  = IF( workdays_to_ready <= 4 AND workdays >= 4, 1 , r2s_workday_4 ),
   r2s_workday_5  = IF( workdays_to_ready <= 5 AND workdays >= 5, 1 , r2s_workday_5 ),
   r2s_workday_6  = IF( workdays_to_ready <= 6 AND workdays >= 6, 1 , r2s_workday_6 ), 
   r2s_workday_7  = IF( workdays_to_ready <= 7 AND workdays >= 7, 1 , r2s_workday_7 ), 
   r2s_workday_8  = IF( workdays_to_ready <= 8 AND workdays >= 8, 1 , r2s_workday_8 ),  
   r2s_workday_9  = IF( workdays_to_ready <= 9 AND workdays >= 9, 1 , r2s_workday_9 ),  
   r2s_workday_10 = IF( workdays_to_ready  >10 AND workdays >=10, 1 , r2s_workday_10)
;

# Acaba Nuevo Calc WD

UPDATE operations_ve.out_order_tracking
SET out_order_tracking.ready_to_pick = 1
WHERE
	out_order_tracking.date_ready_to_pick IS NOT NULL;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_payable_promised = date_procured
WHERE
	out_order_tracking.oms_payment_event = "entrega";


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_payable_promised = date_procured
WHERE
	out_order_tracking.oms_payment_event = "pedido";


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.date_payable_promised = date_procured - supplier_leadtime
WHERE
	out_order_tracking.oms_payment_event = "factura";


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.delay_linio_promise = CASE
WHEN is_linio_promise = 1
AND workdays_to_1st_attempt > 2 THEN
	1
ELSE
	0
END;


UPDATE operations_ve.out_order_tracking
INNER JOIN wmsprod_ve.entrega ON out_order_tracking.wms_tracking_code = entrega.cod_rastreamento
SET out_order_tracking.delivery_fullfilment = entrega.delivery_fulfillment;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.split_order = CASE
WHEN delivery_fullfilment = 'partial' THEN
	1
ELSE
	0
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.effective_1st_attempt = CASE
WHEN date_delivered = date_1st_attempt THEN
	1
ELSE
	0
END;


DELETE
FROM
	operations_ve.pro_value_from_order;


INSERT INTO operations_ve.pro_value_from_order SELECT
	order_number,
	1 / count(order_item_id) AS value_from_order
FROM
	operations_ve.out_order_tracking t
GROUP BY
	order_number
ORDER BY
	date_ordered DESC;


UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.pro_value_from_order ON out_order_tracking.order_number = pro_value_from_order.order_number
SET out_order_tracking.value_from_order = pro_value_from_order.value_from_order;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.shipped = CASE
WHEN date_shipped IS NOT NULL THEN
	1
ELSE
	0
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.delivered = CASE
WHEN date_delivered IS NOT NULL THEN
	1
ELSE
	0
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.first_attempt = 1
WHERE
	date_1st_attempt IS NOT NULL;

UPDATE operations_ve.out_order_tracking
SET out_order_tracking.exported_last_30 = CASE
WHEN datediff(
 curdate(),
 date_exported
) <= 30
AND datediff(
 curdate(),
 date_exported
) > 0 THEN
 1
ELSE
 0
END;



UPDATE operations_ve.out_order_tracking
SET out_order_tracking.procured_promised_last_30 = CASE
WHEN datediff(
	curdate(),
	date_procured_promised
) <= 30
AND datediff(
	curdate(),
	date_procured_promised
) > 0 THEN
	1
ELSE
	0
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.shipped_last_30 = CASE
WHEN datediff(curdate(), date_shipped) <= 30
AND datediff(curdate(), date_shipped) > 0 THEN
	1
ELSE
	0
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.delivered_promised_last_30 = CASE
WHEN datediff(
	curdate(),
	date_delivered_promised
) <= 30
AND datediff(
	curdate(),
	date_delivered_promised
) > 0 THEN
	1
ELSE
	0
END;


UPDATE operations_ve.out_order_tracking
SET out_order_tracking.pending_first_attempt = CASE
WHEN date_shipped IS NOT NULL
AND date_1st_attempt IS NULL THEN
	1
ELSE
	0
END;


UPDATE operations_ve.out_order_tracking
SET 
 price_after_tax = price/(1+(tax_percent/100)),
 cost_after_tax = cost/(1+(tax_percent/100));


UPDATE operations_ve.out_order_tracking a
INNER JOIN bob_live_ve.catalog_config b ON a.sku_config = b.sku
INNER JOIN bob_live_ve.catalog_attribute_option_global_category c ON b.fk_catalog_attribute_option_global_category = c.id_catalog_attribute_option_global_category
INNER JOIN bob_live_ve.catalog_attribute_option_global_sub_category d ON b.fk_catalog_attribute_option_global_sub_category = d.id_catalog_attribute_option_global_sub_category
INNER JOIN bob_live_ve.catalog_attribute_option_global_sub_sub_category e ON b.fk_catalog_attribute_option_global_sub_sub_category = e.id_catalog_attribute_option_global_sub_sub_category
SET a.category_com_main = c. NAME,
 a.category_com_sub = d. NAME,
 a.category_com_sub_sub = e. NAME;


UPDATE operations_ve.out_order_tracking
SET ship_to_zip2 = LEFT (ship_to_zip, 2),
 ship_to_zip3 = LEFT (ship_to_zip, 3);


UPDATE operations_ve.out_order_tracking a
INNER JOIN production_ve.tbl_order_detail b ON a.order_number = b.order_nr
SET a.coupon_code = b.coupon_code;


UPDATE operations_ve.out_order_tracking
SET is_corporate_sale = 1
WHERE
	coupon_code LIKE 'CDEAL%';


UPDATE operations_ve.out_order_tracking a
INNER JOIN development_ve.A_E_M1_New_CategoryBP b ON a.category_com_sub = b.cat2
SET a.category_bp = b.CatBP;


UPDATE operations_ve.out_order_tracking a
INNER JOIN development_ve.A_E_M1_New_CategoryBP b ON a.category_com_main = b.cat1
SET a.category_bp = b.CatBP
WHERE
	a.category_bp IS NULL;


UPDATE operations_ve.out_order_tracking
SET fullfilment_type_bp = CASE
WHEN fullfilment_type_real = 'crossdock' THEN
	'crossdocking'
WHEN fullfilment_type_real = 'inventory'
AND fullfilment_type_bob = 'consignment' THEN
	'consignment'
WHEN fullfilment_type_real = 'inventory'
AND fullfilment_type_bob <> 'consignment' THEN
	'outright buying'
WHEN fullfilment_type_real = 'dropshipping' THEN
	'other'
END;


UPDATE operations_ve.out_order_tracking
SET date_time_1st_attempt = date_1st_attempt + INTERVAL 12 HOUR,
 date_time_delivered = date_delivered + INTERVAL 12 HOUR;


UPDATE operations_ve.out_order_tracking
SET is_late_delivery_candidate = 1
WHERE
	(
		date_delivered_promised <= curdate()
		AND date_shipped IS NULL
	)
OR (
	date_delivered_promised = adddate(curdate(), INTERVAL 1 DAY)
	AND date_procured IS NULL
);


UPDATE
 operations_ve.out_order_tracking
INNER JOIN development_ve.A_Master_Catalog
	ON out_order_tracking.sku_config = A_Master_Catalog.sku_config
SET
 is_market_place = isMarketPlace
WHERE
 date_ordered >= isMarketPlace_Since;

# Nuevo Calc Workdays
UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.calcworkdays
	ON out_order_tracking.date_exported = calcworkdays.date_first
		AND out_order_tracking.date_procurement_order = calcworkdays.date_last
SET 
 out_order_tracking.workdays_to_po =	calcworkdays.workdays,
 out_order_tracking.days_to_po = DATEDIFF(date_procurement_order,date_exported)
WHERE date_procurement_order IS NOT NULL
AND date_exported IS NOT NULL;

UPDATE operations_ve.out_order_tracking
INNER JOIN operations_ve.calcworkdays
	ON out_order_tracking.date_exported = calcworkdays.date_first
		AND out_order_tracking.date_procurement_order = calcworkdays.date_last
SET 
 out_order_tracking.workdays_to_po =	calcworkdays.workdays,
 out_order_tracking.days_to_po = datediff(curdate(), date_exported)
WHERE date_procurement_order IS NULL
AND date_exported IS NOT NULL;
# Nuevo Calc Workdays

Update
 operations_ve.out_order_tracking
INNER JOIN
 bob_live_ve.catalog_config ON out_order_tracking.sku_config = bob_live_ve.catalog_config.sku
INNER JOIN 
  bob_live_ve.catalog_attribute_option_global_analyst_procurement ON bob_live_ve.catalog_config.fk_catalog_attribute_option_global_analyst_procurement = bob_live_ve.catalog_attribute_option_global_analyst_procurement.id_catalog_attribute_option_global_analyst_procurement
SET 
out_order_tracking.Procurement_analist = bob_live_ve.catalog_attribute_option_global_analyst_procurement.name;

#24hr_shipments_Cust_persp

UPDATE operations_ve.out_order_tracking
SET 24hr_shipments_Cust_persp = CASE
		WHEN date_ordered is null and date_shipped IS NULL THEN NULL
    WHEN date_shipped is null THEN
			datediff(curdate(), date_ordered)
		ELSE
			datediff(date_shipped, date_ordered)
		END;

#24hr_shipmnets_WH_persp

UPDATE operations_ve.out_order_tracking
SET 24hr_shipmnets_WH_persp = CASE
		WHEN date_ready_to_pick is null and date_shipped IS NULL THEN NULL
    WHEN date_shipped is null THEN
			datediff(curdate(), date_ready_to_pick)
		ELSE
			datediff(date_shipped, date_ready_to_pick)
		END;


UPDATE operations_ve.out_order_tracking
SET item_counter = 1;

REPLACE operations_ve.pro_backlog_history
SELECT
 curdate() as Date_reported,
 SUM( IF(     presale = 0
          AND status_wms IN ('aguardando estoque','analisando quebra')
                                      , delay_to_procure     , 0 ) ) as procurement_backlog,
 SUM( IF(     date_delivered IS NULL
          AND date_delivered_promised < curdate()
          AND status_wms = 'expedido' , delay_total_delivery , 0 ) ) as deliveries_backlog
FROM
 operations_ve.out_order_tracking
WHERE 
(
         presale = 0
    AND status_wms IN ('aguardando estoque','analisando quebra')
)
OR 
(
        date_delivered IS NULL
    AND date_delivered_promised < curdate()
    AND status_wms = 'expedido'
);

DROP TABLE IF EXISTS production_ve.out_order_tracking;

CREATE TABLE production_ve.out_order_tracking LIKE operations_ve.out_order_tracking; 

INSERT INTO production_ve.out_order_tracking SELECT * FROM operations_ve.out_order_tracking; 

INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Venezuela', 
  'out_order_tracking',
  'finish',
  NOW(),
  max(date_time_ordered),
  count(*),
  count(item_counter)
FROM
  production_ve.out_order_tracking;

INSERT INTO `production`.`table_monitoring_report` VALUES (NULL, 'Daily Procurement - VE', NOW());

INSERT INTO `production`.`table_monitoring_report` VALUES (NULL, 'Daily Warehouse - VE', NOW());

INSERT INTO `production`.`table_monitoring_report` VALUES (NULL, 'Daily Deliveries - VE', NOW());


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 PROCEDURE `out_procurement_tracking`()
BEGIN


	


INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Venezuela', 
  'out_procurement_tracking',
  'start',
  NOW(),
  max(date_created),
  count(*),
  count(item_counter)
FROM
  production_ve.out_procurement_tracking;

TRUNCATE operations_ve.out_procurement_tracking ; 


	INSERT INTO operations_ve.out_procurement_tracking (
		id_procurement_order_item,
		id_procurement_order,
		id_catalog_simple,
		cost_oms,
		is_deleted,
		date_payment_scheduled,
		date_payment_estimation,
		date_payment_promised,
		is_received,
		is_confirmed) 
SELECT
		id_procurement_order_item,
		fk_procurement_order,
		fk_catalog_simple,
		unit_price,
		is_deleted,
		date(schedule_payment_date),
		date(estimated_payment_date),
		date(promised_payment_date),
		sku_received,
		is_confirm
	FROM
		procurement_live_ve.procurement_order_item ; 


UPDATE operations_ve.out_procurement_tracking a
INNER JOIN procurement_live_ve.procurement_order b ON a.id_procurement_order = b.id_procurement_order
SET 
  a.po_name = concat(	b.venture_code, 
											lpad(b.id_procurement_order, 7, 0),
											b.check_digit),
	a.is_cancelled = b.is_cancelled,
	a.procurement_payment_terms = b.procurement_payment_terms,
	a.procurement_payment_event = b.procurement_payment_event,
	a.procurement_payment_status = b.payment_status,
	a.date_created = date(b.created_at) ; 


UPDATE operations_ve.out_procurement_tracking a
INNER JOIN bob_live_mx.catalog_simple b ON a.id_catalog_simple = b.id_catalog_simple
INNER JOIN bob_live_mx.catalog_config c ON b.fk_catalog_config = c.id_catalog_config
INNER JOIN bob_live_mx.supplier d ON c.fk_supplier = d.id_supplier
SET 
 a.sku_simple = b.sku,
 a.sku_name = c. NAME,
 a.supplier_id = d.id_supplier,
 a.supplier_name = d. NAME ; 


UPDATE operations_ve.out_procurement_tracking a
INNER JOIN bob_live_mx.catalog_simple b ON a.id_catalog_simple = b.id_catalog_simple
INNER JOIN bob_live_mx.catalog_config c ON b.fk_catalog_config = c.id_catalog_config
INNER JOIN bob_live_mx.catalog_attribute_option_global_buyer_name d ON c.fk_catalog_attribute_option_global_buyer_name = d.id_catalog_attribute_option_global_buyer_name
INNER JOIN bob_live_mx.catalog_attribute_option_global_head_buyer_name e ON c.fk_catalog_attribute_option_global_head_buyer_name = e.id_catalog_attribute_option_global_head_buyer_name
SET a.buyer = d. NAME,
 a.head_buyer = e. NAME ; 


UPDATE operations_ve.out_procurement_tracking a
INNER JOIN bob_live_mx.catalog_simple b ON a.id_catalog_simple = b.id_catalog_simple
INNER JOIN bob_live_mx.catalog_config c ON b.fk_catalog_config = c.id_catalog_config
INNER JOIN bob_live_mx.catalog_attribute_option_global_category d ON c.fk_catalog_attribute_option_global_category = d.id_catalog_attribute_option_global_category
INNER JOIN bob_live_mx.catalog_attribute_option_global_sub_category e ON c.fk_catalog_attribute_option_global_sub_category = e.id_catalog_attribute_option_global_sub_category
INNER JOIN bob_live_mx.catalog_attribute_option_global_sub_sub_category f ON c.fk_catalog_attribute_option_global_sub_sub_category = f.id_catalog_attribute_option_global_sub_sub_category
SET 
 a.cat_1 = d. NAME,
 a.cat_2 = e. NAME,
 a.cat_3 = f. NAME ; 


UPDATE operations_ve.out_procurement_tracking a
INNER JOIN procurement_live_ve.catalog_supplier_attributes b ON a.supplier_id = b.fk_catalog_supplier
SET 
 a.supplier_name = b. NAME,
 a.catalog_payment_type = b.payment_type,
 a.catalog_payment_terms = b.payment_terms,
 a.procurement_analyst = b.buyer_name ; 


UPDATE operations_ve.out_procurement_tracking a
INNER JOIN procurement_live_ve.procurement_order_item_date_history b ON a.id_procurement_order_item = b.fk_procurement_order_item
SET a.date_goods_received = b.delivery_real_date ; 


UPDATE operations_ve.out_procurement_tracking a
INNER JOIN procurement_live_ve.wms_received_item b ON a.id_procurement_order_item = b.fk_procurement_order_item
INNER JOIN wmsprod.itens_recebimento c ON b.id_wms = c.itens_recebimento_id
INNER JOIN wmsprod.estoque d ON c.itens_recebimento_id = d.itens_recebimento_id
SET 
 a.stock_item_id = d.estoque_id,
 a.wh_location = d.endereco ; 





UPDATE operations_ve.out_procurement_tracking a
INNER JOIN wmsprod.itens_venda b ON a.stock_item_id = b.estoque_id
SET a.order_item_id = b.item_id ; 


UPDATE operations_ve.out_procurement_tracking a
INNER JOIN procurement_live_ve.invoice_item b ON a.id_procurement_order_item = b.fk_procurement_order_item
INNER JOIN procurement_live_ve.invoice c ON b.fk_invoice = c.id_invoice
SET 
 a.date_invoice_issued = date(c.emission_date),
 a.date_invoice_created = date(c.created_at),
 a.date_invoice_received = date(c.invoice_date),
 a.invoice_number = c.invoice_nr ; 


UPDATE operations_ve.out_procurement_tracking a
LEFT JOIN procurement_live_ve.procurement_order_payment_items b ON a.id_procurement_order_item = b.fk_procurement_order_item
INNER JOIN procurement_live_ve.procurement_order_payment c ON b.fk_procurement_order_payment = c.id_procurement_order_payment
SET a.procurement_payment_type = c.payment_type ; 


UPDATE operations_ve.out_procurement_tracking a
LEFT JOIN procurement_live_ve.procurement_order_payment_items b ON a.id_procurement_order_item = b.fk_procurement_order_item
INNER JOIN procurement_live_ve.procurement_order_payment c ON b.fk_procurement_order_payment = c.id_procurement_order_payment
SET a.date_paid = date(c.operation_date) ; 


UPDATE operations_ve.out_procurement_tracking a
INNER JOIN procurement_live_ve.procurement_order_item b ON a.id_procurement_order_item = b.id_procurement_order_item
SET 
 inbound_type = CASE
									WHEN b.transport_type = 2 
									THEN 'FOB'
									ELSE 'CIF'
									END ; 


UPDATE operations_ve.out_procurement_tracking a
INNER JOIN bob_live_mx.catalog_simple b ON a.id_catalog_simple = b.id_catalog_simple
INNER JOIN bob_live_mx.catalog_shipment_type c ON b.fk_catalog_shipment_type = c.id_catalog_shipment_type
SET a.fullfilment_type_bob = c. NAME ; UPDATE operations_ve.out_procurement_tracking a
INNER JOIN operations_ve.out_stock_hist b ON a.stock_item_id = b.stock_item_id
SET 
 a.fullfilment_type_real = b.fullfilment_type_real,
 a.fullfilment_type_bp = b.fullfilment_type_bp ; 


UPDATE operations_ve.out_procurement_tracking a
INNER JOIN procurement_live_ve.procurement_order_item_date_history b ON a.id_procurement_order_item = b.fk_procurement_order_item
SET 
 a.date_collection_scheduled = date(collect_scheduled_date),
 a.date_collection_negotiated = date(collect_negotiated_date),
 a.date_collection_real = date(collect_real_date),
 a.date_delivery_calculated_bob = date(delivery_bob_calculated_date),
 a.date_delivery_scheduled = date(delivery_scheduled_date),
 a.date_delivery_real = date(delivery_real_date) ; 


UPDATE operations_ve.out_procurement_tracking a
INNER JOIN wmsprod.estoque b ON a.stock_item_id = b.estoque_id
SET 
 a.date_exit_wh = date(data_ultima_movimentacao),
 a.exit_type = "sold"
WHERE
	a.wh_location = "vendidos" ; 

UPDATE operations_ve.out_procurement_tracking a
INNER JOIN wmsprod.estoque b ON a.stock_item_id = b.estoque_id
SET a.date_exit_wh = date(data_ultima_movimentacao),
 a.exit_type = "error"
WHERE
	a.wh_location = "error" ; 


UPDATE operations_ve.out_procurement_tracking a
INNER JOIN bob_live_mx.supplier_address b ON a.supplier_id = b.fk_supplier
SET 
 a.pick_at_zip = b.postcode ; 

UPDATE operations_ve.out_procurement_tracking a
SET 
 a.pick_at_zip2 = LEFT (pick_at_zip, 2),
 a.pick_at_zip3 = LEFT (pick_at_zip, 3) ; 


UPDATE operations_ve.out_procurement_tracking
SET 
 week_po_created = operations_ve.week_iso (date_created),
 week_goods_received = operations_ve.week_iso (date_goods_received),
 week_payment = operations_ve.week_iso (date_paid) ; 


UPDATE operations_ve.out_procurement_tracking
SET 
 month_po_created = date_format(date_created, "%x-%m"),
 month_goods_received = date_format(date_goods_received,"%x-%m"),
 month_payment = date_format(date_paid, "%x-%m") ; 


UPDATE operations_ve.out_procurement_tracking a
INNER JOIN bob_live_mx.sales_order_item b ON a.order_item_id = b.id_sales_order_item
SET a.cost_bob = b.cost ; 

UPDATE operations_ve.out_procurement_tracking a
INNER JOIN bob_live_mx.catalog_simple b ON a.id_catalog_simple = b.id_catalog_simple
SET a.cost_bob = b.cost
WHERE
	a.cost_bob = 0 ; 






UPDATE operations_ve.out_procurement_tracking
SET 
 is_paid = 1,
 amount_paid = cost_oms
WHERE
	date_paid IS NOT NULL ; 


UPDATE operations_ve.out_procurement_tracking
SET 
 is_invoiced = 1
WHERE
 date_paid IS NOT NULL ; 


UPDATE operations_ve.out_procurement_tracking
SET 
 payment_terms_real = CASE
												WHEN date_paid IS NULL 
												THEN NULL
												ELSE
											 (CASE
													WHEN date_goods_received IS NULL 
													THEN datediff(date_paid, curdate())
													ELSE datediff(date_paid, date_goods_received)
												END)
											END ; 

UPDATE operations_ve.out_procurement_tracking
SET payment_terms_scheduled = CASE
																WHEN date_payment_scheduled IS NULL 
																THEN NULL
																ELSE (	CASE
																					WHEN date_goods_received IS NULL 
																					THEN datediff(date_payment_scheduled, curdate())
																					ELSE datediff(date_payment_scheduled,	date_goods_received)
																				END)
																END ; 

UPDATE operations_ve.out_procurement_tracking
SET 
 payment_terms_expected = CASE
														WHEN date_goods_received IS NULL 
														THEN datediff(date_payment_estimation,curdate())
													ELSE (	CASE
																		WHEN date_payment_promised IS NULL 
																		THEN datediff(date_payment_estimation,date_goods_received)
																	ELSE datediff(date_payment_promised,date_goods_received)
																		END)
													END ; 


UPDATE operations_ve.out_procurement_tracking a
INNER JOIN procurement_live_ve.procurement_order b ON a.id_procurement_order = b.id_procurement_order
INNER JOIN procurement_live_ve.procurement_order_type c ON b.fk_procurement_order_type = c.id_procurement_order_type
SET a.procurement_order_type = c.procurement_order_type ; 


UPDATE operations_ve.out_procurement_tracking
SET 
 goods_received_last_15 = 1
WHERE
	datediff(curdate(),date_goods_received) <= 15 ; 

UPDATE operations_ve.out_procurement_tracking
SET 
 goods_received_last_30 = 1
WHERE
	datediff(curdate(),date_goods_received) <= 30 ; 

UPDATE operations_ve.out_procurement_tracking
SET 
 goods_received_last_45 = 1
WHERE
	datediff(curdate(),date_goods_received) <= 45 ; 

UPDATE operations_ve.out_procurement_tracking
SET 
 paid_last_15 = 1
WHERE
 datediff(curdate(), date_paid) <= 15 ; 

UPDATE operations_ve.out_procurement_tracking
SET 
 paid_last_30 = 1
WHERE
 datediff(curdate(), date_paid) <= 30 ; 

UPDATE operations_ve.out_procurement_tracking
SET 
 paid_last_45 = 1
WHERE
	datediff(curdate(), date_paid) <= 45 ; 


UPDATE operations_ve.out_procurement_tracking
SET 
 cost_oms_gt_bob = 1
WHERE
 cost_oms > cost_bob ; 


UPDATE operations_ve.out_procurement_tracking
SET 
 cost_oms_bob_gap = cost_oms - cost_bob
WHERE
 cost_oms_gt_bob = 1 ; 


UPDATE operations_ve.out_procurement_tracking a
SET a.date_payment_programmed = CASE
																	WHEN a.is_confirmed = 1 
																	THEN date_payment_scheduled
																	ELSE (	CASE
																						WHEN a.is_received = 1 
																						THEN date_payment_promised
																						ELSE date_payment_estimation
																					END)
																	END
WHERE
	procurement_payment_event = "pedido" ; 

UPDATE operations_ve.out_procurement_tracking a
SET 
 a.date_payment_programmed = CASE
															WHEN a.is_invoiced = 1 
															THEN (CASE
																			WHEN a.is_confirmed = 1 
																			THEN date_payment_scheduled
																			ELSE date_payment_promised
																		END)
															ELSE (CASE
																			WHEN a.is_received = 1 
																			THEN date_payment_promised
																			ELSE date_payment_estimation
																		END)
															END
WHERE
	procurement_payment_event = "factura" ; 

UPDATE operations_ve.out_procurement_tracking a
SET 
 a.date_payment_programmed = CASE
															WHEN a.is_received = 1 
															THEN (CASE
																			WHEN a.is_confirmed = 1
																			AND a.is_invoiced = 1 
																			THEN date_payment_scheduled
																			ELSE date_payment_promised
																		END)
															ELSE date_payment_estimation
														 END
WHERE
	procurement_payment_event = "entrega" ; 


UPDATE operations_ve.out_procurement_tracking
SET payment_terms_programmed = datediff(date_payment_programmed,date_goods_received) ; 

UPDATE operations_ve.out_procurement_tracking
SET payment_terms = payment_terms_real ; 

UPDATE operations_ve.out_procurement_tracking
SET payment_terms = payment_terms_programmed
WHERE
	payment_terms IS NULL ; 

UPDATE operations_ve.out_procurement_tracking
SET payment_terms = payment_terms_scheduled
WHERE
	payment_terms IS NULL ; 

UPDATE operations_ve.out_procurement_tracking
SET payment_terms = payment_terms_scheduled
WHERE
	payment_terms IS NULL ; 

UPDATE operations_ve.out_procurement_tracking
SET payment_terms = payment_terms_expected
WHERE
	payment_terms IS NULL ;


UPDATE operations_ve.out_procurement_tracking
SET item_counter = 1;

DROP TABLE IF EXISTS production_ve.out_procurement_tracking;

CREATE TABLE production_ve.out_procurement_tracking LIKE operations_ve.out_procurement_tracking;

INSERT INTO production_ve.out_procurement_tracking SELECT * FROM operations_ve.out_procurement_tracking;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Venezuela', 
  'out_procurement_tracking',
  'finish',
  NOW(),
  max(date_created),
  count(*),
  count(item_counter)
FROM
  production_ve.out_procurement_tracking;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 PROCEDURE `out_stock_hist`()
BEGIN
	


INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Venezuela', 
  'out_stock_hist',
  'start',
  NOW(),
  max(date_exit),
  count(*),
  count(item_counter)
FROM
  production_ve.out_stock_hist;

TRUNCATE operations_ve.out_stock_hist;


INSERT INTO operations_ve.out_stock_hist (
	stock_item_id,
	barcode_wms,
	date_entrance,
	barcode_bob_duplicated,
	in_stock,
	wh_location
) SELECT
	estoque.estoque_id,
	estoque.cod_barras,
	CASE
WHEN data_criacao IS NULL THEN
	NULL
ELSE
	date(data_criacao)
END AS expr1,
 estoque.minucioso,
 posicoes.participa_estoque,
 estoque.endereco
FROM
	(
		wmsprod_ve.estoque
		LEFT JOIN wmsprod_ve.itens_recebimento ON estoque.itens_recebimento_id = itens_recebimento.itens_recebimento_id
	)
LEFT JOIN wmsprod_ve.posicoes ON estoque.endereco = posicoes.posicao;


UPDATE operations_ve.out_stock_hist
INNER JOIN wmsprod_ve.movimentacoes ON out_stock_hist.stock_item_id = movimentacoes.estoque_id
SET out_stock_hist.date_entrance = date(movimentacoes.data_criacao)
WHERE
	movimentacoes.de_endereco = 'retorno de cancelamento'
OR movimentacoes.de_endereco = 'vendidos';


DELETE operations_ve.pro_unique_ean_bob.*
FROM
	operations_ve.pro_unique_ean_bob;


INSERT INTO operations_ve.pro_unique_ean_bob (ean) SELECT
	produtos.ean AS ean
FROM
	bob_live_ve.catalog_simple
INNER JOIN wmsprod_ve.produtos ON catalog_simple.sku = produtos.sku
WHERE
	catalog_simple. STATUS NOT LIKE "deleted"
GROUP BY
	produtos.ean
HAVING
	count(produtos.ean = 1);


UPDATE operations_ve.out_stock_hist
INNER JOIN wmsprod_ve.traducciones_producto ON out_stock_hist.barcode_wms = traducciones_producto.identificador
SET out_stock_hist.sku_simple = sku;


UPDATE operations_ve.out_stock_hist
INNER JOIN wmsprod_ve.estoque ON out_stock_hist.stock_item_id = estoque.estoque_id
SET out_stock_hist.date_exit = date(data_ultima_movimentacao),
 out_stock_hist.exit_type = "sold"
WHERE
	out_stock_hist.wh_location = "vendidos";


UPDATE operations_ve.out_stock_hist
INNER JOIN wmsprod_ve.estoque ON out_stock_hist.stock_item_id = estoque.estoque_id
SET out_stock_hist.date_exit = date(data_ultima_movimentacao),
 out_stock_hist.exit_type = "error"
WHERE
	out_stock_hist.wh_location = "error";


UPDATE (
	(
		(
			operations_ve.out_stock_hist
			INNER JOIN bob_live_ve.catalog_simple ON out_stock_hist.sku_simple = catalog_simple.sku
		)
		INNER JOIN bob_live_ve.catalog_config ON catalog_simple.fk_catalog_config = catalog_config.id_catalog_config
	)
	INNER JOIN bob_live_ve.catalog_shipment_type ON catalog_simple.fk_catalog_shipment_type = catalog_shipment_type.id_catalog_shipment_type
)
INNER JOIN bob_live_ve.supplier ON catalog_config.fk_supplier = supplier.id_supplier
SET out_stock_hist.barcode_bob = barcode_ean,
 out_stock_hist.sku_config = catalog_config.sku,
 out_stock_hist.sku_name = catalog_config. NAME,
 out_stock_hist.sku_supplier_config = catalog_config.sku_supplier_config,
 out_stock_hist.model = catalog_config.model,
 out_stock_hist.cost = catalog_simple.cost,
 out_stock_hist.supplier_name = supplier. NAME,
 out_stock_hist.supplier_id = supplier.id_supplier,
 out_stock_hist.fullfilment_type_bob = catalog_shipment_type. NAME,
 out_stock_hist.price = catalog_simple.price;


UPDATE operations_ve.out_stock_hist
INNER JOIN operations_ve.pro_category_tree ON out_stock_hist.sku_config = pro_category_tree.sku
SET out_stock_hist.category = cat1,
 out_stock_hist.cat2 = pro_category_tree.cat2;


UPDATE operations_ve.out_stock_hist
INNER JOIN operations_ve.exceptions_supplier_categories ON out_stock_hist.supplier_name = exceptions_supplier_categories.supplier
SET out_stock_hist.category = "ropa, calzado y accesorios"
WHERE
	out_stock_hist.category = "deportes";


UPDATE operations_ve.out_stock_hist
SET out_stock_hist.category = "salud y cuidado personal"
WHERE
	out_stock_hist.category = "ropa, calzado y accesorios"
AND out_stock_hist.cat2 = "salud y cuidado personal";


UPDATE operations_ve.out_stock_hist
INNER JOIN bob_live_ve.catalog_config ON out_stock_hist.sku_config = catalog_config.sku
INNER JOIN bob_live_ve.catalog_attribute_option_global_buyer ON catalog_attribute_option_global_buyer.id_catalog_attribute_option_global_buyer = catalog_config.fk_catalog_attribute_option_global_buyer
SET out_stock_hist.buyer = catalog_attribute_option_global_buyer. NAME;


UPDATE operations_ve.out_stock_hist
INNER JOIN operations_ve.buyer_category ON out_stock_hist.category = buyer_category.category
SET out_stock_hist.category_english = buyer_category.category_english;


UPDATE operations_ve.out_stock_hist
SET out_stock_hist.sold_last_30 = CASE
WHEN datediff(curdate(), date_exit) < 30 THEN
	1
ELSE
	0
END,
 out_stock_hist.sold_yesterday = CASE
WHEN datediff(
	curdate(),
	(
		date_sub(date_exit, INTERVAL 1 DAY)
	)
) = 0 THEN
	1
ELSE
	0
END,
 out_stock_hist.sold_last_10 = CASE
WHEN datediff(curdate(), date_exit) < 10 THEN
	1
ELSE
	0
END,
 out_stock_hist.sold_last_7 = CASE
WHEN datediff(curdate(), date_exit) < 7 THEN
	1
ELSE
	0
END
WHERE
	out_stock_hist.exit_type = "sold";


UPDATE operations_ve.out_stock_hist
SET out_stock_hist.entrance_last_30 = CASE
WHEN datediff(curdate(), date_entrance) < 30 THEN
	1
ELSE
	0
END,
 out_stock_hist.oms_po_last_30 = CASE
WHEN datediff(
	curdate(),
	date_procurement_order
) < 30 THEN
	1
ELSE
	0
END;


UPDATE operations_ve.out_stock_hist
SET out_stock_hist.days_in_stock = CASE
WHEN date_exit IS NULL THEN
	datediff(curdate(), date_entrance)
ELSE
	datediff(date_exit, date_entrance)
END;


UPDATE operations_ve.out_stock_hist
SET out_stock_hist.fullfilment_type_real = "inventory";


UPDATE operations_ve.out_stock_hist
INNER JOIN (
	wmsprod_ve.itens_venda
	INNER JOIN wmsprod_ve.status_itens_venda ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
) ON out_stock_hist.stock_item_id = itens_venda.estoque_id
SET out_stock_hist.fullfilment_type_real = "crossdock"
WHERE
	status_itens_venda. STATUS = "analisando quebra"
OR status_itens_venda. STATUS = "aguardando estoque";


UPDATE (
	operations_ve.out_stock_hist
	INNER JOIN wmsprod_ve.itens_venda ON out_stock_hist.stock_item_id = itens_venda.estoque_id
)
INNER JOIN wmsprod_ve.status_itens_venda ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
SET out_stock_hist.fullfilment_type_real = "dropshipping"
WHERE
	(
		(
			(status_itens_venda. STATUS) = "ds estoque reservado"
		)
	);


UPDATE operations_ve.out_stock_hist
INNER JOIN wmsprod_ve.movimentacoes ON out_stock_hist.stock_item_id = movimentacoes.estoque_id
SET out_stock_hist.fullfilment_type_real = 'inventory'
WHERE
	movimentacoes.de_endereco = 'retorno de cancelamento'
OR movimentacoes.de_endereco = 'vendidos';


UPDATE operations_ve.out_stock_hist
INNER JOIN bob_live_ve.catalog_simple ON out_stock_hist.sku_simple = catalog_simple.sku
INNER JOIN bob_live_ve.catalog_tax_class ON catalog_simple.fk_catalog_tax_class = catalog_tax_class.id_catalog_tax_class
SET out_stock_hist.tax_percentage = tax_percent,
 out_stock_hist.cost_w_o_vat = out_stock_hist.cost / (1 + tax_percent / 100);


UPDATE operations_ve.out_stock_hist
SET out_stock_hist.in_stock_cost_w_o_vat = out_stock_hist.cost_w_o_vat
WHERE
	out_stock_hist.in_stock = 1;


UPDATE operations_ve.out_stock_hist
SET out_stock_hist.sold_last_30_cost_w_o_vat = out_stock_hist.cost_w_o_vat,
 out_stock_hist.sold_last_30_price = out_stock_hist.price
WHERE
	out_stock_hist.sold_last_30 = 1;


DELETE operations_ve.pro_stock_hist_sku_count.*
FROM
	operations_ve.pro_stock_hist_sku_count;


INSERT INTO operations_ve.pro_stock_hist_sku_count (sku_simple, sku_count) SELECT
	out_stock_hist.sku_simple,
	sum(
		out_stock_hist.item_counter
	) AS sumofitem_counter
FROM
	operations_ve.out_stock_hist
GROUP BY
	out_stock_hist.sku_simple,
	out_stock_hist.in_stock
HAVING
	out_stock_hist.in_stock = 1;


UPDATE operations_ve.out_stock_hist
INNER JOIN operations_ve.pro_stock_hist_sku_count ON out_stock_hist.sku_simple = pro_stock_hist_sku_count.sku_simple
SET out_stock_hist.sku_counter = 1 / pro_stock_hist_sku_count.sku_count;


UPDATE operations_ve.out_stock_hist
SET out_stock_hist.reserved = '0';


UPDATE operations_ve.out_stock_hist
INNER JOIN wmsprod_ve.estoque ON out_stock_hist.stock_item_id = estoque.estoque_id
SET out_stock_hist.reserved = "1"
WHERE
	(
		(
			(estoque.almoxarifado) = "separando"
			OR (estoque.almoxarifado) = "estoque reservado"
			OR (estoque.almoxarifado) = "aguardando separacao"
		)
	);


UPDATE operations_ve.out_stock_hist
INNER JOIN bob_live_ve.catalog_simple ON out_stock_hist.sku_simple = catalog_simple.sku
SET out_stock_hist.supplier_leadtime = catalog_simple.delivery_time_supplier;


UPDATE operations_ve.out_stock_hist
INNER JOIN (
	(
		bob_live_ve.catalog_config
		INNER JOIN bob_live_ve.catalog_simple ON catalog_config.id_catalog_config = catalog_simple.fk_catalog_config
	)
	INNER JOIN bob_live_ve.catalog_stock ON catalog_simple.id_catalog_simple = catalog_stock.fk_catalog_simple
) ON out_stock_hist.sku_config = catalog_config.sku
SET out_stock_hist.visible = 1
WHERE
	(
		(
			(catalog_config.pet_status) = "creation,edited,images"
		)
		AND (
			(
				catalog_config.pet_approved
			) = 1
		)
		AND (
			(catalog_config. STATUS) = "active"
		)
		AND (
			(catalog_simple. STATUS) = "active"
		)
		AND ((catalog_simple.price) > 0)
		AND (
			(
				catalog_config.display_if_out_of_stock
			) = 0
		)
		AND ((catalog_stock.quantity) > 0)
		AND ((catalog_simple.cost) > 0)
	)
OR (
	(
		(catalog_config.pet_status) = "creation,edited,images"
	)
	AND (
		(
			catalog_config.pet_approved
		) = 1
	)
	AND (
		(catalog_config. STATUS) = "active"
	)
	AND (
		(catalog_simple. STATUS) = "active"
	)
	AND ((catalog_simple.price) > 0)
	AND (
		(
			catalog_config.display_if_out_of_stock
		) = 1
	)
	AND ((catalog_stock.quantity) > 0)
	AND ((catalog_simple.cost) > 0)
);


UPDATE operations_ve.out_stock_hist
SET out_stock_hist.week_exit = operations_ve.week_iso (date_exit),
out_stock_hist.week_entrance = operations_ve.week_iso (date_entrance);


DELETE operations_ve.pro_stock_hist_sold_last_30_count.*
FROM
	operations_ve.pro_stock_hist_sold_last_30_count;


INSERT INTO operations_ve.pro_stock_hist_sold_last_30_count (
	sold_last_30,
	sku_simple,
	count_of_item_counter
) SELECT
	out_stock_hist.sold_last_30,
	out_stock_hist.sku_simple,
	count(
		out_stock_hist.item_counter
	) AS count_of_item_counter
FROM
	operations_ve.out_stock_hist
GROUP BY
	out_stock_hist.sold_last_30,
	out_stock_hist.sku_simple
HAVING
	out_stock_hist.sold_last_30 = 1;


UPDATE operations_ve.out_stock_hist
INNER JOIN operations_ve.pro_stock_hist_sold_last_30_count ON out_stock_hist.sku_simple = pro_stock_hist_sold_last_30_count.sku_simple
SET out_stock_hist.sold_last_30_counter = 1 / pro_stock_hist_sold_last_30_count.count_of_item_counter;


UPDATE operations_ve.out_stock_hist
SET out_stock_hist.sku_simple_blank = 1
WHERE
	out_stock_hist.sku_simple IS NULL;


UPDATE (
	wmsprod_ve.itens_recebimento
	INNER JOIN wmsprod_ve.estoque ON itens_recebimento.itens_recebimento_id = estoque.itens_recebimento_id
)
INNER JOIN operations_ve.out_stock_hist ON estoque.estoque_id = out_stock_hist.stock_item_id
SET out_stock_hist.quarantine = 1
WHERE
	itens_recebimento.endereco = "cuarentena"
OR itens_recebimento.endereco = "cuarenta2";


DELETE operations_ve.pro_sum_last_5_out_of_6_wks.*
FROM
	operations_ve.pro_sum_last_5_out_of_6_wks;


INSERT INTO operations_ve.pro_sum_last_5_out_of_6_wks (sku_simple, expr1) SELECT
	max_last_6_wks.sku_simple,
	sumofcountofdate_exit - maxofcountofdate_exit AS expr1
FROM
	operations_ve.max_last_6_wks
GROUP BY
	max_last_6_wks.sku_simple,
	sumofcountofdate_exit - maxofcountofdate_exit;


UPDATE operations_ve.out_stock_hist
INNER JOIN operations_ve.pro_sum_last_5_out_of_6_wks ON out_stock_hist.sku_simple = pro_sum_last_5_out_of_6_wks.sku_simple
SET out_stock_hist.sum_sold_last_5_out_of_6_wks = pro_sum_last_5_out_of_6_wks.expr1;


UPDATE operations_ve.out_stock_hist
INNER JOIN bob_live_ve.catalog_config ON out_stock_hist.sku_config = catalog_config.sku
SET out_stock_hist.package_height = catalog_config.package_height,
 out_stock_hist.package_length = catalog_config.package_length,
 out_stock_hist.package_width = catalog_config.package_width;


UPDATE operations_ve.out_stock_hist
SET out_stock_hist.vol_weight = package_height * package_length * package_width / 5000;


UPDATE operations_ve.out_stock_hist
INNER JOIN bob_live_ve.catalog_config ON out_stock_hist.sku_config = catalog_config.sku
SET out_stock_hist.package_weight = catalog_config.package_weight;


UPDATE operations_ve.out_stock_hist
SET out_stock_hist.max_vol_w_vs_w = CASE
WHEN vol_weight > package_weight THEN
	vol_weight
ELSE
	package_weight
END;


UPDATE operations_ve.out_stock_hist
SET out_stock_hist.package_measure = CASE
WHEN max_vol_w_vs_w > 45.486 THEN
	"large"
ELSE
	(
		CASE
		WHEN max_vol_w_vs_w > 2.277 THEN
			"medium"
		ELSE
			"small"
		END
	)
END;

UPDATE operations_ve.out_stock_hist
SET out_stock_hist.package_measure_new = 	CASE
																						WHEN max_vol_w_vs_w > 35 
																						THEN 'oversized'
																					ELSE
																					(	CASE
																							WHEN max_vol_w_vs_w > 5 
																							THEN 'large'
																							ELSE
																							(	CASE
																									WHEN max_vol_w_vs_w > 2 
																									THEN 'medium'
																									ELSE 'small'
																								END)
																							END)
																					END;

UPDATE operations_ve.out_stock_hist
INNER JOIN wmsprod_ve.itens_inventario ON out_stock_hist.barcode_wms = itens_inventario.cod_barras
SET out_stock_hist.sub_position = sub_endereco;


UPDATE wmsprod_ve.posicoes
INNER JOIN operations_ve.out_stock_hist ON posicoes.posicao = out_stock_hist.wh_location
SET out_stock_hist.position_type = posicoes.tipo_posicao;


UPDATE operations_ve.out_stock_hist
SET out_stock_hist.position_item_size_type = CASE
WHEN position_type = "safe" THEN
	"small"
ELSE
	(
		CASE
		WHEN position_type = "mezanine" THEN
			"small"
		ELSE
			(
				CASE
				WHEN position_type = "muebles" THEN
					"large"
				ELSE
					"tbd"
				END
			)
		END
	)
END;


DELETE
FROM
	operations_ve.items_procured_in_transit;


INSERT INTO operations_ve.items_procured_in_transit (
	sku_simple,
	number_ordered,
	unit_price
) SELECT
	catalog_simple.sku,
	count(
		procurement_order_item.id_procurement_order_item
	) AS countofid_procurement_order_item,
	avg(
		procurement_order_item.unit_price
	) AS avgofunit_price
FROM
	bob_live_ve.catalog_simple
INNER JOIN (
	procurement_live_ve.procurement_order_item
	INNER JOIN procurement_live_ve.procurement_order ON procurement_order_item.fk_procurement_order = procurement_order.id_procurement_order
) ON catalog_simple.id_catalog_simple = procurement_order_item.fk_catalog_simple
WHERE
	(
		(
			(
				procurement_order_item.is_deleted
			) = 0
		)
		AND (
			(
				procurement_order.is_cancelled
			) = 0
		)
		AND (
			(
				procurement_order_item.sku_received
			) = 0
		)
	)
GROUP BY
	catalog_simple.sku;


UPDATE operations_ve.out_stock_hist
INNER JOIN operations_ve.items_procured_in_transit ON out_stock_hist.sku_simple = items_procured_in_transit.sku_simple
SET out_stock_hist.items_procured_in_transit = number_ordered,
 out_stock_hist.procurement_price = unit_price;


UPDATE (
	procurement_live_ve.procurement_order
	INNER JOIN (
		(
			bob_live_ve.catalog_simple
			INNER JOIN operations_ve.out_stock_hist ON catalog_simple.sku = out_stock_hist.sku_simple
		)
		INNER JOIN procurement_live_ve.procurement_order_item ON catalog_simple.id_catalog_simple = procurement_order_item.fk_catalog_simple
	) ON procurement_order.id_procurement_order = procurement_order_item.fk_procurement_order
)
SET out_stock_hist.date_procurement_order = procurement_order.created_at;


UPDATE operations_ve.out_stock_hist
SET out_stock_hist.date_payable_promised = adddate(
	date_procurement_order,
	INTERVAL oms_payment_terms DAY
)
WHERE
	out_stock_hist.oms_payment_event IN ("factura", "pedido");


UPDATE operations_ve.out_stock_hist
SET out_stock_hist.date_payable_promised = adddate(
	date_procurement_order,
	INTERVAL oms_payment_terms + supplier_leadtime DAY
)
WHERE
	out_stock_hist.oms_payment_event = "entrega";



UPDATE operations_ve.out_stock_hist
SET out_stock_hist.sold = 1
WHERE
	out_stock_hist.exit_type = "sold";


DELETE
FROM
	operations_ve.sell_rate_simple;


INSERT INTO operations_ve.sell_rate_simple (
	sku_simple,
	num_items,
	num_sales,
	average_days_in_stock
) SELECT
	sku_simple,
	sum(item_counter),
	sum(sold),
	avg(days_in_stock)
FROM
	operations_ve.out_stock_hist
GROUP BY
	sku_simple;


UPDATE operations_ve.sell_rate_simple srs
SET srs.num_items_available = (
	SELECT
		sum(in_stock)
	FROM
		operations_ve.out_stock_hist osh
	WHERE
		reserved = '0'
	AND srs.sku_simple = osh.sku_simple
	GROUP BY
		sku_simple
);


UPDATE operations_ve.sell_rate_simple
SET sell_rate_simple.average_sell_rate = CASE
WHEN average_days_in_stock = 0 THEN
	0
ELSE
	num_sales / average_days_in_stock
END;


UPDATE operations_ve.sell_rate_simple
SET sell_rate_simple.remaining_days = CASE
WHEN average_sell_rate = 0 THEN
	0
ELSE
	num_items_available / average_sell_rate
END;


UPDATE operations_ve.out_stock_hist
INNER JOIN operations_ve.sell_rate_simple ON out_stock_hist.sku_simple = sell_rate_simple.sku_simple
SET out_stock_hist.average_remaining_days = sell_rate_simple.remaining_days;


DELETE
FROM
	operations_ve.pro_max_days_in_stock;


INSERT INTO operations_ve.pro_max_days_in_stock SELECT
	*
FROM
	(
		SELECT
			sku_simple,
			days_in_stock
		FROM
			operations_ve.out_stock_hist
		ORDER BY
			sku_simple,
			days_in_stock DESC
	) n
GROUP BY
	sku_simple;


UPDATE operations_ve.out_stock_hist
INNER JOIN operations_ve.pro_max_days_in_stock ON out_stock_hist.sku_simple = pro_max_days_in_stock.sku_simple
SET out_stock_hist.max_days_in_stock = pro_max_days_in_stock.max_days_in_stock;


UPDATE operations_ve.out_stock_hist
SET out_stock_hist.week_payable_promised = operations_ve.week_iso (date_payable_promised);


UPDATE operations_ve.out_stock_hist
SET out_stock_hist.week_procurement_order = operations_ve.week_iso (date_procurement_order);


UPDATE operations_ve.out_stock_hist
INNER JOIN bob_live_ve.catalog_simple ON out_stock_hist.sku_simple = catalog_simple.sku
SET out_stock_hist.delivery_cost_supplier = catalog_simple.delivery_cost_supplier;


UPDATE operations_ve.out_stock_hist
SET cogs = cost_w_o_vat + delivery_cost_supplier;


UPDATE operations_ve.out_stock_hist
SET out_stock_hist.days_payable_since_entrance = CASE
WHEN date_entrance IS NULL THEN
	NULL
ELSE
	(
		CASE
		WHEN oms_payment_event IN ('factura', 'pedido') THEN
			oms_payment_terms - supplier_leadtime
		ELSE
			(
				CASE
				WHEN oms_payment_event = 'entrega' THEN
					oms_payment_terms
				END
			)
		END
	)
END;


UPDATE operations_ve.out_stock_hist a
INNER JOIN bob_live_ve.catalog_config b ON a.sku_config = b.sku
INNER JOIN bob_live_ve.catalog_attribute_option_global_category c ON b.fk_catalog_attribute_option_global_category = c.id_catalog_attribute_option_global_category
INNER JOIN bob_live_ve.catalog_attribute_option_global_sub_category d ON b.fk_catalog_attribute_option_global_sub_category = d.id_catalog_attribute_option_global_sub_category
INNER JOIN bob_live_ve.catalog_attribute_option_global_sub_sub_category e ON b.fk_catalog_attribute_option_global_sub_sub_category = e.id_catalog_attribute_option_global_sub_sub_category
SET a.category_com_main = c. NAME,
 a.category_com_sub = d. NAME,
 a.category_com_sub_sub = e. NAME;

UPDATE operations_ve.out_stock_hist a
INNER JOIN development_mx.A_E_M1_New_CategoryBP b ON a.category_com_sub = b.cat2
SET a.category_bp = b.CatBP ; 


UPDATE operations_ve.out_stock_hist set is_sell_list = 1
WHERE max_days_in_stock > 30
and in_stock= 1
and reserved= 0
and fullfilment_type_bob <> 'Consignment' 
and fullfilment_type_bob is not null
and category_com_main = 'Home and Living' 
and (average_remaining_days > 150
or average_remaining_days is null);

UPDATE operations_ve.out_stock_hist set is_sell_list = 1
where max_days_in_stock > 30
and in_stock= 1
and reserved= 0
and fullfilment_type_bob <> 'Consignment' 
and fullfilment_type_bob is not null
and category_com_main = 'Fashion' 
and (average_remaining_days > 120
or average_remaining_days is null);

UPDATE operations_ve.out_stock_hist set is_sell_list = 1
where max_days_in_stock > 30
and in_stock= 1
and reserved= 0
and fullfilment_type_bob <> 'Consignment' 
and fullfilment_type_bob is not null
and category_com_main = 'Electrónicos' 
and (average_remaining_days > 60
or average_remaining_days is null);

UPDATE operations_ve.out_stock_hist set is_sell_list = 1
where max_days_in_stock > 30
and in_stock= 1
and reserved= 0
and fullfilment_type_bob <> 'Consignment' 
and fullfilment_type_bob is not null
and category_com_main not in ('Home and Living','Fashion','Electrónicos') 
and (average_remaining_days > 90
or average_remaining_days is null);


SET @i = 0 ; 
DROP TEMPORARY TABLE IF EXISTS TMP ; 
CREATE TEMPORARY TABLE TMP 
SELECT
	sku_config,
	Count(*) AS items_in_stock,
	sum(cost_w_o_vat) AS cost_w_o_vat_in_stock
FROM
	operations_ve.out_stock_hist
WHERE
	in_stock = 1
AND reserved = 0
AND (
			fullfilment_type_bob <> 'consignment'
			OR fullfilment_type_bob IS NOT NULL
		)
GROUP BY
	sku_config;

DROP TABLE IF EXISTS operations_ve.top_skus_sell_list ; 
CREATE TABLE operations_ve.top_skus_sell_list 
SELECT
	sku_config,
	items_in_stock,
	cost_w_o_vat_in_stock,
	@i := @i + 1 AS rkn
FROM
	TMP
ORDER BY
	cost_w_o_vat_in_stock DESC ;

# Sell List

INSERT INTO operations_ve.pro_sell_list_hist_totals (
	date_reported,
	category_bp,
	cost_w_o_vat_over_two_items,
	cost_w_o_vat_under_two_items) 
SELECT
	J.date_reported,
	J.Categoria,
	J.Value_more_than_two,
	J.Value_less_than_two
FROM
	(	SELECT
			curdate() AS date_reported,
			B.category_bp AS 'Categoria',
			B.cost_w_o_vat AS Value_more_than_two,
			D.cost_w_o_vat AS Value_less_than_two
		FROM
			(	SELECT
					category_bp,
					sum(cost_w_o_vat) AS cost_w_o_vat
				FROM
					(	SELECT
							category_bp,
							sku_config,
							sum(cost_w_o_vat) AS cost_w_o_vat
						FROM
							operations_ve.out_stock_hist
						WHERE
							is_sell_list = 1
						AND category_bp IS NOT NULL
						GROUP BY
							sku_config
						HAVING
							count(sku_config) > 2
					) A
				GROUP BY
					category_bp
			) B
		LEFT JOIN (	SELECT
									category_bp,
									sum(cost_w_o_vat) AS cost_w_o_vat
								FROM
									(	SELECT
											category_bp,
											sum(cost_w_o_vat) AS cost_w_o_vat
										FROM
											operations_ve.out_stock_hist
										WHERE
											is_sell_list = 1
										GROUP BY
											sku_config
										HAVING
											count(sku_config) <= 2
									) C
								GROUP BY
									category_bp
									) D 
		ON B.category_bp = D.category_bp
	) J;




DROP TABLE IF EXISTS production_ve.out_stock_hist;

CREATE TABLE production_ve.out_stock_hist LIKE operations_ve.out_stock_hist; 

INSERT INTO production_ve.out_stock_hist SELECT * FROM operations_ve.out_stock_hist;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Venezuela', 
  'out_stock_hist',
  'finish',
  NOW(),
  max(date_exit),
  count(*),
  count(item_counter)
FROM
  production_ve.out_stock_hist;


DROP TABLE IF EXISTS production_ve.items_procured_in_transit;

CREATE TABLE production_ve.items_procured_in_transit LIKE operations_ve.items_procured_in_transit;

INSERT INTO production_ve.items_procured_in_transit SELECT * FROM operations_ve.items_procured_in_transit; 

DROP TABLE IF EXISTS production_ve.pro_sell_list_hist_totals;

CREATE TABLE production_ve.pro_sell_list_hist_totals LIKE operations_ve.pro_sell_list_hist_totals;

INSERT INTO production_ve.pro_sell_list_hist_totals SELECT * FROM operations_ve.pro_sell_list_hist_totals;

DROP TABLE IF EXISTS production_ve.top_skus_sell_list;

CREATE TABLE production_ve.top_skus_sell_list LIKE operations_ve.top_skus_sell_list;

INSERT INTO production_ve.top_skus_sell_list SELECT * FROM operations_ve.top_skus_sell_list;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:04:10
