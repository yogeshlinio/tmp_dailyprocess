-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: cs_performance_co
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'cs_performance_co'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `performance_back_office_cs_agents`()
BEGIN


SET @KPI_Month=cast(date_format(curdate(),"%Y%m")-1 as unsigned);
SET @Perfomance_max_date=cast((select max(date) from customer_service.tbl_zendesk_general
where date_format(date,"%Y%m%")=@KPI_Month) as date);


delete from performance_back_office_cs_agents
where MonthNum=@KPI_Month;

#Insertar datos de la matriz
insert into  performance_back_office_cs_agents 
(MonthNum, Agent, Identification, proceso, subproceso, coordinador, status, start_date, shift_pattern)
select @KPI_Month, name, identification, process, subprocess, coordinator, status, start_date, shift_pattern from bi_ops_matrix_sac_history 
where process='BACK OFFICE' and status='ACTIVO'
and date=@Perfomance_max_date;

#Actualizar datos de zendesk
update  performance_back_office_cs_agents a 
inner join (select date_format(Fecha_Solucion,"%Y%m") yrmonth, name, identification, sum(summation_column) tickets_solved, sum(reply_on_time_bo) tickets_replied_on_time,
sum(solved_on_time_bo) tickets_solved_on_time,
sum(calificados) rated, sum(calificados_bueno) rated_good FROM customer_service.tbl_zendesk_general a
inner join customer_service_co.bi_ops_matrix_sac b on a.Assignee=b.user_zendesk
where pais='CO'
and date_format(Fecha_Solucion,"%Y%m")=@KPI_Month
and time_to_reply_bo_hours is not null
group by identification) b 
on a.Identification=b.identification
set
total_tickets_solved_zendesk=tickets_solved,
total_reply_on_time_zendesk=tickets_replied_on_time,
total_tickets_solved_on_time_zendesk=tickets_solved_on_time,
survey_answered=rated,
survey_answered_good=rated_good
where MonthNum=@KPI_Month;

#Actualizar datos de reembolsos
update  performance_back_office_cs_agents a 
set
total_solved_not_zendesk=(select  count(1) refunded
 from operations_co.out_inverse_logistics_tracking
where (date_format(date_cash_refunded_il,"%Y%m")=@KPI_Month or date_format(date_voucher_refunded_il,"%Y%m")=@KPI_Month)
and workdays_to_refunded_il is not null),
total_solved_on_time_not_zendesk=(select 
sum(if(action_il='Consignación bancaria (7 dias hábiles)',if(workdays_to_refunded_il<=5,1,0),if(action_il='Efecty (7 días hábiles)',if(workdays_to_refunded_il<=4,1,0),
if(action_il='Reversión TC  (25-30 dias hábiles)',if(workdays_to_refunded_il<=20,1,0),if(action_il='Voucher (5 días hábiles)',if(workdays_to_refunded_il<=2,1,0),0))))) refunded_on_time 
 from operations_co.out_inverse_logistics_tracking
where (date_format(date_cash_refunded_il,"%Y%m")=@KPI_Month or date_format(date_voucher_refunded_il,"%Y%m")=@KPI_Month)
and workdays_to_refunded_il is not null)
where MonthNum=@KPI_Month
and subproceso like '%IL - REEMBOLSOS%';

#Actualizar datos de guías de logística inversa
update  performance_back_office_cs_agents a 
set
total_solved_not_zendesk=(select  count(1) cant_il from operations_co.out_inverse_logistics_tracking
where date_format(date_first_track_il,"%Y%m")=@KPI_Month
and workdays_to_track_il is not null),
total_solved_on_time_not_zendesk=(select  sum(if(workdays_to_track_il<=2,1,0)) cant_il_on_time from operations_co.out_inverse_logistics_tracking
where date_format(date_first_track_il,"%Y%m")=@KPI_Month
and workdays_to_track_il is not null)
where MonthNum=@KPI_Month
and subproceso like '%IL- GUIAS%';



#Agregar antiguedad de los asesores
update performance_back_office_cs_agents 
set
Antique='Nuevo'
where DATEDIFF(CAST(@Perfomance_max_date AS DATE),start_date)<31
and MonthNum=@KPI_Month;

#Eliminar datos que no aparecen en la matriz de agentes
delete from performance_back_office_cs_agents
where shift_pattern is null
and MonthNum=@KPI_Month;

#Actualizar horas de los turnos
update  performance_back_office_cs_agents a 
inner join customer_service_co.bi_ops_cc_shift_details b 
on a.shift_pattern=b.shift
set shift_hours=turno_diario
where MonthNum=@KPI_Month;

#Datos de adherencia 1
update performance_back_office_cs_agents a 
inner join (select cedula, sum(tiempo_conexion_segundos)/3600 conexion, (sum(total_break_segundos)+
sum(total_baño_segundos)+sum(total_capacitacion_segundos)+sum(total_almuerzo_segundos) +
sum(total_feedback_segundos)+sum(total_otros_procesos_segundos))/3600 total_pausas,
sum(tiempo_horario_segundos)/3600 horario, sum(inasistencia) inasistencias, sum(retraso) retrasos from customer_service_co.bi_ops_cc_adherencia_sac
where date_format(fecha,"%Y%m%")=@KPI_Month
group by cedula) b 
on a.Identification=b.cedula
set
conection_hours_month=conexion,
pauses_hours_month=total_pausas,
conection_available_hours_month=horario,
total_absence=inasistencias,
total_delays=retrasos
where MonthNum=@KPI_Month;

#Datos de adherencia 2
update performance_back_office_cs_agents a 
inner join (select cedula, sum(tiempo_conexion_segundos)/3600 conexion, (sum(total_break_segundos)+
sum(total_baño_segundos)+sum(total_capacitacion_segundos)+sum(total_almuerzo_segundos) +
sum(total_feedback_segundos)+sum(total_otros_procesos_segundos))/3600 total_pausas,
sum(tiempo_horario_segundos)/3600 horario, sum(inasistencia) inasistencias, sum(retraso) retrasos from customer_service_co.bi_ops_cc_adherencia_sac
where Semana in (select semana from
(select semana, count(distinct(fecha)) cant from  customer_service_co.bi_ops_cc_adherencia_sac
where date_format(fecha,"%Y%m%")=@KPI_Month
group by semana) t
where cant=7)
group by cedula) b 
on a.Identification=b.cedula
set
conection_hours_4weeks=conexion,
pauses_hours_4weeks=total_pausas,
conection_available_hours_4weeks=horario
where MonthNum=@KPI_Month;

#Pausas disponibles
update  performance_back_office_cs_agents
set pauses_available_hours_month=if(shift_hours=8,740/60,410/60) 
where MonthNum=@KPI_Month;

#Datos de calidad
update performance_back_office_cs_agents a 
inner join (select agente_evaluado,evaluador, count(1) monitoreos, sum(if(error_critico=100,0,1)) errores_criticos, sum(error_no_critico) sum_nota
 from customer_service_co.tbl_bi_ops_cc_calidad_formato_bo
where date_format(fechahora,"%Y%m%")=@KPI_Month
group by agente_evaluado, evaluador) b 
on a.Identification=b.agente_evaluado
set
total_monitoring=monitoreos,
critical_mistakes=errores_criticos,
sum_calification_not_critical=sum_nota
where MonthNum=@KPI_Month;

#Nota de training
update  performance_back_office_cs_agents a
inner join customer_service_co.bi_ops_cc_training b
on a.Identification=b.cedula and a.MonthNum=b.mes
set
training_calification=nota
where MonthNum=@KPI_Month;

#Valores Indicadores
update performance_back_office_cs_agents
set
value_first_reply_time=if(total_solved_not_zendesk=0,total_reply_on_time_zendesk/total_tickets_solved_zendesk,0),
value_solution=if(total_solved_not_zendesk=0,total_tickets_solved_on_time_zendesk/total_tickets_solved_zendesk,total_solved_on_time_not_zendesk/total_solved_not_zendesk),
value_quality=((total_monitoring-critical_mistakes)*100/total_monitoring)*0.6+(sum_califications_not_critical/total_monitoring)*0.4,
value_training=training_calification,
value_survey=survey_answered_good/survey_answered,
value_absence=total_absence,
value_delays=total_delays
where MonthNum=@KPI_Month;

#Actualizar KPI
update performance_back_office_cs_agents
set
kpi_adherence=if(value_adherence>=0.98,1,if(value_adherence>=0.95,0.5,0)) ,
kpi_first_reply_time=value_first_reply_time,
kpi_solution=value_solution,
kpi_quality=if(value_quality>=90,value_quality/100,if(value_quality>=80,0.7,if(value_quality>=70,0.5,0))),
kpi_training=if(value_training>=90,value_training/100,if(value_training>=80,0.7,if(value_training>=70,0.5,0))),
kpi_survey=value_survey,
kpi_absence=if(value_absence=0,1,if(value_absence=1,0.5,0)),
kpi_delays=if(value_delays=0,1,if(value_delays=1,0.9,if(value_delays=2,0.8,0)))
where MonthNum=@KPI_Month;

#Principales KPI del mes
update performance_back_office_cs_agents
set
kpi_productivity=if(total_solved_not_zendesk=0,kpi_adherence*0.25+kpi_first_reply_time*0.35+kpi_solution*0.4,kpi_adherence*(0.25/0.65)+kpi_solution*(0.4/0.65)),
kpi_quality_training_nsu=if(total_solved_not_zendesk=0,kpi_quality*0.5+kpi_training*0.3+kpi_survey*0.2,kpi_quality*(0.5/0.8)+kpi_training*(0.3/0.8)),
kpi_absences_and_delays=kpi_absence*0.5+kpi_delays*0.5
where MonthNum=@KPI_Month;

#Indicador General del Mes
update performance_back_office_cs_agents
set
kpi_general=kpi_productivity*0.4+kpi_quality_training_nsu*0.5+kpi_absences_and_delays*0.1
where MonthNum=@KPI_Month;

#Borrar datos de asesores de Chats si existen
delete from perfomance_agents_general
where yrmonth=@KPI_Month
and proceso='BACK OFFICE';

#Agregar datos a la evaluación general
insert into perfomance_agents_general
select MonthNum, null, Identification, Agent, coordinador, 0, kpi_productivity, kpi_quality_training_nsu, kpi_absences_and_delays,
kpi_general, 'BACK OFFICE'  from performance_back_office_cs_agents
where status='ACTIVO' and proceso='BACK OFFICE'
and kpi_general is not null
and MonthNum=@KPI_Month;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `performance_chat_cs_agents`()
BEGIN


SET @KPI_Month=cast(date_format(curdate(),"%Y%m")-1 as unsigned);
SET @Perfomance_max_date=cast((select max(date) from customer_service_co.tbl_bi_ops_cc_chats_performance_agents
where date_format(date,"%Y%m%")=@KPI_Month) as date);


delete from performance_inbound_cs_agents
where MonthNum=@KPI_Month;


#Insertar datos de llamadas de la base de live chat
insert into performance_chat_cs_agents (MonthNum, Agent, Identification, total_chats, total_duration_chats_sec,  
survey_answered, survey_answered_good)
select date_format(date,"%Y%m") MonthNum,name_matrix Agent, agent_identification Identification, 
sum(chats) total_chats, sum(total_chatting_sec) total_chatting_sec,
sum(rated) survey_answered, sum(rated_good) survey_answered_good
from customer_service_co.tbl_bi_ops_cc_chats_performance_agents
where  date_format(date,"%Y%m")=@KPI_Month
and agent_identification is not null
group by agent_identification
order by name_matrix asc;

#Actualizar
update performance_chat_cs_agents a 
inner join bi_ops_matrix_sac_history b 
on a.Identification=b.identification
set
a.start_date=b.start_date,
a.shift_pattern=b.shift_pattern,
a.proceso=b.process,
a.coordinador=b.coordinator,
a.status=b.status
where MonthNum=@KPI_Month
and b.date=@Perfomance_max_date;

#Agregar antiguedad de los asesores
update performance_chat_cs_agents 
set
Antique='Nuevo'
where DATEDIFF(CAST(@Perfomance_max_date AS DATE),start_date)<31
and MonthNum=@KPI_Month;

#Eliminar datos que no aparecen en la matriz de agentes
delete from performance_chat_cs_agents
where shift_pattern is null
and MonthNum=@KPI_Month;

#Actualizar horas de los turnos
update  performance_chat_cs_agents a 
inner join customer_service_co.bi_ops_cc_shift_details b 
on a.shift_pattern=b.shift
set shift_hours=turno_diario
where MonthNum=@KPI_Month;

#Actualizar datos del activity code
update  performance_chat_cs_agents a 
inner join (SELECT t.yrmonth,
t.username, count(1) canti FROM 
(SELECT date_format(date_time,'%Y%m') yrmonth,
a.macro_proceso,a.telefono_email,c.username,c.email,c.phone,c.canal 
FROM customer_service_co.activities a 
left join customer_service_co.users c on a.user_id=c.id where canal='telefono' ) as t
where t.phone is not null and t.phone<>'0000'
group by t.yrmonth,t.username) b 
on a.Identification=b.username
and a.MonthNum=b.yrmonth
set a.activity_chats=b.canti
where MonthNum=@KPI_Month;

#Datos de adherencia 1
update performance_chat_cs_agents a 
inner join (select cedula, sum(tiempo_conexion_segundos)/3600 conexion, (sum(total_break_segundos)+
sum(total_baño_segundos)+sum(total_capacitacion_segundos)+sum(total_almuerzo_segundos) +
sum(total_feedback_segundos)+sum(total_otros_procesos_segundos))/3600 total_pausas,
sum(tiempo_horario_segundos)/3600 horario, sum(inasistencia) inasistencias, sum(retraso) retrasos from customer_service_co.bi_ops_cc_adherencia_sac
where date_format(fecha,"%Y%m%")=@KPI_Month
group by cedula) b 
on a.Identification=b.cedula
set
conection_hours_month=conexion,
pauses_hours_month=total_pausas,
conection_available_hours_month=horario,
total_absence=inasistencias,
total_delays=retrasos
where MonthNum=@KPI_Month;

#Datos de adherencia 2
update performance_chat_cs_agents a 
inner join (select cedula, sum(tiempo_conexion_segundos)/3600 conexion, (sum(total_break_segundos)+
sum(total_baño_segundos)+sum(total_capacitacion_segundos)+sum(total_almuerzo_segundos) +
sum(total_feedback_segundos)+sum(total_otros_procesos_segundos))/3600 total_pausas,
sum(tiempo_horario_segundos)/3600 horario, sum(inasistencia) inasistencias, sum(retraso) retrasos from customer_service_co.bi_ops_cc_adherencia_sac
where Semana in (select semana from
(select semana, count(distinct(fecha)) cant from  customer_service_co.bi_ops_cc_adherencia_sac
where date_format(fecha,"%Y%m%")=@KPI_Month
group by semana) t
where cant=7)
group by cedula) b 
on a.Identification=b.cedula
set
conection_hours_4weeks=conexion,
pauses_hours_4weeks=total_pausas,
conection_available_hours_4weeks=horario
where MonthNum=@KPI_Month;

#Pausas disponibles
update  performance_chat_cs_agents
set pauses_available_hours_month=if(shift_hours=8,740/60,410/60) 
where MonthNum=@KPI_Month;

#Datos de calidad
update performance_chat_cs_agents a 
inner join (select agente_evaluado,evaluador, count(1) monitoreos, sum(if(error_critico=100,0,1)) errores_criticos, sum(error_no_critico) sum_nota
 from customer_service_co.tbl_bi_ops_cc_calidad_formato_chat
where date_format(fechahora,"%Y%m%")=@KPI_Month
group by agente_evaluado, evaluador) b 
on a.Identification=b.agente_evaluado
set
total_monitoring=monitoreos,
critical_mistakes=errores_criticos,
sum_califications_not_critical=sum_nota
where MonthNum=@KPI_Month;

#Nota de training
update  performance_chat_cs_agents a
inner join customer_service_co.bi_ops_cc_training b
on a.Identification=b.cedula and a.MonthNum=b.mes
set
training_calification=nota
where MonthNum=@KPI_Month;

#Valores Indicadores
update performance_chat_cs_agents
set
value_adherence= if((conection_hours_4weeks-pauses_hours_4weeks)/(conection_available_hours_4weeks-pauses_available_hours_month)>1,1,
(conection_hours_4weeks-pauses_hours_4weeks)/(conection_available_hours_4weeks-pauses_available_hours_month)),
value_tmo=(total_duration_chats_sec/total_chats)/60,
value_quality=((total_monitoring-critical_mistakes)*100/total_monitoring)*0.6+(sum_califications_not_critical/total_monitoring)*0.4,
value_training=training_calification,
value_survey=survey_answered_good/survey_answered,
value_activity=if(activity_chats/total_chats>1,1,activity_chats/total_chats),
value_absence=total_absence,
value_delays=total_delays
where MonthNum=@KPI_Month;

#Actualizar KPI
update performance_chat_cs_agents
set
kpi_adherence=if(value_adherence>=0.98,1,if(value_adherence>=0.95,0.5,0)) ,
kpi_tmo=if(value_tmo<=9,1,if(value_tmo<=11,0.8,if(value_tmo<=13,0.5,0))),
kpi_quality=if(value_quality>=90,value_quality/100,if(value_quality>=80,0.7,if(value_quality>=70,0.5,0))),
kpi_training=if(value_training>=90,value_training/100,if(value_training>=80,0.7,if(value_training>=70,0.5,0))),
kpi_survey=if(value_survey>0.6,value_survey,0),
kpi_activity=value_activity,
kpi_absence=if(value_absence=0,1,if(value_absence=1,0.5,0)),
kpi_delays=if(value_delays=0,1,if(value_delays=1,0.9,if(value_delays=2,0.8,0)))
where MonthNum=@KPI_Month;

#Principales KPI del mes
update performance_chat_cs_agents
set
kpi_productivity=kpi_adherence*0.5+kpi_tmo*0.5,
kpi_quality_training_nsu=kpi_quality*0.3+kpi_training*0.2+kpi_survey*0.4+kpi_activity*0.1,
kpi_absences_and_delays=kpi_absence*0.5+kpi_delays*0.5
where MonthNum=@KPI_Month;

#Indicador General del Mes
update performance_chat_cs_agents
set
kpi_general=kpi_productivity*0.4+kpi_quality_training_nsu*0.5+kpi_absences_and_delays*0.1
where MonthNum=@KPI_Month;

#Borrar datos de asesores de Chats si existen
delete from perfomance_agents_general
where yrmonth=@KPI_Month
and proceso='CHAT';

#Agregar datos a la evaluación general
insert into perfomance_agents_general
select MonthNum, null, Identification, Agent, coordinador, 0, kpi_productivity, kpi_quality_training_nsu, kpi_absences_and_delays,
kpi_general, proceso  from performance_chat_cs_agents
where status='ACTIVO' and proceso='CHAT'
and MonthNum=@KPI_Month;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `performance_cs_leaders`()
BEGIN


SET @KPI_Month=cast(date_format(curdate(),"%Y%m")-1 as unsigned);
SET @Perfomance_max_date=cast((select max(event_date) from customer_service_co.bi_ops_cc_dyalogo
where date_format(event_date,"%Y%m%")=@KPI_Month) as date);


delete from performance_cs_leaders
where MonthNum=@KPI_Month;

#Insertar coordinadores activos
insert into  performance_cs_leaders 
(MonthNum, Coordinator, Identification, proceso, coordinador, status, start_date, shift_pattern)
select @KPI_Month, name, identification, process,  coordinator, status, start_date, shift_pattern from bi_ops_matrix_sac_history
where process like '%coordinador sac%' and status='ACTIVO'
and date=@Perfomance_max_date;


#Actualizar datos de Inbound 
update performance_cs_leaders a
inner join
(select nombre_matrix,count(1) cant, total_duration_calls_sec, conection_available_hours_month*3600 conection_available_sec, avg(value_adherence) adherence,
avg(value_tmo) tmo,   avg(value_quality) quality, avg(value_survey) survey
from performance_inbound_telesales_agents a 
inner join bi_ops_cc_leaders b
on a.coordinador=b.coordinador
where MonthNum=@KPI_Month
and proceso like '%SAC%'
and status='ACTIVO') b
on a.MonthNum=b.MonthNum
set
total_agents_inb=cant,
call_time_seg=total_duration_calls_sec,
conection_available_sec_month=conection_available_sec,
value_adherence_inb=adherence,
value_tmo_inb=tmo,
value_quality_inb=quality,
value_survey_inb=survey
where a.MonthNum=@KPI_Month
;



#Actualizar datos de Chat 
update performance_cs_leaders a
inner join
(select MonthNum,count(1) cant, avg(value_adherence) adherence,
  avg(value_quality) quality, avg(value_survey) survey
from performance_inbound_telesales_agents a 
inner join bi_ops_cc_leaders b
on a.coordinador=b.coordinador
where MonthNum=@KPI_Month
and proceso like '%CHAT%'
and status='ACTIVO') b
on a.MonthNum=b.MonthNum
set
total_agents_chat=cant,
value_adherence_chat=adherence,
value_quality_chat=quality,
value_survey_chat=survey
where a.MonthNum=@KPI_Month;

#Actualizar datos de Front Office
update performance_cs_leaders a
inner join
(select MonthNum, count(1) cant, avg(value_adherence) adherence, avg(kpi_first_reply_time) first_reply_time,
  avg(value_quality) quality, avg(value_survey) survey
from performance_inbound_telesales_agents a 
inner join bi_ops_cc_leaders b
on a.coordinador=b.coordinador
where MonthNum=@KPI_Month
and proceso like '%BACK%'
and status='ACTIVO') b
on a.MonthNum=b.MonthNum
set
total_agents_fo=cant,
value_adherence_front=adherence,
value_reply_on_time_front=first_reply_time,
value_quality_front=quality,
value_survey_front=survey
where a.MonthNum=@KPI_Month;

#Actualizar datos de Back Office
update performance_cs_leaders a
inner join
(select MonthNum, count(1) cant, avg(value_adherence) adherence, avg(kpi_first_reply_time) first_reply_time, avg(kpi_solution) solution,
  avg(value_quality) quality, avg(value_survey) survey
from performance_inbound_telesales_agents a 
inner join bi_ops_cc_leaders b
on a.coordinador=b.coordinador
where MonthNum=@KPI_Month
and proceso like '%BACK%'
and status='ACTIVO') b
on a.MonthNum=b.MonthNum
set
total_agents_bo=cant,
value_adherence_back=adherence,
value_reply_on_time_back=first_reply_time,
value_solved_on_time=solution,
value_quality_back=quality,
value_survey_back=survey
where a.MonthNum=@KPI_Month;


#Actualizar nota de cumplimiento de monitoreos
update performance_cs_leaders a 
inner join customer_service_co.bi_ops_cc_cumplimiento_monitoreos b 
on a.Identification=b.cedula
and a.MonthNum=b.mes
set
value_punctuality=nota
where MonthNum=@KPI_Month;

#Actualizar nota de cumplimiento de feedback
update performance_cs_leaders a 
inner join customer_service_co.bi_ops_cc_feedback_ventas b 
on a.Identification=b.cedula
and a.MonthNum=b.mes
set
value_feedback=nota
where MonthNum=@KPI_Month;

#Agregar nivel de servicio y abandono
update performance_cs_leaders
set
value_service_level=(select sum(answered_in_wh_20)/sum(net_event) from customer_service_co.bi_ops_cc_dyalogo
where date_format(event_date,"%Y%m")=@KPI_Month
and pos=1 and net_event=1),
value_abandon=(select sum(unanswered_in_wh)/sum(net_event) from customer_service_co.bi_ops_cc_dyalogo
where date_format(event_date,"%Y%m")=@KPI_Month
and pos=1 and net_event=1)
where MonthNum=@KPI_Month;

#Actualizar indicadores
update  performance_cs_leaders 
SET
value_adherence=(value_adherence_inb*total_agents_inb+value_adherence_front*total_agents_fo+value_adherence_back*total_agents_bo+value_adherence_chat*total_agents_chat)/
				(total_agents_inb+total_agents_fo+total_agents_bo+total_agents_chat),
value_occupancy=call_time_seg/conection_available_sec_month,
value_reply_on_time=(value_reply_on_time_front*total_agents_fo+value_reply_on_time_back*total_agents_bo)/
					(total_agents_fo+total_agents_bo),
value_quality=(value_quality_inb*total_agents_inb+value_quality_front*total_agents_fo+value_quality_back*total_agents_bo+value_quality_chat*total_agents_chat)/
				(total_agents_inb+total_agents_fo+total_agents_bo+total_agents_chat),
value_training_team=(value_training_team_inb*total_agents_inb+value_training_team_front*total_agents_fo+value_training_team_back*total_agents_bo+value_training_team_chat*total_agents_chat)/
				(total_agents_inb+total_agents_fo+total_agents_bo+total_agents_chat),
value_survey=(value_survey_inb*total_agents_inb+value_survey_front*total_agents_fo+value_survey_back*total_agents_bo+value_survey_chat*total_agents_chat)/
				(total_agents_inb+total_agents_fo+total_agents_bo+total_agents_chat),
value_activity=(value_activity_inb*total_agents_inb+value_activity*total_agents_chat)/
				(total_agents_inb+total_agents_chat),
value_absence=(value_absence_inb*total_agents_inb+value_absence_front*total_agents_fo+value_absence_back*total_agents_bo+value_absence_chat*total_agents_chat)/
				(total_agents_inb+total_agents_fo+total_agents_bo+total_agents_chat),
value_delays=(value_delays_inb*total_agents_inb+value_delays_front*total_agents_fo+value_delays_back*total_agents_bo+value_delays_chat*total_agents_chat)/
				(total_agents_inb+total_agents_fo+total_agents_bo+total_agents_chat)
where MonthNum=@KPI_Month;

update  performance_cs_leaders a
inner join customer_service_co.bi_ops_cc_training b
on a.Identification=b.cedula and a.MonthNum=b.mes
set
value_training_leader=nota
where MonthNum=@KPI_Month;

#Valor general de training
update performance_cs_leaders
set 
value_training=value_training_team*0.4+value_training_leader*0.6
where MonthNum=@KPI_Month;

#Actualizar KPI
update performance_cs_leaders
set
kpi_adherence=if(value_adherence>=0.98,1,if(value_adherence>=0.95,0.5,0)) ,
kpi_occupancy=if(value_occupancy>=0.7,1,value_occupancy/0.7),
kpi_reply_on_time=value_reply_on_time,
kpi_solved_on_time=value_solved_on_time,
kpi_service_level=if(value_service_level>=0.80,1,0),
kpi_abandon=if(value_abandon<=0.03,1,0) ,
kpi_tmo=if(value_tmo<=6,1,if(value_tmo<=6.25,0.98,if(value_tmo<=7,0.8,if(value_tmo<=8,0.5,0)))),
kpi_quality=if(value_quality>=90,value_quality/100,if(value_quality>=80,0.7,if(value_quality>=70,0.5,0))),
kpi_training=if(value_training>=90,value_training/100,if(value_training>=80,0.7,if(value_training>=70,0.5,0))),
kpi_survey=value_survey,
kpi_activity=value_activity,
kpi_punctuality=value_punctuality/100,
kpi_feedback=value_feedback/100,
kpi_absence=value_absence,
kpi_delays=value_delays
where MonthNum=@KPI_Month;

#Principales KPI del mes
update performance_cs_leaders
set
kpi_productivity=kpi_adherence*0.15+kpi_tmo*0.10+kpi_reply_on_time*0.15+kpi_solved_on_time*0.15+kpi_service_level*0.15+kpi_abandon*0.15,
kpi_quality_training_nsu=kpi_quality*0.35+kpi_training*0.2+kpi_survey*0.25+kpi_activity*0.1+kpi_punctuality*0.05+kpi_feedback*0.05,
kpi_absences_and_delays=kpi_absence*0.5+kpi_delays*0.5
where MonthNum=@KPI_Month;

#Indicador General del Mes
update performance_cs_leaders
set
kpi_general=kpi_productivity*0.4+kpi_quality_training_nsu*0.5+kpi_absences_and_delays*0.1
where MonthNum=@KPI_Month;

#Borrar datos de asesores de Postventa si existen
delete from perfomance_coord_general
where yrmonth=@KPI_Month
and proceso='CS';

#Agregar datos a la evaluación general
insert into perfomance_coord_general
select MonthNum, Coordinator,  0, kpi_productivity, kpi_quality_training_nsu, kpi_absences_and_delays,0,
kpi_general, 'CS'  from performance_cs_leaders
where status='ACTIVO' 
and MonthNum=@KPI_Month;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `performance_cs_senior_coordinator`()
BEGIN


SET @KPI_Month=cast(date_format(curdate(),"%Y%m")-1 as unsigned);
SET @Perfomance_max_date=cast((select max(event_date) from customer_service_co.bi_ops_cc_dyalogo
where date_format(event_date,"%Y%m%")=@KPI_Month) as date);


delete from performance_cs_senior_coordinator
where MonthNum=@KPI_Month;

#Insertar senior coordinator
insert into  performance_cs_senior_coordinator 
(MonthNum, Coordinator, Identification, proceso, coordinador, status, start_date, shift_pattern)
select @KPI_Month, name, identification, process,  coordinator, status, start_date, shift_pattern from bi_ops_matrix_sac_history 
where process like '%senior%' and status='ACTIVO'
and date=@Perfomance_max_date;


#Actualizar datos de Inbound 
update performance_cs_senior_coordinator a
inner join
(select MonthNum,count(1) cant, total_duration_calls_sec, conection_available_hours_month*3600 conection_available_sec, avg(value_adherence) adherence,
avg(value_tmo) tmo,   avg(value_quality) quality, avg(value_survey) survey
from performance_inbound_cs_agents a 
where MonthNum=@KPI_Month
and proceso like '%SAC%'
and status='ACTIVO') b
on a.MonthNum=b.MonthNum
set
total_agents_inb=cant,
call_time_seg=total_duration_calls_sec,
conection_available_sec_month=conection_available_sec,
value_adherence_inb=adherence,
value_tmo=tmo,
value_quality_inb=quality,
value_survey_inb=survey
where a.MonthNum=@KPI_Month
;


#Actualizar datos de Chat 
update performance_cs_senior_coordinator a
inner join
(select MonthNum,count(1) cant, avg(value_adherence) adherence,
  avg(value_quality) quality, avg(value_survey) survey
from performance_chat_cs_agents a 
where MonthNum=@KPI_Month
and proceso like '%CHAT%'
and status='ACTIVO') b
on a.MonthNum=b.MonthNum
set
total_agents_chat=cant,
value_adherence_chat=adherence,
value_quality_chat=quality,
value_survey_chat=survey
where a.MonthNum=@KPI_Month;

#Actualizar datos de Front Office
update performance_cs_senior_coordinator a
inner join
(select MonthNum, count(1) cant, avg(value_adherence) adherence, avg(kpi_first_reply_time) first_reply_time,
  avg(value_quality) quality, avg(value_survey) survey
from performance_front_office_cs_agents a 
where MonthNum=@KPI_Month
and proceso like '%BACK%'
and status='ACTIVO') b
on a.MonthNum=b.MonthNum
set
total_agents_fo=cant,
value_adherence_front=adherence,
value_reply_on_time_front=first_reply_time,
value_quality_front=quality,
value_survey_front=survey
where a.MonthNum=@KPI_Month;

#Actualizar datos de Back Office
update performance_cs_senior_coordinator a
inner join
(select MonthNum, count(1) cant, avg(value_adherence) adherence, avg(kpi_first_reply_time) first_reply_time, avg(kpi_solution) solution,
  avg(value_quality) quality, avg(value_survey) survey
from performance_back_office_cs_agents a 
where MonthNum=@KPI_Month
and proceso like '%BACK%'
and status='ACTIVO') b
on a.MonthNum=b.MonthNum
set
total_agents_bo=cant,
value_adherence_back=adherence,
value_reply_on_time_back=first_reply_time,
value_solved_on_time=solution,
value_quality_back=quality,
value_survey_back=survey
where a.MonthNum=@KPI_Month;


#Agregar datos generales de Inbound
update performance_cs_senior_coordinator a inner join
(select date_format(event_date,"%Y%m") yrmonth, sum(answered_in_wh) answered, sum(answered_in_wh_20)/sum(net_event) service_level,sum(unanswered_in_wh)/sum(net_event) abandon 
from customer_service_co.bi_ops_cc_dyalogo
where date_format(event_date,"%Y%m")=@KPI_Month
and pos=1
and net_event=1) b
on a.MonthNum=b.yrmonth
set
a.total_calls=b.answered,
a.value_service_level=b.service_level,
a.value_abandon=b.abandon
where MonthNum=@KPI_Month;

#Agregar datos generales de Chat
update performance_cs_senior_coordinator a inner join
(select date_format(date,"%Y%m") yrmonth, sum(chats) chats,
sum(chats)/ sum(queued) sl_chat
from customer_service_co.tbl_bi_ops_cc_chats_performance
where  date_format(date,"%Y%m")=@KPI_Month) b
on a.MonthNum=b.yrmonth
set
a.total_chats=b.chats,
a.value_service_level_chat=b.sl_chat
where MonthNum=@KPI_Month;

#Agregar datos generales de Zendesk
update performance_cs_senior_coordinator a inner join
(SELECT date_format(Fecha_Creacion,"%Y%m") yrmonth, sum(summation_column) tickets
 FROM customer_service.tbl_zendesk_general a 
where date_format(Fecha_Creacion,"%Y%m")=@KPI_Month
and pais='CO'
and via in ('Mail','email')) b
on a.MonthNum=b.yrmonth
set
a.total_inc_tickets=b.tickets
where MonthNum=@KPI_Month;

#Llamadas no contestadas en el mes por asesores
update performance_cs_senior_coordinator a inner join
(select date_format(fecha_hora,"%Y%m") yrmonth, count(1) no_answer from  CS_dyalogo.v_queue_log 
where date_format(fecha_hora,"%Y%m")=@KPI_Month
and evento='RINGNOANSWER'
and cola in ('Reclamos',
'Cancelaciones',
'Devolucionesytramites') ) b
on a.MonthNum=b.yrmonth
set
a.total_calls_no_answer=b.no_answer
where MonthNum=@KPI_Month;

#Gross Orders del mes
update performance_cs_senior_coordinator a inner join
(select MonthNum yrmonth,count(distinct(orderNum)) gross_co from development_co_project.A_Master
where OrderBeforeCan=1
and MonthNum=@KPI_Month) b
on a.MonthNum=b.yrmonth
set
a.total_gross_orders=b.gross_co
where MonthNum=@KPI_Month;

#Actualizar indicadores
update  performance_cs_senior_coordinator 
SET
value_adherence=(value_adherence_inb*total_agents_inb+value_adherence_front*total_agents_fo+value_adherence_back*total_agents_bo+value_adherence_chat*total_agents_chat)/
				(total_agents_inb+total_agents_fo+total_agents_bo+total_agents_chat),
value_occupancy=call_time_seg/conection_available_sec_month,
value_reply_on_time=(value_reply_on_time_front*total_agents_fo+value_reply_on_time_back*total_agents_bo)/
					(total_agents_fo+total_agents_bo),
value_atention=total_calls/(total_calls+total_calls_no_answer),
value_quality=(value_quality_inb*total_agents_inb+value_quality_front*total_agents_fo+value_quality_back*total_agents_bo+value_quality_chat*total_agents_chat)/
				(total_agents_inb+total_agents_fo+total_agents_bo+total_agents_chat),
value_survey_tickets=(value_survey_front*total_agents_fo+value_survey_back*total_agents_bo)/
				     (total_agents_fo+total_agents_bo),
value_contact_ratio_inb=total_calls/total_gross_orders,
value_contact_ratio_chat=total_chats/total_gross_orders,
value_contact_ratio_mails=total_inc_tickets/total_gross_orders
where MonthNum=@KPI_Month;

#Valor general de training
update performance_cs_leaders
set 
value_training=value_training_team*0.4+value_training_leader*0.6
where MonthNum=@KPI_Month;


#Actualizar KPI
update performance_cs_senior_coordinator
set
kpi_adherence=if(value_adherence>=0.98,1,if(value_adherence>=0.95,0.5,0)) ,
kpi_occupancy=if(value_occupancy>=0.7,1,value_occupancy/0.7),
kpi_service_level=if(value_service_level>=0.80,1,0),
kpi_abandon=if(value_abandon<=0.03,1,0) ,
#Revisar indicador de chat con Tahani
kpi_service_level_chat=if(value_service_level_chat>=0.9,1,0) ,
kpi_atention=if(value_atention>=0.97,1,value_atention/0.97),
kpi_reply_on_time=value_reply_on_time,
kpi_solved_on_time=value_solved_on_time,
kpi_tmo=if(value_tmo<=6,1,if(value_tmo<=6.25,0.98,if(value_tmo<=7,0.8,if(value_tmo<=8,0.5,0)))),
kpi_quality=if(value_quality>=90,1,0),
kpi_survey_inb=if(value_survey_inb>=0.9,1,value_survey_inb/0.9),
kpi_survey_chat=if(value_survey_chat>=0.9,1,value_survey_chat/0.9),
kpi_survey_tickets=if(value_survey_tickets>=0.9,1,value_survey_tickets/0.9),
kpi_contact_ratio_inb=if(value_contact_ratio_inb<=0.3,1,0.3/value_contact_ratio_inb),
kpi_contact_ratio_chat=if(value_contact_ratio_chat<=0.7,1,0.7/value_contact_ratio_chat),
kpi_contact_ratio_mails=if(value_contact_ratio_mails<=0.1,1,0.1/value_contact_ratio_mails)
where MonthNum=@KPI_Month;


#Principales KPI del mes
update performance_cs_senior_coordinator
set
kpi_productivity=kpi_adherence*0.10+kpi_occupancy*0.10+kpi_service_level*0.10+kpi_abandon*0.15+kpi_service_level_chat*0.15+kpi_atention*0.10+
kpi_reply_on_time*0.10+kpi_solved_on_time*0.10+kpi_tmo*0.10,
kpi_quality_training_nsu=kpi_quality*0.5+kpi_survey_chat*0.25+kpi_survey_inb*0.125+kpi_survey_tickets*0.125,
kpi_contact_ratio=kpi_contact_ratio_inb*0.4+kpi_contact_ratio_chat*0.3+kpi_contact_ratio_mails*0.3
where MonthNum=@KPI_Month;

#Indicador General del Mes
update performance_cs_senior_coordinator
set
kpi_general=kpi_productivity*0.4+kpi_quality_training_nsu*0.5+kpi_contact_ratio*0.1
where MonthNum=@KPI_Month;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `performance_front_office_cs_agents`()
BEGIN


SET @KPI_Month=cast(date_format(curdate(),"%Y%m")-1 as unsigned);
SET @Perfomance_max_date=cast((select max(date) from customer_service.tbl_zendesk_general
where date_format(date,"%Y%m%")=@KPI_Month) as date);


delete from performance_front_office_cs_agents
where MonthNum=@KPI_Month;


#Insertar datos de base de zendesk
insert into performance_front_office_cs_agents (MonthNum, Agent, Identification, total_tickets, total_first_reply_time_hours,  
survey_answered, survey_answered_good)
SELECT date_format(Fecha_Creacion,"%Y%m") yrmonth, name, identification, sum(summation_column) tickets, sum(first_reply_time_ajustado)/60 reply_time_hours,
sum(calificados) rated, sum(calificados_bueno) rated_good
 FROM customer_service.tbl_zendesk_general a 
inner join customer_service_co.bi_ops_matrix_sac b on a.Assignee=b.user_zendesk
where date_format(Fecha_Creacion,"%Y%m")=@KPI_Month
and pais='CO'
and es_inc_front_office=1
group by identification;

#Actualizar
update performance_front_office_cs_agents a 
inner join bi_ops_matrix_sac_history b 
on a.Identification=b.identification
set
a.start_date=b.start_date,
a.shift_pattern=b.shift_pattern,
a.proceso=b.process,
a.coordinador=b.coordinator,
a.status=b.status
where MonthNum=@KPI_Month
and b.date=@Perfomance_max_date;

#Agregar antiguedad de los asesores
update performance_front_office_cs_agents 
set
Antique='Nuevo'
where DATEDIFF(CAST(@Perfomance_max_date AS DATE),start_date)<31
and MonthNum=@KPI_Month;

#Eliminar datos que no aparecen en la matriz de agentes
delete from performance_front_office_cs_agents
where shift_pattern is null
and MonthNum=@KPI_Month;

#Actualizar horas de los turnos
update  performance_front_office_cs_agents a 
inner join customer_service_co.bi_ops_cc_shift_details b 
on a.shift_pattern=b.shift
set shift_hours=turno_diario
where MonthNum=@KPI_Month;

#Datos de adherencia 1
update performance_front_office_cs_agents a 
inner join (select cedula, sum(tiempo_conexion_segundos)/3600 conexion, (sum(total_break_segundos)+
sum(total_baño_segundos)+sum(total_capacitacion_segundos)+sum(total_almuerzo_segundos) +
sum(total_feedback_segundos)+sum(total_otros_procesos_segundos))/3600 total_pausas,
sum(tiempo_horario_segundos)/3600 horario, sum(inasistencia) inasistencias, sum(retraso) retrasos from customer_service_co.bi_ops_cc_adherencia_sac
where date_format(fecha,"%Y%m%")=@KPI_Month
group by cedula) b 
on a.Identification=b.cedula
set
conection_hours_month=conexion,
pauses_hours_month=total_pausas,
conection_available_hours_month=horario,
total_absence=inasistencias,
total_delays=retrasos
where MonthNum=@KPI_Month;

#Datos de adherencia 2
update performance_front_office_cs_agents a 
inner join (select cedula, sum(tiempo_conexion_segundos)/3600 conexion, (sum(total_break_segundos)+
sum(total_baño_segundos)+sum(total_capacitacion_segundos)+sum(total_almuerzo_segundos) +
sum(total_feedback_segundos)+sum(total_otros_procesos_segundos))/3600 total_pausas,
sum(tiempo_horario_segundos)/3600 horario, sum(inasistencia) inasistencias, sum(retraso) retrasos from customer_service_co.bi_ops_cc_adherencia_sac
where Semana in (select semana from
(select semana, count(distinct(fecha)) cant from  customer_service_co.bi_ops_cc_adherencia_sac
where date_format(fecha,"%Y%m%")=@KPI_Month
group by semana) t
where cant=7)
group by cedula) b 
on a.Identification=b.cedula
set
conection_hours_4weeks=conexion,
pauses_hours_4weeks=total_pausas,
conection_available_hours_4weeks=horario
where MonthNum=@KPI_Month;

#Pausas disponibles
update  performance_front_office_cs_agents
set pauses_available_hours_month=if(shift_hours=8,740/60,410/60) 
where MonthNum=@KPI_Month;

#Datos de calidad
update performance_front_office_cs_agents a 
inner join (select agente_evaluado,evaluador, count(1) monitoreos, sum(if(error_critico=100,0,1)) errores_criticos, sum(error_no_critico) sum_nota
 from customer_service_co.tbl_bi_ops_cc_calidad_formato_bo
where date_format(fechahora,"%Y%m%")=@KPI_Month
group by agente_evaluado, evaluador) b 
on a.Identification=b.agente_evaluado
set
total_monitoring=monitoreos,
critical_mistakes=errores_criticos,
sum_calification_not_critical=sum_nota
where MonthNum=@KPI_Month;

#Nota de training
update  performance_front_office_cs_agents a
inner join customer_service_co.bi_ops_cc_training b
on a.Identification=b.cedula and a.MonthNum=b.mes
set
training_calification=nota
where MonthNum=@KPI_Month;

#Valores Indicadores
update performance_front_office_cs_agents
set
value_first_reply_time=(total_first_reply_time_hours/total_tickets)/60,
value_quality=((total_monitoring-critical_mistakes)*100/total_monitoring)*0.6+(sum_califications_not_critical/total_monitoring)*0.4,
value_training=training_calification,
value_survey=survey_answered_good/survey_answered,
value_absence=total_absence,
value_delays=total_delays
where MonthNum=@KPI_Month;

#Actualizar KPI
update performance_front_office_cs_agents
set
kpi_adherence=if(value_adherence>=0.98,1,if(value_adherence>=0.95,0.5,0)) ,
kpi_first_reply_time=if(value_first_reply_time<=2,1,if(value_first_reply_time<=4,0.7,if(value_first_reply_time<=8,0.5,0))),
kpi_quality=if(value_quality>=90,value_quality/100,if(value_quality>=80,0.7,if(value_quality>=70,0.5,0))),
kpi_training=if(value_training>=90,value_training/100,if(value_training>=80,0.7,if(value_training>=70,0.5,0))),
kpi_survey=value_survey,
kpi_absence=if(value_absence=0,1,if(value_absence=1,0.5,0)),
kpi_delays=if(value_delays=0,1,if(value_delays=1,0.9,if(value_delays=2,0.8,0)))
where MonthNum=@KPI_Month;

#Principales KPI del mes
update performance_front_office_cs_agents
set
kpi_productivity=kpi_adherence*0.5+kpi_first_reply_time*0.5,
kpi_quality_training_nsu=kpi_quality*0.5+kpi_training*0.3+kpi_survey*0.2,
kpi_absences_and_delays=kpi_absence*0.5+kpi_delays*0.5
where MonthNum=@KPI_Month;

#Indicador General del Mes
update performance_front_office_cs_agents
set
kpi_general=kpi_productivity*0.4+kpi_quality_training_nsu*0.5+kpi_absences_and_delays*0.1
where MonthNum=@KPI_Month;

#Borrar datos de asesores de Chats si existen
delete from perfomance_agents_general
where yrmonth=@KPI_Month
and proceso='FRONT OFFICE';

#Agregar datos a la evaluación general
insert into perfomance_agents_general
select MonthNum, null, Identification, Agent, coordinador, 0, kpi_productivity, kpi_quality_training_nsu, kpi_absences_and_delays,
kpi_general, 'FRONT OFFICE'  from performance_front_office_cs_agents
where status='ACTIVO' and proceso='BACK OFFICE'
and MonthNum=@KPI_Month;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `performance_inbound_cs_agents`()
BEGIN


SET @KPI_Month=cast(date_format(curdate(),"%Y%m")-1 as unsigned);
SET @Perfomance_max_date=cast((select max(event_date) from customer_service_co.bi_ops_cc_dyalogo
where date_format(event_date,"%Y%m%")=@KPI_Month) as date);


delete from performance_inbound_cs_agents
where MonthNum=@KPI_Month;


#Insertar datos de llamadas de la base bi_ops_cc
insert into performance_inbound_cs_agents (MonthNum, Agent, Identification, total_calls, total_duration_calls_sec,  total_transfered_queue, total_transfered_calls, 
survey_answered, survey_answered_good)
select date_format(event_date,"%Y%m") MonthNum,agent_name Agent, agent_identification Identification, 
sum(answered_in_wh) total_calls, sum(call_duration_seg) total_duration_calls_sec, sum(transfered_to_queue) total_transfered_queue, 
sum(transfered_survey) total_transfered_calls, 
sum(answered_3) survey_answered, sum(good) survey_answered_good
from customer_service_co.bi_ops_cc_dyalogo
where  date_format(event_date,"%Y%m")=@KPI_Month
and agent_identification is not null
and pos=1
and net_event=1
group by agent_identification
order by agent_name asc;

#Actualizar
update performance_inbound_cs_agents a 
inner join bi_ops_matrix_sac_history b 
on a.Identification=b.identification
set
a.start_date=b.start_date,
a.shift_pattern=b.shift_pattern,
a.proceso=b.process,
a.coordinador=b.coordinator,
a.status=b.status
where MonthNum=@KPI_Month
and b.date=@Perfomance_max_date;

#Agregar antiguedad de los asesores
update performance_inbound_cs_agents 
set
Antique='Nuevo'
where DATEDIFF(CAST(@Perfomance_max_date AS DATE),start_date)<31
and MonthNum=@KPI_Month;

#Eliminar datos que no aparecen en la matriz de agentes
delete from performance_inbound_cs_agents
where shift_pattern is null
and MonthNum=@KPI_Month;

#Actualizar horas de los turnos
update  performance_inbound_cs_agents a 
inner join customer_service_co.bi_ops_cc_shift_details b 
on a.shift_pattern=b.shift
set shift_hours=turno_diario
where MonthNum=@KPI_Month;

#Actualizar datos del activity code
update  performance_inbound_cs_agents a 
inner join (SELECT t.yrmonth,
t.username, count(1) canti FROM 
(SELECT date_format(date_time,'%Y%m') yrmonth,
a.macro_proceso,a.telefono_email,c.username,c.email,c.phone,c.canal 
FROM customer_service_co.activities a 
left join customer_service_co.users c on a.user_id=c.id where canal='telefono' ) as t
where t.phone is not null and t.phone<>'0000'
group by t.yrmonth,t.username) b 
on a.Identification=b.username
and a.MonthNum=b.yrmonth
set a.activity_calls=b.canti
where MonthNum=@KPI_Month;

#Datos de adherencia 1
update performance_inbound_cs_agents a 
inner join (select cedula, sum(tiempo_conexion_segundos)/3600 conexion, (sum(total_break_segundos)+
sum(total_baño_segundos)+sum(total_capacitacion_segundos)+sum(total_almuerzo_segundos) +
sum(total_feedback_segundos)+sum(total_otros_procesos_segundos))/3600 total_pausas,
sum(tiempo_horario_segundos)/3600 horario, sum(inasistencia) inasistencias, sum(retraso) retrasos from customer_service_co.bi_ops_cc_adherencia_sac
where date_format(fecha,"%Y%m%")=@KPI_Month
group by cedula) b 
on a.Identification=b.cedula
set
conection_hours_month=conexion,
pauses_hours_month=total_pausas,
conection_available_hours_month=horario,
total_absence=inasistencias,
total_delays=retrasos
where MonthNum=@KPI_Month;

#Datos de adherencia 2
update performance_inbound_cs_agents a 
inner join (select cedula, sum(tiempo_conexion_segundos)/3600 conexion, (sum(total_break_segundos)+
sum(total_baño_segundos)+sum(total_capacitacion_segundos)+sum(total_almuerzo_segundos) +
sum(total_feedback_segundos)+sum(total_otros_procesos_segundos))/3600 total_pausas,
sum(tiempo_horario_segundos)/3600 horario, sum(inasistencia) inasistencias, sum(retraso) retrasos from customer_service_co.bi_ops_cc_adherencia_sac
where Semana in (select semana from
(select semana, count(distinct(fecha)) cant from  customer_service_co.bi_ops_cc_adherencia_sac
where date_format(fecha,"%Y%m%")=@KPI_Month
group by semana) t
where cant=7)
group by cedula) b 
on a.Identification=b.cedula
set
conection_hours_4weeks=conexion,
pauses_hours_4weeks=total_pausas,
conection_available_hours_4weeks=horario
where MonthNum=@KPI_Month;

#Pausas disponibles
update  performance_inbound_cs_agents
set pauses_available_hours_month=if(shift_hours=8,740/60,410/60) 
where MonthNum=@KPI_Month;

#Datos de calidad
update performance_inbound_cs_agents a 
inner join (select agente_evaluado,evaluador, count(1) monitoreos, sum(if(error_critico=100,0,1)) errores_criticos, sum(error_no_critico) sum_nota
 from customer_service_co.tbl_bi_ops_cc_calidad_formato_pos
where date_format(fechahora,"%Y%m%")=@KPI_Month
group by agente_evaluado, evaluador) b 
on a.Identification=b.agente_evaluado
set
total_monitoring=monitoreos,
critical_mistakes=errores_criticos,
sum_calification_not_critical=sum_nota
where MonthNum=@KPI_Month;

#Nota de training
update  performance_inbound_cs_agents a
inner join customer_service_co.bi_ops_cc_training b
on a.Identification=b.cedula and a.MonthNum=b.mes
set
training_calification=nota
where MonthNum=@KPI_Month;

#Valores Indicadores
update performance_inbound_cs_agents
set
value_adherence= if((conection_hours_4weeks-pauses_hours_4weeks)/(conection_available_hours_4weeks-pauses_available_hours_month)>1,1,
(conection_hours_4weeks-pauses_hours_4weeks)/(conection_available_hours_4weeks-pauses_available_hours_month)),
value_tmo=(total_duration_calls_sec/total_calls)/60,
value_quality=((total_monitoring-critical_mistakes)*100/total_monitoring)*0.6+(sum_califications_not_critical/total_monitoring)*0.4,
value_training=training_calification,
value_survey=if(total_transfered_calls/(total_calls-total_transfered_queue)>0.8,
survey_answered_good/survey_answered,((survey_answered_good/survey_answered)*(total_transfered_calls/(total_calls-total_transfered_queue)))/0.8),
value_activity=if(activity_calls/total_calls>1,1,activity_calls/total_calls),
value_absence=total_absence,
value_delays=total_delays
where MonthNum=@KPI_Month;

#Actualizar KPI
update performance_inbound_cs_agents
set
kpi_adherence=if(value_adherence>=0.98,1,if(value_adherence>=0.95,0.5,0)) ,
kpi_tmo=if(value_tmo<=6,1,if(value_tmo<=6.25,0.98,if(value_tmo<=7,0.8,if(value_tmo<=8,0.5,0)))),
kpi_quality=if(value_quality>=90,value_quality/100,if(value_quality>=80,0.7,if(value_quality>=70,0.5,0))),
kpi_training=if(value_training>=90,value_training/100,if(value_training>=80,0.7,if(value_training>=70,0.5,0))),
kpi_survey=value_survey,
kpi_activity=value_activity,
kpi_absence=if(value_absence=0,1,if(value_absence=1,0.5,0)),
kpi_delays=if(value_delays=0,1,if(value_delays=1,0.9,if(value_delays=2,0.8,0)))
where MonthNum=@KPI_Month;

#Principales KPI del mes
update performance_inbound_cs_agents
set
kpi_productivity=kpi_adherence*0.5+kpi_tmo*0.5,
kpi_quality_training_nsu=kpi_quality*0.3+kpi_training*0.2+kpi_survey*0.4+kpi_activity*0.1,
kpi_absences_and_delays=kpi_absence*0.5+kpi_delays*0.5
where MonthNum=@KPI_Month;

#Indicador General del Mes
update performance_inbound_cs_agents
set
kpi_general=kpi_productivity*0.4+kpi_quality_training_nsu*0.5+kpi_absences_and_delays*0.1
where MonthNum=@KPI_Month;

#Borrar datos de asesores de Postventa si existen
delete from perfomance_agents_general
where yrmonth=@KPI_Month
and proceso='SAC';

#Agregar datos a la evaluación general
insert into perfomance_agents_general
select MonthNum, null, Identification, Agent, coordinador, 0, kpi_productivity, kpi_quality_training_nsu, kpi_absences_and_delays,
kpi_general, proceso  from performance_inbound_cs_agents
where status='ACTIVO' and proceso='SAC'
and MonthNum=@KPI_Month;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `performance_inbound_telesales_agents`()
BEGIN


SET @KPI_Month=cast(date_format(curdate(),"%Y%m")-1 as unsigned);
SET @Perfomance_max_date=cast((select max(event_date) from customer_service_co.bi_ops_cc_dyalogo
where date_format(event_date,"%Y%m%")=@KPI_Month) as date);


delete from performance_inbound_telesales_agents
where MonthNum=@KPI_Month;


#Insertar datos de llamadas de la base bi_ops_cc
insert into performance_inbound_telesales_agents (MonthNum, Agent, Identification, total_calls, total_duration_calls_sec,  total_transfered_queue, total_transfered_calls, 
survey_answered, survey_answered_good)
select date_format(event_date,"%Y%m") MonthNum,agent_name Agent, agent_identification Identification, 
sum(answered_in_wh) total_calls, sum(call_duration_seg) total_duration_calls_sec, sum(transfered_to_queue) total_transfered_queue, 
sum(transfered_survey) total_transfered_calls, 
sum(answered_3) survey_answered, sum(good) survey_answered_good
from customer_service_co.bi_ops_cc_dyalogo
where  date_format(event_date,"%Y%m")=@KPI_Month
and agent_identification is not null
and pre=1
and net_event=1
group by agent_identification
order by agent_name asc;

#Actualizar
update performance_inbound_telesales_agents a 
inner join bi_ops_matrix_ventas_history b 
on a.Identification=b.identification
set
a.start_date=b.start_date,
a.shift_pattern=b.shift_pattern,
a.proceso=b.process,
a.coordinador=b.coordinator,
a.status=b.status
where MonthNum=@KPI_Month
and b.date=@Perfomance_max_date;

#Agregar antiguedad de los asesores
update performance_inbound_telesales_agents 
set
Antique='Nuevo'
where DATEDIFF(CAST(@Perfomance_max_date AS DATE),start_date)<31
and MonthNum=@KPI_Month;

#Eliminar datos que no aparecen en la matriz de agentes
delete from performance_inbound_telesales_agents
where shift_pattern is null
and MonthNum=@KPI_Month;

#Actualizar horas de los turnos
update  performance_inbound_telesales_agents a 
inner join customer_service_co.bi_ops_cc_shift_details b 
on a.shift_pattern=b.shift
set shift_hours=turno_diario
where MonthNum=@KPI_Month;

#Actualizar datos del activity code
update  performance_inbound_telesales_agents a 
inner join (SELECT t.yrmonth,
t.username, count(1) canti FROM 
(SELECT date_format(date_time,'%Y%m') yrmonth,
a.macro_proceso,a.telefono_email,c.username,c.email,c.phone,c.canal 
FROM customer_service_co.activities a 
left join customer_service_co.users c on a.user_id=c.id where canal='telefono' ) as t
where t.phone is not null and t.phone<>'0000'
group by t.yrmonth,t.username) b 
on a.Identification=b.username
and a.MonthNum=b.yrmonth
set a.activity_calls=b.canti
where MonthNum=@KPI_Month;

#Datos de adherencia 1
update performance_inbound_telesales_agents a 
inner join (select cedula, sum(tiempo_conexion_segundos)/3600 conexion, (sum(total_break_segundos)+
sum(total_baño_segundos)+sum(total_capacitacion_segundos)+sum(total_almuerzo_segundos) +
sum(total_feedback_segundos)+sum(total_otros_procesos_segundos))/3600 total_pausas,
sum(tiempo_horario_segundos)/3600 horario, sum(inasistencia) inasistencias, sum(retraso) retrasos from customer_service_co.bi_ops_cc_adherencia_ventas
where date_format(fecha,"%Y%m%")=@KPI_Month
group by cedula) b 
on a.Identification=b.cedula
set
conection_hours_month=conexion,
pauses_hours_month=total_pausas,
conection_available_hours_month=horario,
total_absence=inasistencias,
total_delays=retrasos
where MonthNum=@KPI_Month;

#Datos de adherencia 2
update performance_inbound_telesales_agents a 
inner join (select cedula, sum(tiempo_conexion_segundos)/3600 conexion, (sum(total_break_segundos)+
sum(total_baño_segundos)+sum(total_capacitacion_segundos)+sum(total_almuerzo_segundos) +
sum(total_feedback_segundos)+sum(total_otros_procesos_segundos))/3600 total_pausas,
sum(tiempo_horario_segundos)/3600 horario, sum(inasistencia) inasistencias, sum(retraso) retrasos from customer_service_co.bi_ops_cc_adherencia_ventas
where Semana in (select semana from
(select semana, count(distinct(fecha)) cant from  customer_service_co.bi_ops_cc_adherencia_sac
where date_format(fecha,"%Y%m%")=@KPI_Month
group by semana) t
where cant=7)
group by cedula) b 
on a.Identification=b.cedula
set
conection_hours_4weeks=conexion,
pauses_hours_4weeks=total_pausas,
conection_available_hours_4weeks=horario
where MonthNum=@KPI_Month;

#Pausas disponibles
update  performance_inbound_telesales_agents
set pauses_available_hours_month=if(shift_hours=8,740/60,410/60) 
where MonthNum=@KPI_Month;

#Gross Orders
update performance_inbound_telesales_agents a 
inner join (select Identification, count(distinct(OrderNum)) gross_orders from customer_service.tbl_telesales_co
where date_format(Date,"%Y%m")=@KPI_Month
and OrderBeforeCan=1
and Identification is not null
group by Identification) b 
on a.Identification=b.Identification
set
total_gross_orders=gross_orders
where MonthNum=@KPI_Month;

#Datos de calidad
update performance_inbound_telesales_agents a 
inner join (select agente_evaluado,evaluador, count(1) monitoreos, sum(if(error_critico=100,0,1)) errores_criticos, sum(error_no_critico) sum_nota
 from customer_service_co.tbl_bi_ops_cc_calidad_formato_pos
where date_format(fechahora,"%Y%m%")=@KPI_Month
group by agente_evaluado, evaluador) b 
on a.Identification=b.agente_evaluado
set
total_monitoring=monitoreos,
critical_mistakes=errores_criticos,
sum_calification_not_critical=sum_nota
where MonthNum=@KPI_Month;

#Nota de training
update  performance_inbound_telesales_agents a
inner join customer_service_co.bi_ops_cc_training b
on a.Identification=b.cedula and a.MonthNum=b.mes
set
training_calification=nota
where MonthNum=@KPI_Month;

#Valores Indicadores
update performance_inbound_telesales_agents
set
value_conversion=total_gross_orders/total_calls,
value_adherence= if((conection_hours_4weeks-pauses_hours_4weeks)/(conection_available_hours_4weeks-pauses_available_hours_month)>1,1,
(conection_hours_4weeks-pauses_hours_4weeks)/(conection_available_hours_4weeks-pauses_available_hours_month)),
value_tmo=(total_duration_calls_sec/total_calls)/60,
value_quality=((total_monitoring-critical_mistakes)*100/total_monitoring)*0.6+(sum_califications_not_critical/total_monitoring)*0.4,
value_training=training_calification,
value_survey=if(total_transfered_calls/(total_calls-total_transfered_queue)>0.8,
survey_answered_good/survey_answered,((survey_answered_good/survey_answered)*(total_transfered_calls/(total_calls-total_transfered_queue)))/0.8),
value_activity=if(activity_calls/total_calls>1,1,activity_calls/total_calls),
value_absence=total_absence,
value_delays=total_delays
where MonthNum=@KPI_Month;

#Actualizar KPI
update performance_inbound_telesales_agents
set
kpi_conversion=if(value_conversion>=0.20,1,value_conversion/0.20) ,
kpi_adherence=if(value_adherence>=0.98,1,if(value_adherence>=0.95,0.5,0)) ,
kpi_tmo=if(value_tmo<=5,1,if(value_tmo<=5.25,0.98,if(value_tmo<=6,0.8,if(value_tmo<=7,0.5,0)))),
kpi_quality=if(value_quality>=90,value_quality/100,if(value_quality>=80,0.7,if(value_quality>=70,0.5,0))),
kpi_training=if(value_training>=90,value_training/100,if(value_training>=80,0.7,if(value_training>=70,0.5,0))),
kpi_survey=value_survey,
kpi_activity=value_activity,
kpi_absence=if(value_absence=0,1,if(value_absence=1,0.5,0)),
kpi_delays=if(value_delays=0,1,if(value_delays=1,0.9,if(value_delays=2,0.8,0)))
where MonthNum=@KPI_Month;

#Principales KPI del mes
update performance_inbound_telesales_agents
set
kpi_productivity=kpi_conversion*0.45+kpi_adherence*0.40+kpi_tmo*0.15,
kpi_quality_training_nsu=kpi_quality*0.3+kpi_training*0.2+kpi_survey*0.4+kpi_activity*0.1,
kpi_absences_and_delays=kpi_absence*0.5+kpi_delays*0.5
where MonthNum=@KPI_Month;

#Indicador General del Mes
update performance_inbound_telesales_agents
set
kpi_general=kpi_productivity*0.4+kpi_quality_training_nsu*0.5+kpi_absences_and_delays*0.1
where MonthNum=@KPI_Month;

#Borrar datos de asesores de Postventa si existen
delete from perfomance_agents_general
where yrmonth=@KPI_Month
and proceso='VENTAS';

#Agregar datos a la evaluación general
insert into perfomance_agents_general
select MonthNum, null, Identification, Agent, coordinador, 0, kpi_productivity, kpi_quality_training_nsu, kpi_absences_and_delays,
kpi_general, proceso  from performance_inbound_telesales_agents
where status='ACTIVO' and proceso='VENTAS'
and MonthNum=@KPI_Month;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `performance_outbound_telesales_agents`()
BEGIN


SET @KPI_Month=cast(date_format(curdate(),"%Y%m")-1 as unsigned);
SET @Perfomance_max_date=cast((select max(event_date) from customer_service_co.bi_ops_cc_outbound
where date_format(event_date,"%Y%m%")=@KPI_Month) as date);


delete from performance_inbound_telesales_agents
where MonthNum=@KPI_Month;


#Insertar datos de llamadas de la base bi_ops_cc_outbound
insert into performance_outbound_telesales_agents (MonthNum, Agent, Identification, total_calls, total_duration_calls_sec,  total_calls_answered)
select date_format(event_date,"%Y%m") MonthNum,agent_name Agent, identification Identification, 
sum(calls) total_calls, sum(call_duration_seg) total_duration_calls_sec, sum(answered) answered
from customer_service_co.bi_ops_cc_outbound
where  date_format(event_date,"%Y%m")=@KPI_Month
and identification is not null
group by identification
order by agent_name asc;

#Actualizar
update performance_outbound_telesales_agents a 
inner join bi_ops_matrix_ventas_history b 
on a.Identification=b.identification
set
a.start_date=b.start_date,
a.shift_pattern=b.shift_pattern,
a.proceso=b.process,
a.coordinador=b.coordinator,
a.status=b.status
where MonthNum=@KPI_Month
and b.date=@Perfomance_max_date;

#Agregar antiguedad de los asesores
update performance_outbound_telesales_agents 
set
Antique='Nuevo'
where DATEDIFF(CAST(@Perfomance_max_date AS DATE),start_date)<31
and MonthNum=@KPI_Month;

#Eliminar datos que no aparecen en la matriz de agentes
delete from performance_outbound_telesales_agents
where shift_pattern is null
and MonthNum=@KPI_Month;

#Actualizar horas de los turnos
update  performance_outbound_telesales_agents a 
inner join customer_service_co.bi_ops_cc_shift_details b 
on a.shift_pattern=b.shift
set shift_hours=turno_diario
where MonthNum=@KPI_Month;

#Datos de adherencia 1
update performance_outbound_telesales_agents a 
inner join (select cedula, sum(tiempo_conexion_segundos)/3600 conexion, (sum(total_break_segundos)+
sum(total_baño_segundos)+sum(total_capacitacion_segundos)+sum(total_almuerzo_segundos) +
sum(total_feedback_segundos)+sum(total_otros_procesos_segundos))/3600 total_pausas,
sum(tiempo_horario_segundos)/3600 horario, sum(inasistencia) inasistencias, sum(retraso) retrasos from customer_service_co.bi_ops_cc_adherencia_ventas
where date_format(fecha,"%Y%m%")=@KPI_Month
group by cedula) b 
on a.Identification=b.cedula
set
conection_hours_month=conexion,
pauses_hours_month=total_pausas,
conection_available_hours_month=horario,
total_absence=inasistencias,
total_delays=retrasos
where MonthNum=@KPI_Month;

#Datos de adherencia 2
update performance_outbound_telesales_agents a 
inner join (select cedula, sum(tiempo_conexion_segundos)/3600 conexion, (sum(total_break_segundos)+
sum(total_baño_segundos)+sum(total_capacitacion_segundos)+sum(total_almuerzo_segundos) +
sum(total_feedback_segundos)+sum(total_otros_procesos_segundos))/3600 total_pausas,
sum(tiempo_horario_segundos)/3600 horario, sum(inasistencia) inasistencias, sum(retraso) retrasos from customer_service_co.bi_ops_cc_adherencia_ventas
where Semana in (select semana from
(select semana, count(distinct(fecha)) cant from  customer_service_co.bi_ops_cc_adherencia_sac
where date_format(fecha,"%Y%m%")=@KPI_Month
group by semana) t
where cant=7)
group by cedula) b 
on a.Identification=b.cedula
set
conection_hours_4weeks=conexion,
pauses_hours_4weeks=total_pausas,
conection_available_hours_4weeks=horario
where MonthNum=@KPI_Month;

#Pausas disponibles
update  performance_outbound_telesales_agents
set pauses_available_hours_month=if(shift_hours=8,740/60,410/60) 
where MonthNum=@KPI_Month;

#Gross Orders
update performance_outbound_telesales_agents a 
inner join (select Identification, count(distinct(OrderNum)) gross_orders from customer_service.tbl_telesales_co
where date_format(Date,"%Y%m")=@KPI_Month
and OrderBeforeCan=1
and Identification is not null
group by Identification) b 
on a.Identification=b.Identification
set
total_gross_orders=gross_orders
where MonthNum=@KPI_Month;

#Datos de calidad
update performance_inbound_telesales_agents a 
inner join (select agente_evaluado,evaluador, count(1) monitoreos, sum(if(error_critico=100,0,1)) errores_criticos, sum(error_no_critico) sum_nota
 from customer_service_co.tbl_bi_ops_cc_calidad_formato_pos
where date_format(fechahora,"%Y%m%")=@KPI_Month
group by agente_evaluado, evaluador) b 
on a.Identification=b.agente_evaluado
set
total_monitoring=monitoreos,
critical_mistakes=errores_criticos,
sum_calification_not_critical=sum_nota
where MonthNum=@KPI_Month;

#Nota de training
update  performance_inbound_telesales_agents a
inner join customer_service_co.bi_ops_cc_training b
on a.Identification=b.cedula and a.MonthNum=b.mes
set
training_calification=nota
where MonthNum=@KPI_Month;

#Valores Indicadores
update performance_inbound_telesales_agents
set
value_conversion=total_gross_orders/total_calls,
value_adherence= if((conection_hours_4weeks-pauses_hours_4weeks)/(conection_available_hours_4weeks-pauses_available_hours_month)>1,1,
(conection_hours_4weeks-pauses_hours_4weeks)/(conection_available_hours_4weeks-pauses_available_hours_month)),
value_tmo=(total_duration_calls_sec/total_calls)/60,
value_quality=((total_monitoring-critical_mistakes)*100/total_monitoring)*0.6+(sum_califications_not_critical/total_monitoring)*0.4,
value_training=training_calification,
value_survey=if(total_transfered_calls/(total_calls-total_transfered_queue)>0.8,
survey_answered_good/survey_answered,((survey_answered_good/survey_answered)*(total_transfered_calls/(total_calls-total_transfered_queue)))/0.8),
value_activity=if(activity_calls/total_calls>1,1,activity_calls/total_calls),
value_absence=total_absence,
value_delays=total_delays
where MonthNum=@KPI_Month;

#Actualizar KPI
update performance_inbound_telesales_agents
set
kpi_conversion=if(value_conversion>=0.20,1,value_conversion/0.20) ,
kpi_adherence=if(value_adherence>=0.98,1,if(value_adherence>=0.95,0.5,0)) ,
kpi_tmo=if(value_tmo<=5,1,if(value_tmo<=5.25,0.98,if(value_tmo<=6,0.8,if(value_tmo<=7,0.5,0)))),
kpi_quality=if(value_quality>=90,value_quality/100,if(value_quality>=80,0.7,if(value_quality>=70,0.5,0))),
kpi_training=if(value_training>=90,value_training/100,if(value_training>=80,0.7,if(value_training>=70,0.5,0))),
kpi_survey=value_survey,
kpi_activity=value_activity,
kpi_absence=if(value_absence=0,1,if(value_absence=1,0.5,0)),
kpi_delays=if(value_delays=0,1,if(value_delays=1,0.9,if(value_delays=2,0.8,0)))
where MonthNum=@KPI_Month;

#Principales KPI del mes
update performance_inbound_telesales_agents
set
kpi_productivity=kpi_conversion*0.45+kpi_adherence*0.40+kpi_tmo*0.15,
kpi_quality_training_nsu=kpi_quality*0.3+kpi_training*0.2+kpi_survey*0.4+kpi_activity*0.1,
kpi_absences_and_delays=kpi_absence*0.5+kpi_delays*0.5
where MonthNum=@KPI_Month;

#Indicador General del Mes
update performance_inbound_telesales_agents
set
kpi_general=kpi_productivity*0.4+kpi_quality_training_nsu*0.5+kpi_absences_and_delays*0.1
where MonthNum=@KPI_Month;

#Borrar datos de asesores de Postventa si existen
delete from perfomance_agents_general
where yrmonth=@KPI_Month
and proceso='VENTAS';

#Agregar datos a la evaluación general
insert into perfomance_agents_general
select MonthNum, null, Identification, Agent, coordinador, 0, kpi_productivity, kpi_quality_training_nsu, kpi_absences_and_delays,
kpi_general, proceso  from performance_inbound_telesales_agents
where status='ACTIVO' and proceso='VENTAS'
and MonthNum=@KPI_Month;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `performance_telesales_leaders`()
BEGIN


SET @KPI_Month=cast(date_format(curdate(),"%Y%m")-1 as unsigned);
SET @Perfomance_max_date=cast((select max(event_date) from customer_service_co.bi_ops_cc_dyalogo
where date_format(event_date,"%Y%m%")=@KPI_Month) as date);


delete from performance_telesales_leaders
where MonthNum=@KPI_Month;

#Insertar coordinadores activos
insert into  performance_telesales_leaders 
(MonthNum, Coordinator, Identification, proceso, coordinador, status, start_date, shift_pattern)
select @KPI_Month, name, identification, process,  coordinator, status, start_date, shift_pattern from bi_ops_matrix_sac_ventas
where process like '%coordinador%' and status='ACTIVO'
and date=@Perfomance_max_date;


#Actualizar datos de Inbound 
update performance_telesales_leaders a
inner join
(select nombre_matrix, count(1) cant, total_duration_calls_sec, conection_available_hours_month*3600 conection_available_sec, avg(value_conversion) conversion,
avg(value_tmo) tmo, avg(value_adherence) adherence, avg(value_training) training,  avg(value_quality) quality, avg(value_survey) survey, avg(value_activity) activity, avg(kpi_delays) delays,
avg(kpi_absence)  absence
from performance_inbound_telesales_agents a 
inner join bi_ops_cc_leaders b
on a.coordinador=b.coordinador
where MonthNum=@KPI_Month
and proceso like '%VENTAS%'
and status='ACTIVO'
group by a.coordinador) b
on a.Coordinator=b.nombre_matrix
set
total_agents=cant,
call_time_seg=total_duration_calls_sec,
conection_available_sec_month=conection_available_sec,
value_tmo=tmo,
value_adherence=adherence,
value_conversion=conversion,
value_training_team=training,
value_quality=quality,
value_survey=survey,
value_activity=activity,
value_delays=delays,
value_absence=absence
where MonthNum=@KPI_Month;

#Actualizar nota de cumplimiento de monitoreos
update performance_telesales_leaders a 
inner join customer_service_co.bi_ops_cc_cumplimiento_monitoreos b 
on a.Identification=b.cedula
and a.MonthNum=b.mes
set
value_punctuality=nota
where MonthNum=@KPI_Month;

#Actualizar nota de cumplimiento de feedback
update performance_telesales_leaders a 
inner join customer_service_co.bi_ops_cc_feedback_ventas b 
on a.Identification=b.cedula
and a.MonthNum=b.mes
set
value_feedback=nota
where MonthNum=@KPI_Month;

#Agregar nivel de servicio y abandono
update performance_telesales_leaders
set
value_service_level=(select sum(answered_in_wh_20)/sum(net_event) from customer_service_co.bi_ops_cc_dyalogo
where date_format(event_date,"%Y%m")=@KPI_Month
and pre=1 and net_event=1),
value_abandon=(select sum(unanswered_in_wh)/sum(net_event) from customer_service_co.bi_ops_cc_dyalogo
where date_format(event_date,"%Y%m")=@KPI_Month
and pre=1 and net_event=1)
where MonthNum=@KPI_Month;


update  performance_telesales_leaders a
inner join customer_service_co.bi_ops_cc_training b
on a.Identification=b.cedula and a.MonthNum=b.mes
set
value_training_leader=nota
where MonthNum=@KPI_Month;

#Valor general de training
update performance_telesales_leaders
set 
value_training=value_training_team*0.4+value_training_leader*0.6
where MonthNum=@KPI_Month;

#Actualizar KPI
update performance_telesales_leaders
set
kpi_conversion =if(value_conversion>=0.2,1,value_conversion/0.2) ,
kpi_adherence=if(value_adherence>=0.98,1,if(value_adherence>=0.95,0.5,0)) ,
kpi_service_level=if(value_service_level>=0.80,1,0),
kpi_abandon=if(value_abandon<=0.03,1,0) ,
kpi_tmo=if(value_tmo<=5,1,if(value_tmo<=5.25,0.98,if(value_tmo<=6,0.8,if(value_tmo<=7,0.5,0)))),
kpi_quality=if(value_quality>=90,value_quality/100,if(value_quality>=80,0.7,if(value_quality>=70,0.5,0))),
kpi_training=if(value_training>=90,value_training/100,if(value_training>=80,0.7,if(value_training>=70,0.5,0))),
kpi_survey=value_survey,
kpi_activity=value_activity,
kpi_punctuality=value_punctuality/100,
kpi_feedback=value_feedback/100,
kpi_absence=value_absence,
kpi_delays=value_delays
where MonthNum=@KPI_Month;

#Principales KPI del mes
update performance_telesales_leaders
set
kpi_productivity=kpi_conversion*0.40+kpi_adherence*0.20+kpi_tmo*0.10+kpi_service_level*0.15+kpi_abandon*0.15,
kpi_quality_training_nsu=kpi_quality*0.35+kpi_training*0.2+kpi_survey*0.25+kpi_activity*0.1+kpi_punctuality*0.05+kpi_feedback*0.05,
kpi_absences_and_delays=kpi_absence*0.5+kpi_delays*0.5
where MonthNum=@KPI_Month;

#Indicador General del Mes
update performance_telesales_leaders
set
kpi_general=kpi_productivity*0.4+kpi_quality_training_nsu*0.5+kpi_absences_and_delays*0.1
where MonthNum=@KPI_Month;

#Borrar datos de asesores de Postventa si existen
delete from perfomance_coord_general
where yrmonth=@KPI_Month
and proceso='TELESALES';

#Agregar datos a la evaluación general
insert into perfomance_coord_general
select MonthNum, Coordinator,  0, kpi_productivity, kpi_quality_training_nsu, kpi_absences_and_delays,0,
kpi_general, 'TELESALES'  from performance_telesales_leaders
where status='ACTIVO' 
and MonthNum=@KPI_Month;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:03:36
