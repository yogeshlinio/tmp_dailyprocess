-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: dev_marketing
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'dev_marketing'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `campaign_mx`()
begin

delete from dev_marketing.campaign_mx;

/*insert into dev_marketing.campaign_mx select date, channel, campaign, order_nr obc, oac, sku, product_name, ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0) as net_revenue, ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(payment_cost,0)-ifnull(shipping_cost_per_sku,0) as `PC1.5` from production.tbl_order_detail where obc=1 and production.week_exit(date) = production.week_exit(curdate())-1 and year(date)=year(curdate());*/ 

insert into dev_marketing.campaign_mx select date, channel, campaign, order_nr obc, oac, sku, product_name, ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0) as net_revenue, ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(payment_cost,0)-ifnull(shipping_cost_per_sku,0) as `PC1.5` from production.tbl_order_detail where obc=1 and yrmonth>=201307;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `cashback_surprise`()
begin

update dev_marketing.vouchers v inner join dev_marketing.recompra r on v.custid=r.custid set v.voucher=r.voucher, v.voucher_value=r.voucher_value; 

drop table dev_marketing.cashback_surprise;

create table dev_marketing.cashback_surprise as select a.*, case when order_value between order_value-mod(order_value, 500) and order_value+mod(order_value, 500) and order_value>=1000 and pilot_flag='cashback' then (order_value-mod(order_value, 500)+500)/100 when order_value between 50 and 999 and pilot_flag='surprise' then 50 when order_value between 1000 and 3499 and pilot_flag='surprise' then 100 when order_value between 3500 and 4499 and pilot_flag='surprise' then 200 when order_value between 4500 and 7499 and pilot_flag='surprise' then 250 when order_value between 7500 and 10499 and pilot_flag='surprise' then 500 when order_value >=10500 and pilot_flag='surprise' then 1000 else 0 end as new_voucher_value, case when coupon_code in (select voucher from dev_marketing.vouchers) then 1 else 0 end as voucher_used from (select id_sales_order, date, c.first_name, c.last_name, c.email, v.pilot_flag, t.custid, t.order_nr, t.coupon_code, coupon_money_value, sum(actual_paid_price) as order_value, case when v.voucher=t.coupon_code then 1 else 0 end as voucher_allocate from production.tbl_order_detail t, dev_marketing.vouchers v, bob_live_mx.customer c, bob_live_mx.sales_order s where oac=1 and date between '2013-10-23' and '2013-10-29' and t.custid=v.custid and t.custid=c.id_customer and s.order_nr=t.order_nr group by t.order_nr order by date)a;

create table dev_marketing.temporary_cashback(
custid int,
voucher_used int,
new_voucher_value float
);

insert into dev_marketing.temporary_cashback select * from (select custid, voucher_used, sum(new_voucher_value) as new_voucher_value from dev_marketing.cashback_surprise where pilot_flag='cashback' group by custid)a where new_voucher_value!=0;

update dev_marketing.temporary_cashback t inner join dev_marketing.vouchers v on t.custid=v.custid set new_voucher_value=ifnull(new_voucher_value,0)+ifnull(v.voucher_value,0) where voucher_used=0;

create table dev_marketing.temporary_surprise(
custid int,
order_nr varchar(255),
voucher_used int,
new_voucher_value float,
state varchar(255)
);

insert into dev_marketing.temporary_surprise select custid, order_nr, voucher_used, new_voucher_value, case when substring(id_sales_order, -2) between '00' and '24' then 'Winner' else 'Loser' end from dev_marketing.cashback_surprise where pilot_flag='surprise'; 

create table dev_marketing.temporary_surprise2(
custid int,
voucher_used int,
new_voucher_value float,
state varchar(255)
); 

insert into dev_marketing.temporary_surprise2 select * from (select custid, voucher_used, sum(new_voucher_value) as new_voucher_value, state from dev_marketing.temporary_surprise where state='Winner' group by custid)a where new_voucher_value!=0; 

update dev_marketing.temporary_surprise2 t inner join dev_marketing.vouchers v on t.custid=v.custid set new_voucher_value=ifnull(new_voucher_value,0)+ifnull(v.voucher_value,0) where voucher_used=0;

insert into dev_marketing.temporary_surprise2 select * from (select custid, voucher_used, sum(new_voucher_value) as new_voucher_value, state from dev_marketing.temporary_surprise where state='Loser' group by custid)a where new_voucher_value!=0 and custid not in (select custid from dev_marketing.temporary_surprise2 where state='Winner'); 

drop table dev_marketing.cashback_surprise_result;

create table dev_marketing.cashback_surprise_result(
custid int,
email varchar(255),
pilot_flag varchar(255),
surprise_flag varchar(255),
new_voucher_value float,
voucher_to_cancel varchar(255)
);

insert into dev_marketing.cashback_surprise_result(custid, pilot_flag, surprise_flag, new_voucher_value) select custid, 'cashback', '', new_voucher_value from temporary_cashback union select custid, 'surprise', state, new_voucher_value from temporary_surprise2;

update dev_marketing.cashback_surprise_result r inner join dev_marketing.cashback_surprise c on r.custid=c.custid set r.email=c.email;

update dev_marketing.cashback_surprise_result r inner join  dev_marketing.cashback_surprise c on r.custid=c.custid inner join dev_marketing.vouchers v on v.custid=r.custid set voucher_to_cancel=v.voucher where c.voucher_used=0;

update dev_marketing.cashback_surprise_result set voucher_to_cancel=null where surprise_flag='Loser' or voucher_to_cancel='';

drop table dev_marketing.temporary_cashback;

drop table dev_marketing.temporary_surprise;

drop table dev_marketing.temporary_surprise2;

-- Reminder

delete from dev_marketing.coupon_reminder;

create table dev_marketing.reminder_coupon_code_step_1 as select distinct coupon_code as coupon_code from production.tbl_order_detail where date between '2013-10-23' and '2013-10-29' and obc=1;

create table dev_marketing.reminder_coupon_code_step_2 select distinct v.custid as custid from dev_marketing.vouchers v where v.voucher !='' and v.voucher not in (select coupon_code from dev_marketing.reminder_coupon_code_step_1 where coupon_code is not null);

insert into dev_marketing.coupon_reminder select distinct t.custid, c.email, v.voucher_value, v.voucher from dev_marketing.vouchers v, bob_live_mx.customer c, dev_marketing.reminder_coupon_code_step_2 t where v.custid=c.id_customer and v.custid=t.custid;

delete from dev_marketing.coupon_reminder where custid in (select custid from cashback_surprise_result where surprise_flag!='Loser');

/*create table dev_marketing.reminder_coupon_code_step_3 select distinct v.custid as custid from dev_marketing.reminder_coupon_code_step_2 v, dev_marketing.cashback_surprise_result t where v.custid!=t.custid and t.pilot_flag;*/

drop table dev_marketing.reminder_coupon_code_step_1;

drop table dev_marketing.reminder_coupon_code_step_2;

-- drop table dev_marketing.reminder_coupon_code_step_3;

delete from dev_marketing.cashback_surprise_final;

insert into dev_marketing.cashback_surprise_final select t.email, t.pilot_flag, v.voucher_value, v.voucher from dev_marketing.cashback_surprise_result t, dev_marketing.vouchers v where v.custid=t.custid and t.pilot_flag='cashback';

insert into dev_marketing.cashback_surprise_final select t.email, 'recompensas', v.voucher_value, v.voucher from dev_marketing.cashback_surprise_result t, dev_marketing.vouchers v where v.custid=t.custid and t.pilot_flag='surprise' and t.surprise_flag='Winner';

insert into dev_marketing.cashback_surprise_final select t.email, 'recompensas_noganas', v.voucher_value, v.voucher from dev_marketing.cashback_surprise_result t, dev_marketing.vouchers v where v.custid=t.custid and t.pilot_flag='surprise' and t.surprise_flag='Loser';

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `col_customers`()
begin

delete from dev_marketing.col_customers;

insert into dev_marketing.col_customers
select c.first_name, c.last_name, c.email, t.date as last_purchase, t.sku, t.product_name, t.n1 as category from production_co.tbl_order_detail t, bob_live_co.customer c where t.custid=c.id_customer and date=(select max(date) from production_co.tbl_order_detail z where t.custid=z.custid and oac=1 group by z.custid) group by t.custid order by date desc;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `customers_rev`()
BEGIN

call dev_marketing.customers_rev_ve;
call dev_marketing.customers_rev_co;
call dev_marketing.customers_rev_pe;
call dev_marketing.customers_rev_mx;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`luis.ochoa`@`%`*/ /*!50003 PROCEDURE `customers_rev_co`()
begin

#create index customer on bob_live_co.customer(id_customer);
#create index customers_rev_id on dev_marketing.customers_rev_co(custid);
#create index custid on production_co.tbl_order_detail(custid);

#New Version
select  'CRM Customers Revenue Table: start',now();

delete from dev_marketing.customers_rev_co;

-- alter table dev_marketing.customers_rev_co add column custid integer;

-- create index customers_rev_id on dev_marketing.customers_rev_co(custid);

alter table dev_marketing.customers_rev_co modify MONETARY_VALUE float;
alter table dev_marketing.customers_rev_co modify Sending_Time integer;
alter table dev_marketing.customers_rev_co modify RECENCY integer;
alter table dev_marketing.customers_rev_co modify AV_TICKET float;

insert into dev_marketing.customers_rev_co(date_registred, first_name, last_name, custid, email, gender, MONETARY_VALUE)
select c.created_at, c.first_name, c.last_name, c.id_customer, c.email, c.gender, net.MONETARY_VALUE from bob_live_co.customer c left join (select i.custid as custid, sum(i.paid_price) as MONETARY_VALUE from production_co.tbl_order_detail i where i.oac=1 group by i.custid order by MONETARY_VALUE desc)net on c.id_customer=net.custid;

update dev_marketing.customers_rev_co set Source_data = 'Accoount_creation';

insert into dev_marketing.customers_rev_co(date_registred, email, gender)
select created_at, email, gender from bob_live_co.newsletter_subscription where fk_customer is null; 

update dev_marketing.customers_rev_co set Source_data = 'Bob_newsletter' where Source_data is null;

insert into dev_marketing.customers_rev_co (email) select email from dev_marketing.open_hour_crm_co r where r.email not in (select email from dev_marketing.customers_rev_co);  

INSERT INTO dev_marketing.customers_rev_co (
	date_registred,
	email,
	Source_data
) SELECT
	Date,
	email,
	Source
FROM
	(
		SELECT
			A.*
		FROM
			test_linio.mailsCOL A
		LEFT JOIN dev_marketing.customers_rev_co B ON A.email = B.email
		WHERE
			B.email IS NULL
	) C;

INSERT INTO dev_marketing.customers_rev_co (
	date_registred,
	email,
	Source_data
) SELECT
	Date,
	email,
	Source
FROM
	(
		SELECT
			A.*
		FROM
			test_linio.nuevosMailsCOL A
		LEFT JOIN dev_marketing.customers_rev_co B ON A.email = B.email
		WHERE
			B.email IS NULL
	) C;

alter table dev_marketing.customers_rev_co modify MONETARY_VALUE varchar(100);

update dev_marketing.customers_rev_co set MONETARY_VALUE = 'none' where MONETARY_VALUE is null;

update dev_marketing.customers_rev_co set gender = 'neutral' where gender is null;

update dev_marketing.customers_rev_co rev inner join production_co.tbl_order_detail i on rev.custid=i.custid set TYPE_CUSTOMER= 'customer' where i.oac=1;

update dev_marketing.customers_rev_co rev set TYPE_CUSTOMER= 'non customer' where TYPE_CUSTOMER is null;

update dev_marketing.customers_rev_co rev inner join dev_marketing.open_hour_crm_co crm on rev.email=crm.email set Sending_Time= most_frequently_open_hour;

alter table dev_marketing.customers_rev_co modify Sending_Time varchar(100);

update dev_marketing.customers_rev_co set Sending_Time= 'never open/sent newsletter' where Sending_Time is null;

update dev_marketing.customers_rev_co rev set RECENCY =
(select datediff(curdate(), max(date)) as days from production_co.tbl_order_detail i where i.custid=rev.custid and i.oac=1 group by custid);

alter table dev_marketing.customers_rev_co modify RECENCY varchar(100);

update dev_marketing.customers_rev_co rev set RECENCY = 'never bought' where RECENCY is null;

update dev_marketing.customers_rev_co rev set AV_TICKET= ( 
select avg(paid_price) from production_co.tbl_order_detail i where oac=1 and rev.custid=i.custid group by custid);

alter table dev_marketing.customers_rev_co modify AV_TICKET varchar(100);

update dev_marketing.customers_rev_co rev set AV_TICKET= 'none' where AV_TICKET is null;

update dev_marketing.customers_rev_co rev set FREQUENCY= (select datediff(max(date), min(date))/(count(distinct date)-1) as avg_time from production_co.tbl_order_detail i where oac=1 and i.custid=rev.custid group by custid);

update dev_marketing.customers_rev_co set FREQUENCY= 0 where FREQUENCY is null;

update dev_marketing.customers_rev_co rev set LOCATION= ( 
select region from bob_live_co.customer_address a where rev.custid=a.fk_customer group by fk_customer);

update dev_marketing.customers_rev_co rev set LOCATION= 'unknown' where LOCATION is null;

update dev_marketing.customers_rev_co rev set LOCATION= ( 
select region from production_co.tbl_order_detail i where rev.custid=i.custid group by custid) where LOCATION='unknown' or location='';

update dev_marketing.customers_rev_co rev set LOCATION= 'unknown' where LOCATION is null;

#UMS FORMAT
update dev_marketing.customers_rev_co set MONETARY_VALUE = '0' where MONETARY_VALUE='none'; 
update dev_marketing.customers_rev_co set Sending_Time= '100' where Sending_Time='never open/sent newsletter'; 
update dev_marketing.customers_rev_co set RECENCY = '0' where RECENCY='never bought'; 
update dev_marketing.customers_rev_co set AV_TICKET= '0' where AV_TICKET='none';

alter table dev_marketing.customers_rev_co modify MONETARY_VALUE float;
alter table dev_marketing.customers_rev_co modify Sending_Time integer;
alter table dev_marketing.customers_rev_co modify RECENCY integer;
alter table dev_marketing.customers_rev_co modify AV_TICKET float;
#UMS FORMAT

update dev_marketing.customers_rev_co rev inner join bob_live_co.customer_address c on rev.custid=c.fk_customer set rev.sms=c.mobile_phone;

update dev_marketing.customers_rev_co rev inner join bob_live_co.customer c on rev.custid=c.id_customer set rev.birthday=c.birthday;

update dev_marketing.customers_rev_co rev set rev.new_registry = case when datediff(curdate(), date_registred)<30 then 1 else 0 end;

update dev_marketing.customers_rev_co set new_registry = 0 where new_registry is null;

update dev_marketing.customers_rev_co rev inner join production_co.tbl_order_detail i on rev.custid=i.custid set rev.boughtcoupon= case when (select count(coupon_code) from production_co.tbl_order_detail z where z.custid=rev.custid group by custid)>0 then 1 else 0 end, transactions = (select count(distinct order_nr) from production_co.tbl_order_detail m where oac=1 and rev.custid=m.custid group by custid);

update dev_marketing.customers_rev_co set boughtcoupon = 0 where boughtcoupon is null;

update dev_marketing.customers_rev_co set transactions = 0 where transactions is null;

-- alter table dev_marketing.customers_rev_co drop column custid;

alter ignore table dev_marketing.customers_rev_co add primary key (email);

alter ignore table dev_marketing.customers_rev_co drop primary key;

DROP TEMPORARY TABLE IF EXISTS dev_marketing.TMP3 ; 
CREATE TEMPORARY TABLE dev_marketing.TMP3 
SELECT
email,code, sales_rule.is_active, to_date, used_discount_amount
from bob_live_co.newsletter_subscription 
inner join bob_live_co.sales_rule
on fk_sales_rule=id_sales_rule 
inner join bob_live_co.sales_rule_set on
fk_sales_rule_set=id_sales_rule_set
where code like 'NL%';

create index email on dev_marketing.TMP3(email);

UPDATE dev_marketing.customers_rev_co A
INNER JOIN TMP3
ON A.email = TMP3.email
SET A.Subscription_voucher = TMP3.code,
    A.active = TMP3.is_active,
    A.days_to_inactive = datediff(TMP3.to_date,curdate()),
    A.used = CASE WHEN TMP3.used_discount_amount > 0 THEN 1 ELSE 0 END
;

 update dev_marketing.customers_rev_co rev 
set rev.new_registry_voucher = case when datediff(curdate(), date_registred)<29 
and TYPE_CUSTOMER = 'non customer' 
and Subscription_voucher is not NULL
and active = 1
and days_to_inactive >= 2
and used = 0
then 1 else 0 end;

delete from dev_marketing.customers_rev_co where (email like '%test%'and email like '%linio%') or email like '%mailsolution%';
delete from dev_marketing.customers_rev_co where email like '%@bounceexchangetest.com%';



select  'CRM Customers Revenue Table: end',now();
#End New Version

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`luis.ochoa`@`%`*/ /*!50003 PROCEDURE `customers_rev_mx`()
begin

#create index customer on bob_live_mx.customer(id_customer);
#create index customers_rev_id on dev_marketing.customers_rev_mx(custid);
#create index custid on production.tbl_order_detail(custid);

#New Version
select  'CRM Customers Revenue Table: start',now();

delete from dev_marketing.customers_rev_mx;

-- alter table dev_marketing.customers_rev_mx add column custid integer;

/*create table dev_marketing.unique_id(
email varchar(255),
unique_id varchar(255)
);
create index email on dev_marketing.unique_id(email);

insert into dev_marketing.unique_id select email, unique_id from dev_marketing.customers_rev_mx;*/

-- create index customers_rev_id on dev_marketing.customers_rev_mx(custid);

-- alter table dev_marketing.customers_rev_mx drop unique_id;
alter table dev_marketing.customers_rev_mx modify MONETARY_VALUE float;
alter table dev_marketing.customers_rev_mx modify Sending_Time integer;
alter table dev_marketing.customers_rev_mx modify RECENCY integer;
alter table dev_marketing.customers_rev_mx modify AV_TICKET float;

insert into dev_marketing.customers_rev_mx(date_registred, first_name, last_name, custid, email, gender, MONETARY_VALUE)
select c.created_at, c.first_name, c.last_name, c.id_customer, c.email, c.gender, net.MONETARY_VALUE from bob_live_mx.customer c left join (select i.custid as custid, sum(i.actual_paid_price) as MONETARY_VALUE from production.tbl_order_detail i where i.oac=1 group by i.custid order by MONETARY_VALUE desc)net on c.id_customer=net.custid;

update dev_marketing.customers_rev_mx set Source_data = 'Accoount_creation';

insert into dev_marketing.customers_rev_mx(date_registred, email, gender, Source_data)
select created_at, email, gender, source from bob_live_mx.newsletter_subscription where fk_customer is null; 

update dev_marketing.customers_rev_mx set Source_data = CONCAT('Bob_newsletter ', Source_data) where Source_data not like 'Accoount_creation';

insert into dev_marketing.customers_rev_mx(email) select email from dev_marketing.open_hour_crm_mx r where r.email not in (select email from dev_marketing.customers_rev_mx);  

alter table dev_marketing.customers_rev_mx modify MONETARY_VALUE varchar(100);

INSERT INTO dev_marketing.customers_rev_mx (
	date_registred,
	email,
	Source_data,
	first_name,
	last_name,
	gender
) SELECT
	date,
	email,
	Source,
	first_name,
	last_name,
	gender
FROM
	(
		SELECT
			A.*
		FROM
			test_linio.nuevosMailsMEX A
		LEFT JOIN dev_marketing.customers_rev_mx B ON A.email = B.email
		WHERE
			B.email IS NULL
	) C;

INSERT INTO dev_marketing.customers_rev_mx (
	date_registred,
	email,
	Source_data,
	first_name,
	last_name,
	gender
) SELECT
	date,
	email,
	Base,
	first_name,
	last_name,
	gender
FROM
	(
		SELECT
			A.*
		FROM
			test_linio.mailsMEX A
		LEFT JOIN dev_marketing.customers_rev_mx B ON A.email = B.email
		WHERE
			B.email IS NULL
	) C;


update dev_marketing.customers_rev_mx set MONETARY_VALUE = 'none' where MONETARY_VALUE is null;

update dev_marketing.customers_rev_mx set gender = 'neutral' where gender is null;

update dev_marketing.customers_rev_mx rev inner join production.tbl_order_detail i on rev.custid=i.custid set TYPE_CUSTOMER= 'customer' where i.oac=1;

update dev_marketing.customers_rev_mx rev set TYPE_CUSTOMER= 'non customer' where TYPE_CUSTOMER is null;

update dev_marketing.customers_rev_mx rev inner join dev_marketing.open_hour_crm_mx crm on rev.email=crm.email set Sending_Time= most_frequently_open_hour;

alter table dev_marketing.customers_rev_mx modify Sending_Time varchar(100);

update dev_marketing.customers_rev_mx set Sending_Time= 'never open/sent newsletter' where Sending_Time is null;

update dev_marketing.customers_rev_mx rev set RECENCY =
(select datediff(curdate(), max(date)) as days from production.tbl_order_detail i where i.custid=rev.custid and i.oac=1 group by custid);

alter table dev_marketing.customers_rev_mx modify RECENCY varchar(100);

update dev_marketing.customers_rev_mx rev set RECENCY = 'never bought' where RECENCY is null;

update dev_marketing.customers_rev_mx rev set AV_TICKET= ( 
select avg(actual_paid_price) from production.tbl_order_detail i where oac=1 and rev.custid=i.custid group by custid);

alter table dev_marketing.customers_rev_mx modify AV_TICKET varchar(100);

update dev_marketing.customers_rev_mx rev set AV_TICKET= 'none' where AV_TICKET is null;

update dev_marketing.customers_rev_mx rev set FREQUENCY= (select datediff(max(date), min(date))/(count(distinct date)-1) as avg_time from production.tbl_order_detail i where oac=1 and i.custid=rev.custid group by custid);

update dev_marketing.customers_rev_mx set FREQUENCY= 0 where FREQUENCY is null;

update dev_marketing.customers_rev_mx rev set LOCATION= ( 
select region from bob_live_mx.customer_address a where rev.custid=a.fk_customer group by fk_customer);

update dev_marketing.customers_rev_mx rev set LOCATION= 'unknown' where LOCATION is null;

update dev_marketing.customers_rev_mx rev set LOCATION= ( 
select region from production.tbl_order_detail i where rev.custid=i.custid group by custid) where LOCATION='unknown' or location='';

update dev_marketing.customers_rev_mx rev set LOCATION= 'unknown' where LOCATION is null;

#UMS FORMAT
update dev_marketing.customers_rev_mx set MONETARY_VALUE = '0' where MONETARY_VALUE='none'; 
update dev_marketing.customers_rev_mx set Sending_Time= '100' where Sending_Time='never open/sent newsletter'; 
update dev_marketing.customers_rev_mx set RECENCY = '0' where RECENCY='never bought'; 
update dev_marketing.customers_rev_mx set AV_TICKET= '0' where AV_TICKET='none';

alter table dev_marketing.customers_rev_mx modify MONETARY_VALUE float;
alter table dev_marketing.customers_rev_mx modify Sending_Time integer;
alter table dev_marketing.customers_rev_mx modify RECENCY integer;
alter table dev_marketing.customers_rev_mx modify AV_TICKET float;
#UMS FORMAT

update dev_marketing.customers_rev_mx rev inner join bob_live_mx.customer_address c on rev.custid=c.fk_customer set rev.sms=c.mobile_phone;

update dev_marketing.customers_rev_mx rev inner join bob_live_mx.customer c on rev.custid=c.id_customer set rev.birthday=c.birthday;

update dev_marketing.customers_rev_mx rev set rev.new_registry = case when datediff(curdate(), date_registred)<29 then 1 else 0 end;

update dev_marketing.customers_rev_mx set new_registry = 0 where new_registry is null;

update dev_marketing.customers_rev_mx rev inner join production.tbl_order_detail i on rev.custid=i.custid set rev.boughtcoupon= case when (select count(coupon_code) from production.tbl_order_detail z where z.custid=rev.custid group by custid)>0 then 1 else 0 end, transactions = (select count(distinct order_nr) from production.tbl_order_detail m where oac=1 and rev.custid=m.custid group by custid);

update dev_marketing.customers_rev_mx set boughtcoupon = 0 where boughtcoupon is null;

update dev_marketing.customers_rev_mx set transactions = 0 where transactions is null;

-- alter table dev_marketing.customers_rev_mx drop column custid;

alter ignore table dev_marketing.customers_rev_mx add primary key (email);

alter ignore table dev_marketing.customers_rev_mx drop primary key;

-- Control Group

/* alter table dev_marketing.customers_rev_mx AUTO_INCREMENT = 1;

alter table dev_marketing.customers_rev_mx add unique_id int;

update dev_marketing.customers_rev_mx x inner join dev_marketing.unique_id u on x.email=u.email set x.unique_id=u.unique_id;

alter table dev_marketing.customers_rev_mx modify unique_id int auto_increment primary key;

alter table dev_marketing.customers_rev_mx modify unique_id varchar(100);

update dev_marketing.customers_rev_mx set unique_id='01' where unique_id='1';

update dev_marketing.customers_rev_mx set unique_id='02' where unique_id='2';

update dev_marketing.customers_rev_mx set unique_id='03' where unique_id='3';

update dev_marketing.customers_rev_mx set unique_id='04' where unique_id='4';

update dev_marketing.customers_rev_mx set unique_id='05' where unique_id='5';

update dev_marketing.customers_rev_mx set unique_id='06' where unique_id='6';

update dev_marketing.customers_rev_mx set unique_id='07' where unique_id='7';

update dev_marketing.customers_rev_mx set unique_id='08' where unique_id='8';

update dev_marketing.customers_rev_mx set unique_id='09' where unique_id='9';

update dev_marketing.customers_rev_mx set control_group=1 where substring(unique_id, -2) in ('00', '12', '13');

update dev_marketing.customers_rev_mx set control_group=2 where substring(unique_id, -2) in ('01','02','03','04','05','06','07','08','09','10','11');

/*update dev_marketing.customers_rev_mx set control_group=3 where substring(unique_id, -2) = (select vara from dev_marketing.control_group3_rules where date=curdate()) or substring(unique_id, -2) = (select varb from dev_marketing.control_group3_rules where date=curdate()) or substring(unique_id, -2) = (select varc from dev_marketing.control_group3_rules where date=curdate()) or substring(unique_id, -2) = (select vard from dev_marketing.control_group3_rules where date=curdate());*/

/* update dev_marketing.customers_rev_mx set control_group=0 where control_group is null;

-- update dev_marketing.customers_rev_mx set target_group=1 where weekday(curdate()) in (0, 2) and control_group = 2;

-- update dev_marketing.customers_rev_mx set target_group=0 where control_group = 2 and target_group is null;

update dev_marketing.customers_rev_mx set target_group=0 where control_group=2;

update dev_marketing.customers_rev_mx set target_group=0 where control_group=1;

update dev_marketing.customers_rev_mx set target_group=1 where control_group=0;

update dev_marketing.customers_rev_mx set target_group=0 where control_group=3;

update dev_marketing.customers_rev_mx set Account= case when custid is not null then 1 else 0 end;*/

update dev_marketing.customers_rev_mx set Account= case when custid is not null then 1 else 0 end;

update dev_marketing.customers_rev_mx a inner join bob_live_mx.newsletter_subscription b on a.email = b.email set a.Suscribed = case when b.status = 'subscribed' then 1 else 0 end;


UPDATE dev_marketing.customers_rev_mx A
INNER JOIN test_linio.mailsMEX B ON A.email = B.email
SET A.Source_data = B.base;


DROP TEMPORARY TABLE IF EXISTS dev_marketing.TMP ; 
CREATE TEMPORARY TABLE dev_marketing.TMP 
SELECT
email,code, sales_rule.is_active, to_date, used_discount_amount
from bob_live_mx.newsletter_subscription 
inner join bob_live_mx.sales_rule
on fk_sales_rule=id_sales_rule 
inner join bob_live_mx.sales_rule_set on
fk_sales_rule_set=id_sales_rule_set
where code like 'NL%';

create index email on dev_marketing.TMP(email);

UPDATE dev_marketing.customers_rev_mx A
INNER JOIN TMP
ON A.email = TMP.email
SET A.Subscription_voucher = TMP.code,
    A.active = TMP.is_active,
    A.days_to_inactive = datediff(TMP.to_date, curdate()),
    A.active = CASE WHEN TMP.is_active = 1 AND A.used = 0 AND A.days_to_inactive <= 30 and A.days_to_inactive >= 0 THEN 1 ELSE 0 END
;


update dev_marketing.customers_rev_mx rev 
set rev.new_registry_voucher = case when datediff(curdate(), date_registred)<30 
and TYPE_CUSTOMER = 'non customer' 
and Subscription_voucher is not NULL
and active = 1
and days_to_inactive >= 2
and used = 0
then 1 else 0 end;

delete from dev_marketing.customers_rev_mx where (email like '%test%'and email like '%linio%') or email like '%mailsolution%';
delete from dev_marketing.customers_rev_mx where email like '%@bounceexchangetest.com%';

-- drop table dev_marketing.unique_id;

select  'CRM Customers Revenue Table: end',now();
#End New Version

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`luis.ochoa`@`%`*/ /*!50003 PROCEDURE `customers_rev_pe`()
begin

#create index customer on bob_live_pe.customer(id_customer);
#create index customers_rev_id on dev_marketing.customers_rev_pe(custid);
#create index custid on production_pe.tbl_order_detail(custid);

#New Version
select  'CRM Customers Revenue Table: start',now();

delete from dev_marketing.customers_rev_pe;

-- alter table dev_marketing.customers_rev_pe add column custid integer;

-- create index customers_rev_id on dev_marketing.customers_rev_pe(custid);

alter table dev_marketing.customers_rev_pe modify MONETARY_VALUE float;
alter table dev_marketing.customers_rev_pe modify Sending_Time integer;
alter table dev_marketing.customers_rev_pe modify RECENCY integer;
alter table dev_marketing.customers_rev_pe modify AV_TICKET float;

insert into dev_marketing.customers_rev_pe(date_registred, first_name, last_name, custid, email, gender, MONETARY_VALUE)
select c.created_at, c.first_name, c.last_name, c.id_customer, c.email, c.gender, net.MONETARY_VALUE from bob_live_pe.customer c left join (select i.custid as custid, sum(i.paid_price) as MONETARY_VALUE from production_pe.tbl_order_detail i where i.oac=1 group by i.custid order by MONETARY_VALUE desc)net on c.id_customer=net.custid;

update dev_marketing.customers_rev_pe set Source_data = 'Accoount_creation';

insert into dev_marketing.customers_rev_pe(date_registred, email, gender)
select created_at, email, gender from bob_live_pe.newsletter_subscription where fk_customer is null; 

update dev_marketing.customers_rev_pe set Source_data = 'Bob_newsletter' where Source_data is null;

#insert into dev_marketing.customers_rev_pe (email) select email from dev_marketing.open_hour_crm_pe r where r.email not in (select email from dev_marketing.customers_rev_pe);  

INSERT INTO dev_marketing.customers_rev_pe (
	date_registred,
	email,
	Source_data
) SELECT
	Date,
	email,
	Source
FROM
	(
		SELECT
			A.*
		FROM
			test_linio.mailsPE A
		LEFT JOIN dev_marketing.customers_rev_pe B ON A.email = B.email
		WHERE
			B.email IS NULL
	) C;

alter table dev_marketing.customers_rev_pe modify MONETARY_VALUE varchar(100);

update dev_marketing.customers_rev_pe set MONETARY_VALUE = 'none' where MONETARY_VALUE is null;

update dev_marketing.customers_rev_pe set gender = 'neutral' where gender is null;

update dev_marketing.customers_rev_pe rev inner join production_pe.tbl_order_detail i on rev.custid=i.custid set TYPE_CUSTOMER= 'customer' where i.oac=1;

update dev_marketing.customers_rev_pe rev set TYPE_CUSTOMER= 'non customer' where TYPE_CUSTOMER is null;

#update dev_marketing.customers_rev_pe rev inner join dev_marketing.open_hour_crm_pe crm on rev.email=crm.email set Sending_Time= most_frequently_open_hour;

alter table dev_marketing.customers_rev_pe modify Sending_Time varchar(100);

update dev_marketing.customers_rev_pe set Sending_Time= 'never open/sent newsletter' where Sending_Time is null;

update dev_marketing.customers_rev_pe rev set RECENCY =
(select datediff(curdate(), max(date)) as days from production_pe.tbl_order_detail i where i.custid=rev.custid and i.oac=1 group by custid);

alter table dev_marketing.customers_rev_pe modify RECENCY varchar(100);

update dev_marketing.customers_rev_pe rev set RECENCY = 'never bought' where RECENCY is null;

update dev_marketing.customers_rev_pe rev set AV_TICKET= ( 
select avg(paid_price) from production_pe.tbl_order_detail i where oac=1 and rev.custid=i.custid group by custid);

alter table dev_marketing.customers_rev_pe modify AV_TICKET varchar(100);

update dev_marketing.customers_rev_pe rev set AV_TICKET= 'none' where AV_TICKET is null;

update dev_marketing.customers_rev_pe rev set FREQUENCY= (select datediff(max(date), min(date))/(count(distinct date)-1) as avg_time from production_pe.tbl_order_detail i where oac=1 and i.custid=rev.custid group by custid);

update dev_marketing.customers_rev_pe set FREQUENCY= 0 where FREQUENCY is null;

update dev_marketing.customers_rev_pe rev set LOCATION= ( 
select region from bob_live_pe.customer_address a where rev.custid=a.fk_customer group by fk_customer);

update dev_marketing.customers_rev_pe rev set LOCATION= 'unknown' where LOCATION is null;

update dev_marketing.customers_rev_pe rev set LOCATION= ( 
select region from production_pe.tbl_order_detail i where rev.custid=i.custid group by custid) where LOCATION='unknown' or location='';

update dev_marketing.customers_rev_pe rev set LOCATION= 'unknown' where LOCATION is null;

#UMS FORMAT
update dev_marketing.customers_rev_pe set MONETARY_VALUE = '0' where MONETARY_VALUE='none'; 
update dev_marketing.customers_rev_pe set Sending_Time= '100' where Sending_Time='never open/sent newsletter'; 
update dev_marketing.customers_rev_pe set RECENCY = '0' where RECENCY='never bought'; 
update dev_marketing.customers_rev_pe set AV_TICKET= '0' where AV_TICKET='none';

alter table dev_marketing.customers_rev_pe modify MONETARY_VALUE float;
alter table dev_marketing.customers_rev_pe modify Sending_Time integer;
alter table dev_marketing.customers_rev_pe modify RECENCY integer;
alter table dev_marketing.customers_rev_pe modify AV_TICKET float;
#UMS FORMAT

update dev_marketing.customers_rev_pe rev inner join bob_live_pe.customer_address c on rev.custid=c.fk_customer set rev.sms=c.mobile_phone;

update dev_marketing.customers_rev_pe rev inner join bob_live_pe.customer c on rev.custid=c.id_customer set rev.birthday=c.birthday;

update dev_marketing.customers_rev_pe rev set rev.new_registry = case when datediff(curdate(), date_registred)<30 then 1 else 0 end;

update dev_marketing.customers_rev_pe set new_registry = 0 where new_registry is null;

update dev_marketing.customers_rev_pe rev inner join production_pe.tbl_order_detail i on rev.custid=i.custid set rev.boughtcoupon= case when (select count(coupon_code) from production_pe.tbl_order_detail z where z.custid=rev.custid group by custid)>0 then 1 else 0 end, transactions = (select count(distinct order_nr) from production_pe.tbl_order_detail m where oac=1 and rev.custid=m.custid group by custid);

update dev_marketing.customers_rev_pe set boughtcoupon = 0 where boughtcoupon is null;

update dev_marketing.customers_rev_pe set transactions = 0 where transactions is null;

-- alter table dev_marketing.customers_rev_pe drop column custid;

alter ignore table dev_marketing.customers_rev_pe add primary key (email);

alter ignore table dev_marketing.customers_rev_pe drop primary key;

DROP TEMPORARY TABLE IF EXISTS dev_marketing.TMP1 ; 
CREATE TEMPORARY TABLE dev_marketing.TMP1 
SELECT
email,code, sales_rule.is_active, to_date, used_discount_amount
from bob_live_pe.newsletter_subscription 
inner join bob_live_pe.sales_rule
on fk_sales_rule=id_sales_rule 
inner join bob_live_pe.sales_rule_set on
fk_sales_rule_set=id_sales_rule_set
where code like 'NL%';

create index email on dev_marketing.TMP1(email);

UPDATE dev_marketing.customers_rev_pe A
INNER JOIN TMP1
ON A.email = TMP1.email
SET A.Subscription_voucher = TMP1.code,
    A.active = TMP1.is_active,
    A.days_to_inactive = datediff(TMP1.to_date,curdate()),
    A.used = CASE WHEN TMP1.used_discount_amount > 0 THEN 1 ELSE 0 END
;

update dev_marketing.customers_rev_pe rev 
set rev.new_registry_voucher = case when datediff(curdate(), date_registred)<29 
and TYPE_CUSTOMER = 'non customer' 
and Subscription_voucher is not NULL
and active = 1
and days_to_inactive >= 2
and used = 0
then 1 else 0 end;

delete from dev_marketing.customers_rev_pe where (email like '%test%'and email like '%linio%') or email like '%mailsolution%';
delete from dev_marketing.customers_rev_pe where email like '%@bounceexchangetest.com%';


select  'CRM Customers Revenue Table: end',now();
#End New Version

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`luis.ochoa`@`%`*/ /*!50003 PROCEDURE `customers_rev_ve`()
begin

#create index customer on bob_live_ve.customer(id_customer);
#create index customers_rev_id on dev_marketing.customers_rev_ve(custid);
#create index custid on production_ve.tbl_order_detail(custid);

#New Version
select  'CRM Customers Revenue Table: start',now();

delete from dev_marketing.customers_rev_ve;

-- alter table dev_marketing.customers_rev_ve add column custid integer;

-- create index customers_rev_id on dev_marketing.customers_rev_ve(custid);

alter table dev_marketing.customers_rev_ve modify MONETARY_VALUE float;
alter table dev_marketing.customers_rev_ve modify Sending_Time integer;
alter table dev_marketing.customers_rev_ve modify RECENCY integer;
alter table dev_marketing.customers_rev_ve modify AV_TICKET float;

insert into dev_marketing.customers_rev_ve(date_registred, first_name, last_name, custid, email, gender, MONETARY_VALUE)
select c.created_at, c.first_name, c.last_name, c.id_customer, c.email, c.gender, net.MONETARY_VALUE from bob_live_ve.customer c left join (select i.custid as custid, sum(i.paid_price) as MONETARY_VALUE from production_ve.tbl_order_detail i where i.oac=1 group by i.custid order by MONETARY_VALUE desc)net on c.id_customer=net.custid;

update dev_marketing.customers_rev_ve set Source_data = 'Accoount_creation';

insert into dev_marketing.customers_rev_ve(date_registred, email, gender)
select created_at, email, gender from bob_live_ve.newsletter_subscription where fk_customer is null; 

update dev_marketing.customers_rev_ve set Source_data = 'Bob_newsletter' where Source_data is null;

#insert into dev_marketing.customers_rev_ve (email) select email from dev_marketing.open_hour_crm_ve r where r.email not in (select email from dev_marketing.customers_rev_ve);  

INSERT INTO dev_marketing.customers_rev_ve (
	date_registred,
	email,
	Source_data
) SELECT
	Date,
	email,
	Source
FROM
	(
		SELECT
			A.*
		FROM
			test_linio.mailsVEN A
		LEFT JOIN dev_marketing.customers_rev_ve B ON A.email = B.email
		WHERE
			B.email IS NULL
	) C;

alter table dev_marketing.customers_rev_ve modify MONETARY_VALUE varchar(100);

update dev_marketing.customers_rev_ve set MONETARY_VALUE = 'none' where MONETARY_VALUE is null;

update dev_marketing.customers_rev_ve set gender = 'neutral' where gender is null;

update dev_marketing.customers_rev_ve rev inner join production_ve.tbl_order_detail i on rev.custid=i.custid set TYPE_CUSTOMER= 'customer' where i.oac=1;

update dev_marketing.customers_rev_ve rev set TYPE_CUSTOMER= 'non customer' where TYPE_CUSTOMER is null;

#update dev_marketing.customers_rev_ve rev inner join dev_marketing.open_hour_crm_ve crm on rev.email=crm.email set Sending_Time= most_frequently_open_hour;

alter table dev_marketing.customers_rev_ve modify Sending_Time varchar(100);

update dev_marketing.customers_rev_ve set Sending_Time= 'never open/sent newsletter' where Sending_Time is null;

update dev_marketing.customers_rev_ve rev set RECENCY =
(select datediff(curdate(), max(date)) as days from production_ve.tbl_order_detail i where i.custid=rev.custid and i.oac=1 group by custid);

alter table dev_marketing.customers_rev_ve modify RECENCY varchar(100);

update dev_marketing.customers_rev_ve rev set RECENCY = 'never bought' where RECENCY is null;

update dev_marketing.customers_rev_ve rev set AV_TICKET= ( 
select avg(paid_price) from production_ve.tbl_order_detail i where oac=1 and rev.custid=i.custid group by custid);

alter table dev_marketing.customers_rev_ve modify AV_TICKET varchar(100);

update dev_marketing.customers_rev_ve rev set AV_TICKET= 'none' where AV_TICKET is null;

update dev_marketing.customers_rev_ve rev set FREQUENCY= (select datediff(max(date), min(date))/(count(distinct date)-1) as avg_time from production_ve.tbl_order_detail i where oac=1 and i.custid=rev.custid group by custid);

update dev_marketing.customers_rev_ve set FREQUENCY= 0 where FREQUENCY is null;

update dev_marketing.customers_rev_ve rev set LOCATION= ( 
select region from bob_live_ve.customer_address a where rev.custid=a.fk_customer group by fk_customer);

update dev_marketing.customers_rev_ve rev set LOCATION= 'unknown' where LOCATION is null;

update dev_marketing.customers_rev_ve rev set LOCATION= ( 
select region from production_ve.tbl_order_detail i where rev.custid=i.custid group by custid) where LOCATION='unknown' or location='';

update dev_marketing.customers_rev_ve rev set LOCATION= 'unknown' where LOCATION is null;

#UMS FORMAT
update dev_marketing.customers_rev_ve set MONETARY_VALUE = '0' where MONETARY_VALUE='none'; 
update dev_marketing.customers_rev_ve set Sending_Time= '100' where Sending_Time='never open/sent newsletter'; 
update dev_marketing.customers_rev_ve set RECENCY = '0' where RECENCY='never bought'; 
update dev_marketing.customers_rev_ve set AV_TICKET= '0' where AV_TICKET='none';

alter table dev_marketing.customers_rev_ve modify MONETARY_VALUE float;
alter table dev_marketing.customers_rev_ve modify Sending_Time integer;
alter table dev_marketing.customers_rev_ve modify RECENCY integer;
alter table dev_marketing.customers_rev_ve modify AV_TICKET float;
#UMS FORMAT

update dev_marketing.customers_rev_ve rev inner join bob_live_ve.customer_address c on rev.custid=c.fk_customer set rev.sms=c.mobile_phone;

update dev_marketing.customers_rev_ve rev inner join bob_live_ve.customer c on rev.custid=c.id_customer set rev.birthday=c.birthday;

update dev_marketing.customers_rev_ve rev set rev.new_registry = case when datediff(curdate(), date_registred)<30 then 1 else 0 end;

update dev_marketing.customers_rev_ve set new_registry = 0 where new_registry is null;

update dev_marketing.customers_rev_ve rev inner join production_ve.tbl_order_detail i on rev.custid=i.custid set rev.boughtcoupon= case when (select count(coupon_code) from production_ve.tbl_order_detail z where z.custid=rev.custid group by custid)>0 then 1 else 0 end, transactions = (select count(distinct order_nr) from production_ve.tbl_order_detail m where oac=1 and rev.custid=m.custid group by custid);

update dev_marketing.customers_rev_ve set boughtcoupon = 0 where boughtcoupon is null;

update dev_marketing.customers_rev_ve set transactions = 0 where transactions is null;

-- alter table dev_marketing.customers_rev_ve drop column custid;

alter ignore table dev_marketing.customers_rev_ve add primary key (email);

alter ignore table dev_marketing.customers_rev_ve drop primary key;

DROP TEMPORARY TABLE IF EXISTS dev_marketing.TMP2 ; 
CREATE TEMPORARY TABLE dev_marketing.TMP2 
SELECT
email,code, sales_rule.is_active, to_date, used_discount_amount
from bob_live_ve.newsletter_subscription 
inner join bob_live_ve.sales_rule
on fk_sales_rule=id_sales_rule 
inner join bob_live_ve.sales_rule_set on
fk_sales_rule_set=id_sales_rule_set
where code like 'NL%';

create index email on dev_marketing.TMP2(email);

UPDATE dev_marketing.customers_rev_ve A
INNER JOIN TMP2
ON A.email = TMP2.email
SET A.Subscription_voucher = TMP2.code,
    A.active = TMP2.is_active,
    A.days_to_inactive = datediff(TMP2.to_date,curdate()),
    A.used = CASE WHEN TMP2.used_discount_amount > 0 THEN 1 ELSE 0 END
;


update dev_marketing.customers_rev_ve rev 
set rev.new_registry_voucher = case when datediff(curdate(), date_registred)<29 
and TYPE_CUSTOMER = 'non customer' 
and Subscription_voucher is not NULL
and active = 1
and days_to_inactive >= 2
and used = 0
then 1 else 0 end;

delete from dev_marketing.customers_rev_ve where (email like '%test%'and email like '%linio%') or email like '%mailsolution%';
delete from dev_marketing.customers_rev_ve where email like '%@bounceexchangetest.com%';

select  'CRM Customers Revenue Table: end',now();
#End New Version

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `facebook_optimization_mx`()
begin

select  'Facebook Optimization Table: start',now();

update dev_marketing.facebook_campaign_mx set campaign = replace(campaign, '_', '.');

delete from dev_marketing.facebook_optimization_mx;

insert into dev_marketing.facebook_optimization_mx (hour, date, campaign, country, region, city, obc_transactions, visits, gross_revenue, carts) select hour, date, campaign, country, region, city, transactions, visits, transaction_revenue, goal2starts from dev_marketing.ga_facebook_ads_mx;

update dev_marketing.facebook_optimization_mx set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end;

update dev_marketing.facebook_optimization_mx f inner join dev_marketing.ga_facebook_transaction_id_mx id on f.hour=id.hour and f.date=id.date and f.campaign=id.campaign and f.country=id.country and f.region=id.region and f.city=id.city set f.oac_transactions = (select count(distinct a.order_nr) from production.tbl_order_detail a where a.order_nr=id.transaction_id  and a.oac=1), f.net_revenue = (select sum(b.actual_paid_price) from production.tbl_order_detail b where b.order_nr=id.transaction_id  and b.oac=1), f.new_customers = (select sum(c.new_customers) from production.tbl_order_detail c where c.order_nr=id.transaction_id and c.oac=1); 

update dev_marketing.facebook_optimization_mx set oac_transactions = 0 where oac_transactions is null;

update dev_marketing.facebook_optimization_mx set net_revenue = 0 where net_revenue is null;

update dev_marketing.facebook_optimization_mx set new_customers = 0 where new_customers is null;

update dev_marketing.facebook_optimization_mx f set f.cost = (select b.spent/a.count from  (select date, campaign, count(campaign) as count from dev_marketing.facebook_optimization_mx group by date, campaign)a, (select date, campaign, sum(spent) as spent, sum(clicks) as clicks, sum(social_impressions) as impressions from dev_marketing.facebook_campaign_mx group by date, campaign)b where a.date=b.date and a.campaign=b.campaign and f.date=a.date and f.campaign=a.campaign);

update dev_marketing.facebook_optimization_mx f set f.clicks = (select b.clicks/a.count from  (select date, campaign, count(campaign) as count from dev_marketing.facebook_optimization_mx group by date, campaign)a, (select date, campaign, sum(spent) as spent, sum(clicks) as clicks, sum(social_impressions) as impressions from dev_marketing.facebook_campaign_mx group by date, campaign)b where a.date=b.date and a.campaign=b.campaign and f.date=a.date and f.campaign=a.campaign);

update dev_marketing.facebook_optimization_mx f set f.impressions = (select b.impressions/a.count from  (select date, campaign, count(campaign) as count from dev_marketing.facebook_optimization_mx group by date, campaign)a, (select date, campaign, sum(spent) as spent, sum(clicks) as clicks, sum(social_impressions) as impressions from dev_marketing.facebook_campaign_mx group by date, campaign)b where a.date=b.date and a.campaign=b.campaign and f.date=a.date and f.campaign=a.campaign);

update dev_marketing.facebook_optimization_mx set cost = 0 where cost is null;

update dev_marketing.facebook_optimization_mx set clicks = 0 where clicks is null;

update dev_marketing.facebook_optimization_mx set impressions = 0 where impressions is null;

update dev_marketing.facebook_optimization_mx set category = substr(substr(campaign, locate('.',campaign)+1), 1, locate('.', substr(campaign, locate('.',campaign)+1))-1);

update dev_marketing.facebook_optimization_mx set gender = substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1) ,1 , locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))-1);

update dev_marketing.facebook_optimization_mx set age = substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,1 ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))-1);

update dev_marketing.facebook_optimization_mx set segmentation = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) ,1 ,locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))-1);

update dev_marketing.facebook_optimization_mx set extra_field = substr(substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1) , locate('.', substr(substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1) ,locate('.', substr(substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1),locate('.', substr(substr(campaign, locate('.',campaign)+1) ,locate('.' ,substr(campaign, locate('.',campaign)+1))+1))+1))+1))+1);

select  'Facebook Optimization Table: end',now();

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `open_hour_crm_co`()
begin

delete from dev_marketing.open_hour_crm_co;

insert into dev_marketing.open_hour_crm_co select email, rounded_hour from (select email, rounded_hour, count(rounded_hour) as more_freq from dev_marketing.raw_data_open_hour_crm_co group by email, rounded_hour order by email, more_freq desc)a group by email;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `open_hour_crm_mx`()
begin

delete from dev_marketing.open_hour_crm_mx;

insert into dev_marketing.open_hour_crm_mx select email, rounded_hour from (select email, rounded_hour, count(rounded_hour) as more_freq from dev_marketing.raw_data_open_hour_crm_mx group by email, rounded_hour order by email, more_freq desc)a group by email;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `payment_method_overall`()
begin

delete from dev_marketing.payment_method_overall;

insert into dev_marketing.payment_method_overall
select g.payment_method, gross_orders, gross_rev, avg_gross_ticket, net_orders, net_rev, avg_net_ticket from (select payment_method, count(distinct order_num) as gross_orders, sum(actual_paid_price_after_tax) as gross_rev, sum(actual_paid_price_after_tax)/count(distinct order_num) as avg_gross_ticket from production.out_sales_report_item where order_before_can=1 group by payment_method)g, (select payment_method, count(distinct order_num) as net_orders, sum(actual_paid_price_after_tax) as net_rev, sum(actual_paid_price_after_tax)/count(distinct order_num) as avg_net_ticket from production.out_sales_report_item where order_after_can=1 group by payment_method)n where g.payment_method=n.payment_method;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`borja.fernandez`@`%`*/ /*!50003 PROCEDURE `performance_crm`()
begin

-- Performance Report
-- Mexico

	DELETE
FROM
	dev_marketing.performance_report;

INSERT INTO dev_marketing.performance_report (
	country,
	date,
	source_medium,
	campaign,
	visits,
	carts
) SELECT
	'Mexico',
	date,
	concat(source, ' / ', MEDIUM),
	campaign,
	sum(visits),
	sum(cart)
FROM
	marketing.ga_performance
WHERE
	source = 'Postal'
AND MEDIUM = 'CRM'
AND COUNTRY = 'MEX'
GROUP BY
	date,
	campaign;

UPDATE dev_marketing.performance_report p
SET gross_transactions = (
	SELECT
		count(*)
	FROM
		marketing.ga_transaction c  
	WHERE
		c.campaign = p.campaign
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium 
	AND c.date = p.date 
)
WHERE
	country = 'Mexico'; 

UPDATE dev_marketing.performance_report p
SET gross_revenue = (
	SELECT
		sum(total_value)
	FROM
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND c.date = p.date
)
WHERE
	country = 'Mexico';

UPDATE dev_marketing.performance_report p
SET avg_gross_rev = gross_revenue / gross_transactions
WHERE
	country = 'Mexico';

UPDATE dev_marketing.performance_report
SET gross_conversion_rate = gross_transactions / visits
WHERE
	country = 'Mexico';

UPDATE dev_marketing.performance_report p
SET net_transactions = (
	SELECT
		count(DISTINCT OrderNum)
	FROM
		development_mx.A_Master t, 
		marketing.ga_transaction c 
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderAfterCan = 1
)
WHERE
	country = 'Mexico';

UPDATE dev_marketing.performance_report p
SET net_revenue = (
	SELECT
		sum(
			rev
		)
	FROM
		development_mx.A_Master t,  
		marketing.ga_transaction c  
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderAfterCan = 1
)
WHERE
	country = 'Mexico';

UPDATE dev_marketing.performance_report p
SET net_items = (
	SELECT
		count(OrderNum)
	FROM
		development_mx.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderAfterCan = 1
)
WHERE
	country = 'Mexico';

UPDATE dev_marketing.performance_report p
SET gross_items = (
	SELECT
		count(OrderNum)
	FROM
		development_mx.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderBeforeCan = 1
)
WHERE
	country = 'Mexico';

UPDATE dev_marketing.performance_report p
SET avg_net_rev = net_revenue / net_transactions
WHERE
	country = 'Mexico';

UPDATE dev_marketing.performance_report
SET net_conversion_rate = net_transactions / visits
WHERE
	country = 'Mexico';

UPDATE dev_marketing.performance_report p
SET PC1 = (
	SELECT
		sum(
			PCOne
		)
	FROM
		development_mx.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderAfterCan = 1
)
WHERE
	country = 'Mexico';

UPDATE dev_marketing.performance_report p
SET `PC1.5` = (
	SELECT
		sum(
			PCOnePFive
		)
	FROM
		development_mx.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND t.OrderAfterCan = 1
)
WHERE
	country = 'Mexico';

UPDATE dev_marketing.performance_report p
SET PC2 = (
	SELECT
		sum(
			PCTwo
		)
	FROM
		development_mx.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderAfterCan = 1
)
WHERE
	country = 'Mexico';

UPDATE dev_marketing.performance_report
SET `%PC1` = PC1 / net_revenue
WHERE
	country = 'Mexico';

UPDATE dev_marketing.performance_report
SET `%PC1.5` = `PC1.5` / net_revenue
WHERE
	country = 'Mexico';

UPDATE dev_marketing.performance_report
SET `%PC2` = PC2 / net_revenue
WHERE
	country = 'Mexico';

-- Colombia
INSERT INTO dev_marketing.performance_report (
	country,
	date,
	source_medium,
	campaign,
	visits,
	carts
) SELECT
	'Colombia',
	date,
	concat(source, ' / ', MEDIUM),
	campaign,
	sum(visits),
	sum(cart)
FROM
	marketing.ga_performance
WHERE
	source = 'Postal'
AND MEDIUM = 'CRM'
AND COUNTRY = 'COL'
GROUP BY
	date,
	campaign;

UPDATE dev_marketing.performance_report p
SET gross_transactions = (
	SELECT
		count(*)
	FROM
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND c.date = p.date
)
WHERE
	country = 'Colombia';

UPDATE dev_marketing.performance_report p
SET gross_revenue = (
	SELECT
		sum(total_value)
	FROM
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND c.date = p.date
)
WHERE
	country = 'Colombia';

UPDATE dev_marketing.performance_report p
SET avg_gross_rev = gross_revenue / gross_transactions
WHERE
	country = 'Colombia';

UPDATE dev_marketing.performance_report
SET gross_conversion_rate = gross_transactions / visits
WHERE
	country = 'Colombia';

UPDATE dev_marketing.performance_report p
SET net_transactions = (
	SELECT
		count(DISTINCT OrderNum)
	FROM
		development_co_project.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderAfterCan = 1
)
WHERE
	country = 'Colombia';

UPDATE dev_marketing.performance_report p
SET net_revenue = (
	SELECT
		sum(
			rev
		)
	FROM
		development_co_project.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderAfterCan = 1
)
WHERE
	country = 'Colombia';

UPDATE dev_marketing.performance_report p
SET net_items = (
	SELECT
		count(OrderNum)
	FROM
		development_co_project.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderAfterCan = 1
)
WHERE
	country = 'Colombia';

UPDATE dev_marketing.performance_report p
SET gross_items = (
	SELECT
		count(OrderNum)
	FROM
		development_co_project.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderBeforeCan = 1
)
WHERE
	country = 'Colombia';

UPDATE dev_marketing.performance_report p
SET avg_net_rev = net_revenue / net_transactions
WHERE
	country = 'Colombia';

UPDATE dev_marketing.performance_report
SET net_conversion_rate = net_transactions / visits
WHERE
	country = 'Colombia';

UPDATE dev_marketing.performance_report p
SET PC1 = (
	SELECT
		sum(
			PCOne
		)
	FROM
		development_co_project.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderAfterCan = 1
)
WHERE
	country = 'Colombia';

UPDATE dev_marketing.performance_report p
SET `PC1.5` = (
	SELECT
		sum(
			PCOnePFive
		)
	FROM
		development_co_project.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderAfterCan = 1
)
WHERE
	country = 'Colombia';

UPDATE dev_marketing.performance_report p
SET PC2 = (
	SELECT
		sum(
			PCTwo
		)
	FROM
		development_co_project.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderAfterCan = 1
)
WHERE
	country = 'Colombia';

UPDATE dev_marketing.performance_report
SET `%PC1` = PC1 / net_revenue
WHERE
	country = 'Colombia';

UPDATE dev_marketing.performance_report
SET `%PC1.5` = `PC1.5` / net_revenue
WHERE
	country = 'Colombia';

UPDATE dev_marketing.performance_report
SET `%PC2` = PC2 / net_revenue
WHERE
	country = 'Colombia';

-- Peru
INSERT INTO dev_marketing.performance_report (
	country,
	date,
	source_medium,
	campaign,
	visits,
	carts
) SELECT
	'Peru',
	date,
	concat(source, ' / ', MEDIUM),
	campaign,
	sum(visits),
	sum(cart)
FROM
	marketing.ga_performance
WHERE
	source = 'Postal'
AND MEDIUM = 'CRM'
AND COUNTRY = 'PER'
GROUP BY
	date,
	campaign;

UPDATE dev_marketing.performance_report p
SET gross_transactions = (
	SELECT
		count(*)
	FROM
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND c.date = p.date
)
WHERE
	country = 'Peru';

UPDATE dev_marketing.performance_report p
SET gross_revenue = (
	SELECT
		sum(total_value)
	FROM
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND c.date = p.date
)
WHERE
	country = 'Peru';

UPDATE dev_marketing.performance_report p
SET avg_gross_rev = gross_revenue / gross_transactions
WHERE
	country = 'Peru';

UPDATE dev_marketing.performance_report
SET gross_conversion_rate = gross_transactions / visits
WHERE
	country = 'Peru';

UPDATE dev_marketing.performance_report p
SET net_transactions = (
	SELECT
		count(DISTINCT OrderNum)
	FROM
		development_pe.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderAfterCan = 1
)
WHERE
	country = 'Peru';

UPDATE dev_marketing.performance_report p
SET net_revenue = (
	SELECT
		sum(
			rev
		)
	FROM
		development_pe.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderAfterCan = 1
)
WHERE
	country = 'Peru';

UPDATE dev_marketing.performance_report p
SET net_items = (
	SELECT
		count(OrderNum)
	FROM
		development_pe.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderAfterCan = 1
)
WHERE
	country = 'Peru';

UPDATE dev_marketing.performance_report p
SET gross_items = (
	SELECT
		count(OrderNum)
	FROM
		development_pe.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum= c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderBeforeCan = 1
)
WHERE
	country = 'Peru';

UPDATE dev_marketing.performance_report p
SET avg_net_rev = net_revenue / net_transactions
WHERE
	country = 'Peru';

UPDATE dev_marketing.performance_report
SET net_conversion_rate = net_transactions / visits
WHERE
	country = 'Peru';

UPDATE dev_marketing.performance_report p
SET PC1 = (
	SELECT
		sum(
			PCOne
		)
	FROM
		development_pe.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderAfterCan = 1
)
WHERE
	country = 'Peru';

UPDATE dev_marketing.performance_report p
SET `PC1.5` = (
	SELECT
		sum(
			PCOnePFive
		)
	FROM
		development_pe.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderAfterCan = 1
)
WHERE
	country = 'Peru';

UPDATE dev_marketing.performance_report p
SET PC2 = (
	SELECT
		sum(
			PCTwo
		)
	FROM
		development_pe.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderAfterCan = 1
)
WHERE
	country = 'Peru';

UPDATE dev_marketing.performance_report
SET `%PC1` = PC1 / net_revenue
WHERE
	country = 'Peru';

UPDATE dev_marketing.performance_report
SET `%PC1.5` = `PC1.5` / net_revenue
WHERE
	country = 'Peru';

UPDATE dev_marketing.performance_report
SET `%PC2` = PC2 / net_revenue
WHERE
	country = 'Peru';

-- Venezuela
INSERT INTO dev_marketing.performance_report (
	country,
	date,
	source_medium,
	campaign,
	visits,
	carts
) SELECT
	'Venezuela',
	date,
	concat(source, ' / ', MEDIUM),
	campaign,
	sum(visits),
	sum(cart)
FROM
	marketing.ga_performance
WHERE
	source = 'Postal'
AND MEDIUM = 'CRM'
AND COUNTRY = 'VEN'
GROUP BY
	date,
	campaign;

UPDATE dev_marketing.performance_report p
SET gross_transactions = (
	SELECT
		count(*)
	FROM
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND c.date = p.date
)
WHERE
	country = 'Venezuela';

UPDATE dev_marketing.performance_report p
SET gross_revenue = (
	SELECT
		sum(total_value)
	FROM
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND c.date = p.date
)
WHERE
	country = 'Venezuela';

UPDATE dev_marketing.performance_report p
SET avg_gross_rev = gross_revenue / gross_transactions
WHERE
	country = 'Venezuela';

UPDATE dev_marketing.performance_report
SET gross_conversion_rate = gross_transactions / visits
WHERE
	country = 'Venezuela';

UPDATE dev_marketing.performance_report p
SET net_transactions = (
	SELECT
		count(DISTINCT OrderNum)
	FROM
		development_ve.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderAfterCan = 1
)
WHERE
	country = 'Venezuela';

UPDATE dev_marketing.performance_report p
SET net_revenue = (
	SELECT
		sum(
			rev
		)
	FROM
		development_ve.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderAfterCan = 1
)
WHERE
	country = 'Venezuela';

UPDATE dev_marketing.performance_report p
SET net_items = (
	SELECT
		count(OrderNum)
	FROM
		development_ve.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderAfterCan = 1
)
WHERE
	country = 'Venezuela';

UPDATE dev_marketing.performance_report p
SET gross_items = (
	SELECT
		count(OrderNum)
	FROM
		development_ve.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderBeforeCan = 1
)
WHERE
	country = 'Venezuela';

UPDATE dev_marketing.performance_report p
SET avg_net_rev = net_revenue / net_transactions
WHERE
	country = 'Venezuela';

UPDATE dev_marketing.performance_report
SET net_conversion_rate = net_transactions / visits
WHERE
	country = 'Venezuela';

UPDATE dev_marketing.performance_report p
SET PC1 = (
	SELECT
		sum(
			PCOne
		)
	FROM
		development_ve.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderAfterCan = 1
)
WHERE
	country = 'Venezuela';

UPDATE dev_marketing.performance_report p
SET `PC1.5` = (
	SELECT
		sum(
			PCOnePFive
		)
	FROM
		development_ve.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderAfterCan = 1
)
WHERE
	country = 'Venezuela';

UPDATE dev_marketing.performance_report p
SET PC2 = (
	SELECT
		sum(
			PCTwo
		)
	FROM
		development_ve.A_Master t,
		marketing.ga_transaction c
	WHERE
		c.campaign = p.campaign
	AND c.date = p.date
	AND t.OrderNum = c.transaction_id
	AND concat(c.source, ' / ', c. MEDIUM) = p.source_medium
	AND t.OrderAfterCan = 1
)
WHERE
	country = 'Venezuela';

UPDATE dev_marketing.performance_report
SET `%PC1` = PC1 / net_revenue
WHERE
	country = 'Venezuela';

UPDATE dev_marketing.performance_report
SET `%PC1.5` = `PC1.5` / net_revenue
WHERE
	country = 'Venezuela';

UPDATE dev_marketing.performance_report
SET `%PC2` = PC2 / net_revenue
WHERE
	country = 'Venezuela';

UPDATE dev_marketing.performance_report
SET yrmonth = concat(
	YEAR (date),

IF (
	MONTH (date) < 10,
	concat(0, MONTH(date)),
	MONTH (date)
)
),
 WEEK = production.week_iso (date);

UPDATE dev_marketing.performance_report s
INNER JOIN development_mx.A_E_BI_ExchangeRate r ON r.month_num = s.yrmonth
SET gross_revenue = gross_revenue / xr,
 avg_gross_rev = avg_gross_rev / xr,
 net_revenue = net_revenue / xr,
 avg_net_rev = avg_net_rev / xr,
 PC1 = PC1 / xr,
 `PC1.5` = `PC1.5` / xr,
 PC2 = PC2 / xr
WHERE
	s.country = 'Mexico'
AND r.country = 'MEX';

UPDATE dev_marketing.performance_report s
INNER JOIN development_mx.A_E_BI_ExchangeRate r ON r.month_num = s.yrmonth
SET gross_revenue = gross_revenue / xr,
 avg_gross_rev = avg_gross_rev / xr,
 net_revenue = net_revenue / xr,
 avg_net_rev = avg_net_rev / xr,
 PC1 = PC1 / xr,
 `PC1.5` = `PC1.5` / xr,
 PC2 = PC2 / xr
WHERE
	s.country = 'Colombia'
AND r.country = 'COL';

UPDATE dev_marketing.performance_report s
INNER JOIN development_mx.A_E_BI_ExchangeRate r ON r.month_num = s.yrmonth
SET gross_revenue = gross_revenue / xr,
 avg_gross_rev = avg_gross_rev / xr,
 net_revenue = net_revenue / xr,
 avg_net_rev = avg_net_rev / xr,
 PC1 = PC1 / xr,
 `PC1.5` = `PC1.5` / xr,
 PC2 = PC2 / xr
WHERE
	s.country = 'Peru'
AND r.country = 'PER';

UPDATE dev_marketing.performance_report s
INNER JOIN development_mx.A_E_BI_ExchangeRate r ON r.month_num = s.yrmonth
SET gross_revenue = gross_revenue / xr,
 avg_gross_rev = avg_gross_rev / xr,
 net_revenue = net_revenue / xr,
 avg_net_rev = avg_net_rev / xr,
 PC1 = PC1 / xr,
 `PC1.5` = `PC1.5` / xr,
 PC2 = PC2 / xr
WHERE
	s.country = 'Venezuela'
AND r.country = 'VEN';

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `sku_selection_mx`()
begin

delete from dev_marketing.sku_selection_7_mx;

insert into dev_marketing.sku_selection_7_mx(sku_config, PC2_average, revenue, transactions, average_price) select sku_config, avg(ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2, sum(actual_paid_price), count(distinct order_nr), avg(actual_paid_price) from production.tbl_order_detail where oac=1 and date >= date_sub(curdate(), interval 7 day) group by sku_config;

delete from dev_marketing.sku_selection_30_mx;

insert into dev_marketing.sku_selection_30_mx(sku_config, PC2_average, revenue, transactions, average_price) select sku_config, avg(ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2, sum(actual_paid_price), count(distinct order_nr), avg(actual_paid_price) from production.tbl_order_detail where oac=1 and date >= date_sub(curdate(), interval 30 day) group by sku_config;

delete from dev_marketing.sku_selection_60_mx;

insert into dev_marketing.sku_selection_60_mx(sku_config, PC2_average, revenue, transactions, average_price) select sku_config, avg(ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2, sum(actual_paid_price), count(distinct order_nr), avg(actual_paid_price) from production.tbl_order_detail where oac=1 and date >= date_sub(curdate(), interval 60 day) group by sku_config;

delete from dev_marketing.sku_selection_90_mx;

insert into dev_marketing.sku_selection_90_mx(sku_config, PC2_average, revenue, transactions, average_price) select sku_config, avg(ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2, sum(actual_paid_price), count(distinct order_nr), avg(actual_paid_price) from production.tbl_order_detail where oac=1 and date >= date_sub(curdate(), interval 90 day) group by sku_config;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:03:48
