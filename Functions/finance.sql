-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: finance
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'finance'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 PROCEDURE `cashflow_inputs`()
BEGIN

-- osri w oms cost
TRUNCATE TABLE finance.cashflow_inputs;

INSERT INTO finance.cashflow_inputs (
	Month_Num,
	WEEK,
	Date,
	created_at,
	OrderNum,
	OrderBeforeCan,
	OrderAfterCan,
	Cancellations,
	Pending,
	RETURNS,
	Payment_Method,
	Payment_Type,
	Installment,
	Fees_,
	Extra_Charge_MXN,
	Payment_Fees_MXN,
	STATUS,
	State,
	Product_weight,
	Package_weight,
	Shipping_Cost,
	Shipping_Fee_Charged,
	WH_cost_per_Item,
	CS_cost_per_Item,
	Confirm_Call_1,
	Exportable,
	Fraud_Check_Pending,
	Shipped,
	Category_BP,
	SKU_Config,
	SKU_Simple,
	SKU_Name,
	Brand,
	Supplier,
	Original_Price,
	Price,
	price_after_tax,
	Tax,
	paid_price,
	Cost,
	Cost_after_tax,
	Delivery_cost_supplier,
	Paid_price_after_tax,
	Actual_Paid_price,
	Actual_Paid_price_after_tax,
	Grand_Total,
	CustomerNum,
	coupon_money_value,
	after_vat_coupon,
	non_mkt_coupon_after_vat,
	coupon_code,
	code_prefix,
	tax_percent,
	Items_in_Order,
	COGS,
	ItemID,
	ShipmentCost,
	Rev
) 
SELECT
	osri.MonthNum,
	rafael.week_iso( Date ) ,
	osri.Date,
	osri.Date,
	osri.OrderNum,
	osri.OrderBeforeCan,
	osri.OrderAfterCan,
	osri.Cancellations,
	osri.Pending,
	osri. RETURNS,
	osri.PaymentMethod,
	"" AS PaymentType,
	osri.Installment,
	osri.Fees,
	osri.ExtraCharge,
	osri.PaymentFees,
	osri.STATUS,
	osri.State,
	osri.ProductWeight,
	osri.Packageweight,
	osri.ShippingCost,
	osri.ShippingFee,
	osri.FLWHCost,
	osri.FLCSCost,
	osri.ConfirmCall1,
	osri.Exportable,
	osri.FraudCheckPending,
	osri.Shipped,
	osri.CatBP,
	osri.SKUConfig,
	osri.SKUSimple,
	osri.SKUName,
	osri.Brand,
	osri.Supplier,
	osri.OriginalPrice,
	osri.Price,
	osri.priceaftertax,
	osri.Tax,
	osri.paidPrice,
	osri.Cost,
	osri.Costaftertax,
	osri.Deliverycostsupplier,
	osri.Paidpriceaftertax,
	0 AS ActualPaidPrice,
	0 AS ActualPaidpriceaftertax,
	osri.GrandTotal,
	osri.CustomerNum,
	osri.couponvalue,
	osri.CouponValueAfterTax,
	osri.NonMKTCouponAfterTax,
	osri.couponcode,
	osri.prefixCode,
	osri.taxpercent,
	osri.ItemsInOrder,
	osri.COGS,
	osri.ItemID,
	osri.ShipmentCost,
	osri.Rev

FROM
	development_mx.A_Master_Backup osri
WHERE
	osri.date > '2013-07-01 00:00:00';

UPDATE finance.cashflow_inputs
SET delivery_cost_supplier_after_tax = delivery_cost_supplier / (1.16);

UPDATE finance.cashflow_inputs
SET cogs = cost + delivery_cost_supplier;

UPDATE finance.cashflow_inputs
SET cogs_after_tax = cost_after_tax + delivery_cost_supplier_after_tax;

UPDATE finance.cashflow_inputs
SET cogs_oms = CASE
WHEN cost_oms <> 0 THEN
	cost_oms + delivery_cost_supplier
ELSE
	cost + delivery_cost_supplier
END;

UPDATE finance.cashflow_inputs
SET cogs_oms_after_tax = CASE
WHEN cost_oms_after_tax <> 0 THEN
	cost_oms_after_tax + delivery_cost_supplier_after_tax
ELSE
	cost_after_tax + delivery_cost_supplier_after_tax
END;

UPDATE finance.cashflow_inputs a
INNER JOIN production.out_order_tracking b ON a.ItemID = b.order_item_id
SET a.avg_days_total_shipped = datediff(
	b.date_shipped,
	b.date_ordered
),
 a.avg_days_total_delivery = b.days_total_delivery;

UPDATE finance.cashflow_inputs a
INNER JOIN production.out_order_tracking b ON a.ItemID = b.order_item_id
SET a.fullfilment_type_real = b.fullfilment_type_real,
 a.fullfilment_type_bob = b.fullfilment_type_bob;

UPDATE finance.cashflow_inputs a
INNER JOIN rafael.out_order_tracking b ON a.ItemID = b.order_item_id
SET a.fullfilment_type_bp = b.fullfilment_type_bp;

UPDATE finance.cashflow_inputs a
INNER JOIN bob_live_mx.sales_order b ON a.OrderNum = b.order_nr
INNER JOIN bob_live_mx.sales_order_accertify_log c ON b.id_sales_order = c.fk_sales_order
SET card_number = ExtractValue (
	c.message,
	'/transaction/paymentInformation/cardNumber'
)
WHERE
	c.title = 'transaction sent';

UPDATE finance.cashflow_inputs
SET bin = LEFT (card_number, 6);

UPDATE finance.cashflow_inputs a
INNER JOIN rafael.Pro_catalog_card_bins b ON a.bin = b.bin
SET a.bank = b.bank;

UPDATE finance.cashflow_inputs a
SET fullfilment_type_bp = CASE
WHEN fullfilment_type_real = 'crossdock' THEN
	'crossdocking'
WHEN fullfilment_type_real = 'inventory'
AND fullfilment_type_bob = 'consignment' THEN
	'consignment'
WHEN fullfilment_type_real = 'inventory'
AND fullfilment_type_bob <> 'consignment' THEN
	'outright buying'
WHEN fullfilment_type_real = 'dropshipping' THEN
	'other'
END;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 PROCEDURE `out_deliveries_reconciliation`()
BEGIN


DELETE finance.out_deliveries_reconciliation.*
FROM
	finance.out_deliveries_reconciliation;

INSERT INTO finance.out_deliveries_reconciliation (
	wms_tracking_code,
	entrega_id,
	order_number,
	date_delivered,
	week_delivered,
	amount_to_pay,
	shipping_carrier
) SELECT
	tms.cod_rastreamento,
	di.entrega_id,
	iv.numero_order,
	tms.date,
	rafael.week_iso (tms.date),
	avg(di.total_depois_de_impostos),
	t.nome_transportadora
FROM
	wmsprod.tms_status_delivery tms
INNER JOIN wmsprod.entrega e 
	ON e.cod_rastreamento = tms.cod_rastreamento
INNER JOIN wmsprod.delivery_invoice di 
	ON di.entrega_id = e.entrega_id
INNER JOIN wmsprod.pedidos_romaneio pr 
	ON pr.entrega_id = e.entrega_id
INNER JOIN wmsprod.romaneio r 
	ON r.romaneio_id = pr.romaneio_id
INNER JOIN wmsprod.transportadoras t 
	ON t.transportadoras_id = r.transportadora_id
INNER JOIN wmsprod.itens_entrega en 
	ON di.entrega_id = en.entrega_id
INNER JOIN wmsprod.itens_venda iv 
	ON en.itens_venda_id = iv.itens_venda_id
WHERE
	tms.id_status = 4
AND YEAR (tms.DATE) IN (2013, 2014)
GROUP BY
	tms.cod_rastreamento;

UPDATE finance.out_deliveries_reconciliation a
INNER JOIN wmsprod.vw_itens_venda_entrega b ON a.wms_tracking_code = b.cod_rastreamento
SET a.order_number = b.numero_order;

UPDATE finance.out_deliveries_reconciliation a
INNER JOIN wmsprod.pedidos_venda b ON a.order_number = b.numero_pedido
SET a.payment_method = b.metodo_de_pagamento;

UPDATE finance.out_deliveries_reconciliation a
INNER JOIN rafael.pro_tms_status_delivery1 b ON a.wms_tracking_code = b.wms_tracking_code
SET a.status_delivery = b. STATUS;

UPDATE finance.out_deliveries_reconciliation
SET month_delivered = date_format(date_delivered, "%Y-%m");



UPDATE finance.out_deliveries_reconciliation a
INNER JOIN finance.carriers_reports b
ON a.wms_tracking_code = b.wms_tracking_code
SET 
  a.date_deposited = b.date_deposited,
  a.deposit_carrier = b.deposit_carrier,
  a.deposit_id = b.deposit_id,
  a.bank_account = b.bank_account;

UPDATE finance.out_deliveries_reconciliation
SET 
  month_deposited = DATE_FORMAT(date_deposited,'%Y%m'),
  week_deposited = rafael.week_iso(date_deposited);

UPDATE finance.out_deliveries_reconciliation
SET 
 is_deposited = 1,
 amount_deposited = amount_to_pay
WHERE date_deposited IS NOT NULL;





DROP TABLE IF EXISTS finance.bank_statements_ys;

CREATE TABLE finance.bank_statements_ys LIKE finance.bank_statements; 

INSERT INTO finance.bank_statements_ys SELECT * FROM finance.bank_statements; 

DROP TABLE IF EXISTS finance.ccod_statements_ys;

CREATE TABLE finance.ccod_statements_ys LIKE finance.ccod_statements; 

INSERT INTO finance.ccod_statements_ys SELECT * FROM finance.ccod_statements; 

DROP TABLE IF EXISTS finance.carriers_reports_ys;

CREATE TABLE finance.carriers_reports_ys LIKE finance.carriers_reports; 

INSERT INTO finance.carriers_reports_ys SELECT * FROM finance.carriers_reports; 


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:04:00
