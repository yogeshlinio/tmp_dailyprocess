-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: procurement_live_mx
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'procurement_live_mx'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `visits_costs_channel`()
begin

delete from production.vw_visits_costs_channel;
delete from production.daily_costs_visits_per_channel;

insert into production.vw_visits_costs_channel (date, channel_group, sum_visits, sum_adcost) 
select v.*, if(c.adCost is null, 0, c.adCost) adCost from (
select date, channel_group, sum(if(adCost is null, 0, adCost)) adCost from production.ga_cost_campaign
group by date , channel_group) c right join (
select date, channel_group, sum(visits) visits from production.ga_visits_cost_source_medium
group by date , channel_group) v on c.channel_group = v.channel_group and c.date = v.date
order by c.date desc;

insert into production.daily_costs_visits_per_channel (date, cost_local, reporting_channel, visits) select date, sum_adcost, channel_group, sum_visits from production.vw_visits_costs_channel;

update production.daily_costs_visits_per_channel set month = month(date);

update production.daily_costs_visits_per_channel set reporting_channel = 'Non identified' where reporting_channel is null;

delete from production.media_rev_orders;

insert into production.media_rev_orders(date, channel_group, net_rev, gross_rev, net_orders, gross_orders, new_customers_gross, new_customers) select date, channel_group, sum(net_grand_total_after_vat), sum(gross_grand_total_after_vat), sum(net_orders), sum(gross_orders), sum(new_customers_gross), sum(new_customers) from production.tbl_order_detail group by date, channel_group;

update production.daily_costs_visits_per_channel a inner join production.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.gross_rev = b.gross_rev;

update production.daily_costs_visits_per_channel a inner join production.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.gross_orders = b.gross_orders;

update production.daily_costs_visits_per_channel a inner join production.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.new_customers_gross = b.new_customers_gross;

update production.daily_costs_visits_per_channel a set net_rev_e = (select (avg(net_rev/gross_rev)) from production.media_rev_orders b where b.date between date_sub(a.date, interval 21 day) and date_sub(a.date,interval 7 day) and a.reporting_channel=b.channel_group group by reporting_channel);

update production.daily_costs_visits_per_channel a set net_orders_e = (select (avg(net_orders/gross_orders)) from production.media_rev_orders b where b.date between date_sub(a.date, interval 21 day) and date_sub(a.date,interval 7 day) and a.reporting_channel=b.channel_group group by reporting_channel);

update production.daily_costs_visits_per_channel a set net_orders_e = (select (avg(new_customers/new_customers_gross)) from production.media_rev_orders b where b.date between date_sub(a.date, interval 21 day) and date_sub(a.date,interval 7 day) and a.reporting_channel=b.channel_group group by reporting_channel);

update production.daily_costs_visits_per_channel set net_rev_e = net_rev_e*gross_rev, net_orders_e = net_orders_e*gross_orders, new_customers_e = new_customers_e*new_customers_gross;

update production.daily_costs_visits_per_channel a inner join production.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.net_rev_e = b.net_zsrev, a.net_orders_e = b.net_orders where a.date < date_sub(curdate(), interval 14 day);

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:04:13
