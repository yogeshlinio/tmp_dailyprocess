-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: customer_service
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`eduardo.martinez`@`%`*/ /*!50003 TRIGGER trig_update_ev BEFORE UPDATE ON tbl_extension_ev
FOR EACH ROW
BEGIN

INSERT INTO tbl_incidencias(id_inc,id_op,Extension,Nombre,Fecha)
Values(null,old.id_op,OLD.Extension,OLD.Nombre,date(CURDATE()));


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`eduardo.martinez`@`%`*/ /*!50003 TRIGGER trig_update BEFORE UPDATE ON testExtension
FOR EACH ROW
BEGIN

INSERT INTO  testHistExtension(Fecha,Nombre,Extension)
Values(date(CURDATE()),OLD.Nombre,OLD.Extension);


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping routines for database 'customer_service'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`eduardo.martinez`@`%`*/ /*!50003 FUNCTION `backlog`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id_zendex) from bi_ops_tickets where date_creation<=fecha and (date_solved>fecha or date_solved=0)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`eduardo.martinez`@`%`*/ /*!50003 FUNCTION `backlog2`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from tbl_zendesk_co
where date(created_at)<=fecha and cliente=1 and
(date(`Solved at`)>fecha or `Solved at`=0)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`eduardo.martinez`@`%`*/ /*!50003 FUNCTION `backlog3`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from tbl_zendesk_co
where date(created_at)<=fecha and responsable ='BACK OFFICE' and
(date(`Solved at`)>fecha or `Solved at`=0)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`auto.reports`@`%`*/ /*!50003 FUNCTION `sp_payment_method_summary`() RETURNS int(11)
BEGIN
	



DELETE customer_service.payment_method_summary.*
FROM
	customer_service.payment_method_summary;


INSERT INTO customer_service.payment_method_summary (
	fk_sales_order,
	order_nr,
	grand_total,
	payment_method,
	created_at,
	installments
) SELECT
	bob_live_mx.sales_order.id_sales_order,
	bob_live_mx.sales_order.order_nr,
	bob_live_mx.sales_order.grand_total,
	bob_live_mx.sales_order.payment_method,
	bob_live_mx.sales_order.created_at,
	bob_live_mx.sales_order.installments
FROM
	bob_live_mx.sales_order;


UPDATE bob_live_mx.sales_order_item
INNER JOIN customer_service.payment_method_summary ON bob_live_mx.sales_order_item.fk_sales_order 
= customer_service.payment_method_summary.fk_sales_order
SET customer_service.payment_method_summary.OrderStatus = bob_live_mx.sales_order_item.fk_sales_order_item_status;


UPDATE customer_service.payment_method_summary
INNER JOIN customer_service.sales_order_classification ON customer_service.payment_method_summary.OrderStatus 
= customer_service.sales_order_classification.fk_sales_order_item_status
SET customer_service.payment_method_summary.isGrossOrder 
= customer_service.sales_order_classification.isGrossOrder, customer_service.payment_method_summary.isNetOrder 
= customer_service.sales_order_classification.isNetOrder,customer_service.payment_method_summary.isCanceled 
= customer_service.sales_order_classification.isCanceled,customer_service.payment_method_summary.isReturned 
= customer_service.sales_order_classification.isReturned,
customer_service.payment_method_summary.isPending 
= customer_service.sales_order_classification.isPending;


UPDATE customer_service.payment_method_summary
SET customer_service.payment_method_summary.DayOrderPlaced = date(customer_service.payment_method_summary.created_at)
;





UPDATE (
	customer_service.payment_method_summary INNER
	JOIN bob_live_mx.sales_order ON customer_service.payment_method_summary.fk_sales_order
= bob_live_mx.sales_order.id_sales_order
)
INNER JOIN bob_live_mx.sales_order_address ON bob_live_mx.sales_order.fk_sales_order_address_shipping 
= bob_live_mx.sales_order_address.id_sales_order_address
SET customer_service.payment_method_summary.ShippingRegion = bob_live_mx.sales_order_address.region;


UPDATE (
	customer_service.payment_method_summary INNER
	JOIN bob_live_mx.sales_order ON customer_service.payment_method_summary.fk_sales_order
  = bob_live_mx.sales_order.id_sales_order
)
INNER JOIN bob_live_mx.sales_order_address ON bob_live_mx.sales_order.fk_sales_order_address_billing
 = bob_live_mx.sales_order_address.id_sales_order_address
SET customer_service.payment_method_summary.BillingRegion = bob_live_mx.sales_order_address.region;


UPDATE customer_service.payment_method_summary 
SET customer_service.payment_method_summary.isInvalid = 1
WHERE
	(
		(
			(
				customer_service.payment_method_summary.isGrossOrder
			) = 0
		)
		AND (
			(
				customer_service.payment_method_summary.isNetOrder
			) = 0
		)
	);


UPDATE customer_service.payment_method_summary 
SET customer_service.payment_method_summary.GrossOrderTotal = customer_service.payment_method_summary.grand_total
WHERE
	(
		(
			(
				customer_service.payment_method_summary.isGrossOrder
			) = 1
		)
	);


UPDATE customer_service.payment_method_summary 
SET customer_service.payment_method_summary.NetOrderTotal = customer_service.payment_method_summary.grand_total
WHERE
	(
		(
			(
				customer_service.payment_method_summary.isNetOrder
			) = 1
		)
	);


UPDATE customer_service.payment_method_summary SET customer_service.payment_method_summary.InvalidOrderTotal 
= customer_service.payment_method_summary.grand_total
WHERE
	(
		(
			(
				customer_service.payment_method_summary.isInvalid
			) = 1
		)
	);





UPDATE customer_service.payment_method_groups
INNER JOIN customer_service.payment_method_summary ON customer_service.payment_method_groups.payment_method 
= customer_service.payment_method_summary.payment_method
SET customer_service.payment_method_summary.PaymentType = customer_service.payment_method_groups.PaymentType;


UPDATE customer_service.payment_method_summary INNER
JOIN customer_service.payment_method_groups ON customer_service.payment_method_summary.payment_method 
= customer_service.payment_method_groups.payment_method
SET customer_service.payment_method_summary.PaymentGroup = customer_service.payment_method_groups.PaymentGroup;


UPDATE customer_service.payment_method_summary SET customer_service.payment_method_summary.MonthOrderPlaced = MONTH (
	customer_service.payment_method_summary.created_at
);


UPDATE customer_service.payment_method_summary SET customer_service.payment_method_summary.YearOrderPlaced = YEAR (
	customer_service.payment_method_summary.created_at
);


UPDATE bob_live_mx.sales_order
INNER JOIN customer_service.payment_method_summary ON bob_live_mx.sales_order.id_sales_order 
= customer_service.payment_method_summary.fk_sales_order
SET customer_service.payment_method_summary.ChargedInstallment = 1
WHERE
	(
		(
			(
				bob_live_mx.sales_order.subtotal_without_interests - bob_live_mx.sales_order.grand_total
			) > 0
		)
	);


	RETURN 0;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `bi_ops_cc_backlog_bo`()
BEGIN

SELECT  'Inicio rutina bi_ops_cc_backlog_bo',now();

#Seleccionar fecha máximade la tabla
set @max_day=cast((select max(date) from backlog_bo_tickets_by_country) as date);

#Borrar datos último día
delete from backlog_bo_tickets_by_country where date=@max_day;

SELECT  'Agregar nuevas fechas bo',now();


#Insertar nuevas fechas del reporte por país
insert into backlog_bo_tickets_by_country (date)
select distinct(fecha) from customer_service.test_rep_agt_email_regional
where fecha>=@max_day;

update backlog_bo_tickets_by_country
set pais='MX'
where pais is null;
#
insert into backlog_bo_tickets_by_country (date)
select distinct(fecha) from customer_service.test_rep_agt_email_regional
where fecha>=@max_day;

update backlog_bo_tickets_by_country
set pais='CO'
where pais is null;

insert into backlog_bo_tickets_by_country (date)
select distinct(fecha) from customer_service.test_rep_agt_email_regional
where fecha>=@max_day;

update backlog_bo_tickets_by_country
set pais='PE'
where pais is null;

insert into backlog_bo_tickets_by_country (date)
select distinct(fecha) from customer_service.test_rep_agt_email_regional
where fecha>=@max_day;

update backlog_bo_tickets_by_country
set pais='VE'
where pais is null;

SELECT  'Actualizar datos de backlog bo',now();


#Actualizar datos de backlog por país
update backlog_bo_tickets_by_country
set
backlog=customer_service_co.backlog_bo_total_mx(date),
backlog_72h=customer_service_co.backlog_bo_total_mx_interval1(date),
`backlog<15d`=customer_service_co.backlog_bo_total_mx_interval2(date),
`backlog<1m`=customer_service_co.backlog_bo_total_mx_interval3(date),
`backlog>1m`=customer_service_co.backlog_bo_total_mx_interval4(date)
where pais='MX'
and date>=@max_day;

update backlog_bo_tickets_by_country
set
backlog=customer_service_co.backlog_bo_total_co(date),
backlog_72h=customer_service_co.backlog_bo_total_co_interval1(date),
`backlog<15d`=customer_service_co.backlog_bo_total_co_interval2(date),
`backlog<1m`=customer_service_co.backlog_bo_total_co_interval3(date),
`backlog>1m`=customer_service_co.backlog_bo_total_co_interval4(date)
where pais='CO'
and date>=@max_day;

update backlog_bo_tickets_by_country
set
backlog=customer_service_co.backlog_bo_total_pe(date),
backlog_72h=customer_service_co.backlog_bo_total_pe_interval1(date),
`backlog<15d`=customer_service_co.backlog_bo_total_pe_interval2(date),
`backlog<1m`=customer_service_co.backlog_bo_total_pe_interval3(date),
`backlog>1m`=customer_service_co.backlog_bo_total_pe_interval4(date)
where pais='PE'
and date>=@max_day;

update backlog_bo_tickets_by_country
set
backlog=customer_service_co.backlog_bo_total_ve(date),
backlog_72h=customer_service_co.backlog_bo_total_ve_interval1(date),
`backlog<15d`=customer_service_co.backlog_bo_total_ve_interval2(date),
`backlog<1m`=customer_service_co.backlog_bo_total_ve_interval3(date),
`backlog>1m`=customer_service_co.backlog_bo_total_ve_interval4(date)
where pais='VE'
and date>=@max_day;

SELECT  'Fin rutina bi_ops_cc_backlog_bo',now();


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `bi_ops_cc_backlog_inc_fo`()
BEGIN

SELECT  'Inicio rutina bi_ops_cc_backlog',now();


#Seleccionar fecha máximade la tabla
set @max_day=cast((select max(date) from backlog_incoming_tickets_by_country) as date);

#Borrar datos último día
delete from backlog_incoming_tickets_by_country where date=@max_day;

SELECT  'Agregar nuevas fechas',now();

#Insertar nuevas fechas del reporte por país
insert into backlog_incoming_tickets_by_country (date)
select distinct(fecha) from customer_service.test_rep_agt_email_regional
where fecha>=@max_day;

update backlog_incoming_tickets_by_country
set pais='MX'
where pais is null;

insert into backlog_incoming_tickets_by_country (date)
select distinct(fecha) from customer_service.test_rep_agt_email_regional
where fecha>=@max_day;

update backlog_incoming_tickets_by_country
set pais='CO'
where pais is null;

insert into backlog_incoming_tickets_by_country (date)
select distinct(fecha) from customer_service.test_rep_agt_email_regional
where fecha>=@max_day;

update backlog_incoming_tickets_by_country
set pais='PE'
where pais is null;

insert into backlog_incoming_tickets_by_country (date)
select distinct(fecha) from customer_service.test_rep_agt_email_regional
where fecha>=@max_day;

update backlog_incoming_tickets_by_country
set pais='VE'
where pais is null;

SELECT  'Actualizar datos de backlog',now();

#Actualizar datos de backlog por país
update backlog_incoming_tickets_by_country
set
backlog=customer_service_co.backlog_inc_total_mx(date),
backlog_72h=customer_service_co.backlog_inc_total_mx_interval1(date),
`backlog<15d`=customer_service_co.backlog_inc_total_mx_interval2(date),
`backlog<1m`=customer_service_co.backlog_inc_total_mx_interval3(date),
`backlog>1m`=customer_service_co.backlog_inc_total_mx_interval4(date)
where pais='MX'
and date>=@max_day;

update backlog_incoming_tickets_by_country
set
backlog=customer_service_co.backlog_inc_total_co(date),
backlog_72h=customer_service_co.backlog_inc_total_co_interval1(date),
`backlog<15d`=customer_service_co.backlog_inc_total_co_interval2(date),
`backlog<1m`=customer_service_co.backlog_inc_total_co_interval3(date),
`backlog>1m`=customer_service_co.backlog_inc_total_co_interval4(date)
where pais='CO'
and date>=@max_day;

update backlog_incoming_tickets_by_country
set
backlog=customer_service_co.backlog_inc_total_pe(date),
backlog_72h=customer_service_co.backlog_inc_total_pe_interval1(date),
`backlog<15d`=customer_service_co.backlog_inc_total_pe_interval2(date),
`backlog<1m`=customer_service_co.backlog_inc_total_pe_interval3(date),
`backlog>1m`=customer_service_co.backlog_inc_total_pe_interval4(date)
where pais='PE'
and date>=@max_day;

update backlog_incoming_tickets_by_country
set
backlog=customer_service_co.backlog_inc_total_ve(date),
backlog_72h=customer_service_co.backlog_inc_total_ve_interval1(date),
`backlog<15d`=customer_service_co.backlog_inc_total_ve_interval2(date),
`backlog<1m`=customer_service_co.backlog_inc_total_ve_interval3(date),
`backlog>1m`=customer_service_co.backlog_inc_total_ve_interval4(date)
where pais='VE'
and date>=@max_day;

SELECT  'Fin rutina bi_ops_cc_backlog',now();


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `bi_ops_tickets`()
BEGIN

#Borrar datos
truncate table customer_service.tbl_zendesk_general;

#Insertar datos de México
insert into customer_service.tbl_zendesk_general
(summation_column,
Id,
Requester,
Requester_id,
Requester_external_id,
Requester_email,
Requester_domain,
Submitter,
Assignee,
Group_,
Subject_,
Tags,
Status_,
Priority,
Via,
Ticket_type,
Created_at,
Updated_at,
Assigned_at,
Organization,
Due_date,
Initially_assigned_at,
Solved_at,
Resolution_Time,
Satisfaction_Score,
Group_stations,
Assignee_stations,
Reopens,
Replies,
First_reply_time_in_minutes,
First_reply_time_in_minutes_within_business_hours,
First_resolution_time_in_minutes,
First_resolution_time_in_minutes_within_business_hours,
Full_resolution_time_in_minutes,
Full_resolution_time_in_minutes_within_business_hours,
Agent_wait_time_in_minutes,
Agent_wait_time_in_minutes_within_business_hours,
Requester_wait_time_in_minutes,
Requester_wait_time_in_minutes_within_businesshours,
On_hold_time_in_minutes,
On_hold_time_in_minutes_within_business_hours,
Canal_de_contacto,
Genera_Venta,
Numero_del_pedido,
Telefono,
SKU_1,
SKU_2,
Clasificacion,
pais)
select * from
(select a.*,'MX' from customer_service.tbl_zendesk a) t;

#Insertar datos de Colombia
insert into customer_service.tbl_zendesk_general
(summation_column,
Id,
Requester,
Requester_id,
Requester_external_id,
Requester_email,
Requester_domain,
Submitter,
Assignee,
Group_,
Subject_,
Tags,
Status_,
Priority,
Via,
Ticket_type,
Created_at,
Updated_at,
Assigned_at,
Organization,
Due_date,
Initially_assigned_at,
Solved_at,
Resolution_Time,
Satisfaction_Score,
Group_stations,
Assignee_stations,
Reopens,
Replies,
First_reply_time_in_minutes,
First_reply_time_in_minutes_within_business_hours,
First_resolution_time_in_minutes,
First_resolution_time_in_minutes_within_business_hours,
Full_resolution_time_in_minutes,
Full_resolution_time_in_minutes_within_business_hours,
Agent_wait_time_in_minutes,
Agent_wait_time_in_minutes_within_business_hours,
Requester_wait_time_in_minutes,
Requester_wait_time_in_minutes_within_businesshours,
On_hold_time_in_minutes,
On_hold_time_in_minutes_within_business_hours,
Canal_de_contacto,
Clasificacion,
Numero_del_pedido,
Motivo_devolucion,
Retorno_del_producto,
Respuesta_quality,
Escalamiento_por_piezas_faltantes,
pais)
select * from
(select 
`Summation column`,
`Id`,
`Requester`,
`Requester id`,
`Requester external id`,
`Requester email`,
`Requester domain`,
`Submitter`,
`Assignee`,
`Group`,
`Subject`,
`Tags`,
`Status`,
`Priority`,
`Via`,
`Ticket type`,
`created_at`,
`Updated at`,
`Assigned at`,
`Organization`,
`Due date`,
`Initially assigned at`,
`Solved at`,
`Resolution time`,
`Satisfaction Score`,
`Group stations`,
`Assignee stations`,
`Reopens`,
`Replies`,
`First reply time in minutes`,
`First reply time in minutes within business hours`,
`First resolution time in minutes`,
`First resolution time in minutes within business hours`,
`Full resolution time in minutes`,
`Full resolution time in minutes within business hours`,
`Agent wait time in minutes`,
`Agent wait time in minutes within business hours`,
`Requester wait time in minutes`,
`Requester wait time in minutes within business hours`,
`On hold time in minutes`,
`On hold time in minutes within business hours`,
`Contacto Inicial [list]`,
`Tipo de solicitud [list]`,
`Order ID (BOB) [int]`,
`MOTIVO DE DEVOLUCIÓN O CANCELACION [list]`,
`RETORNO DEL PRODUCTO [list]`,
`RESPUESTA QUALITY [list]`,
`ESCALAMIENTO POR PIEZAS FALTANTES [list]`,
'CO' from customer_service.tbl_zendesk_co a) t;

#Insertar datos Perú

insert into customer_service.tbl_zendesk_general
(summation_column,
Id,
Requester,
Requester_id,
Requester_external_id,
Requester_email,
Requester_domain,
Submitter,
Subgrupo,
Group_,
Subject_,
Tags,
Status_,
Priority,
Via,
Ticket_type,
Created_at,
Updated_at,
Assigned_at,
Organization,
Due_date,
Initially_assigned_at,
Solved_at,
Resolution_Time,
Satisfaction_Score,
Group_stations,
Assignee_stations,
Reopens,
Replies,
First_reply_time_in_minutes,
First_reply_time_in_minutes_within_business_hours,
First_resolution_time_in_minutes,
First_resolution_time_in_minutes_within_business_hours,
Full_resolution_time_in_minutes,
Full_resolution_time_in_minutes_within_business_hours,
Agent_wait_time_in_minutes,
Agent_wait_time_in_minutes_within_business_hours,
Requester_wait_time_in_minutes,
Requester_wait_time_in_minutes_within_businesshours,
On_hold_time_in_minutes,
On_hold_time_in_minutes_within_business_hours,
Canal_de_contacto,
Assignee,
Clasificacion,
pais)
select * from
(select 
`Summation column`,
`Id`,
`Requester`,
`Requester id`,
`Requester external id`,
`Requester email`,
`Requester domain`,
`Submitter`,
`Assignee`,
`Group`,
`Subject`,
`Tags`,
`Status`,
`Priority`,
`Via`,
`Ticket type`,
`created_at`,
`Updated at`,
`Assigned at`,
`Organization`,
`Due date`,
`Initially assigned at`,
`Solved at`,
`Resolution time`,
`Satisfaction Score`,
`Group stations`,
`Assignee stations`,
`Reopens`,
`Replies`,
`First reply time in minutes`,
`First reply time in minutes within business hours`,
`First resolution time in minutes`,
`First resolution time in minutes within business hours`,
`Full resolution time in minutes`,
`Full resolution time in minutes within business hours`,
`Agent wait time in minutes`,
`Agent wait time in minutes within business hours`,
`Requester wait time in minutes`,
`Requester wait time in minutes within business hours`,
`On hold time in minutes`,
`On hold time in minutes within business hours`,
`Canal [list]`,
`Assignee [list]`,
`Tipo de solicitud [list]`
,
'PE' from customer_service.tbl_zendesk_pe a) t;

#Insertar datos Venezuela

insert into customer_service.tbl_zendesk_general
(summation_column,
Id,
Requester,
Requester_id,
Requester_external_id,
Requester_email,
Requester_domain,
Submitter,
Assignee,
Group_,
Subject_,
Tags,
Status_,
Priority,
Via,
Ticket_type,
Created_at,
Updated_at,
Assigned_at,
Organization,
Due_date,
Initially_assigned_at,
Solved_at,
Resolution_Time,
Satisfaction_Score,
Group_stations,
Assignee_stations,
Reopens,
Replies,
First_reply_time_in_minutes,
First_reply_time_in_minutes_within_business_hours,
First_resolution_time_in_minutes,
First_resolution_time_in_minutes_within_business_hours,
Full_resolution_time_in_minutes,
Full_resolution_time_in_minutes_within_business_hours,
Agent_wait_time_in_minutes,
Agent_wait_time_in_minutes_within_business_hours,
Requester_wait_time_in_minutes,
Requester_wait_time_in_minutes_within_businesshours,
On_hold_time_in_minutes,
On_hold_time_in_minutes_within_business_hours,
Clasificacion,
Canal_de_contacto,
Numero_del_pedido,
SKU_1,
SKU_2,
Genera_Venta,
pais)
select * from
(select 
`Summation column`,
`Id`,
`Requester`,
`Requester id`,
`Requester external id`,
`Requester email`,
`Requester domain`,
`Submitter`,
`Assignee`,
`Group`,
`Subject`,
`Tags`,
`Status`,
`Priority`,
`Via`,
`Ticket type`,
`created_at`,
`Updated at`,
`Assigned at`,
`Organization`,
`Due date`,
`Initially assigned at`,
`Solved at`,
`Resolution time`,
`Satisfaction Score`,
`Group stations`,
`Assignee stations`,
`Reopens`,
`Replies`,
`First reply time in minutes`,
`First reply time in minutes within business hours`,
`First resolution time in minutes`,
`First resolution time in minutes within business hours`,
`Full resolution time in minutes`,
`Full resolution time in minutes within business hours`,
`Agent wait time in minutes`,
`Agent wait time in minutes within business hours`,
`Requester wait time in minutes`,
`Requester wait time in minutes within business hours`,
`On hold time in minutes`,
`On hold time in minutes within business hours`,
`Tipo [list]`,
`Canal de contacto [list]`,
`Número de pedido [txt]`,
`SKU 1 [txt]`,
`SKU 2 [txt]`,
`Ventas [list]`
,
'VE' from customer_service.tbl_zendesk_ve a) t;

#-------------------------------------------------------------------
#Definir base de correos entrantes de Front Office para los 4 países

#Front Office México
update customer_service.tbl_zendesk_general
set es_inc_front_office=1
where Pais='MX'
and `Group_` IN ('Front Office Email','Front Office','Front Office - CHAT','','-')
and via in ('Mail','email');

#Front Office Colombia
update customer_service.tbl_zendesk_general
set es_inc_front_office=1
where Pais='CO'
and `Group_` IN ('Front Office','','-')
and via in ('Mail','email');

#Front Office Perú
update customer_service.tbl_zendesk_general
set es_inc_front_office=1
where Pais='PE'
and (`Assignee`='-' or 
`Assignee` IN (SELECT `tbl_staff_zendesk_pe`.`Assignee [list]` FROM 
tbl_staff_zendesk_pe WHERE 
`tbl_staff_zendesk_pe`.`Direcci�n` = 'Front Office'))
and via in ('Mail','email');

#Front Office Venezuela
update customer_service.tbl_zendesk_general
set es_inc_front_office=1
where Pais='VE'
and Via in ('Mail','email') and `Group_` IN ('Front Office', '-');

#Definir base de Back Office para los 4 países

#Back Office México
update customer_service.tbl_zendesk_general
set es_backoffice=1
where Pais='MX'
and (`Group_` like '%BO%'
or `Group_` like '%B.O.%')
and via in ('Mail','email');

#Back Office Colombia
update customer_service.tbl_zendesk_general
set es_backoffice=1
where Pais='CO'
and `Group_` not IN ('FRONT OFFICE','','-','LOGÍSTICA');

#Back Office Perú
update customer_service.tbl_zendesk_general
set es_backoffice=1
where Pais ='PE'
and es_inc_front_office=0;

#Back Office Venezuela
update customer_service.tbl_zendesk_general
set es_backoffice=1
where Pais='VE'
and `Group_` ='Back Office';

#################################################################
#Ajustar tiempo de primera respuesta

update tbl_zendesk_general
set
First_reply_time_in_minutes=First_resolution_time_in_minutes
where 
(First_reply_time_in_minutes is null
or First_reply_time_in_minutes="");


#FILTRO DE INGRESOS EN HORAS NO LABORALES

#México
#LUN-VIE
update customer_service.tbl_zendesk_general
set despues_de_horario=1
where dayofweek(date(Created_at)) in (2,3,4,5,6)
and (hour(Created_at)>=23 or 
hour(Created_at)<8)
and pais='MX';

#SÁBADO
update customer_service.tbl_zendesk_general
set despues_de_horario=1
where dayofweek(date(Created_at)) =7
and (hour(Created_at)>=19 or 
hour(Created_at)<9)
and pais='MX';

#DOMINGO
update customer_service.tbl_zendesk_general
set despues_de_horario=1
where dayofweek(date(Created_at)) =1
and (hour(Created_at)>=18 or 
hour(Created_at)<10)
and pais='MX';

#Colombia
#LUN-VIE
update customer_service.tbl_zendesk_general
set despues_de_horario=1
where dayofweek(date(Created_at)) in (2,3,4,5,6)
and (hour(Created_at)>=21 or 
hour(Created_at)<8)
and pais='CO';

#SÁBADO
update customer_service.tbl_zendesk_general
set despues_de_horario=1
where dayofweek(date(Created_at)) =7
and (hour(Created_at)>=21 or 
hour(Created_at)<9)
and pais='CO';

#DOMINGO
update customer_service.tbl_zendesk_general
set despues_de_horario=1
where dayofweek(date(Created_at)) =1
and (hour(Created_at)>=18 or 
hour(Created_at)<10)
and pais='CO';

#Perú
#LUN-VIE
update customer_service.tbl_zendesk_general
set despues_de_horario=1
where dayofweek(date(Created_at)) in (2,3,4,5,6)
and (hour(Created_at)>=23 or 
hour(Created_at)<8)
and pais='PE';

#SÁBADO
update customer_service.tbl_zendesk_general
set despues_de_horario=1
where dayofweek(date(Created_at)) =7
and (hour(Created_at)>=20 or 
hour(Created_at)<9)
and pais='PE';

#DOMINGO
update customer_service.tbl_zendesk_general
set despues_de_horario=1
where dayofweek(date(Created_at)) =1
and (hour(Created_at)>=20 or 
hour(Created_at)<9)
and pais='PE';

#Venezuela
#LUN-VIE
update customer_service.tbl_zendesk_general
set despues_de_horario=1
where dayofweek(date(Created_at)) in (2,3,4,5,6)
and (hour(Created_at)>=20 or 
hour(Created_at)<8)
and pais='VE';

#SÁBADO
update customer_service.tbl_zendesk_general
set despues_de_horario=1
where dayofweek(date(Created_at)) =7
and (hour(Created_at)>=17 or 
hour(Created_at)<9)
and pais='VE';

#DOMINGO
update customer_service.tbl_zendesk_general
set despues_de_horario=1
where dayofweek(date(Created_at)) =1
and pais='VE';

#Tiempos ajustados de primera respuesta sin tener en cuenta horarios no laborales

#México
update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date(created_at), interval 8 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='MX'
and dayofweek(date(Created_at)) in (2,3,4,5,6)
and hour(Created_at)<8;

update tbl_zendesk_general
set
created_at_ajustado= if(dayofweek(date(Created_at))=6,
DATE_ADD(date_add(date(created_at), interval 1 day), interval 9 hour),
DATE_ADD(date_add(date(created_at), interval 1 day), interval 8 hour)),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='MX'
and dayofweek(date(Created_at)) in (2,3,4,5,6)
and hour(Created_at)>=23;


update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date(created_at), interval 9 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='MX'
and dayofweek(date(Created_at))=7
and hour(Created_at)<9;

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date_add(date(created_at), interval 1 day), interval 10 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='MX'
and dayofweek(date(Created_at))=7
and hour(Created_at)>=19;

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date(created_at), interval 10 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='MX'
and dayofweek(date(Created_at))=1
and hour(Created_at)<10;

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date_add(date(created_at), interval 1 day), interval 8 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='MX'
and dayofweek(date(Created_at))=7
and hour(Created_at)>=18;

#Colombia

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date(created_at), interval 8 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='CO'
and dayofweek(date(Created_at)) in (2,3,4,5,6)
and hour(Created_at)<8;

update tbl_zendesk_general
set
created_at_ajustado= if(dayofweek(date(Created_at))=6,
DATE_ADD(date_add(date(created_at), interval 1 day), interval 9 hour),
DATE_ADD(date_add(date(created_at), interval 1 day), interval 8 hour)),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='CO'
and dayofweek(date(Created_at)) in (2,3,4,5,6)
and hour(Created_at)>=21;


update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date(created_at), interval 9 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='CO'
and dayofweek(date(Created_at))=7
and hour(Created_at)<9;

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date_add(date(created_at), interval 1 day), interval 10 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='CO'
and dayofweek(date(Created_at))=7
and hour(Created_at)>=21;

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date(created_at), interval 10 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='CO'
and dayofweek(date(Created_at))=1
and hour(Created_at)<10;

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date_add(date(created_at), interval 1 day), interval 8 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='CO'
and dayofweek(date(Created_at))=7
and hour(Created_at)>=18;

#Perú

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date(created_at), interval 8 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='PE'
and dayofweek(date(Created_at)) in (2,3,4,5,6)
and hour(Created_at)<8;

update tbl_zendesk_general
set
created_at_ajustado= if(dayofweek(date(Created_at))=6,
DATE_ADD(date_add(date(created_at), interval 1 day), interval 9 hour),
DATE_ADD(date_add(date(created_at), interval 1 day), interval 8 hour)),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='PE'
and dayofweek(date(Created_at)) in (2,3,4,5,6)
and hour(Created_at)>=23;


update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date(created_at), interval 9 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='PE'
and dayofweek(date(Created_at))=7
and hour(Created_at)<9;

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date_add(date(created_at), interval 1 day), interval 9 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='PE'
and dayofweek(date(Created_at))=7
and hour(Created_at)>=20;

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date(created_at), interval 9 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='PE'
and dayofweek(date(Created_at))=1
and hour(Created_at)<9;

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date_add(date(created_at), interval 1 day), interval 8 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='PE'
and dayofweek(date(Created_at))=7
and hour(Created_at)>=20;

#Venezuela

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date(created_at), interval 8 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='VE'
and dayofweek(date(Created_at)) in (2,3,4,5,6)
and hour(Created_at)<8;

update tbl_zendesk_general
set
created_at_ajustado= if(dayofweek(date(Created_at))=6,
DATE_ADD(date_add(date(created_at), interval 1 day), interval 9 hour),
DATE_ADD(date_add(date(created_at), interval 1 day), interval 8 hour)),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='VE'
and dayofweek(date(Created_at)) in (2,3,4,5,6)
and hour(Created_at)>=20;


update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date(created_at), interval 9 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='VE'
and dayofweek(date(Created_at))=7
and hour(Created_at)<9;

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date_add(date(created_at), interval 2 day), interval 8 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='VE'
and dayofweek(date(Created_at))=7
and hour(Created_at)>=17;

update tbl_zendesk_general
set
created_at_ajustado= DATE_ADD(date_add(date(created_at), interval 1 day), interval 8 hour),
date_first_reply_time_ajustado= DATE_ADD(created_at, interval First_reply_time_in_minutes minute)
where despues_de_horario=1
and pais='VE'
and dayofweek(date(Created_at))=1;

#Ingresar nuevos tiempos de primera respuesta

update tbl_zendesk_general
set
first_reply_time_ajustado_time=if(created_at_ajustado>date_first_reply_time_ajustado,0,
TIMEDIFF(date_first_reply_time_ajustado,created_at_ajustado))
where despues_de_horario=1;

update tbl_zendesk_general
set
first_reply_time_ajustado=time_to_sec(first_reply_time_ajustado_time)/60
where despues_de_horario=1;

#Completar tiempos de primera respuesta
update tbl_zendesk_general
set first_reply_time_ajustado=First_reply_time_in_minutes
where first_reply_time_ajustado is null;

#Filtros generales
update tbl_zendesk_general
set 
solved=if (`Solved_at` <> '0000-00-00 00:00:00',1,0),
Fecha_creacion=cast(`Created_at` AS date),
Intervalo_creacion=IF((date_format(`Created_at`,'%i') < 30),concat(date_format(`Created_at`,'%H'),'00'),
					concat(date_format(`Created_at`,'%H'),'30')),
Fecha_solucion=cast(`Solved_at` AS date),
Intervalo_solucion=IF((date_format(`Solved_at`,'%i') < 30),concat(date_format(`Solved_at`,'%H'),'00'),
					concat(date_format(`Solved_at`,'%H'),'30'));

#Otros filtros (solucion el mismo día, solución dentro de 4 horas)

update tbl_zendesk_general
set 
same_day=if(Fecha_creacion=Fecha_solucion,1,0),
4_horas_de_respuesta=if(first_reply_time_ajustado/60<4,1,0),
calificados=if(satisfaction_score in ('Bad','Good'),1,0),
calificados_bueno=if(satisfaction_score ='Good',1,0);

#Tiempos de solución back office Colombia
update customer_service.tbl_zendesk_general a 
inner join customer_service_co.zendesk_tipos_de_solicitud_tiempos b
on a.Clasificacion=b.tipo_solicitud
set
time_to_reply_bo_hours=tiempo_primera_respuesta,
time_to_solve_bo_hours=tiempo_solucion
where pais='CO';

#Solucionados a tiempo colombia
update customer_service.tbl_zendesk_general a 
set 
reply_on_time_bo=if(First_reply_time_in_minutes<=time_to_reply_bo_hours*60,1,0),
solved_on_time_bo=if(Full_resolution_time_in_minutes<=time_to_solve_bo_hours*60,1,0)
where pais='CO'
and time_to_reply_bo_hours is not null
and solved=1;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`eduardo.martinez`@`%`*/ /*!50003 PROCEDURE `DM_CHAT_hist_90_co`()
BEGIN
DROP TEMPORARY TABLE IF EXISTS tmp_amaster;
CREATE TEMPORARY TABLE tmp_amaster (key(date))
SELECT Date,SUM(if(OrderBeforeCan=1,1,0))Ordenes
FROM development_co_project.A_Master
WHERE Date>='2013-10-01'
GROUP BY date;

DROP TEMPORARY TABLE IF EXISTS tmp_chats;
CREATE TEMPORARY TABLE tmp_chats (key(chat_start_time))
SELECT date as chat_start_time,chats 
FROM 
customer_service_co.tbl_bi_ops_cc_chats_performance;

DROP TABLE IF EXISTS DM_CHAT_hist_90_co;
CREATE TABLE DM_CHAT_hist_90_co
SELECT date,Chats,ordenes
 FROM 
tmp_amaster a
LEFT JOIN
tmp_chats b
ON a.Date=b.chat_start_time
WHERE Date>=DATE_SUB(CURDATE(), INTERVAL 90 day) 
and Date <CURDATE()
GROUP BY date;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`eduardo.martinez`@`%`*/ /*!50003 PROCEDURE `DM_Chat_Hist_90_mx`()
BEGIN
DROP TEMPORARY TABLE IF EXISTS tmp_amaster;
CREATE TEMPORARY TABLE tmp_amaster (key(date))
SELECT Date,SUM(if(OrderBeforeCan=1,1,0))Ordenes
FROM development_mx.A_Master
WHERE Date>='2013-10-01'
GROUP BY date;

DROP TEMPORARY TABLE IF EXISTS tmp_chats;
CREATE TEMPORARY TABLE tmp_chats (key(chat_start_time))
SELECT Date(chat_start_time)chat_start_time,COUNT(*)Chats
FROM customer_service.tbl_olark_chat
WHERE chat_start_time>='2013-10-01'
AND visitor_nickname not like 'Mexico (%' 
AND visitor_nickname not like 'Mexico #%' AND visitor_nickname not like 'unknown #%'
AND visitor_nickname not like 'USA (%'
AND visitor_nickname not like 'USA #%'
AND visitor_nickname not like 'Spain (%'
AND visitor_nickname not like 'Colombia (%'
AND visitor_nickname not like 'Panama (%'
AND visitor_nickname not like 'Chile (%'
AND visitor_nickname not like 'Peru (%'
AND visitor_nickname not like 'Peru #%'
AND visitor_location like  'Mexico%'

GROUP BY Date(chat_start_time);

DROP TABLE IF EXISTS DM_CHAT_hist_90_mx;
CREATE TABLE DM_CHAT_hist_90_mx
SELECT date,Chats,ordenes
 FROM 
tmp_amaster a
LEFT JOIN
tmp_chats b
ON a.Date=b.chat_start_time
WHERE Date>=DATE_SUB(CURDATE(), INTERVAL 90 day) 
and Date <CURDATE()
GROUP BY date;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`eduardo.martinez`@`%`*/ /*!50003 PROCEDURE `DM_CS_hist_90_2weeks_co`()
BEGIN
DROP TEMPORARY TABLE IF EXISTS tmp_amaster;
CREATE TEMPORARY TABLE tmp_amaster (key(date))
SELECT DATE_ADD(Date, interval 14 DAY )Date,SUM(if(OrderBeforeCan=1,1,0))Ordenes
FROM development_co_project.A_Master
WHERE Date>='2013-10-01'
GROUP BY date;

DROP TEMPORARY TABLE IF EXISTS tmp_llamadas;
CREATE TEMPORARY TABLE tmp_llamadas (key(fecha))
SELECT event_date as Fecha,SUM(if(queue in ('ivr_cola2','ivr_cola3'),1,0))Llamadas
FROM customer_service_co.bi_ops_cc
WHERE event_date>='2013-10-01' AND net_event=1
GROUP BY Fecha
UNION ALL
SELECT event_date as Fecha,SUM(if(queue in ('Ventaspersonasnaturales'),1,0))Llamadas
FROM customer_service_co.bi_ops_cc_dyalogo
WHERE event_date>='2013-10-01' AND net_event=1
GROUP BY Fecha;

DROP TABLE IF EXISTS DM_CS_hist_90_2weeks_co;
CREATE TABLE DM_CS_hist_90_2weeks_co 
SELECT date,llamadas,ordenes
 FROM 
tmp_amaster a
LEFT JOIN
tmp_llamadas b
ON a.Date=b.Fecha 
WHERE Date>=DATE_SUB(CURDATE(), INTERVAL 90 day) 
and Date <CURDATE()
GROUP BY date;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`eduardo.martinez`@`%`*/ /*!50003 PROCEDURE `DM_CS_Hist_90_2weeks_mx`()
BEGIN

DROP TEMPORARY TABLE IF EXISTS tmp_amaster;
CREATE TEMPORARY TABLE tmp_amaster (key(date))
SELECT DATE_ADD(Date, interval 14 DAY )Date,SUM(if(OrderBeforeCan=1,1,0))Ordenes
FROM development_mx.A_Master
WHERE Date>='2013-10-01'
GROUP BY date;

DROP TEMPORARY TABLE IF EXISTS tmp_llamadas;
CREATE TEMPORARY TABLE tmp_llamadas (key(fecha))
SELECT Fecha,SUM(if(Cola='Post-Venta'=1,1,0))Llamadas
FROM customer_service.tbl_queueMetricsAuto
WHERE Fecha>='2013-10-01'
GROUP BY Fecha;

DROP TABLE IF EXISTS DM_CS_hist_90_2weeks_mx;
CREATE TABLE DM_CS_hist_90_2weeks_mx 
SELECT date,llamadas,ordenes
 FROM 
tmp_amaster a
LEFT JOIN
tmp_llamadas b
ON a.Date=b.Fecha 
WHERE Date>=DATE_SUB(CURDATE(), INTERVAL 90 day) 
and Date <CURDATE()
GROUP BY date;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`eduardo.martinez`@`%`*/ /*!50003 PROCEDURE `DM_CS_TeleSales_hist_90`()
BEGIN

DROP TEMPORARY TABLE IF EXISTS tmp_amaster;
CREATE TEMPORARY TABLE tmp_amaster (key(date))
SELECT Date,SUM(if(OrderBeforeCan=1,1,0))Ordenes
FROM development_mx.A_Master
WHERE Date>='2013-10-01'
GROUP BY date;

DROP TEMPORARY TABLE IF EXISTS tmp_llamadas;
CREATE TEMPORARY TABLE tmp_llamadas (key(fecha))
SELECT Fecha,SUM(if(Cola='Pre-Venta'=1,1,0))Llamadas
FROM customer_service.tbl_queueMetricsAuto
WHERE Fecha>='2013-10-01'
GROUP BY Fecha;

DROP TABLE IF EXISTS DM_CS_TS_hist_90;
CREATE TABLE DM_CS_TS_hist_90 
SELECT date,llamadas,ordenes
 FROM 
tmp_amaster a
LEFT JOIN
tmp_llamadas b
ON a.Date=b.Fecha 
WHERE Date>=DATE_SUB(CURDATE(), INTERVAL 90 day) 
and Date <CURDATE()
GROUP BY date;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`eduardo.martinez`@`%`*/ /*!50003 PROCEDURE `DM_CS_TS_hist_90_CO`()
BEGIN
DROP TEMPORARY TABLE IF EXISTS tmp_amaster;
CREATE TEMPORARY TABLE tmp_amaster (key(date))
SELECT Date,COUNT(DISTINCT OrderNum)Ordenes
FROM development_co_project.A_Master
WHERE Date>='2013-10-01' AND OrderBeforeCan=1
GROUP BY date;

DROP TEMPORARY TABLE IF EXISTS tmp_llamadas;
CREATE TEMPORARY TABLE tmp_llamadas (key(fecha))
SELECT event_date as Fecha,SUM(if(queue='ivr_cola1',1,0))Llamadas
FROM customer_service_co.bi_ops_cc
WHERE event_date>='2013-10-01' AND net_event=1
GROUP BY Fecha
UNION ALL
SELECT event_date as Fecha,SUM(if(queue in ('Estadodepedido','Cancelaciones','Reclamos'),1,0))Llamadas
FROM customer_service_co.bi_ops_cc_dyalogo
WHERE event_date>='2013-10-01' AND net_event=1
GROUP BY Fecha
;

DROP TABLE IF EXISTS DM_CS_TS_hist_90_CO;
CREATE TABLE DM_CS_TS_hist_90_CO 
SELECT date,llamadas,ordenes
 FROM 
tmp_amaster a
LEFT JOIN
tmp_llamadas b
ON a.Date=b.Fecha 
WHERE Date>=DATE_SUB(CURDATE(), INTERVAL 90 day) 
and Date <CURDATE()
GROUP BY date;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`eduardo.martinez`@`%`*/ /*!50003 PROCEDURE `sp_agt_email_regional_ajustado`()
BEGIN

#Llamar rutina para unificacion de datos
call bi_ops_tickets;

SET @max_day = (SELECT max(Fecha) FROM customer_service.test_rep_agt_email_regional);

DELETE FROM customer_service.test_rep_agt_email_regional WHERE Fecha>=@max_day;

#Insertar intervalos Mexico
INSERT INTO test_rep_agt_email_regional 
(Fecha, Id, Franja, Pais, Assignee)

SELECT a.Fecha, a.Id, a.Franja, a.Pais, b.Assignee FROM
(SELECT *,'MX' as Pais FROM customer_service.tbl_base_48) a

LEFT JOIN

(SELECT DISTINCT Fecha,Assignee,Intervalo
FROM
(
SELECT DISTINCT	Fecha_creacion as Fecha, Assignee,Pais,Intervalo_creacion Intervalo
FROM
	tbl_zendesk_general
WHERE
	es_inc_front_office=1
	and pais='MX'
UNION ALL
SELECT DISTINCT	
	Fecha_solucion as Fecha1,
	Assignee,
	Pais,
	Intervalo_solucion
FROM
	`tbl_zendesk_general`
WHERE
	es_inc_front_office=1
	and pais='MX'
)a)b
on a.Fecha=b.Fecha AND a.Id=cast(b.Intervalo as CHAR)
WHERE a.Fecha>=@max_day AND a.Fecha<DATE(CURDATE());

#Insertar intervalos Colombia
INSERT INTO test_rep_agt_email_regional 
(Fecha, Id, Franja, Pais, Assignee)

SELECT a.Fecha, a.Id, a.Franja, a.Pais, b.Assignee FROM
(SELECT *,'CO' as Pais FROM customer_service.tbl_base_48) a

LEFT JOIN

(SELECT DISTINCT Fecha,Assignee,Intervalo
FROM
(
SELECT DISTINCT	Fecha_creacion as Fecha, Assignee,Pais,Intervalo_creacion Intervalo
FROM
	tbl_zendesk_general
WHERE
	es_inc_front_office=1
	and pais='CO'
UNION ALL
SELECT DISTINCT	
	Fecha_solucion as Fecha1,
	Assignee,
	Pais,
	Intervalo_solucion
FROM
	`tbl_zendesk_general`
WHERE
	es_inc_front_office=1
	and pais='CO'
)a)b
on a.Fecha=b.Fecha AND a.Id=cast(b.Intervalo as CHAR)
WHERE a.Fecha>=@max_day AND a.Fecha<DATE(CURDATE());

#Insertar intervalos Peru
INSERT INTO test_rep_agt_email_regional 
(Fecha, Id, Franja, Pais, Assignee)

SELECT a.Fecha, a.Id, a.Franja, a.Pais, b.Assignee FROM
(SELECT *,'PE' as Pais FROM customer_service.tbl_base_48) a

LEFT JOIN

(SELECT DISTINCT Fecha,Assignee,Intervalo
FROM
(
SELECT DISTINCT	Fecha_creacion as Fecha, Assignee,Pais,Intervalo_creacion Intervalo
FROM
	tbl_zendesk_general
WHERE
	es_inc_front_office=1
	and pais='PE'
UNION ALL
SELECT DISTINCT	
	Fecha_solucion as Fecha1,
	Assignee,
	Pais,
	Intervalo_solucion
FROM
	`tbl_zendesk_general`
WHERE
	es_inc_front_office=1
	and pais='PE'
)a)b
on a.Fecha=b.Fecha AND a.Id=cast(b.Intervalo as CHAR)
WHERE a.Fecha>=@max_day AND a.Fecha<DATE(CURDATE());

#Insertar intervalos Venezuela
INSERT INTO test_rep_agt_email_regional 
(Fecha, Id, Franja, Pais, Assignee)

SELECT a.Fecha, a.Id, a.Franja, a.Pais, b.Assignee FROM
(SELECT *,'VE' as Pais FROM customer_service.tbl_base_48) a

LEFT JOIN

(SELECT DISTINCT Fecha,Assignee,Intervalo
FROM
(
SELECT DISTINCT	Fecha_creacion as Fecha, Assignee,Pais,Intervalo_creacion Intervalo
FROM
	tbl_zendesk_general
WHERE
	es_inc_front_office=1
	and pais='VE'
UNION ALL
SELECT DISTINCT	
	Fecha_solucion as Fecha1,
	Assignee,
	Pais,
	Intervalo_solucion
FROM
	`tbl_zendesk_general`
WHERE
	es_inc_front_office=1
	and pais='VE'
)a)b
on a.Fecha=b.Fecha AND a.Id=cast(b.Intervalo as CHAR)
WHERE a.Fecha>=@max_day AND a.Fecha<DATE(CURDATE());

#Datos en base a la fecha de creación
update test_rep_agt_email_regional a
inner join 
(select Pais, Fecha_creacion, Intervalo_creacion, Assignee, sum(summation_column) New_tickets,
sum(solved) solved_tickets,
sum(Full_resolution_time_in_minutes/60) Tiempo_Resolucion_Creacion,
sum(first_reply_time_ajustado/60) Tiempo_primera_respuesta_creacion,
sum(same_day) resuelto_mismo_dia
from tbl_zendesk_general
where es_inc_front_office=1
Group by Pais, Fecha_creacion, Intervalo_creacion, Assignee) b
on a.Fecha=b.Fecha_creacion
and a.Id=b.Intervalo_creacion
and a.Pais=b.Pais
and a.Assignee=b.Assignee
set
a.New_Tickets=b.New_tickets,
a.solved_tickets=b.solved_tickets,
a.Tiempo_Resolucion_Creacion=b.Tiempo_Resolucion_Creacion,
a.Tiempo_primera_respuesta_creacion=b.Tiempo_primera_respuesta_creacion,
a.resuelto_mismo_dia=b.resuelto_mismo_dia
WHERE a.Fecha>=@max_day AND a.Fecha<DATE(CURDATE());

#Datos en base a la fecha de solucion
update test_rep_agt_email_regional a
inner join 
(select Pais, Fecha_solucion, Intervalo_solucion, Assignee, 
sum(solved) solved_ticketsT,
sum(Full_resolution_time_in_minutes/60) Tiempo_Resolucion,
sum(first_reply_time_ajustado/60) Tiempo_primera_respuesta,
sum(calificados) Tickets_calificados,
sum(calificados_bueno) Tickets_calificados_bueno,
sum(4_horas_de_respuesta) Resuelto_en_4_hrs,
sum(First_resolution_time_in_minutes/60) Tiempo_Primera_Solucion,
avg(Reopens)*sum(solved) Reopens
from tbl_zendesk_general
where solved=1
and es_inc_front_office=1
Group by Pais, Fecha_solucion, Intervalo_solucion, Assignee) b
on a.Fecha=b.Fecha_solucion
and a.Id=b.Intervalo_solucion
and a.Pais=b.Pais
and a.Assignee=b.Assignee
set
a.solved_ticketsT=b.solved_ticketsT,
a.Tiempo_Resolucion=b.Tiempo_Resolucion,
a.Tiempo_primera_respuesta=b.Tiempo_primera_respuesta,
a.Tickets_calificados=b.Tickets_calificados,
a.Tickets_calificados_bueno=b.Tickets_calificados_bueno,
a.Resuelto_en_4_hrs=b.Resuelto_en_4_hrs,
a.Tiempo_Primera_Solucion=b.Tiempo_Primera_Solucion,
a.Reopens=b.Reopens
where Fecha>=@max_day AND Fecha<DATE(CURDATE());

#############BACKLOG#############################

#Llama la rutina que llena el backlog de correos entrantes
call bi_ops_cc_backlog_inc_fo;

#Actualiza los datos de backlog general
update test_rep_agt_email_regional a inner join 
backlog_incoming_tickets_by_country b
on a.Fecha=b.date
and a.Pais=b.pais
set 
Backlog_by_day=backlog,
backlog_3d=backlog_72h,
backlog_15d=`backlog<15d`,
backlog_30d=`backlog<1m`,
backlog_mayor_30d=`backlog>1m`;

#Actualizar numero de semana
UPDATE  test_rep_agt_email_regional SET Semana=date_format(Fecha,'%Y-%u');

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `sp_backoffRegional`()
BEGIN

truncate rep_bo_regional;
INSERT INTO rep_bo_regional
SELECT BDS.Fecha,Franja,Pais,New_Tickets,solved_tickets,TiempoResolucion,Tiempo_primera_respuesta
,Resuelto_mismo_dia,NULL as Semana ,solved_ticketsT
FROM 

customer_service.tbl_base_48 BDS

LEFT JOIN

(#BOMX
SELECT * FROM VW_BO_Regional

UNION ALL

#bACKoFFCO
SELECT * FROM VW_BO_Regional_CO

UNION ALL

SELECT * FROM VW_BO_Regional_VE

UNION ALL

SELECT * FROM VW_BO_Regional_PE

)BO
ON BDS.fECHA=BO.Fecha AND BDS.Id=CAST(BO.Intervalo as char)
where BDS.Fecha<date(CURDATE()) and BDS.Fecha>='2013-09-01';

UPDATE  rep_bo_regional SET Semana=date_format(fecha,'%Y-%u');


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `sp_backoffRegionalAgt`()
BEGIN

truncate rep_bo_agt_regional;
INSERT INTO rep_bo_agt_regional (Fecha, Franja, Assignee, Pais, New_Tickets, solved_tickets,TiempoResolucion,
Tiempo_primera_respuesta, Resuelto_mismo_dia, Semana, solved_ticketsT, Tickets_calificados, Tickets_calificados_bueno)
SELECT BDS.Fecha,Franja,Assignee,Pais,New_Tickets,solved_tickets,TiempoResolucion,Tiempo_primera_respuesta,
Resuelto_mismo_dia,NULL as Semana,solved_ticketsT, Tickets_calificados,Tickets_calificados_como_bueno FROM 

customer_service.tbl_base_48 BDS
left JOIN

(#BOMX
SELECT * FROM VW_BO_Agente

UNION ALL
SELECT * FROM VW_BO_Agente_CO

UNION ALL

SELECT * FROM VW_BO_Agente_VE
UNION ALL
SELECT * FROM VW_BO_Agente_PE

)BOAgt

ON BDS.fECHA=BOAgt.Fecha AND BDS.Id=CAST(BOAgt.Intervalo as char)
where BDS.Fecha<date(CURDATE()) and BDS.Fecha>='2013-09-01';

#Llama la rutina que llena el backlog de correos entrantes
call bi_ops_cc_backlog_bo;

#Datos de backlog en la tabla general
update rep_bo_agt_regional a inner join 
backlog_bo_tickets_by_country b
on a.Fecha=b.date
and a.Pais=b.pais
set 
Backlog_by_day=backlog,
backlog_3d=backlog_72h,
backlog_15d=`backlog<15d`,
backlog_30d=`backlog<1m`,
backlog_mayor_30d=`backlog>1m`;

UPDATE rep_bo_agt_regional  SET Semana=date_format(fecha,'%Y-%u');

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `sp_boCOadjust`()
BEGIN

update tbl_zendesk_co
set cliente=1
where via='Mail';


update tbl_zendesk_co a inner join customer_service_co.bi_ops_tickets_solicitudes b on a.`Tipo de solicitud [list]`=b.solicitud
set 
a.responsable=b.responsable;

update tbl_zendesk_co 
set responsable='COORDINADORES'
where assignee='Admin';


update tbl_zendesk_co 
set
responsable='FRONT OFFICE' where responsable ='-';

update tbl_zendesk_co 
set
responsable='FRONT OFFICE' where responsable is null;

update tbl_zendesk_co
set 
solved=if(status in ('Closed','Solved'),1,0),
pending=if(status='Pending',1,0)
;

set @max_day=cast((select max(date) from bi_ops_tickets_backlog) as date);

delete from bi_ops_tickets_backlog where date=@max_day;

insert into bi_ops_tickets_backlog (date,new_tickets) 
select * from
(select date(created_at)created_at ,count(*) 
FROM tbl_zendesk_co 
group by date(created_at) ) a
where a.created_at>=@max_day;

update bi_ops_tickets_backlog  
set 
#backlog = backlog(date),
backlog_inc=backlog2(date),
backlog_bo=backlog3(date)
where date>=@max_day;

update tbl_zendesk_co
set first_reply_time_h=if(`First reply time in minutes`="" or `first reply time in minutes` is null,
if(`First resolution time in minutes`="" or `First resolution time in minutes` is null,"",`First resolution time in minutes`/60),
`first reply time in minutes`/60);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`eduardo.martinez`@`%`*/ /*!50003 PROCEDURE `sp_chatRegional`()
BEGIN
#Reporte Chats Mx
truncate rep_chats_regional;
insert into rep_chats_regional

select Fecha,Franja,Recibido,Pais,TEspera,Abandono,TDuracion,ChatAtn20,null as Semana
FROM

customer_service.tbl_base_48 BDS
left join 

(select a.inicio,a.Intervalo,Recibido,'MX' as Pais,TEspera,Abandono,TDuracion,ChatAtn20
from
(

select substring(CAST((chat_start_time) AS CHAR),1,11) as inicio,
IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
) as Intervalo,cast(DATE_FORMAT(chat_start_time,'%H') as char)hr,
	count(chat_start_time) as Recibido,sum(operator_first_response_delay) as TEspera,
	sum(if(operator_first_response_delay ='None' AND visitor_messages_sent>0,1,0))
 as Abandono,sum(chat_duration) as TDuracion
	from tbl_olark_chat
	
	group by substring(CAST(chat_start_time AS CHAR),1,11),
IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
)
having hr in (08,09,10,11,12,13,14,15,16,17,18,19,20,21,22))a

left join

(select substring(CAST((chat_start_time) AS CHAR),1,11) as inicio,IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
) as Intervalo
	,cast(DATE_FORMAT(chat_start_time,'%H') as char)hr,
	count(chat_start_time) as ChatAtn20
	from tbl_olark_chat
	where operator_first_response_delay<30
	group by substring(CAST(chat_start_time AS CHAR),1,11),IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
) ,DATE_FORMAT(chat_start_time,'%H')
having hr in (08,09,10,11,12,13,14,15,16,17,18,19,20,21,22)
)b
on a.inicio=b.inicio and a.Intervalo=b.Intervalo
union ALL
#Chat Peru
select a.inicio,a.Intervalo,Recibido,'PE' as Pais,TEspera,Abandono,TDuracion,ChatAtn20
from
(

select substring(CAST((chat_start_time) AS CHAR),1,11) as inicio,
IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
) as Intervalo,cast(DATE_FORMAT(chat_start_time,'%H') as char)hr,
	count(chat_start_time) as Recibido,sum(operator_first_response_delay) as TEspera,
	sum(if(operator_first_response_delay ='None' AND visitor_messages_sent>0,1,0))
 as Abandono,sum(chat_duration) as TDuracion
	from tbl_olark_chat_pe
	
	group by substring(CAST(chat_start_time AS CHAR),1,11),
IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
)
having hr in (08,09,10,11,12,13,14,15,16,17,18,19,20,21,22))a

left join

(select substring(CAST((chat_start_time) AS CHAR),1,11) as inicio,IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
) as Intervalo
	,cast(DATE_FORMAT(chat_start_time,'%H') as char)hr,
	count(chat_start_time) as ChatAtn20
	from tbl_olark_chat_pe
	where operator_first_response_delay<30
	group by substring(CAST(chat_start_time AS CHAR),1,11),IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
) ,DATE_FORMAT(chat_start_time,'%H')
having hr in (08,09,10,11,12,13,14,15,16,17,18,19,20,21,22)
)b
on a.inicio=b.inicio and a.Intervalo=b.Intervalo

union ALL
#Chats Ve
select a.inicio,a.Intervalo,Recibido,'VE' as Pais,TEspera,Abandono,TDuracion,ChatAtn20
from
(

select substring(CAST((chat_start_time) AS CHAR),1,11) as inicio,
IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
) as Intervalo,cast(DATE_FORMAT(chat_start_time,'%H') as char)hr,
	count(chat_start_time) as Recibido,sum(operator_first_response_delay) as TEspera,
	sum(if(operator_first_response_delay ='None' AND visitor_messages_sent>0,1,0))
 as Abandono,sum(chat_duration) as TDuracion
	from tbl_olark_chat_ve
	
	group by substring(CAST(chat_start_time AS CHAR),1,11),
IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
)
having hr in (08,09,10,11,12,13,14,15,16,17,18,19,20,21,22))a

left join

(select substring(CAST((chat_start_time) AS CHAR),1,11) as inicio,IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
) as Intervalo
	,cast(DATE_FORMAT(chat_start_time,'%H') as char)hr,
	COUNT(*)-sum(if(operator_first_response_delay ='None' AND visitor_messages_sent>0,1,0)) as ChatAtn20
	from tbl_olark_chat_ve
	where operator_first_response_delay<20
	group by substring(CAST(chat_start_time AS CHAR),1,11),IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
) ,DATE_FORMAT(chat_start_time,'%H')
having hr in (08,09,10,11,12,13,14,15,16,17,18,19,20,21,22)
)b
on a.inicio=b.inicio and a.Intervalo=b.Intervalo
)Chats

on BDS.Fecha=Chats.inicio and BDS.id=cast(Chats.intervalo as char)
where BDS.Fecha<date(CURDATE()) and BDS.Fecha>='2013-09-01'

UNION ALL

SELECT 
			date as Fecha,
			NULL as Franja,
			queued as Recibido,
			'CO' as Pais,
			waiting_time_sec*queued as TEspera,
			queued-chats as Abandono,
			act_sec*chats as TDuracion,
			null as ChatAtn20,
			null as Semana

 FROM customer_service_co.tbl_bi_ops_cc_chats_performance
WHERE date >= '2013-01-01' 

;

UPDATE  rep_chats_regional SET Semana=date_format(fecha,'%Y-%u');

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`eduardo.martinez`@`%`*/ /*!50003 PROCEDURE `sp_chatRegionalAgt`()
BEGIN

truncate rep_chats_agt_regional;
insert into rep_chats_agt_regional
#Reporte Chats Mx x Agt
select Fecha,Franja,operator_nickname,Pais,Recibido,TEspera,Abandono,TDuracion,ChatAtn20,NULL as Semana
 from 

customer_service.tbl_base_48 BDS
left join 


(select a.inicio,a.Intervalo,a.operator_nickname,'MX' as Pais,Recibido,TEspera,Abandono,TDuracion,ChatAtn20
from
(

select substring(CAST((chat_start_time) AS CHAR),1,11) as inicio,operator_nickname,
IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
) as Intervalo,cast(DATE_FORMAT(chat_start_time,'%H') as char)hr,
	count(chat_start_time) as Recibido,sum(operator_first_response_delay) as TEspera,
	sum(if(operator_first_response_delay ='None' AND visitor_messages_sent>0,1,0))
 as Abandono,sum(chat_duration) as TDuracion
	from tbl_olark_chat
	
	group by substring(CAST(chat_start_time AS CHAR),1,11),operator_nickname,
IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
)
having hr in (08,09,10,11,12,13,14,15,16,17,18,19,20,21,22))a

left join

(select substring(CAST((chat_start_time) AS CHAR),1,11) as inicio,operator_nickname,
IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
) as Intervalo
	,cast(DATE_FORMAT(chat_start_time,'%H') as char)hr,
	count(chat_start_time) as ChatAtn20
	from tbl_olark_chat
	where operator_first_response_delay<30
	group by substring(CAST(chat_start_time AS CHAR),1,11),operator_nickname,IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
) ,DATE_FORMAT(chat_start_time,'%H')
having hr in (08,09,10,11,12,13,14,15,16,17,18,19,20,21,22)
)b
on a.inicio=b.inicio and a.Intervalo=b.Intervalo and a.operator_nickname=b.operator_nickname
union ALL

#Chats Peru x Agente
select a.inicio,a.Intervalo,a.operator_nickname,'PE'as Pais,Recibido,TEspera,Abandono,TDuracion,ChatAtn20
from
(

select substring(CAST((chat_start_time) AS CHAR),1,11) as inicio,operator_nickname,
IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
) as Intervalo,cast(DATE_FORMAT(chat_start_time,'%H') as char)hr,
	count(chat_start_time) as Recibido,sum(operator_first_response_delay) as TEspera,
	sum(if(operator_first_response_delay ='None' AND visitor_messages_sent>0,1,0))
 as Abandono,sum(chat_duration) as TDuracion
	from tbl_olark_chat_pe
	
	group by substring(CAST(chat_start_time AS CHAR),1,11),operator_nickname,
IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
)
having hr in (08,09,10,11,12,13,14,15,16,17,18,19,20,21,22))a

left join

(select substring(CAST((chat_start_time) AS CHAR),1,11) as inicio,operator_nickname,
IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
) as Intervalo
	,cast(DATE_FORMAT(chat_start_time,'%H') as char)hr,
	count(chat_start_time) as ChatAtn20
	from tbl_olark_chat_pe
	where operator_first_response_delay<30
	group by substring(CAST(chat_start_time AS CHAR),1,11),operator_nickname,IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
) ,DATE_FORMAT(chat_start_time,'%H')
having hr in (08,09,10,11,12,13,14,15,16,17,18,19,20,21,22)
)b
on a.inicio=b.inicio and a.Intervalo=b.Intervalo and a.operator_nickname=b.operator_nickname
union ALL

#Venezuela Chats X agente
select a.inicio,a.Intervalo,a.operator_nickname,'VE' as Pais,Recibido,TEspera,Abandono,TDuracion,ChatAtn20
from
(

select substring(CAST((chat_start_time) AS CHAR),1,11) as inicio,operator_nickname,
IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
) as Intervalo,cast(DATE_FORMAT(chat_start_time,'%H') as char)hr,
	count(chat_start_time) as Recibido,sum(operator_first_response_delay) as TEspera,
	sum(if(operator_first_response_delay ='None' AND visitor_messages_sent>0,1,0))
 as Abandono,sum(chat_duration) as TDuracion
	from tbl_olark_chat_ve
	
	group by substring(CAST(chat_start_time AS CHAR),1,11),operator_nickname,
IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
)
having hr in (08,09,10,11,12,13,14,15,16,17,18,19,20,21,22))a

left join

(select substring(CAST((chat_start_time) AS CHAR),1,11) as inicio,operator_nickname,
IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
) as Intervalo
	,cast(DATE_FORMAT(chat_start_time,'%H') as char)hr,
	COUNT(*)-sum(if(operator_first_response_delay ='None' AND visitor_messages_sent>0,1,0)) as ChatAtn20
	from tbl_olark_chat_ve
	where operator_first_response_delay<30
	group by substring(CAST(chat_start_time AS CHAR),1,11),operator_nickname,IF (
	 DATE_FORMAT(chat_start_time,'%i')< 30,
	concat(DATE_FORMAT(chat_start_time,'%H'), '00'),
	concat(DATE_FORMAT(chat_start_time,'%H'), '30')
) ,DATE_FORMAT(chat_start_time,'%H')
having hr in (08,09,10,11,12,13,14,15,16,17,18,19,20,21,22)
)b
on a.inicio=b.inicio and a.Intervalo=b.Intervalo and a.operator_nickname=b.operator_nickname)ChatAgt


on BDS.Fecha=ChatAgt.inicio and BDS.id=cast(ChatAgt.intervalo as char)
where BDS.Fecha<date(CURDATE()) and BDS.Fecha>='2013-09-01'


UNION ALL
SELECT 
			date as Fecha,
			NULL as Franja,
			operator_name as operator_nickname,
			'CO' as Pais,
			chats as Recibido,
			NULL as TEspera,
			NULL as Abandono,
			total_chatting_sec as TDuracion,
			null as ChatAtn20,
			null as Semana

 FROM customer_service_co.tbl_bi_ops_cc_chats_performance_agents
WHERE date >= '2013-01-01'
;

UPDATE  rep_chats_agt_regional SET Semana=date_format(fecha,'%Y-%u');

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`eduardo.martinez`@`%`*/ /*!50003 PROCEDURE `sp_emailRegional`()
BEGIN

TRUNCATE rep_email_regional;
INSERT INTO rep_email_regional
SELECT BDS.Fecha,Franja,Pais,New_Tickets,solved_tickets,
TiempoResolucion,Tiempo_primera_respuesta,Resuelto_mismo_dia,NULL as 
Semana,solved_ticketsT
 FROM 

customer_service.tbl_base_48 BDS

LEFT JOIN
(
SELECT * FROM VW_EMAIL_Reg

UNION ALL

SELECT * FROM VW_EMAIL_Reg_CO

UNION ALL

SELECT * FROM VW_EMAIL_Reg_VE

UNION ALL

SELECT * FROM VW_EMAIL_Reg_PE



)email
ON BDS.fECHA=email.Fecha AND BDS.Id=CAST(email.Intervalo as char)
where BDS.Fecha<date(CURDATE()) and BDS.Fecha>='2013-09-01';

UPDATE  rep_email_regional SET Semana=date_format(fecha,'%Y-%u');

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `sp_emailRegionalAgt`()
BEGIN

TRUNCATE rep_email_agt_regional;
INSERT INTO rep_email_agt_regional 
(Fecha, Franja, Assignee, Pais, New_Tickets, solved_tickets,TiempoResolucion,
Tiempo_primera_respuesta, Resuelto_mismo_dia, Semana, solved_ticketsT,Tickets_calificados,
Tickets_calificados_bueno)
SELECT BDS.Fecha,Franja,Assignee,Pais,New_Tickets,solved_tickets,TiempoResolucion,Tiempo_primera_respuesta,
Resuelto_mismo_dia,NULL as Semana,solved_ticketsT, Tickets_calificados,Tickets_calificados_como_bueno  FROM 

customer_service.tbl_base_48 BDS
left JOIN


(
#EmailsMX
SELECT * FROM VW_EMAIL_Agt
UNION ALL
SELECT * FROM VW_EMAIL_Agt_CO
UNION ALL
SELECT * FROM VW_EMAIL_Agt_VE
UNION ALL
SELECT * FROM VW_EMAIL_Agt_PE
)emailAgt

ON BDS.fECHA=emailAgt.Fecha AND BDS.Id=CAST(emailAgt.Intervalo as char)
where BDS.Fecha<date(CURDATE()) and BDS.Fecha>='2013-09-01';

UPDATE rep_email_agt_regional  SET Semana=date_format(fecha,'%Y-%u');

#Llama la rutina que llena el backlog de correos entrantes
call bi_ops_cc_backlog_inc_fo;

#Datos de backlog en la tabla general
update rep_email_agt_regional a inner join 
backlog_incoming_tickets_by_country b
on a.Fecha=b.date
and a.Pais=b.pais
set 
Backlog_by_day=backlog,
backlog_3d=backlog_72h,
backlog_15d=`backlog<15d`,
backlog_30d=`backlog<1m`,
backlog_mayor_30d=`backlog>1m`;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`eduardo.martinez`@`%`*/ /*!50003 PROCEDURE `sp_fraud_Check`()
BEGIN

#1 B 001 D 1tblOut-fraud_check_report  MC  OK
TRUNCATE
TABLE
	customer_service.tbl_fraud_check_report;

#2 B 101 A +tbl-sales_order  MC  ok
INSERT INTO customer_service.tbl_fraud_check_report (
	id_sales_order,
	OrderNumber,
	DateOrderPlaced,
	PaymentMethod,
	GrandTotal
) SELECT
	bob_live_mx.sales_order.id_sales_order,
	bob_live_mx.sales_order.order_nr,
	bob_live_mx.sales_order.created_at,
	bob_live_mx.sales_order.payment_method,
	bob_live_mx.sales_order.grand_total
FROM
	bob_live_mx.sales_order
WHERE bob_live_mx.sales_order.payment_method = "Banorte_Payworks_Debit"
OR bob_live_mx.sales_order.payment_method = "Banorte_Payworks"
OR bob_live_mx.sales_order.payment_method = "Amex_Gateway"
OR bob_live_mx.sales_order.payment_method = "Adquira_Interredes"
OR bob_live_mx.sales_order.payment_method = "Adyen_HostedPaymentPage"
OR bob_live_mx.sales_order.payment_method = "DineroMail_HostedPaymentPage"
OR bob_live_mx.sales_order.payment_method = "DineroMail_Api"
;

#3 B 201 U DayOrderPlaced   MC ok
UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.DayOrderPlaced = DATE_FORMAT(
	customer_service.tbl_fraud_check_report.DateOrderPlaced,
	'%Y-%m-%d'
);

#4 B 202 U WeekOrderPlaced    EC ok
UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.WeekOrderPlaced = production.week_iso (
	customer_service.tbl_fraud_check_report.DayOrderPlaced
);

/*
#5 B 203 U AccertifyScore  MC  unable
UPDATE customer_service.tbl_fraud_check_report
INNER JOIN  bob_live_mx.sales_order_accertify_log
ON customer_service.tbl_fraud_check_report.id_sales_order
= bob_live_mx.sales_order_accertify_log.fk_sales_order
SET customer_service.tbl_fraud_check_report.AccertifyScore = ExtractValue (
		message,
		'/transaction-results/total-score')
WHERE
title = 'accertify response';

#AccertifyRecommendation
UPDATE customer_service.tbl_fraud_check_report
INNER JOIN  bob_live_mx.sales_order_accertify_log
ON customer_service.tbl_fraud_check_report.id_sales_order
= bob_live_mx.sales_order_accertify_log.fk_sales_order
SET customer_service.tbl_fraud_check_report.AccertifyRecommendation = ExtractValue (
		message,
		'/transaction-results/recommendation-code')
WHERE
title = 'accertify response';*/

#############################################################################################
#S0 B 209 U newerror  MC OK
UPDATE (
	customer_service.tbl_fraud_check_report
	INNER JOIN bob_live_mx.sales_order_item ON customer_service.tbl_fraud_check_report.id_sales_order = bob_live_mx.sales_order_item.fk_sales_order
)
INNER JOIN bob_live_mx.sales_order_item_status_history ON bob_live_mx.sales_order_item.id_sales_order_item = bob_live_mx.sales_order_item_status_history.fk_sales_order_item
SET customer_service.tbl_fraud_check_report.PaymentConfirmationPending = 1
WHERE bob_live_mx.sales_order_item_status_history.fk_sales_order_item_status = 23;

#S0 B 209 U clarify_payment_pending_timeout  MC OK


UPDATE (
	customer_service.tbl_fraud_check_report
	INNER JOIN bob_live_mx.sales_order_item ON customer_service.tbl_fraud_check_report.id_sales_order = bob_live_mx.sales_order_item.fk_sales_order
)
INNER JOIN bob_live_mx.sales_order_item_status_history ON bob_live_mx.sales_order_item.id_sales_order_item = bob_live_mx.sales_order_item_status_history.fk_sales_order_item
SET customer_service.tbl_fraud_check_report.clarify_payment_pending_timeout = 1
WHERE bob_live_mx.sales_order_item_status_history.fk_sales_order_item_status = 39;


#S1 B 209 U Invalid  MC OK
UPDATE (
	customer_service.tbl_fraud_check_report
	INNER JOIN bob_live_mx.sales_order_item ON customer_service.tbl_fraud_check_report.id_sales_order = bob_live_mx.sales_order_item.fk_sales_order
)
INNER JOIN bob_live_mx.sales_order_item_status_history ON bob_live_mx.sales_order_item.id_sales_order_item = bob_live_mx.sales_order_item_status_history.fk_sales_order_item
SET customer_service.tbl_fraud_check_report.Invalid = 1
WHERE bob_live_mx.sales_order_item_status_history.fk_sales_order_item_status = 10;


#S2 B 214 U AutoFraudCheckPending  MC ok
UPDATE (
	customer_service.tbl_fraud_check_report
	INNER JOIN bob_live_mx.sales_order_item ON customer_service.tbl_fraud_check_report.id_sales_order = bob_live_mx.sales_order_item.fk_sales_order
)
INNER JOIN bob_live_mx.sales_order_item_status_history ON bob_live_mx.sales_order_item.id_sales_order_item = bob_live_mx.sales_order_item_status_history.fk_sales_order_item
SET customer_service.tbl_fraud_check_report.AutoFraudCheckPending = 1
WHERE bob_live_mx.sales_order_item_status_history.fk_sales_order_item_status = 88;


#S3 B 214 U ClarifyAuto Fraud Check Error  MC ok
UPDATE (
	customer_service.tbl_fraud_check_report
	INNER JOIN bob_live_mx.sales_order_item ON customer_service.tbl_fraud_check_report.id_sales_order = bob_live_mx.sales_order_item.fk_sales_order
)
INNER JOIN bob_live_mx.sales_order_item_status_history ON bob_live_mx.sales_order_item.id_sales_order_item = bob_live_mx.sales_order_item_status_history.fk_sales_order_item
SET customer_service.tbl_fraud_check_report.ClarifyAutoFraudCheckError = 1
WHERE bob_live_mx.sales_order_item_status_history.fk_sales_order_item_status = 89;


#S4 B 206 U PaymentPending  MC OK
UPDATE (
	customer_service.tbl_fraud_check_report
	INNER JOIN bob_live_mx.sales_order_item ON customer_service.tbl_fraud_check_report.id_sales_order = bob_live_mx.sales_order_item.fk_sales_order
)
INNER JOIN bob_live_mx.sales_order_item_status_history ON bob_live_mx.sales_order_item.id_sales_order_item = bob_live_mx.sales_order_item_status_history.fk_sales_order_item
SET customer_service.tbl_fraud_check_report.PaymentPending = 1
WHERE bob_live_mx.sales_order_item_status_history.fk_sales_order_item_status = 2;


#S5 B 210 U ClarifyPaymentError  MC ok

UPDATE (
	customer_service.tbl_fraud_check_report
	INNER JOIN bob_live_mx.sales_order_item ON customer_service.tbl_fraud_check_report.id_sales_order = bob_live_mx.sales_order_item.fk_sales_order
)
INNER JOIN bob_live_mx.sales_order_item_status_history ON bob_live_mx.sales_order_item.id_sales_order_item = bob_live_mx.sales_order_item_status_history.fk_sales_order_item
SET customer_service.tbl_fraud_check_report.ClarifyPaymentError = 1
WHERE bob_live_mx.sales_order_item_status_history.fk_sales_order_item_status= 15
;


#S6 B 215 U ManualFraudCheckPending   MC  ok

UPDATE (
	customer_service.tbl_fraud_check_report
	INNER JOIN bob_live_mx.sales_order_item ON customer_service.tbl_fraud_check_report.id_sales_order = bob_live_mx.sales_order_item.fk_sales_order
)
INNER JOIN bob_live_mx.sales_order_item_status_history ON bob_live_mx.sales_order_item.id_sales_order_item = bob_live_mx.sales_order_item_status_history.fk_sales_order_item
SET customer_service.tbl_fraud_check_report.ManualFraudCheckPending = 1
WHERE bob_live_mx.sales_order_item_status_history.fk_sales_order_item_status = 90
;


#s7 B 212 U ClarifyManualFraudCheck  MC  ok
UPDATE (
	customer_service.tbl_fraud_check_report
	INNER JOIN bob_live_mx.sales_order_item ON customer_service.tbl_fraud_check_report.id_sales_order = bob_live_mx.sales_order_item.fk_sales_order
)
INNER JOIN bob_live_mx.sales_order_item_status_history ON bob_live_mx.sales_order_item.id_sales_order_item = bob_live_mx.sales_order_item_status_history.fk_sales_order_item
SET customer_service.tbl_fraud_check_report.ClarifyManualFraudCheck = 1
WHERE bob_live_mx.sales_order_item_status_history.fk_sales_order_item_status = 91
;


#s8 B 212 U RefundNeeded  MC  ok
UPDATE (
	customer_service.tbl_fraud_check_report
	INNER JOIN bob_live_mx.sales_order_item ON customer_service.tbl_fraud_check_report.id_sales_order = bob_live_mx.sales_order_item.fk_sales_order
)
INNER JOIN bob_live_mx.sales_order_item_status_history ON bob_live_mx.sales_order_item.id_sales_order_item = bob_live_mx.sales_order_item_status_history.fk_sales_order_item
SET customer_service.tbl_fraud_check_report.RefundNeeded = 1
WHERE bob_live_mx.sales_order_item_status_history.fk_sales_order_item_status = 87
;



#S9 B 207 U Exportable  MC  OK
UPDATE (
	customer_service.tbl_fraud_check_report
	INNER JOIN bob_live_mx.sales_order_item ON customer_service.tbl_fraud_check_report.id_sales_order = bob_live_mx.sales_order_item.fk_sales_order
)
INNER JOIN bob_live_mx.sales_order_item_status_history ON bob_live_mx.sales_order_item.id_sales_order_item = bob_live_mx.sales_order_item_status_history.fk_sales_order_item
SET customer_service.tbl_fraud_check_report.Exportable = 1
WHERE bob_live_mx.sales_order_item_status_history.fk_sales_order_item_status = 3;


##################################################################################################

#8 B 208 U Canceled  MC  ok
UPDATE (
	customer_service.tbl_fraud_check_report
	INNER JOIN bob_live_mx.sales_order_item ON customer_service.tbl_fraud_check_report.id_sales_order = bob_live_mx.sales_order_item.fk_sales_order
)
INNER JOIN bob_live_mx.sales_order_item_status_history ON bob_live_mx.sales_order_item.id_sales_order_item = bob_live_mx.sales_order_item_status_history.fk_sales_order_item
SET customer_service.tbl_fraud_check_report.Canceled = 1
WHERE bob_live_mx.sales_order_item_status_history.fk_sales_order_item_status = 9
;		


#11 B 211 U ManualRefundNeeded  MC  ok
UPDATE (
	customer_service.tbl_fraud_check_report
	INNER JOIN bob_live_mx.sales_order_item ON customer_service.tbl_fraud_check_report.id_sales_order = bob_live_mx.sales_order_item.fk_sales_order
)
INNER JOIN bob_live_mx.sales_order_item_status_history ON bob_live_mx.sales_order_item.id_sales_order_item = bob_live_mx.sales_order_item_status_history.fk_sales_order_item
SET customer_service.tbl_fraud_check_report.ManualRefundNeeded = 1
WHERE bob_live_mx.sales_order_item_status_history.fk_sales_order_item_status = 53
;


#13 B 213 U FraudCheckPending MC  ok
UPDATE (
	customer_service.tbl_fraud_check_report
	INNER JOIN bob_live_mx.sales_order_item ON customer_service.tbl_fraud_check_report.id_sales_order = bob_live_mx.sales_order_item.fk_sales_order
)
INNER JOIN bob_live_mx.sales_order_item_status_history ON bob_live_mx.sales_order_item.id_sales_order_item = bob_live_mx.sales_order_item_status_history.fk_sales_order_item
SET customer_service.tbl_fraud_check_report.FraudCheckPending = 1
WHERE bob_live_mx.sales_order_item_status_history.fk_sales_order_item_status = 55
;


#16 B 217 U isScored  MC  ok o rows
UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.isScored = 1
WHERE customer_service.tbl_fraud_check_report.AccertifyScore IS NOT NULL
;

#17 B 218 U isAmexReviewed MC  ok
DROP TABLE IF EXISTS tmp_payment_sales_amex;

CREATE TEMPORARY TABLE tmp_payment_sales_amex

(fk_sales_order FLOAT , KEY(fk_sales_order));

INSERT INTO tmp_payment_sales_amex

SELECT fk_sales_order FROM bob_live_mx.payment_sales_amex;

UPDATE customer_service.tbl_fraud_check_report
INNER JOIN tmp_payment_sales_amex
ON customer_service.tbl_fraud_check_report.id_sales_order = 
tmp_payment_sales_amex.fk_sales_order
SET customer_service.tbl_fraud_check_report.isAmexReviewed = 1;

#18 B 219 U isBanorteReviewed  MC
DROP TABLE IF EXISTS tmp_payment_sales_banortepayworks;

CREATE TEMPORARY TABLE tmp_payment_sales_banortepayworks

(fk_sales_order FLOAT , KEY(fk_sales_order));

INSERT INTO tmp_payment_sales_banortepayworks

SELECT fk_sales_order FROM bob_live_mx.payment_sales_banortepayworks;



UPDATE customer_service.tbl_fraud_check_report
INNER JOIN tmp_payment_sales_banortepayworks ON customer_service.tbl_fraud_check_report.id_sales_order
 = tmp_payment_sales_banortepayworks.fk_sales_order
SET customer_service.tbl_fraud_check_report.isBanorteReviewed = 1;

# 19 B 220 U isDineromailReviewed  MC
UPDATE customer_service.tbl_fraud_check_report
INNER JOIN bob_live_mx.payment_sales_dineromail ON customer_service.tbl_fraud_check_report.id_sales_order = bob_live_mx.payment_sales_dineromail.fk_sales_order
SET customer_service.tbl_fraud_check_report.isDineromailReviewed = 1;

#20 B 221 U isBankReviewed  MC
UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.isBankReviewed = 1
WHERE customer_service.tbl_fraud_check_report.isAmexReviewed = 1
OR customer_service.tbl_fraud_check_report.isBanorteReviewed = 1
OR customer_service.tbl_fraud_check_report.isDineromailReviewed = 1
;
###############################################################################
###############################################################################
###############################################################################
#T0 B 222 U RejectNewInvalid MC   ok 0  <<
UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.NewError = 0;
UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.NewError = 1
WHERE 
customer_service.tbl_fraud_check_report.PaymentPending=0
AND customer_service.tbl_fraud_check_report.Exportable=0
AND customer_service.tbl_fraud_check_report.Canceled=0
AND customer_service.tbl_fraud_check_report.Invalid=0
AND customer_service.tbl_fraud_check_report.ClarifyPaymentError=0
AND customer_service.tbl_fraud_check_report.ManualRefundNeeded=0
AND customer_service.tbl_fraud_check_report.FraudCheckPending=0
AND customer_service.tbl_fraud_check_report.RefundNeeded=0
AND customer_service.tbl_fraud_check_report.PaymentConfirmationPending=0;




#T1 B 222 U RejectNewInvalid MC   ok 0  <<
UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.RejectNewInvalid = 1
WHERE customer_service.tbl_fraud_check_report.Invalid = 1
AND customer_service.tbl_fraud_check_report.isScored =0
AND customer_service.tbl_fraud_check_report.isBankReviewed=0;


#T2 PendingScore  <<
UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.PendingScore = 1
WHERE 
		customer_service.tbl_fraud_check_report.IsScored = 1
AND customer_service.tbl_fraud_check_report.Invalid = 0 
AND customer_service.tbl_fraud_check_report.PaymentPending=0
AND customer_service.tbl_fraud_check_report.RejectNewInvalid = 0
AND customer_service.tbl_fraud_check_report.isBankReviewed=0
;


#T3 B 223 U RejectAuto MC  <<
UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.RejectAuto = 1
WHERE
customer_service.tbl_fraud_check_report.isScored = 1 
AND customer_service.tbl_fraud_check_report.Invalid = 1 
AND customer_service.tbl_fraud_check_report.isBankReviewed=0;

#T4 B 225 U PendingBank MC  0<<
UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.PendingBank = 0;

UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.PendingBank = 1
WHERE 
(customer_service.tbl_fraud_check_report.PaymentPending=1 
AND customer_service.tbl_fraud_check_report.Invalid=0
AND customer_service.tbl_fraud_check_report.ManualFraudCheckPending=0
AND customer_service.tbl_fraud_check_report.Exportable=0
AND customer_service.tbl_fraud_check_report.Canceled=0)

OR(customer_service.tbl_fraud_check_report.PaymentConfirmationPending=1
AND customer_service.tbl_fraud_check_report.Exportable=0
AND customer_service.tbl_fraud_check_report.Canceled=0)
;



#T9 B 224 U RejectBank MC
UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.RejectBank = 0;

UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.RejectBank = 1
WHERE 
customer_service.tbl_fraud_check_report.RejectNewInvalid = 0
AND customer_service.tbl_fraud_check_report.RejectAuto = 0
AND customer_service.tbl_fraud_check_report.PendingBank = 0
AND (customer_service.tbl_fraud_check_report.ManualFraudCheckPending = 0
OR customer_service.tbl_fraud_check_report.FraudCheckPending = 0)
AND (customer_service.tbl_fraud_check_report.Canceled = 1
OR customer_service.tbl_fraud_check_report.Invalid = 1)
AND customer_service.tbl_fraud_check_report.Exportable=0 
;

#T5 B 226 U ApproveAuto  MC
UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.ApproveAuto = 0;

UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.ApproveAuto = 1
WHERE 
customer_service.tbl_fraud_check_report.Exportable=1
AND customer_service.tbl_fraud_check_report.ManualFraudCheckPending= 0
AND customer_service.tbl_fraud_check_report.RejectNewInvalid = 0
AND customer_service.tbl_fraud_check_report.RejectBank = 0
AND customer_service.tbl_fraud_check_report.FraudCheckPending=0
;


#T6 B 229 U PendingManual  MC

UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.PendingManual = 0;


UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.PendingManual = 1
WHERE 
		customer_service.tbl_fraud_check_report.NewError = 0
AND customer_service.tbl_fraud_check_report.RejectNewInvalid = 0
AND customer_service.tbl_fraud_check_report.RejectAuto=0 
AND	customer_service.tbl_fraud_check_report.RejectBank=0
AND customer_service.tbl_fraud_check_report.PendingBank=0
AND customer_service.tbl_fraud_check_report.ApproveAuto=0
AND customer_service.tbl_fraud_check_report.Exportable=0
AND customer_service.tbl_fraud_check_report.FraudCheckPending=1
AND customer_service.tbl_fraud_check_report.ManualRefundNeeded=0
AND customer_service.tbl_fraud_check_report.RefundNeeded=0
OR(customer_service.tbl_fraud_check_report.NewError = 0
AND customer_service.tbl_fraud_check_report.RejectNewInvalid = 0
AND customer_service.tbl_fraud_check_report.RejectAuto=0 
AND	customer_service.tbl_fraud_check_report.RejectBank=0
AND customer_service.tbl_fraud_check_report.PendingBank=0
AND customer_service.tbl_fraud_check_report.ApproveAuto=0
AND customer_service.tbl_fraud_check_report.Exportable=0
AND customer_service.tbl_fraud_check_report.ManualFraudCheckPending=1
AND customer_service.tbl_fraud_check_report.RefundNeeded=0)
;

#T7 B 228 U RejectManual  MC
UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.RejectManual = 0;


UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.RejectManual = 1
WHERE 
    customer_service.tbl_fraud_check_report.NewError = 0
AND customer_service.tbl_fraud_check_report.RejectNewInvalid = 0
AND customer_service.tbl_fraud_check_report.RejectAuto=0 
AND	customer_service.tbl_fraud_check_report.RejectBank=0
AND customer_service.tbl_fraud_check_report.PendingBank=0
AND customer_service.tbl_fraud_check_report.ApproveAuto=0
AND customer_service.tbl_fraud_check_report.Exportable=0
AND customer_service.tbl_fraud_check_report.PendingManual=0
AND (customer_service.tbl_fraud_check_report.ManualRefundNeeded=1
OR customer_service.tbl_fraud_check_report.RefundNeeded=1)
;

#T8 B 227 U ApproveManual  MC
UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.ApproveManual = 0;

UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.ApproveManual = 1
WHERE 
		customer_service.tbl_fraud_check_report.NewError = 0
AND customer_service.tbl_fraud_check_report.RejectNewInvalid = 0
AND customer_service.tbl_fraud_check_report.RejectAuto=0 
AND	customer_service.tbl_fraud_check_report.RejectBank=0
AND customer_service.tbl_fraud_check_report.PendingBank=0
AND customer_service.tbl_fraud_check_report.ApproveAuto=0
AND customer_service.tbl_fraud_check_report.PendingManual=0
AND customer_service.tbl_fraud_check_report.RejectManual=0
OR (customer_service.tbl_fraud_check_report.Exportable=1
AND customer_service.tbl_fraud_check_report.FraudCheckPending=1)
;


#29 B 230 U Analyst  EC
UPDATE (
	(
		customer_service.tbl_fraud_check_report
		INNER JOIN bob_live_mx.sales_order_item 
		ON customer_service.tbl_fraud_check_report.id_sales_order = bob_live_mx.sales_order_item.fk_sales_order
	)
	INNER JOIN bob_live_mx.sales_order_item_status_history 
ON bob_live_mx.sales_order_item.id_sales_order_item = bob_live_mx.sales_order_item_status_history.fk_sales_order_item
)
INNER JOIN 	bob_live_mx.acl_user ON bob_live_mx.sales_order_item_status_history.fk_acl_user = 	bob_live_mx.acl_user.id_acl_user
SET customer_service.tbl_fraud_check_report.Analyst = If (
	bob_live_mx.acl_user.username = "ignacio.diaz",
	"Ignacio Diaz",
If (
		bob_live_mx.acl_user.username = "eva.gerardo",
		"Eva Gerardo",
	If (
		bob_live_mx.acl_user.username = "osiel.espinoza",
		"Osiel Espinoza",
		If (
		bob_live_mx.acl_user.username = "grimkarl11",
		"Carlos Manrique",
		bob_live_mx.acl_user.username
	 )
	)
 )
)
WHERE customer_service.tbl_fraud_check_report.RejectManual = 1
AND bob_live_mx.sales_order_item_status_history.fk_sales_order_item_status = 53
OR bob_live_mx.sales_order_item_status_history.fk_sales_order_item_status = 87
OR bob_live_mx.sales_order_item_status_history.fk_sales_order_item_status = 3
AND customer_service.tbl_fraud_check_report.ApproveManual = 1
;

#30 B 231 U DateApproveAuto MC
UPDATE 
(
	customer_service.tbl_fraud_check_report
	INNER JOIN bob_live_mx.sales_order_item ON customer_service.tbl_fraud_check_report.id_sales_order = bob_live_mx.sales_order_item.fk_sales_order
)
INNER JOIN bob_live_mx.sales_order_item_status_history ON bob_live_mx.sales_order_item.id_sales_order_item = bob_live_mx.sales_order_item_status_history.fk_sales_order_item
SET customer_service.tbl_fraud_check_report.DateApproveAuto = bob_live_mx.sales_order_item_status_history.updated_at
WHERE 
bob_live_mx.sales_order_item_status_history.fk_sales_order_item_status = 3
AND (customer_service.tbl_fraud_check_report.ApproveAuto = 1
AND customer_service.tbl_fraud_check_report.ApproveManual = 0)
;

#31 B 232 U DateApproveManual MC
UPDATE (
	customer_service.tbl_fraud_check_report
	INNER JOIN bob_live_mx.sales_order_item ON customer_service.tbl_fraud_check_report.id_sales_order = bob_live_mx.sales_order_item.fk_sales_order
)
INNER JOIN bob_live_mx.sales_order_item_status_history ON bob_live_mx.sales_order_item.id_sales_order_item = bob_live_mx.sales_order_item_status_history.fk_sales_order_item
SET customer_service.tbl_fraud_check_report.DateApproveManual 
= bob_live_mx.sales_order_item_status_history.updated_at
WHERE 
bob_live_mx.sales_order_item_status_history.fk_sales_order_item_status = 3
AND (customer_service.tbl_fraud_check_report.ApproveAuto = 0
AND customer_service.tbl_fraud_check_report.ApproveManual = 1)
;

#32 B 233 U DateRejectAuto MC  >>
UPDATE (
	customer_service.tbl_fraud_check_report
	INNER JOIN bob_live_mx.sales_order_item ON customer_service.tbl_fraud_check_report.id_sales_order = bob_live_mx.sales_order_item.fk_sales_order
)
INNER JOIN bob_live_mx.sales_order_item_status_history ON bob_live_mx.sales_order_item.id_sales_order_item = bob_live_mx.sales_order_item_status_history.fk_sales_order_item
SET customer_service.tbl_fraud_check_report.DateRejectAuto = bob_live_mx.sales_order_item_status_history.updated_at
WHERE customer_service.tbl_fraud_check_report.RejectAuto = 1
AND bob_live_mx.sales_order_item_status_history.fk_sales_order_item_status = 10
;

#33 B 234 U DateRejectManual MC
UPDATE (
	customer_service.tbl_fraud_check_report
	INNER JOIN bob_live_mx.sales_order_item ON customer_service.tbl_fraud_check_report.id_sales_order = bob_live_mx.sales_order_item.fk_sales_order
)
INNER JOIN bob_live_mx.sales_order_item_status_history ON bob_live_mx.sales_order_item.id_sales_order_item = bob_live_mx.sales_order_item_status_history.fk_sales_order_item
SET customer_service.tbl_fraud_check_report.DateRejectManual = bob_live_mx.sales_order_item_status_history.updated_at
WHERE customer_service.tbl_fraud_check_report.RejectManual = 1
AND (bob_live_mx.sales_order_item_status_history.fk_sales_order_item_status = 53
OR bob_live_mx.sales_order_item_status_history.fk_sales_order_item_status = 87)
AND customer_service.tbl_fraud_check_report.ApproveAuto = 0
AND customer_service.tbl_fraud_check_report.ApproveManual = 0
;
#34 B 235 U TimeToApproveManual MC
UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.TimeToApproveManual =
	timediff(customer_service.tbl_fraud_check_report.DateApproveManual
,customer_service.tbl_fraud_check_report.DateOrderPlaced
)
WHERE customer_service.tbl_fraud_check_report.DateApproveManual IS NOT NULL
;
####################################################################
#35 B 236 U TimeApproveTotal  MC  ¡¡'¡¿¿?
UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.TimeToApproveTotal = 
CASE 1 
WHEN customer_service.tbl_fraud_check_report.DateApproveManual IS NOT NULL 
THEN TIMEDIFF(customer_service.tbl_fraud_check_report.DateApproveManual
,customer_service.tbl_fraud_check_report.DateOrderPlaced)
WHEN customer_service.tbl_fraud_check_report.DateApproveAuto IS NOT NULL
THEN TIMEDIFF(customer_service.tbl_fraud_check_report.DateApproveAuto
,customer_service.tbl_fraud_check_report.DateOrderPlaced)
ELSE 
NULL 
END;
/*(
	DATEDIFF(
	IFNULL (
			customer_service.tbl_fraud_check_report.DateApproveAuto, 0
		) + IFNULL (
			customer_service.tbl_fraud_check_report.DateApproveManual, 0
		), customer_service.tbl_fraud_check_report.DateOrderPlaced
	)
) *24
WHERE
	(
		(
			(
				customer_service.tbl_fraud_check_report.DateApproveAuto
			) IS NOT NULL
		)
	)
OR (
	(
		(
			customer_service.tbl_fraud_check_report.DateApproveManual
		) IS NOT NULL
	)
);*/

#36 B 237 U TimeToRejectManual  MC  OK
UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.TimeToRejectManual = 
	TIMEDIFF(customer_service.tbl_fraud_check_report.DateRejectManual
,customer_service.tbl_fraud_check_report.DateOrderPlaced
)
WHERE customer_service.tbl_fraud_check_report.DateRejectManual IS NOT NULL
;


###################################################
###################################################
#37 B 238 U TimeToRejectTotal  MC   ¿?
UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.TimeToRejectTotal = 
CASE 1 
WHEN customer_service.tbl_fraud_check_report.DateRejectManual IS NOT NULL 
THEN timeDIFF(customer_service.tbl_fraud_check_report.DateRejectManual
,customer_service.tbl_fraud_check_report.DateOrderPlaced)
WHEN customer_service.tbl_fraud_check_report.DateRejectAuto IS NOT NULL 
THEN TIMEDIFF(customer_service.tbl_fraud_check_report.DateRejectAuto
,customer_service.tbl_fraud_check_report.DateOrderPlaced) 
ELSE 
NULL END;
/*((IFNULL (customer_service.tbl_fraud_check_report.DateRejectAuto,0) + IFNULL (
			customer_service.tbl_fraud_check_report.DateRejectManual
		,0) - customer_service.tbl_fraud_check_report.DateOrderPlaced
	)
) * 24
WHERE
	(
		(
			(
				customer_service.tbl_fraud_check_report.DateRejectAuto
			) IS NOT NULL
		)
	)
OR (
	(
		(
			customer_service.tbl_fraud_check_report.DateRejectManual
		) IS NOT NULL
	)
);*/

#38 B 239 U TimeToResolveManual  MC  ok
UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.TimeToResolveManual = IFNULL (
	customer_service.tbl_fraud_check_report.TimeToApproveManual
,0) + IFNULL(
	customer_service.tbl_fraud_check_report.TimeToRejectManual
,0)
WHERE customer_service.tbl_fraud_check_report.TimeToApproveManual IS NOT NULL
OR customer_service.tbl_fraud_check_report.TimeToRejectManual IS NOT NULL
;

######################################################################
######################################################################
######################################################################
#39 B 240 U TimeToResolveTotal  MC  OK
UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.TimeToResolveTotal = 

CASE 1 
WHEN customer_service.tbl_fraud_check_report.TimeToApproveTotal IS NOT NULL
THEN customer_service.tbl_fraud_check_report.TimeToApproveTotal  
WHEN customer_service.tbl_fraud_check_report.TimeToRejectTotal IS NOT NULL
THEN customer_service.tbl_fraud_check_report.TimeToRejectTotal
ELSE NULL
END;

/*IFNULL (
	customer_service.tbl_fraud_check_report.TimeToApproveTotal
,0) +  IFNULL(
	customer_service.tbl_fraud_check_report.TimeToRejectTotal
,0)*24
WHERE
	(
		(
			(
				customer_service.tbl_fraud_check_report.TimeToApproveTotal
			) IS NOT NULL
		)
	)
OR (
	(
		(
			customer_service.tbl_fraud_check_report.TimeToRejectTotal
		) IS NOT NULL
	)
);*/

#40 B 241 U TotalInvalidOrders MC
UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.TotalInvalidOrders = 1
WHERE customer_service.tbl_fraud_check_report.RejectNewInvalid = 1
OR customer_service.tbl_fraud_check_report.PendingBank = 1
OR customer_service.tbl_fraud_check_report.RejectBank = 1
;

#41 B 242 U TotalAnalyzedOrders MC  OK
UPDATE customer_service.tbl_fraud_check_report 
SET customer_service.tbl_fraud_check_report.TotalAnalyzedOrders = 1
WHERE customer_service.tbl_fraud_check_report.RejectAuto = 1
OR customer_service.tbl_fraud_check_report.ApproveAuto = 1
OR customer_service.tbl_fraud_check_report.ApproveManual = 1
OR customer_service.tbl_fraud_check_report.RejectManual = 1
OR customer_service.tbl_fraud_check_report.PendingManual = 1
;

#42 B 243 U MonthOrderPlaced MC  OK
UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.MonthOrderPlaced = MONTH (
	customer_service.tbl_fraud_check_report.DateOrderPlaced
);

#43 B 244 U YearOrderPlaced MC PK
UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.YearOrderPlaced = YEAR (
	customer_service.tbl_fraud_check_report.DateOrderPlaced
);

UPDATE customer_service.tbl_fraud_check_report INNER JOIN 

(select order_nr FROM

(select * from customer_service.chargeback)a
left join 
(select * from bob_live_mx.payment_sales_banortepayworks)b
on a.`CODIGO DE AUTORIZACION`=b.auth_code and a.`FECHA VENTA`=date(b.created_at)
left join 
bob_live_mx.sales_order c
on b.fk_sales_order=c.id_sales_order 
)A ON customer_service.tbl_fraud_check_report.OrderNumber=A.order_nr
SET customer_service.tbl_fraud_check_report.Chargeback = 1
;

#######################################################################################

UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.RejectBank=0
WHERE OrderNumber=200002138;


UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.RejectNewInvalid=0
WHERE OrderNumber=200002138;

UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.PendingBank=0
WHERE OrderNumber=200002138;

UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.ApproveManual=0
WHERE OrderNumber=200002138;

UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.ApproveAuto=1
WHERE OrderNumber=200002138;




UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.RejectBank=1
WHERE OrderNumber IN(200001179,
200001579,
200002179,
200002659,
200006659,
200002229,
200002529,
200002749,
200002869,
200003129,
200003349,
200004769,
200005269,
200005929,
200007529,
200007829,
200002626,
200004346,
200004996,
200005626,
200006726,
200008626
);


UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.PendingBank =0
WHERE OrderNumber IN(
200001179,
200001579,
200002179,
200002659,
200006659,
200002229,
200002529,
200002749,
200002869,
200003129,
200003349,
200004769,
200005269,
200005929,
200007529,
200007829,
200002626,
200004346,
200004996,
200005626,
200006726,
200008626
);

UPDATE customer_service.tbl_fraud_check_report
SET customer_service.tbl_fraud_check_report.RejectNewInvalid
 =0
WHERE OrderNumber IN(
200001179,
200001579,
200002179,
200002659,
200006659,
200002229,
200002529,
200002749,
200002869,
200003129,
200003349,
200004769,
200005269,
200005929,
200007529,
200007829,
200002626,
200004346,
200004996,
200005626,
200006726,
200008626
);

DROP TABLE IF EXISTS tmp_payment_sales_banortepayworks;
create TEMPORARY table tmp_payment_sales_banortepayworks
(auth_code DOUBLE,created_at date,fk_sales_order FLOAT , KEY(fk_sales_order));
insert INTO tmp_payment_sales_banortepayworks
select auth_code,created_at,fk_sales_order from bob_live_mx.payment_sales_banortepayworks;

DROP TABLE IF EXISTS tmp_sales_order;
create TEMPORARY table tmp_sales_order
(id_sales_order DOUBLE,order_nr DOUBLE, KEY(id_sales_order));
insert INTO tmp_sales_order
select id_sales_order,order_nr from bob_live_mx.sales_order;



UPDATE customer_service.tbl_fraud_check_report INNER JOIN 

(select order_nr FROM

(select * from customer_service.chargeback)a
left join 

(select * from tmp_payment_sales_banortepayworks)b
on a.`CODIGO DE AUTORIZACION`=b.auth_code and a.`FECHA VENTA`=date(b.created_at)
left join 
tmp_sales_order c
on b.fk_sales_order=c.id_sales_order 
)A ON customer_service.tbl_fraud_check_report.OrderNumber=A.order_nr
SET customer_service.tbl_fraud_check_report.Chargeback = 1
;

UPDATE 
	
customer_service.tbl_fraud_check_report
INNER JOIN bob_live_mx.sales_order_item 
ON customer_service.tbl_fraud_check_report.id_sales_order = bob_live_mx.sales_order_item.fk_sales_order

SET customer_service.tbl_fraud_check_report.isElectronic_good = 1
WHERE bob_live_mx.sales_order_item.fk_catalog_electronic_good_type is not NULL
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`eduardo.martinez`@`%`*/ /*!50003 PROCEDURE `sp_llamadasRegional`()
BEGIN

truncate rep_llamadas_regional;
insert into rep_llamadas_regional
#Consolidado LLmadas
SELECT BD.Fecha,'MX' AS Pais,BD.Franja,Recibidas,
	Atendidas,
	Abandonadas,
	Duracion,
	Espera,
	Transferencia,
	Atendidas20,
  Satisfaccion,
	gross_orders
,NULL as Semana

 FROM 

tbl_base_48 BD
LEFT JOIN
(
SELECT
		a.Fecha,
	a.Franja,
	Recibidas,
	Atendidas,
	Abandonadas,
	Duracion,
	Espera,
	Transferencia,
	Atendidas20,
  FCR/Peticiones as Satisfaccion
FROM
	(
select Fecha,Franja,
sum(if (cola='Post-Venta',Contador,0)) Recibidas,
			sum(if (cola='Post-Venta',Atendida,0))+sum(if (cola='Post-Venta',Transferencia,0)) Atendidas,
			sum(if (cola='Post-Venta',Abandonada,0)) Abandonadas,
			sum(if (cola='Post-Venta',Duracion,0)) Duracion,
			sum(if (cola='Post-Venta',Transferencia,0)) Transferencia,
			sum(if (cola='Post-Venta',Espera,0)) Espera,
			sum(if (cola='Post-Venta' AND Espera<=20,Atendida,0))+sum(if (cola='Post-Venta' AND Espera<=20,Transferencia,0)) Atendidas20,
			SUM(if (cola='Post-Venta' ,FCR,0))FCR,
			count(if (cola='Post-Venta' ,question2,0))Peticiones
FROM
customer_service.tbl_queueMetrics30
GROUP BY Fecha,Franja)a  
)llamadas
ON BD.Fecha=llamadas.Fecha AND BD.Franja=llamadas.Franja
LEFT JOIN
(SELECT
	date,
IF (
	MINUTE (`hour`) < 30,
	concat(time_format(`hour`,'%H'),'00'),
	concat(time_format(`hour`,'%H'),'30')
) Intervalo,
 count(DISTINCT(orderid)) gross_orders
FROM
	production.tbl_order_detail

WHERE
	obc = 1
GROUP BY
	date,

IF (
	MINUTE (`hour`) < 30,
	concat(time_format(`hour`,'%H'),'00'),
	concat(time_format(`hour`,'%H'),'30')
))Ordenes
ON  BD.Fecha=Ordenes.Date 
AND BD.Id=cast(Ordenes.Intervalo as char)
where BD.Fecha<date(CURDATE())

UNION ALL
SELECT BD.Fecha,'CO' as Pais,Franja,Recibidas,
	Atendidas,
	Abandonadas,
	Duracion,
	Espera,
	Transferencia,
	Atendidas20,
  Satisfaccion,
	gross_orders,NULL as Semana
from 
customer_service.tbl_base_48 BD
left join 
(select t1.*,gross_orders from
(select 
    event_date As Fecha,
		event_shift as Intervalo,
		sum(net_event) Recibidas,	
		sum(answered_in_wh) Atendidas,
		sum(unanswered_in_wh) Abandonadas,
		sum(call_duration_seg) Duracion,
    sum(hold_duration_seg) Espera,
    sum(answered_in_wh_20) Atendidas20,
    sum(transfered_to_queue) Transferencia,
	  sum(good)/COUNT(answered_2) Satisfaccion
from
    customer_service_co.bi_ops_cc
where
    pos = 1 and net_event = 1 and event_date>='2013-10-01'
group by event_date , event_shift) as t1
inner join
(select 
    date_ordered,
    cast((if(minute(hour) < 30,
        concat(hour(hour), ':', '00'),
        concat(hour(hour), ':', '30'))) as time) shift_ordered,
    count(distinct (order_nr)) gross_orders
from
    bazayaco.tbl_order_detail
where
    obc = 1
	and date_ordered>='2013-10-01'
group by date_ordered , cast((if(minute(hour) < 30,
        concat(hour(hour), ':', '00'),
        concat(hour(hour), ':', '30'))) as time))
as t2
on t1.Fecha=t2.date_ordered and t1.Intervalo=t2.shift_ordered
)RepCo
ON
BD.Fecha=RepCo.Fecha and BD.Id_Alt=RepCo.Intervalo

where BD.Fecha<date(CURDATE())

union ALL

SELECT
date(CURDATE()-1) as `Fecha`,
'VE' as `Pais`,
NULL as `Franja`,
NULL as `Recibidas`,
NULL as `Atendidas`,
NULL as `Abandonadas`,
NULL as `Duracion`,
NULL as `Espera`,
NULL as `Transferencia`,
NULL as `Atendidas20`,
NULL as `Satisfaccion`,
NULL as `gross_orders`,
NULL as `Semana`

UNION ALL

SELECT
date(CURDATE()-1) as `Fecha`,
'PE' as `Pais`,
NULL as `Franja`,
NULL as `Recibidas`,
NULL as `Atendidas`,
NULL as `Abandonadas`,
NULL as `Duracion`,
NULL as `Espera`,
NULL as `Transferencia`,
NULL as `Atendidas20`,
NULL as `Satisfaccion`,
NULL as `gross_orders`,
NULL as `Semana`


; #and BD.Fecha>='2013-09-09';

UPDATE  rep_llamadas_regional SET Semana=date_format(fecha,'%Y-%u');

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `sp_llamadasRegionalAgt`()
BEGIN

TRUNCATE rep_llamadas_agt_regional;
INSERT INTO rep_llamadas_agt_regional
SELECT BD.Fecha,
Franja,
'CO' as Pais,
Agente,
Recibidas,
	Atendidas,
	Abandonadas,
	Duracion,
	Espera,
	EsperaAgente,
	Transferencia,
	Atendidas20,
	Satisfaccion,
	Respuestas_Satisfaccion,
	FCR,
	Peticiones_FCR,
	Abandonadas_menor5,
	NULL as Semana
from 
customer_service.tbl_base_48 BD
left join 
(
select 
    event_date As Fecha,
	event_shift as Intervalo,
	agent_name as Agente,
	sum(net_event) Recibidas,	
	sum(answered_in_wh) Atendidas,
	sum(unanswered_in_wh) Abandonadas,
	sum(call_duration_seg) Duracion,
    sum(hold_duration_seg) Espera,
	sum(agent_hold_time_seg) EsperaAgente,
    sum(answered_in_wh_20) Atendidas20,
    sum(transfered_to_queue) Transferencia,
	sum(good) Satisfaccion,
	sum(answered_3) Respuestas_Satisfaccion,
	sum(if(first_contact=1 and resolution=1,1,0)) FCR,
	sum(answ_1_2) Peticiones_FCR,
	sum(unanswered_in_wh_cust) Abandonadas_menor5
from
    customer_service_co.bi_ops_cc
where
    pos = 1 and net_event = 1 and event_date>='2013-10-01'
group by event_date , event_shift,agent_name)
RepCo
ON
BD.Fecha=RepCo.Fecha and BD.Id_Alt=RepCo.Intervalo
where BD.Fecha<date(CURDATE())

union ALL


SELECT 
BD.Fecha,
BD.Franja,
'MX' AS Pais,
Agente,
Recibidas,
Atendidas,
Abandonadas,
Duracion,
Espera,
EsperaAgente,
Transferencia,
Atendidas20,
null as Satisfaccion,
null as Respuestas_Satisfaccion,
FCR,
Peticiones_FCR,
Abandonadas_menor5,
NULL as Semana

 FROM 

tbl_base_48 BD
LEFT JOIN
(
SELECT
	a.Fecha,
	a.Franja,
	Agente,
	Recibidas,
	Atendidas,
	Abandonadas,
	Duracion,
	Espera,
	EsperaAgente,
	Transferencia,
	Atendidas20,
    FCR, Peticiones_FCR,
    Abandonadas_menor5	
FROM
	(
select Fecha,Franja,Agente,
sum(if (cola='Post-Venta',Contador,0)) Recibidas,
			sum(if (cola='Post-Venta',Atendida,0))+sum(if (cola='Post-Venta',Transferencia,0)) Atendidas,
			sum(if (cola='Post-Venta',Abandonada,0)) Abandonadas,
			sum(if (cola='Post-Venta',Duracion,0)) Duracion,
			sum(if (cola='Post-Venta',Transferencia,0)) Transferencia,
			sum(if (cola='Post-Venta',TiempoEnCola,0)) Espera,
			sum(if (cola='Post-Venta',Espera,0)) EsperaAgente,
			sum(if (cola='Post-Venta' AND TiempoEnCola<=20,Atendida,0))+sum(if (cola='Post-Venta' AND TiempoEnCola<=20,Transferencia,0)) Atendidas20,
			SUM(if (cola='Post-Venta' ,FCR,0))FCR,
			count(if (cola='Post-Venta' ,question2,0))Peticiones_FCR,
			sum(if (cola='Post-Venta' and TiempoEnCola<=5,Abandonada,0)) Abandonadas_menor5
FROM
customer_service.tbl_queueMetrics30
GROUP BY Fecha,Franja,Agente)a  
)llamadas
ON BD.Fecha=llamadas.Fecha AND BD.Franja=llamadas.Franja
where BD.Fecha<date(CURDATE())
UNION ALL

SELECT
date(CURDATE()-1)as `Fecha`,
NULL as `Franja`,
'VE' as `Pais`,
NULL as `Agente`,
NULL as `Recibidas`,
NULL as `Atendidas`,
NULL as `Abandonadas`,
NULL as `Duracion`,
NULL as `Espera`,
NULL as EsperaAgente,
NULL as `Transferencia`,
NULL as `Atendidas20`,
NULL as `Satisfaccion`,
null as Respuestas_Satisfaccion,
NULL as FCR,
NULL as Peticiones_FCR,
NULL as `Abandonadas_menor5	`,
NULL as `Semana`


UNION ALL

SELECT
date(CURDATE()-1) as `Fecha`,
NULL as `Franja`,
'PE' as `Pais`,
NULL as `Agente`,
NULL as `Recibidas`,
NULL as `Atendidas`,
NULL as `Abandonadas`,
NULL as `Duracion`,
NULL as `Espera`,
NULL as EsperaAgente,
NULL as `Transferencia`,
NULL as `Atendidas20`,
NULL as `Satisfaccion`,
null as Respuestas_Satisfaccion,
NULL as FCR,
NULL as Peticiones_FCR,
NULL as `Abandonadas_menor5	`,
NULL as `Semana`

;
UPDATE  rep_llamadas_agt_regional SET Semana=date_format(fecha,'%Y-%u');

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `sp_llamadasRegionalAgtDay`()
BEGIN

#Autor:Paula Andrea Mendoza
#Fecha: Mayo 7 de 2014

truncate table rep_llamadas_agt_regional_day;

insert into rep_llamadas_agt_regional_day (Fecha, Pais, Agente, Identificacion, Atendidas, Duracion, EsperaAgente, Atendidas20, Transferencia, Satisfaccion,
Respuestas_Satisfaccion,FCR,Peticiones_FCR)

select 
    event_date As Fecha,
	'CO' as Pais,
	agent_name as Agente,
	agent_identification as Identificacion,
	sum(answered_in_wh) Atendidas,
	sum(call_duration_seg) Duracion,
	sum(agent_hold_time_seg) EsperaAgente,
    sum(answered_in_wh_20) Atendidas20,
    sum(transfered_to_queue) Transferencia,
	sum(good) Satisfaccion,
	sum(answered_3) Respuestas_Satisfaccion,
	sum(if(first_contact=1 and resolution=1,1,0)) FCR,
	sum(answ_1_2) Peticiones_FCR
from
    customer_service_co.bi_ops_cc_dyalogo
where
    pos = 1 and net_event = 1 
	and unanswered_in_wh=0
	and event_date>='2014-04-01'
group by event_date , agent_name

UNION all

select 
    event_date As Fecha,
	'MX' as Pais,
	agent_name as Agente,
	agent_identification as Identificacion,
	sum(answered_in_wh) Atendidas,
	sum(call_duration_seg) Duracion,
	sum(agent_hold_time_seg) EsperaAgente,
    sum(answered_in_wh_20) Atendidas20,
    sum(transfered_to_queue) Transferencia,
	sum(good) Satisfaccion,
	sum(answered_3) Respuestas_Satisfaccion,
	sum(if(first_contact=1 and resolution=1,1,0)) FCR,
	sum(answ_1_2) Peticiones_FCR
from
    customer_service.bi_ops_cc_mx
where
    pos = 1 and net_event = 1 
	and unanswered_in_wh=0
	and event_date>='2014-04-01'
group by event_date , agent_name

UNION ALL

select 
    event_date As Fecha,
	'PE' as Pais,
	agent_name as Agente,
	agent_identification as Identificacion,
	sum(answered_in_wh) Atendidas,
	sum(call_duration_seg) Duracion,
	sum(agent_hold_time_seg) EsperaAgente,
    sum(answered_in_wh_20) Atendidas20,
    sum(transfered_to_queue) Transferencia,
	sum(good) Satisfaccion,
	sum(answered_3) Respuestas_Satisfaccion,
	sum(if(first_contact=1 and resolution=1,1,0)) FCR,
	sum(answ_1_2) Peticiones_FCR
from
    customer_service_pe.bi_ops_cc_pe
where
    pos = 1 and net_event = 1 
	and unanswered_in_wh=0
	and event_date>='2014-04-01'
group by event_date , agent_name;

#Actualizar Holidays

update rep_llamadas_agt_regional_day a 
inner join operations_co.calendar b 
on a.Fecha=b.dt
set is_holiday=isholiday
where pais='CO';

update rep_llamadas_agt_regional_day a 
inner join operations_mx.calendar b 
on a.Fecha=b.dt
set is_holiday=isholiday
where pais='MX';


update rep_llamadas_agt_regional_day a 
inner join operations_pe.calendar b 
on a.Fecha=b.dt
set is_holiday=isholiday
where pais='PE';

update rep_llamadas_agt_regional_day a 
inner join operations_ve.calendar b 
on a.Fecha=b.dt
set is_holiday=isholiday
where pais='VE';

#Horas de llamada
update rep_llamadas_agt_regional_day 
set
HorasEnLlamada=Duracion/3600;

#Tiempos Colombia
update rep_llamadas_agt_regional_day a
inner join 
(select 
fecha, cedula, campana_dyalogo, sum(tiempo_conexion_segundos)/3600 horas_conexion, 
(sum(tiempo_conexion_segundos)-sum(total_tiempo_efectivo_segundos))/3600 horas_pausas
from customer_service_co.bi_ops_cc_adherencia_sac
where hora_conexion is not null
and fecha>='2014-04-01'
group by fecha, cedula) b
on cast(a.identificacion as unsigned)=cast(b.cedula as unsigned)
and a.Fecha=b.fecha
set
HorasConexion=horas_conexion,
HorasPausas=horas_pausas
where pais='CO';

#Agregar tiempos de conexión
update  rep_llamadas_agt_regional_day a 
inner join customer_service_co.bi_ops_matrix_sac b
on cast(a.identificacion as unsigned)=cast(b.identification as unsigned)
set
HorasProgramadas=if(duracion_turno='6.5',6.5,if(is_holiday=1 or weekday(fecha)>=5,5, 9)),
proceso=process;


#Tiempos de pausas México
update rep_llamadas_agt_regional_day a
inner join
(select date(datetime1) date,
nombre,  sum(if(type='Break',tiempo_segundos,0))/3600 horas_break,
sum(if(type='Zendesk',tiempo_segundos,0))/3600 horas_zendesk
 from bi_ops_cc_pauses_mx
where type is not null
group by date(datetime1),
nombre) b
on a.agente=b.nombre
and a.fecha=b.date
set
HorasACW=horas_zendesk,
HorasPausas=horas_break
where pais='MX';

#Tiempos conexion y programación México
update rep_llamadas_agt_regional_day a
inner join
(select Fecha, AGENTE, Hora_entrada, Hora_salida, time_to_sec(TIMEDIFF(Hora_salida, Hora_entrada))/3600 horas_conexion,
JORNADA,PUESTO  from tbl_time_in_office a inner join
tbl_staff_customer_service b ON right(a.agent, 4) = b.Extension
where a.Fecha >= '2014-04-01') b
on a.agente=b.AGENTE
and a.fecha=b.Fecha
set
HorasConexion=horas_conexion,
HorasProgramadas=if(weekday(a.fecha)=6,6,if(weekday(a.fecha)=5,5, JORNADA)),
proceso=PUESTO
where pais='MX';

#Tiempos de conexión y programación Perú
update  rep_llamadas_agt_regional_day t1
inner join 
(select date, Nombre, Puesto,sum(tiempo_conexion)/3600 conexion,
`HORAS BRUTAS LV` horario from customer_service_pe.time_in_office a 
inner join customer_service_pe.tbl_staff_CC_PE b on right(a.agent,3)=b.Extension
group by date, agent) t2
on t1.Agente=t2.Nombre
and t1.Fecha=t2.date
set
HorasConexion=conexion,
HorasProgramadas=if(weekday(t1.fecha)=6 or is_holiday=1,5,if(weekday(t1.fecha)=5,3, horario)),
proceso=Puesto
where pais='PE';

#Ajuste Horas Programadas
update rep_llamadas_agt_regional_day
set
HorasConexion=HorasProgramadas
where HorasConexion>12;

#Ajuste Pausas
update rep_llamadas_agt_regional_day
set HorasPausas=0
where HorasPausas>HorasConexion;

#Filtro de agentes de postventa
update rep_llamadas_agt_regional_day
set
postsales_agent=if(pais='CO' and proceso like '%SAC%',1,if(pais='MX' and proceso like '%FRONT OFFICE%',1,if(pais='PE' and proceso like '%Inbound Calls Agent%',1,0)));

UPDATE  rep_llamadas_agt_regional_day SET Semana=date_format(fecha,'%Y-%u');


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`eduardo.martinez`@`%`*/ /*!50003 PROCEDURE `sp_sales_Report`()
BEGIN

#TRUNCATE TABLE tbl_sales_hist_ev;
DROP TEMPORARY TABLE IF EXISTS sales_order_ev;
CREATE TEMPORARY TABLE sales_order_ev (key(order_nr))
SELECT
	created_at,
	assisted_sales_operator,
	order_nr 
FROM 
	bob_live_mx.sales_order
WHERE 
	DATE_FORMAT(created_at, '%Y%m')>= 201309 and assisted_sales_operator is not null;


DROP TEMPORARY TABLE IF EXISTS A_Master_ev;
CREATE TEMPORARY TABLE A_Master_ev (key(OrderNum))
SELECT 
	Master.OrderNum,
	Master.ItemID,
  Master.OriginalPrice,
	Master.Price,
  Master.CouponCode,
  Master.CouponValue,
  Master.PaidPrice,
  Master.TaxPercent,
  Master.PaidPriceAfterTax,

  Master.Cancellations,
	Master.OrderAfterCan,
	Master.OrderBeforeCan,
  Master.Returns,
  
	Master.Status,
	Master.PaymentMethod,
  Master.Installment,

	Master.DateCollected,
	Master.DateDelivered,
	Sales.created_at,
	Sales.assisted_sales_operator
FROM 
	           development_mx.A_Master as Master
  INNER JOIN sales_order_ev       AS Sales 
          ON Sales.order_nr = Master.OrderNum
;

#INSERT INTO tbl_sales_hist_ev
DROP TABLE IF EXISTS tbl_sales_hist_ev;
CREATE TABLE tbl_sales_hist_ev ( 
                                 PRIMARY KEY ( Date, telesales,agent_Name, OrderNum, ItemID ) , 
                                         KEY ( telesales) , 
                                         KEY ( OrderNum ),
                                         KEY ( ItemID )
 )
SELECT
	DATE(created_at) AS Date,
	Master.created_at,
	Master.assisted_sales_operator AS telesales,
	cast( Nombre as char(50) ) AS agent_Name,
	Master.OrderNum,
	Master.ItemID,
  Master.OriginalPrice,
	Master.Price,
  Master.CouponCode,
  Master.CouponValue,
  Master.PaidPrice,
  Master.TaxPercent,
  Master.PaidPriceAfterTax,

  Master.Cancellations,
	Master.OrderAfterCan,
	Master.OrderBeforeCan,
  Master.Returns,
  
	Master.Status,
	Master.PaymentMethod,
  Master.Installment,

	Master.DateCollected,
	Master.DateDelivered
FROM
	        tbl_staff_ev a
LEFT JOIN A_Master_ev AS Master 
       ON a.telesales = Master.assisted_sales_operator AND a.telesales is not null
ORDER BY OrderNum, ItemId
;


TRUNCATE rep_detalle_ventas_dia;
INSERT INTO rep_detalle_ventas_dia (
Date,
agent_Name
)
SELECT DISTINCT
	case when DateCollected = '0000-00-00' then date else Datecollected end  As Date,
	agent_Name
FROM
	tbl_sales_hist_ev;


DROP TEMPORARY TABLE IF EXISTS grossTABLE;
CREATE TEMPORARY TABLE grossTABLE (key(Date,agent_Name))

SELECT 
	Date,
	agent_Name,
	COUNT(OrderNum) AS gross_Ord,
	SUM(grossRev) AS gross_Rev 

FROM 
(	SELECT
		Date,
		agent_Name,
		OrderNum,
		sum(Price)grossRev 
	FROM 
		tbl_sales_hist_ev
	
	
	
	GROUP BY
		Date,
		agent_Name,
		OrderNum
)A
GROUP BY
	date,
	agent_Name
;

UPDATE customer_service.rep_detalle_ventas_dia A
LEFT JOIN
grossTABLE B
ON A.agent_name=B.agent_Name and A.Date=B.Date
SET grossOrder=gross_Ord;


UPDATE customer_service.rep_detalle_ventas_dia A
LEFT JOIN
grossTABLE B
ON A.agent_name=B.agent_Name and A.Date=B.Date
SET grossRev=gross_rev;


DROP TEMPORARY TABLE IF EXISTS netTABLE;
CREATE TEMPORARY TABLE netTABLE (key(Date,agent_Name))
SELECT 
	Date,
	agent_Name,
	COUNT(OrderNum) AS net_Ord,
	SUM(netRev) AS net_Rev 

FROM 
(	SELECT
case when DateCollected = '0000-00-00' then date else Datecollected end  As Date,		


		agent_Name,
		OrderNum,
		SUM(PaidPrice)netRev 
	FROM 
		tbl_sales_hist_ev
	WHERE
		 
`Status` in ('delivered','shipped_and_stock_updated','closed','ready_to_ship','exported')
		
	
	GROUP BY
		case when DateCollected = '0000-00-00' then date else Datecollected end,
		agent_Name,
		OrderNum
)A
GROUP BY
	Date,
	agent_Name
;

UPDATE customer_service.rep_detalle_ventas_dia A
LEFT JOIN
netTABLE B
ON A.agent_name=B.agent_Name and A.Date=B.Date
SET netOrder=net_Ord;


UPDATE customer_service.rep_detalle_ventas_dia A
LEFT JOIN
netTABLE B
ON A.agent_name=B.agent_Name and A.Date=B.Date
SET netRev=net_Rev;



DROP TEMPORARY TABLE IF EXISTS pendingTABLE;
CREATE TEMPORARY TABLE pendingTABLE (key(Date,agent_Name))

SELECT 
	Date,
	agent_Name,
	COUNT(OrderNum) AS peding_Ord,
	SUM(grossRev) AS pending_Rev 

FROM 
(	SELECT
		Date,
		agent_Name,
		OrderNum,
		sum(PaidPrice)grossRev 
	FROM 
		tbl_sales_hist_ev
where `Status` in ('payment_confirmation_pending_reminded_2','manual_fraud_check_pending','payment_pending','payment_confirmation_pending_reminded_1')
	
	
	
	GROUP BY
		Date,
		agent_Name,
		OrderNum
)A
GROUP BY
	Date,
	agent_Name
;


UPDATE customer_service.rep_detalle_ventas_dia A
LEFT JOIN
pendingTABLE B
ON A.agent_name=B.agent_Name and A.Date=B.Date
SET pendingOrder=peding_Ord;


UPDATE customer_service.rep_detalle_ventas_dia A
LEFT JOIN
pendingTABLE B
ON A.agent_name=B.agent_Name and A.Date=B.Date
SET pendingRev=pending_Rev;

#----------------------Llamadas----------------------------------------

UPDATE customer_service.rep_detalle_ventas_dia A
LEFT JOIN
VW_LlamadasxAsesorPreVenta B
ON A.agent_name=B.TELESALES and A.Date=B.Fecha
SET Llamadas=Atn;



#-------------------------------por mes.---------------------------------------------

TRUNCATE rep_detalle_ventas_mes;
INSERT INTO rep_detalle_ventas_mes 
SELECT 
DATE_FORMAT(date,'%Y%m')Date,
agent_name,
sum(grossOrder)grossOrder,
sum(grossRev)grossRev,
SUM(netOrder)netOrder,
SUM(netRev)netRev,
null as targetOrdAch,
null as targetRevAch,
sum(pendingOrder)pendingOrder,
SUM(pendingRev)pendingRev

FROM customer_service.rep_detalle_ventas_dia
GROUP BY DATE_FORMAT(date,'%Y%m'),agent_name
;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`eduardo.martinez`@`%`*/ /*!50003 PROCEDURE `sp_sales_Report_co`()
BEGIN

#TRUNCATE TABLE tbl_sales_hist_ev;
DROP TEMPORARY TABLE IF EXISTS sales_order_ev;
CREATE TEMPORARY TABLE sales_order_ev (key(order_nr))
SELECT
	created_at,
	assisted_sales_operator,
	order_nr 
FROM 
	bob_live_co.sales_order
WHERE 
	DATE_FORMAT(created_at, '%Y%m')>= 201309 and assisted_sales_operator is not null;


DROP TEMPORARY TABLE IF EXISTS A_Master_ev;
CREATE TEMPORARY TABLE A_Master_ev (key(OrderNum))
SELECT 
	Master.OrderNum,
	Master.ItemID,
  Master.OriginalPrice,
	Master.Price,
  Master.CouponCode,
  Master.CouponValue,
  Master.PaidPrice,
  Master.TaxPercent,
  Master.PaidPriceAfterTax,

  Master.Cancellations,
	Master.OrderAfterCan,
	Master.OrderBeforeCan,
  Master.Returns,
  
	Master.Status,
	Master.PaymentMethod,
  Master.Installment,

	Master.DateCollected,
	Master.DateDelivered,
	Sales.created_at,
	Sales.assisted_sales_operator
FROM 
	           development_co.A_Master as Master
  INNER JOIN sales_order_ev       AS Sales 
          ON Sales.order_nr = Master.OrderNum
;



#INSERT INTO tbl_sales_hist_ev
DROP TABLE IF EXISTS tbl_sales_hist_ev_co;
CREATE TABLE tbl_sales_hist_ev_co ( 
                                 PRIMARY KEY ( Date, telesales, order_nr, ItemID ) , 
                                         KEY ( telesales) , 
                                         KEY ( order_nr ),
                                         KEY ( ItemID )
 )
SELECT
	DATE(created_at) AS Date,
	`name` AS agent_Name,
	Master.OrderNum as order_nr,	
	Master.Status,
	Master.created_at,	
	Master.ItemID,
	Master.Price,
	Master.PaidPrice,
	Master.OrderAfterCan,
	Master.OrderBeforeCan,
	Master.PaymentMethod,
	Master.DateCollected,
	Master.assisted_sales_operator AS telesales
		
FROM
	        (select * from customer_service_co.relacion_agentes where campaign <>'NN') a
LEFT JOIN A_Master_ev AS Master 
       ON a.sales_operator = Master.assisted_sales_operator AND a.sales_operator is not null
WHERE DATE(created_at)<>'0000-00-00'
ORDER BY order_nr, ItemId
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`eduardo.martinez`@`%`*/ /*!50003 PROCEDURE `sp_sales_Report_pe`()
BEGIN


DROP TEMPORARY TABLE IF EXISTS sales_order_ev;
CREATE TEMPORARY TABLE sales_order_ev (key(order_nr))
SELECT
	created_at,
	assisted_sales_operator,
	order_nr 
FROM 
	bob_live_pe.sales_order
WHERE 
	DATE_FORMAT(created_at, '%Y%m')>= 201309 and assisted_sales_operator is not null;


DROP TEMPORARY TABLE IF EXISTS A_Master_ev;
CREATE TEMPORARY TABLE A_Master_ev (key(OrderNum))
SELECT 
	OrderNum,
	ItemID,
	Price,
	OrderAfterCan,
	OrderBeforeCan,
	`Status`,
	PaymentMethod,
	DateCollected
FROM 
	development_pe.A_Master
WHERE	
	DATE_FORMAT(DATE, '%Y%m')>=201309;
TRUNCATE TABLE tbl_sales_hist_ev_pe;

INSERT INTO tbl_sales_hist_ev_pe
SELECT
	DATE(b.created_at) AS Date,
	`username` AS agent_Name,
	b.order_nr,
	`Status`,
  b.created_at,
	ItemID,
	Price,
	c.OrderAfterCan,
	c.OrderBeforeCan,
	PaymentMethod,
	DateCollected,
	assisted_sales_operator
FROM
	customer_service.tbl_staff_pe a
INNER JOIN sales_order_ev b ON a.username = b.assisted_sales_operator
LEFT JOIN A_Master_ev c ON b.order_nr = c.OrderNum;




END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`eduardo.martinez`@`%`*/ /*!50003 PROCEDURE `sp_sales_Report_ve`()
BEGIN

DROP TEMPORARY TABLE IF EXISTS sales_order_ev;
CREATE TEMPORARY TABLE sales_order_ev (key(order_nr))
SELECT
	created_at,
	assisted_sales_operator,
	order_nr 
FROM 
	bob_live_ve.sales_order
WHERE 
	DATE_FORMAT(created_at, '%Y%m')>= 201309 and assisted_sales_operator is not null;


DROP TEMPORARY TABLE IF EXISTS A_Master_ev;
CREATE TEMPORARY TABLE A_Master_ev (key(OrderNum))
SELECT 
	OrderNum,
	ItemID,
	Price,
	OrderAfterCan,
	OrderBeforeCan,
	`Status`,
	PaymentMethod,
	DateCollected
FROM 
	development_ve.A_Master
WHERE	
	DATE_FORMAT(DATE, '%Y%m')>=201309;

TRUNCATE TABLE tbl_sales_hist_ev_ve;

INSERT INTO tbl_sales_hist_ev_ve
SELECT
	DATE(b.created_at) AS Date,
	`Nombre Agente` AS agent_Name,
	b.order_nr,
	`Status`,
  b.created_at,
	ItemID,
	Price,
	c.OrderAfterCan,
	c.OrderBeforeCan,
	PaymentMethod,
	DateCollected,
	assisted_sales_operator
FROM
	customer_service.tbl_staff_ve a
INNER JOIN sales_order_ev b ON a.`Usuario Telesales` = b.assisted_sales_operator
LEFT JOIN A_Master_ev c ON b.order_nr = c.OrderNum;





END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`auto.reports`@`%`*/ /*!50003 PROCEDURE `sp_summary_pymentMethod`()
BEGIN



DELETE customer_service.payment_method_summary.*
FROM
	customer_service.payment_method_summary;


INSERT INTO customer_service.payment_method_summary (
	fk_sales_order,
	order_nr,
  
	grand_total,
	payment_method,
	created_at,
	installments
) SELECT
	bob_live_mx.sales_order.id_sales_order,
	bob_live_mx.sales_order.order_nr,
	bob_live_mx.sales_order.grand_total,
	bob_live_mx.sales_order.payment_method,
	bob_live_mx.sales_order.created_at,
	bob_live_mx.sales_order.installments
FROM
	bob_live_mx.sales_order;


UPDATE bob_live_mx.sales_order_item
INNER JOIN customer_service.payment_method_summary ON bob_live_mx.sales_order_item.fk_sales_order 
= customer_service.payment_method_summary.fk_sales_order
SET customer_service.payment_method_summary.OrderStatus = bob_live_mx.sales_order_item.fk_sales_order_item_status;


UPDATE customer_service.payment_method_summary
INNER JOIN customer_service.sales_order_classification ON customer_service.payment_method_summary.OrderStatus 
= customer_service.sales_order_classification.fk_sales_order_item_status
SET customer_service.payment_method_summary.isGrossOrder 
= customer_service.sales_order_classification.isGrossOrder, customer_service.payment_method_summary.isNetOrder 
= customer_service.sales_order_classification.isNetOrder,customer_service.payment_method_summary.isCanceled 
= customer_service.sales_order_classification.isCanceled,customer_service.payment_method_summary.isReturned 
= customer_service.sales_order_classification.isReturned,
customer_service.payment_method_summary.isPending 
= customer_service.sales_order_classification.isPending;


UPDATE customer_service.payment_method_summary
SET customer_service.payment_method_summary.DayOrderPlaced = date(customer_service.payment_method_summary.created_at)
;



UPDATE customer_service.payment_method_summary SET customer_service.payment_method_summary.WeekOrderPlaced = 
date_format(customer_service.payment_method_summary.DayOrderPlaced,'%Y-%u');


UPDATE (
	customer_service.payment_method_summary INNER
	JOIN bob_live_mx.sales_order ON customer_service.payment_method_summary.fk_sales_order
= bob_live_mx.sales_order.id_sales_order
)
INNER JOIN bob_live_mx.sales_order_address ON bob_live_mx.sales_order.fk_sales_order_address_shipping 
= bob_live_mx.sales_order_address.id_sales_order_address
SET customer_service.payment_method_summary.ShippingRegion = bob_live_mx.sales_order_address.region;


UPDATE (
	customer_service.payment_method_summary INNER
	JOIN bob_live_mx.sales_order ON customer_service.payment_method_summary.fk_sales_order
  = bob_live_mx.sales_order.id_sales_order
)
INNER JOIN bob_live_mx.sales_order_address ON bob_live_mx.sales_order.fk_sales_order_address_billing
 = bob_live_mx.sales_order_address.id_sales_order_address
SET customer_service.payment_method_summary.BillingRegion = bob_live_mx.sales_order_address.region;


UPDATE customer_service.payment_method_summary 
SET customer_service.payment_method_summary.isInvalid = 1
WHERE
	(
		(
			(
				customer_service.payment_method_summary.isGrossOrder
			) = 0
		)
		AND (
			(
				customer_service.payment_method_summary.isNetOrder
			) = 0
		)
	);


UPDATE customer_service.payment_method_summary 
SET customer_service.payment_method_summary.GrossOrderTotal = customer_service.payment_method_summary.grand_total
WHERE
	(
		(
			(
				customer_service.payment_method_summary.isGrossOrder
			) = 1
		)
	);


UPDATE customer_service.payment_method_summary 
SET customer_service.payment_method_summary.NetOrderTotal = customer_service.payment_method_summary.grand_total
WHERE
	(
		(
			(
				customer_service.payment_method_summary.isNetOrder
			) = 1
		)
	);


UPDATE customer_service.payment_method_summary SET customer_service.payment_method_summary.InvalidOrderTotal 
= customer_service.payment_method_summary.grand_total
WHERE
	(
		(
			(
				customer_service.payment_method_summary.isInvalid
			) = 1
		)
	);





UPDATE customer_service.payment_method_groups
INNER JOIN customer_service.payment_method_summary ON customer_service.payment_method_groups.payment_method 
= customer_service.payment_method_summary.payment_method
SET customer_service.payment_method_summary.PaymentType = customer_service.payment_method_groups.PaymentType;


UPDATE customer_service.payment_method_summary INNER
JOIN customer_service.payment_method_groups ON customer_service.payment_method_summary.payment_method 
= customer_service.payment_method_groups.payment_method
SET customer_service.payment_method_summary.PaymentGroup = customer_service.payment_method_groups.PaymentGroup;


UPDATE customer_service.payment_method_summary SET customer_service.payment_method_summary.MonthOrderPlaced = MONTH (
	customer_service.payment_method_summary.created_at
);


UPDATE customer_service.payment_method_summary SET customer_service.payment_method_summary.YearOrderPlaced = YEAR (
	customer_service.payment_method_summary.created_at
);


UPDATE bob_live_mx.sales_order
INNER JOIN customer_service.payment_method_summary ON bob_live_mx.sales_order.id_sales_order 
= customer_service.payment_method_summary.fk_sales_order
SET customer_service.payment_method_summary.ChargedInstallment = 1
WHERE
	(
		(
			(
				bob_live_mx.sales_order.subtotal_without_interests - bob_live_mx.sales_order.grand_total
			) > 0
		)
	);

UPDATE bob_live_mx.sales_order
INNER JOIN customer_service.payment_method_summary ON bob_live_mx.sales_order.id_sales_order 
= customer_service.payment_method_summary.fk_sales_order
SET customer_service.payment_method_summary.MSI = 1
WHERE
bob_live_mx.sales_order.installments>1 and bob_live_mx.sales_order.total_interests=0;


UPDATE bob_live_mx.sales_order
INNER JOIN customer_service.payment_method_summary ON bob_live_mx.sales_order.id_sales_order 
= customer_service.payment_method_summary.fk_sales_order
SET customer_service.payment_method_summary.MCI = 1
WHERE
bob_live_mx.sales_order.installments>1 and bob_live_mx.sales_order.total_interests>0;
	

UPDATE customer_service.payment_method_summary set installments=18 where 
installments=16;


DROP TEMPORARY TABLE IF EXISTS bins;

CREATE TEMPORARY TABLE bins ( KEY ( order_nr ))
SELECT order_nr,
			 bank
FROM
       customer_service.payment_method_summary a
    INNER JOIN bob_live_mx.sales_order_accertify_log soal 
		on  a.fk_sales_order = soal.fk_sales_order
		inner JOIN rafael.Pro_catalog_card_bins ccb 
		on left(ExtractValue( soal.message,'/transaction/paymentInformation/cardNumber'),6)=ccb.bin

WHERE title = 'transaction sent'
;

UPDATE            customer_service.payment_method_summary a
       INNER JOIN bins b
               ON a.order_nr = b.order_nr
SET issuing_bank =b.bank;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `sp_telesalesCo`()
BEGIN

SELECT  'Inicio rutina customer_service.tbl_telesales_co',now();

#Zona horaria de Colombia

set time_zone='-5:00';

truncate table customer_service.tbl_telesales_co; 

#set @max_day=cast((select max(Date) from customer_service.tbl_telesales_co) as date);

#delete from customer_service.tbl_telesales_co
#where Date>=@max_day;
#truncate table bi_ops_cc;


SELECT  'Insertar datos de las ventas',now();
insert into customer_service.tbl_telesales_co(
Date,
`Hour`,`Minute`,
Campaign,AssistedSalesOperator,Channel,ChannelGroup,ChannelGroupAnterior,
OrderNum, ItemID,total_value,
OrderBeforeCan,OrderAfterCan,Status,Source_medium,new_customers,PCOne,PCOnePFive,PCTwo,DateCollected)
select * from
(select a.Date, hour(`Time`) Hour, minute(`Time`) Minute,
b.campaign,b.AssistedSalesOperator,b.channel,channel_group,
channel_group_anterior,a.OrderNum, ItemID, Rev as total_value,
OrderBeforeCan,OrderAfterCan,Status,b.source_medium,CACCustomer,PCOne,PCOnePFive,PCTwo,DateCollected
from development_co_project.A_Master a left join 
marketing_report.channel_report_co b
on a.IdSalesOrder=b.orderID
where (b.AssistedSalesOperator <>"" or b.channel_group='Tele Sales') 
and a.Date>='2014-01-01'
#@max_day
) as t;


SELECT  'Actualizar extensiones de agentes',now();
update customer_service.tbl_telesales_co a
inner join customer_service_co.bi_ops_matrix_ventas b
on a.AssistedSalesOperator=b.sales_operator
set
a.agent_ext=b.phone,
a.identification=b.identification,
a.agent_name=b.name,
a.coordinator=b.coordinator,
a.process=b.process
where a.agent_name is null
;

update customer_service.tbl_telesales_co a
inner join customer_service_co.bi_ops_matrix_ventas b
on a.Campaign=b.channel
set
a.agent_ext=b.phone,
a.identification=b.identification,
a.agent_name=b.name,
a.coordinator=b.coordinator,
a.process=b.process
where a.AssistedSalesOperator =""
;


update customer_service.tbl_telesales_co
set telesales=if(ChannelGroup='Tele Sales',1,0),
other_channels=if(ChannelGroup<>'Tele Sales',1,0);

update customer_service.tbl_telesales_co
set
shift_date_order=if (
		`minute`		<30
		,CONCAT(`hour`,'00'),CONCAT(`hour`,'30'));

update customer_service.tbl_telesales_co a 
inner join development_co_project.A_Master b
on a.ItemID=b.ItemID
set
a.PaidPrice=b.PaidPrice+b.ShippingFee;


SELECT  'Fin rutina bi_ops_cc_sales',now();


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `sp_telesalesConsolidated`()
BEGIN

SELECT  'Inicio rutina customer_service.tbl_telesales_mx',now();

#Zona horaria de México

set time_zone='-6:00';

truncate table customer_service.tbl_telesales_mx; 

#set @max_day=cast((select max(Date) from customer_service.tbl_telesales_co) as date);

#delete from customer_service.tbl_telesales_co
#where Date>=@max_day;
#truncate table bi_ops_cc;


SELECT  'Insertar datos de las ventas',now();
insert into customer_service.tbl_telesales_mx(
Date,
`Hour`,`Minute`,
Campaign,AssistedSalesOperator,Channel,ChannelGroup,ChannelGroupAnterior,
OrderNum, ItemID,total_value,
OrderBeforeCan,OrderAfterCan,Status,Source_medium,new_customers,PCOne,PCOnePFive,PCTwo,DateCollected)
select * from
(select a.Date, hour(`Time`) Hour, minute(`Time`) Minute,
b.campaign,b.AssistedSalesOperator,b.channel,channel_group,
channel_group_anterior,a.OrderNum, ItemID, Rev as total_value,
OrderBeforeCan,OrderAfterCan,Status,b.source_medium,CACCustomer,PCOne,PCOnePFive,PCTwo,DateCollected
from development_mx.A_Master a inner join 
marketing_report.channel_report_mx b
on a.IdSalesOrder=b.orderID
where (b.AssistedSalesOperator is not null or b.channel_group='Tele Sales') 
and a.Date>='2014-01-01'
#@max_day
) as t;


SELECT  'Actualizar extensiones de agentes',now();
update customer_service.tbl_telesales_mx a
inner join customer_service.tbl_staff_ev b
on a.AssistedSalesOperator=b.TELESALES
set
a.agent_ext=b.Extension,
a.agent_name=b.Nombre,
a.coordinator=b.Coordinador,
a.process=b.Puesto
where a.agent_name is null
;


update customer_service.tbl_telesales_mx
set telesales=if(ChannelGroup='Tele Sales',1,0),
other_channels=if(ChannelGroup<>'Tele Sales',1,0);

update customer_service.tbl_telesales_mx
set
shift_date_order=if (
		`minute`		<30
		,CONCAT(`hour`,'00'),CONCAT(`hour`,'30'));


SELECT  'Fin tbl_telesales_mx',now();

SELECT  'Inicio rutina customer_service.tbl_telesales_pe',now();

#Zona horaria de Peru

set time_zone='-5:00';

truncate table customer_service.tbl_telesales_pe; 

#set @max_day=cast((select max(Date) from customer_service.tbl_telesales_co) as date);

#delete from customer_service.tbl_telesales_co
#where Date>=@max_day;
#truncate table bi_ops_cc;


SELECT  'Insertar datos de las ventas',now();
insert into customer_service.tbl_telesales_pe(
Date,
`Hour`,`Minute`,
Campaign,AssistedSalesOperator,Channel,ChannelGroup,ChannelGroupAnterior,
OrderNum, ItemID,total_value,
OrderBeforeCan,OrderAfterCan,Status,Source_medium,new_customers,PCOne,PCOnePFive,PCTwo,DateCollected)
select * from
(select a.Date, hour(`Time`) Hour, minute(`Time`) Minute,
b.campaign,b.AssistedSalesOperator,b.channel,channel_group,
channel_group_anterior,a.OrderNum, ItemID, Rev as total_value,
OrderBeforeCan,OrderAfterCan,Status,b.source_medium,CACCustomer,PCOne,PCOnePFive,PCTwo,DateCollected
from development_pe.A_Master a inner join 
marketing_report.channel_report_pe b
on a.IdSalesOrder=b.orderID
where (b.AssistedSalesOperator is not null or b.channel_group='Tele Sales') 
and a.Date>='2014-01-01'
#@max_day
) as t;



update customer_service.tbl_telesales_pe
set telesales=if(ChannelGroup='Tele Sales',1,0),
other_channels=if(ChannelGroup<>'Tele Sales',1,0);

update customer_service.tbl_telesales_pe
set
shift_date_order=if (
		`minute`		<30
		,CONCAT(`hour`,'00'),CONCAT(`hour`,'30'));


SELECT  'Fin tbl_telesales_pe',now();

SELECT  'Inicio rutina customer_service.tbl_telesales_ve',now();

#Zona horaria de Peru

set time_zone='-4:30';

truncate table customer_service.tbl_telesales_ve; 

#set @max_day=cast((select max(Date) from customer_service.tbl_telesales_co) as date);

#delete from customer_service.tbl_telesales_co
#where Date>=@max_day;
#truncate table bi_ops_cc;


SELECT  'Insertar datos de las ventas',now();
insert into customer_service.tbl_telesales_ve(
Date,
`Hour`,`Minute`,
Campaign,AssistedSalesOperator,Channel,ChannelGroup,ChannelGroupAnterior,
OrderNum, ItemID,total_value,
OrderBeforeCan,OrderAfterCan,Status,Source_medium,new_customers,PCOne,PCOnePFive,PCTwo,DateCollected)
select * from
(select a.Date, hour(`Time`) Hour, minute(`Time`) Minute,
b.campaign,b.AssistedSalesOperator,b.channel,channel_group,
channel_group_anterior,a.OrderNum, ItemID, Rev as total_value,
OrderBeforeCan,OrderAfterCan,Status,b.source_medium,CACCustomer,PCOne,PCOnePFive,PCTwo,DateCollected
from development_ve.A_Master a inner join 
marketing_report.channel_report_ve b
on a.IdSalesOrder=b.orderID
where (b.AssistedSalesOperator is not null or b.channel_group='Tele Sales') 
and a.Date>='2014-01-01'
#@max_day
) as t;



update customer_service.tbl_telesales_ve
set telesales=if(ChannelGroup='Tele Sales',1,0),
other_channels=if(ChannelGroup<>'Tele Sales',1,0);

update customer_service.tbl_telesales_ve
set
shift_date_order=if (
		`minute`		<30
		,CONCAT(`hour`,'00'),CONCAT(`hour`,'30'));


SELECT  'Fin tbl_telesales_ve',now();


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`eduardo.martinez`@`%`*/ /*!50003 PROCEDURE `sp_telesalesRegional`()
BEGIN

TRUNCATE rep_telesales_regional;
INSERT INTO rep_telesales_regional


Select BD.Fecha,
'CO' as Pais,
Franja,
Recibidas,
Atendidas,
Abandonadas,
Duracion,
Espera,
Atendidas20,
Transferencia,
Satisfaccion,
gross_Ord,
gross_Rev,
Items,
net_Ord,
net_Rev,
Tgross_orders,NULL as Semana
FROM

customer_service.tbl_base_48 BD

left join 

(select llamadas.*,gross_Ord,gross_Rev,net_Ord,net_Rev,Items,Tgross_orders from 
(select 
    event_date As Fecha,
		b.id as Intervalo,
		sum(net_event) Recibidas,	
		sum(answered_in_wh) Atendidas,
		sum(unanswered_in_wh) Abandonadas,
		sum(call_duration_seg) Duracion,
    sum(hold_duration_seg) Espera,
    sum(answered_in_wh_20) Atendidas20,
    sum(transfered_to_queue) Transferencia,
	  sum(good)/COUNT(answered_2) Satisfaccion
from
    customer_service_co.bi_ops_cc a
left join 
tbl_48_franjas b
on a.event_shift=b.Id_Alt
where
    pos = 0 and net_event = 1 and event_date>='2013-10-01'
group by event_date , event_shift)llamadas
left JOIN
(SELECT 
	Date,
	#agent_Name,
	Intervalo,
	COUNT(order_nr) AS gross_Ord,
	SUM(grossRev) AS gross_Rev,
	SUM(Items)Items

FROM 
(	SELECT
		Date,
		agent_Name,
		if (
		DATE_FORMAT(created_at,'%i')		<30
		,CONCAT(DATE_FORMAT(created_at,'%H'),'00'),CONCAT(DATE_FORMAT(created_at,'%H'),'30')) as Intervalo,
		order_nr,
		sum(Price)grossRev,
		count(ItemID)As Items
	FROM 
		tbl_sales_hist_ev_co
	GROUP BY
		Date,
		agent_Name,
		if (DATE_FORMAT(created_at,'%i')<30,CONCAT(DATE_FORMAT(created_at,'%H'),'00'),CONCAT(DATE_FORMAT(created_at,'%H'),'30')),
		order_nr
)A
GROUP BY
	date,
	Intervalo
#,agent_Name
)Gross

on llamadas.Fecha=Gross.Date and cast(llamadas.Intervalo as Char)=Gross.Intervalo

left JOIN
(SELECT 
	Date,
	#agent_Name,
	Intervalo,
	COUNT(order_nr) AS net_Ord,
	SUM(netRev) AS net_Rev 

FROM 
(	SELECT
Date(created_at) As Date,		

if (DATE_FORMAT(created_at,'%i')<30,CONCAT(DATE_FORMAT(created_at,'%H'),'00'),CONCAT(DATE_FORMAT(created_at,'%H'),'30')) as Intervalo,
		agent_Name,
		order_nr,
		SUM(Price)netRev 
	FROM 
		tbl_sales_hist_ev_co
	WHERE
		 
`Status` in ('delivered','shipped_and_stock_updated','closed','ready_to_ship','exported')
		
	
	GROUP BY
		Date(created_at),
if (DATE_FORMAT(created_at,'%i')<30,CONCAT(DATE_FORMAT(created_at,'%H'),'00'),CONCAT(DATE_FORMAT(created_at,'%H'),'30')),		
agent_Name,
		order_nr
)A
GROUP BY
	Date,
	Intervalo
#agent_Name
)Net
on llamadas.Fecha=Net.Date and cast(llamadas.Intervalo as Char)=Net.Intervalo




LEFT JOIN
(select 
    date_ordered,
    cast((if(minute(hour) < 30,
        concat(time_format(hour,'%H'), '00'),
        concat(time_format(hour,'%H'), '30'))) as char) shift_ordered,
    count(distinct (order_nr)) Tgross_orders
from
    bazayaco.tbl_order_detail
where
    obc = 1
	and date_ordered>='2013-10-01'
group by date_ordered , cast((if(minute(hour) < 30,
        concat(hour(hour), ':', '00'),
        concat(hour(hour), ':', '30'))) as time))
as t2
on llamadas.Fecha=t2.date_ordered and llamadas.Intervalo=t2.shift_ordered

)gral
on BD.Fecha=gral.Fecha and BD.id=gral.Intervalo

where BD.Fecha<date(CURDATE())


UNION ALL
#INSERT INTO rep_telesales_regional


SELECT BD.Fecha,
'MX' AS Pais,
BD.Franja,
Recibidas,
	Atendidas,
	Abandonadas,
	Duracion,
	Espera,
	Atendidas20,
	Transferencia,
	Satisfaccion,
	gross_Ord,
	gross_Rev,
	Items,
	net_Ord,
	net_Rev,
	Tgross_orders,NULL as Semana
	

FROM 
tbl_base_48 BD
LEFT JOIN

(SELECT
		a.Fecha,
	a.Franja,
	Recibidas,
	Atendidas,
	Abandonadas,
	Duracion,
	Espera,
	Transferencia,
	Atendidas20,
  FCR/Peticiones as Satisfaccion
FROM
	(
select Fecha,Franja,
sum(if (cola='Pre-Venta',Contador,0)) Recibidas,
			sum(if (cola='Pre-Venta',Atendida,0))+sum(if (cola='Pre-Venta',Transferencia,0)) Atendidas,
			sum(if (cola='Pre-Venta',Abandonada,0)) Abandonadas,
			sum(if (cola='Pre-Venta',Duracion,0)) Duracion,
			sum(if (cola='Pre-Venta',Transferencia,0)) Transferencia,
			sum(if (cola='Pre-Venta',Espera,0)) Espera,
			sum(if (cola='Pre-Venta' AND Espera<=20,Atendida,0))+sum(if (cola='Pre-Venta' AND Espera<=20,Transferencia,0)) Atendidas20,
			SUM(if (cola='Pre-Venta' ,FCR,0))FCR,
			COUNT(if (cola='Pre-Venta' ,question2,0))Peticiones
FROM
customer_service.tbl_queueMetrics30
GROUP BY Fecha,Franja)a  
)llamadas
ON BD.Fecha=llamadas.Fecha AND BD.Franja=llamadas.Franja

LEFT JOIN

(#Gross
SELECT 
	Date,
	#agent_Name,
	Franja,
	COUNT(OrderNUm) AS gross_Ord,
	SUM(grossRev) AS gross_Rev,
	SUM(Items)Items

FROM 
(	SELECT
		Date,
		agent_Name,
		if (
		DATE_FORMAT(created_at,'%i')		<30
		,CONCAT(DATE_FORMAT(created_at,'%H'),'00'),CONCAT(DATE_FORMAT(created_at,'%H'),'30')) as Intervalo,
		OrderNum,
		sum(Price)grossRev,
		count(ItemID)As Items
	FROM 
		tbl_sales_hist_ev
	GROUP BY
		Date,
		agent_Name,
		if (DATE_FORMAT(created_at,'%i')<30,CONCAT(DATE_FORMAT(created_at,'%H'),'00'),CONCAT(DATE_FORMAT(created_at,'%H'),'30')),
		OrderNum
 
)A
LEFT JOIN
customer_service.tbl_48_franjas B
ON cast(A.Intervalo as CHAR)=B.Id
GROUP BY
	date,
	Intervalo
#,agent_Name
)Gross

ON BD.Fecha=Gross.Date 
and BD.Franja=cast(Gross.Franja as char)

LEFT JOIN 

(#Net Sales
SELECT 
	Date,
	#agent_Name,
	Franja,
	COUNT(OrderNum) AS net_Ord,
	SUM(netRev) AS net_Rev 

FROM 
(	SELECT
Date,		

if (DATE_FORMAT(created_at,'%i')<30,CONCAT(DATE_FORMAT(created_at,'%H'),'00'),CONCAT(DATE_FORMAT(created_at,'%H'),'30')) as Intervalo,
		agent_Name,
		OrderNum,
		SUM(Price)netRev 
	FROM 
		tbl_sales_hist_ev
	WHERE
		 
`Status` in ('delivered','shipped_and_stock_updated','closed','ready_to_ship','exported')
	GROUP BY
		date,
if (DATE_FORMAT(created_at,'%i')<30,CONCAT(DATE_FORMAT(created_at,'%H'),'00'),CONCAT(DATE_FORMAT(created_at,'%H'),'30')),		
agent_Name,
		OrderNum
)A
LEFT JOIN
customer_service.tbl_48_franjas B
ON cast(A.Intervalo as CHAR)=B.Id

GROUP BY
	Date,
	Intervalo
#agent_Name
)Net

ON BD.Fecha=Net.Date 
and BD.Franja=cast(Net.Franja as char)


INNER JOIN
(SELECT
	date,
IF (
	MINUTE (`hour`) < 30,
	concat(time_format(`hour`,'%H'),'00'),
	concat(time_format(`hour`,'%H'),'30')
) Intervalo,
 count(DISTINCT(orderid)) Tgross_orders
FROM
	production.tbl_order_detail

WHERE
	obc = 1
GROUP BY
	date,

IF (
	MINUTE (`hour`) < 30,
	concat(time_format(`hour`,'%H'),'00'),
	concat(time_format(`hour`,'%H'),'30')
))Ordenes
ON  BD.Fecha=Ordenes.Date 
AND BD.Id=cast(Ordenes.Intervalo as char)
where BD.Fecha<date(CURDATE())
UNION ALL

SELECT
date(CURDATE()-1)as `Fecha`,
'PE' as `Pais`,
NULL as `Franja`,
NULL as `Recibidas`,
NULL as `Atendidas`,
NULL as `Abandonadas`,
NULL as `Duracion`,
NULL as `Espera`,
NULL as `Transferencia`,
NULL as `Atendidas20`,
NULL as `Satisfaccion`,
NULL as `gross_Ord`,
NULL as `gross_Rev`,
NULL as `Items`,
NULL as `net_Ord`,
NULL as `net_Rev`,
NULL as `Tgross_orders`,
NULL as `Semana`

union ALL

SELECT
date(CURDATE()-1) as `Fecha`,
'VE' as `Pais`,
NULL as `Franja`,
NULL as `Recibidas`,
NULL as `Atendidas`,
NULL as `Abandonadas`,
NULL as `Duracion`,
NULL as `Espera`,
NULL as `Transferencia`,
NULL as `Atendidas20`,
NULL as `Satisfaccion`,
NULL as `gross_Ord`,
NULL as `gross_Rev`,
NULL as `Items`,
NULL as `net_Ord`,
NULL as `net_Rev`,
NULL as `Tgross_orders`,
NULL as `Semana`

;
UPDATE  rep_telesales_regional SET Semana=date_format(fecha,'%Y-%u');

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`eduardo.martinez`@`%`*/ /*!50003 PROCEDURE `sp_telesalesRegionalAgt`()
BEGIN

TRUNCATE rep_telesales_agt_regional;
INSERT INTO rep_telesales_agt_regional
Select BD.Fecha,
Franja,
'CO' as Pais,
Nombre,
Recibidas,
Atendidas,
Abandonadas,
Duracion,
Espera,
Atendidas20,
Transferencia,
Satisfaccion,
gross_Ord,
gross_Rev,
net_Ord,
net_Rev,
Items,
NULL as Semana
FROM
customer_service.tbl_base_48 BD
left join 
(select llamadas.*,gross_Ord,gross_Rev,
net_Ord,net_Rev,
Items from 
(select 
    event_date As Fecha,
		b.id as Intervalo,agent_name as Nombre,agent_identification,
		sum(net_event) Recibidas,	
		sum(answered_in_wh) Atendidas,
		sum(unanswered_in_wh) Abandonadas,
		sum(call_duration_seg) Duracion,
    sum(hold_duration_seg) Espera,
    sum(answered_in_wh_20) Atendidas20,
    sum(transfered_to_queue) Transferencia,
	  sum(good)/sum(answered_2) Satisfaccion
from
    customer_service_co.bi_ops_cc a
left join 
tbl_48_franjas b
on a.event_shift=b.Id_Alt
where
    pos = 0 and net_event = 1 and event_date>='2013-10-01'
group by event_date , event_shift,agent_name)llamadas
left JOIN
(
SELECT 
	Date,
	identification,
	agent_Name,
	Intervalo,
	COUNT(OrderNum) AS gross_Ord,
	SUM(grossRev) AS gross_Rev,
	SUM(Items)Items

FROM 
(	SELECT
		Date,
		identification,
		agent_Name,
		if (
		`minute`		<30
		,CONCAT(`hour`,'00'),CONCAT(`hour`,'30')) as Intervalo,
		OrderNum,
		sum(total_value)grossRev,
		count(ItemID)As Items
	FROM 
		customer_service.tbl_telesales_co
	WHERE
		OrderBeforeCan=1 and telesales=1
	GROUP BY
		Date,
		identification,
		agent_Name,
		if (		`minute`<30		,CONCAT(`hour`,'00'),CONCAT(`hour`,'30')),
		OrderNum
)A


GROUP BY
	date,
	Intervalo,
	agent_Name

)Gross

on llamadas.Fecha=Gross.Date and cast(llamadas.Intervalo as Char)=Gross.Intervalo
and llamadas.agent_identification=Gross.identification

left JOIN
(
SELECT 
	Date,
	identification,
	agent_Name,
	Intervalo,
	COUNT(OrderNum) AS net_Ord,
	SUM(netRev) AS net_Rev 

FROM 
(	
SELECT
 Date,		
 identification,

if (
		`minute`		<30
		,CONCAT(`hour`,'00'),CONCAT(`hour`,'30')) as Intervalo,
		agent_Name,
		OrderNum,
		SUM(total_value)netRev 
	FROM 
		customer_service.tbl_telesales_co
	WHERE
		 
OrderAfterCan=1
		AND telesales=1
	
	GROUP BY
		Date,
if (
		`minute`		<30
		,CONCAT(`hour`,'00'),CONCAT(`hour`,'30')),		
agent_Name,
identification,
		OrderNum
)A
GROUP BY
	Date,
	Intervalo,
agent_Name

)Net
on llamadas.Fecha=Net.Date and cast(llamadas.Intervalo as Char)=Net.Intervalo
and llamadas.agent_identification=Net.identification
)gral
on BD.Fecha=gral.Fecha and BD.id=gral.Intervalo
where BD.Fecha<date(CURDATE())
UNION ALL

SELECT bds.Fecha,
bds.Franja,
'MX' AS Pais,
Agente,
Recibidas,
	Atendidas,
	Abandonadas,
	Duracion,
	Espera,
	Atendidas20,
	Transferencia,
	Satisfaccion,
	gross_Ord,
	gross_Rev,
	net_Ord,
	net_Rev,
	Items,NULL as Semana

FROM
customer_service.tbl_base_48  bds

LEFT JOIN
(SELECT llamadas.Fecha,'MX' AS Pais,llamadas.Franja,Agente,Recibidas,
	Atendidas,
	Abandonadas,
	Duracion,
	Espera,
	Atendidas20,
	Transferencia,
	Satisfaccion,
	gross_Ord,
	gross_Rev,
	Items,
	net_Ord,
	net_Rev

FROM 

(SELECT
		a.Fecha,
	a.Franja,
	Agente,
	Recibidas,
	Atendidas,
	Abandonadas,
	Duracion,
	Espera,
	Transferencia,
	Atendidas20,
  FCR/Peticiones as Satisfaccion
FROM
	(
select Fecha,Franja,Agente,
sum(if (cola='Pre-Venta',Contador,0)) Recibidas,
			sum(if (cola='Pre-Venta',Atendida,0))+sum(if (cola='Pre-Venta',Transferencia,0)) Atendidas,
			sum(if (cola='Pre-Venta',Abandonada,0)) Abandonadas,
			sum(if (cola='Pre-Venta',Duracion,0)) Duracion,
			sum(if (cola='Pre-Venta',Transferencia,0)) Transferencia,
			sum(if (cola='Pre-Venta',Espera,0)) Espera,
			sum(if (cola='Pre-Venta' AND Espera<=20,Atendida,0))+sum(if (cola='Pre-Venta' AND Espera<=20,Transferencia,0)) Atendidas20,
			SUM(if (cola='Pre-Venta' ,FCR,0))FCR,
			COUNT(if (cola='Pre-Venta' ,question2,0))Peticiones
FROM
customer_service.tbl_queueMetrics30
GROUP BY Fecha,Franja,Agente)a  
)llamadas

LEFT JOIN

(#Gross
SELECT 
	Date,
	agent_Name,
	Franja,
	COUNT(OrderNUm) AS gross_Ord,
	SUM(grossRev) AS gross_Rev,
	SUM(Items)Items

FROM 
(	SELECT
		Date,
		agent_Name,
		if (
		DATE_FORMAT(created_at,'%i')		<30
		,CONCAT(DATE_FORMAT(created_at,'%H'),'00'),CONCAT(DATE_FORMAT(created_at,'%H'),'30')) as Intervalo,
		OrderNum,
		sum(Price)grossRev,
		count(ItemID)As Items
	FROM 
		tbl_sales_hist_ev
	GROUP BY
		Date,
		agent_Name,
		if (DATE_FORMAT(created_at,'%i')<30,CONCAT(DATE_FORMAT(created_at,'%H'),'00'),CONCAT(DATE_FORMAT(created_at,'%H'),'30')),
		OrderNum
 
)A
LEFT JOIN
customer_service.tbl_48_franjas B
ON cast(A.Intervalo as CHAR)=B.Id
GROUP BY
	date,
	Intervalo
,agent_Name
)Gross

ON llamadas.Fecha=Gross.Date 
and llamadas.Franja=cast(Gross.Franja as char)
AND llamadas.Agente=Gross.agent_Name

LEFT JOIN 

(#Net Sales
SELECT 
	Date,
	agent_Name,
	Franja,
	COUNT(OrderNum) AS net_Ord,
	SUM(netRev) AS net_Rev 

FROM 
(	SELECT
Date,		

if (DATE_FORMAT(created_at,'%i')<30,CONCAT(DATE_FORMAT(created_at,'%H'),'00'),CONCAT(DATE_FORMAT(created_at,'%H'),'30')) as Intervalo,
		agent_Name,
		OrderNum,
		SUM(Price)netRev 
	FROM 
		tbl_sales_hist_ev
	WHERE
		 
`Status` in ('delivered','shipped_and_stock_updated','closed','ready_to_ship','exported')
	GROUP BY
		date,
if (DATE_FORMAT(created_at,'%i')<30,CONCAT(DATE_FORMAT(created_at,'%H'),'00'),CONCAT(DATE_FORMAT(created_at,'%H'),'30')),		
agent_Name,
		OrderNum
)A
LEFT JOIN
customer_service.tbl_48_franjas B
ON cast(A.Intervalo as CHAR)=B.Id

GROUP BY
	Date,
	Intervalo,
agent_Name
)Net

ON llamadas.Fecha=Net.Date 
and llamadas.Franja=cast(Net.Franja as char)
AND llamadas.Agente=Net.agent_Name
)Gral
ON bds.Fecha=Gral.Fecha and bds.Franja=Gral.Franja
where bds.Fecha<date(CURDATE())
UNION ALL

SELECT
date(CURDATE()-1)as `Fecha`,
NULL as `Franja`,
'PE' as `Pais`,
NULL as `Nombre`,
NULL as `Recibidas`,
NULL as `Atendidas`,
NULL as `Abandonadas`,
NULL as `Duracion`,
NULL as `Espera`,
NULL as `Atendidas20`,
NULL as `Transferencia`,
NULL as `Satisfaccion`,
NULL as `gross_Ord`,
NULL as `gross_Rev`,
NULL as `net_Ord`,
NULL as `net_Rev`,
NULL as `Items`,
NULL as `Semana`

UNION ALL

SELECT
date(CURDATE()-1) as `Fecha`,
NULL as `Franja`,
'VE' as `Pais`,
NULL as `Nombre`,
NULL as `Recibidas`,
NULL as `Atendidas`,
NULL as `Abandonadas`,
NULL as `Duracion`,
NULL as `Espera`,
NULL as `Atendidas20`,
NULL as `Transferencia`,
NULL as `Satisfaccion`,
NULL as `gross_Ord`,
NULL as `gross_Rev`,
NULL as `net_Ord`,
NULL as `net_Rev`,
NULL as `Items`,
NULL as `Semana`

;
UPDATE rep_telesales_agt_regional  SET Semana=date_format(fecha,'%Y-%u');

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`eduardo.martinez`@`%`*/ /*!50003 PROCEDURE `sp_telesalesRegionalAjustado`()
BEGIN

DROP TABLE IF EXISTS tbl_sales_Hist_Consolidated;
CREATE TABLE tbl_sales_Hist_Consolidated
SELECT * FROM
tbl_sales_hist_ev
UNION ALL
SELECT * FROM
tbl_sales_hist_co;

SET @max_day = (	SELECT		max(Fecha)	FROM		customer_service.test_rep_telesales_agt_regional
WHERE Pais='CO');
SELECT 'Inicio';
DELETE FROM 	customer_service.test_rep_telesales_agt_regional WHERE	Fecha >=@max_day AND Pais='CO';

DROP TEMPORARY TABLE
IF EXISTS llamadasCO;

CREATE TEMPORARY TABLE llamadasCO (KEY(Fecha), KEY(Intervalo)) SELECT
	event_date AS Fecha,
	b.Franja AS Intervalo,
	agent_name AS Nombre,
	agent_identification,
	null AS Contador,
	null as Contador2,
	sum(net_event) Recibidas,
	sum(answered_in_wh) Atendidas,
	sum(unanswered_in_wh) Abandonadas,
	sum(call_duration_seg) Duracion,
	sum(hold_duration_seg) Espera,
	sum(agent_hold_time_seg) EsperaAgente,
	sum(answered_in_wh_20) Atendidas20,
	sum(transfered_to_queue) Transferencia,
	sum(good) Satisfaccion,
	sum(answered_3) Respuestas_Satisfaccion
FROM
	customer_service_co.bi_ops_cc a
LEFT JOIN tbl_48_franjas b ON a.event_shift = b.Id_Alt
WHERE
	pre = 1
AND net_event = 1
AND event_date >= '2013-05-01'
GROUP BY
	event_date,
	event_shift,
	agent_name
HAVING
	Fecha >=@max_day;

INSERT INTO test_rep_telesales_agt_regional 
SELECT
	a.Fecha,
	a.Franja,
	'CO' AS Pais,
	Nombre,
	agent_identification,
	Contador,
	Contador2,
	Recibidas,
	Atendidas,
	Abandonadas,
	Duracion,
	Espera,
	EsperaAgente,
	Atendidas20,
	Transferencia,
	Satisfaccion,
	Respuestas_Satisfaccion,
	NULL AS gross_Ord,
	NULL AS gross_Rev,
	NULL AS Items,
	NULL AS net_Ord,
	NULL AS net_Rev,
	NULL AS Tgross_orders,
	NULL AS Tnetorders,
	NULL AS TvalueGross,
	NULL AS TvalueNet,
	NULL as Semana
FROM
	customer_service.tbl_base_48 a
LEFT JOIN llamadasCO b ON a.Fecha = b.Fecha
AND a.Franja = b.Intervalo
WHERE
	a.Fecha < DATE(CURDATE())
AND a.Fecha >=@max_day;
SELECT 'Orders_CO';

#--------------------------Update Gross Orders co
#--------Tabla temporal gross co
DROP TEMPORARY TABLE IF EXISTS grosSalesCO;
CREATE TEMPORARY TABLE grosSalesCO(KEY(Date),KEY(identification),KEY(Intervalo))
SELECT 
	Date,
	identification,
	agent_Name,
	Intervalo,
	Franja,
	COUNT(OrderNum) AS gross_Ord,
	SUM(grossRev) AS gross_Rev,
	SUM(Items)Items

FROM 
(	SELECT
		Date,
		identification,
		agent_Name,
		if (
		minute(Time)		<30
		,CONCAT(hour(Time),'00'),CONCAT(hour(Time),'30')) as Intervalo,
		OrderNum,
		sum(Rev)grossRev,
		count(ItemID)As Items
	FROM 
		customer_service.tbl_sales_Hist_Consolidated
		WHERE
		OrderBeforeCan=1 AND Pais='CO'#and telesales=1
	GROUP BY
		Date,
		identification,
		agent_Name,
		if (minute(Time)<30		,CONCAT(hour(Time),'00'),CONCAT(hour(Time),'30')),
		OrderNum
)A
LEFT JOIN
		customer_service.tbl_48_franjas b
	ON A.Intervalo=b.Id
	
WHERE Date>='2013-05-01'
GROUP BY
	date,
	Intervalo,
	agent_Name
;


#-------------Update gross
#SELECT * FROM
UPDATE customer_service.test_rep_telesales_agt_regional a
LEFT JOIN 
grosSalesCO b
ON a.Fecha=b.date AND a.Franja=b.Franja AND a.agent_identification=b.identification
SET a.gross_Ord=b.gross_Ord , a.gross_Rev=b.gross_Rev , a.Items=b.Items
WHERE Pais='CO';

#-------------------------------Update Net CO 
#----------------Temporal NEt

DROP TEMPORARY TABLE IF EXISTS netSalesCO;
CREATE TEMPORARY TABLE netSalesCO(KEY(Date),KEY(identification),KEY(Intervalo))
SELECT 
	Date,
	identification,
	agent_Name,
	Franja,
	Intervalo,
	COUNT(OrderNum) AS net_Ord,
	SUM(netRev) AS net_Rev 

FROM 
(	
SELECT
 case when DateCollected = '0000-00-00' then date else Datecollected end as Date,		
 identification,

if (
		minute(Time)		<30
		,CONCAT(hour(Time),'00'),CONCAT(hour(Time),'30')) as Intervalo,
		agent_Name,
		OrderNum,
		SUM(Rev)netRev 
	FROM 
		customer_service.tbl_sales_Hist_Consolidated
	WHERE
		 
OrderAfterCan=1 AND Pais='CO'
		#AND telesales=1
	
	GROUP BY
		Date,
if (
		minute(Time)		<30
		,CONCAT(hour(Time),'00'),CONCAT(hour(Time),'30')),		
agent_Name,
identification,
		OrderNum
)A
LEFT JOIN
		customer_service.tbl_48_franjas b
	ON A.Intervalo=b.Id


GROUP BY
	Date,
	Intervalo,
agent_Name
;

#---------Update CO
UPDATE customer_service.test_rep_telesales_agt_regional a
LEFT JOIN 
netSalesCO b
ON a.Fecha=b.date AND a.Franja=b.Franja AND a.agent_identification=b.identification
SET a.net_Ord=b.net_Ord , a.net_Rev=b.net_Rev 
WHERE Pais='CO';


#---------------Total Ords CO
DROP TEMPORARY TABLE IF EXISTS totalOrders;
CREATE TEMPORARY TABLE totalOrders(KEY(Date))
SELECT
	date,'1' as Contador,
	count(DISTINCT(OrderNum)) Tgross_orders, sum(Rev) Value
FROM
	development_co_project.A_Master
WHERE
	OrderBeforeCan = 1
AND date >= '2014-01-01'
GROUP BY
	date/*,
	cast(
		(

			IF (
				MINUTE (HOUR) < 30,
				concat(HOUR(HOUR), ':', '00'),
				concat(HOUR(HOUR), ':', '30')
			)
		) AS time
	)*/
;

#---Update Total orders co
UPDATE customer_service.test_rep_telesales_agt_regional a
LEFT JOIN 
totalOrders b
ON a.Fecha=b.date #AND a.Franja=b.Franja 
SET a.Tgross_orders=b.Tgross_orders , a.Contador=b.Contador,
a.TvalueGross=b.Value
WHERE Pais='CO';

#---------------Total Ords Net CO
DROP TEMPORARY TABLE IF EXISTS totalOrders;
CREATE TEMPORARY TABLE totalOrders(KEY(Date))
SELECT
	DateCollected date,
	'1' as Contador,
	count(DISTINCT(OrderNum)) Tnet_orders, sum(Rev) ValueNet
FROM
	development_co_project.A_Master
WHERE
	OrderAfterCan = 1
AND DateCollected >= '2014-01-01'
GROUP BY
	date/*,
	cast(
		(

			IF (
				MINUTE (HOUR) < 30,
				concat(HOUR(HOUR), ':', '00'),
				concat(HOUR(HOUR), ':', '30')
			)
		) AS time
	)*/
;
#---Update Total orders co
UPDATE customer_service.test_rep_telesales_agt_regional a
LEFT JOIN 
totalOrders b
ON a.Fecha=b.date #AND a.Franja=b.Franja 
SET a.Tnetorders=b.Tnet_orders , a.Contador2=b.Contador,
a.TvalueNet=b.ValueNet
WHERE Pais='CO';


###################################################
#################------México-----#################
#################-----------------#################
###################################################

SET @max_dayMX = (	SELECT		max(Fecha)	FROM		customer_service.test_rep_telesales_agt_regional
WHERE Pais='MX');
SELECT 'Orders_MX';
DELETE FROM 	customer_service.test_rep_telesales_agt_regional WHERE	Fecha >=@max_dayMX
AND Pais='MX';


DROP TEMPORARY TABLE IF EXISTS llamadasMx;
CREATE TEMPORARY TABLE llamadasMx (KEY(Fecha), KEY(Franja))
SELECT
		a.Fecha,
	a.Franja,
	Agente AS Nombre,
	NULL as agent_identification,
	null AS Contador,
	null as Contador2,
	Recibidas,
	Atendidas,
	Abandonadas,
	Duracion,
	Espera,
	EsperaAgente,
	Transferencia,
	Atendidas20,
	FCR as Satisfaccion,
	Peticiones as Respuestas_Satisfaccion
FROM
	(
select Fecha,Franja,Agente,
sum(if (cola='Pre-Venta',Contador,0)) Recibidas,
			sum(if (cola='Pre-Venta',Atendida,0))+sum(if (cola='Pre-Venta',Transferencia,0)) Atendidas,
			sum(if (cola='Pre-Venta',Abandonada,0)) Abandonadas,
			sum(if (cola='Pre-Venta',Duracion,0)) Duracion,
			sum(if (cola='Pre-Venta',Transferencia,0)) Transferencia,
			sum(if (cola='Post-Venta',TiempoEnCola,0)) Espera,
			sum(if (cola='Post-Venta',Espera,0)) EsperaAgente,
			sum(if (cola='Pre-Venta' AND Espera<=20,Atendida,0))+sum(if (cola='Pre-Venta' AND Espera<=20,Transferencia,0)) Atendidas20,
			SUM(if (cola='Pre-Venta' ,FCR,0))FCR,
			COUNT(if (cola='Pre-Venta' ,question2,0))Peticiones
FROM
customer_service.tbl_queueMetrics30
GROUP BY Fecha,Franja,Agente)a  
HAVING Fecha >=@max_dayMX
;

INSERT INTO test_rep_telesales_agt_regional 
SELECT
	a.Fecha,
	a.Franja,
	'MX' AS Pais,
	Nombre,
	agent_identification,
	Contador,
	Contador2,
	Recibidas,
	Atendidas,
	Abandonadas,
	Duracion,
	Espera,
	EsperaAgente,
	Atendidas20,
	Transferencia,
	Satisfaccion,
	Respuestas_Satisfaccion,
	NULL AS gross_Ord,
	NULL AS gross_Rev,
	NULL AS Items,
	NULL AS net_Ord,
	NULL AS net_Rev,
	NULL AS Tgross_orders,
	NULL AS Tnetorders,
	NULL AS TvalueGross,
	NULL AS TvalueNet,
	NULL AS Semana
FROM
	tbl_base_48 a
LEFT JOIN llamadasMx b ON a.Fecha = b.Fecha
AND a.Franja = b.Franja
WHERE
	a.Fecha < DATE(CURDATE())
AND a.Fecha >=@max_dayMX
;

##GRoss Orders MX
DROP TEMPORARY TABLE IF EXISTS grosSalesMX;
CREATE TEMPORARY TABLE grosSalesMX(KEY(Date),KEY(agent_Name),KEY(Franja))
SELECT 
	Date,
	agent_Name,
	Franja,
	COUNT(OrderNUm) AS gross_Ord,
	SUM(grossRev) AS gross_Rev,
	SUM(Items)Items

FROM 
(	SELECT
		Date,
		agent_Name,
		if (
		DATE_FORMAT(Time,'%i')		<30
		,CONCAT(DATE_FORMAT(Time,'%H'),'00'),CONCAT(DATE_FORMAT(Time,'%H'),'30')) as Intervalo,
		OrderNum,
		sum(Rev)grossRev,
		count(ItemID)As Items
	FROM 
		customer_service.tbl_sales_Hist_Consolidated
	WHERE OrderBeforeCan=1 AND Pais='MX'
	GROUP BY
		Date,
		agent_Name,
		if (DATE_FORMAT(Time,'%i')<30,CONCAT(DATE_FORMAT(Time,'%H'),'00'),CONCAT(DATE_FORMAT(Time,'%H'),'30')),
		OrderNum
 
)A
LEFT JOIN
customer_service.tbl_48_franjas B
ON cast(A.Intervalo as CHAR)=B.Id
GROUP BY
	date,
	Intervalo
,agent_Name
;

#-------------Update gross
#SELECT * FROM
UPDATE customer_service.test_rep_telesales_agt_regional a
LEFT JOIN 
grosSalesMX b
ON a.Fecha=b.date AND a.Franja=b.Franja AND a.Nombre=b.agent_Name
SET a.gross_Ord=b.gross_Ord , a.gross_Rev=b.gross_Rev , a.Items=b.Items
WHERE Pais='MX';

#3############Net Sales mx

DROP TEMPORARY TABLE IF EXISTS netSalesMX;
CREATE TEMPORARY TABLE netSalesMX(KEY(Date),KEY(agent_Name),KEY(Franja))
SELECT 
	Date,
	agent_Name,
	Franja,
	COUNT(OrderNum) AS net_Ord,
	SUM(netRev) AS net_Rev 

FROM 
(	SELECT
case when DateCollected = '0000-00-00' then date else Datecollected end  As Date,		

if (DATE_FORMAT(Time,'%i')<30,CONCAT(DATE_FORMAT(Time,'%H'),'00'),CONCAT(DATE_FORMAT(Time,'%H'),'30')) as Intervalo,
		agent_Name,
		OrderNum,
		SUM(Rev)netRev 
	FROM 
		tbl_sales_Hist_Consolidated
	WHERE
		 OrderAfterCan=1 AND Pais='MX'
	GROUP BY
		date,
if (DATE_FORMAT(Time,'%i')<30,CONCAT(DATE_FORMAT(Time,'%H'),'00'),
CONCAT(DATE_FORMAT(Time,'%H'),'30')),		
agent_Name,
		OrderNum
)A
LEFT JOIN
customer_service.tbl_48_franjas B
ON cast(A.Intervalo as CHAR)=B.Id

GROUP BY
	Date,
	Intervalo,
agent_Name;

#---------Update CO
UPDATE 
#SELECT * FROM
customer_service.test_rep_telesales_agt_regional a
LEFT JOIN 
netSalesMX b
ON a.Fecha=b.date AND a.Franja=b.Franja AND a.Nombre=b.agent_Name
SET a.net_Ord=b.net_Ord , a.net_Rev=b.net_Rev 
WHERE Pais='MX';

#########
#Gross Orders Mx
DROP TEMPORARY TABLE IF EXISTS totalOrdersMX;
CREATE TEMPORARY TABLE totalOrdersMX(KEY(Date))
SELECT
	date,'1' as Contador,
	count(DISTINCT(OrderNum)) Tgross_orders,
	sum(Rev) Value
FROM
	development_mx.A_Master
WHERE
	OrderBeforeCan = 1
AND date >= '2014-01-01'
GROUP BY
	date
/*,
	cast(
		(

			IF (
				MINUTE (HOUR) < 30,
				concat(HOUR(HOUR), ':', '00'),
				concat(HOUR(HOUR), ':', '30')
			)
		) AS time
	)*/
;

SELECT 'Inter';
UPDATE
#SELECT * FROM 
customer_service.test_rep_telesales_agt_regional a
LEFT JOIN 
totalOrdersMX b
ON a.Fecha=b.date #AND a.Franja=b.Franja 
SET a.Tgross_orders=b.Tgross_orders , a.Contador=b.Contador,
a.TvalueGross=b.Value
WHERE Pais='MX';

#########
#Net Orders Mx
DROP TEMPORARY TABLE IF EXISTS totalOrdersMX;
CREATE TEMPORARY TABLE totalOrdersMX(KEY(Date))
SELECT
	DateCollected date,'1' as Contador,
	count(DISTINCT(OrderNum)) Tnet_orders,
	sum(Rev) ValueNet
FROM
	development_mx.A_Master
WHERE
	OrderAfterCan = 1
AND DateCollected >= '2014-01-01'
GROUP BY
	DateCollected
/*,
	cast(
		(

			IF (
				MINUTE (HOUR) < 30,
				concat(HOUR(HOUR), ':', '00'),
				concat(HOUR(HOUR), ':', '30')
			)
		) AS time
	)*/
;


UPDATE
#SELECT * FROM 
customer_service.test_rep_telesales_agt_regional a
LEFT JOIN 
totalOrdersMX b
ON a.Fecha=b.date #AND a.Franja=b.Franja 
SET a.Tnetorders=b.Tnet_orders , a.Contador2=b.Contador,
a.TvalueNet=b.ValueNet
WHERE Pais='MX';

UPDATE test_rep_telesales_agt_regional  SET Semana=date_format(fecha,'%Y-%u');

SELECT 'Fin';

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`auto.reports`@`%`*/ /*!50003 PROCEDURE `update_clazend`()
BEGIN
#
#
	delete from tbl_clasificacion_zendesk;
	insert into tbl_clasificacion_zendesk
	SELECT
		substring(Clasificacion, 1, 3) Clasificacion,
		substring(
			CAST(Created_at AS CHAR),
			1,
			11
		) AS Dia,
		'Pre-Venta::Dudas Proceso Compras' AS Clase,
		count(summation_column) AS Total
	FROM
		tbl_zendesk
	WHERE
		Clasificacion LIKE 'Pre-Venta:: Dudas Proceso Compras::%'
	AND created_at >= '2013-06-01'
	GROUP BY
		substring(Clasificacion, 1, 3),
		substring(
			CAST(Created_at AS CHAR),
			1,
			11
		),
		'Pre-Venta::Dudas Proceso Compras'
	UNION ALL
		SELECT
			substring(Clasificacion, 1, 3) Clasificacion,
			substring(
				CAST(Created_at AS CHAR),
				1,
				11
			) AS Dia,
			'Pre-Venta:: Dudas Productos' AS Clase,
			count(summation_column) AS Total
		FROM
			tbl_zendesk
		WHERE
			Clasificacion LIKE 'Pre-Venta:: Dudas Productos::%'
		AND created_at >= '2013-06-01'
		GROUP BY
			substring(Clasificacion, 1, 3),
			substring(
				CAST(Created_at AS CHAR),
				1,
				11
			),
			'Pre-Venta:: Dudas Productos'
		UNION ALL
			SELECT
				substring(Clasificacion, 1, 3) Clasificacion,
				substring(
					CAST(Created_at AS CHAR),
					1,
					11
				) AS Dia,
				'Pre-Venta:: Dudas Politicas' AS Clase,
				count(summation_column) AS Total
			FROM
				tbl_zendesk
			WHERE
				Clasificacion LIKE 'Pre-Venta:: Dudas Politicas::%'
			AND created_at >= '2013-06-01'
			GROUP BY
				substring(Clasificacion, 1, 3),
				substring(
					CAST(Created_at AS CHAR),
					1,
					11
				),
				'Pre-Venta:: Dudas Politicas'
			UNION ALL
				SELECT
					substring(Clasificacion, 1, 3) Clasificacion,
					substring(
						CAST(Created_at AS CHAR),
						1,
						11
					) AS Dia,
					'Pre-Venta:: Dudas Promociones' AS Clase,
					count(summation_column) AS Total
				FROM
					tbl_zendesk
				WHERE
					Clasificacion LIKE 'Pre-Venta:: Dudas Promociones::%'
				AND created_at >= '2013-06-01'
				GROUP BY
					substring(Clasificacion, 1, 3),
					substring(
						CAST(Created_at AS CHAR),
						1,
						11
					),
					'Pre-Venta:: Dudas Promociones'
				UNION ALL
					SELECT
						substring(Clasificacion, 1, 3) Clasificacion,
						substring(
							CAST(Created_at AS CHAR),
							1,
							11
						) AS Dia,
						'Pre-Venta:: Ventas de Mayoreo' AS Clase,
						count(summation_column) AS Total
					FROM
						tbl_zendesk
					WHERE
						Clasificacion LIKE 'Pre-Venta:: Ventas de Mayoreo%'
					AND created_at >= '2013-06-01'
					GROUP BY
						substring(Clasificacion, 1, 3),
						substring(
							CAST(Created_at AS CHAR),
							1,
							11
						),
						'Pre-Venta:: Ventas de Mayoreo'
					UNION ALL
						SELECT
							substring(Clasificacion, 1, 3) Clasificacion,
							substring(
								CAST(Created_at AS CHAR),
								1,
								11
							) AS Dia,
							'Outbound:: Invalids' AS Clase,
							count(summation_column) AS Total
						FROM
							tbl_zendesk
						WHERE
							Clasificacion LIKE 'Outbound:: Invalids%'
						AND created_at >= '2013-06-01'
						GROUP BY
							substring(Clasificacion, 1, 3),
							substring(
								CAST(Created_at AS CHAR),
								1,
								11
							),
							'Outbound:: Invalids'
						UNION ALL
							SELECT
								substring(Clasificacion, 1, 3) Clasificacion,
								substring(
									CAST(Created_at AS CHAR),
									1,
									11
								) AS Dia,
								'Outbound:: COD/CCOD' AS Clase,
								count(summation_column) AS Total
							FROM
								tbl_zendesk
							WHERE
								Clasificacion LIKE 'Outbound:: COD/CCOD%'
							AND created_at >= '2013-06-01'
							GROUP BY
								substring(Clasificacion, 1, 3),
								substring(
									CAST(Created_at AS CHAR),
									1,
									11
								),
								'Outbound:: COD/CCOD'
							UNION ALL
								SELECT
									substring(Clasificacion, 1, 3) Clasificacion,
									substring(
										CAST(Created_at AS CHAR),
										1,
										11
									) AS Dia,
									'Post-Venta:: Dudas de Estatus de Pedido' AS Clase,
									count(summation_column) AS Total
								FROM
									tbl_zendesk
								WHERE
									Clasificacion LIKE 'Post-Venta:: Dudas de Estatus de Pedido%'
								AND created_at >= '2013-06-01'
								GROUP BY
									substring(Clasificacion, 1, 3),
									substring(
										CAST(Created_at AS CHAR),
										1,
										11
									),
									'Post-Venta:: Dudas de Estatus de Pedido'
								UNION ALL
									SELECT
										substring(Clasificacion, 1, 3) Clasificacion,
										substring(
											CAST(Created_at AS CHAR),
											1,
											11
										) AS Dia,
										'Post-Venta:: Cancelacion' AS Clase,
										count(summation_column) AS Total
									FROM
										tbl_zendesk
									WHERE
										Clasificacion LIKE 'Post-Venta:: Cancelacion%'
									AND created_at >= '2013-06-01'
									GROUP BY
										substring(Clasificacion, 1, 3),
										substring(
											CAST(Created_at AS CHAR),
											1,
											11
										),
										'Post-Venta:: Cancelacion'
									UNION ALL
										SELECT
											substring(Clasificacion, 1, 3) Clasificacion,
											substring(
												CAST(Created_at AS CHAR),
												1,
												11
											) AS Dia,
											'Post-Venta:: Devoluciones' AS Clase,
											count(summation_column) AS Total
										FROM
											tbl_zendesk
										WHERE
											Clasificacion LIKE 'Post-Venta:: Devoluciones::%'
										AND created_at >= '2013-06-01'
										GROUP BY
											substring(Clasificacion, 1, 3),
											substring(
												CAST(Created_at AS CHAR),
												1,
												11
											),
											'Post-Venta:: Devoluciones'
										UNION ALL
											SELECT
												substring(Clasificacion, 1, 3) Clasificacion,
												substring(
													CAST(Created_at AS CHAR),
													1,
													11
												) AS Dia,
												'Post-Venta:: Devoluciones' AS Clase,
												count(summation_column) AS Total
											FROM
												tbl_zendesk
											WHERE
												Clasificacion LIKE 'Post-Venta:: Devoluciones::%'
											AND created_at >= '2013-06-01'
											GROUP BY
												substring(Clasificacion, 1, 3),
												substring(
													CAST(Created_at AS CHAR),
													1,
													11
												),
												'Post-Venta:: Devoluciones'
											UNION ALL
												SELECT
													substring(Clasificacion, 1, 3) Clasificacion,
													substring(
														CAST(Created_at AS CHAR),
														1,
														11
													) AS Dia,
													'Post-Venta::Otros' AS Clase,
													count(summation_column) AS Total
												FROM
													tbl_zendesk
												WHERE
													Clasificacion LIKE 'Post-Venta:: Compra Invalida::%'
												AND Clasificacion LIKE 'Post-Venta:: Entrega Incompleta%'
												AND Clasificacion LIKE 'Post-Venta:: Facturaci%'
												AND Clasificacion LIKE 'Post-Venta:: Reembolso%'
												AND Clasificacion LIKE 'Post-Venta:: Stockout%'
												AND Clasificacion LIKE 'Post-Venta:: Validacion de Datos%'
												AND created_at >= '2013-06-01'
												GROUP BY
													substring(Clasificacion, 1, 3),
													substring(
														CAST(Created_at AS CHAR),
														1,
														11
													),
													'Post-Venta::Otros';

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`auto.reports`@`%`*/ /*!50003 PROCEDURE `update_clazend_copy`()
BEGIN
#
#
	delete from tbl_clasificacion_zendesk;
	insert into tbl_clasificacion_zendesk
	SELECT
		substring(Clasificacion, 1, 3) Clasificacion,
		substring(
			CAST(Created_at AS CHAR),
			1,
			11
		) AS Dia,
		'Pre-Venta::Dudas Proceso Compras' AS Clase,
		count(summation_column) AS Total
	FROM
		tbl_zendesk
	WHERE
		Clasificacion LIKE 'Pre-Venta:: Dudas Proceso Compras::%'
	AND created_at >= '2013-06-01'
	GROUP BY
		substring(Clasificacion, 1, 3),
		substring(
			CAST(Created_at AS CHAR),
			1,
			11
		),
		'Pre-Venta::Dudas Proceso Compras'
	UNION ALL
		SELECT
			substring(Clasificacion, 1, 3) Clasificacion,
			substring(
				CAST(Created_at AS CHAR),
				1,
				11
			) AS Dia,
			'Pre-Venta:: Dudas Productos' AS Clase,
			count(summation_column) AS Total
		FROM
			tbl_zendesk
		WHERE
			Clasificacion LIKE 'Pre-Venta:: Dudas Productos::%'
		AND created_at >= '2013-06-01'
		GROUP BY
			substring(Clasificacion, 1, 3),
			substring(
				CAST(Created_at AS CHAR),
				1,
				11
			),
			'Pre-Venta:: Dudas Productos'
		UNION ALL
			SELECT
				substring(Clasificacion, 1, 3) Clasificacion,
				substring(
					CAST(Created_at AS CHAR),
					1,
					11
				) AS Dia,
				'Pre-Venta:: Dudas Politicas' AS Clase,
				count(summation_column) AS Total
			FROM
				tbl_zendesk
			WHERE
				Clasificacion LIKE 'Pre-Venta:: Dudas Politicas::%'
			AND created_at >= '2013-06-01'
			GROUP BY
				substring(Clasificacion, 1, 3),
				substring(
					CAST(Created_at AS CHAR),
					1,
					11
				),
				'Pre-Venta:: Dudas Politicas'
			UNION ALL
				SELECT
					substring(Clasificacion, 1, 3) Clasificacion,
					substring(
						CAST(Created_at AS CHAR),
						1,
						11
					) AS Dia,
					'Pre-Venta:: Dudas Promociones' AS Clase,
					count(summation_column) AS Total
				FROM
					tbl_zendesk
				WHERE
					Clasificacion LIKE 'Pre-Venta:: Dudas Promociones::%'
				AND created_at >= '2013-06-01'
				GROUP BY
					substring(Clasificacion, 1, 3),
					substring(
						CAST(Created_at AS CHAR),
						1,
						11
					),
					'Pre-Venta:: Dudas Promociones'
				UNION ALL
					SELECT
						substring(Clasificacion, 1, 3) Clasificacion,
						substring(
							CAST(Created_at AS CHAR),
							1,
							11
						) AS Dia,
						'Pre-Venta:: Ventas de Mayoreo' AS Clase,
						count(summation_column) AS Total
					FROM
						tbl_zendesk
					WHERE
						Clasificacion LIKE 'Pre-Venta:: Ventas de Mayoreo%'
					AND created_at >= '2013-06-01'
					GROUP BY
						substring(Clasificacion, 1, 3),
						substring(
							CAST(Created_at AS CHAR),
							1,
							11
						),
						'Pre-Venta:: Ventas de Mayoreo'
					UNION ALL
						SELECT
							substring(Clasificacion, 1, 3) Clasificacion,
							substring(
								CAST(Created_at AS CHAR),
								1,
								11
							) AS Dia,
							'Outbound:: Invalids' AS Clase,
							count(summation_column) AS Total
						FROM
							tbl_zendesk
						WHERE
							Clasificacion LIKE 'Outbound:: Invalids%'
						AND created_at >= '2013-06-01'
						GROUP BY
							substring(Clasificacion, 1, 3),
							substring(
								CAST(Created_at AS CHAR),
								1,
								11
							),
							'Outbound:: Invalids'
						UNION ALL
							SELECT
								substring(Clasificacion, 1, 3) Clasificacion,
								substring(
									CAST(Created_at AS CHAR),
									1,
									11
								) AS Dia,
								'Outbound:: COD/CCOD' AS Clase,
								count(summation_column) AS Total
							FROM
								tbl_zendesk
							WHERE
								Clasificacion LIKE 'Outbound:: COD/CCOD%'
							AND created_at >= '2013-06-01'
							GROUP BY
								substring(Clasificacion, 1, 3),
								substring(
									CAST(Created_at AS CHAR),
									1,
									11
								),
								'Outbound:: COD/CCOD'
							UNION ALL
								SELECT
									substring(Clasificacion, 1, 3) Clasificacion,
									substring(
										CAST(Created_at AS CHAR),
										1,
										11
									) AS Dia,
									'Post-Venta:: Dudas de Estatus de Pedido' AS Clase,
									count(summation_column) AS Total
								FROM
									tbl_zendesk
								WHERE
									Clasificacion LIKE 'Post-Venta:: Dudas de Estatus de Pedido%'
								AND created_at >= '2013-06-01'
								GROUP BY
									substring(Clasificacion, 1, 3),
									substring(
										CAST(Created_at AS CHAR),
										1,
										11
									),
									'Post-Venta:: Dudas de Estatus de Pedido'
								UNION ALL
									SELECT
										substring(Clasificacion, 1, 3) Clasificacion,
										substring(
											CAST(Created_at AS CHAR),
											1,
											11
										) AS Dia,
										'Post-Venta:: Cancelacion' AS Clase,
										count(summation_column) AS Total
									FROM
										tbl_zendesk
									WHERE
										Clasificacion LIKE 'Post-Venta:: Cancelacion%'
									AND created_at >= '2013-06-01'
									GROUP BY
										substring(Clasificacion, 1, 3),
										substring(
											CAST(Created_at AS CHAR),
											1,
											11
										),
										'Post-Venta:: Cancelacion'
									UNION ALL
										SELECT
											substring(Clasificacion, 1, 3) Clasificacion,
											substring(
												CAST(Created_at AS CHAR),
												1,
												11
											) AS Dia,
											'Post-Venta:: Devoluciones' AS Clase,
											count(summation_column) AS Total
										FROM
											tbl_zendesk
										WHERE
											Clasificacion LIKE 'Post-Venta:: Devoluciones::%'
										AND created_at >= '2013-06-01'
										GROUP BY
											substring(Clasificacion, 1, 3),
											substring(
												CAST(Created_at AS CHAR),
												1,
												11
											),
											'Post-Venta:: Devoluciones'
										UNION ALL
											SELECT
												substring(Clasificacion, 1, 3) Clasificacion,
												substring(
													CAST(Created_at AS CHAR),
													1,
													11
												) AS Dia,
												'Post-Venta:: Devoluciones' AS Clase,
												count(summation_column) AS Total
											FROM
												tbl_zendesk
											WHERE
												Clasificacion LIKE 'Post-Venta:: Devoluciones::%'
											AND created_at >= '2013-06-01'
											GROUP BY
												substring(Clasificacion, 1, 3),
												substring(
													CAST(Created_at AS CHAR),
													1,
													11
												),
												'Post-Venta:: Devoluciones'
											UNION ALL
												SELECT
													substring(Clasificacion, 1, 3) Clasificacion,
													substring(
														CAST(Created_at AS CHAR),
														1,
														11
													) AS Dia,
													'Post-Venta::Otros' AS Clase,
													count(summation_column) AS Total
												FROM
													tbl_zendesk
												WHERE
													Clasificacion LIKE 'Post-Venta:: Compra Invalida::%'
												AND Clasificacion LIKE 'Post-Venta:: Entrega Incompleta%'
												AND Clasificacion LIKE 'Post-Venta:: Facturaci%'
												AND Clasificacion LIKE 'Post-Venta:: Reembolso%'
												AND Clasificacion LIKE 'Post-Venta:: Stockout%'
												AND Clasificacion LIKE 'Post-Venta:: Validacion de Datos%'
												AND created_at >= '2013-06-01'
												GROUP BY
													substring(Clasificacion, 1, 3),
													substring(
														CAST(Created_at AS CHAR),
														1,
														11
													),
													'Post-Venta::Otros';

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`auto.reports`@`%`*/ /*!50003 PROCEDURE `update_customer_service_data`()
BEGIN
	# prepare the data for the report
	#CALL update_survery;
	#CALL update_clazend;
	#CALL update_general;
	#Reporte Regional
	CALL sp_boCOadjust;
	CALL sp_telesalesCo;
	CALL sp_sales_Report_co;
	#CALL sp_backoffRegional;
	#CALL sp_backoffRegionalAgt;
	CALL sp_chatRegional;
	CALL sp_chatRegionalAgt;
	CALL sp_emailRegional;
	CALL sp_emailRegionalAgt;
	CALL sp_llamadasRegional;
	CALL sp_llamadasRegionalAgt;
	CALL sp_telesalesRegionalAjustado;
	CALL sp_telesalesRegional;
	CALL sp_telesalesRegionalAgt;

	# report log, will be read for the Master Macro
		INSERT INTO production.table_monitoring_log (
  country,
  table_name,
  step,
  updated_at,
  key_date,
  total_rows,
  total_rows_check)
SELECT
  'Mexico',
  'CC_2R_routines',
  'finish',
  NOW(),
  max(Fecha),
  count(*),
  count(Fecha)
FROM
  customer_service.rep_telesales_agt_regional;





END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`auto.reports`@`%`*/ /*!50003 PROCEDURE `update_general`()
begin

	delete from tbl_general_cc;
	INSERT INTO tbl_general_cc /*Llamadas*/

	SELECT
		date_format(bd.fecha,'%Y-%u') AS WEEK,
		bd.Fecha,
		bd.Franja,
		Recibidos AS Mails_Recibidos,
		Resueltos AS Mails_Resueltos,
		Tiemporespuesta AS Tiempo_Respuesta_Mail,
		TiempoFR AS Tiempo_Primera_Respuesta,
		RecibidasPre AS Llamadas_RecibidasPre,
		RecibidasPost AS Llamadas_RecibidasPost,
		AtendidasPre AS Llamadas_AtendidasPre,
		AtendidasPost AS Llamadas_AtendidasPost,
		AbandonadasPre AS Llamadas_AbandonadasPre,
		AbandonadasPost AS Llamadas_AbandonadasPost,
		DuracionPre AS TMO_LlamadasPre,
		DuracionPost AS TMO_LlamadasPost,
		EsperaPre AS TME_LlamadasPre,
		EsperaPost AS TME_LlamadasPost,
		Atendidads20Pre AS Atendida_ante_de_20Pre,
		Atendidads20Post AS Atendida_ante_de_20Post,
		Recibido AS Chat_Recibidos,
		(Recibido - Abandono) Chats_Atendidos,
		Abandono AS Chat_Abandonados,
		Tespera AS TME_Chat,
		Tduracion AS TMO_Chats,
		Ventas_Totales,
		NULL as Ventas_Llamada,
		NULL as Ventas_Chat,
		NULL as Ventas_Mail,
		Resueltos_mismo_dia,
		(
			postventa / (postventa + preventa)
		) AS PostVenta,
		(
			preventa / (postventa + preventa)
		) AS PreVenta,
		Satisfaccion,
		Chats_Bandeja,
		Chats_B_Resueltos,
		Chat_B_Tiemporespuesta,
		Chat_B_TiempoFR,null as Dia_Semana
	,Net_Orders as Gross_Orders
	,(((preventa / (postventa + preventa))*(AtendidasPre+AtendidasPost)))/Net_Orders as Contact_Ratio_Pre
	,(((postventa / (postventa + preventa))*(AtendidasPre+AtendidasPost)))/Net_Orders as Contact_Ratio_Pos
	,ChatAtn20
	FROM
		tbl_base_dias bd
	LEFT JOIN 
	(
		SELECT
			substring(
				CAST(Created_at AS CHAR),
				1,
				11
			) AS Dia,
			cast(
				DATE_FORMAT(Created_at, '%H') AS CHAR
			) hr,
			count(summation_column) AS Recibidos,
			sum(if(Resolution_Time<>'0000-00-00',1,0)) Resueltos,
			sum(Resolution_Time) Tiemporespuesta,
			sum(
				(
					First_reply_time_in_minutes / 60
				)
			) TiempoFR
		FROM
			tbl_zendesk
		WHERE
			Via='Mail' and Group_ in ('Front Office Email','Front Office','Front Office - CHAT')
		AND Created_at >= '2013-05-01'
		GROUP BY
			substring(
				CAST(Created_at AS CHAR),
				1,
				11
			),
			DATE_FORMAT(Created_at, '%H')
	)
	email #
	ON bd.Fecha = email.dia
	AND bd.Hora = email.hr
	LEFT JOIN 
	(
		SELECT
			a.Fecha,
			CASE 1
		WHEN length(a.Hora) = 1 THEN
			concat('0', a.Hora)
		ELSE
			a.Hora
		END AS hr,
		RecibidasPre,RecibidasPost,
		AtendidasPre,AtendidasPost,
		AbandonadasPre,AbandonadasPost,
		DuracionPre,DuracionPost,
		EsperaPre,EsperaPost,
		Atendidads20Pre,Atendidads20Post
	FROM
		(
			SELECT
				Fecha,
				Hora,
				sum(if (cola='Pre-Venta',Contador,
	0)) RecibidasPre,
				sum(if (cola='Post-Venta',Contador,0)) RecibidasPost,
				sum(if (cola='Pre-Venta',Atendida,0)) AtendidasPre,
				sum(if (cola='Post-Venta',Atendida,0)) AtendidasPost,
				sum(if (cola='Pre-Venta',Abandonada,0)) AbandonadasPre,
				sum(if (cola='Post-Venta',Abandonada,0)) AbandonadasPost,
				sum(if (cola='Pre-Venta',Duracion,0)) DuracionPre,
				sum(if (cola='Post-Venta',Duracion,0)) DuracionPost,
				sum(if (cola='Pre-Venta',Espera,0)) EsperaPre,
				sum(if (cola='Post-Venta',Espera,0)) EsperaPost
			FROM
				customer_service.tbl_queueMetricsAuto
			GROUP BY
				Fecha,
				Hora
		) a
	LEFT JOIN (
		SELECT
			Fecha,
			Hora,
			sum(if(cola='Pre-Venta',Atendida,0))+sum(if (cola='Pre-Venta',Transferencia,0)) Atendidads20Pre,
			sum(if(cola='Post-Venta',Atendida,0))+sum(if(cola='Post-Venta',Transferencia,0)) Atendidads20Post
		FROM
			customer_service.tbl_queueMetricsAuto
		WHERE
			Espera <=20
		GROUP BY
			Fecha,
			Hora
	) b ON a.Fecha = b.Fecha
	AND a.Hora = b.Hora
	)
	llamadas 
	ON bd.fecha = llamadas.Fecha
	AND bd.hora = llamadas.hr



	LEFT JOIN 
	(
	SELECT a.Fecha,a.hora,count(a.Ventas) as Ventas_Totales
	FROM
	(
	select 
	distinct DATE(created_at) AS Fecha,cast(DATE_FORMAT(Created_at, '%H')as char) AS hora,
	(order_nr)as Ventas
		from bob_live_mx.sales_order 
	WHERE assisted_sales_operator IS NOT NULL AND assisted_sales_operator 
	NOT IN (
	'tania.mejia',
	'andrea.mendoza',
	'arturo.trujillo',
	'Jesus.h.zul',
	'elliut.chavez',
	'lizeth.garcia',
	'mauricio.frias',
	'oscar.jaurez',
	'sandra.ponce'
	)
	)a
	GROUP BY a.Fecha,a.hora
	)
	scorecard

	 ON bd.Fecha = scorecard.Fecha
	AND bd.hora = scorecard.hora
	LEFT JOIN 
	(
		SELECT
			substring(
				CAST(Created_at AS CHAR),
				1,
				11
			) AS Dia,
			cast(
				DATE_FORMAT(Created_at, '%H') AS CHAR
			) hr,
			count(Resolution_Time) Resueltos_mismo_dia
		FROM
			tbl_zendesk
		WHERE
			substring(CAST(Solved_at AS CHAR), 1, 11) = substring(
				CAST(Created_at AS CHAR),
				1,
				11
			)
		AND via = 'mail'
		AND Created_at >= '2013-06-01'
		GROUP BY
			substring(
				CAST(Created_at AS CHAR),
				1,
				11
			),
			cast(
				DATE_FORMAT(Created_at, '%H') AS CHAR
			)
	)
	rmd
	 ON bd.Fecha = rmd.dia
	AND bd.hora = rmd.hr
	LEFT JOIN
	 (
		SELECT
			a.calldate,
			a.hr,
			postventa,
			preventa
		FROM
			(
				SELECT
					substring(CAST(calldate AS CHAR), 1, 11) calldate,
					cast(
						DATE_FORMAT(calldate, '%H') AS CHAR
					) hr,
					count(acctid) postVenta
				FROM
					cdr
				WHERE
					MONTH (calldate) >= '05'
				AND YEAR (calldate) = '2013'
				AND dcontext = 'linio-info'
				GROUP BY
					substring(CAST(calldate AS CHAR), 1, 11),
					cast(
						DATE_FORMAT(calldate, '%H') AS CHAR
					)
			) a
		LEFT JOIN (
			SELECT
				substring(CAST(calldate AS CHAR), 1, 11) calldate,
				cast(
					DATE_FORMAT(calldate, '%H') AS CHAR
				) hr,
				count(acctid) preVenta
			FROM
				cdr
			WHERE
				MONTH (calldate) >= '05'
			AND YEAR (calldate) = '2013'
			AND dcontext = 'linio-venta'
			GROUP BY
				substring(CAST(calldate AS CHAR), 1, 11),
				cast(
					DATE_FORMAT(calldate, '%H') AS CHAR
				)
		) b ON a.calldate = b.calldate
		AND a.hr = b.hr
	)
	ratio ON bd.Fecha = ratio.calldate
	AND bd.hora = ratio.hr
	LEFT JOIN 
	(
		SELECT
			a.Fecha,
			a.hr,
			(Excelente / Encuestas) AS Satisfaccion
		FROM
			(
				SELECT
					date_format(
						cast(
							concat(
								LEFT (substring(`date`, 7, 7), 4),
								substring(`date`, 3, 3),
								'-',
								LEFT (`date`, 2),
								' ',
								REPLACE (RIGHT(`date`, 8), '.', ':')
							) AS datetime
						),
						'%Y-%m-%d'
					) AS Fecha,
					cast(
						date_format(
							cast(
								concat(
									LEFT (substring(`date`, 7, 7), 4),
									substring(`date`, 3, 3),
									'-',
									LEFT (`date`, 2),
									' ',
									REPLACE (RIGHT(`date`, 8), '.', ':')
								) AS datetime
							),
							'%H'
						) AS CHAR
					) hr,
					count(*) Encuestas
				FROM
					questions
				WHERE
					question = 2
				GROUP BY
					date_format(
						cast(
							concat(
								LEFT (substring(`date`, 7, 7), 4),
								substring(`date`, 3, 3),
								'-',
								LEFT (`date`, 2),
								' ',
								REPLACE (RIGHT(`date`, 8), '.', ':')
							) AS datetime
						),
						'%Y-%m-%d'
					),
					date_format(
						cast(
							concat(
								LEFT (substring(`date`, 7, 7), 4),
								substring(`date`, 3, 3),
								'-',
								LEFT (`date`, 2),
								' ',
								REPLACE (RIGHT(`date`, 8), '.', ':')
							) AS datetime
						),
						'%H'
					)
			) a
		LEFT JOIN (
		Select a.Fecha,a.Hr,count(*)As Excelente
from
(SELECT
				date_format(
					cast(
						concat(
							LEFT (substring(`date`, 7, 7), 4),
							substring(`date`, 3, 3),
							'-',
							LEFT (`date`, 2),
							' ',
							REPLACE (RIGHT(`date`, 8), '.', ':')
						) AS datetime
					),
					'%Y-%m-%d'
				) AS Fecha,
				cast(
					date_format(
						cast(
							concat(
								LEFT (substring(`date`, 7, 7), 4),
								substring(`date`, 3, 3),
								'-',
								LEFT (`date`, 2),
								' ',
								REPLACE (RIGHT(`date`, 8), '.', ':')
							) AS datetime
						),
						'%H'
					) AS CHAR
				) hr,
				id,'1' Resuelto
			FROM
				questions
			WHERE
				question = 1
			AND answer1 = 1
			)a
INNER JOIN
(SELECT
				date_format(
					cast(
						concat(
							LEFT (substring(`date`, 7, 7), 4),
							substring(`date`, 3, 3),
							'-',
							LEFT (`date`, 2),
							' ',
							REPLACE (RIGHT(`date`, 8), '.', ':')
						) AS datetime
					),
					'%Y-%m-%d'
				) AS Fecha,
				cast(
					date_format(
						cast(
							concat(
								LEFT (substring(`date`, 7, 7), 4),
								substring(`date`, 3, 3),
								'-',
								LEFT (`date`, 2),
								' ',
								REPLACE (RIGHT(`date`, 8), '.', ':')
							) AS datetime
						),
						'%H'
					) AS CHAR
				) hr,
				id,'1' Intentos
			FROM
				questions
			WHERE
				question = 2
			AND answer1 = 1)b
on a.id=b.id
where a.Fecha>='2013-12-16'
group by a.Fecha,a.Hr) b ON a.Fecha = b.Fecha
		AND a.hr = b.hr
	)
	sat
	ON bd.Fecha = sat.Fecha
	AND bd.hora = sat.hr
	LEFT JOIN 
	(
		SELECT
			DATE_FORMAT((Created_at), '%Y-%m-%d') AS Dia,
			cast(
				DATE_FORMAT((Created_at), '%H') AS CHAR
			) hr,
			count(summation_column) AS Chats_Bandeja,
			count(Resolution_Time) Chats_B_Resueltos,
			sum(Resolution_Time) Chat_B_Tiemporespuesta,
			sum(
				(
					First_reply_time_in_minutes / 60
				)
			) Chat_B_TiempoFR
		FROM
			tbl_zendesk
		WHERE
			subject_ LIKE 'Olark chat with%'
		AND Created_at >= '2013-05-01'
		GROUP BY
			substring(
				CAST(Created_at AS CHAR),
				1,
				11
			),
			DATE_FORMAT(Created_at, '%H')
	)
	CHABN 
	ON bd.Fecha = CHABN.dia
	AND bd.Hora = CHABN.hr

	LEFT JOIN 

	(
	SELECT DATE(DATE) as Fecha,
	CASE 1 
	WHEN CHAR_LENGTH(CAST(EXTRACT(HOUR FROM `HOUR`) AS CHAR))=1
	THEN CONCAT("0",CAST(EXTRACT(HOUR FROM `HOUR`) AS CHAR))
	ELSE CAST(EXTRACT(HOUR FROM `HOUR`) AS CHAR)
	END AS HR,
	COUNT(DISTINCT(order_nr)) AS Net_Orders
	FROM production.tbl_order_detail
	WHERE 
	#oac=1
	status_item <>'invalid'
	GROUP BY DATE(DATE),
	CASE 1 
	WHEN CHAR_LENGTH(CAST(EXTRACT(HOUR FROM `HOUR`) AS CHAR))=1
	THEN CONCAT("0",CAST(EXTRACT(HOUR FROM `HOUR`) AS CHAR))
	ELSE CAST(EXTRACT(HOUR FROM `HOUR`) AS CHAR)
	END
	)net
	ON bd.Fecha = net.fecha
	AND bd.Hora = net.hr


	left join


		(
	select a.inicio,a.hr,Recibido,TEspera,Abandono,TDuracion,ChatAtn20
	from
	(
	select substring(CAST((chat_start_time) AS CHAR),1,11) as inicio
		,cast(DATE_FORMAT(chat_start_time,'%H') as char)hr,
		count(chat_start_time) as Recibido,sum(operator_first_response_delay) as TEspera,
		sum(case 1 when transcript_type ='offlineMessage' and chat_duration=0 then 1 else 0 end) as Abandono,sum(chat_duration) as TDuracion
		from tbl_olark_chat
		
		group by substring(CAST(chat_start_time AS CHAR),1,11),DATE_FORMAT(chat_start_time,'%H')
	having hr in (08,09,10,11,12,13,14,15,16,17,18,19,20,21,22))a

	left join

	(select substring(CAST((chat_start_time) AS CHAR),1,11) as inicio
		,cast(DATE_FORMAT(chat_start_time,'%H') as char)hr,
		count(chat_start_time) as ChatAtn20
		from tbl_olark_chat
		where operator_first_response_delay<30
		group by substring(CAST(chat_start_time AS CHAR),1,11),DATE_FORMAT(chat_start_time,'%H')
	having hr in (08,09,10,11,12,13,14,15,16,17,18,19,20,21,22)
	)b
	on a.inicio=b.inicio and a.hr=b.hr
	)chat

	on bd.fecha=chat.inicio and bd.hora=chat.hr

	WHERE
		bd.fecha >= '2013-09-09'
	AND bd.fecha <= date_sub(curdate(), INTERVAL 1 DAY);
	update tbl_general_cc set Dia_Semana=dayname(Fecha);

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`auto.reports`@`%`*/ /*!50003 PROCEDURE `update_survery`()
begin	

delete from tbl_ivr_FCR;
	insert into tbl_ivr_FCR
	SELECT
			a.Fecha As Fecha,
			a.hr AS Hora,
			(Excelente / Encuestas) AS FCR
		FROM
			(
				SELECT
					date_format(
						cast(
							concat(
								LEFT (substring(`date`, 7, 7), 4),
								substring(`date`, 3, 3),
								'-',
								LEFT (`date`, 2),
								' ',
								REPLACE (RIGHT(`date`, 8), '.', ':')
							) AS datetime
						),
						'%Y-%m-%d'
					) AS Fecha,
					cast(
						date_format(
							cast(
								concat(
									LEFT (substring(`date`, 7, 7), 4),
									substring(`date`, 3, 3),
									'-',
									LEFT (`date`, 2),
									' ',
									REPLACE (RIGHT(`date`, 8), '.', ':')
								) AS datetime
							),
							'%H'
						) AS CHAR
					) hr,
					count(*) Encuestas
				FROM
					questions
				WHERE
					question = 2
				GROUP BY
					date_format(
						cast(
							concat(
								LEFT (substring(`date`, 7, 7), 4),
								substring(`date`, 3, 3),
								'-',
								LEFT (`date`, 2),
								' ',
								REPLACE (RIGHT(`date`, 8), '.', ':')
							) AS datetime
						),
						'%Y-%m-%d'
					),
					date_format(
						cast(
							concat(
								LEFT (substring(`date`, 7, 7), 4),
								substring(`date`, 3, 3),
								'-',
								LEFT (`date`, 2),
								' ',
								REPLACE (RIGHT(`date`, 8), '.', ':')
							) AS datetime
						),
						'%H'
					)
			) a
		LEFT JOIN (
		Select a.Fecha,a.Hr,count(*)As Excelente
from
(SELECT
				date_format(
					cast(
						concat(
							LEFT (substring(`date`, 7, 7), 4),
							substring(`date`, 3, 3),
							'-',
							LEFT (`date`, 2),
							' ',
							REPLACE (RIGHT(`date`, 8), '.', ':')
						) AS datetime
					),
					'%Y-%m-%d'
				) AS Fecha,
				cast(
					date_format(
						cast(
							concat(
								LEFT (substring(`date`, 7, 7), 4),
								substring(`date`, 3, 3),
								'-',
								LEFT (`date`, 2),
								' ',
								REPLACE (RIGHT(`date`, 8), '.', ':')
							) AS datetime
						),
						'%H'
					) AS CHAR
				) hr,
				id,'1' Resuelto
			FROM
				questions
			WHERE
				question = 1
			AND answer1 = 1
			)a
INNER JOIN
(SELECT
				date_format(
					cast(
						concat(
							LEFT (substring(`date`, 7, 7), 4),
							substring(`date`, 3, 3),
							'-',
							LEFT (`date`, 2),
							' ',
							REPLACE (RIGHT(`date`, 8), '.', ':')
						) AS datetime
					),
					'%Y-%m-%d'
				) AS Fecha,
				cast(
					date_format(
						cast(
							concat(
								LEFT (substring(`date`, 7, 7), 4),
								substring(`date`, 3, 3),
								'-',
								LEFT (`date`, 2),
								' ',
								REPLACE (RIGHT(`date`, 8), '.', ':')
							) AS datetime
						),
						'%H'
					) AS CHAR
				) hr,
				id,'1' Intentos
			FROM
				questions
			WHERE
				question = 2
			AND answer1 = 1)b
on a.id=b.id
where a.Fecha>='2013-12-16'
group by a.Fecha,a.Hr) b ON a.Fecha = b.Fecha
		AND a.hr = b.hr
where a.Fecha>='2013-12-16';

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:03:37
