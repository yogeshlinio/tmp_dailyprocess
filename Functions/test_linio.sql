-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: test_linio
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'test_linio'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `attribution_model`()
begin

select 'Attribution Mx: ' as step, 'Start' as state, now() as time;



update attribution_testing.campaigns_config_mx set data_source_value=substring_index(data_source_value, '\'', -1), region=substring_index(region, '\'', 1) where date is null;

update attribution_testing.campaigns_config_mx set date=date(time_start) where date is null;



delete from attribution_testing.bob_ga_mx;



insert into attribution_testing.bob_ga_mx(date, order_nr, channel, campaign, voucher_code, about_linio, new_customers) select t.date, t.order_nr, t.channel_group, t.campaign, t.coupon_code, t.about_linio, case when t.new_customers != 0 then 1 else 0 end from production.tbl_order_detail t where t.date between '2013-11-01' and '2013-11-11' group by t.order_nr;

update attribution_testing.bob_ga_mx b inner join SEM.transaction_id_mx t on b.order_nr=t.transaction_id set b.ad_group=t.ad_group;

update attribution_testing.bob_ga_mx set new_customers = 0 where new_customers is null;



delete from attribution_testing.webtrekk_mx;



insert into attribution_testing.webtrekk_mx(date, time, order_nr, campaign) select distinct c.date, c.time, o.order_id, c.campaign from attribution_testing.getfullcampaign_mx c, attribution_testing.getfullorders_mx o where o.sid=c.sid group by c.date, c.campaign, o.order_id;

update attribution_testing.webtrekk_mx w inner join attribution_testing.campaigns_config_mx c on w.campaign=c.data_source_value set w.channel=c.category_2;

update attribution_testing.webtrekk_mx w inner join attribution_testing.campaigns_config_mx c on w.campaign=c.data_source_value set w.ad_group=c.category_4;

update attribution_testing.webtrekk_mx set campaign=substr(campaign, locate('=', campaign)+1);

set @attribution := 0;
set @var_order_nr := space(20);

create table attribution_testing.temporary_webtrekk1(
date date,
time datetime,
order_nr varchar(255),
campaign varchar(255),
`range` int
);
create index temp on attribution_testing.temporary_webtrekk1(time, order_nr, campaign);

insert into attribution_testing.temporary_webtrekk1(date, time, order_nr, campaign, `range`) select date, time, order_nr, campaign, position from (select date, time, order_nr, campaign, IF( order_nr != @var_order_nr , @attribution := 1, @attribution := @attribution + 1 ) AS position, @var_order_nr := order_nr from (select date, time, order_nr, campaign from attribution_testing.webtrekk_mx)a order by order_nr asc, time asc)a;

update attribution_testing.webtrekk_mx w set position = (select `range` from attribution_testing.temporary_webtrekk1 t where t.order_nr=w.order_nr and t.campaign=w.campaign and t.time=w.time group by t.order_nr, t.campaign, t.time);

drop table attribution_testing.temporary_webtrekk1;



create table attribution_testing.`temporary_webtrekk1.1` select date, time, order_nr, channel, campaign, ad_group, 1 as position from attribution_testing.webtrekk_mx where order_nr in (select order_nr from attribution_testing.webtrekk_mx where channel='Customer Service');

create table attribution_testing.`temporary_webtrekk1.2` select order_nr, max(time) as time from attribution_testing.webtrekk_mx where channel='Customer Service' group by order_nr; 

delete from attribution_testing.webtrekk_mx where order_nr in (select order_nr from attribution_testing.`temporary_webtrekk1.2`);

insert into attribution_testing.webtrekk_mx select a.* from attribution_testing.`temporary_webtrekk1.1` a, attribution_testing.`temporary_webtrekk1.2` b where a.order_nr=b.order_nr and a.time=b.time;

drop table attribution_testing.`temporary_webtrekk1.1`;

drop table attribution_testing.`temporary_webtrekk1.2`;



create table attribution_testing.`temporary_webtrekk1.5`(
date date,
order_nr varchar(255),
channel varchar(255),
campaign varchar(255),
highest_number int
);
create index order_nr on attribution_testing.`temporary_webtrekk1.5`(date, order_nr);

insert into attribution_testing.`temporary_webtrekk1.5` select date, order_nr, channel, campaign, max(position) from attribution_testing.webtrekk_mx group by order_nr;

update attribution_testing.`temporary_webtrekk1.5` t set t.channel= (select w.channel from attribution_testing.webtrekk_mx w where w.channel='SEO Brand' and w.order_nr=t.order_nr and w.date=t.date and t.highest_number !=1);

update attribution_testing.`temporary_webtrekk1.5` t set t.channel= (select w.channel from attribution_testing.webtrekk_mx w where w.channel='SEM Brand' and w.order_nr=t.order_nr and w.date=t.date and t.highest_number !=1) where channel is null;

delete from attribution_testing.webtrekk_mx where order_nr in (select order_nr from attribution_testing.`temporary_webtrekk1.5` where channel in ('SEO Brand', 'SEM Brand'));

drop table attribution_testing.`temporary_webtrekk1.5`;



delete from attribution_testing.middle_recollecting_process_mx;

create table attribution_testing.temporary_webtrekk2(
order_nr varchar(255),
in_webtrekk int
);
create index order_nr on attribution_testing.temporary_webtrekk2(order_nr);

insert into attribution_testing.temporary_webtrekk2(order_nr) select distinct t.order_nr from attribution_testing.bob_ga_mx t;

update attribution_testing.temporary_webtrekk2 t inner join attribution_testing.webtrekk_mx w on t.order_nr=w.order_nr set in_webtrekk=1;

update attribution_testing.temporary_webtrekk2 set in_webtrekk=0 where in_webtrekk is null;

insert into attribution_testing.middle_recollecting_process_mx(date, order_nr, channel_webtrekk, campaign_webtrekk, ad_group_webtrekk, channel_google_analytics, campaign_google_analytics, ad_group_google_analytics, position, about_linio, voucher_code, new_customers) select w.date, w.order_nr, w.channel, w.campaign, w.ad_group, b.channel, b.campaign, b.ad_group, w.position, b.about_linio, b.voucher_code, b.new_customers from attribution_testing.webtrekk_mx w, attribution_testing.temporary_webtrekk2 t, attribution_testing.bob_ga_mx b where t.order_nr=w.order_nr and w.order_nr=b.order_nr and t.in_webtrekk=1;

insert into attribution_testing.middle_recollecting_process_mx(date, order_nr, channel_google_analytics, campaign_google_analytics, ad_group_google_analytics, about_linio, voucher_code, new_customers) select b.date, b.order_nr, b.channel, b.campaign, b.ad_group, b.about_linio, b.voucher_code, b.new_customers from attribution_testing.temporary_webtrekk2 t, attribution_testing.bob_ga_mx b where t.order_nr=b.order_nr and t.in_webtrekk=0;

drop table attribution_testing.temporary_webtrekk2;

update attribution_testing.middle_recollecting_process_mx set channel_webtrekk = about_linio where channel_webtrekk='Customer Service' and about_linio is not null;

update attribution_testing.middle_recollecting_process_mx set channel_google_analytics = about_linio where channel_google_analytics='Tele Sales' and about_linio is not null;  

update attribution_testing.middle_recollecting_process_mx set position=1 where position is null;






delete from attribution_testing.attribution_model_mx;

insert into attribution_testing.attribution_model_mx(date, order_nr, channel, campaign, ad_group, position, new_customers) select date, order_nr, channel_webtrekk, campaign_webtrekk, ad_group_webtrekk, position, new_customers from attribution_testing.middle_recollecting_process_mx where channel_webtrekk is not null;

insert into attribution_testing.attribution_model_mx(date, order_nr, channel, campaign, ad_group, position, new_customers) select date, order_nr, channel_google_analytics, campaign_google_analytics, ad_group_google_analytics, position, new_customers from attribution_testing.middle_recollecting_process_mx where channel_webtrekk is null;

create table attribution_testing.temporary_webtrekk4(
order_nr varchar(255),
highest_number int
);
create index order_nr on attribution_testing.temporary_webtrekk4(order_nr, highest_number);

insert into attribution_testing.temporary_webtrekk4 select order_nr, max(position) from attribution_testing.attribution_model_mx group by order_nr;

update attribution_testing.attribution_model_mx a inner join attribution_testing.temporary_webtrekk4 w on a.order_nr=w.order_nr inner join attribution_testing.percent_attribution_mx x on x.clicks=w.highest_number and x.numbers=a.position and x.new_customers=a.new_customers set a.percent=x.result;

drop table attribution_testing.temporary_webtrekk4;

update attribution_testing.attribution_model_mx set channel='Non Identified' where channel is null;

update attribution_testing.attribution_model_mx set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=SEM.week_iso(date);



update attribution_testing.attribution_model_mx a inner join attribution_testing.dictionary d on a.channel=d.source_channel set a.channel=d.target_channel;

select 'Attribution Mx: ' as step, 'Stop' as state, now() as time;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `A_Master_all`()
BEGIN



#----------------------------Mexico------------------------------------------#

drop TABLE IF EXISTS test_linio.A_Master;

drop TABLE IF EXISTS test_linio.A_Master_MEX;
create table test_linio.A_Master_MEX
select * from development_mx.A_Master;

create  table test_linio.A_Master
select * from test_linio.A_Master_MEX;

#----------------------------Colombia----------------------------------------#

drop  TABLE IF EXISTS test_linio.A_Master_COL;
create table test_linio.A_Master_COL
select * from development_co.A_Master;

insert into   test_linio.A_Master
select * from test_linio.A_Master_COL;

#----------------------------Perú--------------------------------------------#

drop  TABLE IF EXISTS test_linio.A_Master_PER;
create table test_linio.A_Master_PER
select * from development_pe.A_Master;


insert into   test_linio.A_Master
select * from test_linio.A_Master_PER;

#----------------------------VENEZUELA------------------------------------------#

drop  TABLE IF EXISTS test_linio.A_Master_VEN;
create table test_linio.A_Master_VEN
select * from development_ve.A_Master;

insert into   test_linio.A_Master
select * from test_linio.A_Master_VEN;

#----------------------------EUROS------------------------------------------#

update test_linio.A_Master A , development_mx.A_E_BI_ExchangeRate B
set
A.OriginalPrice =round(A.OriginalPrice /B.XR,5),
A.Tax =round(A.Tax /B.XR,5),
A.CreditNotes =round(A.CreditNotes /B.XR,5),
A.Price = round(A.Price/B.XR,5),
A.PriceAfterTax =round(A.PriceAfterTax /B.XR,5),
A.CouponValue = round(A.CouponValue/B.XR,5),
A.CouponValueAfterTax = round(A.CouponValueAfterTax/B.XR,5),
A.NonMKTCouponAfterTax= round(A.NonMKTCouponAfterTax/B.XR,5),
A.OtherRev= round(A.OtherRev/B.XR,5),
A.PaidPrice = round(A.PaidPrice/B.XR,5), 
A.PaidPriceAfterTax = round(A.PaidPriceAfterTax/B.XR,5), 
A.ShippingFee =round(A.ShippingFee/B.XR,5),
A.DeliveryCostSupplier=round(A.DeliveryCostSupplier/B.XR,5),
A.Cost = round(A.Cost/B.XR,5), 
A.CostAfterTax =round(A.CostAfterTax/B.XR,5),
A.ShippingFee =round(A.ShippingFee/B.XR,5),
A.ShippingCost =round(A.ShippingCost /B.XR,5),
A.ShipmentCost=round(A.ShipmentCost /B.XR,5),
A.PaymentFees =round(A.PaymentFees/B.XR,5),
A.FLWHCost =round(A.FLWHCost/B.XR,5),
A.FLCSCost =round(A.FLCSCost/B.XR,5),
A.GrandTotal=round(A.GrandTotal/B.XR,5),
A.Rev=round(A.Rev/B.XR,5),
A.COGS=round(A.COGS/B.XR,5),
A.PCOne=round(A.PCOne/B.XR,5),
A.PCOnePFive=round(A.PCOnePFive/B.XR,5),
A.PCTwo=round(A.PCTwo/B.XR,5)
where B.Country= A.Country and B.Month_Num= A.MonthNum
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`julian.buitrago`@`%`*/ /*!50003 PROCEDURE `A_Master_Consolidated`()
BEGIN

DROP TABLE IF EXISTS test_linio.A_Master;
CREATE TABLE test_linio.A_Master LIKE development_co_project.A_Master;
ALTER TABLE test_linio.A_Master DROP PRIMARY KEY,
                                  ADD PRIMARY KEY ( Country, ItemID );

#-----------Colombia
INSERT test_linio.A_Master SELECT * FROM development_co_project.A_Master;


#-----------Mex

INSERT test_linio.A_Master 
(
Country,
MonthNum,
Date,
Hour,
OrderNum,
IdSalesOrder,
ItemID,
IdSupplier,
Supplier,
Buyer,
HeadBuyer,
Brand,
StoreId,
CatKPI,
CatBP,
Cat1,
Cat2,
Cat3,
SKUConfig,
SKUSimple,
SKUName,
isActiveSKUConfig,
isActiveSKUSimple,
isVisible,
isMPlace,
isMPlaceSince,
isMPlaceUntil,
MPlaceFee,
ProductWeight,
PackageWeight,
PackageHeight,
PackageLength,
PackageWidth,
OrderWeight,
ItemsInOrder,
NetItemsInOrder,
OriginalPrice,
TaxPercent,
Tax,
Price,
PriceAfterTax,
PrefixCode,
CouponCodeDescription,
CouponCode,
CouponValue,
CouponValueAfterTax,
NonMKTCouponAfterTax,
PaidPrice,
PaidPriceAfterTax,
Fees,
ExtraCharge,
GrandTotal,
Cost,
CostAfterTax,
WHCost,
FLWHCost,
CSCost,
FLCSCost,
PackagingCost,
ShipmentType,
DeliveryCostSupplier,
ShipmentCost,
ShippingCost,
ShippingFee,
PaymentMethod,
IssuingBank,
Bin,
Installment,
PaymentFees,
TransactionFee,
TransactionFeeAfterTax,
InstallmentFee,
InstallmentFeeAfterTax,
Interest,
NetInterest,
CustomerNum,
CustomerEmail,
FirstName,
LastName,
PostCode,
State,
City,
Status,
OrderBeforeCan,
OrderAfterCan,
Cancellations,
Pending,
Returns,
Rejected,
Cancelled,
DateDelivered,
Delivered,
DateReturned,
DeliveredReturn,
NetDelivered,
DateExported,
Exported,
DateCollected,
Collected,
DateRefunded,
Refunded,
Exportable,
Shipped,
ConfirmCall1,
FraudCheckPending,
MCI,
NetMCI,
MSI,
MarketingChannel,
CategoryAd,
PaidFreeChannel,
CohortMonthNum,
FirstOrderNum,
NewReturning,
NewReturningGross,
Source_medium,
Campaign,
Channel,
ChannelGroup,
CACCustomer,
CreditNotes,
COGS,
PCOne,
PCOnePFive,
PCTwo,
Rev,
OtherRev
)
SELECT 
Country,
MonthNum,
Date,
Time,
OrderNum,
IdSalesOrder,
ItemID,
IdSupplier,
Supplier,
Buyer,
HeadBuyer,
Brand,
StoreId,
CatKPI,
CatBP,
Cat1,
Cat2,
Cat3,
SKUConfig,
SKUSimple,
SKUName,
isActiveSKUConfig,
isActiveSKUSimple,
isVisible,
isMPlace,
isMPlaceSince,
isMPlaceUntil,
MPlaceFee,
ProductWeight,
PackageWeight,
PackageHeight,
PackageLength,
PackageWidth,
OrderWeight,
ItemsInOrder,
NetItemsInOrder,
OriginalPrice,
TaxPercent,
Tax,
Price,
PriceAfterTax,
PrefixCode,
CouponCodeDescription,
CouponCode,
CouponValue,
CouponValueAfterTax,
NonMKTCouponAfterTax,
PaidPrice,
PaidPriceAfterTax,
Fees,
ExtraCharge,
GrandTotal,
Cost,
CostAfterTax,
WHCost,
FLWHCost,
CSCost,
FLCSCost,
PackagingCost,
ShipmentType,
DeliveryCostSupplier,
ShipmentCost,
ShippingCost,
ShippingFee,
PaymentMethod,
IssuingBank,
Bin,
Installment,
PaymentFees,
TransactionFee,
TransactionFeeAfterTax,
InstallmentFee,
InstallmentFeeAfterTax,
Interest,
NetInterest,
CustomerNum,
CustomerEmail,
FirstName,
LastName,
PostCode,
State,
City,
Status,
OrderBeforeCan,
OrderAfterCan,
Cancellations,
Pending,
Returns,
Rejected,
Cancelled,
DateDelivered,
Delivered,
DateReturned,
DeliveredReturn,
NetDelivered,
DateExported,
Exported,
DateCollected,
Collected,
DateRefunded,
Refunded,
Exportable,
Shipped,
ConfirmCall1,
FraudCheckPending,
MCI,
NetMCI,
MSI,
MarketingChannel,
CategoryAd,
PaidFreeChannel,
CohortMonthNum,
FirstOrderNum,
NewReturning,
NewReturningGross,
Source_medium,
Campaign,
Channel,
Channel_group,
CACCustomer,
CreditNotes,
COGS,
PCOne,
PCOnePFive,
PCTwo,
Rev,
OtherRev
 FROM development_mx.A_Master;

#--------- Peru

INSERT test_linio.A_Master 
(
Country,
MonthNum,
Date,
Hour,
OrderNum,
IdSalesOrder,
ItemID,
IdSupplier,
Supplier,
Buyer,
HeadBuyer,
Brand,
StoreId,
CatKPI,
CatBP,
Cat1,
Cat2,
Cat3,
SKUConfig,
SKUSimple,
SKUName,
isActiveSKUConfig,
isActiveSKUSimple,
isVisible,
isMPlace,
isMPlaceSince,
isMPlaceUntil,
MPlaceFee,
ProductWeight,
PackageWeight,
PackageHeight,
PackageLength,
PackageWidth,
OrderWeight,
ItemsInOrder,
NetItemsInOrder,
OriginalPrice,
TaxPercent,
Tax,
Price,
PriceAfterTax,
PrefixCode,
CouponCodeDescription,
CouponCode,
CouponValue,
CouponValueAfterTax,
NonMKTCouponAfterTax,
PaidPrice,
PaidPriceAfterTax,
Fees,
ExtraCharge,
GrandTotal,
Cost,
CostAfterTax,
WHCost,
FLWHCost,
CSCost,
FLCSCost,
PackagingCost,
ShipmentType,
DeliveryCostSupplier,
ShipmentCost,
ShippingCost,
ShippingFee,
PaymentMethod,
IssuingBank,
Bin,
Installment,
PaymentFees,
TransactionFee,
TransactionFeeAfterTax,
InstallmentFee,
InstallmentFeeAfterTax,
Interest,
NetInterest,
CustomerNum,
CustomerEmail,
FirstName,
LastName,
PostCode,
State,
City,
Status,
OrderBeforeCan,
OrderAfterCan,
Cancellations,
Pending,
Returns,
Rejected,
Cancelled,
DateDelivered,
Delivered,
DateReturned,
DeliveredReturn,
NetDelivered,
DateExported,
Exported,
DateCollected,
Collected,
DateRefunded,
Refunded,
Exportable,
Shipped,
ConfirmCall1,
FraudCheckPending,
MCI,
NetMCI,
MSI,
MarketingChannel,
CategoryAd,
PaidFreeChannel,
CohortMonthNum,
FirstOrderNum,
NewReturning,
NewReturningGross,
Source_medium,
Campaign,
Channel,
ChannelGroup,
CACCustomer,
CreditNotes,
COGS,
PCOne,
PCOnePFive,
PCTwo,
Rev,
OtherRev
)
SELECT 
Country,
MonthNum,
Date,
Time,
OrderNum,
IdSalesOrder,
ItemID,
IdSupplier,
Supplier,
Buyer,
HeadBuyer,
Brand,
StoreId,
CatKPI,
CatBP,
Cat1,
Cat2,
Cat3,
SKUConfig,
SKUSimple,
SKUName,
isActiveSKUConfig,
isActiveSKUSimple,
isVisible,
isMPlace,
isMPlaceSince,
isMPlaceUntil,
MPlaceFee,
ProductWeight,
PackageWeight,
PackageHeight,
PackageLength,
PackageWidth,
OrderWeight,
ItemsInOrder,
NetItemsInOrder,
OriginalPrice,
TaxPercent,
Tax,
Price,
PriceAfterTax,
PrefixCode,
CouponCodeDescription,
CouponCode,
CouponValue,
CouponValueAfterTax,
NonMKTCouponAfterTax,
PaidPrice,
PaidPriceAfterTax,
Fees,
ExtraCharge,
GrandTotal,
Cost,
CostAfterTax,
WHCost,
FLWHCost,
CSCost,
FLCSCost,
PackagingCost,
ShipmentType,
DeliveryCostSupplier,
ShipmentCost,
ShippingCost,
ShippingFee,
PaymentMethod,
IssuingBank,
Bin,
Installment,
PaymentFees,
TransactionFee,
TransactionFeeAfterTax,
InstallmentFee,
InstallmentFeeAfterTax,
Interest,
NetInterest,
CustomerNum,
CustomerEmail,
FirstName,
LastName,
PostCode,
State,
City,
Status,
OrderBeforeCan,
OrderAfterCan,
Cancellations,
Pending,
Returns,
Rejected,
Cancelled,
DateDelivered,
Delivered,
DateReturned,
DeliveredReturn,
NetDelivered,
DateExported,
Exported,
DateCollected,
Collected,
DateRefunded,
Refunded,
Exportable,
Shipped,
ConfirmCall1,
FraudCheckPending,
MCI,
NetMCI,
MSI,
MarketingChannel,
CategoryAd,
PaidFreeChannel,
CohortMonthNum,
FirstOrderNum,
NewReturning,
NewReturningGross,
Source_medium,
Campaign,
Channel,
Channel_group,
CACCustomer,
CreditNotes,
COGS,
PCOne,
PCOnePFive,
PCTwo,
Rev,
OtherRev
 FROM development_pe.A_Master;

#--------Ven

INSERT test_linio.A_Master 
(
Country,
MonthNum,
Date,
Hour,
OrderNum,
IdSalesOrder,
ItemID,
IdSupplier,
Supplier,
Buyer,
HeadBuyer,
Brand,
StoreId,
CatKPI,
CatBP,
Cat1,
Cat2,
Cat3,
SKUConfig,
SKUSimple,
SKUName,
isActiveSKUConfig,
isActiveSKUSimple,
isVisible,
isMPlace,
isMPlaceSince,
isMPlaceUntil,
MPlaceFee,
ProductWeight,
PackageWeight,
PackageHeight,
PackageLength,
PackageWidth,
OrderWeight,
ItemsInOrder,
NetItemsInOrder,
OriginalPrice,
TaxPercent,
Tax,
Price,
PriceAfterTax,
PrefixCode,
CouponCodeDescription,
CouponCode,
CouponValue,
CouponValueAfterTax,
NonMKTCouponAfterTax,
PaidPrice,
PaidPriceAfterTax,
Fees,
ExtraCharge,
GrandTotal,
Cost,
CostAfterTax,
WHCost,
FLWHCost,
CSCost,
FLCSCost,
PackagingCost,
ShipmentType,
DeliveryCostSupplier,
ShipmentCost,
ShippingCost,
ShippingFee,
PaymentMethod,
IssuingBank,
Bin,
Installment,
PaymentFees,
TransactionFee,
TransactionFeeAfterTax,
InstallmentFee,
InstallmentFeeAfterTax,
Interest,
NetInterest,
CustomerNum,
CustomerEmail,
FirstName,
LastName,
PostCode,
State,
City,
Status,
OrderBeforeCan,
OrderAfterCan,
Cancellations,
Pending,
Returns,
Rejected,
Cancelled,
DateDelivered,
Delivered,
DateReturned,
DeliveredReturn,
NetDelivered,
DateExported,
Exported,
DateCollected,
Collected,
DateRefunded,
Refunded,
Exportable,
Shipped,
ConfirmCall1,
FraudCheckPending,
MCI,
NetMCI,
MSI,
MarketingChannel,
CategoryAd,
PaidFreeChannel,
CohortMonthNum,
FirstOrderNum,
NewReturning,
NewReturningGross,
Source_medium,
Campaign,
Channel,
ChannelGroup,
CACCustomer,
CreditNotes,
COGS,
PCOne,
PCOnePFive,
PCTwo,
Rev,
OtherRev
)
SELECT 
Country,
MonthNum,
Date,
Time,
OrderNum,
IdSalesOrder,
ItemID,
IdSupplier,
Supplier,
Buyer,
HeadBuyer,
Brand,
StoreId,
CatKPI,
CatBP,
Cat1,
Cat2,
Cat3,
SKUConfig,
SKUSimple,
SKUName,
isActiveSKUConfig,
isActiveSKUSimple,
isVisible,
isMPlace,
isMPlaceSince,
isMPlaceUntil,
MPlaceFee,
ProductWeight,
PackageWeight,
PackageHeight,
PackageLength,
PackageWidth,
OrderWeight,
ItemsInOrder,
NetItemsInOrder,
OriginalPrice,
TaxPercent,
Tax,
Price,
PriceAfterTax,
PrefixCode,
CouponCodeDescription,
CouponCode,
CouponValue,
CouponValueAfterTax,
NonMKTCouponAfterTax,
PaidPrice,
PaidPriceAfterTax,
Fees,
ExtraCharge,
GrandTotal,
Cost,
CostAfterTax,
WHCost,
FLWHCost,
CSCost,
FLCSCost,
PackagingCost,
ShipmentType,
DeliveryCostSupplier,
ShipmentCost,
ShippingCost,
ShippingFee,
PaymentMethod,
IssuingBank,
Bin,
Installment,
PaymentFees,
TransactionFee,
TransactionFeeAfterTax,
InstallmentFee,
InstallmentFeeAfterTax,
Interest,
NetInterest,
CustomerNum,
CustomerEmail,
FirstName,
LastName,
PostCode,
State,
City,
Status,
OrderBeforeCan,
OrderAfterCan,
Cancellations,
Pending,
Returns,
Rejected,
Cancelled,
DateDelivered,
Delivered,
DateReturned,
DeliveredReturn,
NetDelivered,
DateExported,
Exported,
DateCollected,
Collected,
DateRefunded,
Refunded,
Exportable,
Shipped,
ConfirmCall1,
FraudCheckPending,
MCI,
NetMCI,
MSI,
MarketingChannel,
CategoryAd,
PaidFreeChannel,
CohortMonthNum,
FirstOrderNum,
NewReturning,
NewReturningGross,
Source_medium,
Campaign,
Channel,
Channel_group,
CACCustomer,
CreditNotes,
COGS,
PCOne,
PCOnePFive,
PCTwo,
Rev,
OtherRev
 FROM development_ve.A_Master;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `boost_catalog`(IN CatalogName VARCHAR(250))
BEGIN
	
	SET @s = CONCAT('DROP TABLE IF EXISTS test_linio.A_Catalog_Boost_',CatalogName);
	PREPARE stmt1 FROM @s;
	EXECUTE stmt1; 
	DEALLOCATE PREPARE stmt1; 
	SET @s = CONCAT('CREATE TABLE test_linio.A_Catalog_Boost_',CatalogName,
	'(
		 /*General*/
		 SKU_Config VARCHAR(250) NOT NULL,
		 SKU_Name   VARCHAR(250) NOT NULL,
		 averagePosition DECIMAL (15,5) NOT NULL,
		 lastBoost INT NOT NULL,
		 visibility DECIMAL (15,5) NOT NULL,
		 isVisible  BIT(1) NOT NULL,
		 isMPlace       INT NOT NULL,
		 isMPlace_Since date NOT NULL,

		
		 /*Dana Stuff*/
		 Pricing_PC1_5 DECIMAL(15,5) NOT NULL,
		 
		 /*commercial REPORT*/
		 Commercial_Report_Items   INT NOT NULL,
		 Commercial_Report_PC1     DECIMAL(15,5) NOT NULL,
		 Commercial_Report_PC1_5   DECIMAL(15,5) NOT NULL,
		 Commercial_Report_PC2     DECIMAL(15,5) NOT NULL,
		 Commercial_Report_Rev     DECIMAL(15,5) NOT NULL,
		 Commercial_Report_Price   DECIMAL(15,5) NOT NULL,


		 /*Stock*/
		 Daily_Stock_Fullfilment_Type         VARCHAR(15) NOT NULL,
		 Daily_Stock_Fullfilment_Cost         DECIMAL(15,5) NOT NULL,
		 Daily_Stock_Fullfilment_inStockItems INT NOT NULL,
	 
		 PRIMARY KEY ( SKU_Config)
	);');
	PREPARE stmt1 FROM @s;
	EXECUTE stmt1; 
	DEALLOCATE PREPARE stmt1; 

	
	SET @s = CONCAT('INSERT test_linio.A_Catalog_Boost_',CatalogName, '(SKU_Config, visibility)
	SELECT
				SKU_Config,
				COUNT(SKU_Config)/30 AS visibility
	FROM
			test_linio.product_position_in_catalog
	WHERE
			 date_time >= NOW() - INTERVAL 30 DAY
			AND catalog = CatalogName
	GROUP BY SKU_Config
	;');
	PREPARE stmt1 FROM @s;
	EXECUTE stmt1; 
	DEALLOCATE PREPARE stmt1; 


	
	SET @s = CONCAT('UPDATE test_linio.A_Catalog_Boost_',CatalogName, ' a
		 INNER JOIN ( SELECT SKU_Config , SKU_Name,  isVisible, isMarketPlace, isMarketPlace_Since  
			FROM development_mx.A_Master_Catalog
				GROUP BY Sku_Config) AS b
			ON a.SKU_Config = b.SKU_Config
	SET
			a.SKU_Name = b.SKU_Name,
			a.isVisible = b.isVisible,
			a.isMPlace = b.isMarketPlace,
			a.isMPlace_Since = b.isMarketPlace_Since
	;');
	PREPARE stmt1 FROM @s;
	EXECUTE stmt1; 
	DEALLOCATE PREPARE stmt1;

	
	SET @s = CONCAT('UPDATE test_linio.A_Catalog_Boost_',CatalogName, ' a
		 INNER JOIN ( SELECT SKU_Config, SUM( PC1_5 ) AS PC1_5
										FROM david_dana.`tbl-pricing_current_table`
									 GROUP BY sku_config ) AS b 
						 ON a.SKU_Config = b.SKU_Config
	SET
		a.Pricing_PC1_5 = b.PC1_5
	;');
	PREPARE stmt1 FROM @s;
	EXECUTE stmt1; 
	DEALLOCATE PREPARE stmt1;

	
	SET @s = CONCAT('UPDATE test_linio.A_Catalog_Boost_',CatalogName, ' a
		 INNER JOIN ( SELECT SKU_Config, 
												 fullfilment_type_real,
												 SUM(cost_w_o_vat) AS cost_w_o_vat,
												 SUM(in_stock) AS in_stock
										FROM production.out_stock_hist 
								 GROUP BY sku_config ) b 
						 ON a.SKU_Config = b.SKU_Config
	SET
		 a.Daily_Stock_Fullfilment_Type = b.fullfilment_type_real, 
		 a.Daily_Stock_Fullfilment_inStockItems = b.in_stock, 
		 a.Daily_Stock_Fullfilment_Cost = b.cost_w_o_vat
	;');
	PREPARE stmt1 FROM @s;
	EXECUTE stmt1; 
	DEALLOCATE PREPARE stmt1;

	
	DROP TABLE IF EXISTS development_mx.TMP_COMMERCIAL;
	CREATE TABLE development_mx.TMP_COMMERCIAL (PRIMARY KEY( SKU_Config ))
	SELECT
		 Sku_Config,
		 COUNT(*)           AS Commercial_Report_Items,
		 SUM( PC_One )       AS Commercial_Report_PC1,
		 SUM( PC_OnePFive ) AS Commercial_Report_PC1_5   ,
		 SUM( PC_OnePFive + WH_cost_per_Item + CS_cost_per_Item ) AS Commercial_Report_PC2   ,
		 
		 SUM(Rev) AS Commercial_Report_Rev     ,
		 SUM(Price) AS Commercial_Report_Price          


	FROM
		 development_mx.Out_SalesReportItem
	WHERE
			 OrderAfterCan = 1
	 AND Date >= NOW() - INTERVAL 30 DAY
	GROUP BY SKU_Config
	;


	SET @s = CONCAT('UPDATE test_linio.A_Catalog_Boost_',CatalogName, ' a
		 INNER JOIN development_mx.TMP_COMMERCIAL b
					ON a.SKU_Config = b.SKU_Config
	SET
	a.Commercial_Report_Items = b.Commercial_Report_Items, 
	a.Commercial_Report_PC1 = b.Commercial_Report_PC1, 
	a.Commercial_Report_PC1_5 = b.Commercial_Report_PC1_5, 
	a.Commercial_Report_PC2 = b.Commercial_Report_PC2, 
	a.Commercial_Report_Rev = b.Commercial_Report_Rev, 
	a.Commercial_Report_Price = b.Commercial_Report_Price
	;');
	PREPARE stmt1 FROM @s;
	EXECUTE stmt1; 
	DEALLOCATE PREPARE stmt1;


	SET @s = CONCAT('SELECT * FROM test_linio.A_Catalog_Boost_',CatalogName);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `get_all_catalogs`(IN `get_all_catalogs` int)
BEGIN
	
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`daniel.h`@`%`*/ /*!50003 PROCEDURE `new_procedure`()
BEGIN
	#Routine body goes here...


#----------------------------Mexico------------------------------------------#


drop TABLE test_linio.A_Master;

drop TABLE test_linio.A_Master_MEX;
create table test_linio.A_Master_MEX
select * from development_mx.A_Master;

ALTER TABLE `test_linio`.`A_Master_MEX` 
ADD COLUMN `assisted_sales_operator` VARCHAR(255) NULL DEFAULT NULL   ;

update test_linio.A_Master_MEX A, bob_live_mx.sales_order B
set A.assisted_sales_operator = B.assisted_sales_operator
where A.OrderNum = B.order_nr;
#Euros 
update test_linio.A_Master_MEX A
set
A.OriginalPrice =round(A.OriginalPrice /16.35,5),
A.Tax =round(A.Tax /16.35,5),
A.CreditNotes =round(A.CreditNotes /16.35,5),
A.Price = round(A.Price/16.35,5),
A.PriceAfterTax =round(A.PriceAfterTax /16.35,5),
A.CouponValue = round(A.CouponValue/16.35,5),
A.CouponValueAfterTax = round(A.CouponValueAfterTax/16.35,5),
A.OtherRev= round(A.OtherRev/16.35,5),
A.PaidPrice = round(A.PaidPrice/16.35,5), 
A.PaidPriceAfterTax = round(A.PaidPriceAfterTax/16.35,5), 
A.ShippingFee =round(A.ShippingFee/16.35,5),
A.DeliveryCostSupplier=round(A.DeliveryCostSupplier/16.35,5),
A.Cost = round(A.Cost/16.35,5), 
A.CostAfterTax =round(A.CostAfterTax/16.35,5),
A.ShippingFee =round(A.ShippingFee/16.35,5),
A.ShippingCost =round(A.ShippingCost /16.35,5),
A.PaymentFees =round(A.PaymentFees /16.35,5),
A.FLWHCost =round(A.FLWHCost /16.35,5),
A.FLCSCost =round(A.FLCSCost /16.35,5)
where A.Country='MEX'
;

create  table test_linio.A_Master
select * from test_linio.A_Master_MEX;

#----------------------------Colombia------------------------------------------#


drop  TABLE test_linio.A_Master_COL;
create table test_linio.A_Master_COL
select * from development_co.A_Master;

ALTER TABLE `test_linio`.`A_Master_COL` 
ADD COLUMN `assisted_sales_operator` VARCHAR(255) NULL DEFAULT NULL   ;

update test_linio.A_Master_COL A, bob_live_co.sales_order B
set A.assisted_sales_operator = B.assisted_sales_operator
where A.OrderNum = B.order_nr;

ALTER TABLE `test_linio`.`A_Master_COL` 
ADD COLUMN `isMPlaceUntil` DATE NULL DEFAULT NULL AFTER  `isMPlaceSince`   ;

ALTER TABLE `test_linio`.`A_Master_COL` 
ADD COLUMN `CreditNotes` decimal(15,5) NULL DEFAULT NULL AFTER  `NewReturning`  ;

ALTER TABLE `test_linio`.`A_Master_COL` 
ADD COLUMN `OtherRev` double NULL DEFAULT NULL AFTER  `Rev` ;


#Euros 
update test_linio.A_Master_COL A
set
A.OriginalPrice =round(A.OriginalPrice /2450,5),
A.Tax =round(A.Tax /2450,5),
A.CreditNotes =round(A.CreditNotes /2450,5),
A.Price = round(A.Price/2450,5),
A.PriceAfterTax =round(A.PriceAfterTax /2450,5),
A.CouponValue = round(A.CouponValue/2450,5),
A.CouponValueAfterTax = round(A.CouponValueAfterTax/2450,5),
A.OtherRev= round(A.OtherRev/2450,5),
A.PaidPrice = round(A.PaidPrice/2450,5), 
A.PaidPriceAfterTax = round(A.PaidPriceAfterTax/2450,5), 
A.ShippingFee =round(A.ShippingFee/2450,5),
A.DeliveryCostSupplier=round(A.DeliveryCostSupplier/2450,5),
A.Cost = round(A.Cost/2450,5), 
A.CostAfterTax =round(A.CostAfterTax/2450,5),
A.ShippingFee =round(A.ShippingFee/2450,5),
A.ShippingCost =round(A.ShippingCost /2450,5),
A.PaymentFees =round(A.PaymentFees /2450,5),
A.FLWHCost =round(A.FLWHCost /2450,5),
A.FLCSCost =round(A.FLCSCost /2450,5)
where A.Country='COL'
;

insert into   test_linio.A_Master
select * from test_linio.A_Master_COL;



#----------------------------Perú------------------------------------------#


drop  TABLE test_linio.A_Master_PER;
create table test_linio.A_Master_PER
select * from development_pe.A_Master;

ALTER TABLE `test_linio`.`A_Master_PER` 
ADD COLUMN `assisted_sales_operator` VARCHAR(255) NULL DEFAULT NULL   ;

update test_linio.A_Master_PER A, bob_live_pe.sales_order B
set A.assisted_sales_operator = B.assisted_sales_operator
where A.OrderNum = B.order_nr;


#Euros 
update test_linio.A_Master_PER A
set
A.OriginalPrice =round(A.OriginalPrice /3.3,5),
A.Tax =round(A.Tax /3.3,5),
A.CreditNotes =round(A.CreditNotes /3.3,5),
A.Price = round(A.Price/3.3,5),
A.PriceAfterTax =round(A.PriceAfterTax /3.3,5),
A.CouponValue = round(A.CouponValue/3.3,5),
A.CouponValueAfterTax = round(A.CouponValueAfterTax/3.3,5),
A.OtherRev= round(A.OtherRev/3.3,5),
A.PaidPrice = round(A.PaidPrice/3.3,5), 
A.PaidPriceAfterTax = round(A.PaidPriceAfterTax/3.3,5), 
A.ShippingFee =round(A.ShippingFee/3.3,5),
A.DeliveryCostSupplier=round(A.DeliveryCostSupplier/3.3,5),
A.Cost = round(A.Cost/3.3,5), 
A.CostAfterTax =round(A.CostAfterTax/3.3,5),
A.ShippingFee =round(A.ShippingFee/3.3,5),
A.ShippingCost =round(A.ShippingCost /3.3,5),
A.PaymentFees =round(A.PaymentFees /3.3,5),
A.FLWHCost =round(A.FLWHCost /3.3,5),
A.FLCSCost =round(A.FLCSCost /3.3,5)
where A.Country='PER'
;

insert into   test_linio.A_Master
select * from test_linio.A_Master_PER;



#----------------------------VENEZUELA------------------------------------------#


drop  TABLE test_linio.A_Master_VEN;
create table test_linio.A_Master_VEN
select * from development_ve.A_Master;

ALTER TABLE `test_linio`.`A_Master_VEN` 
ADD COLUMN `assisted_sales_operator` VARCHAR(255) NULL DEFAULT NULL   ;

update test_linio.A_Master_VEN A, bob_live_ve.sales_order B
set A.assisted_sales_operator = B.assisted_sales_operator
where A.OrderNum = B.order_nr;

ALTER TABLE `test_linio`.`A_Master_VEN` 
ADD COLUMN `isMPlaceUntil` DATE NULL DEFAULT NULL AFTER `isMPlaceSince` ;


#Euros 
update test_linio.A_Master_VEN A
SET 
A.OriginalPrice =round(A.OriginalPrice /70,5),
A.Tax =round(A.Tax /70,5),
A.CreditNotes =round(A.CreditNotes /70,5),
A.Price = round(A.Price/70,5),
A.PriceAfterTax =round(A.PriceAfterTax /70,5),
A.CouponValue = round(A.CouponValue/70,5),
A.CouponValueAfterTax = round(A.CouponValueAfterTax/70,5),
A.OtherRev= round(A.OtherRev/70,5),
A.PaidPrice = round(A.PaidPrice/70,5), 
A.PaidPriceAfterTax = round(A.PaidPriceAfterTax/70,5), 
A.ShippingFee =round(A.ShippingFee/70,5),
A.DeliveryCostSupplier=round(A.DeliveryCostSupplier/70,5),
A.Cost = round(A.Cost/70,5), 
A.CostAfterTax =round(A.CostAfterTax/70,5),
A.ShippingFee =round(A.ShippingFee/70,5),
A.ShippingCost =round(A.ShippingCost /70,5),
A.PaymentFees =round(A.PaymentFees /70,5),
A.FLWHCost =round(A.FLWHCost /70,5),
A.FLCSCost =round(A.FLCSCost /70,5)
where A.Country='VEN'
;

insert into   test_linio.A_Master
select * from test_linio.A_Master_VEN;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `november_attribution`()
begin

delete from attribution_testing.global_attribution_mx;

insert into attribution_testing.global_attribution_mx(date, channel_group, gross_orders, net_orders, gross_revenue, net_revenue, `PC1.5`, new_customers_gross, new_customers) select gross.date, gross.channel, gross.orders, net.orders, gross.revenue/16.35, net.revenue/16.35, net.`PC1.5`/16.35, gross.new_customers, net.new_customers  from (select n.date, a.channel, sum(n.unit*a.percent) as orders, sum(n.revenue*a.percent) as revenue, sum(n.new_customers*a.percent)as new_customers from attribution_testing.november_orders n, attribution_testing.attribution_model_mx a where a.order_nr=n.order_nr and n.obc=1 group by n.date, a.channel)gross left join (select n.date, a.channel, sum(n.unit*a.percent) as orders, sum(n.revenue*a.percent) as revenue, sum(n.new_customers*a.percent) as new_customers, sum(`PC1.5`) as `PC1.5` from attribution_testing.november_orders n, attribution_testing.attribution_model_mx a where a.order_nr=n.order_nr and n.oac=1 group by n.date, a.channel)net on gross.date=net.date and gross.channel=net.channel;

update attribution_testing.global_attribution_mx set week=production.week_iso(date), weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end, yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `percent_calculation`()
begin

delete from attribution_testing.percent_attribution_mx;



set @k:=0;
set @i:=1;
while @i<=100 do
	set @j=1;
	while @j<=@i do
insert into attribution_testing.percent_attribution_mx(new_customers, clicks, numbers) values (@k, @i, @j);
	set @j:=@j+1;
	end while;
	set @i:=@i+1;
end while;

update attribution_testing.percent_attribution_mx set result = numbers/(clicks*(clicks+1)/2);



set @k:=1;
set @i:=1;
while @i<=100 do
	set @j=1;
	while @j<=@i do
insert into attribution_testing.percent_attribution_mx(new_customers, clicks, numbers) values (@k, @i, @j);
	set @j:=@j+1;
	end while;
	set @i:=@i+1;
end while;

update attribution_testing.percent_attribution_mx set result=1 where clicks=1 and numbers=1 and new_customers=1;
update attribution_testing.percent_attribution_mx set result=0.5 where clicks=2 and numbers=1 and new_customers=1;
update attribution_testing.percent_attribution_mx set result=0.5 where clicks=2 and numbers=2 and new_customers=1;
update attribution_testing.percent_attribution_mx set result=0.2/(clicks-2) where clicks>=3 and new_customers=1;
update attribution_testing.percent_attribution_mx set result=0.4 where numbers=1 and clicks>=3 and new_customers=1;
update attribution_testing.percent_attribution_mx set result=0.4 where clicks=numbers and clicks>=3 and new_customers=1;
 

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`roberto.lopez`@`%`*/ /*!50003 PROCEDURE `prc_test`(var INT)
BEGIN
    DECLARE  var2 INT;
    SET var2 = 1;
    SELECT  var2;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`roberto.lopez`@`%`*/ /*!50003 PROCEDURE `prueba`()
BEGIN
set @var = 1;
SET @var2 = 8;

WHILE(@var < 10) DO
set @var2 = @var2 - 1;
set @var = @var + 1;
END WHILE;

select @var2;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`172.18.3.21`*/ /*!50003 PROCEDURE `QV_A_Master`()
BEGIN
	#Routine body goes here...


#----------------------------Mexico------------------------------------------#


drop TABLE test_linio.A_Master;

drop TABLE test_linio.A_Master_MEX;
create table test_linio.A_Master_MEX
select * from development_mx.A_Master;

ALTER TABLE `test_linio`.`A_Master_MEX` 
ADD COLUMN `assisted_sales_operator` VARCHAR(255) NULL DEFAULT NULL   ;

update test_linio.A_Master_MEX A, bob_live_mx.sales_order B
set A.assisted_sales_operator = B.assisted_sales_operator
where A.OrderNum = B.order_nr;
#Euros 
update test_linio.A_Master_MEX A
set
A.OriginalPrice =round(A.OriginalPrice /16.35,5),
A.Tax =round(A.Tax /16.35,5),
A.CreditNotes =round(A.CreditNotes /16.35,5),
A.Price = round(A.Price/16.35,5),
A.PriceAfterTax =round(A.PriceAfterTax /16.35,5),
A.CouponValue = round(A.CouponValue/16.35,5),
A.CouponValueAfterTax = round(A.CouponValueAfterTax/16.35,5),
A.OtherRev= round(A.OtherRev/16.35,5),
A.PaidPrice = round(A.PaidPrice/16.35,5), 
A.PaidPriceAfterTax = round(A.PaidPriceAfterTax/16.35,5), 
A.ShippingFee =round(A.ShippingFee/16.35,5),
A.DeliveryCostSupplier=round(A.DeliveryCostSupplier/16.35,5),
A.Cost = round(A.Cost/16.35,5), 
A.CostAfterTax =round(A.CostAfterTax/16.35,5),
A.ShippingFee =round(A.ShippingFee/16.35,5),
A.ShippingCost =round(A.ShippingCost /16.35,5),
A.PaymentFees =round(A.PaymentFees /16.35,5),
A.FLWHCost =round(A.FLWHCost /16.35,5),
A.FLCSCost =round(A.FLCSCost /16.35,5)
where A.Country='MEX'
;

create  table test_linio.A_Master
select * from test_linio.A_Master_MEX;

#----------------------------Colombia------------------------------------------#


drop  TABLE test_linio.A_Master_COL;
create table test_linio.A_Master_COL
select * from development_co.A_Master;

ALTER TABLE `test_linio`.`A_Master_COL` 
ADD COLUMN `assisted_sales_operator` VARCHAR(255) NULL DEFAULT NULL   ;

update test_linio.A_Master_COL A, bob_live_co.sales_order B
set A.assisted_sales_operator = B.assisted_sales_operator
where A.OrderNum = B.order_nr;


#Euros 
update test_linio.A_Master_COL A
set
A.OriginalPrice =round(A.OriginalPrice /2450,5),
A.Tax =round(A.Tax /2450,5),
A.CreditNotes =round(A.CreditNotes /2450,5),
A.Price = round(A.Price/2450,5),
A.PriceAfterTax =round(A.PriceAfterTax /2450,5),
A.CouponValue = round(A.CouponValue/2450,5),
A.CouponValueAfterTax = round(A.CouponValueAfterTax/2450,5),
A.OtherRev= round(A.OtherRev/2450,5),
A.PaidPrice = round(A.PaidPrice/2450,5), 
A.PaidPriceAfterTax = round(A.PaidPriceAfterTax/2450,5), 
A.ShippingFee =round(A.ShippingFee/2450,5),
A.DeliveryCostSupplier=round(A.DeliveryCostSupplier/2450,5),
A.Cost = round(A.Cost/2450,5), 
A.CostAfterTax =round(A.CostAfterTax/2450,5),
A.ShippingFee =round(A.ShippingFee/2450,5),
A.ShippingCost =round(A.ShippingCost /2450,5),
A.PaymentFees =round(A.PaymentFees /2450,5),
A.FLWHCost =round(A.FLWHCost /2450,5),
A.FLCSCost =round(A.FLCSCost /2450,5)
where A.Country='COL'
;

insert into   test_linio.A_Master
select * from test_linio.A_Master_COL;



#----------------------------Perú------------------------------------------#


drop  TABLE test_linio.A_Master_PER;
create table test_linio.A_Master_PER
select * from development_pe.A_Master;

ALTER TABLE `test_linio`.`A_Master_PER` 
ADD COLUMN `assisted_sales_operator` VARCHAR(255) NULL DEFAULT NULL   ;

update test_linio.A_Master_PER A, bob_live_pe.sales_order B
set A.assisted_sales_operator = B.assisted_sales_operator
where A.OrderNum = B.order_nr;


#Euros 
update test_linio.A_Master_PER A
set
A.OriginalPrice =round(A.OriginalPrice /3.3,5),
A.Tax =round(A.Tax /3.3,5),
A.CreditNotes =round(A.CreditNotes /3.3,5),
A.Price = round(A.Price/3.3,5),
A.PriceAfterTax =round(A.PriceAfterTax /3.3,5),
A.CouponValue = round(A.CouponValue/3.3,5),
A.CouponValueAfterTax = round(A.CouponValueAfterTax/3.3,5),
A.OtherRev= round(A.OtherRev/3.3,5),
A.PaidPrice = round(A.PaidPrice/3.3,5), 
A.PaidPriceAfterTax = round(A.PaidPriceAfterTax/3.3,5), 
A.ShippingFee =round(A.ShippingFee/3.3,5),
A.DeliveryCostSupplier=round(A.DeliveryCostSupplier/3.3,5),
A.Cost = round(A.Cost/3.3,5), 
A.CostAfterTax =round(A.CostAfterTax/3.3,5),
A.ShippingFee =round(A.ShippingFee/3.3,5),
A.ShippingCost =round(A.ShippingCost /3.3,5),
A.PaymentFees =round(A.PaymentFees /3.3,5),
A.FLWHCost =round(A.FLWHCost /3.3,5),
A.FLCSCost =round(A.FLCSCost /3.3,5)
where A.Country='PER'
;

insert into   test_linio.A_Master
select * from test_linio.A_Master_PER;



#----------------------------VENEZUELA------------------------------------------#


drop  TABLE test_linio.A_Master_VEN;
create table test_linio.A_Master_VEN
select * from development_ve.A_Master;

ALTER TABLE `test_linio`.`A_Master_VEN` 
ADD COLUMN `assisted_sales_operator` VARCHAR(255) NULL DEFAULT NULL   ;

update test_linio.A_Master_VEN A, bob_live_ve.sales_order B
set A.assisted_sales_operator = B.assisted_sales_operator
where A.OrderNum = B.order_nr;


#Euros 
update test_linio.A_Master_VEN A
SET 
A.OriginalPrice =round(A.OriginalPrice /70,5),
A.Tax =round(A.Tax /70,5),
A.CreditNotes =round(A.CreditNotes /70,5),
A.Price = round(A.Price/70,5),
A.PriceAfterTax =round(A.PriceAfterTax /70,5),
A.CouponValue = round(A.CouponValue/70,5),
A.CouponValueAfterTax = round(A.CouponValueAfterTax/70,5),
A.OtherRev= round(A.OtherRev/70,5),
A.PaidPrice = round(A.PaidPrice/70,5), 
A.PaidPriceAfterTax = round(A.PaidPriceAfterTax/70,5), 
A.ShippingFee =round(A.ShippingFee/70,5),
A.DeliveryCostSupplier=round(A.DeliveryCostSupplier/70,5),
A.Cost = round(A.Cost/70,5), 
A.CostAfterTax =round(A.CostAfterTax/70,5),
A.ShippingFee =round(A.ShippingFee/70,5),
A.ShippingCost =round(A.ShippingCost /70,5),
A.PaymentFees =round(A.PaymentFees /70,5),
A.FLWHCost =round(A.FLWHCost /70,5),
A.FLCSCost =round(A.FLCSCost /70,5)
where A.Country='VEN'
;

insert into   test_linio.A_Master
select * from test_linio.A_Master_VEN;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`julian.buitrago`@`%`*/ /*!50003 PROCEDURE `test`()
BEGIN


#----------------------------Colombia----------------------------------------#

drop TABLE IF EXISTS test_linio.A_Master;

drop  TABLE IF EXISTS test_linio.A_Master_COL;
create table test_linio.A_Master_COL LIKE development_co_project.A_Master;
INSERT INTO test_linio.A_Master_COL
select * from development_co_project.A_Master;

create table test_linio.A_Master
select * from test_linio.A_Master_COL;

#----------------------------Mexico------------------------------------------#

drop TABLE IF EXISTS test_linio.A_Master_MEX;
create table test_linio.A_Master_MEX
select * from development_mx.A_Master;

insert into   test_linio.A_Master
select * from test_linio.A_Master_MEX;

#----------------------------Perú--------------------------------------------#

drop  TABLE IF EXISTS test_linio.A_Master_PER;
create table test_linio.A_Master_PER
select * from development_pe.A_Master;


insert into   test_linio.A_Master
select * from test_linio.A_Master_PER;

#----------------------------VENEZUELA------------------------------------------#

drop  TABLE IF EXISTS test_linio.A_Master_VEN;
create table test_linio.A_Master_VEN
select * from development_ve.A_Master;

insert into   test_linio.A_Master
select * from test_linio.A_Master_VEN;

#----------------------------EUROS------------------------------------------#

update test_linio.A_Master A , development_mx.A_E_BI_ExchangeRate B
set
A.OriginalPrice =round(A.OriginalPrice /B.XR,5),
A.Tax =round(A.Tax /B.XR,5),
A.CreditNotes =round(A.CreditNotes /B.XR,5),
A.Price = round(A.Price/B.XR,5),
A.PriceAfterTax =round(A.PriceAfterTax /B.XR,5),
A.CouponValue = round(A.CouponValue/B.XR,5),
A.CouponValueAfterTax = round(A.CouponValueAfterTax/B.XR,5),
A.NonMKTCouponAfterTax= round(A.NonMKTCouponAfterTax/B.XR,5),
A.OtherRev= round(A.OtherRev/B.XR,5),
A.PaidPrice = round(A.PaidPrice/B.XR,5), 
A.PaidPriceAfterTax = round(A.PaidPriceAfterTax/B.XR,5), 
A.ShippingFee =round(A.ShippingFee/B.XR,5),
A.DeliveryCostSupplier=round(A.DeliveryCostSupplier/B.XR,5),
A.Cost = round(A.Cost/B.XR,5), 
A.CostAfterTax =round(A.CostAfterTax/B.XR,5),
A.ShippingFee =round(A.ShippingFee/B.XR,5),
A.ShippingCost =round(A.ShippingCost /B.XR,5),
A.ShipmentCost=round(A.ShipmentCost /B.XR,5),
A.PaymentFees =round(A.PaymentFees/B.XR,5),
A.FLWHCost =round(A.FLWHCost/B.XR,5),
A.FLCSCost =round(A.FLCSCost/B.XR,5),
A.GrandTotal=round(A.GrandTotal/B.XR,5),
A.Rev=round(A.Rev/B.XR,5),
A.COGS=round(A.COGS/B.XR,5),
A.PCOne=round(A.PCOne/B.XR,5),
A.PCOnePFive=round(A.PCOnePFive/B.XR,5),
A.PCTwo=round(A.PCTwo/B.XR,5)
where B.Country= A.Country and B.Month_Num= A.MonthNum
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `test_mkt`()
begin

set @days=60;

select  'Marketing Report: start',now();

delete from marketing_report.global_report where datediff(curdate(), date)<@days;

-- Mexico

update production.tbl_order_detail t inner join development_mx.A_Master o on t.item=o.itemID set t.unit_price_after_vat=o.priceaftertax, t.coupon_money_after_vat=o.couponvalueaftertax, t.shipping_fee_after_vat=o.shippingfee, t.costo_after_vat=o.costaftertax, t.delivery_cost_supplier=o.deliverycostsupplier, t.payment_cost=o.paymentfees, t.shipping_cost_per_sku=o.shippingcost, t.wh=o.flwhcost, t.cs=o.flcscost, t.oac=o.orderaftercan, t.obc=o.orderbeforecan, t.returned=o.returns, t.pending=o.pending, t.cancel=o.cancellations where datediff(curdate(), t.date)<@days;

insert into marketing_report.global_report(country, yrmonth, week, date, weekday, channel_group, channel, gross_orders, net_orders, gross_revenue, net_revenue, `PC1.5`, new_customers_gross, new_customers) select 'Mexico', gross.yrmonth, gross.week, gross.date, gross.weekday, gross.channel_group, gross.channel, gross.gross_orders, net.net_orders, gross.gross_revenue, net.net_revenue, net.`PC1.5`, gross.new_customers_gross, net.new_customers from (select yrmonth, production.week_iso(date) as week, date, case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end as weekday,  channel_group, channel, count(distinct order_nr) as gross_orders, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as gross_revenue, sum(new_customers_gross) as new_customers_gross from production.tbl_order_detail where obc=1 group by yrmonth, production.week_iso(date), date, channel_group, channel)gross left join (select yrmonth, production.week_iso(date) as week, date, channel_group, channel, count(distinct order_nr) as net_orders, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(payment_cost,0)-ifnull(shipping_cost_per_sku,0)) as `PC1.5`, sum(new_customers) as new_customers from production.tbl_order_detail where oac=1 group by yrmonth, production.week_iso(date), date, channel_group, channel)net on gross.date=net.date and gross.channel=net.channel and gross.channel_group=net.channel_group where datediff(curdate(), gross.date)<@days order by date desc;

create table marketing_report.temporary(
date date,
channel varchar(255),
channel_group varchar(255),
include int
);

insert into marketing_report.temporary(date, channel_group, channel) select distinct date, channel_group, channel from SEM.campaign_ad_group_mx where datediff(curdate(), date)<@days;

update marketing_report.temporary t inner join marketing_report.global_report g on t.channel_group=g.channel_group and t.channel=g.channel and t.date=g.date set include = 1 where country='Mexico' and datediff(curdate(), g.date)<@days;

insert into marketing_report.global_report(country, date, channel_group, channel) select 'Mexico', date, channel_group, channel from marketing_report.temporary where include is null;

-- Cost

update marketing_report.global_report g set visits = (select sum(visits) from SEM.campaign_ad_group_mx c where c.channel_group=g.channel_group and c.channel=g.channel and c.date=g.date) where country='Mexico' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set visits = (select sum(visits) from production.mobileapp_campaign c where c.date=g.date) where channel_group='Mobile App' and channel='Mobile App' and country='Mexico' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_mx r where channel='Criteo' and g.date=r.date) where channel='Criteo' and country='Mexico' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(cost) from facebook.facebook_optimization_region_mx r where g.date=r.date) where channel='Facebook Ads' and country='Mexico' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_mx r where channel='GLG' and g.date=r.date) where channel='Glg' and country='Mexico' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(cost) from SEM.google_optimization_ad_group_mx r where channel='GDN' and g.date=r.date) where channel='Google Display Network' and country='Mexico' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_mx r where channel='Pampa Network' and g.date=r.date) where channel='Pampa Network' and country='Mexico' and datediff(curdate(), g.date)<@days;  

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_mx r where channel='Main ADV' and g.date=r.date) where channel='Main ADV' and country='Mexico' and datediff(curdate(), g.date)<@days;  

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_mx r where channel='My Things' and g.date=r.date) where channel='My Things' and country='Mexico' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_mx r where channel='Avazu' and g.date=r.date) where channel='Avazu' and country='Mexico' and datediff(curdate(), g.date)<@days;  

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_mx r where channel='Trade Tracker' and g.date=r.date) where channel='Trade Tracker' and country='Mexico' and datediff(curdate(), g.date)<@days;  

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_mx r where channel='FBX' and g.date=r.date) where channel='Facebook R.' and country='Mexico' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(cost) from SEM.google_optimization_ad_group_mx r where channel='SEM' and g.date=r.date) where channel='SEM' and country='Mexico' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(ad_cost) from SEM.campaign_ad_group_mx r where source = 'google' and medium = 'CPC' and campaign like 'brandb%' and g.date=r.date) where channel='SEM Branded' and country='Mexico' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_mx r where channel='Sociomantic' and g.date=r.date) where channel='Sociomantic' and country='Mexico' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_mx r where channel='Veinteractive' and g.date=r.date) where channel='Newsletter' and country='Mexico' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_mx r where channel='Vizury' and g.date=r.date) where channel='Vizury' and country='Mexico' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from facebook.fanpage_optimization_mx r where g.date=r.date) where channel='FB Posts' and country='Mexico' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(spent) from facebook.facebook_campaign r where g.date=r.date and campaign like 'MEX_darkpost%') where channel_group='Facebook Ads' and  channel='Dark Post' and country='Mexico' and datediff(curdate(), g.date)<@days;

create table marketing_report.temporary_offline(
date date,
count int
);

insert into marketing_report.temporary_offline select date, count(distinct channel) from marketing_report.global_report where channel_group='Offline Marketing' and country='Mexico' and datediff(curdate(), date)<@days group by date;

update marketing_report.global_report g set cost = (select sum(cost)/count from marketing_report.offline_mx t, marketing_report.temporary_offline r where g.date=r.date and t.date=r.date) where channel_group='Offline Marketing' and country='Mexico' and datediff(curdate(), g.date)<@days;

drop table marketing_report.temporary_offline;

create table marketing_report.temporary_tele_sales(
date date,
count int
);

insert into marketing_report.temporary_tele_sales select date, count(distinct channel) from marketing_report.global_report where channel_group='Tele Sales' and country='Mexico' and datediff(curdate(), date)<@days group by date;

update marketing_report.global_report g set cost = (select sum(cost)/count from marketing_report.tele_sales_mx t, marketing_report.temporary_tele_sales r where g.date=r.date and t.date=r.date) where channel_group='Tele Sales' and datediff(curdate(), g.date)<@days and country='Mexico';

-- update marketing_report.global_report g set cost = cost + (select sum(after_vat_coupon)/count from development_mx.Out_SalesReportItem i, marketing_report.temporary_tele_sales t where (coupon_code like '%cbit%' or coupon_code like '%CCEMP%') and OrderAfterCan = 1 and i.date=g.date and t.date=i.date group by i.Date) where channel_group='Tele Sales' and country='Mexico';

drop table marketing_report.temporary_tele_sales;

create table marketing_report.temporary_partnerships(
date date,
count int
);

insert into marketing_report.temporary_partnerships select date, count(distinct channel) from marketing_report.global_report where channel_group='Partnerships' and country='Mexico' and datediff(curdate(), date)<@days group by date;

-- update marketing_report.global_report g set cost = cost + (select sum(Interest)/count from development_mx.Out_SalesReportItem i, marketing_report.temporary_partnerships t where OrderAfterCan = 1 and  MCI = 1  and i.date=g.date and t.date=i.date group by i.Date) where channel_group='Partnerships' and country='Mexico';

drop table marketing_report.temporary_partnerships;

-- Moving Avg

create table marketing_report.temporary_global_report(
date date,
channel_group varchar(255),
channel varchar(255),
gross_orders int,
net_orders int,
e_orders float,
ee_orders float,
gross_revenue float,
net_revenue float,
e_revenue float,
ee_revenue float,
new_customers_gross int,
new_customers int,
e_new_customers float,
ee_new_customers float,
cancel float,
returns float,
voucher float
);

create index temporary_mkt on marketing_report.temporary_global_report(date, channel_group, channel);

insert into marketing_report.temporary_global_report(date, channel_group, channel, gross_orders, net_orders, gross_revenue, net_revenue, new_customers_gross, new_customers) select date, channel_group, channel, gross_orders, net_orders, gross_revenue, net_revenue, new_customers_gross, new_customers from marketing_report.global_report where country='Mexico' and datediff(curdate(), date)<@days;

-- Orders

set @date=date_sub(curdate(), interval @days day);

while @date!=curdate() do
update marketing_report.temporary_global_report t set e_orders = (select (avg(g.net_orders/g.gross_orders)) from marketing_report.global_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Mexico') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_global_report t set e_orders = gross_orders*e_orders where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_orders = case when e_orders>net_orders then e_orders else net_orders end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set cancel = (select count(distinct order_nr) from production.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set returns = (select count(distinct order_nr) from production.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.returned=1) where datediff(curdate(), date)<@days; 

update marketing_report.temporary_global_report t set cancel = cancel/gross_orders, returns = returns/gross_orders where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set ee_orders = gross_orders*(1-ifnull(cancel,0)-ifnull(returns,0)) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_orders = case when e_orders>ee_orders then ee_orders else e_orders end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_orders = case when e_orders>net_orders then e_orders else net_orders end where datediff(curdate(), date)<@days;

-- Revenue

set @date=date_sub(curdate(), interval @days day);

while @date!=curdate() do
update marketing_report.temporary_global_report t set e_revenue = (select (avg(g.net_revenue/g.gross_revenue)) from marketing_report.global_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Mexico') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_global_report t set e_revenue = gross_revenue*e_revenue where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_revenue = case when e_revenue>net_revenue then e_revenue else net_revenue end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set cancel = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set returns = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.returned=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set voucher = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.coupon_code is not null and d.oac=1) where datediff(curdate(), date)<@days; 

update marketing_report.temporary_global_report t set cancel = cancel/gross_revenue, returns = returns/gross_revenue, voucher=voucher/gross_revenue where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set ee_revenue = gross_revenue*(1-ifnull(cancel,0)-ifnull(returns,0)-ifnull(voucher,0)) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_revenue = case when e_revenue>ee_revenue then ee_revenue else e_revenue end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_revenue = case when e_revenue>net_revenue then e_revenue else net_revenue end where datediff(curdate(), date)<@days;

-- New Customers

set @date=date_sub(curdate(), interval @days day);

while @date!=curdate() do
update marketing_report.temporary_global_report t set e_new_customers = (select (avg(g.new_customers/g.new_customers_gross)) from marketing_report.global_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Mexico') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_global_report t set e_new_customers = new_customers_gross*e_new_customers where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_new_customers = case when e_new_customers>new_customers then e_new_customers else new_customers end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set cancel = (select sum(new_customers_gross) from production.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set returns = (select sum(new_customers_gross) from production.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.returned=1) where datediff(curdate(), date)<@days; 

update marketing_report.temporary_global_report t set cancel = cancel/new_customers_gross, returns = returns/new_customers_gross where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set ee_new_customers = new_customers_gross*(1-ifnull(cancel,0)-ifnull(returns,0)) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_new_customers = case when e_new_customers>ee_new_customers then ee_new_customers else e_new_customers end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_new_customers = case when e_new_customers>new_customers then e_new_customers else new_customers end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t inner join marketing_report.global_report g on t.date=g.date and t.channel_group=g.channel_group and t.channel=g.channel set g.e_orders=t.e_orders, g.e_revenue=t.e_revenue, g.e_new_customers=t.e_new_customers where country = 'Mexico' and datediff(curdate(), t.date)<@days;

-- Exchange Rate

update marketing_report.global_report s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, e_revenue=e_revenue/xr, a_revenue=a_revenue/xr, `PC1.5`=`PC1.5`/xr where s.country = 'Mexico' and r.country='MEX' and datediff(curdate(), s.date)<@days; 

update marketing_report.global_report set cost = cost/16.35 where channel= 'SEM Branded' and country = 'Mexico' and datediff(curdate(), date)<@days;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `Viviane_normalize_cat`()
BEGIN
	#Routine body goes here...

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`daniel.h`@`%`*/ /*!50003 PROCEDURE `_Navicat_Temp_Stored_Proc`()
BEGIN




#----------------------------Mexico------------------------------------------#

drop TABLE test_linio.A_Master;

#drop TABLE test_linio.A_Master_MEX;
#create table test_linio.A_Master_MEX
#select * from development_mx.A_Master;

create table test_linio.A_Master
select * from development_mx.A_Master;

#----------------------------Colombia----------------------------------------#
/*
drop  TABLE test_linio.A_Master_COL;
create table test_linio.A_Master_COL
select * from development_co.A_Master;
*/
insert into   test_linio.A_Master
select * from development_co.A_Master;

#----------------------------Perú--------------------------------------------#
/*
drop  TABLE test_linio.A_Master_PER;
create table test_linio.A_Master_PER
select * from development_pe.A_Master;
*/

insert into   test_linio.A_Master
select * from development_pe.A_Master;

#----------------------------VENEZUELA------------------------------------------#
/*
drop  TABLE test_linio.A_Master_VEN;
create table test_linio.A_Master_VEN
select * from development_ve.A_Master;
*/
insert into   test_linio.A_Master
select * from development_ve.A_Master;

#----------------------------EUROS------------------------------------------#

update test_linio.A_Master A , development_mx.A_E_BI_ExchangeRate B
set
A.OriginalPrice =round(A.OriginalPrice /B.XR,5),
A.Tax =round(A.Tax /B.XR,5),
A.CreditNotes =round(A.CreditNotes /B.XR,5),
A.Price = round(A.Price/B.XR,5),
A.PriceAfterTax =round(A.PriceAfterTax /B.XR,5),
A.CouponValue = round(A.CouponValue/B.XR,5),
A.CouponValueAfterTax = round(A.CouponValueAfterTax/B.XR,5),
A.NonMKTCouponAfterTax= round(A.NonMKTCouponAfterTax/B.XR,5),
A.OtherRev= round(A.OtherRev/B.XR,5),
A.PaidPrice = round(A.PaidPrice/B.XR,5), 
A.PaidPriceAfterTax = round(A.PaidPriceAfterTax/B.XR,5), 
A.ShippingFee =round(A.ShippingFee/B.XR,5),
A.DeliveryCostSupplier=round(A.DeliveryCostSupplier/B.XR,5),
A.Cost = round(A.Cost/B.XR,5), 
A.CostAfterTax =round(A.CostAfterTax/B.XR,5),
A.ShippingFee =round(A.ShippingFee/B.XR,5),
A.ShippingCost =round(A.ShippingCost /B.XR,5),
A.ShipmentCost=round(A.ShipmentCost /B.XR,5),
A.PaymentFees =round(A.PaymentFees/B.XR,5),
A.FLWHCost =round(A.FLWHCost/B.XR,5),
A.FLCSCost =round(A.FLCSCost/B.XR,5),
A.GrandTotal=round(A.GrandTotal/B.XR,5),
A.Rev=round(A.Rev/B.XR,5),
A.COGS=round(A.COGS/B.XR,5),
A.PCOne=round(A.PCOne/B.XR,5),
A.PCOnePFive=round(A.PCOnePFive/B.XR,5),
A.PCTwo=round(A.PCTwo/B.XR,5)
where B.Country= A.Country and B.Month_Num= A.MonthNum
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:04:25
