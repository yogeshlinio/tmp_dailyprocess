-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: boosting_co
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'boosting_co'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `1_Gather_Boosting_Info_CO`()
BEGIN
	#Create Table
	DROP TABLE IF EXISTS boosting_co.Boost_Catalog_CO;
	CREATE TABLE boosting_co.Boost_Catalog_CO(
		#General
		id MEDIUMINT NOT NULL AUTO_INCREMENT,
		SKU_Config VARCHAR(80) NOT NULL,
		SKU_Name VARCHAR(250) NOT NULL,
		Catalog VARCHAR(250) NOT NULL,
		Average_Position DECIMAL (15,5) NOT NULL,
		Last_Position INT,
		Last_Boost INT,
		Novelty INT NOT NULL,
		Days_Visible INT NOT NULL,
		Is_Visible tinyint DEFAULT 0,
		Is_Market_Place BIT(1) NOT NULL,
		Is_Sell_List BIT(1) NOT NULL,
		Is_Consignment SMALLINT NOT NULL,
		Is_Bundle tinyint DEFAULT 0,
		 
		#Commercial REPORT
		Items_Sold_Last_30_Days   INT NOT NULL,
		PC1_Last_30_Days     DECIMAL(15,5) NOT NULL,
		PC1_5_Last_30_Days   DECIMAL(15,5) NOT NULL,
		PC2_Last_30_Days     DECIMAL(15,5) NOT NULL,
		Revenue_Last_30_Days DECIMAL(15,5) NOT NULL,
	
		#Stock
		Inventory_Stock_Type         VARCHAR(15) NOT NULL,
		Inventory_Stock_Revenue        DECIMAL(15,5) NOT NULL,
		Inventory_Stock_Items INT NOT NULL,
		Days_In_Inventory_Stock INT NOT NULL,
		Supplier_Stock INT NOT NULL,

		#Weekly coverage of sales
		Weekly_Cov DECIMAL(15,5) NOT NULL,
		
		#Daily data
		Per_Day_Items    DECIMAL(15,5) NOT NULL,
		Per_Day_Revenue     DECIMAL(15,5) NOT NULL,
		Per_Day_PC1_5 DECIMAL(15,5) NOT NULL,
		
		#Bob live pricing
		Price DECIMAL(15,5) NOT NULL,
		Discount DECIMAL(15,5),
		
		PRIMARY KEY (id)
	);
	CREATE INDEX SKU_Config_And_Catalog on boosting_co.Boost_Catalog_CO(SKU_Config, Catalog);
	CREATE INDEX SKU_Config on boosting_co.Boost_Catalog_CO(SKU_Config);

	#Clan empty entrences for the Boost_Product_Position_In_Catalog_CO table
	DELETE FROM boosting_co.Boost_Product_Position_In_Catalog_CO WHERE SKU_Config = "";
	DELETE FROM boosting_co.Boost_Product_Position_In_Catalog_CO WHERE Catalog = "";
	
	#INSERT SKUS WITH VISIBLE IN THE LAS 30 DAYS
	INSERT boosting_co.Boost_Catalog_CO(SKU_Config, Catalog, Average_Position)
	SELECT 
		SKU_Config,
		a.Catalog,
		AVG(numb_of_position) AS Average_Position
	FROM boosting_co.Boost_Product_Position_In_Catalog_CO a
	INNER JOIN boosting_co.Boost_Weights_CO b ON a.Catalog = b.Catalog
	WHERE  date_time >=  NOW() - INTERVAL 5 DAY AND b.Active = 1
	GROUP BY Catalog, SKU_Config;

	#Days visible for daily data. (Set of 30 days)
	UPDATE boosting_co.Boost_Catalog_CO a
	INNER JOIN (SELECT  SKU_Config, COUNT(DISTINCT DATE(date_time)) AS days
		FROM boosting_co.Boost_Product_Position_In_Catalog_CO
		WHERE date_time >=  NOW() - INTERVAL 30 DAY
		GROUP   BY  SKU_Config) b
		ON a.SKU_Config = b.SKU_Config
	SET 
		a.Days_Visible =  b.days;
	
	#Exclude deleted SKUs
	DELETE FROM boosting_co.Boost_Catalog_CO
	WHERE SKU_Config NOT IN (SELECT SKU FROM bob_live_co.catalog_config);
	
	#INSERT LAST POSITION
	UPDATE boosting_co.Boost_Catalog_CO a
	INNER JOIN (SELECT SKU_Config, numb_of_position, catalog
		FROM boosting_co.Boost_Product_Position_In_Catalog_CO
		GROUP BY id ORDER BY date_time DESC) AS b
			ON a.SKU_Config = b.SKU_Config AND a.Catalog = b.catalog
		SET
			a.Last_Position =  b.numb_of_position;
	
	#LAST BOOST
	DROP TABLE IF EXISTS boosting_co.Boost_TMP;
	CREATE TABLE boosting_co.Boost_TMP (PRIMARY KEY( SKU_Config ))
	SELECT
		catalog_config.sku AS SKU_Config,
		catalog_product_boost.boost AS Last_Boost
	FROM
		bob_live_co.catalog_product_boost
	INNER JOIN bob_live_co.catalog_config ON catalog_product_boost.fk_catalog_config = catalog_config.id_catalog_config;
	
	UPDATE boosting_co.Boost_Catalog_CO a, boosting_co.Boost_TMP b
		SET
			a.Last_Boost = b.Last_Boost
		WHERE a.SKU_Config = b.SKU_Config;
	
	#UPDATE COMMETCIAL
	DROP TABLE IF EXISTS boosting_co.Boost_TMP;
	CREATE TABLE boosting_co.Boost_TMP (PRIMARY KEY( SKU_Config ))
	SELECT
		 Sku_Config,
		 SKU_Name,
		 isVisible AS Is_Visible,
		 isMarketPlace AS Is_Market_Place
	FROM development_co_project.A_Master_Catalog
	GROUP BY Sku_Config;
	CREATE INDEX SKU_Config ON boosting_co.Boost_TMP(SKU_Config);

	UPDATE boosting_co.Boost_Catalog_CO a, boosting_co.Boost_TMP b
		SET
			a.SKU_Name = b.SKU_Name,
			a.Is_Visible = b.Is_Visible,
			a.Is_Market_Place = b.Is_Market_Place
		WHERE a.SKU_Config = b.SKU_Config;
	
	#SELL LIST
	DROP TABLE IF EXISTS boosting_co.Boost_TMP;
	CREATE TABLE boosting_co.Boost_TMP (PRIMARY KEY( SKU_Config ))
	SELECT
		Sku_Config,
		max_days_in_stock,
		average_remaining_days
	FROM operations_co.out_stock_hist
	WHERE (in_stock = 1 AND
				reserved = 0 AND
				fulfillment_type_bob <> 'Consignment' AND
				max_days_in_stock > 30 AND
				average_remaining_days > 90) OR
				(in_stock = 1 AND
				reserved = 0 AND
				fulfillment_type_bob <> 'Consignment' AND
				max_days_in_stock > 30 AND
				average_remaining_days IS NULL)
	GROUP BY SKU_Config;

	UPDATE boosting_co.Boost_Catalog_CO a, boosting_co.Boost_TMP b
		SET
			a.Is_Sell_List = "1"
		WHERE a.SKU_Config = b.SKU_Config;
	
	#CONSIGNMENT
	DROP TABLE IF EXISTS boosting_co.Boost_TMP;
	CREATE TABLE boosting_co.Boost_TMP (PRIMARY KEY( SKU_Config ))
	SELECT
		Sku_Config,
		fulfillment_type_bob
	FROM operations_co.out_stock_hist
	WHERE in_stock = 1 AND
				reserved = 0 AND
				fulfillment_type_bob = 'Consignment'
	GROUP BY Sku_Config;

	UPDATE boosting_co.Boost_Catalog_CO a, boosting_co.Boost_TMP b
		SET
			a.Is_Consignment = 1
		WHERE a.SKU_Config = b.SKU_Config;
	
	#BUNDLES
	#Products that have a free product attached need to be tracked because the SKU doesn't show sales.
	UPDATE boosting_co.Boost_Catalog_CO a, bob_live_co.catalog_config b
		SET
			a.Is_Bundle = 1
		WHERE b.description LIKE '%"bundle-data"%' AND a.SKU_Config = b.sku;
	
	UPDATE boosting_co.Boost_Catalog_CO a, (SELECT LEFT(sku,17)as SKU_Config FROM bob_live_co.bundle_item b
		INNER JOIN (SELECT id_bundle,status FROM  bob_live_co.bundle ) c
		ON b.fk_bundle = c.id_bundle
		WHERE b.leader = 1 AND c.status = "active") d
		SET
			a.Is_Bundle = 1
		WHERE 
			a.SKU_Config = d.SKU_Config;

	#UPDATE Stock
	UPDATE boosting_co.Boost_Catalog_CO a
		 INNER JOIN ( SELECT SKU_Config, 
												 fulfillment_type_real,
												 SUM(cost_w_o_vat) AS cost_w_o_vat,
												 AVG(days_in_stock) AS daysInStock
										FROM operations_co.out_stock_hist
										WHERE in_stock = 1 AND reserved = 0
								 GROUP BY sku_config ) b 
						 ON a.SKU_Config = b.SKU_Config
	SET
		 a.Inventory_Stock_Type = b.fulfillment_type_real,
		 a.Inventory_Stock_Revenue = b.cost_w_o_vat,
		 a.Days_In_Inventory_Stock = b.daysInStock;

	DROP TABLE IF EXISTS boosting_co.Boost_TMP;
	CREATE TABLE boosting_co.Boost_TMP(
		SKU_Config VARCHAR(255),
		Warehouse_Stock INT,
		Supplier_Stock INT,
		Item_Reserved INT,
		Inventory_Stock_Items INT
	);
	CREATE INDEX SKU_Config ON boosting_co.Boost_TMP(SKU_Config);
	INSERT INTO boosting_co.Boost_TMP (SKU_Config, Warehouse_Stock, Supplier_Stock, Item_Reserved)
	SELECT 	LEFT(catalog_simple.sku,17) AS SKU_Config,
					SUM(IFNULL((SELECT CAST(quantity AS SIGNED INT) FROM bob_live_co.catalog_warehouse_stock WHERE catalog_warehouse_stock.fk_catalog_simple = catalog_simple.id_catalog_simple),0)) AS Warehouse_Stock,
					SUM(IFNULL((SELECT CAST(quantity AS SIGNED INT) FROM bob_live_co.catalog_supplier_stock WHERE catalog_supplier_stock.fk_catalog_simple = catalog_simple.id_catalog_simple),0)) AS Supplier_Stock,
					SUM(((SELECT COUNT(*) FROM bob_live_co.sales_order_item JOIN bob_live_co.sales_order ON fk_sales_order = id_sales_order WHERE fk_sales_order_process IN (33,5,27,26,31,8,23,28,32,34,29,9,30) AND is_reserved = 1 AND sales_order_item.sku = catalog_simple.sku) + IFNULL((SELECT SUM(catalog_stock_shared.reserved) FROM bob_live_co.catalog_stock_shared WHERE catalog_stock_shared.sku = catalog_simple.sku GROUP BY catalog_stock_shared.sku), 0))) AS Item_Reserved
		FROM bob_live_co.catalog_simple GROUP BY SKU_Config;
	
	UPDATE boosting_co.Boost_TMP
		SET Inventory_Stock_Items = Warehouse_Stock - Item_Reserved;

	UPDATE boosting_co.Boost_Catalog_CO a, boosting_co.Boost_TMP b
		SET
			a.Inventory_Stock_Items = b.Inventory_Stock_Items,
			a.Supplier_Stock = b.Supplier_Stock
		WHERE a.SKU_Config = b.SKU_Config;
	
	#COMMERCIAL STUFF
	DROP TABLE IF EXISTS boosting_co.Boost_TMP;
	CREATE TABLE boosting_co.Boost_TMP (PRIMARY KEY( SKU_Config ))
	SELECT
		 SkuConfig AS SKU_Config,
		 COUNT(*) AS Items_Sold_Last_30_Days,
		 SUM(PCOne) AS PC1_Last_30_Days,
		 SUM(PCOnePFive ) AS PC1_5_Last_30_Days,
		 SUM(PCOnePFive + FLWHCost + FLCSCost) AS PC2_Last_30_Days,
		 SUM(Rev) AS Revenue_Last_30_Days
	FROM
		 development_co_project.A_Master
	WHERE
			 OrderAfterCan = 1
	 AND Date >= NOW() - INTERVAL 30 DAY
	GROUP BY SKU_Config;
	CREATE INDEX SKU_Config ON boosting_co.Boost_TMP(SKU_Config);

	UPDATE boosting_co.Boost_Catalog_CO a
		 INNER JOIN boosting_co.Boost_TMP b
					ON a.SKU_Config = b.SKU_Config
	SET
		a.Items_Sold_Last_30_Days = b.Items_Sold_Last_30_Days, 
		a.PC1_Last_30_Days = b.PC1_Last_30_Days, 
		a.PC1_5_Last_30_Days = b.PC1_5_Last_30_Days, 
		a.PC2_Last_30_Days = b.PC2_Last_30_Days, 
		a.Revenue_Last_30_Days = b.Revenue_Last_30_Days;

	#NOVELTY
	DROP TABLE IF EXISTS boosting_co.Boost_TMP;
	CREATE TABLE boosting_co.Boost_TMP (PRIMARY KEY( SKU_Config ))
	SELECT
			sku AS SKU_Config,
			created_at
	FROM
		 bob_live_co.catalog_config
	GROUP BY sku;
	CREATE INDEX SKU_Config ON boosting_co.Boost_TMP(SKU_Config);

	UPDATE boosting_co.Boost_Catalog_CO a, boosting_co.Boost_TMP b
		SET
			a.Novelty = DATEDIFF(CURDATE(), b.created_at)
		WHERE a.SKU_Config = b.SKU_Config;

	#SET SKUS BASED ON THE Days_Visible
	UPDATE boosting_co.Boost_Catalog_CO
	SET
		Per_Day_Items = Items_Sold_Last_30_Days/Days_Visible,
		Per_Day_Revenue = Revenue_Last_30_Days/Days_Visible,
		Per_Day_PC1_5	= PC1_5_Last_30_Days/Days_Visible;
	
	#Future weekly coverage of sales
	DROP TABLE IF EXISTS boosting_co.Boost_TMP;
	CREATE TABLE boosting_co.Boost_TMP(
		SKU_Config VARCHAR(255),
		Warehouse_Stock INT
	);
	CREATE INDEX SKU_Config ON boosting_co.Boost_TMP(SKU_Config);
	INSERT INTO boosting_co.Boost_TMP (SKU_Config, Warehouse_Stock)
	SELECT 	LEFT(development_co_project.tbl_catalog_product_stock.sku,17) AS SKU_Config,
					SUM(IFNULL((SELECT CAST(ownstock AS SIGNED INT)),0)) AS Warehouse_Stock
		FROM development_co_project.tbl_catalog_product_stock GROUP BY SKU_Config;

	UPDATE boosting_co.Boost_Catalog_CO a, boosting_co.Boost_TMP b
	SET
		a.Weekly_Cov = (b.Warehouse_Stock /a.Per_Day_Items)/7
	WHERE a.SKU_Config = b.SKU_Config;
	
	#SET PRICE
	DROP TABLE IF EXISTS boosting_co.Boost_TMP;
	CREATE TABLE boosting_co.Boost_TMP (PRIMARY KEY( SKU_Config ))
	SELECT
		LEFT(sku,17) AS SKU_Config,
		price AS Price
	FROM bob_live_co.catalog_simple
	GROUP BY SKU_Config;
	CREATE INDEX SKU_Config ON boosting_co.Boost_TMP(SKU_Config);
	
	UPDATE boosting_co.Boost_Catalog_CO a, boosting_co.Boost_TMP b
		SET
			a.Price = b.Price
		WHERE a.SKU_Config = b.SKU_Config;

	#SET DISCOUNT
	DROP TABLE IF EXISTS boosting_co.Boost_TMP;
	CREATE TABLE boosting_co.Boost_TMP (PRIMARY KEY( SKU_Config ))
	SELECT
		LEFT(sku,17) AS SKU_Config,
		(price-special_price)/price AS Discount
	FROM bob_live_co.catalog_simple
		WHERE special_from_date < NOW() AND NOW() < special_to_date
	GROUP BY SKU_Config;
	CREATE INDEX SKU_Config ON boosting_co.Boost_TMP(SKU_Config);
	
	UPDATE boosting_co.Boost_Catalog_CO a, boosting_co.Boost_TMP b
		SET
			a.Discount = b.Discount
		WHERE a.SKU_Config = b.SKU_Config;
	DROP TABLE IF EXISTS boosting_co.Boost_TMP;

	#Separete into cetegories and calculate boost scores.
	CALL boosting_co.2_Boost_Existing_Catalogs_CO();

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `2_Boost_Existing_Catalogs_CO`()
BEGIN
	#CREATES ALL THE INDIVIDUAL TABLES FOR ALL EXISTING CATALOGS
	DECLARE done INT DEFAULT 0;
  DECLARE v_cat VARCHAR(50) DEFAULT NULL;
  #declare cursor
  DECLARE cur1 CURSOR FOR SELECT DISTINCT catalog FROM  boosting_co.Boost_Catalog_CO;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

	DROP TABLE IF EXISTS boosting_co.Final_Boost_CO;
		CREATE TABLE boosting_co.Final_Boost_CO(
			Catalog VARCHAR(250) NOT NULL,
			TopBucket INT NOT NULL,
			SKU_Config VARCHAR(250) NOT NULL,
			Score INT NOT NULL
		);
	
	OPEN cur1;
		cur1_loop:LOOP
			FETCH cur1 INTO v_cat;
			IF done = 1 THEN
				LEAVE cur1_loop;
			END IF;
			CALL boosting_co.3_Create_Catalog_Table_CO(v_cat);
		END LOOP cur1_loop;
  CLOSE cur1;
	SET done = 0;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `3_Create_Catalog_Table_CO`(IN cat VARCHAR(255))
BEGIN
	SET @CatalogName = cat;
	
	#CREATE TABLE
	SET @s = CONCAT('DROP TABLE IF EXISTS boosting_co.Boost_Catalog_',@CatalogName);
	PREPARE stmt1 FROM @s; EXECUTE stmt1; DEALLOCATE PREPARE stmt1; 
	SET @s = CONCAT('CREATE TABLE boosting_co.Boost_Catalog_',@CatalogName,
	'(
		#General
		SKU_Config VARCHAR(250) NOT NULL,
		SKU_Name VARCHAR(250) NOT NULL,
		Average_Position DECIMAL (15,5) NOT NULL,
		Last_Position INT,
		New_Position INT NOT NULL,
		Last_Boost INT,
		Novelty INT NOT NULL,
		Days_Visible INT NOT NULL,
		Is_Visible BIT(1) NOT NULL,
		Is_Market_Place BIT(1) NOT NULL,
		Is_Sell_List BIT(1) NOT NULL,
		Is_Consignment SMALLINT NOT NULL,
		Is_Bundle BIT(1) NOT NULL,
		 
		#Commercial REPORT
		Items_Sold_Last_30_Days   INT NOT NULL,
		PC1_Last_30_Days     DECIMAL(15,5) NOT NULL,
		PC1_5_Last_30_Days   DECIMAL(15,5) NOT NULL,
		PC2_Last_30_Days     DECIMAL(15,5) NOT NULL,
		Revenue_Last_30_Days DECIMAL(15,5) NOT NULL,
		
		#Stock
		Inventory_Stock_Type VARCHAR(15) NOT NULL,
		Inventory_Stock_Items INT NOT NULL,
		Inventory_Stock_Revenue DECIMAL(15,5) NOT NULL,
		Days_In_Inventory_Stock INT NOT NULL,
		Supplier_Stock INT NOT NULL,
		
		#Coverage of sales Week based
		Weekly_Cov DECIMAL(15,5) NOT NULL,
		
		#Divided by Days_Visible
		Per_Day_Items     DECIMAL(15,5) NOT NULL,
		Per_Day_Revenue     DECIMAL(15,5) NOT NULL,
		Per_Day_PC1_5 DECIMAL(15,5) NOT NULL,
		
		#PRICING
		Price DECIMAL(15,5) NOT NULL,
		Discount DECIMAL(15,5) NOT NULL,
		
		#NORMALIZED DATA
		Novelty_Norm DECIMAL(15,7) NOT NULL,
		Is_Market_Place_Norm DECIMAL(15,7) NOT NULL,
		Is_Sell_List_Norm DECIMAL(15,7) NOT NULL,
		Is_Consignment_Norm DECIMAL(15,7) NOT NULL,
		Is_Bundle_Norm DECIMAL(15,7) NOT NULL,
		Inventory_Stock_Items_Norm DECIMAL(15,7) NOT NULL,
		Inventory_Stock_Revenue_Norm DECIMAL(15,7) NOT NULL,
		Days_In_Inventory_Stock_Norm DECIMAL(15,7) NOT NULL,
		Supplier_Stock_Norm DECIMAL(15,7) NOT NULL,
		Weekly_Cov_Norm DECIMAL(15,7) NOT NULL,
		Per_Day_Items_Norm DECIMAL(15,7) NOT NULL,
		Per_Day_Revenue_Norm DECIMAL(15,7) NOT NULL,
		Per_Day_PC1_5_Norm DECIMAL(15,7) NOT NULL,
		Price_Norm DECIMAL(15,7) NOT NULL,
		Discount_Norm DECIMAL(15,7) NOT NULL,
		
		#Boost order
		Boost_Order DECIMAL(15,5) NOT NULL,

		#Fixed Boost
		Fixed_Boost INT,
		
		#Final Boost
		Final_Boost INT,

		PRIMARY KEY (SKU_Config)
	);');
	PREPARE stmt2 FROM @s; EXECUTE stmt2; DEALLOCATE PREPARE stmt2;
	
	#INSERT SKUS OF THIS CATALOG
	SET @s = CONCAT('INSERT INTO boosting_co.Boost_Catalog_',@CatalogName, '(
		SKU_Config,
		SKU_Name,
		Average_Position,
		Last_Position,
		Last_Boost,
		Novelty,
		Days_Visible,
		Is_Visible,
		Is_Market_Place,
		Is_Sell_List,
		Is_Consignment,
		Is_Bundle,
		Items_Sold_Last_30_Days,
		PC1_Last_30_Days,
		PC1_5_Last_30_Days,
		PC2_Last_30_Days,
		Revenue_Last_30_Days,
		Inventory_Stock_Type,
		Inventory_Stock_Items,
		Inventory_Stock_Revenue,
		Days_In_Inventory_Stock,
		Supplier_Stock,
		Per_Day_Items,
		Per_Day_Revenue,
		Per_Day_PC1_5,
		Price,
		Discount)
	SELECT
		SKU_Config,
		SKU_Name,
		Average_Position,
		Last_Position,
		Last_Boost,
		Novelty,
		Days_Visible,
		Is_Visible,
		Is_Market_Place,
		Is_Sell_List,
		Is_Consignment,
		Is_Bundle,
		Items_Sold_Last_30_Days,
		PC1_Last_30_Days,
		PC1_5_Last_30_Days,
		PC2_Last_30_Days,
		Revenue_Last_30_Days,
		Inventory_Stock_Type,
		Inventory_Stock_Items,
		Inventory_Stock_Revenue,
		Days_In_Inventory_Stock,
		Supplier_Stock,
		Per_Day_Items,
		Per_Day_Revenue,
		Per_Day_PC1_5,
		Price,
		Discount
	FROM boosting_co.Boost_Catalog_CO
	WHERE catalog = @CatalogName
	GROUP BY SKU_Config
	;');
	PREPARE stmt3 FROM @s; EXECUTE stmt3; DEALLOCATE PREPARE stmt3;
	
	#SAVE ALL MAX AND MIN IN TEMPORAL TABLE
	DROP TABLE IF EXISTS boosting_co.Boost_TMP_MinAndMax;
	CREATE TABLE boosting_co.Boost_TMP_MinAndMax (
		KPI VARCHAR(250) NOT NULL,
		MIN DECIMAL (15,5) NOT NULL,
		MAX DECIMAL (15,5) NOT NULL,
		PRIMARY KEY(KPI));

	SET @s = CONCAT('INSERT INTO boosting_co.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Novelty", MIN(Novelty), MAX(Novelty) FROM boosting_co.Boost_Catalog_', @CatalogName,';');
		PREPARE stmt11 FROM @s; EXECUTE stmt11; DEALLOCATE PREPARE stmt11;
	SET @s = CONCAT('INSERT INTO boosting_co.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Is_Consignment", MIN(Is_Consignment), MAX(Is_Consignment) FROM boosting_co.Boost_Catalog_', @CatalogName,';');
		PREPARE stmt12 FROM @s; EXECUTE stmt12; DEALLOCATE PREPARE stmt12;
	SET @s = CONCAT('INSERT INTO boosting_co.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Days_In_Inventory_Stock", MIN(Days_In_Inventory_Stock), MAX(Days_In_Inventory_Stock) FROM boosting_co.Boost_Catalog_', @CatalogName,';');
		PREPARE stmt13 FROM @s; EXECUTE stmt13; DEALLOCATE PREPARE stmt13;
	SET @s = CONCAT('INSERT INTO boosting_co.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Inventory_Stock_Revenue", MIN(Inventory_Stock_Revenue), MAX(Inventory_Stock_Revenue) FROM boosting_co.Boost_Catalog_', @CatalogName,';');
		PREPARE stmt14 FROM @s; EXECUTE stmt14; DEALLOCATE PREPARE stmt14;
	SET @s = CONCAT('INSERT INTO boosting_co.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Inventory_Stock_Items", MIN(Inventory_Stock_Items), MAX(Inventory_Stock_Items) FROM boosting_co.Boost_Catalog_', @CatalogName,';');
		PREPARE stmt15 FROM @s; EXECUTE stmt15; DEALLOCATE PREPARE stmt15;
	SET @s = CONCAT('INSERT INTO boosting_co.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Supplier_Stock", MIN(Supplier_Stock), MAX(Supplier_Stock) FROM boosting_co.Boost_Catalog_', @CatalogName,';');
		PREPARE stmt16 FROM @s; EXECUTE stmt16; DEALLOCATE PREPARE stmt16;
	SET @s = CONCAT('INSERT INTO boosting_co.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Weekly_Cov", MIN(Weekly_Cov), MAX(Weekly_Cov) FROM boosting_co.Boost_Catalog_', @CatalogName,';');
		PREPARE stmt17 FROM @s; EXECUTE stmt17; DEALLOCATE PREPARE stmt17;
	SET @s = CONCAT('INSERT INTO boosting_co.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Per_Day_Items", MIN(Per_Day_Items), MAX(Per_Day_Items) FROM boosting_co.Boost_Catalog_', @CatalogName,';');
		PREPARE stmt19 FROM @s; EXECUTE stmt19; DEALLOCATE PREPARE stmt19;
	SET @s = CONCAT('INSERT INTO boosting_co.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Per_Day_Revenue", MIN(Per_Day_Revenue), MAX(Per_Day_Revenue) FROM boosting_co.Boost_Catalog_', @CatalogName,';');
		PREPARE stmt20 FROM @s; EXECUTE stmt20; DEALLOCATE PREPARE stmt20;
	SET @s = CONCAT('INSERT INTO boosting_co.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Per_Day_PC1_5", MIN(Per_Day_PC1_5), MAX(Per_Day_PC1_5) FROM boosting_co.Boost_Catalog_', @CatalogName,';');
		PREPARE stmt21 FROM @s; EXECUTE stmt21; DEALLOCATE PREPARE stmt21;
	SET @s = CONCAT('INSERT INTO boosting_co.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Price", MIN(Price), MAX(Price) FROM boosting_co.Boost_Catalog_', @CatalogName,';');
		PREPARE stmt22 FROM @s; EXECUTE stmt22; DEALLOCATE PREPARE stmt22;
	SET @s = CONCAT('INSERT INTO boosting_co.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Discount", MIN(Discount), MAX(Discount) FROM boosting_co.Boost_Catalog_', @CatalogName,';');
		PREPARE stmt23 FROM @s; EXECUTE stmt23; DEALLOCATE PREPARE stmt23;

	#SET THE TOTAL OF WEIGHTS
	SET @s = CONCAT('UPDATE boosting_co.Boost_Weights_CO
			SET Total_Weight =  Novelty + 
													Days_In_Inventory_Stock + 
													Inventory_Stock_Revenue + 
													Inventory_Stock_Items + 
													Supplier_Stock +
													Weekly_Cov +
													Per_Day_Items + 
													Per_Day_Revenue + 
													Per_Day_PC1_5 + 
													Is_Market_Place + 
													Is_Sell_List + 
													Is_Consignment + 
													Is_Bundle + 
													Price + 
													Discount
			WHERE Catalog = "', @CatalogName, '";');
		PREPARE stmt24 FROM @s; EXECUTE stmt24; DEALLOCATE PREPARE stmt24;

	#NORMALIZE DATA ---(Value-MIN)/(MAX-MIN)*Weight-#
	#SellList, Commertial-Marketing.
	SET @s = CONCAT('UPDATE boosting_co.Boost_Catalog_',@CatalogName, ' a, boosting_co.Boost_TMP_MinAndMax b, boosting_co.Boost_Weights_CO c
			SET a.Novelty_Norm = ((b.MAX + b.MIN - a.Novelty) - b.MIN) / (b.MAX-b.MIN) * c.Novelty
			WHERE b.KPI = "Novelty" AND c.Catalog = "', @CatalogName,'";');
		PREPARE stmt25 FROM @s; EXECUTE stmt25; DEALLOCATE PREPARE stmt25;
	SET @s = CONCAT('UPDATE boosting_co.Boost_Catalog_',@CatalogName, ' a, boosting_co.Boost_Weights_CO c
			SET a.Is_Market_Place_Norm = a.Is_Market_Place * c.Is_Market_Place
			WHERE c.Catalog = "', @CatalogName,'";');
		PREPARE stmt26 FROM @s; EXECUTE stmt26; DEALLOCATE PREPARE stmt26;
	SET @s = CONCAT('UPDATE boosting_co.Boost_Catalog_',@CatalogName, ' a, boosting_co.Boost_Weights_CO c
			SET a.Is_Sell_List_Norm = a.Is_Sell_List * c.Is_Sell_List
			WHERE c.Catalog = "', @CatalogName,'";');
		PREPARE stmt27 FROM @s; EXECUTE stmt27; DEALLOCATE PREPARE stmt27;
	SET @s = CONCAT('UPDATE boosting_co.Boost_Catalog_',@CatalogName, ' a, boosting_co.Boost_TMP_MinAndMax b, boosting_co.Boost_Weights_CO c
			SET a.Is_Consignment_Norm = ((b.MAX + b.MIN - a.Is_Consignment) - b.MIN) / (b.MAX-b.MIN) * c.Is_Consignment
			WHERE b.KPI = "Is_Consignment" AND c.Catalog = "', @CatalogName,'";');
		PREPARE stmt28 FROM @s; EXECUTE stmt28; DEALLOCATE PREPARE stmt28;
	SET @s = CONCAT('UPDATE boosting_co.Boost_Catalog_',@CatalogName, ' a, boosting_co.Boost_Weights_CO c
			SET a.Is_Bundle_Norm = a.Is_Bundle * c.Is_Bundle
			WHERE c.Catalog = "', @CatalogName,'";');
		PREPARE stmt29 FROM @s; EXECUTE stmt29; DEALLOCATE PREPARE stmt29;
	SET @s = CONCAT('UPDATE boosting_co.Boost_Catalog_',@CatalogName, ' a, boosting_co.Boost_TMP_MinAndMax b, boosting_co.Boost_Weights_CO c
			SET a.Days_In_Inventory_Stock_Norm = (a.Days_In_Inventory_Stock - b.MIN) / (b.MAX-b.MIN) * c.Days_In_Inventory_Stock
			WHERE b.KPI = "Days_In_Inventory_Stock" AND c.Catalog = "', @CatalogName,'";');
		PREPARE stmt30 FROM @s; EXECUTE stmt30; DEALLOCATE PREPARE stmt30;
	SET @s = CONCAT('UPDATE boosting_co.Boost_Catalog_',@CatalogName, ' a, boosting_co.Boost_TMP_MinAndMax b, boosting_co.Boost_Weights_CO c
			SET a.Inventory_Stock_Revenue_Norm = (a.Inventory_Stock_Revenue - b.MIN) / (b.MAX-b.MIN) * c.Inventory_Stock_Revenue
			WHERE b.KPI = "Inventory_Stock_Revenue" AND c.Catalog = "', @CatalogName,'";');
		PREPARE stmt31 FROM @s; EXECUTE stmt31; DEALLOCATE PREPARE stmt31;
	SET @s = CONCAT('UPDATE boosting_co.Boost_Catalog_',@CatalogName, ' a, boosting_co.Boost_TMP_MinAndMax b, boosting_co.Boost_Weights_CO c
			SET a.Inventory_Stock_Items_Norm = (a.Inventory_Stock_Items - b.MIN) / (b.MAX-b.MIN) * c.Inventory_Stock_Items
			WHERE b.KPI = "Inventory_Stock_Items" AND c.Catalog = "', @CatalogName,'";');
		PREPARE stmt32 FROM @s; EXECUTE stmt32; DEALLOCATE PREPARE stmt32;
	SET @s = CONCAT('UPDATE boosting_co.Boost_Catalog_',@CatalogName, ' a, boosting_co.Boost_TMP_MinAndMax b, boosting_co.Boost_Weights_CO c
			SET a.Supplier_Stock_Norm = (a.Supplier_Stock - b.MIN) / (b.MAX-b.MIN) * c.Supplier_Stock
			WHERE b.KPI = "Supplier_Stock" AND c.Catalog = "', @CatalogName,'";');
		PREPARE stmt33 FROM @s; EXECUTE stmt33; DEALLOCATE PREPARE stmt33;
	SET @s = CONCAT('UPDATE boosting_co.Boost_Catalog_',@CatalogName, ' a, boosting_co.Boost_TMP_MinAndMax b, boosting_co.Boost_Weights_CO c
			SET a.Weekly_Cov_Norm = (a.Weekly_Cov - b.MIN) / (b.MAX-b.MIN) * c.Weekly_Cov
			WHERE b.KPI = "Weekly_Cov" AND c.Catalog = "', @CatalogName,'";');
		PREPARE stmt34 FROM @s; EXECUTE stmt34; DEALLOCATE PREPARE stmt34;
	SET @s = CONCAT('UPDATE boosting_co.Boost_Catalog_',@CatalogName, ' a, boosting_co.Boost_TMP_MinAndMax b, boosting_co.Boost_Weights_CO c
			SET a.Per_Day_Items_Norm = (a.Per_Day_Items - b.MIN) / (b.MAX-b.MIN) * c.Per_Day_Items
			WHERE b.KPI = "Per_Day_Items" AND c.Catalog = "', @CatalogName,'";');
		PREPARE stmt36 FROM @s; EXECUTE stmt36; DEALLOCATE PREPARE stmt36;
	SET @s = CONCAT('UPDATE boosting_co.Boost_Catalog_',@CatalogName, ' a, boosting_co.Boost_TMP_MinAndMax b, boosting_co.Boost_Weights_CO c
			SET a.Per_Day_Revenue_Norm = (a.Per_Day_Revenue - b.MIN) / (b.MAX-b.MIN) * c.Per_Day_Revenue
			WHERE b.KPI = "Per_Day_Revenue" AND c.Catalog = "', @CatalogName,'";');
		PREPARE stmt37 FROM @s; EXECUTE stmt37; DEALLOCATE PREPARE stmt37;
	SET @s = CONCAT('UPDATE boosting_co.Boost_Catalog_',@CatalogName, ' a, boosting_co.Boost_TMP_MinAndMax b, boosting_co.Boost_Weights_CO c
			SET a.Per_Day_PC1_5_Norm = (a.Per_Day_PC1_5 - b.MIN) / (b.MAX-b.MIN) * c.Per_Day_PC1_5
			WHERE b.KPI = "Per_Day_PC1_5" AND c.Catalog = "', @CatalogName,'";');
		PREPARE stmt38 FROM @s; EXECUTE stmt38; DEALLOCATE PREPARE stmt38;
	SET @s = CONCAT('UPDATE boosting_co.Boost_Catalog_',@CatalogName, ' a, boosting_co.Boost_TMP_MinAndMax b, boosting_co.Boost_Weights_CO c
			SET a.Price_Norm = (a.Price - b.MIN) / (b.MAX-b.MIN) * c.Price
			WHERE b.KPI = "Price" AND c.Catalog = "', @CatalogName,'";');
		PREPARE stmt39 FROM @s; EXECUTE stmt39; DEALLOCATE PREPARE stmt39;
	SET @s = CONCAT('UPDATE boosting_co.Boost_Catalog_',@CatalogName, ' a, boosting_co.Boost_TMP_MinAndMax b, boosting_co.Boost_Weights_CO c
			SET a.Discount_Norm = (a.Discount - b.MIN) / (b.MAX-b.MIN) * c.Discount
			WHERE b.KPI = "Discount" AND c.Catalog = "', @CatalogName,'";');
		PREPARE stmt40 FROM @s; EXECUTE stmt40; DEALLOCATE PREPARE stmt40;

	#SET BOOST ORDER
	#Set novelty, set last Boost, set Boost_Order, set Final_Boost
	SET @s = CONCAT('UPDATE boosting_co.Boost_Catalog_',@CatalogName, ' a, boosting_co.Boost_Weights_CO b
				SET a.Boost_Order =(a.Novelty_Norm
													+ a.Is_Market_Place_Norm
													+ a.Is_Sell_List_Norm
													+ a.Is_Consignment_Norm
													+ a.Is_Bundle_Norm
													+ a.Days_In_Inventory_Stock_Norm
													+ a.Inventory_Stock_Revenue_Norm
													+ a.Inventory_Stock_Items_Norm
													+ a.Supplier_Stock_Norm
													+ a.Weekly_Cov_Norm
													+ a.Per_Day_Items_Norm
													+ a.Per_Day_Revenue_Norm
													+ a.Per_Day_PC1_5_Norm
													+ a.Price_Norm
													+ a.Discount_Norm) / b.Total_Weight
			WHERE b.Catalog = "',@CatalogName, '";');
		PREPARE stmt41 FROM @s; EXECUTE stmt41; DEALLOCATE PREPARE stmt41;

	#SET FIXED BOOST ACCORDING TO MARKETING AND COMMETCIAL SCHEDULE
	SET @s = CONCAT('UPDATE boosting_co.Boost_Catalog_',@CatalogName, ' a, boosting_co.Boost_Marketing_Commercial_Schedule_CO b
		SET
				a.Fixed_Boost =  b.Boost
		WHERE a.SKU_Config = b.SKU_Config AND (b.Start_Date <= NOW() AND NOW() <= b.End_Date)
		;');
	PREPARE stmt42 FROM @s; EXECUTE stmt42; DEALLOCATE PREPARE stmt42;

	#SORT ORDER BY BOOST DESCENDENT
	SET @s = CONCAT('ALTER TABLE boosting_co.Boost_Catalog_',@CatalogName, ' ORDER BY Boost_Order DESC;');
		PREPARE stmt43 FROM @s; EXECUTE stmt43; DEALLOCATE PREPARE stmt43;

	#SET NEW POSITION
	SET @count = 0;
	SET @s = CONCAT('UPDATE boosting_co.Boost_Catalog_',@CatalogName, '
		SET
				New_Position = @count:=@count+1;
		;');
	PREPARE stmt44 FROM @s; EXECUTE stmt44; DEALLOCATE PREPARE stmt44;

	#SET FINAL BOOST
	SET @s = CONCAT('INSERT INTO boosting_co.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Total", 0, COUNT(SKU_Config) FROM boosting_co.Boost_Catalog_',@CatalogName, ';');
	PREPARE stmt45 FROM @s; EXECUTE stmt45; DEALLOCATE PREPARE stmt45;

	SET @count = 0;
	SET @s = CONCAT('UPDATE boosting_co.Boost_Catalog_',@CatalogName, ' a, boosting_co.Boost_TMP_MinAndMax b, boosting_co.Boost_Weights_CO c
		SET
		a.Final_Boost = CASE
				WHEN a.Fixed_Boost IS NOT NULL THEN 0
				ELSE  @count:=@count+1
			END,
			a.Final_Boost = CASE
				WHEN a.Fixed_Boost IS NOT NULL THEN a.Fixed_Boost
				ELSE CASE
						WHEN @count <= 24 THEN c.TopBucket-@count
						ELSE CASE
						WHEN FLOOR(-0.5*(-SQRT(8*(@count-24))-1))  < ROUND((b.MAX-24)/((c.TopBucket-24)-(c.FloorBucket)+1),0) THEN c.TopBucket-24-FLOOR(-0.5*(-SQRT(8*(@count-24))-1))
						ELSE CASE
					WHEN c.TopBucket - 24 - (FLOOR(((@count-24)-((ROUND((b.MAX-24)/(c.TopBucket-24-(c.FloorBucket)+1),0)-1)*ROUND((b.MAX-24)/(c.TopBucket-24-(c.FloorBucket)+1),0)/2)-1)/ROUND((b.MAX-24)/(c.TopBucket-24-(c.FloorBucket)+1),0)) + ROUND((b.MAX-24)/(c.TopBucket-24-(c.FloorBucket)+1),0)) < c.FloorBucket THEN c.FloorBucket
					ELSE c.TopBucket - 24 - (FLOOR(((@count-24)-((ROUND((b.MAX-24)/(c.TopBucket-24-(c.FloorBucket)+1),0)-1)*ROUND((b.MAX-24)/(c.TopBucket-24-(c.FloorBucket)+1),0)/2)-1)/ROUND((b.MAX-24)/(c.TopBucket-24-(c.FloorBucket)+1),0)) + ROUND((b.MAX-24)/(c.TopBucket-24-(c.FloorBucket)+1),0))
						END
					END
				END
			END
		WHERE b.KPI = "Total" AND c.catalog = @CatalogName;');
	PREPARE stmt46 FROM @s; EXECUTE stmt46; DEALLOCATE PREPARE stmt46;

	SET @s = CONCAT('UPDATE boosting_co.Boost_Catalog_',@CatalogName, ' a, boosting_co.Boost_Weights_CO c
		SET
			a.Final_Boost = c.FloorBucket
		WHERE c.catalog = @CatalogName AND a.Final_Boost < c.FloorBucket AND a.Fixed_Boost IS NULL;');
	PREPARE stmt47 FROM @s; EXECUTE stmt47; DEALLOCATE PREPARE stmt47;


	#INSERT FINAL BOOST INTO LAST TABLE
	SET @s = CONCAT('INSERT INTO boosting_co.Final_Boost_CO (Catalog, TopBucket, SKU_Config, Score)
		SELECT
			@CatalogName,
			b.TopBucket,
			SKU_Config,
			Final_Boost FROM boosting_co.Boost_Catalog_',@CatalogName, '
			INNER JOIN (SELECT TopBucket, Catalog FROM boosting_co.Boost_Weights_CO) b
			ON @CatalogName = b.Catalog;');
	PREPARE stmt48 FROM @s; EXECUTE stmt48; DEALLOCATE PREPARE stmt48;

	DROP TABLE IF EXISTS boosting_co.Boost_TMP_MinAndMax;
	
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `Boost_Report_2_CO`(IN v_catalog VARCHAR(255), v_start_pos INT, v_end_pos INT, v_start_date DATE, v_days INT)
BEGIN
	SET @Catalog = v_catalog, @StartPos = v_start_pos, @EndPos = v_end_pos, @StartDate = v_start_date, @Days = v_days;
		INSERT INTO Boost_Report_CO()
		SELECT 
			@Catalog AS Catalog,
			dates.date AS Date,
			WEEK(dates.date) AS Week,
			CONCAT(@StartPos," to ",@EndPos) AS Set_of_SKUs,
			SUM(stock.numStock) as Stock,
			sales.items_sold AS Items_sold,
			sales.pc1_5 AS PC1_5,
			sales.revenue_sold AS Revenue_sold
		FROM 
			(SELECT DATE_ADD(@StartDate, INTERVAL n.id - 1 DAY) AS date
			FROM boosting.Boost_dates n
			WHERE DATE_ADD(@StartDate, INTERVAL n.id -1 DAY) <=  @StartDate + interval @Days day) dates
			INNER JOIN (SELECT SKU_Config, catalog, numb_of_position, DATE(date_time) AS date FROM Boost_Product_Position_In_Catalog_CO
									WHERE catalog = @Catalog AND numb_of_position >= @StartPos AND numb_of_position <= @EndPos AND DATE(date_time) >= @StartDate AND DATE(date_time) < @StartDate + interval @Days day) pos
									ON dates.date = pos.date
			LEFT JOIN (select sku_config, count(*) as numStock, date_entrance, date_exit from operations_co.out_stock_hist
									WHERE (date_exit >= @StartDate OR date_exit IS NULL) AND date_entrance <= @StartDate + interval @Days day
									GROUP BY sku_config, date_entrance, date_exit) stock
									ON stock.sku_config = pos.SKU_Config AND stock.date_entrance <= dates.date AND (stock.date_exit IS NULL OR stock.date_exit >= dates.date)
			LEFT JOIN (SELECT SkuConfig, COUNT(*) AS items_sold, SUM(PCOnePFive) AS pc1_5, SUM(Rev) AS revenue_sold, Date FROM development_co_project.A_Master
									WHERE OrderAfterCan = 1 AND Date >= @StartDate AND Date < @StartDate + interval @Days day
									GROUP BY SkuConfig, Date) sales
									ON sales.SkuConfig = pos.SKU_Config AND sales.Date = dates.date
		GROUP BY dates.date
		ORDER BY dates.date asc;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `Boos_Report_1_CO`()
BEGIN
	DECLARE done INT DEFAULT 0;
  DECLARE v_cat VARCHAR(50) DEFAULT NULL;
  #declare cursor
  DECLARE cur1 CURSOR FOR SELECT DISTINCT Catalog FROM Boost_Weights_CO WHERE Active = 1 AND Catalog IS NOT NULL ORDER BY TopBucket DESC;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
	SET @days = 30;
	
	#Routine body goes here...
	DROP TABLE IF EXISTS Boost_Report_CO;
	CREATE TABLE Boost_Report_CO(
		Catalog VARCHAR(255),
		Date DATE,
		Week SMALLINT,
		Set_of_SKUs VARCHAR(255),
		Stock DECIMAL(10,2),
		Items_sold DECIMAL(10,2),
		PC1_5 DECIMAL(10,2),
		Revenue_sold DECIMAL(10,2)
		);

	OPEN cur1;
		cur1_loop:LOOP
			FETCH cur1 INTO v_cat;
			IF done = 1 THEN
				LEAVE cur1_loop;
			END IF;
			CALL Boost_Report_2_CO(v_cat, 1, 25, NOW()- INTERVAL @days DAY, @days);
			CALL Boost_Report_2_CO(v_cat, 26, 50, NOW()- INTERVAL @days DAY, @days);
			CALL Boost_Report_2_CO(v_cat, 51, 100, NOW()- INTERVAL @days DAY, @days);
			CALL Boost_Report_2_CO(v_cat, 101, 200, NOW()- INTERVAL @days DAY, @days);
		END LOOP cur1_loop;
  CLOSE cur1;
	SET done = 0;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:03:33
