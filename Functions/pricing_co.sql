-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: pricing_co
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'pricing_co'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`luis.rodriguez`@`%`*/ /*!50003 PROCEDURE `Campaing_Results`()
BEGIN
drop table if exists pricing_co.M_Campaing_Results;
create table pricing_co.M_Campaing_Results as
select a.date 
, a.SKUSimple
, a.SKUConfig 
, a.Cat1
, a.Cat2
, a.Cat3
, a.CatBP
, count(a.ItemID)
, sum(a.Rev)
, sum(a.PCOnePFive)
, sum(a.PCTwo)
, b.add_cart
, b.product_views
, count(a.ItemID)/b.product_views as CR
from development_co_project.A_Master a
left join (
select 
date
, sku
, sum(product_views) as product_views
, sum(add_cart) as add_cart
from bazayaco.tbl_daily_visits 
where date >= curdate()- interval 60 day and product_views > 0
group by sku, date
) b
on a.SKUConfig  = b.sku and b.date = a.date
where date >= curdate() - interval 60 day and a.orderAfterCan = 1
group by a.SKUConfig, date
;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`luis.rodriguez`@`%`*/ /*!50003 PROCEDURE `Price_Attack`(in weekAnalisis INT )
BEGIN
drop table if exists pricing_co.M_Price_Attack; 
create table pricing_co.M_Price_Attack as
select 
'Colombia' as "pais"
,a.competitor
,a.product_name
,a.brand
,a.category
,if(a.special_price > 0 , a.special_price, a.price) price_competitor
,a.url
,b.sku
,c.Head_Buyer
,c.Cat1
,c.Cat2
,if(c.cost_oms > 0, c.cost_oms, cost) as cost_oms
,c.tax_percent/100 as tax_percent
,c.delivery_cost_supplier
,d.shipping_cost
,d.shipping_fee
,if(if((curdate() >= c.special_from_date and curdate() <= c.special_to_date and c.special_price > 0) =true, c.special_price, c.price)*0.025 > 2500,if((curdate() >= c.special_from_date and curdate() <= c.special_to_date and c.special_price > 0) =true, c.special_price, c.price)*0.025,2500) as payment_fee
,if((curdate() >= c.special_from_date and curdate() <= c.special_to_date and c.special_price > 0) =true, c.special_price, c.price) price_today
,if(e.Rank <= 100, 'Top 100 Ventas', if (e.Rank <= 500, 'Top 500 Ventas',NULL)) as Tier_Ventas
,c.isVisible
from M_Crawl_Comp a
left join pricing_co.M_Coverage_Competition b
on a.url = b.url
left join development_co_project.A_Master_Catalog c
on b.sku = c.sku_config
left join (select sku_config
, sku
, shipping_fee
, shipping_cost
from bazayaco.finalPricingTable) d
on d.sku_config = b.sku
left join (
select SKUConfig
,SKUSimple
,sum(Rev)
,@curRank := @curRank + 1 as Rank 
from development_co_project.A_Master p, (select @curRank := 0 ) r
where date >= CURDATE() - INTERVAL 1 MONTH
group by SKUSimple
order by Rank asc
limit 500
) e
on e.SKUConfig = b.sku
where week = weekAnalisis and a.competitor in ('Falabella', 'Alkosto', 'Ktronix');
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`luis.rodriguez`@`%`*/ /*!50003 PROCEDURE `voucher_rate`()
BEGIN
drop table if exists pricing_co.tbl_coupon_values;
create table pricing_co.tbl_coupon_values as
select id_rango, precio_desde, precio_hasta
, sum(unit_price) unit_price
, sum(coupon_money_value) coupon_money_value
, sum(coupon_money_value)/sum(unit_price) proporcion
, label
from bazayaco.tbl_order_detail
inner join bazayaco.tbl_rangos_de_precios
on unit_price >= precio_desde and unit_price < precio_hasta
where oac = 1
and returned = 0
and rejected = 0
and date >= CURDATE() - INTERVAL 30 DAY
and ((coupon_code_prefix not in
(select Prefijo from bazayaco.tbl_prefijos_vouchers_sac)
and coupon_code not like 'VC%'
and coupon_code not like 'REO%'
and coupon_code not like 'REP%'
and coupon_code not like 'ER%'
and coupon_code not like 'CAN%'
and coupon_code not like 'DEV%'
and coupon_code_prefix not like 'CAC%'
and coupon_code not like '0000%'
and coupon_code_description not like 'Partner%'
and coupon_code_description not like '%CAC%'
and coupon_code not like 'CASHBACK%'
and coupon_code not like '%COMBO%'
and coupon_code_prefix <> ''
and coupon_code_prefix not like 'OUT%'
and coupon_code not like 'FBCAC%'
and coupon_code_prefix not like 'BANDEJA%'
and coupon_code_prefix not like 'ESTUFA%'
and coupon_code_prefix not like 'LM2X'
and coupon_code_prefix not like 'GC%'
and coupon_code_prefix not like 'KER%'
and coupon_code_prefix not like 'DOBLEOUT%'
and coupon_code_prefix not like 'PERFUME%'
AND coupon_code_prefix not like 'INVCAC%'
and coupon_code_description not like 'PR%'
and coupon_code_prefix not like 'NPS%'
and coupon_code_prefix not like 'INV%'
and coupon_code not like 'TAPETES%'
and coupon_code not like 'AUDIFONOS%'
and coupon_code not like 'SARTENES%'
and coupon_code not like 'alamaula%'
and coupon_code not like 'ML%'
and coupon_code not like 'cbit%'
and coupon_code not like 'Curebit%'
and coupon_code not like 'HVE%'
and coupon_code not like 'BLOG%'
and coupon_code not like 'CEROIVA%'
and coupon_code not like 'CS%'
and coupon_code not like 'Fabian%'
and coupon_code not like 'ANDRES%'
and coupon_code not like 'fbtrtost%'
and coupon_code not like 'HAV%'
and coupon_code not like 'TAPETE%'
and coupon_code not like 'CITI%'
and coupon_code not like 'PAGNOIVA%'
and coupon_code not like 'NOIVA%'
and coupon_code not like 'PAG%'
and coupon_code not like 'V-%'
and coupon_code not like 'VUELVEDINERO%'
and coupon_code not like 'DINEROVUELVE%'
and coupon_code not like 'VOUCHER%'
and coupon_code not like 'ONE80%'
and coupon_code not like 'CINTU50%'
and coupon_code not like 'PAN2X1%'
and coupon_code not like 'BO2X1%'
and coupon_code not like 'LE-MINUIT-%'
and coupon_code not like '2X1%'
and coupon_code not like 'oakley70%'
and coupon_code not like 'FELIZ%'
and coupon_code not like 'AV332HL77AJILACOL-119599%'
and coupon_code not like 'AV332HL77AJILACOL-119599%'
and coupon_code not like 'BCWAFLERAQZY%'
and coupon_code not like 'FBHOR%'
and coupon_code not like 'FBCITI282%'
and coupon_code not like 'AV332HL77AJILACOL-119599%'
and coupon_code not like 'FBTITANBX%'
and coupon_code not like 'FBBTA%'
and coupon_code not like 'DOBLEDINB0PqUO%'
and coupon_code not like 'FBTITANB%'
and coupon_code not like 'FBNIKONB%'
and coupon_code not like 'FBCCAM%'
and coupon_code not like 'FBBB%'
and coupon_code not like 'LUZMARINARIANO%'
and coupon_code not like 'FBO1%'
and coupon_code not like 'AV332HL77AJILACOL-119599%'
and coupon_code not like 'PRECIOBAJO%'
and coupon_code not like 'NADIEPUEDE%'
and coupon_code not like 'SHILHER%'
and coupon_code not like 'JULIANJIMENEZ1m6Zy%'
and coupon_code not like 'PRECIOBAJO%'
and coupon_code not like 'REBLU10jU6l%'
and coupon_code not like 'MUYFELIZ%'
and coupon_code not like 'NADIEPUEDE%'
and coupon_code not like 'AV332HL77AJILACOL-119599%'
and coupon_code not like 'PRECIOBAJO%'
and coupon_code not like 'NADIEPUEDE%'
and coupon_code not like 'SIMBLU09Rhr%'
and coupon_code not like 'PRECIOBAJO%'
and coupon_code not like 'JUAN.CORREDOR%'
and coupon_code not like 'NADIEPUEDE%'
and coupon_code not like 'IVANVELEZ%'
and coupon_code not like 'NADIEPUEDE%'
and coupon_code not like 'PRECIOBAJO%'
and coupon_code not like 'NADIEPUEDE%'
and coupon_code not like 'BCWAFLERAQZY%'
and coupon_code not like 'PRECIOBAJO%'
and coupon_code not like 'LOGJULIANESCANDONtdcO7%'
and coupon_code not like 'Karen%'
and coupon_code not like 'FBHOR%'
and coupon_code not like 'FBCITI282%'
and coupon_code not like 'AV332HL77AJILACOL-119599%'
and coupon_code not like 'NADIEPUEDE%'
and coupon_code not like 'FBTITANBX%'
and coupon_code not like 'FBBTA%'
and coupon_code not like 'NADIEPUEDE%'
and coupon_code not like 'Sergio%'
and coupon_code not like 'Jair%'
and coupon_code not like 'PRECIOBAJO%'
and coupon_code not like 'INS05uyv%'
and coupon_code not like 'DOBLEDINB0PqUO%'
and coupon_code not like 'FBTITANB%'
and coupon_code not like 'PRECIOBAJO%'
and coupon_code not like 'FBF70000%'
and coupon_code not like 'NADIEPUEDE%'
and coupon_code not like 'LUISALARCON%'
and coupon_code not like 'NADIEPUEDE%'
and coupon_code not like 'PRECIOBAJO%'
and coupon_code not like 'NADIEPUEDE%'
and coupon_code not like 'PRECIOBAJO%'
and coupon_code not like 'FBNIKONB%'
and coupon_code not like 'FBCCAM%'
and coupon_code not like 'FBBB%'
and coupon_code not like 'NADIEPUEDE%'
and coupon_code not like 'LENNDOQIb%'
and coupon_code not like 'PRECIOBAJO%'
and coupon_code in (select coupon_code from bazayaco.tbl_marketing_vouchers) )
or coupon_code is null)
and unit_price > 1000
and coupon_money_value >= 0
group by id_rango
order by precio_desde asc;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:04:12
