-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: CS_dyalogo
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'CS_dyalogo'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogoadm`@`%`*/ /*!50003 FUNCTION `derecha`(cadena varchar(255),len int) RETURNS varchar(255) CHARSET latin1
BEGIN
	return right(cadena,len);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogoadm`@`%`*/ /*!50003 FUNCTION `duracion_descansos_agente`(id_agente_p int) RETURNS varchar(200) CHARSET latin1
BEGIN
	DECLARE duracionDescanso int;
	DECLARE fhInicioSesion datetime;
	DECLARE duracionActual int;

	set fhInicioSesion = (select max(fecha_hora_inicio) from dy_sesiones where id_agente=id_agente_p and fecha_hora_fin is null);
	set duracionDescanso = (select ifnull(sum(duracion),0) from dy_descansos where id_agente=id_agente_p and fecha_hora_inicio>=fhInicioSesion);
	set duracionActual = duracionDescanso + (select TIME_TO_SEC(timediff(now(),max(fecha_hora_inicio))) from dy_descansos where id_agente=id_agente_p and fecha_hora_fin is null);
	
	return concat(SEC_TO_TIME(duracionActual),'.');
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogoadm`@`%`*/ /*!50003 FUNCTION `duracion_sesion_agente`(id_agente_p int) RETURNS varchar(200) CHARSET latin1
BEGIN
	DECLARE ret varchar(200);
	set ret = (select timediff(now(),max(fecha_hora_inicio)) from dy_sesiones where id_agente=id_agente_p and fecha_hora_fin is null);
	return concat(ret,'.');
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogoadm`@`%`*/ /*!50003 FUNCTION `ft_nom_age_even`(uid varchar(200)) RETURNS varchar(200) CHARSET latin1
BEGIN
	DECLARE ret varchar(200);
	set ret=(SELECT agente_nombre FROM dyalogo_telefonia.dy_v_historico_llamadas WHERE dyalogo_telefonia.dy_v_historico_llamadas.llamada_id_asterisk=uid);
	return ret;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogo`@`%`*/ /*!50003 FUNCTION `get_ext_by_id`(id_p int) RETURNS varchar(200) CHARSET latin1
begin
	DECLARE ret varchar(200);
	set ret=(SELECT extension FROM dyalogo_telefonia.dy_extensiones WHERE dyalogo_telefonia.dy_extensiones.id=id_p);
	return ret;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogo`@``*/ /*!50003 FUNCTION `get_id_campana`(nom_interno varchar(200)) RETURNS int(11)
begin
	DECLARE ret int;
	set ret=(SELECT id FROM dyalogo_telefonia.dy_campanas WHERE dyalogo_telefonia.dy_campanas.nombre_interno=nom_interno);
	return ret;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogo`@``*/ /*!50003 FUNCTION `get_id_extension`(numExtension varchar(200)) RETURNS varchar(20) CHARSET latin1
begin
	DECLARE ext varchar(20);
	DECLARE ret varchar(253);
	set ext = replace(numExtension,'SIP/','');
	set ret=(SELECT id FROM dyalogo_telefonia.dy_extensiones WHERE dyalogo_telefonia.dy_extensiones.extension=ext);
	return ret;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogo`@``*/ /*!50003 FUNCTION `get_id_troncal`(nom_interno varchar(200)) RETURNS int(11)
begin
	DECLARE ret int;
	set ret=(SELECT id FROM dyalogo_telefonia.dy_troncales WHERE dyalogo_telefonia.dy_troncales.nombre_interno=nom_interno);
	return ret;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogo`@`%`*/ /*!50003 FUNCTION `get_info_espera`(id_campana_p int) RETURNS varchar(200) CHARSET latin1
begin
	DECLARE ret varchar(200);
	set ret=concat('TEm=',(select min(tiempo_espera) from dy_llamadas where resultado='Contestada' and id_campana=id_campana_p and fecha_hora>=(now() - interval 1 day)));
	set ret=concat(ret,'|TEM=',(select max(tiempo_espera) from dy_llamadas where resultado='Contestada' and id_campana=id_campana_p and fecha_hora>=(now() - interval 1 day)));
	set ret=concat(ret,'|TEP=',(select avg(tiempo_espera) from dy_llamadas where resultado='Contestada' and id_campana=id_campana_p and fecha_hora>=(now() - interval 1 day)));
	set ret=concat(ret,'|AL=',(select count(*) from dy_v_actividad_actual_por_campanas where campana_id=id_campana_p));
	set ret=concat(ret,'|ADE=',(select count(*) from dy_v_actividad_actual_por_campanas where estado='Descanso' and campana_id=id_campana_p));
	set ret=concat(ret,'|AD=',(select count(*) from dy_v_actividad_actual_por_campanas where estado='Disponible' and campana_id=id_campana_p));
	set ret=concat(ret,'|ALL=',(select count(*) from dy_v_actividad_actual_por_campanas where estado like '%Llamada%' and campana_id=id_campana_p));
	return ret;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogo`@`%`*/ /*!50003 FUNCTION `get_nomage_by_id`(id_p int) RETURNS varchar(200) CHARSET latin1
begin
	DECLARE ret varchar(200);
	set ret=(SELECT nombre FROM dyalogo_telefonia.dy_agentes WHERE dyalogo_telefonia.dy_agentes.id=id_p);
	return ret;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogo`@``*/ /*!50003 FUNCTION `get_nomusu_by_id_campana`(id_p int) RETURNS varchar(200) CHARSET latin1
begin
	DECLARE ret varchar(200);
	set ret=(SELECT nombre_usuario FROM dyalogo_telefonia.dy_campanas WHERE dyalogo_telefonia.dy_campanas.id=id_p);
	return ret;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogo`@``*/ /*!50003 FUNCTION `get_nomusu_by_id_troncal`(id_p int) RETURNS varchar(200) CHARSET latin1
begin
	DECLARE ret varchar(200);
	set ret=(SELECT nombre_usuario FROM dyalogo_telefonia.dy_troncales WHERE dyalogo_telefonia.dy_troncales.id=id_p);
	return ret;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogo`@``*/ /*!50003 FUNCTION `get_nom_cola`(nom_interno varchar(200)) RETURNS varchar(20) CHARSET latin1
begin
	DECLARE ret varchar(253);
	set ret=(SELECT nombre_usuario FROM dyalogo_telefonia.dy_campanas WHERE dyalogo_telefonia.dy_campanas.nombre_interno=nom_interno);
	return ret;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogo`@``*/ /*!50003 FUNCTION `get_numero_telefonico`(clid varchar(200),src varchar(200)) RETURNS varchar(200) CHARSET latin1
begin
	DECLARE ret varchar(200);
	set ret = clid;
	if (INSTR(clid,'SIM')>0) then
		set ret = replace(clid,'"','');
		set ret = substring(ret,instr(ret,'<')+1,instr(ret,'>'));
		set ret = replace(ret,'<','');
		set ret = replace(ret,'>','');
	else set ret = src;
	end if;
	return ret;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogoadm`@`%`*/ /*!50003 FUNCTION `intervalo_a_fecha`(intervalo_p varchar(20)) RETURNS varchar(200) CHARSET latin1
    DETERMINISTIC
BEGIN
	declare ret varchar(200);
	declare fechaSinHora varchar(20);
	
	set fechaSinHora = left(intervalo_p,6);
	set ret=INSERT(fechaSinHora,3,0,'-');
	set ret=INSERT(ret,6,0,'-');
	return ret;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogo`@`%`*/ /*!50003 FUNCTION `intervalo_media_hora`(dato varchar(200)) RETURNS varchar(200) CHARSET latin1
begin
    declare minutos varchar(2);
    declare intMinutos INTEGER;
    declare retorno varchar(200);
    set minutos = MID(dato,CHAR_LENGTH(dato)-1,2);
    set intMinutos = CAST(minutos AS SIGNED);
    
    if (intMinutos<30) then
        set retorno = '00';
    else set retorno ='30';
    end if;
    return retorno;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogo`@`%`*/ /*!50003 FUNCTION `intervalo_texto`(dato varchar(200), hora varchar(200)) RETURNS varchar(200) CHARSET latin1
begin
    declare minutos varchar(2);
    declare intMinutos INTEGER;
    declare retorno varchar(200);
    set minutos = MID(dato,CHAR_LENGTH(dato)-1,2);
    set intMinutos = CAST(minutos AS SIGNED);
    
    if (intMinutos<30) then
        set retorno = concat(hora,':00',' - ',hora,':29');
    else set retorno = concat(hora,':30',' - ',hora,':59');
    end if;
    return retorno;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 FUNCTION `intervalo_traducido`(intervalo_p varchar(255)) RETURNS varchar(200) CHARSET latin1
BEGIN
	DECLARE ret varchar (200);
		if(LENGTH(intervalo_p)=8) then set ret = mid(intervalo_p,LENGTH(intervalo_p)-1,LENGTH(intervalo_p));
		end if;

		if(LENGTH(intervalo_p)=10) then set ret = insert(cast(substr(intervalo_p,7,4) as char charset utf8),3,0,':');
		end if;

	return ret;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 FUNCTION `llamada_espera_campana`(campana_p int) RETURNS int(11)
BEGIN
	declare ret int;
	set ret = (select count(*) from dy_llamadas_espera where id_campana=campana_p);
	return ret;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 FUNCTION `nombre_dia`(fecha datetime) RETURNS varchar(200) CHARSET latin1
BEGIN
	declare ret varchar(200);
	declare diaEn varchar(200);
	set diaEn = DAYNAME(fecha);
	
	if(diaEn='Monday') then set ret='1-Lunes';
	end if;

	if(diaEn='Tuesday') then set ret='2-Martes';
	end if;

	if(diaEn='Wednesday') then set ret='3-Miercoles';
	end if;

	if(diaEn='Thursday') then set ret='4-Jueves';
	end if;

	if(diaEn='Friday') then set ret='5-Viernes';
	end if;

	if(diaEn='Saturday') then set ret='6-Sabado';
	end if;

	if(diaEn='Sunday') then set ret='7-Domingo';
	end if;
	
	return ret;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogo`@`%`*/ /*!50003 FUNCTION `queue_log_duracion`(dato varchar(200), evento varchar(200)) RETURNS varchar(200) CHARSET latin1
begin
    if (evento='COMPLETEAGENT' OR evento='COMPLETECALLER') then
        return dato;
    else return 0;
    end if;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogoadm`@`%`*/ /*!50003 FUNCTION `retorna_estado_corregido`(estado varchar(200)) RETURNS varchar(200) CHARSET latin1
BEGIN
	declare ret varchar(200);
	if(INSTR(estado,' ')=0) then set ret=estado;
	else set ret = REPLACE(substring(estado,1,INSTR(estado,' ')),'Llamada','Llamada Entrante');
	end if;
	return ret;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogoadm`@`%`*/ /*!50003 FUNCTION `tiempo_productivo_agente`(id_agente_p int) RETURNS varchar(200) CHARSET latin1
BEGIN
	DECLARE ret varchar(200);
	DECLARE duracionSesion int;
	DECLARE duracionDescanso int;
	DECLARE fhInicioSesion datetime;

	set fhInicioSesion = (select max(fecha_hora_inicio) from dy_sesiones where id_agente=id_agente_p and fecha_hora_fin is null);
	set duracionSesion = (select TIMESTAMPDIFF(SECOND,max(fecha_hora_inicio),now()) from dy_sesiones where id_agente=id_agente_p and fecha_hora_fin is null);
	set duracionDescanso = (select ifnull(sum(duracion),0) from dy_descansos where id_agente=id_agente_p and fecha_hora_inicio>=fhInicioSesion);
	set ret = concat(SEC_TO_TIME(duracionSesion-duracionDescanso),'.');
	return ret;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_actualiza_actividad_actual`(numero_extension_p int, actualiza_fecha_llamada_p bool, estado_p varchar(255), estado_desc_p varchar(255), fecha_estado_p datetime, fecha_llamada_p datetime)
BEGIN
	
	declare id_actividad_actual int;

	set id_actividad_actual = (select max(id) from dy_v_actividad_actual where extension=numero_extension_p);

	IF actualiza_fecha_llamada_p = true then UPDATE dy_actividad_actual SET estado=estado_p , estado_desc=estado_desc_p , fecha_hora=fecha_estado_p , fecha_hora_llamada=fecha_llamada_p where id=id_actividad_actual;
	else UPDATE dy_actividad_actual SET estado=estado_p , estado_desc=estado_desc_p , fecha_hora=fecha_estado_p  where id=id_actividad_actual;
	END IF;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogoadm`@`%`*/ /*!50003 PROCEDURE `sp_actualiza_grabacion`(uniqueid_p varchar(255), grabacion_p varchar(255))
BEGIN

    declare idLlamada INT;
    declare cuenta INT;

    set idLlamada = 0;
    set cuenta = (select count(*) from dy_llamadas where unique_id=uniqueid_p);
    
    if(cuenta<=0) then set idLlamada = (select id from dy_llamadas where unique_id_original=uniqueid_p); 
    else set idLlamada = (select id from dy_llamadas where unique_id=uniqueid_p);
    end if;

    

    update dy_llamadas set grabacion = grabacion_p where id=idLlamada;
    update dy_llamadas_espejo set grabacion = grabacion_p where id=idLlamada;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogoadm`@`%`*/ /*!50003 PROCEDURE `sp_borrar_agente`(id_agente_p int)
BEGIN
	

	delete from dy_campanas_agentes where id_agente = id_agente_p;
	delete from dy_llamadas where id_agente = id_agente_p;
	delete from dy_agentes_turnos where id_agente = id_agente_p;

	delete from dy_sesiones_campanas;
	delete from dy_descansos_campanas;

	delete from dy_sesiones where id_agente = id_agente_p;
	delete from dy_descansos where id_agente = id_agente_p; 

	delete from dy_agentes where id = id_agente_p;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogoadm`@`%`*/ /*!50003 PROCEDURE `sp_borrar_extension`(id_extension_p int)
BEGIN
	

            delete from dy_llamadas where id_extension = id_extension_p;
	
        delete from dy_sesiones where id_extension = id_extension_p;

	END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_inserta_llamada_entrante`(uniqueid_p varchar(255), exten_p varchar(255), llamante_p varchar(255), canal_p varchar(255),id_ruta_entrante_p int)
BEGIN
	INSERT INTO `dyalogo_telefonia`.`dy_llamadas_entrantes` (fecha_hora, unique_id, exten, llamante, canal, id_ruta_entrante) 
	VALUES (now(), uniqueid_p, exten_p, llamante_p, canal_p, id_ruta_entrante_p);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_inserta_llamada_interna`(uniqueid_p varchar(255), exten_origen_p int, exten_destino_p int)
BEGIN
	INSERT INTO `dyalogo_telefonia`.`dy_llamadas_internas` (fecha_hora, unique_id, extension_origen, extension_destino) 
	VALUES (now(), uniqueid_p, exten_origen_p, exten_destino_p);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_inserta_llamada_saliente`(uniqueid_p varchar(255), extension_p int, numero_marcado_p varchar(255), id_troncal_p int, id_ruta_saliente_p int, nombre_troncal_p varchar(255), id_agente_p int)
BEGIN
	insert into dy_llamadas_salientes(fecha_hora, unique_id, numero_extension,numero_marcado,id_troncal,id_ruta_saliente,nombre_troncal, id_agente) 
	values (now(), uniqueid_p, extension_p, numero_marcado_p,id_troncal_p,id_ruta_saliente_p,nombre_troncal_p,id_agente_p);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_inserta_registro_ivr`(unique_id_p varchar(255), nombre_ivr_p varchar(255), opcion_interna_p varchar(255))
BEGIN

	declare intIvr_______t int;
	declare intOpciIvr___t int;
	
	set intIvr_______t = (select id from dy_ivrs where nombre_interno_ivr=nombre_ivr_p);
	set intOpciIvr___t = (select max(id) from dy_opciones_ivrs where id_ivr=intIvr_______t and nombre_interno_opcion = opcion_interna_p);

	insert into dy_log_ivrs(unique_id, fecha_hora, id_opcion) values(unique_id_p, now(), intOpciIvr___t);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_llena_encuesta`(fecha_hora_p datetime,id_encuesta_p int, id_pregunta_p int, uniqueid_p varchar(255), callerid_p varchar(255), tag_p varchar(255), campana_p varchar(255), agente_p varchar(255), respuesta_p int)
BEGIN
	declare idcampana int;
	declare idagente int;

	set idcampana = (select id from dy_campanas where nombre_interno like campana_p);
	set idagente = MID(agente_p,1,INSTR(agente_p,'|')-1);

	insert into dy_encuestas_resultados(fecha_hora,id_encuesta,id_pregunta, unique_id, caller_id,tag,campana,id_campana, nombre_agente,id_agente,respuesta)
	values(fecha_hora_p, id_encuesta_p, id_pregunta_p, uniqueid_p, callerid_p,tag_p, campana_p,idcampana,agente_p,idagente,respuesta_p);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_llena_info_act_cam`(id_campana_p int)
BEGIN
	declare segundos_tsf INT;
	declare segundos_abandono INT;
	declare m_tsf INT;
	declare recibidas INT;
	declare contestadas INT;
	declare contestadas_ns INT;
	declare contestadas_despues_s_tsf INT;
	declare abandonadas INT;
	declare abandonadas_despues_s_tsf INT;
	declare nivel_servicio DOUBLE;
	declare agentes_conectados INT;
	declare agentes_descanso INT;
	declare agentes_disponibles INT;
	declare agentes_llamando INT;
	declare fondo_tsf VARCHAR(100);
	declare fondo_tsf_contestadas_t VARCHAR(45);
	declare nom_campana VARCHAR(100);
	declare llamadas_esperando INT;
	declare otras_llamadas_t int;
	declare recibidas_total INT;
	

	declare nivelServContestadas DOUBLE;
	declare nivelAbandonoUmbral DOUBLE;
	declare abandonadas_total INT;
	declare porcentajeParticipacion DOUBLE;
	
	
	delete from dy_informacion_actual_campanas where id_campana = id_campana_p and fecha>=CURDATE();
	set segundos_tsf = (select tiempo_tsf  from dy_campanas where id=id_campana_p);
	set segundos_abandono = (select tiempo_abandono  from dy_campanas where id=id_campana_p);
	set m_tsf = (select meta_tsf  from dy_campanas where id=id_campana_p);
	set nom_campana = (select nombre_usuario from dy_campanas where id=id_campana_p);
	

	set recibidas_total = (select count(*) from dy_llamadas_espejo where DATE(fecha_hora)=CURDATE() and sentido='Entrante' and id_excepcion is null);
	set recibidas = (select count(*) from dy_llamadas_espejo where DATE(fecha_hora)=CURDATE() and id_campana_espejo=id_campana_p and sentido='Entrante' and id_excepcion is null);
	set contestadas = (select count(*) from dy_llamadas_espejo where DATE(fecha_hora)=CURDATE() and id_campana_espejo=id_campana_p and resultado='Contestada' and sentido='Entrante'  and id_excepcion is null);
	set contestadas_ns = (select count(*) from dy_llamadas_espejo where DATE(fecha_hora)=CURDATE() and id_campana_espejo=id_campana_p and resultado='Contestada' and tiempo_espera<=segundos_tsf and sentido='Entrante'  and id_excepcion is null);
	set contestadas_despues_s_tsf = (select count(*) from dy_llamadas_espejo where DATE(fecha_hora)=CURDATE() and id_campana_espejo=id_campana_p and resultado='Contestada' and tiempo_espera>segundos_tsf and sentido='Entrante' and id_excepcion is null);
	set abandonadas = (select count(*) from dy_llamadas_espejo where DATE(fecha_hora)=CURDATE() and id_campana_espejo=id_campana_p and resultado like  'Abandona%' and tiempo_espera<=segundos_abandono and sentido='Entrante' and id_excepcion is null);
	set abandonadas_despues_s_tsf = (select count(*) from dy_llamadas_espejo where DATE(fecha_hora)=CURDATE() and id_campana_espejo=id_campana_p and resultado ='Abandonada' and tiempo_espera>segundos_abandono and sentido='Entrante' and id_excepcion is null);
	set abandonadas_total = abandonadas+abandonadas_despues_s_tsf;

	set otras_llamadas_t = recibidas_total-recibidas;

	if(recibidas>0) then set nivel_servicio = ((contestadas_ns/recibidas)*100); end if;
	if(recibidas<=0) then set nivel_servicio = 0; end if;

	
	if(recibidas>0) then set nivelServContestadas = ((contestadas_ns/contestadas)*100); end if;
	if(recibidas<=0) then set nivelServContestadas = 0; end if;
	
	if(recibidas>0) then set nivelAbandonoUmbral = ((abandonadas_despues_s_tsf/(contestadas+abandonadas_despues_s_tsf))*100); end if;

	

	set porcentajeParticipacion = (contestadas_despues_s_tsf/contestadas)*100;
	
	
	select @aht_t:=avg(duracion_al_aire),@duracion_total_llamadas:=sum(duracion_al_aire),@espera_maximo:=max(tiempo_espera), @espera_minimo:=min(tiempo_espera), @espera_promedio:=avg(tiempo_espera), @espera_total:=sum(tiempo_espera) from dy_llamadas_espejo where resultado='Contestada' and id_campana_espejo=id_campana_p and DATE(fecha_hora)=CURDATE()  and sentido='Entrante'  and id_excepcion is null;
	if(@aht_t is null) then set @aht_t:=0; end if;
	if(@duracion_total_llamadas is null) then set @duracion_total_llamadas:=0; end if;
	if(@espera_maximo is null) then set @espera_maximo:=0; end if;
	if(@espera_minimo is null) then set @espera_minimo:=0; end if;
	if(@espera_promedio is null) then set @espera_promedio:=0; end if;
	if(@espera_total is null) then set @espera_total:=0; end if;

	select @espera_maxima_a:=max(tiempo_espera), @espera_minima_a:=min(tiempo_espera), @espera_promedio_a:=avg(tiempo_espera), @espera_total_a:=sum(tiempo_espera) from dy_llamadas_espejo where resultado='Abandonada' and id_campana_espejo=id_campana_p and DATE(fecha_hora)=CURDATE()  and sentido='Entrante'  and id_excepcion is null;

	if(@espera_maxima_a is null) then set @espera_maxima_a:=0; end if;
	if(@espera_minima_a is null) then set @espera_minima_a:=0; end if;
	if(@espera_promedio_a is null) then set @espera_promedio_a:=0; end if;
	if(@espera_total_a is null) then set @espera_total_a:=0; end if;

	set agentes_conectados = (select count(*) from dy_v_actividad_actual_por_campanas where campana_id=id_campana_p);
	set agentes_descanso = (select count(*) from dy_v_actividad_actual_por_campanas where estado='Descanso' and campana_id=id_campana_p);
	set agentes_disponibles = (select count(*) from dy_v_actividad_actual_por_campanas where estado='Disponible' and campana_id=id_campana_p);
	set agentes_llamando = (select count(*) from dy_v_actividad_actual_por_campanas where estado like '%onectado%' and campana_id=id_campana_p);
	
	set fondo_tsf = 'ffffff';
	if(nivel_servicio>=m_tsf) then set fondo_tsf='35d224'; end if;
	if(nivel_servicio<=m_tsf && nivel_servicio>=(m_tsf-5)) then set fondo_tsf='f19e1c'; end if;
	if(nivel_servicio<(m_tsf-5)) then set fondo_tsf='d22424'; end if;

	set fondo_tsf_contestadas_t= 'ffffff';
	if(nivelServContestadas>=m_tsf) then set fondo_tsf_contestadas_t='35d224'; end if;
	if(nivelServContestadas<=m_tsf && nivelServContestadas>=(m_tsf-5)) then set fondo_tsf_contestadas_t='f19e1c'; end if;
	if(nivelServContestadas<(m_tsf-5)) then set fondo_tsf_contestadas_t='d22424'; end if;

	set llamadas_esperando = 0;

	insert into 
	dy_informacion_actual_campanas(fecha,id_campana,nombre_campana,segundos_tsf,
		meta_tsf,recibidas,contestadas,contestadas_ns,contestadas_despues_s_tsf,abandonadas,abandonadas_despues_s_tsf,nivel_servicio,
		espera_promedio,espera_minimo,espera_maximo,agentes_conectados,agentes_descanso,agentes_disponibles,
		agentes_llamando,fondo_tsf,llamadas_esperando,nivel_servicio_contestadas,nivel_abandono_umbral,abandonadas_total
		,porcentaje_participacion,aht,espera_total,espera_minima_abandono,espera_maxima_abandono,espera_promedio_abandono,
		espera_total_abandono,otras_llamadas,tiempo_conversacion_total,fondo_tsf_contestadas,recibidas_totales) 

	values(curdate(),id_campana_p,nom_campana,segundos_tsf,m_tsf,recibidas,contestadas,contestadas_ns,contestadas_despues_s_tsf,abandonadas,
		abandonadas_despues_s_tsf,nivel_servicio,@espera_promedio,@espera_minimo,@espera_maximo,agentes_conectados,
		agentes_descanso,agentes_disponibles,agentes_llamando,fondo_tsf,llamadas_esperando,nivelServContestadas,
		nivelAbandonoUmbral,abandonadas_total,porcentajeParticipacion,@aht_t,@espera_total,@espera_minima_a,@espera_maxima_a,
		@espera_promedio_a,@espera_total_a,otras_llamadas_t,@duracion_total_llamadas,fondo_tsf_contestadas_t,recibidas_total);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_llena_info_act_cam_fecha`(id_campana_p int,fecha_p DATE)
BEGIN
	declare segundos_tsf INT;
	declare segundos_abandono INT;
	declare m_tsf INT;
	declare recibidas INT;
	declare contestadas INT;
	declare contestadas_ns INT;
	declare contestadas_despues_s_tsf INT;
	declare abandonadas INT;
	declare abandonadas_despues_s_tsf INT;
	declare nivel_servicio DOUBLE;
	declare fondo_tsf VARCHAR(100);
	declare fondo_tsf_contestadas_t VARCHAR(45);
	declare nom_campana VARCHAR(100);
	declare llamadas_esperando INT;
	declare otras_llamadas_t int;
	

	declare nivelServContestadas DOUBLE;
	declare nivelAbandonoUmbral DOUBLE;
	declare abandonadas_total INT;
	declare porcentajeParticipacion DOUBLE;
	declare recibidas_total INT;
	

	delete from dy_informacion_actual_campanas where id_campana = id_campana_p and fecha=fecha_p;
	set segundos_tsf = (select tiempo_tsf  from dy_campanas where id=id_campana_p);
	set segundos_abandono = (select tiempo_abandono  from dy_campanas where id=id_campana_p);
	set m_tsf = (select meta_tsf  from dy_campanas where id=id_campana_p);
	set nom_campana = (select nombre_usuario from dy_campanas where id=id_campana_p);
	
	set recibidas_total = (select count(*) from dy_llamadas_espejo where DATE(fecha_hora)=fecha_p and sentido='Entrante' and id_excepcion is null);
	set recibidas = (select count(*) from dy_llamadas_espejo where DATE(fecha_hora)=fecha_p and id_campana_espejo=id_campana_p and sentido='Entrante' and id_excepcion is null);
	set contestadas = (select count(*) from dy_llamadas_espejo where DATE(fecha_hora)=fecha_p and id_campana_espejo=id_campana_p and resultado='Contestada' and sentido='Entrante' and id_excepcion is null);
	set contestadas_ns = (select count(*) from dy_llamadas_espejo where DATE(fecha_hora)=fecha_p and id_campana_espejo=id_campana_p and resultado='Contestada' and tiempo_espera<=segundos_tsf and sentido='Entrante' and id_excepcion is null);
	set contestadas_despues_s_tsf = (select count(*) from dy_llamadas_espejo where DATE(fecha_hora)=fecha_p and id_campana_espejo=id_campana_p and resultado='Contestada' and tiempo_espera>segundos_tsf and sentido='Entrante' and id_excepcion is null);
	set abandonadas = (select count(*) from dy_llamadas_espejo where DATE(fecha_hora)=fecha_p and id_campana_espejo=id_campana_p and resultado like  'Abandona%' and tiempo_espera<=segundos_abandono and sentido='Entrante' and id_excepcion is null);
	set abandonadas_despues_s_tsf = (select count(*) from dy_llamadas_espejo where DATE(fecha_hora)=fecha_p and id_campana_espejo=id_campana_p and resultado ='Abandonada' and tiempo_espera>segundos_abandono and sentido='Entrante' and id_excepcion is null);
	set abandonadas_total = abandonadas+abandonadas_despues_s_tsf;

	set otras_llamadas_t = recibidas_total-recibidas;

	if(recibidas>0) then set nivel_servicio = ((contestadas_ns/recibidas)*100); end if;
	if(recibidas<=0) then set nivel_servicio = 0; end if;

	
	if(contestadas>0) then set nivelServContestadas = ((contestadas_ns/contestadas)*100); end if;
	if(contestadas<=0) then set nivelServContestadas = 0; end if;
	
	
	if(recibidas>0) then set nivelAbandonoUmbral = ((abandonadas_despues_s_tsf/(contestadas+abandonadas_despues_s_tsf))*100); end if;

	

	set porcentajeParticipacion = (contestadas_despues_s_tsf/contestadas)*100;
	
	
	select @aht_t:=avg(duracion_al_aire),@duracion_total_llamadas:=sum(duracion_al_aire),@espera_maximo:=max(tiempo_espera), @espera_minimo:=min(tiempo_espera), @espera_promedio:=avg(tiempo_espera), @espera_total:=sum(tiempo_espera) from dy_llamadas_espejo where resultado='Contestada' and id_campana_espejo=id_campana_p and DATE(fecha_hora)=fecha_p  and sentido='Entrante' and id_excepcion is null;
	if(@aht_t is null) then set @aht_t:=0; end if;
	if(@duracion_total_llamadas is null) then set @duracion_total_llamadas:=0; end if;
	if(@espera_maximo is null) then set @espera_maximo:=0; end if;
	if(@espera_minimo is null) then set @espera_minimo:=0; end if;
	if(@espera_promedio is null) then set @espera_promedio:=0; end if;
	if(@espera_total is null) then set @espera_total:=0; end if;

	
	select @espera_maxima_a:=max(tiempo_espera), @espera_minima_a:=min(tiempo_espera), @espera_promedio_a:=avg(tiempo_espera), @espera_total_a:=sum(tiempo_espera) from dy_llamadas_espejo where resultado='Abandonada' and id_campana_espejo=id_campana_p and DATE(fecha_hora)=fecha_p  and sentido='Entrante' and id_excepcion is null;

	if(@espera_maxima_a is null) then set @espera_maxima_a:=0; end if;
	if(@espera_minima_a is null) then set @espera_minima_a:=0; end if;
	if(@espera_promedio_a is null) then set @espera_promedio_a:=0; end if;
	if(@espera_total_a is null) then set @espera_total_a:=0; end if;


	set fondo_tsf = 'ffffff';
	if(nivel_servicio>=m_tsf) then set fondo_tsf='35d224'; end if;
	if(nivel_servicio<=m_tsf && nivel_servicio>=(m_tsf-5)) then set fondo_tsf='f19e1c'; end if;
	if(nivel_servicio<(m_tsf-5)) then set fondo_tsf='d22424'; end if;

	set fondo_tsf_contestadas_t= 'ffffff';
	if(nivelServContestadas>=m_tsf) then set fondo_tsf_contestadas_t='35d224'; end if;
	if(nivelServContestadas<=m_tsf && nivelServContestadas>=(m_tsf-5)) then set fondo_tsf_contestadas_t='f19e1c'; end if;
	if(nivelServContestadas<(m_tsf-5)) then set fondo_tsf_contestadas_t='d22424'; end if;


	insert into 
	dy_informacion_actual_campanas(fecha,id_campana,nombre_campana,segundos_tsf,
		meta_tsf,recibidas,contestadas,contestadas_ns,contestadas_despues_s_tsf,abandonadas,abandonadas_despues_s_tsf,nivel_servicio,
		espera_promedio,espera_minimo,espera_maximo,fondo_tsf,nivel_servicio_contestadas,nivel_abandono_umbral,abandonadas_total
		,porcentaje_participacion,aht,espera_total,espera_minima_abandono,espera_maxima_abandono,
		espera_promedio_abandono,espera_total_abandono,tiempo_conversacion_total,otras_llamadas,
	fondo_tsf_contestadas,recibidas_totales) 
	values(fecha_p,id_campana_p,nom_campana,segundos_tsf,m_tsf,recibidas,contestadas,contestadas_ns,contestadas_despues_s_tsf,abandonadas,
		abandonadas_despues_s_tsf,nivel_servicio,@espera_promedio,@espera_minimo,@espera_maximo,
		fondo_tsf,nivelServContestadas,
		nivelAbandonoUmbral,abandonadas_total,porcentajeParticipacion,@aht_t,@espera_total,@espera_minima_a,@espera_maxima_a,
		@espera_promedio_a,@espera_total_a,@duracion_total_llamadas,otras_llamadas_t,fondo_tsf_contestadas_t,recibidas_total);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogoadm`@`%`*/ /*!50003 PROCEDURE `sp_llena_info_intervalos_ch`(id_campana_p int, intervalo_ch int)
BEGIN
	declare segundos_tsf INT;
	declare segundos_abandono INT;
	declare m_tsf INT;
	declare recibidas INT;
	declare contestadas INT;
	declare contestadas_ns INT;
	declare contestadas_despues_s_tsf INT;
	declare abandonadas INT;
	declare abandonadas_despues_s_tsf INT;
	declare nivel_servicio DOUBLE;
	declare agentes_conectados INT;
	declare fondo_tsf VARCHAR(100);
	declare nom_campana VARCHAR(100);
	declare intervalo_traducido_t VARCHAR(100);
	

	declare nivelServContestadas DOUBLE;
	declare nivelAbandonoUmbral DOUBLE;
	declare abandonadas_total INT;
	declare porcentajeParticipacion DOUBLE;

	declare fecha_hora_llamada DATE;
	set fecha_hora_llamada = (select date(max(fecha_hora)) from dy_llamadas_espejo where cuarto_hora=intervalo_ch and id_campana_espejo=id_campana_p);
	
	
	set segundos_tsf = (select tiempo_tsf  from dy_campanas where id=id_campana_p);
	set segundos_abandono = (select tiempo_abandono  from dy_campanas where id=id_campana_p);
	set m_tsf = (select meta_tsf  from dy_campanas where id=id_campana_p);
	set nom_campana = (select nombre_usuario from dy_campanas where id=id_campana_p);
	
	set recibidas = (select count(*) from dy_llamadas_espejo where cuarto_hora=intervalo_ch and id_campana_espejo=id_campana_p and sentido='Entrante' and id_excepcion is null);
	set contestadas = (select count(*) from dy_llamadas_espejo where cuarto_hora=intervalo_ch and id_campana_espejo=id_campana_p and resultado='Contestada' and sentido='Entrante' and id_excepcion is null);
	set contestadas_ns = (select count(*) from dy_llamadas_espejo where cuarto_hora=intervalo_ch and id_campana_espejo=id_campana_p and resultado='Contestada' and tiempo_espera<=segundos_tsf and sentido='Entrante' and id_excepcion is null);
	set contestadas_despues_s_tsf = (select count(*) from dy_llamadas_espejo where cuarto_hora=intervalo_ch and id_campana_espejo=id_campana_p and resultado='Contestada' and tiempo_espera>segundos_tsf and sentido='Entrante' and id_excepcion is null);
	set abandonadas = (select count(*) from dy_llamadas_espejo where cuarto_hora=intervalo_ch and id_campana_espejo=id_campana_p and resultado = 'Abandonada' and tiempo_espera<=segundos_abandono and sentido='Entrante' and id_excepcion is null);
	set abandonadas_despues_s_tsf = (select count(*) from dy_llamadas_espejo where cuarto_hora=intervalo_ch and id_campana_espejo=id_campana_p and resultado ='Abandonada' and tiempo_espera>segundos_abandono and sentido='Entrante' and id_excepcion is null);
	set abandonadas_total = abandonadas+abandonadas_despues_s_tsf;

	if(recibidas>0) then set nivel_servicio = ((contestadas_ns/recibidas)*100); end if;
	if(recibidas<=0) then set nivel_servicio = 0; end if;

	
	if(recibidas>0) then set nivelServContestadas = ((contestadas_ns/contestadas)*100); end if;
	if(recibidas<=0) then set nivelServContestadas = 0; end if;
	
	
	if(recibidas>0) then set nivelAbandonoUmbral = ((abandonadas_despues_s_tsf/(contestadas+abandonadas_despues_s_tsf))*100); end if;

	

	set porcentajeParticipacion = (contestadas_despues_s_tsf/contestadas)*100;
	
	
	select @aht_t:=avg(duracion_al_aire),@espera_maximo:=max(tiempo_espera), @espera_minimo:=min(tiempo_espera), @espera_promedio:=avg(tiempo_espera), @espera_total:=sum(tiempo_espera) from dy_llamadas_espejo where resultado='Contestada' and id_campana_espejo=id_campana_p and cuarto_hora=intervalo_ch and sentido='Entrante' and id_excepcion is null;

	select @espera_maxima_a:=max(tiempo_espera), @espera_minima_a:=min(tiempo_espera), @espera_promedio_a:=avg(tiempo_espera), @espera_total_a:=sum(tiempo_espera) from dy_llamadas_espejo where resultado='Abandonada' and id_campana_espejo=id_campana_p and cuarto_hora=intervalo_ch and sentido='Entrante' and id_excepcion is null;

	set agentes_conectados = (select count(*) from dy_v_actividad_actual_por_campanas where campana_id=id_campana_p);

	
	
	set fondo_tsf = 'ffffff';
	if(nivel_servicio>=m_tsf) then set fondo_tsf='35d224'; end if;
	if(nivel_servicio<=m_tsf && nivel_servicio>=(m_tsf-5)) then set fondo_tsf='f19e1c'; end if;
	if(nivel_servicio<(m_tsf-5)) then set fondo_tsf='d22424'; end if;

	set intervalo_traducido_t = (select intervalo_traducido(intervalo_ch));
	insert into 
	dy_informacion_intervalos_ch(fecha,id_campana,nombre_campana,segundos_tsf,
		meta_tsf,recibidas,contestadas,contestadas_ns,contestadas_despues_s_tsf,abandonadas,abandonadas_despues_s_tsf,nivel_servicio,
		espera_promedio,espera_minimo,espera_maximo,agentes_conectados,fondo_tsf,llamadas_esperando,nivel_servicio_contestadas,nivel_abandono_umbral,abandonadas_total
		,porcentaje_participacion,aht,espera_total,espera_minima_abandono,espera_maxima_abandono,espera_promedio_abandono,espera_total_abandono
		,cuarto_hora,intervalo_traducido) 
	values(fecha_hora_llamada,id_campana_p,nom_campana,segundos_tsf,m_tsf,recibidas,contestadas,contestadas_ns,contestadas_despues_s_tsf,abandonadas,
		abandonadas_despues_s_tsf,nivel_servicio,@espera_promedio,@espera_minimo,@espera_maximo,agentes_conectados,
		fondo_tsf,llamadas_esperando,nivelServContestadas,
		nivelAbandonoUmbral,abandonadas_total,porcentajeParticipacion,@aht_t,@espera_total,@espera_minima_a,@espera_maxima_a,
		@espera_promedio_a,@espera_total_a,intervalo_ch,intervalo_traducido_t);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogoadm`@`%`*/ /*!50003 PROCEDURE `sp_llena_info_intervalos_h`(id_campana_p int, intervalo_h int)
BEGIN
	declare segundos_tsf INT;
	declare segundos_abandono INT;
	declare m_tsf INT;
	declare recibidas INT;
	declare contestadas INT;
	declare contestadas_ns INT;
	declare contestadas_despues_s_tsf INT;
	declare abandonadas INT;
	declare abandonadas_despues_s_tsf INT;
	declare nivel_servicio DOUBLE;
	declare agentes_conectados INT;
	declare fondo_tsf VARCHAR(100);
	declare fondo_tsf_contestadas_t VARCHAR(45);
	declare nom_campana VARCHAR(100);
	declare intervalo_traducido_t VARCHAR(100);
	declare otras_llamadas_t int;	

	declare nivelServContestadas DOUBLE;
	declare nivelAbandonoUmbral DOUBLE;
	declare abandonadas_total INT;
	declare porcentajeParticipacion DOUBLE;

	declare fecha_hora_llamada DATE;
	set fecha_hora_llamada = (select date(max(fecha_hora))  from dy_llamadas_espejo where hora=intervalo_h and id_campana_espejo=id_campana_p);
	if(fecha_hora_llamada is null) then set fecha_hora_llamada = STR_TO_DATE(intervalo_a_fecha(intervalo_h),'%Y-%m-%d'); end if;
	
	
	set segundos_tsf = (select tiempo_tsf  from dy_campanas where id=id_campana_p);
	set segundos_abandono = (select tiempo_abandono  from dy_campanas where id=id_campana_p);
	set m_tsf = (select meta_tsf  from dy_campanas where id=id_campana_p);
	set nom_campana = (select nombre_usuario from dy_campanas where id=id_campana_p);

	update dy_llamadas_espejo set tiempo_espera=0 where tiempo_espera is null and hora=intervalo_h;
	
	set recibidas = (select count(*) from dy_llamadas_espejo where hora=intervalo_h and id_campana_espejo=id_campana_p and sentido='Entrante' and id_excepcion is null);
	set contestadas = (select count(*) from dy_llamadas_espejo where hora=intervalo_h and id_campana_espejo=id_campana_p and resultado='Contestada'  and sentido='Entrante' and id_excepcion is null);
	set contestadas_ns = (select count(*) from dy_llamadas_espejo where hora=intervalo_h and id_campana_espejo=id_campana_p and resultado='Contestada' and tiempo_espera<=segundos_tsf  and sentido='Entrante' and id_excepcion is null);
	set contestadas_despues_s_tsf = (select count(*) from dy_llamadas_espejo where hora=intervalo_h and id_campana_espejo=id_campana_p and resultado='Contestada' and tiempo_espera>segundos_tsf  and sentido='Entrante' and id_excepcion is null);
	set abandonadas = (select count(*) from dy_llamadas_espejo where hora=intervalo_h and id_campana_espejo=id_campana_p and resultado = 'Abandonada' and tiempo_espera<=segundos_abandono  and sentido='Entrante' and id_excepcion is null);
	set abandonadas_despues_s_tsf = (select count(*) from dy_llamadas_espejo where hora=intervalo_h and id_campana_espejo=id_campana_p and resultado ='Abandonada' and tiempo_espera>segundos_abandono  and sentido='Entrante' and id_excepcion is null);
	set abandonadas_total = abandonadas+abandonadas_despues_s_tsf;

	set otras_llamadas_t = recibidas-(contestadas+abandonadas_total);

	if(recibidas>0) then set nivel_servicio = ((contestadas_ns/recibidas)*100); end if;
	if(recibidas<=0) then set nivel_servicio = 0; end if;

	
	if(contestadas>0) then set nivelServContestadas = ((contestadas_ns/contestadas)*100); end if;
	if(contestadas<=0) then set nivelServContestadas = 0; end if;
	
	
	if((contestadas+abandonadas_despues_s_tsf)>0) then set nivelAbandonoUmbral = ((abandonadas_despues_s_tsf/(contestadas+abandonadas_despues_s_tsf))*100); end if;
	if((contestadas+abandonadas_despues_s_tsf)<=0) then set nivelAbandonoUmbral = 0; end if;

	

	set porcentajeParticipacion = (contestadas_despues_s_tsf/contestadas)*100;
	
	
	select @aht_t:=avg(duracion_al_aire),@duracion_total_llamadas:=sum(duracion_al_aire),@espera_maximo:=max(tiempo_espera), @espera_minimo:=min(tiempo_espera), @espera_promedio:=avg(tiempo_espera), @espera_total:=sum(tiempo_espera) from dy_llamadas_espejo where resultado='Contestada' and id_campana_espejo=id_campana_p and hora=intervalo_h  and sentido='Entrante' and id_excepcion is null;

	if(@aht_t is null) then set @aht_t:=0; end if;
	if(@duracion_total_llamadas is null) then set @duracion_total_llamadas:=0; end if;
	if(@espera_maximo is null) then set @espera_maximo:=0; end if;
	if(@espera_minimo is null) then set @espera_minimo:=0; end if;
	if(@espera_promedio is null) then set @espera_promedio:=0; end if;
	if(@espera_total is null) then set @espera_total:=0; end if;

	select @espera_maxima_a:=max(tiempo_espera), @espera_minima_a:=min(tiempo_espera), @espera_promedio_a:=avg(tiempo_espera), @espera_total_a:=sum(tiempo_espera) from dy_llamadas_espejo where resultado='Abandonada' and id_campana_espejo=id_campana_p and hora=intervalo_h  and sentido='Entrante' and id_excepcion is null;

	set agentes_conectados = (select count(*) from dy_v_actividad_actual_por_campanas where campana_id=id_campana_p);

	if(@espera_maxima_a is null) then set @espera_maxima_a:=0; end if;
	if(@espera_minima_a is null) then set @espera_minima_a:=0; end if;
	if(@espera_promedio_a is null) then set @espera_promedio_a:=0; end if;
	if(@espera_total_a is null) then set @espera_total_a:=0; end if;

	
	set fondo_tsf = 'ffffff';
	if(nivel_servicio>=m_tsf) then set fondo_tsf='35d224'; end if;
	if(nivel_servicio<=m_tsf && nivel_servicio>=(m_tsf-5)) then set fondo_tsf='f19e1c'; end if;
	if(nivel_servicio<(m_tsf-5)) then set fondo_tsf='d22424'; end if;

	set fondo_tsf_contestadas_t= 'ffffff';
	if(nivelServContestadas>=m_tsf) then set fondo_tsf_contestadas_t='35d224'; end if;
	if(nivelServContestadas<=m_tsf && nivelServContestadas>=(m_tsf-5)) then set fondo_tsf_contestadas_t='f19e1c'; end if;
	if(nivelServContestadas<(m_tsf-5)) then set fondo_tsf_contestadas_t='d22424'; end if;

	set intervalo_traducido_t = (select intervalo_traducido(intervalo_h));
	insert into 
	dy_informacion_intervalos_h(fecha,id_campana,nombre_campana,segundos_tsf,
		meta_tsf,recibidas,contestadas,contestadas_ns,contestadas_despues_s_tsf,abandonadas,abandonadas_despues_s_tsf,nivel_servicio,
		espera_promedio,espera_minimo,espera_maximo,agentes_conectados,fondo_tsf,llamadas_esperando,nivel_servicio_contestadas,nivel_abandono_umbral,abandonadas_total
		,porcentaje_participacion,aht,espera_total,espera_minima_abandono,espera_maxima_abandono,espera_promedio_abandono,espera_total_abandono
		,hora,intervalo_traducido,otras_llamadas,tiempo_conversacion_total,fondo_tsf_contestadas) 
	values(fecha_hora_llamada,id_campana_p,nom_campana,segundos_tsf,m_tsf,recibidas,contestadas,contestadas_ns,contestadas_despues_s_tsf,abandonadas,
		abandonadas_despues_s_tsf,nivel_servicio,@espera_promedio,@espera_minimo,@espera_maximo,agentes_conectados,
		fondo_tsf,llamadas_esperando,nivelServContestadas,
		nivelAbandonoUmbral,abandonadas_total,porcentajeParticipacion,@aht_t,@espera_total,@espera_minima_a,@espera_maxima_a,
		@espera_promedio_a,@espera_total_a,intervalo_h,intervalo_traducido_t,otras_llamadas_t,@duracion_total_llamadas,fondo_tsf_contestadas_t);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogoadm`@`%`*/ /*!50003 PROCEDURE `sp_llena_info_intervalos_mh`(id_campana_p int, intervalo_mh int)
BEGIN
	declare segundos_tsf INT;
	declare segundos_abandono INT;
	declare m_tsf INT;
	declare recibidas INT;
	declare contestadas INT;
	declare contestadas_ns INT;
	declare contestadas_despues_s_tsf INT;
	declare abandonadas INT;
	declare abandonadas_despues_s_tsf INT;
	declare nivel_servicio DOUBLE;
	declare agentes_conectados INT;
	declare fondo_tsf VARCHAR(100);
	declare nom_campana VARCHAR(100);
	declare intervalo_traducido_t VARCHAR(100);
	

	declare nivelServContestadas DOUBLE;
	declare nivelAbandonoUmbral DOUBLE;
	declare abandonadas_total INT;
	declare porcentajeParticipacion DOUBLE;

	declare fecha_hora_llamada DATE;
	set fecha_hora_llamada = (select date(max(fecha_hora))  from dy_llamadas_espejo where media_hora=intervalo_mh and id_campana_espejo=id_campana_p);
	
	
	set segundos_tsf = (select tiempo_tsf  from dy_campanas where id=id_campana_p);
	set segundos_abandono = (select tiempo_abandono  from dy_campanas where id=id_campana_p);
	set m_tsf = (select meta_tsf  from dy_campanas where id=id_campana_p);
	set nom_campana = (select nombre_usuario from dy_campanas where id=id_campana_p);
	
	set recibidas = (select count(*) from dy_llamadas_espejo where media_hora=intervalo_mh and id_campana_espejo=id_campana_p and sentido='Entrante'  and id_excepcion is null);
	set contestadas = (select count(*) from dy_llamadas_espejo where media_hora=intervalo_mh and id_campana_espejo=id_campana_p and resultado='Contestada' and sentido='Entrante'  and id_excepcion is null);
	set contestadas_ns = (select count(*) from dy_llamadas_espejo where media_hora=intervalo_mh and id_campana_espejo=id_campana_p and resultado='Contestada' and tiempo_espera<=segundos_tsf and sentido='Entrante'  and id_excepcion is null);
	set contestadas_despues_s_tsf = (select count(*) from dy_llamadas_espejo where media_hora=intervalo_mh and id_campana_espejo=id_campana_p and resultado='Contestada' and tiempo_espera>segundos_tsf and sentido='Entrante'  and id_excepcion is null);
	set abandonadas = (select count(*) from dy_llamadas_espejo where media_hora=intervalo_mh and id_campana_espejo=id_campana_p and resultado = 'Abandonada' and tiempo_espera<=segundos_abandono and sentido='Entrante'  and id_excepcion is null);
	set abandonadas_despues_s_tsf = (select count(*) from dy_llamadas_espejo where media_hora=intervalo_mh and id_campana_espejo=id_campana_p and resultado ='Abandonada' and tiempo_espera>segundos_abandono and sentido='Entrante'  and id_excepcion is null);
	set abandonadas_total = abandonadas+abandonadas_despues_s_tsf;

	if(recibidas>0) then set nivel_servicio = ((contestadas_ns/recibidas)*100); end if;
	if(recibidas<=0) then set nivel_servicio = 0; end if;

	
	if(recibidas>0) then set nivelServContestadas = ((contestadas_ns/contestadas)*100); end if;
	if(recibidas<=0) then set nivelServContestadas = 0; end if;
	
	
	if((contestadas+abandonadas_despues_s_tsf)>0) then set nivelAbandonoUmbral = ((abandonadas_despues_s_tsf/(contestadas+abandonadas_despues_s_tsf))*100); end if;
	if((contestadas+abandonadas_despues_s_tsf)<=0) then set nivelAbandonoUmbral = 0; end if;

	

	set porcentajeParticipacion = (contestadas_despues_s_tsf/contestadas)*100;
	
	
	select @aht_t:=avg(duracion_al_aire),@espera_maximo:=max(tiempo_espera), @espera_minimo:=min(tiempo_espera), @espera_promedio:=avg(tiempo_espera), @espera_total:=sum(tiempo_espera) from dy_llamadas_espejo where resultado='Contestada' and id_campana_espejo=id_campana_p and media_hora=intervalo_mh and sentido='Entrante'  and id_excepcion is null;

	select @espera_maxima_a:=max(tiempo_espera), @espera_minima_a:=min(tiempo_espera), @espera_promedio_a:=avg(tiempo_espera), @espera_total_a:=sum(tiempo_espera) from dy_llamadas_espejo where resultado='Abandonada' and id_campana_espejo=id_campana_p and media_hora=intervalo_mh and sentido='Entrante'  and id_excepcion is null;

	set agentes_conectados = (select count(*) from dy_v_actividad_actual_por_campanas where campana_id=id_campana_p);

	
	
	set fondo_tsf = 'ffffff';
	if(nivel_servicio>=m_tsf) then set fondo_tsf='35d224'; end if;
	if(nivel_servicio<=m_tsf && nivel_servicio>=(m_tsf-5)) then set fondo_tsf='f19e1c'; end if;
	if(nivel_servicio<(m_tsf-5)) then set fondo_tsf='d22424'; end if;

	set intervalo_traducido_t = (select intervalo_traducido(intervalo_mh));
	insert into 
	dy_informacion_intervalos_mh(fecha,id_campana,nombre_campana,segundos_tsf,
		meta_tsf,recibidas,contestadas,contestadas_ns,contestadas_despues_s_tsf,abandonadas,abandonadas_despues_s_tsf,nivel_servicio,
		espera_promedio,espera_minimo,espera_maximo,agentes_conectados,fondo_tsf,llamadas_esperando,nivel_servicio_contestadas,nivel_abandono_umbral,abandonadas_total
		,porcentaje_participacion,aht,espera_total,espera_minima_abandono,espera_maxima_abandono,espera_promedio_abandono,espera_total_abandono
		,media_hora,intervalo_traducido) 
	values(fecha_hora_llamada,id_campana_p,nom_campana,segundos_tsf,m_tsf,recibidas,contestadas,contestadas_ns,contestadas_despues_s_tsf,abandonadas,
		abandonadas_despues_s_tsf,nivel_servicio,@espera_promedio,@espera_minimo,@espera_maximo,agentes_conectados,
		fondo_tsf,llamadas_esperando,nivelServContestadas,
		nivelAbandonoUmbral,abandonadas_total,porcentajeParticipacion,@aht_t,@espera_total,@espera_minima_a,@espera_maxima_a,
		@espera_promedio_a,@espera_total_a,intervalo_mh,intervalo_traducido_t);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 PROCEDURE `sp_llena_log_opciones_ivr`(ani_p varchar(255),tag_p varchar(255),id_ivr_p int, nom_opc_act_p varchar(255), nom_opc_des_p varchar(255),digito_p int, unique_id_p varchar(255))
BEGIN
	declare var_id_opcion_actual int;
	declare var_id_opcion_destino int;
	declare var_exitoso boolean;
	
	set var_id_opcion_actual = (select id from dy_opciones_ivrs where nombre_interno_opcion=nom_opc_act_p and id_ivr=id_ivr_p);
	set var_id_opcion_destino = (select id from dy_opciones_ivrs where nombre_interno_opcion=nom_opc_des_p and id_ivr=id_ivr_p);
	

	set var_exitoso = (select if(count(1)>0,true,false) from dy_opciones_ivrs where id = var_id_opcion_destino and valida=true);	
	
	INSERT INTO dyalogo_telefonia.dy_log_opciones (ani, tag,id_ivr, opcion_actual, id_opcion_actual, opcion_destino, id_opcion_destino, digito, fecha_hora, exito, unique_id) 
											VALUES (ani_p, tag_p, id_ivr_p, nom_opc_act_p, var_id_opcion_actual, nom_opc_des_p, var_id_opcion_destino, digito_p, now(), var_exitoso, unique_id_p);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ALLOW_INVALID_DATES,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`dyalogoadm`@`%`*/ /*!50003 PROCEDURE `sp_valor_pivoteo`(IN strsql VARCHAR(9900), OUT cuenta DOUBLE)
    READS SQL DATA
BEGIN
SET @s = CONCAT('select @i :=count(*) ',strsql);
PREPARE stmt FROM @s;
EXECUTE stmt;
set cuenta =  @i;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:03:19
