-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: CRM_bases
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'CRM_bases'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`roberto.lopez`@`%`*/ /*!50003 PROCEDURE `BasesFashionCOyPE`()
BEGIN

#Base fashion co y pe

#COLOMBIA
drop table if EXISTS CRM_bases.SuscritosFashionCO;
CREATE table CRM_bases.SuscritosFashionCO
select  distinct(a.email) as email, 
    Type_Customer as Type_Customer, Source_data as source, b.gender, first_name, birthday, new_registry
     from bob_live_co.newsletter_subscription a 
inner join dev_marketing.customers_rev_co b on b.email=a.email
      where fk_catalog_store_id=2 and created_at>'2013-04-01';
#79,691 el 6mayo2014

drop table if EXISTS CRM_bases.CompradoresFashionCO;
CREATE table CRM_bases.CompradoresFashionCO
select  distinct(CustomerEmail) as email,
    Type_Customer, Source_data as source, d.gender, first_name, birthday, new_registry
  from development_co_project.A_Master c
inner join dev_marketing.customers_rev_co d on d.email=c.CustomerEmail
   where cat1 in ('Accesorios','Hombres','Mujeres','Kids','Unisex','Zapatos');
#41,888 el 6 mayo 2014

drop table if EXISTS CRM_bases.BaseFashionCO;
CREATE table CRM_bases.BaseFashionCO
select * from CRM_bases.SuscritosFashionCO UNION ALL select * from CRM_bases.CompradoresFashionCO;

#121579 el 29 abril 2014

#select * from CRM_bases.SuscritosFashionCO;
#select * from CRM_bases.CompradoresFashionCO;
#select * from CRM_bases.BaseFashionCO limit 200;

#para adri:
#select distinct email,Type_customer,gender,birthday,new_registry from CRM_bases.BaseFashionCO;
  #103,970 distintos el 24abril. 104,899 el 25.    105,309 el 28 abril    105,464 el 29


#PERU
drop table if EXISTS CRM_bases.SuscritosFashionPE;
CREATE table CRM_bases.SuscritosFashionPE
select  distinct(a.email) as email,
    Type_Customer as Type_Customer, Source_data as source, b.gender, first_name, birthday, new_registry
     from bob_live_pe.newsletter_subscription a
inner join dev_marketing.customers_rev_pe b on b.email=a.email
      where fk_catalog_store_id=2 and created_at>'2013-04-01';
#7645 el 6mayo2014

drop table if EXISTS CRM_bases.CompradoresFashionPE;
CREATE table CRM_bases.CompradoresFashionPE
select  distinct(CustomerEmail) as email,
    Type_Customer, Source_data as source, d.gender, first_name, birthday, new_registry
  from development_pe.A_Master c
inner join dev_marketing.customers_rev_pe d on d.email=c.CustomerEmail
   where cat1 in ('Accesorios','Hombres','Mujeres','Kids','Unisex','Zapatos');
#26,110 el 6mayo2014

drop table if EXISTS CRM_bases.BaseFashionPE;
CREATE table CRM_bases.BaseFashionPE
select * from CRM_bases.SuscritosFashionPE UNION ALL select * from CRM_bases.CompradoresFashionPE;
#33,755 el 6mayo2014

#select * from CRM_bases.SuscritosFashionPE;
#select * from CRM_bases.CompradoresFashionPE;
#select * from CRM_bases.BaseFashionPE;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`adrian.lopez`@`%`*/ /*!50003 PROCEDURE `CampaignFactory_Report`(
	# IN varcountry	VARCHAR(2),
	# IN vartable		VARCHAR(100)
)
BEGIN

TRUNCATE TABLE CRM_bases.MX_CF_Recos;

-- How to use: CALL CampaignFactory_Report('MX', 'campaing_messages');

INSERT INTO CRM_bases.MX_CF_Recos(sku) VALUES ('SK1');
INSERT INTO CRM_bases.MX_CF_Recos(sku) VALUES ('SK2');
INSERT INTO CRM_bases.MX_CF_Recos(sku) VALUES ('SK3');
INSERT INTO CRM_bases.MX_CF_Recos(sku) VALUES ('SK4');
INSERT INTO CRM_bases.MX_CF_Recos(sku) VALUES ('SK5');
INSERT INTO CRM_bases.MX_CF_Recos(sku) VALUES ('SK6');
INSERT INTO CRM_bases.MX_CF_Recos(sku) VALUES ('SK7');


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`roberto.lopez`@`%`*/ /*!50003 PROCEDURE `firstPurchs`()
BEGIN

#MEXICO

drop table if exists OrdersMX;
create temporary table OrdersMX
select CustomerNum,OrderNum,if(CouponCode like 'CAC%','Con CAC','Sin CAC') as isCac,Date,sum(PaidPrice) as PaidPrice,sum(PCOnePFive) as PC15, sum(Rev) as Revenue from development_mx.A_Master where OrderAfterCan = 1 group by OrderNum order by CustomerNum asc, Date asc;  #309,915

drop table if exists ordersTable;
create temporary table ordersTable (id INT auto_increment, PRIMARY KEY(CustomerNum,id))
select CustomerNum,OrderNum,isCac,NULL AS id,Date,PaidPrice,PC15,Revenue from OrdersMX; #309,915

#para crear la tabla CRM_bases.firstDatesMX
drop table if exists datesTable;
create temporary table datesTable (id INT auto_increment, PRIMARY KEY(CustomerNum,id))
select CustomerNum,Date,count(distinct OrderNum) as ordenes,NULL AS id from OrdersMX group by CustomerNum,Date;

drop table if exists fechas1;
create temporary table fechas1 (PRIMARY KEY(CustomerNum))
select CustomerNum,sum(ordenes) as Ordenes,ordenes as OrdenesEnFecha1, min(Date) as PrimeraFecha,max(Date) as UltimaFecha from datesTable group by CustomerNum;

drop table if exists fechas2;
create temporary table fechas2 (PRIMARY KEY(CustomerNum))
select CustomerNum,Date as SegundaFecha from datesTable where id = 2;

drop table if exists CRM_bases.firstDatesMX;
create table CRM_bases.firstDatesMX (PRIMARY KEY (CustomerNum))
select a.CustomerNum,Ordenes,OrdenesEnFecha1,PrimeraFecha,SegundaFecha,UltimaFecha from fechas1 a left join fechas2 b on a.CustomerNum = b.CustomerNum; #156,262

#ya se creó la tabla CRM_bases.firstDatesMX

#para crear la tabla CRM_bases.firstOrdersMX
drop table if exists orders1;
create temporary table orders1 (PRIMARY KEY (CustomerNum))
select CustomerNum,OrderNum as Orden1,isCac,Date as FechaOrd1 from ordersTable where id = 1;

drop table if exists orders2;
create temporary table orders2 (PRIMARY KEY (CustomerNum))
select CustomerNum,OrderNum as Orden2,Date as FechaOrd2 from ordersTable where id = 2;

drop table if exists ultimaOrden;
create temporary table ultimaOrden
select a.CustomerNum,Ordenes,ultimaFecha,ultimaOrden from CRM_bases.firstDatesMX a inner join (select CustomerNum,OrderNum as ultimaOrden,id from ordersTable) b on a.CustomerNum = b.CustomerNum and a.Ordenes = b.id;

drop table if exists CRM_bases.firstOrdersMX;
create table CRM_bases.firstOrdersMX (Primary Key (CustomerNum))
select a.CustomerNum,Ordenes,Orden1,isCac,FechaOrd1,Orden2,FechaOrd2,ultimaOrden,ultimaFecha from orders1 a inner join ultimaOrden b on a.CustomerNum = b.CustomerNum left join orders2 c on a.CustomerNum = c.CustomerNum;

# se creó la tabla CRM_bases.firstOrdersMX

#termina MEXICO

#empieza COLOMBIA

drop table if exists OrdersCO;
create temporary table OrdersCO
select CustomerNum,OrderNum,Date,sum(PaidPrice) as PaidPrice,sum(PCOnePFive) as PC15, sum(Rev) as Revenue from development_co_project.A_Master where OrderAfterCan = 1 group by OrderNum order by CustomerNum asc, Date asc;  #309,915

drop table if exists ordersTableCO;
create temporary table ordersTableCO (id INT auto_increment, PRIMARY KEY(CustomerNum,id))
select CustomerNum,OrderNum,NULL AS id,Date,PaidPrice,PC15,Revenue from OrdersCO; #309,915

#para crear la tabla CRM_bases.firstDatesCO
drop table if exists datesTableCO;
create temporary table datesTableCO (id INT auto_increment, PRIMARY KEY(CustomerNum,id))
select CustomerNum,Date,count(distinct OrderNum) as ordenes,NULL AS id from OrdersCO group by CustomerNum,Date;

drop table if exists fechas1CO;
create temporary table fechas1CO (PRIMARY KEY(CustomerNum))
select CustomerNum,sum(ordenes) as Ordenes,ordenes as OrdenesEnFecha1, min(Date) as PrimeraFecha,max(Date) as UltimaFecha from datesTableCO group by CustomerNum;

drop table if exists fechas2CO;
create temporary table fechas2CO (PRIMARY KEY(CustomerNum))
select CustomerNum,Date as SegundaFecha from datesTableCO where id = 2;

drop table if exists CRM_bases.firstDatesCO;
create table CRM_bases.firstDatesCO (PRIMARY KEY (CustomerNum))
select a.CustomerNum,Ordenes,OrdenesEnFecha1,PrimeraFecha,SegundaFecha,UltimaFecha from fechas1CO a left join fechas2CO b on a.CustomerNum = b.CustomerNum; #156,262

#ya se creó la tabla CRM_bases.firstDatesCO

#para crear la tabla CRM_bases.firstOrdersCO
drop table if exists orders1CO;
create temporary table orders1CO (PRIMARY KEY (CustomerNum))
select CustomerNum,OrderNum as Orden1,Date as FechaOrd1 from ordersTableCO where id = 1;

drop table if exists orders2CO;
create temporary table orders2CO (PRIMARY KEY (CustomerNum))
select CustomerNum,OrderNum as Orden2,Date as FechaOrd2 from ordersTableCO where id = 2;

drop table if exists ultimaOrdenCO;
create temporary table ultimaOrdenCO
select a.CustomerNum,Ordenes,ultimaFecha,ultimaOrden from CRM_bases.firstDatesCO a inner join (select CustomerNum,OrderNum as ultimaOrden,id from ordersTableCO) b on a.CustomerNum = b.CustomerNum and a.Ordenes = b.id;

drop table if exists CRM_bases.firstOrdersCO;
create table CRM_bases.firstOrdersCO (Primary Key (CustomerNum))
select a.CustomerNum,Ordenes,Orden1,FechaOrd1,Orden2,FechaOrd2,ultimaOrden,ultimaFecha from orders1CO a inner join ultimaOrdenCO b on a.CustomerNum = b.CustomerNum left join orders2CO c on a.CustomerNum = c.CustomerNum;

# se creó la tabla CRM_bases.firstOrdersCO

#termina COLOMBIA

#empieza PERU

drop table if exists OrdersPE;
create temporary table OrdersPE
select CustomerNum,OrderNum,Date,sum(PaidPrice) as PaidPrice,sum(PCOnePFive) as PC15, sum(Rev) as Revenue from development_pe.A_Master where OrderAfterCan = 1 group by OrderNum order by CustomerNum asc, Date asc;  #309,915

drop table if exists ordersTablePE;
create temporary table ordersTablePE (id INT auto_increment, PRIMARY KEY(CustomerNum,id))
select CustomerNum,OrderNum,NULL AS id,Date,PaidPrice,PC15,Revenue from OrdersPE; #309,915

#para crear la tabla CRM_bases.firstDatesPE
drop table if exists datesTablePE;
create temporary table datesTablePE (id INT auto_increment, PRIMARY KEY(CustomerNum,id))
select CustomerNum,Date,count(distinct OrderNum) as ordenes,NULL AS id from OrdersPE group by CustomerNum,Date;

drop table if exists fechas1PE;
create temporary table fechas1PE (PRIMARY KEY(CustomerNum))
select CustomerNum,sum(ordenes) as Ordenes,ordenes as OrdenesEnFecha1, min(Date) as PrimeraFecha,max(Date) as UltimaFecha from datesTablePE group by CustomerNum;

drop table if exists fechas2PE;
create temporary table fechas2PE (PRIMARY KEY(CustomerNum))
select CustomerNum,Date as SegundaFecha from datesTablePE where id = 2;

drop table if exists CRM_bases.firstDatesPE;
create table CRM_bases.firstDatesPE (PRIMARY KEY (CustomerNum))
select a.CustomerNum,Ordenes,OrdenesEnFecha1,PrimeraFecha,SegundaFecha,UltimaFecha from fechas1PE a left join fechas2PE b on a.CustomerNum = b.CustomerNum; #156,262

#ya se creó la tabla CRM_bases.firstDatesPE

#para crear la tabla CRM_bases.firstOrdersPE
drop table if exists orders1PE;
create temporary table orders1PE (PRIMARY KEY (CustomerNum))
select CustomerNum,OrderNum as Orden1,Date as FechaOrd1 from ordersTablePE where id = 1;

drop table if exists orders2PE;
create temporary table orders2PE (PRIMARY KEY (CustomerNum))
select CustomerNum,OrderNum as Orden2,Date as FechaOrd2 from ordersTablePE where id = 2;

drop table if exists ultimaOrdenPE;
create temporary table ultimaOrdenPE
select a.CustomerNum,Ordenes,ultimaFecha,ultimaOrden from CRM_bases.firstDatesPE a inner join (select CustomerNum,OrderNum as ultimaOrden,id from ordersTablePE) b on a.CustomerNum = b.CustomerNum and a.Ordenes = b.id;

drop table if exists CRM_bases.firstOrdersPE;
create table CRM_bases.firstOrdersPE (Primary Key (CustomerNum))
select a.CustomerNum,Ordenes,Orden1,FechaOrd1,Orden2,FechaOrd2,ultimaOrden,ultimaFecha from orders1PE a inner join ultimaOrdenPE b on a.CustomerNum = b.CustomerNum left join orders2PE c on a.CustomerNum = c.CustomerNum;

#termina PERU

#empieza VENEZUELA 

drop table if exists OrdersVE;
create temporary table OrdersVE
select CustomerNum,OrderNum,Date,sum(PaidPrice) as PaidPrice,sum(PCOnePFive) as PC15, sum(Rev) as Revenue from development_ve.A_Master where OrderAfterCan = 1 group by OrderNum order by CustomerNum asc, Date asc;  #309,915

drop table if exists ordersTableVE;
create temporary table ordersTableVE (id INT auto_increment, PRIMARY KEY(CustomerNum,id))
select CustomerNum,OrderNum,NULL AS id,Date,PaidPrice,PC15,Revenue from OrdersVE; #309,915

#para crear la tabla CRM_bases.firstDatesVE
drop table if exists datesTableVE;
create temporary table datesTableVE (id INT auto_increment, PRIMARY KEY(CustomerNum,id))
select CustomerNum,Date,count(distinct OrderNum) as ordenes,NULL AS id from OrdersVE group by CustomerNum,Date;

drop table if exists fechas1VE;
create temporary table fechas1VE (PRIMARY KEY(CustomerNum))
select CustomerNum,sum(ordenes) as Ordenes,ordenes as OrdenesEnFecha1, min(Date) as PrimeraFecha,max(Date) as UltimaFecha from datesTableVE group by CustomerNum;

drop table if exists fechas2VE;
create temporary table fechas2VE (PRIMARY KEY(CustomerNum))
select CustomerNum,Date as SegundaFecha from datesTableVE where id = 2;

drop table if exists CRM_bases.firstDatesVE;
create table CRM_bases.firstDatesVE (PRIMARY KEY (CustomerNum))
select a.CustomerNum,Ordenes,OrdenesEnFecha1,PrimeraFecha,SegundaFecha,UltimaFecha from fechas1VE a left join fechas2VE b on a.CustomerNum = b.CustomerNum; #156,262

#ya se creó la tabla CRM_bases.firstDatesVE

#para crear la tabla CRM_bases.firstOrdersVE
drop table if exists orders1VE;
create temporary table orders1VE (PRIMARY KEY (CustomerNum))
select CustomerNum,OrderNum as Orden1,Date as FechaOrd1 from ordersTableVE where id = 1;

drop table if exists orders2VE;
create temporary table orders2VE (PRIMARY KEY (CustomerNum))
select CustomerNum,OrderNum as Orden2,Date as FechaOrd2 from ordersTableVE where id = 2;

drop table if exists ultimaOrdenVE;
create temporary table ultimaOrdenVE
select a.CustomerNum,Ordenes,ultimaFecha,ultimaOrden from CRM_bases.firstDatesVE a inner join (select CustomerNum,OrderNum as ultimaOrden,id from ordersTableVE) b on a.CustomerNum = b.CustomerNum and a.Ordenes = b.id;

drop table if exists CRM_bases.firstOrdersVE;
create table CRM_bases.firstOrdersVE (Primary Key (CustomerNum))
select a.CustomerNum,Ordenes,Orden1,FechaOrd1,Orden2,FechaOrd2,ultimaOrden,ultimaFecha from orders1VE a inner join ultimaOrdenVE b on a.CustomerNum = b.CustomerNum left join orders2VE c on a.CustomerNum = c.CustomerNum;

#termina VENEZUELA

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`roberto.lopez`@`%`*/ /*!50003 PROCEDURE `Loyalty`()
BEGIN

drop table if exists preKPIs;
create temporary table preKPIs (PRIMARY KEY (CustomerNum))
select CustomerNum,count(distinct OrderNum) as ordenes, count(*) as Items, sum(PaidPrice) as PaidPrice, sum(Rev) as Revenue, sum(PCOnePFive) as PC1_5 
from development_mx.A_Master where Date >= '2013-08-07' and OrderAfterCan = 1 group by CustomerNum;

drop table if exists preKPIsCoupons;
create temporary table preKPIsCoupons
select CustomerNum,OrderNum,CouponCode,sum(CouponValue) as CuponValue,sum(PCOnePFive) as PC1_5wVoucher, sum(Rev) as RevwVoucher 
from development_mx.A_Master where Date >= '2013-08-07' and (CouponCode like 'REC%' or CouponCode like 'CAS%') and OrderAfterCan = 1 group by OrderNum;

drop table if exists preKPICouponsPERCUSTOMER;
create temporary table preKPICouponsPERCUSTOMER
select CustomerNum,sum(if(CouponCode like 'REC%',1,0)) AS OrdenesREC,
sum(if(CouponCode like 'CAS%',1,0)) AS OrdenesCAS,sum(if(CouponCode like 'REC%',CuponValue,0)) as CouponValueREC, 
sum(if(CouponCode like 'CAS%',CuponValue,0)) as CouponValueCAS, sum(if(CouponCode like 'REC%',PC1_5wVoucher,0)) as PC1_5REC, 
sum(if(CouponCode like 'CAS%',PC1_5wVoucher,0)) as PC1_5CAS,sum(if(CouponCode like 'REC%',RevwVoucher,0)) as RevREC, 
sum(if(CouponCode like 'CAS%',RevwVoucher,0)) as RevCAS from preKPIsCoupons group by CustomerNum;

drop table if exists perCustomer;
create temporary table perCustomer
select a.CustomerNum,Grupo,ordenes,Items,PaidPrice,Revenue,PC1_5,OrdenesREC,OrdenesCAS,CouponValueREC,CouponValueCAS,PC1_5REC,PC1_5CAS,RevREC,RevCAS 
from preKPIs a left join CRM_bases.Loyalty_people b on a.CustomerNum = b.custid left join preKPICouponsPERCUSTOMER c on a.CustomerNum = c.CustomerNum;

#select * from perCustomer;
update perCustomer set Grupo = 'Other customers' where Grupo is null;
update perCustomer set OrdenesREC = 0 where OrdenesREC is null;
update perCustomer set OrdenesCAS = 0 where OrdenesCAS is null;
update perCustomer set CouponValueREC = 0 where CouponValueREC is null;
update perCustomer set CouponValueCAS = 0 where CouponValueCAS is null;
update perCustomer set PC1_5REC = 0 where PC1_5REC is null;
update perCustomer set PC1_5CAS = 0 where PC1_5CAS is null;
update perCustomer set RevREC = 0 where RevREC is null;
update perCustomer set RevCAS = 0 where RevCAS is null;

drop table if exists CRM_bases.Loyalty_KPIs;
create table CRM_bases.Loyalty_KPIs
select Grupo,count(distinct CustomerNum) as NumCustomers,0 as UpliftCustsvsControl,
sum(ordenes) as NumTrans, 0 as UpliftTransvsControl,sum(Items) as NumItems, 0 as UpliftItemsvsControl,
sum(Revenue) as NetRevenue,sum(Revenue)/count(distinct CustomerNum) as RevenuePerCust,
0 as UpliftRevvsControl,sum(OrdenesREC) as NumVouchersRedeemedREC, sum(OrdenesCAS) as NumVouchersRedeemedCAS,
sum(CouponValueREC) as VoucherValueRedeemedCostREC,sum(CouponValueCAS) as VoucherValueRedeemedCostCAS,sum(PC1_5) as PC1_5,
sum(PC1_5)/sum(Revenue) as PC1_5pct 
FROM perCustomer group by Grupo order by FIELD(Grupo,'cashback','surprise','c_cashback','c_surprise','Other customers');

alter table CRM_bases.Loyalty_KPIs modify NetRevenue decimal(15,2);
alter table CRM_bases.Loyalty_KPIs modify RevenuePerCust decimal(10,2);
alter table CRM_bases.Loyalty_KPIs modify VoucherValueRedeemedCostREC decimal(10,2);
alter table CRM_bases.Loyalty_KPIs modify VoucherValueRedeemedCostCAS decimal(10,2);
alter table CRM_bases.Loyalty_KPIs modify PC1_5 decimal(10,2);
alter table CRM_bases.Loyalty_KPIs modify PC1_5pct decimal(10,2);

select @CustsCashback,@CustsControlCashback,@CustsSurprise,@CustsControlSurprise;
set @CustsSurprise = (select NumCustomers from CRM_bases.Loyalty_KPIs where Grupo = 'surprise');
set @CustsControlSurprise = (select NumCustomers from CRM_bases.Loyalty_KPIs where Grupo = 'c_surprise');
set @CustsCashback = (select NumCustomers from CRM_bases.Loyalty_KPIs where Grupo = 'cashback');
set @CustsControlCashback = (select NumCustomers from CRM_bases.Loyalty_KPIs where Grupo = 'c_cashback');

update CRM_bases.Loyalty_KPIs set UpliftCustsvsControl = 
if(Grupo = 'surprise',@CustsSurprise-9.2424*@CustsControlSurprise,if(Grupo = 'cashback',@CustsCashback-10.6*@CustsControlCashback,0));

select @PurchCashback,@PurchControlCashback,@PurchSurprise,@PurchControlSurprise;
set @PurchSurprise = (select NumTrans from CRM_bases.Loyalty_KPIs where Grupo = 'surprise');
set @PurchControlSurprise = (select NumTrans from CRM_bases.Loyalty_KPIs where Grupo = 'c_surprise');
set @PurchCashback = (select NumTrans from CRM_bases.Loyalty_KPIs where Grupo = 'cashback');
set @PurchControlCashback = (select NumTrans from CRM_bases.Loyalty_KPIs where Grupo = 'c_cashback');

update CRM_bases.Loyalty_KPIs set UpliftTransvsControl = 
if(Grupo = 'surprise',@PurchSurprise-6.646*@PurchControlSurprise,if(Grupo = 'cashback',@PurchCashback-9.069*@PurchControlCashback,0));

select @ItemsCashback,@ItemsControlCashback,@ItemsSurprise,@ItemsControlSurprise;
set @ItemsSurprise = (select NumItems from CRM_bases.Loyalty_KPIs where Grupo = 'surprise');
set @ItemsControlSurprise = (select NumItems from CRM_bases.Loyalty_KPIs where Grupo = 'c_surprise');
set @ItemsCashback = (select NumItems from CRM_bases.Loyalty_KPIs where Grupo = 'cashback');
set @ItemsControlCashback = (select NumItems from CRM_bases.Loyalty_KPIs where Grupo = 'c_cashback');

update CRM_bases.Loyalty_KPIs set UpliftItemsvsControl = 
if(Grupo = 'surprise',@ItemsSurprise-6.283*@ItemsControlSurprise,if(Grupo = 'cashback',@ItemsCashback-11.236*@ItemsControlCashback,0));

select @RevCashback,@RevControlCashback,@RevSurprise,@RevControlSurprise;
set @RevSurprise = (select NetRevenue from CRM_bases.Loyalty_KPIs where Grupo = 'surprise');
set @RevControlSurprise = (select NetRevenue from CRM_bases.Loyalty_KPIs where Grupo = 'c_surprise');
set @RevCashback = (select NetRevenue from CRM_bases.Loyalty_KPIs where Grupo = 'cashback');
set @RevControlCashback = (select NetRevenue from CRM_bases.Loyalty_KPIs where Grupo = 'c_cashback');

update CRM_bases.Loyalty_KPIs set UpliftRevvsControl = 
if(Grupo = 'surprise',@RevSurprise-9.188*@RevControlSurprise,if(Grupo = 'cashback',@RevCashback-11.164*@RevControlCashback,0));
#select * from CRM_bases.Loyalty_KPIs;

#Analisis de metricas diarias para el piloto de lealtad de Laura.
drop table if exists porPersona;
create temporary table porPersona (PRIMARY KEY (Date,CustomerNum))
select Date,CustomerNum,count(distinct OrderNum) as Transacciones,sum(Rev) as Revenue,count(*) as Items 
from development_mx.A_Master where Date >= '2013-05-27' and OrderAfterCan = 1 group by Date,CustomerNum; #243,511
#select * from porPersona limit 200;

drop table if exists CRM_bases.Loyalty_dailyResults;
create table CRM_bases.Loyalty_dailyResults
select Date,Grupo,count(distinct CustomerNum) as Clientes, sum(Transacciones) as Transacciones, 
sum(Revenue) as Revenue, sum(Items) as Items,WeekOfYear(Date) as Week,year(Date) as año 
from porPersona t left join CRM_bases.Loyalty_people c on t.CustomerNum = c.custid group by Date, Grupo order by Date; 
#1529. no tardó mucho.

update CRM_bases.Loyalty_dailyResults set año = año + 1 where Week = 1 and month(Date) = 12;
#esto porque quiero poner 2013-52, 2014-1, 2014-2.  los que compraban en el 31 dic 2013 salian con week = 1 y year 2013 (2013-1). ahora saldrian con 2014-1.
ALTER TABLE CRM_bases.Loyalty_dailyResults ADD YearWeek VARCHAR(10);
update CRM_bases.Loyalty_dailyResults set YearWeek = CONCAT(año,'-',Week); #se pone 2013-22, 2013-23, ..., 2013-52, 2014-1,2014-2

ALTER TABLE CRM_bases.Loyalty_dailyResults DROP Week;
ALTER TABLE CRM_bases.Loyalty_dailyResults DROP año;
alter table CRM_bases.Loyalty_dailyResults modify Revenue decimal(15,2); 
#select * from CRM_bases.Loyalty_dailyResults;  #1529.

drop table if exists CRM_bases.Loyalty_weekpivot;
create table CRM_bases.Loyalty_weekpivot
select YearWeek,sum(if(Grupo = 'surprise',Revenue,0)) as Surprise, 
sum(if(Grupo = 'cashback',Revenue,0)) as Cashback,
sum(if(Grupo = 'c_surprise',Revenue,0)) as ControlSurprise,
sum(if(Grupo = 'c_cashback',Revenue,0)) as ControlCashback, 
sum(if(Grupo is null,Revenue,0)) as NULLs,sum(Revenue) as Total 
from CRM_bases.Loyalty_dailyResults GROUP BY YearWeek order by left(YearWeek,4) asc, right(YearWeek,2) asc;
#48 rows. corre muy rapido.
#select * from CRM_bases.Loyalty_weekpivot;

drop table if exists CRM_bases.Loyalty_pivotRevenuexDateGroup;
create table CRM_bases.Loyalty_pivotRevenuexDateGroup
select Date,sum(if(Grupo = 'surprise',Revenue,0)) as Surprise, 
sum(if(Grupo = 'cashback',Revenue,0)) as Cashback,
sum(if(Grupo = 'c_surprise',Revenue,0)) as ControlSurprise,
sum(if(Grupo = 'c_cashback',Revenue,0)) as ControlCashback, 
sum(if(Grupo is null,Revenue,0)) as NUlls,
sum(Revenue) as Total from CRM_bases.Loyalty_dailyResults GROUP BY Date; #333
#select * from CRM_bases.Loyalty_pivotRevenuexDateGroup;
select CURDATE();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`adrian.lopez`@`%`*/ /*!50003 PROCEDURE `MX_CF_Periodicity`()
BEGIN

DECLARE PurchasedDate, PurchasedSKU, NextDate, NextSKU VARCHAR(10);
DECLARE PurchasedCustomer, NextCustomer, TotalCount, i INT DEFAULT 0;

TRUNCATE TABLE CRM_bases.DaysBetweenPurchase;

DROP TABLE IF EXISTS CRM_bases.PeriodicityTable;

CREATE TEMPORARY TABLE CRM_bases.PeriodicityTable (id_periodicity INT AUTO_INCREMENT, PRIMARY KEY (id_periodicity))
SELECT NULL AS id_periodicity, CustomerNum, `date`, SKUSimple FROM development_mx.A_Master WHERE Cat1 = 'Mascotas' AND Cat2 = 'Perros' AND Cat3 = 'Comida' AND OrderAfterCan = 1 AND YEAR(`Date`) = YEAR(CURDATE());

SET TotalCount = (SELECT COUNT(*) FROM CRM_bases.PeriodicityTable);

WHILE @i <= @TotalCount DO
	
	SELECT CustomerNum, `date`, SKUSimple INTO @PurchasedCustomer, @PurchasedDate, @PuchasedSKU FROM CRM_bases.PeriodicityTable 	WHERE id_periodicity = i;

	IF @i = 1 THEN
		SELECT CustomerNum, `date`, SKUSimple INTO @NextCustomer, @NextDate, @NextSKU FROM CRM_bases.PeriodicityTable WHERE id_periodicity = i;
	ELSEIF @PurchasedCustomer = @NextCustomer THEN

		SET @bandera = 'PurchasedCustomer = NextCustomer';

		IF @PurchasedSKU = @NextSKU THEN
	
			SET @bandera = 'PurchasedCustomer = NextCustomer AND PurchasedSKU = NextSKU';

			IF @PurchasedDate <> @NextDate THEN

				SET @bandera = 'PurchasedCustomer = NextCustomer AND PurchasedSKU = NextSKU AND PurchasedDate <> NextDate';

				SELECT DATEDIFF(@NextDate, @PurchaseDate) AS Days FROM CRM_bases.PeriodicityTable;
				INSERT INTO CRM_bases.DaysBetweenPurchase(customer, days) VALUES (@PurchasedCustomer, @Days);
			ELSE
			SELECT CustomerNum, `date`, SKUSimple INTO @NewCustomer, @NewDate, @NewSKU FROM CRM_bases.PeriodicityTable WHERE id_periodicity = @i;
			END IF;
		ELSE
			SELECT CustomerNum, `date`, SKUSimple INTO @NewCustomer, @NewDate, @NewSKU FROM CRM_bases.PeriodicityTable WHERE id_periodicity = @i;
		END IF;
		SELECT CustomerNum, `date`, SKUSimple INTO @NewCustomer, @NewDate, @NewSKU FROM CRM_bases.PeriodicityTable WHERE id_periodicity = @i;
	END IF;

	SET @i = @i + 1;

END WHILE;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`roberto.lopez`@`%`*/ /*!50003 PROCEDURE `procRecurrentItems`()
BEGIN

#MEXICO

drop table if exists CustItemsMX;
create temporary table CustItemsMX (id INT auto_increment, Primary Key(CustomerNum,SKUConfig,id))
select CustomerNum,SKUConfig,SKUName,OriginalPrice,NULL AS id,Date as FechaCompra,OrderNum,'' as Comentario,0 as dias from development_mx.A_Master where OrderAfterCan = 1 and (Source_medium is null or Source_medium <> 'CorporateSales / CRM') order by CustomerNum asc, id asc,SKUName asc,Date asc;  #401,948

drop table if exists CustItemsMX2;
create temporary table CustItemsMX2 (id2 INT auto_increment, Primary Key (id2))
select null as id2, CustomerNum, SKUConfig,SKUName,OriginalPrice, id, FechaCompra, OrderNum, Comentario, dias from CustItemsMX;

alter table CustItemsMX2 modify Comentario varchar(20);

#select * from CustItemsMX limit 50;
#select * from CustItemsMX2 where CustomerNum in (134383,354907);

SELECT CustomerNum, SKUConfig, cast(FechaCompra as char)  INTO @Cust, @SKU, @fecha FROM CustItemsMX limit 1;
#select @Cust,@SKU,@fecha;
#debe ser 2,Camara Digital Samsung, 2012-05-21
#tal vez sea mejor poner el SKUConfig

set @cuenta :=1;
set @totalRenglones := (select count(*) from CustItemsMX);

#select @cuenta,@totalRenglones;

WHILE(@cuenta <= @totalRenglones) do
  update CustItemsMX2 set Comentario = if(id=1,'el primero',if(CustomerNum = @Cust and SKUConfig = @SKU and DATEDIFF(FechaCompra,@fecha)<7,'no contarlo','se cuenta')), dias = if(id=1,0,if(CustomerNum = @Cust and SKUConfig = @SKU, DATEDIFF(FechaCompra,@fecha),-1)) where id2 = @cuenta;

SELECT CustomerNum, SKUConfig, cast(FechaCompra as char) INTO @Cust, @SKU, @fecha FROM CustItemsMX2 where id2 = @cuenta;

set @cuenta = @cuenta + 1;

END WHILE;

#drop table if exists SKUtimes;
#create temporary table SKUtimes (PRIMARY KEY (SKUName))
#select SKUConfig,SKUName,count(distinct CustomerNum) as #ClientesDistintos, count(*) as VecesComprado from CustItemsMX2 group by SKUConfig;
#select * from CustItemsMX2 where CustomerNum in (38818,13663,182218) and SKUConfig in ('TE478EG55LMMLMX','TE478EG54LMNLMX');


drop table if exists CustSKU_masde1;
create temporary table CustSKU_masde1
select CustomerNum,SKUConfig,SKUName,OriginalPrice,count(*) as CuantasVeces,(sum(dias))/(count(*)-1) as diasPromedio from CustItemsMX2 where Comentario <> 'no contarlo' group by CustomerNum,SKUConfig having CuantasVeces > 1;

#SELECT * FROM CustSKU_masde1 where SKUConfig in ('TE478EG55LMMLMX','TE478EG54LMNLMX');

drop table if exists recurrentSKUsMX;
create temporary table recurrentSKUsMX
select 'MX' as Country,SKUConfig,SKUName,OriginalPrice,count(distinct CustomerNum) as ClientesMasde1Orden,avg(CuantasVeces) as OrdenesEnPromedio,avg(diasPromedio) as diasPromedio from CustSKU_masde1 group by SKUConfig order by ClientesMasde1Orden desc;

#drop table if exists CRM_bases.recurrentSKUs;
#create table CRM_bases.recurrentSKUs
#select #a.SKUName,ClientesDistintos,VecesComprado,ClientesMasde1Orden,OrdenesEnPromedio from SKUtimes a inner join SKU_masde1 b on a.SKUName = b.SKUName order by ClientesMasde1Orden desc;

#termina MEXICO

#COLOMBIA

drop table if exists CustItemsCO;
create temporary table CustItemsCO (id INT auto_increment, Primary Key(CustomerNum,SKUConfig,id))
select CustomerNum,SKUConfig,SKUName,OriginalPrice,NULL AS id,Date as FechaCompra,OrderNum,'' as Comentario, 0 as dias from development_co_project.A_Master where OrderAfterCan = 1 and (Source_medium is null or Source_medium <> 'CorporateSales / CRM') order by CustomerNum asc, id asc,SKUName asc,Date asc;  #477,725

drop table if exists CustItemsCO2;
create temporary table CustItemsCO2 (id2 INT auto_increment, Primary Key (id2))
select null as id2, CustomerNum, SKUConfig,SKUName,OriginalPrice,id, FechaCompra, OrderNum, Comentario, dias from CustItemsCO;

alter table CustItemsCO2 modify Comentario varchar(20);

SELECT CustomerNum, SKUConfig, cast(FechaCompra as char)  INTO @Cust, @SKU, @fecha FROM CustItemsCO limit 1;

set @cuenta :=1;
set @totalRenglones := (select count(*) from CustItemsCO);

WHILE(@cuenta <= @totalRenglones) do
  update CustItemsCO2 set Comentario = if(id=1,'el primero',if(CustomerNum = @Cust and SKUConfig = @SKU and DATEDIFF(FechaCompra,@fecha)<7,'no contarlo','se cuenta')), dias = if(id=1,0,if(CustomerNum = @Cust and SKUConfig = @SKU, DATEDIFF(FechaCompra,@fecha),-1)) where id2 = @cuenta;

SELECT CustomerNum, SKUConfig, cast(FechaCompra as char)  INTO @Cust, @SKU, @fecha FROM CustItemsCO2 where id2 = @cuenta;

set @cuenta = @cuenta + 1;

END WHILE;

drop table if exists CustSKU_masde1;
create temporary table CustSKU_masde1
select CustomerNum,SKUConfig,SKUName,OriginalPrice,count(*) as CuantasVeces,(sum(dias))/(count(*)-1) as diasPromedio from CustItemsCO2 where Comentario <> 'no contarlo' group by CustomerNum,SKUConfig having CuantasVeces > 1;

drop table if exists recurrentSKUsCO;
create temporary table recurrentSKUsCO
select 'CO' as Country,SKUConfig,SKUName,OriginalPrice,count(distinct CustomerNum) as ClientesMasde1Orden,avg(CuantasVeces) as OrdenesEnPromedio,avg(diasPromedio) as diasPromedio from CustSKU_masde1 group by SKUConfig order by ClientesMasde1Orden desc;


#termina COLOMBIA

#PERU

drop table if exists CustItemsPE;
create temporary table CustItemsPE (id INT auto_increment, Primary Key(CustomerNum,SKUConfig,id))
select CustomerNum,SKUConfig,SKUName,OriginalPrice,NULL AS id,Date as FechaCompra,OrderNum,'' as Comentario, 0 as dias from development_pe.A_Master where OrderAfterCan = 1 and (Source_medium is null or Source_medium <> 'CorporateSales / CRM') order by CustomerNum asc, id asc,SKUName asc,Date asc;  #477,725

drop table if exists CustItemsPE2;
create temporary table CustItemsPE2 (id2 INT auto_increment, Primary Key (id2))
select null as id2, CustomerNum, SKUConfig,SKUName,OriginalPrice,id, FechaCompra, OrderNum, Comentario, dias from CustItemsPE;

alter table CustItemsPE2 modify Comentario varchar(20);

SELECT CustomerNum, SKUConfig, cast(FechaCompra as char)  INTO @Cust, @SKU, @fecha FROM CustItemsPE limit 1;

set @cuenta :=1;
set @totalRenglones := (select count(*) from CustItemsPE);

WHILE(@cuenta <= @totalRenglones) do
  update CustItemsPE2 set Comentario = if(id=1,'el primero',if(CustomerNum = @Cust and SKUConfig = @SKU and DATEDIFF(FechaCompra,@fecha)<7,'no contarlo','se cuenta')), dias = if(id=1,0,if(CustomerNum = @Cust and SKUConfig = @SKU, DATEDIFF(FechaCompra,@fecha),-1)) where id2 = @cuenta;

SELECT CustomerNum, SKUConfig, cast(FechaCompra as char)  INTO @Cust, @SKU, @fecha FROM CustItemsPE2 where id2 = @cuenta;

set @cuenta = @cuenta + 1;

END WHILE;

drop table if exists CustSKU_masde1;
create temporary table CustSKU_masde1
select CustomerNum,SKUConfig,SKUName,OriginalPrice,count(*) as CuantasVeces,(sum(dias))/(count(*)-1) as diasPromedio from CustItemsPE2 where Comentario <> 'no contarlo' group by CustomerNum,SKUConfig having CuantasVeces > 1;

drop table if exists recurrentSKUsPE;
create temporary table recurrentSKUsPE
select 'PE' as Country,SKUConfig,SKUName,OriginalPrice,count(distinct CustomerNum) as ClientesMasde1Orden,avg(CuantasVeces) as OrdenesEnPromedio,avg(diasPromedio) as diasPromedio from CustSKU_masde1 group by SKUConfig order by ClientesMasde1Orden desc;


#termina PERU

#VENEZUELA

drop table if exists CustItemsVE;
create temporary table CustItemsVE (id INT auto_increment, Primary Key(CustomerNum,SKUConfig,id))
select CustomerNum,SKUConfig,SKUName,OriginalPrice,NULL AS id,Date as FechaCompra,OrderNum,'' as Comentario, 0 as dias from development_ve.A_Master where OrderAfterCan = 1 and (Source_medium is null or Source_medium <> 'CorporateSales / CRM') order by CustomerNum asc, id asc,SKUName asc,Date asc;  #477,725

drop table if exists CustItemsVE2;
create temporary table CustItemsVE2 (id2 INT auto_increment, Primary Key (id2))
select null as id2, CustomerNum, SKUConfig,SKUName,OriginalPrice,id, FechaCompra, OrderNum, Comentario,dias from CustItemsVE;

alter table CustItemsVE2 modify Comentario varchar(20);

SELECT CustomerNum, SKUConfig, cast(FechaCompra as char)  INTO @Cust, @SKU, @fecha FROM CustItemsVE limit 1;

set @cuenta :=1;
set @totalRenglones := (select count(*) from CustItemsVE);

WHILE(@cuenta <= @totalRenglones) do
  update CustItemsVE2 set Comentario = if(id=1,'el primero',if(CustomerNum = @Cust and SKUConfig = @SKU and DATEDIFF(FechaCompra,@fecha)<7,'no contarlo','se cuenta')), dias = if(id=1,0,if(CustomerNum = @Cust and SKUConfig = @SKU, DATEDIFF(FechaCompra,@fecha),-1)) where id2 = @cuenta;

SELECT CustomerNum, SKUConfig, cast(FechaCompra as char)  INTO @Cust, @SKU, @fecha FROM CustItemsVE2 where id2 = @cuenta;

set @cuenta = @cuenta + 1;

END WHILE;

drop table if exists CustSKU_masde1;
create temporary table CustSKU_masde1
select CustomerNum,SKUConfig,SKUName,OriginalPrice,count(*) as CuantasVeces,(sum(dias))/(count(*)-1) as diasPromedio from CustItemsVE2 where Comentario <> 'no contarlo' group by CustomerNum,SKUConfig having CuantasVeces > 1;

drop table if exists recurrentSKUsVE;
create temporary table recurrentSKUsVE
select 'VE' as Country,SKUConfig,SKUName,OriginalPrice,count(distinct CustomerNum) as ClientesMasde1Orden,avg(CuantasVeces) as OrdenesEnPromedio,avg(diasPromedio) as diasPromedio from CustSKU_masde1 group by SKUConfig order by ClientesMasde1Orden desc;

#termina VENEZUELA

#UNION ALL
drop table if exists CRM_bases.recurrentSKU;
create table CRM_bases.recurrentSKU
(select * from recurrentSKUsMX) UNION ALL (select * from recurrentSKUsCO) UNION ALL (select * from recurrentSKUsPE) UNION ALL (select * from recurrentSKUsVE);

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`roberto.lopez`@`%`*/ /*!50003 PROCEDURE `prueba2`()
    READS SQL DATA
BEGIN

drop table if exists CustItemsMX;
create table CustItemsMX (id INT auto_increment, Primary Key(CustomerNum,SKUName,id))
select CustomerNum,SKUName,NULL AS id,Date as FechaCompra,OrderNum,CouponCode,'' as Comentario, 0 as dias from development_mx.A_Master_Backup where OrderAfterCan = 1 order by CustomerNum asc, id asc,SKUName asc,Date asc;  #477,725

drop table if exists CustItemsMX2;
create table CustItemsMX2 (id2 INT auto_increment, Primary Key (id2))
select null as id2, CustomerNum, SKUName, id, FechaCompra, OrderNum,CouponCode,Comentario,dias from CustItemsMX;

alter table CustItemsMX2 modify Comentario varchar(20);

#select * from CustItemsMX;
#select * from CustItemsMX2;

SELECT CustomerNum, SKUName, cast(FechaCompra as char)  INTO @Cust, @SKU, @fecha FROM CustItemsMX limit 1;
#select @Cust,@SKU,@fecha;
#debe ser 2,Camara Digital Samsung, 2012-05-21
#tal vez sea mejor poner el SKUConfig

set @cuenta :=1;
set @totalRenglones := (select count(*) from CustItemsMX);

#select @cuenta,@totalRenglones;

WHILE(@cuenta <= @totalRenglones) do
  update CustItemsMX2 set Comentario = if(id=1,'el primero',if(CustomerNum = @Cust and SKUName = @SKU and DATEDIFF(FechaCompra,@fecha)<7,'no contarlo','se cuenta')), dias = if(id=1,0,if(CustomerNum = @Cust and SKUName = @SKU,DATEDIFF(FechaCompra,@fecha),0)) where id2 = @cuenta;

SELECT CustomerNum, SKUName, cast(FechaCompra as char)  INTO @Cust, @SKU, @fecha FROM CustItemsMX2 where id2 = @cuenta;

set @cuenta = @cuenta + 1;

END WHILE;

drop table if exists SKUtimes;
create temporary table SKUtimes (PRIMARY KEY (SKUName))
select SKUName,count(distinct CustomerNum) as ClientesDistintos, count(*) as VecesComprado from CustItemsMX2 group by SKUName;

drop table if exists CustSKU_masde1;
create temporary table CustSKU_masde1
select CustomerNum,SKUName,max(id) as CuantasVeces from CustItemsMX2 where Comentario <> 'no contarlo' group by CustomerNum,SKUName having CuantasVeces > 1;

drop table if exists SKU_masde1;
create temporary table SKU_masde1 (PRIMARY KEY(SKUName))
select SKUName,count(distinct CustomerNum) as ClientesMasde1Orden,avg(CuantasVeces) as OrdenesEnPromedio from CustSKU_masde1 group by SKUName;

drop table if exists CRM_bases.prueba;
create table CRM_bases.prueba
select a.SKUName,ClientesDistintos,VecesComprado,ClientesMasde1Orden,OrdenesEnPromedio from SKUtimes a inner join SKU_masde1 b on a.SKUName = b.SKUName order by ClientesMasde1Orden desc;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`borja.fernandez`@`%`*/ /*!50003 PROCEDURE `Report_Campaign_Factory`()
BEGIN
#################Resultados############################

select * from CRM_bases.cf_report limit 10;

select 	week,
				Country,
				campaign_nick_name,
				sum(NumSent) as NumSent,
				sum(TotalBounces) as TotalBounces,
				sum(NumOpens) as NumOpens,
				sum(NumClicks) as NumClicks,
				sum(visits) as visits,
				sum(carts) as carts,
				sum(gross_transactions) as gross_transactions,
				sum(gross_revenue) as gross_revenue,
				sum(net_transactions) as net_transactions,
				sum(net_revenue) as net_revenue,
				sum(PC1) as PC1,
				sum(PC1P5) as PC1P5,
				sum(PC2) as PC2
	from CRM_bases.cf_report
		group by campaign_nick_name, week, country
			order by week,country,campaign_nick_name;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`borja.fernandez`@`%`*/ /*!50003 PROCEDURE `Report_Clicks_por_Categoria`()
BEGIN

##Resultados_Clicks_por_Categoria_v1

#################RESULTADO RESUMEN CLICKS POR PERSONA POR CATEGORIA#################

##Result 1
select * from CRM_bases.MX_ClicksResumenPorMailPorCategoria 
	order by Total_Clicks DESC
		limit 100;

##Result 2
select * from CRM_bases.CO_ClicksResumenPorMailPorCategoria 
	order by Total_Clicks DESC
		limit 100;

##Result 3
select * from CRM_bases.PE_ClicksResumenPorMailPorCategoria 
	order by Total_Clicks DESC
		limit 100;

##Result 4
select * from CRM_bases.VE_ClicksResumenPorMailPorCategoria 
	order by Total_Clicks DESC
		limit 100;

#################RESULTADO RESUMEN CLICKS POR PERSONA POR CATEGORIA PORCENTUAL#################

##Result 5
select * from CRM_bases.MX_ClicksResumenPorMailPorCategoriaPCT 
	order by Total_Clicks DESC
		limit 100;

##Result 6
select * from CRM_bases.CO_ClicksResumenPorMailPorCategoriaPCT 
	order by Total_Clicks DESC
		limit 100;

##Result 7
select * from CRM_bases.PE_ClicksResumenPorMailPorCategoriaPCT 
	order by Total_Clicks DESC
		limit 100;

##Result 8
select * from CRM_bases.VE_ClicksResumenPorMailPorCategoriaPCT 
	order by Total_Clicks DESC
		limit 100;

#################Categorias mas clicadas#################

##Result 9
select 	categoria,
				sum(NumClicks) as Total_Clicks_por_Categoria
	from CRM_bases.MX_ClicksPorEmailPorCategoria
		group by categoria
			order by sum(NumClicks) desc;

##Result 10
select 	categoria,
				sum(NumClicks) as Total_Clicks_por_Categoria
	from CRM_bases.CO_ClicksPorEmailPorCategoria
		group by categoria
			order by sum(NumClicks) desc;

##Result 11
select 	categoria,
				sum(NumClicks) as Total_Clicks_por_Categoria
	from CRM_bases.PE_ClicksPorEmailPorCategoria
		group by categoria
			order by sum(NumClicks) desc;

##Result 12
select 	categoria,
				sum(NumClicks) as Total_Clicks_por_Categoria
	from CRM_bases.VE_ClicksPorEmailPorCategoria
		group by categoria
			order by sum(NumClicks) desc;

#################Categorias mas clicadas por non-customers#################

##Result 13
select 	categoria,
				sum(NumClicks) as Total_Clicks_por_Categoria
	from CRM_bases.MX_ClicksPorEmailPorCategoria
		where TYPE_CUSTOMER='non customer'
			group by categoria
				order by sum(NumClicks) desc;

##Result 14
select 	categoria,
				sum(NumClicks) as Total_Clicks_por_Categoria
	from CRM_bases.CO_ClicksPorEmailPorCategoria
		where TYPE_CUSTOMER='non customer'
			group by categoria
				order by sum(NumClicks) desc;

##Result 15
select 	categoria,
				sum(NumClicks) as Total_Clicks_por_Categoria
	from CRM_bases.PE_ClicksPorEmailPorCategoria
		where TYPE_CUSTOMER='non customer'
			group by categoria
				order by sum(NumClicks) desc;

##Result 16
select 	categoria,
				sum(NumClicks) as Total_Clicks_por_Categoria
	from CRM_bases.VE_ClicksPorEmailPorCategoria
		where TYPE_CUSTOMER='non customer'
			group by categoria
				order by sum(NumClicks) desc;

#################MEXICO#################

##Result 17
select count(*) from CRM_bases.MX_ClicksResumenPorMailPorCategoriaPCT 
	where TYPE_CUSTOMER='non customer';

##Result 18
select count(*) from dev_marketing.customers_rev_mx 
	where TYPE_CUSTOMER='non customer';

##Result 19
select count(*) from CRM_bases.MX_ClicksResumenPorMailPorCategoriaPCT 
	where TYPE_CUSTOMER='customer';

##Result 20
select count(*) from dev_marketing.customers_rev_mx 
	where TYPE_CUSTOMER='customer';

#################COLOMBIA#################

##Result 21
select count(*) from CRM_bases.CO_ClicksResumenPorMailPorCategoriaPCT 
	where TYPE_CUSTOMER='non customer';

##Result 22
select count(*) from dev_marketing.customers_rev_co
	where TYPE_CUSTOMER='non customer';

##Result 23
select count(*) from CRM_bases.CO_ClicksResumenPorMailPorCategoriaPCT 
	where TYPE_CUSTOMER='customer';

##Result 24
select count(*) from dev_marketing.customers_rev_co 
	where TYPE_CUSTOMER='customer';

#################PERU############################

##Result 25
select count(*) from CRM_bases.PE_ClicksResumenPorMailPorCategoriaPCT 
	where TYPE_CUSTOMER='non customer';

##Result 26
select count(*) from dev_marketing.customers_rev_pe
	where TYPE_CUSTOMER='non customer';

##Result 27
select count(*) from CRM_bases.PE_ClicksResumenPorMailPorCategoriaPCT 
	where TYPE_CUSTOMER='customer';

##Result 28
select count(*) from dev_marketing.customers_rev_pe
	where TYPE_CUSTOMER='customer';

##################VENEZUELA######################

##Result 29
select count(*) from CRM_bases.VE_ClicksResumenPorMailPorCategoriaPCT 
	where TYPE_CUSTOMER='non customer';

##Result 30
select count(*) from dev_marketing.customers_rev_ve 
	where TYPE_CUSTOMER='non customer';

##Result 31
select count(*) from CRM_bases.VE_ClicksResumenPorMailPorCategoriaPCT 
	where TYPE_CUSTOMER='customer';

##Result 32
select count(*) from dev_marketing.customers_rev_ve 
	where TYPE_CUSTOMER='customer';


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`borja.fernandez`@`%`*/ /*!50003 PROCEDURE `Report_Interfaces`()
BEGIN

#################Reporte Interfaces##################

select * from CRM_bases.Regional_InterfacesUMSsummary;

select 	country,
				production.week_iso(dia) as WeekYear,
				Alias,
				NumSent,
				Total_Bounces,
				NumOpens,
				NumClicks
	from CRM_bases.Regional_InterfacesUMSsummary
	group by Country, production.week_iso(dia), alias;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`borja.fernandez`@`%`*/ /*!50003 PROCEDURE `Report_Vaciados`()
BEGIN

##########Resultados Vaciados################

select * from CRM_bases.vaciados
	where campaign_nick_name='Other'
		and year(dia)=2014
			and campaign not like '%Customer-Satisfaction-Survey%'
				order by yrmonth
					limit 1000;

###############Resultados por Semana###############

select * from CRM_bases.vaciados limit 10;

select 	week as semana,
				country as pais,
				sum(NumSent) as SumSent,
				sum(NumOpens)/sum(NumSent) as OpenRate,
				sum(NumClicks)/sum(NumOpens) as Click2OpenRate,
				sum(visits) as SumVisit,
				sum(carts) as SumCarts,
				sum(gross_transactions) as GrossTransactions,
				sum(gross_revenue) as GrossRevenue,
				sum(net_transactions) as NetTransactions,
				sum(net_revenue) as NetRevenue,
				sum(PC1) as TotalPC1,
				sum(`PC1.5`) as TotalPC1P5,
				sum(`PC2`) as PC2
	from CRM_bases.vaciados
		group by week, country
			order by week,country;

##############Resultados por Mes#######################

select * from CRM_bases.vaciados limit 10;

select 	yrmonth as mes,
				country as pais,
				sum(NumSent) as SumSent,
				sum(NumOpens)/sum(NumSent) as OpenRate,
				sum(NumClicks)/sum(NumOpens) as Click2OpenRate,
				sum(visits) as SumVisit,
				sum(carts) as SumCarts,
				sum(gross_transactions) as GrossTransactions,
				sum(gross_revenue) as GrossRevenue,
				sum(net_transactions) as NetTransactions,
				sum(net_revenue) as NetRevenue,
				sum(PC1) as TotalPC1,
				sum(`PC1.5`) as TotalPC1P5,
				sum(`PC2`) as PC2
	from CRM_bases.vaciados
		group by yrmonth, country
			order by yrmonth,country;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`roberto.lopez`@`%`*/ /*!50003 PROCEDURE `RepurchasebyProduct`()
BEGIN

drop table if exists itemsPurchSeptAprilMX;
create temporary table itemsPurchSeptAprilMX(id INT auto_increment, Primary Key(CustomerNum,SKUConfig,id))
select 'MX' as Country,NULL as id,CustomerNum,if(NewReturning = 'NEW','Nuevo','Viejo') as nuevoCliente,
'' as Mes,Date,-1 as DateDif,0 as a90Dias,OrderNum,Cat1,SKUConfig,SKUName,if(CouponCode like '%CAC%','CAC','NoCAC') AS Cac,
sum(Price) as Price,sum(PaidPrice) as PaidPrice,sum(Price)/16.35 as PriceEuros,sum(PaidPrice)/16.35 as PaidPrEuros,
'Ninguno' as Comentario
from development_mx.A_Master where Date between '2013-09-01' and '2014-04-30' and OrderAfterCan = 1
group by CustomerNum,Date,SKUConfig 
order by CustomerNum,SKUConfig,Date,nuevoCliente; #294,792
#select * from itemsPurchSeptAprilMX;


alter table itemsPurchSeptAprilMX modify Mes varchar(15);
update itemsPurchSeptAprilMX set Mes = CASE when month(Date) = 9 then 'September' when month(Date) = 10 then 'October'
when month(Date) = 11 then 'November' when month(Date) = 12 then 'December' when month(Date) = 1 then 'January' 
when month(Date) = 2 then 'February' when month(Date) = 3 then 'March' else 'April' END;


drop table if exists CRM_bases.RRitemsSeptAprilMX;
create table CRM_bases.RRitemsSeptAprilMX(id2 INT auto_increment, Primary Key(id2))
select * from itemsPurchSeptAprilMX;  #200,984

#select * from CRM_bases.RRitemsSeptAprilMX;

set @totalRenglones := (select count(*) from CRM_bases.RRitemsSeptAprilMX); #debe ser 200,984
SELECT CustomerNum, id, SKUConfig, cast(Date as char)  INTO @Cust,@id,@sku,@fecha FROM CRM_bases.RRitemsSeptAprilMX where id2 = @totalRenglones;
set @cuenta := @totalRenglones;
#select @cuenta,@totalRenglones,@Cust,@fecha;


update CRM_bases.RRitemsSeptAprilMX set Comentario = if(id = 1 or Mes in('February','March','April'),'Borrar',Comentario) 
where id2 = @totalRenglones;

WHILE(@cuenta >1) do
  update CRM_bases.RRitemsSeptAprilMX set DateDif = if(CustomerNum = @Cust and SKUConfig = @sku,DATEDIFF(@fecha,Date),DateDif),
Comentario = if(id = @id or Mes in('February','March','April'),'Borrar',Comentario)  
where id2 = @cuenta-1;

SELECT CustomerNum, id, SKUConfig, cast(Date as char)  INTO @Cust,@id,@sku,@fecha FROM CRM_bases.RRitemsSeptAprilMX where id2 = @cuenta-1;


set @cuenta = @cuenta - 1;
END WHILE;

update CRM_bases.RRitemsSeptAprilMX set a90Dias = 1 
where Date <= '2014-01-31' and DateDif > 0 and DateDif <= 90;

set @Borrados:=(select count(*) from CRM_bases.RRitemsSeptAprilMX where Comentario = 'Borrar');
select CONCAT(@totalRenglones,' renglones en la tabla Inicial.') as RenglonesAlInicio,
CONCAT('Se borraron ',@Borrados,' renglones') as Borrados;

delete from CRM_bases.RRitemsSeptAprilMX where Comentario = 'Borrar';
select CURDATE();

select * from CRM_bases.RRitemsSeptAprilMX; 

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`roberto.lopez`@`%`*/ /*!50003 PROCEDURE `RepurchaseSMS`()
BEGIN

#MEXICO
drop table if exists comprasSeptaAbrilMX_v2;
create temporary table comprasSeptaAbrilMX_v2(id INT auto_increment, Primary Key(CustomerNum,Date,id))
select 'MX' as Country,NULL as id,CustomerNum,Date,'' as Mes,OrderNum,if(CouponCode like '%CAC%','CAC','NoCAC') AS Cac,
if(NewReturning = 'NEW','Nuevo','Viejo') as nuevoCliente,
0000000000 as sms
from development_mx.A_Master where Date between '2013-09-01' and '2014-04-30' and OrderAfterCan = 1 GROUP BY OrderNum
order by CustomerNum,Date,nuevoCliente; #213,604

select * from comprasSeptaAbrilMX_v2 limit 5000;

delete from comprasSeptaAbrilMX_v2 where id > 1;
alter table comprasSeptaAbrilMX_v2 drop column id;
alter table comprasSeptaAbrilMX_v2 modify Mes varchar(15);
update comprasSeptaAbrilMX_v2 set Mes = CASE when month(Date) = 9 then 'September' when month(Date) = 10 then 'October'
when month(Date) = 11 then 'November' when month(Date) = 12 then 'December' when month(Date) = 1 then 'January' 
when month(Date) = 2 then 'February' when month(Date) = 3 then 'March' else 'April' END;


drop table if exists smsMX;
create temporary table smsMX(PRIMARY KEY (custid), INDEX(custid))
select custid,sms from dev_marketing.customers_rev_mx where TYPE_CUSTOMER = 'customer' and sms is not null and custid is not null;

update comprasSeptaAbrilMX_v2 a inner join smsMX b on a.CustomerNum = b.custid set a.sms = b.sms;

delete from comprasSeptaAbrilMX_v2 where sms = 0 or length(sms) < 8;

drop table if exists CRM_bases.comprasSeptAbrilMX_sms;
create table CRM_bases.comprasSeptAbrilMX_sms(id INT auto_increment, Primary Key(id))
select *,-1 as DateDif,0 as a90Dias from comprasSeptaAbrilMX_v2 order by sms,Date;  #200,984


#select * from CRM_bases.comprasSeptAbrilMX_sms limit 500;

set @totalRenglones := (select count(*) from CRM_bases.comprasSeptAbrilMX_sms); #debe ser 200,984
SELECT CustomerNum,sms, cast(Date as char)  INTO @Cust,@sms, @fecha FROM CRM_bases.comprasSeptAbrilMX_sms where id = @totalRenglones;
set @cuenta := @totalRenglones;
#select @cuenta,@totalRenglones,@Cust,@fecha;

WHILE(@cuenta >1) do
  update CRM_bases.comprasSeptAbrilMX_sms set DateDif = if(sms = @sms and CustomerNum <> @Cust,DATEDIFF(@fecha,Date),DateDif) 
where id = @cuenta-1;

SELECT CustomerNum,sms, cast(Date as char)  INTO @Cust,@sms, @fecha FROM CRM_bases.comprasSeptAbrilMX_sms where id = @cuenta-1;

set @cuenta = @cuenta - 1;
END WHILE;

update CRM_bases.comprasSeptAbrilMX_sms set a90Dias = 1 
where Date <= '2014-01-31' and DateDif > 0 and DateDif <= 90;

#termina MEXICO

#COLOMBIA
drop table if exists comprasSeptaAbrilCO_v2;
create temporary table comprasSeptaAbrilCO_v2(id INT auto_increment, Primary Key(CustomerNum,Date,id))
select 'CO' as Country,NULL as id,CustomerNum,Date,'' as Mes,OrderNum,if(CouponCode like '%CAC%','CAC','NoCAC') AS Cac,
if(NewReturning = 'NEW','Nuevo','Viejo') as nuevoCliente,
0000000000 as sms
from development_co_project.A_Master where Date between '2013-09-01' and '2014-04-30' and OrderAfterCan = 1 GROUP BY OrderNum
order by CustomerNum,Date,nuevoCliente; #213,604

#select * from comprasSeptaAbrilCO_v2 limit 5000;

delete from comprasSeptaAbrilCO_v2 where id > 1;
alter table comprasSeptaAbrilCO_v2 drop column id;
alter table comprasSeptaAbrilCO_v2 modify Mes varchar(15);
update comprasSeptaAbrilCO_v2 set Mes = CASE when month(Date) = 9 then 'September' when month(Date) = 10 then 'October'
when month(Date) = 11 then 'November' when month(Date) = 12 then 'December' when month(Date) = 1 then 'January' 
when month(Date) = 2 then 'February' when month(Date) = 3 then 'March' else 'April' END;


drop table if exists smsCO;
create temporary table smsCO(PRIMARY KEY (custid), INDEX(custid))
select custid,sms from dev_marketing.customers_rev_co where TYPE_CUSTOMER = 'customer' and sms is not null and custid is not null;

update comprasSeptaAbrilCO_v2 a inner join smsCO b on a.CustomerNum = b.custid set a.sms = b.sms;

delete from comprasSeptaAbrilCO_v2 where sms = 0 or length(sms) < 8;

drop table if exists CRM_bases.comprasSeptAbrilCO_sms;
create table CRM_bases.comprasSeptAbrilCO_sms(id INT auto_increment, Primary Key(id))
select *,-1 as DateDif,0 as a90Dias from comprasSeptaAbrilCO_v2 order by sms,Date;  #200,984


#select * from CRM_bases.comprasSeptAbrilMX_sms limit 500;

set @totalRenglones := (select count(*) from CRM_bases.comprasSeptAbrilCO_sms); #debe ser 200,984
SELECT CustomerNum,sms, cast(Date as char)  INTO @Cust,@sms, @fecha FROM CRM_bases.comprasSeptAbrilCO_sms where id = @totalRenglones;
set @cuenta := @totalRenglones;
#select @cuenta,@totalRenglones,@Cust,@fecha;

WHILE(@cuenta >1) do
  update CRM_bases.comprasSeptAbrilCO_sms set DateDif = if(sms = @sms and CustomerNum <> @Cust,DATEDIFF(@fecha,Date),DateDif) 
where id = @cuenta-1;

SELECT CustomerNum,sms, cast(Date as char)  INTO @Cust,@sms, @fecha FROM CRM_bases.comprasSeptAbrilCO_sms where id = @cuenta-1;

set @cuenta = @cuenta - 1;
END WHILE;

update CRM_bases.comprasSeptAbrilCO_sms set a90Dias = 1 
where Date <= '2014-01-31' and DateDif > 0 and DateDif <= 90;

#termina COLOMBIA


#PERU
drop table if exists comprasSeptaAbrilPE_v2;
create temporary table comprasSeptaAbrilPE_v2(id INT auto_increment, Primary Key(CustomerNum,Date,id))
select 'PE' as Country,NULL as id,CustomerNum,Date,'' as Mes,OrderNum,if(CouponCode like '%CAC%','CAC','NoCAC') AS Cac,
if(NewReturning = 'NEW','Nuevo','Viejo') as nuevoCliente,
0000000000 as sms
from development_pe.A_Master where Date between '2013-09-01' and '2014-04-30' and OrderAfterCan = 1 GROUP BY OrderNum
order by CustomerNum,Date,nuevoCliente; #213,604

#select * from comprasSeptaAbrilPE_v2 limit 5000;

delete from comprasSeptaAbrilPE_v2 where id > 1;
alter table comprasSeptaAbrilPE_v2 drop column id;
alter table comprasSeptaAbrilPE_v2 modify Mes varchar(15);
update comprasSeptaAbrilPE_v2 set Mes = CASE when month(Date) = 9 then 'September' when month(Date) = 10 then 'October'
when month(Date) = 11 then 'November' when month(Date) = 12 then 'December' when month(Date) = 1 then 'January' 
when month(Date) = 2 then 'February' when month(Date) = 3 then 'March' else 'April' END;


drop table if exists smsPE;
create temporary table smsPE(PRIMARY KEY (custid), INDEX(custid))
select custid,sms from dev_marketing.customers_rev_pe where TYPE_CUSTOMER = 'customer' and sms is not null and custid is not null;

update comprasSeptaAbrilPE_v2 a inner join smsPE b on a.CustomerNum = b.custid set a.sms = b.sms;

delete from comprasSeptaAbrilPE_v2 where sms = 0 or length(sms) < 8;

drop table if exists CRM_bases.comprasSeptAbrilPE_sms;
create table CRM_bases.comprasSeptAbrilPE_sms(id INT auto_increment, Primary Key(id))
select *,-1 as DateDif,0 as a90Dias from comprasSeptaAbrilPE_v2 order by sms,Date;  #200,984


#select * from CRM_bases.comprasSeptAbrilMX_sms limit 500;

set @totalRenglones := (select count(*) from CRM_bases.comprasSeptAbrilPE_sms); #debe ser 200,984
SELECT CustomerNum,sms, cast(Date as char)  INTO @Cust,@sms, @fecha FROM CRM_bases.comprasSeptAbrilPE_sms where id = @totalRenglones;
set @cuenta := @totalRenglones;
#select @cuenta,@totalRenglones,@Cust,@fecha;

WHILE(@cuenta >1) do
  update CRM_bases.comprasSeptAbrilPE_sms set DateDif = if(sms = @sms and CustomerNum <> @Cust,DATEDIFF(@fecha,Date),DateDif) 
where id = @cuenta-1;

SELECT CustomerNum,sms, cast(Date as char)  INTO @Cust,@sms, @fecha FROM CRM_bases.comprasSeptAbrilPE_sms where id = @cuenta-1;

set @cuenta = @cuenta - 1;
END WHILE;

update CRM_bases.comprasSeptAbrilPE_sms set a90Dias = 1 
where Date <= '2014-01-31' and DateDif > 0 and DateDif <= 90;
#termina PERU

#union all:
drop table if exists CRM_bases.RRanalysis_sms;
create table CRM_bases.RRanalysis_sms
(select * from CRM_bases.comprasSeptAbrilMX_sms) 
union all (select * from CRM_bases.comprasSeptAbrilCO_sms) union all(select * from CRM_bases.comprasSeptAbrilPE_sms);

#select * from CRM_bases.RRanalysis;
select CURDATE();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`roberto.lopez`@`%`*/ /*!50003 PROCEDURE `Repurchase_CAC`()
BEGIN

#MEXICO
drop table if exists comprasSeptaAbrilMX;
create temporary table comprasSeptaAbrilMX(id INT auto_increment, Primary Key(CustomerNum,Date,id))
select 'MX' as Country,NULL as id,CustomerNum,Date,'' as Mes,OrderNum,if(CouponCode like '%CAC%','CAC','NoCAC') AS Cac,
sum(Price) as Price,sum(PaidPrice) as PaidPr,sum(Price)/16.35 as PriceEuros,sum(PaidPrice)/16.35 as PaidPrEuros,
sum(CouponValue) as ValorCupon, if(CouponCode = '','SinCupon','ConCupon') AS Cupon, CouponCode, 
if(CouponCode = '',0,sum(CouponValue)/sum(Price)) as pctCupon,
if(NewReturning = 'NEW','Nuevo','Viejo') as nuevoCliente,
count(*) as Items,sum(PaidPrice)/count(*) as AvgTicketbyItem,
ChannelGroup,sum(PCOnePFive) AS PC15, sum(PCOnePFive)/sum(Rev) as PC15pct, sum(PCOne) as PC1, sum(PCOne)/sum(Rev) as PC1divRevenue
from development_mx.A_Master where Date between '2013-09-01' and '2014-04-30' and OrderAfterCan = 1 GROUP BY OrderNum
order by CustomerNum,Date,nuevoCliente; #213,604      23mayo: 212,795

#select * from comprasSeptaAbrilMX limit 5000;
#select * from comprasSeptaAbrilMX where id > 1 and nuevoCliente = 'Nuevo';  #4367,6939,7762,8879,10659,11888,12252,13736,23021,27227
#select * from comprasSeptaAbrilMX where CustomerNum in (4367,6939,7762,8879,10659,11888,12252,13736,23021,27227);
#el 12 compro 2 veces el 14 de nov. (y sale id = 1 y 2 en los 2 renglones de ese dia). p las demas personas siempre id=1

delete from comprasSeptaAbrilMX where id > 1;
alter table comprasSeptaAbrilMX drop column id;
alter table comprasSeptaAbrilMX modify Mes varchar(15);
update comprasSeptaAbrilMX set Mes = CASE when month(Date) = 9 then 'September' when month(Date) = 10 then 'October'
when month(Date) = 11 then 'November' when month(Date) = 12 then 'December' when month(Date) = 1 then 'January' 
when month(Date) = 2 then 'February' when month(Date) = 3 then 'March' else 'April' END;

drop table if exists CRM_bases.comprasSeptAbrilMX;
create table CRM_bases.comprasSeptAbrilMX(id INT auto_increment, Primary Key(id))
select *,-1 as DateDif,0 as a90Dias,
if(Cupon = 'SinCupon','SinCupon',if(ValorCupon/Price <=.05, '<5pct',if(ValorCupon/Price <=.1, '5 to 10pct',if(ValorCupon/Price <=.15, '10 to 15pct',if(ValorCupon/Price <=.25, '15 to 25pct',if(ValorCupon/Price <=.5, '25 to 50pct',if(ValorCupon/Price <=.8, '50 to 80pct','>80pct'))))))) as pctVoucher,
case
when ChannelGroup='Newsletter' then 'NL'
when ChannelGroup='Facebook Ads' or ChannelGroup='Social Media' then 'FB+SM'
when ChannelGroup='Search Engine Marketing' then 'SEM'
when ChannelGroup='Retargeting' then 'Retargeting'
when ChannelGroup='Search Engine Optimization' then 'SEO'
when ChannelGroup='Branded' then 'Branded'
else 'Other' end as ChannelG,
0 as Days_to_Deliver,
0 as Days_Delay,
case
	when PaidPrEuros BETWEEN 0 and 10 then '0-10'
	when PaidPrEuros BETWEEN 10 and 30 then '10-30'
	when PaidPrEuros BETWEEN 30 and 60 then '30-60'
	when PaidPrEuros BETWEEN 60 and 100 then '60-100'
	when PaidPrEuros BETWEEN 100 and 200 then '100-200'
	when PaidPrEuros>200 then '>200'
	end as PriceRangeEuros,
CASE when PC15pct is null or PC15pct < -5 then '< -5 '
when PC15pct between -5 and -2 then '-5 to -2'
when PC15pct between -2 and 0 then '-2 to 0'
when PC15pct between 0 and 2 then '0 to 1'
when PC15pct between 2 and 5 then '2 to 5'
when PC15pct > 5 then '> 5' end
as PC1P5Range,
CASE when PC1divRevenue is null or PC1divRevenue < -2 then '< -2'
when PC1divRevenue between -2 and -1 then '-2 to -1'
when PC1divRevenue between -1 and 0 then '-1 to 0'
when PC1divRevenue between 0 and 0.1 then '0 to 0.1'
when PC1divRevenue between 0.1 and 0.2 then '0.1 to 0.2'
when PC1divRevenue between 0.2 and 0.3 then '0.2 to 0.3'
when PC1divRevenue between 0.3 and 0.4 then '0.3 to 0.4'
when PC1divRevenue between 0.4 and 0.5 then '0.4 to 0.5'
when PC1divRevenue between 0.5 and 1 then '0.5 to 1'
when PC1divRevenue > 1 then '> 1' end
as PC1divRevRange,
 'Not defined' as ExperienceRange,
'Not defined' as DeliverRange
from comprasSeptaAbrilMX;  #200,984     23mayo: 200,274


#select * from CRM_bases.comprasSeptAbrilMX limit 500;

set @totalRenglones := (select count(*) from CRM_bases.comprasSeptAbrilMX); #debe ser 200,984
SELECT CustomerNum, cast(Date as char)  INTO @Cust, @fecha FROM CRM_bases.comprasSeptAbrilMX where id = @totalRenglones;
set @cuenta := @totalRenglones;
#select @cuenta,@totalRenglones,@Cust,@fecha;

WHILE(@cuenta >1) do
  update CRM_bases.comprasSeptAbrilMX set DateDif = if(CustomerNum = @Cust,DATEDIFF(@fecha,Date),DateDif) 
where id = @cuenta-1;

SELECT CustomerNum, cast(Date as char)  INTO @Cust, @fecha FROM CRM_bases.comprasSeptAbrilMX where id = @cuenta-1;

set @cuenta = @cuenta - 1;
END WHILE;

update CRM_bases.comprasSeptAbrilMX set a90Dias = 1 
where Date <= '2014-01-31' and DateDif > 0 and DateDif <= 90;

#termina MEXICO

#COLOMBIA
drop table if exists comprasSeptaAbrilCO;
create temporary table comprasSeptaAbrilCO(id INT auto_increment, Primary Key(CustomerNum,Date,id))
select 'CO' as Country,NULL as id,CustomerNum,Date,'' as Mes,OrderNum,if(CouponCode like '%CAC%','CAC','NoCAC') AS Cac,
sum(Price) as Price,sum(PaidPrice) as PaidPr,sum(Price)/2450 as PriceEuros,sum(PaidPrice)/2450 as PaidPrEuros,
sum(CouponValue) as ValorCupon, if(CouponCode = '','SinCupon','ConCupon') AS Cupon, CouponCode,
if(CouponCode = '',0,sum(CouponValue)/sum(Price)) as pctCupon,
if(NewReturning = 'NEW','Nuevo','Viejo') as nuevoCliente,
count(*) as Items,sum(PaidPrice)/count(*) as AvgTicketbyItem, 
ChannelGroup,sum(PCOnePFive) AS PC15, sum(PCOnePFive)/sum(Rev) as PC15pct, sum(PCOne) as PC1, sum(PCOne)/sum(Rev) as PC1divRevenue
from development_co_project.A_Master where Date between '2013-09-01' and '2014-04-30' and OrderAfterCan = 1 GROUP BY OrderNum
order by CustomerNum,Date,nuevoCliente; #23mayo: 216,536

#select * from comprasSeptiembreaAbril limit 2000;

delete from comprasSeptaAbrilCO where id > 1;
alter table comprasSeptaAbrilCO drop column id;
alter table comprasSeptaAbrilCO modify Mes varchar(15);
update comprasSeptaAbrilCO set Mes = CASE when month(Date) = 9 then 'September' when month(Date) = 10 then 'October'
when month(Date) = 11 then 'November' when month(Date) = 12 then 'December' when month(Date) = 1 then 'January' 
when month(Date) = 2 then 'February' when month(Date) = 3 then 'March' else 'April' END;

drop table if exists CRM_bases.comprasSeptAbrilCO;
create table CRM_bases.comprasSeptAbrilCO(id INT auto_increment, Primary Key(id))
select *,-1 as DateDif,0 as a90Dias,
if(Cupon = 'SinCupon','SinCupon',if(ValorCupon/Price <=.05, '<5pct',if(ValorCupon/Price <=.1, '5 to 10pct',if(ValorCupon/Price <=.15, '10 to 15pct',if(ValorCupon/Price <=.25, '15 to 25pct',if(ValorCupon/Price <=.5, '25 to 50pct',if(ValorCupon/Price <=.8, '50 to 80pct','>80pct'))))))) as pctVoucher,
case
when ChannelGroup='Newsletter' then 'NL'
when ChannelGroup='Facebook Ads' or ChannelGroup='Social Media' then 'FB+SM'
when ChannelGroup='Search Engine Marketing' then 'SEM'
when ChannelGroup='Retargeting' then 'Retargeting'
when ChannelGroup='Search Engine Optimization' then 'SEO'
when ChannelGroup='Branded' then 'Branded'
else 'Other' end as ChannelG,
0 as Days_to_Deliver,
0 as Days_Delay,
case
	when PaidPrEuros BETWEEN 0 and 10 then '0-10'
	when PaidPrEuros BETWEEN 10 and 30 then '10-30'
	when PaidPrEuros BETWEEN 30 and 60 then '30-60'
	when PaidPrEuros BETWEEN 60 and 100 then '60-100'
	when PaidPrEuros BETWEEN 100 and 200 then '100-200'
	when PaidPrEuros>200 then '>200'
	end as PriceRangeEuros,
CASE when PC15pct is null or PC15pct < -5 then '< -5 '
when PC15pct between -5 and -2 then '-5 to -2'
when PC15pct between -2 and 0 then '-2 to 0'
when PC15pct between 0 and 2 then '0 to 2'
when PC15pct between 2 and 5 then '2 to 5'
when PC15pct > 5 then '> 5' end
as PC1P5Range,
CASE when PC1divRevenue is null or PC1divRevenue < -2 then '< -2'
when PC1divRevenue between -2 and -1 then '-2 to -1'
when PC1divRevenue between -1 and 0 then '-1 to 0'
when PC1divRevenue between 0 and 0.1 then '0 to 0.1'
when PC1divRevenue between 0.1 and 0.2 then '0.1 to 0.2'
when PC1divRevenue between 0.2 and 0.3 then '0.2 to 0.3'
when PC1divRevenue between 0.3 and 0.4 then '0.3 to 0.4'
when PC1divRevenue between 0.4 and 0.5 then '0.4 to 0.5'
when PC1divRevenue between 0.5 and 1 then '0.5 to 1'
when PC1divRevenue > 1 then '> 1' end
as PC1divRevRange,
 'Not defined' as ExperienceRange,
'Not defined' as DeliverRange
from comprasSeptaAbrilCO;  #23mayo: 208,066

#select * from CRM_bases.comprasSeptAbrilCO limit 500;

set @totalRenglones := (select count(*) from CRM_bases.comprasSeptAbrilCO); #debe ser 200,984
SELECT CustomerNum, cast(Date as char)  INTO @Cust, @fecha FROM CRM_bases.comprasSeptAbrilCO where id = @totalRenglones;
set @cuenta := @totalRenglones;
#select @cuenta,@totalRenglones,@Cust,@fecha;

WHILE(@cuenta >1) do
  update CRM_bases.comprasSeptAbrilCO set DateDif = if(CustomerNum = @Cust,DATEDIFF(@fecha,Date),DateDif) 
where id = @cuenta-1;

SELECT CustomerNum, cast(Date as char)  INTO @Cust, @fecha FROM CRM_bases.comprasSeptAbrilCO where id = @cuenta-1;

set @cuenta = @cuenta - 1;
END WHILE;

update CRM_bases.comprasSeptAbrilCO set a90Dias = 1 
where Date <= '2014-01-31' and DateDif > 0 and DateDif <= 90;


#termina COLOMBIA


#PERU

drop table if exists comprasSeptaAbrilPE;
create temporary table comprasSeptaAbrilPE(id INT auto_increment, Primary Key(CustomerNum,Date,id))
select 'PE' as Country,NULL as id,CustomerNum,Date,'' as Mes,OrderNum,if(CouponCode like '%CAC%','CAC','NoCAC') AS Cac,
sum(Price) as Price,sum(PaidPrice) as PaidPr,sum(Price)/3.3 as PriceEuros,sum(PaidPrice)/3.3 as PaidPrEuros,
sum(CouponValue) as ValorCupon, if(CouponCode = '','SinCupon','ConCupon') AS Cupon, CouponCode,
if(CouponCode = '',0,sum(CouponValue)/sum(Price)) as pctCupon,
if(NewReturning = 'NEW','Nuevo','Viejo') as nuevoCliente,
count(*) as Items,sum(PaidPrice)/count(*) as AvgTicketbyItem, 
ChannelGroup,sum(PCOnePFive) AS PC15, sum(PCOnePFive)/sum(Rev) as PC15pct, sum(PCOne) as PC1, sum(PCOne)/sum(Rev) as PC1divRevenue
from development_pe.A_Master where Date between '2013-09-01' and '2014-04-30' and OrderAfterCan = 1 GROUP BY OrderNum
order by CustomerNum,Date,nuevoCliente; #213,604

#select * from comprasSeptiembreaAbril limit 2000;
#el 12 compro 2 veces el 14 de nov. (y sale id = 1 y 2 en los 2 renglones de ese dia). p las demas personas siempre id=1

delete from comprasSeptaAbrilPE where id > 1;
alter table comprasSeptaAbrilPE drop column id;
alter table comprasSeptaAbrilPE modify Mes varchar(15);
update comprasSeptaAbrilPE set Mes = CASE when month(Date) = 9 then 'September' when month(Date) = 10 then 'October'
when month(Date) = 11 then 'November' when month(Date) = 12 then 'December' when month(Date) = 1 then 'January' 
when month(Date) = 2 then 'February' when month(Date) = 3 then 'March' else 'April' END;

drop table if exists CRM_bases.comprasSeptAbrilPE;
create table CRM_bases.comprasSeptAbrilPE(id INT auto_increment, Primary Key(id))
select *,-1 as DateDif,0 as a90Dias,
if(Cupon = 'SinCupon','SinCupon',if(ValorCupon/Price <=.05, '<5pct',if(ValorCupon/Price <=.1, '5 to 10pct',if(ValorCupon/Price <=.15, '10 to 15pct',if(ValorCupon/Price <=.25, '15 to 25pct',if(ValorCupon/Price <=.5, '25 to 50pct',if(ValorCupon/Price <=.8, '50 to 80pct','>80pct'))))))) as pctVoucher,
case
when ChannelGroup='Newsletter' then 'NL'
when ChannelGroup='Facebook Ads' or ChannelGroup='Social Media' then 'FB+SM'
when ChannelGroup='Search Engine Marketing' then 'SEM'
when ChannelGroup='Retargeting' then 'Retargeting'
when ChannelGroup='Search Engine Optimization' then 'SEO'
when ChannelGroup='Branded' then 'Branded'
else 'Other' end as ChannelG,
0 as Days_to_Deliver,
0 as Days_Delay,
case
	when PaidPrEuros BETWEEN 0 and 10 then '0-10'
	when PaidPrEuros BETWEEN 10 and 30 then '10-30'
	when PaidPrEuros BETWEEN 30 and 60 then '30-60'
	when PaidPrEuros BETWEEN 60 and 100 then '60-100'
	when PaidPrEuros BETWEEN 100 and 200 then '100-200'
	when PaidPrEuros>200 then '>200'
	end as PriceRangeEuros,
CASE when PC15pct is null or PC15pct < -5 then '< -5 '
when PC15pct between -5 and -2 then '-5 to -2'
when PC15pct between -2 and 0 then '-2 to 0'
when PC15pct between 0 and 2 then '0 to 2'
when PC15pct between 2 and 5 then '2 to 5'
when PC15pct > 5 then '> 5' end
as PC1P5Range,
CASE when PC1divRevenue is null or PC1divRevenue < -2 then '< -2'
when PC1divRevenue between -2 and -1 then '-2 to -1'
when PC1divRevenue between -1 and 0 then '-1 to 0'
when PC1divRevenue between 0 and 0.1 then '0 to 0.1'
when PC1divRevenue between 0.1 and 0.2 then '0.1 to 0.2'
when PC1divRevenue between 0.2 and 0.3 then '0.2 to 0.3'
when PC1divRevenue between 0.3 and 0.4 then '0.3 to 0.4'
when PC1divRevenue between 0.4 and 0.5 then '0.4 to 0.5'
when PC1divRevenue between 0.5 and 1 then '0.5 to 1'
when PC1divRevenue > 1 then '> 1' end
as PC1divRevRange,
 'Not defined' as ExperienceRange,
'Not defined' as DeliverRange
from comprasSeptaAbrilPE;  
#DeliveredRange
#rangos de delivered 0-2,3-4,5-7,7-14,15-20,21-50,>50
#select * from CRM_bases.comprasSeptAbrilv2 limit 500;

set @totalRenglones := (select count(*) from CRM_bases.comprasSeptAbrilPE); #debe ser 200,984
SELECT CustomerNum, cast(Date as char)  INTO @Cust, @fecha FROM CRM_bases.comprasSeptAbrilPE where id = @totalRenglones;
set @cuenta := @totalRenglones;
#select @cuenta,@totalRenglones,@Cust,@fecha;

WHILE(@cuenta >1) do
  update CRM_bases.comprasSeptAbrilPE set DateDif = if(CustomerNum = @Cust,DATEDIFF(@fecha,Date),DateDif) 
where id = @cuenta-1;

SELECT CustomerNum, cast(Date as char)  INTO @Cust, @fecha FROM CRM_bases.comprasSeptAbrilPE where id = @cuenta-1;

set @cuenta = @cuenta - 1;
END WHILE;

update CRM_bases.comprasSeptAbrilPE set a90Dias = 1 
where Date <= '2014-01-31' and DateDif > 0 and DateDif <= 90;

#termina PERU

#actualizar el date delivered y days DELAYED
select 'Ejecutando join de date delivered y delay...';

drop table if exists CRM_bases.aux_days_to_deliverMX;
create table CRM_bases.aux_days_to_deliverMX
SELECT order_number,date_ordered,date_delivered,date_delivered_promised,
	datediff(date_delivered,date_ordered) as Days_to_Deliver,
	datediff(date_delivered,date_delivered_promised) as Days_Delay
FROM operations_mx.out_order_tracking
where date_ordered between '2013-09-01' and '2014-04-30';


drop table if exists CRM_bases.aux_days_to_deliverCO;
create table CRM_bases.aux_days_to_deliverCO
SELECT order_number,date_ordered,date_delivered,date_delivered_promised,
	datediff(date_delivered,date_ordered) as Days_to_Deliver,
	datediff(date_delivered,date_delivered_promised) as Days_Delay
FROM operations_co.out_order_tracking
where date_ordered between '2013-09-01' and '2014-04-30';


drop table if exists CRM_bases.aux_days_to_deliverPE;
create table CRM_bases.aux_days_to_deliverPE
SELECT order_number,date_ordered,date_delivered,date_delivered_promised,
	datediff(date_delivered,date_ordered) as Days_to_Deliver,
	datediff(date_delivered,date_delivered_promised) as Days_Delay
FROM 	operations_pe.out_order_tracking
where date_ordered between '2013-09-01' and '2014-04-30';


drop table if exists CRM_bases.days_to_deliverMX;
create table CRM_bases.days_to_deliverMX(PRIMARY KEY(order_number), INDEX(order_number))
select order_number,Max(Days_to_Deliver) as max_Days_to_Deliver,MAX(Days_Delay) as max_Days_Delay
from CRM_bases.aux_days_to_deliverMX group by order_number;


drop table if exists CRM_bases.days_to_deliverCO;
create table CRM_bases.days_to_deliverCO(PRIMARY KEY(order_number), INDEX(order_number))
select order_number,Max(Days_to_Deliver) as max_Days_to_Deliver,MAX(Days_Delay) as max_Days_Delay
from CRM_bases.aux_days_to_deliverCO group by order_number;

drop table if exists CRM_bases.days_to_deliverPE;
create table CRM_bases.days_to_deliverPE(PRIMARY KEY(order_number), INDEX(order_number))
select order_number,Max(Days_to_Deliver) as max_Days_to_Deliver,MAX(Days_Delay) as max_Days_Delay
from CRM_bases.aux_days_to_deliverPE group by order_number;

update CRM_bases.comprasSeptAbrilMX a Inner Join CRM_bases.days_to_deliverMX b
		on a.OrderNum=b.order_number set a.Days_to_Deliver=b.max_Days_to_Deliver, a.Days_Delay=b.max_Days_Delay;

update CRM_bases.comprasSeptAbrilCO a Inner Join CRM_bases.days_to_deliverCO b
		on a.OrderNum=b.order_number set a.Days_to_Deliver=b.max_Days_to_Deliver, a.Days_Delay=b.max_Days_Delay;

update CRM_bases.comprasSeptAbrilPE a Inner Join CRM_bases.days_to_deliverPE b
		on a.OrderNum=b.order_number set a.Days_to_Deliver=b.max_Days_to_Deliver, a.Days_Delay=b.max_Days_Delay;

update CRM_bases.comprasSeptAbrilMX set ExperienceRange = 
 case 
when Days_Delay <= -2 THEN 'Very Good'
when Days_Delay between -1 and 0 then 'Good'
when Days_Delay between 1 and 3 then 'Bad'
when Days_Delay >=4 THEN 'Very Bad'  end, 
DeliverRange = 
CASE
when Days_to_Deliver between 0 and 2 then '0 to 2'
when Days_to_Deliver between 3 and 4 then '3 to 4'
when Days_to_Deliver between 5 and 7 then '5 to 7'
when Days_to_Deliver between 8 and 14 then '8 to 14'
when Days_to_Deliver between 15 and 20 then '15 to 20'
when Days_to_Deliver between 21 and 50 then '21 to 50'
else '>50'
end;

update CRM_bases.comprasSeptAbrilCO set ExperienceRange = 
 case 
when Days_Delay <= -2 THEN 'Very Good'
when Days_Delay between -1 and 0 then 'Good'
when Days_Delay between 1 and 3 then 'Bad'
when Days_Delay >=4 THEN 'Very Bad'
end, DeliverRange = 
CASE
when Days_to_Deliver between 0 and 2 then '0 to 2'
when Days_to_Deliver between 3 and 4 then '3 to 4'
when Days_to_Deliver between 5 and 7 then '5 to 7'
when Days_to_Deliver between 8 and 14 then '8 to 14'
when Days_to_Deliver between 15 and 20 then '15 to 20'
when Days_to_Deliver between 21 and 50 then '21 to 50'
else '>50'
end;

update CRM_bases.comprasSeptAbrilPE set ExperienceRange = 
 case 
when Days_Delay <= -2 THEN 'Very Good'
when Days_Delay between -1 and 0 then 'Good'
when Days_Delay between 1 and 3 then 'Bad'
when Days_Delay >=4 THEN 'Very Bad'
end, DeliverRange = 
CASE
when Days_to_Deliver between 0 and 2 then '0 to 2'
when Days_to_Deliver between 3 and 4 then '3 to 4'
when Days_to_Deliver between 5 and 7 then '5 to 7'
when Days_to_Deliver between 8 and 14 then '8 to 14'
when Days_to_Deliver between 15 and 20 then '15 to 20'
when Days_to_Deliver between 21 and 50 then '21 to 50'
else '>50'
end;


#union all:
drop table if exists CRM_bases.RRanalysis;
create table CRM_bases.RRanalysis
(select * from CRM_bases.comprasSeptAbrilMX) 
union all (select * from CRM_bases.comprasSeptAbrilCO) union all(select * from CRM_bases.comprasSeptAbrilPE);

#select * from CRM_bases.RRanalysis;

select CURDATE();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`roberto.lopez`@`%`*/ /*!50003 PROCEDURE `RutinaLeadsByWeekMX`()
BEGIN


DROP TEMPORARY TABLE IF EXISTS TMP_Orders;
CREATE TEMPORARY TABLE TMP_Orders(PRIMARY KEY (OrderNum),INDEX (IdSalesOrder)) 
SELECT IdSalesOrder, OrderNum, CustomerNum, date, Time, count(*) AS Items, SUM(Rev) AS Rev, SUM(PCOnePFive) AS PCOnePFive
FROM development_mx.A_Master WHERE OrderAfterCan = 1 GROUP BY OrderNum;
#select * from TMP_Orders;   #TIENE TODAS LAS ORDENES QUE SE HAN HECHO. EL CLIENTE, FECHA, CUANTOS ITEMS, SUMA REV Y PC1.5
#1 RENGLON POR ORDEN.

DROP TEMPORARY TABLE IF EXISTS TMP_CustomerOrders;
CREATE TEMPORARY TABLE TMP_CustomerOrders ( id INT auto_increment, PRIMARY KEY (CustomerNum, id), INDEX (IdSalesOrder) ) 
SELECT IdSalesOrder,OrderNum,CustomerNum,date,Time,NULL AS id,Items,Rev,PCOnePFive
FROM TMP_Orders ORDER BY CustomerNum,Date,Time;  #301,361 ordenes
#SELECT * from TMP_CustomerOrders;  #1 RENGLON POR ORDEN PERO ESTA ORDENADO POR CLIENTE. LA variable id numera las ordenes por cliente.
#ejemplo 1,2,3,1,2,1,1,2,3,4,5,6,1,


DROP TEMPORARY TABLE IF EXISTS TMP_Orders_last4w;
CREATE TEMPORARY TABLE TMP_Orders_last4w (PRIMARY KEY (OrderNum),INDEX (IdSalesOrder)) 
SELECT IdSalesOrder,OrderNum,CustomerNum,date,Time,count(*) AS Items,SUM(Rev) AS Rev,SUM(PCOnePFive) AS PCOnePFive
FROM development_mx.A_Master WHERE OrderAfterCan = 1 and Date >= CURDATE()-INTERVAL 28 DAY
GROUP BY OrderNum;
#select * from TMP_Orders_last4w;   #TIENE TODAS LAS ORDENES ULT 4 SEM. EL CLIENTE, FECHA, CUANTOS ITEMS, SUMA REV Y PC1.5. 1 RENG X ORD.

DROP TEMPORARY TABLE IF EXISTS TMP_CustomerOrders_last4w;
CREATE TEMPORARY TABLE TMP_CustomerOrders_last4w (id INT auto_increment,PRIMARY KEY (CustomerNum, id),INDEX (IdSalesOrder)) 
SELECT IdSalesOrder,OrderNum,CustomerNum,date,Time,NULL AS id,Items,Rev,PCOnePFive
FROM TMP_Orders_last4w ORDER BY CustomerNum,Date,Time;

#select * from TMP_CustomerOrders_last4w;


DROP TEMPORARY TABLE IF EXISTS TMP_CustomerOrders_4to3W;
CREATE TEMPORARY TABLE TMP_CustomerOrders_4to3W 
select OrderNum,CustomerNum,date,Time,id,Items,Rev,PCOnePFive from TMP_CustomerOrders_last4w where 
date between (CURDATE()-interval 28 DAY) and (CURDATE()-INTERVAL 22 DAY) ORDER BY CustomerNum,Date,Time;

DROP TEMPORARY TABLE IF EXISTS TMP_CustomerOrders_3to2W;
CREATE TEMPORARY TABLE TMP_CustomerOrders_3to2W 
select OrderNum,CustomerNum,date,Time,id,Items,Rev,PCOnePFive from TMP_CustomerOrders_last4w where 
date between (CURDATE()-interval 21 DAY) and (CURDATE()-INTERVAL 15 DAY) ORDER BY CustomerNum,Date,Time;

DROP TEMPORARY TABLE IF EXISTS TMP_CustomerOrders_2to1W;
CREATE TEMPORARY TABLE TMP_CustomerOrders_2to1W 
select OrderNum,CustomerNum,date,Time,id,Items,Rev,PCOnePFive from TMP_CustomerOrders_last4w where 
date between (CURDATE()-interval 14 DAY) and (CURDATE()-INTERVAL 8 DAY) ORDER BY CustomerNum,Date,Time;

DROP TEMPORARY TABLE IF EXISTS TMP_CustomerOrders_lastW;
CREATE TEMPORARY TABLE TMP_CustomerOrders_lastW 
select OrderNum,CustomerNum,date,Time,id,Items,Rev,PCOnePFive from TMP_CustomerOrders_last4w where 
date >= (CURDATE()-interval 7 DAY) ORDER BY CustomerNum,Date,Time;

#select * from TMP_CustomerOrders_4to3W;  #24 feb a 02 mar
#select * from TMP_CustomerOrders_3to2W;  #03 a 09 mar
#select * from TMP_CustomerOrders_2to1W;  #10 a 16
#select * from TMP_CustomerOrders_lastW;  #17 a 23


#select * from TMP_CUSTOMERS;
DROP TEMPORARY TABLE IF EXISTS TMP_CUSTOMERS;
CREATE TEMPORARY TABLE TMP_CUSTOMERS (PRIMARY KEY(email)) 
SELECT custid, date_registred,
  (CASE WHEN date_registred BETWEEN (CURDATE()-INTERVAL 28 DAY) AND (CURDATE()-INTERVAL 22 DAY) THEN '4 TO 3 WEEKS AGO'
       WHEN date_registred BETWEEN (CURDATE()-INTERVAL 21 DAY) AND (CURDATE()-INTERVAL 15 DAY) THEN '3 TO 2 WEEKS AGO'
       WHEN date_registred BETWEEN (CURDATE()-INTERVAL 14 DAY) AND (CURDATE()-INTERVAL 8 DAY) THEN '2 TO 1 WEEK AGO'
       WHEN date_registred BETWEEN (CURDATE()-INTERVAL 7 DAY) AND CURDATE() THEN 'LAST WEEK'
       ELSE 'BEFORE 4 WEEKS AGO' END) AS Period,
	Source_data as Source, email, 0 as isCustomer, 0 AS NumTransac, "0000-00-00" AS date_first_order,
  00000000000.00 AS Rev_Order1_pesos, 00000000000.00 AS PCOnePFive_Order1_pesos, 00000000000.00 AS RevTotal_pesos, 
  00000000000.00 AS PCOnePFiveTotal_pesos,
  'No customer' as daysToOrd1
FROM dev_marketing.customers_rev_mx
WHERE custid IS NOT NULL 
GROUP BY custid;


#TMP_CUSTOMERS sera una tabla con un renglon por cliente. incluye su source,año y mes de registro, si es cust o NO,fecha,rev,pc1.5 de la primera orden
#tambien revenue total de esa persona y pc1.5 total (suma)
#aqui abajo actualiza la info. de la primer compra por cliente.   despues lo hara para el rev total y pc1.5 total
UPDATE TMP_CUSTOMERS
INNER JOIN TMP_CustomerOrders ON TMP_CustomerOrders.CustomerNum = TMP_CUSTOMERS.custid
AND TMP_CustomerOrders.id = 1
SET TMP_CUSTOMERS.date_first_order = TMP_CustomerOrders.Date,
 TMP_CUSTOMERS.Rev_Order1_pesos = TMP_CustomerOrders.Rev,
 TMP_CUSTOMERS.PCOnePFive_Order1_pesos = TMP_CustomerOrders.PCOnePFive,
 TMP_CUSTOMERS.daysToOrd1 = DATEDIFF(TMP_CustomerOrders.Date,TMP_CUSTOMERS.date_registred);

DROP TEMPORARY TABLE IF EXISTS TMP_CustomerTotal;
CREATE TEMPORARY TABLE TMP_CustomerTotal (PRIMARY KEY(CustomerNum)) 
SELECT CustomerNum,COUNT(DISTINCT OrderNum) AS NumTransac,SUM(Rev) AS Rev,SUM(PCOnePFive) AS PCOnePFive
FROM TMP_Orders GROUP BY CustomerNum;

UPDATE TMP_CUSTOMERS
INNER JOIN TMP_CustomerTotal ON TMP_CustomerTotal.CustomerNum = TMP_CUSTOMERS.custid
SET TMP_CUSTOMERS.NumTransac = TMP_CustomerTotal.NumTransac,
 TMP_CUSTOMERS.RevTotal_pesos = TMP_CustomerTotal.Rev,
 TMP_CUSTOMERS.PCOnePFiveTotal_pesos = TMP_CustomerTotal.PCOnePFive,
 TMP_CUSTOMERS.isCustomer = 1;

#agregare esto:  personas sin custid
#ojo: los que no eran null se insertaron al inicio en TMP_CUSTOMERS
INSERT INTO TMP_CUSTOMERS (Source,email,date_registred) 
SELECT Source_data,email,date_registred FROM dev_marketing.customers_rev_mx WHERE custid IS NULL;

UPDATE TMP_CUSTOMERS
SET date_first_order = "0000-00-00", daysToOrd1 = 'No customer',
Period = (CASE WHEN date_registred BETWEEN (CURDATE()-INTERVAL 28 DAY) AND (CURDATE()-INTERVAL 22 DAY) THEN '4 TO 3 WEEKS AGO'
       WHEN date_registred BETWEEN (CURDATE()-INTERVAL 21 DAY) AND (CURDATE()-INTERVAL 15 DAY) THEN '3 TO 2 WEEKS AGO'
       WHEN date_registred BETWEEN (CURDATE()-INTERVAL 14 DAY) AND (CURDATE()-INTERVAL 8 DAY) THEN '2 TO 1 WEEK AGO'
       WHEN date_registred BETWEEN (CURDATE()-INTERVAL 7 DAY) AND CURDATE() THEN 'LAST WEEK'
       ELSE 'BEFORE 4 WEEKS AGO' END)
WHERE (custid is null) and (date_registred is not null);


update TMP_CUSTOMERS set Source = CASE WHEN Source like '%natexo%' then 'Natexo'
when Source like '%curebit%' then 'Curebit' 
when Source like '%BX%' or Source like '%exchange%' or Source like '%bounce%' then 'Bounce Exchange'
when Source is null then 'NULL' 
ELSE Source END;


#select * from TMP_CUSTOMERS_last4Weeks;
DROP TEMPORARY TABLE IF EXISTS TMP_CUSTOMERS_last4Weeks;
CREATE TEMPORARY TABLE TMP_CUSTOMERS_last4Weeks 
SELECT custid, date_registred,Period,Source, email,isCustomer,0 as isCust_4to3w,0 as isCust_3to2w,0 as isCust_2to1w,0 as isCust_lastw,
0 as NumTransac_4to3w,0 as NumTransac_3to2w,0 as NumTransac_2to1w,0 as NumTransac_lastw,
0.0 as Rev_4to3w,0.0 as Rev_3to2w,0.0 as Rev_2to1w,0.0 as Rev_lastw,
0.0 as PC15_4to3w,0.0 as PC15_3to2w,0.0 as PC15_2to1w,0.0 as PC15_lastw, 
0 as Opener_2to1w, 0 as Opener_lastw
FROM TMP_CUSTOMERS WHERE Period <> 'BEFORE 4 WEEKS AGO';

#cambiar esto de abajo.     FALTA QUE PARA CUST 4 TO 3 W SOLO TOME LOS QUE SE AGREGARON EN ESE PERIODO, OTROS NO.
DROP TEMPORARY TABLE IF EXISTS TMP_CustomerTotal_4to3W;
CREATE TEMPORARY TABLE TMP_CustomerTotal_4to3W (PRIMARY KEY(CustomerNum)) 
SELECT CustomerNum,COUNT(DISTINCT OrderNum) AS NumTransac,SUM(Rev) AS Rev,SUM(PCOnePFive) AS PCOnePFive
FROM TMP_CustomerOrders_4to3W GROUP BY CustomerNum;

UPDATE TMP_CUSTOMERS_last4Weeks
INNER JOIN TMP_CustomerTotal_4to3W ON TMP_CustomerTotal_4to3W.CustomerNum = TMP_CUSTOMERS_last4Weeks.custid
SET TMP_CUSTOMERS_last4Weeks.NumTransac_4to3w = TMP_CustomerTotal_4to3W.NumTransac,
 TMP_CUSTOMERS_last4Weeks.Rev_4to3w = TMP_CustomerTotal_4to3W.Rev,
 TMP_CUSTOMERS_last4Weeks.PC15_4to3w = TMP_CustomerTotal_4to3W.PCOnePFive,
 TMP_CUSTOMERS_last4Weeks.isCust_4to3w = 1;


DROP TEMPORARY TABLE IF EXISTS TMP_CustomerTotal_3to2W;
CREATE TEMPORARY TABLE TMP_CustomerTotal_3to2W (PRIMARY KEY(CustomerNum)) 
SELECT CustomerNum,COUNT(DISTINCT OrderNum) AS NumTransac,SUM(Rev) AS Rev,SUM(PCOnePFive) AS PCOnePFive
FROM TMP_CustomerOrders_3to2W GROUP BY CustomerNum;

UPDATE TMP_CUSTOMERS_last4Weeks
INNER JOIN TMP_CustomerTotal_3to2W ON TMP_CustomerTotal_3to2W.CustomerNum = TMP_CUSTOMERS_last4Weeks.custid
SET TMP_CUSTOMERS_last4Weeks.NumTransac_4to3w = TMP_CustomerTotal_3to2W.NumTransac,
 TMP_CUSTOMERS_last4Weeks.Rev_3to2w = TMP_CustomerTotal_3to2W.Rev,
 TMP_CUSTOMERS_last4Weeks.PC15_3to2w = TMP_CustomerTotal_3to2W.PCOnePFive,
 TMP_CUSTOMERS_last4Weeks.isCust_3to2w = 1;


DROP TEMPORARY TABLE IF EXISTS TMP_CustomerTotal_2to1W;
CREATE TEMPORARY TABLE TMP_CustomerTotal_2to1W (PRIMARY KEY(CustomerNum)) 
SELECT CustomerNum,COUNT(DISTINCT OrderNum) AS NumTransac,SUM(Rev) AS Rev,SUM(PCOnePFive) AS PCOnePFive
FROM TMP_CustomerOrders_2to1W GROUP BY CustomerNum;

UPDATE TMP_CUSTOMERS_last4Weeks
INNER JOIN TMP_CustomerTotal_2to1W ON TMP_CustomerTotal_2to1W.CustomerNum = TMP_CUSTOMERS_last4Weeks.custid
SET TMP_CUSTOMERS_last4Weeks.NumTransac_2to1w = TMP_CustomerTotal_2to1W.NumTransac,
 TMP_CUSTOMERS_last4Weeks.Rev_2to1w = TMP_CustomerTotal_2to1W.Rev,
 TMP_CUSTOMERS_last4Weeks.PC15_2to1w = TMP_CustomerTotal_2to1W.PCOnePFive,
 TMP_CUSTOMERS_last4Weeks.isCust_2to1w = 1;


DROP TEMPORARY TABLE IF EXISTS TMP_CustomerTotal_lastW;
CREATE TEMPORARY TABLE TMP_CustomerTotal_lastW (PRIMARY KEY(CustomerNum)) 
SELECT CustomerNum,COUNT(DISTINCT OrderNum) AS NumTransac,SUM(Rev) AS Rev,SUM(PCOnePFive) AS PCOnePFive
FROM TMP_CustomerOrders_lastW GROUP BY CustomerNum;

UPDATE TMP_CUSTOMERS_last4Weeks
INNER JOIN TMP_CustomerTotal_lastW ON TMP_CustomerTotal_lastW.CustomerNum = TMP_CUSTOMERS_last4Weeks.custid
SET TMP_CUSTOMERS_last4Weeks.NumTransac_lastw = TMP_CustomerTotal_lastW.NumTransac,
 TMP_CUSTOMERS_last4Weeks.Rev_lastw = TMP_CustomerTotal_lastW.Rev,
 TMP_CUSTOMERS_last4Weeks.PC15_lastw = TMP_CustomerTotal_lastW.PCOnePFive,
 TMP_CUSTOMERS_last4Weeks.isCust_lastw = 1;

#cambiar esto de arriba

drop table if exists Openers2to1w;
create temporary table Openers2to1w (PRIMARY KEY(UniqueKey))
select distinct UniqueKey from CRM_MX.campaing_openings 
where `Dispatch-Start` >= (CURDATE()-INTERVAL 14 DAY) and `Dispatch-Start` <= (CURDATE()-INTERVAL 8 DAY) and
`Opening-Timestamp` >= (CURDATE()-INTERVAL 14 DAY) and `Opening-Timestamp` <= (CURDATE()-INTERVAL 8 DAY);

drop table if exists Openerslastw;
create temporary table Openerslastw (PRIMARY KEY(UniqueKey))
select distinct UniqueKey from CRM_MX.campaing_openings 
where `Dispatch-Start` >= (CURDATE()-INTERVAL 7 DAY) and `Opening-Timestamp` >= (CURDATE()-INTERVAL 7 DAY);

UPDATE TMP_CUSTOMERS_last4Weeks
INNER JOIN Openers2to1w ON Openers2to1w.UniqueKey = TMP_CUSTOMERS_last4Weeks.email
SET TMP_CUSTOMERS_last4Weeks.Opener_2to1w = 1;

UPDATE TMP_CUSTOMERS_last4Weeks
INNER JOIN Openerslastw ON Openerslastw.UniqueKey = TMP_CUSTOMERS_last4Weeks.email
SET TMP_CUSTOMERS_last4Weeks.Opener_lastw = 1;


#seguirle aqui

update TMP_CUSTOMERS_last4Weeks set Source = CASE WHEN Source like '%natexo%' then 'Natexo'
when Source like '%curebit%' then 'Curebit' 
when Source like '%BX%' or Source like '%exchange%' or Source like '%bounce%' then 'Bounce Exchange'
when Source is null then 'NULL' 
ELSE Source END;
#select * from TMP_CUSTOMERS_last4Weeks;

drop table if exists PivotSourcesLast4weeks;
create temporary table PivotSourcesLast4weeks
SELECT Source,sum(if(Period='4 TO 3 WEEKS AGO',1,0)) AS Personas4to3w, sum(if(Period='3 TO 2 WEEKS AGO',1,0)) AS Personas3to2w, 
sum(if(Period='2 TO 1 WEEK AGO',1,0)) AS agregados2to1w, sum(if(Period='LAST WEEK',1,0)) AS PersonasLastw,
sum(isCust_4to3w) as Clientes4to3w, sum(isCust_3to2w) as Clientes3to2w, sum(isCust_2to1w) as Clientes2to1w, 
sum(isCust_lastw) as ClientesLastw, sum(NumTransac_4to3w) as SNumTrans4to3w,avg(NumTransac_4to3w) as AvgNumTrans4to3w,
sum(Rev_4to3w) as SRev4to3w,avg(Rev_4to3w) as AvRev4to3w ,
sum(Opener_2to1w) as Openers2to1w, sum(Opener_lastw) as OpenersLastw
from TMP_CUSTOMERS_last4Weeks 
group by Source;
#select * from PivotSourcesLast4weeks;  #27 rows.
update PivotSourcesLast4weeks set Source = 'NULL' where Source is null;


DROP TABLE IF EXISTS CRM_bases.SourcesByWeek; 
CREATE TABLE CRM_bases.SourcesByWeek SELECT
	Source,
	COUNT(*) AS Personas,
	SUM(IsCustomer) AS Clientes,
	(SUM(IsCustomer) / COUNT(*))*100 AS pctConversionRate,
	sum(NumTransac) / SUM(IsCustomer) AS AvgOrdersxCustomer,
	avg(DATEDIFF(date_first_order,date_registred)) AS AvgDaysToFirstOrder,
  0 as Personas4to3w, 0 as Personas3to2w, 0 as agregados2to1w, 0 as PersonasLastw,
  0 as Clientes4to3w, 0 as Clientes3to2w, 0 as Clientes2to1w, 0 as ClientesLastw,
  0 as SNumTrans4to3w, 0 as AvgNumTrans4to3w, 0 as SRev4to3w, 0 as AvRev4to3w,
  0 as Openers2to1w, 0 as OpenersLastw
FROM TMP_CUSTOMERS 
GROUP BY Source;
#REVISAR AVGDAYSTOFIRSTORDERS

update CRM_bases.SourcesByWeek set Source = 'NULL' where Source is null;

UPDATE CRM_bases.SourcesByWeek a
LEFT JOIN PivotSourcesLast4weeks b ON a.Source = b.Source
SET a.Personas4to3w = b.Personas4to3w,
 a.Personas3to2w = b.Personas3to2w,
 a.agregados2to1w = b.agregados2to1w,
 a.PersonasLastw = b.PersonasLastw,
 a.Clientes4to3w = b.Clientes4to3w,
 a.Clientes3to2w = b.Clientes3to2w,
 a.Clientes2to1w = b.Clientes2to1w,
 a.ClientesLastw = b.ClientesLastw,
 a.SNumTrans4to3w = b.SNumTrans4to3w,
 a.SRev4to3w = b.SRev4to3w,
 a.AvRev4to3w = b.AvRev4to3w,
 a.Openers2to1w = b.Openers2to1w,
 a.OpenersLastw = b.OpenersLastw;
 
#¿actualiza al NULL? NO.  hay que escribirle 'NULL'  y esperar a que asi se haga el join

update CRM_bases.SourcesByWeek set AvgOrdersxCustomer = 0 where AvgOrdersxCustomer is null;
update CRM_bases.SourcesByWeek set AvgDaysToFirstOrder = 0 where AvgDaysToFirstOrder is null;
alter table CRM_bases.SourcesByWeek modify AvgDaysToFirstOrder decimal(10,2);
alter table CRM_bases.SourcesByWeek modify pctConversionRate decimal(10,2);
alter table CRM_bases.SourcesByWeek modify AvgOrdersxCustomer decimal(10,2);

select CURDATE();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`roberto.lopez`@`%`*/ /*!50003 PROCEDURE `RutinaLeadsCO`()
BEGIN
#Lead generation report para Colombia
DROP TEMPORARY TABLE IF EXISTS TMP_Orders;
CREATE TEMPORARY TABLE TMP_Orders ( PRIMARY KEY (OrderNum),INDEX (IdSalesOrder)) 
SELECT IdSalesOrder, OrderNum, CustomerNum, date, Time, count(*) AS Items, SUM(Rev) AS Rev, SUM(PCOnePFive) AS PCOnePFive
FROM development_co_project.A_Master WHERE OrderAfterCan = 1 GROUP BY OrderNum;

#select * from TMP_Orders;   #TIENE TODAS LAS ORDENES QUE SE HAN HECHO. EL CLIENTE, FECHA, CUANTOS ITEMS, SUMA REV Y PC1.5
#1 RENGLON POR ORDEN.
DROP TEMPORARY TABLE IF EXISTS TMP_CustomerOrders;
CREATE TEMPORARY TABLE TMP_CustomerOrders (
	id INT auto_increment, PRIMARY KEY (CustomerNum, id), INDEX (IdSalesOrder) ) 
SELECT IdSalesOrder,OrderNum,CustomerNum,date,Time,NULL AS id,Items,Rev,PCOnePFive
FROM TMP_Orders ORDER BY CustomerNum,Date,Time;

#SELECT * from TMP_CustomerOrders;  #1 RENGLON POR ORDEN PERO ESTA ORDENADO POR CLIENTE. LA variable id numera las ordenes por cliente.
#ejemplo 1,2,3,1,2,1,1,2,3,4,5,6,1,

DROP TEMPORARY TABLE IF EXISTS TMP_CUSTOMERS;
CREATE TEMPORARY TABLE TMP_CUSTOMERS (PRIMARY KEY(email)) 
SELECT custid, YEAR (date_registred) AS Year_date_registred, MONTH (date_registred) AS Month_date_registred,
	source_data, email,
IF(SUM(IF(TYPE_CUSTOMER = "customer",1,0))>0,1,0) AS IsCustomer,
 0 AS NumTransac, date_registred,
 "0000-00-00" AS date_first_order,
 00000000000.00 AS Rev_first_order,
 00000000000.00 AS PCOnePFive_first_order,
 00000000000.00 AS Rev_Total,
 00000000000.00 AS PCOnePFive_Total
FROM dev_marketing.customers_rev_co WHERE custid IS NOT NULL GROUP BY custid;

#TMP_CUSTOMERS sera una tabla con un renglon por cliente. incluye su source,año y mes de registro, si es cust o NO,fecha,rev,pc1.5 de la primera orden
#tambien revenue total de esa persona y pc1.5 total (suma)
#aqui abajo actualiza la info. de la primer compra por cliente.   despues lo hara para el rev total y pc1.5 total
UPDATE TMP_CUSTOMERS
INNER JOIN TMP_CustomerOrders ON TMP_CustomerOrders.CustomerNum = TMP_CUSTOMERS.custid
AND TMP_CustomerOrders.id = 1
SET TMP_CUSTOMERS.date_first_order = TMP_CustomerOrders.Date,
 TMP_CUSTOMERS.Rev_first_order = TMP_CustomerOrders.Rev,
 TMP_CUSTOMERS.PCOnePFive_first_order = TMP_CustomerOrders.PCOnePFive;

DROP TEMPORARY TABLE IF EXISTS TMP_CustomerTotal;
CREATE TEMPORARY TABLE TMP_CustomerTotal (PRIMARY KEY(CustomerNum)) 
SELECT CustomerNum,COUNT(DISTINCT OrderNum) AS NumTransac,SUM(Rev) AS Rev,SUM(PCOnePFive) AS PCOnePFive
FROM TMP_Orders GROUP BY CustomerNum;

UPDATE TMP_CUSTOMERS
INNER JOIN TMP_CustomerTotal ON TMP_CustomerTotal.CustomerNum = TMP_CUSTOMERS.custid
SET TMP_CUSTOMERS.NumTransac = TMP_CustomerTotal.NumTransac,
 TMP_CUSTOMERS.Rev_Total = TMP_CustomerTotal.Rev,
 TMP_CUSTOMERS.PCOnePFive_Total = TMP_CustomerTotal.PCOnePFive;

#agregare esto:  personas sin custid
INSERT INTO TMP_CUSTOMERS (source_data,email,date_registred) 
SELECT Source_data,email,date_registred FROM dev_marketing.customers_rev_co WHERE custid IS NULL;

UPDATE TMP_CUSTOMERS
SET Year_date_registred = YEAR(date_registred),Month_date_registred = MONTH(date_registred),
 date_first_order = "0000-00-00" WHERE (custid is null) and (date_registred is not null);

update TMP_CUSTOMERS set Source_data = CASE WHEN Source_data like '%natexo%' then 'Natexo'
when Source_data like '%curebit%' then 'Curebit' 
when Source_data like '%exchange%' or Source_data like '%bounce%' then 'Bounce Exchange'
when Source_data is null then 'NULL' 
ELSE Source_data END;

#fin de lo que agregue
#despues de hacer el update de arriba ya se tiene completa la tabla con cliente,source,año y mes de registro, todo de la orden 1 y totales
#esa tabla se llama TMP_CUSTOMERS
#select * from TMP_CUSTOMERS where source_data like 'Curebit%';
DROP TABLE IF EXISTS CRM_bases.LeadsCO;
CREATE TABLE CRM_bases.LeadsCO SELECT
	Year_date_registred AS AñoRegistro, Month_date_registred AS MesRegistro,
  CONCAT(Year_date_registred,"-",Month_date_registred) AS YearMonth_reg,
	source_data AS Source, COUNT(*) AS Personas, SUM(IsCustomer) AS Clientes,
	(SUM(IsCustomer) / COUNT(*))*100 AS pctConversionRate,
	sum(NumTransac) / SUM(IsCustomer) AS AvgOrdersxCustomer,
	sum(Rev_first_order)/ SUM(IsCustomer) AS AvgRevenueEnPrimeraOrden_PESOS,
	sum(Rev_Total)/ SUM(IsCustomer) AS AvgRevenueTotal_PESOS,
	sum(PCOnePFive_first_order)/ SUM(IsCustomer) AS AvgPC1_5EnPrimeraOrden_PESOS,
	sum(PCOnePFive_Total)/ SUM(IsCustomer) AS AvgPC1_5Total_PESOS,
	sum(DATEDIFF(date_first_order,date_registred))/ SUM(IsCustomer)  AS AvgDaysToFirstOrder
FROM TMP_CUSTOMERS GROUP BY Year_date_registred,Month_date_registred,source_data;

update CRM_bases.LeadsCO set AvgOrdersxCustomer = 0 where AvgOrdersxCustomer is null;
update CRM_bases.LeadsCO set AvgDaysToFirstOrder = 0 where AvgDaysToFirstOrder is null;
update CRM_bases.LeadsCO set AvgRevenueEnPrimeraOrden_PESOS = 0 where AvgRevenueEnPrimeraOrden_PESOS is null;
update CRM_bases.LeadsCO set AvgRevenueTotal_PESOS = 0 where AvgRevenueTotal_PESOS is null;
update CRM_bases.LeadsCO set AvgPC1_5EnPrimeraOrden_PESOS = 0 where AvgPC1_5EnPrimeraOrden_PESOS is null;
update CRM_bases.LeadsCO set AvgPC1_5Total_PESOS = 0 where AvgPC1_5Total_PESOS is null;
alter table CRM_bases.LeadsCO modify pctConversionRate decimal(10,2);
alter table CRM_bases.LeadsCO modify AvgOrdersxCustomer decimal(10,2);
alter table CRM_bases.LeadsCO modify AvgRevenueEnPrimeraOrden_PESOS decimal(10,2);
alter table CRM_bases.LeadsCO modify AvgRevenueTotal_PESOS decimal(10,2);
alter table CRM_bases.LeadsCO modify AvgPC1_5EnPrimeraOrden_PESOS decimal(10,2);
alter table CRM_bases.LeadsCO modify AvgPC1_5Total_PESOS decimal(10,2);
alter table CRM_bases.LeadsCO modify YearMonth_reg varchar(10);
ALTER TABLE CRM_bases.LeadsCO DROP AñoRegistro;
ALTER TABLE CRM_bases.LeadsCO DROP MesRegistro;
update CRM_bases.LeadsCO set Source = if(Source like 'Bob_newsletter%',if(Source = 'Bob_newsletter','Bob_newsletter',right(Source,length(Source)-15)),Source);
#SELECT * FROM CRM_bases.LeadsCO;
select CURDATE();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`roberto.lopez`@`%`*/ /*!50003 PROCEDURE `RutinaLeadsMX`()
BEGIN

DROP TEMPORARY TABLE IF EXISTS TMP_Orders;
CREATE TEMPORARY TABLE TMP_Orders ( PRIMARY KEY (OrderNum),INDEX (IdSalesOrder)) 
SELECT IdSalesOrder, OrderNum, CustomerNum, date, Time, count(*) AS Items, SUM(Rev) AS Rev, SUM(PCOnePFive) AS PCOnePFive
FROM development_mx.A_Master WHERE OrderAfterCan = 1 GROUP BY OrderNum;

#select * from TMP_Orders;   #TIENE TODAS LAS ORDENES QUE SE HAN HECHO. EL CLIENTE, FECHA, CUANTOS ITEMS, SUMA REV Y PC1.5
#1 RENGLON POR ORDEN.
DROP TEMPORARY TABLE IF EXISTS TMP_CustomerOrders;
CREATE TEMPORARY TABLE TMP_CustomerOrders (
	id INT auto_increment, PRIMARY KEY (CustomerNum, id), INDEX (IdSalesOrder) ) 
SELECT IdSalesOrder,OrderNum,CustomerNum,date,Time,NULL AS id,Items,Rev,PCOnePFive
FROM TMP_Orders ORDER BY CustomerNum,Date,Time;

#SELECT * from TMP_CustomerOrders;  #1 RENGLON POR ORDEN PERO ESTA ORDENADO POR CLIENTE. LA variable id numera las ordenes por cliente.
#ejemplo 1,2,3,1,2,1,1,2,3,4,5,6,1,


DROP TEMPORARY TABLE IF EXISTS TMP_CUSTOMERS;
CREATE TEMPORARY TABLE TMP_CUSTOMERS (PRIMARY KEY(email)) 
SELECT custid, YEAR (date_registred) AS Year_date_registred, MONTH (date_registred) AS Month_date_registred,
	source_data, email,
IF(SUM(IF(TYPE_CUSTOMER = "customer",1,0))>0,1,0) AS IsCustomer,
 0 AS NumTransac,
 date_registred,
 "0000-00-00" AS date_first_order,
 00000000000.00 AS Rev_first_order, 
 00000000000.00 AS PCOnePFive_first_order,
 00000000000.00 AS Rev_Total,
 00000000000.00 AS PCOnePFive_Total FROM dev_marketing.customers_rev_mx WHERE custid IS NOT NULL GROUP BY custid;

#TMP_CUSTOMERS sera una tabla con un renglon por cliente. incluye su source,año y mes de registro, si es cust o NO,fecha,rev,pc1.5 de la primera orden
#tambien revenue total de esa persona y pc1.5 total (suma)
#aqui abajo actualiza la info. de la primer compra por cliente.   despues lo hara para el rev total y pc1.5 total
UPDATE TMP_CUSTOMERS
INNER JOIN TMP_CustomerOrders ON TMP_CustomerOrders.CustomerNum = TMP_CUSTOMERS.custid
AND TMP_CustomerOrders.id = 1
SET TMP_CUSTOMERS.date_first_order = TMP_CustomerOrders.Date,
 TMP_CUSTOMERS.Rev_first_order = TMP_CustomerOrders.Rev,
 TMP_CUSTOMERS.PCOnePFive_first_order = TMP_CustomerOrders.PCOnePFive;

DROP TEMPORARY TABLE IF EXISTS TMP_CustomerTotal;
CREATE TEMPORARY TABLE TMP_CustomerTotal (PRIMARY KEY(CustomerNum)) 
SELECT CustomerNum, COUNT(DISTINCT OrderNum) AS NumTransac, SUM(Rev) AS Rev, SUM(PCOnePFive) AS PCOnePFive
FROM TMP_Orders GROUP BY CustomerNum;

UPDATE TMP_CUSTOMERS
INNER JOIN TMP_CustomerTotal ON TMP_CustomerTotal.CustomerNum = TMP_CUSTOMERS.custid
SET TMP_CUSTOMERS.NumTransac = TMP_CustomerTotal.NumTransac,
 TMP_CUSTOMERS.Rev_Total = TMP_CustomerTotal.Rev,
 TMP_CUSTOMERS.PCOnePFive_Total = TMP_CustomerTotal.PCOnePFive;

#agregare esto:  personas sin custid
INSERT INTO TMP_CUSTOMERS ( source_data, email, date_registred) 
SELECT Source_data,email,date_registred FROM dev_marketing.customers_rev_mx WHERE custid IS NULL;

UPDATE TMP_CUSTOMERS
SET Year_date_registred = YEAR(date_registred),Month_date_registred = MONTH(date_registred),
 date_first_order = "0000-00-00" WHERE (custid is null) and (date_registred is not null);

#SELECT * FROM	TMP_CUSTOMERS;
update TMP_CUSTOMERS set Source_data = CASE WHEN Source_data like '%natexo%' then 'Natexo'
when Source_data like '%curebit%' then 'Curebit' 
when Source_data like '%BX%' or Source_data like '%exchange%' or Source_data like '%bounce%' then 'Bounce Exchange'
when Source_data is null then 'NULL' 
ELSE Source_data END;

DROP TABLE IF EXISTS CRM_bases.LeadsMX;
CREATE TABLE CRM_bases.LeadsMX SELECT
	Year_date_registred AS AñoRegistro, Month_date_registred AS MesRegistro,
  CONCAT(Year_date_registred,"-",Month_date_registred) AS YearMonth_reg,
	source_data AS Source, COUNT(*) AS Personas, SUM(IsCustomer) AS Clientes,
  (SUM(IsCustomer) / COUNT(*))*100 AS pctConversionRate, 
	sum(NumTransac) / SUM(IsCustomer) AS AvgOrdersxCustomer,
	sum(Rev_first_order)/ SUM(IsCustomer) AS AvgRevenueEnPrimeraOrden_PESOS,
	sum(Rev_Total)/ SUM(IsCustomer) AS AvgRevenueTotal_PESOS,
	sum(PCOnePFive_first_order)/ SUM(IsCustomer) AS AvgPC1_5EnPrimeraOrden_PESOS,
	sum(PCOnePFive_Total)/ SUM(IsCustomer) AS AvgPC1_5Total_PESOS,
	sum( DATEDIFF(date_first_order,date_registred)) / SUM(IsCustomer) AS AvgDaysToFirstOrder
FROM TMP_CUSTOMERS GROUP BY Year_date_registred,Month_date_registred,source_data;

update CRM_bases.LeadsMX set AvgOrdersxCustomer = 0 where AvgOrdersxCustomer is null;
update CRM_bases.LeadsMX set AvgDaysToFirstOrder = 0 where AvgDaysToFirstOrder is null;
update CRM_bases.LeadsMX set AvgRevenueEnPrimeraOrden_PESOS = 0 where AvgRevenueEnPrimeraOrden_PESOS is null;
update CRM_bases.LeadsMX set AvgRevenueTotal_PESOS = 0 where AvgRevenueTotal_PESOS is null;
update CRM_bases.LeadsMX set AvgPC1_5EnPrimeraOrden_PESOS = 0 where AvgPC1_5EnPrimeraOrden_PESOS is null;
update CRM_bases.LeadsMX set AvgPC1_5Total_PESOS = 0 where AvgPC1_5Total_PESOS is null;
alter table CRM_bases.LeadsMX modify pctConversionRate decimal(10,2);
alter table CRM_bases.LeadsMX modify AvgOrdersxCustomer decimal(10,2);
alter table CRM_bases.LeadsMX modify AvgRevenueEnPrimeraOrden_PESOS decimal(10,2);
alter table CRM_bases.LeadsMX modify AvgRevenueTotal_PESOS decimal(10,2);
alter table CRM_bases.LeadsMX modify AvgPC1_5EnPrimeraOrden_PESOS decimal(10,2);
alter table CRM_bases.LeadsMX modify AvgPC1_5Total_PESOS decimal(10,2);
alter table CRM_bases.LeadsMX modify YearMonth_reg varchar(10);
ALTER TABLE CRM_bases.LeadsMX DROP AñoRegistro;
ALTER TABLE CRM_bases.LeadsMX DROP MesRegistro;
update CRM_bases.LeadsMX set Source = if(Source like 'Bob_newsletter%',if(Source = 'Bob_newsletter','Bob_newsletter',right(Source,length(Source)-15)),Source);
#falta corregir el days to first order
#SELECT * FROM CRM_bases.LeadsMX;
select CURDATE();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`roberto.lopez`@`%`*/ /*!50003 PROCEDURE `RutinaLeadsPE`()
BEGIN
#Lead generation report para Peru
DROP TEMPORARY TABLE IF EXISTS TMP_Orders;
CREATE TEMPORARY TABLE TMP_Orders ( PRIMARY KEY (OrderNum),INDEX (IdSalesOrder)) 
SELECT IdSalesOrder, OrderNum, CustomerNum, date, Time, count(*) AS Items, SUM(Rev) AS Rev, SUM(PCOnePFive) AS PCOnePFive
FROM development_pe.A_Master WHERE OrderAfterCan = 1 GROUP BY OrderNum;

#select * from TMP_Orders;   #TIENE TODAS LAS ORDENES QUE SE HAN HECHO. EL CLIENTE, FECHA, CUANTOS ITEMS, SUMA REV Y PC1.5
#1 RENGLON POR ORDEN.
DROP TEMPORARY TABLE IF EXISTS TMP_CustomerOrders;
CREATE TEMPORARY TABLE TMP_CustomerOrders (
	id INT auto_increment, PRIMARY KEY (CustomerNum, id), INDEX (IdSalesOrder) ) 
SELECT IdSalesOrder,OrderNum,CustomerNum,date,Time,NULL AS id,Items,Rev,PCOnePFive
FROM TMP_Orders ORDER BY CustomerNum,Date,Time;

#SELECT * from TMP_CustomerOrders;  #1 RENGLON POR ORDEN PERO ESTA ORDENADO POR CLIENTE. LA variable id numera las ordenes por cliente.
#ejemplo 1,2,3,1,2,1,1,2,3,4,5,6,1,

DROP TEMPORARY TABLE IF EXISTS TMP_CUSTOMERS;
CREATE TEMPORARY TABLE TMP_CUSTOMERS (PRIMARY KEY(email)) 
SELECT custid, YEAR (date_registred) AS Year_date_registred, MONTH (date_registred) AS Month_date_registred,
	source_data, email,
IF(SUM(IF(TYPE_CUSTOMER = "customer",1,0))>0,1,0) AS IsCustomer,
 0 AS NumTransac,
 date_registred,
 "0000-00-00" AS date_first_order,
 00000000000.00 AS Rev_first_order,
 00000000000.00 AS PCOnePFive_first_order,
 00000000000.00 AS Rev_Total,
 00000000000.00 AS PCOnePFive_Total
FROM
	dev_marketing.customers_rev_pe
WHERE
	custid IS NOT NULL
GROUP BY
	custid;

#TMP_CUSTOMERS sera una tabla con un renglon por cliente. incluye su source,año y mes de registro, si es cust o NO,fecha,rev,pc1.5 de la primera orden
#tambien revenue total de esa persona y pc1.5 total (suma)
#aqui abajo actualiza la info. de la primer compra por cliente.   despues lo hara para el rev total y pc1.5 total
UPDATE TMP_CUSTOMERS
INNER JOIN TMP_CustomerOrders ON TMP_CustomerOrders.CustomerNum = TMP_CUSTOMERS.custid
AND TMP_CustomerOrders.id = 1
SET TMP_CUSTOMERS.date_first_order = TMP_CustomerOrders.Date,
 TMP_CUSTOMERS.Rev_first_order = TMP_CustomerOrders.Rev,
 TMP_CUSTOMERS.PCOnePFive_first_order = TMP_CustomerOrders.PCOnePFive;

DROP TEMPORARY TABLE
IF EXISTS TMP_CustomerTotal;

CREATE TEMPORARY TABLE TMP_CustomerTotal (PRIMARY KEY(CustomerNum)) SELECT
	CustomerNum,
	COUNT(DISTINCT OrderNum) AS NumTransac,
	SUM(Rev) AS Rev,
	SUM(PCOnePFive) AS PCOnePFive
FROM
	TMP_Orders
GROUP BY
	CustomerNum;

UPDATE TMP_CUSTOMERS
INNER JOIN TMP_CustomerTotal ON TMP_CustomerTotal.CustomerNum = TMP_CUSTOMERS.custid
SET TMP_CUSTOMERS.NumTransac = TMP_CustomerTotal.NumTransac,
 TMP_CUSTOMERS.Rev_Total = TMP_CustomerTotal.Rev,
 TMP_CUSTOMERS.PCOnePFive_Total = TMP_CustomerTotal.PCOnePFive;

#agregare esto:  personas sin custid
INSERT INTO TMP_CUSTOMERS (
	source_data,
	email,
	date_registred
) SELECT
	Source_data,
	email,
	date_registred
FROM
	dev_marketing.customers_rev_pe
WHERE
	custid IS NULL;

UPDATE TMP_CUSTOMERS
SET Year_date_registred = YEAR(date_registred),Month_date_registred = MONTH(date_registred),
 date_first_order = "0000-00-00"
WHERE (custid is null) and (date_registred is not null);

update TMP_CUSTOMERS set Source_data = CASE WHEN Source_data like '%natexo%' then 'Natexo'
when Source_data like '%curebit%' then 'Curebit' 
when Source_data like '%exchange%' or Source_data like '%bounce%' then 'Bounce Exchange'
when Source_data is null then 'NULL' 
ELSE Source_data END;

DROP TABLE IF EXISTS CRM_bases.LeadsPE;
CREATE TABLE CRM_bases.LeadsPE SELECT
	Year_date_registred AS AñoRegistro, Month_date_registred AS MesRegistro,
  CONCAT(Year_date_registred,"-",Month_date_registred) AS YearMonth_reg,
	source_data AS Source, COUNT(*) AS Personas, SUM(IsCustomer) AS Clientes,
	(SUM(IsCustomer) / COUNT(*))*100 AS pctConversionRate,
	sum(NumTransac) / SUM(IsCustomer) AS AvgOrdersxCustomer,
	SUM(Rev_first_order)/ SUM(IsCustomer) AS AvgRevenueEnPrimeraOrden_SOLES,
	sum(Rev_Total)/ SUM(IsCustomer) AS AvgRevenueTotal_SOLES,
	sum(PCOnePFive_first_order)/ SUM(IsCustomer) AS AvgPC1_5EnPrimeraOrden_SOLES,
	sum(PCOnePFive_Total)/ SUM(IsCustomer) AS AvgPC1_5Total_SOLES,
	sum(DATEDIFF(date_first_order,date_registred))/ SUM(IsCustomer) AS AvgDaysToFirstOrder
FROM TMP_CUSTOMERS GROUP BY Year_date_registred,Month_date_registred,source_data;

update CRM_bases.LeadsPE set AvgOrdersxCustomer = 0 where AvgOrdersxCustomer is null;
update CRM_bases.LeadsPE set AvgDaysToFirstOrder = 0 where AvgDaysToFirstOrder is null;
update CRM_bases.LeadsPE set AvgRevenueEnPrimeraOrden_SOLES = 0 where AvgRevenueEnPrimeraOrden_SOLES is null;
update CRM_bases.LeadsPE set AvgRevenueTotal_SOLES = 0 where AvgRevenueTotal_SOLES is null;
update CRM_bases.LeadsPE set AvgPC1_5EnPrimeraOrden_SOLES = 0 where AvgPC1_5EnPrimeraOrden_SOLES is null;
update CRM_bases.LeadsPE set AvgPC1_5Total_SOLES = 0 where AvgPC1_5Total_SOLES is null;
alter table CRM_bases.LeadsPE modify pctConversionRate decimal(10,2);
alter table CRM_bases.LeadsPE modify AvgOrdersxCustomer decimal(10,2);
alter table CRM_bases.LeadsPE modify AvgRevenueEnPrimeraOrden_SOLES decimal(10,2);
alter table CRM_bases.LeadsPE modify AvgRevenueTotal_SOLES decimal(10,2);
alter table CRM_bases.LeadsPE modify AvgPC1_5EnPrimeraOrden_SOLES decimal(10,2);
alter table CRM_bases.LeadsPE modify AvgPC1_5Total_SOLES decimal(10,2);
alter table CRM_bases.LeadsPE modify YearMonth_reg varchar(10);
ALTER TABLE CRM_bases.LeadsPE DROP AñoRegistro;
ALTER TABLE CRM_bases.LeadsPE DROP MesRegistro;
update CRM_bases.LeadsPE set Source = if(Source like 'Bob_newsletter%',if(Source = 'Bob Newsletter','Bob_newsletter',right(Source,length(Source)-15)),Source);
#FALTA CORREGIR SOURCES. SOLO 4 TIENEN CLIENTES.
select CURDATE();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`roberto.lopez`@`%`*/ /*!50003 PROCEDURE `RutinaLeadsVE`()
BEGIN
#Lead generation report para Venezuela
DROP TEMPORARY TABLE IF EXISTS TMP_Orders;
CREATE TEMPORARY TABLE TMP_Orders ( PRIMARY KEY (OrderNum),INDEX (IdSalesOrder)) 
SELECT
	IdSalesOrder, OrderNum, CustomerNum, date, Time, count(*) AS Items, SUM(Rev) AS Rev, SUM(PCOnePFive) AS PCOnePFive
FROM development_ve.A_Master WHERE OrderAfterCan = 1 GROUP BY OrderNum;

#select * from TMP_Orders;   #TIENE TODAS LAS ORDENES QUE SE HAN HECHO. EL CLIENTE, FECHA, CUANTOS ITEMS, SUMA REV Y PC1.5
#1 RENGLON POR ORDEN.
DROP TEMPORARY TABLE IF EXISTS TMP_CustomerOrders;
CREATE TEMPORARY TABLE TMP_CustomerOrders (
	id INT auto_increment, PRIMARY KEY (CustomerNum, id), INDEX (IdSalesOrder) ) 
SELECT IdSalesOrder,OrderNum,CustomerNum,date,Time,NULL AS id,Items,Rev,PCOnePFive
FROM TMP_Orders ORDER BY CustomerNum,Date,Time;

#SELECT * from TMP_CustomerOrders;  #1 RENGLON POR ORDEN PERO ESTA ORDENADO POR CLIENTE. LA variable id numera las ordenes por cliente.
#ejemplo 1,2,3,1,2,1,1,2,3,4,5,6,1,

DROP TEMPORARY TABLE IF EXISTS TMP_CUSTOMERS;
CREATE TEMPORARY TABLE TMP_CUSTOMERS (PRIMARY KEY(email)) 
SELECT custid, YEAR (date_registred) AS Year_date_registred, MONTH (date_registred) AS Month_date_registred,
	source_data, email,
IF(SUM(IF(TYPE_CUSTOMER = "customer",1,0))>0,1,0) AS IsCustomer,
 0 AS NumTransac, date_registred,
 "0000-00-00" AS date_first_order,
 00000000000.00 AS Rev_first_order,
 00000000000.00 AS PCOnePFive_first_order,
 00000000000.00 AS Rev_Total,
 00000000000.00 AS PCOnePFive_Total
FROM dev_marketing.customers_rev_ve WHERE custid IS NOT NULL GROUP BY custid;

#TMP_CUSTOMERS sera una tabla con un renglon por cliente. incluye su source,año y mes de registro, si es cust o NO,fecha,rev,pc1.5 de la primera orden
#tambien revenue total de esa persona y pc1.5 total (suma)
#aqui abajo actualiza la info. de la primer compra por cliente.   despues lo hara para el rev total y pc1.5 total
UPDATE TMP_CUSTOMERS
INNER JOIN TMP_CustomerOrders ON TMP_CustomerOrders.CustomerNum = TMP_CUSTOMERS.custid
AND TMP_CustomerOrders.id = 1
SET TMP_CUSTOMERS.date_first_order = TMP_CustomerOrders.Date,
 TMP_CUSTOMERS.Rev_first_order = TMP_CustomerOrders.Rev,
 TMP_CUSTOMERS.PCOnePFive_first_order = TMP_CustomerOrders.PCOnePFive;

DROP TEMPORARY TABLE IF EXISTS TMP_CustomerTotal;
CREATE TEMPORARY TABLE TMP_CustomerTotal (PRIMARY KEY(CustomerNum)) SELECT
	CustomerNum,
	COUNT(DISTINCT OrderNum) AS NumTransac,
	SUM(Rev) AS Rev,
	SUM(PCOnePFive) AS PCOnePFive
FROM TMP_Orders GROUP BY CustomerNum;

UPDATE TMP_CUSTOMERS
INNER JOIN TMP_CustomerTotal ON TMP_CustomerTotal.CustomerNum = TMP_CUSTOMERS.custid
SET TMP_CUSTOMERS.NumTransac = TMP_CustomerTotal.NumTransac,
 TMP_CUSTOMERS.Rev_Total = TMP_CustomerTotal.Rev,
 TMP_CUSTOMERS.PCOnePFive_Total = TMP_CustomerTotal.PCOnePFive;

#agregare esto:  personas sin custid
INSERT INTO TMP_CUSTOMERS (source_data, email, date_registred) 
SELECT Source_data,email,date_registred FROM dev_marketing.customers_rev_ve WHERE custid IS NULL;

UPDATE TMP_CUSTOMERS
SET Year_date_registred = YEAR(date_registred),Month_date_registred = MONTH(date_registred),
 date_first_order = "0000-00-00" WHERE (custid is null) and (date_registred is not null);

update TMP_CUSTOMERS set Source_data = CASE WHEN Source_data like '%natexo%' then 'Natexo'
when Source_data like '%curebit%' then 'Curebit' 
when Source_data like '%exchange%' or Source_data like '%bounce%' then 'Bounce Exchange'
when Source_data is null then 'NULL' 
ELSE Source_data END;

#fin de lo que agregue
#despues de hacer el update de arriba ya se tiene completa la tabla con cliente,source,año y mes de registro, todo de la orden 1 y totales
#esa tabla se llama TMP_CUSTOMERS

DROP TABLE IF EXISTS CRM_bases.LeadsVE;
CREATE TABLE CRM_bases.LeadsVE SELECT
	Year_date_registred AS AñoRegistro, Month_date_registred AS MesRegistro,
  CONCAT(Year_date_registred,"-",Month_date_registred) AS YearMonth_reg,
	source_data AS Source,
	COUNT(*) AS Personas,
	SUM(IsCustomer) AS Clientes,
	(SUM(IsCustomer) / COUNT(*))*100 AS pctConversionRate,
	sum(NumTransac) / SUM(IsCustomer) AS AvgOrdersxCustomer,
	SUM(Rev_first_order)/ SUM(IsCustomer) AS AvgRevenueEnPrimeraOrden_Bolivares,
	sum(Rev_Total)/ SUM(IsCustomer) AS AvgRevenueTotal_Bolivares,
	sum(PCOnePFive_first_order)/ SUM(IsCustomer) AS AvgPC1_5EnPrimeraOrden_Bolivares,
	sum(PCOnePFive_Total)/ SUM(IsCustomer) AS AvgPC1_5Total_Bolivares,
	sum(DATEDIFF(date_first_order,date_registred))/ SUM(IsCustomer) AS AvgDaysToFirstOrder
FROM TMP_CUSTOMERS GROUP BY Year_date_registred,Month_date_registred,source_data;

update CRM_bases.LeadsVE set AvgOrdersxCustomer = 0 where AvgOrdersxCustomer is null;
update CRM_bases.LeadsVE set AvgDaysToFirstOrder = 0 where AvgDaysToFirstOrder is null;
update CRM_bases.LeadsVE set AvgRevenueEnPrimeraOrden_Bolivares = 0 WHERE AvgRevenueEnPrimeraOrden_Bolivares IS NULL;
update CRM_bases.LeadsVE set AvgRevenueTotal_Bolivares = 0 WHERE AvgRevenueTotal_Bolivares IS NULL;
update CRM_bases.LeadsVE set AvgPC1_5EnPrimeraOrden_Bolivares = 0 WHERE AvgPC1_5EnPrimeraOrden_Bolivares IS NULL;
update CRM_bases.LeadsVE set AvgPC1_5Total_Bolivares = 0 WHERE AvgPC1_5Total_Bolivares IS NULL;

alter table CRM_bases.LeadsVE modify pctConversionRate decimal(10,2);
alter table CRM_bases.LeadsVE modify AvgOrdersxCustomer decimal(10,2);
alter table CRM_bases.LeadsVE modify AvgRevenueEnPrimeraOrden_Bolivares decimal(10,2);
alter table CRM_bases.LeadsVE modify AvgRevenueTotal_Bolivares decimal(10,2);
alter table CRM_bases.LeadsVE modify AvgPC1_5EnPrimeraOrden_Bolivares decimal(10,2);
alter table CRM_bases.LeadsVE modify AvgPC1_5Total_Bolivares decimal(10,2);
alter table CRM_bases.LeadsVE modify YearMonth_reg varchar(10);
ALTER TABLE CRM_bases.LeadsVE DROP AñoRegistro;
ALTER TABLE CRM_bases.LeadsVE DROP MesRegistro;
update CRM_bases.LeadsVE set Source = if(Source like 'Bob_newsletter%',if(Source = 'Bob Newsletter','Bob_newsletter',right(Source,length(Source)-15)),Source);

select CURDATE();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`borja.fernandez`@`%`*/ /*!50003 PROCEDURE `Rutina_Create_Bases_Fashion_CO_y_PE`()
BEGIN
#################CREACION TABLA BASE DE FASHION COLOMBIA###########################################33

drop table if EXISTS CRM_bases.SuscritosFashionCO;
CREATE table CRM_bases.SuscritosFashionCO
select 	distinct(a.email) as email,
				Type_Customer as Type_Customer,
				Source_data as source,
				b.gender,
				first_name,
				birthday,
				new_registry
					from bob_live_co.newsletter_subscription a
inner join dev_marketing.customers_rev_co b on b.email=a.email
						where fk_catalog_store_id=2
						and created_at>'2013-04-01';

drop table if EXISTS CRM_bases.CompradoresFashionCO;
CREATE table CRM_bases.CompradoresFashionCO
select 	distinct(CustomerEmail) as email,
				Type_Customer,
				Source_data as source,
				d.gender,
				first_name,
				birthday,
				new_registry
		from development_co_project.A_Master c
inner join dev_marketing.customers_rev_co d on d.email=c.CustomerEmail
			where cat1='fashion';

drop table if EXISTS CRM_bases.BaseFashionCO;
CREATE table CRM_bases.BaseFashionCO
select * from CRM_bases.SuscritosFashionCO
UNION ALL
select * from CRM_bases.CompradoresFashionCO;

###########################################################################

#################CREACION TABLA BASE DE FASHION PERU###########################################33

drop table if EXISTS CRM_bases.SuscritosFashionPE;
CREATE table CRM_bases.SuscritosFashionPE
select 	distinct(a.email) as email,
				Type_Customer as Type_Customer,
				Source_data as source,
				b.gender,
				first_name,
				birthday,
				new_registry
					from bob_live_pe.newsletter_subscription a
inner join dev_marketing.customers_rev_pe b on b.email=a.email
						where fk_catalog_store_id=2
						and created_at>'2013-04-01';

drop table if EXISTS CRM_bases.CompradoresFashionPE;
CREATE table CRM_bases.CompradoresFashionPE
select 	distinct(CustomerEmail) as email,
				Type_Customer,
				Source_data as source,
				d.gender,
				first_name,
				birthday,
				new_registry
		from development_pe.A_Master c
inner join dev_marketing.customers_rev_pe d on d.email=c.CustomerEmail
			where cat1='fashion';

drop table if EXISTS CRM_bases.BaseFashionPE;
CREATE table CRM_bases.BaseFashionPE
select * from CRM_bases.SuscritosFashionPE
UNION ALL
select * from CRM_bases.CompradoresFashionPE;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`borja.fernandez`@`%`*/ /*!50003 PROCEDURE `Rutina_Create_CampaignUMSsummary`()
BEGIN

-- --------------------------------------------------------------------------------
-- Routine: Campaigns
-- Country: CO
-- Version: 1
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
-- Campaign Sents
-- --------------------------------------------------------------------------------

DROP TABLE IF EXISTS CRM_bases.CO_CampaignSents;
CREATE TABLE CRM_bases.CO_CampaignSents SELECT 'Colombia' AS Country,
    DATE(`Dispatch-End`) AS 'Day',
    `Mailing-ID` AS MailingID,
    `Mailing-Name` AS Campaign,
    CASE
        WHEN (coms.`Campaign-ID` = '83538') THEN 'MIAC'
        WHEN (coms.`Campaign-ID` = '97991') THEN 'MIAC_REM24H'
        WHEN (coms.`Campaign-ID` = '83539') THEN 'MISH'
        WHEN (coms.`Campaign-ID` = '87443') THEN 'MIPR'
        WHEN (coms.`Campaign-ID` = '91463') THEN 'MIPR_REC'
        WHEN (coms.`Campaign-ID` = '91084') THEN 'MIRR_REC'
        WHEN (coms.`Campaign-ID` = '91085') THEN 'MIRAO'
        WHEN
            (coms.`Campaign-ID` = '104619'
                OR coms.`Campaign-ID` = '104620')
        THEN
            'MIVAO'
        WHEN (coms.`Campaign-ID` = '107135') THEN 'MIVAO_REM'
        WHEN (coms.`Campaign-ID` = '104621') THEN 'BH_T'
        WHEN (coms.`Campaign-ID` = '104793') THEN 'BH_CU'
        WHEN
            (coms.`Campaign-ID` = '105025'
                OR coms.`Campaign-ID` = '105026'
                OR coms.`Campaign-ID` = '105027'
                OR coms.`Campaign-ID` = '105028'
                OR coms.`Campaign-ID` = '105029'
                OR coms.`Campaign-ID` = '105030'
                OR coms.`Campaign-ID` = '106697')
        THEN
            'MIRPP_AT'
        WHEN (coms.`Campaign-ID` = '106999') THEN 'MIRPP_FRA'
        WHEN (coms.`Campaign-ID` = '106481') THEN 'MICSLL'
        WHEN (coms.`Campaign-ID` = '106649') THEN 'MIROOS'
        WHEN (coms.`Campaign-ID` = '106694') THEN 'MIRPOP'
        WHEN
            (coms.`Campaign-ID` = '106653'
                OR coms.`Campaign-ID` = '106654')
        THEN
            'CSS (NPS)'
        WHEN
            (coms.`Campaign-ID` = '106897'
                OR coms.`Campaign-ID` = '106898')
        THEN
            'CSS_REM (NPS)'
        ELSE 'Other'
    END AS campaign_nick_name,
    COUNT(DISTINCT UniqueKey) AS NumSent FROM
    CRM_CO.campaing_messages AS coms
WHERE
    YEAR(`Dispatch-End`) = 2014
GROUP BY `Mailing-ID`;

CREATE INDEX campaign ON CRM_bases.CO_CampaignSents(campaign);

-- --------------------------------------------------------------------------------
-- Campaign Bounces
-- --------------------------------------------------------------------------------

DROP TABLE IF EXISTS CRM_bases.CO_CampaignBounces;
CREATE TABLE CRM_bases.CO_CampaignBounces SELECT 'Colombia' AS Country,
    DATE(`Dispatch-End`) AS 'Day',
    `Mailing-ID` AS MailingID,
    `Mailing-Name` AS Campaign,
    CASE
        WHEN (cobs.`Campaign-ID` = '83538') THEN 'MIAC'
        WHEN (cobs.`Campaign-ID` = '97991') THEN 'MIAC_REM24H'
        WHEN (cobs.`Campaign-ID` = '83539') THEN 'MISH'
        WHEN (cobs.`Campaign-ID` = '87443') THEN 'MIPR'
        WHEN (cobs.`Campaign-ID` = '91463') THEN 'MIPR_REC'
        WHEN (cobs.`Campaign-ID` = '91084') THEN 'MIRR_REC'
        WHEN (cobs.`Campaign-ID` = '91085') THEN 'MIRAO'
        WHEN
            (cobs.`Campaign-ID` = '104619'
                OR cobs.`Campaign-ID` = '104620')
        THEN
            'MIVAO'
        WHEN (cobs.`Campaign-ID` = '107135') THEN 'MIVAO_REM'
        WHEN (cobs.`Campaign-ID` = '104621') THEN 'BH_T'
        WHEN (cobs.`Campaign-ID` = '104793') THEN 'BH_CU'
        WHEN
            (cobs.`Campaign-ID` = '105025'
                OR cobs.`Campaign-ID` = '105026'
                OR cobs.`Campaign-ID` = '105027'
                OR cobs.`Campaign-ID` = '105028'
                OR cobs.`Campaign-ID` = '105029'
                OR cobs.`Campaign-ID` = '105030'
                OR cobs.`Campaign-ID` = '106697')
        THEN
            'MIRPP_AT'
        WHEN (cobs.`Campaign-ID` = '106999') THEN 'MIRPP_FRA'
        WHEN (cobs.`Campaign-ID` = '106481') THEN 'MICSLL'
        WHEN (cobs.`Campaign-ID` = '106649') THEN 'MIROOS'
        WHEN (cobs.`Campaign-ID` = '106694') THEN 'MIRPOP'
        WHEN
            (cobs.`Campaign-ID` = '106653'
                OR cobs.`Campaign-ID` = '106654')
        THEN
            'CSS (NPS)'
        WHEN
            (cobs.`Campaign-ID` = '106897'
                OR cobs.`Campaign-ID` = '106898')
        THEN
            'CSS_REM (NPS)'
        ELSE 'Other'
    END AS campaign_nick_name,
    COUNT(DISTINCT UniqueKey) AS TotalBounces FROM
    CRM_CO.campaing_bounces AS cobs
WHERE
    YEAR(`Dispatch-End`) = 2014
GROUP BY `Mailing-ID`;

CREATE INDEX campaign ON CRM_bases.CO_CampaignBounces(campaign);

-- --------------------------------------------------------------------------------
-- Campaign Opens
-- --------------------------------------------------------------------------------

DROP TABLE IF EXISTS CRM_bases.CO_CampaignOpens;
CREATE TABLE CRM_bases.CO_CampaignOpens SELECT 'Colombia' AS Country,
    DATE(`Dispatch-End`) AS 'Day',
    `Mailing-ID` AS MailingID,
    `Mailing-Name` AS Campaign,
    CASE
        WHEN (coop.`Campaign-ID` = '83538') THEN 'MIAC'
        WHEN (coop.`Campaign-ID` = '97991') THEN 'MIAC_REM24H'
        WHEN (coop.`Campaign-ID` = '83539') THEN 'MISH'
        WHEN (coop.`Campaign-ID` = '87443') THEN 'MIPR'
        WHEN (coop.`Campaign-ID` = '91463') THEN 'MIPR_REC'
        WHEN (coop.`Campaign-ID` = '91084') THEN 'MIRR_REC'
        WHEN (coop.`Campaign-ID` = '91085') THEN 'MIRAO'
        WHEN
            (coop.`Campaign-ID` = '104619'
                OR coop.`Campaign-ID` = '104620')
        THEN
            'MIVAO'
        WHEN (coop.`Campaign-ID` = '107135') THEN 'MIVAO_REM'
        WHEN (coop.`Campaign-ID` = '104621') THEN 'BH_T'
        WHEN (coop.`Campaign-ID` = '104793') THEN 'BH_CU'
        WHEN
            (coop.`Campaign-ID` = '105025'
                OR coop.`Campaign-ID` = '105026'
                OR coop.`Campaign-ID` = '105027'
                OR coop.`Campaign-ID` = '105028'
                OR coop.`Campaign-ID` = '105029'
                OR coop.`Campaign-ID` = '105030'
                OR coop.`Campaign-ID` = '106697')
        THEN
            'MIRPP_AT'
        WHEN (coop.`Campaign-ID` = '106999') THEN 'MIRPP_FRA'
        WHEN (coop.`Campaign-ID` = '106481') THEN 'MICSLL'
        WHEN (coop.`Campaign-ID` = '106649') THEN 'MIROOS'
        WHEN (coop.`Campaign-ID` = '106694') THEN 'MIRPOP'
        WHEN
            (coop.`Campaign-ID` = '106653'
                OR coop.`Campaign-ID` = '106654')
        THEN
            'CSS (NPS)'
        WHEN
            (coop.`Campaign-ID` = '106897'
                OR coop.`Campaign-ID` = '106898')
        THEN
            'CSS_REM (NPS)'
        ELSE 'Other'
    END AS campaign_nick_name,
    COUNT(DISTINCT UniqueKey) AS NumOpens FROM
    CRM_CO.campaing_openings AS coop
WHERE
    YEAR(`Dispatch-End`) = 2014
GROUP BY `Mailing-ID`;

CREATE INDEX campaign ON CRM_bases.CO_CampaignOpens(campaign);

-- --------------------------------------------------------------------------------
-- Campaign Clicks
-- --------------------------------------------------------------------------------

DROP TABLE IF EXISTS CRM_bases.CO_CampaignClicks;
CREATE TABLE CRM_bases.CO_CampaignClicks SELECT 'Colombia' AS Country,
    DATE(`Dispatch-End`) AS 'Day',
    `Mailing-ID` AS MailingID,
    `Mailing-Name` AS Campaign,
    CASE
        WHEN (cocl.`Campaign-ID` = '83538') THEN 'MIAC'
        WHEN (cocl.`Campaign-ID` = '97991') THEN 'MIAC_REM24H'
        WHEN (cocl.`Campaign-ID` = '83539') THEN 'MISH'
        WHEN (cocl.`Campaign-ID` = '87443') THEN 'MIPR'
        WHEN (cocl.`Campaign-ID` = '91463') THEN 'MIPR_REC'
        WHEN (cocl.`Campaign-ID` = '91084') THEN 'MIRR_REC'
        WHEN (cocl.`Campaign-ID` = '91085') THEN 'MIRAO'
        WHEN
            (cocl.`Campaign-ID` = '104619'
                OR cocl.`Campaign-ID` = '104620')
        THEN
            'MIVAO'
        WHEN (cocl.`Campaign-ID` = '107135') THEN 'MIVAO_REM'
        WHEN (cocl.`Campaign-ID` = '104621') THEN 'BH_T'
        WHEN (cocl.`Campaign-ID` = '104793') THEN 'BH_CU'
        WHEN
            (cocl.`Campaign-ID` = '105025'
                OR cocl.`Campaign-ID` = '105026'
                OR cocl.`Campaign-ID` = '105027'
                OR cocl.`Campaign-ID` = '105028'
                OR cocl.`Campaign-ID` = '105029'
                OR cocl.`Campaign-ID` = '105030'
                OR cocl.`Campaign-ID` = '106697')
        THEN
            'MIRPP_AT'
        WHEN (cocl.`Campaign-ID` = '106999') THEN 'MIRPP_FRA'
        WHEN (cocl.`Campaign-ID` = '106481') THEN 'MICSLL'
        WHEN (cocl.`Campaign-ID` = '106649') THEN 'MIROOS'
        WHEN (cocl.`Campaign-ID` = '106694') THEN 'MIRPOP'
        WHEN
            (cocl.`Campaign-ID` = '106653'
                OR cocl.`Campaign-ID` = '106654')
        THEN
            'CSS (NPS)'
        WHEN
            (cocl.`Campaign-ID` = '106897'
                OR cocl.`Campaign-ID` = '106898')
        THEN
            'CSS_REM (NPS)'
        ELSE 'Other'
    END AS campaign_nick_name,
    COUNT(DISTINCT UniqueKey) AS NumClicks FROM
    CRM_CO.campaing_clicks AS cocl
WHERE
    YEAR(`Dispatch-End`) = 2014
GROUP BY `Mailing-ID`;

CREATE INDEX campaign ON CRM_bases.CO_CampaignClicks(campaign);

-- --------------------------------------------------------------------------------
-- Campaign Summary
-- --------------------------------------------------------------------------------

DROP TABLE IF EXISTS CRM_bases.CO_CampaignUMSsummary;
CREATE TABLE CRM_bases.CO_CampaignUMSsummary select CO_CampaignSents.Country,
    CO_CampaignSents.Day,
    CO_CampaignSents.MailingID,
    CO_CampaignSents.campaign_nick_name,
    CO_CampaignSents.campaign,
    CO_CampaignSents.NumSent,
    CO_CampaignBounces.TotalBounces,
    0 AS NumOpens,
    0 AS NumClicks FROM
    CRM_bases.CO_CampaignSents
        LEFT JOIN
    CRM_bases.CO_CampaignBounces ON CO_CampaignSents.MailingID = CO_CampaignBounces.MailingID
GROUP BY Day , campaign;

-- Update Summary

UPDATE CRM_bases.CO_CampaignUMSsummary cumss
        LEFT JOIN
    CRM_bases.CO_CampaignOpens cop ON cumss.MailingID = cop.MailingID
SET
    cumss.NumOpens = cop.NumOpens;

-- Update Summary

UPDATE CRM_bases.CO_CampaignUMSsummary cumss
        LEFT JOIN
    CRM_bases.CO_CampaignClicks ccl ON cumss.MailingID = ccl.MailingID
SET
    cumss.NumClicks = ccl.NumClicks;

-- --------------------------------------------------------------------------------
-- Routine: Campaigns
-- Country: MX
-- Version: 1
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
-- Campaign Sents
-- --------------------------------------------------------------------------------

DROP TABLE IF EXISTS CRM_bases.MX_CampaignSents;
CREATE TABLE CRM_bases.MX_CampaignSents SELECT 'Mexico' AS Country,
    DATE(`Dispatch-End`) AS 'Day',
    `Mailing-ID` AS MailingID,
    `Mailing-Name` AS Campaign,
    CASE
        WHEN (mxms.`Campaign-ID` = '82669') THEN 'MIAC'
        WHEN (mxms.`Campaign-ID` = '96972') THEN 'MIAC_REM24H'
        WHEN (mxms.`Campaign-ID` = '82780') THEN 'MISH'
        WHEN (mxms.`Campaign-ID` = '86826') THEN 'MIPR'
        WHEN (mxms.`Campaign-ID` = '91458') THEN 'MIPR_REC'
        WHEN (mxms.`Campaign-ID` = '91018') THEN 'MIRR_REC'
        WHEN (mxms.`Campaign-ID` = '91022') THEN 'MIRAO'
        WHEN
            (mxms.`Campaign-ID` = '103992'
                OR mxms.`Campaign-ID` = '105357')
        THEN
            'MIVAO'
        WHEN (mxms.`Campaign-ID` = '107136') THEN 'MIVAO_REM'
        WHEN (mxms.`Campaign-ID` = '104082') THEN 'BH_T'
        WHEN (mxms.`Campaign-ID` = '104794') THEN 'BH_CU'
        WHEN
            (mxms.`Campaign-ID` = '105031'
                OR mxms.`Campaign-ID` = '105032'
                OR mxms.`Campaign-ID` = '105033'
                OR mxms.`Campaign-ID` = '105034'
                OR mxms.`Campaign-ID` = '105035'
                OR mxms.`Campaign-ID` = '106619')
        THEN
            'MIRPP_AT'
        WHEN (mxms.`Campaign-ID` = '106244') THEN 'MIRPP_FRA'
        WHEN (mxms.`Campaign-ID` = '106482') THEN 'MICSLL'
        WHEN (mxms.`Campaign-ID` = '106562') THEN 'MIROOS'
        WHEN (mxms.`Campaign-ID` = '106566') THEN 'MIRPOP'
        WHEN
            (mxms.`Campaign-ID` = '106651'
                OR mxms.`Campaign-ID` = '106652')
        THEN
            'CSS (NPS)'
        WHEN
            (mxms.`Campaign-ID` = '106895'
                OR mxms.`Campaign-ID` = '106896')
        THEN
            'CSS_REM (NPS)'
        ELSE 'Other'
    END AS campaign_nick_name,
    COUNT(DISTINCT UniqueKey) AS NumSent FROM
    CRM_MX.campaing_messages AS mxms
WHERE
    YEAR(`Dispatch-End`) = 2014
GROUP BY `Mailing-ID`;

CREATE INDEX campaign ON CRM_bases.MX_CampaignSents(campaign);

-- --------------------------------------------------------------------------------
-- Campaign Bounces
-- --------------------------------------------------------------------------------

DROP TABLE IF EXISTS CRM_bases.MX_CampaignBounces;
CREATE TABLE CRM_bases.MX_CampaignBounces SELECT 'Mexico' AS Country,
    DATE(`Dispatch-End`) AS 'Day',
    `Mailing-ID` AS MailingID,
    `Mailing-Name` AS Campaign,
    CASE
        WHEN (mxbs.`Campaign-ID` = '82669') THEN 'MIAC'
        WHEN (mxbs.`Campaign-ID` = '96972') THEN 'MIAC_REM24H'
        WHEN (mxbs.`Campaign-ID` = '82780') THEN 'MISH'
        WHEN (mxbs.`Campaign-ID` = '86826') THEN 'MIPR'
        WHEN (mxbs.`Campaign-ID` = '91458') THEN 'MIPR_REC'
        WHEN (mxbs.`Campaign-ID` = '91018') THEN 'MIRR_REC'
        WHEN (mxbs.`Campaign-ID` = '91022') THEN 'MIRAO'
        WHEN
            (mxbs.`Campaign-ID` = '103992'
                OR mxbs.`Campaign-ID` = '105357')
        THEN
            'MIVAO'
        WHEN (mxbs.`Campaign-ID` = '107136') THEN 'MIVAO_REM'
        WHEN (mxbs.`Campaign-ID` = '104082') THEN 'BH_T'
        WHEN (mxbs.`Campaign-ID` = '104794') THEN 'BH_CU'
        WHEN
            (mxbs.`Campaign-ID` = '105031'
                OR mxbs.`Campaign-ID` = '105032'
                OR mxbs.`Campaign-ID` = '105033'
                OR mxbs.`Campaign-ID` = '105034'
                OR mxbs.`Campaign-ID` = '105035'
                OR mxbs.`Campaign-ID` = '106619')
        THEN
            'MIRPP_AT'
        WHEN (mxbs.`Campaign-ID` = '106244') THEN 'MIRPP_FRA'
        WHEN (mxbs.`Campaign-ID` = '106482') THEN 'MICSLL'
        WHEN (mxbs.`Campaign-ID` = '106562') THEN 'MIROOS'
        WHEN (mxbs.`Campaign-ID` = '106566') THEN 'MIRPOP'
        WHEN
            (mxbs.`Campaign-ID` = '106651'
                OR mxbs.`Campaign-ID` = '106652')
        THEN
            'CSS (NPS)'
        WHEN
            (mxbs.`Campaign-ID` = '106895'
                OR mxbs.`Campaign-ID` = '106896')
        THEN
            'CSS_REM (NPS)'
        ELSE 'Other'
    END AS campaign_nick_name,
    COUNT(DISTINCT UniqueKey) AS TotalBounces FROM
    CRM_MX.campaing_bounces AS mxbs
WHERE
    YEAR(`Dispatch-End`) = 2014
GROUP BY `Mailing-ID`;

CREATE INDEX campaign ON CRM_bases.MX_CampaignBounces(campaign);

-- --------------------------------------------------------------------------------
-- Campaign Opens
-- --------------------------------------------------------------------------------

DROP TABLE IF EXISTS CRM_bases.MX_CampaignOpens;
CREATE TABLE CRM_bases.MX_CampaignOpens SELECT 'Mexico' AS Country,
    DATE(`Dispatch-End`) AS 'Day',
    `Mailing-ID` AS MailingID,
    `Mailing-Name` AS Campaign,
    CASE
        WHEN (mxop.`Campaign-ID` = '82669') THEN 'MIAC'
        WHEN (mxop.`Campaign-ID` = '96972') THEN 'MIAC_REM24H'
        WHEN (mxop.`Campaign-ID` = '82780') THEN 'MISH'
        WHEN (mxop.`Campaign-ID` = '86826') THEN 'MIPR'
        WHEN (mxop.`Campaign-ID` = '91458') THEN 'MIPR_REC'
        WHEN (mxop.`Campaign-ID` = '91018') THEN 'MIRR_REC'
        WHEN (mxop.`Campaign-ID` = '91022') THEN 'MIRAO'
        WHEN
            (mxop.`Campaign-ID` = '103992'
                OR mxop.`Campaign-ID` = '105357')
        THEN
            'MIVAO'
        WHEN (mxop.`Campaign-ID` = '107136') THEN 'MIVAO_REM'
        WHEN (mxop.`Campaign-ID` = '104082') THEN 'BH_T'
        WHEN (mxop.`Campaign-ID` = '104794') THEN 'BH_CU'
        WHEN
            (mxop.`Campaign-ID` = '105031'
                OR mxop.`Campaign-ID` = '105032'
                OR mxop.`Campaign-ID` = '105033'
                OR mxop.`Campaign-ID` = '105034'
                OR mxop.`Campaign-ID` = '105035'
                OR mxop.`Campaign-ID` = '106619')
        THEN
            'MIRPP_AT'
        WHEN (mxop.`Campaign-ID` = '106244') THEN 'MIRPP_FRA'
        WHEN (mxop.`Campaign-ID` = '106482') THEN 'MICSLL'
        WHEN (mxop.`Campaign-ID` = '106562') THEN 'MIROOS'
        WHEN (mxop.`Campaign-ID` = '106566') THEN 'MIRPOP'
        WHEN
            (mxop.`Campaign-ID` = '106651'
                OR mxop.`Campaign-ID` = '106652')
        THEN
            'CSS (NPS)'
        WHEN
            (mxop.`Campaign-ID` = '106895'
                OR mxop.`Campaign-ID` = '106896')
        THEN
            'CSS_REM (NPS)'
        ELSE 'Other'
    END AS campaign_nick_name,
    COUNT(DISTINCT UniqueKey) AS NumOpens FROM
    CRM_MX.campaing_openings AS mxop
WHERE
    YEAR(`Dispatch-End`) = 2014
GROUP BY `Mailing-ID`;

CREATE INDEX campaign ON CRM_bases.MX_CampaignOpens(campaign);

-- --------------------------------------------------------------------------------
-- Campaign Clicks
-- --------------------------------------------------------------------------------

DROP TABLE IF EXISTS CRM_bases.MX_CampaignClicks;
CREATE TABLE CRM_bases.MX_CampaignClicks SELECT 'Mexico' AS Country,
    DATE(`Dispatch-End`) AS 'Day',
    `Mailing-ID` AS MailingID,
    `Mailing-Name` AS Campaign,
    CASE
        WHEN (mxcl.`Campaign-ID` = '82669') THEN 'MIAC'
        WHEN (mxcl.`Campaign-ID` = '96972') THEN 'MIAC_REM24H'
        WHEN (mxcl.`Campaign-ID` = '82780') THEN 'MISH'
        WHEN (mxcl.`Campaign-ID` = '86826') THEN 'MIPR'
        WHEN (mxcl.`Campaign-ID` = '91458') THEN 'MIPR_REC'
        WHEN (mxcl.`Campaign-ID` = '91018') THEN 'MIRR_REC'
        WHEN (mxcl.`Campaign-ID` = '91022') THEN 'MIRAO'
        WHEN
            (mxcl.`Campaign-ID` = '103992'
                OR mxcl.`Campaign-ID` = '105357')
        THEN
            'MIVAO'
        WHEN (mxcl.`Campaign-ID` = '107136') THEN 'MIVAO_REM'
        WHEN (mxcl.`Campaign-ID` = '104082') THEN 'BH_T'
        WHEN (mxcl.`Campaign-ID` = '104794') THEN 'BH_CU'
        WHEN
            (mxcl.`Campaign-ID` = '105031'
                OR mxcl.`Campaign-ID` = '105032'
                OR mxcl.`Campaign-ID` = '105033'
                OR mxcl.`Campaign-ID` = '105034'
                OR mxcl.`Campaign-ID` = '105035'
                OR mxcl.`Campaign-ID` = '106619')
        THEN
            'MIRPP_AT'
        WHEN (mxcl.`Campaign-ID` = '106244') THEN 'MIRPP_FRA'
        WHEN (mxcl.`Campaign-ID` = '106482') THEN 'MICSLL'
        WHEN (mxcl.`Campaign-ID` = '106562') THEN 'MIROOS'
        WHEN (mxcl.`Campaign-ID` = '106566') THEN 'MIRPOP'
        WHEN
            (mxcl.`Campaign-ID` = '106651'
                OR mxcl.`Campaign-ID` = '106652')
        THEN
            'CSS (NPS)'
        WHEN
            (mxcl.`Campaign-ID` = '106895'
                OR mxcl.`Campaign-ID` = '106896')
        THEN
            'CSS_REM (NPS)'
        ELSE 'Other'
    END AS campaign_nick_name,
    COUNT(DISTINCT UniqueKey) AS NumClicks FROM
    CRM_MX.campaing_clicks AS mxcl
WHERE
    YEAR(`Dispatch-End`) = 2014
GROUP BY `Mailing-ID`;

CREATE INDEX campaign ON CRM_bases.MX_CampaignClicks(campaign);

-- --------------------------------------------------------------------------------
-- Campaign Summary
-- --------------------------------------------------------------------------------

DROP TABLE IF EXISTS CRM_bases.MX_CampaignUMSsummary;
CREATE TABLE CRM_bases.MX_CampaignUMSsummary select MX_CampaignSents.Country,
    MX_CampaignSents.Day,
    MX_CampaignSents.MailingID,
    MX_CampaignSents.campaign_nick_name,
    MX_CampaignSents.campaign,
    MX_CampaignSents.NumSent,
    MX_CampaignBounces.TotalBounces,
    0 AS NumOpens,
    0 AS NumClicks FROM
    CRM_bases.MX_CampaignSents
        LEFT JOIN
    CRM_bases.MX_CampaignBounces ON MX_CampaignSents.MailingID = MX_CampaignBounces.MailingID
GROUP BY Day , campaign;

-- Update Summary

UPDATE CRM_bases.MX_CampaignUMSsummary cumss
        LEFT JOIN
    CRM_bases.MX_CampaignOpens cop ON cumss.MailingID = cop.MailingID
SET
    cumss.NumOpens = cop.NumOpens;

-- Update Summary

UPDATE CRM_bases.MX_CampaignUMSsummary cumss
        LEFT JOIN
    CRM_bases.MX_CampaignClicks ccl ON cumss.MailingID = ccl.MailingID
SET
    cumss.NumClicks = ccl.NumClicks;

-- --------------------------------------------------------------------------------
-- Routine: Campaigns
-- Country: PE
-- Version: 1
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
-- Campaign Sents
-- --------------------------------------------------------------------------------

DROP TABLE IF EXISTS CRM_bases.PE_CampaignSents;
CREATE TABLE CRM_bases.PE_CampaignSents SELECT 'Peru' AS Country,
    DATE(`Dispatch-End`) AS 'Day',
    `Mailing-ID` AS MailingID,
    `Mailing-Name` AS Campaign,
    CASE
        WHEN (pems.`Campaign-ID` = '83540') THEN 'MIAC'
        WHEN (pems.`Campaign-ID` = '97766') THEN 'MIAC_REM24H'
        WHEN (pems.`Campaign-ID` = '83541') THEN 'MISH'
        WHEN (pems.`Campaign-ID` = '87448') THEN 'MIPR'
        WHEN (pems.`Campaign-ID` = '91463') THEN 'MIPR_REC'
        WHEN (pems.`Campaign-ID` = '91241') THEN 'MIRR_REC'
        WHEN (pems.`Campaign-ID` = '91245') THEN 'MIRAO'
        WHEN (pems.`Campaign-ID` = '88212') THEN 'MICRT'
        WHEN
            (pems.`Campaign-ID` = '104622'
                OR pems.`Campaign-ID` = '104623')
        THEN
            'MIVAO'
        WHEN (pems.`Campaign-ID` = '107138') THEN 'MIVAO_REM'
        WHEN (pems.`Campaign-ID` = '104624') THEN 'BH_T'
        WHEN (pems.`Campaign-ID` = '104796') THEN 'BH_CU'
        WHEN (pems.`Campaign-ID` = '107005') THEN 'MIRPP_FRA'
        WHEN (pems.`Campaign-ID` = '106483') THEN 'MICSLL'
        WHEN (pems.`Campaign-ID` = '106660') THEN 'MIROOS'
        WHEN (pems.`Campaign-ID` = '106695') THEN 'MIRPOP'
        WHEN
            (pems.`Campaign-ID` = '106655'
                OR pems.`Campaign-ID` = '106656')
        THEN
            'CSS (NPS)'
        WHEN
            (pems.`Campaign-ID` = '106899'
                OR pems.`Campaign-ID` = '106900')
        THEN
            'CSS_REM (NPS)'
        ELSE 'Other'
    END AS campaign_nick_name,
    COUNT(DISTINCT UniqueKey) AS NumSent FROM
    CRM_PE.campaing_messages AS pems
WHERE
    YEAR(`Dispatch-End`) = 2014
GROUP BY `Mailing-ID`;

CREATE INDEX campaign ON CRM_bases.PE_CampaignSents(campaign);

-- --------------------------------------------------------------------------------
-- Campaign Bounces
-- --------------------------------------------------------------------------------

DROP TABLE IF EXISTS CRM_bases.PE_CampaignBounces;
CREATE TABLE CRM_bases.PE_CampaignBounces SELECT 'Peru' AS Country,
    DATE(`Dispatch-End`) AS 'Day',
    `Mailing-ID` AS MailingID,
    `Mailing-Name` AS Campaign,
    CASE
        WHEN (pebs.`Campaign-ID` = '83540') THEN 'MIAC'
        WHEN (pebs.`Campaign-ID` = '97766') THEN 'MIAC_REM24H'
        WHEN (pebs.`Campaign-ID` = '83541') THEN 'MISH'
        WHEN (pebs.`Campaign-ID` = '87448') THEN 'MIPR'
        WHEN (pebs.`Campaign-ID` = '91463') THEN 'MIPR_REC'
        WHEN (pebs.`Campaign-ID` = '91241') THEN 'MIRR_REC'
        WHEN (pebs.`Campaign-ID` = '91245') THEN 'MIRAO'
        WHEN (pebs.`Campaign-ID` = '88212') THEN 'MICRT'
        WHEN
            (pebs.`Campaign-ID` = '104622'
                OR pebs.`Campaign-ID` = '104623')
        THEN
            'MIVAO'
        WHEN (pebs.`Campaign-ID` = '107138') THEN 'MIVAO_REM'
        WHEN (pebs.`Campaign-ID` = '104624') THEN 'BH_T'
        WHEN (pebs.`Campaign-ID` = '104796') THEN 'BH_CU'
        WHEN (pebs.`Campaign-ID` = '107005') THEN 'MIRPP_FRA'
        WHEN (pebs.`Campaign-ID` = '106483') THEN 'MICSLL'
        WHEN (pebs.`Campaign-ID` = '106660') THEN 'MIROOS'
        WHEN (pebs.`Campaign-ID` = '106695') THEN 'MIRPOP'
        WHEN
            (pebs.`Campaign-ID` = '106655'
                OR pebs.`Campaign-ID` = '106656')
        THEN
            'CSS (NPS)'
        WHEN
            (pebs.`Campaign-ID` = '106899'
                OR pebs.`Campaign-ID` = '106900')
        THEN
            'CSS_REM (NPS)'
        ELSE 'Other'
    END AS campaign_nick_name,
    COUNT(DISTINCT UniqueKey) AS TotalBounces FROM
    CRM_PE.campaing_bounces AS pebs
WHERE
    YEAR(`Dispatch-End`) = 2014
GROUP BY `Mailing-ID`;

CREATE INDEX campaign ON CRM_bases.PE_CampaignBounces(campaign);

-- --------------------------------------------------------------------------------
-- Campaign Opens
-- --------------------------------------------------------------------------------

DROP TABLE IF EXISTS CRM_bases.PE_CampaignOpens;
CREATE TABLE CRM_bases.PE_CampaignOpens SELECT 'Peru' AS Country,
    DATE(`Dispatch-End`) AS 'Day',
    `Mailing-ID` AS MailingID,
    `Mailing-Name` AS Campaign,
    CASE
        WHEN (peop.`Campaign-ID` = '83540') THEN 'MIAC'
        WHEN (peop.`Campaign-ID` = '97766') THEN 'MIAC_REM24H'
        WHEN (peop.`Campaign-ID` = '83541') THEN 'MISH'
        WHEN (peop.`Campaign-ID` = '87448') THEN 'MIPR'
        WHEN (peop.`Campaign-ID` = '91463') THEN 'MIPR_REC'
        WHEN (peop.`Campaign-ID` = '91241') THEN 'MIRR_REC'
        WHEN (peop.`Campaign-ID` = '91245') THEN 'MIRAO'
        WHEN (peop.`Campaign-ID` = '88212') THEN 'MICRT'
        WHEN
            (peop.`Campaign-ID` = '104622'
                OR peop.`Campaign-ID` = '104623')
        THEN
            'MIVAO'
        WHEN (peop.`Campaign-ID` = '107138') THEN 'MIVAO_REM'
        WHEN (peop.`Campaign-ID` = '104624') THEN 'BH_T'
        WHEN (peop.`Campaign-ID` = '104796') THEN 'BH_CU'
        WHEN (peop.`Campaign-ID` = '107005') THEN 'MIRPP_FRA'
        WHEN (peop.`Campaign-ID` = '106483') THEN 'MICSLL'
        WHEN (peop.`Campaign-ID` = '106660') THEN 'MIROOS'
        WHEN (peop.`Campaign-ID` = '106695') THEN 'MIRPOP'
        WHEN
            (peop.`Campaign-ID` = '106655'
                OR peop.`Campaign-ID` = '106656')
        THEN
            'CSS (NPS)'
        WHEN
            (peop.`Campaign-ID` = '106899'
                OR peop.`Campaign-ID` = '106900')
        THEN
            'CSS_REM (NPS)'
        ELSE 'Other'
    END AS campaign_nick_name,
    COUNT(DISTINCT UniqueKey) AS NumOpens FROM
    CRM_PE.campaing_openings AS peop
WHERE
    YEAR(`Dispatch-End`) = 2014
GROUP BY `Mailing-ID`;

CREATE INDEX campaign ON CRM_bases.PE_CampaignOpens(campaign);

-- --------------------------------------------------------------------------------
-- Campaign Clicks
-- --------------------------------------------------------------------------------

DROP TABLE IF EXISTS CRM_bases.PE_CampaignClicks;
CREATE TABLE CRM_bases.PE_CampaignClicks SELECT 'Peru' AS Country,
    DATE(`Dispatch-End`) AS 'Day',
    `Mailing-ID` AS MailingID,
    `Mailing-Name` AS Campaign,
    CASE
        WHEN (pecl.`Campaign-ID` = '83540') THEN 'MIAC'
        WHEN (pecl.`Campaign-ID` = '97766') THEN 'MIAC_REM24H'
        WHEN (pecl.`Campaign-ID` = '83541') THEN 'MISH'
        WHEN (pecl.`Campaign-ID` = '87448') THEN 'MIPR'
        WHEN (pecl.`Campaign-ID` = '91463') THEN 'MIPR_REC'
        WHEN (pecl.`Campaign-ID` = '91241') THEN 'MIRR_REC'
        WHEN (pecl.`Campaign-ID` = '91245') THEN 'MIRAO'
        WHEN (pecl.`Campaign-ID` = '88212') THEN 'MICRT'
        WHEN
            (pecl.`Campaign-ID` = '104622'
                OR pecl.`Campaign-ID` = '104623')
        THEN
            'MIVAO'
        WHEN (pecl.`Campaign-ID` = '107138') THEN 'MIVAO_REM'
        WHEN (pecl.`Campaign-ID` = '104624') THEN 'BH_T'
        WHEN (pecl.`Campaign-ID` = '104796') THEN 'BH_CU'
        WHEN (pecl.`Campaign-ID` = '107005') THEN 'MIRPP_FRA'
        WHEN (pecl.`Campaign-ID` = '106483') THEN 'MICSLL'
        WHEN (pecl.`Campaign-ID` = '106660') THEN 'MIROOS'
        WHEN (pecl.`Campaign-ID` = '106695') THEN 'MIRPOP'
        WHEN
            (pecl.`Campaign-ID` = '106655'
                OR pecl.`Campaign-ID` = '106656')
        THEN
            'CSS (NPS)'
        WHEN
            (pecl.`Campaign-ID` = '106899'
                OR pecl.`Campaign-ID` = '106900')
        THEN
            'CSS_REM (NPS)'
        ELSE 'Other'
    END AS campaign_nick_name,
    COUNT(DISTINCT UniqueKey) AS NumClicks FROM
    CRM_PE.campaing_clicks AS pecl
WHERE
    YEAR(`Dispatch-End`) = 2014
GROUP BY `Mailing-ID`;

CREATE INDEX campaign ON CRM_bases.PE_CampaignClicks(campaign);

-- --------------------------------------------------------------------------------
-- Campaign Summary
-- --------------------------------------------------------------------------------

DROP TABLE IF EXISTS CRM_bases.PE_CampaignUMSsummary;
CREATE TABLE CRM_bases.PE_CampaignUMSsummary select PE_CampaignSents.Country,
    PE_CampaignSents.Day,
    PE_CampaignSents.MailingID,
    PE_CampaignSents.campaign_nick_name,
    PE_CampaignSents.campaign,
    PE_CampaignSents.NumSent,
    PE_CampaignBounces.TotalBounces,
    0 AS NumOpens,
    0 AS NumClicks FROM
    CRM_bases.PE_CampaignSents
        LEFT JOIN
    CRM_bases.PE_CampaignBounces ON PE_CampaignSents.MailingID = PE_CampaignBounces.MailingID
GROUP BY Day , campaign;

-- Update Summary

UPDATE CRM_bases.PE_CampaignUMSsummary cumss
        LEFT JOIN
    CRM_bases.PE_CampaignOpens cop ON cumss.MailingID = cop.MailingID
SET
    cumss.NumOpens = cop.NumOpens;

-- Update Summary

UPDATE CRM_bases.PE_CampaignUMSsummary cumss
        LEFT JOIN
    CRM_bases.PE_CampaignClicks ccl ON cumss.MailingID = ccl.MailingID
SET
    cumss.NumClicks = ccl.NumClicks;

-- --------------------------------------------------------------------------------
-- Routine: Campaigns
-- Country: VE
-- Version: 1
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
-- Campaign Sents
-- --------------------------------------------------------------------------------

DROP TABLE IF EXISTS CRM_bases.VE_CampaignSents;
CREATE TABLE CRM_bases.VE_CampaignSents SELECT 'Venezuela' AS Country,
    DATE(`Dispatch-End`) AS 'Day',
    `Mailing-ID` AS MailingID,
    `Mailing-Name` AS Campaign,
    CASE
        WHEN (vems.`Campaign-ID` = '83542') THEN 'MIAC'
        WHEN (vems.`Campaign-ID` = '97771') THEN 'MIAC_REM24H'
        WHEN (vems.`Campaign-ID` = '83543') THEN 'MISH'
        WHEN (vems.`Campaign-ID` = '87450') THEN 'MIPR'
        WHEN (vems.`Campaign-ID` = '92671') THEN 'MIPR_REC'
        WHEN (vems.`Campaign-ID` = '91258') THEN 'MIRR_REC'
        WHEN (vems.`Campaign-ID` = '91261') THEN 'MIRAO'
        WHEN
            (vems.`Campaign-ID` = '104625'
                OR vems.`Campaign-ID` = '104626')
        THEN
            'MIVAO'
        WHEN (vems.`Campaign-ID` = '107137') THEN 'MIVAO_REM'
        WHEN (vems.`Campaign-ID` = '104627') THEN 'BH_T'
        WHEN (vems.`Campaign-ID` = '104797') THEN 'BH_CU'
        WHEN (vems.`Campaign-ID` = '107012') THEN 'MIRPP_FRA'
        WHEN (vems.`Campaign-ID` = '106484') THEN 'MICSLL'
        WHEN (vems.`Campaign-ID` = '106661') THEN 'MIROOS'
        WHEN (vems.`Campaign-ID` = '106696') THEN 'MIRPOP'
        WHEN
            (vems.`Campaign-ID` = '106657'
                OR vems.`Campaign-ID` = '106658')
        THEN
            'CSS (NPS)'
        WHEN
            (vems.`Campaign-ID` = '106901'
                OR vems.`Campaign-ID` = '106902')
        THEN
            'CSS_REM (NPS)'
        ELSE 'Other'
    END AS campaign_nick_name,
    COUNT(DISTINCT UniqueKey) AS NumSent FROM
    CRM_VE.campaing_messages AS vems
WHERE
    YEAR(`Dispatch-End`) = 2014
GROUP BY `Mailing-ID`;

CREATE INDEX campaign ON CRM_bases.VE_CampaignSents(campaign);

-- --------------------------------------------------------------------------------
-- Campaign Bounces
-- --------------------------------------------------------------------------------

DROP TABLE IF EXISTS CRM_bases.VE_CampaignBounces;
CREATE TABLE CRM_bases.VE_CampaignBounces SELECT 'Venezuela' AS Country,
    DATE(`Dispatch-End`) AS 'Day',
    `Mailing-ID` AS MailingID,
    `Mailing-Name` AS Campaign,
    CASE
        WHEN (vebs.`Campaign-ID` = '83542') THEN 'MIAC'
        WHEN (vebs.`Campaign-ID` = '97771') THEN 'MIAC_REM24H'
        WHEN (vebs.`Campaign-ID` = '83543') THEN 'MISH'
        WHEN (vebs.`Campaign-ID` = '87450') THEN 'MIPR'
        WHEN (vebs.`Campaign-ID` = '92671') THEN 'MIPR_REC'
        WHEN (vebs.`Campaign-ID` = '91258') THEN 'MIRR_REC'
        WHEN (vebs.`Campaign-ID` = '91261') THEN 'MIRAO'
        WHEN
            (vebs.`Campaign-ID` = '104625'
                OR vebs.`Campaign-ID` = '104626')
        THEN
            'MIVAO'
        WHEN (vebs.`Campaign-ID` = '107137') THEN 'MIVAO_REM'
        WHEN (vebs.`Campaign-ID` = '104627') THEN 'BH_T'
        WHEN (vebs.`Campaign-ID` = '104797') THEN 'BH_CU'
        WHEN (vebs.`Campaign-ID` = '107012') THEN 'MIRPP_FRA'
        WHEN (vebs.`Campaign-ID` = '106484') THEN 'MICSLL'
        WHEN (vebs.`Campaign-ID` = '106661') THEN 'MIROOS'
        WHEN (vebs.`Campaign-ID` = '106696') THEN 'MIRPOP'
        WHEN
            (vebs.`Campaign-ID` = '106657'
                OR vebs.`Campaign-ID` = '106658')
        THEN
            'CSS (NPS)'
        WHEN
            (vebs.`Campaign-ID` = '106901'
                OR vebs.`Campaign-ID` = '106902')
        THEN
            'CSS_REM (NPS)'
        ELSE 'Other'
    END AS campaign_nick_name,
    COUNT(DISTINCT UniqueKey) AS TotalBounces FROM
    CRM_VE.campaing_bounces AS vebs
WHERE
    YEAR(`Dispatch-End`) = 2014
GROUP BY `Mailing-ID`;

CREATE INDEX campaign ON CRM_bases.VE_CampaignBounces(campaign);

-- --------------------------------------------------------------------------------
-- Campaign Opens
-- --------------------------------------------------------------------------------

DROP TABLE IF EXISTS CRM_bases.VE_CampaignOpens;
CREATE TABLE CRM_bases.VE_CampaignOpens SELECT 'Venezuela' AS Country,
    DATE(`Dispatch-End`) AS 'Day',
    `Mailing-ID` AS MailingID,
    `Mailing-Name` AS Campaign,
    CASE
        WHEN (veop.`Campaign-ID` = '83542') THEN 'MIAC'
        WHEN (veop.`Campaign-ID` = '97771') THEN 'MIAC_REM24H'
        WHEN (veop.`Campaign-ID` = '83543') THEN 'MISH'
        WHEN (veop.`Campaign-ID` = '87450') THEN 'MIPR'
        WHEN (veop.`Campaign-ID` = '92671') THEN 'MIPR_REC'
        WHEN (veop.`Campaign-ID` = '91258') THEN 'MIRR_REC'
        WHEN (veop.`Campaign-ID` = '91261') THEN 'MIRAO'
        WHEN
            (veop.`Campaign-ID` = '104625'
                OR veop.`Campaign-ID` = '104626')
        THEN
            'MIVAO'
        WHEN (veop.`Campaign-ID` = '107137') THEN 'MIVAO_REM'
        WHEN (veop.`Campaign-ID` = '104627') THEN 'BH_T'
        WHEN (veop.`Campaign-ID` = '104797') THEN 'BH_CU'
        WHEN (veop.`Campaign-ID` = '107012') THEN 'MIRPP_FRA'
        WHEN (veop.`Campaign-ID` = '106484') THEN 'MICSLL'
        WHEN (veop.`Campaign-ID` = '106661') THEN 'MIROOS'
        WHEN (veop.`Campaign-ID` = '106696') THEN 'MIRPOP'
        WHEN
            (veop.`Campaign-ID` = '106657'
                OR veop.`Campaign-ID` = '106658')
        THEN
            'CSS (NPS)'
        WHEN
            (veop.`Campaign-ID` = '106901'
                OR veop.`Campaign-ID` = '106902')
        THEN
            'CSS_REM (NPS)'
        ELSE 'Other'
    END AS campaign_nick_name,
    COUNT(DISTINCT UniqueKey) AS NumOpens FROM
    CRM_VE.campaing_openings AS veop
WHERE
    YEAR(`Dispatch-End`) = 2014
GROUP BY `Mailing-ID`;

CREATE INDEX campaign ON CRM_bases.VE_CampaignOpens(campaign);

-- --------------------------------------------------------------------------------
-- Campaign Clicks
-- --------------------------------------------------------------------------------

DROP TABLE IF EXISTS CRM_bases.VE_CampaignClicks;
CREATE TABLE CRM_bases.VE_CampaignClicks SELECT 'Venezuela' AS Country,
    DATE(`Dispatch-End`) AS 'Day',
    `Mailing-ID` AS MailingID,
    `Mailing-Name` AS Campaign,
    CASE
        WHEN (vecl.`Campaign-ID` = '83542') THEN 'MIAC'
        WHEN (vecl.`Campaign-ID` = '97771') THEN 'MIAC_REM24H'
        WHEN (vecl.`Campaign-ID` = '83543') THEN 'MISH'
        WHEN (vecl.`Campaign-ID` = '87450') THEN 'MIPR'
        WHEN (vecl.`Campaign-ID` = '92671') THEN 'MIPR_REC'
        WHEN (vecl.`Campaign-ID` = '91258') THEN 'MIRR_REC'
        WHEN (vecl.`Campaign-ID` = '91261') THEN 'MIRAO'
        WHEN
            (vecl.`Campaign-ID` = '104625'
                OR vecl.`Campaign-ID` = '104626')
        THEN
            'MIVAO'
        WHEN (vecl.`Campaign-ID` = '107137') THEN 'MIVAO_REM'
        WHEN (vecl.`Campaign-ID` = '104627') THEN 'BH_T'
        WHEN (vecl.`Campaign-ID` = '104797') THEN 'BH_CU'
        WHEN (vecl.`Campaign-ID` = '107012') THEN 'MIRPP_FRA'
        WHEN (vecl.`Campaign-ID` = '106484') THEN 'MICSLL'
        WHEN (vecl.`Campaign-ID` = '106661') THEN 'MIROOS'
        WHEN (vecl.`Campaign-ID` = '106696') THEN 'MIRPOP'
        WHEN
            (vecl.`Campaign-ID` = '106657'
                OR vecl.`Campaign-ID` = '106658')
        THEN
            'CSS (NPS)'
        WHEN
            (vecl.`Campaign-ID` = '106901'
                OR vecl.`Campaign-ID` = '106902')
        THEN
            'CSS_REM (NPS)'
        ELSE 'Other'
    END AS campaign_nick_name,
    COUNT(DISTINCT UniqueKey) AS NumClicks FROM
    CRM_VE.campaing_clicks AS vecl
WHERE
    YEAR(`Dispatch-End`) = 2014
GROUP BY `Mailing-ID`;

CREATE INDEX campaign ON CRM_bases.VE_CampaignClicks(campaign);

-- --------------------------------------------------------------------------------
-- Campaign Summary
-- --------------------------------------------------------------------------------

DROP TABLE IF EXISTS CRM_bases.VE_CampaignUMSsummary;
CREATE TABLE CRM_bases.VE_CampaignUMSsummary select VE_CampaignSents.Country,
    VE_CampaignSents.Day,
    VE_CampaignSents.MailingID,
    VE_CampaignSents.campaign_nick_name,
    VE_CampaignSents.campaign,
    VE_CampaignSents.NumSent,
    VE_CampaignBounces.TotalBounces,
    0 AS NumOpens,
    0 AS NumClicks FROM
    CRM_bases.VE_CampaignSents
        LEFT JOIN
    CRM_bases.VE_CampaignBounces ON VE_CampaignSents.MailingID = VE_CampaignBounces.MailingID
GROUP BY Day , campaign;

-- Update Summary

UPDATE CRM_bases.VE_CampaignUMSsummary cumss
        LEFT JOIN
    CRM_bases.VE_CampaignOpens cop ON cumss.MailingID = cop.MailingID
SET
    cumss.NumOpens = cop.NumOpens;

-- Update Summary

UPDATE CRM_bases.VE_CampaignUMSsummary cumss
        LEFT JOIN
    CRM_bases.VE_CampaignClicks ccl ON cumss.MailingID = ccl.MailingID
SET
    cumss.NumClicks = ccl.NumClicks;

-- --------------------------------------------------------------------------------
-- Routine Regional Campaign UMS Summary
-- --------------------------------------------------------------------------------

DROP TABLE IF EXISTS CRM_bases.Regional_CampaignUMSsummary;
CREATE TABLE CRM_bases.Regional_CampaignUMSsummary
SELECT
    *
FROM
    CRM_bases.MX_CampaignUMSsummary
UNION ALL SELECT
    *
FROM
    CRM_bases.CO_CampaignUMSsummary
UNION ALL SELECT
    *
FROM
    CRM_bases.PE_CampaignUMSsummary
UNION ALL SELECT
    *
FROM
    CRM_bases.VE_CampaignUMSsummary;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`borja.fernandez`@`%`*/ /*!50003 PROCEDURE `Rutina_create_cf_report`()
BEGIN

##create_cf_report_v2

##########Tabla Auxiliar Datos Experian sólo de CF#####################################

drop table if exists CRM_bases.tmp_aux_Regional_CampaignUMSsummary;
create temporary table CRM_bases.tmp_aux_Regional_CampaignUMSsummary
select 	country,
				Day,
				campaign_nick_name,
				sum(NumSent)as NumSent,
				sum(TotalBounces) as TotalBounces,
				sum(NumOpens) as NumOpens,
				sum(NumClicks) as NumClicks
	from CRM_bases.Regional_CampaignUMSsummary 
		where campaign_nick_name <> 'Other'
			group by campaign_nick_name, Day, Country;

##########Tabla Auxiliar Datos Performance sólo de CF#####################################

drop table if exists CRM_bases.tmp_aux_performance_report_for_CF;
create temporary table CRM_bases.tmp_aux_performance_report_for_CF
select 	country,
				yrmonth,
				week,
				date,
				campaign_nick_name,
				sum(visits) as visits,
				sum(carts) as carts,
				sum(gross_transactions) as gross_transactions,
				sum(gross_revenue) as gross_revenue,
				sum(net_transactions) as net_transactions,
				sum(net_revenue) as net_revenue,
				sum(`PC1`) as PC1,
				sum(`PC1.5`) as PC1P5,
				sum(`PC2`) as PC2
	from CRM_bases.performance_report_summary_with_campaign_nick_names
		where campaign_nick_name <> 'Other'
			group by campaign_nick_name, date, country;

#####################Creación Reporte de CF###############################

drop table if exists CRM_bases.cf_report;
create table CRM_bases.cf_report
select 	a.country,
				yrmonth,
				week,
				Day,
				a.campaign_nick_name,
				NumSent,
				TotalBounces,
				NumOpens,
				NumClicks,
				visits,
				carts,
				gross_transactions,
				gross_revenue,
				net_transactions,
				net_revenue,
				PC1,
				PC1P5,
				PC2
	from 	CRM_bases.tmp_aux_Regional_CampaignUMSsummary a	
		left join CRM_bases.tmp_aux_performance_report_for_CF b
			on a.country=b.country and a.day=b.date and a.campaign_nick_name=b.campaign_nick_name;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`borja.fernandez`@`%`*/ /*!50003 PROCEDURE `Rutina_Create_clicks_por_categoria`()
BEGIN

##clicks_por_categoria_v11

##############Query Final####################

################MEXICO#######################

Drop table if EXISTS CRM_bases.MX_ClicksEmailDiaCategoria;
Create table CRM_bases.MX_ClicksEmailDiaCategoria
select 
			UniqueKey as email,
			`Dispatch-Start` as dia,
			`Mailing-ID` as MailingID,
			if(`Link-URL` like 'fashion','moda',
			if(
					LOCATE(	'/',
							substring(`Link-URL`,25))=0,
							'',
							left(substring(`Link-URL`,25),
							LOCATE('/',substring(`Link-URL`,25))-1)))
			as categoria
from CRM_MX.campaing_clicks 
  where `Link-URL` like 'http://www.linio.com.mx/%';

Drop table if EXISTS CRM_bases.MX_ClicksPorEmailPorCategoria;
Create table CRM_bases.MX_ClicksPorEmailPorCategoria
select 
			a.email,
			TYPE_CUSTOMER,
			categoria,
			count(distinct MailingID) as NumClicks
from CRM_bases.MX_ClicksEmailDiaCategoria a
	LEFT JOIN dev_marketing.customers_rev_mx b on a.email=b.email
	where a.email not like ' '
		and categoria not like ' '
	group by email,categoria;

create index email on CRM_bases.MX_ClicksPorEmailPorCategoria(email);

Drop table if EXISTS CRM_bases.MX_TotalClicksPorPersona;
Create table CRM_bases.MX_TotalClicksPorPersona
select 	email,
				sum(NumClicks) as Total_Clicks
	from CRM_bases.MX_ClicksPorEmailPorCategoria
		group by email;

create index email on CRM_bases.MX_TotalClicksPorPersona(email);

Drop table if EXISTS CRM_bases.MX_ClicksResumenPorMailPorCategoria;
Create table CRM_bases.MX_ClicksResumenPorMailPorCategoria
select 	distinct(email) as email,
				TYPE_CUSTOMER,
				0 as Total_Clicks,
				0 as Moda,
				0 as TV_Audio_Video,
				0 as Hogar,
				0 as Computadoras,
				0 as Super_Descuentos,
				0 as Celulares,
				0 as Deportes,
				0 as Ninos_y_Bebes,
				0 as Salud_y_Cuidado,
				0 as Venta_Nocturna,
				0 as CAC,
				0 as Camaras,
				0 as Videojuegos,
				0 as Electrodomesticos,
				0 as Mascotas
		from CRM_bases.MX_ClicksPorEmailPorCategoria;

create index email on CRM_bases.MX_ClicksResumenPorMailPorCategoria(email);

update CRM_bases.MX_ClicksResumenPorMailPorCategoria a 
 INNER JOIN CRM_bases.MX_TotalClicksPorPersona b
		on a.email=b.email 
				set a.Total_Clicks=b.Total_Clicks;

update CRM_bases.MX_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.MX_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Moda=b.NumClicks
					where b.categoria='moda';

update CRM_bases.MX_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.MX_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.TV_Audio_Video=b.NumClicks
					where b.categoria='tv-audio-y-video';

update CRM_bases.MX_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.MX_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Hogar=b.NumClicks
					where b.categoria='hogar';

update CRM_bases.MX_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.MX_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Computadoras=b.NumClicks
					where b.categoria='computadoras';

update CRM_bases.MX_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.MX_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Super_Descuentos=b.NumClicks
					where b.categoria='super-descuentos';

update CRM_bases.MX_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.MX_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Celulares=b.NumClicks
					where b.categoria='celulares-telefonia-y-gps';

update CRM_bases.MX_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.MX_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Deportes=b.NumClicks
					where b.categoria='deportes';

update CRM_bases.MX_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.MX_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Ninos_y_Bebes=b.NumClicks
					where b.categoria='ninos-y-bebes';

update CRM_bases.MX_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.MX_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Salud_y_Cuidado=b.NumClicks
					where b.categoria='salud-y-cuidado-personal';

update CRM_bases.MX_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.MX_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Venta_Nocturna=b.NumClicks
					where b.categoria='venta-nocturna';

update CRM_bases.MX_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.MX_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.CAC=b.NumClicks
					where b.categoria='cac';

update CRM_bases.MX_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.MX_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Camaras=b.NumClicks
					where b.categoria='camaras';

update CRM_bases.MX_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.MX_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Videojuegos=b.NumClicks
					where b.categoria='videojuegos';

update CRM_bases.MX_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.MX_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Electrodomesticos=b.NumClicks
					where b.categoria='electrodomesticos';

update CRM_bases.MX_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.MX_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Mascotas=b.NumClicks
					where b.categoria='mascotas';

drop table if exists CRM_bases.MX_ClicksResumenPorMailPorCategoriaPCT;
create table CRM_bases.MX_ClicksResumenPorMailPorCategoriaPCT
select 	email,
				TYPE_CUSTOMER,
				Total_Clicks,
				Moda/Total_Clicks as Moda_PCT,
				TV_Audio_Video/Total_Clicks as TV_Audio_Video_PCT,
				Hogar/Total_Clicks as Hogar_PCT,
				Computadoras/Total_Clicks as Computadoras_PCT,
				Super_Descuentos/Total_Clicks as Super_Descuentos_PCT,
				Celulares/Total_Clicks as Celulares_PCT,
				Deportes/Total_Clicks as Deportes_PCT,
				Ninos_y_Bebes/Total_Clicks as Ninos_y_Bebes_PCT,
				Salud_y_Cuidado/Total_Clicks as Salud_y_Cuidado_PCT,
				Venta_Nocturna/Total_Clicks as Venta_Nocturna_PCT,
				CAC/Total_Clicks as CAC_PCT,
				Camaras/Total_Clicks as Camaras_PCT,
				Videojuegos/Total_Clicks as Videojuegos_PCT,
				Electrodomesticos/Total_Clicks as Electrodomesticos_PCT,
				Mascotas/Total_Clicks as Mascotas_PCT
	from CRM_bases.MX_ClicksResumenPorMailPorCategoria;

################COLOMBIA#######################

Drop table if EXISTS CRM_bases.CO_ClicksEmailDiaCategoria;
Create table CRM_bases.CO_ClicksEmailDiaCategoria
select 
			UniqueKey as email,
			`Dispatch-Start` as dia,
			`Mailing-ID` as MailingID,
			if(`Link-URL` like 'fashion','moda',
			if(
					LOCATE(	'/',
							substring(`Link-URL`,25))=0,
							'',
							left(substring(`Link-URL`,25),
							LOCATE('/',substring(`Link-URL`,25))-1)))
			as categoria
from CRM_CO.campaing_clicks 
  where `Link-URL` like 'http://www.linio.com.co/%' or `Link-URL` like 'http://www.liniofashion.com.co/%';

Drop table if EXISTS CRM_bases.CO_ClicksPorEmailPorCategoria;
Create table CRM_bases.CO_ClicksPorEmailPorCategoria
select 
			a.email,
			TYPE_CUSTOMER,
			categoria,
			count(distinct MailingID) as NumClicks
from CRM_bases.CO_ClicksEmailDiaCategoria a
	LEFT JOIN dev_marketing.customers_rev_co b 
		on a.email=b.email
	where a.email not like ' '
		and categoria not like ' '
	group by email,categoria;

create index email on CRM_bases.CO_ClicksPorEmailPorCategoria(email);

Drop table if EXISTS CRM_bases.CO_TotalClicksPorPersona;
Create table CRM_bases.CO_TotalClicksPorPersona
select 	email,
				sum(NumClicks) as Total_Clicks
	from CRM_bases.CO_ClicksPorEmailPorCategoria
		group by email;

create index email on CRM_bases.CO_TotalClicksPorPersona(email);

Drop table if EXISTS CRM_bases.CO_ClicksResumenPorMailPorCategoria;
Create table CRM_bases.CO_ClicksResumenPorMailPorCategoria
select 	distinct(email) as email,
				TYPE_CUSTOMER,
				0 as Total_Clicks,
				0 as Tecnologia,
				0 as Campana_cac,
				0 as Precios_Trasnochadores,
				0 as Hogar,
				0 as Super_Descuentos,
				0 as Temprano_Barato,
				0 as Precios_Fugaces,
				0 as Salud_y_Cuidado_Personal,
				0 as Juguetes_y_Bebes,
				0 as Tv_Video_y_Audio,
				0 as Deportes_y_Tiempo_Libre,
				0 as Mega_Liquidacion,
				0 as Computadores_y_Tablets,
				0 as Celulares_Telefonia_Gps,
				0 as Ninos_y_Bebes
		from CRM_bases.CO_ClicksPorEmailPorCategoria;

create index email on CRM_bases.CO_ClicksResumenPorMailPorCategoria(email);

update CRM_bases.CO_ClicksResumenPorMailPorCategoria a 
 INNER JOIN CRM_bases.CO_TotalClicksPorPersona b
		on a.email=b.email 
				set a.Total_Clicks=b.Total_Clicks;

update CRM_bases.CO_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.CO_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Tecnologia=b.NumClicks
					where b.categoria='tecnologia';

update CRM_bases.CO_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.CO_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Campana_Cac=b.NumClicks
					where b.categoria='campana-cac';

update CRM_bases.CO_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.CO_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Precios_Trasnochadores=b.NumClicks
					where b.categoria='precios-trasnochadores';

update CRM_bases.CO_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.CO_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Hogar=b.NumClicks
					where b.categoria='hogar';

update CRM_bases.CO_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.CO_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Super_Descuentos=b.NumClicks
					where b.categoria='super-descuentos';

update CRM_bases.CO_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.CO_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Temprano_Barato=b.NumClicks
					where b.categoria='temprano-barato';

update CRM_bases.CO_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.CO_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Precios_Fugaces=b.NumClicks
					where b.categoria='precios-fugaces';

update CRM_bases.CO_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.CO_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Salud_y_Cuidado_Personal=b.NumClicks
					where b.categoria='salud-y-cuidado-personal';

update CRM_bases.CO_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.CO_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Juguetes_y_Bebes=b.NumClicks
					where b.categoria='juguetes-y-bebes';

update CRM_bases.CO_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.CO_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Tv_Video_y_Audio=b.NumClicks
					where b.categoria='tv-video-y-audio';

update CRM_bases.CO_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.CO_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Deportes_y_Tiempo_Libre=b.NumClicks
					where b.categoria='deportes-y-tiempo-libre';

update CRM_bases.CO_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.CO_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Mega_Liquidacion=b.NumClicks
					where b.categoria='mega-liquidacion';

update CRM_bases.CO_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.CO_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Computadores_y_Tablets=b.NumClicks
					where b.categoria='computadores-y-tablets';

update CRM_bases.CO_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.CO_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Celulares_Telefonia_Gps=b.NumClicks
					where b.categoria='celulares-telefonia-gps';

update CRM_bases.CO_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.CO_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Ninos_y_Bebes=b.NumClicks
					where b.categoria='ninos-y-bebes';

drop table if exists CRM_bases.CO_ClicksResumenPorMailPorCategoriaPCT;
create table CRM_bases.CO_ClicksResumenPorMailPorCategoriaPCT
select 	email,
				TYPE_CUSTOMER,
				Total_Clicks,
				Tecnologia/Total_Clicks as Tecnologia_PCT,
				Campana_cac/Total_Clicks as Campana_cac_PCT,
				Precios_Trasnochadores/Total_Clicks as Precios_Trasnochadores_PCT,
				Hogar/Total_Clicks as Hogar_PCT,
				Super_Descuentos/Total_Clicks as Super_Descuentos_PCT,
				Temprano_Barato/Total_Clicks as Temprano_Barato_PCT,
				Precios_Fugaces/Total_Clicks as Precios_Fugaces_PCT,
				Salud_y_Cuidado_Personal/Total_Clicks as Salud_y_Cuidado_Personal_PCT,
				Juguetes_y_Bebes/Total_Clicks as Juguetes_y_Bebes_PCT,
				Tv_Video_y_Audio/Total_Clicks as Tv_Video_y_Audio_PCT,
				Deportes_y_Tiempo_Libre/Total_Clicks as Deportes_y_Tiempo_Libre_PCT,
				Mega_Liquidacion/Total_Clicks as Mega_Liquidacion_PCT,
				Computadores_y_Tablets/Total_Clicks as Computadores_y_Tablets_PCT,
				Celulares_Telefonia_Gps/Total_Clicks as Celulares_Telefonia_Gps_PCT,
				Ninos_y_Bebes/Total_Clicks as Ninos_y_Bebes_PCT
	from CRM_bases.CO_ClicksResumenPorMailPorCategoria;

###############PERU#######################

Drop table if EXISTS CRM_bases.PE_ClicksEmailDiaCategoria;
Create table CRM_bases.PE_ClicksEmailDiaCategoria
select 
			UniqueKey as email,
			`Dispatch-Start` as dia,
			`Mailing-ID` as MailingID,
			if(`Link-URL` like 'fashion','moda',
			if(
					LOCATE(	'/',
							substring(`Link-URL`,25))=0,
							'',
							left(substring(`Link-URL`,25),
							LOCATE('/',substring(`Link-URL`,25))-1)))
			as categoria
from CRM_PE.campaing_clicks 
  where `Link-URL` like 'http://www.linio.com.pe/%' or `Link-URL` like 'http://www.liniofashion.com.pe/%';

Drop table if EXISTS CRM_bases.PE_ClicksPorEmailPorCategoria;
Create table CRM_bases.PE_ClicksPorEmailPorCategoria
select 
			a.email,
			TYPE_CUSTOMER,
			categoria,
			count(distinct MailingID) as NumClicks
from CRM_bases.PE_ClicksEmailDiaCategoria a
	LEFT JOIN dev_marketing.customers_rev_pe b on a.email=b.email
	where a.email not like ' '
		and categoria not like ' '
	group by email,categoria;

create index email on CRM_bases.PE_ClicksPorEmailPorCategoria(email);

Drop table if EXISTS CRM_bases.PE_TotalClicksPorPersona;
Create table CRM_bases.PE_TotalClicksPorPersona
select 	email,
				sum(NumClicks) as Total_Clicks
	from CRM_bases.PE_ClicksPorEmailPorCategoria
		group by email;

create index email on CRM_bases.PE_TotalClicksPorPersona(email);

Drop table if EXISTS CRM_bases.PE_ClicksResumenPorMailPorCategoria;
Create table CRM_bases.PE_ClicksResumenPorMailPorCategoria
select 	distinct(email) as email,
				TYPE_CUSTOMER,
				0 as Total_Clicks,
				0 as Celulares_Telefonia_y_Gps,
				0 as Computadoras,
				0 as Tv_Audio_y_Video,
				0 as Hogar,
				0 as Cac,
				0 as Electrodomesticos,
				0 as Remate,
				0 as Ropa_Calzado_y_Accesorios,
				0 as Deportes,
				0 as Tecnologia,
				0 as Videojuegos,
				0 as Camaras,
				0 as Ninos_y_Bebes,
				0 as Cuidado_Personal_y_Salud,
				0 as Cuenta_Regresiva
		from CRM_bases.PE_ClicksPorEmailPorCategoria;

create index email on CRM_bases.PE_ClicksResumenPorMailPorCategoria(email);

update CRM_bases.PE_ClicksResumenPorMailPorCategoria a 
 INNER JOIN CRM_bases.PE_TotalClicksPorPersona b
		on a.email=b.email 
				set a.Total_Clicks=b.Total_Clicks;

update CRM_bases.PE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.PE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Celulares_Telefonia_y_Gps=b.NumClicks
					where b.categoria='celulares-telefonia-y-gps';

update CRM_bases.PE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.PE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Computadoras=b.NumClicks
					where b.categoria='computadoras';

update CRM_bases.PE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.PE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Tv_Audio_y_Video=b.NumClicks
					where b.categoria='tv-audio-y-video';

update CRM_bases.PE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.PE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Hogar=b.NumClicks
					where b.categoria='hogar';

update CRM_bases.PE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.PE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Cac=b.NumClicks
					where b.categoria='cac';

update CRM_bases.PE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.PE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Electrodomesticos=b.NumClicks
					where b.categoria='electrodomesticos';

update CRM_bases.PE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.PE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Remate=b.NumClicks
					where b.categoria='remate';

update CRM_bases.PE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.PE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Ropa_Calzado_y_Accesorios=b.NumClicks
					where b.categoria='ropa-calzado-y-accesorios';

update CRM_bases.PE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.PE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.deportes=b.NumClicks
					where b.categoria='deportes';

update CRM_bases.PE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.PE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Tecnologia=b.NumClicks
					where b.categoria='tecnologia';

update CRM_bases.PE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.PE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Videojuegos=b.NumClicks
					where b.categoria='videojuegos';

update CRM_bases.PE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.PE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Camaras=b.NumClicks
					where b.categoria='camaras';

update CRM_bases.PE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.PE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Ninos_y_Bebes=b.NumClicks
					where b.categoria='ninos-y-bebes';

update CRM_bases.PE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.PE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Cuidado_Personal_y_Salud=b.NumClicks
					where b.categoria='cuidado-personal-y-salud';

update CRM_bases.PE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.PE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Cuenta_Regresiva=b.NumClicks
					where b.categoria='cuenta-regresiva';

drop table if exists CRM_bases.PE_ClicksResumenPorMailPorCategoriaPCT;
create table CRM_bases.PE_ClicksResumenPorMailPorCategoriaPCT
select 	email,
				TYPE_CUSTOMER,
				Total_Clicks,
				Celulares_Telefonia_y_Gps/Total_Clicks as Celulares_Telefonia_y_Gps_PCT,
				Computadoras/Total_Clicks as Computadoras_PCT,
				Tv_Audio_y_Video/Total_Clicks as Tv_Audio_y_Video_PCT,
				Hogar/Total_Clicks as Hogar_PCT,
				Cac/Total_Clicks as Cac_PCT,
				Electrodomesticos/Total_Clicks as Electrodomesticos_PCT,
				Remate/Total_Clicks as Remate_PCT,
				Ropa_Calzado_y_Accesorios/Total_Clicks as Ropa_Calzado_y_Accesorios,
				Deportes/Total_Clicks as Deportes_PCT,
				Tecnologia/Total_Clicks as Tecnologia_PCT,
				Videojuegos/Total_Clicks as Videojuegos_PCT,
				Camaras/Total_Clicks as Camaras_PCT,
				Ninos_y_Bebes/Total_Clicks as Ninos_y_Bebes_PCT,
				Cuidado_Personal_y_Salud/Total_Clicks as Cuidado_Personal_y_Salud_PCT,
				Cuenta_Regresiva/Total_Clicks as Cuenta_Regresiva_PCT
	from CRM_bases.PE_ClicksResumenPorMailPorCategoria;

################VENEZUELA#######################

Drop table if EXISTS CRM_bases.VE_ClicksEmailDiaCategoria;
Create table CRM_bases.VE_ClicksEmailDiaCategoria
select 
			UniqueKey as email,
			`Dispatch-Start` as dia,
			`Mailing-ID` as MailingID,
			if(`Link-URL` like 'fashion','moda',
			if(
					LOCATE(	'/',
							substring(`Link-URL`,25))=0,
							'',
							left(substring(`Link-URL`,25),
							LOCATE('/',substring(`Link-URL`,25))-1)))
			as categoria
from CRM_VE.campaing_clicks 
  where `Link-URL` like 'http://www.linio.com.ve/%';

Drop table if EXISTS CRM_bases.VE_ClicksPorEmailPorCategoria;
Create table CRM_bases.VE_ClicksPorEmailPorCategoria
select 
			a.email,
			TYPE_CUSTOMER,
			categoria,
			count(distinct MailingID) as NumClicks
from CRM_bases.VE_ClicksEmailDiaCategoria a
	LEFT JOIN dev_marketing.customers_rev_ve b on a.email=b.email
	where a.email not like ' '
		and categoria not like ' '
	group by email,categoria;

create index email on CRM_bases.VE_ClicksPorEmailPorCategoria(email);

Drop table if EXISTS CRM_bases.VE_TotalClicksPorPersona;
Create table CRM_bases.VE_TotalClicksPorPersona
select 	email,
				sum(NumClicks) as Total_Clicks
	from CRM_bases.VE_ClicksPorEmailPorCategoria
		group by email;

create index email on CRM_bases.VE_TotalClicksPorPersona(email);

Drop table if EXISTS CRM_bases.VE_ClicksResumenPorMailPorCategoria;
Create table CRM_bases.VE_ClicksResumenPorMailPorCategoria
select 	distinct(email) as email,
				TYPE_CUSTOMER,
				0 as Total_Clicks,
				0 as Viernes_Locos,
				0 as Arma_Tu_Trio,
				0 as Celulares_Telefonos_y_Gps,
				0 as Tv_Audio_y_Video,
				0 as Computadoras_y_Tabletas,
				0 as Moda,
				0 as Hogar,
				0 as Electrodomesticos,
				0 as Campana_Cac,
				0 as Ultimas_Unidades,
				0 as Campana_Clientes_Cac,
				0 as Deportes,
				0 as Salud_Cuidado_Personal,
				0 as Precios_Sales,
				0 as Ninos_y_Bebes
		from CRM_bases.VE_ClicksPorEmailPorCategoria;

create index email on CRM_bases.VE_ClicksResumenPorMailPorCategoria(email);

update CRM_bases.VE_ClicksResumenPorMailPorCategoria a 
 INNER JOIN CRM_bases.VE_TotalClicksPorPersona b
		on a.email=b.email 
				set a.Total_Clicks=b.Total_Clicks;

update CRM_bases.VE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.VE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Viernes_Locos=b.NumClicks
					where b.categoria='viernes-locos';

update CRM_bases.VE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.VE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Arma_Tu_Trio=b.NumClicks
					where b.categoria='arma-tu-trio';

update CRM_bases.VE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.VE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Celulares_Telefonos_y_Gps=b.NumClicks
					where b.categoria='celulares-telefonos-y-gps';

update CRM_bases.VE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.VE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Tv_Audio_y_Video=b.NumClicks
					where b.categoria='tv-audio-y-video';

update CRM_bases.VE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.VE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Computadoras_y_Tabletas=b.NumClicks
					where b.categoria='computadoras-y-tabletas';

update CRM_bases.VE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.VE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Moda=b.NumClicks
					where b.categoria='moda';

update CRM_bases.VE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.VE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Hogar=b.NumClicks
					where b.categoria='hogar';

update CRM_bases.VE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.VE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Electrodomesticos=b.NumClicks
					where b.categoria='electrodomesticos';

update CRM_bases.VE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.VE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Campana_Cac=b.NumClicks
					where b.categoria='campana-cac';

update CRM_bases.VE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.VE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Ultimas_Unidades=b.NumClicks
					where b.categoria='ultimas-unidades';

update CRM_bases.VE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.VE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Campana_Clientes_Cac=b.NumClicks
					where b.categoria='campana-clientes-cac';

update CRM_bases.VE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.VE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Deportes=b.NumClicks
					where b.categoria='deportes';

update CRM_bases.VE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.VE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Salud_Cuidado_Personal=b.NumClicks
					where b.categoria='salud-cuidado-personal';

update CRM_bases.VE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.VE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Precios_Sales=b.NumClicks
					where b.categoria='precios-sales';

update CRM_bases.VE_ClicksResumenPorMailPorCategoria a 
	inner join CRM_bases.VE_ClicksPorEmailPorCategoria b
		on a.email=b.email 
				set a.Ninos_y_Bebes=b.NumClicks
					where b.categoria='ninos-y-bebes';

drop table if exists CRM_bases.VE_ClicksResumenPorMailPorCategoriaPCT;
create table CRM_bases.VE_ClicksResumenPorMailPorCategoriaPCT
select 	email,
				TYPE_CUSTOMER,
				Total_Clicks,
				Viernes_Locos/Total_Clicks as Viernes_Locos_PCT,
				Arma_Tu_Trio/Total_Clicks as Arma_Tu_Trio_PCT,
				Celulares_Telefonos_y_Gps/Total_Clicks as Celulares_Telefonos_y_Gps_PCT,
				Tv_Audio_y_Video/Total_Clicks as Tv_Audio_y_Video_PCT,
				Computadoras_y_Tabletas/Total_Clicks as Computadoras_y_Tabletas_PCT,
				Moda/Total_Clicks as Moda_PCT,
				Hogar/Total_Clicks as Hogar_PCT,
				Electrodomesticos/Total_Clicks as Electrodomesticos_PCT,
				Campana_Cac/Total_Clicks as Campana_Cac_PCT,
				Ultimas_Unidades/Total_Clicks as Ultimas_Unidades_PCT,
				Campana_Clientes_Cac/Total_Clicks as Campana_Clientes_Cac_PCT,
				Deportes/Total_Clicks as Deportes_PCT,
				Salud_Cuidado_Personal/Total_Clicks as Salud_Cuidado_Personal_PCT,
				Precios_Sales/Total_Clicks as Precios_Sales_PCT,
				Ninos_y_Bebes/Total_Clicks as Ninos_y_Bebes_PCT
	from CRM_bases.VE_ClicksResumenPorMailPorCategoria;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`borja.fernandez`@`%`*/ /*!50003 PROCEDURE `Rutina_Create_CO_Clicks_por_banner_en_cada_NL`()
BEGIN
#############Clicks por banner en cada NL###############

drop table if exists CRM_bases.CO_tmp_aux_sents_por_mailingID;
create TEMPORARY table CRM_bases.CO_tmp_aux_sents_por_mailingID
select 	date(`Dispatch-End`) as dia,
				`Mailing-Name` as campaign,
				`Mailing-ID` as MailingID,
				count(DISTINCT UniqueKey) as NumSents
	from CRM_CO.campaing_messages
		where year(`Dispatch-End`)=2014
				group by `Mailing-ID`;

create index MailingID on CRM_bases.CO_tmp_aux_sents_por_mailingID(mailingID);

drop table if exists CRM_bases.CO_tmp_aux_opens_por_mailingID;
create TEMPORARY table CRM_bases.CO_tmp_aux_opens_por_mailingID
select 	`Mailing-ID` as MailingID,
				count(DISTINCT UniqueKey) as NumOpens
	from CRM_CO.campaing_openings
		where year(`Dispatch-End`)=2014
				group by `Mailing-ID`;

create index MailingID on CRM_bases.CO_tmp_aux_opens_por_mailingID(mailingID);

drop table if exists CRM_bases.CO_click_por_banner_por_campaña;
create table CRM_bases.CO_click_por_banner_por_campaña
select  dia,
				`Mailing-ID` as MailingID,
				campaign,
				`Link-URL` as Banner_Link,
				count(DISTINCT UniqueKey) as NumClicks,
				NumOpens,
				NumSents
	from CRM_CO.campaing_clicks a
		RIGHT JOIN CRM_bases.CO_tmp_aux_sents_por_mailingID b on a.`Mailing-ID`=b.MailingID
			RIGHT JOIN CRM_bases.CO_tmp_aux_opens_por_mailingID c on a.`Mailing-ID`=c.MailingID
		where year(`Dispatch-End`)=2014
				group by `Mailing-ID`, `Link-URL`;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`borja.fernandez`@`%`*/ /*!50003 PROCEDURE `Rutina_Create_CRM_bases.Net_Rev_por_Canal_por_cat`()
BEGIN

drop table if EXISTS CRM_bases.Net_Rev_por_Canal_por_cat;
create table CRM_bases.Net_Rev_por_Canal_por_cat
select 	country,
				MonthNum as YrMonth,
				Cat1,
				Cat2,
				Cat3,
				CatKPI,
				CatBP,
				ChannelGroup,
				sum(PaidPrice)/18 as NetRev
from development_mx.A_Master
	where OrderAfterCan=1 and MonthNum like '2014%'
		group by country, MonthNum, Cat1, Cat2, Cat3, CatKPI, CatBP, ChannelGroup
UNION ALL
select 	country,
				MonthNum as YrMonth,
				Cat1,
				Cat2,
				Cat3,
				CatKPI,
				CatBP,
				ChannelGroup,
				sum(PaidPrice)/2450 as NetRev
from development_co_project.A_Master
	where OrderAfterCan=1 and MonthNum like '2014%'
		group by country, MonthNum, Cat1, Cat2, Cat3, CatKPI, CatBP, ChannelGroup
UNION ALL
select 	country,
				MonthNum as YrMonth,
				Cat1,
				Cat2,
				Cat3,
				CatKPI,
				CatBP,
				ChannelGroup,
				sum(PaidPrice)/3.3 as NetRev
from development_pe.A_Master
	where OrderAfterCan=1 and MonthNum like '2014%'
		group by country, MonthNum, Cat1, Cat2, Cat3, CatKPI, CatBP, ChannelGroup
UNION ALL
select 	country,
				MonthNum as YrMonth,
				Cat1,
				Cat2,
				Cat3,
				CatKPI,
				CatBP,
				ChannelGroup,
				sum(PaidPrice)/39 as NetRev
from development_ve.A_Master
	where OrderAfterCan=1 and MonthNum like '2014%'
		group by country, MonthNum, Cat1, Cat2, Cat3, CatKPI, CatBP, ChannelGroup;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`borja.fernandez`@`%`*/ /*!50003 PROCEDURE `Rutina_create_InterfacesUMSsummary`()
BEGIN
##create_InterfacesUMSsummary_v5

#############MEXICO###########################

Drop table if EXISTS CRM_bases.MX_InterfacesSents;
Create table CRM_bases.MX_InterfacesSents
	select 	'Mexico' as Country, 
					date(Timestamp) as dia,
					`Interface-ID` as Interface_ID,
					Alias, 
					count(distinct UniqueKey) as NumSent
		from CRM_MX.interface_messages
		group by date(Timestamp),`Interface-ID`; 

create index alias on CRM_bases.MX_InterfacesSents(alias);

Drop table if EXISTS CRM_bases.MX_InterfacesBounces;
Create table CRM_bases.MX_InterfacesBounces
	select 	'Mexico' as Country, 
					date(Timestamp) as dia,
					`Interface-ID` as Interface_ID,
					Alias, 
					count(distinct UniqueKey) as Total_Bounces
		from CRM_MX.interface_bounces
		group by date(Timestamp),`Interface-ID`; 

create index alias on CRM_bases.MX_InterfacesBounces(alias);

Drop table if EXISTS CRM_bases.MX_InterfacesOpens;
Create table CRM_bases.MX_InterfacesOpens
	select 	'Mexico' as Country, 
					date(Timestamp) as dia, 
					`Interface-ID` as Interface_ID,
					Alias, 
					count(distinct UniqueKey) as NumOpens
		from CRM_MX.interface_openings
		group by date(Timestamp),`Interface-ID`; 

create index alias on CRM_bases.MX_InterfacesOpens(alias);

Drop table if EXISTS CRM_bases.MX_InterfacesClicks;
Create table CRM_bases.MX_InterfacesClicks
	select 	'Mexico' as Country, 
					date(Timestamp) as dia, 
					`Interface-ID` as Interface_ID,
					Alias, 
					count(distinct UniqueKey) as NumClicks
		from CRM_MX.interface_clicks
		group by date(Timestamp),`Interface-ID`; 

create index alias on CRM_bases.MX_InterfacesClicks(alias);

Drop table if EXISTS CRM_bases.MX_InterfacesUMSsummary;
Create table CRM_bases.MX_InterfacesUMSsummary
select 	MX_InterfacesSents.Country, 
				MX_InterfacesSents.Dia, 
				MX_InterfacesSents.Interface_ID,
				MX_InterfacesSents.Alias, 
				MX_InterfacesSents.NumSent,
				MX_InterfacesBounces.Total_Bounces,
				0 as NumOpens,
				0 as NumClicks
	from CRM_bases.MX_InterfacesSents
		INNER JOIN CRM_bases.MX_InterfacesBounces 	on MX_InterfacesSents.Alias=MX_InterfacesBounces.Alias 
																										and MX_InterfacesSents.dia=MX_InterfacesBounces.dia 
		#INNER JOIN CRM_bases.MX_InterfacesOpens 		on MX_InterfacesSents.Alias=MX_InterfacesOpens.Alias
		#INNER JOIN CRM_bases.MX_InterfacesClicks		on MX_InterfacesSents.Alias=MX_InterfacesClicks.Alias
	group by Dia, Interface_ID;

update CRM_bases.MX_InterfacesUMSsummary i 
	inner join CRM_bases.MX_InterfacesOpens o 
		on i.Interface_ID=o.Interface_ID
			and i.dia=o.dia
				set i.NumOpens=o.NumOpens;

update CRM_bases.MX_InterfacesUMSsummary a 
	inner join CRM_bases.MX_InterfacesClicks b 
		on a.Interface_ID=b.Interface_ID
			and a.dia=b.dia
				set a.NumClicks=b.NumClicks;

#############COLOMBIA###########################

Drop table if EXISTS CRM_bases.CO_InterfacesSents;
Create table CRM_bases.CO_InterfacesSents
	select 	'Colombia' as Country, 
					date(Timestamp) as dia, 
					`Interface-ID` as Interface_ID,
					Alias, 
					count(distinct UniqueKey) as NumSent
		from CRM_CO.interface_messages
		group by date(Timestamp),Alias; 

create index alias on CRM_bases.CO_InterfacesSents(alias);

Drop table if EXISTS CRM_bases.CO_InterfacesBounces;
Create table CRM_bases.CO_InterfacesBounces
	select 	'Colombia' as Country, 
					date(Timestamp) as dia, 
					`Interface-ID` as Interface_ID, 
					Alias, 
					count(distinct UniqueKey) as Total_Bounces
		from CRM_CO.interface_bounces
		group by date(Timestamp),`Interface-ID`; 

create index alias on CRM_bases.CO_InterfacesBounces(alias);

Drop table if EXISTS CRM_bases.CO_InterfacesOpens;
Create table CRM_bases.CO_InterfacesOpens
	select 	'Colombia' as Country, 
					date(Timestamp) as dia, 
					`Interface-ID` as Interface_ID,  
					Alias, 
					count(distinct UniqueKey) as NumOpens
		from CRM_CO.interface_openings
		group by date(Timestamp),`Interface-ID`; 

create index alias on CRM_bases.CO_InterfacesOpens(alias);

Drop table if EXISTS CRM_bases.CO_InterfacesClicks;
Create table CRM_bases.CO_InterfacesClicks
	select 	'Colombia' as Country, 
					date(Timestamp) as dia,  
					`Interface-ID` as Interface_ID, 
					Alias, 
					count(distinct UniqueKey) as NumClicks
		from CRM_CO.interface_clicks
		group by date(Timestamp),`Interface-ID`; 

create index alias on CRM_bases.CO_InterfacesClicks(alias);

Drop table if EXISTS CRM_bases.CO_InterfacesUMSsummary;
Create table CRM_bases.CO_InterfacesUMSsummary
select 	CO_InterfacesSents.Country, 
				CO_InterfacesSents.Dia, 
				CO_InterfacesSents.Interface_ID,
				CO_InterfacesSents.Alias, 
				CO_InterfacesSents.NumSent,
				CO_InterfacesBounces.Total_Bounces,
				0 as NumOpens,
				0 as NumClicks
	from CRM_bases.CO_InterfacesSents
		INNER JOIN CRM_bases.CO_InterfacesBounces 	on CO_InterfacesSents.Alias=CO_InterfacesBounces.Alias
																									and CO_InterfacesSents.dia=CO_InterfacesBounces.dia
		#INNER JOIN CRM_bases.CO_InterfacesOpens 		on CO_InterfacesSents.Alias=CO_InterfacesOpens.Alias
		#INNER JOIN CRM_bases.CO_InterfacesClicks		on CO_InterfacesSents.Alias=CO_InterfacesClicks.Alias
	group by Dia, Interface_ID;

update CRM_bases.CO_InterfacesUMSsummary c 
	inner join CRM_bases.MX_InterfacesOpens d 
		on c.Interface_ID=d.Interface_ID
			and c.dia=d.dia
				set c.NumOpens=d.NumOpens;

update CRM_bases.MX_InterfacesUMSsummary e 
	inner join CRM_bases.MX_InterfacesClicks f 
		on e.Interface_ID=f.Interface_ID
			and e.dia=f.dia
				set e.NumClicks=f.NumClicks;

#############PERU###########################

Drop table if EXISTS CRM_bases.PE_InterfacesSents;
Create table CRM_bases.PE_InterfacesSents
	select 	'Peru' as Country, 
					date(Timestamp) as dia,   
					`Interface-ID` as Interface_ID, 
					Alias, 
					count(distinct UniqueKey) as NumSent
		from CRM_PE.interface_messages
		group by date(Timestamp),`Interface-ID`; 

create index alias on CRM_bases.PE_InterfacesSents(alias);

Drop table if EXISTS CRM_bases.PE_InterfacesBounces;
Create table CRM_bases.PE_InterfacesBounces
	select 	'Peru' as Country, 
					date(Timestamp) as dia,  
					`Interface-ID` as Interface_ID,  
					Alias, 
					count(distinct UniqueKey) as Total_Bounces
		from CRM_PE.interface_bounces
		group by date(Timestamp),`Interface-ID`; 

create index alias on CRM_bases.PE_InterfacesBounces(alias);

Drop table if EXISTS CRM_bases.PE_InterfacesOpens;
Create table CRM_bases.PE_InterfacesOpens
	select 	'Peru' as Country, 
					date(Timestamp) as dia,  
					`Interface-ID` as Interface_ID,  
					Alias, 
					count(distinct UniqueKey) as NumOpens
		from CRM_PE.interface_openings
		group by date(Timestamp),`Interface-ID`; 

create index alias on CRM_bases.PE_InterfacesOpens(alias);

Drop table if EXISTS CRM_bases.PE_InterfacesClicks;
Create table CRM_bases.PE_InterfacesClicks
	select 	'Peru' as Country, 
					date(Timestamp) as dia,   
					`Interface-ID` as Interface_ID, 
					Alias, 
					count(distinct UniqueKey) as NumClicks
		from CRM_PE.interface_clicks
		group by date(Timestamp),`Interface-ID`; 

create index alias on CRM_bases.PE_InterfacesClicks(alias);

Drop table if EXISTS CRM_bases.PE_InterfacesUMSsummary;
Create table CRM_bases.PE_InterfacesUMSsummary
select 	PE_InterfacesSents.Country, 
				PE_InterfacesSents.Dia, 
				PE_InterfacesSents.Interface_ID,
				PE_InterfacesSents.Alias, 
				PE_InterfacesSents.NumSent,
				PE_InterfacesBounces.Total_Bounces,
				0 as NumOpens,
				0 as NumClicks
	from CRM_bases.PE_InterfacesSents
		INNER JOIN CRM_bases.PE_InterfacesBounces 	on PE_InterfacesSents.Alias=PE_InterfacesBounces.Alias
																										and PE_InterfacesSents.dia=PE_InterfacesBounces.dia
		#INNER JOIN CRM_bases.PE_InterfacesOpens 		on PE_InterfacesSents.Alias=PE_InterfacesOpens.Alias
		#INNER JOIN CRM_bases.PE_InterfacesClicks		on PE_InterfacesSents.Alias=PE_InterfacesClicks.Alias
	group by Dia, Interface_ID;

update CRM_bases.MX_InterfacesUMSsummary g 
	inner join CRM_bases.MX_InterfacesOpens h 
		on g.Interface_ID=h.Interface_ID 
			and g.dia=h.dia
				set g.NumOpens=h.NumOpens;

update CRM_bases.MX_InterfacesUMSsummary i 
	inner join CRM_bases.MX_InterfacesClicks j 
		on i.Interface_ID=j.Interface_ID
			and i.dia=j.dia
				set i.NumClicks=j.NumClicks;

#############VENEZUELA###########################

Drop table if EXISTS CRM_bases.VE_InterfacesSents;
Create table CRM_bases.VE_InterfacesSents
	select 	'Venezuela' as Country, 
					date(Timestamp) as dia,    
					`Interface-ID` as Interface_ID, 
					Alias, 
					count(distinct UniqueKey) as NumSent
		from CRM_VE.interface_messages
		group by date(Timestamp),`Interface-ID`; 

create index alias on CRM_bases.VE_InterfacesSents(alias);

Drop table if EXISTS CRM_bases.VE_InterfacesBounces;
Create table CRM_bases.VE_InterfacesBounces
	select 	'Venezuela' as Country, 
					date(Timestamp) as dia,   
					`Interface-ID` as Interface_ID,  
					Alias, 
					count(distinct UniqueKey) as Total_Bounces
		from CRM_VE.interface_bounces
		group by date(Timestamp),`Interface-ID`; 

create index alias on CRM_bases.VE_InterfacesBounces(alias);

Drop table if EXISTS CRM_bases.VE_InterfacesOpens;
Create table CRM_bases.VE_InterfacesOpens
	select 	'Venezuela' as Country, 
					date(Timestamp) as dia,   
					`Interface-ID` as Interface_ID,  
					Alias, 
					count(distinct UniqueKey) as NumOpens
		from CRM_VE.interface_openings
		group by date(Timestamp),`Interface-ID`; 

create index alias on CRM_bases.VE_InterfacesOpens(alias);

Drop table if EXISTS CRM_bases.VE_InterfacesClicks;
Create table CRM_bases.VE_InterfacesClicks
	select 	'Venezuela' as Country, 
					date(Timestamp) as dia,    
					`Interface-ID` as Interface_ID, 
					Alias, 
					count(distinct UniqueKey) as NumClicks
		from CRM_VE.interface_clicks
		group by date(Timestamp),`Interface-ID`; 

create index alias on CRM_bases.VE_InterfacesClicks(alias);

Drop table if EXISTS CRM_bases.VE_InterfacesUMSsummary;
Create table CRM_bases.VE_InterfacesUMSsummary
select 	VE_InterfacesSents.Country, 
				VE_InterfacesSents.Dia,
				VE_InterfacesSents.Interface_ID,
				VE_InterfacesSents.Alias, 
				VE_InterfacesSents.NumSent,
				VE_InterfacesBounces.Total_Bounces,
				0 as NumOpens,
				0 as NumClicks
	from CRM_bases.VE_InterfacesSents
		INNER JOIN CRM_bases.VE_InterfacesBounces 	on VE_InterfacesSents.Alias=VE_InterfacesBounces.Alias
																									and VE_InterfacesSents.dia=VE_InterfacesBounces.dia
		#INNER JOIN CRM_bases.VE_InterfacesOpens 		on VE_InterfacesSents.Alias=VE_InterfacesOpens.Alias
		#INNER JOIN CRM_bases.VE_InterfacesClicks		on VE_InterfacesSents.Alias=VE_InterfacesClicks.Alias
	group by Dia, Interface_ID;

update CRM_bases.MX_InterfacesUMSsummary m 
	inner join CRM_bases.MX_InterfacesOpens n 
		on m.Interface_ID=n.Interface_ID
			and m.dia=n.dia
				set m.NumOpens=n.NumOpens;

update CRM_bases.MX_InterfacesUMSsummary o 
	inner join CRM_bases.MX_InterfacesClicks p 
		on o.Interface_ID=p.Interface_ID
			and o.dia=p.dia
				set o.NumClicks=p.NumClicks;

#################COMPLETE#######################

Drop table if EXISTS CRM_bases.Regional_InterfacesUMSsummary;
Create table CRM_bases.Regional_InterfacesUMSsummary
select * from CRM_bases.MX_InterfacesUMSsummary UNION ALL
select * from CRM_bases.CO_InterfacesUMSsummary UNION ALL
select * from CRM_bases.PE_InterfacesUMSsummary UNION ALL
select * from CRM_bases.VE_InterfacesUMSsummary;

#################################################################


				
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`borja.fernandez`@`%`*/ /*!50003 PROCEDURE `Rutina_Create_MX_Clicks_por_banner_en_cada_NL`()
BEGIN
#############Clicks por banner en cada NL###############

drop table if exists CRM_bases.MX_tmp_aux_sents_por_mailingID;
create TEMPORARY table CRM_bases.MX_tmp_aux_sents_por_mailingID
select 	date(`Dispatch-End`) as dia,
				`Mailing-Name` as campaign,
				`Mailing-ID` as MailingID,
				count(DISTINCT UniqueKey) as NumSents
	from CRM_MX.campaing_messages
		where year(`Dispatch-End`)=2014
				group by `Mailing-ID`;

create index MailingID on CRM_bases.MX_tmp_aux_sents_por_mailingID(mailingID);

drop table if exists CRM_bases.MX_tmp_aux_opens_por_mailingID;
create TEMPORARY table CRM_bases.MX_tmp_aux_opens_por_mailingID
select 	`Mailing-ID` as MailingID,
				count(DISTINCT UniqueKey) as NumOpens
	from CRM_MX.campaing_openings
		where year(`Dispatch-End`)=2014
				group by `Mailing-ID`;

create index MailingID on CRM_bases.MX_tmp_aux_opens_por_mailingID(mailingID);

drop table if exists CRM_bases.MX_click_por_banner_por_campaña;
create table CRM_bases.MX_click_por_banner_por_campaña
select  dia,
				`Mailing-ID` as MailingID,
				campaign,
				`Link-URL` as Banner_Link,
				count(DISTINCT UniqueKey) as NumClicks,
				NumOpens,
				NumSents
	from CRM_MX.campaing_clicks a
		RIGHT JOIN CRM_bases.MX_tmp_aux_sents_por_mailingID b on a.`Mailing-ID`=b.MailingID
			RIGHT JOIN CRM_bases.MX_tmp_aux_opens_por_mailingID c on a.`Mailing-ID`=c.MailingID
		where year(`Dispatch-End`)=2014
				group by `Mailing-ID`, `Link-URL`;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`borja.fernandez`@`%`*/ /*!50003 PROCEDURE `Rutina_Create_Performance_Report_with_Nick_Names`()
BEGIN

-- Create performance report with campaign's nick_names
-- Global Report (Charle's Performance Report)

DROP TABLE IF EXISTS CRM_bases.performance_report_summary_with_campaign_nick_names;
CREATE TABLE CRM_bases.performance_report_summary_with_campaign_nick_names SELECT country,
    yrmonth,
    week,
    `date`,
    campaign,
    CASE
        WHEN
            (campaign LIKE '%MIAC%'
                AND campaign NOT LIKE '%MIAC_REM1%'
                AND campaign NOT LIKE '%MIAC_REM24H%')
        THEN
            'MIAC'
        WHEN
            (campaign LIKE '%MIAC_REM1%'
                OR campaign LIKE '%MIAC_REM24H%')
        THEN
            'MIAC_REM'
        WHEN (campaign = 'Surf_History') THEN 'MISH'
        WHEN (campaign LIKE '%MISH%') THEN 'MISH_CID'
        WHEN (campaign LIKE '%MIRAO%') THEN 'MIRAO'
        WHEN
            (campaign LIKE '%Product_review%'
                OR campaign LIKE '%MIPR%'
                AND (campaign NOT LIKE '%MIPR_NBO%'
                OR campaign NOT LIKE '%MIPR_REC%'))
        THEN
            'MIPR'
        WHEN (campaign LIKE '%MIPR_REC%') THEN 'MIPR_REC'
        WHEN (campaign LIKE '%MIPR_NBO%') THEN 'MIPR_NBO'
        WHEN
            (campaign LIKE '%tepremia%'
                OR campaign LIKE '%MIVAO%')
        THEN
            'MIVAO'
        WHEN (campaign = 'MIRR_REC') THEN 'MIRR'
        WHEN
            (campaign LIKE '%tiempo_aire%'
                OR campaign LIKE '%MIRPP%'
                AND campaign NOT LIKE '%FRAGANCIAS%')
        THEN
            'MIRPP_AIRTIME'
        WHEN (campaign LIKE '%MIRPP_FRAGANCIAS%') THEN 'MIRPP_FRAGANCIAS'
        WHEN (campaign LIKE '%MIROOS%') THEN 'MIROOS'
        WHEN
            (campaign LIKE '%Birthday%'
                AND campaign NOT LIKE '%Birthday_Reminder%')
        THEN
            'BH_TODAY'
        WHEN (campaign LIKE '%Birthday_Reminder%') THEN 'BH_COMING_UP'
        WHEN (campaign LIKE '%MICSLL%') THEN 'MICSLL'
        WHEN (campaign LIKE '%CSS%') THEN 'CSS (NPS)'
        ELSE 'Other'
    END AS campaign_nick_name,
    visits,
    carts,
    gross_transactions,
    gross_revenue,
    net_transactions,
    net_revenue,
    `PC1`,
    `PC1.5`,
    `PC2`
FROM
    dev_marketing.performance_report;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`borja.fernandez`@`%`*/ /*!50003 PROCEDURE `Rutina_Create_PE_Clicks_por_banner_en_cada_NL`()
BEGIN
#############Clicks por banner en cada NL###############

drop table if exists CRM_bases.PE_tmp_aux_sents_por_mailingID;
create TEMPORARY table CRM_bases.PE_tmp_aux_sents_por_mailingID
select 	date(`Dispatch-End`) as dia,
				`Mailing-Name` as campaign,
				`Mailing-ID` as MailingID,
				count(DISTINCT UniqueKey) as NumSents
	from CRM_PE.campaing_messages
		where year(`Dispatch-End`)=2014
				group by `Mailing-ID`;

create index MailingID on CRM_bases.PE_tmp_aux_sents_por_mailingID(mailingID);

drop table if exists CRM_bases.PE_tmp_aux_opens_por_mailingID;
create TEMPORARY table CRM_bases.PE_tmp_aux_opens_por_mailingID
select 	`Mailing-ID` as MailingID,
				count(DISTINCT UniqueKey) as NumOpens
	from CRM_PE.campaing_openings
		where year(`Dispatch-End`)=2014
				group by `Mailing-ID`;

create index MailingID on CRM_bases.PE_tmp_aux_opens_por_mailingID(mailingID);

drop table if exists CRM_bases.PE_click_por_banner_por_campaña;
create table CRM_bases.PE_click_por_banner_por_campaña
select  dia,
				`Mailing-ID` as MailingID,
				campaign,
				`Link-URL` as Banner_Link,
				count(DISTINCT UniqueKey) as NumClicks,
				NumOpens,
				NumSents
	from CRM_PE.campaing_clicks a
		RIGHT JOIN CRM_bases.PE_tmp_aux_sents_por_mailingID b on a.`Mailing-ID`=b.MailingID
			RIGHT JOIN CRM_bases.PE_tmp_aux_opens_por_mailingID c on a.`Mailing-ID`=c.MailingID
		where year(`Dispatch-End`)=2014
				group by `Mailing-ID`, `Link-URL`;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`borja.fernandez`@`%`*/ /*!50003 PROCEDURE `Rutina_Create_Summary_Orders_with_ID_and_sms`()
BEGIN



drop table if exists CRM_bases.aux2_CAC_orders;
create table CRM_bases.aux2_CAC_orders
select 	country,
				date,
				MonthNum,
				CustomerNum,
				OrderNum,
				CASE
					when CouponCode like 'CAC%'
						then 'CACCustomer'
					else 'no_CAC' end
				as CACCustomer,
				NewReturning,
				CouponCode,
				sum(PaidPrice) as PaidPrice,
				sum(PCOnePFive) as PCOnePFive,
				sms,
				national_registration_number
from development_mx.A_Master
left JOIN dev_marketing.customers_rev_mx on CustomerNum=custid
left join bob_live_mx.customer on CustomerNum=id_customer
where OrderAfterCan=1
group by OrderNum
	
Union ALL


select 	country,
				date,
				MonthNum,
				CustomerNum,
				OrderNum,
				CASE
					when CouponCode like 'CAC%'
						then 'CACCustomer'
					else 'no_CAC' end
				as CACCustomer,
				NewReturning,
				CouponCode,
				sum(PaidPrice) as PaidPrice,
				sum(PCOnePFive) as PCOnePFive,
				sms,
				national_registration_number
from development_co.A_Master
left JOIN dev_marketing.customers_rev_co on CustomerNum=custid
left join bob_live_co.customer on CustomerNum=id_customer
where OrderAfterCan=1
group by OrderNum


Union ALL


select 	country,
				date,
				MonthNum,
				CustomerNum,
				OrderNum,
				CASE
					when CouponCode like 'CAC%'
						then 'CACCustomer'
					else 'no_CAC' end
				as CACCustomer,
				NewReturning,
				CouponCode,
				sum(PaidPrice) as PaidPrice,
				sum(PCOnePFive) as PCOnePFive,
				sms,
				national_registration_number
from development_pe.A_Master
left JOIN dev_marketing.customers_rev_pe on CustomerNum=custid
left join bob_live_pe.customer on CustomerNum=id_customer
where OrderAfterCan=1
group by OrderNum

Union ALL

select 	country,
				date,
				MonthNum,
				CustomerNum,
				OrderNum,
				CASE
					when CouponCode like 'CAC%'
						then 'CACCustomer'
					else 'no_CAC' end
				as CACCustomer,
				NewReturning,
				CouponCode,
				sum(PaidPrice) as PaidPrice,
				sum(PCOnePFive) as PCOnePFive,
				sms,
				national_registration_number
from development_ve.A_Master
left JOIN dev_marketing.customers_rev_ve on CustomerNum=custid
left join bob_live_ve.customer on CustomerNum=id_customer
where OrderAfterCan=1
group by OrderNum;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`borja.fernandez`@`%`*/ /*!50003 PROCEDURE `Rutina_Create_Vaciados`()
BEGIN

###Creacion tabla agrupar datos performance report por campaña###

drop table if exists CRM_bases.performance_report_agrupado_por_campaña;
create table CRM_bases.performance_report_agrupado_por_campaña
select 	country,
				campaign,
				sum(visits) as TotalVisits,
				sum(carts) as TotalCarts,
				sum(gross_transactions) as TotalGrossTransactions,
				sum(gross_revenue) as TotalGrossRevenue,
				sum(gross_items) as TotalGrossItems,
				sum(gross_revenue)/sum(gross_transactions) as avg_gross_rev,
				sum(gross_transactions)/sum(visits) as gross_conversion_rate,
				sum(net_transactions) as TotalNetTransactions,
				sum(net_revenue) as TotalNetRevenue,
				sum(net_items) as TotalNetItems,
				sum(net_revenue)/sum(net_transactions) as avg_net_rev,
				sum(net_transactions)/sum(visits) as net_conversion_rate,
				sum(PC1) as PC1,
				sum(`PC1.5`) as `PC1.5`,
				sum(PC2) as PC2,
				sum(PC1)/sum(net_revenue) as `%PC1`,
				sum(`PC1.5`)/sum(net_revenue) as `%PC1.5`,
				sum(PC2)/sum(net_revenue) as `%PC2`
				from dev_marketing.performance_report
group by campaign,country;

####Creacion tabla joining datos de Experian con datos de performance####

drop table if exists CRM_bases.vaciados;
create table CRM_bases.vaciados
select 	a.country,
				concat(year(Day),month(Day)) as yrmonth,
				production.week_iso(Day) as week,
				Day,
				MailingID,
				a.campaign,
				a.campaign_nick_name,
				NumSent,
				TotalBounces,
				NumOpens,
				NumClicks,
				TotalVisits,
				TotalCarts,
				TotalGrossTransactions,
				TotalGrossRevenue,
				TotalGrossItems,
				avg_gross_rev,
				gross_conversion_rate,
				TotalNetTransactions,
				TotalNetRevenue,
				TotalNetItems,
				avg_net_rev,
				net_conversion_rate,
				PC1,
				`PC1.5`,
				PC2,
				`%PC1`,
				`%PC1.5`,
				`%PC2`
	from 	CRM_bases.Regional_CampaignUMSsummary a	
		left join CRM_bases.performance_report_agrupado_por_campaña b
			on a.country=b.country and a.campaign=b.campaign;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`borja.fernandez`@`%`*/ /*!50003 PROCEDURE `Rutina_Create_VE_Clicks_por_banner_en_cada_NL`()
BEGIN
#############Clicks por banner en cada NL###############

drop table if exists CRM_bases.VE_tmp_aux_sents_por_mailingID;
create TEMPORARY table CRM_bases.VE_tmp_aux_sents_por_mailingID
select 	date(`Dispatch-End`) as dia,
				`Mailing-Name` as campaign,
				`Mailing-ID` as MailingID,
				count(DISTINCT UniqueKey) as NumSents
	from CRM_VE.campaing_messages
		where year(`Dispatch-End`)=2014
				group by `Mailing-ID`;

create index MailingID on CRM_bases.VE_tmp_aux_sents_por_mailingID(mailingID);

drop table if exists CRM_bases.VE_tmp_aux_opens_por_mailingID;
create TEMPORARY table CRM_bases.VE_tmp_aux_opens_por_mailingID
select 	`Mailing-ID` as MailingID,
				count(DISTINCT UniqueKey) as NumOpens
	from CRM_VE.campaing_openings
		where year(`Dispatch-End`)=2014
				group by `Mailing-ID`;

create index MailingID on CRM_bases.VE_tmp_aux_opens_por_mailingID(mailingID);

drop table if exists CRM_bases.VE_click_por_banner_por_campaña;
create table CRM_bases.VE_click_por_banner_por_campaña
select  dia,
				`Mailing-ID` as MailingID,
				campaign,
				`Link-URL` as Banner_Link,
				count(DISTINCT UniqueKey) as NumClicks,
				NumOpens,
				NumSents
	from CRM_VE.campaing_clicks a
		RIGHT JOIN CRM_bases.VE_tmp_aux_sents_por_mailingID b on a.`Mailing-ID`=b.MailingID
			RIGHT JOIN CRM_bases.VE_tmp_aux_opens_por_mailingID c on a.`Mailing-ID`=c.MailingID
		where year(`Dispatch-End`)=2014
				group by `Mailing-ID`, `Link-URL`;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`borja.fernandez`@`%`*/ /*!50003 PROCEDURE `Rutina_Diaria_Borja_Step1`()
BEGIN
call CRM_bases.Rutina_Create_CampaignUMSsummary;
select "CampaignUMSsummary-OK";

call CRM_bases.Rutina_create_InterfacesUMSsummary;
select "InterfacesUMSsummary-OK";

call CRM_bases.Rutina_Create_Performance_Report_with_Nick_Names;
select "Performance_Report_with_Nick_Names-OK";

call CRM_bases.Rutina_Create_MX_Clicks_por_banner_en_cada_NL;
select "MX_Clicks_por_banner_en_cada_NL-OK";

call CRM_bases.Rutina_Create_CO_Clicks_por_banner_en_cada_NL;
select "CO_Clicks_por_banner_en_cada_NL-OK";

call CRM_bases.Rutina_Create_PE_Clicks_por_banner_en_cada_NL;
select "PE_Clicks_por_banner_en_cada_NL-OK";

call CRM_bases.Rutina_Create_VE_Clicks_por_banner_en_cada_NL;
select "VE_Clicks_por_banner_en_cada_NL-OK";

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`borja.fernandez`@`%`*/ /*!50003 PROCEDURE `Rutina_Diaria_Borja_Step2`()
BEGIN

call CRM_bases.Rutina_create_cf_report;
select "create_cf_report-OK";

call CRM_bases.Rutina_Create_Vaciados;
select "Create_Vaciados-OK";

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:03:19
