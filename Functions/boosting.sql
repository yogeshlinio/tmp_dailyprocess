-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: boosting
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'boosting'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `1_Gather_Boosting_Info_MX`()
BEGIN
		#Create Table
	DROP TABLE IF EXISTS boosting.Boost_Catalog_MX;
	CREATE TABLE boosting.Boost_Catalog_MX(
		#General
		id MEDIUMINT NOT NULL AUTO_INCREMENT,
		SKU_Config VARCHAR(80) NOT NULL,
		SKU_Name VARCHAR(250) NOT NULL,
		Catalog VARCHAR(250) NOT NULL,
		Average_Position DECIMAL (15,5) NOT NULL,
		Last_Position INT,
		Last_Boost INT,
		Novelty INT NOT NULL,
		Days_Visible INT NOT NULL,
		Is_Visible tinyint DEFAULT 0,
		Is_Market_Place BIT(1) NOT NULL,
		Is_Sell_List BIT(1) NOT NULL,
		Is_Consignment SMALLINT NOT NULL,
		Is_Bundle tinyint DEFAULT 0,
		 
		#Commercial REPORT
		Items_Sold_Last_30_Days   INT NOT NULL,
		PC1_Last_30_Days     DECIMAL(15,5) NOT NULL,
		PC1_5_Last_30_Days   DECIMAL(15,5) NOT NULL,
		PC2_Last_30_Days     DECIMAL(15,5) NOT NULL,
		Revenue_Last_30_Days DECIMAL(15,5) NOT NULL,
	
		#Stock
		Inventory_Stock_Type         VARCHAR(15) NOT NULL,
		Inventory_Stock_Revenue        DECIMAL(15,5) NOT NULL,
		Inventory_Stock_Items INT NOT NULL,
		Days_In_Inventory_Stock INT NOT NULL,
		Supplier_Stock INT NOT NULL,
		
		#Daily data
		Per_Day_Items    DECIMAL(15,5) NOT NULL,
		Per_Day_Revenue     DECIMAL(15,5) NOT NULL,
		Per_Day_PC1_5 DECIMAL(15,5) NOT NULL,
		
		#Bob live pricing
		Price DECIMAL(15,5) NOT NULL,
		Discount DECIMAL(15,5),
		
		PRIMARY KEY (id)
	);
	CREATE INDEX SKU_Config_And_Catalog on boosting.Boost_Catalog_MX(SKU_Config, Catalog);
	CREATE INDEX SKU_Config on boosting.Boost_Catalog_MX(SKU_Config);

	#Clan empty entrences for the Boost_Product_Position_In_Catalog_MX table
	DELETE FROM boosting.Boost_Product_Position_In_Catalog_MX WHERE SKU_Config = "";
	DELETE FROM boosting.Boost_Product_Position_In_Catalog_MX WHERE Catalog = "";
	
	#INSERT SKUS WITH VISIBLE IN THE LAS 30 DAYS AND ARE ACTIVE IN WEIGHTS
	INSERT boosting.Boost_Catalog_MX(SKU_Config, Catalog, Average_Position)
	SELECT 
		SKU_Config,
		a.Catalog,
		AVG(numb_of_position) AS Average_Position
	FROM boosting.Boost_Product_Position_In_Catalog_MX a
	INNER JOIN boosting.Boost_Weights_MX b ON a.Catalog = b.Catalog
	WHERE  date_time >=  NOW() - INTERVAL 5 DAY AND b.Active = 1
	GROUP BY Catalog, SKU_Config;
	
	#Days visible for daily data. (Set of 30 days)
	UPDATE boosting.Boost_Catalog_MX a
	INNER JOIN (SELECT  SKU_Config, COUNT(DISTINCT DATE(date_time)) AS days
		FROM boosting.Boost_Product_Position_In_Catalog_MX
		WHERE date_time >=  NOW() - INTERVAL 30 DAY
		GROUP   BY  SKU_Config) b
		ON a.SKU_Config = b.SKU_Config
	SET 
		a.Days_Visible =  b.days;

	#Exclude deleted SKUs
	DELETE FROM boosting.Boost_Catalog_MX
	WHERE SKU_Config NOT IN (SELECT SKU FROM bob_live_mx.catalog_config);
	
	#INSERT LAST POSITION
	UPDATE boosting.Boost_Catalog_MX a
	INNER JOIN (SELECT SKU_Config, numb_of_position, catalog
		FROM boosting.Boost_Product_Position_In_Catalog_MX
		GROUP BY id ORDER BY date_time DESC) AS b
			ON a.SKU_Config = b.SKU_Config AND a.Catalog = b.catalog
		SET
			a.Last_Position =  b.numb_of_position;
	
	#LAST BOOST
	DROP TABLE IF EXISTS boosting.Boost_TMP;
	CREATE TABLE boosting.Boost_TMP (PRIMARY KEY( SKU_Config ))
	SELECT
		bob_live_mx.catalog_config.sku AS SKU_Config,
		bob_live_mx.catalog_product_boost.boost AS Last_Boost
	FROM
		bob_live_mx.catalog_product_boost
	INNER JOIN bob_live_mx.catalog_config ON catalog_product_boost.fk_catalog_config = catalog_config.id_catalog_config;
	
	UPDATE boosting.Boost_Catalog_MX a, boosting.Boost_TMP b
		SET
			a.Last_Boost = b.Last_Boost
		WHERE a.SKU_Config = b.SKU_Config;
	
	#UPDATE COMMETCIAL
	DROP TABLE IF EXISTS boosting.Boost_TMP;
	CREATE TABLE boosting.Boost_TMP (PRIMARY KEY( SKU_Config ))
	SELECT
		 Sku_Config,
		 SKU_Name,
		 isVisible AS Is_Visible,
		 isMarketPlace AS Is_Market_Place
	FROM development_mx.A_Master_Catalog
	GROUP BY Sku_Config;
	CREATE INDEX SKU_Config ON boosting.Boost_TMP(SKU_Config);

	UPDATE boosting.Boost_Catalog_MX a, boosting.Boost_TMP b
		SET
			a.SKU_Name = b.SKU_Name,
			a.Is_Visible = b.Is_Visible,
			a.Is_Market_Place = b.Is_Market_Place
		WHERE a.SKU_Config = b.SKU_Config;

	#SELL LIST
	DROP TABLE IF EXISTS boosting.Boost_TMP;
	CREATE TABLE boosting.Boost_TMP (PRIMARY KEY( SKU_Config ))
	SELECT
		Sku_Config,
		max_days_in_stock,
		average_remaining_days
	FROM operations_mx.out_stock_hist
	WHERE (in_stock = 1 AND
				reserved = 0 AND
				fulfillment_type_bob <> 'Consignment' AND
				max_days_in_stock > 30 AND
				average_remaining_days > 90) OR
				(in_stock = 1 AND
				reserved = 0 AND
				fulfillment_type_bob <> 'Consignment' AND
				max_days_in_stock > 30 AND
				average_remaining_days IS NULL)
	GROUP BY SKU_Config;

	UPDATE boosting.Boost_Catalog_MX a, boosting.Boost_TMP b
		SET
			a.Is_Sell_List = "1"
		WHERE a.SKU_Config = b.SKU_Config;
	
	#CONSIGNMENT
	DROP TABLE IF EXISTS boosting.Boost_TMP;
	CREATE TABLE boosting.Boost_TMP (PRIMARY KEY( SKU_Config ))
	SELECT
		Sku_Config,
		fulfillment_type_bob
	FROM operations_mx.out_stock_hist
	WHERE in_stock = 1 AND
				reserved = 0 AND
				fulfillment_type_bob = 'Consignment'
	GROUP BY Sku_Config;

	UPDATE boosting.Boost_Catalog_MX a, boosting.Boost_TMP b
		SET
			a.Is_Consignment = 1
		WHERE a.SKU_Config = b.SKU_Config;
	
	#BUNDLES
	#Products that have a free product attached need to be tracked because the SKU doesn't show sales.
	UPDATE boosting.Boost_Catalog_MX a, bob_live_mx.catalog_config b
		SET
			a.Is_Bundle = 1
		WHERE b.description LIKE '%"bundle-data"%' AND a.SKU_Config = b.sku;

	UPDATE boosting.Boost_Catalog_MX a, (SELECT LEFT(sku,15)as SKU_Config FROM bob_live_mx.bundle_item b
		INNER JOIN (SELECT id_bundle,status FROM  bob_live_mx.bundle ) c
		ON b.fk_bundle = c.id_bundle
		WHERE b.leader = 1 AND c.status = "active") d
		SET
			a.Is_Bundle = 1
		WHERE 
			a.SKU_Config = d.SKU_Config;

	#UPDATE Stock
	UPDATE boosting.Boost_Catalog_MX a
		 INNER JOIN ( SELECT SKU_Config, 
												 fulfillment_type_real,
												 SUM(cost_w_o_vat) AS cost_w_o_vat,
												 AVG(days_in_stock) AS daysInStock
										FROM operations_mx.out_stock_hist
										WHERE in_stock = 1 AND reserved = 0
								 GROUP BY sku_config ) b 
						 ON a.SKU_Config = b.SKU_Config
	SET
		 a.Inventory_Stock_Type = b.fulfillment_type_real,
		 a.Inventory_Stock_Revenue = b.cost_w_o_vat,
		 a.Days_In_Inventory_Stock = b.daysInStock;
	
	DROP TABLE IF EXISTS boosting.Boost_TMP;
	CREATE TABLE boosting.Boost_TMP(
		SKU_Config VARCHAR(255),
		Warehouse_Stock INT,
		Supplier_Stock INT,
		Item_Reserved INT,
		Inventory_Stock_Items INT
	);
	CREATE INDEX SKU_Config ON boosting.Boost_TMP(SKU_Config);
	INSERT INTO boosting.Boost_TMP (SKU_Config, Warehouse_Stock, Supplier_Stock, Item_Reserved)
	SELECT 	LEFT(catalog_simple.sku,15) AS SKU_Config,
					SUM(IFNULL((SELECT CAST(quantity AS SIGNED INT) FROM bob_live_mx.catalog_warehouse_stock WHERE catalog_warehouse_stock.fk_catalog_simple = catalog_simple.id_catalog_simple),0)) AS Warehouse_Stock,
					SUM(IFNULL((SELECT CAST(quantity AS SIGNED INT) FROM bob_live_mx.catalog_supplier_stock WHERE catalog_supplier_stock.fk_catalog_simple = catalog_simple.id_catalog_simple),0)) AS Supplier_Stock,
					SUM(((SELECT COUNT(*) FROM bob_live_mx.sales_order_item JOIN bob_live_mx.sales_order ON fk_sales_order = id_sales_order WHERE fk_sales_order_process IN (33,5,27,26,31,8,23,28,32,34,29,9,30) AND is_reserved = 1 AND sales_order_item.sku = catalog_simple.sku) + IFNULL((SELECT SUM(catalog_stock_shared.reserved) FROM bob_live_mx.catalog_stock_shared WHERE catalog_stock_shared.sku = catalog_simple.sku GROUP BY catalog_stock_shared.sku), 0))) AS Item_Reserved
		FROM bob_live_mx.catalog_simple GROUP BY SKU_Config;
	
	UPDATE boosting.Boost_TMP	
		SET Inventory_Stock_Items = Warehouse_Stock - Item_Reserved;

	UPDATE boosting.Boost_Catalog_MX a, boosting.Boost_TMP b
		SET
			a.Inventory_Stock_Items = b.Inventory_Stock_Items,
			a.Supplier_Stock = b.Supplier_Stock
		WHERE a.SKU_Config = b.SKU_Config;
	
	#COMMERCIAL STUFF
	DROP TABLE IF EXISTS boosting.Boost_TMP;
	CREATE TABLE boosting.Boost_TMP (PRIMARY KEY( SKU_Config ))
	SELECT
		 SkuConfig AS SKU_Config,
		 COUNT(*) AS Items_Sold_Last_30_Days,
		 SUM(PCOne) AS PC1_Last_30_Days,
		 SUM(PCOnePFive ) AS PC1_5_Last_30_Days,
		 SUM(PCOnePFive + FLWHCost + FLCSCost) AS PC2_Last_30_Days,
		 SUM(Rev) AS Revenue_Last_30_Days
	FROM
		 development_mx.A_Master
	WHERE
			 OrderAfterCan = 1
	 AND Date >= NOW() - INTERVAL 30 DAY
	GROUP BY SKU_Config;
	CREATE INDEX SKU_Config ON boosting.Boost_TMP(SKU_Config);

	UPDATE boosting.Boost_Catalog_MX a
		 INNER JOIN boosting.Boost_TMP b
					ON a.SKU_Config = b.SKU_Config
	SET
		a.Items_Sold_Last_30_Days = b.Items_Sold_Last_30_Days, 
		a.PC1_Last_30_Days = b.PC1_Last_30_Days, 
		a.PC1_5_Last_30_Days = b.PC1_5_Last_30_Days, 
		a.PC2_Last_30_Days = b.PC2_Last_30_Days, 
		a.Revenue_Last_30_Days = b.Revenue_Last_30_Days;

	#NOVELTY
	DROP TABLE IF EXISTS boosting.Boost_TMP;
	CREATE TABLE boosting.Boost_TMP (PRIMARY KEY( SKU_Config ))
	SELECT
			sku AS SKU_Config,
			created_at
	FROM
		 bob_live_mx.catalog_config
	GROUP BY sku;
	CREATE INDEX SKU_Config ON boosting.Boost_TMP(SKU_Config);

	UPDATE boosting.Boost_Catalog_MX a, boosting.Boost_TMP b
		SET
			a.Novelty = DATEDIFF(CURDATE(), b.created_at)
		WHERE a.SKU_Config = b.SKU_Config;

	#SET SKUS BASED ON THE Days_Visible
	UPDATE boosting.Boost_Catalog_MX
	SET
		Per_Day_Items = Items_Sold_Last_30_Days/Days_Visible,
		Per_Day_Revenue = Revenue_Last_30_Days/Days_Visible,
		Per_Day_PC1_5	= PC1_5_Last_30_Days/Days_Visible;
	
	#SET PRICE
	DROP TABLE IF EXISTS boosting.Boost_TMP;
	CREATE TABLE boosting.Boost_TMP (PRIMARY KEY( SKU_Config ))
	SELECT
		LEFT(sku,15) AS SKU_Config,
		price AS Price
	FROM bob_live_mx.catalog_simple
	GROUP BY SKU_Config;
	CREATE INDEX SKU_Config ON boosting.Boost_TMP(SKU_Config);
	
	UPDATE boosting.Boost_Catalog_MX a, boosting.Boost_TMP b
		SET
			a.Price = b.Price
		WHERE a.SKU_Config = b.SKU_Config;

	#SET DISCOUNT
	DROP TABLE IF EXISTS boosting.Boost_TMP;
	CREATE TABLE boosting.Boost_TMP (PRIMARY KEY( SKU_Config ))
	SELECT
		LEFT(sku,15) AS SKU_Config,
		(price-special_price)/price AS Discount
	FROM bob_live_mx.catalog_simple
		WHERE special_from_date < NOW() AND NOW() < special_to_date
	GROUP BY SKU_Config;
	CREATE INDEX SKU_Config ON boosting.Boost_TMP(SKU_Config);
	
	UPDATE boosting.Boost_Catalog_MX a, boosting.Boost_TMP b
		SET
			a.Discount = b.Discount
		WHERE a.SKU_Config = b.SKU_Config;
	DROP TABLE IF EXISTS boosting.Boost_TMP;
	
	#Separete into cetegories and calculate boost scores.
	CALL boosting.2_Boost_Existing_Catalogs_MX();
	
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `2_Boost_Existing_Catalogs_MX`()
BEGIN
	#CREATES ALL THE INDIVIDUAL TABLES FOR ALL EXISTING CATALOGS
	DECLARE done INT DEFAULT 0;
  DECLARE v_cat VARCHAR(50) DEFAULT NULL;
  #declare cursor
  DECLARE cur1 CURSOR FOR SELECT DISTINCT catalog FROM  boosting.Boost_Catalog_MX;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

	DROP TABLE IF EXISTS boosting.Final_Boost_MX;
		CREATE TABLE boosting.Final_Boost_MX(
			Catalog VARCHAR(250) NOT NULL,
			TopBucket INT NOT NULL,
			SKU_Config VARCHAR(250) NOT NULL,
			Score INT NOT NULL
		);

  OPEN cur1;
		cur1_loop:LOOP
			FETCH cur1 INTO v_cat;
			IF done = 1 THEN
				LEAVE cur1_loop;
			END IF;
			CALL boosting.3_Create_Catalog_Table_MX(v_cat);
		END LOOP cur1_loop;
  CLOSE cur1;
	SET done = 0;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `3_Create_Catalog_Table_MX`(IN cat VARCHAR(255))
BEGIN
	#CREATES A TABLE WITH THE FINAL BOOST FOR THAT CATALOG. 
	#Final_Boost_CO WILL RECIEVE THE SKU, CATALOG AND SCORE TO EASILY UPLOAD TO BOB.
	SET @CatalogName = cat;
	
	#CREATE TABLE
	SET @s = CONCAT('DROP TABLE IF EXISTS boosting.Boost_Catalog_',@CatalogName);
	PREPARE stmt1 FROM @s; EXECUTE stmt1; DEALLOCATE PREPARE stmt1; 
	SET @s = CONCAT('CREATE TABLE boosting.Boost_Catalog_',@CatalogName,
	'(
		#General
		SKU_Config VARCHAR(250) NOT NULL,
		SKU_Name VARCHAR(250) NOT NULL,
		Average_Position DECIMAL (15,5) NOT NULL,
		Last_Position INT,
		New_Position INT NOT NULL,
		Last_Boost INT,
		Novelty INT NOT NULL,
		Days_Visible INT NOT NULL,
		Is_Visible BIT(1) NOT NULL,
		Is_Market_Place BIT(1) NOT NULL,
		Is_Sell_List BIT(1) NOT NULL,
		Is_Consignment SMALLINT NOT NULL,
		Is_Bundle BIT(1) NOT NULL,
		 
		#Commercial REPORT
		Items_Sold_Last_30_Days   INT NOT NULL,
		PC1_Last_30_Days     DECIMAL(15,5) NOT NULL,
		PC1_5_Last_30_Days   DECIMAL(15,5) NOT NULL,
		PC2_Last_30_Days     DECIMAL(15,5) NOT NULL,
		Revenue_Last_30_Days DECIMAL(15,5) NOT NULL,
		
		#Stock
		Inventory_Stock_Type VARCHAR(15) NOT NULL,
		Inventory_Stock_Items INT NOT NULL,
		Inventory_Stock_Revenue DECIMAL(15,5) NOT NULL,
		Days_In_Inventory_Stock INT NOT NULL,
		Supplier_Stock INT NOT NULL,
		
		#Divided by Days_Visible
		Per_Day_Items     DECIMAL(15,5) NOT NULL,
		Per_Day_Revenue     DECIMAL(15,5) NOT NULL,
		Per_Day_PC1_5 DECIMAL(15,5) NOT NULL,
		
		#PRICING
		Price DECIMAL(15,5) NOT NULL,
		Discount DECIMAL(15,5) NOT NULL,
		
		#NORMALIZED DATA
		Novelty_Norm DECIMAL(15,7) NOT NULL,
		Is_Market_Place_Norm DECIMAL(15,7) NOT NULL,
		Is_Sell_List_Norm DECIMAL(15,7) NOT NULL,
		IS_Consignment_Norm DECIMAL(15,7) NOT NULL,
		Is_Bundle_Norm DECIMAL(15,7) NOT NULL,
		Inventory_Stock_Items_Norm DECIMAL(15,7) NOT NULL,
		Inventory_Stock_Revenue_Norm DECIMAL(15,7) NOT NULL,
		Days_In_Inventory_Stock_Norm DECIMAL(15,7) NOT NULL,
		Supplier_Stock_Norm DECIMAL(15,7) NOT NULL,
		Per_Day_Items_Norm DECIMAL(15,7) NOT NULL,
		Per_Day_Revenue_Norm DECIMAL(15,7) NOT NULL,
		Per_Day_PC1_5_Norm DECIMAL(15,7) NOT NULL,
		Price_Norm DECIMAL(15,7) NOT NULL,
		Discount_Norm DECIMAL(15,7) NOT NULL,
		
		#Boost order
		Boost_Order DECIMAL(15,5) NOT NULL,

		#Fixed Boost
		Fixed_Boost INT,
		
		#Final Boost
		Final_Boost INT,
	 
		PRIMARY KEY (SKU_Config)
	);');
	PREPARE stmt2 FROM @s; EXECUTE stmt2; DEALLOCATE PREPARE stmt2;
	
	#INSERT SKUS OF THIS CATALOG
	SET @s = CONCAT('INSERT INTO boosting.Boost_Catalog_',@CatalogName, '(
		SKU_Config,
		SKU_Name,
		Average_Position,
		Last_Position,
		Last_Boost,
		Novelty,
		Days_Visible,
		Is_Visible,
		Is_Market_Place,
		Is_Sell_List,
		Is_Consignment,
		Is_Bundle,
		Items_Sold_Last_30_Days,
		PC1_Last_30_Days,
		PC1_5_Last_30_Days,
		PC2_Last_30_Days,
		Revenue_Last_30_Days,
		Inventory_Stock_Type,
		Inventory_Stock_Items,
		Inventory_Stock_Revenue,
		Days_In_Inventory_Stock,
		Supplier_Stock,
		Per_Day_Items,
		Per_Day_Revenue,
		Per_Day_PC1_5,
		Price,
		Discount)
	SELECT
		SKU_Config,
		SKU_Name,
		Average_Position,
		Last_Position,
		Last_Boost,
		Novelty,
		Days_Visible,
		Is_Visible,
		Is_Market_Place,
		Is_Sell_List,
		Is_Consignment,
		Is_Bundle,
		Items_Sold_Last_30_Days,
		PC1_Last_30_Days,
		PC1_5_Last_30_Days,
		PC2_Last_30_Days,
		Revenue_Last_30_Days,
		Inventory_Stock_Type,
		Inventory_Stock_Items,
		Inventory_Stock_Revenue,
		Days_In_Inventory_Stock,
		Supplier_Stock,
		Per_Day_Items,
		Per_Day_Revenue,
		Per_Day_PC1_5,
		Price,
		Discount
	FROM boosting.Boost_Catalog_MX
	WHERE catalog = @CatalogName
	GROUP BY SKU_Config
	;');
	PREPARE stmt3 FROM @s; EXECUTE stmt3; DEALLOCATE PREPARE stmt3;
	
	#SAVE ALL MAX AND MIN IN TEMPORAL TABLE
	DROP TABLE IF EXISTS boosting.Boost_TMP_MinAndMax;
	CREATE TABLE boosting.Boost_TMP_MinAndMax (
		KPI VARCHAR(250) NOT NULL,
		MIN DECIMAL (15,5) NOT NULL,
		MAX DECIMAL (15,5) NOT NULL,
		PRIMARY KEY(KPI));

	SET @s = CONCAT('INSERT INTO boosting.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Novelty", MIN(Novelty), MAX(Novelty) FROM boosting.Boost_Catalog_', @CatalogName,';');
		PREPARE stmt11 FROM @s; EXECUTE stmt11; DEALLOCATE PREPARE stmt11;
	SET @s = CONCAT('INSERT INTO boosting.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Is_Consignment", MIN(Is_Consignment), MAX(Is_Consignment) FROM boosting.Boost_Catalog_', @CatalogName,';');
		PREPARE stmt12 FROM @s; EXECUTE stmt12; DEALLOCATE PREPARE stmt12;
	SET @s = CONCAT('INSERT INTO boosting.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Days_In_Inventory_Stock", MIN(Days_In_Inventory_Stock), MAX(Days_In_Inventory_Stock) FROM boosting.Boost_Catalog_', @CatalogName,';');
		PREPARE stmt13 FROM @s; EXECUTE stmt13; DEALLOCATE PREPARE stmt13;
	SET @s = CONCAT('INSERT INTO boosting.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Inventory_Stock_Revenue", MIN(Inventory_Stock_Revenue), MAX(Inventory_Stock_Revenue) FROM boosting.Boost_Catalog_', @CatalogName,';');
		PREPARE stmt14 FROM @s; EXECUTE stmt14; DEALLOCATE PREPARE stmt14;
	SET @s = CONCAT('INSERT INTO boosting.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Inventory_Stock_Items", MIN(Inventory_Stock_Items), MAX(Inventory_Stock_Items) FROM boosting.Boost_Catalog_', @CatalogName,';');
		PREPARE stmt15 FROM @s; EXECUTE stmt15; DEALLOCATE PREPARE stmt15;
	SET @s = CONCAT('INSERT INTO boosting.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Supplier_Stock", MIN(Supplier_Stock), MAX(Supplier_Stock) FROM boosting.Boost_Catalog_', @CatalogName,';');
		PREPARE stmt16 FROM @s; EXECUTE stmt16; DEALLOCATE PREPARE stmt16;
	SET @s = CONCAT('INSERT INTO boosting.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Per_Day_Items", MIN(Per_Day_Items), MAX(Per_Day_Items) FROM boosting.Boost_Catalog_', @CatalogName,';');
		PREPARE stmt18 FROM @s; EXECUTE stmt18; DEALLOCATE PREPARE stmt18;
	SET @s = CONCAT('INSERT INTO boosting.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Per_Day_Revenue", MIN(Per_Day_Revenue), MAX(Per_Day_Revenue) FROM boosting.Boost_Catalog_', @CatalogName,';');
		PREPARE stmt19 FROM @s; EXECUTE stmt19; DEALLOCATE PREPARE stmt19;
	SET @s = CONCAT('INSERT INTO boosting.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Per_Day_PC1_5", MIN(Per_Day_PC1_5), MAX(Per_Day_PC1_5) FROM boosting.Boost_Catalog_', @CatalogName,';');
		PREPARE stmt20 FROM @s; EXECUTE stmt20; DEALLOCATE PREPARE stmt20;
	SET @s = CONCAT('INSERT INTO boosting.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Price", MIN(Price), MAX(Price) FROM boosting.Boost_Catalog_', @CatalogName,';');
		PREPARE stmt21 FROM @s; EXECUTE stmt21; DEALLOCATE PREPARE stmt21;
	SET @s = CONCAT('INSERT INTO boosting.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Discount", MIN(Discount), MAX(Discount) FROM boosting.Boost_Catalog_', @CatalogName,';');
		PREPARE stmt22 FROM @s; EXECUTE stmt22; DEALLOCATE PREPARE stmt22;

	#SET THE TOTAL OF WEIGHTS
	SET @s = CONCAT('UPDATE boosting.Boost_Weights_MX
			SET Total_Weight =  Novelty + 
													Days_In_Inventory_Stock + 
													Inventory_Stock_Revenue + 
													Inventory_Stock_Items + 
													Supplier_Stock +
													Per_Day_Items + 
													Per_Day_Revenue + 
													Per_Day_PC1_5 + 
													Is_Market_Place + 
													Is_Sell_List + 
													Is_Consignment + 
													Is_Bundle + 
													Price + 
													Discount
			WHERE Catalog = "', @CatalogName, '";');
		PREPARE stmt23 FROM @s; EXECUTE stmt23; DEALLOCATE PREPARE stmt23;

	#NORMALIZE DATA ---(Value-MIN)/(MAX-MIN)*Weight-#
	#SellList, Commertial-Marketing.
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName, ' a, boosting.Boost_TMP_MinAndMax b, boosting.Boost_Weights_MX c
			SET a.Novelty_Norm = ((b.MAX + b.MIN - a.Novelty) - b.MIN) / (b.MAX-b.MIN) * c.Novelty
			WHERE b.KPI = "Novelty" AND c.Catalog = "', @CatalogName,'";');
		PREPARE stmt24 FROM @s; EXECUTE stmt24; DEALLOCATE PREPARE stmt24;
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName, ' a, boosting.Boost_Weights_MX c
			SET a.Is_Market_Place_Norm = a.Is_Market_Place * c.Is_Market_Place
			WHERE c.Catalog = "', @CatalogName,'";');
		PREPARE stmt25 FROM @s; EXECUTE stmt25; DEALLOCATE PREPARE stmt25;
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName, ' a, boosting.Boost_Weights_MX c
			SET a.Is_Sell_List_Norm = a.Is_Sell_List * c.Is_Sell_List
			WHERE c.Catalog = "', @CatalogName,'";');
		PREPARE stmt26 FROM @s; EXECUTE stmt26; DEALLOCATE PREPARE stmt26;
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName, ' a, boosting.Boost_TMP_MinAndMax b, boosting.Boost_Weights_MX c
			SET a.Is_Consignment_Norm = ((b.MAX + b.MIN - a.Is_Consignment) - b.MIN) / (b.MAX-b.MIN) * c.Is_Consignment
			WHERE b.KPI = "Is_Consignment" AND c.Catalog = "', @CatalogName,'";');
		PREPARE stmt27 FROM @s; EXECUTE stmt27; DEALLOCATE PREPARE stmt27;
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName, ' a, boosting.Boost_Weights_MX c
			SET a.Is_Bundle_Norm = a.Is_Bundle * c.Is_Bundle
			WHERE c.Catalog = "', @CatalogName,'";');
		PREPARE stmt28 FROM @s; EXECUTE stmt28; DEALLOCATE PREPARE stmt28;
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName, ' a, boosting.Boost_TMP_MinAndMax b, boosting.Boost_Weights_MX c
			SET a.Days_In_Inventory_Stock_Norm = (a.Days_In_Inventory_Stock - b.MIN) / (b.MAX-b.MIN) * c.Days_In_Inventory_Stock
			WHERE b.KPI = "Days_In_Inventory_Stock" AND c.Catalog = "', @CatalogName,'";');
		PREPARE stmt29 FROM @s; EXECUTE stmt29; DEALLOCATE PREPARE stmt29;
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName, ' a, boosting.Boost_TMP_MinAndMax b, boosting.Boost_Weights_MX c
			SET a.Inventory_Stock_Revenue_Norm = (a.Inventory_Stock_Revenue - b.MIN) / (b.MAX-b.MIN) * c.Inventory_Stock_Revenue
			WHERE b.KPI = "Inventory_Stock_Revenue" AND c.Catalog = "', @CatalogName,'";');
		PREPARE stmt30 FROM @s; EXECUTE stmt30; DEALLOCATE PREPARE stmt30;
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName, ' a, boosting.Boost_TMP_MinAndMax b, boosting.Boost_Weights_MX c
			SET a.Inventory_Stock_Items_Norm = (a.Inventory_Stock_Items - b.MIN) / (b.MAX-b.MIN) * c.Inventory_Stock_Items
			WHERE b.KPI = "Inventory_Stock_Items" AND c.Catalog = "', @CatalogName,'";');
		PREPARE stmt31 FROM @s; EXECUTE stmt31; DEALLOCATE PREPARE stmt31;
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName, ' a, boosting.Boost_TMP_MinAndMax b, boosting.Boost_Weights_MX c
			SET a.Supplier_Stock_Norm = (a.Supplier_Stock - b.MIN) / (b.MAX-b.MIN) * c.Supplier_Stock
			WHERE b.KPI = "Supplier_Stock" AND c.Catalog = "', @CatalogName,'";');
		PREPARE stmt32 FROM @s; EXECUTE stmt32; DEALLOCATE PREPARE stmt32;
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName, ' a, boosting.Boost_TMP_MinAndMax b, boosting.Boost_Weights_MX c
			SET a.Per_Day_Items_Norm = (a.Per_Day_Items - b.MIN) / (b.MAX-b.MIN) * c.Per_Day_Items
			WHERE b.KPI = "Per_Day_Items" AND c.Catalog = "', @CatalogName,'";');
		PREPARE stmt34 FROM @s; EXECUTE stmt34; DEALLOCATE PREPARE stmt34;
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName, ' a, boosting.Boost_TMP_MinAndMax b, boosting.Boost_Weights_MX c
			SET a.Per_Day_Revenue_Norm = (a.Per_Day_Revenue - b.MIN) / (b.MAX-b.MIN) * c.Per_Day_Revenue
			WHERE b.KPI = "Per_Day_Revenue" AND c.Catalog = "', @CatalogName,'";');
		PREPARE stmt35 FROM @s; EXECUTE stmt35; DEALLOCATE PREPARE stmt35;
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName, ' a, boosting.Boost_TMP_MinAndMax b, boosting.Boost_Weights_MX c
			SET a.Per_Day_PC1_5_Norm = (a.Per_Day_PC1_5 - b.MIN) / (b.MAX-b.MIN) * c.Per_Day_PC1_5
			WHERE b.KPI = "Per_Day_PC1_5" AND c.Catalog = "', @CatalogName,'";');
		PREPARE stmt36 FROM @s; EXECUTE stmt36; DEALLOCATE PREPARE stmt36;
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName, ' a, boosting.Boost_TMP_MinAndMax b, boosting.Boost_Weights_MX c
			SET a.Price_Norm = (a.Price - b.MIN) / (b.MAX-b.MIN) * c.Price
			WHERE b.KPI = "Price" AND c.Catalog = "', @CatalogName,'";');
		PREPARE stmt37 FROM @s; EXECUTE stmt37; DEALLOCATE PREPARE stmt37;
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName, ' a, boosting.Boost_TMP_MinAndMax b, boosting.Boost_Weights_MX c
			SET a.Discount_Norm = (a.Discount - b.MIN) / (b.MAX-b.MIN) * c.Discount
			WHERE b.KPI = "Discount" AND c.Catalog = "', @CatalogName,'";');
		PREPARE stmt38 FROM @s; EXECUTE stmt38; DEALLOCATE PREPARE stmt38;

	#SET BOOST ORDER
	#Set novelty, set last Boost, set Boost_Order, set Final_Boost
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName, ' a, boosting.Boost_Weights_MX b
				SET a.Boost_Order =(a.Novelty_Norm
													+ a.Is_Market_Place_Norm
													+ a.Is_Sell_List_Norm
													+ a.Is_Consignment_Norm
													+ a.Is_Bundle_Norm
													+ a.Days_In_Inventory_Stock_Norm
													+ a.Inventory_Stock_Revenue_Norm
													+ a.Inventory_Stock_Items_Norm
													+ a.Supplier_Stock_Norm
													+ a.Per_Day_Items_Norm
													+ a.Per_Day_Revenue_Norm
													+ a.Per_Day_PC1_5_Norm
													+ a.Price_Norm
													+ a.Discount_Norm) / b.Total_Weight
			WHERE b.Catalog = "',@CatalogName, '";');
		PREPARE stmt39 FROM @s; EXECUTE stmt39; DEALLOCATE PREPARE stmt39;

	#Find if SKUs exist in the catalog.
		SET @s = CONCAT('UPDATE boosting.Boost_Fixed_SKUs
		SET
				SKU1_Status = CASE WHEN SKU1 <> "" AND SKU1 NOT IN (SELECT SKU_Config FROM boosting.Boost_Catalog_', @CatalogName, ') THEN "Not found" ELSE Null END,
				SKU2_Status = CASE WHEN SKU2 <> "" AND SKU2 NOT IN (SELECT SKU_Config FROM boosting.Boost_Catalog_', @CatalogName, ') THEN "Not found" ELSE Null END,
				SKU3_Status = CASE WHEN SKU3 <> "" AND SKU3 NOT IN (SELECT SKU_Config FROM boosting.Boost_Catalog_', @CatalogName, ') THEN "Not found" ELSE Null END,
				SKU4_Status = CASE WHEN SKU4 <> "" AND SKU4 NOT IN (SELECT SKU_Config FROM boosting.Boost_Catalog_', @CatalogName, ') THEN "Not found" ELSE Null END,
				SKU5_Status = CASE WHEN SKU5 <> "" AND SKU5 NOT IN (SELECT SKU_Config FROM boosting.Boost_Catalog_', @CatalogName, ') THEN "Not found" ELSE Null END,
				SKU6_Status = CASE WHEN SKU6 <> "" AND SKU6 NOT IN (SELECT SKU_Config FROM boosting.Boost_Catalog_', @CatalogName, ') THEN "Not found" ELSE Null END,
				SKU7_Status = CASE WHEN SKU7 <> "" AND SKU7 NOT IN (SELECT SKU_Config FROM boosting.Boost_Catalog_', @CatalogName, ') THEN "Not found" ELSE Null END,
				SKU8_Status = CASE WHEN SKU8 <> "" AND SKU8 NOT IN (SELECT SKU_Config FROM boosting.Boost_Catalog_', @CatalogName, ') THEN "Not found" ELSE Null END
			WHERE Catalog = @CatalogName;');
	PREPARE stmt40 FROM @s; EXECUTE stmt40; DEALLOCATE PREPARE stmt40;
	
	#Fixed boost scores from GDocs
	SET @reserved = 0;
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName,' a, boosting.Boost_Fixed_SKUs b, boosting.Boost_Weights_MX c
		SET
			a.Fixed_Boost = c.TopBucket - @reserved := @reserved + 1
			WHERE b.SKU1 = a.SKU_Config AND b.Catalog = @CatalogName AND c.Catalog = @CatalogName;');
	PREPARE stmt41 FROM @s; EXECUTE stmt41; DEALLOCATE PREPARE stmt41;
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName,' a, boosting.Boost_Fixed_SKUs b, boosting.Boost_Weights_MX c
			SET
				a.Fixed_Boost = c.TopBucket - @reserved := @reserved + 1
				WHERE b.SKU2 = a.SKU_Config AND b.Catalog = @CatalogName AND c.Catalog = @CatalogName;');
		PREPARE stmt41 FROM @s; EXECUTE stmt41; DEALLOCATE PREPARE stmt41;
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName,' a, boosting.Boost_Fixed_SKUs b, boosting.Boost_Weights_MX c
			SET
				a.Fixed_Boost = c.TopBucket - @reserved := @reserved + 1
				WHERE b.SKU3 = a.SKU_Config AND b.Catalog = @CatalogName AND c.Catalog = @CatalogName;');
		PREPARE stmt41 FROM @s; EXECUTE stmt41; DEALLOCATE PREPARE stmt41;
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName,' a, boosting.Boost_Fixed_SKUs b, boosting.Boost_Weights_MX c
			SET
				a.Fixed_Boost = c.TopBucket - @reserved := @reserved + 1
				WHERE b.SKU4 = a.SKU_Config AND b.Catalog = @CatalogName AND c.Catalog = @CatalogName;');
		PREPARE stmt41 FROM @s; EXECUTE stmt41; DEALLOCATE PREPARE stmt41;
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName,' a, boosting.Boost_Fixed_SKUs b, boosting.Boost_Weights_MX c
			SET
				a.Fixed_Boost = c.TopBucket - @reserved := @reserved + 1
				WHERE b.SKU5 = a.SKU_Config AND b.Catalog = @CatalogName AND c.Catalog = @CatalogName;');
		PREPARE stmt41 FROM @s; EXECUTE stmt41; DEALLOCATE PREPARE stmt41;
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName,' a, boosting.Boost_Fixed_SKUs b, boosting.Boost_Weights_MX c
			SET
				a.Fixed_Boost = c.TopBucket - @reserved := @reserved + 1
				WHERE b.SKU6 = a.SKU_Config AND b.Catalog = @CatalogName AND c.Catalog = @CatalogName;');
		PREPARE stmt41 FROM @s; EXECUTE stmt41; DEALLOCATE PREPARE stmt41;
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName,' a, boosting.Boost_Fixed_SKUs b, boosting.Boost_Weights_MX c
			SET
				a.Fixed_Boost = c.TopBucket - @reserved := @reserved + 1
				WHERE b.SKU7 = a.SKU_Config AND b.Catalog = @CatalogName AND c.Catalog = @CatalogName;');
		PREPARE stmt41 FROM @s; EXECUTE stmt41; DEALLOCATE PREPARE stmt41;
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName,' a, boosting.Boost_Fixed_SKUs b, boosting.Boost_Weights_MX c
			SET
				a.Fixed_Boost = c.TopBucket - @reserved := @reserved + 1
				WHERE b.SKU8 = a.SKU_Config AND b.Catalog = @CatalogName AND c.Catalog = @CatalogName;');
		PREPARE stmt41 FROM @s; EXECUTE stmt41; DEALLOCATE PREPARE stmt41;

	#SET FIXED BOOST ACCORDING TO MARKETING AND COMMETCIAL SCHEDULE
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName, ' a, boosting.Boost_Marketing_Commercial_Schedule_MX b
		SET
				a.Fixed_Boost = b.Boost
			WHERE a.SKU_Config = b.SKU_Config AND (b.Start_Date <= NOW() AND NOW() <= b.End_Date)
		;');
	PREPARE stmt42 FROM @s; EXECUTE stmt42; DEALLOCATE PREPARE stmt42;

	#SORT ORDER BY BOOST DESCENDENT
	SET @s = CONCAT('ALTER TABLE boosting.Boost_Catalog_',@CatalogName, ' ORDER BY Boost_Order DESC;');
		PREPARE stmt43 FROM @s; EXECUTE stmt43; DEALLOCATE PREPARE stmt43;

	#SET NEW POSITION
	SET @count = 0;
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName, '
		SET
				New_Position = @count:=@count+1;
		;');
	PREPARE stmt44 FROM @s; EXECUTE stmt44; DEALLOCATE PREPARE stmt44;

	#SET FINAL BOOST
	SET @s = CONCAT('INSERT INTO boosting.Boost_TMP_MinAndMax (KPI, MIN, MAX) SELECT "Total", 0, COUNT(SKU_Config) FROM boosting.Boost_Catalog_',@CatalogName, ' WHERE Fixed_Boost IS NULL;');
	PREPARE stmt45 FROM @s; EXECUTE stmt45; DEALLOCATE PREPARE stmt45;

	SET @count = 0;
	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName, ' a, boosting.Boost_TMP_MinAndMax b, boosting.Boost_Weights_MX c
		SET
		a.Final_Boost = CASE
				WHEN a.Fixed_Boost IS NOT NULL THEN 0
				ELSE  @count:=@count+1
			END,
			a.Final_Boost = CASE
				WHEN a.Fixed_Boost IS NOT NULL THEN a.Fixed_Boost
				ELSE CASE
						WHEN FLOOR(-0.5*(-SQRT(8*@count)-1))  < ROUND(b.MAX/((c.TopBucket-@reserved)-(c.FloorBucket)+1),0) THEN (c.TopBucket-@reserved)-FLOOR(-0.5*(-SQRT(8*@count)-1))
						ELSE CASE
					WHEN (c.TopBucket-@reserved) - (FLOOR((@count-((ROUND(b.MAX/((c.TopBucket-@reserved)-(c.FloorBucket)+1),0)-1)*ROUND(b.MAX/((c.TopBucket-@reserved)-(c.FloorBucket)+1),0)/2)-1)/ROUND(b.MAX/((c.TopBucket-@reserved)-(c.FloorBucket)+1),0)) + ROUND(b.MAX/((c.TopBucket-@reserved)-(c.FloorBucket)+1),0)) < c.FloorBucket THEN c.FloorBucket
					ELSE (c.TopBucket-@reserved) - (FLOOR((@count-((ROUND(b.MAX/((c.TopBucket-@reserved)-(c.FloorBucket)+1),0)-1)*ROUND(b.MAX/((c.TopBucket-@reserved)-(c.FloorBucket)+1),0)/2)-1)/ROUND(b.MAX/((c.TopBucket-@reserved)-(c.FloorBucket)+1),0)) + ROUND(b.MAX/((c.TopBucket-@reserved)-(c.FloorBucket)+1),0))
						END
				END
			END
		WHERE b.KPI = "Total" AND c.catalog = @CatalogName;');
	PREPARE stmt46 FROM @s; EXECUTE stmt46; DEALLOCATE PREPARE stmt46;

	SET @s = CONCAT('UPDATE boosting.Boost_Catalog_',@CatalogName, ' a, boosting.Boost_Weights_MX c
		SET
			a.Final_Boost = c.FloorBucket
		WHERE c.catalog = @CatalogName AND a.Final_Boost < c.FloorBucket AND a.Fixed_Boost IS NULL;');
	PREPARE stmt47 FROM @s; EXECUTE stmt47; DEALLOCATE PREPARE stmt47;


	#INSERT FINAL BOOST INTO LAST TABLE
	SET @s = CONCAT('INSERT INTO boosting.Final_Boost_MX (Catalog, TopBucket, SKU_Config, Score)
		SELECT
			@CatalogName,
			b.TopBucket,
			SKU_Config,
			Final_Boost FROM boosting.Boost_Catalog_',@CatalogName, '
			INNER JOIN (SELECT TopBucket, Catalog FROM boosting.Boost_Weights_MX) b
			ON @CatalogName = b.Catalog;');
	PREPARE stmt48 FROM @s; EXECUTE stmt48; DEALLOCATE PREPARE stmt48;

	DROP TABLE IF EXISTS boosting.Boost_TMP_MinAndMax;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `Boost_Report_2_MX`(IN v_catalog VARCHAR(255), v_start_pos INT, v_end_pos INT, v_start_date DATE, v_days INT)
BEGIN
	SET @Catalog = v_catalog, @StartPos = v_start_pos, @EndPos = v_end_pos, @StartDate = v_start_date, @Days = v_days;
		INSERT INTO Boost_Report_MX()
		SELECT 
			@Catalog AS Catalog,
			dates.date AS Date,
			WEEK(dates.date) AS Week,
			CONCAT(@StartPos," to ",@EndPos) AS Set_of_SKUs,
			SUM(stock.numStock) as Stock,
			sales.items_sold AS Items_sold,
			sales.pc1_5 AS PC1_5,
			sales.revenue_sold AS Revenue_sold
		FROM 
			(SELECT DATE_ADD(@StartDate, INTERVAL n.id - 1 DAY) AS date
			FROM boosting.Boost_dates n
			WHERE DATE_ADD(@StartDate, INTERVAL n.id -1 DAY) <=  @StartDate + interval @Days day) dates
			INNER JOIN (SELECT SKU_Config, catalog, numb_of_position, DATE(date_time) AS date FROM Boost_Product_Position_In_Catalog_MX
									WHERE catalog = @Catalog AND numb_of_position >= @StartPos AND numb_of_position <= @EndPos AND DATE(date_time) >= @StartDate AND DATE(date_time) < @StartDate + interval @Days day) pos
									ON dates.date = pos.date
			LEFT JOIN (select sku_config, count(*) as numStock, date_entrance, date_exit from operations_mx.out_stock_hist
									WHERE (date_exit >= @StartDate OR date_exit IS NULL) AND date_entrance <= @StartDate + interval @Days day
									GROUP BY sku_config, date_entrance, date_exit) stock
									ON stock.sku_config = pos.SKU_Config AND stock.date_entrance <= dates.date AND (stock.date_exit IS NULL OR stock.date_exit >= dates.date)
			LEFT JOIN (SELECT SkuConfig, COUNT(*) AS items_sold, SUM(PCOnePFive) AS pc1_5, SUM(Rev) AS revenue_sold, Date FROM development_mx.A_Master
									WHERE OrderAfterCan = 1 AND Date >= @StartDate AND Date < @StartDate + interval @Days day
									GROUP BY SkuConfig, Date) sales
									ON sales.SkuConfig = pos.SKU_Config AND sales.Date = dates.date
		GROUP BY dates.date
		ORDER BY dates.date asc;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `Boos_Report_1_MX`()
BEGIN
	DECLARE done INT DEFAULT 0;
  DECLARE v_cat VARCHAR(50) DEFAULT NULL;
  #declare cursor
  DECLARE cur1 CURSOR FOR SELECT DISTINCT Catalog FROM Boost_Weights_MX WHERE Active = 1 AND Catalog IS NOT NULL ORDER BY TopBucket DESC;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
	SET @days = 30;
	
	#Routine body goes here...
	DROP TABLE IF EXISTS Boost_Report_MX;
	CREATE TABLE Boost_Report_MX(
		Catalog VARCHAR(255),
		Date DATE,
		Week SMALLINT,
		Set_of_SKUs VARCHAR(255),
		Stock DECIMAL(10,2),
		Items_sold DECIMAL(10,2),
		PC1_5 DECIMAL(10,2),
		Revenue_sold DECIMAL(10,2)
		);

	OPEN cur1;
		cur1_loop:LOOP
			FETCH cur1 INTO v_cat;
			IF done = 1 THEN
				LEAVE cur1_loop;
			END IF;
			CALL Boost_Report_2_MX(v_cat, 1, 25, NOW()- INTERVAL @days DAY, @days);
			CALL Boost_Report_2_MX(v_cat, 26, 50, NOW()- INTERVAL @days DAY, @days);
			CALL Boost_Report_2_MX(v_cat, 51, 100, NOW()- INTERVAL @days DAY, @days);
			CALL Boost_Report_2_MX(v_cat, 101, 200, NOW()- INTERVAL @days DAY, @days);
		END LOOP cur1_loop;
  CLOSE cur1;
	SET done = 0;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `Code_EAN`()
BEGIN
	#Routine body goes here...
	DROP TABLE IF EXISTS boosting.Code_EAN;
	CREATE TABLE boosting.Code_EAN(
		id int NOT NULL AUTO_INCREMENT,
		SKU_Config VARCHAR(250),
		SKU_Simple VARCHAR(250),
		Status VARCHAR (255),
		CAT1 VARCHAR (255),
		CAT2 VARCHAR (255),
		CAT3 VARCHAR (255),
		sku_supplier_simple VARCHAR (255),
		supplier_simple VARCHAR (255),
		supplier_simple_name VARCHAR (255),
		barcode_ean VARCHAR (255),
		is_unique INT,
		has_correct_format INT,
		has_GS1MX INT,
		PRIMARY KEY (id)
);

INSERT INTO boosting.Code_EAN (SKU_Simple, Status, sku_supplier_simple, supplier_simple, supplier_simple_name, barcode_ean)
SELECT sku, status, sku_supplier_simple, supplier_simple, supplier_simple_name, barcode_ean  FROM bob_live_mx.catalog_simple;
	
	#Set SKU Config
	UPDATE boosting.Code_EAN
	SET SKU_Config = LEFT(SKU_Simple,15);

	#Set is_unique
	UPDATE boosting.Code_EAN c
	INNER JOIN
	(
		select distinct barcode_ean, MIN(id) as keep
		from boosting.Code_EAN
		where barcode_ean is not null
		group by barcode_ean
	) k
	ON c.id = k.keep
	SET
			is_unique = 1;

	#Set is not unique
	UPDATE boosting.Code_EAN
	SET
			is_unique = 0
	WHERE is_unique is null AND barcode_ean is not null;
	
	#Set has_correct_format
	UPDATE boosting.Code_EAN
	SET
			has_correct_format = 1
	WHERE
		(barcode_ean  REGEXP '^[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]$'
		OR barcode_ean  REGEXP '^[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]$');

	#Ser has_GS1MX
	UPDATE boosting.Code_EAN
	SET
			has_GS1MX = 1
	WHERE
		(barcode_ean  REGEXP '^[7]+[5]+[0]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]$'
		OR barcode_ean  REGEXP '^[7]+[5]+[0]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]+[0-9]$');
	
	#Set Cat1
	UPDATE boosting.Code_EAN ean
	INNER JOIN bob_live_mx.catalog_config config
	ON LEFT(ean.SKU_Simple,15)=config.sku
	INNER JOIN bob_live_mx.catalog_attribute_option_global_category c
		ON c.id_catalog_attribute_option_global_category = config.fk_catalog_attribute_option_global_category 
		SET ean.CAT1 = c.name;

	#Set Cat2
	UPDATE boosting.Code_EAN ean
	INNER JOIN bob_live_mx.catalog_config config
	ON LEFT(ean.SKU_Simple,15) = config.sku
	INNER JOIN bob_live_mx.catalog_attribute_option_global_sub_category c 
	ON c.id_catalog_attribute_option_global_sub_category = config.fk_catalog_attribute_option_global_sub_category 
	SET ean.CAT2 = c.name;

	#Set Cat3
	UPDATE boosting.Code_EAN ean
	INNER JOIN bob_live_mx.catalog_config config 
	ON LEFT(ean.SKU_Simple,15) = config.sku
	INNER JOIN bob_live_mx.catalog_attribute_option_global_sub_sub_category c 
	ON c.id_catalog_attribute_option_global_sub_sub_category = config.fk_catalog_attribute_option_global_sub_sub_category 
	SET ean.CAT3 = c.name;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `RR_Sales_Reconciliation`()
BEGIN
	/*drop table if EXISTS `RR_Sales_PE`;
create TABLE `RR_Sales_PE` (id_sales_rr_pe int AUTO_INCREMENT, rr_date_ordered VARCHAR(255), rr_user_id VARCHAR(255),
rr_session_id VARCHAR(255), order_nr VARCHAR(45), rr_product_id int, rr_units int, rr_price FLOAT4, rr_sales FLOAT4
,PRIMARY key (id_sales_rr_pe))*/

drop table if EXISTS `RR_Sales_Reconciliation_MX`;

create table `RR_Sales_Reconciliation_MX` (id_rr_sales_recon int not null AUTO_INCREMENT, date_linio_end date NOT NULL
,date_rr_end VARCHAR(255), order_nr VARCHAR(45), sku_config VARCHAR(255), item_name varchar (255), rr_units int,
 items_in_order int, rr_price FLOAT4, rr_sales FLOAT4, linio_price FLOAT4, paid_price FLOAT4, item_status varchar (255),
isMrktPlace int, coupon_code VARCHAR (255), coupon_value FLOAT4, cat1 VARCHAR(255), cat2 VARCHAR(255),
cat3 VARCHAR(255), main_category varchar(255), customer_email VARCHAR(255), customer_name VARCHAR(255), new_or_returning VARCHAR(255),
PRIMARY key (id_rr_sales_recon));

insert into `RR_Sales_Reconciliation_MX` ( date_linio_end, date_rr_end, order_nr, sku_config, item_name, rr_units,
items_in_order, rr_price, rr_sales, linio_price, paid_price, item_status, isMrktPlace, coupon_code, coupon_value,
cat1, cat2, cat3, main_category, customer_email, customer_name, new_or_returning)

SELECT c.Date, a.rr_date_ordered, a.order_nr, b.sku, b.`name`, a.rr_units,c.ItemsInOrder, a.rr_price / 100,
a.rr_sales / 100, c.Price ,c.PaidPrice ,c.`Status`, c.isMPlace,c.CouponCode,c.CouponValue, c.cat1,
c.cat2, c.cat3, d.`name`, c.CustomerEmail, CONCAT(c.FirstName, " ", c.LastName), c.NewReturning

from RR_Sales_MX a

inner join bob_live_mx.catalog_config b on a.rr_product_id = b.id_catalog_config
INNER JOIN development_mx.A_Master c on a.order_nr = c.OrderNum and b.sku = c.SKUConfig
INNER JOIN bob_live_mx.catalog_category d on b.main_category = d.id_catalog_category

GROUP BY a.rr_product_id, a.order_nr;



drop table if EXISTS `RR_Sales_Reconciliation_CO`;

create table `RR_Sales_Reconciliation_CO` (id_rr_sales_recon int not null AUTO_INCREMENT, date_linio_end date NOT NULL
,date_rr_end VARCHAR(255), order_nr VARCHAR(45), sku_config VARCHAR(255), item_name varchar (255), rr_units int,
 items_in_order int, rr_price FLOAT4, rr_sales FLOAT4, linio_price FLOAT4, paid_price FLOAT4, item_status varchar (255),
isMrktPlace int, coupon_code VARCHAR (255), coupon_value FLOAT4, cat1 VARCHAR(255), cat2 VARCHAR(255),
cat3 VARCHAR(255), customer_email VARCHAR(255), customer_name VARCHAR(255), new_or_returning VARCHAR(255),
PRIMARY key (id_rr_sales_recon));

Insert into `RR_Sales_Reconciliation_CO` ( date_linio_end, date_rr_end, order_nr, sku_config, item_name, rr_units,
items_in_order, rr_price, rr_sales, linio_price, paid_price, item_status, isMrktPlace, coupon_code, coupon_value,
cat1, cat2, cat3, customer_email, customer_name, new_or_returning)

SELECT c.Date, a.rr_date_ordered, a.order_nr, b.sku, b.`name`, a.rr_units,c.ItemsInOrder, a.rr_price / 100,
a.rr_sales / 100, c.Price ,c.PaidPrice ,c.`Status`, c.isMPlace,c.CouponCode,c.CouponValue, c.cat1,
c.cat2, c.cat3, c.CustomerEmail, CONCAT(c.FirstName, " ", c.LastName), c.NewReturning

from RR_Sales_CO a

INNER JOIN bob_live_co.catalog_config b on a.rr_product_id = b.id_catalog_config
INNER JOIN development_co_project.A_Master c on a.order_nr = c.OrderNum and b.sku = c.SKUConfig

GROUP BY a.rr_product_id, a.order_nr;


drop table if EXISTS `RR_Sales_Reconciliation_VE`;

create table `RR_Sales_Reconciliation_VE` (id_rr_sales_recon int not null AUTO_INCREMENT, date_linio_end date NOT NULL
,date_rr_end VARCHAR(255), order_nr VARCHAR(45), sku_config VARCHAR(255), item_name varchar (255), rr_units int,
 items_in_order int, rr_price FLOAT4, rr_sales FLOAT4, linio_price FLOAT4, paid_price FLOAT4, item_status varchar (255),
isMrktPlace int, coupon_code VARCHAR (255), coupon_value FLOAT4, cat1 VARCHAR(255), cat2 VARCHAR(255),
cat3 VARCHAR(255), customer_email VARCHAR(255), customer_name VARCHAR(255), new_or_returning VARCHAR(255),
PRIMARY key (id_rr_sales_recon));

Insert into `RR_Sales_Reconciliation_VE` ( date_linio_end, date_rr_end, order_nr, sku_config, item_name, rr_units,
items_in_order, rr_price, rr_sales, linio_price, paid_price, item_status, isMrktPlace, coupon_code, coupon_value,
cat1, cat2, cat3, customer_email, customer_name, new_or_returning)

SELECT c.Date, a.rr_date_ordered, a.order_nr, b.sku, b.`name`, a.rr_units,c.ItemsInOrder, a.rr_price / 100,
a.rr_sales / 100, c.Price ,c.PaidPrice ,c.`Status`, c.isMPlace,c.CouponCode,c.CouponValue, c.cat1,
c.cat2, c.cat3, c.CustomerEmail, CONCAT(c.FirstName, " ", c.LastName), c.NewReturning

from RR_Sales_VE a

INNER JOIN bob_live_ve.catalog_config b on a.rr_product_id = b.id_catalog_config
INNER JOIN development_ve.A_Master c on a.order_nr = c.OrderNum and b.sku = c.SKUConfig

GROUP BY a.rr_product_id, a.order_nr;

drop table if EXISTS `RR_Sales_Reconciliation_PE`;

create table `RR_Sales_Reconciliation_PE` (id_rr_sales_recon int not null AUTO_INCREMENT, date_linio_end date NOT NULL
,date_rr_end VARCHAR(255), order_nr VARCHAR(45), sku_config VARCHAR(255), item_name varchar (255), rr_units int,
 items_in_order int, rr_price FLOAT4, rr_sales FLOAT4, linio_price FLOAT4, paid_price FLOAT4, item_status varchar (255),
isMrktPlace int, coupon_code VARCHAR (255), coupon_value FLOAT4, cat1 VARCHAR(255), cat2 VARCHAR(255),
cat3 VARCHAR(255), customer_email VARCHAR(255), customer_name VARCHAR(255), new_or_returning VARCHAR(255),
PRIMARY key (id_rr_sales_recon));

Insert into `RR_Sales_Reconciliation_PE` ( date_linio_end, date_rr_end, order_nr, sku_config, item_name, rr_units,
items_in_order, rr_price, rr_sales, linio_price, paid_price, item_status, isMrktPlace, coupon_code, coupon_value,
cat1, cat2, cat3, customer_email, customer_name, new_or_returning)

SELECT c.Date, a.rr_date_ordered, a.order_nr, b.sku, b.`name`, a.rr_units,c.ItemsInOrder, a.rr_price / 100,
a.rr_sales / 100, c.Price ,c.PaidPrice ,c.`Status`, c.isMPlace,c.CouponCode,c.CouponValue, c.cat1,
c.cat2, c.cat3, c.CustomerEmail, CONCAT(c.FirstName, " ", c.LastName), c.NewReturning

from RR_Sales_PE a

INNER JOIN bob_live_pe.catalog_config b on a.rr_product_id = b.id_catalog_config
INNER JOIN development_pe.A_Master c on a.order_nr = c.OrderNum and b.sku = c.SKUConfig

GROUP BY a.rr_product_id, a.order_nr;



END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `Test`()
BEGIN
	#Routine body goes here...
DROP table if exists hola;
	Create table hola(id int);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `Viviane_Normalize_Cat`(IN cat VARCHAR(255))
BEGIN
	#Routine body goes here...
	SET @CatalogName = cat;
	
	
	DROP TABLE IF EXISTS test_linio.Viviane_TMP;
	CREATE TABLE test_linio.Viviane_TMP (
		`ID` int(255) NOT NULL,
		`SKU_Config` varchar(255) NOT NULL,
		`Catalog` varchar(255) NOT NULL,
		`TopBucket` tinyint(4) NOT NULL,
		`FloorBucket` tinyint(4) NOT NULL,
		`Days_Visible` INT NOT NULL,
		`Novelty` double(10,5) NOT NULL,
		`Daily_Items_Sold_Last_7_Days` double(10,5) NOT NULL,
		`Daily_Revenue_Sold_Last_7_Days` double(10,5) NOT NULL,
		`Price` double(10,5) NOT NULL,
		`Discount` double(10,5) NOT NULL,
		`Novelty_Norm` double(10,5) NOT NULL,
		`Daily_Items_Sold_Last_7_Days_Norm` double(10,5) NOT NULL,
		`Daily_Revenue_Sold_Last_7_Days_Norm` double(10,5) NOT NULL,
		`Price_Norm` double(10,5) NOT NULL,
		`Discount_Norm` double(10,5) NOT NULL,
		`Score` double(10,5) NOT NULL
	) ENGINE=MyISAM DEFAULT CHARSET=latin1;
	INSERT INTO test_linio.Viviane_TMP
	SELECT * FROM test_linio.Viviane_data WHERE Catalog = @CatalogName;

	#Normalization
	#SAVE ALL MAX AND MIN IN TEMPORAL TABLE
	DROP TABLE IF EXISTS test_linio.Viviane_MinMax;
	CREATE TABLE test_linio.Viviane_MinMax (
		KPI VARCHAR(250) NOT NULL,
		MIN DECIMAL (15,5) NOT NULL,
		MAX DECIMAL (15,5) NOT NULL,
		PRIMARY KEY(KPI));

	INSERT INTO test_linio.Viviane_MinMax (KPI, MIN, MAX) SELECT "Novelty", MIN(Novelty), MAX(Novelty) FROM test_linio.Viviane_TMP;
	INSERT INTO test_linio.Viviane_MinMax (KPI, MIN, MAX) SELECT "Daily_Items_Sold_Last_7_Days", MIN(Daily_Items_Sold_Last_7_Days), MAX(Daily_Items_Sold_Last_7_Days) FROM test_linio.Viviane_TMP;
	INSERT INTO test_linio.Viviane_MinMax (KPI, MIN, MAX) SELECT "Daily_Revenue_Sold_Last_7_Days", MIN(Daily_Revenue_Sold_Last_7_Days), MAX(Daily_Revenue_Sold_Last_7_Days) FROM test_linio.Viviane_TMP;
	INSERT INTO test_linio.Viviane_MinMax (KPI, MIN, MAX) SELECT "Price", MIN(Price), MAX(Price) FROM test_linio.Viviane_TMP;
	INSERT INTO test_linio.Viviane_MinMax (KPI, MIN, MAX) SELECT "Discount", MIN(Discount), MAX(Discount) FROM test_linio.Viviane_TMP;
	
	UPDATE test_linio.Viviane_TMP a, test_linio.Viviane_MinMax b, test_linio.Viviane_pesos c
			SET a.Novelty_Norm = ((b.MAX + b.MIN - a.Novelty) - b.MIN) / (b.MAX-b.MIN) * c.Novelty
			WHERE b.KPI = "Novelty";
	UPDATE test_linio.Viviane_TMP a, test_linio.Viviane_MinMax b, test_linio.Viviane_pesos c
			SET a.Daily_Items_Sold_Last_7_Days_Norm = ((b.MAX + b.MIN - a.Daily_Items_Sold_Last_7_Days) - b.MIN) / (b.MAX-b.MIN) * c.Daily_Items_Sold_Last_7_Days
			WHERE b.KPI = "Daily_Items_Sold_Last_7_Days";
	UPDATE test_linio.Viviane_TMP a, test_linio.Viviane_MinMax b, test_linio.Viviane_pesos c
			SET a.Daily_Revenue_Sold_Last_7_Days_Norm = ((b.MAX + b.MIN - a.Daily_Revenue_Sold_Last_7_Days) - b.MIN) / (b.MAX-b.MIN) * c.Daily_Revenue_Sold_Last_7_Days
			WHERE b.KPI = "Daily_Revenue_Sold_Last_7_Days";
	UPDATE test_linio.Viviane_TMP a, test_linio.Viviane_MinMax b, test_linio.Viviane_pesos c
			SET a.Price_Norm = ((b.MAX + b.MIN - a.Price) - b.MIN) / (b.MAX-b.MIN) * c.Price
			WHERE b.KPI = "Price";
	UPDATE test_linio.Viviane_TMP a, test_linio.Viviane_MinMax b, test_linio.Viviane_pesos c
			SET a.Discount_Norm = ((b.MAX + b.MIN - a.Discount) - b.MIN) / (b.MAX-b.MIN) * c.Discount
			WHERE b.KPI = "Discount";
	
	UPDATE test_linio.Viviane_data a, test_linio.Viviane_TMP b
		SET
			a.Novelty_Norm = b.Novelty_Norm,
			a.Daily_Items_Sold_Last_7_Days_Norm = b.Daily_Items_Sold_Last_7_Days_Norm,
			a.Daily_Revenue_Sold_Last_7_Days_Norm = b.Daily_Revenue_Sold_Last_7_Days_Norm,
			a.Price_Norm = b.Price_Norm,
			a.Discount_Norm = b.Discount_Norm
		WHERE a.ID = b.ID;
		
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `Viviane_Scores`()
BEGIN
	DECLARE v_rowcount integer unsigned;
	DECLARE v_cat VARCHAR(50) DEFAULT NULL;
  #declare cursor
  DECLARE cur1 CURSOR FOR SELECT DISTINCT catalog FROM  boosting.Boost_Catalog_MX;

	DROP TABLE IF EXISTS test_linio.Viviane_data;
	CREATE TABLE test_linio.Viviane_data (
		`ID` int(255) NOT NULL AUTO_INCREMENT,
		`SKU_Config` varchar(255) NOT NULL,
		`Catalog` varchar(255) NOT NULL,
		`TopBucket` tinyint(4) NOT NULL,
		`FloorBucket` tinyint(4) NOT NULL,
		`Days_Visible` INT NOT NULL,
		`Novelty` double(10,5) NOT NULL,
		`Daily_Items_Sold_Last_7_Days` double(10,5) NOT NULL,
		`Daily_Revenue_Sold_Last_7_Days` double(10,5) NOT NULL,
		`Price` double(10,5) NOT NULL,
		`Discount` double(10,5) NOT NULL,
		`Novelty_Norm` double(10,5) NOT NULL,
		`Daily_Items_Sold_Last_7_Days_Norm` double(10,5) NOT NULL,
		`Daily_Revenue_Sold_Last_7_Days_Norm` double(10,5) NOT NULL,
		`Price_Norm` double(10,5) NOT NULL,
		`Discount_Norm` double(10,5) NOT NULL,
		`Score` double(10,5) NOT NULL,
		PRIMARY KEY (`ID`)
	) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT test_linio.Viviane_data(SKU_Config, Catalog, Days_Visible)
	SELECT 
		a.SKU_Config,
		a.Catalog,
		COUNT(SKU_Config) AS Days_Visible
	FROM boosting.Boost_Product_Position_In_Catalog_MX a
	INNER JOIN test_linio.Viviane_pesos b ON a.Catalog = b.Catalog
	WHERE  date_time >=  NOW() - INTERVAL 7 DAY
	GROUP BY Catalog, SKU_Config;

#TopBucket and FloorBucket
UPDATE test_linio.Viviane_data a, test_linio.Viviane_pesos b
SET 
	a.TopBucket = b.TopBucket,
	a.FloorBucket = b.FloorBucket
WHERE a.Catalog = b.Catalog;


	#NOVELTY
	DROP TABLE IF EXISTS test_linio.Viviane_TMP;
	CREATE TABLE test_linio.Viviane_TMP (PRIMARY KEY( SKU_Config ))
	SELECT
			sku AS SKU_Config,
			created_at
	FROM
		 bob_live_mx.catalog_config
	GROUP BY sku;
	CREATE INDEX SKU_Config ON test_linio.Viviane_TMP(SKU_Config);

	UPDATE test_linio.Viviane_data a, test_linio.Viviane_TMP b
		SET
			a.Novelty = DATEDIFF(CURDATE(), b.created_at)
		WHERE a.SKU_Config = b.SKU_Config;

#COMMERCIAL STUFF
	DROP TABLE IF EXISTS test_linio.Viviane_TMP;
	CREATE TABLE test_linio.Viviane_TMP (PRIMARY KEY( SKU_Config ))
	SELECT
		 SkuConfig AS SKU_Config,
		 COUNT(*) AS Items_Sold_Last_7_Days,
		 SUM(Rev) AS Revenue_Last_7_Days
	FROM
		 development_mx.A_Master
	WHERE
			 OrderAfterCan = 1
	 AND Date >= NOW() - INTERVAL 7 DAY
	GROUP BY SKU_Config;
	CREATE INDEX SKU_Config ON test_linio.Viviane_TMP(SKU_Config);

	UPDATE test_linio.Viviane_data a
		 INNER JOIN test_linio.Viviane_TMP b
					ON a.SKU_Config = b.SKU_Config
	SET
		a.Daily_Items_Sold_Last_7_Days = b.Items_Sold_Last_7_Days / a.Days_Visible,
		a.Daily_Revenue_Sold_Last_7_Days = b.Revenue_Last_7_Days / a.Days_Visible;


	#SET PRICE
	DROP TABLE IF EXISTS test_linio.Viviane_TMP;
	CREATE TABLE test_linio.Viviane_TMP (PRIMARY KEY( SKU_Config ))
	SELECT
		LEFT(sku,15) AS SKU_Config,
		price AS Price
	FROM bob_live_mx.catalog_simple
	GROUP BY SKU_Config;
	CREATE INDEX SKU_Config ON test_linio.Viviane_TMP(SKU_Config);
	
	UPDATE test_linio.Viviane_data a, test_linio.Viviane_TMP b
		SET
			a.Price = b.Price
		WHERE a.SKU_Config = b.SKU_Config;

	#SET DISCOUNT
	DROP TABLE IF EXISTS test_linio.Viviane_TMP;
	CREATE TABLE test_linio.Viviane_TMP (PRIMARY KEY( SKU_Config ))
	SELECT
		LEFT(sku,15) AS SKU_Config,
		(price-special_price)/price AS Discount
	FROM bob_live_mx.catalog_simple
		WHERE special_from_date < NOW() AND NOW() < special_to_date
	GROUP BY SKU_Config;
	CREATE INDEX SKU_Config ON test_linio.Viviane_TMP(SKU_Config);
	
	UPDATE test_linio.Viviane_data a, test_linio.Viviane_TMP b
		SET
			a.Discount = b.Discount
		WHERE a.SKU_Config = b.SKU_Config;
	DROP TABLE IF EXISTS test_linio.Viviane_TMP;
	
  #open cursor
  OPEN cur1;
		set v_rowcount = found_rows();
		#starts the loop
		if v_rowcount > 0 then
			#get the values of each column into our variables
			FETCH cur1 INTO v_cat;
			#Insert it
			CALL boosting.Viviane_Normalize_Cat(v_cat);
		end if;
  CLOSE cur1;
	
	UPDATE test_linio.Viviane_data
  SET
   Score = Novelty_Norm + Daily_Items_Sold_Last_7_Days_Norm + Daily_Revenue_Sold_Last_7_Days_Norm + Price_Norm + Discount_Norm;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `Warranty_Check_reacondicionados`()
BEGIN

#Get SKUs offering warranty with name LIKE reacondicionado
select  b.sku,b.status,b.name, b.main_category, a.isVisible
from bob_live_mx.catalog_config b 
left join development_mx.A_Master_Catalog a
on a.sku_config = b.sku
where a.isVisible = 1
and b.name like '%eacondicionad%' 
and b.main_category <>""
and b.main_category in (select category from boosting.Warranty_Categories_rules);

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `Warranty_Items_Sold_With_Warranty`()
BEGIN
	#Make table with Warranty category rules
	DROP TABLE IF EXISTS `Warranty_Categories_rules`;
	CREATE TABLE `Warranty_Categories_rules` (
		`category` int(11) NOT NULL DEFAULT '0',
		`name` varchar(255) NOT NULL
	) ENGINE=MyISAM DEFAULT CHARSET=latin1;

	insert into boosting.Warranty_Categories_rules (category)
	select distinct category from bob_live_mx.supplementary_product_config ORDER BY category DESC;
	#Set main category name
	UPDATE boosting.Warranty_Categories_rules a, bob_live_mx.catalog_category b
	 SET a.name = b.name
	 WHERE a.category = b.id_catalog_category;

	SET FOREIGN_KEY_CHECKS=0;

	-- ----------------------------
	-- Table structure for `Warranty_Items_Sold_With_Warranty`
	-- ----------------------------
	DROP TABLE IF EXISTS `Warranty_Items_sold_with_warranty`;
	CREATE TABLE `Warranty_Items_sold_with_warranty` (
		`month_ordered` varchar(255) DEFAULT NULL,
		`date_ordered` date DEFAULT NULL,
		`month_delivered` varchar(255) DEFAULT NULL,
		`date_delivered` date DEFAULT NULL,
		`order_nr` varchar(255) DEFAULT NULL,
		`order_id` varchar(255) DEFAULT NULL,
		`warranty_code` varchar(255) DEFAULT NULL,
		`warranty_sku_simple` varchar(255) DEFAULT NULL,
		`warranty_name` varchar(255) DEFAULT NULL,
		`warranty_category` varchar(255) DEFAULT NULL,
		`warranty_cost` float DEFAULT NULL,
		`warranty_retail_price` float DEFAULT NULL,
		`warranty_profit` float DEFAULT NULL,
		`warranty_years_cov` tinyint DEFAULT NULL,
		`warranty_status_BOB` varchar(255) DEFAULT NULL,
		`product_code` varchar(255) DEFAULT NULL,
		`product_sku_config` varchar(255) DEFAULT NULL,
		`product_sku_simple` varchar(255) DEFAULT NULL,
		`product_name` varchar(255) DEFAULT NULL,
		`product_cost` float DEFAULT NULL,
		`product_retail_price` float DEFAULT NULL,
		`product_status_BOB` varchar(255) DEFAULT NULL,
		`Cat1_name` varchar(255) DEFAULT NULL,
		`Cat1_numb` int DEFAULT NULL,
		`Cat2_name` varchar(255) DEFAULT NULL,
		`Cat2_numb` int DEFAULT NULL,
		`Cat3_name` varchar(255) DEFAULT NULL,
		`Cat3_numb` int DEFAULT NULL,
		`main_category` int DEFAULT NULL,
		`main_category_name` varchar(255) DEFAULT NULL
	) ENGINE=MyISAM DEFAULT CHARSET=latin1;

	DELETE FROM boosting.Warranty_Items_sold_with_warranty;

	#Get Warranty name, order, creation, sku simple, retail price
	INSERT INTO boosting.Warranty_Items_sold_with_warranty(
		date_ordered,
		order_nr,
		warranty_sku_simple,
		warranty_name,
		warranty_retail_price,
		product_sku_simple,
		warranty_years_cov)
	SELECT
		created_at,
		fk_sales_order,
		sku,
		name,
		unit_price,
		sku_parent_supplementary,
		RIGHT(LEFT(name,LOCATE(' año', name)-1),1)
	FROM bob_live_mx.sales_order_item
	WHERE sku LIKE 'EW%' OR sku LIKE 'RW%';

	#Set order number
	UPDATE boosting.Warranty_Items_sold_with_warranty a, bob_live_mx.sales_order b
	 SET 
		a.order_id = b.order_nr
	 WHERE a.order_nr = b.id_sales_order;

	#Set main category
	UPDATE boosting.Warranty_Items_sold_with_warranty a, bob_live_mx.catalog_config b
	 SET a.main_category = b.main_category
	 WHERE b.sku = SUBSTR(a.product_sku_simple	, 1, 15);

	#Set main category name
	UPDATE boosting.Warranty_Items_sold_with_warranty a, bob_live_mx.catalog_category b
	 SET a.main_category_name = b.name
	 WHERE a.main_category = b.id_catalog_category;

	#Set the month_ordered
	UPDATE boosting.Warranty_Items_sold_with_warranty
	SET month_ordered = concat(year(date_ordered), '-', if(month(date_ordered)<10,concat(0,month(date_ordered)),month(date_ordered)));

	#Product status and code from production
	UPDATE boosting.Warranty_Items_sold_with_warranty w, production.tbl_order_detail t
	SET
		w.product_name = t.product_name,
		w.product_retail_price = t.unit_price,
		w.product_cost = t.cost_pet,
		w.product_code = t.item
	WHERE w.order_id = t.order_nr AND w.product_sku_simple = t.sku;

	#Warranty code from production
	UPDATE boosting.Warranty_Items_sold_with_warranty w, production.tbl_order_detail t
	SET
		w.warranty_code = t.item
	WHERE w.order_id = t.order_nr AND w.warranty_sku_simple = t.sku;

	#Insert Warranty Status from Bob 
	update boosting.Warranty_Items_sold_with_warranty we
	set warranty_status_BOB = 
	(select s.fk_sales_order_item_status from bob_live_mx.sales_order_item s 
  where s.id_sales_order_item = we.warranty_code);

	#Insert Product Status from Bob 
	update boosting.Warranty_Items_sold_with_warranty we
	set product_status_BOB = 
	(select s.fk_sales_order_item_status from bob_live_mx.sales_order_item s 
  where s.id_sales_order_item = we.product_code);

	#Set sku config
	UPDATE boosting.Warranty_Items_sold_with_warranty
	SET product_sku_config = LEFT(product_sku_simple, 15);
	
	#Set Cat1
	UPDATE boosting.Warranty_Items_sold_with_warranty w
	INNER JOIN bob_live_mx.catalog_config conf 
	ON w.product_sku_config=conf.sku 
	INNER JOIN bob_live_mx.catalog_attribute_option_global_category c 
	ON c.id_catalog_attribute_option_global_category=conf.fk_catalog_attribute_option_global_category 
	SET w.Cat1_name = c.name,	w.Cat1_numb = conf.fk_catalog_attribute_option_global_category;

	#Set Cat2
	UPDATE boosting.Warranty_Items_sold_with_warranty w
	INNER JOIN bob_live_mx.catalog_config conf 
	ON w.product_sku_config=conf.sku 
	INNER JOIN bob_live_mx.catalog_attribute_option_global_sub_category c 
	ON c.id_catalog_attribute_option_global_sub_category=conf.fk_catalog_attribute_option_global_sub_category 
	SET w.Cat2_name = c.name, w.Cat2_numb = conf.fk_catalog_attribute_option_global_sub_category;

	#Set Cat3
	UPDATE boosting.Warranty_Items_sold_with_warranty w
	INNER JOIN bob_live_mx.catalog_config conf 
	ON w.product_sku_config=conf.sku 
	INNER JOIN bob_live_mx.catalog_attribute_option_global_sub_sub_category c 
	ON c.id_catalog_attribute_option_global_sub_sub_category=conf.fk_catalog_attribute_option_global_sub_sub_category 
	SET w.Cat3_name = c.name, w.Cat3_numb = conf.fk_catalog_attribute_option_global_sub_sub_category;

	#Date delivered
	UPDATE boosting.Warranty_Items_sold_with_warranty w
	INNER JOIN rafael.out_order_tracking t
	ON w.order_id = t.order_number AND w.product_sku_simple=t.sku_simple 
	SET w.date_delivered = t.date_delivered;

	#Set the month_delivered
	UPDATE boosting.Warranty_Items_sold_with_warranty
	SET month_delivered = concat(year(date_delivered), '-', if(month(date_delivered)<10,concat(0,month(date_delivered)),month(date_delivered)));

	#Set Warranty category
	UPDATE boosting.Warranty_Items_sold_with_warranty w 
	SET warranty_category = 'Extended Warranty' 
	WHERE warranty_sku_simple like 'EW%';

	#Set Warranty category
	UPDATE boosting.Warranty_Items_sold_with_warranty w 
	SET warranty_category = 'Replacement Warranty' 
	WHERE warranty_sku_simple like 'RW%';

	#Warranty cost
	UPDATE boosting.Warranty_Items_sold_with_warranty w 
	INNER JOIN bob_live_mx.catalog_simple s 
	ON w.warranty_sku_simple = s.sku 
	SET w.warranty_cost = s.cost,
			w.warranty_profit = w.warranty_retail_price - s.cost;

	#Change ID for name 
	update boosting.Warranty_Items_sold_with_warranty we 
	set product_status_BOB = 
	(select b.name from bob_live_mx.sales_order_item_status b where b.id_sales_order_item_status = we.product_status_BOB);
	#Change ID for name 
	update boosting.Warranty_Items_sold_with_warranty we 
	set warranty_status_BOB = 
	(select b.name from bob_live_mx.sales_order_item_status b where b.id_sales_order_item_status = we.warranty_status_BOB);

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `Warranty_Reconciliation`()
BEGIN

	#Make table with warranty sold emails.
	DROP TABLE IF EXISTS `Warranty_Emails_sent`;
	create table `Warranty_Emails_sent`(
		mail_certificate VARCHAR(255),
		mail_user VARCHAR (255),
		mail_SKU VARCHAR(255),
		mail_address VARCHAR(255),
		mail_state VARCHAR(255),
		mail_status_code VARCHAR(255),
		mail_date VARCHAR(255));
	insert into boosting.Warranty_Emails_sent(
		mail_certificate,
		mail_user,
		mail_SKU,
		mail_address,
		mail_state,
		mail_status_code,
		mail_date)
	SELECT
		RIGHT(url, LENGTH(url) - locate("certificate=", url) - 11),
		RIGHT(url, LENGTH(url) - locate("first_name=", url) - 10),
		RIGHT(url, LENGTH(url) - locate("item_sku=", url) - 8),
		RIGHT(url , LENGTH(url) - locate("email=", url) - 5),
		state,
		status_code,
		created_at
	FROM bob_live_mx.ums
	where (url like '%AID=69092');

	UPDATE boosting.Warranty_Emails_sent
	SET mail_certificate = LEFT(mail_certificate, locate("&", mail_certificate) - 1),
			mail_user = LEFT(mail_user, locate("&", mail_user) - 1),
			mail_SKU = LEFT(mail_SKU, locate("&", mail_SKU) - 1),
			mail_address = REPLACE(LEFT(mail_address, locate("&", mail_address) - 1), '%40', '@');

		#Create reconciliation table and add sales data.
		DROP TABLE IF EXISTS boosting.Warranty_Reconciliation;
		CREATE TABLE boosting.Warranty_Reconciliation(
			`sales_month_ordered` varchar(255) DEFAULT NULL,
			`sales_date_ordered` date DEFAULT NULL,
			`sales_month_delivered` varchar(255) DEFAULT NULL,
			`sales_date_delivered` date DEFAULT NULL,
			`sales_order_nr` varchar(255) DEFAULT NULL,
			`sales_order_id` varchar(255) DEFAULT NULL,
			`sales_warranty_code` varchar(255) DEFAULT NULL,
			`sales_warranty_sku_simple` varchar(255) DEFAULT NULL,
			`sales_warranty_name` varchar(255) DEFAULT NULL,
			`sales_warranty_category` varchar(255) DEFAULT NULL,
			`sales_warranty_cost` float DEFAULT NULL,
			`sales_warranty_retail_price` float DEFAULT NULL,
			`sales_warranty_years_cov` tinyint DEFAULT NULL,
			`sales_warranty_status_BOB` varchar(255) DEFAULT NULL,
			`sales_product_code` varchar(255) DEFAULT NULL,
			`sales_product_sku_config` varchar(255) DEFAULT NULL,
			`sales_product_sku_simple` varchar(255) DEFAULT NULL,
			`sales_product_name` varchar(255) DEFAULT NULL,
			`sales_product_cost` float DEFAULT NULL,
			`sales_product_retail_price` float DEFAULT NULL,
			`sales_product_status_BOB` varchar(255) DEFAULT NULL,
			`sales_Cat1_name` varchar(255) DEFAULT NULL,
			`sales_Cat1_numb` int DEFAULT NULL,
			`sales_Cat2_name` varchar(255) DEFAULT NULL,
			`sales_Cat2_numb` int DEFAULT NULL,
			`sales_Cat3_name` varchar(255) DEFAULT NULL,
			`sales_Cat3_numb` int DEFAULT NULL,
			`sales_main_category` int DEFAULT NULL,
			`sales_main_category_name` varchar(255) DEFAULT NULL,
			assurant_product_code INT DEFAULT NULL,
			assurant_product_price DECIMAL (14,2) DEFAULT NULL,
			assurant_price_policiy DECIMAL (5,4) DEFAULT NULL,
			assurant_sales_tax DECIMAL (14,2) DEFAULT NULL,
			assurant_month_comp  varchar(255) DEFAULT NULL,
			assurant_date_comp	date DEFAULT NULL,
			assurant_month_sold	varchar	(255) DEFAULT NULL,
			assurant_date_sold	date DEFAULT NULL,
			assurant_manufacturer	varchar	(50) DEFAULT NULL,
			assurant_model	varchar	(30) DEFAULT NULL,
			assurant_fk_sales_order_ew_assurant_status  INT DEFAULT NULL,
			assurant_certificate varchar (255) DEFAULT NULL,
			assurant_customer_name varchar (255) DEFAULT NULL,
			assurant_email varchar (255) DEFAULT NULL,
			mail_certificate VARCHAR (255) DEFAULT NULL,
			mail_user VARCHAR (255) DEFAULT NULL,
			mail_SKU VARCHAR(255) DEFAULT NULL,
			mail_address VARCHAR(255) DEFAULT NULL,
			mail_state VARCHAR(255) DEFAULT NULL,
			mail_status_code VARCHAR(255) DEFAULT NULL,
			mail_date date DEFAULT NULL
			);
		INSERT INTO boosting.Warranty_Reconciliation (
			`sales_month_ordered`,
			`sales_date_ordered`,
			`sales_month_delivered`,
			`sales_date_delivered`,
			`sales_order_nr`,
			`sales_order_id`,
			`sales_warranty_code`,
			`sales_warranty_sku_simple`,
			`sales_warranty_name`,
			`sales_warranty_category`,
			`sales_warranty_cost`,
			`sales_warranty_retail_price`,
			`sales_warranty_years_cov`,
			`sales_warranty_status_BOB`,
			`sales_product_code`,
			`sales_product_sku_config`,
			`sales_product_sku_simple`,
			`sales_product_name`,
			`sales_product_cost`,
			`sales_product_retail_price`,
			`sales_product_status_BOB`,
			`sales_Cat1_name`,
			`sales_Cat1_numb`,
			`sales_Cat2_name`,
			`sales_Cat2_numb`,
			`sales_Cat3_name`,
			`sales_Cat3_numb`,
			`sales_main_category`,
			`sales_main_category_name` 
		)
		SELECT
			`month_ordered`,
			`date_ordered`,
			`month_delivered`,
			`date_delivered`,
			`order_nr`,
			`order_id`,
			`warranty_code`,
			`warranty_sku_simple`,
			`warranty_name`,
			`warranty_category`,
			`warranty_cost`,
			`warranty_retail_price`,
			`warranty_years_cov`,
			`warranty_status_BOB`,
			`product_code`,
			`product_sku_config`,
			`product_sku_simple`,
			`product_name`,
			`product_cost`,
			`product_retail_price`,
			`product_status_BOB`,
			`Cat1_name`,
			`Cat1_numb`,
			`Cat2_name`,
			`Cat2_numb`,
			`Cat3_name`,
			`Cat3_numb`,
			`main_category`,
			`main_category_name` 
		 FROM boosting.Warranty_Items_sold_with_warranty;

    
		#Update matching data of assurant table
		UPDATE boosting.Warranty_Reconciliation a, bob_live_mx.sales_order_ew_assurant b
		SET
			a.assurant_product_code = b.item_code,
			a.assurant_product_price = b.item_price,
			a.assurant_price_policiy = b.price_pol,
			a.assurant_sales_tax = sales_tax,
			a.assurant_date_comp = concat(RIGHT(b.date_comp,4), '-', LEFT(RIGHT(b.date_comp,6),2), '-', LEFT(b.date_comp,2)),
			a.assurant_date_sold = concat(RIGHT(b.extwar_saledate,4), '-', LEFT(RIGHT(b.extwar_saledate,6),2), '-', LEFT(b.extwar_saledate,2)),
			a.assurant_manufacturer = b.manufacturer,
			a.assurant_model = b.model,
			a.assurant_fk_sales_order_ew_assurant_status = b.fk_sales_order_ew_assurant_status,
			a.assurant_certificate = b.certificate
		WHERE a.sales_product_code = b.item_code;


		#Add data that was not mached
		INSERT INTO boosting.Warranty_Reconciliation(
			assurant_product_code,
			assurant_product_price,
			assurant_price_policiy,
			assurant_sales_tax,
			assurant_date_comp,
			assurant_date_sold,
			assurant_manufacturer,
			assurant_model,
			assurant_fk_sales_order_ew_assurant_status,
			assurant_certificate)
		SELECT 
			item_code,
			item_price,
			price_pol,
			sales_tax,
			concat(RIGHT(date_comp,4), '-', LEFT(RIGHT(date_comp,6),2), '-', LEFT(date_comp,2)),
			concat(RIGHT(extwar_saledate,4), '-', LEFT(RIGHT(extwar_saledate,6),2), '-', LEFT(extwar_saledate,2)),
			manufacturer,
			model,
			fk_sales_order_ew_assurant_status,
			certificate
		FROM bob_live_mx.sales_order_ew_assurant
		WHERE item_code not in (SELECT DISTINCT sales_product_code FROM boosting.Warranty_Reconciliation WHERE assurant_certificate is not null);

		#Create the mont year columns
		UPDATE boosting.Warranty_Reconciliation
		SET assurant_month_comp = concat(year(assurant_date_comp), '-',  if(month(assurant_date_comp)<10, concat(0,month(assurant_date_comp)), month(assurant_date_comp))),
				assurant_month_sold = concat(year(assurant_date_sold), '-',  if(month(assurant_date_sold)<10, concat(0,month(assurant_date_sold)), month(assurant_date_sold)));
		
		#Update matching mails with sales data
		UPDATE boosting.Warranty_Reconciliation a, boosting.Warranty_Emails_sent b
		SET a.mail_certificate = b.mail_certificate,
				a.mail_user = b.mail_user,
				a.mail_SKU = b.mail_SKU,
				a.mail_address = b.mail_address,
				a.mail_state = b.mail_state,
				a.mail_status_code = b.mail_status_code,
				a.mail_date = b.mail_date
		WHERE a.assurant_certificate =  b.mail_certificate;
		
		#Insert data that was not mached.
		INSERT INTO boosting.Warranty_Reconciliation(
			mail_certificate,
			mail_user,
			mail_SKU,
			mail_address,
			mail_state,
			mail_status_code,
			mail_date)
		SELECT
			mail_certificate,
			mail_user,
			mail_SKU,
			mail_address,
			mail_state,
			mail_status_code,
			mail_date
		FROM boosting.Warranty_Emails_sent
		WHERE mail_certificate NOT IN (SELECT Distinct assurant_certificate FROM boosting.Warranty_Reconciliation WHERE assurant_certificate is not null);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `Warranty_Sales_of_products_that_offer_warranty`()
BEGIN
	#Make table with Warranty category rules
	DROP TABLE IF EXISTS `Warranty_Categories_rules`;
	CREATE TABLE `Warranty_Categories_rules` (
		`category` int(11) NOT NULL DEFAULT '0',
		`name` varchar(255) NOT NULL
	) ENGINE=MyISAM DEFAULT CHARSET=latin1;

	insert into boosting.Warranty_Categories_rules (category)
	select distinct category from bob_live_mx.supplementary_product_config ORDER BY category DESC;
	#Set main category name
	UPDATE boosting.Warranty_Categories_rules a, bob_live_mx.catalog_category b
	 SET a.name = b.name
	 WHERE a.category = b.id_catalog_category;

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `warranty_extended`
-- ----------------------------
DROP TABLE IF EXISTS `Warranty_Sales_of_products_that_offer_warranty`;
CREATE TABLE `Warranty_Sales_of_products_that_offer_warranty` (
  `month_ordered` int(11) DEFAULT NULL,
  `date_ordered` date DEFAULT NULL,
  `month_delivered` int(11) DEFAULT NULL,
  `date_delivered` date DEFAULT NULL,
  `order_nr` varchar(255) DEFAULT NULL,
  `item_code` varchar(255) DEFAULT NULL,
  `product_sku_config` varchar(255) DEFAULT NULL,
  `product_sku_simple` varchar(255) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_cost` float DEFAULT NULL,
  `product_retail_price` float DEFAULT NULL,
  `Cat1` varchar(255) DEFAULT NULL,
  `Cat2` varchar(255) DEFAULT NULL,
  `Cat3` varchar(255) DEFAULT NULL,
  `status_item` varchar(255) DEFAULT NULL,
 `status_item_Bob` varchar(255) DEFAULT NULL,
  `order_total` float DEFAULT NULL,
 `main_category` int DEFAULT NULL,
 `main_category_name` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DELETE FROM boosting.Warranty_Sales_of_products_that_offer_warranty;

INSERT INTO boosting.Warranty_Sales_of_products_that_offer_warranty(date_ordered, order_nr, product_sku_simple,main_category, main_category_name)
SELECT i.created_at, o.order_nr, i.sku, c.main_category, d.name
from bob_live_mx.sales_order_item i 
inner join bob_live_mx.catalog_config c on c.sku = SUBSTR(i.sku, 1, 15)
inner join bob_live_mx.sales_order o on i.fk_sales_order = o.id_sales_order
inner join bob_live_mx.catalog_category d on c.main_category = d.id_catalog_category
where c.main_category <> "" and c.main_category in (select category from boosting.Warranty_Categories_rules);

#Set the month_ordered
UPDATE boosting.Warranty_Sales_of_products_that_offer_warranty
SET month_ordered = concat(year(date_ordered), if(month(date_ordered)<10,concat(0,month(date_ordered)),month(date_ordered)));

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
UPDATE boosting.Warranty_Sales_of_products_that_offer_warranty w
#Cambiar status_item por el de BOB
INNER JOIN production.tbl_order_detail t
ON w.order_nr = t.order_nr
SET 
  w.status_item = t.status_item,
  w.item_code = t.item;

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
UPDATE boosting.Warranty_Sales_of_products_that_offer_warranty w
#Cambiar status_item por el de BOB
INNER JOIN production.tbl_order_detail t
ON w.product_sku_simple = t.sku AND w.order_nr = t.order_nr
SET 
  w.product_sku_config = t.sku_config,
  w.product_name = t.product_name,
  w.product_retail_price = t.unit_price,
  w.product_cost = t.cost_pet,
  w.status_item = t.status_item,
  w.item_code = t.item;

#Set Cat1
UPDATE boosting.Warranty_Sales_of_products_that_offer_warranty w
INNER JOIN bob_live_mx.catalog_config conf 
ON w.product_sku_config=conf.sku 
INNER JOIN bob_live_mx.catalog_attribute_option_global_category c 
ON c.id_catalog_attribute_option_global_category=conf.fk_catalog_attribute_option_global_category 
SET w.Cat1 = c.name;

#Set Cat2
UPDATE boosting.Warranty_Sales_of_products_that_offer_warranty w
INNER JOIN bob_live_mx.catalog_config conf 
ON w.product_sku_config=conf.sku 
INNER JOIN bob_live_mx.catalog_attribute_option_global_sub_category c 
ON c.id_catalog_attribute_option_global_sub_category=conf.fk_catalog_attribute_option_global_sub_category 
SET w.Cat2 = c.name;

#Set Cat3
UPDATE boosting.Warranty_Sales_of_products_that_offer_warranty p 
INNER JOIN bob_live_mx.catalog_config conf 
ON p.product_sku_config=conf.sku 
INNER JOIN bob_live_mx.catalog_attribute_option_global_sub_sub_category c 
ON c.id_catalog_attribute_option_global_sub_sub_category=conf.fk_catalog_attribute_option_global_sub_sub_category 
SET p.Cat3 = c.name;

#Date delivered
UPDATE boosting.Warranty_Sales_of_products_that_offer_warranty w 
INNER JOIN rafael.out_order_tracking t 
ON w.order_nr=t.order_number AND w.product_sku_simple=t.sku_simple 
SET w.date_delivered=t.date_delivered;

#Set the month_delivered
UPDATE boosting.Warranty_Sales_of_products_that_offer_warranty
SET month_delivered = concat(year(date_delivered), if(month(date_delivered)<10,concat(0,month(date_delivered)),month(date_delivered)));

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
UPDATE boosting.Warranty_Sales_of_products_that_offer_warranty w 
SET order_total = (SELECT sum(actual_paid_price) 
FROM production.tbl_order_detail t 
WHERE t.order_nr = w.order_nr group by order_nr);

#Insert Status from Bob 
update boosting.Warranty_Sales_of_products_that_offer_warranty we
set status_item_Bob = 
(select s.fk_sales_order_item_status from bob_live_mx.sales_order_item s where s.id_sales_order_item = we.item_code);

#Change ID for name 
update boosting.Warranty_Sales_of_products_that_offer_warranty we 
set status_item_Bob = 
(select b.name from bob_live_mx.sales_order_item_status b where b.id_sales_order_item_status = we.status_item_Bob);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `Warranty_Stock`()
BEGIN
	DECLARE var VARCHAR(50) DEFAULT NULL;
  #declare cursor
  DECLARE cur1 CURSOR FOR SELECT DISTINCT sku FROM  bob_live_mx.catalog_simple WHERE sku LIKE 'EW%' OR sku LIKE 'RW%';

	DROP TABLE IF EXISTS `Warranty_Stock`;
	CREATE TABLE `Warranty_Stock` (
		`id_stock_import` int(11) NOT NULL,
		`file_name` varchar(255) NOT NULL,
		`created_by` varchar(50) NOT NULL,
		`created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

	#open cursor
  OPEN cur1;
  #starts the loop
  the_loop: LOOP
 
    #get the values of each column into our variables
    FETCH cur1 INTO var;
 
    #Insert it
    CALL boosting.Warranty_Stock_Insert(var, "2014/01/01 00:00:00");
  END LOOP the_loop;
  CLOSE cur1;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `Warranty_Stock_Insert`(IN WarrantySKU VARCHAR(255), StartDate DATETIME)
BEGIN
	SET @WSKU = WarrantySKU;
	SET @SDate = StartDate;
	SET @s = CONCAT('INSERT INTO boosting.Warranty_Stock (id_stock_import, file_name, created_by, created_at)
	SELECT id_stock_import, file_name, created_by, created_at FROM bob_live_mx.stock_import
	WHERE file_content LIKE "%',@WSKU, '%" AND created_at > @SDate AND id_stock_import not in (SELECT DISTINCT id_stock_import FROM boosting.Warranty_Stock);');
		PREPARE stmt1 FROM @s; EXECUTE stmt1; DEALLOCATE PREPARE stmt1; 
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:03:32
