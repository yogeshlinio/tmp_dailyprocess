-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: second_purchase
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'second_purchase'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`david.lobo`@`%`*/ /*!50003 PROCEDURE `churn_model`()
BEGIN

-- Index
/*
create index requester_email on customer_service.tbl_zendesk(requester_email);

create index email on bob_live_mx.customer(email);

create index order_item_id on production.out_order_tracking(order_item_id);

create index sales_rule on bob_live_mx.sales_rule_set (id_sales_rule_set);

create index sales_rule on bob_live_mx.sales_rule (fk_sales_rule_set);

create index code on bob_live_mx.sales_rule (code);

*/

delete from churn_model;

insert into churn_model(customerid, order_price, order_tax, order_quantity, order_min_item_price, order_max_item_price, order_avg_item_price, pc1, `%pc1`, shipping_cost, voucher, voucher_amount, day_purchased, month_purchased, year_purchased, season_purchased, date_purchased) select custid, sum(unit_price), sum(unit_price)-sum(unit_price_after_vat), count(*), min(unit_price), max(unit_price), avg(unit_price), sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)), sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0))/sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)), sum(shipping_cost_per_sku), case when coupon_code is null then 0 else 1 end, coupon_money_value, day(date), month(date), year(date), case when month(date) between 1 and 3 then 'Spring' when month(date) between 4 and 6 then 'Summer' when month(date) between 7 and 9 then 'Fall' when month(date) between 10 and 12 then 'Winter' end, date from production.tbl_order_detail where oac=1 and new_customers is not null group by custid;

update churn_model set season_purchased = 'Spring' where (date_purchased between '2012-03-20' and '2012-06-20') or (date_purchased between '2013-03-20' and '2013-06-20');

update churn_model set season_purchased = 'Summer' where (date_purchased between '2012-06-21' and '2012-09-21') or (date_purchased between '2013-06-21' and '2013-09-21');

update churn_model set season_purchased = 'Fall' where (date_purchased between '2012-09-22' and '2012-12-20') or (date_purchased between '2013-09-22' and '2013-12-20');

update churn_model set season_purchased = 'Winter' where (date_purchased between '2012-12-20' and '2013-03-19') or (date_purchased between '2013-12-20' and '2014-03-19');

update churn_model c inner join bob_live_mx.customer_address a on c.customerid=a.fk_customer inner join bob_live_mx.customer_address_city y on y.id_customer_address_city=a.fk_customer_address_city set c.city=y.name;

update churn_model c inner join bob_live_mx.customer_address a on c.customerid=a.fk_customer inner join bob_live_mx.customer_address_region y on y.id_customer_address_region=a.fk_customer_address_region set c.region=y.name;

update churn_model c set itemid_highest_product =  (select item from production.tbl_order_detail x where unit_price in (select max(unit_price) from production.tbl_order_detail y where x.order_nr=y.order_nr and oac=1 and new_customers is not null group by order_nr) and oac=1 and new_customers is not null and c.customerid=x.custid group by custid);

update churn_model c set days_until_2nd_purchase = (select datediff(x.date, c.date_purchased) from production.tbl_order_detail t, (select custid, min(date) as date from production.tbl_order_detail where oac=1 and new_customers is null and date!=c.date_purchased group by custid)x where x.custid=t.custid and c.customerid=t.custid group by c.customerid);

update churn_model c inner join production.tbl_order_detail t on c.itemid_highest_product=t.item set c.max_item_product_category = t.new_cat1, c.brand_or_generic = t.brand, c.campaign_purchased=t.campaign, c.payment_type=t.payment_method, c.installments=t.installment, c.channel_group_purchased=t.channel_group, c.channel_purchased=t.channel;

update churn_model c set installments = 1 where installments>=1;

update churn_model c set installments = 0 where installments is null;

update churn_model c set voucher_percentage=voucher_percentage/100;

update churn_model c set payment_type = 'Other Online' where payment_type in ('Adyen_HostedPaymentPage', 'DineroMail_HostedPaymentPage', 'DineroMail_Api', 'Adquira_Interredes');

update churn_model c set payment_type = 'Cash on Delivery' where payment_type in ('CashOnDelivery_Payment');

update churn_model c set payment_type = 'Debit Online' where payment_type in ('Paypal_Express_Checkout', 'Banorte_Payworks_Debit');

update churn_model c set payment_type = 'Credit Online' where payment_type in ('Amex_Gateway', 'Banorte_Payworks');

update churn_model c set payment_type = 'Deposit' where payment_type in ('Oxxo_Direct', 'Bancomer_PagoReferenciado');

update churn_model c set payment_type = 'Credit on Delivery' where payment_type in ('CreditCardOnDelivery_Payment');

update churn_model c inner join development_mx.A_Master a on c.itemid_highest_product=a.itemid set linio_or_mkt_place = a.isMPlace;

update churn_model c inner join bob_live_mx.customer_address a on c.customerid=a.fk_customer set c.city=a.city, c.region=a.region, c.postcode=a.postcode;

update churn_model c inner join bob_live_mx.customer a on c.customerid=a.id_customer set c.gender=a.gender, c.customer_age=datediff(curdate(),a.birthday)/365;

update churn_model c inner join production.out_order_tracking o on c.itemid_highest_product=o.order_item_id set c.time_purchase_vs_actual_delivery=datediff(o.date_delivered,c.date_purchased);

update churn_model c inner join production.out_order_tracking o on c.itemid_highest_product=o.order_item_id set c.time_purchase_vs_promised_delivery=datediff(o.date_delivered_promised,c.date_purchased);

update churn_model c inner join production.out_order_tracking o on c.itemid_highest_product=o.order_item_id set c.time_promised_delivery_vs_actual_delivery=datediff(o.date_delivered,o.date_delivered_promised);

update churn_model c inner join production.out_order_tracking o on c.itemid_highest_product=o.order_item_id set c.carrier=o.shipping_carrier;

update churn_model c inner join production.out_order_tracking o on c.itemid_highest_product=o.order_item_id set c.number_delivery_attempts=case when datediff(o.date_delivered, o.date_1st_attempt)=0 then 1 when datediff(o.date_delivered, o.date_1st_attempt)!=0 then 2 when o.date_1st_attempt is null then 0 end;

update churn_model c set directdeposit_bank=1 where payment_type in ('Oxxo_Direct', 'Banorte_PagoReferenciado');

update churn_model c set directdeposit_bank=0 where directdeposit_bank is null;

update churn_model c set creditcard=1 where payment_type in ('CreditCardOnDelivery_Payment');

update churn_model c set creditcard=0 where creditcard is null;

update churn_model c set c.return_before_first_purchase = (select case when sum(returned)>0 then 1 else 0 end from production.tbl_order_detail t where t.custid=c.customerid and t.date<c.date_purchased and obc=1 group by t.custid);

update churn_model c set c.return_after_first_purchase = (select case when sum(returned)>0 then 1 else 0 end from production.tbl_order_detail t where t.custid=c.customerid and t.date>c.date_purchased and obc=1 group by t.custid);

update churn_model c set c.number_returns_before_first_purchase = (select count(distinct order_nr) from production.tbl_order_detail t where t.custid=c.customerid and t.date<c.date_purchased and obc=1 and returned=1 group by t.custid);

update churn_model c set c.number_returns_after_first_purchase = (select count(distinct order_nr) from production.tbl_order_detail t where t.custid=c.customerid and t.date between c.date_purchased and (select date from production.tbl_order_detail y where oac=1 and new_customers is null and t.custid=y.custid group by custid ) and obc=1 and returned=1 group by t.custid);

update churn_model c set c.return_before_first_purchase = 0 where c.return_before_first_purchase is null;

update churn_model c set c.cancellation_before_first_purchase = (select case when sum(cancel)>0 then 1 else 0 end from production.tbl_order_detail t where t.custid=c.customerid and t.date<c.date_purchased and obc=1 group by t.custid);

update churn_model c set c.cancellation_after_first_purchase = (select case when sum(cancel)>0 then 1 else 0 end from production.tbl_order_detail t where t.custid=c.customerid and t.date>c.date_purchased and obc=1 group by t.custid);

update churn_model c set c.number_cancellations_before_first_purchase = (select count(distinct order_nr) from production.tbl_order_detail t where t.custid=c.customerid and t.date<c.date_purchased and obc=1 and cancel=1 group by t.custid);

update churn_model c set c.number_cancellations_after_first_purchase = (select count(distinct order_nr) from production.tbl_order_detail t where t.custid=c.customerid and t.date between c.date_purchased and (select date from production.tbl_order_detail y where oac=1 and new_customers is null and t.custid=y.custid group by custid ) and obc=1 and cancel=1 group by t.custid);

update churn_model c set c.cancellation_before_first_purchase = 0 where c.cancellation_before_first_purchase is null;

update churn_model c inner join production.tbl_order_detail t on c.itemid_highest_product=t.item set c.orderid=t.orderid, c.purchase_number=t.order_nr;

update churn_model c set c.total_number_purchases = (select count(distinct order_nr) from production.tbl_order_detail t where c.customerid=t.custid and t.oac=1);

update churn_model c inner join production.tbl_order_detail t on c.itemid_highest_product=t.item set  c.voucher_percentage_of_price = (select sum(t.coupon_money_value)/c.order_price from production.tbl_order_detail t where c.purchase_number=t.order_nr);

update churn_model c inner join production.tbl_order_detail t on c.itemid_highest_product=t.item inner join bob_live_mx.sales_rule r on r.code=t.coupon_code inner join bob_live_mx.sales_rule_set s on s.id_sales_rule_set=r.fk_sales_rule_set set c.type_of_voucher=s.discount_type, c.voucher_percentage=s.discount_percentage;

update churn_model c set churner = (select case when datediff(x.date, c.date_purchased)<120 then 0 else 1 end from production.tbl_order_detail t, (select custid, min(date) as date from production.tbl_order_detail where oac=1 and new_customers is null group by custid)x where x.custid=t.custid and c.customerid=t.custid group by c.customerid);

update churn_model c set churner = 0 where churner is null;

update churn_model c inner join bob_live_mx.sales_order s on c.purchase_number=s.order_nr set c.hour_purchased=hour(s.created_at);

update churn_model c inner join bob_live_mx.customer a on c.customerid=a.id_customer inner join customer_service.tbl_zendesk z on a.email=z.requester_email and c.purchase_number=z.Numero_del_pedido set c.method_of_contact=z.Canal_de_contacto, c.reason_of_contact=z.clasificacion, c.length_of_call=z.full_resolution_time_in_minutes, c.length_of_waiting=requester_wait_time_in_minutes where c.method_of_contact='llamada';

update churn_model c inner join bob_live_mx.customer a on a.id_customer=c.customerid set c.email=a.email;

update churn_model c inner join (SELECT 
    Date(`Date Submitted`)DateSubmitted,
    
   email,
   "Net promoter score" AS KPI,
   "Promoters (9 and 10) - detractors (0 through 6); as per net promoter score survey done to new customers" AS Description,
   round( (sum(if(NPS in(11,10),1,0))-
sum(if(NPS in(1,2,3,4,5,6,7),1,0)))/count(NPS) * 100  ) AS  Value
    
FROM 
    customer_service.questionProMX
  
GROUP BY DateSubmitted,email)a on a.email=c.email set c.NPS_score=a.value;

update churn_model c inner join test.abc a on a.email=c.email set c.NPS_score=a.value where datediff(DateSubmitted, date_purchased)<30;

update churn_model c inner join customer_service.questionProMX p on c.order_purchased=p.ordernumber set c.NPS_score=p.NPS;

update churn_model c inner join customer_service.questionProMX p on c.email=p.email set c.NPS_score=p.NPS where datediff(`Date Submitted`, date_purchased)<30;

UPDATE second_purchase.churn_model a INNER JOIN  second_purchase.churn_model_2 b  ON a.purchase_number = b.purchase_number SET 	a.tickets_per_order = b.tickets_per_order,	a.replies_per_order = b.replies_per_order , a.is_Solved = b.is_Solved;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 PROCEDURE `churn_model_2`()
BEGIN
TRUNCATE churn_model_2;

INSERT INTO churn_model_2 (
	customerid,
	#order_price,
	#order_tax,
	#order_quantity,
	#order_min_item_price,
	#order_max_item_price,
	#order_avg_item_price,
	#pc1,
	#`%pc1`,
	shipping_cost,
	#voucher,
	#voucher_amount,
	#day_purchased,
	#month_purchased,
	#year_purchased,
	#season_purchased,
	date_purchased
	) 
SELECT
	custid,
	#sum(unit_price),
	#sum(unit_price) - sum(unit_price_after_vat),
	#count(*),
	#min(unit_price),
	#max(unit_price),
	#avg(unit_price),
	#sum(	ifnull(unit_price_after_vat, 0) 
	#		- ifnull(coupon_money_after_vat, 0) 
	#		+ ifnull(shipping_fee_after_vat, 0) 
	#		- ifnull(costo_after_vat, 0) 
	#		- ifnull(delivery_cost_supplier, 0)),
	#sum(	ifnull(unit_price_after_vat, 0) 
	#		- ifnull(coupon_money_after_vat, 0) 
	#		+ ifnull(shipping_fee_after_vat, 0) 
	#		- ifnull(costo_after_vat, 0) 
	#		- ifnull(delivery_cost_supplier, 0)) / 
	#sum(	ifnull(unit_price_after_vat, 0) 
	#		- ifnull(coupon_money_after_vat, 0) 
	#		+ ifnull(shipping_fee_after_vat, 0)),
	sum(shipping_cost_per_sku),
	#CASE	WHEN coupon_code IS NULL 
	#			THEN 0
	#			ELSE 1
	#END,
	#coupon_money_value,
	#DAY (date),
	#MONTH (date),
	#YEAR (date),
	#CASE	WHEN MONTH (date) BETWEEN 1 AND 3 
	#			THEN 'Spring'
	#			WHEN MONTH (date) BETWEEN 4 AND 6 
	#			THEN 'Summer'
	#			WHEN MONTH (date) BETWEEN 7 AND 9 
	#			THEN 'Fall'
	#			WHEN MONTH (date) BETWEEN 10 AND 12 
	#			THEN 'Winter'
	#END,
 date
FROM production.tbl_order_detail
WHERE	oac = 1
AND 	new_customers IS NOT NULL
GROUP BY
	custid;
/*
UPDATE churn_model_2
SET season_purchased = 'Spring'
	WHERE	(date_purchased BETWEEN '2012-03-20' AND '2012-06-20')
	OR 		(date_purchased BETWEEN '2013-03-20' AND '2013-06-20');

UPDATE churn_model_2
SET season_purchased = 'Summer'
	WHERE (date_purchased BETWEEN '2012-06-21' AND '2012-09-21')
	OR 		(date_purchased BETWEEN '2013-06-21' AND '2013-09-21');

UPDATE churn_model_2
SET season_purchased = 'Fall'
	WHERE	(date_purchased BETWEEN '2012-09-22' AND '2012-12-20')
	OR 		(date_purchased BETWEEN '2013-09-22' AND '2013-12-20');

UPDATE churn_model_2
SET season_purchased = 'Winter'
	WHERE	(date_purchased BETWEEN '2012-12-20' AND '2013-03-19')
	OR 		(date_purchased BETWEEN '2013-12-20' AND '2014-03-19');
*/
UPDATE churn_model_2 c
INNER JOIN bob_live_mx.customer_address a ON c.customerid = a.fk_customer
INNER JOIN bob_live_mx.customer_address_city y ON y.id_customer_address_city = a.fk_customer_address_city
SET c.city = y. NAME;

UPDATE churn_model_2 c
INNER JOIN bob_live_mx.customer_address a ON c.customerid = a.fk_customer
INNER JOIN bob_live_mx.customer_address_region y ON y.id_customer_address_region = a.fk_customer_address_region
SET c.region = y. NAME;


UPDATE churn_model_2 c
SET itemid_highest_product = (SELECT
																item
															FROM
																production.tbl_order_detail x
															WHERE
																unit_price IN (	SELECT
																									max(unit_price)
																								FROM
																									production.tbl_order_detail y
																								WHERE
																									x.order_nr = y.order_nr
																								AND oac = 1
																								AND new_customers IS NOT NULL
																								GROUP BY
																									order_nr
																							)
															AND oac = 1
															AND new_customers IS NOT NULL
															AND c.customerid = x.custid
															GROUP BY
																custid
);

/*
UPDATE churn_model_2 c
SET days_until_2nd_purchase = (SELECT
																datediff(x.date, c.date_purchased)
															FROM
																production.tbl_order_detail t,
																(
																	SELECT
																		custid,
																		min(date) AS date
																	FROM
																		production.tbl_order_detail
																	WHERE
																		oac = 1
																	AND new_customers IS NULL
																	AND date != c.date_purchased
																	GROUP BY
																		custid
																) x
															WHERE
																x.custid = t.custid
															AND c.customerid = t.custid
															GROUP BY
																c.customerid
);
*/
/*
UPDATE churn_model_2 c
INNER JOIN production.tbl_order_detail t ON c.itemid_highest_product = t.item
SET 
 c.max_item_product_category = t.new_cat1,
 c.brand_or_generic = t.brand,
 c.campaign_purchased = t.campaign,
 c.payment_type = t.payment_method,
 c.installments = t.installment,
 c.channel_group_purchased = t.channel_group,
 c.channel_purchased = t.channel;
*/
/*
UPDATE churn_model_2 c
SET installments = 1
WHERE
	installments >= 1;
*/
/*
UPDATE churn_model_2 c
SET installments = 0
WHERE
	installments IS NULL;
*/
/*
UPDATE churn_model_2 c
SET voucher_percentage = voucher_percentage / 100;
*/
/*
UPDATE churn_model_2 c
SET payment_type = 'Other Online'
WHERE
	payment_type IN (
		'Adyen_HostedPaymentPage',
		'DineroMail_HostedPaymentPage',
		'DineroMail_Api',
		'Adquira_Interredes'
	);

UPDATE churn_model_2 c
SET payment_type = 'Cash on Delivery'
WHERE
	payment_type IN ('CashOnDelivery_Payment');

UPDATE churn_model_2 c
SET payment_type = 'Debit Online'
WHERE
	payment_type IN (
		'Paypal_Express_Checkout',
		'Banorte_Payworks_Debit'
	);


UPDATE churn_model_2 c
SET payment_type = 'Credit Online'
WHERE
	payment_type IN (
		'Amex_Gateway',
		'Banorte_Payworks'
	);

UPDATE churn_model_2 c
SET payment_type = 'Deposit'
WHERE
	payment_type IN (
		'Oxxo_Direct',
		'Bancomer_PagoReferenciado'
	);

UPDATE churn_model_2 c
SET payment_type = 'Credit on Delivery'
WHERE
	payment_type IN (
		'CreditCardOnDelivery_Payment'
	);
*/
/*
UPDATE churn_model_2 c
INNER JOIN development_mx.A_Master a ON c.itemid_highest_product = a.itemid
SET linio_or_mkt_place = a.isMPlace;
*/

UPDATE churn_model_2 c
INNER JOIN bob_live_mx.customer_address a ON c.customerid = a.fk_customer
SET c.city = a.city,
 c.region = a.region,
 c.postcode = a.postcode;

/*
UPDATE churn_model_2 c
INNER JOIN bob_live_mx.customer a ON c.customerid = a.id_customer
SET c.gender = a.gender,
 c.customer_age = datediff(curdate(), a.birthday) / 365;
*/


UPDATE churn_model c
INNER JOIN production.out_order_tracking o ON c.itemid_highest_product = o.order_item_id
SET 
  c.time_purchase_vs_promised_delivery = datediff(o.date_delivered_promised,o.date_ordered
);
UPDATE churn_model c
INNER JOIN production.out_order_tracking o ON c.itemid_highest_product = o.order_item_id
SET 
 c.time_purchase_vs_actual_delivery = o.days_total_delivery,
 c.leadtime_highest_item = o.days_total_delivery
;

UPDATE churn_model_2 c
INNER JOIN production.out_order_tracking o ON c.itemid_highest_product = o.order_item_id
SET 
 c.time_purchase_vs_actual_delivery = o.days_total_delivery,
 c.leadtime_highest_item = o.days_total_delivery
;

UPDATE churn_model_2 c
INNER JOIN production.out_order_tracking o ON c.itemid_highest_product = o.order_item_id
SET 
  c.time_purchase_vs_promised_delivery = datediff(o.date_delivered_promised,o.date_ordered
);

UPDATE churn_model c
INNER JOIN production.out_order_tracking o ON c.itemid_highest_product = o.order_item_id
SET 
  c.time_promised_delivery_vs_actual_delivery = datediff(o.date_delivered,o.date_delivered_promised)
WHERE
	o.date_delivered IS NOT NULL
;

UPDATE churn_model_2 c
INNER JOIN production.out_order_tracking o ON c.itemid_highest_product = o.order_item_id
SET 
  c.time_promised_delivery_vs_actual_delivery = datediff(o.date_delivered,o.date_delivered_promised)
WHERE
	o.date_delivered IS NOT NULL
;

UPDATE churn_model_2 c
INNER JOIN (SELECT
						 order_number,
						 max(days_total_delivery) AS max_leadtime_per_order,
						 sum(stockout) as stockout
						FROM
						 production.out_order_tracking
						GROUP BY order_number) o 
ON c.purchase_number = o.order_number
SET 
  c.stockouts = o.stockout,
  c.max_leadtime_per_order = o.max_leadtime_per_order;
/*
UPDATE churn_model_2 c
INNER JOIN production.out_order_tracking o ON c.itemid_highest_product = o.order_item_id
SET c.carrier = o.shipping_carrier;
*/

UPDATE churn_model_2 c
INNER JOIN production.out_order_tracking o ON c.itemid_highest_product = o.order_item_id
SET c.number_delivery_attempts = CASE
WHEN datediff(
	o.date_delivered,
	o.date_1st_attempt
) = 0 THEN
	1
WHEN datediff(
	o.date_delivered,
	o.date_1st_attempt
) != 0 THEN
	2
WHEN o.date_1st_attempt IS NULL THEN
	0
END;
/*
UPDATE churn_model_2 c
SET directdeposit_bank = 1
WHERE
	payment_type IN (
		'Oxxo_Direct',
		'Banorte_PagoReferenciado'
	);

UPDATE churn_model_2 c
SET directdeposit_bank = 0
WHERE
	directdeposit_bank IS NULL;
*/
/*
UPDATE churn_model_2 c
SET creditcard = 1
WHERE
	payment_type IN (
		'CreditCardOnDelivery_Payment',
		'Amex_Gateway',
		'Banorte_Payworks'
	);

UPDATE churn_model_2 c
SET creditcard = 0
WHERE
	creditcard IS NULL;
*/

UPDATE churn_model_2 c
SET c.return_before_first_purchase = (
	SELECT
		CASE
	WHEN sum(returned) > 0 THEN
		1
	ELSE
		0
	END
	FROM
		production.tbl_order_detail t
	WHERE
		t.custid = c.customerid
	AND t.date < c.date_purchased
	AND obc = 1
	GROUP BY
		t.custid
);

UPDATE churn_model_2 c
SET c.return_after_first_purchase = (
	SELECT
		CASE
	WHEN sum(returned) > 0 THEN
		1
	ELSE
		0
	END
	FROM
		production.tbl_order_detail t
	WHERE
		t.custid = c.customerid
	AND t.date > c.date_purchased
	AND obc = 1
	GROUP BY
		t.custid
);

UPDATE churn_model_2 c
SET c.number_returns_before_first_purchase = (
	SELECT
		count(DISTINCT order_nr)
	FROM
		production.tbl_order_detail t
	WHERE
		t.custid = c.customerid
	AND t.date < c.date_purchased
	AND obc = 1
	AND returned = 1
	GROUP BY
		t.custid
);

UPDATE churn_model_2 c
SET c.number_returns_after_first_purchase = (
	SELECT
		count(DISTINCT order_nr)
	FROM
		production.tbl_order_detail t
	WHERE
		t.custid = c.customerid
	AND t.date BETWEEN c.date_purchased
	AND (
		SELECT
			date
		FROM
			production.tbl_order_detail y
		WHERE
			oac = 1
		AND new_customers IS NULL
		AND t.custid = y.custid
		GROUP BY
			custid
	)
	AND obc = 1
	AND returned = 1
	GROUP BY
		t.custid
);

UPDATE churn_model_2 c
SET c.return_before_first_purchase = 0
WHERE
	c.return_before_first_purchase IS NULL;

UPDATE churn_model_2 c
SET c.cancellation_before_first_purchase = (
	SELECT
		CASE
	WHEN sum(cancel) > 0 THEN
		1
	ELSE
		0
	END
	FROM
		production.tbl_order_detail t
	WHERE
		t.custid = c.customerid
	AND t.date < c.date_purchased
	AND obc = 1
	GROUP BY
		t.custid
);

UPDATE churn_model_2 c
SET c.cancellation_after_first_purchase = (
	SELECT
		CASE
	WHEN sum(cancel) > 0 THEN
		1
	ELSE
		0
	END
	FROM
		production.tbl_order_detail t
	WHERE
		t.custid = c.customerid
	AND t.date > c.date_purchased
	AND obc = 1
	GROUP BY
		t.custid
);

UPDATE churn_model_2 c
SET c.number_cancellations_before_first_purchase = (
	SELECT
		count(DISTINCT order_nr)
	FROM
		production.tbl_order_detail t
	WHERE
		t.custid = c.customerid
	AND t.date < c.date_purchased
	AND obc = 1
	AND cancel = 1
	GROUP BY
		t.custid
);

UPDATE churn_model_2 c
SET c.number_cancellations_after_first_purchase = (
	SELECT
		count(DISTINCT order_nr)
	FROM
		production.tbl_order_detail t
	WHERE
		t.custid = c.customerid
	AND t.date BETWEEN c.date_purchased
	AND (
		SELECT
			date
		FROM
			production.tbl_order_detail y
		WHERE
			oac = 1
		AND new_customers IS NULL
		AND t.custid = y.custid
		GROUP BY
			custid
	)
	AND obc = 1
	AND cancel = 1
	GROUP BY
		t.custid
);

UPDATE churn_model_2 c
SET c.cancellation_before_first_purchase = 0
WHERE
	c.cancellation_before_first_purchase IS NULL;

UPDATE churn_model_2 c
INNER JOIN production.tbl_order_detail t ON c.itemid_highest_product = t.item
SET 
 c.orderid = t.orderid,
 c.purchase_number = t.order_nr
;
/*
UPDATE churn_model_2 c
SET c.total_number_purchases = (
	SELECT
		count(DISTINCT order_nr)
	FROM
		production.tbl_order_detail t
	WHERE
		c.customerid = t.custid
	AND t.oac = 1
);
*/
/*
UPDATE churn_model_2 c
INNER JOIN production.tbl_order_detail t ON c.itemid_highest_product = t.item
SET c.voucher_percentage_of_price = (
	SELECT
		sum(t.coupon_money_value) / c.order_price
	FROM
		production.tbl_order_detail t
	WHERE
		c.purchase_number = t.order_nr
);
*/
/*
UPDATE churn_model_2 c
INNER JOIN production.tbl_order_detail t ON c.itemid_highest_product = t.item
INNER JOIN bob_live_mx.sales_rule r ON r. CODE = t.coupon_code
INNER JOIN bob_live_mx.sales_rule_set s ON s.id_sales_rule_set = r.fk_sales_rule_set
SET c.type_of_voucher = s.discount_type,
 c.voucher_percentage = s.discount_percentage;
*/

DROP TABLE IF EXISTS second_purchase.churners;

CREATE TABLE second_purchase.churners (INDEX (custid))
SELECT
 t.custid AS custid,
 t.date AS date,
 x.date_second AS date_second,
 datediff(x.date_second,t.date) as date_diff,
if (datediff(x.date_second,t.date) > 120,1,0) AS churner
FROM production.tbl_order_detail t
LEFT JOIN
(SELECT
	custid,
	min(date) AS date_second
FROM
	production.tbl_order_detail
WHERE
	oac = 1
AND new_customers IS NULL
GROUP BY
	custid) x
ON t.custid = x.custid
WHERE t.oac = 1
AND t.new_customers IS NOT NULL
GROUP BY t.custid;

UPDATE
 second_purchase.churn_model_2 a
INNER JOIN second_purchase.churners x
ON a.customerid = x.custid
SET a.churner = x.churner;

UPDATE churn_model_2 c
SET churner = 0
WHERE
	churner IS NULL;

/*
UPDATE churn_model_2 c
INNER JOIN bob_live_mx.sales_order s ON c.purchase_number = s.order_nr
SET c.hour_purchased = HOUR (s.created_at);
*/
/*
UPDATE churn_model_2 c
INNER JOIN bob_live_mx.customer a ON c.customerid = a.id_customer
INNER JOIN customer_service.tbl_zendesk z ON a.email = z.requester_email
AND c.purchase_number = z.Numero_del_pedido
SET c.method_of_contact = z.Canal_de_contacto,
 c.reason_of_contact = z.clasificacion,
 c.length_of_call = z.full_resolution_time_in_minutes,
 c.length_of_waiting = requester_wait_time_in_minutes
WHERE
	c.method_of_contact = 'llamada';
*/

UPDATE churn_model_2 c
INNER JOIN bob_live_mx.customer a ON a.id_customer = c.customerid
SET c.email = a.email;

/*
UPDATE churn_model_2 c
INNER JOIN (
	SELECT
		Date(`Date Submitted`) DateSubmitted,
		email,
		"Net promoter score" AS KPI,
		"Promoters (9 and 10) - detractors (0 through 6); as per net promoter score survey done to new customers" AS Description,
		round(
			(
				sum(IF(NPS IN(11, 10), 1, 0)) - sum(

					IF (NPS IN(1, 2, 3, 4, 5, 6, 7), 1, 0)
				)
			) / count(NPS) * 100
		) AS
	VALUE

	FROM
		customer_service.questionProMX
	GROUP BY
		DateSubmitted,
		email
) a ON a.email = c.email
SET c.NPS_score = a.
VALUE
;
*/

/*
UPDATE churn_model_2 c
INNER JOIN test.abc a ON a.email = c.email
SET c.NPS_score = a.
VALUE
WHERE
	datediff(
		DateSubmitted,
		date_purchased
	) < 30;
*/
/*
UPDATE churn_model_2 c
INNER JOIN customer_service.questionProMX p ON c.order_purchased = p.ordernumber
SET c.NPS_score = p.NPS;
*/
/*
UPDATE churn_model_2 c
INNER JOIN customer_service.questionProMX p ON c.email = p.email
SET c.NPS_score = p.NPS
WHERE
	datediff(
		`Date Submitted`,
		date_purchased
	) < 30;
*/

UPDATE churn_model_2 c
INNER JOIN 
( SELECT
		order_number,
		sum(stockout) as stockouts,
		sum(has_been_backorder) as backorders
	FROM	production.out_order_tracking
	GROUP BY order_number
) a
ON c.purchase_number = a.order_number
SET 
  c.stockouts = a.stockouts,
	c.backorders = a.backorders;

UPDATE churn_model_2 c
INNER JOIN 
( SELECT
		order_number,
		sum(failed_delivery) as deliveryfails
	FROM	production.out_inverse_logistics_tracking
	GROUP BY order_number
) a
ON c.purchase_number = a.order_number
SET 
  c.delivery_fail = a.deliveryfails;

UPDATE second_purchase.churn_model_2 a
INNER JOIN 
(	SELECT
		date(Created_at),
		Numero_del_pedido,
		count(Numero_del_pedido) AS ContacRatio,
		sum(Replies) AS Replies
	FROM
		customer_service.tbl_zendesk
	WHERE	Numero_del_pedido IS NOT NULL
	AND 	Numero_del_pedido <> '-'
	GROUP BY
		date(Created_at),
		Numero_del_pedido) b 
ON a.purchase_number = b.Numero_del_pedido
SET 
	a.tickets_per_order = b.ContacRatio,
	a.replies_per_order = b.Replies;

UPDATE second_purchase.churn_model_2 a
INNER JOIN 
(	SELECT
		date(Created_at),
		Numero_del_pedido,
		if(Solved_at<>0,1,0)is_Solved
	FROM
		customer_service.tbl_zendesk
	WHERE	Numero_del_pedido IS NOT NULL
	AND 	Numero_del_pedido <> '-'
	GROUP BY
		date(Created_at),
		Numero_del_pedido) b 
ON a.purchase_number = b.Numero_del_pedido
SET 
	a.is_Solved = b.is_Solved;


UPDATE second_purchase.churn_model_2 a
INNER JOIN 
(	SELECT
		date(`Date Submitted`) AS date,
		ordernumber,
		max(NPS) - 1 AS NPS
	FROM
		customer_service.questionProMX
	GROUP BY
		date(`Date Submitted`),
		ordernumber
) b 
ON a.purchase_number = b.ordernumber
SET 
 a.NPS_score = b.NPS;

UPDATE second_purchase.churn_model_2 c
INNER JOIN 
(SELECT 
 a.email,
 datediff(a.`Date`,b.date_purchased),
 a.NPS AS NPS
FROM
(SELECT
  date(`Date Submitted`) AS date,
  email,
  max(NPS) AS NPS
 FROM
  customer_service.NPSMX
GROUP BY
  date(`Date Submitted`),
  email)a
INNER JOIN
second_purchase.churn_model_2 b
on a.email = b.email
WHERE 
datediff(a.`Date`,b.date_purchased)<=30) d
ON c.email = d.email
SET c.NPS_score = d.NPS;

UPDATE
 second_purchase.churn_model a
INNER JOIN
 second_purchase.churn_model_2 b
ON a.orderid = b.orderid
SET
 a.stockouts = b.stockouts,
 a.backorders = b.backorders,
 a.delivery_fail = b.delivery_fail,
 a.tickets_per_order = b.tickets_per_order,
 a.NPS_score = b.NPS_score,
 a.max_leadtime_per_order = b.max_leadtime_per_order,
 a.leadtime_highest_item = b.leadtime_highest_item;

UPDATE
 second_purchase.churn_model
SET
	delivery_status = CASE	WHEN time_promised_delivery_vs_actual_delivery < 0
													THEN 'early'
													WHEN time_promised_delivery_vs_actual_delivery = 0
													THEN 'On Time'
													WHEN time_promised_delivery_vs_actual_delivery > 0
													THEN 'Late'
													ELSE NULL
										END;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 PROCEDURE `churn_model_3`()
BEGIN

TRUNCATE churn_model;

INSERT INTO churn_model (
	customerid,
	order_price,
	order_tax,
	order_quantity,
	order_min_item_price,
	order_max_item_price,
	order_avg_item_price,
	pc1,
	`%pc1`,
	shipping_cost,
	voucher,
	voucher_amount,
	day_purchased,
	month_purchased,
	year_purchased,
	#season_purchased,
	date_purchased,
	orderid
	) 
SELECT
	custid,
	sum(unit_price),
	sum(unit_price) - sum(unit_price_after_vat),
	count(*),
	min(unit_price),
	max(unit_price),
	avg(unit_price),
	sum(	ifnull(unit_price_after_vat, 0) 
			- ifnull(coupon_money_after_vat, 0) 
			+ ifnull(shipping_fee_after_vat, 0) 
			- ifnull(costo_after_vat, 0) 
			- ifnull(delivery_cost_supplier, 0)),
	sum(	ifnull(unit_price_after_vat, 0) 
			- ifnull(coupon_money_after_vat, 0) 
			+ ifnull(shipping_fee_after_vat, 0) 
			- ifnull(costo_after_vat, 0) 
			- ifnull(delivery_cost_supplier, 0)) / 
	sum(	ifnull(unit_price_after_vat, 0) 
			- ifnull(coupon_money_after_vat, 0) 
			+ ifnull(shipping_fee_after_vat, 0)),
	sum(shipping_cost_per_sku),
	CASE	WHEN coupon_code IS NULL 
				THEN 0
				ELSE 1
	END,
	coupon_money_value,
	DAY (date),
	MONTH (date),
	YEAR (date),
	#CASE	WHEN MONTH (date) BETWEEN 1 AND 3 
	#			THEN 'Spring'
	#			WHEN MONTH (date) BETWEEN 4 AND 6 
	#			THEN 'Summer'
	#			WHEN MONTH (date) BETWEEN 7 AND 9 
	#			THEN 'Fall'
	#			WHEN MONTH (date) BETWEEN 10 AND 12 
	#			THEN 'Winter'
	#END,
 date,
 min(orderid)
FROM production.tbl_order_detail
WHERE	oac = 1
AND 	new_customers IS NOT NULL
GROUP BY
	custid;
/*
UPDATE churn_model
SET season_purchased = 'Spring'
	WHERE	(date_purchased BETWEEN '2012-03-20' AND '2012-06-20')
	OR 		(date_purchased BETWEEN '2013-03-20' AND '2013-06-20');

UPDATE churn_model
SET season_purchased = 'Summer'
	WHERE (date_purchased BETWEEN '2012-06-21' AND '2012-09-21')
	OR 		(date_purchased BETWEEN '2013-06-21' AND '2013-09-21');

UPDATE churn_model
SET season_purchased = 'Fall'
	WHERE	(date_purchased BETWEEN '2012-09-22' AND '2012-12-20')
	OR 		(date_purchased BETWEEN '2013-09-22' AND '2013-12-20');

UPDATE churn_model
SET season_purchased = 'Winter'
	WHERE	(date_purchased BETWEEN '2012-12-20' AND '2013-03-19')
	OR 		(date_purchased BETWEEN '2013-12-20' AND '2014-03-19');
*/
UPDATE churn_model c
INNER JOIN bob_live_mx.customer_address a ON c.customerid = a.fk_customer
INNER JOIN bob_live_mx.customer_address_city y ON y.id_customer_address_city = a.fk_customer_address_city
SET c.city = y. NAME;

/*
UPDATE churn_model c
INNER JOIN bob_live_mx.customer_address a ON c.customerid = a.fk_customer
INNER JOIN bob_live_mx.customer_address_region y ON y.id_customer_address_region = a.fk_customer_address_region
SET c.region = y. NAME;
*/

UPDATE churn_model c
SET itemid_highest_product = (SELECT
																item
															FROM
																production.tbl_order_detail x
															WHERE
																unit_price IN (	SELECT
																									max(unit_price)
																								FROM
																									production.tbl_order_detail y
																								WHERE
																									x.order_nr = y.order_nr
																								AND oac = 1
																								AND new_customers IS NOT NULL
																								GROUP BY
																									order_nr
																							)
															AND oac = 1
															AND new_customers IS NOT NULL
															AND c.customerid = x.custid
															GROUP BY
																custid
);


UPDATE churn_model c
INNER JOIN production.tbl_order_detail t ON c.itemid_highest_product = t.item
SET 
 c.max_item_product_category = t.new_cat1,
 c.brand_or_generic = t.brand,
 c.campaign_purchased = t.campaign,
 c.payment_type = t.payment_method,
 c.installments = t.installment,
 c.channel_group_purchased = t.channel_group,
 c.channel_purchased = t.channel;


UPDATE churn_model c
SET installments = 1
WHERE
	installments >= 1;


UPDATE churn_model c
SET installments = 0
WHERE
	installments IS NULL;


UPDATE churn_model c
SET voucher_percentage = voucher_percentage / 100;


UPDATE churn_model c
SET payment_type = 'Other Online'
WHERE
	payment_type IN (
		'Adyen_HostedPaymentPage',
		'DineroMail_HostedPaymentPage',
		'DineroMail_Api',
		'Adquira_Interredes'
	);

UPDATE churn_model c
SET payment_type = 'Cash on Delivery'
WHERE
	payment_type IN ('CashOnDelivery_Payment');

UPDATE churn_model c
SET payment_type = 'Debit Online'
WHERE
	payment_type IN (
		'Paypal_Express_Checkout',
		'Banorte_Payworks_Debit'
	);


UPDATE churn_model c
SET payment_type = 'Credit Online'
WHERE
	payment_type IN (
		'Amex_Gateway',
		'Banorte_Payworks'
	);

UPDATE churn_model c
SET payment_type = 'Deposit'
WHERE
	payment_type IN (
		'Oxxo_Direct',
		'Bancomer_PagoReferenciado'
	);

UPDATE churn_model c
SET payment_type = 'Credit on Delivery'
WHERE
	payment_type IN (
		'CreditCardOnDelivery_Payment'
	);


UPDATE churn_model c
INNER JOIN development_mx.A_Master a ON c.itemid_highest_product = a.itemid
SET linio_or_mkt_place = a.isMPlace;


UPDATE churn_model c
INNER JOIN bob_live_mx.customer_address a ON c.customerid = a.fk_customer
SET c.city = a.city;

UPDATE churn_model c
INNER JOIN production.out_order_tracking a ON c.itemid_highest_product = a.order_item_id
SET 
 #c.region = a.ship_to_state,
 c.postcode = a.ship_to_zip,
 c.package_measure_new = a.package_measure_new,
 c.linio_or_mkt_place = a.is_market_place;

UPDATE
  second_purchase.churn_model
INNER JOIN bob_live_mx.sales_order 
	ON churn_model.purchase_number = sales_order.order_nr
INNER JOIN bob_live_mx.sales_order_address
	ON sales_order.fk_sales_order_address_shipping = sales_order_address.id_sales_order_address
INNER JOIN bob_live_mx.customer_address_region
	 ON sales_order_address.fk_customer_address_region = customer_address_region.id_customer_address_region
SET 
 churn_model.region = customer_address_region.code;



UPDATE churn_model c
INNER JOIN bob_live_mx.customer a ON c.customerid = a.id_customer
SET c.gender = a.gender,
 c.customer_age = datediff(curdate(), a.birthday) / 365;



UPDATE churn_model c
INNER JOIN production.out_order_tracking o ON c.itemid_highest_product = o.order_item_id
SET 
  c.time_purchase_vs_promised_delivery = datediff(o.date_delivered_promised,o.date_ordered),
	c.time_purchase_vs_actual_delivery = o.days_total_delivery,
  c.time_purchase_vs_promised_delivery = datediff(o.date_delivered_promised,o.date_ordered),
	c.purchase_number = o.order_number
;

UPDATE churn_model c
INNER JOIN production.out_order_tracking o ON c.itemid_highest_product = o.order_item_id
SET 
  c.time_promised_delivery_vs_actual_delivery = datediff(o.date_delivered,o.date_delivered_promised)
WHERE
	o.date_delivered IS NOT NULL
;

UPDATE churn_model c
INNER JOIN (SELECT
						 order_number,
						 max(days_total_delivery) AS max_leadtime_per_order,
						 sum(stockout) as stockout
						FROM
						 production.out_order_tracking
						GROUP BY order_number) o 
ON c.purchase_number = o.order_number
SET 
  c.stockouts = o.stockout,
  c.max_leadtime_per_order = o.max_leadtime_per_order;

UPDATE churn_model c
INNER JOIN production.out_order_tracking o ON c.itemid_highest_product = o.order_item_id
SET c.carrier = o.shipping_carrier;


UPDATE churn_model c
INNER JOIN production.out_order_tracking o ON c.itemid_highest_product = o.order_item_id
SET c.number_delivery_attempts = CASE
																	WHEN datediff(o.date_delivered,o.date_1st_attempt) = 0 
																	THEN 1
																	WHEN datediff(o.date_delivered,o.date_1st_attempt) != 0 
																	THEN 2
																	WHEN o.date_1st_attempt IS NULL 
																	THEN 0
																 END;

UPDATE churn_model c
SET directdeposit_bank = 1
WHERE
	payment_type IN (
		'Oxxo_Direct',
		'Banorte_PagoReferenciado'
	);

UPDATE churn_model c
SET directdeposit_bank = 0
WHERE
	directdeposit_bank IS NULL;


UPDATE churn_model c
SET creditcard = 1
WHERE
	payment_type IN (
		'CreditCardOnDelivery_Payment',
		'Amex_Gateway',
		'Banorte_Payworks'
	);

UPDATE churn_model c
SET creditcard = 0
WHERE
	creditcard IS NULL;


UPDATE churn_model c
SET c.return_before_first_purchase = (
	SELECT
		CASE
	WHEN sum(returned) > 0 THEN
		1
	ELSE
		0
	END
	FROM
		production.tbl_order_detail t
	WHERE
		t.custid = c.customerid
	AND t.date < c.date_purchased
	AND obc = 1
	GROUP BY
		t.custid
);

UPDATE churn_model c
SET c.return_after_first_purchase = (
	SELECT
		CASE
	WHEN sum(returned) > 0 THEN
		1
	ELSE
		0
	END
	FROM
		production.tbl_order_detail t
	WHERE
		t.custid = c.customerid
	AND t.date > c.date_purchased
	AND obc = 1
	GROUP BY
		t.custid
);

UPDATE churn_model c
SET c.number_returns_before_first_purchase = (
	SELECT
		count(DISTINCT order_nr)
	FROM
		production.tbl_order_detail t
	WHERE
		t.custid = c.customerid
	AND t.date < c.date_purchased
	AND obc = 1
	AND returned = 1
	GROUP BY
		t.custid
);

UPDATE churn_model c
SET c.number_returns_after_first_purchase = (
	SELECT
		count(DISTINCT order_nr)
	FROM
		production.tbl_order_detail t
	WHERE
		t.custid = c.customerid
	AND t.date BETWEEN c.date_purchased
	AND (
		SELECT
			date
		FROM
			production.tbl_order_detail y
		WHERE
			oac = 1
		AND new_customers IS NULL
		AND t.custid = y.custid
		GROUP BY
			custid
	)
	AND obc = 1
	AND returned = 1
	GROUP BY
		t.custid
);

UPDATE churn_model c
SET c.return_before_first_purchase = 0
WHERE
	c.return_before_first_purchase IS NULL;

UPDATE churn_model c
SET c.cancellation_before_first_purchase = (
	SELECT
		CASE
	WHEN sum(cancel) > 0 THEN
		1
	ELSE
		0
	END
	FROM
		production.tbl_order_detail t
	WHERE
		t.custid = c.customerid
	AND t.date < c.date_purchased
	AND obc = 1
	GROUP BY
		t.custid
);

UPDATE churn_model c
SET c.cancellation_after_first_purchase = (
	SELECT
		CASE
	WHEN sum(cancel) > 0 THEN
		1
	ELSE
		0
	END
	FROM
		production.tbl_order_detail t
	WHERE
		t.custid = c.customerid
	AND t.date > c.date_purchased
	AND obc = 1
	GROUP BY
		t.custid
);

UPDATE churn_model c
SET c.number_cancellations_before_first_purchase = (
	SELECT
		count(DISTINCT order_nr)
	FROM
		production.tbl_order_detail t
	WHERE
		t.custid = c.customerid
	AND t.date < c.date_purchased
	AND obc = 1
	AND cancel = 1
	GROUP BY
		t.custid
);

UPDATE churn_model c
SET c.number_cancellations_after_first_purchase = (
	SELECT
		count(DISTINCT order_nr)
	FROM
		production.tbl_order_detail t
	WHERE
		t.custid = c.customerid
	AND t.date BETWEEN c.date_purchased
	AND (
		SELECT
			date
		FROM
			production.tbl_order_detail y
		WHERE
			oac = 1
		AND new_customers IS NULL
		AND t.custid = y.custid
		GROUP BY
			custid
	)
	AND obc = 1
	AND cancel = 1
	GROUP BY
		t.custid
);

UPDATE churn_model c
SET c.cancellation_before_first_purchase = 0
WHERE
	c.cancellation_before_first_purchase IS NULL;


UPDATE churn_model c
SET c.total_number_purchases = (
	SELECT
		count(DISTINCT order_nr)
	FROM
		production.tbl_order_detail t
	WHERE
		c.customerid = t.custid
	AND t.oac = 1
);


UPDATE churn_model c
INNER JOIN production.tbl_order_detail t ON c.itemid_highest_product = t.item
SET c.voucher_percentage_of_price = (
	SELECT
		sum(t.coupon_money_value) / c.order_price
	FROM
		production.tbl_order_detail t
	WHERE
		c.purchase_number = t.order_nr
);


UPDATE churn_model c
INNER JOIN production.tbl_order_detail t ON c.itemid_highest_product = t.item
INNER JOIN bob_live_mx.sales_rule r ON r. CODE = t.coupon_code
INNER JOIN bob_live_mx.sales_rule_set s ON s.id_sales_rule_set = r.fk_sales_rule_set
SET c.type_of_voucher = s.discount_type,
 c.voucher_percentage = s.discount_percentage;


DROP TABLE IF EXISTS production.tbl_order;
CREATE TABLE production.tbl_order ( 
    custid int,
    orderid varchar(20),
    date DATE,
    items_order int,
    PRIMARY KEY ( custid ,orderid ),
    KEY ( custid ),
    KEY ( orderid )
)
SELECT 
    custid,
    orderid,
    date,
    count(*) as items_order
FROM production.tbl_order_detail 
where oac = 1
GROUP BY orderid;


# First Purchases
DROP TABLE IF EXISTS second_purchase.FirstPurchases;
CREATE TABLE second_purchase.FirstPurchases( id int AUTO_INCREMENT , PRIMARY KEY( custid , id ))
SELECT
 c.custid    AS custid,
 null id,
 c.date     AS date
FROM production.tbl_order c
ORDER BY  custid,date ASC ;

DELETE FROM second_purchase.FirstPurchases WHERE id > 1;

# Second Purchases
DROP TABLE IF EXISTS second_purchase.SecondPurchases1;
CREATE TABLE second_purchase.SecondPurchases1( id int AUTO_INCREMENT , PRIMARY KEY( custid , id ))
SELECT
 c.custid    AS custid,
 null id,
 c.date     AS date
FROM production.tbl_order c
ORDER BY  custid,date ASC ;

DELETE FROM second_purchase.SecondPurchases1 WHERE id = 1;

DELETE
 a.*
FROM second_purchase.SecondPurchases1 a
INNER JOIN second_purchase.FirstPurchases b
ON a.custid = b.custid
WHERE a.date = b.date;

DROP TABLE IF EXISTS second_purchase.SecondPurchases;
CREATE TABLE second_purchase.SecondPurchases( id int AUTO_INCREMENT , PRIMARY KEY( custid , id ))
SELECT
 c.custid    AS custid,
 null id,
 c.date     AS date
FROM second_purchase.SecondPurchases1 c
ORDER BY  custid,date ASC ;

DELETE FROM second_purchase.SecondPurchases WHERE id > 1;

# Churners
DROP TABLE IF EXISTS second_purchase.Churners;
CREATE TABLE second_purchase.Churners( 
   custid int, 
   date_first_purchase date, 
   date_second_purchase date, 
   days_between_first_second int,
   churners int,
   PRIMARY KEY ( custid)
)
SELECT
 FirstPurchases.custid   AS custid,
 FirstPurchases.date     AS date_first_purchase,
 SecondPurchases.date    AS date_second_purchase,
 NULL AS days_between_first_second,
 0 AS churners 
FROM          second_purchase.FirstPurchases
    LEFT JOIN 
		second_purchase.SecondPurchases
	      USING ( custid )
;

UPDATE second_purchase.Churners
SET 
   days_between_first_second = DATEDIFF(date_second_purchase,date_first_purchase  ),
   churners = if( DATEDIFF(date_second_purchase,date_first_purchase) > 120, 1, 0 )
WHERE
   date_second_purchase is not NULL;

UPDATE second_purchase.Churners
SET
   churners = 1
WHERE
   date_second_purchase is null;

UPDATE second_purchase.Churners
SET
   churners = NULL
WHERE	DATEDIFF(curdate(),date_first_purchase) <= 120
AND 	churners = 1;

UPDATE second_purchase.churn_model
INNER JOIN second_purchase.Churners
	ON churn_model.customerid = Churners.custid
SET churn_model.churner = Churners.churners;


UPDATE churn_model c
INNER JOIN bob_live_mx.sales_order s ON c.purchase_number = s.order_nr
SET c.hour_purchased = HOUR (s.created_at);


UPDATE churn_model c
INNER JOIN bob_live_mx.customer a ON c.customerid = a.id_customer
INNER JOIN customer_service.tbl_zendesk z ON a.email = z.requester_email
AND c.purchase_number = z.Numero_del_pedido
SET c.method_of_contact = z.Canal_de_contacto,
 c.reason_of_contact = z.clasificacion,
 c.length_of_call = z.full_resolution_time_in_minutes,
 c.length_of_waiting = requester_wait_time_in_minutes
WHERE
	c.method_of_contact = 'llamada';


UPDATE churn_model c
INNER JOIN bob_live_mx.customer a ON a.id_customer = c.customerid
SET c.email = a.email;

/*
UPDATE churn_model c
INNER JOIN test.abc a ON a.email = c.email
SET c.NPS_score = a.
VALUE
WHERE
	datediff(
		DateSubmitted,
		date_purchased
	) <= 30;




UPDATE churn_model c
INNER JOIN 
( SELECT
		order_number,
		sum(failed_delivery) as deliveryfails
	FROM	production.out_inverse_logistics_tracking
	GROUP BY order_number
) a
ON c.purchase_number = a.order_number
SET 
  c.delivery_fail = a.deliveryfails;
*/
UPDATE second_purchase.churn_model a
INNER JOIN 
(	SELECT
		date(Created_at),
		Numero_del_pedido,
		count(Numero_del_pedido) AS ContacRatio,
		sum(Replies) AS Replies
	FROM
		customer_service.tbl_zendesk
	WHERE	Numero_del_pedido IS NOT NULL
	AND 	Numero_del_pedido <> '-'
	GROUP BY
		date(Created_at),
		Numero_del_pedido) b 
ON a.purchase_number = b.Numero_del_pedido
SET 
	a.tickets_per_order = b.ContacRatio,
	a.replies_per_order = b.Replies;

UPDATE second_purchase.churn_model a
INNER JOIN 
(	SELECT
		date(Created_at),
		Numero_del_pedido,
		if(Solved_at<>0,1,0)is_Solved
	FROM
		customer_service.tbl_zendesk
	WHERE	Numero_del_pedido IS NOT NULL
	AND 	Numero_del_pedido <> '-'
	GROUP BY
		date(Created_at),
		Numero_del_pedido) b 
ON a.purchase_number = b.Numero_del_pedido
SET 
	a.is_Solved = b.is_Solved;


UPDATE second_purchase.churn_model a
INNER JOIN 
(	SELECT
		date(`Date Submitted`) AS date,
		ordernumber,
		max(NPS) - 1 AS NPS
	FROM
		customer_service.questionProMX
	GROUP BY
		date(`Date Submitted`),
		ordernumber
) b 
ON a.purchase_number = b.ordernumber
SET 
 a.NPS_score = b.NPS;

UPDATE second_purchase.churn_model c
INNER JOIN 
(SELECT 
 a.email,
 datediff(a.`Date`,b.date_purchased),
 a.NPS AS NPS
FROM
(SELECT
  date(`Date Submitted`) AS date,
  email,
  max(NPS) AS NPS
 FROM
  customer_service.NPSMX
GROUP BY
  date(`Date Submitted`),
  email)a
INNER JOIN
second_purchase.churn_model b
on a.email = b.email
WHERE 
datediff(a.`Date`,b.date_purchased)<=30) d
ON c.email = d.email
SET c.NPS_score = d.NPS;

/*
UPDATE
 second_purchase.churn_model a
INNER JOIN
 second_purchase.churn_model_2 b
ON a.orderid = b.orderid
SET
 a.stockouts = b.stockouts,
 a.backorders = b.backorders,
 a.delivery_fail = b.delivery_fail,
 a.tickets_per_order = b.tickets_per_order,
 a.NPS_score = b.NPS_score,
 a.max_leadtime_per_order = b.max_leadtime_per_order,
 a.leadtime_highest_item = b.leadtime_highest_item;
*/

UPDATE
 second_purchase.churn_model
SET
 delivered = 1 
WHERE  time_promised_delivery_vs_actual_delivery IS NOT NULL;

UPDATE
 second_purchase.churn_model
SET
	delivery_status = CASE	WHEN time_promised_delivery_vs_actual_delivery < 0
													THEN 'early'
													WHEN time_promised_delivery_vs_actual_delivery <= 0
													THEN 'On Time'
													WHEN time_promised_delivery_vs_actual_delivery > 0
													THEN 'Late'
													ELSE NULL
										END;

UPDATE
 second_purchase.churn_model
SET	
	delivery_delay = CASE	WHEN time_promised_delivery_vs_actual_delivery <= 0
													THEN 0
													WHEN time_promised_delivery_vs_actual_delivery > 0
													THEN time_promised_delivery_vs_actual_delivery
													ELSE NULL
										END;

UPDATE
 second_purchase.churn_model
SET
 early = if(delivery_status = 'early',1,0),
 on_time = if(delivery_status = 'on time',1,0), 
 late = if(delivery_status = 'late',1,0);

UPDATE
 second_purchase.churn_model
SET
 purchased_before_120 = 1
WHERE DATEDIFF(curdate(),date_purchased) > 120;

UPDATE churn_model c
INNER JOIN Churners a
ON c.customerid = a.custid
SET c.days_until_2nd_purchase = a.days_between_first_second
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:04:23
