-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: customer_service_pe
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'customer_service_pe'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`marco.lazo`@`%`*/ /*!50003 PROCEDURE `bi_ops_cc_survey`()
BEGIN
SELECT  'Inicio rutina CC Survey',now();

#Horario Colombia

set time_zone='-5:00';

#Datos ultimo día
set @max_day=cast((select max(call_date) from bi_ops_cc_survey) as date);


delete from bi_ops_cc_survey
where call_date>=@max_day;

select date(@max_day);

#Ingresar datos a la tabla principal
insert into customer_service_pe.bi_ops_cc_survey
(uniqueid,
call_datetime,
call_date,
agent_name,
agent_ext,
call_time,
call_duration,
callerid,
respuestas,
abandon_in,
answer1_option_1,
answer1_option_2,
answer1_abandon,
answer2_option_1,
answer2_option_2,
answer2_option_3,
answer2_abandon,
answer3_option_1,
answer3_option_2,
answer3_option_3,
answer3_abandon,
answer_1,
answer_2,
answer_3)
Select B.uniqueid,
B.timestamp call_datetime,
date(B.timestamp)call_date,
C.name agent_name,
B.agent agent_ext,
time(B.timestamp) call_time,
B.duration call_duration,
B.phone callerid,
((case when B.id_quest_1=0 then 0 else 1 end)+(case when B.id_quest_2=0 then 0 else 1 end)+(case when B.id_quest_3=0 then 0 else 1 end)) respuestas,
((case when B.id_quest_1=0 then 1 else 0 end)+(case when B.id_quest_2=0 then 1 else 0 end)+(case when B.id_quest_3=0 then 1 else 0 end)) abandon_in,

(case when B.sel_opt_1=1 then 1 else 0 end) answer1_option_1,
(case when B.sel_opt_1=2 then 1 else 0 end) answer1_option_2,
(case when B.id_quest_1=0 then 1 else 0 end) answer1_abandon,

(case when B.sel_opt_2=1 then 1 else 0 end) answer2_option_1,
(case when B.sel_opt_2=2 then 1 else 0 end) answer2_option_2,
(case when B.sel_opt_2=3 then 1 else 0 end) answer2_option_3,
(case when B.id_quest_2=0 then 1 else 0 end) answer2_abandon,

(case when B.sel_opt_3=1 then 1 else 0 end) answer3_option_1,
(case when B.sel_opt_3=2 then 1 else 0 end) answer3_option_2,
(case when B.sel_opt_3=3 then 1 else 0 end) answer3_option_3,
(case when B.id_quest_3=0 then 1 else 0 end) answer3_abandon,

(case when B.id_quest_1=0 then 0 else 1 end) answer_1,
(case when B.id_quest_2=0 then 0 else 1 end) answer_2,
(case when B.id_quest_3=0 then 0 else 1 end) answer_3

from  customer_service_pe.reports B 
left join customer_service_pe.users C on B.agent=C.extension
where 
#qname='1002'
#and action in ('COMPLETEAGENT','COMPLETECALLER') and 
date(B.timestamp)>=@max_day
and B.status is not null;

#Agregar datos a tabla principal
update bi_ops_cc_pe a inner join bi_ops_cc_survey b
on a.queue_stats_id=b.uniqueid
set
a.transfered_survey=1,
a.answered_3=answer_3,
a.good=answer3_option_1,
a.regular=answer3_option_2,
a.answ_1_2=if(answer_1=1 and answer_2=1,1,0),
a.first_contact=answer2_option_1,
a.resolution=answer1_option_1
where a.transfered_survey =0;

SELECT  'Rutina finalizada Survey',now();

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`marco.lazo`@`%`*/ /*!50003 PROCEDURE `rout_test`()
BEGIN

drop table if  exists customer_service_pe.test;
create table customer_service_pe.test(
Select B.uniqueid,
FROM_UNIXTIME(A.timestamp,'%Y-%m-%d %H:%i:%s')call_datetime,
FROM_UNIXTIME(A.timestamp,'%Y-%m-%d')call_date,
C.name agent_name,
(case when A.agent<1000 then SUBSTR(A.agent,5,3) else A.agent end)agent_ext,
FROM_UNIXTIME(A.timestamp,'%H:%i:%s')call_time,
A.info2 call_duration,
B.phone callerid,
((case when B.id_quest_1=0 then 0 else 1 end)+(case when B.id_quest_2=0 then 0 else 1 end)+(case when B.id_quest_3=0 then 0 else 1 end)) respuestas,
((case when B.id_quest_1=0 then 1 else 0 end)+(case when B.id_quest_2=0 then 1 else 0 end)+(case when B.id_quest_3=0 then 1 else 0 end)) abandon_in,

(case when B.sel_opt_1=1 then 1 else 0 end) answer1_option_1,
(case when B.sel_opt_1=2 then 1 else 0 end) answer1_option_2,
(case when B.id_quest_1=0 then 1 else 0 end) answer1_abandon,

(case when B.sel_opt_2=1 then 1 else 0 end) answer2_option_1,
(case when B.sel_opt_2=2 then 1 else 0 end) answer2_option_2,
(case when B.sel_opt_2=3 then 1 else 0 end) answer2_option_3,
(case when B.id_quest_2=0 then 1 else 0 end) answer2_abandon,

(case when B.sel_opt_3=1 then 1 else 0 end) answer3_option_1,
(case when B.sel_opt_3=2 then 1 else 0 end) answer3_option_2,
(case when B.sel_opt_3=3 then 1 else 0 end) answer3_option_3,
(case when B.id_quest_3=0 then 1 else 0 end) answer3_abandon,

(case when B.id_quest_1=0 then 0 else 1 end) answer_1,
(case when B.id_quest_2=0 then 0 else 1 end) answer_2,
(case when B.id_quest_3=0 then 0 else 1 end) answer_3

from asteriskcdrdb.queuelog A
left join survey.reports B on A.callid=B.callid
left join asterisk.users C on SUBSTR(A.agent,5,3)=C.extension
where qname='1002'
and action in ('COMPLETEAGENT','COMPLETECALLER') and FROM_UNIXTIME(A.timestamp,'%Y-%m-%d')>='2014-05-15'
and B.status is not null

);

alter table customer_service_pe.test
ADD id int NOT NULL AUTO_INCREMENT primary key FIRST
 
 ;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `sp_time_in_office`()
BEGIN

#Agregar datos genrales tabla de tiempos
DROP  TABLE if EXISTS sampleQueue;
CREATE  TABLE sampleQueue
select a.*, date(FROM_UNIXTIME(timestamp,'%Y-%m-%d')) date, FROM_UNIXTIME(timestamp,'%Y-%m-%d %H:%i:%s') datetime
 from queuelog a  where date(FROM_UNIXTIME(timestamp,'%Y-%m-%d'))>='2014-05-01'
and action in ('ADDMEMBER','REMOVEMEMBER')
;

#Horas de ingreso
truncate table time_in_office;

insert into time_in_office (date, agent, fecha_entrada)
select date, agent,min(datetime) fecha_entrada from sampleQueue
where agent like '%sip%'
and action='ADDMEMBER'
group by agent,date;

#Horas de salida
update time_in_office a
inner join
(select date, agent,max(datetime) fecha from sampleQueue
where agent like '%sip%'
and action='REMOVEMEMBER'
group by agent,date) b
on a.date=b.date and a.agent=b.agent
set
a.fecha_salida=b.fecha;

#Actualizar tiempo
update time_in_office
set
tiempo_conexion=time_to_sec(TIMEDIFF(fecha_salida, fecha_entrada));

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:03:38
