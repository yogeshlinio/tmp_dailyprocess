-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: development_pe
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'development_pe'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 FUNCTION `FactorMonth`( `date` datetime , Month_Num INT ) RETURNS float(7,5)
BEGIN
	#Routine body goes here...
  DECLARE factor FLOAT(7,5);
 
  select  
         if( date_format( `date` , "%Y%m" ) > Month_Num , 1 , 
            if( date_format( `date` , "%Y%m" ) < Month_Num , 0 ,
            if( DAYOFMONTH( `date` ) = 1 , 1 , DAYOFMONTH( `date` ) / DAYOFMONTH( LAST_DAY( `date` ) ) )
         ))
 into factor;

	RETURN factor;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 FUNCTION `Val`(`value` varchar(50) ) RETURNS decimal(26,5)
BEGIN
	#Routine body goes here...
  DECLARE weight DECIMAL(15,5);
 
  select cast( REPLACE( value , " " , ""  )   AS DECIMAL(15,5) ) into weight;

  #select if(  abs( weight ) < 1  , 0 , weight ) into weight;   

	RETURN weight;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `KPIs`(IN MonthNum INT)
BEGIN

/*
* KPI's calculations
*/
/*
*  Kpi Group: Main
*  Created By: BI Team
*  Created at: 2013-11-05
*  Lastest Updated: 2013-11-05
*/

/*
*  KPI Table Creation
*/
DROP TABLE IF EXISTS KPIs;
CREATE TABLE IF NOT EXISTS KPIs
(
  MonthNum    INT NOT NULL,
  SpreadSheet INT NOT NULL,
  `Group`     VARCHAR(100) NOT NULL,
  RowId       INT NOT NULL,
  KPI         TEXT NOT NULL,
  Description TEXT NOT NULL,
  value       DECIMAL (15,5),
  Updated_at  DATETIME,
  PRIMARY KEY ( MonthNum , `Group`, RowId )
)
;

CALL KPI_ORDER_AND_CUSTOMER_DATA_INCL_MARKETPLACE(MonthNum);
CALL KPI_VOLUME_DRIVERS_INCL_MARKETPLACE(MonthNum);
#CALL KPI_BUYING_EFFICIENCY(MonthNum);
CALL KPI_OPERATIONS_EFFICIENCY(MonthNum);
CALL KPI_REVENUE_WATERFALL_INCL_MARKETPLACE(MonthNum);
CALL KPI_PL_ACCOUNTING_VIEW_NOT_CASH_VIEW(MonthNum);
CALL KPI_MARKETPLACE_REVENUE_AND_ORDER_WATERFALL(MonthNum);

CALL KPI_ORDER_REVENUE_DRIVERS(MonthNum);
CALL KPI_NET_PROMOTER_SCORE(MonthNum);

CALL KPI_MARKETING_MAIN();
call KPI_EXTRA(201404);
CALL KPI_CONCENTRATION();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `KPI_BUYING_EFFICIENCY`(IN KPI_MonthNum INT)
BEGIN
/*
*  Kpi Group:  BUYING EFFICIENCY
*  Created By: BI Team
*  Developer:  Carlos Antonio Mondragón Soria
*  Created at: 2013-10-03
*  Lastest Updated: 2013-10-03
*/
SET @KPI_Month=KPI_MonthNum;
SET @KPI_Year=SUBSTR( KPI_MonthNum FROM 1 FOR 4 );
SET @KPI_SpreadSheet = 1;
SET @KPI_UpdatedAt = Now();
SET @KPI_Group = "BUYING EFFICIENCY";

/*
* Sample Table Creations
*/
DROP TEMPORARY TABLE IF EXISTS out_order_tracking_Year;
CREATE TEMPORARY TABLE 	out_order_tracking_Year
SELECT * FROM production_pe.out_order_tracking 
WHERE 
    YEAR( date_shipped ) = @KPI_Year
AND status_wms <> 'cancelado'     
;

DROP TEMPORARY TABLE IF EXISTS A_Master_Year;
CREATE TEMPORARY TABLE A_Master_Year ( INDEX( ItemId ) )
SELECT 
   a.*, 
   b.fulfillment_type_bp ,
   date_shipped        
FROM          A_Master a
   INNER JOIN out_order_tracking_Year b
 ON a.ItemID = b.item_id 
WHERE
   YEAR( date ) = @KPI_Year;

DROP TEMPORARY TABLE IF EXISTS A_Master_Year_Totals;
CREATE TEMPORARY TABLE A_Master_Year_Totals ( INDEX( MonthNum ) )
SELECT MonthNum,
       SUM( IF( OrderAfterCan = 1, Rev , 0  )) AS Net_Rev
FROM          A_Master_Year a
WHERE
   YEAR( date ) = @KPI_Year
GROUP BY MonthNum
;	
   
DROP TEMPORARY TABLE IF EXISTS A_Master_Orders_Year;
CREATE TEMPORARY TABLE A_Master_Orders_Year 
SELECT * FROM Out_SalesReportOrder
WHERE
   YEAR( date ) = @KPI_Year;

DROP TEMPORARY TABLE IF EXISTS out_stock_hist_Year;
CREATE TEMPORARY TABLE out_stock_hist_Year
SELECT * FROM production_pe.out_stock_hist 
WHERE
      YEAR(date_entrance)= @KPI_Year
   OR date_exit IS NULL 
   OR YEAR(date_exit ) = @KPI_Year
;

DROP TEMPORARY TABLE IF EXISTS KPI_inventory_age;
CREATE TEMPORARY TABLE KPI_inventory_age
SELECT * FROM A_Stock
WHERE MonthNum = @KPI_Month
;

DROP TEMPORARY TABLE IF EXISTS out_procurement_tracking_Year;
CREATE TEMPORARY TABLE out_procurement_tracking_Year
SELECT * FROM operations_pe.out_procurement_tracking
WHERE
       YEAR(date_goods_received) = @KPI_Year
   AND is_deleted = 0
   AND is_cancelled = 0
;

/*
* KPIs calculations
*/
#Inventory type (share of sales) - Outright buying (in %)
REPLACE KPIs
SELECT 
   A_Master_Year.MonthNum    AS MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group  AS`Group`,
   108         AS RowID,
   "Inventory type (share of sales) - Outright buying (in %)"                                       AS KPI,
   "Gross revenues before cancelations (DWH definition: Net Revenue before cancelations excl. VAT)" AS Description,
   SUM( IF(     A_Master_Year.OrderAfterCan = 1 
            AND A_Master_Year.fulfillment_type_bp="outright buying",
                A_Master_Year.Rev , 0 ) ) / A_Master_Year_Totals.Net_Rev AS Value,
   @KPI_UpdatedAt
FROM 
                A_Master_Year
	INNER JOIN  A_Master_Year_Totals
	     USING  ( MonthNum )
WHERE A_Master_Year.MonthNum = @KPI_Month
GROUP BY MonthNum;


#Inventory type (share of sales) - Consignment - stocked in own warehouse (in %)
REPLACE KPIs
SELECT 
   MonthNum   AS MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group AS `Group`,
   109        AS RowID,
   "Inventory type (share of sales) - Consignment - stocked in own warehouse (in %)"                AS KPI,
   "Gross revenues before cancelations (DWH definition: Net Revenue before cancelations excl. VAT)" AS Description,
   SUM( IF(     OrderAfterCan = 1 
            AND fulfillment_type_bp="consignment",
                A_Master_Year.Rev , 0 ) ) / A_Master_Year_Totals.Net_Rev AS Value,
   @KPI_UpdatedAt
FROM 
                A_Master_Year
	INNER JOIN  A_Master_Year_Totals
	     USING  ( MonthNum )
WHERE A_Master_Year.MonthNum = @KPI_Month
GROUP BY MonthNum;

#Inventory type (share of sales) - Cross-docking (in %)
REPLACE KPIs
SELECT 
   MonthNum    AS MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group  AS `Group`,
   110         AS RowID,
   "Inventory type (share of sales) - Cross-docking (in %)"                                         AS KPI,
   "Gross revenues before cancelations (DWH definition: Net Revenue before cancelations excl. VAT)" AS Description,
   SUM( IF(     OrderAfterCan = 1 
            AND fulfillment_type_bp="crossdocking",
                A_Master_Year.Rev , 0 ) ) / A_Master_Year_Totals.Net_Rev AS Value,
   @KPI_UpdatedAt
FROM 
                A_Master_Year
	INNER JOIN  A_Master_Year_Totals
	     USING  ( MonthNum )
WHERE A_Master_Year.MonthNum = @KPI_Month
GROUP BY MonthNum;

# 111
# Inventory type (share of sales) - Private Label (in %)

#Inventory type (share of sales) - Other (in %)
REPLACE KPIs
SELECT 
   MonthNum   AS MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group AS `Group`,
   112        AS RowID,
   "Inventory type (share of sales) - Other (in %)"                                                 AS KPI,
   "Gross revenues before cancelations (DWH definition: Net Revenue before cancelations excl. VAT)" AS Description,
   SUM( IF(     OrderAfterCan = 1 
            AND fulfillment_type_bp="other",
                A_Master_Year.Rev , 0 ) ) / A_Master_Year_Totals.Net_Rev AS Value,
   @KPI_UpdatedAt
FROM 
                A_Master_Year
	INNER JOIN  A_Master_Year_Totals
	     USING  ( MonthNum )
WHERE A_Master_Year.MonthNum = @KPI_Month
GROUP BY MonthNum;

# COGS by inventory type outright (in %)
REPLACE KPIs
SELECT 
   DATE_FORMAT(date_shipped, "%x%m")                 AS MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   114                                                 AS RowID, 
   "COGS by inventory type - Outright (in %)"          AS KPI,
   "COGS (incl. landed cost, port costs, customs and any "
   "other costs until goods arrive at the warehouse) as a"
   " % of original price (excl. VAT) - based on gross orders" AS Description,
   SUM( IF(     OrderBeforeCan =1
            AND fulfillment_type_bp="outright buying",
	            COGS  , 0))     /
   SUM( IF(     OrderBeforeCan =1
            AND fulfillment_type_bp="outright buying",
	            PriceAfterTax , 0)) AS Value,
   @KPI_UpdatedAt
FROM 
    A_Master_Year 
WHERE
   DATE_FORMAT(date_shipped, "%x%m") = @KPI_Month                 
GROUP BY MonthNum;

#COGS by inventory type - Consignment - stocked in own warehouse (in %)
REPLACE KPIs
SELECT 
   DATE_FORMAT(date_shipped, "%x%m") AS MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                        AS `Group`,
   115                                                                      AS RowID,
   "COGS by inventory type - Consignment - stocked in own warehouse (in %)" AS KPI,
   "COGS (incl. landed cost, port costs, customs and any other costs until "
   "goods arrive at the warehouse) as a % of original price (excl. VAT) - based on gross orders"            AS Description,
   SUM( IF(     OrderBeforeCan =1
            AND fulfillment_type_bp="consignment",
	            COGS  , 0))     /
   SUM( IF(     OrderBeforeCan =1
            AND fulfillment_type_bp="consignment",
                PriceAfterTax , 0)) AS Value,
   @KPI_UpdatedAt
FROM 
    A_Master_Year 
WHERE DATE_FORMAT(date_shipped, "%x%m")  = @KPI_Month
GROUP BY MonthNum;

#COGS by inventory type - Cross-docking (in %)
REPLACE KPIs
SELECT 
   DATE_FORMAT(date_shipped, "%x%m")                   AS MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   116                                                 AS RowID,
   "COGS by inventory type - Cross-docking (in %)"     AS KPI,
   "COGS (incl. landed cost, port costs, customs and "
   "any other costs until goods arrive at the warehouse) "
   "as a % of original price (excl. VAT) - based on gross orders" AS Description,
    SUM( IF(     OrderBeforeCan =1
            AND fulfillment_type_bp="crossdocking",
	            COGS  , 0))     /
   SUM( IF(     OrderBeforeCan =1
            AND fulfillment_type_bp="crossdocking",
                PriceAfterTax , 0)) AS Value,		  
   @KPI_UpdatedAt
FROM 
    A_Master_Year 
WHERE
    DATE_FORMAT(date_shipped, "%x%m") = @KPI_Month
GROUP BY MonthNum;

#COGS by inventory type - Private Label (in %)
REPLACE KPIs
SELECT 
   DATE_FORMAT(date_shipped, "%x%m")                   AS MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   117                                                 AS RowID,
   "COGS by inventory type - Private Label (in %)"     AS KPI,
   "COGS (incl. landed cost, port costs, customs and "
   "any other costs until goods arrive at the warehouse)"
   " as a % of original price (excl. VAT) - based on "
   "gross orders"  AS Description,
    SUM( IF(     OrderBeforeCan =1
            AND fulfillment_type_bp="",
	            COGS  , 0))     /
    SUM( IF(     OrderBeforeCan =1
            AND fulfillment_type_bp="",
                PriceAfterTax , 0)) AS Value,		  
   @KPI_UpdatedAt
FROM 
    A_Master_Year 
WHERE
   DATE_FORMAT(date_shipped, "%x%m") = @KPI_Month                   
GROUP BY MonthNum;

#COGS by inventory type - Other (in %)
REPLACE KPIs
SELECT 
   MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   118                                                 AS RowID,
   "COGS by inventory type - Other (in %)"             AS KPI,
   "COGS (incl. landed cost, port costs, customs and "
   "any other costs until goods arrive at the warehouse) "
   "as a % of original price (excl. VAT) - based on gross orders" AS Description,
    SUM( IF(     OrderBeforeCan =1
            AND fulfillment_type_bp="other",
	            COGS  , 0))     /
    SUM( IF(     OrderBeforeCan =1
            AND fulfillment_type_bp="other",
                PriceAfterTax , 0)) AS Value,		  
   @KPI_UpdatedAt
FROM 
    A_Master_Year 
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;

#Discount rate (in %)
REPLACE KPIs
SELECT 
   MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   119                                                 AS RowID,
   "Discount rate (in %)"             AS KPI,
   "Discounts / Net Revenues before discounts "
   "(see DWH definition below); Where applicable "
   "use Recommended Retail Price (RRP) as starting "
   "point for discount calculation" AS Description,
   ( ( SUM(OriginalPrice) - SUM(Price) ) ) / SUM(OriginalPrice) AS Value,
   @KPI_UpdatedAt
FROM 
    A_Master_Year 
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;


#Revenue per category (in k local) - 
SET @index=121;
SET @indexIni=121;
SET @monthNumAct=0;
SET @KPI = "COGS by category - ";
SET @KPI_Description = "COGS (incl. landed cost, port costs, customs and "
                       "any other costs until goods arrive at the warehouse) "
                       "as a % of original price (excl. VAT)";

REPLACE KPIs
SELECT
   MonthNum,
   SpreadSheet,
   `Group`,
   RowId,
   KPI,
   descripcion,
   Value,
   KPI_UpdatedAt
FROM
(
SELECT
   KPI.MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group               AS `Group`,
   @index := IF( @monthNumAct != KPI.MonthNum , @indexIni ,
                 @index + 1 ) AS RowId,
   @monthNumAct := KPI.MonthNum ,
   CONCAT( @KPI , KPI.CatKPI )  AS KPI,
   @KPI_Description  AS descripcion,
   KPI.Value,
   @KPI_UpdatedAt AS KPI_UpdatedAt
FROM
(
   SELECT
      TMP.MonthNum,
      TMP.CatKPI,
      TMP.Value,
      KPIorder 
   FROM
      development_mx.M_CategoryKPI
      INNER JOIN
      (
         SELECT 
            MonthNum, 
            CatKPI,
            SUM( IF( OrderBeforeCan =1,
	                 COGS  , 0))     /
            SUM( IF( OrderBeforeCan =1,
                     PriceAfterTax , 0)) AS Value		 
        FROM 
           A_Master_Year 
        WHERE 
           MonthNum = @KPI_Month
        GROUP BY MonthNum, CatKPI
      ) AS TMP 
      ON     TMP.CatKPI = development_mx.M_CategoryKPI.CatKPI
   GROUP BY MonthNum , CONCAT( @KPI , TMP.CatKPI )
   ORDER BY
      MonthNum, development_mx.M_CategoryKPI.KPIorder
) AS KPI
ORDER BY
   MonthNum, KPIorder
) AS Final
;

# Payment terms per FT Outright
REPLACE KPIs
SELECT 
   TMP.MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   137                                                 AS RowID,
   "Payment terms - Outright (in days)"                    AS KPI,
   "Actual days payable (i.e. not negotiated) that include"
   " shipping, internal processes (e.g. production process),"
   " etc."                                                 AS Description,
   sum( TMP.producto)/sum(TMP.suma)     AS Value,
   @KPI_UpdatedAt

FROM (
         SELECT
            date_format(date_goods_received, "%x%m") as MonthNum,
            fulfillment_type_bp,
            supplier_name,
            sum(cost_oms)*avg(payment_terms) as producto,
            sum(cost_oms) as suma 
         FROM
            out_procurement_tracking_Year
         WHERE
                fulfillment_type_bp = "Outright Buying" 
            AND date_format(date_goods_received, "%x%m") = @KPI_Month
         GROUP BY MonthNum
      ) AS TMP
GROUP BY MonthNum

;

#Payment terms - Consignment (in days)
REPLACE KPIs
SELECT 
   TMP.MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   138                                                 AS RowID,
   "Payment terms - Consignment (in days)"             AS KPI,
   "Actual days payable (i.e. not negotiated) that "
   "include shipping, internal processes (e.g. production "
   "process), etc." AS Description,
   sum( TMP.producto)/sum(TMP.suma)     AS Value,
   @KPI_UpdatedAt

FROM (
         SELECT
            date_format(date_goods_received, "%x%m") as MonthNum,
            fulfillment_type_bp,
            supplier_name,
            sum(cost_oms)*avg(payment_terms) as producto,
            sum(cost_oms) as suma 
         FROM
            out_procurement_tracking_Year
         WHERE
                fulfillment_type_bp="consignment" 
            AND date_format(date_goods_received, "%x%m") = @KPI_Month
         GROUP BY MonthNum
      ) AS TMP
GROUP BY MonthNum

;

#Payment terms - Cross-docking (in days)
REPLACE KPIs
SELECT 
   TMP.MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   139                                                 AS RowID,
   "Payment terms - Cross-docking (in days)"           AS KPI,
   "Actual days payable (i.e. not negotiated) that "
   "include shipping, internal processes (e.g. production "
   "process), etc." AS Description,
   sum( TMP.producto)/sum(TMP.suma)     AS Value,
   @KPI_UpdatedAt

FROM (
         SELECT
            date_format(date_goods_received, "%x%m") as MonthNum,
            fulfillment_type_bp,
            supplier_name,
            sum(cost_oms)*avg(payment_terms) as producto,
            sum(cost_oms) as suma 
         FROM
            out_procurement_tracking_Year
         WHERE
                fulfillment_type_bp="crossdocking"
            AND date_format(date_goods_received, "%x%m") = @KPI_Month
         GROUP BY MonthNum
      ) AS TMP
GROUP BY MonthNum

;

#Payment terms - Private Label (in days)

# Payment terms per FT otherREPLACE KPIs
REPLACE KPIs
SELECT 
   TMP.MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   141                                                 AS RowID,
   "Payment terms - Other (in days)"                   AS KPI,
   "Actual days payable (i.e. not negotiated) that "
   "include shipping, internal processes (e.g. production "
   "process), etc."                                    AS Description,
   sum( TMP.producto)/sum(TMP.suma)     AS Value,
   @KPI_UpdatedAt

FROM (
         SELECT
            date_format(out_order_tracking_Year.date_ordered, "%x%m") as MonthNum,
            out_order_tracking_Year.fulfillment_type_bp,
            out_procurement_tracking_Year.supplier_name,
            sum(out_procurement_tracking_Year.cost_oms)*avg(datediff(out_procurement_tracking_Year.date_paid, out_order_tracking_Year.date_delivered)) as producto,
            sum(out_procurement_tracking_Year.cost_oms) as suma 
         FROM
                       out_procurement_tracking_Year
            INNER JOIN out_order_tracking_Year
							   USING  ( item_id )
         WHERE
                out_order_tracking_Year.fulfillment_type_bp="other"
            AND date_format(out_order_tracking_Year.date_ordered, "%x%m") = @KPI_Month
         GROUP BY MonthNum

      ) AS TMP

GROUP BY MonthNum

;

# Inventory ageing - Share of owned inventory < 30 days old
REPLACE KPIs
SELECT 
   MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   143                                                 AS RowID,
   "Inventory ageing - Share of owned inventory < 30 days old"           AS KPI,
   "Purchase value of outright inventory added to warehouse "
   "less than 30 days ago / total purchase value of outright "
   "inventory" AS Description,
   SUM(IF( Days_inStock <= 30 ,1,0) )/COUNT(*)     AS Value,
   @KPI_UpdatedAt
FROM 
   KPI_inventory_age
WHERE
   MonthNum = @KPI_Month
GROUP BY MonthNum;

#Inventory ageing - Share of owned  inventory between 31 and 60 days old
REPLACE KPIs
SELECT 
   MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   144                                                 AS RowID,
   "Inventory ageing - Share of owned  inventory between 31 and 60 days old"           AS KPI,
   "Purchase value of outright inventory added to warehouse less than 30 "
   "days ago / total purchase value of outright inventory" AS Description,
   SUM(IF( Days_inStock BETWEEN 31 AND 60 ,1,0) )/COUNT(*)     AS Value,
   @KPI_UpdatedAt
FROM 
   KPI_inventory_age
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;

#Inventory ageing - Share of owned  inventory between 61 and 90 days old
REPLACE KPIs
SELECT 
   MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   145                                                 AS RowID,
   "Inventory ageing - Share of owned  inventory between 91 and 120 days old"           AS KPI,
   "Purchase value of outright inventory added to warehouse "
   "less than 30 days ago / total purchase value of outright "
   "inventory" AS Description,
   SUM(IF( Days_inStock BETWEEN 61 AND 90 ,1,0) )/COUNT(*)     AS Value,
   @KPI_UpdatedAt
FROM 
   KPI_inventory_age
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;

#Inventory ageing - Share of owned  inventory between 91 and 120 days old
REPLACE KPIs
SELECT 
   MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   146                                                 AS RowID,
   "Inventory ageing - Share of owned  inventory between 91 and 120 days old"           AS KPI,
   "Purchase value of outright inventory added to warehouse less than 30 "
   "days ago / total purchase value of outright inventory" AS Description,
   SUM(IF( Days_inStock BETWEEN 91 AND 120 ,1,0) )/COUNT(*)     AS Value,
   @KPI_UpdatedAt
FROM 
   KPI_inventory_age
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;

#Inventory ageing - Share of owned  inventory more than 120 days old
REPLACE KPIs
SELECT 
   MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   147                                                 AS RowID,
   "Inventory ageing - Share of owned  inventory more than 120 days old"           AS KPI,
   "Purchase value of outright inventory added to warehouse less than 30 "
   "days ago / total purchase value of outright inventory" AS Description,
   SUM(IF( Days_inStock > 120 ,1,0) )/COUNT(*)     AS Value,
   @KPI_UpdatedAt
FROM 
   KPI_inventory_age
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;

REPLACE KPIs
SELECT 
   @MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   152                                                 AS RowID,
   "Share of live SKU configs on sale/discount (in %)"           AS KPI,
   "Number of SKUs on sale / total # of SKUs"	AS Description,
   DISCOUNTS / DISTINCTS     AS Value,
   @KPI_UpdatedAt
FROM 
    (SELECT 
        count(DISTINCT CONFIG) DISTINCTS
    FROM
        (SELECT 
        `A_Master_Catalog`.`sku_config` AS CONFIG,
            (`A_Master_Catalog`.`isvisible`) VISIBLE,
            `A_Master_Catalog`.`special_from_date`,
            `A_Master_Catalog`.`special_to_date`,
            IF((`A_Master_Catalog`.`special_to_date`) >= CAST(DATE_FORMAT(CURDATE() - INTERVAL 1 MONTH, '%Y-%m-01')
                as DATE), 1, 0)
    FROM
        A_Master_Catalog) R
    WHERE
        VISIBLE = 1) T
        INNER JOIN
    (SELECT 
        count(DISTINCT CONFIG) as DISCOUNTS
    FROM
        (SELECT 
        `A_Master_Catalog`.`sku_config` AS CONFIG,
            (`A_Master_Catalog`.`isvisible`) VISIBLE,
            `A_Master_Catalog`.`special_from_date`,
            `A_Master_Catalog`.`special_to_date`,
            IF((`A_Master_Catalog`.`special_to_date`) >= CAST(DATE_FORMAT(CURDATE() - INTERVAL 1 MONTH, '%Y-%m-01')
                as DATE), 1, 0) DISC
    FROM
        A_Master_Catalog) R
    WHERE
        VISIBLE = 1 AND DISC = 1) Y
#WHERE MonthNum = @KPI_Month 
;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `KPI_CONCENTRATION`()
BEGIN

set @i=0;

replace into KPIs select  a.yrmonth, 3,'CONCENTRATION ANALYSES', 13, 'Top 10 best selling SKUs', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, sku, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, sku, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, sku order by yrmonth, net_revenue desc)a group by yrmonth, sku order by yrmonth, net_revenue desc)b where concat<=10 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into KPIs select  a.yrmonth, 3,'CONCENTRATION ANALYSES', 14, 'Top 11-30 best selling SKUs', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, sku, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, sku, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, sku order by yrmonth, net_revenue desc)a group by yrmonth, sku order by yrmonth, net_revenue desc)b where concat between 11 and 30 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into KPIs select  a.yrmonth, 3,'CONCENTRATION ANALYSES', 15, 'Top 31-50 best selling SKUs', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, sku, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, sku, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, sku order by yrmonth, net_revenue desc)a group by yrmonth, sku order by yrmonth, net_revenue desc)b where concat between 31 and 50 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into KPIs select  a.yrmonth, 3,'CONCENTRATION ANALYSES', 16, 'Top 51-100 best selling SKUs', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, sku, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, sku, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, sku order by yrmonth, net_revenue desc)a group by yrmonth, sku order by yrmonth, net_revenue desc)b where concat between 51 and 100 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into KPIs select  a.yrmonth, 3,'CONCENTRATION ANALYSES', 17, 'Top 101-1000 best selling SKUs', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, sku, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, sku, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, sku order by yrmonth, net_revenue desc)a group by yrmonth, sku order by yrmonth, net_revenue desc)b where concat between 101 and 1000 group by yrmonth)b where a.yrmonth=b.yrmonth;


replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 18, 'Top >1000 best selling SKUs', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, sku, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, sku, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, sku order by yrmonth, net_revenue desc)a group by yrmonth, sku order by yrmonth, net_revenue desc)b where concat >1000 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 21, 'Top 10 best selling SKUs', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, sku, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, sku, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, sku order by yrmonth, net_revenue desc)a group by yrmonth, sku order by yrmonth, net_revenue desc)b where concat<=10 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 22, 'Top 20 best selling SKUs', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, sku, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, sku, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, sku order by yrmonth, net_revenue desc)a group by yrmonth, sku order by yrmonth, net_revenue desc)b where concat<=20 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 23, 'Top 30 best selling SKUs', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, sku, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, sku, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, sku order by yrmonth, net_revenue desc)a group by yrmonth, sku order by yrmonth, net_revenue desc)b where concat<=30 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 24, 'Top 50 best selling SKUs', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, sku, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, sku, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, sku order by yrmonth, net_revenue desc)a group by yrmonth, sku order by yrmonth, net_revenue desc)b where concat<=50 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 25, 'Top 100 best selling SKUs', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, sku, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, sku, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, sku order by yrmonth, net_revenue desc)a group by yrmonth, sku order by yrmonth, net_revenue desc)b where concat<=100 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 26, 'Top 1000 best selling SKUs', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, sku, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, sku, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, sku order by yrmonth, net_revenue desc)a group by yrmonth, sku order by yrmonth, net_revenue desc)b where concat<=1000 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 28, 'Top 5 best selling brands', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, brand, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, brand, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, brand order by yrmonth, net_revenue desc)a group by yrmonth, brand order by yrmonth, net_revenue desc)b where concat<=5 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 29, 'Top 6-10 best selling brands', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, brand, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, brand, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, brand order by yrmonth, net_revenue desc)a group by yrmonth, brand order by yrmonth, net_revenue desc)b where concat between 6 and 10 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 30, 'Top 11-30 best selling brands', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, brand, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, brand, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, brand order by yrmonth, net_revenue desc)a group by yrmonth, brand order by yrmonth, net_revenue desc)b where concat between 11 and 30 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 31, 'Top 31-50 best selling brands', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, brand, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, brand, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, brand order by yrmonth, net_revenue desc)a group by yrmonth, brand order by yrmonth, net_revenue desc)b where concat between 31 and 50 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 32, 'Top >50 best selling brands', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, brand, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, brand, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, brand order by yrmonth, net_revenue desc)a group by yrmonth, brand order by yrmonth, net_revenue desc)b where concat>50 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 35, 'Top 5 best selling brands', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, brand, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, brand, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, brand order by yrmonth, net_revenue desc)a group by yrmonth, brand order by yrmonth, net_revenue desc)b where concat<=5 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 36, 'Top 10 best selling brands', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, brand, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, brand, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, brand order by yrmonth, net_revenue desc)a group by yrmonth, brand order by yrmonth, net_revenue desc)b where concat<=10 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 37, 'Top 20 best selling brands', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, brand, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, brand, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, brand order by yrmonth, net_revenue desc)a group by yrmonth, brand order by yrmonth, net_revenue desc)b where concat<=20 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 38, 'Top 30 best selling brands', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, brand, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, brand, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, brand order by yrmonth, net_revenue desc)a group by yrmonth, brand order by yrmonth, net_revenue desc)b where concat<=30 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 39, 'Top 50 best selling brands', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, brand, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, brand, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, brand order by yrmonth, net_revenue desc)a group by yrmonth, brand order by yrmonth, net_revenue desc)b where concat<=50 group by yrmonth)b where a.yrmonth=b.yrmonth;

create table  temporary_kpi as
select yrmonth, round(count(distinct custid)*0.1) as percent1, round(count(distinct custid)*0.2) as percent2, round(count(distinct custid)*0.3) as percent3, round(count(distinct custid)*0.4) as percent4, round(count(distinct custid)*0.5) as percent5 from production_pe.tbl_order_detail t where oac=1 group by yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 41, 'Top 10% of best purchasing customers', '% of total net revenue generated by customer group', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select b.yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, custid, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, custid, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, custid order by yrmonth, net_revenue desc)a group by yrmonth, custid order by yrmonth, net_revenue desc)b,  temporary_kpi t where concat<=t.percent1 and t.yrmonth=b.yrmonth group by b.yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 42, 'Next 10% of best purchasing customers', '% of total net revenue generated by customer group', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select b.yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, custid, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, custid, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, custid order by yrmonth, net_revenue desc)a group by yrmonth, custid order by yrmonth, net_revenue desc)b,  temporary_kpi t where concat between t.percent1 and percent2 and t.yrmonth=b.yrmonth group by b.yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 43, 'Next 10% of best purchasing customers', '% of total net revenue generated by customer group', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select b.yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, custid, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, custid, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, custid order by yrmonth, net_revenue desc)a group by yrmonth, custid order by yrmonth, net_revenue desc)b,  temporary_kpi t where concat between t.percent2 and percent3 and t.yrmonth=b.yrmonth group by b.yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 44, 'Next 10% of best purchasing customers', '% of total net revenue generated by customer group', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select b.yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, custid, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, custid, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, custid order by yrmonth, net_revenue desc)a group by yrmonth, custid order by yrmonth, net_revenue desc)b,  temporary_kpi t where concat between t.percent3 and percent4 and t.yrmonth=b.yrmonth group by b.yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 45, 'Rest', '% of total net revenue generated by customer group', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select b.yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, custid, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, custid, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, custid order by yrmonth, net_revenue desc)a group by yrmonth, custid order by yrmonth, net_revenue desc)b,  temporary_kpi t where concat>percent4 and t.yrmonth=b.yrmonth group by b.yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 48, 'Top 10% of best purchasing customers', '% of total net revenue generated by customer group', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select b.yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, custid, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, custid, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, custid order by yrmonth, net_revenue desc)a group by yrmonth, custid order by yrmonth, net_revenue desc)b,  temporary_kpi t where concat<=percent1 and t.yrmonth=b.yrmonth group by b.yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 49, 'Top 20% of best purchasing customers', '% of total net revenue generated by customer group', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select b.yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, custid, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, custid, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, custid order by yrmonth, net_revenue desc)a group by yrmonth, custid order by yrmonth, net_revenue desc)b,  temporary_kpi t where concat<=percent2 and t.yrmonth=b.yrmonth group by b.yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 50, 'Top 30% of best purchasing customers', '% of total net revenue generated by customer group', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select b.yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, custid, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, custid, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, custid order by yrmonth, net_revenue desc)a group by yrmonth, custid order by yrmonth, net_revenue desc)b,  temporary_kpi t where concat<=percent3 and t.yrmonth=b.yrmonth group by b.yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 51, 'Top 40% of best purchasing customers', '% of total net revenue generated by customer group', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select b.yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, custid, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, custid, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, custid order by yrmonth, net_revenue desc)a group by yrmonth, custid order by yrmonth, net_revenue desc)b,  temporary_kpi t where concat<=percent4 and t.yrmonth=b.yrmonth group by b.yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 52, 'Top 50% of best purchasing customers', '% of total net revenue generated by customer group', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth)a, (select b.yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, custid, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, custid, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_pe.tbl_order_detail where oac=1 group by yrmonth, custid order by yrmonth, net_revenue desc)a group by yrmonth, custid order by yrmonth, net_revenue desc)b,  temporary_kpi t where concat<=percent5 and t.yrmonth=b.yrmonth group by b.yrmonth)b where a.yrmonth=b.yrmonth;

drop table  temporary_kpi;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`lorena.ramirez`@`%`*/ /*!50003 PROCEDURE `KPI_CONCENTRATION_ANALYSES`( IN KPI_MonthNum INT)
BEGIN
SET @KPI_Month=KPI_MonthNum;
SET @KPI_SpreadSheet = 3;
SET @KPI_UpdatedAt = Now();
SET @KPI_Group = 'CONCENTRATION ANALYSES';

/****************************************/
-- SKUs separate buckets
/****************************************/
REPLACE KPIs (MonthNum,`Group`,RowId,KPI,Description,value,Updated_at, SpreadSheet)
SELECT
@KPI_Month,
 @KPI_Group,
CASE WHEN rango = 10 THEN 13
     WHEN rango = 11 THEN 14
     WHEN rango = 31 THEN 15
     WHEN rango = 51 THEN 16
     WHEN rango = 101 THEN 17
     WHEN rango = 1000 THEN 18
END AS rowId,
CASE WHEN rango = 10 THEN 'Top 10 best selling SKUs'
     WHEN rango = 11 THEN 'Top 11-30 best selling SKUs'
     WHEN rango = 31 THEN 'Top 31-50 best selling SKUs'
     WHEN rango = 51 THEN 'Top 51-100 best selling SKUs'
     WHEN rango = 101 THEN 'Top 101-1000 best selling SKUs'
     WHEN rango = 1000 THEN 'Top >1000 best selling SKUs'
END as KPI,
'% of total net revenue generated by SKUs' AS description, 
SUM(ventas)/total AS value,
 @KPI_UpdatedAt,
@KPI_SpreadSheet
FROM
(
SELECT SKUConfig, ventas, @rank := @rank +1 AS rank, 
       CASE WHEN @rank BETWEEN 1 AND 10 THEN 10 
            WHEN @rank BETWEEN 11 AND 30 THEN 11
            WHEN @rank BETWEEN 31 AND 50 THEN 31
            WHEN @rank BETWEEN 51 AND 100 THEN 51
            WHEN @rank BETWEEN 101 AND 1000 THEN 101
       ELSE 1000
       END AS rango, c.total
FROM 
(select SKUConfig, sum(Rev) ventas
from A_Master 
where OrderAfterCan = 1 and MonthNum = @KPI_Month
group by SKUConfig
order by ventas desc) a, (SELECT @rank:=0) b, 
(SELECT sum(Rev) AS total from A_Master 
where OrderAfterCan = 1 and MonthNum = @KPI_Month) c
) d
GROUP BY rango;

/****************************************/
-- SKUs cumulative buckets
/****************************************/
REPLACE KPIs (MonthNum,`Group`,RowId,KPI,Description,value,Updated_at, SpreadSheet)
SELECT
@KPI_Month,
@KPI_Group,
CASE WHEN rango = 10 THEN 21
     WHEN rango = 11 THEN 22
     WHEN rango = 31 THEN 23
     WHEN rango = 51 THEN 24
     WHEN rango = 101 THEN 25
     WHEN rango = 1000 THEN 26
END AS rowId,
CASE WHEN rango = 10 THEN 'Top 10 best selling SKUs'
     WHEN rango = 11 THEN 'Top 20 best selling SKUs'
     WHEN rango = 31 THEN 'Top 30 best selling SKUs'
     WHEN rango = 51 THEN 'Top 50 best selling SKUs'
     WHEN rango = 101 THEN 'Top 100 best selling SKUs'
     WHEN rango = 1000 THEN 'Top 1000 best selling SKUs'
END as KPI,
'% of total net revenue generated by SKUs' AS description, 
kpi as value,
 @KPI_UpdatedAt,
@KPI_SpreadSheet 
FROM 
(
SELECT rango, ventas, @cummulative:=@cummulative+ventas AS cummulative, @cummulative/total as kpi
FROM (
SELECT rango, SUM(ventas) as ventas, total
FROM
(
SELECT SKUConfig, ventas, @rank := @rank +1 AS rank, 
       CASE WHEN @rank BETWEEN 1 AND 10 THEN 10 
            WHEN @rank BETWEEN 11 AND 20 THEN 11
            WHEN @rank BETWEEN 21 AND 30 THEN 31
            WHEN @rank BETWEEN 31 AND 50 THEN 51
            WHEN @rank BETWEEN 51 AND 100 THEN 101
            WHEN @rank BETWEEN 101 AND 1000 THEN 1000
       END AS rango, c.total
FROM 
(select SKUConfig, sum(Rev) ventas
from A_Master 
where OrderAfterCan = 1 and MonthNum = @KPI_Month
group by SKUConfig
order by ventas desc) a, (SELECT @rank:=0) b, 
(SELECT sum(Rev) AS total from A_Master 
where OrderAfterCan = 1 and MonthNum = @KPI_Month) c
) d
WHERE rango IS NOT NULL
GROUP BY rango) e, (SELECT @cummulative:=0) f) g;

/****************************************/
-- BRANDS
/****************************************/
REPLACE KPIs (MonthNum,`Group`,RowId,KPI,Description,value,Updated_at, SpreadSheet)
SELECT
@KPI_Month,
@KPI_Group,
CASE WHEN rango = 1 THEN 28
     WHEN rango = 6 THEN 29
     WHEN rango = 11 THEN 30
     WHEN rango = 31 THEN 31
     WHEN rango = 50 THEN 32
END AS rowId,
CASE WHEN rango = 1 THEN 'Top 5 best selling brands'
     WHEN rango = 6 THEN 'Top 6-10 best selling brands'
     WHEN rango = 11 THEN 'Top 11-30 best selling brands'
     WHEN rango = 31 THEN 'Top 31-50 best selling brands'
     WHEN rango = 50 THEN 'Top >50 best selling brands'
END as KPI,
'% of total net revenue generated by SKUs' AS description, 
SUM(ventas)/total as value,
 @KPI_UpdatedAt,
@KPI_SpreadSheet
FROM
(
SELECT brand, ventas, @rank := @rank +1 AS rank, 
       CASE WHEN @rank BETWEEN 1 AND 5 THEN 1 
            WHEN @rank BETWEEN 6 AND 10 THEN 6
            WHEN @rank BETWEEN 11 AND 30 THEN 11
            WHEN @rank BETWEEN 31 AND 50 THEN 31
       ELSE 50
       END AS rango, c.total
FROM (
select brand, sum(Rev) ventas
from A_Master 
where OrderAfterCan = 1 and MonthNum = @KPI_Month
group by brand
order by ventas desc) a, (SELECT @rank:=0) b, 
(SELECT sum(Rev) AS total from A_Master 
where OrderAfterCan = 1 and MonthNum = @KPI_Month) c
) d
GROUP BY rango;

/****************************************/
-- BRANDS cummulative
/****************************************/
REPLACE KPIs (MonthNum,`Group`,RowId,KPI,Description,value,Updated_at, SpreadSheet)
SELECT
@KPI_Month,
@KPI_Group,
CASE WHEN rango = 1 THEN 35
     WHEN rango = 6 THEN 36
     WHEN rango = 11 THEN 37
     WHEN rango = 21 THEN 38
     WHEN rango = 31 THEN 39
END AS rowId,
CASE WHEN rango = 1 THEN 'Top 5 best selling brands'
     WHEN rango = 6 THEN 'Top 10 best selling brands'
     WHEN rango = 11 THEN 'Top 20 best selling brands'
     WHEN rango = 21 THEN 'Top 30 best selling brands'
     WHEN rango = 31 THEN 'Top 50 best selling brands'
END as KPI,
'% of total net revenue generated by SKUs' AS description,
kpi as value,
 @KPI_UpdatedAt,
@KPI_SpreadSheet
FROM 
(
SELECT rango, ventas, @cummulative:=@cummulative+ventas AS cummulative, @cummulative/total as kpi
FROM (
SELECT rango, SUM(ventas) AS ventas, total
FROM
(
SELECT brand, ventas, @rank := @rank +1 AS rank, 
       CASE WHEN @rank BETWEEN 1 AND 5 THEN 1 
            WHEN @rank BETWEEN 6 AND 10 THEN 6
            WHEN @rank BETWEEN 11 AND 20 THEN 11
            WHEN @rank BETWEEN 21 AND 30 THEN 21
            WHEN @rank BETWEEN 31 AND 50 THEN 31
       END AS rango, c.total
FROM (
select brand, sum(Rev) ventas
from A_Master 
where OrderAfterCan = 1 and MonthNum = @KPI_Month
group by brand
order by ventas desc) a, (SELECT @rank:=0) b, 
(SELECT sum(Rev) AS total from A_Master 
where OrderAfterCan = 1 and MonthNum = @KPI_Month) c
) d
WHERE rango IS NOT NULL
GROUP BY rango) e, (SELECT @cummulative:=0) f) g;

/****************************************/
-- CUSTOMERS
/****************************************/
REPLACE KPIs (MonthNum,`Group`,RowId,KPI,Description,value,Updated_at, SpreadSheet)
SELECT
@KPI_Month,
@KPI_Group,
CASE WHEN rango = 10 THEN 41
     WHEN rango = 20 THEN 42
     WHEN rango = 30 THEN 43
     WHEN rango = 40 THEN 44
     WHEN rango = 50 THEN 45
END AS rowId,
CASE WHEN rango = 10 THEN 'Top 10% of best purchasing customers'
     WHEN rango = 20 THEN 'Next 10% of best purchasing customers'
     WHEN rango = 30 THEN 'Next 10% of best purchasing customers'
     WHEN rango = 40 THEN 'Next 10% of best purchasing customers'
     WHEN rango = 50 THEN 'Rest'
END as KPI,
'% of total net revenue generated by SKUs' AS description,
SUM(ventas)/total AS value,
 @KPI_UpdatedAt,
@KPI_SpreadSheet
FROM
(
SELECT CustomerNum, ventas, @rank := @rank +1 AS rank, 
       CASE WHEN @rank BETWEEN 1 AND ROUND(c.total/10) THEN 10 
            WHEN @rank BETWEEN ROUND((c.total/10))+1 AND ROUND((c.total/10))*2 THEN 20
            WHEN @rank BETWEEN ROUND((c.total/10))*2+1 AND ROUND((c.total/10))*3 THEN 30
            WHEN @rank BETWEEN ROUND((c.total/10))*3+1 AND ROUND((c.total/10))*4 THEN 40
       ELSE 50
       END AS rango, d.total
FROM (
select CustomerNum, sum(Rev) ventas
from A_Master 
where OrderAfterCan = 1 
and MonthNum = @KPI_Month
group by CustomerNum
order by ventas desc) a, (SELECT @rank:=0) b,
(SELECT COUNT(DISTINCT CustomerNum) AS total from A_Master 
where OrderAfterCan = 1 and MonthNum = @KPI_Month) c,
(SELECT sum(Rev) AS total from A_Master 
where OrderAfterCan = 1 and MonthNum = @KPI_Month) d
) e
GROUP BY rango;

/****************************************/
-- CUSTOMERS cummulative
/****************************************/
REPLACE KPIs (MonthNum,`Group`,RowId,KPI,Description,value,Updated_at, SpreadSheet)
SELECT
@KPI_Month,
@KPI_Group,
CASE WHEN rango = 10 THEN 48
     WHEN rango = 20 THEN 49
     WHEN rango = 30 THEN 50
     WHEN rango = 40 THEN 51
     WHEN rango = 50 THEN 52
END AS rowId,
CASE WHEN rango = 10 THEN 'Top 10% of best purchasing customers'
     WHEN rango = 20 THEN 'Top 20% of best purchasing customers'
     WHEN rango = 30 THEN 'Top 30% of best purchasing customers'
     WHEN rango = 40 THEN 'Top 40% of best purchasing customers'
     WHEN rango = 50 THEN 'Top 50% of best purchasing customers'
END as KPI,
'% of total net revenue generated by SKUs' AS description,
kpi as value,
 @KPI_UpdatedAt,
@KPI_SpreadSheet
FROM 
(
SELECT rango, ventas, @cummulative:=@cummulative+ventas AS cummulative, @cummulative/total as kpi
FROM (
SELECT rango, SUM(ventas) AS ventas, total
FROM
(
SELECT CustomerNum, ventas, @rank := @rank +1 AS rank, 
       CASE WHEN @rank BETWEEN 1 AND ROUND(c.total/10) THEN 10 
            WHEN @rank BETWEEN ROUND((c.total/10))+1 AND ROUND((c.total/10))*2 THEN 20
            WHEN @rank BETWEEN ROUND((c.total/10))*2+1 AND ROUND((c.total/10))*3 THEN 30
            WHEN @rank BETWEEN ROUND((c.total/10))*3+1 AND ROUND((c.total/10))*4 THEN 40
            WHEN @rank BETWEEN ROUND((c.total/10))*4+1 AND ROUND((c.total/10))*5 THEN 50
       END AS rango, d.total
FROM (
select CustomerNum, sum(Rev) ventas
from A_Master 
where OrderAfterCan = 1 
and MonthNum = @KPI_Month
group by CustomerNum
order by ventas desc) a, (SELECT @rank:=0) b,
(SELECT COUNT(DISTINCT CustomerNum) AS total from A_Master 
where OrderAfterCan = 1 and MonthNum = @KPI_Month) c,
(SELECT sum(Rev) AS total from A_Master 
where OrderAfterCan = 1 and MonthNum = @KPI_Month) d
) e
WHERE rango IS NOT NULL
GROUP BY rango) f, (SELECT @cummulative:=0) g) h;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `KPI_EXTRA`(in KPI_MonthNum int)
BEGIN

SET @KPI_Month:=KPI_MonthNum;
SET @KPI_SpreadSheet:=2;
SET @KPI_UpdatedAt:=now();

replace into   KPIs 
select 
concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), 2,'PRODUCT PERFORMANCE KPIs', 13, 'Branded Traffic (in k #)', 'Number of visits via direct URL typing or direct brand Search (SEM and SEO)', sum(visits)/1000, now() from SEM.campaign_ad_group_pe where ((source='google' and medium='cpc') or source like '%bing%') AND campaign like 'brandb%' group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));

replace into   KPIs 
select 
yrmonth, 2,'PRODUCT PERFORMANCE KPIs', 15, 'thereof SEM', 'Total traffic via channel',
 sum(visits)/1000, now() from SEM.google_optimization_ad_group_pe 
where channel = 'SEM' group by yrmonth;

replace into   KPIs 
select date_format(date, '%Y%m') yrmonth,2, 
'PRODUCT PERFORMANCE KPIs', 16, 'thereof Affiliate', 'Total traffic via channel',
 sum(visits)/1000, now() 
from SEM.campaign_ad_group_pe 
where (source_medium like '%affiliate%' 
or source_medium in ('pampanetwork / affiliates',
'tradetracker / Affiliates',
'pampanetwork / Affiliates',
'ads1.solocpm.com / referral'
)) group by yrmonth;

replace into   KPIs 
select date_format(date, '%Y%m') yrmonth,2, 
'PRODUCT PERFORMANCE KPIs', 18, 
'thereof Re-targeting (e.g. Sociomantic)', 'Total traffic via channel', 
sum(visits)/1000, now() 
from SEM.campaign_ad_group_pe 
where (source_medium like '%retargeting%' 
or source_medium in ('sociomantic / retargeting',
'vizury / retargeting',
'triggit / retargeting'
)) group by yrmonth;

replace into   KPIs
 select 	date_format(date, '%Y%m') yrmonth,2, 
'PRODUCT PERFORMANCE KPIs', 22, 'thereof Other sources', 'Total traffic via channel', sum(visits)/1000, now() from SEM.campaign_ad_group_pe where ((source!='google' and medium!='cpc') and source not like '%bing%') AND campaign not like 'brandb%' and
channel != 'SEM' 
and (source_medium like '%affiliate%' 
or source_medium in ('pampanetwork / affiliates',
'tradetracker / Affiliates',
'pampanetwork / Affiliates',
'ads1.solocpm.com / referral'
)) and (source_medium like '%retargeting%' 
or source_medium in ('sociomantic / retargeting',
'vizury / retargeting',
'triggit / retargeting'
))
 group by yrmonth;

replace into   KPIs 
select date_format(date,'%Y%m') yrmonth,2, 
'PRODUCT PERFORMANCE KPIs', 19, 'thereof Display w/o re-targeting', 
'Total traffic via channel', 
sum(visits)/1000, now()
 from SEM.campaign_ad_group_pe 
where source_medium = 'google / cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' 
or campaign like '|R%'
or campaign like '|C%')
 group by yrmonth;

replace into   KPIs 
select date_format(date,'%Y%m') yrmonth, 
2,'PRODUCT PERFORMANCE KPIs', 20, 'thereof Social Media Paid', 
'Total traffic via channel', sum(visits)/1000, now() 
from SEM.campaign_ad_group_pe
 where source_medium = 'facebook / socialmedia' 
or source_medium = 'facebook / socialmediaads'
or source_medium = 'facebook / socialmedia_fashion'
group by yrmonth;


replace into   KPIs
 select date_format(date, '%Y%m') yrmonth,2, 
'PRODUCT PERFORMANCE KPIs', 21, 'thereof CRM', 'Total traffic via channel', 
sum(visits)/1000, now() from SEM.campaign_ad_group_pe
 where source_medium = 'postal / crm' or source_medium like '%veinteractive%' 
or source_medium='%newsletter%' 
group by  yrmonth;


REPLACE KPIs (`Group`,KPI,MonthNum,value,RowId, SpreadSheet, Updated_at)
select 'PRODUCT PERFORMANCE KPIs' KPI_Group,
'Page impressions per visit (#)' KPI,
yrmonth, 
sum(impressions)/sum(visits) value,
26 RowId,
@KPI_SpreadSheet,
@KPI_UpdatedAt
from ga_mobile
where yrmonth = KPI_MonthNum
group by yrmonth desc;

REPLACE KPIs (`Group`,KPI,MonthNum,value,RowId, SpreadSheet, Updated_at)
select 'PRODUCT PERFORMANCE KPIs' KPI_Group,
'Page impressions per mobile visit (#)' KPI,
yrmonth, 
sum(impressions)/sum(visits) value,
27 RowId,
@KPI_SpreadSheet,
@KPI_UpdatedAt
from ga_mobile
where is_mobile = 1 and  yrmonth = KPI_MonthNum
group by yrmonth desc;

REPLACE KPIs (`Group`,KPI,MonthNum,value,RowId, SpreadSheet, Updated_at)
select 'PRODUCT PERFORMANCE KPIs' KPI_Group,
'CR mobile visits (%)' KPI,
yrmonth, 
sum(transactions)/sum(visits) value,
28 RowId,
@KPI_SpreadSheet,
@KPI_UpdatedAt
from ga_mobile
where is_mobile = 1
and  yrmonth = KPI_MonthNum
group by yrmonth desc;

REPLACE KPIs (`Group`,KPI,MonthNum,value,RowId, SpreadSheet, Updated_at)
select 'PRODUCT PERFORMANCE KPIs' KPI_Group,
'# of visits where on-site search was used' KPI,
yrmonth, 
sum(visits) value,
29 RowId,
@KPI_SpreadSheet,
@KPI_UpdatedAt
from ga_search_usage
where search_used = 'Visits With Site Search'
and  yrmonth = KPI_MonthNum
group by yrmonth desc;

REPLACE KPIs (`Group`,KPI,MonthNum,value,RowId, SpreadSheet, Updated_at)
#CR for visits with on-site search (%)
select 'PRODUCT PERFORMANCE KPIs' KPI_Group,
'CR for visits with on-site search (%)' KPI,
yrmonth, 
sum(transactions)/sum(visits) value,
31 RowId,
@KPI_SpreadSheet,
@KPI_UpdatedAt
from ga_search_usage
where search_used = 'Visits With Site Search'
and  yrmonth = KPI_MonthNum
group by yrmonth desc;

REPLACE KPIs (`Group`,KPI,MonthNum,value,RowId, SpreadSheet, Updated_at)
#CR for visits with on-site search (%)
select 'PRODUCT PERFORMANCE KPIs' KPI_Group,
'CR for visits without on-site search (%)' KPI,
yrmonth, 
sum(transactions)/sum(visits) value,
32 RowId,
@KPI_SpreadSheet,
@KPI_UpdatedAt
from ga_search_usage
where search_used = 'Visits Without Site Search'
and  yrmonth = KPI_MonthNum
group by yrmonth desc;

REPLACE KPIs (`Group`,KPI,MonthNum,value,RowId, SpreadSheet, Updated_at)
#CR for visits with on-site search (%)
select 'PRODUCT PERFORMANCE KPIs' KPI_Group,
'% of on site search w/o result' KPI,
yrmonth, 
sum(search_refinements)/sum(search_result_views) value,
34 RowId,
@KPI_SpreadSheet,
@KPI_UpdatedAt
from ga_search_usage
where search_used = 'Visits With Site Search'
and  yrmonth = KPI_MonthNum
group by yrmonth desc;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `KPI_MARKETING_MAIN`()
BEGIN

REPLACE INTO  KPIs 
SELECT
	concat(YEAR (date),	IF (MONTH (date) < 10,concat(0, MONTH(date)),MONTH (date)	)	) AS MonthNum,
  1 AS SpreadSheet,
	'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE' AS ´GROUP´,
	84 AS RowId,
	'# of total visits (in k)' AS KPI,
	'Number of total visits  incl. mobile' AS Descripcion,
	sum(visits)/1000 AS Value,
	now() AS UpdatedAt
FROM
	SEM.campaign_ad_group_pe
GROUP BY
	concat(YEAR (date),	IF (MONTH (date) < 10,concat(0, MONTH(date)),MONTH (date)));

REPLACE INTO  KPIs SELECT
	b.yrmonth,
  1 AS SpreadSheet,
	'VOLUME DRIVERS - INCL. MARKETPLACE',
	56,
	'Volume share of orders from paid channels',
	'Volume share of gross orders generated from paid channels (online, offline)',
	sum(gross_orders) / a.total_orders,
	now()
FROM
	marketing_report.global_report b,
	(
		SELECT
			yrmonth,
			sum(gross_orders) AS total_orders
		FROM
			marketing_report.global_report
		WHERE
			country = 'Peru'
		GROUP BY
			yrmonth
	) a
WHERE
	b.yrmonth = a.yrmonth
AND b.country = 'Peru'
AND b.channel_group IN (
	'Affiliates',
	'Branded',
	'Facebook Ads',
	'Google Display Network',
	'NewsLetter',
	'Offline Marketing',
	'Retargeting',
	'Search Engine Marketing',
	'Search Engine Optimization',
	'Social Media',
	'Mobile App',
	'Display'
)
GROUP BY
	a.yrmonth;

REPLACE INTO  KPIs SELECT
	b.yrmonth,
  1 AS SpreadSheet,
	'VOLUME DRIVERS - INCL. MARKETPLACE',
	57,
	'Volume share of orders from free channels',
	'Volume share of gross orders generated from free channels (online, offline)',
	sum(gross_orders) / a.total_orders,
	now()
FROM
	marketing_report.global_report b,
	(
		SELECT
			yrmonth,
			sum(gross_orders) AS total_orders
		FROM
			marketing_report.global_report
		WHERE
			country = 'Peru'
		GROUP BY
			yrmonth
	) a
WHERE
	b.yrmonth = a.yrmonth
AND b.country = 'Peru'
AND b.channel_group NOT IN (
	'Affiliates',
	'Branded',
	'Facebook Ads',
	'Google Display Network',
	'NewsLetter',
	'Offline Marketing',
	'Retargeting',
	'Search Engine Marketing',
	'Search Engine Optimization',
	'Social Media',
	'Mobile App',
	'Display'
)
GROUP BY
	a.yrmonth;

REPLACE INTO  KPIs SELECT
	yrmonth,
  1 AS SpreadSheet,
	'MARKETING EFFICIENCY - INCL. MARKETPLACE',
	199,
	'New customers from "free" channels ',
	'New customers (based on on net orders post rejections) generated from free traffic (direct, seo, mailings, etc.)',
	sum(b.new_customers),
	now()
FROM
	marketing_report.global_report b
WHERE
	b.channel_group NOT IN (
		'Affiliates',
		'Branded',
		'Facebook Ads',
		'Google Display Network',
		'NewsLetter',
		'Offline Marketing',
		'Retargeting',
		'Search Engine Marketing',
		'Search Engine Optimization',
		'Social Media',
		'Mobile App',
	'Display'
	)
AND country = 'Peru'
GROUP BY
	yrmonth;

REPLACE INTO  KPIs SELECT
	concat(
		YEAR (date),

	IF (
		MONTH (date) < 10,
		concat(0, MONTH(date)),
		MONTH (date)
	)
	),
  1 AS SpreadSheet,
	'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE',
	96,
	'% basket used',
	'Share of total visitors that have used the basket',
	sum(cart) / sum(visits),
	now()
FROM
	SEM.campaign_ad_group_pe
GROUP BY
	concat(
		YEAR (date),

	IF (
		MONTH (date) < 10,
		concat(0, MONTH(date)),
		MONTH (date)
	)
	);

REPLACE INTO  KPIs SELECT
	concat(
		YEAR (b.date),

	IF (
		MONTH (b.date) < 10,
		concat(0, MONTH(b.date)),
		MONTH (b.date)
	)
	),
  1 AS SpreadSheet,
	'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE',
	102,
	'% converted (step 4)',
	'Share of total visitors that have converted',
	TRANSACTION / sum(visits),
	now()
FROM
	SEM.campaign_ad_group_pe b,
	(
		SELECT
			concat(
				YEAR (date),

			IF (
				MONTH (date) < 10,
				concat(0, MONTH(date)),
				MONTH (date)
			)
			) AS yrmonth,
			count(*) AS TRANSACTION
		FROM
			SEM.transaction_id_mx
		GROUP BY
			concat(
				YEAR (date),

			IF (
				MONTH (date) < 10,
				concat(0, MONTH(date)),
				MONTH (date)
			)
			)
	) a
WHERE
	a.yrmonth = concat(
		YEAR (b.date),

	IF (
		MONTH (b.date) < 10,
		concat(0, MONTH(b.date)),
		MONTH (b.date)
	)
	)
GROUP BY
	concat(
		YEAR (b.date),

	IF (
		MONTH (b.date) < 10,
		concat(0, MONTH(b.date)),
		MONTH (b.date)
	)
	);

REPLACE INTO  KPIs SELECT
	concat(
		YEAR (date),

	IF (
		MONTH (date) < 10,
		concat(0, MONTH(date)),
		MONTH (date)
	)
	),
  1 AS SpreadSheet,
	'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE',
	93,
	'Bounce rate',
	'Share of immediate exits after landing on site',
	sum(bounce) / SUM(visits),
	now()
FROM
	SEM.campaign_ad_group_pe
GROUP BY
	concat(
		YEAR (date),

	IF (
		MONTH (date) < 10,
		concat(0, MONTH(date)),
		MONTH (date)
	)
	);

REPLACE INTO  KPIs SELECT
	concat(
		YEAR (date),

	IF (
		MONTH (date) < 10,
		concat(0, MONTH(date)),
		MONTH (date)
	)
	),
  1 AS SpreadSheet,
	'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE',
	89,
	'# of unique visitors (in k)',
	'Number of total unique visitors',
	sum(unique_visits)/1000,
	now()
FROM
	development_pe.kpi_automation_1
GROUP BY
	concat(
		YEAR (date),

	IF (
		MONTH (date) < 10,
		concat(0, MONTH(date)),
		MONTH (date)
	)
	);

REPLACE INTO  KPIs SELECT
	concat(
		YEAR (date),

	IF (
		MONTH (date) < 10,
		concat(0, MONTH(date)),
		MONTH (date)
	)
	),
  1 AS SpreadSheet,
	'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE',
	90,
	'# of unique mobile visitors (in k) - subset of above',
	'Number of total unique mobile visitors',
	sum(unique_visits)/1000,
	now()
FROM
	development_pe.kpi_automation_2
GROUP BY
	concat(
		YEAR (date),

	IF (
		MONTH (date) < 10,
		concat(0, MONTH(date)),
		MONTH (date)
	)
	);

REPLACE INTO  KPIs SELECT
	concat(
		YEAR (date),

	IF (
		MONTH (date) < 10,
		concat(0, MONTH(date)),
		MONTH (date)
	)
	),
  1 AS SpreadSheet,
	'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE',
	94,
	'% catalogue page seen',
	'Share of total visitors that have seen the catalogue page',
	sum(page_views),
	now()
FROM
	development_pe.kpi_automation_1
GROUP BY
	concat(
		YEAR (date),

	IF (
		MONTH (date) < 10,
		concat(0, MONTH(date)),
		MONTH (date)
	)
	);

replace into KPIs select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, 1, 'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE', 89, '# of unique visitors (in k)', 'Number of total unique visitors', sum(visitors)/1000, now() from development_pe.kpi_automation_6 where is_mobile='No' group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));

replace into KPIs select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, 1, 'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE', 90, '# of unique mobile visitors (in k) - subset of above', 'Number of total unique mobile visitors', sum(visitors)/1000, now() from development_pe.kpi_automation_6 where is_mobile='Yes' group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));

replace into KPIs select a.yrmonth, 1, 'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE', 97, '%  started checkout (step 1)', 'Share of total visitors that have started the checkout', checkout/visits, now() from (select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, sum(checkout) as checkout from development_pe.kpi_automation_5 group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))))b, (select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, sum(visits) as visits from SEM.campaign_ad_group_pe group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))))a where a.yrmonth=b.yrmonth;

replace into KPIs select a.yrmonth, 1, 'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE', 98, '% seen payment selection page (step 2) - not for single page checkout', 'Share of total visitors that have seen the payment selection page', checkout/visits, now() from (select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, sum(checkout) as checkout from development_pe.kpi_automation_5 group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))))b, (select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, sum(visits) as visits from SEM.campaign_ad_group_pe group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))))a where a.yrmonth=b.yrmonth;

replace into KPIs select a.yrmonth, 1, 'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE', 102, '% converted (step 4)', 'Share of total visitors that have converted', order_nr/visits, now() from (select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, count(distinct order_nr) as order_nr from production_pe.tbl_order_detail where oac=1 group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))))b, (select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, sum(visits) as visits from SEM.campaign_ad_group_pe group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))))a where a.yrmonth=b.yrmonth;

replace into KPIs select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, 1, 'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE', 85, 'thereof # of total mobile visits (in k)', 'Number of total mobile visits', sum(visits)/1000, now() from development_pe.kpi_automation_4 where is_mobile='Yes' group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `KPI_MARKETPLACE_REVENUE_AND_ORDER_WATERFALL`(IN KPI_MonthNum INT)
BEGIN
/*
*  Kpi Group:  MARKETPLACE REVENUE AND ORDER WATERFALL
*  Created By: BI Team
*  Developer:  Carlos Antonio Mondragón Soria
*  Created at: 2013-10-03
*  Lastest Updated: 2013-10-03
*/
SET @KPI_Month=KPI_MonthNum;
SET @KPI_Year=SUBSTR( KPI_MonthNum FROM 1 FOR 4 );

SET @KPI_SpreadSheet = 1;
SET @KPI_UpdatedAt = Now();
SET @KPI_Group = "MARKETPLACE REVENUE AND ORDER WATERFALL";

/*
* Sample Table Creations
*/ 
DROP TEMPORARY TABLE IF EXISTS KPI_MPlace;
CREATE TEMPORARY TABLE KPI_MPlace ( INDEX ( MonthNum, ItemId ), INDEX( OrderNum ) )
SELECT 
   A_Master.*
FROM A_Master
WHERE
       YEAR( Date ) = @KPI_Year
   AND isMPlace = 1
;

DROP TABLE IF EXISTS KPI_MPlace_OSRO;
CREATE TABLE KPI_MPlace_OSRO LIKE Out_SalesReportOrder;
INSERT KPI_MPlace_OSRO 
SELECT 
   Out_SalesReportOrder.* 
FROM          Out_SalesReportOrder 	
   INNER JOIN KPI_MPlace 
        USING ( OrderNum )
GROUP BY OrderNum
;

/*
* KPIs Calculations
*/

# 539 Gross valid merchandise orders
REPLACE KPIs
SELECT 
   Month_Num        AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   539         AS RowID,
   "Gross valid merchandise orders" AS KPI,
   "" AS Description,
   SUM( IF( KPI_MPlace_OSRO.OrderBeforeCan = 1,
            1 , 0 ) ) AS Value,
   @KPI_UpdatedAt
FROM 
   KPI_MPlace_OSRO
WHERE  Month_Num  = @KPI_Month      
GROUP BY MonthNum
;	

#540 Gross valid merchandise revenues = Marketplace GMV (in k local)
REPLACE KPIs 
SELECT
   MonthNum    AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   540,
   "Gross valid merchandise revenues = Marketplace GMV (in k local)" ,
   "" AS Description,
   sum( IF( OrderBeforeCan = 1, REV, 0 )) / 1000 AS Value,
   @KPI_UpdatedAt
FROM 
   KPI_MPlace
WHERE  MonthNum  = @KPI_Month      
GROUP BY MonthNum
;

#541 Gross valid venture marketplace revenues (in k local)
REPLACE KPIs 
SELECT
   MonthNum    AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   541,
   "Gross valid venture marketplace revenues (in k local)" ,
   "Gross valid merchandise revenues * commission" AS Description,
   sum( IF( OrderBeforeCan = 1 , PriceAfterTax - CostAfterTax, 0  )) /1000 ,
   @KPI_UpdatedAt
FROM 
   KPI_MPlace
WHERE  MonthNum  = @KPI_Month      
GROUP BY MonthNum
;

#542 Cancelled merchandise orders
REPLACE KPIs
SELECT
   MonthNum    AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   542,
   "Cancelled merchandise orders" ,
   "",
   -1 * count( distinct OrderNum ) ,
   @KPI_UpdatedAt
FROM 
   KPI_MPlace
WHERE
   /*NET KPI*/
       OrderBeforeCan = 1 
   AND Cancellations  = 1 
   AND MonthNum  = @KPI_Month      
GROUP BY MonthNum   
;

#   543 Cancelled merchandise revenues (in k local)
REPLACE KPIs
SELECT
   MonthNum    AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   543,
   "Cancelled merchandise revenues (in k local)" ,
   "" AS Description,
   COALESCE( SUM( PriceAfterTax - CostAfterTax  ) , 0 ) / -1000,
   @KPI_UpdatedAt
FROM
   KPI_MPlace
WHERE
   /*NET KPI*/
       OrderAfterCan =1  
   AND MonthNum  = @KPI_Month      
GROUP BY MonthNum
;

#  546   "Net venture marketplace revenues (in k local)" ,
REPLACE KPIs
SELECT
   #MonthNum    AS MonthNum, 
   @KPI_Month,
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   546,
   "Net venture marketplace revenues (in k local)" ,
   "Net merchandise revenues * commission" AS Description,
	Rev * MPlaceFee  / 1000  AS Value,
 #  0 AS Value,
   @KPI_UpdatedAt
FROM 
   KPI_MPlace
WHERE
   #Supplier in ( SELECT Bob_Supplier_Name FROM A_E_BI_Marketplace_Commission WHERE isRocketVenture = 1 )
   #AND 
	OrderAfterCan = 1 AND
   MonthNum  = @KPI_Month      
GROUP BY MonthNuM
;



#551 Total active listings
REPLACE KPIs
SELECT
   @KPI_Month AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   551,
   "Total active listings" ,
   "Number of online listings at the end of the month" AS Description,
   count( distinct sku_config ),
   @KPI_UpdatedAt
FROM 
              A_Master_Catalog AS osri
   INNER JOIN bob_live_pe.supplier soi 
           ON osri.id_supplier=soi.id_supplier
WHERE 
 #is_option_marketplace  > 0 
    Name like '%(MP)%'
AND isActive_SKUConfig  = 1
AND isVisible  = 1

;

#   552 Total active listings Of which from Rocket Internet ventures
REPLACE KPIs
SELECT
   @KPI_Month AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   552,
   "Total active listings Of which from Rocket Internet ventures" ,
   "" AS Descripcion,
   #count( distinct sku_config ),
   0 AS Value,
   @KPI_UpdatedAt
#FROM 
#   A_Master_Catalog
#WHERE 
#    A_Master_Catalog.isMarketPlace       = 1
#AND A_Master_Catalog.isActive_SKUConfig  = 1
#AND date_format( A_Master_Catalog.isMarketPlace_Since , "%Y%m" ) <= @KPI_Month
#AND A_Master_Catalog.supplier in ( SELECT A_E_BI_Marketplace_Commission.Bob_Supplier_Name FROM A_E_BI_Marketplace_Commission WHERE A_E_BI_Marketplace_Commission.isRocketVenture = 1 )

;

# 553 Merchant base at the end of the month
REPLACE KPIs
SELECT
   @KPI_Month AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   553,
   "Merchant base at the end of the month" ,
   "Total number of merchants at the end of the month" AS Description,
   count( DISTINCT supplier ),
   @KPI_UpdatedAt
FROM 
   A_Master_Catalog
WHERE 
    A_Master_Catalog.isMarketPlace       = 1
AND isActive_SKUConfig  = 1
AND isVisible  = 1
;
#   554 Merchant base at the end of the month Of which active" 
REPLACE KPIs
SELECT
   @KPI_Month AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   554,
   "Merchant base at the end of the month Of which active" ,
   "At least 5 net merchandise transactions per month" AS Description,
   COUNT(*),
   @KPI_UpdatedAt
FROM
   (
   SELECT 
      Supplier
   FROM 
      KPI_MPlace
   WHERE
          MonthNum = @KPI_Month 
      AND OrderAfterCan = 1
   GROUP BY Supplier
   ) AS OrdersSupplier
;

REPLACE KPIs
SELECT
   @KPI_Month AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   555,
   "Merchants delisted this month" ,
   "",
   0,
   @KPI_UpdatedAt
;

REPLACE KPIs
SELECT
   date_format( created_at , "%Y%m" )  AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   556,
   "New merchants this month" ,
   "",
   count(*),
   @KPI_UpdatedAt
FROM 
   bob_live_pe.supplier
WHERE 
    `name` like '%(MP)%'
GROUP BY date_format( created_at , "%Y%m" )
;

#   557 "New gross customers this month" ,
REPLACE KPIs
SELECT
   Month_Num  AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   557,
   "New gross customers this month" ,
   "Additional customers that placed at least one gross valid merchandise order",
   count(*),
   @KPI_UpdatedAt
FROM
   KPI_MPlace_OSRO
WHERE
       OrderBeforeCan = 1 
   AND First_Gross_Order = KPI_MPlace_OSRO.OrderNum
   AND Month_Num  = @KPI_Month      
GROUP BY MonthNum   
;

# 558 New net customers this month
REPLACE KPIs
SELECT
   Month_Num  AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   558,
   "New net customers this month" ,
   "Additional customers that placed at least one net merchandise order",
   count(*),
   @KPI_UpdatedAt
FROM
   KPI_MPlace_OSRO
WHERE
       KPI_MPlace_OSRO.OrderAfterCan = 1 
   AND KPI_MPlace_OSRO.First_Net_Order = KPI_MPlace_OSRO.OrderNum
   AND Month_Num = @KPI_Month
GROUP BY MonthNum
;

REPLACE KPIs 
SELECT
   MonthNum  AS MonthNum,
   @KPI_SpreadSheet AS SpreadSheet, 
   @KPI_Group  AS`Group`,
   561,
   "Total net merchandise items"  AS KPI,
   "In net merchandise orders" AS Description,
   count(*),
   @KPI_UpdatedAt
FROM 
   KPI_MPlace
WHERE
   OrderAfterCan = 1
AND MonthNum = @KPI_Month
GROUP BY MonthNum
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `KPI_NET_PROMOTER_SCORE`(IN KPI_MonthNum INT)
BEGIN
/*
*  Kpi Group:  NET PROMOTER SCORE
*  Created By: BI Team
*  Developer:  Eduardo Martinez
*  Created at: 2013-11-01
*  Lastest Updated: 2013-11-01
* Modified by: Paula Mendoza (2014-07-05)
*/

/*
*  Global Variables
*/
SET @KPI_Month=KPI_MonthNum;
SET @KPI_Year=SUBSTR( KPI_MonthNum FROM 1 FOR 4 );
SET @KPI_UpdatedAt = Now();
SET @KPI_SpreadSheet = 1;
SET @KPI_Group='NET PROMOTER SCORE';


Drop TEMPORARY TABLE if EXISTS NPS;
create TEMPORARY TABLE NPS
SELECT  
`Response ID`,
`IP Address`,
`Date Submitted`,
`Duplicate`,
`Time Taken to Complete (Seconds)`,
`Response Status`,
`NPS`
from  customer_service.NPSMailPE
where date_format(`Date Submitted`, "%x%m") = @KPI_Month
union all
select
`Response ID`,
`IP Address`,
`Date Submitted`,
`Duplicate`,
`Time Taken to Complete (Seconds)`,
`Response Status`,
`NPS`
from customer_service.NPSMailPE2
union all
select
`Response ID`,
`IP Address`,
`Date Submitted`,
`Duplicate`,
`Time Taken to Complete (Seconds)`,
`Response Status`,
`NPS`
from nps.NPSMarketPlacePE
where date_format(`Date Submitted`, "%x%m") = @KPI_Month;

replace KPIs
SELECT 
   date_format(`Date Submitted`, "%x%m")    AS MonthNum, 
   @KPI_SpreadSheet,
   @KPI_Group  AS`Group`,
   155         AS RowID,
   "Net promoter score" AS KPI,
   "Promoters (9 and 10) - detractors (0 through 6); as per net promoter score survey done to new customers" AS Description,
   round( (sum(if(NPS in(11,10),1,0))-
sum(if(NPS in(1,2,3,4,5,6,7),1,0)))/count(NPS) * 100  ) AS Value,
   @KPI_UpdatedAt
FROM 
   NPS
WHERE
   date_format(`Date Submitted`, "%x%m") = @KPI_Month
GROUP BY MonthNum
; 

#Cambio la encuesta, no se pueden medir estos npúmeros

/*
replace KPIs
SELECT 
   date_format(`Date Submitted`, "%x%m")    AS MonthNum, 
   @KPI_SpreadSheet,
   @KPI_Group  AS`Group`,
   156         AS RowID,
   "Satisfaction - website" AS KPI,
   "1 minimum, 5 maximum; as per net promoter score survey done to new customers" AS Description,
   (sum(`Facilidad de uso`)
+SUM(`Facilidad para navegar y filtrar`)+sum(`Ahorro del tiempo`)
+sum(`Diseos inspiradores`)+sum(`Facilidad en el proceso de pago`)
)
/(
sum(if(`Facilidad de uso`<>'',1,0))
+sum(if(`Facilidad para navegar y filtrar`<>'',1,0))+SUM(if(`Ahorro del tiempo`<>'',1,0))
+sum(if(`Diseos inspiradores`,1,0))
+sum(if(`Facilidad en el proceso de pago`<>'',1,0))
)
AS Value,
   @KPI_UpdatedAt
FROM 
   NPSMX
WHERE
   date_format(`Date Submitted`, "%x%m") = @KPI_Month
GROUP BY MonthNum
; 

replace KPIs
SELECT 
   date_format(`Date Submitted`, "%x%m")    AS MonthNum, 
   @KPI_SpreadSheet,
   @KPI_Group  AS`Group`,
   157         AS RowID,
   "Satisfaction - products" AS KPI,
   "1 minimum, 5 maximum; as per net promoter score survey done to new customers" AS Description,
   (sum(`Cantidad de productos`)+SUM(`Calidad de los productos`)+sum(`Calidad de las marcas`)
+sum(`Disponibilidad de tamaos de los productos`)+sum(`Informacin de tamaos/tallas`))/
(sum(if(`Cantidad de productos`<>'',1,0))+sum(if(`Calidad de los productos`<>'',1,0))
+sum(if(`Calidad de las marcas`<>'',1,0))
+sum(if(`Disponibilidad de tamaos de los productos`<>'',1,0))+sum(if(`Informacin de tamaos/tallas`<>'',1,0))
) AS Value,
   @KPI_UpdatedAt
FROM 
   NPSMX
WHERE
   date_format(`Date Submitted`, "%x%m") = @KPI_Month
GROUP BY MonthNum
; 

replace KPIs
SELECT 
   date_format(`Date Submitted`, "%x%m")    AS MonthNum, 
   @KPI_SpreadSheet,
   @KPI_Group  AS`Group`,
   158         AS RowID,
   "Satisfaction - delivery" AS KPI,
   "1 minimum, 5 maximum; as per net promoter score survey done to new customers" AS Description,
   (sum(`Informacin clara frente a la entrega`)+sum(`Servicio por parte de nuestros mensajeros`)
+SUM(`Tiempo de entrega`)+sum(`Calidad del empaque`)+sum(`Exactitud de los productos y documentos entregados`))
/
 (sum(if(`Informacin clara frente a la entrega`<>'',1,0))
+sum(if(`Servicio por parte de nuestros mensajeros`<>'',1,0))
+sum(if(`Tiempo de entrega`<>'',1,0))
+sum(if(`Calidad del empaque`<>'',1,0))
+sum(if(`Exactitud de los productos y documentos entregados`<>'',1,0))
) AS Value,
   @KPI_UpdatedAt
FROM 
   NPSMX
WHERE
   date_format(`Date Submitted`, "%x%m") = @KPI_Month

GROUP BY MonthNum
; 

replace KPIs
SELECT 
   date_format(`Date Submitted`, "%x%m")    AS MonthNum, 
   @KPI_SpreadSheet,
   @KPI_Group  AS`Group`,
   159         AS RowID,
   "Satisfaction - customer care" AS KPI,
   "1 minimum, 5 maximum; as per net promoter score survey done to new customers" AS Description,
   (sum(`Capacidad para resolver el problema`)+sum(`Aptitudes`)+SUM(`Velocidad de respuesta`)
+sum(`Amabilidad`))
/
(
sum(if(`Capacidad para resolver el problema`<>'',1,0))
+sum(if(`Aptitudes`<>'',1,0))
+sum(if(`Velocidad de respuesta`<>'',1,0))
+sum(if(`Amabilidad`<>'',1,0))
) AS Value,
   @KPI_UpdatedAt
FROM 
   NPSMX
WHERE
   date_format(`Date Submitted`, "%x%m") = @KPI_Month

GROUP BY MonthNum
; 

replace KPIs
SELECT 
   date_format(`Date Submitted`, "%x%m")    AS MonthNum, 
   @KPI_SpreadSheet,
   @KPI_Group  AS`Group`,
   160         AS RowID,
   "Satisfaction - returns & refunds" AS KPI,
   "1 minimum, 5 maximum; as per net promoter score survey done to new customers" AS Description,
   (sum(`Devolver un producto`)+sum(`Obtencin del reembolso`)+SUM(`Interaccin con atencin al cliente`))
/
(sum(if(`Devolver un producto`<>'',1,0))
+sum(if(`Obtencin del reembolso`<>'',1,0))
+sum(if(`Interaccin con atencin al cliente`<>'',1,0))
)AS Value,
   @KPI_UpdatedAt
FROM 
   NPSMX
WHERE
   date_format(`Date Submitted`, "%x%m") = @KPI_Month

GROUP BY MonthNum
;
*/
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 PROCEDURE `KPI_OPERATIONS_EFFICIENCY`(IN KPI_MonthNum INT)
BEGIN
#
SET @KPI_Month=KPI_MonthNum;
SET @KPI_Year=SUBSTR( KPI_MonthNum FROM 1 FOR 4 );

SET @KPI_UpdatedAt = Now();
SET @KPI_SpreadSheet = 1;
SET @KPI_Group = "OPERATIONS EFFICIENCY";


DROP TEMPORARY TABLE IF EXISTS KPI_CUSTOMER_in20_pe;
CREATE TEMPORARY TABLE KPI_CUSTOMER_in20_pe
SELECT *  FROM customer_service.bd_llamadas_pe
#WHERE YEAR(Fecha)=@KPI_Year
;

DROP TEMPORARY TABLE IF EXISTS KPI_CUSTOMER_ZEN;
CREATE TEMPORARY TABLE KPI_CUSTOMER_ZEN
SELECT `Created_at`,`Resolution time`  FROM customer_service.tbl_zendesk_pe
where `Solved at` is not null and `Resolution time`<72
#WHERE YEAR(Fecha)=@KPI_Year
;

DROP TEMPORARY TABLE IF EXISTS KPI_CUSTOMER_ZENv;
CREATE TEMPORARY TABLE KPI_CUSTOMER_ZENv
SELECT `Created_at`,`Resolution time`  FROM customer_service.tbl_zendesk_ve
where `Solved at` is not null and `Resolution time`<120
#WHERE YEAR(Fecha)=@KPI_Year
;

DROP TEMPORARY TABLE IF EXISTS KPI_MASTER_CAT;
CREATE TEMPORARY TABLE KPI_MASTER_CAT
SELECT * FROM A_Master
WHERE YEAR(Date)=@KPI_Year;

DROP TEMPORARY TABLE IF EXISTS KPI_Master_Catalog;
CREATE TEMPORARY TABLE KPI_Master_Catalog
SELECT * FROM A_Master_Catalog
WHERE YEAR(created_at_Simple)=@KPI_Year;

DROP TEMPORARY TABLE IF EXISTS KPI_Catalog_history;
CREATE TEMPORARY TABLE KPI_Catalog_history
SELECT * FROM production_pe.catalog_history 
where 
		YEAR( date ) = @KPI_Year;

DROP TEMPORARY TABLE IF EXISTS out_order_tracking_Year;
CREATE TEMPORARY TABLE 	out_order_tracking_Year
SELECT * FROM production_pe.out_order_tracking 
WHERE 
   YEAR( date_ordered ) = @KPI_Year
AND status_wms <> 'cancelado'     
;

DROP TEMPORARY TABLE IF EXISTS out_order_tracking_totals;
CREATE TEMPORARY TABLE 	out_order_tracking_totals
SELECT  
   date_format(date_shipped, "%x%m") AS MonthNum,
   SUM( IF( status_wms NOT IN ('Quebrado','quebra tratada','Cancelado'), 1, 0 ) ) AS tot_items,
   SUM( IF( date_shipped IS NOT NULL, 1, 0 ) ) AS tot_delivers
FROM out_order_tracking_Year 
WHERE 
    YEAR( date_ordered ) = @KPI_Year
AND status_wms <> 'cancelado'     
GROUP BY MonthNum
;
	
DROP TEMPORARY TABLE IF EXISTS A_SKUsVisible_Sample;
CREATE TEMPORARY TABLE  A_SKUsVisible_Sample LIKE A_SKUsVisible;
INSERT A_SKUsVisible_Sample SELECT * FROM A_SKUsVisible WHERE MonthNum = @KPI_Month;

DROP TEMPORARY TABLE IF EXISTS A_Stock_Sample;
CREATE TEMPORARY TABLE  A_Stock_Sample LIKE A_Stock;
INSERT A_Stock_Sample  SELECT * FROM A_Stock WHERE MonthNum = @KPI_Month;

REPLACE KPIs
SELECT 
   @KPI_Month                AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   164                       AS RowId ,
   "Production - Total number of SKUs online" AS KPI,
   "Total number of SKU configs online that "
   "are in own warehouse (outright or consignment)" AS Description ,
   COUNT( DISTINCT A_Stock.SKU_Config ) AS Value,
   @KPI_UpdatedAt  
FROM 
           A_SKUsVisible_Sample AS A_SKUsVisible 
INNER JOIN A_Stock_Sample       AS A_Stock
     USING (SKU_Config, MonthNum )
WHERE 
   MonthNum = @KPI_Month
GROUP BY MonthNum 
;


REPLACE KPIs
SELECT 
   @KPI_Month                AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   165                       AS RowId ,
   "Total number of SKUs in warehouse" AS KPI,
   "Total number of SKU configs that are "
   "in own warehouse (outright or consignment)" AS Description ,
   COUNT( DISTINCT A_Stock.SKU_Config ) AS Value,
   @KPI_UpdatedAt  
FROM 
A_Stock
WHERE 
   MonthNum = @KPI_Month
GROUP BY MonthNum 
;


REPLACE KPIs
SELECT 
   date_format(created_at_Config, "%x%m") AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   167                       AS RowId ,
   "Production - SKU configs created (#)" AS KPI,
   "Number of SKU configs added online in this month"
   "(incl. production, content-writing, quality check)" AS Description ,
   count(distinct(sku_config)) AS Value,
   @KPI_UpdatedAt  
FROM 
   KPI_Master_Catalog
WHERE date_format(created_at_Config, "%x%m") = @KPI_Month
GROUP BY date_format(created_at_Config, "%x%m")
;


REPLACE KPIs
SELECT 
   c.month_s as MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   171                       AS RowId ,
   "Logistics - 24h shipments (%) - customer perspective" AS KPI,
   "Orders (or items) shipped within 24h from date and time "
   "of order / total valid orders within period. Shipment = "
   "when the parcel is picked up by the carrier (either in "
   "house or third party)." AS Description ,   
   c.24hr/tot_items AS Value,
   @KPI_UpdatedAt     
FROM
(
   SELECT 
      date_format(out_order_tracking_Year.date_shipped, "%x%m") as month_s, 
	  count(*) as 24hr
   FROM 
      out_order_tracking_Year
   WHERE 
          out_order_tracking_Year.date_shipped <= production.workday(out_order_tracking_Year.date_ordered,1)
      AND out_order_tracking_Year.date_shipped is not null
      AND out_order_tracking_Year.status_wms NOT IN ('Quebrado','quebra tratada','Cancelado')
   GROUP BY 
      date_format(date_shipped, "%x%m")
) AS c
INNER JOIN out_order_tracking_totals
           ON c.month_s = out_order_tracking_totals.MonthNum
WHERE c.month_s = @KPI_Month
GROUP BY out_order_tracking_totals.MonthNum
;


REPLACE KPIs
SELECT 
   c.month_s as MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   172                       AS RowId ,
   "Logistics - 24h shipments (%) - warehouse perspective" AS KPI,
   "Measures warehouse performance: orders (or items) shipped "
   "within 24 hours of receiving the pick request or receiving "
   "the cross docked items / total orders (items) within period "
   "(in OMS 'item received'). It disregards the time it takes to "
   "validate the payment and / or receive the cross docked items. "
   "Shipment = when the parcel is picked up by the carrier (either "
   "in house or third party)." AS Description ,   
   c.24hr/tot_items AS Value,
   @KPI_UpdatedAt     
FROM
(
   SELECT 
      date_format(out_order_tracking_Year.date_shipped, "%x%m") as month_s, 
	  count(*) as 24hr
   FROM 
      out_order_tracking_Year
   WHERE 
          out_order_tracking_Year.date_shipped <= production.workday(out_order_tracking_Year.date_ready_to_pick,1)
      AND out_order_tracking_Year.date_shipped is not null
   GROUP BY 
      date_format(date_shipped, "%x%m")
) AS c
INNER JOIN out_order_tracking_totals
           ON c.month_s = out_order_tracking_totals.MonthNum
WHERE c.month_s = @KPI_Month
GROUP BY out_order_tracking_totals.MonthNum
;


REPLACE KPIs
SELECT 
   c.MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   173                       AS RowId ,
   "Logistics - On time delivery (or shipment) %" AS KPI,
   "Deliveries (shipments) within promise to customer (e.g. on website) "
   "/ total deliveries. If delivery information not available / reliable, "
   "shipment date are to be taken discounting an average delivery time (e.g."
   "5 days delivery promise and 2 days average delivery time = on time "
   "shipment up to and including day 3)" AS Description ,   
   c.Value/Total AS Value,
   @KPI_UpdatedAt     
FROM
(
SELECT
   date_format(date_delivered_promised, "%x%m") as MonthNum, 
   SUM( IF(  on_time_total_1st_attempt = 1 , 1, 0 ) ) AS Value,
   COUNT(*) AS Total
 FROM   
   out_order_tracking_Year
WHERE
    is_presale = 0
AND status_wms in( 
                        'aguardando estoque', 
                        'aguardando separacao', 
                        'analisando quebra', 
                        'DS estoque reservado', 
                        'Estoque reservado' ,
                        'expedido',
                        'faturado',
                        'separando'
                      )
AND date_format(date_delivered_promised, "%x%m") = @KPI_Month
GROUP BY MonthNum
) as c
;


REPLACE KPIs
SELECT 
   MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   174                       AS RowId ,
   "Logistics - Average order delivery lead time (days)" AS KPI,
   "Lead time from order to customer delivery (first trial). "
   "If delivery not available, shipment + estimated delivery "
   "lead time to be taken" AS Description ,   
   Value AS Value,
   @KPI_UpdatedAt     
FROM   
   (
     SELECT
	    DATE_FORMAT(date_delivered_promised, "%x%m") as MonthNum, 
		avg(
           case 
              when date_exported is null then null 
	         else production.calcworkdays(date_exported, 
			                                             case when date_1st_attempt is null then curdate() 
														 else date_1st_attempt end)end
            ) as Value 
	 FROM  out_order_tracking_Year
     WHERE     check_dates = 'correct'
           AND status_wms <> 'cancelado'
           AND DATE_FORMAT(date_delivered_promised, "%x%m") = @KPI_Month
     GROUP BY MonthNum
    ) AS TMP
;


REPLACE KPIs
SELECT 
   date_format(date_procured_promised, "%x%m") AS MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   175                       AS RowId ,
   "Logistics - Unable to fulfil (%)" AS KPI,
   "items cancelled due to out of stock, "
   "non availability at supplier or quality "
   "issues / total items validated (after "
   "payment and CS validation or alternatively "
   "items shipped)" AS Description ,   
   sum(is_stockout)/SUM( Item_counter ) AS Value,
   @KPI_UpdatedAt     
FROM   
	out_order_tracking_Year
where 
    is_presale = 0
AND fulfillment_type_real = "crossdocking"
AND status_wms not in('cancelado', 'quebrado', 'exportado','quebra_tratada', 'ac_analisando_quebra', 'ac_estoque_reservado','precancelado','aguardando confirmacion' )
AND date_format(date_procured_promised, "%x%m") = @KPI_Month
GROUP BY MonthNum;

#178
REPLACE KPIs
SELECT 
   date_format( event_date, "%Y%m" ) AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                    AS `Group`,
   178                           AS RowId ,
   "Customer Service - Inbound call service level (%)" AS KPI,
   "Calls received within 20seconds / total calls received during business hours (incl. abandoned)"  AS Description ,
   sum(answered_in_wh_20)/
sum(net_event)

AS Value,
   @Updated_at  
FROM 
   customer_service_pe.bi_ops_cc_pe
WHERE date_format( event_date, "%Y%m" )=@KPI_Month AND (pre=1 or pos=1)
GROUP BY date_format( event_date, "%Y%m" )
;

#179	Customer Service - Inbound chat service level (%) (if the case)
REPLACE KPIs
SELECT 
   date_format( chat_start_time , "%Y%m" ) AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                    AS `Group`,
   179                           AS RowId ,
   "Customer Service - Inbound chat service level (%) (if the case)" AS KPI,
   "Chats answered within 20 seconds / total chats received during business hours"  AS Description ,
sum(if(operator_first_response_delay <>'None' AND visitor_messages_sent>0 and operator_first_response_delay<=20,1,0) )
/ 
SUM(if(operator_first_response_delay <>'None' AND visitor_messages_sent>0,1,0) ) AS Value
,
  @Updated_at  
FROM 
   customer_service.tbl_olark_chat_pe
WHERE date_format(chat_start_time ,'%Y%m')=@KPI_Month
GROUP BY date_format( chat_start_time , "%Y%m" )
;



#180	Customer Service - Mail response time (hours)
REPLACE KPIs
SELECT
date_format(Fecha_creacion,'%Y%m'),
1,
'OPERATIONS EFFICIENCY',
180 as RowId,
'Customer Service - Mail response time (hours)' as KPI,
'Average time to answer a customer email (customer perspective, no adjustment for working days)' as Description,
avg(first_reply_time_in_minutes)/60  as value,
now() as Updated_at
from customer_service.tbl_zendesk_general
where pais='PE' and via in ('Mail','email')
and Clasificacion not like '%SPAM%'
and date_format(Fecha_creacion,'%Y%m')=@KPI_Month
;


#181	Customer Service - Total inbound calls answered (#)
REPLACE KPIs
SELECT 
   date_format( event_date , "%Y%m" ) AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                    AS `Group`,
   181                           AS RowId ,
   "Customer Service - Total inbound calls answered (#)" AS KPI,
   "Total inbound calls answered (i.e. picked up and served customer) incl. live chats"  AS Description ,
   sum(answered_in_wh) AS Value,
   @Updated_at
FROM 
customer_service_pe.bi_ops_cc_pe
where net_event=1  and (pre=1 or pos=1)
and date_format( event_date, "%Y%m" )=@KPI_Month
GROUP BY date_format( event_date, "%Y%m" )
;

#182	Customer Service - Total outbound calls made (#)###
REPLACE KPIs
SELECT 
   date_format( calldate , "%Y%m" ) AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                    AS `Group`,
   182                           AS RowId ,
   "Customer Service - Total outbound calls made (#)" AS KPI,
   "Total outbound calls made (i.e. called and reached)"  AS Description ,
    count(disposition) AS Value,
   @Updated_at
FROM 
   customer_service_pe.cdr
WHERE date_format( calldate, "%Y%m" )=@KPI_Month AND
lastapp='Dial' and disposition='ANSWERED'
and src in ('824',
'833','830','834','831','808','806','823','811','825','813','835','807','802','812',
'817','820','827','805','821','826','804','818','826','832','142',
'139','136','147','121','130','146','113','144','143','122','119','126','123','829')

GROUP BY date_format( calldate , "%Y%m" )
;

#183	Customer Service - Total emails received (#)
REPLACE KPIs 
SELECT
date_format(Fecha_creacion,'%Y%m'),
1,
'OPERATIONS EFFICIENCY',
183 as RowId,
'Customer Service - Total emails received (sun)' as KPI,
'Total emails received by customers (excl. SPAM)' as Description,
count(1) as value,
now() as Updated_at
from customer_service.tbl_zendesk_general 
where pais='PE' and via='Mail'
and Clasificacion not like '%SPAM%' and
date_format(Fecha_creacion,'%Y%m')=@KPI_Month
;


REPLACE KPIs
SELECT 
   date_format( Fecha , "%Y%m" ) AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                    AS `Group`,
   184                           AS RowId ,
   "Customer Service - Total other contacts received (#)" AS KPI,
   "Social media contacts (e.g. facebook, twitter), chat and similar"  AS Description ,
   #SUM(Todas) AS Value,
   sum(`comment`) AS Value,
   @KPI_UpdatedAt
FROM (SELECT Fecha,`comment` from customer_service.tbl_facebook_comments_pe
			UNION ALL
			SELECT Date(chat_start_time) as Fecha,count(*) as `comment` 
			FROM customer_service.tbl_olark_chat_pe GROUP BY Date(chat_start_time))a
#   customer_service.VW_REDES_SOCIALES_DIA
WHERE date_format( Fecha , "%Y%m" ) = @KPI_Month
GROUP BY date_format( Fecha , "%Y%m" )
;





#190

DROP   TEMPORARY TABLE IF EXISTS temp_items_returned_hours;
CREATE TEMPORARY TABLE temp_items_returned_hours
SELECT date(date_inbound_il) AS date,
((UNIX_TIMESTAMP(date_quality_il)-UNIX_TIMESTAMP(date_inbound_il))/60)/60 AS return_hours
FROM operations_pe.out_inverse_logistics_tracking
WHERE is_returned=1
AND date_inbound_il IS NOT NULL
AND date_quality_il IS NOT NULL
AND date_closed_il IS NULL

GROUP BY date(date_inbound_il);

#REPLACE  KPIs
SELECT 
	 date_format( date, "%Y%m" ) AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                    AS `Group`,
   190                           AS RowId ,
   "Returns and refunds - Return processing lead time (hours)" AS KPI,
   "Average lead time from arrival of parcels at warehouse dock to return confirmation to customer (can be an estimate, no adjustment for working hours)"  AS Description ,
    avg(return_hours) AS Value,
 @Updated_at
FROM 
 temp_items_returned_hours
WHERE
  date_format( date  , "%Y%m" ) = @KPI_Month
 and return_hours > '0'
GROUP BY date_format( date, "%Y%m" );

#191


DROP   TEMPORARY TABLE IF EXISTS temp_items_returned_hours_total;
CREATE TEMPORARY TABLE temp_items_returned_hours_total
SELECT date_format((date_customer_request_il), "%Y%m") AS yrmonth,
avg(if(date_cash_refunded_il is not null,
			(((UNIX_TIMESTAMP(date_cash_refunded_il)-UNIX_TIMESTAMP(date_customer_request_il))/60)/60),
			(((UNIX_TIMESTAMP(date_voucher_refunded_il)-UNIX_TIMESTAMP(date_customer_request_il))/60)/60))) as return_hours_total
FROM operations_pe.out_inverse_logistics_tracking
WHERE is_returned=1
AND date_quality_il IS NOT NULL
AND date_refunded_il IS NOT NULL
GROUP BY yrmonth;

REPLACE KPIs
SELECT 
 yrmonth AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                    AS `Group`,
   191                           AS RowId ,
   "Returns and refunds - Refund processing lead time (hours)" AS KPI,
   "Average lead time from cancellation / return processing to refund confirmation to customer (can be an estimate, no adjustment for working time)"  
		AS Description ,
 return_hours_total AS Value,
 @Updated_at
FROM 
 temp_items_returned_hours_total
WHERE yrmonth = @KPI_Month;

#192 Returns and refunds - Refund tickets / requests older than 72h (%)####
SELECT  'Refund tickets / requests older than 72h (%)',now();
DROP   TEMPORARY TABLE IF EXISTS temp_items_refund_tickets;
CREATE TEMPORARY TABLE temp_items_refund_tickets
SELECT date_format((date_customer_request_il), "%Y%m") AS yrmonth,count(date_customer_request_il) AS refund_tickets
FROM operations_pe.out_inverse_logistics_tracking
WHERE is_returned=1
AND date_quality_il IS NOT NULL
AND date_closed_il IS NULL
#AND date_format((date_customer_request_il), "%Y%m")<(
#IF((date_cash_refunded_il IS NULL AND date_voucher_refunded_il IS NULL),date_format((CURDATE()), "%Y%m"),
#(if (date_cash_refunded_il is null ,date_format((date_voucher_refunded_il), "%Y%m"),
#date_format((date_cash_refunded_il), "%Y%m")))))
GROUP BY yrmonth;

#De los anteriores cual es mayor a 72 horas.
SELECT  'mas 72 horas',now();
DROP   TEMPORARY TABLE IF EXISTS temp_items_total;
CREATE TEMPORARY TABLE temp_items_total
SELECT date_format((date_customer_request_il), "%Y%m") AS yrmonth,count(date_customer_request_il) as total
FROM operations_pe.out_inverse_logistics_tracking
WHERE is_returned=1
AND date_quality_il IS NOT NULL
AND date_closed_il IS NULL
#AND date_format((date_customer_request_il), "%Y%m")<(
#IF((date_cash_refunded_il IS NULL AND date_voucher_refunded_il IS NULL),date_format((curdate()), "%Y%m"),
#(if (date_cash_refunded_il is null ,date_format((date_voucher_refunded_il), "%Y%m"),
#date_format((date_cash_refunded_il), "%Y%m")))))
AND (workdays_total_refunded_il)>3
GROUP BY date_format((date_customer_request_il), "%Y%m");

DROP   TEMPORARY TABLE IF EXISTS temp_items_final;
CREATE TEMPORARY TABLE temp_items_final
SELECT a.yrmonth,total/refund_tickets as refund_tickets FROM temp_items_refund_tickets as a
INNER JOIN temp_items_total as b
ON a.yrmonth=b.yrmonth;

REPLACE KPIs
SELECT 
   yrmonth AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                    AS `Group`,
   192                           AS RowId ,
   "Returns and refunds - Refund tickets / requests older than 72h (%)" AS KPI,
   "Refund tickets or requests older than 72h / Total refund tickets or requests open at the end of the month"  
		AS Description ,
   refund_tickets AS Value,
   @Updated_at
FROM 
   temp_items_final
WHERE yrmonth = @KPI_Month
;
SELECT  'fin mas 72 horas',now();


#193 Own last mile (if the case) - Delivery lead time (hours)
REPLACE KPIs
SELECT 
   date_format(date_delivered_promised, "%x%m")                   AS MonthNum,  
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                                     AS `Group`,   
   193                                                            AS RowId ,
   "Own last mile (if the case) - Delivery lead time (hours)"     AS KPI,
   "Average lead time from ready to pick up at main warehouse to delivered to customer. E.g. order ready at warehouse for pick up at 15.30, final delivery  next day at 15.30 = 24hours."                                                        AS Description ,
   AVG(TIME_TO_SEC(  TIMEDIFF(date_delivered + interval 12 hour, datetime_ready_to_ship ) ) / 3600) AS Value,
   @KPI_UpdatedAt
FROM 
   out_order_tracking_Year
WHERE 
    ( 
        shipping_carrier like '%MOTORIZADO%' 
     OR shipping_carrier like '%MOYSAC%' 
    )
AND date_format(date_delivered_promised, "%x%m") = @KPI_Month
GROUP BY MonthNum;


REPLACE KPIs
SELECT
  date_format(date_delivered_promised, "%x%m")                   AS MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
  @KPI_Group                                                     AS `Group`,  
  194                                                            AS RowId ,
  "Own last mile (if the case) - Delivery lead time (hours)"     AS KPI,
  "Average lead time from ready to pick up at main warehouse to"
  " delivered to customer. E.g. order ready at warehouse for pick "
  "up at 15.30, final delivery next day at 15.30 = 24hours." AS Description ,
  AVG(TIME_TO_SEC(  TIMEDIFF(date_delivered + interval 12 hour, datetime_ready_to_ship ) ) / 3600)                                                         AS Value,
  @Updated_at
FROM
   out_order_tracking_Year
WHERE         
    ( 
        shipping_carrier like '%MOTORIZADO%' 
     OR shipping_carrier like '%MOYSAC%' 
    )
    AND date_format(date_delivered_promised, "%x%m") = @KPI_Month                  
GROUP BY MonthNum;



REPLACE KPIs
SELECT 
   date_format(date_procured_promised, "%x%m") AS MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   195                       AS RowId ,
   "Own last mile (if the case) - First delivery attempt (%)" AS KPI,
   "Packages delivered at first attempt / total packages" AS Description ,   
   SUM(effective_1st_attempt)/SUM(item_counter) AS Value,
   @KPI_UpdatedAt     
FROM   
	out_order_tracking_Year
WHERE	
        ( 
           shipping_carrier like '%MOTORIZADO%' 
        OR shipping_carrier like '%MOYSAC%' 
        )
    AND is_presale = 0
    AND status_wms not in('cancelado', 'quebrado', 'quebra_tratada', 'ac_analisando_quebra', 'ac_estoque_reservado' )
	  AND cat_2 NOT LIKE "%CINE%"
    AND date_format(date_procured_promised, "%x%m") = @KPI_Month
GROUP BY MonthNum;


REPLACE KPIs
SELECT 
   MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   196                       AS RowId ,
   "Own last mile (if the case) - Coverage (%)" AS KPI,
   "Orders delivered with own last mile / total orders delivered (Packages can be taken if orders split)" AS Description ,   
   Own_Delivery/out_order_tracking_totals.tot_delivers AS Value,
   @KPI_UpdatedAt     
FROM
   (
   SELECT   
      date_format(date_shipped, "%x%m") AS MonthNum,
      COUNT(*) AS Own_Delivery
   FROM   
   	  out_order_tracking_Year
   WHERE	
        ( 
           shipping_carrier like '%MOTORIZADO%' 
        OR shipping_carrier like '%MOYSAC%' 
        )
    AND is_presale = 0
    AND status_wms not in('cancelado', 'quebrado', 'quebra_tratada', 'ac_analisando_quebra', 'ac_estoque_reservado' )
  	AND cat_2 NOT LIKE "%CINE%"
    AND date_format(date_shipped, "%x%m") = @KPI_Month
   GROUP BY MonthNum
   ) AS TMP
INNER JOIN 	out_order_tracking_totals
     USING ( MonthNum )
;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `KPI_ORDER_AND_CUSTOMER_DATA_INCL_MARKETPLACE`(IN KPI_MonthNum INT)
BEGIN
/*
*  Kpi Group:  ORDER AND CUSTOMER DATA - INCL. MARKETPLACE
*  Created By: BI Team
*  Developer:  Carlos Antonio Mondragón Soria
*  Created at: 2013-10-03
*  Lastest Updated: 2013-10-03
*/

/*
*  Global Variables
*/
SET @KPI_Month=KPI_MonthNum;
SET @KPI_Year=SUBSTR( KPI_MonthNum FROM 1 FOR 4 );
SET @KPI_SpreadSheet=1;

SET @KPI_UpdatedAt = Now();
SET @KPI_Group = "ORDER AND CUSTOMER DATA - INCL. MARKETPLACE";

/*
* Sample Table Creations
*/																					
DROP TEMPORARY TABLE IF EXISTS A_Master_Year;
CREATE TEMPORARY TABLE A_Master_Year ( INDEX( ItemId ), INDEX( OrderNum ) )
SELECT * FROM A_Master
WHERE
   YEAR( date ) = @KPI_Year;

DROP TEMPORARY TABLE IF EXISTS A_Master_Orders_Year;
CREATE TEMPORARY TABLE A_Master_Orders_Year  ( INDEX (OrderNum ) )
SELECT * FROM Out_SalesReportOrder
WHERE
   YEAR( date ) = @KPI_Year;

DROP TEMPORARY TABLE IF EXISTS TMP;
CREATE TEMPORARY TABLE TMP ( PRIMARY KEY ( OrderNum ) )
SELECT OrderNum , SUM( Returns ) FROM A_Master_Year WHERE Returns = 1 GROUP BY OrderNum;

UPDATE A_Master_Orders_Year SET Returns = 0;
UPDATE 
              A_Master_Orders_Year 
   INNER JOIN TMP
        USING ( OrderNum )
SET
   A_Master_Orders_Year.Returns = 1
WHERE   
   A_Master_Orders_Year.OrderAfterCan = 0 
;

DROP TEMPORARY TABLE IF EXISTS out_order_tracking_Year;
CREATE TEMPORARY TABLE 	out_order_tracking_Year
SELECT * FROM production_pe.out_order_tracking 
WHERE 
(
   YEAR( date_ordered  ) = @KPI_Year
OR YEAR( date_shipped  ) = @KPI_Year
OR YEAR( date_exported ) = @KPI_Year
OR YEAR( date_procured ) = @KPI_Year
OR YEAR( date_ready_to_pick ) = @KPI_Year
OR YEAR( date_ready_to_ship ) = @KPI_Year
OR YEAR( date_1st_attempt )   = @KPI_Year
OR YEAR( date_delivered )     = @KPI_Year
OR YEAR( date_procured_promised      ) = @KPI_Year
OR YEAR( date_ready_to_ship_promised ) = @KPI_Year
OR YEAR( date_delivered_promised     ) = @KPI_Year
)

AND status_wms <> 'cancelado'     
;


/*Orders Shipped*/
DROP   TEMPORARY TABLE IF EXISTS item_tracking;
CREATE TEMPORARY TABLE item_tracking
SELECT 
   order_number, 
   item_id, 
   date_format(max(date_shipped), "%x%m") as month_shipped, 
   0 AS ItemsInOrder, 
   SUM( IF( date_shipped is not null, 1, 0 ) ) AS Shipped,
   SUM( IF(    shipping_carrier like '%MOTORIZADO%' 
            OR shipping_carrier like '%MOYSAC%' 
           , 1, 0 ) ) AS OwnFleet
FROM out_order_tracking_Year  
WHERE
   status_wms <> 'cancelado'
GROUP BY order_number, item_id;

DROP   TEMPORARY TABLE IF EXISTS order_tracking;
CREATE TEMPORARY TABLE order_tracking
SELECT
	order_number,
	count(item_id) AS items_in_order,
	sum(shipped) AS items_shipped,
	month_shipped,
  IF( count(item_id) = sum(shipped) , 1 , 0 ) AS Shipped,
  IF( sum(OwnFleet) > 0  , 1 , 0 ) AS OwnFleet
FROM item_tracking  
GROUP BY order_number;


/*PerMonth Totals*/
DROP TEMPORARY TABLE IF EXISTS A_Master_Totals_PerMonth;
CREATE TEMPORARY TABLE A_Master_Totals_PerMonth
SELECT 
   MonthNum, 

   COUNT( Distinct ItemId )   AS Items,
	 COUNT( Distinct OrderNum ) AS Orders,
   SUM( Rev)                  AS Rev,   


   SUM( IF( OrderBeforeCan = 1, 1, 0 ) )     AS Gross_Items,
   0                                         AS Gross_Orders,
   SUM( IF( OrderBeforeCan = 1, Rev, 0 ) )   AS Gross_Rev,
   

   SUM( IF( OrderAfterCan = 1, 1, 0 ) )   AS Net_Items,
   0                                      AS Net_Orders,
   SUM( IF( OrderAfterCan = 1, Rev, 0 ) ) AS Net_Rev

FROM 
   A_Master_Year
GROUP BY MonthNum
;

DROP TEMPORARY TABLE IF EXISTS A_Master_Orders_PerMonth;
CREATE TEMPORARY TABLE A_Master_Orders_PerMonth
SELECT
   Month_Num as MonthNum,  
   COUNT(*)                                  AS Orders,

   SUM( OrderBeforeCan )                     AS Gross,
   SUM( IF( OrderBeforeCan = 1, Rev, 0 ) )   AS Gross_Rev,
   
   SUM( OrderAfterCan )                      AS Net,
   SUM( IF( OrderAfterCan = 1, Rev, 0 ) )    AS Net_Rev

FROM
   A_Master_Orders_Year
GROUP BY Month_Num;

UPDATE        A_Master_Totals_PerMonth 
   INNER JOIN A_Master_Orders_PerMonth
        USING ( MonthNum )
SET
   A_Master_Totals_PerMonth.Gross_Orders = A_Master_Orders_PerMonth.Gross,
   A_Master_Totals_PerMonth.Net_Orders   = A_Master_Orders_PerMonth.Net
;

/*
* KPIs calculations
*/
# Total Orders received
REPLACE KPIs
SELECT 
   MonthNum                  AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   12                        AS RowId ,
   "Total # orders received" AS KPI,
   "Orders received in BOB"  AS Description ,
   COUNT( DISTINCT OrderNum) ,
   @KPI_UpdatedAt
FROM 
   A_Master_Year
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum
;


# - thereof invalid
REPLACE KPIs
SELECT 
   Month_Num                 AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   13                        AS RowId ,
   "- thereof invalid"      AS KPI,
   "Orders invalidated (failed first fraud filter by Adyen or similar)"  AS Description ,
   COUNT(*)                 AS Value,
   @KPI_UpdatedAt  
FROM 
   A_Master_Orders_Year
WHERE
    OrderBeforeCan = 0
AND Month_Num = @KPI_Month
GROUP BY Month_Num
;

# - thereof canceled
REPLACE KPIs
SELECT 
  Month_Num                 AS MonthNum,
  @KPI_SpreadSheet          AS SpreadSheet,
  @KPI_Group                AS `Group`,
  15                        AS RowId ,
  "- thereof canceled"      AS KPI,
  "Orders cancelled or similar by customer or internally "
  "(for instance payslip system in some LatAm markets) "
  "(of the orders received in the period); only fully canceled orders"  AS Description ,
  SUM( IF( Cancelled = 1, 1, 0 ) ) AS Value,  
  @KPI_UpdatedAt  
FROM 
   A_Master_Orders_Year
WHERE Month_Num = @KPI_Month
GROUP BY Month_Num  
;

# - thereof rejected
REPLACE KPIs
SELECT 
  Month_Num                 AS MonthNum,
  @KPI_SpreadSheet          AS SpreadSheet,
  @KPI_Group                AS `Group`,
  17                        AS RowId ,
  "- thereof rejected"      AS KPI,
  "Orders refused on the day of "
  " shipment or post shipment,  "
  " for instance cash on delivery (no payment taken)"  AS Description ,
  SUM( IF( Rejected = 1, 1 , 0 ) ) AS Value,
  @KPI_UpdatedAt  
FROM 
   A_Master_Orders_Year 
WHERE Month_Num = @KPI_Month
GROUP BY MonthNum
;

# CHECK - thereof B2B
REPLACE KPIs
SELECT 
  MonthNum                  AS MonthNum,
  @KPI_SpreadSheet          AS SpreadSheet,
  @KPI_Group                AS `Group`,
  19                        AS RowId ,
  "- thereof B2B"           AS KPI,
  "B2B orders starting the "
  "fulfillment process minus "
  "canceled and rejected orders "
  "(before returned orders)"  AS Description ,
  COUNT( DISTINCT OrderNum ) AS Value,
  @KPI_UpdatedAt  
FROM 
   A_Master_Year
WHERE  
   (    OrderNum in   ( SELECT OrderNum FROM M_CorporativeOrders ) 
     OR PrefixCode in ( "CDEAL" ) 
     OR OrderNum in  ( SELECT order_nr FROM bob_live_pe.sales_order WHERE assisted_sales_operator  like '%ventascorporativas%'  ) 
   )
AND OrderAfterCan = 1
AND MonthNum = @KPI_Month
GROUP BY MonthNum;

# Total # items in gross orders
REPLACE KPIs
SELECT 
  Month_Num                 AS MonthNum,
  @KPI_SpreadSheet          AS SpreadSheet,
  @KPI_Group                AS `Group`,
  20                        AS RowId ,
  "Total # items in gross orders" AS KPI,
  "Total number of items in gross orders (valid orders)"  AS Description ,
  SUM( IF( OrderBeforeCan = 1 , ItemsInOrder , 0 ) ) AS Value,
  @KPI_UpdatedAt  
FROM 
   A_Master_Orders_Year
WHERE Month_Num = @KPI_Month
GROUP BY MonthNum;

#Total # items in net orders post rejections
REPLACE KPIs
SELECT 
   
  MonthNum                 AS MonthNum,
  @KPI_SpreadSheet         AS SpreadSheet,
  @KPI_Group               AS `Group`,
  21                       AS RowId ,
  "Total # items in net orders post rejections" AS KPI,
  "Total number of items in net orders post rejections (before returns)"  AS Description ,
  SUM( IF(     OrderAfterCan = 1 
           AND     Cancelled = 0 
           AND      Rejected = 0, 1 , 0 ) ) AS Value,
  @KPI_UpdatedAt  
FROM 
   A_Master_Year
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;

# Total # orders shipped
REPLACE KPIs
SELECT 
  month_shipped AS MonthNum,
  @KPI_SpreadSheet         AS SpreadSheet,
  @KPI_Group               AS `Group`,
  24                       AS RowId ,
  "Total # orders shipped" AS KPI,
  "Total number of orders "
  "shipped in the period"          AS Description, 
  SUM( IF( Shipped = 1 , 1 , 0 ) ) AS Value,
  @KPI_UpdatedAt
FROM
   order_tracking
WHERE month_shipped = @KPI_Month
GROUP BY month_shipped;

# - thereof own delivery fleet
REPLACE KPIs
SELECT 
  month_shipped AS MonthNum,
  @KPI_SpreadSheet         AS SpreadSheet,
  @KPI_Group               AS `Group`,
  25                       AS RowId ,
  "- thereof own delivery fleet" AS KPI,
  "Total number of items shipped in the period"  AS Description, 
  SUM( IF( OwnFleet = 1, 1 , 0 ) ) AS Value,
  @KPI_UpdatedAt
FROM
   order_tracking
WHERE month_shipped = @KPI_Month
GROUP BY month_shipped;

# Total # items shipped
REPLACE KPIs
SELECT 
  month_shipped                                    AS MonthNum,  
  #@KPI_Month,
  @KPI_SpreadSheet         AS SpreadSheet,
  @KPI_Group                                       AS `Group`,   
  26                                               AS RowId ,
  "Total # items shipped"                          AS KPI,
  "Total number of items shipped in the period"    AS Description ,
  SUM( IF( Shipped , 1 , 0 ) )     AS Value,
  # 0 AS Value,
   @KPI_UpdatedAt  
FROM 
   item_tracking
WHERE month_shipped = @KPI_Month
GROUP BY MonthNum;

#27 Total # items returned
#Total # items returned
REPLACE KPIs
SELECT 
  DATE_FORMAT(date_inbound_il,'%x%m')           AS MonthNum,  
  @KPI_SpreadSheet         AS SpreadSheet,
  @KPI_Group                                       AS `Group`,   
  27                                               AS RowId ,
  "Total # items returned"                         AS KPI,
  "Total number of items returned during the period"   AS Description ,
   sum(is_returned)                                   AS Value,
   @KPI_UpdatedAt  
FROM
   operations_pe.out_inverse_logistics_tracking
WHERE DATE_FORMAT(date_inbound_il,'%x%m') = @KPI_Month
GROUP BY MonthNum;

#Total # items returned (definition 2)

REPLACE KPIs
SELECT 
  DATE_FORMAT(date_ordered,'%x%m')                AS MonthNum,  
  @KPI_SpreadSheet         AS SpreadSheet,
  @KPI_Group                                       AS `Group`,   
  28                                               AS RowId ,
  "Total # items returned (definition 2)"          AS KPI,
  "Total number of items returned for the period in which order was placed (to be adjusted retroactively)"   AS Description ,
   sum(is_returned)                                   AS Value,
   @KPI_UpdatedAt  
FROM
   operations_pe.out_inverse_logistics_tracking
WHERE
   DATE_FORMAT(date_ordered,'%x%m') = @KPI_Month
GROUP BY
   MonthNum;


# of new customers (based on gross orders)
DROP TEMPORARY TABLE IF EXISTS TMP_KPI_Customers;
CREATE TEMPORARY TABLE TMP_KPI_Customers ( Id INT AUTO_INCREMENT, 
                                           PRIMARY KEY( CustomerNum, Id ) 
                                         )
SELECT
   CustomerNum,
   NULL AS Id,
   Date,
   Month_num,
   OrderNum,
   Rejected,
   Returns
FROM
   A_Master_Orders_Year  
WHERE
    OrderBeforeCan = 1
ORDER BY CustomerNum,Date
;
DELETE FROM TMP_KPI_Customers WHERE Id > 1;

REPLACE KPIs
SELECT 
  Month_Num                AS MonthNum,  
  @KPI_SpreadSheet         AS SpreadSheet,
  @KPI_Group              AS `Group`,   
  31                                               AS RowId ,
  "Total # items shipped"                          AS KPI,
  "Total number of items shipped in the period"    AS Description ,
  COUNT( DISTINCT CustomerNum ) AS Value,
  @KPI_UpdatedAt
FROM 
  TMP_KPI_Customers
WHERE Month_Num = @KPI_Month
GROUP BY MonthNum;


# of new customers (based on gross orders)
DROP TEMPORARY TABLE IF EXISTS TMP_KPI_Customers;
CREATE TEMPORARY TABLE TMP_KPI_Customers ( Id INT AUTO_INCREMENT, 
                                           PRIMARY KEY( CustomerNum, Id ) 
                                         )
SELECT
   CustomerNum,
   NULL AS Id,
   Date,
   Month_num,
   OrderNum,
   Rejected,
   Returns
FROM
   A_Master_Orders_Year  
WHERE
        OrderBeforeCan = 1
    AND Cancelled = 0
    AND Rejected  = 0
ORDER BY CustomerNum,Date
;
DELETE FROM TMP_KPI_Customers WHERE Id > 1;

# of new customers (based on net orders post rejection)
REPLACE KPIs
SELECT 
  Month_Num  AS MonthNum,  
  @KPI_SpreadSheet         AS SpreadSheet,
  @KPI_Group AS `Group`,   
  32                                               AS RowId ,
  "of new customers (based on net orders post rejection)" AS KPI,
  "Number of new customers with at least one valid purchase (based on net orders post rejections)"    AS Description ,
  COUNT( DISTINCT CustomerNum ) AS Value,
  @KPI_UpdatedAt
FROM 
  TMP_KPI_Customers
WHERE     Month_Num = @KPI_Month
group by Month_Num
;

# of new customers (based on net orders post rejections and returns)
DROP TEMPORARY TABLE IF EXISTS TMP_KPI_Orders;
CREATE TEMPORARY TABLE TMP_KPI_Orders
SELECT  
   MonthNum,
   CustomerNum,
   OrderNum,
   date
FROM
   A_Master
WHERE  
       MonthNum <= @KPI_Month
   AND Date > '2012-05-08'
   AND OrderBeforeCan = 1 
   AND Cancellations = 0 
   AND Rejected = 0
GROUP BY OrderNum
;

DROP TEMPORARY TABLE IF EXISTS TMP_KPI_Customers;
CREATE TEMPORARY TABLE TMP_KPI_Customers ( id int auto_increment, PRIMARY KEY ( CustomerNum, id ) )
SELECT  
   MonthNum AS MonthNum,
   CustomerNum,
   null as id,
   date
FROM
  TMP_KPI_Orders
ORDER BY OrderNum ASC, date ASC 
;

# of new customers (based on gross orders)
DROP TEMPORARY TABLE IF EXISTS TMP_KPI_Customers;
CREATE TEMPORARY TABLE TMP_KPI_Customers ( Id INT AUTO_INCREMENT, 
                                           PRIMARY KEY( CustomerNum, Id ) 
                                         )
SELECT
   CustomerNum,
   NULL AS Id,
   Date,
   Month_num,
   OrderNum,
   Rejected,
   Returns
FROM
   A_Master_Orders_Year  
WHERE
        OrderBeforeCan = 1
    AND Cancelled = 0
    AND Rejected  = 0
    AND Returns   = 0
ORDER BY CustomerNum,Date
;


REPLACE KPIs
SELECT 
  @KPI_Month              AS MonthNum,  
  @KPI_SpreadSheet        AS SpreadSheet,
  @KPI_Group              AS `Group`,   
  33                      AS RowId ,
  "of new customers (based on net orders post rejection and returns)" AS KPI,
  "Number of new customers with at least one valid purchase (based on net orders post rejections and returns)"    AS Description ,
  COUNT( DISTINCT CustomerNum ) AS Value,
  @KPI_UpdatedAt
FROM 
  TMP_KPI_Customers
WHERE  Month_Num = @KPI_Month
   AND id = 1
GROUP BY MonthNum
;


# of repeat customers (based on net orders post rejections)
REPLACE KPIs
SELECT 
  Month_Num                AS MonthNum,  
  @KPI_SpreadSheet         AS SpreadSheet,
  @KPI_Group              AS `Group`,   
  34                                               AS RowId ,
  "of repeat customers (based on net orders post rejections)" AS KPI,
  "Number of existing customers with at least one valid "
  "purchase in period (based on net orders post rejections)"    AS Description ,
  COUNT( DISTINCT CustomerNum ) AS Value,
  @KPI_UpdatedAt
FROM 
  TMP_KPI_Customers
WHERE  Month_Num = @KPI_Month
   AND id != 1
GROUP BY MonthNum
;

REPLACE KPIs
SELECT 
  @KPI_Month              AS MonthNum,  
  @KPI_SpreadSheet        AS SpreadSheet,
  @KPI_Group              AS `Group`,   
  36                                               AS RowId ,
  "Total customer accounts per end of month (#)" AS KPI,
  "Number of customers in the database with at least one valid purchase post rejections (cumulated)"    AS Description ,
  COUNT( DISTINCT CustomerNum ) AS Value,
  @KPI_UpdatedAt
FROM 
  TMP_KPI_Customers
WHERE  Month_Num <= @KPI_Month
GROUP BY MonthNum
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `KPI_ORDER_REVENUE_DRIVERS`(IN KPI_MonthNum INT)
BEGIN
/*
*  Kpi Group:  ORDER REVENUE DRIVERS
*  Created By: BI Team
*  Developer:  Carlos Antonio Mondragón Soria
*  Created at: 2013-10-03
*  Lastest Updated: 2013-10-03
*/
SET @KPI_Month=KPI_MonthNum;
SET @KPI_Year=SUBSTR( KPI_MonthNum FROM 1 FOR 4 );

SET @KPI_SpreadSheet = 1;
SET @KPI_UpdatedAt = Now();
SET @KPI_Group = "ORDER REVENUE DRIVERS";

/*
* SAMPLE TABLE
*/

/*
* KPI Calculations
*/ 
SET @index=576;
SET @indexIni=576;
SET @monthNumAct=0;
SET @KPI = "Active listings by category - ";
SET @KPI_Description = "Number of online listings at the end of the month by category";

REPLACE KPIs
SELECT
   MonthNum,
   SpreadSheet,
   `Group`,
   RowId,
   KPI,
   descripcion,
   Value ,
   KPI_UpdatedAt
FROM
(
SELECT
   @KPI_Month AS MonthNum,
   @KPI_SpreadSheet AS SpreadSheet, 
   @KPI_Group               AS `Group`,
   @index := IF( @monthNumAct != KPI.MonthNum , @indexIni ,
                 @index + 1 ) AS RowId,
   @monthNumAct := KPI.MonthNum ,
   CONCAT( @KPI , KPI.CatKPI )  AS KPI,
   @KPI_Description  AS descripcion,
   KPI.Value,
   @KPI_UpdatedAt AS KPI_UpdatedAt
FROM
(
   SELECT
      @KPI_Month AS MonthNum,
      M_CategoryKPI.CatKPI,
      COALESCE( TMP.Value, 0)  AS Value,
      M_CategoryKPI.KPIorder 
   FROM
      development_mx.M_CategoryKPI
      LEFT JOIN
      (
         SELECT 
            @KPI_Month AS MonthNum, 
            Cat_KPI AS CatKPI,
            COUNT( DISTINCT sku_config ) AS Value		 
        FROM 
           A_Master_Catalog
        WHERE
               isVisible     = 1
           AND isMarketPlace = 1

        GROUP BY MonthNum, CatKPI
      ) AS TMP 
      ON     TMP.CatKPI = development_mx.M_CategoryKPI.CatKPI
   GROUP BY MonthNum , CONCAT( @KPI , TMP.CatKPI )
   ORDER BY
      MonthNum, development_mx.M_CategoryKPI.KPIorder
) AS KPI
ORDER BY
   MonthNum, KPIorder
) AS Final
;

SET @index=592;
SET @indexIni=592;
SET @monthNumAct=0;
SET @KPI = "Gross valid merchandise orders by category - ";
SET @KPI_Description = "";

REPLACE KPIs
SELECT
   MonthNum,
   SpreadSheet,
   `Group`,
   RowId,
   KPI,
   descripcion,
   Value ,
   KPI_UpdatedAt
FROM
(
SELECT
   KPI.MonthNum,
   @KPI_SpreadSheet         AS SpreadSheet,
   @KPI_Group               AS `Group`,
   @index := IF( @monthNumAct != KPI.MonthNum , @indexIni ,
                 @index + 1 ) AS RowId,
   @monthNumAct := KPI.MonthNum ,
   CONCAT( @KPI , KPI.CatKPI )  AS KPI,
   @KPI_Description  AS descripcion,
   KPI.Value,
   @KPI_UpdatedAt AS KPI_UpdatedAt
FROM
(
   SELECT
      @KPI_Month AS MonthNum,
      M_CategoryKPI.CatKPI,
      COALESCE( TMP.Value, 0)  AS Value,
      M_CategoryKPI.KPIorder 
   FROM
      development_mx.M_CategoryKPI
      LEFT JOIN
      (
         SELECT 
            MonthNum, 
            CatKPI,
            COUNT( DISTINCT OrderNum ) AS Value		 
        FROM 
           A_Master
        WHERE
               OrderBeforeCan = 1
           AND isMPlace = 1
           AND MonthNum = @KPI_Month
        GROUP BY MonthNum, CatKPI
      ) AS TMP 
      ON     TMP.CatKPI = development_mx.M_CategoryKPI.CatKPI
   GROUP BY MonthNum , CONCAT( @KPI , TMP.CatKPI )
   ORDER BY
      MonthNum, development_mx.M_CategoryKPI.KPIorder
) AS KPI
ORDER BY
   MonthNum, KPIorder
) AS Final
;

SET @index=608;
SET @indexIni=608;
SET @monthNumAct=0;
SET @KPI = "Net merchandise orders by category - ";
SET @KPI_Description = "";

REPLACE KPIs
SELECT
   MonthNum,
   SpreadSheet,
   `Group`,
   RowId,
   KPI,
   descripcion,
   Value ,
   KPI_UpdatedAt
FROM
(
SELECT
   KPI.MonthNum,
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group               AS `Group`,
   @index := IF( @monthNumAct != KPI.MonthNum , @indexIni ,
                 @index + 1 ) AS RowId,
   @monthNumAct := KPI.MonthNum ,
   CONCAT( @KPI , KPI.CatKPI )  AS KPI,
   @KPI_Description  AS descripcion,
   KPI.Value,
   @KPI_UpdatedAt AS KPI_UpdatedAt
FROM
(
   SELECT
      @KPI_Month AS MonthNum,
      M_CategoryKPI.CatKPI,
      COALESCE( TMP.Value, 0)  AS Value,
      M_CategoryKPI.KPIorder 
   FROM
      development_mx.M_CategoryKPI
      LEFT JOIN
      (
         SELECT 
            MonthNum, 
            CatKPI,
            COUNT( DISTINCT OrderNum ) AS Value		 
        FROM 
           A_Master
        WHERE
               Cancellations  = 0
           AND OrderBeforeCan = 1
           AND isMPlace = 1
           AND MonthNum = @KPI_Month

        GROUP BY MonthNum, CatKPI
      ) AS TMP 
      ON     TMP.CatKPI = development_mx.M_CategoryKPI.CatKPI
   GROUP BY MonthNum , CONCAT( @KPI , TMP.CatKPI )
   ORDER BY
      MonthNum, development_mx.M_CategoryKPI.KPIorder
) AS KPI
ORDER BY
   MonthNum, KPIorder
) AS Final
;

SET @index=624;
SET @indexIni=624;
SET @monthNumAct=0;
SET @KPI = "Net merchandise revenues by category - ";
SET @KPI_Description = "";

REPLACE KPIs
SELECT
   MonthNum,
   SpreadSheet,
   `Group`,
   RowId,
   KPI,
   descripcion,
   Value ,
   KPI_UpdatedAt
FROM
(
SELECT
   KPI.MonthNum,
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group               AS `Group`,
   @index := IF( @monthNumAct != KPI.MonthNum , @indexIni ,
                 @index + 1 ) AS RowId,
   @monthNumAct := KPI.MonthNum ,
   CONCAT( @KPI , KPI.CatKPI )  AS KPI,
   @KPI_Description  AS descripcion,
   KPI.Value,
   @KPI_UpdatedAt AS KPI_UpdatedAt
FROM
(
   SELECT
      @KPI_Month AS MonthNum,
      M_CategoryKPI.CatKPI,
      COALESCE( TMP.Value, 0)  AS Value,
      M_CategoryKPI.KPIorder 
   FROM
      development_mx.M_CategoryKPI
      LEFT JOIN
      (
         SELECT 
            MonthNum AS MonthNum, 
            CatKPI,
            SUM( Rev ) / 1000 AS Value		 
        FROM 
           A_Master
        WHERE
               Cancellations  = 0
           AND Rejected       = 0        
           AND OrderBeforeCan = 1
           AND isMPlace = 1
           AND MonthNum = @KPI_Month

        GROUP BY MonthNum, CatKPI
      ) AS TMP 
      ON     TMP.CatKPI = development_mx.M_CategoryKPI.CatKPI
   GROUP BY MonthNum , CONCAT( @KPI , TMP.CatKPI )
   ORDER BY
      MonthNum, development_mx.M_CategoryKPI.KPIorder
) AS KPI
ORDER BY
   MonthNum, KPIorder
) AS Final
;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `KPI_PL_ACCOUNTING_VIEW_NOT_CASH_VIEW`(IN KPI_MonthNum INT)
BEGIN
/*
*  Kpi Group:  NET PROMOTER SCORE
*  Created By: BI Team
*  Developer:  Eduardo Martinez
*  Created at: 2013-11-01
*  Lastest Updated: 2013-11-01
*/

/*
*  Global Variables
*/
SET @KPI_Month=KPI_MonthNum;
SET @KPI_Year=SUBSTR( KPI_MonthNum FROM 1 FOR 4 );

SET @KPI_UpdatedAt = Now();
SET @KPI_SpreadSheet = 1;
SET @KPI_Group='P&L (ACCOUNTING VIEW - NOT CASH VIEW)';


#Row Id - 262
#Net revenue from merchandise (in k local)
#Net sales as per accounting rules (usually sales recognition as per shipped order or, for COD, upon proof of delivery) after VAT, discounts, coupons, returns and cancelations; includes provision for returns
REPLACE KPIs
select 
   a.Yearmonth               AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   262                       AS RowId ,
   "Net revenue from merchandise (in k local)" AS KPI,
   "Net sales as per accounting rules (usually "
   "sales recognition as per shipped order or, "
   "for COD, upon proof of delivery) after VAT,"
   " discounts, coupons, returns and cancelations; "
   "includes provision for returns"  AS Description ,
   COALESCE(a.DelSales,0) - COALESCE(b.RetSales,0)   AS Value,
   @KPI_UpdatedAt
#from (select date_format(DateDelivered,'%Y%m') as 'Yearmonth', sum(PriceAfterTax - CouponValueAfterTax)/1000 as 'DelSales'
from (select date_format(DateDelivered,'%Y%m') as 'Yearmonth', sum(Rev-ShippingFee-Interest)/1000 as 'DelSales'

			from A_Master
			where DateDelivered >= 20130101
				#and Delivered = 1
            and isMPlace = 0
            and not (    OrderNum in   ( SELECT OrderNum FROM M_CorporativeOrders ) 
                      OR PrefixCode in ( "CDEAL" ) 
                      OR OrderNum in  ( SELECT order_nr FROM bob_live_pe.sales_order WHERE assisted_sales_operator  like '%ventascorporativas%'  ) 
                    )
            and date_format(DateDelivered,'%Y%m') <= @KPI_Month
			group by date_format(DateDelivered,'%Y%m')
			order by date_format(DateDelivered,'%Y%m') asc) a
#left join (select date_format(DateReturned,'%Y%m') as 'Yearmonth', sum(PriceAfterTax - CouponValueAfterTax)/1000 as 'RetSales'
left join (select date_format(DateReturned,'%Y%m') as 'Yearmonth', sum(Rev-ShippingFee-Interest)/1000 as 'RetSales'

						from A_Master
						where     DateReturned >= 20130101
                  and isMPlace = 0
                  and not (    OrderNum in   ( SELECT OrderNum FROM M_CorporativeOrders ) 
                            OR PrefixCode in ( "CDEAL" ) 
                            OR OrderNum in  ( SELECT order_nr FROM bob_live_pe.sales_order WHERE assisted_sales_operator  like '%ventascorporativas%'  ) 
                          )
                  and date_format(DateReturned,'%Y%m') <= @KPI_Month
							#and Returns = 1
						group by date_format(DateReturned,'%Y%m')
						order by date_format(DateReturned,'%Y%m') asc) b
on a.Yearmonth = b.Yearmonth
where a.Yearmonth <> (select Month_Num									
											from Out_SalesReportOrder
											order by Month_Num desc
											limit 1)
;

#Row 263
#Net revenue from B2B merchandise sales (in k local) 
#Net sales as per accounting rules (usually sales recognition 
#as per shipped order or, for COD, upon proof of delivery); after VAT, discounts, coupons, 
#returns and cancelations; includes provision for returns
REPLACE KPIs
select 
   a.Yearmonth               AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   263                       AS RowId ,
   "Net revenue from B2B merchandise sales (in k local) " AS KPI,
   "Net sales as per accounting rules (usually sales recognition "
   "as per shipped order or, for COD, upon proof of delivery); after VAT, discounts, coupons, "
   "returns and cancelations; includes provision for returns" AS Description ,
   COALESCE(a.DelSales,0) - COALESCE(b.RetSales,0)   AS Value,
   @KPI_UpdatedAt
#from (select date_format(DateDelivered,'%Y%m') as 'Yearmonth', sum(PriceAfterTax - CouponValueAfterTax)/1000 as 'DelSales'
from (select date_format(DateDelivered,'%Y%m') as 'Yearmonth', sum(Rev-ShippingFee-Interest)/1000 as 'DelSales'
			from A_Master
			where DateDelivered >= 20130101
				#and Delivered = 1
            and (    OrderNum in   ( SELECT OrderNum FROM M_CorporativeOrders ) 
                  OR PrefixCode in ( "CDEAL" ) 
                  OR OrderNum in  ( SELECT order_nr FROM bob_live_pe.sales_order WHERE assisted_sales_operator  like '%ventascorporativas%'  ) 
                )
            and date_format(DateDelivered,'%Y%m') <= @KPI_Month
			group by date_format(DateDelivered,'%Y%m')
			order by date_format(DateDelivered,'%Y%m') asc) a
#left join (select date_format(DateReturned,'%Y%m') as 'Yearmonth', sum(PriceAfterTax - CouponValueAfterTax)/1000 as 'RetSales'
left join (select date_format(DateReturned,'%Y%m') as 'Yearmonth', sum(Rev-ShippingFee-Interest)/1000 as 'RetSales'
						from A_Master
						where DateReturned >= 20130101
							#and Returns = 1
                  and (    OrderNum in   ( SELECT OrderNum FROM M_CorporativeOrders ) 
                            OR PrefixCode in ( "CDEAL" ) 
                            OR OrderNum in  ( SELECT order_nr FROM bob_live_pe.sales_order WHERE assisted_sales_operator  like '%ventascorporativas%'  ) 
                      )
                  and date_format(DateReturned,'%Y%m') <= @KPI_Month <= @KPI_Month
						group by date_format(DateReturned,'%Y%m')
						order by date_format(DateReturned,'%Y%m') asc) b
on a.Yearmonth = b.Yearmonth
where a.Yearmonth <> (select Month_Num									
											from Out_SalesReportOrder
											order by Month_Num desc
											limit 1)
;

#Row Id - 265
#Shipping net revenues (in k local)
#Revenues derived from charging customers for shipping
REPLACE KPIs
select 
   a.Yearmonth               AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   265                       AS RowId ,
   "Shipping net revenues (in k local)" AS KPI,
   "Shipping net revenues (in k local)"
   "Revenues derived from charging customers for shipping" AS Description ,
   COALESCE(a.DelShipRev ,0) - COALESCE(b.RetShipRev,0)   AS Value,
   @KPI_UpdatedAt
  
from (select date_format(DateDelivered,'%Y%m') as 'Yearmonth', sum(ShippingFee)/1000 as 'DelShipRev'
			from A_Master
			where DateDelivered >= 20130101
				    and Delivered = 1
            and date_format(DateDelivered,'%Y%m') <= @KPI_Month
			group by date_format(DateDelivered,'%Y%m')
			order by date_format(DateDelivered,'%Y%m') asc) a
left join (select date_format(DateReturned,'%Y%m') as 'Yearmonth', sum(ShippingFee)/1000 as 'RetShipRev'
						from A_Master
						where DateReturned >= 20130101
							and `Returns` = 1
                  and date_format(DateReturned,'%Y%m') <= @KPI_Month
						group by date_format(DateReturned,'%Y%m')
						order by date_format(DateReturned,'%Y%m') asc) b
on a.Yearmonth = b.Yearmonth
where a.Yearmonth <> (select Month_Num									
											from Out_SalesReportOrder
											order by Month_Num desc
											limit 1)
;

#Row Id - 267
#Revenues derived from charging customers for shipping
SELECT
   a.Yearmonth               AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   267                       AS RowId ,
   "Net venture marketplace revenues (in k local)" AS KPI,
   "Net merchandise revenues * commission" AS Description,
   a.DelSales AS Value,
   @KPI_UpdatedAt
from 		(
		select date_format(DateDelivered,'%Y%m') as 'Yearmonth', 
		sum( Price*MPlaceFee )/1000 as 'DelSales'
		from A_Master
		where	date_format(DateDelivered,'%Y%m') = @KPI_MonthNum
    and 	isMPlace = 1
		AND		OrderAfterCan = 1
		group by date_format(DateDelivered,'%Y%m')) a
# Condición B2B
;

#Row Id - 270
#Net financial revenue (in k local)
#Revenue from sale of financing solutions to customers (also if in cooperation with financial institution)
REPLACE KPIs
select 
   a.Yearmonth               AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   270                       AS RowId ,
   "Shipping net revenues (in k local)" AS KPI,
   "Shipping net revenues (in k local)"
   "Revenues derived from charging customers for shipping" AS Description ,
   COALESCE(a.DelInterest,0) - COALESCE(b.RetInterest,0) as Value,
   @KPI_UpdatedAt
from (select date_format(DateDelivered,'%Y%m') as 'Yearmonth', sum(Interest)/1000 as 'DelInterest'
			from A_Master
			where DateDelivered >= 20130101
				and Delivered = 1
            and date_format(DateDelivered,'%Y%m') <= @KPI_Month
			group by date_format(DateDelivered,'%Y%m')
			order by date_format(DateDelivered,'%Y%m') asc) a
left join (select date_format(DateReturned,'%Y%m') as 'Yearmonth', sum(Interest)/1000 as 'RetInterest'
						from A_Master
						where DateReturned >= 20130101
							and Returns = 1
              and date_format(DateReturned,'%Y%m') <= @KPI_Month
						group by date_format(DateReturned,'%Y%m')
						order by date_format(DateReturned,'%Y%m') asc) b
on a.Yearmonth = b.Yearmonth
where a.Yearmonth <> (select Month_Num									
											from Out_SalesReportOrder
											order by Month_Num desc
											limit 1)
;

#Row Id - 273
#Basic COGS
#Cost of Goods Sold (net of returns) - reductions in purchase price + cost of inbound logistics if possible, FIFO
REPLACE KPIs
SELECT
   a.Yearmonth               AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   273                       AS RowId ,
   "Basic COGS"              AS KPI,

   "Cost of Goods Sold (net of returns) - "
   "reductions in purchase price + cost of "
   "inbound logistics if possible, FIFO   " AS Description,
   ( COALESCE(a.DelCOGS,0) - COALESCE(b.RetCOGS,0)  )*-1 as Value,
   @KPI_UpdatedAt
from (select date_format(DateDelivered,'%Y%m') as 'Yearmonth', sum(COGS)/1000 as 'DelCOGS'
			from A_Master
			where DateDelivered >= 20130101
				    and Delivered = 1
            and isMPlace = 0
            and date_format(DateDelivered,'%Y%m') <= @KPI_Month
			group by date_format(DateDelivered,'%Y%m')
			order by date_format(DateDelivered,'%Y%m') asc) a
left join (select date_format(DateReturned,'%Y%m') as 'Yearmonth', sum(COGS)/1000 as 'RetCOGS'
						from A_Master
						where DateReturned >= 20130101
							and Returns = 1
              and isMPlace = 0
              and date_format(DateReturned,'%Y%m') <= @KPI_Month
						group by date_format(DateReturned,'%Y%m')
						order by date_format(DateReturned,'%Y%m') asc) b
on a.Yearmonth = b.Yearmonth
where a.Yearmonth <> (select Month_Num									
											from Out_SalesReportOrder
											order by Month_Num desc
											limit 1)
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `KPI_REVENUE_WATERFALL_INCL_MARKETPLACE`(IN KPI_MonthNum INT)
BEGIN
/*
*  Kpi Group:  REVENUE WATERFALL INCL MARKETPLACE
*  Created By: BI Team
*  Developer:  Carlos Antonio Mondragón Soria
*  Created at: 2013-10-03
*  Lastest Updated: 2014-4-21
*/
SET @KPI_Month=KPI_MonthNum;
SET @KPI_Year=SUBSTR( KPI_MonthNum FROM 1 FOR 4 );

SET @KPI_SpreadSheet = 1;
SET @KPI_UpdatedAt = Now();
SET @KPI_Group = "REVENUE WATERFALL INCL MARKETPLACE";

/*
* SAMPLE TABLES
*/
DROP TEMPORARY TABLE IF EXISTS A_Master_Year;
CREATE TEMPORARY TABLE A_Master_Year ( INDEX( ItemId ) )
SELECT * FROM development_pe.A_Master
WHERE YEAR( date ) = @KPI_Year;

/*
*  KPIs Calculation
*/

# Gross revenue before validation (in k local)
REPLACE KPIs
SELECT 
   A_Master_Year.MonthNum    AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   240         AS RowID,
   "Gross revenue before validation (in k local)" AS KPI,
   "Revenue of all orders received in BOB (before VAT); including marketplace" AS Description,
   sum(OriginalPrice)/1000 AS Value,
   @KPI_UpdatedAt
FROM 
   A_Master_Year
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;


# VAT (or similar) (in k local)
REPLACE KPIs
SELECT 
   A_Master_Year.MonthNum    AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   241         AS RowID,
   "VAT (or similar) (in k local)" AS KPI,
   "VAT (or similar)" AS Description,
   sum(OriginalPrice*(TaxPercent/(100+TaxPercent)))/-1000 AS Value,
   @KPI_UpdatedAt
FROM 
   A_Master_Year
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;

#Invalid net revenue (in k local)
REPLACE KPIs
SELECT 
   A_Master_Year.MonthNum    AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   243         AS RowID,
   "Invalid net revenue (in k local)" AS KPI,
   "Revenue lost due to invalid orders (after VAT); including marketplace" AS Description,
   sum( IF( OrderBeforeCan = 0 , OriginalPrice/((100+TaxPercent)/100), 0 ) )/-1000 AS Value,
   @KPI_UpdatedAt
FROM 
   A_Master_Year
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;

#Cancelation (in k local)
REPLACE KPIs
SELECT 
   A_Master_Year.MonthNum    AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   245         AS RowID,
   "Cancelation (in k local)" AS KPI,
   "Revenue lost due to cancelations "
   "(initiated by customer and internally, "
   "for instance payslip system in some LatAm "
   "markets) (after VAT); including marketplace" AS Description,
   SUM(IF( 	OrderBeforeCan = 1 AND Cancelled  = 1, OriginalPrice/((100+TaxPercent)/100) , 0 ) ) / -1000 AS Value,
   @KPI_UpdatedAt
FROM 
   A_Master_Year
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;

# Discounts (in k local)
REPLACE KPIs
SELECT 
   A_Master_Year.MonthNum    AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   247         AS RowID,
   "Discounts (in k local)" AS KPI,
   "On-site discounts on the sales price (after VAT if applicable); including marketplace" AS Description,
   -1 * ( SUM(if(OrderBeforeCan = 1 AND Cancelled = 0, OriginalPrice/((100+TaxPercent)/100)- PriceAfterTax,0))/1000) AS Value,
   @KPI_UpdatedAt
FROM 
   A_Master_Year
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;



#249 Coupons (in k local)
REPLACE KPIs
SELECT 
   A_Master_Year.MonthNum    AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   249         AS RowID,
   "Coupons (in k local)" AS KPI,
   "Revenue lost due to coupons and redemptions "
   "(after VAT if applicable); including marketplace"
   AS Description,
   SUM(IF( OrderBeforeCan = 1 AND NOT ( Cancellations  = 1 AND Rejected = 0) , CouponValueAfterTax, 0 ) ) / -1000 AS Value,
   @KPI_UpdatedAt
FROM 
   A_Master_Year
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;

# Special discounts

# Rejections (in k local)
REPLACE KPIs
SELECT 
   A_Master_Year.MonthNum    AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   253         AS RowID,
   "Rejections (in k local)" AS KPI,
   "Revenue lost due to rejections /"
   "refusals by customer (customer cannot "
   "be reached by the carrier) (after VAT); "
   "including marketplace" AS Description,
   SUM(IF( OrderBeforeCan = 1 AND NOT ( Cancellations  = 1 AND Rejected = 0) AND Rejected = 1 , PriceAfterTax - CouponValueAfterTax , 0 ) ) / -1000 AS Value,
   @KPI_UpdatedAt
FROM 
   A_Master_Year
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;

#255 Returns (in k local)
REPLACE KPIs
SELECT 
   A_Master_Year.MonthNum    AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   255         AS RowID,
   "Returns (in k local)" AS KPI,
   "Revenue lost due to customer returns (after VAT); "
   "including marketplace" AS Description,
   #SUM(IF(     OrderBeforeCan = 1
	 #        and Cancelled = 0
	 #        and Rejected = 0
	 #        and `Returns` = 1
   #        , PriceAfterTax - CouponValueAfterTax , 0 ) ) / -1000 AS Value,
   SUM(IF( OrderBeforeCan = 1 AND NOT ( Cancellations  = 1 AND Rejected = 0) AND Rejected = 0
	         and `Returns`     = 1
           , PriceAfterTax - CouponValueAfterTax , 0 ) ) / -1000 AS Value,

   @KPI_UpdatedAt
FROM 
   A_Master_Year
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `KPI_VOLUME_DRIVERS_INCL_MARKETPLACE`( IN KPI_MonthNum INT )
BEGIN
/*
*  Kpi Group:  VOLUME DRIVERS - INCL. MARKETPLACE
*  Created By: BI Team
*  Developer:  Carlos Antonio Mondragón Soria
*  Created at: 2013-10-03
*  Lastest Updated: 2013-10-03
*/
SET @KPI_Month=KPI_MonthNum;
SET @KPI_Year=SUBSTR( KPI_MonthNum FROM 1 FOR 4 );
#SET @KPI_Month=201309;
#SET @KPI_Year=SUBSTR( 201309 FROM 1 FOR 4 );

SET @KPI_UpdatedAt = Now();
SET @KPI_SpreadSheet = 1;
SET @KPI_Group = "VOLUME DRIVERS - INCL. MARKETPLACE";

/*
* Sample Table Creations
*/
DROP TEMPORARY TABLE IF EXISTS A_Master_Year;
CREATE TEMPORARY TABLE A_Master_Year ( INDEX( ItemId ) )
SELECT * FROM A_Master
WHERE
   YEAR( date ) = @KPI_Year;

DROP TEMPORARY TABLE IF EXISTS A_Master_Orders_Year;
CREATE TEMPORARY TABLE A_Master_Orders_Year 
SELECT * FROM Out_SalesReportOrder
WHERE
   YEAR( date ) = @KPI_Year;

/*
* KPIs calculations
*/

# of gross items per category
SET @index=40;
SET @indexIni=40;
SET @monthNumAct=0;
SET @KPI = "# of gross items per category - ";
SET @KPI_Description = "Gross items as per gross orders; easier to pull out "
   "from BOB / DWH; should correlate with net items (after "
   "cancelations and returns)";

REPLACE KPIs
SELECT
   MonthNum,
   SpreadSheet,
   `Group`,
   RowId,
   KPI,
   descripcion,
   Value,
   KPI_UpdatedAt
FROM
(
SELECT
   KPI.MonthNum,
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group               AS `Group`,
   @index := IF( @monthNumAct != KPI.MonthNum , @indexIni ,
                 @index + 1 ) AS RowId,
   @monthNumAct := KPI.MonthNum ,
   CONCAT( @KPI , KPI.CatKPI )  AS KPI,
   @KPI_Description  AS descripcion,
   KPI.Value,
   @KPI_UpdatedAt AS KPI_UpdatedAt
FROM
(
   SELECT
      TMP.MonthNum,
      TMP.CatKPI,
      TMP.Value,
      KPIorder 
   FROM
      development_mx.M_CategoryKPI
      INNER JOIN
      (
         SELECT
            MonthNum,
            CatKPI,
            SUM( IF( OrderBeforeCan = 1 , 1 , 0 ) ) AS Value
         FROM 
            A_Master_Year
         GROUP BY MonthNum, CatKPI

      ) AS TMP 
      ON     TMP.CatKPI = development_mx.M_CategoryKPI.CatKPI
   GROUP BY MonthNum , CONCAT( @KPI , TMP.CatKPI )
   ORDER BY
      MonthNum, development_mx.M_CategoryKPI.KPIorder
) AS KPI
ORDER BY
   MonthNum, KPIorder
) AS Final
;

#Revenue per category (in k local) - 
SET @index=59;
SET @indexIni=59;
SET @monthNumAct=0;
SET @KPI = "Revenue per category (in k local) - ";
SET @KPI_Description = "Gross revenues before cancelations "
                       "(DWH definition: Net Revenue before cancelations excl. VAT)";

REPLACE KPIs
SELECT
   MonthNum,
   SpreadSheet,
   `Group`,
   RowId,
   KPI,
   descripcion,
   Value / 1000,
   KPI_UpdatedAt
FROM
(
SELECT
   KPI.MonthNum,
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group               AS `Group`,
   @index := IF( @monthNumAct != KPI.MonthNum , @indexIni ,
                 @index + 1 ) AS RowId,
   @monthNumAct := KPI.MonthNum ,
   CONCAT( @KPI , KPI.CatKPI )  AS KPI,
   @KPI_Description  AS descripcion,
   KPI.Value,
   @KPI_UpdatedAt AS KPI_UpdatedAt
FROM
(
   SELECT
      TMP.MonthNum,
      TMP.CatKPI,
      TMP.Value,
      KPIorder 
   FROM
      development_mx.M_CategoryKPI
      INNER JOIN
      (
         SELECT
            MonthNum,
            CatKPI,
            SUM( IF( OrderBeforeCan = 1 , Rev , 0 ) ) AS Value
         FROM 
            A_Master_Year
         GROUP BY MonthNum, CatKPI

      ) AS TMP 
      ON     TMP.CatKPI = development_mx.M_CategoryKPI.CatKPI
   GROUP BY MonthNum , CONCAT( @KPI , TMP.CatKPI )
   ORDER BY
      MonthNum, development_mx.M_CategoryKPI.KPIorder
) AS KPI
ORDER BY
   MonthNum, KPIorder
) AS Final
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:03:57
