-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: test_marketing
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`lorena.ramirez`@`%`*/ /*!50003 TRIGGER ins_sum
    AFTER UPDATE ON foo
    FOR EACH ROW
    UPDATE foo SET campaign_query =
							(if(new.campaign_like = 'starts', concat(NEW.campaign,'%'),
							 if(new.campaign_like = 'contains', concat('%', NEW.campaign,'%'),
								concat('%', NEW.campaign)))) */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping routines for database 'test_marketing'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `global_report`()
begin

set @days:=1;

set @freeze:=1;

set @channel_marketing:=0;

select  'Marketing Report: start',now();

delete from marketing_report.global_report where datediff(curdate(), date)<@days;

-- Mexico

update production.tbl_order_detail t inner join development_mx.A_Master o on t.item=o.itemID set t.unit_price_after_vat=o.priceaftertax, t.coupon_money_after_vat=o.couponvalueaftertax, t.shipping_fee_after_vat=o.shippingfee, t.costo_after_vat=o.costaftertax, t.delivery_cost_supplier=o.deliverycostsupplier, t.payment_cost=o.paymentfees, t.shipping_cost_per_sku=o.shippingcost, t.wh=o.flwhcost, t.cs=o.flcscost, t.oac=o.orderaftercan, t.obc=o.orderbeforecan, t.returned=o.returns, t.pending=o.pending, t.cancel=o.cancellations where datediff(curdate(), t.date)<@freeze;

insert into marketing_report.global_report(country, yrmonth, week, date, weekday, channel_group, channel, gross_orders, net_orders, gross_revenue, net_revenue, PC1, `PC1.5`, PC2, new_customers_gross, new_customers) select 'Mexico', gross.yrmonth, gross.week, gross.date, gross.weekday, gross.channel_group, gross.channel, gross.gross_orders, net.net_orders, gross.gross_revenue, net.net_revenue, net.PC1, net.`PC1.5`, net.PC2, gross.new_customers_gross, net.new_customers from (select yrmonth, production.week_iso(date) as week, date, case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end as weekday,  channel_group, channel, count(distinct order_nr) as gross_orders, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as gross_revenue, sum(new_customers_gross) as new_customers_gross from production.tbl_order_detail where obc=1 group by yrmonth, production.week_iso(date), date, channel_group, channel)gross left join (select yrmonth, production.week_iso(date) as week, date, channel_group, channel, count(distinct order_nr) as net_orders, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)) as PC1, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(payment_cost,0)-ifnull(shipping_cost_per_sku,0)) as `PC1.5`, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(payment_cost,0)-ifnull(shipping_cost_per_sku,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2, sum(new_customers) as new_customers from production.tbl_order_detail where oac=1 group by yrmonth, production.week_iso(date), date, channel_group, channel)net on gross.date=net.date and gross.channel=net.channel and gross.channel_group=net.channel_group where datediff(curdate(), gross.date)<@days order by date desc;

-- Pending Rev

update marketing_report.global_report g set pending_revenue = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production.tbl_order_detail t where t.pending=1 and g.date=t.date and g.channel_group=t.channel_group and g.channel=t.channel) where country='Mexico';

-- Canceled Rev

update marketing_report.global_report g set canceled_revenue = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production.tbl_order_detail t where t.cancel=1 and g.date=t.date and g.channel_group=t.channel_group and g.channel=t.channel) where country='Mexico';

-- Visits

if @channel_marketing=1 then update SEM.campaign_ad_group_mx set source_medium=concat(source,' / ',medium);
update SEM.campaign_ad_group_mx set channel=null, channel_group=null;
call production.channel_marketing;
end if;

create table marketing_report.temporary(
date date,
channel varchar(255),
channel_group varchar(255),
include int
);

insert into marketing_report.temporary(date, channel_group, channel) select distinct date, channel_group, channel from SEM.campaign_ad_group_mx where datediff(curdate(), date)<@days;

update marketing_report.temporary t inner join marketing_report.global_report g on t.channel_group=g.channel_group and t.channel=g.channel and t.date=g.date set include = 1 where country='Mexico' and datediff(curdate(), g.date)<@days;

insert into marketing_report.global_report(country, date, channel_group, channel) select 'Mexico', date, channel_group, channel from marketing_report.temporary where include is null;

update marketing_report.global_report g set visits = (select sum(visits) from SEM.campaign_ad_group_mx c where c.channel_group=g.channel_group and c.channel=g.channel and c.date=g.date) where country='Mexico' and datediff(curdate(), g.date)<@days;

-- Cost

update marketing_report.global_report g set visits = (select sum(visits) from production.mobileapp_campaign c where c.date=g.date) where channel_group='Mobile App' and channel='Mobile App' and country='Mexico' and datediff(curdate(), g.date)<@days;

##update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_mx r where channel='Criteo' and g.date=r.date) where channel='Criteo' and country='Mexico' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(cost) from marketing_report.criteo_mx r where g.date=r.date) where channel='Criteo' and country='Mexico' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(cost) from facebook.facebook_optimization_region_mx r where g.date=r.date) where channel='Facebook Ads' and country='Mexico' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_mx r where channel='GLG' and g.date=r.date) where channel='Glg' and country='Mexico' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(cost) from SEM.google_optimization_ad_group_mx r where channel='GDN' and g.date=r.date) where channel='Google Display Network' and country='Mexico' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_mx r where channel='Pampa Network' and g.date=r.date) where channel='Pampa Network' and country='Mexico' and datediff(curdate(), g.date)<@days;  

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_mx r where channel='Main ADV' and g.date=r.date) where channel='Main ADV' and country='Mexico' and datediff(curdate(), g.date)<@days;  

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_mx r where channel='My Things' and g.date=r.date) where channel='My Things' and country='Mexico' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_mx r where channel='Avazu' and g.date=r.date) where channel='Avazu' and country='Mexico' and datediff(curdate(), g.date)<@days;  

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_mx r where channel='Trade Tracker' and g.date=r.date) where channel='Trade Tracker' and country='Mexico' and datediff(curdate(), g.date)<@days;  

##update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_mx r where channel='FBX' and g.date=r.date) where channel='Facebook R.' and country='Mexico' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(l.cost)+sum(r.cost) from marketing_report.sociomantic_fbx_loyalty_mx l, marketing_report.sociomantic_fbx_retargeting_mx r where l.date=r.date and g.date=r.date) where channel='Facebook R.' and country='Mexico' and datediff(curdate(), g.date)<@days;  

update marketing_report.global_report g set retention_cost = (select sum(l.cost) from marketing_report.sociomantic_fbx_loyalty_mx l where l.date=g.date) where channel='Facebook R.' and country='Mexico' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set acquisition_cost = (select sum(r.cost) from marketing_report.sociomantic_fbx_retargeting_mx r where r.date=g.date) where channel='Facebook R.' and country='Mexico' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(cost) from SEM.google_optimization_ad_group_mx r where channel='SEM' and g.date=r.date) where channel='SEM' and country='Mexico' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(ad_cost) from SEM.campaign_ad_group_mx r where source = 'google' and medium = 'CPC' and campaign like 'brandb%' and g.date=r.date) where channel='SEM Branded' and country='Mexico' and datediff(curdate(), g.date)<@days;

##update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_mx r where channel='Sociomantic' and g.date=r.date) where channel='Sociomantic' and country='Mexico' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(l.cost)+sum(r.cost) from marketing_report.sociomantic_loyalty_mx l, marketing_report.sociomantic_basket_mx r where l.date=r.date and g.date=r.date) where channel='Sociomantic' and country='Mexico' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set retention_cost = (select sum(l.cost) from marketing_report.sociomantic_loyalty_mx l where l.date=g.date) where channel='Sociomantic' and country='Mexico' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set acquisition_cost = (select sum(r.cost) from marketing_report.sociomantic_basket_mx r where g.date=r.date) where channel='Sociomantic' and country='Mexico' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_mx r where channel='Sociomantic' and g.date=r.date) where channel='Sociomantic' and country='Mexico' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_mx r where channel='Veinteractive' and g.date=r.date) where channel='Newsletter' and country='Mexico' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_mx r where channel='Vizury' and g.date=r.date) where channel='Vizury' and country='Mexico' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from facebook.fanpage_optimization_mx r where g.date=r.date) where channel='FB Posts' and country='Mexico' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(spent) from facebook.facebook_campaign r where g.date=r.date and campaign like 'MEX_darkpost%') where channel_group='Facebook Ads' and  channel='Dark Post' and country='Mexico' and datediff(curdate(), g.date)<@days;

create table marketing_report.temporary_offline(
date date,
count int
);

insert into marketing_report.temporary_offline select date, count(distinct channel) from marketing_report.global_report where channel_group='Offline Marketing' and country='Mexico' and datediff(curdate(), date)<@days group by date;

update marketing_report.global_report g set cost = (select sum(cost)/count from marketing_report.offline_mx t, marketing_report.temporary_offline r where g.date=r.date and t.date=r.date) where channel_group='Offline Marketing' and country='Mexico' and datediff(curdate(), g.date)<@days;

drop table marketing_report.temporary_offline;

create table marketing_report.temporary_tele_sales(
date date,
count int
);

insert into marketing_report.temporary_tele_sales select date, count(distinct channel) from marketing_report.global_report where channel_group='Tele Sales' and country='Mexico' and datediff(curdate(), date)<@days group by date;

update marketing_report.global_report g set cost = (select sum(cost)/count from marketing_report.tele_sales_mx t, marketing_report.temporary_tele_sales r where g.date=r.date and t.date=r.date) where channel_group='Tele Sales' and datediff(curdate(), g.date)<@days and country='Mexico';

drop table marketing_report.temporary_tele_sales;

-- PC3

update marketing_report.global_report set PC3 = PC2-cost where country='Mexico';

-- Moving Avg

create table marketing_report.temporary_global_report(
date date,
channel_group varchar(255),
channel varchar(255),
gross_orders int not null default 0,
net_orders int not null default 0,
e_orders float not null default 0,
ee_orders float not null default 0,
gross_revenue float not null default 0,
net_revenue float not null default 0,
e_revenue float not null default 0,
ee_revenue float not null default 0,
new_customers_gross int not null default 0,
new_customers int not null default 0,
e_new_customers float not null default 0,
ee_new_customers float not null default 0,
cancel float,
returns float,
voucher float
);

create index temporary_mkt on marketing_report.temporary_global_report(date, channel_group, channel);

insert into marketing_report.temporary_global_report(date, channel_group, channel, gross_orders, net_orders, gross_revenue, net_revenue, new_customers_gross, new_customers) select date, channel_group, channel, gross_orders, net_orders, gross_revenue, net_revenue, new_customers_gross, new_customers from marketing_report.global_report where country='Mexico' and datediff(curdate(), date)<@days;

-- Orders

set @date=date_sub(curdate(), interval @days day);

while @date!=curdate() do
update marketing_report.temporary_global_report t set e_orders = (select (avg(g.net_orders/g.gross_orders)) from marketing_report.global_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Mexico') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_global_report t set e_orders = gross_orders*e_orders where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_orders = case when e_orders>net_orders then e_orders else net_orders end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set cancel = (select count(distinct order_nr) from production.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set returns = (select count(distinct order_nr) from production.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.returned=1) where datediff(curdate(), date)<@days; 

update marketing_report.temporary_global_report t set cancel = cancel/gross_orders, returns = returns/gross_orders where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set ee_orders = gross_orders*(1-ifnull(cancel,0)-ifnull(returns,0)) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_orders = case when e_orders>ee_orders then ee_orders else e_orders end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_orders = case when e_orders>net_orders then e_orders else net_orders end where datediff(curdate(), date)<@days;

-- Revenue

set @date=date_sub(curdate(), interval @days day);

while @date!=curdate() do
update marketing_report.temporary_global_report t set e_revenue = (select (avg(g.net_revenue/g.gross_revenue)) from marketing_report.global_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Mexico') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_global_report t set e_revenue = gross_revenue*e_revenue where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_revenue = case when e_revenue>net_revenue then e_revenue else net_revenue end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set cancel = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set returns = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.returned=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set voucher = (select sum(coupon_money_after_vat) from production.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.coupon_code is not null and d.oac=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set cancel = cancel/gross_revenue, returns = returns/gross_revenue, voucher=voucher/gross_revenue where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set ee_revenue = gross_revenue*(1-ifnull(cancel,0)-ifnull(returns,0)-ifnull(voucher,0)) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_revenue = case when e_revenue>ee_revenue then ee_revenue else e_revenue end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_revenue = case when e_revenue>net_revenue then e_revenue else net_revenue end where datediff(curdate(), date)<@days;

-- New Customers

set @date=date_sub(curdate(), interval @days day);

while @date!=curdate() do
update marketing_report.temporary_global_report t set e_new_customers = (select (avg(g.new_customers/g.new_customers_gross)) from marketing_report.global_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Mexico') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_global_report t set e_new_customers = new_customers_gross*e_new_customers where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_new_customers = case when e_new_customers>new_customers then e_new_customers else new_customers end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set cancel = (select sum(new_customers_gross) from production.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set returns = (select sum(new_customers_gross) from production.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.returned=1) where datediff(curdate(), date)<@days; 

update marketing_report.temporary_global_report t set cancel = cancel/new_customers_gross, returns = returns/new_customers_gross where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set ee_new_customers = new_customers_gross*(1-ifnull(cancel,0)-ifnull(returns,0)) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_new_customers = case when e_new_customers>ee_new_customers then ee_new_customers else e_new_customers end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_new_customers = case when e_new_customers>new_customers then e_new_customers else new_customers end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t inner join marketing_report.global_report g on t.date=g.date and t.channel_group=g.channel_group and t.channel=g.channel set g.e_orders=t.e_orders, g.e_revenue=t.e_revenue, g.e_new_customers=t.e_new_customers where country = 'Mexico' and datediff(curdate(), t.date)<@days;

-- Exchange Rate

update marketing_report.global_report s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, e_revenue=e_revenue/xr, a_revenue=a_revenue/xr, PC1=PC1/xr, `PC1.5`=`PC1.5`/xr, PC2=PC2/xr, PC3=PC3/xr, pending_revenue=pending_revenue/xr, canceled_revenue=canceled_revenue/xr where s.country = 'Mexico' and r.country='MEX' and datediff(curdate(), s.date)<@days; 

update marketing_report.global_report set cost = cost/16.35 where channel= 'SEM Branded' and country = 'Mexico' and datediff(curdate(), date)<@days;

update marketing_report.global_report set cost = cost*1.23 where channel= 'Criteo' and country = 'Mexico' and datediff(curdate(), date)<@days;

-- Colombia

insert into marketing_report.global_report(country, yrmonth, week, date, weekday, channel_group, channel, gross_orders, net_orders, gross_revenue, net_revenue, PC1, `PC1.5`, PC2, new_customers_gross, new_customers) select 'Colombia', gross.yrmonth, gross.week, gross.date, gross.weekday, gross.channel_group, gross.channel, gross.gross_orders, net.net_orders, gross.gross_revenue, net.net_revenue, net.PC1, net.`PC1.5`, net.PC2, gross.new_customers_gross, net.new_customers from (select yrmonth, production_co.week_iso(date) as week, date, case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end as weekday,  channel_group, channel, count(distinct order_nr) as gross_orders, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as gross_revenue, sum(new_customers_gross) as new_customers_gross from production_co.tbl_order_detail where obc=1 group by yrmonth, production_co.week_iso(date), date, channel_group, channel)gross left join (select yrmonth, production_co.week_iso(date) as week, date, channel_group, channel, count(distinct order_nr) as net_orders, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)) as PC1, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(payment_cost,0)-ifnull(shipping_cost,0)) as `PC1.5`, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(payment_cost,0)-ifnull(shipping_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2, sum(new_customers) as new_customers from production_co.tbl_order_detail where oac=1 group by yrmonth, production_co.week_iso(date), date, channel_group, channel)net on gross.date=net.date and gross.channel=net.channel and gross.channel_group=net.channel_group where datediff(curdate(), gross.date)<@days order by date desc;

-- Pending Rev

update marketing_report.global_report g set pending_revenue = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production_co.tbl_order_detail t where t.pending=1 and g.date=t.date and g.channel_group=t.channel_group and g.channel=t.channel) where country='Colombia';

-- Canceled Rev

update marketing_report.global_report g set canceled_revenue = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production_co.tbl_order_detail t where t.cancel=1 and g.date=t.date and g.channel_group=t.channel_group and g.channel=t.channel) where country='Colombia';

-- Visits

if @channel_marketing=1 then update SEM.campaign_ad_group_co set source_medium=concat(source,' / ',medium);
update SEM.campaign_ad_group_co set channel=null, channel_group=null;
call production_co.channel_marketing;
end if;

delete from marketing_report.temporary;

insert into marketing_report.temporary(date, channel_group, channel) select distinct date, channel_group, channel from SEM.campaign_ad_group_co where datediff(curdate(), date)<@days;

update marketing_report.temporary t inner join marketing_report.global_report g on t.channel_group=g.channel_group and t.channel=g.channel and t.date=g.date set include = 1 where country='Colombia' and datediff(curdate(), g.date)<@days;

insert into marketing_report.global_report(country, date, channel_group, channel) select 'Colombia', date, channel_group, channel from marketing_report.temporary where include is null;

update marketing_report.global_report g set visits = (select sum(visits) from SEM.campaign_ad_group_co c where c.channel_group=g.channel_group and c.channel=g.channel and c.date=g.date) where country='Colombia' and datediff(curdate(), g.date)<@days;

-- Cost

##update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_co r where channel='Criteo' and g.date=r.date) where channel='Criteo' and country='Colombia' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(cost) from marketing_report.criteo_co r where g.date=r.date) where channel='Criteo' and country='Colombia' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(cost) from facebook.facebook_optimization_region_co r where g.date=r.date) where channel='Facebook Ads' and country='Colombia' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_co r where channel='GLG' and g.date=r.date) where channel='Glg' and country='Colombia' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(cost) from SEM.google_optimization_ad_group_co r where channel='GDN' and g.date=r.date) where channel='Google Display Network' and country='Colombia' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_co r where channel='Pampa Network' and g.date=r.date) where channel='Pampa Network' and country='Colombia' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_co r where channel='Main ADV' and g.date=r.date) where channel='Main ADV' and country='Colombia' and datediff(curdate(), g.date)<@days;   

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_co r where channel='My Things' and g.date=r.date) where channel='My Things' and country='Colombia' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_co r where channel='Trade Tracker' and g.date=r.date) where channel='Trade Tracker' and country='Colombia' and datediff(curdate(), g.date)<@days;      

##update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_co r where channel='FBX' and g.date=r.date) where channel='Facebook Retargeting' and country='Colombia' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(l.cost)+sum(r.cost) from marketing_report.sociomantic_fbx_loyalty_co l, marketing_report.sociomantic_fbx_co r where l.date=r.date and g.date=r.date) where channel='Facebook Retargeting' and country='Colombia' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set retention_cost = (select sum(l.cost) from marketing_report.sociomantic_fbx_loyalty_co l where l.date=g.date) where channel='Facebook Retargeting' and country='Colombia' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set acquisition_cost = (select sum(r.cost) from marketing_report.sociomantic_fbx_co r where g.date=r.date) where channel='Facebook Retargeting' and country='Colombia' and datediff(curdate(), g.date)<@days;  

update marketing_report.global_report g set cost = (select sum(cost) from SEM.google_optimization_ad_group_co r where channel='SEM' and g.date=r.date) where channel='SEM (unbranded)' and country='Colombia' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(ad_cost) from SEM.campaign_ad_group_co r where source = 'google' and medium = 'CPC' and campaign like 'brandb%' and g.date=r.date) where channel='SEM Branded' and country='Colombia' and datediff(curdate(), g.date)<@days;

##update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_co r where channel='Sociomantic' and g.date=r.date) where channel='Sociomantic' and country='Colombia' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(l.cost)+sum(r.cost) from marketing_report.sociomantic_loyalty_co l, marketing_report.sociomantic_basket_co r where l.date=r.date and g.date=r.date) where channel='Sociomantic' and country='Colombia' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set retention_cost = (select sum(l.cost) from marketing_report.sociomantic_loyalty_co l where l.date=g.date) where channel='Sociomantic' and country='Colombia' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set acquisition_cost = (select sum(r.cost) from marketing_report.sociomantic_basket_co r where g.date=r.date) where channel='Sociomantic' and country='Colombia' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_co r where channel='Veinteractive' and g.date=r.date) where channel='Newsletter' and country='Colombia' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_co r where channel='Vizury' and g.date=r.date) where channel='Vizury' and country='Colombia' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from facebook.fanpage_optimization_co r where g.date=r.date) where channel='Linio Fan Page' and country='Colombia' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(spent) from facebook.facebook_campaign r where g.date=r.date and campaign like 'COL_fanpagefashion%') where channel='Fashion Fan Page' and country='Colombia' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(spent) from facebook.facebook_campaign r where g.date=r.date and campaign like 'COL_darkpost%') where channel_group='Facebook Ads' and  channel='Dark Post' and country='Colombia' and datediff(curdate(), g.date)<@days;

create table marketing_report.temporary_offline(
date date,
count int
);

insert into marketing_report.temporary_offline select date, count(distinct channel) from marketing_report.global_report where channel_group='Offline Marketing' and country='Colombia' and datediff(curdate(), date)<@days group by date;

update marketing_report.global_report g set cost = (select sum(cost)/count from marketing_report.offline_co t, marketing_report.temporary_offline r where g.date=r.date and t.date=r.date) where channel_group='Offline Marketing' and country='Colombia' and datediff(curdate(), date)<@days;

drop table marketing_report.temporary_offline;

create table marketing_report.temporary_tele_sales(
date date,
count int
);

insert into marketing_report.temporary_tele_sales select date, count(distinct channel) from marketing_report.global_report where channel_group='Tele Sales' and country='Colombia' and datediff(curdate(), date)<@days group by date;

update marketing_report.global_report g set cost = (select sum(cost)/count from marketing_report.tele_sales_co t, marketing_report.temporary_tele_sales r where g.date=r.date and t.date=r.date) where channel_group='Tele Sales' and country='Colombia' and datediff(curdate(), date)<@days;

drop table marketing_report.temporary_tele_sales;

-- PC3

update marketing_report.global_report set PC3 = PC2-cost where country='Colombia';

-- Moving Avg

delete from marketing_report.temporary_global_report;

insert into marketing_report.temporary_global_report(date, channel_group, channel, gross_orders, net_orders, gross_revenue, net_revenue, new_customers_gross, new_customers) select date, channel_group, channel, gross_orders, net_orders, gross_revenue, net_revenue, new_customers_gross, new_customers from marketing_report.global_report where country='Colombia' and datediff(curdate(), date)<@days;

-- Orders

set @date=date_sub(curdate(), interval @days day);

while @date!=curdate() do
update marketing_report.temporary_global_report t set e_orders = (select (avg(g.net_orders/g.gross_orders)) from marketing_report.global_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Colombia') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;


update marketing_report.temporary_global_report t set e_orders = gross_orders*e_orders where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_orders = case when e_orders>net_orders then e_orders else net_orders end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set cancel = (select count(distinct order_nr) from production_co.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set returns = (select count(distinct order_nr) from production_co.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.returned=1) where datediff(curdate(), date)<@days; 

update marketing_report.temporary_global_report t set cancel = cancel/gross_orders, returns = returns/gross_orders where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set ee_orders = gross_orders*(1-ifnull(cancel,0)-ifnull(returns,0)) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_orders = case when e_orders>ee_orders then ee_orders else e_orders end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_orders = case when e_orders>net_orders then e_orders else net_orders end where datediff(curdate(), date)<@days;

-- Revenue

set @date=date_sub(curdate(), interval @days day);

while @date!=curdate() do
update marketing_report.temporary_global_report t set e_revenue = (select (avg(g.net_revenue/g.gross_revenue)) from marketing_report.global_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Colombia') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_global_report t set e_revenue = gross_revenue*e_revenue where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_revenue = case when e_revenue>net_revenue then e_revenue else net_revenue end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set cancel = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production_co.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set returns = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production_co.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.returned=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set voucher = (select sum(coupon_money_after_vat) from production_co.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.coupon_code is not null and d.oac=1) where datediff(curdate(), date)<@days; 

update marketing_report.temporary_global_report t set cancel = cancel/gross_revenue, returns = returns/gross_revenue, voucher=voucher/gross_revenue where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set ee_revenue = gross_revenue*(1-ifnull(cancel,0)-ifnull(returns,0)-ifnull(voucher,0)) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_revenue = case when e_revenue>ee_revenue then ee_revenue else e_revenue end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_revenue = case when e_revenue>net_revenue then e_revenue else net_revenue end where datediff(curdate(), date)<@days;

-- New Customers

set @date=date_sub(curdate(), interval @days day);

while @date!=curdate() do
update marketing_report.temporary_global_report t set e_new_customers = (select (avg(g.new_customers/g.new_customers_gross)) from marketing_report.global_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Colombia') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_global_report t set e_new_customers = new_customers_gross*e_new_customers where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_new_customers = case when e_new_customers>new_customers then e_new_customers else new_customers end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set cancel = (select sum(new_customers_gross) from production_co.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set returns = (select sum(new_customers_gross) from production_co.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.returned=1) where datediff(curdate(), date)<@days; 

update marketing_report.temporary_global_report t set cancel = cancel/new_customers_gross, returns = returns/new_customers_gross where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set ee_new_customers = new_customers_gross*(1-ifnull(cancel,0)-ifnull(returns,0)) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_new_customers = case when e_new_customers>ee_new_customers then ee_new_customers else e_new_customers end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_new_customers = case when e_new_customers>new_customers then e_new_customers else new_customers end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t inner join marketing_report.global_report g on t.date=g.date and t.channel_group=g.channel_group and t.channel=g.channel set g.e_orders=t.e_orders, g.e_revenue=t.e_revenue, g.e_new_customers=t.e_new_customers where country = 'Colombia' and datediff(curdate(), g.date)<@days;

-- Exchange Rate

update marketing_report.global_report set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=production.week_iso(date), weekday=case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update marketing_report.global_report s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, e_revenue=e_revenue/xr, a_revenue=a_revenue/xr, PC1=PC1/xr, `PC1.5`=`PC1.5`/xr, PC2=PC2/xr, PC3=PC3/xr, pending_revenue=pending_revenue/xr, canceled_revenue=canceled_revenue/xr where s.country = 'Colombia' and r.country='COL' and datediff(curdate(), s.date)<@days; 

update marketing_report.global_report set cost = cost/16.35 where channel= 'SEM Branded' and country = 'Colombia' and yrmonth between 201305 and 201310 and datediff(curdate(), date)<@days;

update marketing_report.global_report set cost = cost/2450 where channel= 'SEM Branded' and country = 'Colombia' and yrmonth >= 201311 and datediff(curdate(), date)<@days;

update marketing_report.global_report set cost = cost*1.23 where channel= 'Criteo' and country = 'Colombia' and datediff(curdate(), date)<@days;

-- Peru

update production_pe.tbl_order_detail t inner join development_pe.A_Master o on t.item=o.itemID set t.unit_price_after_vat=o.priceaftertax, t.coupon_money_after_vat=o.couponvalueaftertax, t.shipping_fee_after_vat=o.shippingfee, t.costo_after_vat=o.costaftertax, t.delivery_cost_supplier=o.deliverycostsupplier, t.payment_cost=o.paymentfees, t.shipping_cost=o.shippingcost, t.wh=o.flwhcost, t.cs=o.flcscost, t.oac=o.orderaftercan, t.obc=o.orderbeforecan, t.returned=o.returns, t.pending=o.pending, t.cancel=o.cancellations where datediff(curdate(), t.date)<@freeze;

insert into marketing_report.global_report(country, yrmonth, week, date, weekday, channel_group, channel, gross_orders, net_orders, gross_revenue, net_revenue, PC1, `PC1.5`, PC2, new_customers_gross, new_customers) select 'Peru', gross.yrmonth, gross.week, gross.date, gross.weekday, gross.channel_group, gross.channel, gross.gross_orders, net.net_orders, gross.gross_revenue, net.net_revenue, net.PC1, net.`PC1.5`, net.PC2, gross.new_customers_gross, net.new_customers from (select yrmonth, production_pe.week_iso(date) as week, date, case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end as weekday,  channel_group, channel, count(distinct order_nr) as gross_orders, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as gross_revenue, sum(new_customers_gross) as new_customers_gross from production_pe.tbl_order_detail where obc=1 group by yrmonth, production_pe.week_iso(date), date, channel_group, channel)gross left join (select yrmonth, production_pe.week_iso(date) as week, date, channel_group, channel, count(distinct order_nr) as net_orders, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)) as PC1, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(payment_cost,0)-ifnull(shipping_cost,0)) as `PC1.5`, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(payment_cost,0)-ifnull(shipping_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2, sum(new_customers) as new_customers from production_pe.tbl_order_detail where oac=1 group by yrmonth, production_pe.week_iso(date), date, channel_group, channel)net on gross.date=net.date and gross.channel=net.channel and gross.channel_group=net.channel_group where datediff(curdate(), gross.date)<@days order by date desc;

-- Pending Rev

update marketing_report.global_report g set pending_revenue = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production_pe.tbl_order_detail t where t.pending=1 and g.date=t.date and g.channel_group=t.channel_group and g.channel=t.channel) where country='Peru';

-- Canceled Rev

update marketing_report.global_report g set canceled_revenue = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production_pe.tbl_order_detail t where t.cancel=1 and g.date=t.date and g.channel_group=t.channel_group and g.channel=t.channel) where country='Peru';

-- Visits

if @channel_marketing=1 then update SEM.campaign_ad_group_pe set source_medium=concat(source,' / ',medium);
update SEM.campaign_ad_group_pe set channel=null, channel_group=null;
call production_pe.channel_marketing;
end if;

delete from marketing_report.temporary;

insert into marketing_report.temporary(date, channel_group, channel) select distinct date, channel_group, channel from SEM.campaign_ad_group_pe where datediff(curdate(), date)<@days;

update marketing_report.temporary t inner join marketing_report.global_report g on t.channel_group=g.channel_group and t.channel=g.channel and t.date=g.date set include = 1 where country='Peru' and datediff(curdate(), g.date)<@days;

insert into marketing_report.global_report(country, date, channel_group, channel) select 'Peru', date, channel_group, channel from marketing_report.temporary where include is null;

update marketing_report.global_report g set visits = (select sum(visits) from SEM.campaign_ad_group_pe c where c.channel_group=g.channel_group and c.channel=g.channel and c.date=g.date) where country='Peru' and datediff(curdate(), g.date)<@days;

-- Cost

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_pe r where channel='Criteo' and g.date=r.date) where channel='Criteo' and country='Peru' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(cost) from facebook.facebook_optimization_region_pe r where g.date=r.date) where channel='Facebook Ads' and country='Peru' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_pe r where channel='GLG' and g.date=r.date) where channel='Glg' and country='Peru' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(cost) from SEM.google_optimization_ad_group_pe r where channel='GDN' and g.date=r.date) where channel='Google Display Network' and country='Peru' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_pe r where channel='Main ADV' and g.date=r.date) where channel='Main ADV' and country='Peru' and datediff(curdate(), g.date)<@days;  

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_pe r where channel='Trade Tracker' and g.date=r.date) where channel='Trade Tracker' and country='Peru' and datediff(curdate(), g.date)<@days; 

##update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_pe r where channel='FBX' and g.date=r.date) where channel='Facebook R.' and country='Peru' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(l.cost)+sum(r.cost) from marketing_report.sociomantic_fbx_loyalty_pe l, marketing_report.sociomantic_fbx_pe r where l.date=r.date and g.date=r.date) where channel='Facebook R.' and country='Peru' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set retention_cost = (select sum(l.cost) from marketing_report.sociomantic_fbx_loyalty_pe l where l.date=g.date) where channel='Facebook R.' and country='Peru' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set acquisition_cost = (select sum(r.cost) from marketing_report.sociomantic_fbx_pe r where g.date=r.date) where channel='Facebook R.' and country='Peru' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(cost) from SEM.google_optimization_ad_group_pe r where channel='SEM' and g.date=r.date) where channel='SEM' and country='Peru' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(ad_cost) from SEM.campaign_ad_group_pe r where source = 'google' and medium = 'CPC' and campaign like 'brandb%' and g.date=r.date) where channel='SEM Branded' and country='Peru' and datediff(curdate(), g.date)<@days;

##update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_pe r where channel='Sociomantic' and g.date=r.date) where channel='Sociomantic' and country='Peru' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(l.cost)+sum(r.cost) from marketing_report.sociomantic_loyalty_pe l, marketing_report.sociomantic_retargeting_pe r where l.date=r.date and g.date=r.date) where channel='Sociomantic' and country='Peru' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set retention_cost = (select sum(l.cost) from marketing_report.sociomantic_loyalty_pe l where l.date=g.date) where channel='Sociomantic' and country='Peru' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set acquisition_cost = (select sum(r.cost) from marketing_report.sociomantic_retargeting_pe r where g.date=r.date) where channel='Sociomantic' and country='Peru' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_pe r where channel='Veinteractive' and g.date=r.date) where channel='Newsletter' and country='Peru' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_pe r where channel='Vizury' and g.date=r.date) where channel='Vizury' and country='Peru' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from facebook.fanpage_optimization_pe r where g.date=r.date) where channel='FB Posts' and country='Peru' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(spent) from facebook.facebook_campaign r where g.date=r.date and campaign like 'PER_darkpost%') where channel_group='Facebook Ads' and  channel='Dark Post' and country='Peru' and datediff(curdate(), g.date)<@days;

create table marketing_report.temporary_offline(
date date,
count int
);

insert into marketing_report.temporary_offline select date, count(distinct channel) from marketing_report.global_report where channel_group='Offline Marketing' and country='Peru' and datediff(curdate(), date)<@days group by date;

update marketing_report.global_report g set cost = (select sum(cost)/count from marketing_report.offline_pe t, marketing_report.temporary_offline r where g.date=r.date and t.date=r.date) where channel_group='Offline Marketing' and country='Peru' and datediff(curdate(), date)<@days;

drop table marketing_report.temporary_offline;

create table marketing_report.temporary_tele_sales(
date date,
count int
);

insert into marketing_report.temporary_tele_sales select date, count(distinct channel) from marketing_report.global_report where channel_group='Tele Sales' and country='Peru' and datediff(curdate(), date)<@days group by date;

update marketing_report.global_report g set cost = (select sum(cost)/count from marketing_report.tele_sales_pe t, marketing_report.temporary_tele_sales r where g.date=r.date and t.date=r.date) where channel_group='Tele Sales' and country='Peru' and datediff(curdate(), g.date)<@days;

drop table marketing_report.temporary_tele_sales;

-- PC3

update marketing_report.global_report set PC3 = PC2-cost where country='Peru';

-- Moving Avg

delete from marketing_report.temporary_global_report;

insert into marketing_report.temporary_global_report(date, channel_group, channel, gross_orders, net_orders, gross_revenue, net_revenue, new_customers_gross, new_customers) select date, channel_group, channel, gross_orders, net_orders, gross_revenue, net_revenue, new_customers_gross, new_customers from marketing_report.global_report where country='Peru' and datediff(curdate(), date)<@days;

-- Orders

set @date=date_sub(curdate(), interval @days day);

while @date!=curdate() do
update marketing_report.temporary_global_report t set e_orders = (select (avg(g.net_orders/g.gross_orders)) from marketing_report.global_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Peru') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_global_report t set e_orders = gross_orders*e_orders where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_orders = case when e_orders>net_orders then e_orders else net_orders end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set cancel = (select count(distinct order_nr) from production_pe.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set returns = (select count(distinct order_nr) from production_pe.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.returned=1) where datediff(curdate(), date)<@days; 

update marketing_report.temporary_global_report t set cancel = cancel/gross_orders, returns = returns/gross_orders where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set ee_orders = gross_orders*(1-ifnull(cancel,0)-ifnull(returns,0)) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_orders = case when e_orders>ee_orders then ee_orders else e_orders end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_orders = case when e_orders>net_orders then e_orders else net_orders end where datediff(curdate(), date)<@days;

-- Revenue

set @date=date_sub(curdate(), interval @days day);

while @date!=curdate() do
update marketing_report.temporary_global_report t set e_revenue = (select (avg(g.net_revenue/g.gross_revenue)) from marketing_report.global_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Peru') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_global_report t set e_revenue = gross_revenue*e_revenue where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_revenue = case when e_revenue>net_revenue then e_revenue else net_revenue end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set cancel = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production_pe.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set returns = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production_pe.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.returned=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set voucher = (select sum(coupon_money_after_vat) from production_pe.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.coupon_code is not null and d.oac=1) where datediff(curdate(), date)<@days; 

update marketing_report.temporary_global_report t set cancel = cancel/gross_revenue, returns = returns/gross_revenue, voucher=voucher/gross_revenue where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set ee_revenue = gross_revenue*(1-ifnull(cancel,0)-ifnull(returns,0)-ifnull(voucher,0)) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_revenue = case when e_revenue>ee_revenue then ee_revenue else e_revenue end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_revenue = case when e_revenue>net_revenue then e_revenue else net_revenue end where datediff(curdate(), date)<@days;

-- New Customers

set @date=date_sub(curdate(), interval @days day);

while @date!=curdate() do
update marketing_report.temporary_global_report t set e_new_customers = (select (avg(g.new_customers/g.new_customers_gross)) from marketing_report.global_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Peru') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_global_report t set e_new_customers = new_customers_gross*e_new_customers where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_new_customers = case when e_new_customers>new_customers then e_new_customers else new_customers end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set cancel = (select sum(new_customers_gross) from production_pe.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set returns = (select sum(new_customers_gross) from production_pe.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.returned=1) where datediff(curdate(), date)<@days; 

update marketing_report.temporary_global_report t set cancel = cancel/new_customers_gross, returns = returns/new_customers_gross where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set ee_new_customers = new_customers_gross*(1-ifnull(cancel,0)-ifnull(returns,0));

update marketing_report.temporary_global_report t set e_new_customers = case when e_new_customers>ee_new_customers then ee_new_customers else e_new_customers end;

update marketing_report.temporary_global_report t set e_new_customers = case when e_new_customers>new_customers then e_new_customers else new_customers end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t inner join marketing_report.global_report g on t.date=g.date and t.channel_group=g.channel_group and t.channel=g.channel set g.e_orders=t.e_orders, g.e_revenue=t.e_revenue, g.e_new_customers=t.e_new_customers where country = 'Peru' and datediff(curdate(), g.date)<@days;

-- Exchange Rate

update marketing_report.global_report s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, e_revenue=e_revenue/xr, a_revenue=a_revenue/xr, PC1=PC1/xr, `PC1.5`=`PC1.5`/xr, PC2=PC2/xr, PC3=PC3/xr, pending_revenue=pending_revenue/xr, canceled_revenue=canceled_revenue/xr where s.country = 'Peru' and r.country='PER' and datediff(curdate(), s.date)<@days;

update marketing_report.global_report set cost = cost/16.35 where channel= 'SEM Branded' and country = 'Peru' and yrmonth<=201311 and datediff(curdate(), date)<@days;

update marketing_report.global_report set cost = cost*1.23 where channel= 'SEM Branded' and country = 'Peru' and yrmonth>=201312 and datediff(curdate(), date)<@days;

-- Venezuela

update production_ve.tbl_order_detail t inner join development_ve.A_Master o on t.item=o.itemID set t.unit_price_after_vat=o.priceaftertax, t.coupon_money_after_vat=o.couponvalueaftertax, t.shipping_fee_after_vat=o.shippingfee, t.costo_after_vat=o.costaftertax, t.delivery_cost_supplier=o.deliverycostsupplier, t.payment_cost=o.paymentfees, t.shipping_cost=o.shippingcost, t.wh=o.flwhcost, t.cs=o.flcscost, t.oac=o.orderaftercan, t.obc=o.orderbeforecan, t.returned=o.returns, t.pending=o.pending, t.cancel=o.cancellations where datediff(curdate(), t.date)<@freeze;

insert into marketing_report.global_report(country, yrmonth, week, date, weekday, channel_group, channel, gross_orders, net_orders, gross_revenue, net_revenue, PC1, `PC1.5`, PC2, new_customers_gross, new_customers) select 'Venezuela', gross.yrmonth, gross.week, gross.date, gross.weekday, gross.channel_group, gross.channel, gross.gross_orders, net.net_orders, gross.gross_revenue, net.net_revenue, net.PC1, net.`PC1.5`, net.PC2, gross.new_customers_gross, net.new_customers from (select yrmonth, production_ve.week_iso(date) as week, date, case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end as weekday,  channel_group, channel, count(distinct order_nr) as gross_orders, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as gross_revenue, sum(new_customers_gross) as new_customers_gross from production_ve.tbl_order_detail where obc=1 group by yrmonth, production_ve.week_iso(date), date, channel_group, channel)gross left join (select yrmonth, production_ve.week_iso(date) as week, date, channel_group, channel, count(distinct order_nr) as net_orders, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)) as PC1, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(payment_cost,0)-ifnull(shipping_cost,0)) as `PC1.5`, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(payment_cost,0)-ifnull(shipping_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2, sum(new_customers) as new_customers from production_ve.tbl_order_detail where oac=1 group by yrmonth, production_ve.week_iso(date), date, channel_group, channel)net on gross.date=net.date and gross.channel=net.channel and gross.channel_group=net.channel_group where datediff(curdate(), gross.date)<@days order by date desc;

-- Pending Rev

update marketing_report.global_report g set pending_revenue = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production_ve.tbl_order_detail t where t.pending=1 and g.date=t.date and g.channel_group=t.channel_group and g.channel=t.channel) where country='Venezuela';

-- Canceled Rev

update marketing_report.global_report g set canceled_revenue = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production_ve.tbl_order_detail t where t.cancel=1 and g.date=t.date and g.channel_group=t.channel_group and g.channel=t.channel) where country='Venezuela';

-- Visits

if @channel_marketing=1 then update SEM.campaign_ad_group_ve set source_medium=concat(source,' / ',medium);
update SEM.campaign_ad_group_ve set channel=null, channel_group=null;
call production_ve.channel_marketing;
end if;

delete from marketing_report.temporary;

insert into marketing_report.temporary(date, channel_group, channel) select distinct date, channel_group, channel from SEM.campaign_ad_group_ve where datediff(curdate(), date)<@days;

update marketing_report.temporary t inner join marketing_report.global_report g on t.channel_group=g.channel_group and t.channel=g.channel and t.date=g.date set include = 1 where country='Venezuela' and datediff(curdate(), g.date)<@days;

insert into marketing_report.global_report(country, date, channel_group, channel) select 'Venezuela', date, channel_group, channel from marketing_report.temporary where include is null;

drop table marketing_report.temporary;

update marketing_report.global_report g set visits = (select sum(visits) from SEM.campaign_ad_group_ve c where c.channel_group=g.channel_group and c.channel=g.channel and c.date=g.date) where country='Venezuela' and datediff(curdate(), g.date)<@days;

-- Cost

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_ve r where channel='Criteo' and g.date=r.date) where channel='Criteo' and country='Venezuela' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_ve r where channel='GLG' and g.date=r.date) where channel='Glg' and country='Venezuela' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(cost) from SEM.google_optimization_ad_group_ve r where channel='GDN' and g.date=r.date) where channel='Google Display Network' and country='Venezuela' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_ve r where channel='Pampa Network' and g.date=r.date) where channel='Pampa Network' and country='Venezuela' and datediff(curdate(), g.date)<@days;  

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_ve r where channel='Main ADV' and g.date=r.date) where channel='Main ADV' and country='Venezuela' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_ve r where channel='Trade Tracker' and g.date=r.date) where channel='Trade Tracker' and country='Venezuela' and datediff(curdate(), g.date)<@days; 

##update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_ve r where channel='FBX' and g.date=r.date) where channel='Facebook R.' and country='Venezuela' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(l.cost)+sum(r.cost) from marketing_report.sociomantic_fbx_loyalty_ve l, marketing_report.sociomantic_fbx_ve r where l.date=r.date and g.date=r.date) where channel='Facebook R.' and country='Venezuela' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set retention_cost = (select sum(l.cost) from marketing_report.sociomantic_fbx_loyalty_ve l where l.date=g.date) where channel='Facebook R.' and country='Venezuela' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set acquisition_cost = (select sum(r.cost) from marketing_report.sociomantic_fbx_ve r where g.date=r.date) where channel='Facebook R.' and country='Venezuela' and datediff(curdate(), g.date)<@days; 

update marketing_report.global_report g set cost = (select sum(cost) from SEM.google_optimization_ad_group_ve r where channel='SEM' and g.date=r.date) where channel='SEM' and country='Venezuela' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(ad_cost) from SEM.campaign_ad_group_ve r where source = 'google' and medium = 'CPC' and campaign like 'brandb%' and g.date=r.date) where channel='SEM Branded' and country='Venezuela' and datediff(curdate(), g.date)<@days;

##update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_ve r where channel='Sociomantic' and g.date=r.date) where channel='Sociomantic' and country='Venezuela' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(l.cost)+sum(r.cost) from marketing_report.sociomantic_loyalty_ve l, marketing_report.sociomantic_basket_ve r where l.date=r.date and g.date=r.date) where channel='Sociomantic' and country='Venezuela' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set retention_cost = (select sum(l.cost) from marketing_report.sociomantic_loyalty_ve l where l.date=g.date) where channel='Sociomantic' and country='Venezuela' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set acquisition_cost = (select sum(r.cost) from marketing_report.sociomantic_basket_ve r where g.date=r.date) where channel='Sociomantic' and country='Venezuela' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_ve r where channel='Veinteractive' and g.date=r.date) where channel='Newsletter' and country='Venezuela' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from production.retargeting_affiliates_ve r where channel='Vizury' and g.date=r.date) where channel='Vizury' and country='Venezuela' and datediff(curdate(), g.date)<@days;

update marketing_report.global_report g set cost = (select sum(cost) from facebook.fanpage_optimization_ve r where g.date=r.date) where channel='FB Posts' and country='Venezuela' and datediff(curdate(), g.date)<@days;

create table marketing_report.temporary_offline(
date date,
count int
);

insert into marketing_report.temporary_offline select date, count(distinct channel) from marketing_report.global_report where channel_group='Offline Marketing' and country='Venezuela' and datediff(curdate(), date)<@days group by date;

update marketing_report.global_report g set cost = (select sum(cost)/count from marketing_report.offline_ve t, marketing_report.temporary_offline r where g.date=r.date and t.date=r.date) where channel_group='Offline Marketing' and country='Venezuela' and datediff(curdate(), date)<@days;

drop table marketing_report.temporary_offline;

-- PC3

update marketing_report.global_report set PC3 = PC2-cost where country='Venezuela';

-- Moving Avg

delete from marketing_report.temporary_global_report;

insert into marketing_report.temporary_global_report(date, channel_group, channel, gross_orders, net_orders, gross_revenue, net_revenue, new_customers_gross, new_customers) select date, channel_group, channel, gross_orders, net_orders, gross_revenue, net_revenue, new_customers_gross, new_customers from marketing_report.global_report where country='Venezuela' and datediff(curdate(), date)<@days;

-- Orders

set @date=date_sub(curdate(), interval @days day);

while @date!=curdate() do
update marketing_report.temporary_global_report t set e_orders = (select (avg(g.net_orders/g.gross_orders)) from marketing_report.global_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Venezuela') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_global_report t set e_orders = gross_orders*e_orders where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_orders = case when e_orders>net_orders then e_orders else net_orders end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set cancel = (select count(distinct order_nr) from production_ve.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set returns = (select count(distinct order_nr) from production_ve.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.returned=1) where datediff(curdate(), date)<@days; 

update marketing_report.temporary_global_report t set cancel = cancel/gross_orders, returns = returns/gross_orders where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set ee_orders = gross_orders*(1-ifnull(cancel,0)-ifnull(returns,0)) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_orders = case when e_orders>ee_orders then ee_orders else e_orders end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_orders = case when e_orders>net_orders then e_orders else net_orders end where datediff(curdate(), date)<@days;

-- Revenue

set @date=date_sub(curdate(), interval @days day);

while @date!=curdate() do
update marketing_report.temporary_global_report t set e_revenue = (select (avg(g.net_revenue/g.gross_revenue)) from marketing_report.global_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Venezuela') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;

update marketing_report.temporary_global_report t set e_revenue = gross_revenue*e_revenue where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_revenue = case when e_revenue>net_revenue then e_revenue else net_revenue end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set cancel = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production_ve.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set returns = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production_ve.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.returned=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set voucher = (select sum(coupon_money_after_vat) from production_ve.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.coupon_code is not null and d.oac=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set cancel = cancel/gross_revenue, returns = returns/gross_revenue, voucher=voucher/gross_revenue where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set ee_revenue = gross_revenue*(1-ifnull(cancel,0)-ifnull(returns,0)-ifnull(voucher,0)) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_revenue = case when e_revenue>ee_revenue then ee_revenue else e_revenue end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_revenue = case when e_revenue>net_revenue then e_revenue else net_revenue end where datediff(curdate(), date)<@days;

-- New Customers

set @date=date_sub(curdate(), interval @days day);

while @date!=curdate() do
update marketing_report.temporary_global_report t set e_new_customers = (select (avg(g.new_customers/g.new_customers_gross)) from marketing_report.global_report g where g.date between date_sub(@date, interval 42 day) and date_sub(@date,interval 14 day) and country='Venezuela') where t.date=@date;
set @date=adddate(@date, interval 1 day);
end while;
update marketing_report.temporary_global_report t set e_new_customers = new_customers_gross*e_new_customers where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_new_customers = case when e_new_customers>new_customers then e_new_customers else new_customers end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set cancel = (select sum(new_customers_gross) from production_ve.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.cancel=1) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set returns = (select sum(new_customers_gross) from production_ve.tbl_order_detail d where t.channel_group=d.channel_group and t.channel=d.channel and t.date=d.date and d.returned=1) where datediff(curdate(), date)<@days; 

update marketing_report.temporary_global_report t set cancel = cancel/new_customers_gross, returns = returns/new_customers_gross where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set ee_new_customers = new_customers_gross*(1-ifnull(cancel,0)-ifnull(returns,0)) where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_new_customers = case when e_new_customers>ee_new_customers then ee_new_customers else e_new_customers end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t set e_new_customers = case when e_new_customers>new_customers then e_new_customers else new_customers end where datediff(curdate(), date)<@days;

update marketing_report.temporary_global_report t inner join marketing_report.global_report g on t.date=g.date and t.channel_group=g.channel_group and t.channel=g.channel set g.e_orders=t.e_orders, g.e_revenue=t.e_revenue, g.e_new_customers=t.e_new_customers where country = 'Venezuela' and datediff(curdate(), g.date)<@days;

-- Exchange Rate

update marketing_report.global_report s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, e_revenue=e_revenue/xr, a_revenue=a_revenue/xr, PC1=PC1/xr, `PC1.5`=`PC1.5`/xr, PC2=PC2/xr, PC3=PC3/xr, pending_revenue=pending_revenue/xr, canceled_revenue=canceled_revenue/xr where s.country = 'Venezuela' and r.country='VEN' and datediff(curdate(), s.date)<@days;

update marketing_report.global_report set cost = cost/16.35 where channel= 'SEM Branded' and country = 'Venezuela' and yrmonth<=201311 and datediff(curdate(), date)<@days;

update marketing_report.global_report set cost = cost*1.23 where channel= 'SEM Branded' and country = 'Venezuela' and yrmonth>=201312 and datediff(curdate(), date)<@days;

-- Yrmonth &  Week & Weekday

update marketing_report.global_report set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=production.week_iso(date), weekday=case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

-- Null

update marketing_report.global_report set gross_orders = 0 where gross_orders is null;

update marketing_report.global_report set net_orders = 0 where net_orders is null;

update marketing_report.global_report set e_orders = 0 where e_orders is null;

update marketing_report.global_report set gross_revenue = 0 where gross_revenue is null;

update marketing_report.global_report set net_revenue = 0 where net_revenue is null;

update marketing_report.global_report set e_revenue = 0 where e_revenue is null;

update marketing_report.global_report set new_customers = 0 where new_customers is null;

update marketing_report.global_report set new_customers_gross = 0 where new_customers_gross is null;

update marketing_report.global_report set e_new_customers = 0 where e_new_customers is null;

update marketing_report.global_report set visits = 0 where visits is null;

update marketing_report.global_report set cost = 0 where cost is null;

delete from marketing_report.global_report where date is null;

drop table marketing_report.temporary_global_report;

select  'Marketing Report: stop',now();

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`lorena.ramirez`@`%`*/ /*!50003 PROCEDURE `outbound`()
BEGIN

#Colombia
delete from test_marketing.outbound_co;

insert into test_marketing.outbound_co (yrmonth, date, channel_group, channel, net_sales, net_orders, PC_1, PC_15, PC_2)
select yrmonth,date,channel_group, channel, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) net_sales,
count(distinct orderID) net_orders,
sum(paid_price_after_vat)+sum(shipping_fee_after_vat) - (sum(cost_oms)+sum(delivery_cost_supplier)) PC_1,
sum(paid_price_after_vat)+sum(shipping_fee_after_vat) - (sum(cost_oms)+sum(delivery_cost_supplier)+sum(shipping_cost)+ sum(payment_cost)) PC_15,
sum(paid_price_after_vat)+sum(shipping_fee_after_vat) - (sum(cost_oms)+sum(delivery_cost_supplier)+sum(shipping_cost)+ sum(payment_cost)+sum(cs)+sum(wh)) PC_2
from bazayaco.tbl_order_detail
where oac = 1 and returned = 0 and rejected = 0 and 
(`source/medium` like '%outbound%' or `source/medium` like '%reorder%' or `source/medium` like '%replace%'
or `source/medium` like '%upsell%' or coupon_code like 'REO%' or coupon_code like 'REP%') and  channel_group not like '%Tele%' 
group by yrmonth desc, date,channel_group, channel;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`lorena.ramirez`@`%`*/ /*!50003 PROCEDURE `partnerships`()
BEGIN

delete from test_marketing.partnerships_co;

insert into test_marketing.partnerships_co (yrmonth, date, channel_group, net_orders, net_items_sold, net_revenue, new_customers, PC_1, PC_15, PC_2)
select yrmonth,date, channel_group, count(distinct order_nr) as net_orders, 
count(item) net_items_sold, 
sum(paid_price_after_vat)+sum(shipping_fee_after_vat) net_revenue,
sum(new_customers) as new_customers,
sum(paid_price_after_vat)+sum(shipping_fee_after_vat) - (sum(cost_oms)+sum(delivery_cost_supplier)) PC_1,
sum(paid_price_after_vat)+sum(shipping_fee_after_vat) - (sum(cost_oms)+sum(delivery_cost_supplier)+sum(shipping_cost)+ sum(payment_cost)) PC_1_5,
sum(paid_price_after_vat)+sum(shipping_fee_after_vat) - (sum(cost_oms)+sum(delivery_cost_supplier)+sum(shipping_cost)+ sum(payment_cost)+sum(cs)+sum(wh)) PC_2
from bazayaco.tbl_order_detail 
where coupon_code_description like 'Partnerships%' and oac = 1 and returned = 0 and rejected = 0
group by yrmonth, date,channel_group;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`lorena.ramirez`@`%`*/ /*!50003 PROCEDURE `partnerships_co`()
BEGIN

delete from partnerships_co;

insert into partnerships_co (yrmonth, date, channel_group, net_orders, net_items_sold, net_revenue, new_customers, PC_1, PC_15, PC_2)
select yrmonth,date, channel_group, count(distinct order_nr) as net_orders, 
count(item) net_items_sold, 
sum(paid_price_after_vat)+sum(shipping_fee_after_vat) net_revenue,
sum(new_customers) as new_customers,
sum(paid_price_after_vat)+sum(shipping_fee_after_vat) - (sum(cost_oms)+sum(delivery_cost_supplier)) PC_1,
sum(paid_price_after_vat)+sum(shipping_fee_after_vat) - (sum(cost_oms)+sum(delivery_cost_supplier)+sum(shipping_cost)+ sum(payment_cost)) PC_1_5,
sum(paid_price_after_vat)+sum(shipping_fee_after_vat) - (sum(cost_oms)+sum(delivery_cost_supplier)+sum(shipping_cost)+ sum(payment_cost)+sum(cs)+sum(wh)) PC_2
from bazayaco.tbl_order_detail 
where coupon_code_description like 'Partnerships%' and oac = 1 and returned = 0 and rejected = 0
group by yrmonth, date,channel_group;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:04:25
