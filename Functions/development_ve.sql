-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: development_ve
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'development_ve'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 FUNCTION `calcworkdays`(sdate varchar(50), edate varchar(50)) RETURNS int(11)
begin
 declare wdays, tdiff, counter, thisday smallint;
 declare newdate date;
 set newdate := sdate;
 set wdays = 0;
 if sdate is null then return 0; end if;
 if edate is null then return 0; end if;
 if edate like '1%' then return 0; end if;
 if edate like '0%' then return 0; end if;
 if sdate like '1%' then return 0; end if;
 if sdate like '0%' then return 0; end if;
    if datediff(edate, sdate) <= 0 then return 0; end if;
         label1: loop
          set thisday = dayofweek(newdate);
            if thisday in(1,2,3,4,5) then set wdays := wdays + 1; end if;
             set newdate = date_add(newdate, interval 1 day);
            if datediff(edate, newdate) <= 0 then leave label1; end if;
       end loop label1;
      return wdays;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 FUNCTION `maximum`(procure integer, ready integer, to_ship integer,  attempt integer ) RETURNS int(11)
begin
declare compare integer; 
		set compare = procure;
		if compare < ready then set compare = ready; end if;
		if compare < to_ship then set compare = to_ship; end if;
		if compare < attempt then set compare = attempt; end if;
 return compare;
 end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 FUNCTION `week_exit`(edate date) RETURNS varchar(20) CHARSET latin1
begin
 declare sdate varchar(20);
 if year(edate) = 2012 then set sdate = week(edate, 5)+1; else set sdate = date_format(edate, "%v"); end if;
      return sdate;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 FUNCTION `week_iso`(edate date) RETURNS varchar(20) CHARSET latin1
begin
 declare sdate varchar(20);
 if year(edate) = 2012 then set sdate = concat(year(edate), '-',(week(edate, 5))+1); else set sdate = date_format(edate, "%x-%v"); end if;
      return sdate;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 FUNCTION `workday`(edate date, workdays integer ) RETURNS date
begin
 declare wdays, thisday, thisday0, timer smallint;
 declare newdate, middate date;
 set wdays = 0;
 set newdate :=  edate;
 if edate is null then set newdate = '2012-05-01'; end if;
 if workdays is null then return '2012-05-01'; end if;
 set thisday0 = dayofweek(edate);
 if workdays in (0) and thisday0 between 2 and 6 then return newdate; end if;
 if (workdays in (0) and thisday0 in (7)) then return date(adddate(newdate, 2)); end if;
 if (workdays in (0) and thisday0 in (1)) then return date(adddate(newdate, 1)); end if;
	label2: loop
		set newdate = date(adddate(newdate, 1));
        set thisday = dayofweek(newdate);
		if thisday between 2 and 6 then 
			set wdays = wdays + 1;
			if wdays >= workdays then 
				leave label2; 
			end if;
		else 
			set wdays = wdays;
		end if;
	end loop label2;
return newdate;
 end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `catalog_creation`()
begin

delete from production_ve.catalog_creation;

insert into production_ve.catalog_creation
select z.cat1, a201208.number as '201208', a201209.number as '201209', a201210.number as '201210', a201211.number as '201211', a201212.number as '201212', a201301.number as '201301', a201302.number as '201302', a201303.number as '201303', a201304.number as '201304' from (select cat1 from production_ve.tbl_catalog_product_v2 c where cat1 is not null group by cat1)z left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_ve.tbl_catalog_product_v2 c, bob_live_ve.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) = 201208 group by cat1)a201208 on z.cat1=a201208.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_ve.tbl_catalog_product_v2 c, bob_live_ve.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201209 group by cat1)a201209 on z.cat1=a201209.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_ve.tbl_catalog_product_v2 c, bob_live_ve.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201210 group by cat1)a201210 on z.cat1=a201210.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_ve.tbl_catalog_product_v2 c, bob_live_ve.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201211 group by cat1)a201211 on z.cat1=a201211.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_ve.tbl_catalog_product_v2 c, bob_live_ve.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201212 group by cat1)a201212 on z.cat1=a201212.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_ve.tbl_catalog_product_v2 c, bob_live_ve.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201301 group by cat1)a201301 on z.cat1=a201301.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_ve.tbl_catalog_product_v2 c, bob_live_ve.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201302 group by cat1)a201302 on z.cat1=a201302.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_ve.tbl_catalog_product_v2 c, bob_live_ve.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201303 group by cat1)a201303 on z.cat1=a201303.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_ve.tbl_catalog_product_v2 c, bob_live_ve.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201304 group by cat1)a201304 on z.cat1=a201304.cat1 order by cat1;

update production_ve.catalog_creation set `201208` = 0  where `201208` is null;

update production_ve.catalog_creation set `201209` = 0  where `201209` is null;

update production_ve.catalog_creation set `201210` = 0  where `201210` is null;

update production_ve.catalog_creation set `201211` = 0  where `201211` is null;

update production_ve.catalog_creation set `201212` = 0  where `201212` is null;

update production_ve.catalog_creation set `201301` = 0  where `201301` is null;

update production_ve.catalog_creation set `201302` = 0  where `201302` is null;

update production_ve.catalog_creation set `201303` = 0  where `201303` is null;

update production_ve.catalog_creation set `201304` = 0  where `201304` is null;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `catalog_history`()
begin

call production_ve.catalog_visible;

insert into production_ve.catalog_history(date, sku_config, sku_simple, product_name, status_config, status_simple, quantity, price) 
select curdate(), catalog_config.sku as sku_config, catalog_simple.sku as sku_simple, catalog_config.name, catalog_config.status, catalog_simple.status, catalog_stock.quantity, catalog_simple.price
from (bob_live_ve.catalog_config inner join bob_live_ve.catalog_simple on catalog_config.id_catalog_config = catalog_simple.fk_catalog_config) left join bob_live_ve.catalog_stock on catalog_simple.id_catalog_simple = catalog_stock.fk_catalog_simple;

update production_ve.catalog_history set quantity = 0  where quantity is null and date = curdate();

update production_ve.catalog_history c inner join production_ve.catalog_visible v on c.sku_simple=v.sku_simple set visible = 1 where date = curdate();

update production_ve.catalog_history set visible = 0  where visible is null and date = curdate();

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `catalog_visible`()
begin

delete from production_ve.catalog_visible;

insert into production_ve.catalog_visible(sku_config, sku_simple, pet_status, pet_approved, status_config, status_simple, name, display_if_out_of_stock, quantity, updated_at, activated_at, price) 
select catalog_config.sku as sku_config, catalog_simple.sku as sku_simple, catalog_config.pet_status, catalog_config.pet_approved, catalog_config.status, catalog_simple.status, catalog_config.name, catalog_config.display_if_out_of_stock, catalog_stock.quantity, catalog_config.updated_at, catalog_config.activated_at, catalog_simple.price
from (bob_live_ve.catalog_config inner join bob_live_ve.catalog_simple on catalog_config.id_catalog_config = catalog_simple.fk_catalog_config) inner join bob_live_ve.catalog_stock on catalog_simple.id_catalog_simple = catalog_stock.fk_catalog_simple
where (((catalog_config.pet_status)="creation,edited,images") and ((catalog_config.pet_approved)=1) and ((catalog_config.status)="active") and ((catalog_simple.status)="active") and ((catalog_config.display_if_out_of_stock)=0) and ((catalog_stock.quantity)>0) and ((catalog_simple.price)>0)) or (((catalog_config.pet_status)="creation,edited,images") and ((catalog_config.pet_approved)=1) and ((catalog_config.status)="active") and ((catalog_simple.status)="active") and ((catalog_config.display_if_out_of_stock)=1) and ((catalog_simple.price)>0));

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `channel_report`()
BEGIN


#Channel Report---VENEZUELA

#Define channel---
UPDATE tbl_order_detail SET source='',channel='', channel_group='';
#Voucher is priority statemnt
#If voucher is empty use GA source
UPDATE tbl_order_detail SET source=source_medium;
UPDATE tbl_order_detail SET source=coupon_code where source_medium is null;

#NEW REGISTERS
UPDATE tbl_order_detail SET channel='New Register' 
WHERE (source like 'NL%' OR source like 'NR%' OR source like
'COM%' OR source like 'BNR%') AND source_medium is null;
UPDATE tbl_order_detail SET source=source_medium
WHERE (coupon_code like 'NL%' OR coupon_code='SINIVA' OR coupon_code like 'COM%' 
OR coupon_code like 'NR%' OR coupon_code like 'BNR%')
 AND source_medium is not null;
#BLOG
UPDATE tbl_order_detail SET channel='Blog Linio' WHERE source like 'blog%' 
OR source='blog.linio.com.ve / referral' or source like 'BL%';

#REFERRAL
UPDATE tbl_order_detail SET channel='Referral Sites' WHERE source like '%referral%' 
AND source<>'facebook.com / referral'
AND source<>'m.facebook.com / referral'
AND source<>'linio.com.co / referral'
AND source<>'linio.com / referral'
AND source<>'linio.com.mx / referral'
AND source<>'google.co.ve / referral'
AND source<>'linio.com.ve / referral'
AND source<>'linio.com.pe / referral'
AND source<>'www-staging.linio.com.ve / referral'
AND source<>'google.com / referral'
AND source<>'.com.ve / referral'
AND source<>'blog.linio.com.ve / referral' AND source<>'t.co / referral'
AND source not like '%google%' AND source not like '%blog%';


UPDATE tbl_order_detail SET channel='Buscape' WHERE source='Buscape / Price Comparison';
#FB ADS
UPDATE tbl_order_detail SET channel='Facebook Ads' WHERE source='facebook / socialmediaads' or source='facebook / socialmediapaid';
#SEM - GDN
UPDATE tbl_order_detail SET channel='SEM Branded' WHERE source='google / cpc' AND campaign like '%linio%';
UPDATE tbl_order_detail SET channel='SEM' WHERE source='google / cpc' AND campaign not like '%linio%';

#GDN
UPDATE tbl_order_detail SET channel='Google Display Network' WHERE source='google / cpc' AND 
(campaign like 'r.%' or campaign like '[D%');

#RETARGETING
UPDATE tbl_order_detail SET channel='Sociomantic' WHERE source='sociomantic / (not set)' OR source='sociomantic / retargeting';
UPDATE tbl_order_detail SET channel='Vizury' WHERE source='vizury / retargeting' OR source='vizury.com / referral';
UPDATE tbl_order_detail SET channel='VEInteractive' WHERE source like '%VEInteractive%';

#RETARGETING
UPDATE tbl_order_detail SET channel='Facebook R.' WHERE source='facebook / retargeting';
#Criteo
UPDATE tbl_order_detail set channel='Criteo' where source like '%criteo%';
#PAMPA
UPDATE tbl_order_detail set channel='Pampa Network' where source like '%pampa%';
#SOCIAL MEDIA
UPDATE tbl_order_detail SET channel='Facebook Referral' WHERE 
(source='facebook.com / referral' OR source='m.facebook.com / referral') AND source not like '%socialmediaads%';
UPDATE tbl_order_detail SET channel='Twitter Referral' WHERE source='t.co / referral' OR source='twitter / socialmedia';
UPDATE tbl_order_detail SET channel='FB Posts' 
WHERE source='facebook / socialmedia' OR source='SocialMedia / FacebookVoucher';
#SERVICIO AL CLIENTE
UPDATE tbl_order_detail SET channel='Inbound' WHERE source like 'Tele%' or source like '%inbound%' or source like '%outbound%';
UPDATE tbl_order_detail SET channel='OutBound' WHERE source like 'REC%';
UPDATE tbl_order_detail SET channel='CS Generic Voucher' WHERE source like 'CCemp%';

UPDATE tbl_order_detail SET channel='Search Organic' WHERE source like '%organic' OR source='google.com.co / referral';
#NEWSLETTER
UPDATE tbl_order_detail set Channel='Newsletter' WHERE source like '%CRM' AND source<>'TeleSales / CRM';
UPDATE tbl_order_detail SET channel='MKT Bs. 50' WHERE source='MKT02kVLM';
UPDATE tbl_order_detail SET channel='MKT 15%' WHERE source='MKT048S8J';
UPDATE tbl_order_detail SET channel='MKT Bs. 50' WHERE source='MKT04GPXB';
UPDATE tbl_order_detail SET channel='MKT 15% hasta 31 de diciembre' WHERE source='MKT06Ru';
UPDATE tbl_order_detail SET channel='MKT Bs. 50' WHERE source='MKT07kiKz';
UPDATE tbl_order_detail SET channel='MKT 15%' WHERE source='MKT0a3YVI';
UPDATE tbl_order_detail SET channel='MKT 50Bs  hasta 31 de diciembre' WHERE source='MKT0FTh';
UPDATE tbl_order_detail SET channel='Tramontina Utilita Sacarcorcho' WHERE source='MKT0hAb7W';
UPDATE tbl_order_detail SET channel='Victorinox Memoria USB Slim 4GB Verde' WHERE source='MKT0iuXXU';
UPDATE tbl_order_detail SET channel='HP FlashDrive Pen 4GB Usb V195B Azul' WHERE source='MKT0j2Efo';
UPDATE tbl_order_detail SET channel='GBTech Audífono GBTech X5 GBX5 Amarillo' WHERE source='MKT0oShGI';
UPDATE tbl_order_detail SET channel='Perfect Choice Audifono Diadema PC-110309 Con Banda Para Cuello' WHERE source='MKT0P2Bsq';
UPDATE tbl_order_detail SET channel='MKT Bs. 50' WHERE source='MKT0qCcAP';
UPDATE tbl_order_detail SET channel='Decocar Combo Tropical Azul' WHERE source='MKT0rjeet';
UPDATE tbl_order_detail SET channel='MKT 15%' WHERE source='MKT0WFMCm';
UPDATE tbl_order_detail SET channel='Ocean Vaso Corto Studio Set 6' WHERE source='MKT0yj5mw';
UPDATE tbl_order_detail SET channel='MKT Bs. 50' WHERE source='MKT0Z8YYn';
UPDATE tbl_order_detail SET channel='X-mini Altavoz Capsule v1.1 Rojo' WHERE source='MKT10hQfI';
UPDATE tbl_order_detail SET channel='HP P-FD4GBHP165-GEL Pendrive USB' WHERE source='MKT11vw0x';
UPDATE tbl_order_detail SET channel='SABA Set de 2 Cuchillos Colores' WHERE source='MKT12jfHL';
UPDATE tbl_order_detail SET channel='MKT Bs. 50' WHERE source='MKT14zmPN';
UPDATE tbl_order_detail SET channel='Kingston DT101G2/4GB PenDrive USB' WHERE source='MKT16mWW7';
UPDATE tbl_order_detail SET channel='Ifrogz Audífono EarPollution Plugz Azul Cielo' WHERE source='MKT18qw1j';
UPDATE tbl_order_detail SET channel='MKT 30%' WHERE source='MKT1DcqIV';
UPDATE tbl_order_detail SET channel='GENIUS Speakers 31731006103 Blanco' WHERE source='MKT1Hn1B5';
UPDATE tbl_order_detail SET channel='Bajo Techo Pizarra Autoadhesiva Tipo Vinil' WHERE source='MKT1HWmFm';
UPDATE tbl_order_detail SET channel='MKT 15%' WHERE source='MKT1i3zA8';
UPDATE tbl_order_detail SET channel='MKT Bs. 50' WHERE source='MKT1ktP10';
UPDATE tbl_order_detail SET channel='Desarmador trompo con matraca con 12 puntas' WHERE source='MKT1ORfvU';
UPDATE tbl_order_detail SET channel='SABA – Taza de Porcelana Rayas (Facebook)' WHERE source='MKT1P4NbB';
UPDATE tbl_order_detail SET channel='MKT 15%' WHERE source='MKT1rTCoJ';
UPDATE tbl_order_detail SET channel='MKT Bs. 50' WHERE source='MKT1xLsRR';
UPDATE tbl_order_detail SET channel='Botella de Alumino para Agua No soy de plástico Gaiam-Plata' WHERE source='MKT1Z54Lh';
UPDATE tbl_order_detail SET channel='MKT 15% hasta 31 de diciembre' WHERE source='MKT1ZtV';
UPDATE tbl_order_detail SET channel='SABA – Taza de Porcelana Rayas ' WHERE source='MKT2ww6wJ';
UPDATE tbl_order_detail SET channel='SABA – Taza de Porcelana Rayas ' WHERE source='MKT7XzxhT';
UPDATE tbl_order_detail SET channel='Cupón de reemplazo a cupón de Mkt' WHERE source='MKTAeF';
UPDATE tbl_order_detail SET channel='MKT 15%' WHERE source='MKTbS3gmp';
UPDATE tbl_order_detail SET channel='MKT 15%' WHERE source='MKTcwB9vv';
UPDATE tbl_order_detail SET channel='SABA Porta Botellas Neopreno Negro' WHERE source='MKTdtlbpI';
UPDATE tbl_order_detail SET channel='ILUV Audífonos in ear Bubble Gum Rojo IEP205-R' WHERE source='MKTECOjZv';
UPDATE tbl_order_detail SET channel='Reemplazo cupón de Marketing' WHERE source='MKTenQ';
UPDATE tbl_order_detail SET channel='Tostador Proctor Silex 24605Y 4 Rebanadas-Blanco' WHERE source='MKTf4A6Sk';
UPDATE tbl_order_detail SET channel='HP P-FD4GBHP165-GEL Pendrive USB' WHERE source='MKTf8VV4U';
UPDATE tbl_order_detail SET channel='Morrocoy Silla de Director' WHERE source='MKTGOhXfE';
UPDATE tbl_order_detail SET channel='Kalosh Almohada 95% Pluma y 5% Plumón Blanca' WHERE source='MKTkZ047R';
UPDATE tbl_order_detail SET channel='MKT Bs. 50' WHERE source='MKTLf9Mdg';
UPDATE tbl_order_detail SET channel='Kingston Tarjeta de memoria SDC4/4GB-1ADP' WHERE source='MKTljJ3QL';
UPDATE tbl_order_detail SET channel='PRO CHEF Parrillera Hamburguer' WHERE source='MKTmCN60a';
UPDATE tbl_order_detail SET channel='Credito por bono no funcionando 2' WHERE source='MKTMKT';
UPDATE tbl_order_detail SET channel='Credito por bono no funcionando 3' WHERE source='MKTMKT250';
UPDATE tbl_order_detail SET channel='Contigo West loop Vaso Térmico con autosello 16 Oz/ 473 ml Rojo' WHERE source='MKToVzJ1d';
UPDATE tbl_order_detail SET channel='MKT 50Bs  hasta 31 de diciembre' WHERE source='MKTQaQk';
UPDATE tbl_order_detail SET channel='MKT 15%' WHERE source='MKTr1jvkH';
UPDATE tbl_order_detail SET channel='ARGOM Teclado multimedia en español ARG-KB-7805 Negro' WHERE source='MKTr2DzaC';
UPDATE tbl_order_detail SET channel='GBTech MP4 GBTech GB1106 Azul 4 GB' WHERE source='MKTRp7vye';
UPDATE tbl_order_detail SET channel='SABA Set de 3 utensilios para Parrilla' WHERE source='MKTsd9V9B';
UPDATE tbl_order_detail SET channel='Decocar Cava Americana 11,4Lt Roja' WHERE source='MKTthEw98';
UPDATE tbl_order_detail SET channel='MKT Bs. 50' WHERE source='MKTuWtOQ3';
UPDATE tbl_order_detail SET channel='Marketing en ferias' WHERE source='MKTVngFxL';
UPDATE tbl_order_detail SET channel='SABA - Dispensador de Aceite y Vinagre 2 en 1 / SABA-511 / Transparente' WHERE source='MKTvpeCte';
UPDATE tbl_order_detail SET channel='MKT 30%' WHERE source='MKTy6v5rt';
UPDATE tbl_order_detail SET channel='Easy Line Mouse Retráctil EL-993346 Negro' WHERE source='MKTzBMwc3';
UPDATE tbl_order_detail SET channel='Reactivation campaign' WHERE source='MKTzOcbUq';



#OTHER
UPDATE tbl_order_detail SET channel='Exchange' WHERE source like 'CCE%';
UPDATE tbl_order_detail SET channel='Content Mistake' WHERE source like 'CON%';
UPDATE tbl_order_detail SET channel='Procurement Mistake' WHERE source like 'PRC%';
UPDATE tbl_order_detail SET channel='Employee Vouchers' 
WHERE (source like 'TE%' AND source<>'TeleSales / CRM') OR source like 'EA%';


#MERCADO LIBRE
UPDATE tbl_order_detail SET channel='Mercado Libre Voucher' WHERE source like 'ML%';

#PERIODISTAS

UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT02kER0';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT08jq2i';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT0arRZF';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT0hoq3F';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT0JgIhn';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT0LeO2h';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT0Olwjk';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT1d8pkJ';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT1fspLY';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT1h2uie';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT1jzjKP';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT1llem9';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT1Pk3Ap';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT1u1JSo';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT1v5kLS';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT1Yz6yi';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKT4rXdCS';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTA0aIrT';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTac0eLL';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTCcHdRX';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKThMerN5';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKThNplZF';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTiE46NZ';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTJy4dxm';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTlzVdEY';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTmvE0F7';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTn7VdJO';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTna93IX';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTpYM3yV';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTQdcuzY';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTrI3ds7';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTS55mkn';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTSR8lJR';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTt1kyoi';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTTRutlc';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTuUrfbj';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTvkHUlg';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTvsJVH4';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTwdi2uO';
UPDATE tbl_order_detail SET channel='Cupón para periodistas' WHERE source='MKTXVRpth';





#PARTNERSHIPS

#DIRECT
UPDATE tbl_order_detail SET channel='Linio.com Referral' WHERE  
source='linio.com.co / referral' OR
source='linio.com / referral' OR
source='linio.com.mx / referral' OR
source='linio.com.ve / referral' OR
source='linio.com.pe / referral' OR
source='www-staging.linio.com.ve / referral';
UPDATE tbl_order_detail SET channel='Directo / Typed' WHERE  source='(direct) / (none)';



#CORPORATE SALES
UPDATE tbl_order_detail SET channel='Corporate Sales' WHERE source like 'CDEAL%';


#OFFLINE

#N/A
UPDATE tbl_order_detail SET channel='Unknown-No voucher' WHERE source='';
UPDATE tbl_order_detail SET channel='Unknown-Voucher' WHERE channel='' AND source<>'';

#Channel Report Extra Stuff to define Channel---
#Venta corporativa 



#Partnerships Channel Extra


#Mercado libre Orders 


#DEFINE CHANNEL GROUP

#Facebook Ads Group
UPDATE tbl_order_detail SET channel_group='Facebook Ads' WHERE channel='Facebook Ads';

#OTHER
UPDATE tbl_order_detail SET channel_group='Mercado Libre ' WHERE channel='Mercado Libre Voucher';

#SEM Group
UPDATE tbl_order_detail SET channel_group='Search Engine Marketing' WHERE channel='SEM';

#Social Media Group
UPDATE tbl_order_detail SET channel_group='Social Media' 
WHERE channel='Facebook Referral' OR channel='Twitter Referral' OR
channel='FB Posts';

#Retargeting Group
UPDATE tbl_order_detail SET channel_group='Retargeting' WHERE channel='Sociomantic' OR channel like '%retargeting'
OR channel='Facebook R.' OR channel='Vizury' OR channel = 'VEInteractive' OR channel = 'GDN Retargeting' OR channel = 'Criteo';

#GDN Group
UPDATE tbl_order_detail SET channel_group='Google Display Network' WHERE channel like 'GDN'; 

#NewsLetter Group
UPDATE tbl_order_detail SET channel_group='NewsLetter' WHERE channel='Newsletter';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 15% hasta 31 de diciembre';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 50Bs  hasta 31 de diciembre';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Tramontina Utilita Sacarcorcho';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Victorinox Memoria USB Slim 4GB Verde';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='HP FlashDrive Pen 4GB Usb V195B Azul';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='GBTech Audífono GBTech X5 GBX5 Amarillo';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Perfect Choice Audifono Diadema PC-110309 Con Banda Para Cuello';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Decocar Combo Tropical Azul';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Ocean Vaso Corto Studio Set 6';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='X-mini Altavoz Capsule v1.1 Rojo';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='HP P-FD4GBHP165-GEL Pendrive USB';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='SABA Set de 2 Cuchillos Colores';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Kingston DT101G2/4GB PenDrive USB';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Ifrogz Audífono EarPollution Plugz Azul Cielo';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 30%';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='GENIUS Speakers 31731006103 Blanco';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Bajo Techo Pizarra Autoadhesiva Tipo Vinil';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Desarmador trompo con matraca con 12 puntas';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='SABA – Taza de Porcelana Rayas (Facebook)';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Botella de Alumino para Agua No soy de plástico Gaiam-Plata';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 15% hasta 31 de diciembre';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='SABA – Taza de Porcelana Rayas ';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='SABA – Taza de Porcelana Rayas ';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Cupón de reemplazo a cupón de Mkt';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='SABA Porta Botellas Neopreno Negro';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='ILUV Audífonos in ear Bubble Gum Rojo IEP205-R';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Reemplazo cupón de Marketing';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Tostador Proctor Silex 24605Y 4 Rebanadas-Blanco';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='HP P-FD4GBHP165-GEL Pendrive USB';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Morrocoy Silla de Director';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Kalosh Almohada 95% Pluma y 5% Plumón Blanca';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Kingston Tarjeta de memoria SDC4/4GB-1ADP';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='PRO CHEF Parrillera Hamburguer';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Credito por bono no funcionando 2';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Credito por bono no funcionando 3';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Contigo West loop Vaso Térmico con autosello 16 Oz/ 473 ml Rojo';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 50Bs  hasta 31 de diciembre';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='ARGOM Teclado multimedia en español ARG-KB-7805 Negro';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='GBTech MP4 GBTech GB1106 Azul 4 GB';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='SABA Set de 3 utensilios para Parrilla';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Decocar Cava Americana 11,4Lt Roja';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Marketing en ferias';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='SABA - Dispensador de Aceite y Vinagre 2 en 1 / SABA-511 / Transparente';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='MKT 30%';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Easy Line Mouse Retráctil EL-993346 Negro';
UPDATE tbl_order_detail SET channel_group='Newsletter' WHERE channel='Reactivation campaign';


#SEO Group
UPDATE tbl_order_detail SET channel_group='Search Engine Optimization' WHERE channel='Search Organic';

#Partnerships Group

#CORPORATE SALES

UPDATE tbl_order_detail SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';

#Corporate Sales Group
UPDATE tbl_order_detail SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';

#Offline Group


#Tele Sales Group
UPDATE tbl_order_detail SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales' OR channel='CS Generic Voucher';

#Branded Group
UPDATE tbl_order_detail SET channel_group='Branded' 
WHERE channel='Linio.com Referral' OR channel='Directo / Typed' OR
channel='SEM Branded';

#ML Group
UPDATE tbl_order_detail SET channel_group='Mercado Libre' 
WHERE channel='Mercado Libre Referral' OR channel='Mercado Libre Voucher' 
OR channel='Otros - Mercado Libre';

#Blog Group
UPDATE tbl_order_detail SET channel_group='Blogs'
WHERE channel='Blog Linio';

#Affiliates Group
UPDATE tbl_order_detail SET channel_group='Affiliates' 
WHERE channel='Buscape' or channel='Pampa Network';


#Other (identified) Group
UPDATE tbl_order_detail SET channel_group='Other (identified)' 
WHERE channel='Referral Sites' OR channel='Reactivation Letters' 
OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' OR
channel='Exchange' OR
channel='Content Mistake' OR
channel='Procurement Mistake' OR
channel='Employee Voucher' OR channel='Cupón para periodistas';

#Extra Channel Group
update tbl_order_detail set channel_group='Google Display Network' where source_medium = 'Noticias24 / banner5A';
update tbl_order_detail set channel_group='Google Display Network' where source_medium = 'Noticias24 / intersticial';
update tbl_order_detail set channel_group='Google Display Network' where source_medium = 'Noticias24 / Display';
update tbl_order_detail set channel_group='Other (identified)' where source_medium = 'googleads.g.doubleclick.net / referral';
update tbl_order_detail set channel_group='Google Display Network' where source_medium = 'LaPatilla / banner650';
update tbl_order_detail set channel_group='Tele Sales' where source_medium = 'CS / Inbound';
update tbl_order_detail set channel_group='Tele Sales' where source_medium = 'CS / Outbound';
update tbl_order_detail set channel_group='Affilates' where source_medium = 'MercadoLibre / cpc';
update tbl_order_detail set channel_group='Google Display Network' where source_medium = 'LaPatilla / Display';
update tbl_order_detail set channel_group='Google Display Network' where source_medium = 'Noticias24 / banner';
update tbl_order_detail set channel_group='Other (identified)' where source_medium = 'google.co.ve / referral';

#Unknown Group
UPDATE tbl_order_detail SET channel_group='Non identified' 
WHERE channel='Unknown-No voucher' OR channel='Unknown-Voucher';

#Define Channel Type---
#TO BE DONE

#Ownership

UPDATE tbl_order_detail set Ownership='' where channel_group='Social Media';
UPDATE tbl_order_detail set Ownership='' where channel_group='NewsLetter';
UPDATE tbl_order_detail set Ownership='' where channel_group='Search Engine Optimization';
UPDATE tbl_order_detail set Ownership='' where channel_group='Blogs';
UPDATE tbl_order_detail set Ownership='' where channel_group='Offline Marketing';
UPDATE tbl_order_detail set Ownership='' where channel_group='Partnerships';
UPDATE tbl_order_detail set Ownership='' where channel_group='Mercado Libre';
UPDATE tbl_order_detail set Ownership='' where channel_group='Tele Sales';

SELECT 'Start: channel_report_no_voucher ', now();
call channel_report_no_voucher();

SELECT 'Start: visits_costs_channel ', now();
call visits_costs_channel();

-- call daily_marketing();
-- call monthly_marketing();

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `channel_report_no_voucher`()
BEGIN


#Channel Report---

#ga_cost_Campaign
select @last_date:=max(date) from ga_cost_campaign where source = 'vizury';

insert into ga_cost_campaign(date, source, medium, campaign, adCost)
SELECT date, "Vizury" as source, "Retargeting" as medium, "(not set)" as campaign, (sum(paid_price_after_vat) +sum(shipping_fee_after_vat) )*0.08 as cost
from tbl_order_detail
where source_medium like 'vizury /%' and obc = 1 and date > @last_date
group by date
order by date desc;

select @last_date:=max(date) from ga_cost_campaign where source like 'VEInteractive%';

insert into ga_cost_campaign(date, source, medium, campaign, adCost)
SELECT date, "VEInteractive" as source, "Retargeting" as medium, "(not set)" as campaign, (sum(paid_price_after_vat) +sum(shipping_fee_after_vat) )*0.085 as cost
from tbl_order_detail
where `source_medium` like 'VEInteractive%' and obc = 1 and date > @last_date
group by date
order by date desc;

update ga_visits_cost_source_medium set source_medium = concat( source, ' / ', medium) where source_medium is null;


#NEW REGISTERS
UPDATE ga_visits_cost_source_medium SET channel='New Register' 
WHERE (source_medium like 'NL%' OR source_medium like 'NR%' OR source_medium like
'COM%' OR source_medium like 'BNR%') AND source_medium is null;

UPDATE ga_visits_cost_source_medium SET channel='Blog Linio' WHERE source_medium like 'blog%' 
OR source_medium='blog.linio.com.ve / referral' or source_medium like 'BL%';

#REFERRAL
UPDATE ga_visits_cost_source_medium SET channel='Referral Sites' WHERE source_medium like '%referral' 
AND source_medium<>'facebook.com / referral'
AND source_medium<>'m.facebook.com / referral'
AND source_medium<>'linio.com.co / referral'
AND source_medium<>'linio.com / referral'
AND source_medium<>'linio.com.mx / referral'
AND source_medium<>'google.co.ve / referral'
AND source_medium<>'linio.com.ve / referral'
AND source_medium<>'linio.com.pe / referral'
AND source_medium<>'www-staging.linio.com.ve / referral'
AND source_medium<>'google.com / referral'
AND source_medium<>'.com.ve / referral'
AND source_medium<>'blog.linio.com.ve / referral' AND source_medium<>'t.co / referral'
AND source_medium not like '%google%';


UPDATE ga_visits_cost_source_medium SET channel='Buscape' WHERE source_medium='Buscape / Price Comparison';
#FB ADS
UPDATE ga_visits_cost_source_medium SET channel='Facebook Ads' WHERE source_medium='facebook / socialmediaads' or source_medium='facebook / socialmediapaid';
#SEM - GDN
UPDATE ga_visits_cost_source_medium SET channel='SEM Branded' WHERE source_medium='google / cpc'
 AND (campaign like 'brand%');
UPDATE ga_visits_cost_source_medium SET channel='SEM' WHERE source_medium='google / cpc' 
AND (campaign not like 'brand%');
UPDATE ga_visits_cost_source_medium SET channel='GDN' 
WHERE source_medium='google / cpc' AND campaign not like 'b.%' AND campaign like 'r.%';
UPDATE ga_visits_cost_source_medium SET channel='GDN' 
WHERE source like 'googleads.g.doubleclick.net%';
UPDATE ga_visits_cost_source_medium SET channel='GDN' 
WHERE source_medium='google / cpc' AND campaign like '[D[D%';

#RETARGETING
UPDATE ga_visits_cost_source_medium SET channel='Sociomantic' WHERE source_medium='sociomantic / (not set)' OR source_medium='sociomantic / retargeting';
UPDATE ga_visits_cost_source_medium SET channel='Vizury' WHERE source_medium='vizury / retargeting';
UPDATE ga_visits_cost_source_medium SET channel='VEInteractive' WHERE source like '%VEInteractive%';
UPDATE ga_visits_cost_source_medium SET channel='GDN Retargeting' 
WHERE source_medium='google / cpc' AND campaign like '[D[R%';
#RETARGETING
UPDATE ga_visits_cost_source_medium SET channel='Facebook R.' WHERE source_medium='facebook / retargeting';
#SOCIAL MEDIA
UPDATE ga_visits_cost_source_medium SET channel='Facebook Referral' WHERE 
(source_medium='facebook.com / referral' OR source_medium='m.facebook.com / referral') AND source_medium not like '%socialmediaads%';
UPDATE ga_visits_cost_source_medium SET channel='Twitter Referral' WHERE source_medium='t.co / referral' OR source_medium='twitter / socialmedia';
UPDATE ga_visits_cost_source_medium SET channel='FB Posts' 
WHERE source_medium='facebook / socialmedia' OR source_medium='SocialMedia / FacebookVoucher';
#SERVICIO AL CLIENTE
UPDATE ga_visits_cost_source_medium SET channel='Inbound' WHERE source_medium like 'Tele%';
UPDATE ga_visits_cost_source_medium SET channel='OutBound' WHERE source_medium like 'REC%';
UPDATE ga_visits_cost_source_medium SET channel='CS Generic Voucher' WHERE source_medium like 'CCemp%';

UPDATE ga_visits_cost_source_medium SET channel='Search Organic' WHERE source_medium like '%organic' OR source_medium like 'google.% / referral';
#NEWSLETTER
UPDATE ga_visits_cost_source_medium set Channel='Newsletter' WHERE source_medium like '%CRM' AND source_medium<>'TeleSales / CRM';
UPDATE ga_visits_cost_source_medium SET channel='MKT Bs. 50' WHERE source_medium='MKT02kVLM';
UPDATE ga_visits_cost_source_medium SET channel='MKT 15%' WHERE source_medium='MKT048S8J';
UPDATE ga_visits_cost_source_medium SET channel='MKT Bs. 50' WHERE source_medium='MKT04GPXB';
UPDATE ga_visits_cost_source_medium SET channel='MKT 15% hasta 31 de diciembre' WHERE source_medium='MKT06Ru';
UPDATE ga_visits_cost_source_medium SET channel='MKT Bs. 50' WHERE source_medium='MKT07kiKz';
UPDATE ga_visits_cost_source_medium SET channel='MKT 15%' WHERE source_medium='MKT0a3YVI';
UPDATE ga_visits_cost_source_medium SET channel='MKT 50Bs  hasta 31 de diciembre' WHERE source_medium='MKT0FTh';
UPDATE ga_visits_cost_source_medium SET channel='Tramontina Utilita Sacarcorcho' WHERE source_medium='MKT0hAb7W';
UPDATE ga_visits_cost_source_medium SET channel='Victorinox Memoria USB Slim 4GB Verde' WHERE source_medium='MKT0iuXXU';
UPDATE ga_visits_cost_source_medium SET channel='HP FlashDrive Pen 4GB Usb V195B Azul' WHERE source_medium='MKT0j2Efo';
UPDATE ga_visits_cost_source_medium SET channel='GBTech Audífono GBTech X5 GBX5 Amarillo' WHERE source_medium='MKT0oShGI';
UPDATE ga_visits_cost_source_medium SET channel='Perfect Choice Audifono Diadema PC-110309 Con Banda Para Cuello' WHERE source_medium='MKT0P2Bsq';
UPDATE ga_visits_cost_source_medium SET channel='MKT Bs. 50' WHERE source_medium='MKT0qCcAP';
UPDATE ga_visits_cost_source_medium SET channel='Decocar Combo Tropical Azul' WHERE source_medium='MKT0rjeet';
UPDATE ga_visits_cost_source_medium SET channel='MKT 15%' WHERE source_medium='MKT0WFMCm';
UPDATE ga_visits_cost_source_medium SET channel='Ocean Vaso Corto Studio Set 6' WHERE source_medium='MKT0yj5mw';
UPDATE ga_visits_cost_source_medium SET channel='MKT Bs. 50' WHERE source_medium='MKT0Z8YYn';
UPDATE ga_visits_cost_source_medium SET channel='X-mini Altavoz Capsule v1.1 Rojo' WHERE source_medium='MKT10hQfI';
UPDATE ga_visits_cost_source_medium SET channel='HP P-FD4GBHP165-GEL Pendrive USB' WHERE source_medium='MKT11vw0x';
UPDATE ga_visits_cost_source_medium SET channel='SABA Set de 2 Cuchillos Colores' WHERE source_medium='MKT12jfHL';
UPDATE ga_visits_cost_source_medium SET channel='MKT Bs. 50' WHERE source_medium='MKT14zmPN';
UPDATE ga_visits_cost_source_medium SET channel='Kingston DT101G2/4GB PenDrive USB' WHERE source_medium='MKT16mWW7';
UPDATE ga_visits_cost_source_medium SET channel='Ifrogz Audífono EarPollution Plugz Azul Cielo' WHERE source_medium='MKT18qw1j';
UPDATE ga_visits_cost_source_medium SET channel='MKT 30%' WHERE source_medium='MKT1DcqIV';
UPDATE ga_visits_cost_source_medium SET channel='GENIUS Speakers 31731006103 Blanco' WHERE source_medium='MKT1Hn1B5';
UPDATE ga_visits_cost_source_medium SET channel='Bajo Techo Pizarra Autoadhesiva Tipo Vinil' WHERE source_medium='MKT1HWmFm';
UPDATE ga_visits_cost_source_medium SET channel='MKT 15%' WHERE source_medium='MKT1i3zA8';
UPDATE ga_visits_cost_source_medium SET channel='MKT Bs. 50' WHERE source_medium='MKT1ktP10';
UPDATE ga_visits_cost_source_medium SET channel='Desarmador trompo con matraca con 12 puntas' WHERE source_medium='MKT1ORfvU';
UPDATE ga_visits_cost_source_medium SET channel='SABA – Taza de Porcelana Rayas (Facebook)' WHERE source_medium='MKT1P4NbB';
UPDATE ga_visits_cost_source_medium SET channel='MKT 15%' WHERE source_medium='MKT1rTCoJ';
UPDATE ga_visits_cost_source_medium SET channel='MKT Bs. 50' WHERE source_medium='MKT1xLsRR';
UPDATE ga_visits_cost_source_medium SET channel='Botella de Alumino para Agua No soy de plástico Gaiam-Plata' WHERE source_medium='MKT1Z54Lh';
UPDATE ga_visits_cost_source_medium SET channel='MKT 15% hasta 31 de diciembre' WHERE source_medium='MKT1ZtV';
UPDATE ga_visits_cost_source_medium SET channel='SABA – Taza de Porcelana Rayas ' WHERE source_medium='MKT2ww6wJ';
UPDATE ga_visits_cost_source_medium SET channel='SABA – Taza de Porcelana Rayas ' WHERE source_medium='MKT7XzxhT';
UPDATE ga_visits_cost_source_medium SET channel='Cupón de reemplazo a cupón de Mkt' WHERE source_medium='MKTAeF';
UPDATE ga_visits_cost_source_medium SET channel='MKT 15%' WHERE source_medium='MKTbS3gmp';
UPDATE ga_visits_cost_source_medium SET channel='MKT 15%' WHERE source_medium='MKTcwB9vv';
UPDATE ga_visits_cost_source_medium SET channel='SABA Porta Botellas Neopreno Negro' WHERE source_medium='MKTdtlbpI';
UPDATE ga_visits_cost_source_medium SET channel='ILUV Audífonos in ear Bubble Gum Rojo IEP205-R' WHERE source_medium='MKTECOjZv';
UPDATE ga_visits_cost_source_medium SET channel='Reemplazo cupón de Marketing' WHERE source_medium='MKTenQ';
UPDATE ga_visits_cost_source_medium SET channel='Tostador Proctor Silex 24605Y 4 Rebanadas-Blanco' WHERE source_medium='MKTf4A6Sk';
UPDATE ga_visits_cost_source_medium SET channel='HP P-FD4GBHP165-GEL Pendrive USB' WHERE source_medium='MKTf8VV4U';
UPDATE ga_visits_cost_source_medium SET channel='Morrocoy Silla de Director' WHERE source_medium='MKTGOhXfE';
UPDATE ga_visits_cost_source_medium SET channel='Kalosh Almohada 95% Pluma y 5% Plumón Blanca' WHERE source_medium='MKTkZ047R';
UPDATE ga_visits_cost_source_medium SET channel='MKT Bs. 50' WHERE source_medium='MKTLf9Mdg';
UPDATE ga_visits_cost_source_medium SET channel='Kingston Tarjeta de memoria SDC4/4GB-1ADP' WHERE source_medium='MKTljJ3QL';
UPDATE ga_visits_cost_source_medium SET channel='PRO CHEF Parrillera Hamburguer' WHERE source_medium='MKTmCN60a';
UPDATE ga_visits_cost_source_medium SET channel='Credito por bono no funcionando 2' WHERE source_medium='MKTMKT';
UPDATE ga_visits_cost_source_medium SET channel='Credito por bono no funcionando 3' WHERE source_medium='MKTMKT250';
UPDATE ga_visits_cost_source_medium SET channel='Contigo West loop Vaso Térmico con autosello 16 Oz/ 473 ml Rojo' WHERE source_medium='MKToVzJ1d';
UPDATE ga_visits_cost_source_medium SET channel='MKT 50Bs  hasta 31 de diciembre' WHERE source_medium='MKTQaQk';
UPDATE ga_visits_cost_source_medium SET channel='MKT 15%' WHERE source_medium='MKTr1jvkH';
UPDATE ga_visits_cost_source_medium SET channel='ARGOM Teclado multimedia en español ARG-KB-7805 Negro' WHERE source_medium='MKTr2DzaC';
UPDATE ga_visits_cost_source_medium SET channel='GBTech MP4 GBTech GB1106 Azul 4 GB' WHERE source_medium='MKTRp7vye';
UPDATE ga_visits_cost_source_medium SET channel='SABA Set de 3 utensilios para Parrilla' WHERE source_medium='MKTsd9V9B';
UPDATE ga_visits_cost_source_medium SET channel='Decocar Cava Americana 11,4Lt Roja' WHERE source_medium='MKTthEw98';
UPDATE ga_visits_cost_source_medium SET channel='MKT Bs. 50' WHERE source_medium='MKTuWtOQ3';
UPDATE ga_visits_cost_source_medium SET channel='Marketing en ferias' WHERE source_medium='MKTVngFxL';
UPDATE ga_visits_cost_source_medium SET channel='SABA - Dispensador de Aceite y Vinagre 2 en 1 / SABA-511 / Transparente' WHERE source_medium='MKTvpeCte';
UPDATE ga_visits_cost_source_medium SET channel='MKT 30%' WHERE source_medium='MKTy6v5rt';
UPDATE ga_visits_cost_source_medium SET channel='Easy Line Mouse Retráctil EL-993346 Negro' WHERE source_medium='MKTzBMwc3';
UPDATE ga_visits_cost_source_medium SET channel='Reactivation campaign' WHERE source_medium='MKTzOcbUq';



#OTHER
UPDATE ga_visits_cost_source_medium SET channel='Exchange' WHERE source_medium like 'CCE%';
UPDATE ga_visits_cost_source_medium SET channel='Content Mistake' WHERE source_medium like 'CON%';
UPDATE ga_visits_cost_source_medium SET channel='Procurement Mistake' WHERE source_medium like 'PRC%';
UPDATE ga_visits_cost_source_medium SET channel='Employee Vouchers' 
WHERE (source_medium like 'TE%' AND source_medium<>'TeleSales / CRM') OR source_medium like 'EA%';


#MERCADO LIBRE
UPDATE ga_visits_cost_source_medium SET channel='Mercado Libre Voucher' WHERE source_medium like 'ML%';

#PERIODISTAS

UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT02kER0';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT08jq2i';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT0arRZF';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT0hoq3F';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT0JgIhn';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT0LeO2h';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT0Olwjk';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT1d8pkJ';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT1fspLY';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT1h2uie';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT1jzjKP';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT1llem9';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT1Pk3Ap';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT1u1JSo';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT1v5kLS';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT1Yz6yi';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKT4rXdCS';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTA0aIrT';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTac0eLL';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTCcHdRX';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKThMerN5';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKThNplZF';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTiE46NZ';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTJy4dxm';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTlzVdEY';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTmvE0F7';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTn7VdJO';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTna93IX';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTpYM3yV';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTQdcuzY';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTrI3ds7';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTS55mkn';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTSR8lJR';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTt1kyoi';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTTRutlc';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTuUrfbj';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTvkHUlg';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTvsJVH4';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTwdi2uO';
UPDATE ga_visits_cost_source_medium SET channel='Cupón para periodistas' WHERE source_medium='MKTXVRpth';





#PARTNERSHIPS

#DIRECT
UPDATE ga_visits_cost_source_medium SET channel='Linio.com Referral' WHERE  
source_medium='linio.com.co / referral' OR
source_medium='linio.com / referral' OR
source_medium='linio.com.mx / referral' OR
source_medium='linio.com.ve / referral' OR
source_medium='linio.com.pe / referral' OR
source_medium='www-staging.linio.com.ve / referral';
UPDATE ga_visits_cost_source_medium SET channel='Directo / Typed' WHERE  source_medium='(direct) / (none)';



#CORPORATE SALES
UPDATE ga_visits_cost_source_medium SET channel='Corporate Sales' WHERE source_medium like 'CDEAL%';


#OFFLINE

#N/A
UPDATE ga_visits_cost_source_medium SET channel='Unknown-No voucher' WHERE source_medium='';
UPDATE ga_visits_cost_source_medium SET channel='Unknown-Voucher' WHERE (channel='' AND source_medium<>'') or channel is null;

#Channel Report Extra Stuff to define Channel---
#Venta corporativa 



#Partnerships Channel Extra


#Mercado libre Orders 


#DEFINE CHANNEL GROUP

#Facebook Ads Group
UPDATE ga_visits_cost_source_medium SET channel_group='Facebook Ads' WHERE channel='Facebook Ads';

#OTHER
UPDATE ga_visits_cost_source_medium SET channel_group='Mercado Libre ' WHERE channel='Mercado Libre Voucher';

#SEM Group
UPDATE ga_visits_cost_source_medium SET channel_group='Search Engine Marketing' WHERE channel='SEM';

#Social Media Group
UPDATE ga_visits_cost_source_medium SET channel_group='Social Media' 
WHERE channel='Facebook Referral' OR channel='Twitter Referral' OR
channel='FB Posts';

#Retargeting Group
UPDATE ga_visits_cost_source_medium SET channel_group='Retargeting' WHERE channel='Sociomantic' OR channel like '%retargeting'
OR channel='Facebook R.' OR channel='Vizury'  OR channel = 'VEInteractive' OR channel = 'GDN Retargeting';

#GDN Group
UPDATE ga_visits_cost_source_medium SET channel_group='Google Display Network' WHERE channel like 'GDN'; 

#NewsLetter Group
UPDATE ga_visits_cost_source_medium SET channel_group='NewsLetter' WHERE channel='Newsletter';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 15% hasta 31 de diciembre';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 50Bs  hasta 31 de diciembre';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Tramontina Utilita Sacarcorcho';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Victorinox Memoria USB Slim 4GB Verde';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='HP FlashDrive Pen 4GB Usb V195B Azul';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='GBTech Audífono GBTech X5 GBX5 Amarillo';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Perfect Choice Audifono Diadema PC-110309 Con Banda Para Cuello';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Decocar Combo Tropical Azul';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Ocean Vaso Corto Studio Set 6';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='X-mini Altavoz Capsule v1.1 Rojo';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='HP P-FD4GBHP165-GEL Pendrive USB';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='SABA Set de 2 Cuchillos Colores';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Kingston DT101G2/4GB PenDrive USB';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Ifrogz Audífono EarPollution Plugz Azul Cielo';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 30%';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='GENIUS Speakers 31731006103 Blanco';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Bajo Techo Pizarra Autoadhesiva Tipo Vinil';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Desarmador trompo con matraca con 12 puntas';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='SABA – Taza de Porcelana Rayas (Facebook)';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Botella de Alumino para Agua No soy de plástico Gaiam-Plata';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 15% hasta 31 de diciembre';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='SABA – Taza de Porcelana Rayas ';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='SABA – Taza de Porcelana Rayas ';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Cupón de reemplazo a cupón de Mkt';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='SABA Porta Botellas Neopreno Negro';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='ILUV Audífonos in ear Bubble Gum Rojo IEP205-R';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Reemplazo cupón de Marketing';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Tostador Proctor Silex 24605Y 4 Rebanadas-Blanco';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='HP P-FD4GBHP165-GEL Pendrive USB';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Morrocoy Silla de Director';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Kalosh Almohada 95% Pluma y 5% Plumón Blanca';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Kingston Tarjeta de memoria SDC4/4GB-1ADP';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='PRO CHEF Parrillera Hamburguer';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Credito por bono no funcionando 2';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Credito por bono no funcionando 3';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Contigo West loop Vaso Térmico con autosello 16 Oz/ 473 ml Rojo';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 50Bs  hasta 31 de diciembre';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='ARGOM Teclado multimedia en español ARG-KB-7805 Negro';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='GBTech MP4 GBTech GB1106 Azul 4 GB';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='SABA Set de 3 utensilios para Parrilla';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Decocar Cava Americana 11,4Lt Roja';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Marketing en ferias';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='SABA - Dispensador de Aceite y Vinagre 2 en 1 / SABA-511 / Transparente';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='MKT 30%';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Easy Line Mouse Retráctil EL-993346 Negro';
UPDATE ga_visits_cost_source_medium SET channel_group='Newsletter' WHERE channel='Reactivation campaign';


#SEO Group
UPDATE ga_visits_cost_source_medium SET channel_group='Search Engine Optimization' WHERE channel='Search Organic';

#Partnerships Group

#CORPORATE SALES

UPDATE ga_visits_cost_source_medium SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';

#Corporate Sales Group
UPDATE ga_visits_cost_source_medium SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';

#Offline Group


#Tele Sales Group
UPDATE ga_visits_cost_source_medium SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales' OR channel='CS Generic Voucher';

#Branded Group
UPDATE ga_visits_cost_source_medium SET channel_group='Branded' 
WHERE channel='Linio.com Referral' OR channel='Directo / Typed' OR
channel='SEM Branded';

#ML Group
UPDATE ga_visits_cost_source_medium SET channel_group='Mercado Libre' 
WHERE channel='Mercado Libre Referral' OR channel='Mercado Libre Voucher' 
OR channel='Otros - Mercado Libre';

#Blog Group
UPDATE ga_visits_cost_source_medium SET channel_group='Blogs'
WHERE channel='Blog Linio';

#Affiliates Group
UPDATE ga_visits_cost_source_medium SET channel_group='Affiliates' 
WHERE channel='Buscape';


#Other (identified) Group
UPDATE ga_visits_cost_source_medium SET channel_group='Other (identified)' 
WHERE channel='Referral Sites' OR channel='Reactivation Letters' 
OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' OR
channel='Exchange' OR
channel='Content Mistake' OR
channel='Procurement Mistake' OR
channel='Employee Voucher' OR channel='Cupón para periodistas';



#Unknown Group
UPDATE ga_visits_cost_source_medium SET channel_group='Non identified' 
WHERE channel='Unknown-No voucher' OR channel='Unknown-Voucher';

#Define Channel Type---
#TO BE DONE


#Channel Report---

update ga_cost_campaign set source_medium = concat( source, ' / ', medium) where source_medium is null;


#NEW REGISTERS
UPDATE ga_cost_campaign SET channel='New Register' 
WHERE (source_medium like 'NL%' OR source_medium like 'NR%' OR source_medium like
'COM%' OR source_medium like 'BNR%') AND source_medium is null;

UPDATE ga_cost_campaign SET channel='Blog Linio' WHERE source_medium like 'blog%' 
OR source_medium='blog.linio.com.ve / referral' or source_medium like 'BL%';

#REFERRAL
UPDATE ga_cost_campaign SET channel='Referral Sites' WHERE source_medium like '%referral' 
AND source_medium<>'facebook.com / referral'
AND source_medium<>'m.facebook.com / referral'
AND source_medium<>'linio.com.co / referral'
AND source_medium<>'linio.com / referral'
AND source_medium<>'linio.com.mx / referral'
AND source_medium<>'google.co.ve / referral'
AND source_medium<>'linio.com.ve / referral'
AND source_medium<>'linio.com.pe / referral'
AND source_medium<>'www-staging.linio.com.ve / referral'
AND source_medium<>'google.com / referral'
AND source_medium<>'.com.ve / referral'
AND source_medium<>'blog.linio.com.ve / referral' AND source_medium<>'t.co / referral'
AND source_medium not like '%google%';


UPDATE ga_cost_campaign SET channel='Buscape' WHERE source_medium='Buscape / Price Comparison';
#FB ADS
UPDATE ga_cost_campaign SET channel='Facebook Ads' WHERE source_medium='facebook / socialmediaads' or source_medium='facebook / socialmediapaid';
#SEM - GDN
UPDATE ga_cost_campaign SET channel='SEM Branded' WHERE source_medium='google / cpc' AND (campaign like 'brand%');
UPDATE ga_cost_campaign SET channel='SEM' WHERE source_medium='google / cpc' AND (campaign not like 'brand%' );
UPDATE ga_cost_campaign SET channel='GDN' WHERE source_medium='google / cpc' AND campaign not like 'b.%' AND campaign like 'r.%';
UPDATE ga_cost_campaign SET channel='GDN' WHERE source like 'googleads.g.doubleclick.net%';
UPDATE ga_cost_campaign SET channel='GDN' 
WHERE source_medium='google / cpc' AND campaign like '[D[D%';
#RETARGETING
UPDATE ga_cost_campaign SET channel='Sociomantic' WHERE source_medium='sociomantic / (not set)' OR source_medium='sociomantic / retargeting';
UPDATE ga_cost_campaign SET channel='Vizury' WHERE source_medium='vizury / retargeting';
UPDATE ga_cost_campaign SET channel='VEInteractive' WHERE source like '%VEInteractive%';
UPDATE ga_cost_campaign SET channel='GDN Retargeting' 
WHERE source_medium='google / cpc' AND campaign like '[D[R%';
#RETARGETING
UPDATE ga_cost_campaign SET channel='Facebook R.' WHERE source_medium='facebook / retargeting';
#SOCIAL MEDIA
UPDATE ga_cost_campaign SET channel='Facebook Referral' WHERE 
(source_medium='facebook.com / referral' OR source_medium='m.facebook.com / referral') AND source_medium not like '%socialmediaads%';
UPDATE ga_cost_campaign SET channel='Twitter Referral' WHERE source_medium='t.co / referral' OR source_medium='twitter / socialmedia';
UPDATE ga_cost_campaign SET channel='FB Posts' 
WHERE source_medium='facebook / socialmedia' OR source_medium='SocialMedia / FacebookVoucher';
#SERVICIO AL CLIENTE
UPDATE ga_cost_campaign SET channel='Inbound' WHERE source_medium like 'Tele%';
UPDATE ga_cost_campaign SET channel='OutBound' WHERE source_medium like 'REC%';
UPDATE ga_cost_campaign SET channel='CS Generic Voucher' WHERE source_medium like 'CCemp%';

UPDATE ga_cost_campaign SET channel='Search Organic' WHERE source_medium like '%organic' OR source_medium like 'google.% / referral';
#NEWSLETTER
UPDATE ga_cost_campaign set Channel='Newsletter' WHERE source_medium like '%CRM' AND source_medium<>'TeleSales / CRM';
UPDATE ga_cost_campaign SET channel='MKT Bs. 50' WHERE source_medium='MKT02kVLM';
UPDATE ga_cost_campaign SET channel='MKT 15%' WHERE source_medium='MKT048S8J';
UPDATE ga_cost_campaign SET channel='MKT Bs. 50' WHERE source_medium='MKT04GPXB';
UPDATE ga_cost_campaign SET channel='MKT 15% hasta 31 de diciembre' WHERE source_medium='MKT06Ru';
UPDATE ga_cost_campaign SET channel='MKT Bs. 50' WHERE source_medium='MKT07kiKz';
UPDATE ga_cost_campaign SET channel='MKT 15%' WHERE source_medium='MKT0a3YVI';
UPDATE ga_cost_campaign SET channel='MKT 50Bs  hasta 31 de diciembre' WHERE source_medium='MKT0FTh';
UPDATE ga_cost_campaign SET channel='Tramontina Utilita Sacarcorcho' WHERE source_medium='MKT0hAb7W';
UPDATE ga_cost_campaign SET channel='Victorinox Memoria USB Slim 4GB Verde' WHERE source_medium='MKT0iuXXU';
UPDATE ga_cost_campaign SET channel='HP FlashDrive Pen 4GB Usb V195B Azul' WHERE source_medium='MKT0j2Efo';
UPDATE ga_cost_campaign SET channel='GBTech Audífono GBTech X5 GBX5 Amarillo' WHERE source_medium='MKT0oShGI';
UPDATE ga_cost_campaign SET channel='Perfect Choice Audifono Diadema PC-110309 Con Banda Para Cuello' WHERE source_medium='MKT0P2Bsq';
UPDATE ga_cost_campaign SET channel='MKT Bs. 50' WHERE source_medium='MKT0qCcAP';
UPDATE ga_cost_campaign SET channel='Decocar Combo Tropical Azul' WHERE source_medium='MKT0rjeet';
UPDATE ga_cost_campaign SET channel='MKT 15%' WHERE source_medium='MKT0WFMCm';
UPDATE ga_cost_campaign SET channel='Ocean Vaso Corto Studio Set 6' WHERE source_medium='MKT0yj5mw';
UPDATE ga_cost_campaign SET channel='MKT Bs. 50' WHERE source_medium='MKT0Z8YYn';
UPDATE ga_cost_campaign SET channel='X-mini Altavoz Capsule v1.1 Rojo' WHERE source_medium='MKT10hQfI';
UPDATE ga_cost_campaign SET channel='HP P-FD4GBHP165-GEL Pendrive USB' WHERE source_medium='MKT11vw0x';
UPDATE ga_cost_campaign SET channel='SABA Set de 2 Cuchillos Colores' WHERE source_medium='MKT12jfHL';
UPDATE ga_cost_campaign SET channel='MKT Bs. 50' WHERE source_medium='MKT14zmPN';
UPDATE ga_cost_campaign SET channel='Kingston DT101G2/4GB PenDrive USB' WHERE source_medium='MKT16mWW7';
UPDATE ga_cost_campaign SET channel='Ifrogz Audífono EarPollution Plugz Azul Cielo' WHERE source_medium='MKT18qw1j';
UPDATE ga_cost_campaign SET channel='MKT 30%' WHERE source_medium='MKT1DcqIV';
UPDATE ga_cost_campaign SET channel='GENIUS Speakers 31731006103 Blanco' WHERE source_medium='MKT1Hn1B5';
UPDATE ga_cost_campaign SET channel='Bajo Techo Pizarra Autoadhesiva Tipo Vinil' WHERE source_medium='MKT1HWmFm';
UPDATE ga_cost_campaign SET channel='MKT 15%' WHERE source_medium='MKT1i3zA8';
UPDATE ga_cost_campaign SET channel='MKT Bs. 50' WHERE source_medium='MKT1ktP10';
UPDATE ga_cost_campaign SET channel='Desarmador trompo con matraca con 12 puntas' WHERE source_medium='MKT1ORfvU';
UPDATE ga_cost_campaign SET channel='SABA – Taza de Porcelana Rayas (Facebook)' WHERE source_medium='MKT1P4NbB';
UPDATE ga_cost_campaign SET channel='MKT 15%' WHERE source_medium='MKT1rTCoJ';
UPDATE ga_cost_campaign SET channel='MKT Bs. 50' WHERE source_medium='MKT1xLsRR';
UPDATE ga_cost_campaign SET channel='Botella de Alumino para Agua No soy de plástico Gaiam-Plata' WHERE source_medium='MKT1Z54Lh';
UPDATE ga_cost_campaign SET channel='MKT 15% hasta 31 de diciembre' WHERE source_medium='MKT1ZtV';
UPDATE ga_cost_campaign SET channel='SABA – Taza de Porcelana Rayas ' WHERE source_medium='MKT2ww6wJ';
UPDATE ga_cost_campaign SET channel='SABA – Taza de Porcelana Rayas ' WHERE source_medium='MKT7XzxhT';
UPDATE ga_cost_campaign SET channel='Cupón de reemplazo a cupón de Mkt' WHERE source_medium='MKTAeF';
UPDATE ga_cost_campaign SET channel='MKT 15%' WHERE source_medium='MKTbS3gmp';
UPDATE ga_cost_campaign SET channel='MKT 15%' WHERE source_medium='MKTcwB9vv';
UPDATE ga_cost_campaign SET channel='SABA Porta Botellas Neopreno Negro' WHERE source_medium='MKTdtlbpI';
UPDATE ga_cost_campaign SET channel='ILUV Audífonos in ear Bubble Gum Rojo IEP205-R' WHERE source_medium='MKTECOjZv';
UPDATE ga_cost_campaign SET channel='Reemplazo cupón de Marketing' WHERE source_medium='MKTenQ';
UPDATE ga_cost_campaign SET channel='Tostador Proctor Silex 24605Y 4 Rebanadas-Blanco' WHERE source_medium='MKTf4A6Sk';
UPDATE ga_cost_campaign SET channel='HP P-FD4GBHP165-GEL Pendrive USB' WHERE source_medium='MKTf8VV4U';
UPDATE ga_cost_campaign SET channel='Morrocoy Silla de Director' WHERE source_medium='MKTGOhXfE';
UPDATE ga_cost_campaign SET channel='Kalosh Almohada 95% Pluma y 5% Plumón Blanca' WHERE source_medium='MKTkZ047R';
UPDATE ga_cost_campaign SET channel='MKT Bs. 50' WHERE source_medium='MKTLf9Mdg';
UPDATE ga_cost_campaign SET channel='Kingston Tarjeta de memoria SDC4/4GB-1ADP' WHERE source_medium='MKTljJ3QL';
UPDATE ga_cost_campaign SET channel='PRO CHEF Parrillera Hamburguer' WHERE source_medium='MKTmCN60a';
UPDATE ga_cost_campaign SET channel='Credito por bono no funcionando 2' WHERE source_medium='MKTMKT';
UPDATE ga_cost_campaign SET channel='Credito por bono no funcionando 3' WHERE source_medium='MKTMKT250';
UPDATE ga_cost_campaign SET channel='Contigo West loop Vaso Térmico con autosello 16 Oz/ 473 ml Rojo' WHERE source_medium='MKToVzJ1d';
UPDATE ga_cost_campaign SET channel='MKT 50Bs  hasta 31 de diciembre' WHERE source_medium='MKTQaQk';
UPDATE ga_cost_campaign SET channel='MKT 15%' WHERE source_medium='MKTr1jvkH';
UPDATE ga_cost_campaign SET channel='ARGOM Teclado multimedia en español ARG-KB-7805 Negro' WHERE source_medium='MKTr2DzaC';
UPDATE ga_cost_campaign SET channel='GBTech MP4 GBTech GB1106 Azul 4 GB' WHERE source_medium='MKTRp7vye';
UPDATE ga_cost_campaign SET channel='SABA Set de 3 utensilios para Parrilla' WHERE source_medium='MKTsd9V9B';
UPDATE ga_cost_campaign SET channel='Decocar Cava Americana 11,4Lt Roja' WHERE source_medium='MKTthEw98';
UPDATE ga_cost_campaign SET channel='MKT Bs. 50' WHERE source_medium='MKTuWtOQ3';
UPDATE ga_cost_campaign SET channel='Marketing en ferias' WHERE source_medium='MKTVngFxL';
UPDATE ga_cost_campaign SET channel='SABA - Dispensador de Aceite y Vinagre 2 en 1 / SABA-511 / Transparente' WHERE source_medium='MKTvpeCte';
UPDATE ga_cost_campaign SET channel='MKT 30%' WHERE source_medium='MKTy6v5rt';
UPDATE ga_cost_campaign SET channel='Easy Line Mouse Retráctil EL-993346 Negro' WHERE source_medium='MKTzBMwc3';
UPDATE ga_cost_campaign SET channel='Reactivation campaign' WHERE source_medium='MKTzOcbUq';



#OTHER
UPDATE ga_cost_campaign SET channel='Exchange' WHERE source_medium like 'CCE%';
UPDATE ga_cost_campaign SET channel='Content Mistake' WHERE source_medium like 'CON%';
UPDATE ga_cost_campaign SET channel='Procurement Mistake' WHERE source_medium like 'PRC%';
UPDATE ga_cost_campaign SET channel='Employee Vouchers' 
WHERE (source_medium like 'TE%' AND source_medium<>'TeleSales / CRM') OR source_medium like 'EA%';


#MERCADO LIBRE
UPDATE ga_cost_campaign SET channel='Mercado Libre Voucher' WHERE source_medium like 'ML%';

#PERIODISTAS

UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT02kER0';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT08jq2i';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT0arRZF';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT0hoq3F';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT0JgIhn';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT0LeO2h';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT0Olwjk';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT1d8pkJ';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT1fspLY';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT1h2uie';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT1jzjKP';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT1llem9';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT1Pk3Ap';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT1u1JSo';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT1v5kLS';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT1Yz6yi';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKT4rXdCS';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTA0aIrT';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTac0eLL';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTCcHdRX';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKThMerN5';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKThNplZF';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTiE46NZ';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTJy4dxm';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTlzVdEY';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTmvE0F7';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTn7VdJO';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTna93IX';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTpYM3yV';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTQdcuzY';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTrI3ds7';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTS55mkn';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTSR8lJR';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTt1kyoi';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTTRutlc';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTuUrfbj';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTvkHUlg';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTvsJVH4';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTwdi2uO';
UPDATE ga_cost_campaign SET channel='Cupón para periodistas' WHERE source_medium='MKTXVRpth';





#PARTNERSHIPS

#DIRECT
UPDATE ga_cost_campaign SET channel='Linio.com Referral' WHERE  
source_medium='linio.com.co / referral' OR
source_medium='linio.com / referral' OR
source_medium='linio.com.mx / referral' OR
source_medium='linio.com.ve / referral' OR
source_medium='linio.com.pe / referral' OR
source_medium='www-staging.linio.com.ve / referral';
UPDATE ga_cost_campaign SET channel='Directo / Typed' WHERE  source_medium='(direct) / (none)';



#CORPORATE SALES
UPDATE ga_cost_campaign SET channel='Corporate Sales' WHERE source_medium like 'CDEAL%';


#OFFLINE

#N/A
UPDATE ga_cost_campaign SET channel='Unknown-No voucher' WHERE source_medium='';
UPDATE ga_cost_campaign SET channel='Unknown-Voucher' WHERE (channel='' AND source_medium<>'') or channel is null;

#Channel Report Extra Stuff to define Channel---
#Venta corporativa 



#Partnerships Channel Extra


#Mercado libre Orders 


#DEFINE CHANNEL GROUP

#Facebook Ads Group
UPDATE ga_cost_campaign SET channel_group='Facebook Ads' WHERE channel='Facebook Ads';

#OTHER
UPDATE ga_cost_campaign SET channel_group='Mercado Libre ' WHERE channel='Mercado Libre Voucher';

#SEM Group
UPDATE ga_cost_campaign SET channel_group='Search Engine Marketing' WHERE channel='SEM';

#Social Media Group
UPDATE ga_cost_campaign SET channel_group='Social Media' 
WHERE channel='Facebook Referral' OR channel='Twitter Referral' OR
channel='FB Posts';

#Retargeting Group
UPDATE ga_cost_campaign SET channel_group='Retargeting' WHERE channel='Sociomantic' OR channel like '%retargeting'
OR channel='Facebook R.' OR channel='Vizury' OR channel = 'VEInteractive' OR channel = 'GDN Retargeting';

#GDN group
UPDATE ga_cost_campaign SET channel_group='Google Display Network' WHERE channel like 'GDN'; 

#NewsLetter Group
UPDATE ga_cost_campaign SET channel_group='NewsLetter' WHERE channel='Newsletter';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 15% hasta 31 de diciembre';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 50Bs  hasta 31 de diciembre';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Tramontina Utilita Sacarcorcho';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Victorinox Memoria USB Slim 4GB Verde';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='HP FlashDrive Pen 4GB Usb V195B Azul';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='GBTech Audífono GBTech X5 GBX5 Amarillo';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Perfect Choice Audifono Diadema PC-110309 Con Banda Para Cuello';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Decocar Combo Tropical Azul';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Ocean Vaso Corto Studio Set 6';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='X-mini Altavoz Capsule v1.1 Rojo';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='HP P-FD4GBHP165-GEL Pendrive USB';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='SABA Set de 2 Cuchillos Colores';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Kingston DT101G2/4GB PenDrive USB';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Ifrogz Audífono EarPollution Plugz Azul Cielo';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 30%';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='GENIUS Speakers 31731006103 Blanco';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Bajo Techo Pizarra Autoadhesiva Tipo Vinil';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Desarmador trompo con matraca con 12 puntas';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='SABA – Taza de Porcelana Rayas (Facebook)';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Botella de Alumino para Agua No soy de plástico Gaiam-Plata';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 15% hasta 31 de diciembre';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='SABA – Taza de Porcelana Rayas ';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='SABA – Taza de Porcelana Rayas ';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Cupón de reemplazo a cupón de Mkt';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='SABA Porta Botellas Neopreno Negro';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='ILUV Audífonos in ear Bubble Gum Rojo IEP205-R';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Reemplazo cupón de Marketing';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Tostador Proctor Silex 24605Y 4 Rebanadas-Blanco';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='HP P-FD4GBHP165-GEL Pendrive USB';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Morrocoy Silla de Director';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Kalosh Almohada 95% Pluma y 5% Plumón Blanca';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Kingston Tarjeta de memoria SDC4/4GB-1ADP';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='PRO CHEF Parrillera Hamburguer';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Credito por bono no funcionando 2';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Credito por bono no funcionando 3';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Contigo West loop Vaso Térmico con autosello 16 Oz/ 473 ml Rojo';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 50Bs  hasta 31 de diciembre';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 15%';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='ARGOM Teclado multimedia en español ARG-KB-7805 Negro';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='GBTech MP4 GBTech GB1106 Azul 4 GB';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='SABA Set de 3 utensilios para Parrilla';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Decocar Cava Americana 11,4Lt Roja';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT Bs. 50';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Marketing en ferias';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='SABA - Dispensador de Aceite y Vinagre 2 en 1 / SABA-511 / Transparente';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='MKT 30%';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Easy Line Mouse Retráctil EL-993346 Negro';
UPDATE ga_cost_campaign SET channel_group='Newsletter' WHERE channel='Reactivation campaign';


#SEO Group
UPDATE ga_cost_campaign SET channel_group='Search Engine Optimization' WHERE channel='Search Organic';

#Partnerships Group

#CORPORATE SALES

UPDATE ga_cost_campaign SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';

#Corporate Sales Group
UPDATE ga_cost_campaign SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';

#Offline Group


#Tele Sales Group
UPDATE ga_cost_campaign SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales' OR channel='CS Generic Voucher';

#Branded Group
UPDATE ga_cost_campaign SET channel_group='Branded' 
WHERE channel='Linio.com Referral' OR channel='Directo / Typed' OR
channel='SEM Branded';

#ML Group
UPDATE ga_cost_campaign SET channel_group='Mercado Libre' 
WHERE channel='Mercado Libre Referral' OR channel='Mercado Libre Voucher' 
OR channel='Otros - Mercado Libre';

#Blog Group
UPDATE ga_cost_campaign SET channel_group='Blogs'
WHERE channel='Blog Linio';

#Affiliates Group
UPDATE ga_cost_campaign SET channel_group='Affiliates' 
WHERE channel='Buscape';


#Other (identified) Group
UPDATE ga_cost_campaign SET channel_group='Other (identified)' 
WHERE channel='Referral Sites' OR channel='Reactivation Letters' 
OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' OR
channel='Exchange' OR
channel='Content Mistake' OR
channel='Procurement Mistake' OR
channel='Employee Voucher' OR channel='Cupón para periodistas';



#Unknown Group
UPDATE ga_cost_campaign SET channel_group='Non identified' 
WHERE channel='Unknown-No voucher' OR channel='Unknown-Voucher';

call visits_costs_channel();

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `clv_monthly`()
begin

delete from production_ve.clv_monthly;

insert into production_ve.clv_monthly(custid, yrmonth, date, voucher, net_revenue, PC2, nr_items) select CustomerNum, yrmonth, date, coupon_code, sum(ifnull(price_after_tax,0)-ifnull(after_vat_coupon,0)+ifnull(shipping_fee_charged,0)), sum(ifnull(price_after_tax,0)-ifnull(after_vat_coupon,0)+ifnull(shipping_fee_charged,0)-ifnull(cost_after_tax,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_fees,0)-ifnull(wh_cost_per_item,0)-ifnull(cs_cost_per_item,0)) as PC2, count(OrderNum) from production_ve.out_sales_report_item z where OrderAfterCan='Y' and New_Returning ='New' group by CustomerNum;

update production_ve.clv_monthly b set nr_unique_repurcharse_same_month = (select case when count(distinct t.OrderNum)>=1 then 1 else 0 end from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and b.yrmonth=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set transactions_same_month = (select count(distinct t.OrderNum) from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and b.yrmonth=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set nr_items_same_month = (select count(t.OrderNum) from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and b.yrmonth=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set net_revenue_same_month = (select sum(ifnull(t.price_after_tax,0)-ifnull(t.after_vat_coupon,0)+ifnull(t.shipping_fee_charged,0)) from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and b.yrmonth=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set PC2_same_month = (select sum(ifnull(price_after_tax,0)-ifnull(after_vat_coupon,0)+ifnull(shipping_fee_charged,0)-ifnull(cost_after_tax,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_fees,0)-ifnull(wh_cost_per_item,0)-ifnull(cs_cost_per_item,0)) as PC2 from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and b.yrmonth=t.yrmonth group by b.custid);

-- 30 Days

update production_ve.clv_monthly b set nr_unique_repurcharse_30 = (select case when count(distinct t.OrderNum)>=1 then 1 else 0 end from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 1 month)),if(month(date_add(b.date, interval 1 month))<10,concat(0,month(date_add(b.date, interval 1 month))),month(date_add(b.date, interval 1 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set transactions_30 = (select count(distinct t.OrderNum) from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 1 month)),if(month(date_add(b.date, interval 1 month))<10,concat(0,month(date_add(b.date, interval 1 month))),month(date_add(b.date, interval 1 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set nr_items_30 = (select count(t.OrderNum) from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 1 month)),if(month(date_add(b.date, interval 1 month))<10,concat(0,month(date_add(b.date, interval 1 month))),month(date_add(b.date, interval 1 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set net_revenue_30 = (select sum(ifnull(t.price_after_tax,0)-ifnull(t.after_vat_coupon,0)+ifnull(t.shipping_fee_charged,0)) from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 1 month)),if(month(date_add(b.date, interval 1 month))<10,concat(0,month(date_add(b.date, interval 1 month))),month(date_add(b.date, interval 1 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set PC2_30 = (select sum(ifnull(price_after_tax,0)-ifnull(after_vat_coupon,0)+ifnull(shipping_fee_charged,0)-ifnull(cost_after_tax,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_fees,0)-ifnull(wh_cost_per_item,0)-ifnull(cs_cost_per_item,0)) as PC2 from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 1 month)),if(month(date_add(b.date, interval 1 month))<10,concat(0,month(date_add(b.date, interval 1 month))),month(date_add(b.date, interval 1 month))))>=t.yrmonth group by b.custid);

-- 60 Days

update production_ve.clv_monthly b set nr_unique_repurcharse_60 = (select case when count(distinct t.OrderNum)>=1 then 1 else 0 end from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 2 month)),if(month(date_add(b.date, interval 2 month))<10,concat(0,month(date_add(b.date, interval 2 month))),month(date_add(b.date, interval 2 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set transactions_60 = (select count(distinct t.OrderNum) from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 2 month)),if(month(date_add(b.date, interval 2 month))<10,concat(0,month(date_add(b.date, interval 2 month))),month(date_add(b.date, interval 2 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set nr_items_60 = (select count(t.OrderNum) from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 2 month)),if(month(date_add(b.date, interval 2 month))<10,concat(0,month(date_add(b.date, interval 2 month))),month(date_add(b.date, interval 2 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set net_revenue_60 = (select sum(ifnull(t.price_after_tax,0)-ifnull(t.after_vat_coupon,0)+ifnull(t.shipping_fee_charged,0)) from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 2 month)),if(month(date_add(b.date, interval 2 month))<10,concat(0,month(date_add(b.date, interval 2 month))),month(date_add(b.date, interval 2 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set PC2_60 = (select sum(ifnull(price_after_tax,0)-ifnull(after_vat_coupon,0)+ifnull(shipping_fee_charged,0)-ifnull(cost_after_tax,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_fees,0)-ifnull(wh_cost_per_item,0)-ifnull(cs_cost_per_item,0)) as PC2 from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 2 month)),if(month(date_add(b.date, interval 2 month))<10,concat(0,month(date_add(b.date, interval 2 month))),month(date_add(b.date, interval 2 month))))>=t.yrmonth group by b.custid);

-- 90 Days

update production_ve.clv_monthly b set nr_unique_repurcharse_90 = (select case when count(distinct t.OrderNum)>=1 then 1 else 0 end from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 3 month)),if(month(date_add(b.date, interval 3 month))<10,concat(0,month(date_add(b.date, interval 3 month))),month(date_add(b.date, interval 3 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set transactions_90 = (select count(distinct t.OrderNum) from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 3 month)),if(month(date_add(b.date, interval 3 month))<10,concat(0,month(date_add(b.date, interval 3 month))),month(date_add(b.date, interval 3 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set nr_items_90 = (select count(t.OrderNum) from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 3 month)),if(month(date_add(b.date, interval 3 month))<10,concat(0,month(date_add(b.date, interval 3 month))),month(date_add(b.date, interval 3 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set net_revenue_90 = (select sum(ifnull(t.price_after_tax,0)-ifnull(t.after_vat_coupon,0)+ifnull(t.shipping_fee_charged,0)) from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 3 month)),if(month(date_add(b.date, interval 3 month))<10,concat(0,month(date_add(b.date, interval 3 month))),month(date_add(b.date, interval 3 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set PC2_90 = (select sum(ifnull(price_after_tax,0)-ifnull(after_vat_coupon,0)+ifnull(shipping_fee_charged,0)-ifnull(cost_after_tax,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_fees,0)-ifnull(wh_cost_per_item,0)-ifnull(cs_cost_per_item,0)) as PC2 from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 3 month)),if(month(date_add(b.date, interval 3 month))<10,concat(0,month(date_add(b.date, interval 3 month))),month(date_add(b.date, interval 3 month))))>=t.yrmonth group by b.custid);

-- 120 Days

update production_ve.clv_monthly b set nr_unique_repurcharse_120 = (select case when count(distinct t.OrderNum)>=1 then 1 else 0 end from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 4 month)),if(month(date_add(b.date, interval 4 month))<10,concat(0,month(date_add(b.date, interval 4 month))),month(date_add(b.date, interval 4 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set transactions_120 = (select count(distinct t.OrderNum) from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 4 month)),if(month(date_add(b.date, interval 4 month))<10,concat(0,month(date_add(b.date, interval 4 month))),month(date_add(b.date, interval 4 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set nr_items_120 = (select count(t.OrderNum) from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 4 month)),if(month(date_add(b.date, interval 4 month))<10,concat(0,month(date_add(b.date, interval 4 month))),month(date_add(b.date, interval 4 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set net_revenue_120 = (select sum(ifnull(t.price_after_tax,0)-ifnull(t.after_vat_coupon,0)+ifnull(t.shipping_fee_charged,0)) from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 4 month)),if(month(date_add(b.date, interval 4 month))<10,concat(0,month(date_add(b.date, interval 4 month))),month(date_add(b.date, interval 4 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set PC2_120 = (select sum(ifnull(price_after_tax,0)-ifnull(after_vat_coupon,0)+ifnull(shipping_fee_charged,0)-ifnull(cost_after_tax,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_fees,0)-ifnull(wh_cost_per_item,0)-ifnull(cs_cost_per_item,0)) as PC2 from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 4 month)),if(month(date_add(b.date, interval 4 month))<10,concat(0,month(date_add(b.date, interval 4 month))),month(date_add(b.date, interval 4 month))))>=t.yrmonth group by b.custid);

-- 150 Days

update production_ve.clv_monthly b set nr_unique_repurcharse_150 = (select case when count(distinct t.OrderNum)>=1 then 1 else 0 end from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 5 month)),if(month(date_add(b.date, interval 5 month))<10,concat(0,month(date_add(b.date, interval 5 month))),month(date_add(b.date, interval 5 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set transactions_150 = (select count(distinct t.OrderNum) from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 5 month)),if(month(date_add(b.date, interval 5 month))<10,concat(0,month(date_add(b.date, interval 5 month))),month(date_add(b.date, interval 5 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set nr_items_150 = (select count(t.OrderNum) from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 5 month)),if(month(date_add(b.date, interval 5 month))<10,concat(0,month(date_add(b.date, interval 5 month))),month(date_add(b.date, interval 5 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set net_revenue_150 = (select sum(ifnull(t.price_after_tax,0)-ifnull(t.after_vat_coupon,0)+ifnull(t.shipping_fee_charged,0)) from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 5 month)),if(month(date_add(b.date, interval 5 month))<10,concat(0,month(date_add(b.date, interval 5 month))),month(date_add(b.date, interval 5 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set PC2_150 = (select sum(ifnull(price_after_tax,0)-ifnull(after_vat_coupon,0)+ifnull(shipping_fee_charged,0)-ifnull(cost_after_tax,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_fees,0)-ifnull(wh_cost_per_item,0)-ifnull(cs_cost_per_item,0)) as PC2 from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 5 month)),if(month(date_add(b.date, interval 5 month))<10,concat(0,month(date_add(b.date, interval 5 month))),month(date_add(b.date, interval 5 month))))>=t.yrmonth group by b.custid);

-- 180 Days

update production_ve.clv_monthly b set nr_unique_repurcharse_180 = (select case when count(distinct t.OrderNum)>=1 then 1 else 0 end from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 6 month)),if(month(date_add(b.date, interval 6 month))<10,concat(0,month(date_add(b.date, interval 6 month))),month(date_add(b.date, interval 6 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set transactions_180 = (select count(distinct t.OrderNum) from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 6 month)),if(month(date_add(b.date, interval 6 month))<10,concat(0,month(date_add(b.date, interval 6 month))),month(date_add(b.date, interval 6 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set nr_items_180 = (select count(t.OrderNum) from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 6 month)),if(month(date_add(b.date, interval 6 month))<10,concat(0,month(date_add(b.date, interval 6 month))),month(date_add(b.date, interval 6 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set net_revenue_180 = (select sum(ifnull(t.price_after_tax,0)-ifnull(t.after_vat_coupon,0)+ifnull(t.shipping_fee_charged,0)) from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 6 month)),if(month(date_add(b.date, interval 6 month))<10,concat(0,month(date_add(b.date, interval 6 month))),month(date_add(b.date, interval 6 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly b set PC2_180 = (select sum(ifnull(price_after_tax,0)-ifnull(after_vat_coupon,0)+ifnull(shipping_fee_charged,0)-ifnull(cost_after_tax,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_fees,0)-ifnull(wh_cost_per_item,0)-ifnull(cs_cost_per_item,0)) as PC2 from production_ve.out_sales_report_item t where t.CustomerNum=b.custid and t.OrderAfterCan='Y' and t.New_Returning != 'New' and concat(year(date_add(b.date, interval 6 month)),if(month(date_add(b.date, interval 6 month))<10,concat(0,month(date_add(b.date, interval 6 month))),month(date_add(b.date, interval 6 month))))>=t.yrmonth group by b.custid);

update production_ve.clv_monthly set net_revenue=net_revenue/32.5, net_revenue_same_month=net_revenue_same_month/32.5, net_revenue_30=net_revenue_30/32.5, net_revenue_60=net_revenue_60/32.5, net_revenue_90=net_revenue_90/32.5, net_revenue_120=net_revenue_120/32.5, net_revenue_150=net_revenue_150/32.5, net_revenue_180=net_revenue_180/32.5, PC2=PC2/32.5, PC2_same_month=PC2_same_month/32.5, PC2_30=PC2_30/32.5, PC2_60=PC2_60/32.5, PC2_90=PC2_90/32.5, PC2_120=PC2_120/32.5, PC2_150=PC2_150/32.5, PC2_180=PC2_180/32.5;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `daily_execution_ops`()
begin

select  'daily execution ops: start',now();

-- Operaciones
-- Macro A

-- A200
delete production_ve.pro_sku_con_categorias.* from production_ve.pro_sku_con_categorias;

-- A201
insert into production_ve.pro_sku_con_categorias ( sku, catalog_config_name, catalog_category_name, lft, rgt )
select catalog_config.sku, catalog_config.name, catalog_category.name, catalog_category.lft, catalog_category.rgt
from (bob_live_ve.catalog_config inner join bob_live_ve.catalog_config_has_catalog_category on catalog_config.id_catalog_config = catalog_config_has_catalog_category.fk_catalog_config) inner join bob_live_ve.catalog_category on catalog_config_has_catalog_category.fk_catalog_category = catalog_category.id_catalog_category
where catalog_category.lft<>1;

-- A202
update production_ve.pro_sku_con_categorias set pro_sku_con_categorias.dif = rgt-lft;

-- A211
delete production_ve.pro_category_tree.* from production_ve.pro_category_tree;

drop table production_ve.cat1_step_1_test;
drop table production_ve.cat1_final_test;
drop table production_ve.cat2_step_1_test;
drop table production_ve.cat2_step_2_test;
drop table production_ve.cat2_final_test;
drop table production_ve.cat3_step_1_test;
drop table production_ve.cat3_step_2_test;
drop table production_ve.cat3_final_test;

-- Cat 1
create table production_ve.cat1_step_1_test (
sku text,
catalog_config_name text,
maxofdif integer
);
create index cat1_step_1_test on production_ve.cat1_step_1_test (sku(15), catalog_config_name(5));

create table production_ve.cat1_final_test (
sku text,
catalog_config_name text,
catalog_category_name text
);
create index cat1_final_test on production_ve.cat1_final_test (sku(15), catalog_config_name(5), catalog_category_name(5));

-- Cat 2
create table production_ve.cat2_step_1_test (
sku text,
catalog_config_name text,
catalog_category_name text,
dif integer
);
create index cat2_step_1_test on production_ve.cat2_step_1_test (sku(15), catalog_config_name(5), catalog_category_name(5));

create table production_ve.cat2_step_2_test (
sku text,
catalog_config_name text,
maxofdif integer
);
create index cat2_step_2_test on production_ve.cat2_step_2_test (sku(15), catalog_config_name(5));

create table production_ve.cat2_final_test (
sku text,
catalog_config_name text,
catalog_category_name text
);
create index cat2_final_test on production_ve.cat2_final_test (sku(15), catalog_config_name(5), catalog_category_name(5));

-- Cat 3

create table production_ve.cat3_step_1_test (
sku text,
catalog_config_name text,
catalog_category_name text,
dif integer
);
create index cat3_step_1_test on production_ve.cat3_step_1_test (sku(15), catalog_config_name(5), catalog_category_name(5));

create table production_ve.cat3_step_2_test (
sku text,
catalog_config_name text,
maxofdif integer
);
create index cat3_step_2_test on production_ve.cat3_step_2_test (sku(15), catalog_config_name(5));

create table production_ve.cat3_final_test (
sku text,
catalog_config_name text,
catalog_category_name text
);
create index cat3_final_test on production_ve.cat3_final_test (sku(15), catalog_config_name(5), catalog_category_name(5));

insert into production_ve.cat1_step_1_test (sku, catalog_config_name, maxofdif) select pro_sku_con_categorias.sku, pro_sku_con_categorias.catalog_config_name, max(pro_sku_con_categorias.dif) as maxofdif from production_ve.pro_sku_con_categorias group by pro_sku_con_categorias.sku, pro_sku_con_categorias.catalog_config_name;

insert into production_ve.cat1_final_test (sku, catalog_config_name, catalog_category_name) select cat1_step_1_test.sku, cat1_step_1_test.catalog_config_name, pro_sku_con_categorias.catalog_category_name
from production_ve.cat1_step_1_test inner join production_ve.pro_sku_con_categorias on (cat1_step_1_test.maxofdif = pro_sku_con_categorias.dif) and (cat1_step_1_test.sku = pro_sku_con_categorias.sku);


insert into production_ve.cat2_step_1_test (sku, catalog_config_name, catalog_category_name, dif) select pro_sku_con_categorias.sku, pro_sku_con_categorias.catalog_config_name, pro_sku_con_categorias.catalog_category_name, pro_sku_con_categorias.dif from production_ve.pro_sku_con_categorias inner join production_ve.cat1_step_1_test on pro_sku_con_categorias.sku = cat1_step_1_test.sku where (((pro_sku_con_categorias.dif)<maxofdif));

insert into production_ve.cat2_step_2_test (sku, catalog_config_name, maxofdif) select cat2_step_1_test.sku, cat2_step_1_test.catalog_config_name, max(cat2_step_1_test.dif) as maxofdif from production_ve.cat2_step_1_test group by cat2_step_1_test.sku, cat2_step_1_test.catalog_config_name;

insert into production_ve.cat2_final_test (sku, catalog_config_name, catalog_category_name) select cat2_step_2_test.sku, cat2_step_2_test.catalog_config_name, pro_sku_con_categorias.catalog_category_name
from production_ve.cat2_step_2_test inner join production_ve.pro_sku_con_categorias on (cat2_step_2_test.sku = pro_sku_con_categorias.sku) and (cat2_step_2_test.maxofdif = pro_sku_con_categorias.dif);


insert into production_ve.cat3_step_1_test (sku, catalog_config_name, catalog_category_name, dif) select pro_sku_con_categorias.sku, pro_sku_con_categorias.catalog_config_name, pro_sku_con_categorias.catalog_category_name, pro_sku_con_categorias.dif
from production_ve.pro_sku_con_categorias inner join production_ve.cat2_step_2_test on pro_sku_con_categorias.sku = cat2_step_2_test.sku
where (((pro_sku_con_categorias.dif)<maxofdif));

insert into production_ve.cat3_step_2_test (sku, catalog_config_name, maxofdif) select cat3_step_1_test.sku, cat3_step_1_test.catalog_config_name, max(cat3_step_1_test.dif) as maxofdif
from production_ve.cat3_step_1_test
group by cat3_step_1_test.sku, cat3_step_1_test.catalog_config_name;

insert into production_ve.cat3_final_test (sku, catalog_config_name, catalog_category_name) select pro_sku_con_categorias.sku, pro_sku_con_categorias.catalog_config_name,  pro_sku_con_categorias.catalog_category_name
from production_ve.cat3_step_2_test inner join production_ve.pro_sku_con_categorias on (cat3_step_2_test.sku = pro_sku_con_categorias.sku) and (cat3_step_2_test.maxofdif = pro_sku_con_categorias.dif);

-- A212
insert into production_ve.pro_category_tree ( sku, catalog_config_name, cat1, cat2, cat3 )
select cat1_final_test.sku, cat1_final_test.catalog_config_name, cat1_final_test.catalog_category_name as cat1, cat2_final_test.catalog_category_name as cat2, cat3_final_test.catalog_category_name as cat3 from (production_ve.cat1_final_test left join production_ve.cat2_final_test on cat1_final_test.sku = cat2_final_test.sku) left join production_ve.cat3_final_test on cat2_final_test.sku = cat3_final_test.sku;

-- Operaciones
-- Macro B

-- B100 A
delete production_ve.out_order_tracking.* from production_ve.out_order_tracking;

-- B101 A
insert into production_ve.out_order_tracking ( order_item_id, order_number, sku_simple ) select itens_venda.item_id, itens_venda.numero_order, itens_venda.sku from wmsprod_ve.itens_venda;

-- B102 U
update ((bob_live_ve.sales_order_address right join (production_ve.out_order_tracking inner join bob_live_ve.sales_order on out_order_tracking.order_number = sales_order.order_nr) on sales_order_address.id_sales_order_address = sales_order.fk_sales_order_address_billing) left join bob_live_ve.customer_address_region on sales_order_address.fk_customer_address_region = customer_address_region.id_customer_address_region) inner join bob_live_ve.sales_order_item on out_order_tracking.order_item_id = sales_order_item.id_sales_order_item set out_order_tracking.date_ordered = date(sales_order.created_at), out_order_tracking.payment_method = sales_order.payment_method, out_order_tracking.ship_to_state = customer_address_region.code, out_order_tracking.min_delivery_time = sales_order_item.min_delivery_time, out_order_tracking.max_delivery_time = sales_order_item.max_delivery_time, out_order_tracking.supplier_leadtime = sales_order_item.delivery_time_supplier;

-- B103 U
update production_ve.out_order_tracking inner join bob_live_ve.catalog_simple on out_order_tracking.sku_simple = catalog_simple.sku inner join bob_live_ve.catalog_config on catalog_simple.fk_catalog_config = catalog_config.id_catalog_config inner join bob_live_ve.supplier on catalog_config.fk_supplier = supplier.id_supplier set out_order_tracking.sku_config = catalog_config.sku, out_order_tracking.sku_name = catalog_config.name, out_order_tracking.supplier_id = supplier.id_supplier, out_order_tracking.supplier_name = supplier.name, out_order_tracking.min_delivery_time = case when out_order_tracking.min_delivery_time is null then catalog_simple.min_delivery_time else out_order_tracking.min_delivery_time end, out_order_tracking.max_delivery_time = case when out_order_tracking.max_delivery_time is null then catalog_simple.max_delivery_time else out_order_tracking.max_delivery_time end, out_order_tracking.package_height = catalog_config.package_height, out_order_tracking.package_length = catalog_config.package_length, out_order_tracking.package_width = catalog_config.package_width, out_order_tracking.package_weight = catalog_config.package_weight;

-- B103b U
update production_ve.out_order_tracking inner join bob_live_ve.catalog_simple on out_order_tracking.sku_simple = catalog_simple.sku inner join bob_live_ve.catalog_config on catalog_simple.fk_catalog_config = catalog_config.id_catalog_config set out_order_tracking.supplier_leadtime = case when out_order_tracking.supplier_leadtime is null then 0 else out_order_tracking.supplier_leadtime end;

-- B103c U extra
update production_ve.out_order_tracking set out_order_tracking.fullfilment_type_real = "inventory";

-- B104 U
update production_ve.out_order_tracking inner join (wmsprod_ve.itens_venda inner join wmsprod_ve.status_itens_venda on itens_venda.itens_venda_id = status_itens_venda.itens_venda_id) on out_order_tracking.order_item_id = itens_venda.item_id set out_order_tracking.fullfilment_type_real = "crossdock" where status_itens_venda.status="analisando quebra" or status_itens_venda.status="aguardando estoque";

-- B104 U Extra
update (production_ve.out_order_tracking inner join wmsprod_ve.itens_venda on out_order_tracking.order_item_id = itens_venda.item_id) inner join wmsprod_ve.status_itens_venda on itens_venda.itens_venda_id = status_itens_venda.itens_venda_id set out_order_tracking.fullfilment_type_real = "dropshipping"
where (((status_itens_venda.status)="ds estoque reservado"));

-- B105 U
update production_ve.out_order_tracking inner join production_ve.pro_category_tree on out_order_tracking.sku_config = pro_category_tree.sku set out_order_tracking.category = cat1, out_order_tracking.cat2 = pro_category_tree.cat2;

-- B105a U
update production_ve.out_order_tracking inner join production_ve.exceptions_supplier_categories on out_order_tracking.supplier_name = exceptions_supplier_categories.supplier set out_order_tracking.category = "ropa, calzado y accesorios"
where out_order_tracking.category="deportes";

-- B105b U
update production_ve.out_order_tracking set out_order_tracking.category = "salud y cuidado personal" where out_order_tracking.category="ropa, calzado y accesorios" and out_order_tracking.cat2="salud y cuidado personal";

-- B106 U
update production_ve.out_order_tracking inner join production_ve.buyer_category on out_order_tracking.category = buyer_category.category set out_order_tracking.category_english = buyer_category.category_english;

-- B110 D
delete production_ve.pro_min_date_exported.* from production_ve.pro_min_date_exported;

-- B111 A
insert into production_ve.pro_min_date_exported ( fk_sales_order_item, name, min_of_created_at )
select sales_order_item_status_history.fk_sales_order_item, sales_order_item_status.name, min(sales_order_item_status_history.created_at) as min_of_created_at from bob_live_ve.sales_order_item_status inner join bob_live_ve.sales_order_item_status_history on sales_order_item_status.id_sales_order_item_status = sales_order_item_status_history.fk_sales_order_item_status group by sales_order_item_status_history.fk_sales_order_item, sales_order_item_status.name having (((sales_order_item_status.name)="exported"));

-- B112 U
update production_ve.out_order_tracking inner join production_ve.pro_min_date_exported on out_order_tracking.order_item_id = pro_min_date_exported.fk_sales_order_item set out_order_tracking.date_exported = date(min_of_created_at);

-- B120 U
update production_ve.out_order_tracking inner join wmsprod_ve.itens_venda on out_order_tracking.order_item_id = itens_venda.item_id inner join wmsprod_ve.status_itens_venda on itens_venda.itens_venda_id = status_itens_venda.itens_venda_id set out_order_tracking.date_procured = date(data) where status_itens_venda.status="estoque reservado";

-- B120b U
update production_ve.out_order_tracking set out_order_tracking.date_ready_to_pick = case when dayofweek(date_procured)=1 then production_ve.workday(date_procured,1) else (case when dayofweek(date_procured)=7 then production_ve.workday(date_procured,1) else date_procured end)end;

-- B120c U
update production_ve.out_order_tracking set out_order_tracking.date_ready_to_pick = case when date_ready_to_pick in('2012-11-19','2012-12-25','2013-1-1','2013-2-4','2013-3-18','2013-05-01') then production_ve.workday(date_ready_to_pick,1) when date_ready_to_pick = '2013-3-28' then production_ve.workday(date_ready_to_pick,2) else date_ready_to_pick end;

-- B121a D
delete production_ve.pro_max_date_ready_to_ship.* from production_ve.pro_max_date_ready_to_ship;

-- B121b A
insert into production_ve.pro_max_date_ready_to_ship ( date_ready, order_item_id )
select min(date(data)) as date_ready, out_order_tracking.order_item_id
from production_ve.out_order_tracking inner join wmsprod_ve.itens_venda on out_order_tracking.order_item_id = itens_venda.item_id inner join wmsprod_ve.status_itens_venda on itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
where status_itens_venda.status="faturado"
group by out_order_tracking.order_item_id;

-- B121c U
update production_ve.out_order_tracking inner join production_ve.pro_max_date_ready_to_ship on out_order_tracking.order_item_id = pro_max_date_ready_to_ship.order_item_id set out_order_tracking.date_ready_to_ship = pro_max_date_ready_to_ship.date_ready;

-- B122 U
update production_ve.out_order_tracking inner join (wmsprod_ve.itens_venda b inner join wmsprod_ve.status_itens_venda d on b.itens_venda_id = d.itens_venda_id) on out_order_tracking.order_item_id = b.item_id set out_order_tracking.date_shipped = (select max(date(data)) from wmsprod_ve.itens_venda c, wmsprod_ve.status_itens_venda e where e.status="expedido" and b.itens_venda_id = c.itens_venda_id and e.itens_venda_id = d.itens_venda_id);

-- B123 U
update production_ve.out_order_tracking inner join wmsprod_ve.vw_itens_venda_entrega vw on out_order_tracking.order_item_id = vw.item_id inner join wmsprod_ve.tms_status_delivery del on vw.cod_rastreamento = del.cod_rastreamento set out_order_tracking.date_delivered = (select case when tms.date < '2012-05-01' then null else max(tms.date) end from wmsprod_ve.vw_itens_venda_entrega itens, wmsprod_ve.tms_status_delivery tms where itens.cod_rastreamento = vw.cod_rastreamento and tms.cod_rastreamento = del.cod_rastreamento and tms.id_status = 4);

-- B124 U
update production_ve.out_order_tracking set out_order_tracking.date_procured_promised = production_ve.workday(date_exported, supplier_leadtime), out_order_tracking.date_delivered_promised = production_ve.workday(date_exported,max_delivery_time)
where out_order_tracking.date_exported is not null;

-- B124a U
update bob_live_ve.sales_order_item inner join production_ve.out_order_tracking on sales_order_item.id_sales_order_item = out_order_tracking.order_item_id set out_order_tracking.is_linio_promise = sales_order_item.is_linio_promise;

-- B124b U
update production_ve.out_order_tracking set is_linio_promise = 0 where is_linio_promise is null;

-- B124c U
update production_ve.out_order_tracking set date_delivered_promised = production_ve.workday(date_exported,2) where is_linio_promise = 1;

-- B124d1 U
update production_ve.out_order_tracking set out_order_tracking.date_procured_promised = production_ve.workday(out_order_tracking.date_procured_promised,1)
where out_order_tracking.date_procured_promised>='2013-1-1' and out_order_tracking.date_exported<'2013-1-1';

-- B124d2 U
update production_ve.out_order_tracking set out_order_tracking.date_delivered_promised = production_ve.workday(out_order_tracking.date_delivered_promised,1) where out_order_tracking.date_delivered_promised>='2013-1-1' and out_order_tracking.date_exported<'2013-1-1';

-- B124d4 U
update production_ve.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_ve.workday(out_order_tracking.date_ready_to_ship_promised,1) where out_order_tracking.date_ready_to_ship_promised>='2013-1-1' and out_order_tracking.date_exported<'2013-1-1';

-- B124a1 U
update production_ve.out_order_tracking set out_order_tracking.date_procured_promised = production_ve.workday(out_order_tracking.date_procured_promised,1)
where out_order_tracking.date_procured_promised>='2013-2-11' and out_order_tracking.date_exported<'2013-2-11';

-- B124a2 U
update production_ve.out_order_tracking set out_order_tracking.date_delivered_promised = production_ve.workday(out_order_tracking.date_delivered_promised,1) where out_order_tracking.date_delivered_promised>='2013-2-11' and out_order_tracking.date_exported<'2013-2-11';

-- B124a4 U
update production_ve.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_ve.workday(out_order_tracking.date_ready_to_ship_promised,1) where out_order_tracking.date_ready_to_ship_promised>='2013-2-11' and out_order_tracking.date_exported<'2013-2-11';

-- B124b1 U
update production_ve.out_order_tracking set out_order_tracking.date_procured_promised = production_ve.workday(out_order_tracking.date_procured_promised,1)
where out_order_tracking.date_procured_promised>='2013-2-12' and out_order_tracking.date_exported<'2013-2-12';

-- B124b2 U
update production_ve.out_order_tracking set out_order_tracking.date_delivered_promised = production_ve.workday(out_order_tracking.date_delivered_promised,1) where out_order_tracking.date_delivered_promised>='2013-2-12' and out_order_tracking.date_exported<'2013-2-12';

-- B124b4 U
update production_ve.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_ve.workday(out_order_tracking.date_ready_to_ship_promised,1) where out_order_tracking.date_ready_to_ship_promised>='2013-2-12' and out_order_tracking.date_exported<'2013-2-12';

-- B124e1 U
update production_ve.out_order_tracking set out_order_tracking.date_procured_promised = production_ve.workday(out_order_tracking.date_procured_promised,1)
where out_order_tracking.date_procured_promised>='2013-03-28' and out_order_tracking.date_exported<'2013-03-28';

-- B124e2 U
update production_ve.out_order_tracking set out_order_tracking.date_delivered_promised = production_ve.workday(out_order_tracking.date_delivered_promised,1) where out_order_tracking.date_delivered_promised>='2013-03-28' and out_order_tracking.date_exported<'2013-03-28';

-- B124e4 U
update production_ve.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_ve.workday(out_order_tracking.date_ready_to_ship_promised,1) where out_order_tracking.date_ready_to_ship_promised>='2013-03-28' and out_order_tracking.date_exported<'2013-03-28';

-- B124h1 U
update production_ve.out_order_tracking set out_order_tracking.date_procured_promised = production_ve.workday(out_order_tracking.date_procured_promised,1)
where out_order_tracking.date_procured_promised>='2013-3-29' and out_order_tracking.date_exported<'2013-3-29';

-- B124h2 U
update production_ve.out_order_tracking set out_order_tracking.date_delivered_promised = production_ve.workday(out_order_tracking.date_delivered_promised,1)
where out_order_tracking.date_delivered_promised>='2013-3-29' and out_order_tracking.date_exported<'2013-3-29';

-- B124h4 U
update production_ve.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_ve.workday(out_order_tracking.date_ready_to_ship_promised,1) where out_order_tracking.date_ready_to_ship_promised>='2013-3-29' and out_order_tracking.date_exported<'2013-3-29';

-- B124c1 U
update production_ve.out_order_tracking set out_order_tracking.date_procured_promised = production_ve.workday(out_order_tracking.date_procured_promised,1)
where out_order_tracking.date_procured_promised>='2013-04-19' and out_order_tracking.date_exported<'2013-04-19';

-- B124c2 U
update production_ve.out_order_tracking set out_order_tracking.date_delivered_promised = production_ve.workday(out_order_tracking.date_delivered_promised,1)
where out_order_tracking.date_delivered_promised>='2013-04-19' and out_order_tracking.date_exported<'2013-04-19';

-- B124c4 U
update production_ve.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_ve.workday(out_order_tracking.date_ready_to_ship_promised,1) where out_order_tracking.date_ready_to_ship_promised>='2013-04-19' and out_order_tracking.date_exported<'2013-04-19';

-- B124i1 U
update production_ve.out_order_tracking set out_order_tracking.date_procured_promised = production_ve.workday(out_order_tracking.date_procured_promised,1)
where out_order_tracking.date_procured_promised>='2013-05-01' and out_order_tracking.date_exported<'2013-05-01';

-- B124i2 U
update production_ve.out_order_tracking set out_order_tracking.date_delivered_promised = production_ve.workday(out_order_tracking.date_delivered_promised,1)
where out_order_tracking.date_delivered_promised>='2013-05-01' and out_order_tracking.date_exported<'2013-05-01';

-- B124i4 U
update production_ve.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_ve.workday(out_order_tracking.date_ready_to_ship_promised,1) where out_order_tracking.date_ready_to_ship_promised>='2013-05-01' and out_order_tracking.date_exported<'2013-05-01';

-- B124j1 U
update production_ve.out_order_tracking set out_order_tracking.date_procured_promised = production_ve.workday(out_order_tracking.date_procured_promised,1)
where out_order_tracking.date_procured_promised>='2013-06-24' and out_order_tracking.date_exported<'2013-06-24';

-- B124j2 U
update production_ve.out_order_tracking set out_order_tracking.date_delivered_promised = production_ve.workday(out_order_tracking.date_delivered_promised,1)
where out_order_tracking.date_delivered_promised>='2013-06-24' and out_order_tracking.date_exported<'2013-06-24';

-- B124j4 U
update production_ve.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_ve.workday(out_order_tracking.date_ready_to_ship_promised,1) where out_order_tracking.date_ready_to_ship_promised>='2013-06-24' and out_order_tracking.date_exported<'2013-06-24';

-- B124f U
update production_ve.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_ve.workday(date_procured_promised, wh_time);

-- B124f1 U Extra
update production_ve.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_ve.workday(out_order_tracking.date_ready_to_ship_promised,1)
where (((out_order_tracking.date_ready_to_ship_promised)>='2013-01-01') and ((out_order_tracking.date_procured_promised)<'2013-01-01'));

-- B124f2 U Extra
update production_ve.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_ve.workday(out_order_tracking.date_ready_to_ship_promised,1)
where (((out_order_tracking.date_ready_to_ship_promised)>='2013-02-11') and ((out_order_tracking.date_procured_promised)<'2013-02-11'));

-- B124f3 U Extra
update production_ve.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_ve.workday(out_order_tracking.date_ready_to_ship_promised,1)
where (((out_order_tracking.date_ready_to_ship_promised)>='2013-02-12') and ((out_order_tracking.date_procured_promised)<'2013-02-12'));

-- B124f4 U Extra
update production_ve.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_ve.workday(out_order_tracking.date_ready_to_ship_promised,1)
where (((out_order_tracking.date_ready_to_ship_promised)>='2013-03-28') and ((out_order_tracking.date_procured_promised)<'2013-03-28'));

-- B124f5 U Extra
update production_ve.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_ve.workday(out_order_tracking.date_ready_to_ship_promised,1)
where (((out_order_tracking.date_ready_to_ship_promised)>='2013-03-29') and ((out_order_tracking.date_procured_promised)<'2013-03-29'));

-- B124f6 U Extra
update production_ve.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_ve.workday(out_order_tracking.date_ready_to_ship_promised,1)
where (((out_order_tracking.date_ready_to_ship_promised)>='2013-04-19') and ((out_order_tracking.date_procured_promised)<'2013-04-19'));

-- B124f7 U Extra
update production_ve.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_ve.workday(out_order_tracking.date_ready_to_ship_promised,1)
where (((out_order_tracking.date_ready_to_ship_promised)>='2013-05-01') and ((out_order_tracking.date_procured_promised)<'2013-05-01'));

-- B124f8 U Extra
update production_ve.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production_ve.workday(out_order_tracking.date_ready_to_ship_promised,1)
where (((out_order_tracking.date_ready_to_ship_promised)>='2013-06-24') and ((out_order_tracking.date_procured_promised)<'2013-06-24'));

-- B124j U
update production_ve.out_order_tracking inner join wmsprod_ve.itens_venda on out_order_tracking.order_item_id = itens_venda.item_id set out_order_tracking.wh_time = tempo_armazem where tempo_armazem >1;

-- B125 U
update production_ve.out_order_tracking inner join bob_live_ve.sales_order_item on out_order_tracking.sku_simple = sales_order_item.sku inner join bob_live_ve.catalog_shipment_type on sales_order_item.fk_catalog_shipment_type = catalog_shipment_type.id_catalog_shipment_type set out_order_tracking.fullfilment_type_bob = catalog_shipment_type.name;

-- B130 D
delete production_ve.pro_1st_attempt.* from production_ve.pro_1st_attempt;

-- B131 A
insert into production_ve.pro_1st_attempt ( order_item_id, min_of_date )
select out_order_tracking.order_item_id, min(tms_status_delivery.date) as min_of_date from (production_ve.out_order_tracking inner join wmsprod_ve.
vw_itens_venda_entrega on out_order_tracking.order_item_id = vw_itens_venda_entrega.item_id) inner join wmsprod_ve.
tms_status_delivery on vw_itens_venda_entrega.cod_rastreamento = tms_status_delivery.cod_rastreamento group by out_order_tracking.order_item_id, tms_status_delivery.id_status
having tms_status_delivery.id_status=5;

-- B132 U
update production_ve.out_order_tracking inner join production_ve.pro_1st_attempt on out_order_tracking.order_item_id = pro_1st_attempt.order_item_id set out_order_tracking.date_1st_attempt = min_of_date;

-- B133 U
update production_ve.out_order_tracking inner join wmsprod_ve.vw_itens_venda_entrega vw on out_order_tracking.order_item_id = vw.item_id inner join wmsprod_ve.tms_status_delivery del on vw.cod_rastreamento = del.cod_rastreamento set out_order_tracking.date_1st_attempt = (select max(tms.date) from wmsprod_ve.tms_status_delivery tms, wmsprod_ve.vw_itens_venda_entrega itens where del.cod_rastreamento = tms.cod_rastreamento and vw.item_id = itens.item_id and tms.id_status=4) where out_order_tracking.date_1st_attempt is null or out_order_tracking.date_1st_attempt < '2011-05-01';

-- B140 U
update production_ve.out_order_tracking set out_order_tracking.week_exported = production_ve.week_iso(date_exported), out_order_tracking.week_procured_promised = production_ve.week_iso(date_procured_promised), out_order_tracking.week_delivered_promised = production_ve.week_iso(date_delivered_promised), out_order_tracking.month_num_delivered_promised = date_format(date_delivered_promised, "%x-%m"), out_order_tracking.month_num_procured_promised = date_format(date_procured_promised, "%x-%m"), out_order_tracking.week_ready_to_ship_promised = production_ve.week_iso(date_ready_to_ship_promised), out_order_tracking.week_ready_to_pick = production_ve.week_iso(date_ready_to_pick), production_ve.out_order_tracking.week_ordered = production_ve.week_iso(date_ordered),month_num_ready_to_pick = date_format(date_ready_to_pick, "%x-%m");

-- B141a U
update production_ve.out_order_tracking set out_order_tracking.month_procured_promised = monthname(date_procured_promised);

-- B141a1 U
update production_ve.out_order_tracking set out_order_tracking.month_num_procured_promised = date_format(date_procured_promised, "%x-%m");

-- B141b U
update production_ve.out_order_tracking set out_order_tracking.month_delivered_promised = monthname(date_delivered_promised);

-- B141b1 U
update production_ve.out_order_tracking set out_order_tracking.month_num_delivered_promised = date_format(date_delivered_promised, "%x-%m");

-- B141c U
update production_ve.out_order_tracking set month_num_procured_promised = '2012-12' where date_procured_promised = '2012-12-31';
 
-- B141c2 U
update production_ve.out_order_tracking set month_num_delivered_promised = '2012-12' where date_delivered_promised = '2012-12-31';

-- B150 U
update production_ve.out_order_tracking inner join wmsprod_ve.itens_venda on out_order_tracking.order_item_id = itens_venda.item_id inner join wmsprod_ve.romaneio on itens_venda.romaneio_id = romaneio.romaneio_id inner join wmsprod_ve.transportadoras on romaneio.transportadora_id = transportadoras.transportadoras_id set out_order_tracking.shipping_carrier = nome_transportadora;

-- B151 U
update production_ve.out_order_tracking set out_order_tracking.year_procured_promised = date_format(date_procured_promised, "%x");

-- B152 U
update production_ve.out_order_tracking set out_order_tracking.year_delivered_promised = date_format(date_delivered_promised,"%x");

-- B153 U
update production_ve.out_order_tracking set out_order_tracking.sl0 = 1
where (((out_order_tracking.supplier_leadtime)=0));

-- B190 U
update wmsprod_ve.itens_venda inner join production_ve.out_order_tracking on itens_venda.item_id = out_order_tracking.order_item_id set out_order_tracking.status_wms = status;

-- B191 U
update production_ve.out_order_tracking set out_order_tracking.stockout = "1"
where out_order_tracking.status_wms="quebra tratada" or out_order_tracking.status_wms="quebrado";

-- B201 U
update production_ve.out_order_tracking set out_order_tracking.workdays_to_procure = case when stockout = 1 then null else (case when date_exported is null then null else production_ve.calcworkdays(date_exported,case when date_procured is null then curdate() else date_procured end)end)end, out_order_tracking.workdays_to_ready = case when date_ready_to_pick is null then null else production_ve.calcworkdays(date_ready_to_pick, case when date_ready_to_ship is null then curdate() else date_ready_to_ship end)end, out_order_tracking.workdays_to_ship = case when date_ready_to_ship is null then null else production_ve.calcworkdays(date_ready_to_ship, case when date_shipped is null then curdate() else date_shipped end)end, out_order_tracking.workdays_to_1st_attempt = case when date_shipped is null then null else production_ve.calcworkdays(date_shipped, case when date_1st_attempt is null then curdate() else date_1st_attempt end)end, out_order_tracking.workdays_to_deliver = case when date_shipped is null then null else production_ve.calcworkdays(date_shipped,case when date_delivered is null then curdate() else date_delivered end)end, out_order_tracking.workdays_total_1st_attempt = case when date_shipped is null then null else production_ve.calcworkdays(date_exported, case when date_1st_attempt is null then curdate() else date_1st_attempt end)end, out_order_tracking.workdays_total_delivery = case when date_shipped is null then null else production_ve.calcworkdays(date_exported, case when date_delivered is null then curdate() else date_delivered end)end, out_order_tracking.days_total_delivery = case when date_shipped is null then null else (case when date_delivered is null then datediff(curdate(),date_exported) else datediff(date_delivered,date_exported) end)end, out_order_tracking.workdays_to_export = case when date_ordered is null then null else production_ve.calcworkdays(date_ordered,case when date_exported is null then curdate() else date_exported end)end;

-- B201a U
update production_ve.out_order_tracking set date_delivered_errors = case when days_total_delivery < 0 then 1  else 0 end;

-- B202 U
update production_ve.out_order_tracking set out_order_tracking.delay_to_procure = case when date_procured is null then (case when curdate()>date_procured_promised then 1 else 0 end) else (case when date_procured>date_procured_promised then 1 else 0 end)end, out_order_tracking.delay_to_ready = case when workdays_to_ready>1 then 1 else 0 end, out_order_tracking.delay_to_ship = case when workdays_to_ship>1 then 1 else 0 end, out_order_tracking.delay_to_1st_attempt = case when workdays_to_1st_attempt>2 then 1 else 0 end, out_order_tracking.delay_to_deliver = case when workdays_to_deliver> 2 then 1 else 0 end, out_order_tracking.delay_total_1st_attempt = case when date_1st_attempt is null then (case when curdate()>date_delivered_promised then 1 else 0 end) else (case when date_1st_attempt>date_delivered_promised then 1 else 0 end)end, out_order_tracking.delay_total_delivery = case when date_delivered is null then (case when curdate()>date_delivered_promised then 1 else 0 end) else (case when date_delivered>date_delivered_promised then 1 else 0 end)end, out_order_tracking.workdays_delay_to_procure = case when workdays_to_procure-supplier_leadtime<0 then 0 else workdays_to_procure-supplier_leadtime end, out_order_tracking.workdays_delay_to_ready = case when workdays_to_ready-1<0 then 0 else workdays_to_ready-1 end, out_order_tracking.workdays_delay_to_ship = case when workdays_to_ship-1<0 then 0 else workdays_to_ship-1 end, out_order_tracking.workdays_delay_to_1st_attempt = case when workdays_to_1st_attempt-3<0 then 0 else workdays_to_1st_attempt-3 end, out_order_tracking.workdays_delay_to_deliver = case when workdays_to_deliver-3<0 then 0 else workdays_to_deliver-3 end, out_order_tracking.on_time_to_procure = case when date_procured<=date_procured_promised then 1 else 0 end, out_order_tracking.on_time_total_1st_attempt = case when date_1st_attempt<=date_delivered_promised then 1 else 0 end, out_order_tracking.on_time_total_delivery = case when date_delivered<=date_delivered_promised then 1 when date_delivered is null then 0 else 0 end;

-- B203 U
update production_ve.out_order_tracking inner join bob_live_ve.catalog_config on out_order_tracking.sku_config = catalog_config.sku set out_order_tracking.presale = "1" where catalog_config.name like "preventa%";

-- B204 U
update production_ve.out_order_tracking inner join bob_live_ve.catalog_config on out_order_tracking.sku_config = catalog_config.sku inner join bob_live_ve.catalog_attribute_option_global_buyer on catalog_attribute_option_global_buyer.id_catalog_attribute_option_global_buyer = catalog_config.fk_catalog_attribute_option_global_buyer set out_order_tracking.buyer = catalog_attribute_option_global_buyer.name;

-- B206 U
update production_ve.out_order_tracking inner join bob_live_ve.sales_order_item on out_order_tracking.order_item_id = sales_order_item.id_sales_order_item inner join bob_live_ve.sales_order_item_status on sales_order_item.fk_sales_order_item_status = sales_order_item_status.id_sales_order_item_status set out_order_tracking.status_bob = sales_order_item_status.name;

-- B207 U
update production_ve.out_order_tracking inner join wmsprod_ve.vw_itens_venda_entrega on out_order_tracking.order_item_id = vw_itens_venda_entrega.item_id set out_order_tracking.wms_tracking_code = cod_rastreamento;

-- B208 U
update production_ve.out_order_tracking inner join wmsprod_ve.tms_tracks on out_order_tracking.wms_tracking_code = tms_tracks.cod_rastreamento set out_order_tracking.shipping_carrier_tracking_code = track;

-- B210 U
update production_ve.out_order_tracking inner join production_ve.status_match_bob_wms on out_order_tracking.status_wms = status_match_bob_wms.status_wms and out_order_tracking.status_bob = status_match_bob_wms.status_bob set out_order_tracking.status_match_bob_wms = status_match_bob_wms.correct;

-- B211 U
update production_ve.out_order_tracking set out_order_tracking.check_dates = case when date_ordered>date_exported then "date ordered > date exported" else (case when date_exported>date_procured then "date exported > date procured" else (case when date_procured>date_ready_to_ship then "date procured > date ready to ship" else (case when date_ready_to_ship>date_shipped then "date ready to ship > date shipped" else (case when date_shipped>date_1st_attempt then "date shipped > date 1st attempt" else (case when date_1st_attempt>date_delivered then "date shipped > date 1st attempt" else "correct" end)end)end)end)end)end;

-- B211a U
update production_ve.out_order_tracking set out_order_tracking.check_date_1st_attempt = case when date_1st_attempt is null then (case when date_delivered is null then 0 else 1 end) else (case when date_1st_attempt>date_delivered then 1 else 0 end)end;

-- B211b U
update production_ve.out_order_tracking set out_order_tracking.check_date_shipped = case when date_shipped is null then (case when date_1st_attempt is null and date_delivered is null then 0 else 1 end) else (case when date_shipped>date_1st_attempt then 1 else 0 end)end;

-- B211c U
update production_ve.out_order_tracking set out_order_tracking.check_date_ready_to_ship = case when date_ready_to_ship is null then (case when date_shipped is null and date_1st_attempt is null and date_delivered is null then 0 else 1 end) else (case when date_ready_to_ship>date_shipped then 1 else 0 end)end;

-- B211d U
update production_ve.out_order_tracking set out_order_tracking.check_date_procured = case when date_procured is null then (case when date_ready_to_ship is null and date_shipped is null and date_1st_attempt is null and date_delivered is null then 0 else 1 end) else (case when date_procured>date_ready_to_ship then 1 else 0 end)end;

-- B211e U
update production_ve.out_order_tracking set out_order_tracking.check_date_exported = case when date_exported is null then (case when date_procured is null and date_ready_to_ship is null and date_shipped is null and date_1st_attempt is null and date_delivered is null then 0 else 1 end) else (case when date_exported>date_procured then 1 else 0 end)end;

-- B211f U
update production_ve.out_order_tracking set out_order_tracking.check_date_ordered = case when date_ordered is null then (case when date_exported is null and date_procured is null and date_ready_to_ship is null and date_shipped is null and date_1st_attempt is null and date_delivered is null then 0 else 1 end) else (case when date_ordered>date_exported then 1 else 0 end)end;

-- B214 U
update production_ve.out_order_tracking set out_order_tracking.date_procured = null
where out_order_tracking.status_wms="aguardando estoque" or out_order_tracking.status_wms="analisando quebra";

-- B216 U
update production_ve.out_order_tracking set out_order_tracking.still_to_procure = 1
where out_order_tracking.date_procured is null;

-- B216b U
update production_ve.out_order_tracking set out_order_tracking.still_to_procure = 0
where out_order_tracking.status_wms="quebrado" or out_order_tracking.status_wms="quebra tratada";

-- B218 U
update production_ve.out_order_tracking set out_order_tracking.vol_weight = package_height*package_length*package_width/5000;

-- B219 U
update production_ve.out_order_tracking set out_order_tracking.max_vol_w_vs_w= case when vol_weight>package_weight then vol_weight else package_weight end;

-- B220 U
update production_ve.out_order_tracking set out_order_tracking.package_measure = case when max_vol_w_vs_w>45.486 then "large" else (case when max_vol_w_vs_w>2.277 then "medium" else "small" end)end;

-- B221 U
update production_ve.out_order_tracking inner join bob_live_ve.sales_order on out_order_tracking.order_number = sales_order.order_nr inner join bob_live_ve.sales_order_address on sales_order.fk_sales_order_address_billing = sales_order_address.id_sales_order_address set out_order_tracking.ship_to_zip = postcode;

-- B222 U
update production_ve.out_order_tracking set out_order_tracking.delay_carrier_shipment = case when package_measure="large" then (case when workdays_to_1st_attempt-5>0 then 1 else 0 end) else (case when workdays_to_1st_attempt-3>0 then 1 else 0 end)end;

-- B223 U
update production_ve.out_order_tracking set out_order_tracking.days_left_to_procure = case when date_procured is null then (case when datediff(date_procured_promised,curdate())<0 then 0 else datediff(date_procured_promised,curdate()) end) else 555 end;

-- B224 U
update production_ve.out_order_tracking set out_order_tracking.deliv_within_3_days_of_order = case when datediff(date_delivered,date_ordered)<=5 then (case when production_ve.calcworkdays(date_ordered,date_delivered)<=3 then 1 else 0 end) else 0 end, out_order_tracking.first_att_within_3_days_of_order = case when datediff(date_1st_attempt,date_ordered)<=5 then (case when production_ve.calcworkdays(date_ordered,date_1st_attempt)<=3 then 1 else 0 end) else 0 end;

-- B225 U
update production_ve.out_order_tracking set out_order_tracking.shipped_same_day_as_order = case when date_ordered=date_shipped then 1 else 0 end;

-- B226 U
update production_ve.out_order_tracking set out_order_tracking.shipped_same_day_as_procured = case when date_procured=date_shipped then 1 else 0 end;

-- B227 U extra
update production_ve.out_order_tracking set out_order_tracking.reason_for_delay = "on time 1st attempt";

-- B227 U
update production_ve.out_order_tracking set out_order_tracking.reason_for_delay = "procurement" where out_order_tracking.delay_to_procure=1 and out_order_tracking.delay_total_1st_attempt=1;

-- B228 U
update production_ve.out_order_tracking set out_order_tracking.reason_for_delay = "preparing item for shipping" where out_order_tracking.reason_for_delay="on time 1st attempt" and out_order_tracking.delay_to_ready=1 and out_order_tracking.delay_total_1st_attempt=1;

-- B229 U
update production_ve.out_order_tracking set out_order_tracking.reason_for_delay = "preparing item for shipping"
where out_order_tracking.reason_for_delay="on time 1st attempt" and out_order_tracking.delay_to_ready=1 and out_order_tracking.delay_total_1st_attempt=1;

-- B230 U
update production_ve.out_order_tracking set out_order_tracking.reason_for_delay = "carrier late delivering" where out_order_tracking.reason_for_delay="on time 1st attempt" and out_order_tracking.delay_total_1st_attempt=1;

-- B231 U
update production_ve.out_order_tracking set out_order_tracking.early_to_procure = case when (case when date_procured is null then datediff(date_procured_promised,curdate()) else datediff(date_procured_promised,date_procured) end)>0 then 1 else 0 end, out_order_tracking.early_to_1st_attempt = case when (case when date_1st_attempt is null then datediff(date_delivered_promised,curdate()) else datediff(date_delivered_promised,date_1st_attempt) end)>0 then 1 else 0 end;

-- B232 U
update production_ve.out_order_tracking set out_order_tracking.delay_reason_maximum_value = production_ve.maximum(workdays_delay_to_procure,workdays_delay_to_ready,workdays_delay_to_ship,workdays_delay_to_1st_attempt);

-- B233 U
update production_ve.out_order_tracking set out_order_tracking.delay_reason = case when delay_total_1st_attempt=1 then (case when delay_reason_maximum_value=workdays_delay_to_procure then "procurement" else (case when delay_reason_maximum_value=workdays_delay_to_ready then "preparing item for shipping" else (case when delay_reason_maximum_value=workdays_delay_to_ship then "carrier late to pick up item" else (case when delay_reason_maximum_value=workdays_delay_to_1st_attempt then "carrier late delivering" else "on time 1st attempt" end)end)end)end) else "on time 1st attempt" end;

-- B234 U
update production_ve.out_order_tracking set out_order_tracking.on_time_to_ship = case when date_shipped<= date_ready_to_ship_promised then 1 else 0 end, out_order_tracking.on_time_r2s = case when date_ready_to_ship<=date_ready_to_ship_promised then 1 else 0 end;

-- B235 D
delete production_ve.pro_order_tracking_num_items_per_order.* from production_ve.pro_order_tracking_num_items_per_order;

-- B236 A
insert into production_ve.pro_order_tracking_num_items_per_order ( order_number, count_of_order_item_id ) select out_order_tracking.order_number, count(out_order_tracking.order_item_id) as countoforder_item_id from production_ve.out_order_tracking group by out_order_tracking.order_number;

-- B237 U
update production_ve.out_order_tracking inner join production_ve.pro_order_tracking_num_items_per_order on out_order_tracking.order_number = pro_order_tracking_num_items_per_order.order_number set out_order_tracking.num_items_per_order = 1/pro_order_tracking_num_items_per_order.count_of_order_item_id;

-- B238 U
update production_ve.out_order_tracking set out_order_tracking.procurement_actual_time = case when date_exported is null then null else (case when date_procured is null then production_ve.calcworkdays(date_exported,curdate()) else production_ve.calcworkdays(date_exported,date_procured)end)end;

-- B239 U
update production_ve.out_order_tracking set out_order_tracking.procurement_delay= case when date_exported>=curdate() then 0 else (case when date_sub(procurement_actual_time, interval supplier_leadtime day)>0 then date_sub(procurement_actual_time, interval supplier_leadtime day) else 0 end)end;

-- B240 U
update production_ve.out_order_tracking set out_order_tracking.procurement_delay_counter = 1
where out_order_tracking.procurement_delay>0;

-- B241 U
update production_ve.out_order_tracking set out_order_tracking.warehouse_actual_time = case when date_ready_to_pick is null then null else (case when date_ready_to_ship is null then production_ve.calcworkdays(date_ready_to_pick,curdate()) else production_ve.calcworkdays(date_ready_to_pick,date_ready_to_ship)end)end;

-- B242 U
update production_ve.out_order_tracking set out_order_tracking.warehouse_delay = case when date_ready_to_ship_promised>curdate() then 0 else (case when date_ready_to_ship is null then production_ve.calcworkdays(date_ready_to_ship_promised,curdate()) else production_ve.calcworkdays(date_ready_to_ship_promised,date_ready_to_ship)end)end;

-- B243 U
update production_ve.out_order_tracking set out_order_tracking.warehouse_delay_counter = 1
where out_order_tracking.warehouse_delay>0;

-- B244 U
update production_ve.out_order_tracking set out_order_tracking.carrier_time = max_delivery_time-wh_time-supplier_leadtime;

-- B245 U
update production_ve.out_order_tracking set out_order_tracking.actual_carrier_time = case when date_ready_to_ship is null then null else (case when date_delivered is null then production_ve.calcworkdays(date_ready_to_ship,curdate()) else production_ve.calcworkdays(date_ready_to_ship,date_delivered)end)end;

-- B246 U
update production_ve.out_order_tracking set out_order_tracking.delivery_delay = case when date_ready_to_ship>=curdate() then 0 else (case when actual_carrier_time>carrier_time then actual_carrier_time-carrier_time else 0 end)end;

-- B247 U
update production_ve.out_order_tracking set out_order_tracking.delivery_delay_counter = 1
where out_order_tracking.delivery_delay>0 and out_order_tracking.date_delivered_promised<=curdate();

-- B248 U
update production_ve.out_order_tracking set out_order_tracking.delay_exceptions = case when out_order_tracking.supplier_leadtime>=out_order_tracking.max_delivery_time then 1 else 0 end;

-- B250 U 
update production_ve.out_order_tracking set out_order_tracking.ready_to_ship = 1 where out_order_tracking.date_ready_to_ship is not null;

-- B251 U
update production_ve.out_order_tracking set out_order_tracking.wh_workdays_to_r2s = case when date_ready_to_ship is null then null else (case when date_ready_to_pick is null then null else production_ve.calcworkdays(date_ready_to_pick, date_ready_to_ship)end)end;

-- B251b U
update production_ve.out_order_tracking set out_order_tracking.week_r2s_promised = production_ve.week_iso(date_ready_to_ship_promised), out_order_tracking.month_num_r2s_promised = date_format(date_ready_to_ship_promised, "%x-%m");

-- B252 U
update production_ve.out_order_tracking set out_order_tracking.r2s_workday_0 = case when production_ve.calcworkdays(date_ready_to_pick,curdate())<0 then 0 else (case when wh_workdays_to_r2s=0 then 1 else 0 end)end, out_order_tracking.r2s_workday_1 = case when production_ve.calcworkdays(date_ready_to_pick,curdate())<1 then 0 else (case when wh_workdays_to_r2s<=1 then 1 else 0 end)end, out_order_tracking.r2s_workday_2 = case when production_ve.calcworkdays(date_ready_to_pick,curdate())<2 then 0 else (case when wh_workdays_to_r2s<=2 then 1 else 0 end)end, out_order_tracking.r2s_workday_3 = case when production_ve.calcworkdays(date_ready_to_pick,curdate())<3 then 0 else (case when wh_workdays_to_r2s<=3 then 1 else 0 end)end, out_order_tracking.r2s_workday_4 = case when production_ve.calcworkdays(date_ready_to_pick,curdate())<4 then 0 else (case when wh_workdays_to_r2s<=4 then 1 else 0 end)end, out_order_tracking.r2s_workday_5 = case when production_ve.calcworkdays(date_ready_to_pick,curdate())<5 then 0 else (case when wh_workdays_to_r2s<=5 then 1 else 0 end)end, out_order_tracking.r2s_workday_6 = case when production_ve.calcworkdays(date_ready_to_pick,curdate())<6 then 0 else (case when wh_workdays_to_r2s<=6 then 1 else 0 end)end, out_order_tracking.r2s_workday_7 = case when production_ve.calcworkdays(date_ready_to_pick,curdate())<7 then 0 else (case when wh_workdays_to_r2s<=7 then 1 else 0 end)end, out_order_tracking.r2s_workday_8 = case when production_ve.calcworkdays(date_ready_to_pick,curdate())<8 then 0 else (case when wh_workdays_to_r2s<=8 then 1 else 0 end)end, out_order_tracking.r2s_workday_9 = case when production_ve.calcworkdays(date_ready_to_pick,curdate())<9 then 0 else (case when wh_workdays_to_r2s<=9 then 1 else 0 end)end, out_order_tracking.r2s_workday_10 = case when production_ve.calcworkdays(date_ready_to_pick,curdate())<10 then 0 else (case when wh_workdays_to_r2s<=10 then 1 else 0 end)end, out_order_tracking.r2s_workday_superior_10 = case when production_ve.calcworkdays(date_ready_to_pick,curdate())<10 then 0 else (case when wh_workdays_to_r2s>10 then 1 else 0 end)end;

-- B253 U
update production_ve.out_order_tracking set out_order_tracking.ready_to_pick = 1
where out_order_tracking.date_ready_to_pick is not null;
 
-- B256 U
update production_ve.out_order_tracking set out_order_tracking.date_payable_promised = date_procured
where out_order_tracking.oms_payment_event="entrega";
 
-- B257 U
update production_ve.out_order_tracking set out_order_tracking.date_payable_promised = date_procured
where out_order_tracking.oms_payment_event="pedido";
 
-- B258 U
update production_ve.out_order_tracking set out_order_tracking.date_payable_promised = date_procured-supplier_leadtime
where out_order_tracking.oms_payment_event="factura";

-- B259 U
update production_ve.out_order_tracking set out_order_tracking.delay_linio_promise = case when is_linio_promise = 1 and workdays_to_1st_attempt > 2 then 1 else 0 end;

-- B260
update production_ve.out_order_tracking inner join wmsprod_ve.entrega on out_order_tracking.wms_tracking_code = entrega.cod_rastreamento set out_order_tracking.delivery_fullfilment = entrega.delivery_fulfillment;
 
-- B261
update production_ve.out_order_tracking set out_order_tracking.split_order = case when delivery_fullfilment = 'partial' then 1 else 0 end;

-- B262
update production_ve.out_order_tracking set out_order_tracking.effective_1st_attempt = case when date_delivered = date_1st_attempt then 1 else 0 end;

-- B263
delete from production_ve.pro_value_from_order;

-- B264
insert into production_ve.pro_value_from_order
select order_number, 1/count(order_item_id) as value_from_order from production_ve.out_order_tracking t group by order_number order by date_ordered desc;

-- B265
update production_ve.out_order_tracking inner join production_ve.pro_value_from_order on out_order_tracking.order_number=pro_value_from_order.order_number set out_order_tracking.value_from_order=pro_value_from_order.value_from_order;

-- B267
update production_ve.out_order_tracking set out_order_tracking.shipped = case when date_shipped is not null then 1 else 0 end;

-- B268
update production_ve.out_order_tracking set out_order_tracking.delivered = case when date_delivered is not null then 1 else 0 end;
 
-- B269 
update production_ve.out_order_tracking set out_order_tracking.first_attempt = 1 where date_1st_attempt is not null;

-- B270 U
update production_ve.out_stock_hist set out_stock_hist.entrance_last_30= case when datediff(curdate(),date_entrance)<30 then 1 else 0 end, out_stock_hist.oms_po_last_30= case when datediff(curdate(),date_procurement_order)<=30 and datediff(curdate(),date_procurement_order)>0 then 1 else 0 end;
 
-- B271 U
update production_ve.out_order_tracking set out_order_tracking.procured_promised_last_30 = case when datediff(curdate(),date_procured_promised)<=30 and datediff(curdate(),date_procured_promised)>0 then 1 else 0 end;
 
-- B272 U
update production_ve.out_order_tracking set out_order_tracking.shipped_last_30 = case when datediff(curdate(),date_shipped)<=30 and datediff(curdate(),date_shipped)>0 then 1 else 0 end;
 
-- B273 U
update production_ve.out_order_tracking set out_order_tracking.delivered_promised_last_30 = case when datediff(curdate(),date_delivered_promised)<=30 and datediff(curdate(),date_delivered_promised)>0 then 1 else 0 end;

-- B276 U
update production_ve.out_order_tracking o inner join production_ve.tbl_order_detail t on o.order_item_id=t.item set o.actual_paid_price_after_tax=t.paid_price_after_vat, o.costo_after_tax=t.costo_after_vat;

-- Operaciones
-- Macro C

-- C001 D
delete production_ve.out_stock_hist.* from production_ve.out_stock_hist;

-- C002 A
insert into production_ve.out_stock_hist ( stock_item_id, barcode_wms, date_entrance, barcode_bob_duplicated, in_stock, wh_location )
select estoque.estoque_id, estoque.cod_barras, case when data_criacao is null then null else date(data_criacao) end as expr1, estoque.minucioso, posicoes.participa_estoque, estoque.endereco from (wmsprod_ve.estoque left join wmsprod_ve.itens_recebimento on estoque.itens_recebimento_id = itens_recebimento.itens_recebimento_id) left join wmsprod_ve.posicoes on estoque.endereco = posicoes.posicao;

-- C002 a
update production_ve.out_stock_hist inner join wmsprod_ve.movimentacoes on out_stock_hist.stock_item_id = movimentacoes.estoque_id set out_stock_hist.date_entrance = date(movimentacoes.data_criacao)
where movimentacoes.de_endereco='retorno de cancelamento' or movimentacoes.de_endereco='vendidos';

-- C003 D
delete production_ve.pro_unique_ean_bob.* from production_ve.pro_unique_ean_bob;

-- C004 A
insert into production_ve.pro_unique_ean_bob ( ean ) select produtos.ean as ean from bob_live_ve.catalog_simple inner join wmsprod_ve.produtos on catalog_simple.sku = produtos.sku where catalog_simple.status not like "deleted"
group by produtos.ean
having count(produtos.ean=1);

-- C102 U
update production_ve.out_stock_hist inner join wmsprod_ve.traducciones_producto on out_stock_hist.barcode_wms = traducciones_producto.identificador set out_stock_hist.sku_simple = sku;

-- C104 U
update production_ve.out_stock_hist inner join wmsprod_ve.estoque on out_stock_hist.stock_item_id = estoque.estoque_id set out_stock_hist.date_exit = date(data_ultima_movimentacao), out_stock_hist.exit_type = "sold" where out_stock_hist.wh_location="vendidos";

-- C105 U
update production_ve.out_stock_hist inner join wmsprod_ve.estoque on out_stock_hist.stock_item_id = estoque.estoque_id set out_stock_hist.date_exit = date(data_ultima_movimentacao), out_stock_hist.exit_type = "error" where out_stock_hist.wh_location="error";

-- C106 U
update (((production_ve.out_stock_hist inner join bob_live_ve.
catalog_simple on out_stock_hist.sku_simple = catalog_simple.sku) inner join bob_live_ve.
catalog_config on catalog_simple.fk_catalog_config = catalog_config.id_catalog_config) inner join bob_live_ve.
catalog_shipment_type on catalog_simple.fk_catalog_shipment_type = catalog_shipment_type.id_catalog_shipment_type) inner join bob_live_ve.supplier on catalog_config.fk_supplier = supplier.id_supplier set out_stock_hist.barcode_bob = barcode_ean, out_stock_hist.sku_config = catalog_config.sku, out_stock_hist.sku_name = catalog_config.name, out_stock_hist.sku_supplier_config = catalog_config.sku_supplier_config, out_stock_hist.model = catalog_config.model, out_stock_hist.cost = catalog_simple.cost, out_stock_hist.supplier_name = supplier.name, out_stock_hist.supplier_id = supplier.id_supplier, out_stock_hist.fullfilment_type_bob = catalog_shipment_type.name, out_stock_hist.price = catalog_simple.price;

-- C107 U
update production_ve.out_stock_hist inner join production_ve.pro_category_tree on out_stock_hist.sku_config = pro_category_tree.sku set out_stock_hist.category = cat1, out_stock_hist.cat2 = pro_category_tree.cat2;

-- C107a U
update production_ve.out_stock_hist inner join production_ve.exceptions_supplier_categories on out_stock_hist.supplier_name = exceptions_supplier_categories.supplier set out_stock_hist.category = "ropa, calzado y accesorios"
where out_stock_hist.category="deportes";

-- C107b U
update production_ve.out_stock_hist set out_stock_hist.category = "salud y cuidado personal"
where out_stock_hist.category="ropa, calzado y accesorios" and out_stock_hist.cat2="salud y cuidado personal";

-- C107c U
update production_ve.out_stock_hist inner join bob_live_ve.catalog_config on out_stock_hist.sku_config = catalog_config.sku inner join bob_live_ve.catalog_attribute_option_global_buyer on catalog_attribute_option_global_buyer.id_catalog_attribute_option_global_buyer = catalog_config.fk_catalog_attribute_option_global_buyer set out_stock_hist.buyer = catalog_attribute_option_global_buyer.name;

-- C107d U
update production_ve.out_stock_hist inner join production_ve.buyer_category on out_stock_hist.category = buyer_category.category set out_stock_hist.category_english = buyer_category.category_english;

-- C108 U
update production_ve.out_stock_hist set out_stock_hist.sold_last_30= case when datediff(curdate(),date_exit)<30 then 1 else 0 end, out_stock_hist.sold_yesterday = case when datediff(curdate(),(date_sub(date_exit, interval 1 day)))=0 then 1 else 0 end, out_stock_hist.sold_last_10 = case when datediff(curdate(),date_exit)<10 then 1 else 0 end, out_stock_hist.sold_last_7 = case when datediff(curdate(),date_exit)<7 then 1 else 0 end where out_stock_hist.exit_type="sold";

-- C108a U
update production_ve.out_stock_hist set out_stock_hist.entrance_last_30= case when datediff(curdate(),date_entrance)<30 then 1 else 0 end, out_stock_hist.oms_po_last_30= case when datediff(curdate(),date_procurement_order)<30 then 1 else 0 end;

-- C109 U
update production_ve.out_stock_hist set out_stock_hist.days_in_stock = case when date_exit is null then datediff(curdate(),date_entrance) else datediff(date_exit,date_entrance) end;

-- Extra
update production_ve.out_stock_hist set out_stock_hist.fullfilment_type_real = "inventory";

-- C110 U
update production_ve.out_stock_hist inner join (wmsprod_ve.itens_venda inner join wmsprod_ve.status_itens_venda on itens_venda.itens_venda_id = status_itens_venda.itens_venda_id) on out_stock_hist.stock_item_id = itens_venda.estoque_id set out_stock_hist.fullfilment_type_real = "crossdock"
where status_itens_venda.status="analisando quebra" or status_itens_venda.status="aguardando estoque";

-- C110 U Extra
update (production_ve.out_stock_hist inner join wmsprod_ve.itens_venda on out_stock_hist.stock_item_id = itens_venda.estoque_id) inner join wmsprod_ve.status_itens_venda on itens_venda.itens_venda_id = status_itens_venda.itens_venda_id set out_stock_hist.fullfilment_type_real = "dropshipping"
where (((status_itens_venda.status)="ds estoque reservado"));

-- C110 c
update production_ve.out_stock_hist inner join wmsprod_ve.movimentacoes on out_stock_hist.stock_item_id = movimentacoes.estoque_id set out_stock_hist.fullfilment_type_real = 'inventory'
where movimentacoes.de_endereco='retorno de cancelamento' or movimentacoes.de_endereco='vendidos';

-- C111 U
update production_ve.out_stock_hist inner join bob_live_ve.catalog_simple on out_stock_hist.sku_simple = catalog_simple.sku inner join bob_live_ve.catalog_tax_class on catalog_simple.fk_catalog_tax_class = catalog_tax_class.id_catalog_tax_class set out_stock_hist.tax_percentage = tax_percent, out_stock_hist.cost_w_o_vat = out_stock_hist.cost/(1+tax_percent/100);

-- C111b U
update production_ve.out_stock_hist set out_stock_hist.in_stock_cost_w_o_vat = out_stock_hist.cost_w_o_vat where out_stock_hist.in_stock=1;

-- C111c U
update production_ve.out_stock_hist set out_stock_hist.sold_last_30_cost_w_o_vat = out_stock_hist.cost_w_o_vat, out_stock_hist.sold_last_30_price = out_stock_hist.price
where out_stock_hist.sold_last_30=1;

-- C112 D
delete production_ve.pro_stock_hist_sku_count.* from production_ve.pro_stock_hist_sku_count;

-- C113 A
insert into production_ve.pro_stock_hist_sku_count ( sku_simple, sku_count )
select out_stock_hist.sku_simple, sum(out_stock_hist.item_counter) as sumofitem_counter from production_ve.out_stock_hist
group by out_stock_hist.sku_simple, out_stock_hist.in_stock having out_stock_hist.in_stock=1;

-- C114 U
update production_ve.out_stock_hist inner join production_ve.pro_stock_hist_sku_count on out_stock_hist.sku_simple = pro_stock_hist_sku_count.sku_simple set out_stock_hist.sku_counter= 1/pro_stock_hist_sku_count.sku_count;

-- C114b U
update production_ve.out_stock_hist set out_stock_hist.reserved = '0';

-- C115 U
update production_ve.out_stock_hist inner join wmsprod_ve.estoque on out_stock_hist.stock_item_id = estoque.estoque_id set out_stock_hist.reserved = "1"
where (((estoque.almoxarifado)="separando" or (estoque.almoxarifado)="estoque reservado" or (estoque.almoxarifado)="aguardando separacao"));

-- C116 U
update production_ve.out_stock_hist inner join bob_live_ve.catalog_simple on out_stock_hist.sku_simple = catalog_simple.sku set out_stock_hist.supplier_leadtime = catalog_simple.delivery_time_supplier;

-- C117 U
update production_ve.out_stock_hist inner join ((bob_live_ve.catalog_config inner join bob_live_ve.catalog_simple on catalog_config.id_catalog_config = catalog_simple.fk_catalog_config) inner join bob_live_ve.catalog_stock on catalog_simple.id_catalog_simple = catalog_stock.fk_catalog_simple) on out_stock_hist.sku_config = catalog_config.sku set out_stock_hist.visible = 1
where (((catalog_config.pet_status)="creation,edited,images") and ((catalog_config.pet_approved)=1) and ((catalog_config.status)="active") and ((catalog_simple.status)="active") and ((catalog_simple.price)>0) and ((catalog_config.display_if_out_of_stock)=0) and ((catalog_stock.quantity)>0) and ((catalog_simple.cost)>0)) or (((catalog_config.pet_status)="creation,edited,images") and ((catalog_config.pet_approved)=1) and ((catalog_config.status)="active") and ((catalog_simple.status)="active") and ((catalog_simple.price)>0) and ((catalog_config.display_if_out_of_stock)=1) and ((catalog_stock.quantity)>0) and ((catalog_simple.cost)>0));

-- C118 U
update production_ve.out_stock_hist set out_stock_hist.week_exit = production_ve.week_iso(date_exit);

-- C119a D
delete production_ve.pro_stock_hist_sold_last_30_count.* from production_ve.pro_stock_hist_sold_last_30_count;

-- C119b A
insert into production_ve.pro_stock_hist_sold_last_30_count ( sold_last_30, sku_simple, count_of_item_counter )
select out_stock_hist.sold_last_30, out_stock_hist.sku_simple, count(out_stock_hist.item_counter) as count_of_item_counter
from production_ve.out_stock_hist group by out_stock_hist.sold_last_30, out_stock_hist.sku_simple having out_stock_hist.sold_last_30=1;

-- C119c U
update production_ve.out_stock_hist inner join production_ve.pro_stock_hist_sold_last_30_count on out_stock_hist.sku_simple = pro_stock_hist_sold_last_30_count.sku_simple set out_stock_hist.sold_last_30_counter = 1/pro_stock_hist_sold_last_30_count.count_of_item_counter;

-- C120 U
update production_ve.out_stock_hist set out_stock_hist.sku_simple_blank = 1
where out_stock_hist.sku_simple is null;

-- C121 U
update (wmsprod_ve.itens_recebimento inner join wmsprod_ve.estoque on itens_recebimento.itens_recebimento_id = estoque.itens_recebimento_id) inner join production_ve.out_stock_hist on estoque.estoque_id = out_stock_hist.stock_item_id set out_stock_hist.quarantine = 1 where itens_recebimento.endereco="cuarentena" or itens_recebimento.endereco="cuarenta2";

-- C124 D
delete production_ve.pro_sum_last_5_out_of_6_wks.* from production_ve.pro_sum_last_5_out_of_6_wks;

-- C125 A
insert into production_ve.pro_sum_last_5_out_of_6_wks ( sku_simple, expr1 )
select max_last_6_wks.sku_simple, sumofcountofdate_exit-maxofcountofdate_exit as expr1 from production_ve.max_last_6_wks
group by max_last_6_wks.sku_simple, sumofcountofdate_exit-maxofcountofdate_exit;

-- C126 U
update production_ve.out_stock_hist inner join production_ve.pro_sum_last_5_out_of_6_wks on out_stock_hist.sku_simple = pro_sum_last_5_out_of_6_wks.sku_simple set out_stock_hist.sum_sold_last_5_out_of_6_wks = pro_sum_last_5_out_of_6_wks.expr1;

-- C127 U
update production_ve.out_stock_hist inner join bob_live_ve.catalog_config on out_stock_hist.sku_config = catalog_config.sku set out_stock_hist.package_height = catalog_config.package_height, out_stock_hist.package_length = catalog_config.package_length, out_stock_hist.package_width = catalog_config.package_width;

-- C128 U
update production_ve.out_stock_hist set out_stock_hist.vol_weight = package_height*package_length*package_width/5000;

-- C129 U
update production_ve.out_stock_hist inner join bob_live_ve.catalog_config on out_stock_hist.sku_config = catalog_config.sku set out_stock_hist.package_weight = catalog_config.package_weight;

-- C130 U
update production_ve.out_stock_hist set out_stock_hist.max_vol_w_vs_w = case when vol_weight>package_weight then vol_weight else package_weight end;

-- C131 U
update production_ve.out_stock_hist set out_stock_hist.package_measure = case when max_vol_w_vs_w>45.486 then "large" else (case when max_vol_w_vs_w>2.277 then "medium" else "small" end)end;

-- C132 U
update production_ve.out_stock_hist inner join wmsprod_ve.itens_inventario on out_stock_hist.barcode_wms = itens_inventario.cod_barras set out_stock_hist.sub_position = sub_endereco;

-- C133 U
update wmsprod_ve.posicoes inner join production_ve.out_stock_hist on posicoes.posicao = out_stock_hist.wh_location set out_stock_hist.position_type= posicoes.tipo_posicao;

-- C135 U
update production_ve.out_stock_hist set out_stock_hist.position_item_size_type = case when position_type="safe" then "small" else (case when position_type="mezanine" then "small" else (case when position_type="muebles" then "large" else "tbd" end)end)end;

-- C136 D
delete from production_ve.items_procured_in_transit;

-- C136 A
insert into production_ve.items_procured_in_transit ( sku_simple, number_ordered, unit_price )
select catalog_simple.sku, count(procurement_order_item.id_procurement_order_item) as countofid_procurement_order_item, avg(procurement_order_item.unit_price) as avgofunit_price
from bob_live_ve.catalog_simple inner join (procurement_live_ve.procurement_order_item inner join procurement_live_ve.procurement_order on procurement_order_item.fk_procurement_order = procurement_order.id_procurement_order) on catalog_simple.id_catalog_simple = procurement_order_item.fk_catalog_simple
where (((procurement_order_item.is_deleted)=0) and ((procurement_order.is_cancelled)=0) and ((procurement_order_item.sku_received)=0))
group by catalog_simple.sku;

-- C137 U
update production_ve.out_stock_hist inner join production_ve.items_procured_in_transit on out_stock_hist.sku_simple = items_procured_in_transit.sku_simple set out_stock_hist.items_procured_in_transit = number_ordered, out_stock_hist.procurement_price = unit_price;

-- C138 U
update (procurement_live_ve.procurement_order inner join ((bob_live_ve.catalog_simple inner join production_ve.out_stock_hist on catalog_simple.sku = out_stock_hist.sku_simple) inner join procurement_live_ve.procurement_order_item on catalog_simple.id_catalog_simple = procurement_order_item.fk_catalog_simple) on procurement_order.id_procurement_order = procurement_order_item.fk_procurement_order) set out_stock_hist.date_procurement_order = procurement_order.created_at;

-- C139 U
update production_ve.out_stock_hist set out_stock_hist.date_payable_promised = adddate(date_procurement_order, interval oms_payment_terms day) where out_stock_hist.oms_payment_event in("factura","pedido");

-- C140 U
update production_ve.out_stock_hist set out_stock_hist.date_payable_promised = adddate(date_procurement_order, interval oms_payment_terms + supplier_leadtime day)
where out_stock_hist.oms_payment_event="entrega";
 
/*-- C139 U 
update production_ve.out_stock_hist set out_stock_hist.date_payable_promised = date_entrance-supplier_leadtime
where out_stock_hist.oms_payment_event="factura";
 
-- C140 U
update production_ve.out_stock_hist set out_stock_hist.date_payable_promised = date_entrance
where out_stock_hist.oms_payment_event="entrega";
 
-- C141 U 
update production_ve.out_stock_hist set out_stock_hist.date_payable_promised = date_entrance
where out_stock_hist.oms_payment_event="pedido";*/
 
-- C142 U
update production_ve.out_stock_hist set out_stock_hist.sold = 1
where out_stock_hist.exit_type="sold";

-- C143 A 
insert into production_ve.sell_rate_simple(sku_simple,num_items,num_sales,average_days_in_stock)
select sku_simple, sum(item_counter),sum(sold),avg(days_in_stock)
from production_ve.out_stock_hist group by sku_simple; 
 
-- C143 U 
update production_ve.sell_rate_simple set sell_rate_simple.average_sell_rate = case when average_days_in_stock=0 then 0 else num_sales/average_days_in_stock end;
 
-- C144 U
update production_ve.sell_rate_simple set sell_rate_simple.remaining_days = case when average_sell_rate=0 then 0 else num_items/average_sell_rate end;
 
-- C145 U
update production_ve.out_stock_hist inner join production_ve.sell_rate_simple on out_stock_hist.sku_simple = sell_rate_simple.sku_simple set out_stock_hist.average_remaining_days = sell_rate_simple.remaining_days;

-- C146 D
delete from production_ve.pro_max_days_in_stock; 

-- C147 A
insert into production_ve.pro_max_days_in_stock select * from (select sku_simple, days_in_stock from production_ve.out_stock_hist order by sku_simple, days_in_stock desc)n group by sku_simple;

-- C148 U  
update production_ve.out_stock_hist inner join production_ve.pro_max_days_in_stock on out_stock_hist.sku_simple= pro_max_days_in_stock.sku_simple set out_stock_hist.max_days_in_stock = pro_max_days_in_stock.max_days_in_stock;

-- C149 U
update production_ve.out_stock_hist set out_stock_hist.week_payable_promised = production_ve.week_iso(date_payable_promised);

-- C150 U
update production_ve.out_stock_hist set out_stock_hist. week_procurement_order = production_ve.week_iso(date_procurement_order);

-- C151 U
update production_ve.out_stock_hist inner join bob_live_ve.catalog_simple on out_stock_hist.sku_simple = catalog_simple.sku set out_stock_hist.delivery_cost_supplier = catalog_simple.delivery_cost_supplier;

-- C152 U
update production_ve.out_stock_hist set cogs = cost_w_o_vat + delivery_cost_supplier;

-- C153
update production_ve.out_stock_hist set out_stock_hist.days_payable_since_entrance = case when date_entrance is null then null else (case when oms_payment_event in('factura','pedido') then oms_payment_terms - supplier_leadtime else (case when oms_payment_event = 'entrega' then oms_payment_terms end)end)end;

-- Operaciones
-- Macro R

-- R100 D
delete production_ve.pro_leadtime_track_ow.*
from production_ve.pro_leadtime_track_ow;

-- R101 D
delete production_ve.pro_leadtime_track_crossd.*
from production_ve.pro_leadtime_track_crossd;

-- R102 D
delete production_ve.out_leadtime_tracking.*
from production_ve.out_leadtime_tracking;

-- R103 A
insert into production_ve.pro_leadtime_track_crossd ( category, sku_config, sku_simple, supplier_leadtime, max_delivery_time, min_delivery_time, name, visible, max_delivery_time_published, min_delivery_time_published )
select categories_no_duplicates.maxofcat1, catalog_config.sku as sku_config, catalog_simple.sku as sku, sales_order_item.delivery_time_supplier, delivery_time_crossdocking.max_delivery_time as max_delivery_time, delivery_time_crossdocking.min_delivery_time as min_delivery_time, catalog_shipment_type.name, case when sku_simple is null then 0 else 1 end as visible, catalog_simple.max_delivery_time, catalog_simple.min_delivery_time
from (production_ve.items_visible right join ((((bob_live_ve.catalog_config inner join bob_live_ve.catalog_simple on catalog_config.id_catalog_config = catalog_simple.fk_catalog_config) inner join production_ve.categories_no_duplicates on catalog_config.sku = categories_no_duplicates.sku) inner join production_ve.delivery_time_crossdocking on catalog_simple.delivery_time_supplier = delivery_time_crossdocking.supplier_leadtime) inner join bob_live_ve.catalog_shipment_type on catalog_simple.fk_catalog_shipment_type = catalog_shipment_type.id_catalog_shipment_type) on items_visible.sku_config = catalog_config.sku) inner join bob_live_ve.sales_order_item on catalog_simple.sku = sales_order_item.sku
group by categories_no_duplicates.maxofcat1, catalog_config.sku, catalog_simple.sku, sales_order_item.delivery_time_supplier, delivery_time_crossdocking.max_delivery_time, delivery_time_crossdocking.min_delivery_time, catalog_shipment_type.name, case when sku_simple is null then 0 else 1 end, catalog_simple.max_delivery_time, catalog_simple.min_delivery_time, sales_order_item.delivery_time_supplier, catalog_config.status
having (((catalog_shipment_type.name) like "crossdocking") and ((catalog_config.status) like "active"))
order by categories_no_duplicates.maxofcat1;

-- R104 U
update production_ve.pro_leadtime_track_crossd set pro_leadtime_track_crossd.error_max_delivery = case when  max_delivery_time=max_delivery_time_published then 0 else 1 end, pro_leadtime_track_crossd.error_min_delivery = case when min_delivery_time=min_delivery_time_published then 0 else 1 end, pro_leadtime_track_crossd.error = case when error_max_delivery+error_min_delivery>0 then 1 else 0 end;

-- R105 U 
update production_ve.pro_leadtime_track_crossd inner join (bob_live_ve.catalog_config inner join bob_live_ve.catalog_attribute_option_global_buyer on catalog_config.fk_catalog_attribute_option_global_buyer = catalog_attribute_option_global_buyer.id_catalog_attribute_option_global_buyer) on pro_leadtime_track_crossd.sku_config = catalog_config.sku set pro_leadtime_track_crossd.buyer = catalog_attribute_option_global_buyer.name;

update production_ve.pro_leadtime_track_crossd set item_counter = 1;

-- R107 A
insert into production_ve.pro_leadtime_track_ow ( category, sku_config, sku_simple, max_delivery_time, min_delivery_time, name, package_weight, status, visible, max_delivery_time_published, min_delivery_time_published )
select categories_no_duplicates.maxofcat1, catalog_config.sku as sku_config, catalog_simple.sku as sku, case when catalog_config.package_weight<21 then (case when catalog_config.package_height<41 then (case when catalog_config.package_length<41 then (case when catalog_config.package_width<41 then 5 else 7 end) else 7 end) else 7 end) else 7 end as max_delivery_time, case when catalog_config.package_weight<21 then ( case when catalog_config.package_height<41 then (case when catalog_config.package_length<41 then (case when catalog_config.package_width<41 then 3 else 4 end) else 4 end) else 4 end) else 4 end as min_delivery_time, catalog_shipment_type.name, catalog_config.package_weight, catalog_config.status, case when sku_simple is null then 0 else 1 end as visible, catalog_simple.max_delivery_time as max_delivery_time_published, catalog_simple.min_delivery_time as min_delivery_time_published
from production_ve.items_visible right join ((((bob_live_ve.catalog_config inner join bob_live_ve.catalog_simple on catalog_config.id_catalog_config = catalog_simple.fk_catalog_config) inner join production_ve.categories_no_duplicates on catalog_config.sku = categories_no_duplicates.sku) inner join production_ve.delivery_time_crossdocking on catalog_simple.delivery_time_supplier = delivery_time_crossdocking.supplier_leadtime) inner join bob_live_ve.catalog_shipment_type on catalog_simple.fk_catalog_shipment_type = catalog_shipment_type.id_catalog_shipment_type) on items_visible.sku_config = catalog_config.sku
where (((catalog_shipment_type.name) like "own warehouse") and ((catalog_config.status) like "active")) or (((catalog_shipment_type.name) like "consignment") and ((catalog_config.status) like "active"))
order by catalog_config.package_weight desc;

-- R108 U
update production_ve.pro_leadtime_track_ow set pro_leadtime_track_ow.error_max_delivery = case when max_delivery_time=max_delivery_time_published then 0 else 1 end, pro_leadtime_track_ow.error_min_delivery = case when min_delivery_time=min_delivery_time_published then 0 else 1 end, pro_leadtime_track_ow.error = case when error_max_delivery+error_min_delivery>0 then 1 else 0 end;

-- R109 U
update production_ve.pro_leadtime_track_ow inner join (bob_live_ve.catalog_config inner join bob_live_ve.catalog_attribute_option_global_buyer on catalog_config.fk_catalog_attribute_option_global_buyer = catalog_attribute_option_global_buyer.id_catalog_attribute_option_global_buyer) on pro_leadtime_track_ow.sku_config = catalog_config.sku set pro_leadtime_track_ow.buyer = catalog_attribute_option_global_buyer.name;

update production_ve.pro_leadtime_track_ow set item_counter = 1;

-- R111 A
insert into production_ve.out_leadtime_tracking ( category, sku_config, sku_simple, supplier_leadtime, max_delivery_time, max_delivery_time_published, error_max_delivery, min_delivery_time, min_delivery_time_published, error_min_delivery, error, fullfilment_type_bob, visible, buyer, item_counter )
select pro_leadtime_track_crossd.category, pro_leadtime_track_crossd.sku_config, pro_leadtime_track_crossd.sku_simple, pro_leadtime_track_crossd.supplier_leadtime, pro_leadtime_track_crossd.max_delivery_time, pro_leadtime_track_crossd.max_delivery_time_published, pro_leadtime_track_crossd.error_max_delivery, pro_leadtime_track_crossd.min_delivery_time, pro_leadtime_track_crossd.min_delivery_time_published, pro_leadtime_track_crossd.error_min_delivery, pro_leadtime_track_crossd.error, pro_leadtime_track_crossd.name, pro_leadtime_track_crossd.visible, pro_leadtime_track_crossd.buyer, pro_leadtime_track_crossd.item_counter
from production_ve.pro_leadtime_track_crossd;

-- R112 A
insert into production_ve.out_leadtime_tracking ( category, sku_config, sku_simple, max_delivery_time, max_delivery_time_published, error_max_delivery, min_delivery_time, error_min_delivery, error, fullfilment_type_bob, visible, buyer, package_weight, status, item_counter )
select pro_leadtime_track_ow.category, pro_leadtime_track_ow.sku_config, pro_leadtime_track_ow.sku_simple, pro_leadtime_track_ow.max_delivery_time, pro_leadtime_track_ow.max_delivery_time_published, pro_leadtime_track_ow.error_max_delivery, pro_leadtime_track_ow.min_delivery_time, pro_leadtime_track_ow.error_min_delivery, pro_leadtime_track_ow.error, pro_leadtime_track_ow.name, pro_leadtime_track_ow.visible, pro_leadtime_track_ow.buyer, pro_leadtime_track_ow.package_weight, pro_leadtime_track_ow.status, pro_leadtime_track_ow.item_counter
from production_ve.pro_leadtime_track_ow;

-- R113 U
update production_ve.out_leadtime_tracking inner join ((bob_live_ve.catalog_simple inner join bob_live_ve.catalog_config on catalog_simple.fk_catalog_config = catalog_config.id_catalog_config) inner join bob_live_ve.supplier on catalog_config.fk_supplier = supplier.id_supplier) on out_leadtime_tracking.sku_simple = catalog_simple.sku set out_leadtime_tracking.supplier_id = id_supplier, out_leadtime_tracking.supplier_name = supplier.name;

select  'daily execution ops: end',now();

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `daily_marketing`()
BEGIN

#date

DELETE FROM tbl_daily_marketing WHERE date=CAST(CURDATE()-1 as date);

INSERT INTO tbl_daily_marketing (date, channel) 
SELECT DISTINCT date, channel_group FROM tbl_order_detail WHERE date=CAST(CURDATE()-1 as date);

#VISITS	
UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, reporting_channel, sum(visits) visits
FROM daily_costs_visits_per_channel
GROUP BY date, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_daily_marketing.visits=daily_costs_visits_per_channel.visits;

#GROSS ORDERS
UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, channel_group, sum(gross_orders) gross_orders
FROM tbl_order_detail
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=tbl_order_detail.channel_group
SET tbl_daily_marketing.gross_orders=tbl_order_detail.gross_orders;

#NET ORDERS
UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, channel_group, sum(net_orders) net_orders
FROM tbl_order_detail
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=tbl_order_detail.channel_group
SET tbl_daily_marketing.net_orders=tbl_order_detail.net_orders;

#ORDERS NET EXPECTED

UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, reporting_channel, sum(net_orders_e) net_orders_e
FROM daily_costs_visits_per_channel
GROUP BY date, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_daily_marketing.orders_net_expected=daily_costs_visits_per_channel.net_orders_e;

#CONVERSION RATE
UPDATE tbl_daily_marketing SET conversion_rate=IF(gross_orders/visits IS NULL, 0, gross_orders/visits);

#PENDING REVENUE
UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, channel_group, 
(SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat))/15 pending_sales
FROM tbl_order_detail WHERE tbl_order_detail.pending=1 
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing.date 
SET tbl_daily_marketing.pending_revenue=tbl_order_detail.pending_sales
WHERE tbl_daily_marketing.channel=tbl_order_detail.channel_group;

#GROSS REVENUE
UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, channel_group, 
(SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat))/15 gross_sales
FROM tbl_order_detail WHERE tbl_order_detail.obc=1 
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing.date 
SET tbl_daily_marketing.gross_revenue=tbl_order_detail.gross_sales
WHERE tbl_daily_marketing.channel=tbl_order_detail.channel_group;

#NET REVENUE
UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, channel_group, 
(SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat))/15 net_sales
FROM tbl_order_detail WHERE tbl_order_detail.oac=1 AND tbl_order_detail.returned=0  
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing.date 
SET tbl_daily_marketing.net_revenue=tbl_order_detail.net_sales
WHERE tbl_daily_marketing.channel=tbl_order_detail.channel_group;

#NET REVENUE EXPECTED

UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, 
reporting_channel, sum(net_rev_e)/15 net_rev_e
FROM daily_costs_visits_per_channel
GROUP BY date, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_daily_marketing.net_revenue_expected=daily_costs_visits_per_channel.net_rev_e;
UPDATE tbl_daily_marketing SET net_revenue_expected = net_revenue WHERE net_revenue_expected=0;

#MONTHLY TARGET

UPDATE tbl_daily_marketing  
INNER JOIN (SELECT date, channel_group, sum(target_net_sales) daily_target
FROM daily_targets_per_channel
GROUP BY date, channel_group) daily_targets_per_channel
ON daily_targets_per_channel.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=daily_targets_per_channel.channel_group
SET tbl_daily_marketing.daily_target=daily_targets_per_channel.daily_target;

#COST VS REV
UPDATE tbl_daily_marketing SET target_vs_rev=(net_revenue_expected/daily_target)-1;

#AVG TICKET
UPDATE tbl_daily_marketing SET avg_ticket=net_revenue/net_orders;

#MARKETING COST

UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, 
reporting_channel, sum(cost_local)/15 cost
FROM daily_costs_visits_per_channel
GROUP BY date, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_daily_marketing.marketing_cost=daily_costs_visits_per_channel.cost;

#COST/REV
UPDATE tbl_daily_marketing SET cost_rev=IF(marketing_cost/net_revenue_expected IS NULL, 0, marketing_cost/net_revenue_expected);

#CPO
UPDATE tbl_daily_marketing SET cost_per_order=IF(marketing_cost/net_orders IS NULL, 0, marketing_cost/net_orders);



#CPO TARGET

UPDATE tbl_daily_marketing  
INNER JOIN (SELECT date, channel_group, MAX(target_cpo) target_cpo
FROM daily_targets_per_channel
GROUP BY date, channel_group) daily_targets_per_channel
ON daily_targets_per_channel.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=daily_targets_per_channel.channel_group
SET tbl_daily_marketing.cost_per_order_target=daily_targets_per_channel.target_cpo;

#CPO VS TARGET
UPDATE tbl_daily_marketing SET cpo_vs_target_cpo=(cost_per_order/cost_per_order_target)-1;

#NEW CLIENTS
UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, channel_group, sum(new_customers) nc
FROM tbl_order_detail
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=tbl_order_detail.channel_group
SET tbl_daily_marketing.new_clients=tbl_order_detail.nc;

#NEW CLIENTS EXPECTED
UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, 
reporting_channel, sum(new_customers_e) nc_e
FROM daily_costs_visits_per_channel
GROUP BY date, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_daily_marketing.new_clientes_expected=daily_costs_visits_per_channel.nc_e;
UPDATE tbl_daily_marketing SET new_clientes_expected=new_clients WHERE new_clientes_expected=0;

#CAC
UPDATE tbl_daily_marketing SET client_acquisition_cost=marketing_cost/new_clientes_expected;

#PC2_COST

UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, channel_group, 
(SUM(costo_after_vat)+SUM(delivery_cost_supplier)+SUM(wh)+SUM(cs)+SUM(payment_cost)+SUM(shipping_cost))/15 pc2_costs
FROM tbl_order_detail WHERE tbl_order_detail.oac=1 AND tbl_order_detail.returned=0  
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing.date 
SET tbl_daily_marketing.pc2_costs=tbl_order_detail.pc2_costs
WHERE tbl_daily_marketing.channel=tbl_order_detail.channel_group;

#PC2
UPDATE tbl_daily_marketing SET profit_contribution_2=1-(pc2_costs/net_revenue);

#MOM
select 'Month over month: ', now();
CALL mom_net_sales_per_channel();

UPDATE tbl_daily_marketing INNER JOIN tbl_mom_net_sales_per_channel 
ON tbl_daily_marketing.channel = tbl_mom_net_sales_per_channel.channel_group
AND tbl_daily_marketing.date = tbl_mom_net_sales_per_channel.date
SET tbl_daily_marketing.month_over_month = tbl_mom_net_sales_per_channel.month_over_month;

INSERT IGNORE INTO tbl_daily_marketing (date, channel) 
VALUES(date_add(now(), interval -1 day), 'MTD');

UPDATE tbl_daily_marketing INNER JOIN tbl_mom_net_sales_per_channel 
ON tbl_daily_marketing.channel = tbl_mom_net_sales_per_channel.channel_group
AND tbl_daily_marketing.date = tbl_mom_net_sales_per_channel.date
SET tbl_daily_marketing.month_over_month = tbl_mom_net_sales_per_channel.month_over_month
where channel_group = 'MTD';

#Adjusted revenue
select 'Adjusted revenue: ', now();
#Net
update tbl_daily_marketing m left join 
(
select t1.*, t2.totalAbout, t1.totalChannel/t2.totalAbout as proporcion, t3.netSales, t1.totalChannel/t2.totalAbout*t3.netSales adjusted_net_revenue
from
(select t.date, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by t.date desc,l.about_linio, t.channel_group) t1 inner join
(select t.date, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by t.date desc,l.about_linio) t2 on t1.date = t2.date and t1.about_linio = t2.about_linio
inner join
(select date, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0
group by date desc, t.about_linio) t3 on t3.date = t2.date and t3.about_linio = t2.about_linio) t 
on m.date = t.date and m.channel = t.channel_group 
set m.adjusted_net_revenue = ifnull(m.net_revenue + t.adjusted_net_revenue/15, m.net_revenue)
where m.channel <> 'Tele Sales';

update tbl_daily_marketing m left join 
(
select t1.*, t2.totalAbout, t1.totalChannel/t2.totalAbout as proporcion, t3.netSales, t1.totalChannel/t2.totalAbout*t3.netSales adjusted_net_revenue
from
(select t.date, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by t.date desc,l.about_linio, t.channel_group) t1 inner join
(select t.date, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by t.date desc,l.about_linio) t2 on t1.date = t2.date and t1.about_linio = t2.about_linio
inner join
(select date, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0
group by date desc, t.about_linio) t3 on t3.date = t2.date and t3.about_linio = t2.about_linio) t 
on m.date = t.date and m.channel = t.channel_group set m.adjusted_net_revenue = ifnull(t.adjusted_net_revenue/15,0)
where m.channel = 'Tele Sales';

#Gross
update tbl_daily_marketing m left join 
(
select t1.*, t2.totalAbout, t1.totalChannel/t2.totalAbout as proporcion, t3.netSales, t1.totalChannel/t2.totalAbout*t3.netSales adjusted_revenue
from
(select t.date, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where obc  = 1
group by t.date desc,l.about_linio, t.channel_group) t1 inner join
(select t.date, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where obc  = 1
group by t.date desc,l.about_linio) t2 on t1.date = t2.date and t1.about_linio = t2.about_linio
inner join
(select date, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and obc  = 1
group by date desc, t.about_linio) t3 on t3.date = t2.date and t3.about_linio = t2.about_linio) t 
on m.date = t.date and m.channel = t.channel_group 
set m.adjusted_gross_revenue = ifnull(m.gross_revenue + t.adjusted_revenue/15, m.gross_revenue)
where m.channel <> 'Tele Sales';

update tbl_daily_marketing m left join 
(
select t1.*, t2.totalAbout, t1.totalChannel/t2.totalAbout as proporcion, t3.netSales, t1.totalChannel/t2.totalAbout*t3.netSales adjusted_revenue
from
(select t.date, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where obc  = 1
group by t.date desc,l.about_linio, t.channel_group) t1 inner join
(select t.date, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where obc  = 1
group by t.date desc,l.about_linio) t2 on t1.date = t2.date and t1.about_linio = t2.about_linio
inner join
(select date, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and obc  = 1
group by date desc, t.about_linio) t3 on t3.date = t2.date and t3.about_linio = t2.about_linio) t 
on m.date = t.date and m.channel = t.channel_group 
set m.adjusted_gross_revenue = ifnull(t.adjusted_revenue/15,0)
where m.channel = 'Tele Sales';

#Pending
update tbl_daily_marketing m left join 
(
select t1.*, t2.totalAbout, t1.totalChannel/t2.totalAbout as proporcion, t3.netSales, t1.totalChannel/t2.totalAbout*t3.netSales adjusted_revenue
from
(select t.date, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where pending  = 1
group by t.date desc,l.about_linio, t.channel_group) t1 inner join
(select t.date, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where pending  = 1
group by t.date desc,l.about_linio) t2 on t1.date = t2.date and t1.about_linio = t2.about_linio
inner join
(select date, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and pending  = 1
group by date desc, t.about_linio) t3 on t3.date = t2.date and t3.about_linio = t2.about_linio) t 
on m.date = t.date and m.channel = t.channel_group 
set m.adjusted_pending_revenue = ifnull(m.pending_revenue + t.adjusted_revenue/15, m.pending_revenue)
where m.channel <> 'Tele Sales';

update tbl_daily_marketing m left join 
(
select t1.*, t2.totalAbout, t1.totalChannel/t2.totalAbout as proporcion, t3.netSales, t1.totalChannel/t2.totalAbout*t3.netSales adjusted_revenue
from
(select t.date, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where pending  = 1
group by t.date desc,l.about_linio, t.channel_group) t1 inner join
(select t.date, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where pending  = 1
group by t.date desc,l.about_linio) t2 on t1.date = t2.date and t1.about_linio = t2.about_linio
inner join
(select date, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and pending  = 1
group by date desc, t.about_linio) t3 on t3.date = t2.date and t3.about_linio = t2.about_linio) t 
on m.date = t.date and m.channel = t.channel_group set m.adjusted_pending_revenue = ifnull(t.adjusted_revenue/15,0)
where m.channel = 'Tele Sales';



END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `daily_report`()
begin

truncate development_ve.tbl_dailyReport;
insert into development_ve.tbl_dailyReport (date) select dt from development_ve.calendar where dt>'2012-07-31' and dt<now();

truncate development_ve.tbl_dailyReport;
insert into development_ve.tbl_dailyReport (date) select dt from development_ve.calendar where dt>'2012-07-31' and dt<now();

#gross
update development_ve.tbl_dailyReport join 
(select date,sum(unit_price) as grossrev, count(distinct order_nr,oac,returned,pending) as grossorders, 
	sum(unit_price_after_vat) as grosssalesprecancel
            from development_ve.tbl_order_detail where   (obc = '1')
            group by date) as tbl_gross on tbl_gross.date=tbl_dailyReport.date
set tbl_dailyReport.grossrev=tbl_gross.grossrev , tbl_dailyReport.grossorders=tbl_gross.grossorders, 
tbl_dailyReport.grosssalesprecancel=tbl_gross.grosssalesprecancel;

#cancel
update development_ve.tbl_dailyReport join 
(select date,sum(unit_price_after_vat) as cancelrev, count(distinct order_nr) as cancelorders
from development_ve.tbl_order_detail 
where   (cancel = '1')
group by date) as tbl_cancel on tbl_cancel.date=tbl_dailyReport.date
set tbl_dailyReport.cancellations=tbl_cancel.cancelrev , tbl_dailyReport.cancelorders=tbl_cancel.cancelorders;


#pending
update development_ve.tbl_dailyReport join 
(select date,sum(unit_price_after_vat) as pendingrev, count(distinct order_nr) as pendingorders
from development_ve.tbl_order_detail 
where   (pending = '1')
group by date) as tbl_pending on tbl_pending.date=tbl_dailyReport.date
set tbl_dailyReport.pendingcop=tbl_pending.pendingrev , tbl_dailyReport.pendingorders=tbl_pending.pendingorders;


#cogs_gross_post_cancel
update development_ve.tbl_dailyReport join 
(select date,(sum(costo_after_vat) + sum(delivery_cost_supplier)) as cogs_gross_post_cancel
from development_ve.tbl_order_detail 
where   (oac = '1')
group by date) as tbl_cogs_post_cancel on tbl_cogs_post_cancel.date=tbl_dailyReport.date
set tbl_dailyReport.cogs_gross_post_cancel=tbl_cogs_post_cancel.cogs_gross_post_cancel;

#pending
update development_ve.tbl_dailyReport join 
(select date,sum(unit_price_after_vat) as returnrev, count(distinct order_nr) as returnorders
from development_ve.tbl_order_detail 
where   (returned = '1')
group by date) as tbl_returned on tbl_returned.date=tbl_dailyReport.date
set tbl_dailyReport.returnedcop=tbl_returned.returnrev , tbl_dailyReport.returnedorders=tbl_returned.returnorders;

#net
update development_ve.tbl_dailyReport join 
(select date,sum(unit_price_after_vat) as netsalesprevoucher, sum(coupon_money_after_vat) as vouchermktcost,
sum(paid_price_after_vat) as netsales,count(distinct order_nr) as netorders,
 (sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
count(order_nr)/count(distinct order_nr) as averageitemsperbasket,
count(distinct custid) as customers,
sum(shipping_fee) as shippingfee, 
sum(shipping_cost)as shippingcost, 
sum(payment_cost) as paymentcost
from development_ve.tbl_order_detail 
where   (returned = '0') and (oac = '1')
group by date) as tbl_net on tbl_net.date=tbl_dailyReport.date
set tbl_dailyReport.netsalesprevoucher=tbl_net.netsalesprevoucher , tbl_dailyReport.vouchermktcost=tbl_net.vouchermktcost,
tbl_dailyReport.netsales=tbl_net.netsales,tbl_dailyReport.netorders=tbl_net.netorders,
tbl_dailyReport.cogs_on_net_sales=tbl_net.cogs_on_net_sales,tbl_dailyReport.averageitemsperbasket=tbl_net.averageitemsperbasket,
tbl_dailyReport.customers=tbl_net.customers,
tbl_dailyReport.shippingfee=tbl_net.shippingfee/1.12,
tbl_dailyReport.shippingcost=tbl_net.shippingcost,tbl_dailyReport.paymentcost=tbl_net.paymentcost;

 
#categorías
#electrodomésticos
update development_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from development_ve.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 = 'electrodomésticos')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_appiliances=tbl_cat.netsales , tbl_dailyReport.cogs_appiliances=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_appliances=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_appliances=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_appliances=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_appliances=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_appliances=tbl_cat.payment_fee;

#cámaras y fotografía
update development_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from development_ve.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 = 'cámaras y fotografía')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_photography=tbl_cat.netsales , tbl_dailyReport.cogs_photography=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_photography=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_photography=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_photography=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_photography=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_photography=tbl_cat.payment_fee;


#tv, video y audio
update development_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from development_ve.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 ='tv, audio y video')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_tv_audio_video=tbl_cat.netsales , tbl_dailyReport.cogs_tv_audio_video=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_tv_audio_video=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_tv_audio_video=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_tv_audio_video=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_tv_audio_video=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_tv_audio_video=tbl_cat.payment_fee;

#libros
update development_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from development_ve.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 = 'libros')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_books=tbl_cat.netsales , tbl_dailyReport.cogs_books=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_books=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_books=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_books=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_books=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_books=tbl_cat.payment_fee;

#videojuegos
update development_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from development_ve.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 = 'videojuegos')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_videogames=tbl_cat.netsales , tbl_dailyReport.cogs_videogames=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_videogames=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_videogames=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_videogames=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_videogames=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_videogames=tbl_cat.payment_fee;

#fashion
update development_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from development_ve.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 = 'accesorios de moda' or n1 = 'ropa, calzado y accesorios')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_fashion=tbl_cat.netsales , tbl_dailyReport.cogs_fashion=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_fashion=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_fashion=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_fashion=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_fashion=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_fashion=tbl_cat.payment_fee;


#cuidado personal
update development_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from development_ve.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 like '%cuidado personal%' or n1 = 'secadores')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_health_beauty=tbl_cat.netsales , tbl_dailyReport.cogs_health_beauty=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_health_beauty=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_health_beauty=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_health_beauty=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_health_beauty=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_health_beauty=tbl_cat.payment_fee;


#teléfonos y gps
update development_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from development_ve.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 = 'celulares, teléfonos y gps' or n1 = 'ipad 100')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_cellphones=tbl_cat.netsales , tbl_dailyReport.cogs_cellphones=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_cellphones=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_cellphones=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_cellphones=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_cellphones=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_cellphones=tbl_cat.payment_fee;


 
#computadores y tablets    
update development_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from development_ve.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 = 'computadoras y tabletas')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_computing=tbl_cat.netsales , tbl_dailyReport.cogs_computing=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_computing=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_computing=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_computing=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_computing=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_computing=tbl_cat.payment_fee;


#hogar y muebles   
update development_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from development_ve.tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 = 'línea blanca' or n1 = 'comerdor y cocina' or n1 = 'hogar')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_hogar=tbl_cat.netsales , tbl_dailyReport.cogs_hogar=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_hogar=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_hogar=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_hogar=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_hogar=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_hogar=tbl_cat.payment_fee;


#juguetes, niños y bebés 
update development_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 like 'juguetes%')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_kids_babies=tbl_cat.netsales , tbl_dailyReport.cogs_kids_babies=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_kids_babies=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_kids_babies=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_kids_babies=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_kids_babies=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_kids_babies=tbl_cat.payment_fee;

#Camping y Exteriores
update development_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from tbl_order_detail 
where  (oac = '1') and (returned = '0') and(n1 ='camping y exteriores')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_outdoor=tbl_cat.netsales , tbl_dailyReport.cogs_outdoor=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_outdoor=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_outdoor=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_outdoor=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_kids_outdoor=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_outdoor=tbl_cat.payment_fee;

#paidchannels
 update development_ve.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as net_sales_paid_channel,
sum(coupon_money_after_vat) as voucher_paid_channel
from development_ve.tbl_order_detail 
where  (oac = '1') and (returned = '0') 
and   ((source_medium like '%price comparison%')
  or (source_medium like '%socialmediaads%')
  or (source_medium like '%cpc%')
  or (source_medium like '%sociomantic%'))
group by date) as tbl_paid on tbl_paid.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_paid_channel=tbl_paid.net_sales_paid_channel
 , tbl_dailyReport.voucher_paid_channel=tbl_paid.voucher_paid_channel;

# new customers
update development_ve.tbl_dailyReport join  
(select firstorder as date,count(*) as newcustomers from development_ve.view_cohort 
group by firstorder) as tbl_nc on tbl_nc.date=tbl_dailyReport.date
set tbl_dailyReport.newcustomers=tbl_nc.newcustomers;


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `daily_report_EUR`()
BEGIN

DECLARE rate Integer;
#TO-DO query de una tabla con las tasas de cambio
SET rate = 15;

truncate tbl_dailyReport_EUR;
insert into tbl_dailyReport_EUR (date) select dt from calendar where dt>'2012-07-31' and dt<now();

update tbl_dailyReport_EUR
set year = year(date) , month = month(date) , yrmonth = concat(year,if(month<10,concat(0,month),month));

#Visits
UPDATE tbl_dailyReport_EUR t INNER JOIN tbl_dailyReport_EUR g 
ON t.date = g.date
SET t.Visits = g.visits;

#Visits
UPDATE tbl_dailyReport_EUR t INNER JOIN tbl_dailyReport_EUR g 
ON t.date = g.date
SET t.UniqueVisits = g.UniqueVisits;

#Mobile Visits
UPDATE tbl_dailyReport_EUR t INNER JOIN tbl_dailyReport_EUR g 
ON t.date = g.date
SET t.MobileVisits = if(g.MobileVisits is null, 0, g.MobileVisits);

#EUR
update tbl_dailyReport_EUR e join 
tbl_dailyReport_local l on e.date = l.date
join tbl_exchange_rate 
on tbl_exchange_rate.yrmonth = cast(concat(year(e.date),if(month(e.date)<10, concat(0,month(e.date)),month(e.date))) as unsigned )
set e.GrossRev = l.GrossRev/rate,
e.GrossSalesPreCancel = l.GrossSalesPreCancel/rate,
e.Cancellations = l.Cancellations/rate,
e.Pending = l.PendingCOP/rate,
e.cogs_gross_post_cancel = l.cogs_gross_post_cancel/rate,
e.Returned = l.ReturnedCop/rate,
e.NetSales = l.NetSales/rate,
e.NetSalesPreVoucher = l.NetSalesPreVoucher/rate,
e.VoucherMktCost = l.VoucherMktCost/rate,
e.cogs_on_net_sales = l.cogs_on_net_sales/rate,
e.marketing_spend = l.marketing_spend/rate,
e.net_sales_paid_channel = l.net_sales_paid_channel/rate,
e.voucher_paid_channel = l.voucher_paid_channel/rate,
e.ShippingFee = l.ShippingFee/rate,
e.ShippingCost = l.ShippingCost/rate,
e.ShippingCostPerOrder = l.ShippingCostPerOrder/rate,
e.PaymentCost = l.PaymentCost/rate,
e.PaymentCostPerOrder = l.PaymentCostPerOrder/rate,
e.WHCostPerOrder = l.WHCostPerOrder/rate,
e.CSCostPerOrder = l.CSCostPerOrder,
e.net_sales_Appliances = l.net_sales_Appliances/rate,
e.costs_Appliances = l.costs_Appliances/rate,
e.inboundCosts_Appliances = l.inboundCosts_Appliances/rate,
e.cogs_Appliances = l.cogs_Appliances/rate,
e.ShippingFee_Appliances = l.ShippingFee_Appliances/rate,
e.ShippingCost_Appliances = l.ShippingCost_Appliances/rate,
e.WH_Cost_Appliances = l.WH_Cost_Appliances/rate,
e.CS_Cost_Appliances = l.CS_Cost_Appliances/rate,
e.Payment_Fees_Appliances = l.Payment_Fees_Appliances/rate,
e.net_sales_Photography = l.net_sales_Photography/rate,
e.costs_Photography = l.costs_Photography/rate,
e.inboundCosts_Photography = l.inboundCosts_Photography/rate,
e.cogs_Photography = l.cogs_Photography/rate,
e.ShippingFee_Photography = l.ShippingFee_Photography/rate,
e.ShippingCost_Photography = l.ShippingCost_Photography/rate,
e.WH_Cost_Photography = l.WH_Cost_Photography/rate,
e.CS_Cost_Photography = l.CS_Cost_Photography/rate,
e.Payment_Fees_Photography = l.Payment_Fees_Photography/rate,
e.net_sales_TV_Audio_Video = l.net_sales_TV_Audio_Video/rate,
e.costs_TV_Audio_Video = l.costs_TV_Audio_Video/rate,
e.inboundCosts_TV_Audio_Video = l.inboundCosts_TV_Audio_Video/rate,
e.cogs_TV_Audio_Video = l.cogs_TV_Audio_Video/rate,
e.ShippingFee_TV_Audio_Video = l.ShippingFee_TV_Audio_Video/rate,
e.ShippingCost_TV_Audio_Video = l.ShippingCost_TV_Audio_Video/rate,
e.WH_Cost_TV_Audio_Video = l.WH_Cost_TV_Audio_Video/rate,
e.CS_Cost_TV_Audio_Video = l.CS_Cost_TV_Audio_Video/rate,
e.Payment_Fees_TV_Audio_Video = l.Payment_Fees_TV_Audio_Video/rate,
e.net_sales_Books = l.net_sales_Books/rate,
e.costs_Books = l.costs_Books/rate,
e.inboundCosts_Books = l.inboundCosts_Books/rate,
e.cogs_Books = l.cogs_Books/rate,
e.ShippingFee_Books = l.ShippingFee_Books/rate,
e.ShippingCost_Books = l.ShippingCost_Books/rate,
e.WH_Cost_Books = l.WH_Cost_Books/rate,
e.CS_Cost_Books = l.CS_Cost_Books/rate,
e.Payment_Fees_Books = l.Payment_Fees_Books/rate,
e.net_sales_Videogames = l.net_sales_Videogames/rate,
e.costs_Videogames = l.costs_Videogames/rate,
e.inboundCosts_Videogames = l.inboundCosts_Videogames/rate,
e.cogs_Videogames = l.cogs_Videogames/rate,
e.ShippingFee_Videogames = l.ShippingFee_Videogames/rate,
e.ShippingCost_Videogames = l.ShippingCost_Videogames/rate,
e.WH_Cost_Videogames = l.WH_Cost_Videogames/rate,
e.CS_Cost_Videogames = l.CS_Cost_Videogames/rate,
e.Payment_Fees_Videogames = l.Payment_Fees_Videogames/rate,
e.net_sales_Fashion = l.net_sales_Fashion/rate,
e.costs_Fashion = l.costs_Fashion/rate,
e.inboundCosts_Fashion = l.inboundCosts_Fashion/rate,
e.cogs_Fashion = l.cogs_Fashion/rate,
e.ShippingFee_Fashion = l.ShippingFee_Fashion/rate,
e.ShippingCost_Fashion = l.ShippingCost_Fashion/rate,
e.WH_Cost_Fashion = l.WH_Cost_Fashion/rate,
e.CS_Cost_Fashion = l.CS_Cost_Fashion/rate,
e.Payment_Fees_Fashion = l.Payment_Fees_Fashion/rate,
e.net_sales_Health_Beauty = l.net_sales_Health_Beauty/rate,
e.costs_Health_Beauty = l.costs_Health_Beauty/rate,
e.inboundCosts_Health_Beauty = l.inboundCosts_Health_Beauty/rate,
e.cogs_Health_Beauty = l.cogs_Health_Beauty/rate,
e.ShippingFee_Health_Beauty = l.ShippingFee_Health_Beauty/rate,
e.ShippingCost_Health_Beauty = l.ShippingCost_Health_Beauty/rate,
e.WH_Cost_Health_Beauty = l.WH_Cost_Health_Beauty/rate,
e.CS_Cost_Health_Beauty = l.CS_Cost_Health_Beauty/rate,
e.Payment_Fees_Health_Beauty = l.Payment_Fees_Health_Beauty/rate,
e.net_sales_Cellphones = l.net_sales_Cellphones/rate,
e.costs_Cellphones = l.costs_Cellphones/rate,
e.inboundCosts_Cellphones = l.inboundCosts_Cellphones/rate,
e.cogs_Cellphones = l.cogs_Cellphones/rate,
e.ShippingFee_Cellphones = l.ShippingFee_Cellphones/rate,
e.ShippingCost_Cellphones = l.ShippingCost_Cellphones/rate,
e.WH_Cost_Cellphones = l.WH_Cost_Cellphones/rate,
e.CS_Cost_Cellphones = l.CS_Cost_Cellphones/rate,
e.Payment_Fees_Cellphones = l.Payment_Fees_Cellphones/rate,
e.net_sales_Home = l.net_sales_Home/rate,
e.costs_Home = l.costs_Home/rate,
e.inboundCosts_Home = l.inboundCosts_Home/rate,
e.cogs_Home = l.cogs_Home/rate,
e.ShippingFee_Home = l.ShippingFee_Home/rate,
e.ShippingCost_Home = l.ShippingCost_Home/rate,
e.WH_Cost_Home = l.WH_Cost_Home/rate,
e.CS_Cost_Home = l.CS_Cost_Home/rate,
e.Payment_Fees_Home = l.Payment_Fees_Home/rate,
e.net_sales_Kids_Babies = l.net_sales_Kids_Babies/rate,
e.costs_Kids_Babies = l.costs_Kids_Babies/rate,
e.inboundCosts_Kids_Babies = l.inboundCosts_Kids_Babies/rate,
e.cogs_Kids_Babies = l.cogs_Kids_Babies/rate,
e.ShippingFee_Kids_Babies = l.ShippingFee_Kids_Babies/rate,
e.ShippingCost_Kids_Babies = l.ShippingCost_Kids_Babies/rate,
e.WH_Cost_Kids_Babies = l.WH_Cost_Kids_Babies/rate,
e.CS_Cost_Kids_Babies = l.CS_Cost_Kids_Babies/rate,
e.Payment_Fees_Kids_Babies = l.Payment_Fees_Kids_Babies/rate,
e.net_sales_Sports = l.net_sales_Sports/rate,
e.costs_Sports = l.costs_Sports/rate,
e.inboundCosts_Sports = l.inboundCosts_Sports/rate,
e.cogs_Sports = l.cogs_Sports/rate,
e.ShippingFee_Sports = l.ShippingFee_Sports/rate,
e.ShippingCost_Sports = l.ShippingCost_Sports/rate,
e.WH_Cost_Sports = l.WH_Cost_Sports/rate,
e.CS_Cost_Sports = l.CS_Cost_Sports/rate,
e.Payment_Fees_Sports = l.Payment_Fees_Sports/rate,
e.GrossRev_Appliances = l.GrossRev_Appliances/rate,
e.GrossRev_Photography = l.GrossRev_Photography/rate,
e.GrossRev_TV_Audio_Video = l.GrossRev_TV_Audio_Video/rate,
e.GrossRev_Books = l.GrossRev_Books/rate,
e.GrossRev_Videogames = l.GrossRev_Videogames/rate,
e.GrossRev_Fashion = l.GrossRev_Fashion/rate,
e.GrossRev_Health_Beauty = l.GrossRev_Health_Beauty/rate,
e.GrossRev_Cellphones = l.GrossRev_Cellphones/rate,
e.GrossRev_Computing = l.GrossRev_Computing/rate,
e.GrossRev_Home = l.GrossRev_Home/rate,
e.GrossRev_Kids_Babies = l.GrossRev_Kids_Babies/rate,
e.GrossRev_Sports = l.GrossRev_Sports/rate;

update tbl_dailyReport_EUR e join 
tbl_dailyReport_local l on e.date = l.date
set e.Visits = l.Visits,
e.UniqueVisits = l.UniqueVisits,
e.MobileVisits = l.MobileVisits,
e.Visits = l.Visits,
e.GrossOrders = l.GrossOrders,
e.CancelOrders = l.CancelOrders,
e.PendingOrders = l.PendingOrders,
e.ReturnedOrders = l.ReturnedOrders,
e.NetOrders = l.NetOrders,
e.PromisedDeliveries = l.PromisedDeliveries,
e.DeliveriesOnTime = l.DeliveriesOnTime,
e.AverageItemsPerBasket = l.AverageItemsPerBasket,
e.WHIdleCapacity = l.WHIdleCapacity,
e.newCustomers = l.newCustomers,
e.itemsInStock = l.itemsInStock,
e.brandsInStock = l.brandsInStock;




END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `daily_report_local`()
BEGIN

truncate tbl_dailyReport_local;
insert into tbl_dailyReport_local (date) select dt from calendar where dt>'2012-07-31' and dt<now();

update tbl_dailyReport_local
set year = year(date) , month = month(date) , yrmonth = concat(year,if(month<10,concat(0,month),month));

#Visits
UPDATE tbl_dailyReport_local t INNER JOIN ga_daily_visits g 
ON t.date = g.date
SET t.Visits = g.visits;

#Visits
UPDATE tbl_dailyReport_local t INNER JOIN ga_daily_visits g 
ON t.date = g.date
SET t.UniqueVisits = g.unique_visits;

#Mobile Visits
UPDATE tbl_dailyReport_local t INNER JOIN ga_daily_visits g 
ON t.date = g.date
SET t.MobileVisits = if(g.mobile_visits is null, 0, g.mobile_visits);

#Gross
update tbl_dailyReport_local join 
(select date,sum(`unit_price`) as grossrev, count(distinct `order_nr`,`OAC`,`RETURNED`,`PENDING`) as grossorders, 
	sum(`unit_price_after_vat`) as GrossSalesPreCancel
            from `tbl_order_detail` where   (`OBC` = '1')
            group by `date`) AS tbl_gross on tbl_gross.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.GrossRev=tbl_gross.grossrev , tbl_dailyReport_local.GrossOrders=tbl_gross.grossorders, 
tbl_dailyReport_local.GrossSalesPreCancel=tbl_gross.GrossSalesPreCancel;

#Cancel
update tbl_dailyReport_local join 
(select date,sum(`unit_price_after_vat`) as cancelrev, count(distinct `order_nr`) as cancelorders
from `tbl_order_detail` 
where   (`CANCEL` = '1')
group by `date`) AS tbl_cancel on tbl_cancel.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.Cancellations=tbl_cancel.cancelrev , tbl_dailyReport_local.CancelOrders=tbl_cancel.cancelorders;


#Pending
update tbl_dailyReport_local join 
(select date,sum(if(`unit_price_after_vat` is null, 0, unit_price_after_vat))  as pendingrev, if( count(distinct `order_nr`) is null, 0,  count(distinct `order_nr`)) as pendingorders
from `tbl_order_detail` 
where   (`PENDING` = '1')
group by `date`) AS tbl_pending on tbl_pending.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.PendingCOP=tbl_pending.pendingrev , tbl_dailyReport_local.PendingOrders=tbl_pending.pendingorders;

#cogs_gross_post_cancel
update tbl_dailyReport_local join 
(select date,(sum(`costo_after_vat`) + sum(`delivery_cost_supplier`)) as cogs_gross_post_cancel
from `tbl_order_detail` 
where   (`OAC` = '1')
group by `date`) AS tbl_cogs_post_cancel on tbl_cogs_post_cancel.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.cogs_gross_post_cancel=tbl_cogs_post_cancel.cogs_gross_post_cancel;

#Returned
update tbl_dailyReport_local join 
(select date,sum(`unit_price_after_vat`) as returnrev, count(distinct `order_nr`) as returnorders
from `tbl_order_detail` 
where   (`RETURNED` = '1')
group by `date`) AS tbl_returned on tbl_returned.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.ReturnedCOP=tbl_returned.returnrev , tbl_dailyReport_local.ReturnedOrders=tbl_returned.returnorders;

#Net
update tbl_dailyReport_local join 
(select date,sum(`unit_price_after_vat`) as NetSalesPreVoucher, SUM(coupon_money_after_vat) as VoucherMktCost,
SUM(`paid_price_after_vat`) as NetSales,count(distinct `order_nr`) as NetOrders,
 (sum(`costo_after_vat`) + sum(`delivery_cost_supplier`))as cogs_on_net_sales,
count(`order_nr`)/count(distinct `order_nr`) as AverageItemsPerBasket,
count(distinct `CustID`) as customers,
SUM(`shipping_fee`) as ShippingFee, 
SUM(`shipping_cost`)as ShippingCost, 
SUM(`payment_cost`) as PaymentCost
from `tbl_order_detail` 
where   (`RETURNED` = '0') AND (`OAC` = '1')
group by `date`) AS tbl_net on tbl_net.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.NetSalesPreVoucher=tbl_net.NetSalesPreVoucher ,
tbl_dailyReport_local.VoucherMktCost=tbl_net.VoucherMktCost,
tbl_dailyReport_local.NetSales=tbl_net.NetSales,
tbl_dailyReport_local.NetOrders=tbl_net.NetOrders,
tbl_dailyReport_local.cogs_on_net_sales=tbl_net.cogs_on_net_sales,
tbl_dailyReport_local.AverageItemsPerBasket=tbl_net.AverageItemsPerBasket,
tbl_dailyReport_local.customers=tbl_net.customers,
tbl_dailyReport_local.ShippingFee=tbl_net.ShippingFee/1.12,
tbl_dailyReport_local.ShippingCost=tbl_net.ShippingCost,
tbl_dailyReport_local.PaymentCost=tbl_net.PaymentCost;

#Marketing spend
UPDATE tbl_dailyReport_local t
INNER JOIN (select date, sum(adCost) adCost from ga_cost_campaign group by date) g 
ON t.date = g.date SET t.marketing_spend = adCost;

# New Customers
update tbl_dailyReport_local join  
(select firstOrder as date,COUNT(*) as newCustomers from `view_cohort` 
group by firstOrder) as tbl_nc on tbl_nc.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.newcustomers=tbl_nc.newCustomers,
tbl_dailyReport_local.ShippingCostPerOrder=tbl_dailyReport_local.ShippingCost/tbl_nc.newCustomers,
tbl_dailyReport_local.PaymentCostPerOrder=tbl_dailyReport_local.PaymentCost/tbl_nc.newCustomers;

#PaidChannels
 update production_ve.tbl_dailyReport_local join 
(select date,sum(paid_price_after_vat) as net_sales_paid_channel,
sum(coupon_money_after_vat) as voucher_paid_channel
from production_ve.tbl_order_detail 
where  (oac = '1') and (returned = '0') 
and   ((source_medium like '%price comparison%')
  or (source_medium like '%socialmediaads%')
  or (source_medium like '%cpc%')
  or (source_medium like '%sociomantic%'))
group by date) as tbl_paid on tbl_paid.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_paid_channel=tbl_paid.net_sales_paid_channel
 , tbl_dailyReport_local.voucher_paid_channel=tbl_paid.voucher_paid_channel;

 
#Categorías
#Electrodomésticos
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` = 'electrodomésticos')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Appliances=tbl_cat.netsales , tbl_dailyReport_local.cogs_Appliances=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Appliances=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Appliances=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Appliances=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Appliances=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Appliances=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Appliances = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Appliances = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Appliances= tbl_cat.grossRev;

#Cámaras y Fotografía
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` = 'cámaras y fotografía')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Photography=tbl_cat.netsales , tbl_dailyReport_local.cogs_Photography=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Photography=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Photography=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Photography=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Photography=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Photography=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Photography = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Photography = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Photography= tbl_cat.grossRev ;


#TV, Video y Audio
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 ='tv, audio y video' )
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_TV_Audio_Video=tbl_cat.netsales , tbl_dailyReport_local.cogs_TV_Audio_Video=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_TV_Audio_Video=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_TV_Audio_Video=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_TV_Audio_Video=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_TV_Audio_Video=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_TV_Audio_Video=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_TV_Audio_Video = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_TV_Audio_Video = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_TV_Audio_Video= tbl_cat.grossRev ;

#Libros
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 = 'libros')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Books=tbl_cat.netsales , tbl_dailyReport_local.cogs_Books=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Books=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Books=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Books=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Books=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Books=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Books = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Books = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Books= tbl_cat.grossRev  ;
#Videojuegos
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` = 'videojuegos')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Videogames=tbl_cat.netsales , tbl_dailyReport_local.cogs_Videogames=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Videogames=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Videogames=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Videogames=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Videogames=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Videogames=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Videogames = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Videogames = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Videogames= tbl_cat.grossRev ;

#Fashion
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 = 'ropa, calzado y accesorios')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Fashion=tbl_cat.netsales , tbl_dailyReport_local.cogs_Fashion=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Fashion=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Fashion=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Fashion=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Fashion=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Fashion=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Fashion = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Fashion = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Fashion= tbl_cat.grossRev  ;


#Cuidado Personal
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 like '%cuidado personal%' or n1 = 'rizadoras' or n1 = 'cabello' or n1 = 'secadores')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Health_Beauty=tbl_cat.netsales , tbl_dailyReport_local.cogs_Health_Beauty=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Health_Beauty=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Health_Beauty=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Health_Beauty=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Health_Beauty=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Health_Beauty=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Health_Beauty = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Health_Beauty = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Health_Beauty= tbl_cat.grossRev  ;


#Teléfonos y GPS
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND( n1 = 'celulares, teléfonos y gps' or n1 = 'ipad 100')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Cellphones=tbl_cat.netsales , tbl_dailyReport_local.cogs_Cellphones=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Cellphones=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Cellphones=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Cellphones=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Cellphones=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Cellphones=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Cellphones = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Cellphones = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Cellphones= tbl_cat.grossRev   ;


 
#Computadores y Tablets    
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 = 'computadoras y tabletas')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Computing=tbl_cat.netsales , tbl_dailyReport_local.cogs_Computing=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Computing=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Computing=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Computing=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Computing=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Computing=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Computing = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Computing = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Computing = tbl_cat.grossRev  ;


#Home y Muebles   
update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 = 'línea blanca' or n1 = 'comerdor y cocina' or n1 = 'hogar')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Home=tbl_cat.netsales , 
tbl_dailyReport_local.cogs_Home=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Home=tbl_cat.shipping_fee,
 tbl_dailyReport_local.ShippingCost_Home=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Home=tbl_cat.WH_cost, 
tbl_dailyReport_local.CS_Cost_Home=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Home=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Home = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Home = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Home = tbl_cat.grossRev ;


#Juguetes, Niños y Bebés 
 update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 like 'juguetes%')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Kids_Babies=tbl_cat.netsales , tbl_dailyReport_local.cogs_Kids_Babies=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Kids_Babies=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Kids_Babies=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Kids_Babies=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Kids_Babies=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Kids_Babies=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Kids_Babies = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Kids_Babies = tbl_cat.inboundCosts , 
tbl_dailyReport_local.GrossRev_Kids_Babies = tbl_cat.grossRev ;

#Deportes
 update tbl_dailyReport_local join 
(select date,sum(`paid_price_after_vat`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.12) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` ='Deportes')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Sports=tbl_cat.netsales , 
tbl_dailyReport_local.cogs_Sports=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Sports=tbl_cat.shipping_fee, 
tbl_dailyReport_local.ShippingCost_Sports=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Sports=tbl_cat.WH_cost, 
tbl_dailyReport_local.CS_Cost_Sports=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Sports=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Sports = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Sports = tbl_cat.inboundCosts , 
tbl_dailyReport_local.GrossRev_Sports = tbl_cat.grossRev ;



END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `daily_routine`()
begin
select  'daily routine: start',now();

select  'daily routine: tbl_catalog_product start',now();
#table catalog_product_v2
drop table if exists production_ve.simple_variation;
create table production_ve.simple_variation as (select * from production_ve.simple_variation_view);
alter table production_ve.simple_variation add primary key (fk_catalog_simple) ;

#tbl_catalog_stock
truncate production_ve.tbl_catalog_product_stock;
insert into production_ve.tbl_catalog_product_stock(fk_catalog_simple,sku,reservedbob,stockbob,availablebob)
select catalog_simple.id_catalog_simple,catalog_simple.sku,
if(sum(is_reserved) is null, 0, sum(is_reserved))  as reservedbob ,catalog_stock.quantity as availablebob,
catalog_stock.quantity-if(sum(is_reserved) is null, 0, sum(is_reserved)) as disponible
from 
(bob_live_ve.catalog_simple join bob_live_ve.catalog_stock on catalog_simple.id_catalog_simple=catalog_stock.fk_catalog_simple) left join bob_live_ve.sales_order_item
 on sales_order_item.sku=catalog_simple.sku
group by catalog_simple.sku
order by catalog_stock.quantity desc;
drop table if exists tbl_catalog_product_v3;
create table production_ve.tbl_catalog_product_v3 as (select * from production_ve.catalog_product);
update production_ve.tbl_catalog_product_v3  set visible='no' where cat1 is null;

#actualiza lo recientamente reservado, desde el 01 de enero
update production_ve.tbl_catalog_product_stock inner join (select tbl_catalog_product_stock.fk_catalog_simple,
if(sum(is_reserved) is null, 0, sum(is_reserved))  as recently_reserved
from 
production_ve.tbl_catalog_product_stock inner join bob_live_ve.sales_order_item on tbl_catalog_product_stock.sku=sales_order_item.sku
where date(sales_order_item.created_at)>'2013-01-01' group by fk_catalog_simple) as a 
on  tbl_catalog_product_stock.fk_catalog_simple = a.fk_catalog_simple
set tbl_catalog_product_stock.recently_reservedbob = a.recently_reserved;


alter table production_ve.tbl_catalog_product_v3 add primary key (sku) ;

drop table if exists tbl_catalog_product_v2;
alter table production_ve.tbl_catalog_product_v3 rename to  production_ve.tbl_catalog_product_v2 ;

alter table production_ve.tbl_catalog_product_v2 
add index (sku_config ) ;

select  'daily routine: tbl_catalog_product ok',now();

#special cost
#call special_cost();
select  'daily routine: tbl_order_detail start',now();

call production_ve.update_tbl_order_detail();

select  'daily routine: tbl_order_detail end',now();

call production_ve.daily_report_local();

#ventas netas mes corrido
/*insert into production_ve.tbl_sales_growth_mom_cat
select date(now() - interval 1 day) date, tab.n1, ventas_netas_t, ventas_netas_t_1, 
cast(((ventas_netas_t/ventas_netas_t_1)-1) as decimal(30,10)) mom from (
select n1,sum(paid_price_after_vat) + sum(if(shipping_fee_after_vat is null, shipping_fee_after_vat,0)) ventas_netas_t
 from production_ve.tbl_order_detail where date>= date(now() - interval day(now()) day) and date <= now() 
 and month(date) = month(now() - interval 1 day) and oac = 1 and returned = 0 group by n1) tab inner join 
(select n1,sum(paid_price_after_vat) + sum(if(shipping_fee_after_vat is null, shipping_fee_after_vat,0)) ventas_netas_t_1
 from production_ve.tbl_order_detail where date<= date(now() - interval 1 month)
 and date >= date(now() - interval 1 month - interval day(now()) day ) 
and month(date) = month(now() - interval 1 day) - 1
 and oac = 1 and returned = 0 group by n1) tab2
on tab.n1 = tab2.n1;

insert into production_ve.tbl_sales_growth_mom_buyer
select date(now() - interval 1 day) date, tab.buyer, ventas_netas_t, ventas_netas_t_1, 
cast(((ventas_netas_t/ventas_netas_t_1)-1) as decimal(30,10)) mom from (
select buyer,sum(paid_price_after_vat) + sum(if(shipping_fee_after_vat is null, shipping_fee_after_vat,0)) ventas_netas_t
 from production_ve.tbl_order_detail where date>= date(now() - interval day(now()) day) and date <= now() 
 and month(date) = month(now() - interval 1 day) and oac = 1 and returned = 0 group by buyer) tab inner join 
(select buyer,sum(paid_price_after_vat) + sum(if(shipping_fee_after_vat is null, shipping_fee_after_vat,0)) ventas_netas_t_1
 from production_ve.tbl_order_detail where date<= date(now() - interval 1 month)
 and date >= date(now() - interval 1 month - interval day(now()) day ) 
and month(date) =  month(now() - interval 1 day) - 1
 and oac = 1 and returned = 0 group by buyer) tab2
on tab.buyer = tab2.buyer;*/

select  'bireporting: start',now();

#sales cube
-- call bireporting.etl_fact_sales_comercial();

select  'bireporting: ok',now();


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `extra_queries`()
BEGIN

#status
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200022566' AND c.sku = 'SA717EL02GMBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200022566' AND c.sku = 'SA717EL29ANELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200022566' AND c.sku = 'SA717EL28KDFLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200022566' AND c.sku = 'SA717EL62ATPLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200091766' AND c.sku = 'CO748HL71PCILAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044882' AND c.sku = 'SA717EL68BEXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044882' AND c.sku = 'SA717EL68BEXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044882' AND c.sku = 'SA717EL68BEXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044882' AND c.sku = 'SA717EL68BEXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044882' AND c.sku = 'SA717EL68BEXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044882' AND c.sku = 'SA717EL68BEXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044882' AND c.sku = 'SA717EL68BEXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044882' AND c.sku = 'SA717EL68BEXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044882' AND c.sku = 'SA717EL68BEXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044882' AND c.sku = 'SA717EL68BEXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044882' AND c.sku = 'SO722EL80BXRLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044882' AND c.sku = 'SA717EL68BEXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044882' AND c.sku = 'SO722EL80BXRLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044882' AND c.sku = 'SO722EL80BXRLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044882' AND c.sku = 'SO722EL80BXRLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044882' AND c.sku = 'SO722EL80BXRLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044882' AND c.sku = 'SO722EL80BXRLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044882' AND c.sku = 'SO722EL34AQVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044882' AND c.sku = 'SO722EL80BXRLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044882' AND c.sku = 'SO722EL80BXRLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044882' AND c.sku = 'SO722EL80BXRLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200024882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200024882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200024882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200024882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200024882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200024882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200024882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200024882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200024882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200024882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200024882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200024882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200024882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200024882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200024882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094882' AND c.sku = 'SO722EL34AQVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094882' AND c.sku = 'SO722EL34AQVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094882' AND c.sku = 'SO722EL34AQVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094882' AND c.sku = 'SO722EL34AQVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094882' AND c.sku = 'SO722EL34AQVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094882' AND c.sku = 'SO722EL34AQVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094882' AND c.sku = 'SO722EL34AQVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094882' AND c.sku = 'SO722EL34AQVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094882' AND c.sku = 'SO722EL34AQVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094882' AND c.sku = 'SO722EL34AQVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094882' AND c.sku = 'SO722EL34AQVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094882' AND c.sku = 'SO722EL34AQVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094882' AND c.sku = 'SO722EL34AQVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094882' AND c.sku = 'SO722EL34AQVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094882' AND c.sku = 'SO722EL34AQVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094882' AND c.sku = 'SO722EL34AQVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094882' AND c.sku = 'SO722EL34AQVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094882' AND c.sku = 'SO722EL34AQVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200084882' AND c.sku = 'SO722EL35BZKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200084882' AND c.sku = 'SO722EL35BZKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200084882' AND c.sku = 'SO722EL35BZKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200084882' AND c.sku = 'SO722EL35BZKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200084882' AND c.sku = 'SO722EL35BZKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200084882' AND c.sku = 'SO722EL35BZKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200084882' AND c.sku = 'SO722EL35BZKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200084882' AND c.sku = 'SO722EL35BZKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200084882' AND c.sku = 'SO722EL35BZKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200084882' AND c.sku = 'SO722EL35BZKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200084882' AND c.sku = 'SO722EL35BZKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200084882' AND c.sku = 'SO722EL35BZKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200084882' AND c.sku = 'SO722EL35BZKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200084882' AND c.sku = 'SO722EL35BZKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200084882' AND c.sku = 'SO722EL35BZKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200084882' AND c.sku = 'SO722EL35BZKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200084882' AND c.sku = 'SO722EL35BZKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200084882' AND c.sku = 'SO722EL35BZKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200084882' AND c.sku = 'SO722EL35BZKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200074882' AND c.sku = 'SO722EL43AQMLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200074882' AND c.sku = 'NO706EL20BCXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200074882' AND c.sku = 'NI704EL03CQALAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200074882' AND c.sku = 'SO722EL12ARRLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200074882' AND c.sku = 'EP671EL89BTMLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200074882' AND c.sku = 'CA660EL79APCLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200074882' AND c.sku = 'SO722EL79BIILAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200074882' AND c.sku = 'SO722EL67APOLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200074882' AND c.sku = 'SO722EL67BYELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200074882' AND c.sku = 'SA717EL68BEXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200074882' AND c.sku = 'AP650EL65CFYLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200074882' AND c.sku = 'DE668EL97BTELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054882' AND c.sku = 'CA660EL12ANVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054882' AND c.sku = 'SO722EL94BXDLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200056372' AND c.sku = 'GO675EL13HEWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200039382' AND c.sku = 'KI747HL84DRRLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200098472' AND c.sku = 'SO722EL27BSALAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200025472' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200081272' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200042672' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200065672' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200041672' AND c.sku = 'NO758HL64EHVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200021672' AND c.sku = 'CA780HL21GDQLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200065972' AND c.sku = 'SA717EL93EOKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200046872' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200079872' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200019872' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200077872' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200057872' AND c.sku = 'HP677EL02ECNLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054572' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200014572' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200014572' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200016572' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200071572' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200089172' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200089172' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200079172' AND c.sku = 'MA746HL15DQMLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200039172' AND c.sku = 'GO675EL13HEWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200058172' AND c.sku = 'SA718EL99GIILAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200091172' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200081172' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200071172' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200051172' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200056372' AND c.sku = 'GO675EL13HEWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200056572' AND c.sku = 'WE744HL55DHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200056572' AND c.sku = 'WE744HL55DHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200056572' AND c.sku = 'WE744HL55DHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200056572' AND c.sku = 'WE744HL55DHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200056572' AND c.sku = 'WE744HL55DHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200056572' AND c.sku = 'WE744HL55DHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200059172' AND c.sku = 'NO706EL56GCHLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200018172' AND c.sku = 'KO763EL34GSNLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200067172' AND c.sku = 'MO700EL55GCILAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200092372' AND c.sku = 'BL657EL55BBOLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200092372' AND c.sku = 'BL657EL55BBOLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200059972' AND c.sku = 'GA766EL71FEQLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200059972' AND c.sku = 'GA766EL77FEKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200028972' AND c.sku = 'NO706EL68DGTLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200068972' AND c.sku = 'SA717EL65BFALAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200098972' AND c.sku = 'SO722EL08CTRLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200078972' AND c.sku = 'SA717EL29ANELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200058972' AND c.sku = 'SA717EL29ANELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200018972' AND c.sku = 'SA717EL29ANELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200038972' AND c.sku = 'SA717EL29ANELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200047972' AND c.sku = 'GA766EL71FEQLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200027972' AND c.sku = 'GA766EL71FEQLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200067972' AND c.sku = 'GA766EL71FEQLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200097972' AND c.sku = 'GA766EL71FEQLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200087972' AND c.sku = 'GA766EL71FEQLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200087972' AND c.sku = 'PA708EL42BRLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200087972' AND c.sku = 'HP677EL74GFLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200087972' AND c.sku = 'BE656EL05FHELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200077972' AND c.sku = 'BE656EL05FHELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200077972' AND c.sku = 'GA766EL71FEQLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200077972' AND c.sku = 'PA708EL42BRLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200077972' AND c.sku = 'HP677EL74GFLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200057972' AND c.sku = 'BE656EL05FHELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200057972' AND c.sku = 'HP677EL74GFLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200057972' AND c.sku = 'PA708EL42BRLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200017972' AND c.sku = 'PA708EL42BRLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200017972' AND c.sku = 'HP677EL74GFLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200017972' AND c.sku = 'BE656EL05FHELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200037972' AND c.sku = 'PA708EL42BRLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200037972' AND c.sku = 'HP677EL74GFLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200037972' AND c.sku = 'BE656EL05FHELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200045972' AND c.sku = 'PA708EL42BRLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200045972' AND c.sku = 'HP677EL74GFLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200045972' AND c.sku = 'BE656EL05FHELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200025972' AND c.sku = 'PA708EL42BRLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200025972' AND c.sku = 'HP677EL74GFLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200025972' AND c.sku = 'BE656EL05FHELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200065972' AND c.sku = 'SA717EL93EOKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200095972' AND c.sku = 'PA708EL42BRLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200095972' AND c.sku = 'HP677EL74GFLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200095972' AND c.sku = 'BE656EL05FHELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200085972' AND c.sku = 'SA717EL29ANELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200035972' AND c.sku = 'NY707EL03GMALAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200041972' AND c.sku = 'NY707EL03GMALAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200021972' AND c.sku = 'NY707EL03GMALAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200061972' AND c.sku = 'NY707EL03GMALAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200091972' AND c.sku = 'PA708EL42BRLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200081972' AND c.sku = 'PA708EL42BRLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200071972' AND c.sku = 'PA708EL42BRLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200011972' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200043972' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200023972' AND c.sku = 'WE744HL97DJMLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200023972' AND c.sku = 'WE744HL31DIELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200063972' AND c.sku = 'NY707EL03GMALAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200083972' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044872' AND c.sku = 'WE744HL31DIELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044872' AND c.sku = 'WE744HL97DJMLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200084872' AND c.sku = 'GE674EL23EJKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200074872' AND c.sku = 'SO722EL57EICLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054872' AND c.sku = 'WE744HL06DJDLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200014872' AND c.sku = 'WE744HL06DJDLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200034872' AND c.sku = 'TA724EL48GGLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200042872' AND c.sku = 'LI690EL50BBTLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200022872' AND c.sku = 'LI690EL50BBTLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200072872' AND c.sku = 'GE674EL15CEALAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200052872' AND c.sku = 'GE674EL15CEALAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200046872' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200096872' AND c.sku = 'SY754EL27EQYLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200076872' AND c.sku = 'BL657EL55BBOLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200016872' AND c.sku = 'MO700EL44BBZLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200036872' AND c.sku = 'SA717EL07BLCLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200029872' AND c.sku = 'BL657EL55BBOLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200069872' AND c.sku = 'SA717EL07BLCLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200079872' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200019872' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200028872' AND c.sku = 'SA717EL07BLCLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200068872' AND c.sku = 'SA717EL07BLCLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200098872' AND c.sku = 'SA717EL07BLCLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200078872' AND c.sku = 'SO722EL83CYMLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200078872' AND c.sku = 'SO722EL08CTRLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200038872' AND c.sku = 'SA717EL29ANELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200047872' AND c.sku = 'SI732EL83GBGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200097872' AND c.sku = 'NO706EL23BCULAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200077872' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200057872' AND c.sku = 'HP677EL02ECNLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200017872' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200045872' AND c.sku = 'CA740HL65DDALAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200025872' AND c.sku = 'CA740HL65DDALAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200065872' AND c.sku = 'SO722EL57EICLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200075872' AND c.sku = 'EP671EL07FDGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200055872' AND c.sku = 'EP671EL07FDGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200061872' AND c.sku = 'PA708EL29DAOLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200051872' AND c.sku = 'LG689EL12FSLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200031872' AND c.sku = 'SO722EL53DWSLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200023872' AND c.sku = 'SO722EL57EICLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200093872' AND c.sku = 'SO722EL57EICLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200083872' AND c.sku = 'SO722EL57EICLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200053872' AND c.sku = 'SI732EL23BSELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054772' AND c.sku = 'NO706EL68DGTLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054772' AND c.sku = 'SA717EL68BEXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200034772' AND c.sku = 'NO706EL68DGTLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200034772' AND c.sku = 'SA717EL68BEXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200042772' AND c.sku = 'TR768HL85FPQLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200042772' AND c.sku = 'BE655EL24AVBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200052772' AND c.sku = 'AL646EL39GGULAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200052772' AND c.sku = 'AL646EL39GGULAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200052772' AND c.sku = 'AL646EL39GGULAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200052772' AND c.sku = 'AL646EL39GGULAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200052772' AND c.sku = 'AL646EL39GGULAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200052772' AND c.sku = 'AL646EL39GGULAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200066772' AND c.sku = 'BL657EL36AMXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200076772' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200056772' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200016772' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200036772' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200029772' AND c.sku = 'AP650EL21ANMLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200099772' AND c.sku = 'SA717EL68BEXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200099772' AND c.sku = 'SA717EL68BEXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200089772' AND c.sku = 'SA717EL68BEXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200089772' AND c.sku = 'AI787EL21HASLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200059772' AND c.sku = 'SO722EL57EICLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200039772' AND c.sku = 'SO722EL57EICLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200048772' AND c.sku = 'SO722EL57EICLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200028772' AND c.sku = 'SO722EL57EICLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200068772' AND c.sku = 'SO722EL57EICLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200098772' AND c.sku = 'SO722EL57EICLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200088772' AND c.sku = 'SO722EL57EICLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200078772' AND c.sku = 'SO722EL57EICLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200058772' AND c.sku = 'SO722EL57EICLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200047772' AND c.sku = 'SA717EL46FYVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200017772' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200045772' AND c.sku = 'KI747HL84DRRLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200045772' AND c.sku = 'KI747HL83DRSLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200045772' AND c.sku = 'KI747HL82DRTLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200045772' AND c.sku = 'KI747HL81DRULAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200025772' AND c.sku = 'SA717EL62ATPLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200065772' AND c.sku = 'KI747HL83DRSLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200065772' AND c.sku = 'KI747HL84DRRLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200065772' AND c.sku = 'KI747HL82DRTLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200065772' AND c.sku = 'KI747HL81DRULAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200095772' AND c.sku = 'KI747HL82DRTLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200095772' AND c.sku = 'KI747HL83DRSLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200095772' AND c.sku = 'KI747HL84DRRLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200095772' AND c.sku = 'KI747HL81DRULAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200085772' AND c.sku = 'SA717EL62ATPLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200075772' AND c.sku = 'SA717EL62ATPLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200055772' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200041772' AND c.sku = 'SA717EL29ANELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200041772' AND c.sku = 'SA717EL29ANELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200021772' AND c.sku = 'SE745HL00DFNLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200021772' AND c.sku = 'WE744HL84DGDLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200061772' AND c.sku = 'SA717EL29ANELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200091772' AND c.sku = 'ES767HL12FOPLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200091772' AND c.sku = 'PE771HL35FROLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200091772' AND c.sku = 'DE788EL02HBLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200011772' AND c.sku = 'NO706EL23BCULAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200043772' AND c.sku = 'MO700EL67DGULAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200023772' AND c.sku = 'SA717EL85CUOLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200063772' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200093772' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200083772' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094572' AND c.sku = 'ES767HL17FOKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094572' AND c.sku = 'DE788EL05HBILAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094572' AND c.sku = 'DE788EL05HBILAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054572' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200014572' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200014572' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200034572' AND c.sku = 'BE656EL69FXYLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200042572' AND c.sku = 'BE656EL69FXYLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200022572' AND c.sku = 'BE656EL69FXYLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200062572' AND c.sku = 'SA717EL02GMBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200092572' AND c.sku = 'BE656EL69FXYLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200082572' AND c.sku = 'BE656EL69FXYLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200072572' AND c.sku = 'SA717EL02GMBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200052572' AND c.sku = 'BE656EL69FXYLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200012572' AND c.sku = 'SA717EL02GMBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200032572' AND c.sku = 'BE656EL69FXYLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200046572' AND c.sku = 'SA717EL02GMBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200076572' AND c.sku = 'SA717EL02GMBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200056572' AND c.sku = 'WE744HL55DHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200056572' AND c.sku = 'WE744HL55DHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200056572' AND c.sku = 'WE744HL55DHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200056572' AND c.sku = 'WE744HL55DHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200056572' AND c.sku = 'WE744HL55DHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200056572' AND c.sku = 'WE744HL55DHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200016572' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200069572' AND c.sku = 'MO700EL46BBXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200099572' AND c.sku = 'GE674EL22EJLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200059572' AND c.sku = 'NO706EL35BCILAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200019572' AND c.sku = 'NO706EL35BCILAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200039572' AND c.sku = 'SI732EL83GBGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200048572' AND c.sku = 'SI732EL83GBGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200028572' AND c.sku = 'SI732EL83GBGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200068572' AND c.sku = 'SI732EL83GBGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200078572' AND c.sku = 'HP677EL76BTZLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200058572' AND c.sku = 'DE788EL95HBSLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200018572' AND c.sku = 'KI747HL81DRULAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200018572' AND c.sku = 'KI747HL84DRRLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200018572' AND c.sku = 'VA813HL66GYZLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200038572' AND c.sku = 'BE656EL69FXYLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200067572' AND c.sku = 'BE656EL69FXYLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200097572' AND c.sku = 'BE656EL69FXYLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200017572' AND c.sku = 'SE745HL00DFNLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200045572' AND c.sku = 'CA740HL57DDILAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200025572' AND c.sku = 'SA717EL29ANELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200095572' AND c.sku = 'SA717EL47AUELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200015572' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200035572' AND c.sku = 'SA717EL29ANELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200041572' AND c.sku = 'SA717EL62ATPLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200041572' AND c.sku = 'SA717EL62ATPLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200061572' AND c.sku = 'SA717EL62ATPLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200061572' AND c.sku = 'SA717EL62ATPLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200071572' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200031572' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200031572' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200063572' AND c.sku = 'ME697EL61HDALAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200063572' AND c.sku = 'ME696EL59BBKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200093572' AND c.sku = 'SA717EL29ANELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200033572' AND c.sku = 'AC644EL48DWXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044172' AND c.sku = 'SA717EL29ANELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200024172' AND c.sku = 'SA717EL29ANELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200064172' AND c.sku = 'SA717EL29ANELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200074172' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200074172' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054172' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054172' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200014172' AND c.sku = 'SA717EL62ATPLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200034172' AND c.sku = 'SA717EL62ATPLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200042172' AND c.sku = 'SA717EL62ATPLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200022172' AND c.sku = 'SA717EL62ATPLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200072172' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200052172' AND c.sku = 'SA717EL29ANELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200032172' AND c.sku = 'DE788EL04HBJLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200046172' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200026172' AND c.sku = 'DE788EL04HBJLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200066172' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200096172' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200086172' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200076172' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200056172' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200016172' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200049172' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200029172' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200089172' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200089172' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200079172' AND c.sku = 'MA746HL15DQMLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200059172' AND c.sku = 'NO706EL56GCHLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200039172' AND c.sku = 'GO675EL13HEWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200058172' AND c.sku = 'SA718EL99GIILAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200018172' AND c.sku = 'KO763EL34GSNLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200067172' AND c.sku = 'MO700EL55GCILAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200077172' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200057172' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200017172' AND c.sku = 'CA740HL71DCULAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200037172' AND c.sku = 'DE788EL87HCALAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200045172' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200065172' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200095172' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200055172' AND c.sku = 'TI725EL63GJSLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200015172' AND c.sku = 'CA661EL49BNILAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200015172' AND c.sku = 'TO802EL44GSDLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200035172' AND c.sku = 'TO802EL44GSDLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200035172' AND c.sku = 'CA661EL49BNILAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200091172' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200081172' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200071172' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200051172' AND c.sku = 'SO722EL26BSBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200011172' AND c.sku = 'OL750EL84DVNLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200031172' AND c.sku = 'OL750EL84DVNLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200023172' AND c.sku = 'OL750EL84DVNLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200083172' AND c.sku = 'CA740HL61DDELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200073172' AND c.sku = 'CA740HL61DDELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200053172' AND c.sku = 'SO722EL53DWSLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200013172' AND c.sku = 'CA740HL61DDELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200033172' AND c.sku = 'CA740HL61DDELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094372' AND c.sku = 'CA740HL61DDELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200074372' AND c.sku = 'CA740HL61DDELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054372' AND c.sku = 'CA740HL61DDELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200062372' AND c.sku = 'BL657EL55BBOLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200062372' AND c.sku = 'BL657EL55BBOLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200092372' AND c.sku = 'BL657EL55BBOLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200092372' AND c.sku = 'BL657EL55BBOLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200056372' AND c.sku = 'GO675EL13HEWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200016372' AND c.sku = 'TI725EL92BLRLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200099372' AND c.sku = 'TI725EL92BLRLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200059372' AND c.sku = 'TI725EL92BLRLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200019372' AND c.sku = 'SA717EL29ANELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200039372' AND c.sku = 'SA717EL29ANELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200039372' AND c.sku = 'SA717EL29ANELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200028372' AND c.sku = 'SA717EL02GMBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200064772' AND c.sku = 'PI711EL26GDLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200097372' AND c.sku = 'GO675EL13HEWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200087372' AND c.sku = 'GO675EL13HEWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200077372' AND c.sku = 'GO675EL13HEWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200088972' AND c.sku = 'SA717EL28ANFLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200069772' AND c.sku = 'AP650EL21ANMLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200087772' AND c.sku = 'SA717EL62ATPLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200087772' AND c.sku = 'SA717EL62ATPLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200086572' AND c.sku = 'TO726EL05FSSLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200086572' AND c.sku = 'TO726EL05FSSLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200071882' AND c.sku = 'SO722EL81BXQLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200024782' AND c.sku = 'PN712EL20BKPLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200084782' AND c.sku = 'HP677EL05CIGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200034782' AND c.sku = 'IM681EL82CJDLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200076782' AND c.sku = 'CA661EL17BOOLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200016782' AND c.sku = 'IM681EL86CIZLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200036782' AND c.sku = 'IM681EL86CIZLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200049782' AND c.sku = 'IM681EL86CIZLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200029782' AND c.sku = 'IM681EL86CIZLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200069782' AND c.sku = 'SA717EL42BCBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200098782' AND c.sku = 'GO675EL74BAVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200078782' AND c.sku = 'CA661EL37BNULAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200038782' AND c.sku = 'SA718EL84ASTLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200097782' AND c.sku = 'SI732EL20BSHLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200045782' AND c.sku = 'HP677EL71BUELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200045782' AND c.sku = 'HP677EL58BURLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200045782' AND c.sku = 'HP677EL55BUULAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200045782' AND c.sku = 'HP677EL72BUDLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200045782' AND c.sku = 'TA724EL19AZCLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200045782' AND c.sku = 'HP677EL72BUDLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200045782' AND c.sku = 'HP677EL71BUELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200065782' AND c.sku = 'SA717EL69BEWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200055782' AND c.sku = 'SA717EL69BEWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200015782' AND c.sku = 'LI690EL50BBTLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200061782' AND c.sku = 'GE674EL03BPCLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200063782' AND c.sku = 'CA661EL22BOJLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200063782' AND c.sku = 'GE674EL89CFALAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200063782' AND c.sku = 'BL657EL77CNELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200093782' AND c.sku = 'SO722EL03CTWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200073782' AND c.sku = 'SA717EL27AUYLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044582' AND c.sku = 'SO722EL38BZHLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200024582' AND c.sku = 'SO722EL38BZHLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094582' AND c.sku = 'SA717EL42BCBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200062582' AND c.sku = 'MO700EL23CPGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200046582' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200026582' AND c.sku = 'SA717EL18DAZLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200066582' AND c.sku = 'HP677EL14CHXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200096582' AND c.sku = 'CA661EL44BNNLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200089582' AND c.sku = 'PA708EL25DASLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200019582' AND c.sku = 'DE668EL15AVKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200039582' AND c.sku = 'TP727EL81BMCLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200039582' AND c.sku = 'KI685EL77BEOLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200048582' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200028582' AND c.sku = 'NO706EL36DAHLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200098582' AND c.sku = 'MI698EL19BGULAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200077882' AND c.sku = 'CA660EL19ANOLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200077882' AND c.sku = 'CA660EL19ANOLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200077882' AND c.sku = 'CA660EL19ANOLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200057882' AND c.sku = 'HP677EL99CIMLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200017882' AND c.sku = 'NI704EL03CQALAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200037882' AND c.sku = 'SI732EL17BSKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200045882' AND c.sku = 'SA717EL08AVRLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200025882' AND c.sku = 'ZT736EL68CVFLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200065882' AND c.sku = 'HP677EL99CIMLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200095882' AND c.sku = 'AP650EL21ANMLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200085882' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200075882' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200055882' AND c.sku = 'AP650EL21ANMLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200015882' AND c.sku = 'HP677EL43BVGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200035882' AND c.sku = 'HP677EL23CHOLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200041882' AND c.sku = 'MY703EL29BKGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200021882' AND c.sku = 'BL657EL41AMSLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200061882' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200061882' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200091882' AND c.sku = 'ZT736EL68CVFLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200051882' AND c.sku = 'ZT736EL67CVGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200011882' AND c.sku = 'SI732EL14BSNLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200031882' AND c.sku = 'AP650EL62CGBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200043882' AND c.sku = 'SO722EL79BIILAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200023882' AND c.sku = 'SA717EL68BEXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200063882' AND c.sku = 'SA717EL29ANELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200093882' AND c.sku = 'SO722EL79BIILAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200083882' AND c.sku = 'DE668EL97BTELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200073882' AND c.sku = 'SA717EL29ANELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200053882' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200013882' AND c.sku = 'ZT736EL67CVGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200033882' AND c.sku = 'SA717EL30ANDLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200044782' AND c.sku = 'SO722EL52BYTLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200064782' AND c.sku = 'MY703EL29BKGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094782' AND c.sku = 'SO722EL83BIELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200094782' AND c.sku = 'AP650EL65CFYLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200074782' AND c.sku = 'SA717EL08AVRLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054782' AND c.sku = 'PN712EL20BKPLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200014782' AND c.sku = 'SA717EL68BEXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200042782' AND c.sku = 'SA718EL71ATGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200022782' AND c.sku = 'SO722EL71BYALAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200062782' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200092782' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200092782' AND c.sku = 'SO722EL52BYTLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200082782' AND c.sku = 'SA717EL30ANDLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200072782' AND c.sku = 'SA717EL30ANDLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200052782' AND c.sku = 'AP650EL21ANMLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200012782' AND c.sku = 'GE674EL03BPCLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200032782' AND c.sku = 'AP650EL21ANMLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200046782' AND c.sku = 'AP650EL21ANMLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200026782' AND c.sku = 'AP650EL21ANMLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200066782' AND c.sku = 'AP650EL21ANMLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200096782' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200086782' AND c.sku = 'CA661EL18BONLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200056782' AND c.sku = 'SA717EL42BCBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200099782' AND c.sku = 'AP650EL72BETLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200089782' AND c.sku = 'AP650EL72BETLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200079782' AND c.sku = 'GO675EL74BAVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200059782' AND c.sku = 'GO675EL74BAVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200019782' AND c.sku = 'AP650EL72BETLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200039782' AND c.sku = 'AP650EL72BETLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200048782' AND c.sku = 'AP650EL72BETLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200048782' AND c.sku = 'AP650EL72BETLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200028782' AND c.sku = 'SA717EL69BEWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200068782' AND c.sku = 'AP650EL66CFXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200088782' AND c.sku = 'CA661EL37BNULAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200058782' AND c.sku = 'SO722EL59BYMLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200018782' AND c.sku = 'NI704EL03CQALAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200047782' AND c.sku = 'SO722EL51BJKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200027782' AND c.sku = 'SO722EL51BJKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200067782' AND c.sku = 'SO722EL51BJKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200087782' AND c.sku = 'AP650EL72BETLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200077782' AND c.sku = 'IM681EL86CIZLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200057782' AND c.sku = 'AP650EL72BETLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200017782' AND c.sku = 'AP650EL72BETLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200017782' AND c.sku = 'AP650EL72BETLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200037782' AND c.sku = 'AP650EL72BETLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200037782' AND c.sku = 'AP650EL72BETLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200037782' AND c.sku = 'AP650EL72BETLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200025782' AND c.sku = 'SO722EL27ARCLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200095782' AND c.sku = 'SA717EL13BHALAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200085782' AND c.sku = 'TA724EL78BQBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200075782' AND c.sku = 'AP650EL72BETLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200035782' AND c.sku = 'BL657EL41AMSLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200041782' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200021782' AND c.sku = 'AP650EL72BETLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200091782' AND c.sku = 'CA660EL87AOULAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200081782' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200071782' AND c.sku = 'BL657EL41AMSLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200051782' AND c.sku = 'SO722EL52BYTLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200011782' AND c.sku = 'PA708EL28DAPLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200031782' AND c.sku = 'SO722EL52BYTLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200043782' AND c.sku = 'SO722EL52BYTLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200023782' AND c.sku = 'GE674EL89CFALAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200023782' AND c.sku = 'BL657EL77CNELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200023782' AND c.sku = 'CA661EL22BOJLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200083782' AND c.sku = 'SO722EL52BYTLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200053782' AND c.sku = 'HP677EL26CHLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200013782' AND c.sku = 'SO722EL55BYQLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200033782' AND c.sku = 'SI732EL14BSNLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200064582' AND c.sku = 'SA717EL42BCBLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200084582' AND c.sku = 'AP650EL22ANLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200074582' AND c.sku = 'AP650EL22ANLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054582' AND c.sku = 'CA661EL17BOOLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200014582' AND c.sku = 'SA717EL29ANELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200034582' AND c.sku = 'SA717EL62ATPLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200042582' AND c.sku = 'SO722EL15AROLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200022582' AND c.sku = 'SO722EL15AROLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200092582' AND c.sku = 'DE668EL11AVOLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200082582' AND c.sku = 'BL657EL55BBOLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200072582' AND c.sku = 'BL657EL55BBOLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200052582' AND c.sku = 'SO722EL52BYTLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200012582' AND c.sku = 'HP677EL43BVGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200032582' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200086582' AND c.sku = 'SO722EL01BHMLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200076582' AND c.sku = 'SO722EL01BHMLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200056582' AND c.sku = 'SO722EL01BHMLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200016582' AND c.sku = 'NO706EL35BCILAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200036582' AND c.sku = 'SO722EL15AROLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200049582' AND c.sku = 'SO722EL15AROLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200029582' AND c.sku = 'SO722EL15AROLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200069582' AND c.sku = 'SO722EL52BYTLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200099582' AND c.sku = 'SA717EL69BEWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200079582' AND c.sku = 'ZT736EL67CVGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200059582' AND c.sku = 'CA660EL12ANVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200068582' AND c.sku = 'BL657EL41AMSLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200088582' AND c.sku = 'SA717EL69BEWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200078582' AND c.sku = 'DE668EL15AVKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200058582' AND c.sku = 'BL657EL34AURLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200018582' AND c.sku = 'ZT736EL68CVFLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200018582' AND c.sku = 'HP677EL58BURLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200038582' AND c.sku = 'SO722EL76BILLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200047582' AND c.sku = 'HP677EL26CHLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =0, CANCEL =1, PENDING =0, RETURNED = 0 WHERE order_nr = '200086572' AND c.sku = 'TO726EL05FSSLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200042252' AND c.sku = 'XT820EL10HIVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200054882' AND c.sku = 'SO722EL07BHGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200034882' AND c.sku = 'MO700EL24CPFLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200034882' AND c.sku = 'CA660EL19ANOLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200022882' AND c.sku = 'SO722EL79BIILAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200012882' AND c.sku = 'NO706EL37BCGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200032882' AND c.sku = 'SO722EL67BYELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200046882' AND c.sku = 'CA660EL02AOFLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200026882' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200066882' AND c.sku = 'SO722EL12ARRLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200016882' AND c.sku = 'SI732EL14BSNLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200029882' AND c.sku = 'NO706EL37BCGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200029882' AND c.sku = 'AP650EL23ANKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200099882' AND c.sku = 'NO706EL20BCXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200089882' AND c.sku = 'SO722EL94BXDLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200079882' AND c.sku = 'SI732EL17BSKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200059882' AND c.sku = 'SA717EL68BEXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200019882' AND c.sku = 'SO722EL37BZILAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200039882' AND c.sku = 'NO706EL37BCGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200048882' AND c.sku = 'CA661EL53BNELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200028882' AND c.sku = 'AP650EL23ANKLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200028882' AND c.sku = 'NO706EL37BCGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200068882' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200098882' AND c.sku = 'NO706EL37BCGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200088882' AND c.sku = 'HP677EL26CHLLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200078882' AND c.sku = 'NO706EL37BCGLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200038882' AND c.sku = 'BL657EL41AMSLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200047882' AND c.sku = 'AP650EL21ANMLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200027882' AND c.sku = 'BL657EL37AMWLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200097882' AND c.sku = 'SO722EL79BIILAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200087882' AND c.sku = 'SO722EL34AQVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 1, OAC =1, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200066646' AND c.sku = 'CA740HL62DDDLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200079526' AND c.sku = 'SA717EL06HMVLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200099626' AND c.sku = 'CO789EL72HCPLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200029926' AND c.sku = 'SA717EL68BEXLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200058926' AND c.sku = 'LG689EL35MUELAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200057926' AND c.sku = 'TO726EL22LANLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200095926' AND c.sku = 'BL657EL55BBOLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200013926' AND c.sku = 'LG689EL30LAFLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200036826' AND c.sku = 'SP896TB15LEQLAVEN';
UPDATE tbl_order_detail t inner join bob_live_ve.catalog_simple s on s.sku = t.sku inner join bob_live_ve.catalog_config c on c.id_catalog_config = s.fk_catalog_config SET OBC = 0, OAC =0, CANCEL =0, PENDING =0, RETURNED = 0 WHERE order_nr = '200022566' AND c.sku = 'SA717EL02GMBLAVEN';

#Costos
UPDATE tbl_order_detail t SET cost_pet = 5460, costo_after_vat=4875 WHERE order_nr = '200011246' AND sku = 'BL657EL55BBOLAVEN-63999';
UPDATE tbl_order_detail t SET cost_pet = 999, costo_after_vat=892 WHERE order_nr = '200011252' AND sku = 'SI732EL73GBQLAVEN-67390';
UPDATE tbl_order_detail t SET cost_pet = 8075, costo_after_vat=7210 WHERE order_nr = '200011646' AND sku = 'SA717EL28KDFLAVEN-70184';
UPDATE tbl_order_detail t SET cost_pet = 1446, costo_after_vat=1292 WHERE order_nr = '200011946' AND sku = 'SO722EL40KZVLAVEN-70771';
UPDATE tbl_order_detail t SET cost_pet = 345, costo_after_vat=308 WHERE order_nr = '200011946' AND sku = 'LA857EL82IKVLAVEN-69030';
UPDATE tbl_order_detail t SET cost_pet = 412, costo_after_vat=368 WHERE order_nr = '200012646' AND sku = 'OS854EL95IKILAVEN-69017';
UPDATE tbl_order_detail t SET cost_pet = 1033, costo_after_vat=922 WHERE order_nr = '200012846' AND sku = 'CA740HL71DCULAVEN-65392';
UPDATE tbl_order_detail t SET cost_pet = 540, costo_after_vat=482 WHERE order_nr = '200012946' AND sku = 'TO912TB72LKDLAVEN-71074';
UPDATE tbl_order_detail t SET cost_pet = 783, costo_after_vat=699 WHERE order_nr = '200012946' AND sku = 'CA740HL26IBLLAVEN-68739';
UPDATE tbl_order_detail t SET cost_pet = 412, costo_after_vat=368 WHERE order_nr = '200013246' AND sku = 'OS854EL95IKILAVEN-69017';
UPDATE tbl_order_detail t SET cost_pet = 8075, costo_after_vat=7210 WHERE order_nr = '200013646' AND sku = 'SA717EL62ATPLAVEN-63792';
UPDATE tbl_order_detail t SET cost_pet = 6900, costo_after_vat=6161 WHERE order_nr = '200013646' AND sku = 'SA717EL02GMBLAVEN-67661';
UPDATE tbl_order_detail t SET cost_pet = 742, costo_after_vat=663 WHERE order_nr = '200014646' AND sku = 'SO722EL57EICLAVEN-66205';
UPDATE tbl_order_detail t SET cost_pet = 345, costo_after_vat=308 WHERE order_nr = '200014946' AND sku = 'LA857EL82IKVLAVEN-69030';
UPDATE tbl_order_detail t SET cost_pet = 187, costo_after_vat=167 WHERE order_nr = '200014946' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET cost_pet = 187, costo_after_vat=167 WHERE order_nr = '200014946' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET cost_pet = 6900, costo_after_vat=6161 WHERE order_nr = '200015646' AND sku = 'SA717EL02GMBLAVEN-67661';
UPDATE tbl_order_detail t SET cost_pet = 684, costo_after_vat=611 WHERE order_nr = '200015846' AND sku = 'SO722EL77CBQLAVEN-64687';
UPDATE tbl_order_detail t SET cost_pet = 143, costo_after_vat=128 WHERE order_nr = '200015846' AND sku = 'DI899TB04LFBLAVEN-70907';
UPDATE tbl_order_detail t SET cost_pet = 160, costo_after_vat=143 WHERE order_nr = '200015846' AND sku = 'HA894TB28LEDLAVEN-70883';
UPDATE tbl_order_detail t SET cost_pet = 201, costo_after_vat=179 WHERE order_nr = '200015946' AND sku = 'DI905TB80LJVLAVEN-71066';
UPDATE tbl_order_detail t SET cost_pet = 110, costo_after_vat=98 WHERE order_nr = '200015946' AND sku = 'HA894TB37LDULAVEN-70874';
UPDATE tbl_order_detail t SET cost_pet = 121, costo_after_vat=108 WHERE order_nr = '200015946' AND sku = 'TO912TB71LKELAVEN-71075';
UPDATE tbl_order_detail t SET cost_pet = 137, costo_after_vat=122 WHERE order_nr = '200015946' AND sku = 'HA894TB39LDSLAVEN-70872';
UPDATE tbl_order_detail t SET cost_pet = 5460, costo_after_vat=4875 WHERE order_nr = '200016646' AND sku = 'BL657EL55BBOLAVEN-63999';
UPDATE tbl_order_detail t SET cost_pet = 491, costo_after_vat=439 WHERE order_nr = '200016846' AND sku = 'CA740HL80DCLLAVEN-65383';
UPDATE tbl_order_detail t SET cost_pet = 360, costo_after_vat=321 WHERE order_nr = '200016946' AND sku = 'OS854EL93IKKLAVEN-69019';
UPDATE tbl_order_detail t SET cost_pet = 300, costo_after_vat=268 WHERE order_nr = '200017646' AND sku = 'SO722EL33LACLAVEN-70778';
UPDATE tbl_order_detail t SET cost_pet = 4892, costo_after_vat=4368 WHERE order_nr = '200017846' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET cost_pet = 477, costo_after_vat=426 WHERE order_nr = '200017946' AND sku = 'NO758HL61EHYLAVEN-66201';
UPDATE tbl_order_detail t SET cost_pet = 208, costo_after_vat=186 WHERE order_nr = '200018646' AND sku = 'DI899TB05LFALAVEN-70906';
UPDATE tbl_order_detail t SET cost_pet = 122, costo_after_vat=109 WHERE order_nr = '200018646' AND sku = 'CA897TB11LEULAVEN-70900';
UPDATE tbl_order_detail t SET cost_pet = 4892, costo_after_vat=4368 WHERE order_nr = '200018846' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET cost_pet = 2995, costo_after_vat=2674 WHERE order_nr = '200018946' AND sku = 'BL657EL37AMWLAVEN-63612';
UPDATE tbl_order_detail t SET cost_pet = 1294, costo_after_vat=1155 WHERE order_nr = '200019646' AND sku = 'CA740HL65DDALAVEN-65398';
UPDATE tbl_order_detail t SET cost_pet = 554, costo_after_vat=494 WHERE order_nr = '200019846' AND sku = 'CA740HL64DDBLAVEN-65399';
UPDATE tbl_order_detail t SET cost_pet = 725, costo_after_vat=647 WHERE order_nr = '200019946' AND sku = 'CA740HL73DCSLAVEN-65390';
UPDATE tbl_order_detail t SET cost_pet = 1200, costo_after_vat=1071 WHERE order_nr = '200019946' AND sku = 'SA717EL10IFXLAVEN-68902';
UPDATE tbl_order_detail t SET cost_pet = 2022, costo_after_vat=1805 WHERE order_nr = '200021646' AND sku = 'GA766EL73FEOLAVEN-66790';
UPDATE tbl_order_detail t SET cost_pet = 413, costo_after_vat=369 WHERE order_nr = '200021846' AND sku = 'MO917TB58LKRLAVEN-71088';
UPDATE tbl_order_detail t SET cost_pet = 823, costo_after_vat=735 WHERE order_nr = '200021946' AND sku = 'CA740HL76DCPLAVEN-65387';
UPDATE tbl_order_detail t SET cost_pet = 1033, costo_after_vat=922 WHERE order_nr = '200021946' AND sku = 'CA740HL71DCULAVEN-65392';
UPDATE tbl_order_detail t SET cost_pet = 605, costo_after_vat=540 WHERE order_nr = '200021946' AND sku = 'CA740HL32IBFLAVEN-68733';
UPDATE tbl_order_detail t SET cost_pet = 2410, costo_after_vat=2152 WHERE order_nr = '200021952' AND sku = 'BL657EL41AMSLAVEN-63608';
UPDATE tbl_order_detail t SET cost_pet = 86, costo_after_vat=77 WHERE order_nr = '200022452' AND sku = 'OC741HL12DFBLAVEN-65451';
UPDATE tbl_order_detail t SET cost_pet = 269, costo_after_vat=240 WHERE order_nr = '200022646' AND sku = 'CO748HL41DTILAVEN-65818';
UPDATE tbl_order_detail t SET cost_pet = 345, costo_after_vat=308 WHERE order_nr = '200022946' AND sku = 'LA857EL82IKVLAVEN-69030';
UPDATE tbl_order_detail t SET cost_pet = 362, costo_after_vat=323 WHERE order_nr = '200022946' AND sku = 'OS854EL29JRQLAVEN-69883';
UPDATE tbl_order_detail t SET cost_pet = 285, costo_after_vat=254 WHERE order_nr = '200023246' AND sku = 'MA746HL69DKOLAVEN-65590';
UPDATE tbl_order_detail t SET cost_pet = 285, costo_after_vat=254 WHERE order_nr = '200023246' AND sku = 'MA746HL69DKOLAVEN-65590';
UPDATE tbl_order_detail t SET cost_pet = 4848, costo_after_vat=4328 WHERE order_nr = '200023646' AND sku = 'PA708EL81HNULAVEN-68384';
UPDATE tbl_order_detail t SET cost_pet = 2065, costo_after_vat=1844 WHERE order_nr = '200023946' AND sku = 'CA740HL51DDOLAVEN-65412';
UPDATE tbl_order_detail t SET cost_pet = 1401, costo_after_vat=1251 WHERE order_nr = '200023946' AND sku = 'CA740HL24IBNLAVEN-68741';
UPDATE tbl_order_detail t SET cost_pet = 280, costo_after_vat=250 WHERE order_nr = '200023946' AND sku = 'OS854EL04IJZLAVEN-69008';
UPDATE tbl_order_detail t SET cost_pet = 151, costo_after_vat=134 WHERE order_nr = '200024646' AND sku = 'CA780HL19GDSLAVEN-67444';
UPDATE tbl_order_detail t SET cost_pet = 172, costo_after_vat=154 WHERE order_nr = '200024646' AND sku = 'CA780HL15GDWLAVEN-67448';
UPDATE tbl_order_detail t SET cost_pet = 113, costo_after_vat=101 WHERE order_nr = '200024646' AND sku = 'PH898TB07LEYLAVEN-70904';
UPDATE tbl_order_detail t SET cost_pet = 9990, costo_after_vat=8920 WHERE order_nr = '200024946' AND sku = 'SA717EL36IITLAVEN-68976';
UPDATE tbl_order_detail t SET cost_pet = 121, costo_after_vat=108 WHERE order_nr = '200025646' AND sku = 'TO912TB71LKELAVEN-71075';
UPDATE tbl_order_detail t SET cost_pet = 496, costo_after_vat=443 WHERE order_nr = '200025646' AND sku = 'OS854EL96IKHLAVEN-69016';
UPDATE tbl_order_detail t SET cost_pet = 8075, costo_after_vat=7210 WHERE order_nr = '200025846' AND sku = 'SA717EL28KDFLAVEN-70184';
UPDATE tbl_order_detail t SET cost_pet = 79, costo_after_vat=71 WHERE order_nr = '200025946' AND sku = 'AD731EL10GEBLAVEN-67453';
UPDATE tbl_order_detail t SET cost_pet = 360, costo_after_vat=321 WHERE order_nr = '200025946' AND sku = 'OS854EL93IKKLAVEN-69019';
UPDATE tbl_order_detail t SET cost_pet = 1200, costo_after_vat=1071 WHERE order_nr = '200026646' AND sku = 'SA717EL10IFXLAVEN-68902';
UPDATE tbl_order_detail t SET cost_pet = 2949, costo_after_vat=2633 WHERE order_nr = '200026846' AND sku = 'ZT736EL67CVGLAVEN-65197';
UPDATE tbl_order_detail t SET cost_pet = 413, costo_after_vat=369 WHERE order_nr = '200026846' AND sku = 'MO903TB91LFOLAVEN-70920';
UPDATE tbl_order_detail t SET cost_pet = 413, costo_after_vat=369 WHERE order_nr = '200026846' AND sku = 'MO903TB91LFOLAVEN-70920';
UPDATE tbl_order_detail t SET cost_pet = 745, costo_after_vat=665 WHERE order_nr = '200026846' AND sku = 'PR822HL91HFSLAVEN-68174';
UPDATE tbl_order_detail t SET cost_pet = 188, costo_after_vat=168 WHERE order_nr = '200026846' AND sku = 'PR822HL89HFULAVEN-68176';
UPDATE tbl_order_detail t SET cost_pet = 2065, costo_after_vat=1844 WHERE order_nr = '200026946' AND sku = 'CA740HL51DDOLAVEN-65412';
UPDATE tbl_order_detail t SET cost_pet = 201, costo_after_vat=179 WHERE order_nr = '200027646' AND sku = 'DI905TB80LJVLAVEN-71066';
UPDATE tbl_order_detail t SET cost_pet = 137, costo_after_vat=122 WHERE order_nr = '200027646' AND sku = 'SP896TB14LERLAVEN-70897';
UPDATE tbl_order_detail t SET cost_pet = 1205, costo_after_vat=1076 WHERE order_nr = '200027846' AND sku = 'CA740HL48DDRLAVEN-65415';
UPDATE tbl_order_detail t SET cost_pet = 4892, costo_after_vat=4368 WHERE order_nr = '200027946' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET cost_pet = 362, costo_after_vat=323 WHERE order_nr = '200028646' AND sku = 'OS854EL29JRQLAVEN-69883';
UPDATE tbl_order_detail t SET cost_pet = 1999, costo_after_vat=1785 WHERE order_nr = '200028846' AND sku = 'LG689EL16JVZLAVEN-69996';
UPDATE tbl_order_detail t SET cost_pet = 2608, costo_after_vat=2328 WHERE order_nr = '200028946' AND sku = 'CO826EL67JUALAVEN-69945';
UPDATE tbl_order_detail t SET cost_pet = 3616, costo_after_vat=3229 WHERE order_nr = '200028946' AND sku = 'SO722EL34LABLAVEN-70777';
UPDATE tbl_order_detail t SET cost_pet = 112, costo_after_vat=100 WHERE order_nr = '200029552' AND sku = 'LU870FA27JCILAVEN-69485';
UPDATE tbl_order_detail t SET cost_pet = 112, costo_after_vat=100 WHERE order_nr = '200029552' AND sku = 'LU870FA26JCJLAVEN-69486';
UPDATE tbl_order_detail t SET cost_pet = 112, costo_after_vat=100 WHERE order_nr = '200029552' AND sku = 'LU870FA31JCELAVEN-69481';
UPDATE tbl_order_detail t SET cost_pet = 112, costo_after_vat=100 WHERE order_nr = '200029552' AND sku = 'LU870FA33JCCLAVEN-69479';
UPDATE tbl_order_detail t SET cost_pet = 112, costo_after_vat=100 WHERE order_nr = '200029552' AND sku = 'LU870FA28JCHLAVEN-69484';
UPDATE tbl_order_detail t SET cost_pet = 112, costo_after_vat=100 WHERE order_nr = '200029552' AND sku = 'LU870FA32JCDLAVEN-69480';
UPDATE tbl_order_detail t SET cost_pet = 112, costo_after_vat=100 WHERE order_nr = '200029552' AND sku = 'LU870FA30JCFLAVEN-69482';
UPDATE tbl_order_detail t SET cost_pet = 112, costo_after_vat=100 WHERE order_nr = '200029552' AND sku = 'LU870FA29JCGLAVEN-69483';
UPDATE tbl_order_detail t SET cost_pet = 742, costo_after_vat=663 WHERE order_nr = '200029646' AND sku = 'SO722EL57EICLAVEN-66205';
UPDATE tbl_order_detail t SET cost_pet = 8075, costo_after_vat=7210 WHERE order_nr = '200029846' AND sku = 'SA717EL29ANELAVEN-63620';
UPDATE tbl_order_detail t SET cost_pet = 554, costo_after_vat=494 WHERE order_nr = '200029946' AND sku = 'CA740HL63DDCLAVEN-65400';
UPDATE tbl_order_detail t SET cost_pet = 1033, costo_after_vat=922 WHERE order_nr = '200029946' AND sku = 'CA740HL72DCTLAVEN-65391';
UPDATE tbl_order_detail t SET cost_pet = 491, costo_after_vat=439 WHERE order_nr = '200029946' AND sku = 'CA740HL81DCKLAVEN-65382';
UPDATE tbl_order_detail t SET cost_pet = 344, costo_after_vat=307 WHERE order_nr = '200031246' AND sku = 'TO912TB70LKFLAVEN-71076';
UPDATE tbl_order_detail t SET cost_pet = 344, costo_after_vat=307 WHERE order_nr = '200031246' AND sku = 'TO912TB70LKFLAVEN-71076';
UPDATE tbl_order_detail t SET cost_pet = 662, costo_after_vat=591 WHERE order_nr = '200031646' AND sku = 'CO826EL45JUWLAVEN-69967';
UPDATE tbl_order_detail t SET cost_pet = 553, costo_after_vat=494 WHERE order_nr = '200031946' AND sku = 'CA740HL43DDWLAVEN-65420';
UPDATE tbl_order_detail t SET cost_pet = 1205, costo_after_vat=1076 WHERE order_nr = '200031946' AND sku = 'CA740HL49DDQLAVEN-65414';
UPDATE tbl_order_detail t SET cost_pet = 886, costo_after_vat=791 WHERE order_nr = '200031946' AND sku = 'CA740HL42DDXLAVEN-65421';
UPDATE tbl_order_detail t SET cost_pet = 117, costo_after_vat=104 WHERE order_nr = '200032646' AND sku = 'OC741HL12DFBLAVEN-65451';
UPDATE tbl_order_detail t SET cost_pet = 123, costo_after_vat=110 WHERE order_nr = '200032646' AND sku = 'OC741HL16DEXLAVEN-65447';
UPDATE tbl_order_detail t SET cost_pet = 4139, costo_after_vat=3696 WHERE order_nr = '200032646' AND sku = 'SA717EL43JRCLAVEN-69869';
UPDATE tbl_order_detail t SET cost_pet = 8075, costo_after_vat=7210 WHERE order_nr = '200032846' AND sku = 'SA717EL28KDFLAVEN-70184';
UPDATE tbl_order_detail t SET cost_pet = 491, costo_after_vat=439 WHERE order_nr = '200032946' AND sku = 'CA740HL80DCLLAVEN-65383';
UPDATE tbl_order_detail t SET cost_pet = 400, costo_after_vat=357 WHERE order_nr = '200033246' AND sku = 'BI873EL26JRTLAVEN-69886';
UPDATE tbl_order_detail t SET cost_pet = 8075, costo_after_vat=7210 WHERE order_nr = '200033646' AND sku = 'SA717EL29ANELAVEN-63620';
UPDATE tbl_order_detail t SET cost_pet = 431, costo_after_vat=385 WHERE order_nr = '200034646' AND sku = 'OS854EL11IJSLAVEN-69001';
UPDATE tbl_order_detail t SET cost_pet = 183, costo_after_vat=163 WHERE order_nr = '200034646' AND sku = 'MA914TB65LKKLAVEN-71081';
UPDATE tbl_order_detail t SET cost_pet = 300, costo_after_vat=268 WHERE order_nr = '200034646' AND sku = 'SO722EL33LACLAVEN-70778';
UPDATE tbl_order_detail t SET cost_pet = 2442, costo_after_vat=2180 WHERE order_nr = '200034652' AND sku = 'KI684EL67FAYLAVEN-66695';
UPDATE tbl_order_detail t SET cost_pet = 3890, costo_after_vat=3473 WHERE order_nr = '200034946' AND sku = 'BL657EL36AMXLAVEN-63613';
UPDATE tbl_order_detail t SET cost_pet = 150, costo_after_vat=134 WHERE order_nr = '200035646' AND sku = 'MI911TB77LJYLAVEN-71069';
UPDATE tbl_order_detail t SET cost_pet = 344, costo_after_vat=307 WHERE order_nr = '200035646' AND sku = 'TO912TB70LKFLAVEN-71076';
UPDATE tbl_order_detail t SET cost_pet = 745, costo_after_vat=665 WHERE order_nr = '200035646' AND sku = 'PR822HL91HFSLAVEN-68174';
UPDATE tbl_order_detail t SET cost_pet = 950, costo_after_vat=848 WHERE order_nr = '200035846' AND sku = 'CO826EL60JUHLAVEN-69952';
UPDATE tbl_order_detail t SET cost_pet = 137, costo_after_vat=122 WHERE order_nr = '200035846' AND sku = 'HA894TB39LDSLAVEN-70872';
UPDATE tbl_order_detail t SET cost_pet = 412, costo_after_vat=368 WHERE order_nr = '200035946' AND sku = 'OS854EL95IKILAVEN-69017';
UPDATE tbl_order_detail t SET cost_pet = 10298, costo_after_vat=9195 WHERE order_nr = '200036252' AND sku = 'LE688EL58GZHLAVEN-68006';
UPDATE tbl_order_detail t SET cost_pet = 70, costo_after_vat=62 WHERE order_nr = '200036646' AND sku = 'PU858EL66ILLLAVEN-69046';
UPDATE tbl_order_detail t SET cost_pet = 179, costo_after_vat=160 WHERE order_nr = '200036846' AND sku = 'XM850EL97IGKLAVEN-68915';
UPDATE tbl_order_detail t SET cost_pet = 2683, costo_after_vat=2395 WHERE order_nr = '200036946' AND sku = 'CA740HL50DDPLAVEN-65413';
UPDATE tbl_order_detail t SET cost_pet = 208, costo_after_vat=186 WHERE order_nr = '200037646' AND sku = 'DI899TB05LFALAVEN-70906';
UPDATE tbl_order_detail t SET cost_pet = 4892, costo_after_vat=4368 WHERE order_nr = '200037846' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET cost_pet = 612, costo_after_vat=546 WHERE order_nr = '200037946' AND sku = 'NI704EL74CYVLAVEN-65289';
UPDATE tbl_order_detail t SET cost_pet = 594, costo_after_vat=530 WHERE order_nr = '200037946' AND sku = 'NI704EL19LAQLAVEN-70792';
UPDATE tbl_order_detail t SET cost_pet = 785, costo_after_vat=701 WHERE order_nr = '200037946' AND sku = 'CA740HL29IBILAVEN-68736';
UPDATE tbl_order_detail t SET cost_pet = 1200, costo_after_vat=1071 WHERE order_nr = '200038646' AND sku = 'SA717EL10IFXLAVEN-68902';
UPDATE tbl_order_detail t SET cost_pet = 1198, costo_after_vat=1070 WHERE order_nr = '200038846' AND sku = 'OL750EL85DVMLAVEN-65874';
UPDATE tbl_order_detail t SET cost_pet = 1473, costo_after_vat=1315 WHERE order_nr = '200038946' AND sku = 'CO826EL59JUILAVEN-69953';
UPDATE tbl_order_detail t SET cost_pet = 5460, costo_after_vat=4875 WHERE order_nr = '200039646' AND sku = 'BL657EL55BBOLAVEN-63999';
UPDATE tbl_order_detail t SET cost_pet = 2407, costo_after_vat=2149 WHERE order_nr = '200039846' AND sku = 'PA708EL84HNRLAVEN-68381';
UPDATE tbl_order_detail t SET cost_pet = 1356, costo_after_vat=1211 WHERE order_nr = '200039946' AND sku = 'AD731EL93OEKLAVEN-72953';
UPDATE tbl_order_detail t SET cost_pet = 662, costo_after_vat=591 WHERE order_nr = '200041646' AND sku = 'CO826EL45JUWLAVEN-69967';
UPDATE tbl_order_detail t SET cost_pet = 413, costo_after_vat=369 WHERE order_nr = '200041846' AND sku = 'MO903TB91LFOLAVEN-70920';
UPDATE tbl_order_detail t SET cost_pet = 2448, costo_after_vat=2186 WHERE order_nr = '200041846' AND sku = 'NI704EL41EMOLAVEN-66321';
UPDATE tbl_order_detail t SET cost_pet = 8075, costo_after_vat=7210 WHERE order_nr = '200041946' AND sku = 'SA717EL29ANELAVEN-63620';
UPDATE tbl_order_detail t SET cost_pet = 269, costo_after_vat=240 WHERE order_nr = '200042646' AND sku = 'CO748HL44DTFLAVEN-65815';
UPDATE tbl_order_detail t SET cost_pet = 1999, costo_after_vat=1785 WHERE order_nr = '200042946' AND sku = 'LG689EL16JVZLAVEN-69996';
UPDATE tbl_order_detail t SET cost_pet = 151, costo_after_vat=134 WHERE order_nr = '200043246' AND sku = 'CA780HL19GDSLAVEN-67444';
UPDATE tbl_order_detail t SET cost_pet = 113, costo_after_vat=101 WHERE order_nr = '200043246' AND sku = 'PH898TB07LEYLAVEN-70904';
UPDATE tbl_order_detail t SET cost_pet = 180, costo_after_vat=161 WHERE order_nr = '200043246' AND sku = 'PR822HL90HFTLAVEN-68175';
UPDATE tbl_order_detail t SET cost_pet = 477, costo_after_vat=426 WHERE order_nr = '200043646' AND sku = 'NO758HL61EHYLAVEN-66201';
UPDATE tbl_order_detail t SET cost_pet = 412, costo_after_vat=368 WHERE order_nr = '200043646' AND sku = 'OS854EL95IKILAVEN-69017';
UPDATE tbl_order_detail t SET cost_pet = 9990, costo_after_vat=8920 WHERE order_nr = '200043946' AND sku = 'SA717EL36IITLAVEN-68976';
UPDATE tbl_order_detail t SET cost_pet = 86, costo_after_vat=77 WHERE order_nr = '200044452' AND sku = 'OC741HL12DFBLAVEN-65451';
UPDATE tbl_order_detail t SET cost_pet = 246, costo_after_vat=220 WHERE order_nr = '200044646' AND sku = 'OS854EL00IKDLAVEN-69012';
UPDATE tbl_order_detail t SET cost_pet = 2410, costo_after_vat=2152 WHERE order_nr = '200044852' AND sku = 'BL657EL41AMSLAVEN-63608';
UPDATE tbl_order_detail t SET cost_pet = 437, costo_after_vat=390 WHERE order_nr = '200044946' AND sku = 'TA893TB43LDOLAVEN-70868';
UPDATE tbl_order_detail t SET cost_pet = 269, costo_after_vat=240 WHERE order_nr = '200044946' AND sku = 'CO748HL41DTILAVEN-65818';
UPDATE tbl_order_detail t SET cost_pet = 269, costo_after_vat=240 WHERE order_nr = '200044946' AND sku = 'CO748HL47DTCLAVEN-65812';
UPDATE tbl_order_detail t SET cost_pet = 70, costo_after_vat=62 WHERE order_nr = '200045646' AND sku = 'PU858EL66ILLLAVEN-69046';
UPDATE tbl_order_detail t SET cost_pet = 90, costo_after_vat=80 WHERE order_nr = '200045646' AND sku = 'PU858EL65ILMLAVEN-69047';
UPDATE tbl_order_detail t SET cost_pet = 5460, costo_after_vat=4875 WHERE order_nr = '200045846' AND sku = 'BL657EL55BBOLAVEN-63999';
UPDATE tbl_order_detail t SET cost_pet = 4892, costo_after_vat=4368 WHERE order_nr = '200045846' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET cost_pet = 2995, costo_after_vat=2674 WHERE order_nr = '200045946' AND sku = 'BL657EL37AMWLAVEN-63612';
UPDATE tbl_order_detail t SET cost_pet = 413, costo_after_vat=369 WHERE order_nr = '200046646' AND sku = 'MO903TB92LFNLAVEN-70919';
UPDATE tbl_order_detail t SET cost_pet = 8075, costo_after_vat=7210 WHERE order_nr = '200046846' AND sku = 'SA717EL29ANELAVEN-63620';
UPDATE tbl_order_detail t SET cost_pet = 3616, costo_after_vat=3229 WHERE order_nr = '200046946' AND sku = 'SO722EL34LABLAVEN-70777';
UPDATE tbl_order_detail t SET cost_pet = 745, costo_after_vat=665 WHERE order_nr = '200047646' AND sku = 'PR822HL91HFSLAVEN-68174';
UPDATE tbl_order_detail t SET cost_pet = 1198, costo_after_vat=1070 WHERE order_nr = '200047846' AND sku = 'OL750EL85DVMLAVEN-65874';
UPDATE tbl_order_detail t SET cost_pet = 4892, costo_after_vat=4368 WHERE order_nr = '200047946' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET cost_pet = 269, costo_after_vat=240 WHERE order_nr = '200048646' AND sku = 'CO748HL46DTDLAVEN-65813';
UPDATE tbl_order_detail t SET cost_pet = 4772, costo_after_vat=4261 WHERE order_nr = '200048846' AND sku = 'SO722EL15LAULAVEN-70796';
UPDATE tbl_order_detail t SET cost_pet = 2407, costo_after_vat=2149 WHERE order_nr = '200048946' AND sku = 'PA708EL84HNRLAVEN-68381';
UPDATE tbl_order_detail t SET cost_pet = 151, costo_after_vat=134 WHERE order_nr = '200049646' AND sku = 'CA780HL19GDSLAVEN-67444';
UPDATE tbl_order_detail t SET cost_pet = 151, costo_after_vat=134 WHERE order_nr = '200049646' AND sku = 'CA780HL17GDULAVEN-67446';
UPDATE tbl_order_detail t SET cost_pet = 117, costo_after_vat=104 WHERE order_nr = '200049646' AND sku = 'OC741HL19DEULAVEN-65444';
UPDATE tbl_order_detail t SET cost_pet = 117, costo_after_vat=104 WHERE order_nr = '200049646' AND sku = 'OC741HL19DEULAVEN-65444';
UPDATE tbl_order_detail t SET cost_pet = 592, costo_after_vat=529 WHERE order_nr = '200049846' AND sku = 'MO902TB95LFKLAVEN-70916';
UPDATE tbl_order_detail t SET cost_pet = 437, costo_after_vat=390 WHERE order_nr = '200049846' AND sku = 'TA893TB42LDPLAVEN-70869';
UPDATE tbl_order_detail t SET cost_pet = 437, costo_after_vat=390 WHERE order_nr = '200049846' AND sku = 'TA893TB42LDPLAVEN-70869';
UPDATE tbl_order_detail t SET cost_pet = 8075, costo_after_vat=7210 WHERE order_nr = '200049946' AND sku = 'SA717EL62ATPLAVEN-63792';
UPDATE tbl_order_detail t SET cost_pet = 8075, costo_after_vat=7210 WHERE order_nr = '200049946' AND sku = 'SA717EL29ANELAVEN-63620';
UPDATE tbl_order_detail t SET cost_pet = 187, costo_after_vat=167 WHERE order_nr = '200051246' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET cost_pet = 187, costo_after_vat=167 WHERE order_nr = '200051246' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET cost_pet = 408, costo_after_vat=364 WHERE order_nr = '200051246' AND sku = 'MA746HL39DLSLAVEN-65620';
UPDATE tbl_order_detail t SET cost_pet = 4892, costo_after_vat=4368 WHERE order_nr = '200051646' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET cost_pet = 345, costo_after_vat=308 WHERE order_nr = '200051646' AND sku = 'LA857EL82IKVLAVEN-69030';
UPDATE tbl_order_detail t SET cost_pet = 2995, costo_after_vat=2674 WHERE order_nr = '200051946' AND sku = 'BL657EL37AMWLAVEN-63612';
UPDATE tbl_order_detail t SET cost_pet = 86, costo_after_vat=77 WHERE order_nr = '200052452' AND sku = 'OC741HL12DFBLAVEN-65451';
UPDATE tbl_order_detail t SET cost_pet = 564, costo_after_vat=504 WHERE order_nr = '200052646' AND sku = 'PA708EL43KZSLAVEN-70768';
UPDATE tbl_order_detail t SET cost_pet = 187, costo_after_vat=167 WHERE order_nr = '200052646' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET cost_pet = 187, costo_after_vat=167 WHERE order_nr = '200052646' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET cost_pet = 3428, costo_after_vat=3061 WHERE order_nr = '200052646' AND sku = 'SA717EL44JRBLAVEN-69868';
UPDATE tbl_order_detail t SET cost_pet = 437, costo_after_vat=390 WHERE order_nr = '200052846' AND sku = 'TA893TB43LDOLAVEN-70868';
UPDATE tbl_order_detail t SET cost_pet = 437, costo_after_vat=390 WHERE order_nr = '200052846' AND sku = 'TA893TB43LDOLAVEN-70868';
UPDATE tbl_order_detail t SET cost_pet = 330, costo_after_vat=295 WHERE order_nr = '200052846' AND sku = 'MI915TB62LKNLAVEN-71084';
UPDATE tbl_order_detail t SET cost_pet = 973, costo_after_vat=869 WHERE order_nr = '200052946' AND sku = 'FR860FA55ILWLAVEN-69057';
UPDATE tbl_order_detail t SET cost_pet = 457, costo_after_vat=408 WHERE order_nr = '200052946' AND sku = 'OS854EL28JRRLAVEN-69884';
UPDATE tbl_order_detail t SET cost_pet = 269, costo_after_vat=240 WHERE order_nr = '200053246' AND sku = 'CO748HL41DTILAVEN-65818';
UPDATE tbl_order_detail t SET cost_pet = 2995, costo_after_vat=2674 WHERE order_nr = '200053646' AND sku = 'BL657EL37AMWLAVEN-63612';
UPDATE tbl_order_detail t SET cost_pet = 5814, costo_after_vat=5191 WHERE order_nr = '200054646' AND sku = 'LE688EL41LLILAVEN-71105';
UPDATE tbl_order_detail t SET cost_pet = 187, costo_after_vat=167 WHERE order_nr = '200054646' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET cost_pet = 950, costo_after_vat=848 WHERE order_nr = '200054946' AND sku = 'CO826EL60JUHLAVEN-69952';
UPDATE tbl_order_detail t SET cost_pet = 710, costo_after_vat=634 WHERE order_nr = '200054946' AND sku = 'BO829EL36IQLLAVEN-69176';
UPDATE tbl_order_detail t SET cost_pet = 710, costo_after_vat=634 WHERE order_nr = '200054946' AND sku = 'BO829EL39IQILAVEN-69173';
UPDATE tbl_order_detail t SET cost_pet = 407, costo_after_vat=364 WHERE order_nr = '200055646' AND sku = 'CA740HL85DJYLAVEN-65574';
UPDATE tbl_order_detail t SET cost_pet = 2448, costo_after_vat=2186 WHERE order_nr = '200055846' AND sku = 'NI704EL41EMOLAVEN-66321';
UPDATE tbl_order_detail t SET cost_pet = 5478, costo_after_vat=4891 WHERE order_nr = '200055946' AND sku = 'SO722EL14LAVLAVEN-70797';
UPDATE tbl_order_detail t SET cost_pet = 86, costo_after_vat=77 WHERE order_nr = '200056452' AND sku = 'OC741HL12DFBLAVEN-65451';
UPDATE tbl_order_detail t SET cost_pet = 564, costo_after_vat=504 WHERE order_nr = '200056646' AND sku = 'PA708EL43KZSLAVEN-70768';
UPDATE tbl_order_detail t SET cost_pet = 360, costo_after_vat=321 WHERE order_nr = '200056846' AND sku = 'OS854EL93IKKLAVEN-69019';
UPDATE tbl_order_detail t SET cost_pet = 360, costo_after_vat=321 WHERE order_nr = '200056846' AND sku = 'OS854EL93IKKLAVEN-69019';
UPDATE tbl_order_detail t SET cost_pet = 1200, costo_after_vat=1071 WHERE order_nr = '200056846' AND sku = 'SA717EL10IFXLAVEN-68902';
UPDATE tbl_order_detail t SET cost_pet = 8075, costo_after_vat=7210 WHERE order_nr = '200056946' AND sku = 'SA717EL28KDFLAVEN-70184';
UPDATE tbl_order_detail t SET cost_pet = 1076, costo_after_vat=961 WHERE order_nr = '200057646' AND sku = 'LG689EL47KZOLAVEN-70764';
UPDATE tbl_order_detail t SET cost_pet = 2022, costo_after_vat=1805 WHERE order_nr = '200057846' AND sku = 'GA766EL73FEOLAVEN-66790';
UPDATE tbl_order_detail t SET cost_pet = 4892, costo_after_vat=4368 WHERE order_nr = '200057946' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET cost_pet = 345, costo_after_vat=308 WHERE order_nr = '200057946' AND sku = 'LA857EL82IKVLAVEN-69030';
UPDATE tbl_order_detail t SET cost_pet = 345, costo_after_vat=308 WHERE order_nr = '200057946' AND sku = 'LA857EL82IKVLAVEN-69030';
UPDATE tbl_order_detail t SET cost_pet = 5460, costo_after_vat=4875 WHERE order_nr = '200058646' AND sku = 'BL657EL55BBOLAVEN-63999';
UPDATE tbl_order_detail t SET cost_pet = 2995, costo_after_vat=2674 WHERE order_nr = '200058846' AND sku = 'BL657EL59KRKLAVEN-70552';
UPDATE tbl_order_detail t SET cost_pet = 9990, costo_after_vat=8920 WHERE order_nr = '200058946' AND sku = 'SA717EL36IITLAVEN-68976';
UPDATE tbl_order_detail t SET cost_pet = 344, costo_after_vat=307 WHERE order_nr = '200059646' AND sku = 'TO912TB70LKFLAVEN-71076';
UPDATE tbl_order_detail t SET cost_pet = 285, costo_after_vat=254 WHERE order_nr = '200059646' AND sku = 'MA746HL69DKOLAVEN-65590';
UPDATE tbl_order_detail t SET cost_pet = 437, costo_after_vat=390 WHERE order_nr = '200059846' AND sku = 'TA893TB42LDPLAVEN-70869';
UPDATE tbl_order_detail t SET cost_pet = 2022, costo_after_vat=1805 WHERE order_nr = '200059946' AND sku = 'GA766EL73FEOLAVEN-66790';
UPDATE tbl_order_detail t SET cost_pet = 1098, costo_after_vat=980 WHERE order_nr = '200061352' AND sku = 'ST878EL98KIFLAVEN-70314';
UPDATE tbl_order_detail t SET cost_pet = 2995, costo_after_vat=2674 WHERE order_nr = '200061646' AND sku = 'BL657EL37AMWLAVEN-63612';
UPDATE tbl_order_detail t SET cost_pet = 684, costo_after_vat=611 WHERE order_nr = '200061846' AND sku = 'SO722EL77CBQLAVEN-64687';
UPDATE tbl_order_detail t SET cost_pet = 5478, costo_after_vat=4891 WHERE order_nr = '200061846' AND sku = 'SO722EL14LAVLAVEN-70797';
UPDATE tbl_order_detail t SET cost_pet = 3890, costo_after_vat=3473 WHERE order_nr = '200061946' AND sku = 'BL657EL36AMXLAVEN-63613';
UPDATE tbl_order_detail t SET cost_pet = 742, costo_after_vat=663 WHERE order_nr = '200062646' AND sku = 'SO722EL57EICLAVEN-66205';
UPDATE tbl_order_detail t SET cost_pet = 891, costo_after_vat=796 WHERE order_nr = '200062646' AND sku = 'SO722EL10LAZLAVEN-70801';
UPDATE tbl_order_detail t SET cost_pet = 9358, costo_after_vat=8356 WHERE order_nr = '200062946' AND sku = 'LE688EL64GFVLAVEN-67499';
UPDATE tbl_order_detail t SET cost_pet = 247, costo_after_vat=221 WHERE order_nr = '200063246' AND sku = 'HA894TB24LEHLAVEN-70887';
UPDATE tbl_order_detail t SET cost_pet = 412, costo_after_vat=368 WHERE order_nr = '200063246' AND sku = 'OS854EL95IKILAVEN-69017';
UPDATE tbl_order_detail t SET cost_pet = 4892, costo_after_vat=4368 WHERE order_nr = '200063646' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET cost_pet = 461, costo_after_vat=412 WHERE order_nr = '200063946' AND sku = 'CA740HL86DJXLAVEN-65573';
UPDATE tbl_order_detail t SET cost_pet = 605, costo_after_vat=540 WHERE order_nr = '200063946' AND sku = 'CA740HL31IBGLAVEN-68734';
UPDATE tbl_order_detail t SET cost_pet = 6602, costo_after_vat=5895 WHERE order_nr = '200064646' AND sku = 'PA708EL82HNTLAVEN-68383';
UPDATE tbl_order_detail t SET cost_pet = 823, costo_after_vat=735 WHERE order_nr = '200064946' AND sku = 'CA740HL77DCOLAVEN-65386';
UPDATE tbl_order_detail t SET cost_pet = 1660, costo_after_vat=1483 WHERE order_nr = '200065646' AND sku = 'CA740HL47DDSLAVEN-65416';
UPDATE tbl_order_detail t SET cost_pet = 187, costo_after_vat=167 WHERE order_nr = '200065646' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET cost_pet = 3890, costo_after_vat=3473 WHERE order_nr = '200065846' AND sku = 'BL657EL36AMXLAVEN-63613';
UPDATE tbl_order_detail t SET cost_pet = 72, costo_after_vat=64 WHERE order_nr = '200065846' AND sku = 'PR856HL34IIVLAVEN-68978';
UPDATE tbl_order_detail t SET cost_pet = 345, costo_after_vat=308 WHERE order_nr = '200065946' AND sku = 'LA857EL82IKVLAVEN-69030';
UPDATE tbl_order_detail t SET cost_pet = 950, costo_after_vat=848 WHERE order_nr = '200065946' AND sku = 'CO826EL60JUHLAVEN-69952';
UPDATE tbl_order_detail t SET cost_pet = 70, costo_after_vat=62 WHERE order_nr = '200065946' AND sku = 'PU858EL66ILLLAVEN-69046';
UPDATE tbl_order_detail t SET cost_pet = 140, costo_after_vat=125 WHERE order_nr = '200065946' AND sku = 'PU858EL79IKYLAVEN-69033';
UPDATE tbl_order_detail t SET cost_pet = 351, costo_after_vat=314 WHERE order_nr = '200066646' AND sku = 'CA740HL62DDDLAVEN-65401';
UPDATE tbl_order_detail t SET cost_pet = 8075, costo_after_vat=7210 WHERE order_nr = '200066846' AND sku = 'SA717EL62ATPLAVEN-63792';
UPDATE tbl_order_detail t SET cost_pet = 662, costo_after_vat=591 WHERE order_nr = '200066946' AND sku = 'CO826EL45JUWLAVEN-69967';
UPDATE tbl_order_detail t SET cost_pet = 2995, costo_after_vat=2674 WHERE order_nr = '200066946' AND sku = 'BL657EL37AMWLAVEN-63612';
UPDATE tbl_order_detail t SET cost_pet = 143, costo_after_vat=128 WHERE order_nr = '200067646' AND sku = 'DI899TB04LFBLAVEN-70907';
UPDATE tbl_order_detail t SET cost_pet = 662, costo_after_vat=591 WHERE order_nr = '200067846' AND sku = 'CO826EL45JUWLAVEN-69967';
UPDATE tbl_order_detail t SET cost_pet = 110, costo_after_vat=98 WHERE order_nr = '200067946' AND sku = 'HA894TB29LECLAVEN-70882';
UPDATE tbl_order_detail t SET cost_pet = 137, costo_after_vat=122 WHERE order_nr = '200067946' AND sku = 'HA894TB39LDSLAVEN-70872';
UPDATE tbl_order_detail t SET cost_pet = 1123, costo_after_vat=1003 WHERE order_nr = '200067946' AND sku = 'CA740HL25IBMLAVEN-68740';
UPDATE tbl_order_detail t SET cost_pet = 1294, costo_after_vat=1155 WHERE order_nr = '200068646' AND sku = 'CA740HL67DCYLAVEN-65396';
UPDATE tbl_order_detail t SET cost_pet = 187, costo_after_vat=167 WHERE order_nr = '200068646' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET cost_pet = 187, costo_after_vat=167 WHERE order_nr = '200068646' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET cost_pet = 179, costo_after_vat=160 WHERE order_nr = '200068846' AND sku = 'XM850EL96IGLLAVEN-68916';
UPDATE tbl_order_detail t SET cost_pet = 229, costo_after_vat=204 WHERE order_nr = '200068946' AND sku = 'MO917TB57LKSLAVEN-71089';
UPDATE tbl_order_detail t SET cost_pet = 351, costo_after_vat=313 WHERE order_nr = '200068946' AND sku = 'BA891TB47LDKLAVEN-70864';
UPDATE tbl_order_detail t SET cost_pet = 86, costo_after_vat=77 WHERE order_nr = '200069452' AND sku = 'OC741HL12DFBLAVEN-65451';
UPDATE tbl_order_detail t SET cost_pet = 1294, costo_after_vat=1155 WHERE order_nr = '200069646' AND sku = 'CA740HL68DCXLAVEN-65395';
UPDATE tbl_order_detail t SET cost_pet = 9990, costo_after_vat=8920 WHERE order_nr = '200069846' AND sku = 'SA717EL36IITLAVEN-68976';
UPDATE tbl_order_detail t SET cost_pet = 742, costo_after_vat=663 WHERE order_nr = '200069946' AND sku = 'SO722EL57EICLAVEN-66205';
UPDATE tbl_order_detail t SET cost_pet = 9990, costo_after_vat=8920 WHERE order_nr = '200069946' AND sku = 'SA717EL36IITLAVEN-68976';
UPDATE tbl_order_detail t SET cost_pet = 5460, costo_after_vat=4875 WHERE order_nr = '200071246' AND sku = 'BL657EL55BBOLAVEN-63999';
UPDATE tbl_order_detail t SET cost_pet = 1098, costo_after_vat=980 WHERE order_nr = '200071352' AND sku = 'ST878EL98KIFLAVEN-70314';
UPDATE tbl_order_detail t SET cost_pet = 1098, costo_after_vat=980 WHERE order_nr = '200071352' AND sku = 'ST878EL98KIFLAVEN-70314';
UPDATE tbl_order_detail t SET cost_pet = 4892, costo_after_vat=4368 WHERE order_nr = '200071646' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET cost_pet = 172, costo_after_vat=154 WHERE order_nr = '200071846' AND sku = 'CA780HL16GDVLAVEN-67447';
UPDATE tbl_order_detail t SET cost_pet = 564, costo_after_vat=504 WHERE order_nr = '200071846' AND sku = 'PA708EL43KZSLAVEN-70768';
UPDATE tbl_order_detail t SET cost_pet = 74, costo_after_vat=66 WHERE order_nr = '200071846' AND sku = 'AD731EL92OELLAVEN-72954';
UPDATE tbl_order_detail t SET cost_pet = 74, costo_after_vat=66 WHERE order_nr = '200071846' AND sku = 'AD731EL92OELLAVEN-72954';
UPDATE tbl_order_detail t SET cost_pet = 74, costo_after_vat=66 WHERE order_nr = '200071846' AND sku = 'AD731EL92OELLAVEN-72954';
UPDATE tbl_order_detail t SET cost_pet = 745, costo_after_vat=665 WHERE order_nr = '200071846' AND sku = 'PR822HL91HFSLAVEN-68174';
UPDATE tbl_order_detail t SET cost_pet = 805, costo_after_vat=719 WHERE order_nr = '200071846' AND sku = 'CA740HL83DCILAVEN-65380';
UPDATE tbl_order_detail t SET cost_pet = 6900, costo_after_vat=6161 WHERE order_nr = '200071946' AND sku = 'SA717EL02GMBLAVEN-67661';
UPDATE tbl_order_detail t SET cost_pet = 496, costo_after_vat=443 WHERE order_nr = '200072646' AND sku = 'OS854EL96IKHLAVEN-69016';
UPDATE tbl_order_detail t SET cost_pet = 187, costo_after_vat=167 WHERE order_nr = '200072646' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET cost_pet = 752, costo_after_vat=672 WHERE order_nr = '200072946' AND sku = 'CA740HL45DDULAVEN-65418';
UPDATE tbl_order_detail t SET cost_pet = 594, costo_after_vat=530 WHERE order_nr = '200072946' AND sku = 'NI704EL19LAQLAVEN-70792';
UPDATE tbl_order_detail t SET cost_pet = 310, costo_after_vat=277 WHERE order_nr = '200073246' AND sku = 'MI915TB63LKMLAVEN-71083';
UPDATE tbl_order_detail t SET cost_pet = 72, costo_after_vat=64 WHERE order_nr = '200073246' AND sku = 'PR856HL34IIVLAVEN-68978';
UPDATE tbl_order_detail t SET cost_pet = 6900, costo_after_vat=6161 WHERE order_nr = '200073646' AND sku = 'SA717EL02GMBLAVEN-67661';
UPDATE tbl_order_detail t SET cost_pet = 564, costo_after_vat=504 WHERE order_nr = '200074646' AND sku = 'PA708EL43KZSLAVEN-70768';
UPDATE tbl_order_detail t SET cost_pet = 4892, costo_after_vat=4368 WHERE order_nr = '200074946' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET cost_pet = 1098, costo_after_vat=980 WHERE order_nr = '200075646' AND sku = 'ST878EL02KIBLAVEN-70310';
UPDATE tbl_order_detail t SET cost_pet = 280, costo_after_vat=250 WHERE order_nr = '200075646' AND sku = 'OS854EL04IJZLAVEN-69008';
UPDATE tbl_order_detail t SET cost_pet = 1076, costo_after_vat=961 WHERE order_nr = '200075646' AND sku = 'LG689EL47KZOLAVEN-70764';
UPDATE tbl_order_detail t SET cost_pet = 2022, costo_after_vat=1805 WHERE order_nr = '200075846' AND sku = 'GA766EL73FEOLAVEN-66790';
UPDATE tbl_order_detail t SET cost_pet = 950, costo_after_vat=848 WHERE order_nr = '200075946' AND sku = 'CO826EL60JUHLAVEN-69952';
UPDATE tbl_order_detail t SET cost_pet = 5460, costo_after_vat=4875 WHERE order_nr = '200076646' AND sku = 'BL657EL55BBOLAVEN-63999';
UPDATE tbl_order_detail t SET cost_pet = 179, costo_after_vat=160 WHERE order_nr = '200076846' AND sku = 'XM850EL97IGKLAVEN-68915';
UPDATE tbl_order_detail t SET cost_pet = 783, costo_after_vat=699 WHERE order_nr = '200076946' AND sku = 'CA740HL27IBKLAVEN-68738';
UPDATE tbl_order_detail t SET cost_pet = 187, costo_after_vat=167 WHERE order_nr = '200077646' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET cost_pet = 187, costo_after_vat=167 WHERE order_nr = '200077646' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET cost_pet = 540, costo_after_vat=482 WHERE order_nr = '200077846' AND sku = 'TO912TB72LKDLAVEN-71074';
UPDATE tbl_order_detail t SET cost_pet = 351, costo_after_vat=313 WHERE order_nr = '200077846' AND sku = 'BA891TB47LDKLAVEN-70864';
UPDATE tbl_order_detail t SET cost_pet = 653, costo_after_vat=583 WHERE order_nr = '200077946' AND sku = 'FR860FA46IMFLAVEN-69066';
UPDATE tbl_order_detail t SET cost_pet = 893, costo_after_vat=798 WHERE order_nr = '200077946' AND sku = 'FR860FA17INILAVEN-69095';
UPDATE tbl_order_detail t SET cost_pet = 1446, costo_after_vat=1292 WHERE order_nr = '200078646' AND sku = 'SO722EL40KZVLAVEN-70771';
UPDATE tbl_order_detail t SET cost_pet = 330, costo_after_vat=295 WHERE order_nr = '200078846' AND sku = 'MI915TB62LKNLAVEN-71084';
UPDATE tbl_order_detail t SET cost_pet = 276, costo_after_vat=246 WHERE order_nr = '200078946' AND sku = 'CE900TB00LFFLAVEN-70911';
UPDATE tbl_order_detail t SET cost_pet = 247, costo_after_vat=221 WHERE order_nr = '200078946' AND sku = 'CA897TB08LEXLAVEN-70903';
UPDATE tbl_order_detail t SET cost_pet = 269, costo_after_vat=240 WHERE order_nr = '200079646' AND sku = 'CO748HL05INULAVEN-69107';
UPDATE tbl_order_detail t SET cost_pet = 3050, costo_after_vat=2723 WHERE order_nr = '200079846' AND sku = 'SA717EL60ATRLAVEN-63794';
UPDATE tbl_order_detail t SET cost_pet = 4892, costo_after_vat=4368 WHERE order_nr = '200079946' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET cost_pet = 362, costo_after_vat=323 WHERE order_nr = '200079946' AND sku = 'OS854EL29JRQLAVEN-69883';
UPDATE tbl_order_detail t SET cost_pet = 1098, costo_after_vat=980 WHERE order_nr = '200081352' AND sku = 'ST878EL98KIFLAVEN-70314';
UPDATE tbl_order_detail t SET cost_pet = 1198, costo_after_vat=1070 WHERE order_nr = '200081646' AND sku = 'OL750EL88DVJLAVEN-65871';
UPDATE tbl_order_detail t SET cost_pet = 172, costo_after_vat=154 WHERE order_nr = '200081846' AND sku = 'CA780HL16GDVLAVEN-67447';
UPDATE tbl_order_detail t SET cost_pet = 564, costo_after_vat=504 WHERE order_nr = '200081846' AND sku = 'PA708EL43KZSLAVEN-70768';
UPDATE tbl_order_detail t SET cost_pet = 742, costo_after_vat=663 WHERE order_nr = '200081846' AND sku = 'SO722EL57EICLAVEN-66205';
UPDATE tbl_order_detail t SET cost_pet = 5460, costo_after_vat=4875 WHERE order_nr = '200081846' AND sku = 'BL657EL55BBOLAVEN-63999';
UPDATE tbl_order_detail t SET cost_pet = 891, costo_after_vat=796 WHERE order_nr = '200081846' AND sku = 'SO722EL09LBALAVEN-70802';
UPDATE tbl_order_detail t SET cost_pet = 2661, costo_after_vat=2376 WHERE order_nr = '200081846' AND sku = 'NI704EL31NJSLAVEN-72415';
UPDATE tbl_order_detail t SET cost_pet = 2971, costo_after_vat=2653 WHERE order_nr = '200081846' AND sku = 'NI704EL20LAPLAVEN-70791';
UPDATE tbl_order_detail t SET cost_pet = 269, costo_after_vat=240 WHERE order_nr = '200081946' AND sku = 'CO748HL41DTILAVEN-65818';
UPDATE tbl_order_detail t SET cost_pet = 269, costo_after_vat=240 WHERE order_nr = '200081946' AND sku = 'CO748HL41DTILAVEN-65818';
UPDATE tbl_order_detail t SET cost_pet = 269, costo_after_vat=240 WHERE order_nr = '200081946' AND sku = 'CO748HL41DTILAVEN-65818';
UPDATE tbl_order_detail t SET cost_pet = 109, costo_after_vat=97 WHERE order_nr = '200082646' AND sku = 'OC741HL11DFCLAVEN-65452';
UPDATE tbl_order_detail t SET cost_pet = 123, costo_after_vat=110 WHERE order_nr = '200082646' AND sku = 'OC741HL17DEWLAVEN-65446';
UPDATE tbl_order_detail t SET cost_pet = 188, costo_after_vat=168 WHERE order_nr = '200082646' AND sku = 'PR822HL89HFULAVEN-68176';
UPDATE tbl_order_detail t SET cost_pet = 269, costo_after_vat=240 WHERE order_nr = '200082646' AND sku = 'CO748HL41DTILAVEN-65818';
UPDATE tbl_order_detail t SET cost_pet = 72, costo_after_vat=64 WHERE order_nr = '200082646' AND sku = 'PR856HL34IIVLAVEN-68978';
UPDATE tbl_order_detail t SET cost_pet = 9990, costo_after_vat=8920 WHERE order_nr = '200082946' AND sku = 'SA717EL36IITLAVEN-68976';
UPDATE tbl_order_detail t SET cost_pet = 408, costo_after_vat=364 WHERE order_nr = '200083246' AND sku = 'MA746HL38DLTLAVEN-65621';
UPDATE tbl_order_detail t SET cost_pet = 115, costo_after_vat=103 WHERE order_nr = '200083246' AND sku = 'MA746HL06DQVLAVEN-65753';
UPDATE tbl_order_detail t SET cost_pet = 115, costo_after_vat=103 WHERE order_nr = '200083246' AND sku = 'MA746HL06DQVLAVEN-65753';
UPDATE tbl_order_detail t SET cost_pet = 115, costo_after_vat=103 WHERE order_nr = '200083246' AND sku = 'MA746HL06DQVLAVEN-65753';
UPDATE tbl_order_detail t SET cost_pet = 115, costo_after_vat=103 WHERE order_nr = '200083246' AND sku = 'MA746HL06DQVLAVEN-65753';
UPDATE tbl_order_detail t SET cost_pet = 512, costo_after_vat=457 WHERE order_nr = '200083246' AND sku = 'MA746HL90DRLLAVEN-65769';
UPDATE tbl_order_detail t SET cost_pet = 5460, costo_after_vat=4875 WHERE order_nr = '200083646' AND sku = 'BL657EL33GDELAVEN-67430';
UPDATE tbl_order_detail t SET cost_pet = 2995, costo_after_vat=2674 WHERE order_nr = '200083646' AND sku = 'BL657EL59KRKLAVEN-70552';
UPDATE tbl_order_detail t SET cost_pet = 752, costo_after_vat=672 WHERE order_nr = '200084646' AND sku = 'CA740HL44DDVLAVEN-65419';
UPDATE tbl_order_detail t SET cost_pet = 2949, costo_after_vat=2633 WHERE order_nr = '200084946' AND sku = 'ZT736EL67CVGLAVEN-65197';
UPDATE tbl_order_detail t SET cost_pet = 123, costo_after_vat=110 WHERE order_nr = '200085646' AND sku = 'OC741HL15DEYLAVEN-65448';
UPDATE tbl_order_detail t SET cost_pet = 1473, costo_after_vat=1315 WHERE order_nr = '200085846' AND sku = 'CO826EL59JUILAVEN-69953';
UPDATE tbl_order_detail t SET cost_pet = 2995, costo_after_vat=2674 WHERE order_nr = '200085946' AND sku = 'BL657EL59KRKLAVEN-70552';
UPDATE tbl_order_detail t SET cost_pet = 2995, costo_after_vat=2674 WHERE order_nr = '200085946' AND sku = 'BL657EL37AMWLAVEN-63612';
UPDATE tbl_order_detail t SET cost_pet = 742, costo_after_vat=663 WHERE order_nr = '200086646' AND sku = 'SO722EL57EICLAVEN-66205';
UPDATE tbl_order_detail t SET cost_pet = 9990, costo_after_vat=8920 WHERE order_nr = '200086846' AND sku = 'SA717EL36IITLAVEN-68976';
UPDATE tbl_order_detail t SET cost_pet = 9990, costo_after_vat=8920 WHERE order_nr = '200086946' AND sku = 'SA717EL36IITLAVEN-68976';
UPDATE tbl_order_detail t SET cost_pet = 1294, costo_after_vat=1155 WHERE order_nr = '200087646' AND sku = 'CA740HL66DCZLAVEN-65397';
UPDATE tbl_order_detail t SET cost_pet = 4892, costo_after_vat=4368 WHERE order_nr = '200087846' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET cost_pet = 805, costo_after_vat=719 WHERE order_nr = '200087946' AND sku = 'CA740HL82DCJLAVEN-65381';
UPDATE tbl_order_detail t SET cost_pet = 362, costo_after_vat=323 WHERE order_nr = '200088646' AND sku = 'OS854EL29JRQLAVEN-69883';
UPDATE tbl_order_detail t SET cost_pet = 1733, costo_after_vat=1547 WHERE order_nr = '200088846' AND sku = 'CA740HL53DDMLAVEN-65410';
UPDATE tbl_order_detail t SET cost_pet = 2995, costo_after_vat=2674 WHERE order_nr = '200088946' AND sku = 'BL657EL59KRKLAVEN-70552';
UPDATE tbl_order_detail t SET cost_pet = 362, costo_after_vat=323 WHERE order_nr = '200089646' AND sku = 'OS854EL29JRQLAVEN-69883';
UPDATE tbl_order_detail t SET cost_pet = 362, costo_after_vat=323 WHERE order_nr = '200089646' AND sku = 'OS854EL29JRQLAVEN-69883';
UPDATE tbl_order_detail t SET cost_pet = 1999, costo_after_vat=1785 WHERE order_nr = '200089846' AND sku = 'LG689EL16JVZLAVEN-69996';
UPDATE tbl_order_detail t SET cost_pet = 1033, costo_after_vat=922 WHERE order_nr = '200089946' AND sku = 'CA740HL70DCVLAVEN-65393';
UPDATE tbl_order_detail t SET cost_pet = 805, costo_after_vat=719 WHERE order_nr = '200089946' AND sku = 'CA740HL83DCILAVEN-65380';
UPDATE tbl_order_detail t SET cost_pet = 1098, costo_after_vat=980 WHERE order_nr = '200091352' AND sku = 'ST878EL98KIFLAVEN-70314';
UPDATE tbl_order_detail t SET cost_pet = 8075, costo_after_vat=7210 WHERE order_nr = '200091646' AND sku = 'SA717EL29ANELAVEN-63620';
UPDATE tbl_order_detail t SET cost_pet = 344, costo_after_vat=307 WHERE order_nr = '200091846' AND sku = 'TO912TB70LKFLAVEN-71076';
UPDATE tbl_order_detail t SET cost_pet = 4892, costo_after_vat=4368 WHERE order_nr = '200091846' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET cost_pet = 74, costo_after_vat=66 WHERE order_nr = '200091846' AND sku = 'AD731EL92OELLAVEN-72954';
UPDATE tbl_order_detail t SET cost_pet = 94, costo_after_vat=84 WHERE order_nr = '200091946' AND sku = 'PR822HL00HFJLAVEN-68165';
UPDATE tbl_order_detail t SET cost_pet = 2410, costo_after_vat=2152 WHERE order_nr = '200091952' AND sku = 'BL657EL41AMSLAVEN-63608';
UPDATE tbl_order_detail t SET cost_pet = 246, costo_after_vat=220 WHERE order_nr = '200092646' AND sku = 'OS854EL00IKDLAVEN-69012';
UPDATE tbl_order_detail t SET cost_pet = 1200, costo_after_vat=1071 WHERE order_nr = '200092946' AND sku = 'SA717EL10IFXLAVEN-68902';
UPDATE tbl_order_detail t SET cost_pet = 926, costo_after_vat=827 WHERE order_nr = '200092946' AND sku = 'NO758HL64EHVLAVEN-66198';
UPDATE tbl_order_detail t SET cost_pet = 285, costo_after_vat=254 WHERE order_nr = '200093246' AND sku = 'MA746HL69DKOLAVEN-65590';
UPDATE tbl_order_detail t SET cost_pet = 8075, costo_after_vat=7210 WHERE order_nr = '200093646' AND sku = 'SA717EL62ATPLAVEN-63792';
UPDATE tbl_order_detail t SET cost_pet = 2995, costo_after_vat=2674 WHERE order_nr = '200093946' AND sku = 'BL657EL59KRKLAVEN-70552';
UPDATE tbl_order_detail t SET cost_pet = 123, costo_after_vat=110 WHERE order_nr = '200094646' AND sku = 'OC741HL14DEZLAVEN-65449';
UPDATE tbl_order_detail t SET cost_pet = 445, costo_after_vat=397 WHERE order_nr = '200094946' AND sku = 'ZT736EL71CVCLAVEN-65193';
UPDATE tbl_order_detail t SET cost_pet = 1660, costo_after_vat=1483 WHERE order_nr = '200094946' AND sku = 'CA740HL46DDTLAVEN-65417';
UPDATE tbl_order_detail t SET cost_pet = 269, costo_after_vat=240 WHERE order_nr = '200095646' AND sku = 'CO748HL44DTFLAVEN-65815';
UPDATE tbl_order_detail t SET cost_pet = 269, costo_after_vat=240 WHERE order_nr = '200095646' AND sku = 'CO748HL46DTDLAVEN-65813';
UPDATE tbl_order_detail t SET cost_pet = 2995, costo_after_vat=2674 WHERE order_nr = '200095846' AND sku = 'BL657EL37AMWLAVEN-63612';
UPDATE tbl_order_detail t SET cost_pet = 2022, costo_after_vat=1805 WHERE order_nr = '200095946' AND sku = 'GA766EL73FEOLAVEN-66790';
UPDATE tbl_order_detail t SET cost_pet = 86, costo_after_vat=77 WHERE order_nr = '200096452' AND sku = 'OC741HL12DFBLAVEN-65451';
UPDATE tbl_order_detail t SET cost_pet = 121, costo_after_vat=108 WHERE order_nr = '200096646' AND sku = 'MI911TB76LJZLAVEN-71070';
UPDATE tbl_order_detail t SET cost_pet = 1999, costo_after_vat=1785 WHERE order_nr = '200096846' AND sku = 'LG689EL16JVZLAVEN-69996';
UPDATE tbl_order_detail t SET cost_pet = 4892, costo_after_vat=4368 WHERE order_nr = '200096946' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET cost_pet = 2995, costo_after_vat=2674 WHERE order_nr = '200096946' AND sku = 'BL657EL37AMWLAVEN-63612';
UPDATE tbl_order_detail t SET cost_pet = 316, costo_after_vat=282 WHERE order_nr = '200097646' AND sku = 'MA746HL92DRJLAVEN-65767';
UPDATE tbl_order_detail t SET cost_pet = 2995, costo_after_vat=2674 WHERE order_nr = '200097846' AND sku = 'BL657EL59KRKLAVEN-70552';
UPDATE tbl_order_detail t SET cost_pet = 2022, costo_after_vat=1805 WHERE order_nr = '200097946' AND sku = 'GA766EL73FEOLAVEN-66790';
UPDATE tbl_order_detail t SET cost_pet = 496, costo_after_vat=443 WHERE order_nr = '200098646' AND sku = 'OS854EL96IKHLAVEN-69016';
UPDATE tbl_order_detail t SET cost_pet = 1042, costo_after_vat=930 WHERE order_nr = '200098646' AND sku = 'PR856HL25IJELAVEN-68987';
UPDATE tbl_order_detail t SET cost_pet = 594, costo_after_vat=530 WHERE order_nr = '200098846' AND sku = 'NI704EL19LAQLAVEN-70792';
UPDATE tbl_order_detail t SET cost_pet = 5478, costo_after_vat=4891 WHERE order_nr = '200098946' AND sku = 'SO722EL14LAVLAVEN-70797';
UPDATE tbl_order_detail t SET cost_pet = 1446, costo_after_vat=1292 WHERE order_nr = '200099646' AND sku = 'SO722EL40KZVLAVEN-70771';
UPDATE tbl_order_detail t SET cost_pet = 8075, costo_after_vat=7210 WHERE order_nr = '200099846' AND sku = 'SA717EL29ANELAVEN-63620';
UPDATE tbl_order_detail t SET cost_pet = 926, costo_after_vat=827 WHERE order_nr = '200099946' AND sku = 'NO758HL64EHVLAVEN-66198';

#Precios
UPDATE tbl_order_detail t SET unit_price = 5570, unit_price_after_vat=4973, shipping_fee = 0, paid_price = 5570, paid_price_after_vat = 4973 WHERE order_nr = '200011246' AND sku = 'BL657EL55BBOLAVEN-63999';
UPDATE tbl_order_detail t SET unit_price = 8237, unit_price_after_vat=7354, shipping_fee = 0, paid_price = 8237, paid_price_after_vat = 7354 WHERE order_nr = '200011646' AND sku = 'SA717EL28KDFLAVEN-70184';
UPDATE tbl_order_detail t SET unit_price = 267, unit_price_after_vat=238, shipping_fee = 0, paid_price = 267, paid_price_after_vat = 238 WHERE order_nr = '200011946' AND sku = 'LA857EL82IKVLAVEN-69030';
UPDATE tbl_order_detail t SET unit_price = 1506, unit_price_after_vat=1345, shipping_fee = 0, paid_price = 1506, paid_price_after_vat = 1345 WHERE order_nr = '200011946' AND sku = 'SO722EL40KZVLAVEN-70771';
UPDATE tbl_order_detail t SET unit_price = 482, unit_price_after_vat=430, shipping_fee = 0, paid_price = 482, paid_price_after_vat = 430 WHERE order_nr = '200012646' AND sku = 'OS854EL95IKILAVEN-69017';
UPDATE tbl_order_detail t SET unit_price = 1137, unit_price_after_vat=1015, shipping_fee = 0, paid_price = 1137, paid_price_after_vat = 1015 WHERE order_nr = '200012846' AND sku = 'CA740HL71DCULAVEN-65392';
UPDATE tbl_order_detail t SET unit_price = 743, unit_price_after_vat=663, shipping_fee = 0, paid_price = 743, paid_price_after_vat = 663 WHERE order_nr = '200012946' AND sku = 'TO912TB72LKDLAVEN-71074';
UPDATE tbl_order_detail t SET unit_price = 861, unit_price_after_vat=769, shipping_fee = 0, paid_price = 861, paid_price_after_vat = 769 WHERE order_nr = '200012946' AND sku = 'CA740HL26IBLLAVEN-68739';
UPDATE tbl_order_detail t SET unit_price = 482, unit_price_after_vat=430, shipping_fee = 0, paid_price = 482, paid_price_after_vat = 430 WHERE order_nr = '200013246' AND sku = 'OS854EL95IKILAVEN-69017';
UPDATE tbl_order_detail t SET unit_price = 8237, unit_price_after_vat=7354, shipping_fee = 0, paid_price = 8237, paid_price_after_vat = 7354 WHERE order_nr = '200013646' AND sku = 'SA717EL62ATPLAVEN-63792';
UPDATE tbl_order_detail t SET unit_price = 8237, unit_price_after_vat=7354, shipping_fee = 0, paid_price = 8237, paid_price_after_vat = 7354 WHERE order_nr = '200013646' AND sku = 'SA717EL02GMBLAVEN-67661';
UPDATE tbl_order_detail t SET unit_price = 850, unit_price_after_vat=759, shipping_fee = 0, paid_price = 850, paid_price_after_vat = 759 WHERE order_nr = '200014646' AND sku = 'SO722EL57EICLAVEN-66205';
UPDATE tbl_order_detail t SET unit_price = 267, unit_price_after_vat=238, shipping_fee = 0, paid_price = 267, paid_price_after_vat = 238 WHERE order_nr = '200014946' AND sku = 'LA857EL82IKVLAVEN-69030';
UPDATE tbl_order_detail t SET unit_price = 261, unit_price_after_vat=233, shipping_fee = 0, paid_price = 261, paid_price_after_vat = 233 WHERE order_nr = '200014946' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET unit_price = 261, unit_price_after_vat=233, shipping_fee = 0, paid_price = 261, paid_price_after_vat = 233 WHERE order_nr = '200014946' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET unit_price = 8237, unit_price_after_vat=7354, shipping_fee = 0, paid_price = 8237, paid_price_after_vat = 7354 WHERE order_nr = '200015646' AND sku = 'SA717EL02GMBLAVEN-67661';
UPDATE tbl_order_detail t SET unit_price = 698, unit_price_after_vat=623, shipping_fee = 0, paid_price = 698, paid_price_after_vat = 623 WHERE order_nr = '200015846' AND sku = 'SO722EL77CBQLAVEN-64687';
UPDATE tbl_order_detail t SET unit_price = 197, unit_price_after_vat=176, shipping_fee = 0, paid_price = 197, paid_price_after_vat = 176 WHERE order_nr = '200015846' AND sku = 'DI899TB04LFBLAVEN-70907';
UPDATE tbl_order_detail t SET unit_price = 220, unit_price_after_vat=196, shipping_fee = 0, paid_price = 220, paid_price_after_vat = 196 WHERE order_nr = '200015846' AND sku = 'HA894TB28LEDLAVEN-70883';
UPDATE tbl_order_detail t SET unit_price = 277, unit_price_after_vat=247, shipping_fee = 0, paid_price = 277, paid_price_after_vat = 247 WHERE order_nr = '200015946' AND sku = 'DI905TB80LJVLAVEN-71066';
UPDATE tbl_order_detail t SET unit_price = 152, unit_price_after_vat=136, shipping_fee = 0, paid_price = 152, paid_price_after_vat = 136 WHERE order_nr = '200015946' AND sku = 'HA894TB37LDULAVEN-70874';
UPDATE tbl_order_detail t SET unit_price = 167, unit_price_after_vat=149, shipping_fee = 0, paid_price = 167, paid_price_after_vat = 149 WHERE order_nr = '200015946' AND sku = 'TO912TB71LKELAVEN-71075';
UPDATE tbl_order_detail t SET unit_price = 189, unit_price_after_vat=169, shipping_fee = 0, paid_price = 189, paid_price_after_vat = 169 WHERE order_nr = '200015946' AND sku = 'HA894TB39LDSLAVEN-70872';
UPDATE tbl_order_detail t SET unit_price = 5570, unit_price_after_vat=4973, shipping_fee = 0, paid_price = 5570, paid_price_after_vat = 4973 WHERE order_nr = '200016646' AND sku = 'BL657EL55BBOLAVEN-63999';
UPDATE tbl_order_detail t SET unit_price = 541, unit_price_after_vat=483, shipping_fee = 0, paid_price = 541, paid_price_after_vat = 483 WHERE order_nr = '200016846' AND sku = 'CA740HL80DCLLAVEN-65383';
UPDATE tbl_order_detail t SET unit_price = 423, unit_price_after_vat=378, shipping_fee = 0, paid_price = 423, paid_price_after_vat = 378 WHERE order_nr = '200016946' AND sku = 'OS854EL93IKKLAVEN-69019';
UPDATE tbl_order_detail t SET unit_price = 313, unit_price_after_vat=279, shipping_fee = 0, paid_price = 313, paid_price_after_vat = 279 WHERE order_nr = '200017646' AND sku = 'SO722EL33LACLAVEN-70778';
UPDATE tbl_order_detail t SET unit_price = 5081, unit_price_after_vat=4537, shipping_fee = 0, paid_price = 5081, paid_price_after_vat = 4537 WHERE order_nr = '200017846' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET unit_price = 437, unit_price_after_vat=390, shipping_fee = 0, paid_price = 437, paid_price_after_vat = 390 WHERE order_nr = '200017946' AND sku = 'NO758HL61EHYLAVEN-66201';
UPDATE tbl_order_detail t SET unit_price = 286, unit_price_after_vat=255, shipping_fee = 0, paid_price = 286, paid_price_after_vat = 255 WHERE order_nr = '200018646' AND sku = 'DI899TB05LFALAVEN-70906';
UPDATE tbl_order_detail t SET unit_price = 169, unit_price_after_vat=151, shipping_fee = 0, paid_price = 169, paid_price_after_vat = 151 WHERE order_nr = '200018646' AND sku = 'CA897TB11LEULAVEN-70900';
UPDATE tbl_order_detail t SET unit_price = 5081, unit_price_after_vat=4537, shipping_fee = 0, paid_price = 5081, paid_price_after_vat = 4537 WHERE order_nr = '200018846' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET unit_price = 3055, unit_price_after_vat=2728, shipping_fee = 0, paid_price = 3055, paid_price_after_vat = 2728 WHERE order_nr = '200018946' AND sku = 'BL657EL37AMWLAVEN-63612';
UPDATE tbl_order_detail t SET unit_price = 1424, unit_price_after_vat=1271, shipping_fee = 0, paid_price = 1424, paid_price_after_vat = 1271 WHERE order_nr = '200019646' AND sku = 'CA740HL65DDALAVEN-65398';
UPDATE tbl_order_detail t SET unit_price = 610, unit_price_after_vat=545, shipping_fee = 0, paid_price = 610, paid_price_after_vat = 545 WHERE order_nr = '200019846' AND sku = 'CA740HL64DDBLAVEN-65399';
UPDATE tbl_order_detail t SET unit_price = 798, unit_price_after_vat=712, shipping_fee = 0, paid_price = 798, paid_price_after_vat = 712 WHERE order_nr = '200019946' AND sku = 'CA740HL73DCSLAVEN-65390';
UPDATE tbl_order_detail t SET unit_price = 1326, unit_price_after_vat=1184, shipping_fee = 0, paid_price = 1326, paid_price_after_vat = 1184 WHERE order_nr = '200019946' AND sku = 'SA717EL10IFXLAVEN-68902';
UPDATE tbl_order_detail t SET unit_price = 2063, unit_price_after_vat=1842, shipping_fee = 0, paid_price = 2063, paid_price_after_vat = 1842 WHERE order_nr = '200021646' AND sku = 'GA766EL73FEOLAVEN-66790';
UPDATE tbl_order_detail t SET unit_price = 568, unit_price_after_vat=507, shipping_fee = 0, paid_price = 568, paid_price_after_vat = 507 WHERE order_nr = '200021846' AND sku = 'MO917TB58LKRLAVEN-71088';
UPDATE tbl_order_detail t SET unit_price = 906, unit_price_after_vat=809, shipping_fee = 0, paid_price = 906, paid_price_after_vat = 809 WHERE order_nr = '200021946' AND sku = 'CA740HL76DCPLAVEN-65387';
UPDATE tbl_order_detail t SET unit_price = 1137, unit_price_after_vat=1015, shipping_fee = 0, paid_price = 1137, paid_price_after_vat = 1015 WHERE order_nr = '200021946' AND sku = 'CA740HL71DCULAVEN-65392';
UPDATE tbl_order_detail t SET unit_price = 666, unit_price_after_vat=595, shipping_fee = 0, paid_price = 666, paid_price_after_vat = 595 WHERE order_nr = '200021946' AND sku = 'CA740HL32IBFLAVEN-68733';
UPDATE tbl_order_detail t SET unit_price = 351, unit_price_after_vat=313, shipping_fee = 0, paid_price = 351, paid_price_after_vat = 313 WHERE order_nr = '200022646' AND sku = 'CO748HL41DTILAVEN-65818';
UPDATE tbl_order_detail t SET unit_price = 267, unit_price_after_vat=238, shipping_fee = 0, paid_price = 267, paid_price_after_vat = 238 WHERE order_nr = '200022946' AND sku = 'LA857EL82IKVLAVEN-69030';
UPDATE tbl_order_detail t SET unit_price = 358, unit_price_after_vat=320, shipping_fee = 0, paid_price = 358, paid_price_after_vat = 320 WHERE order_nr = '200022946' AND sku = 'OS854EL29JRQLAVEN-69883';
UPDATE tbl_order_detail t SET unit_price = 369, unit_price_after_vat=329, shipping_fee = 0, paid_price = 369, paid_price_after_vat = 329 WHERE order_nr = '200023246' AND sku = 'MA746HL69DKOLAVEN-65590';
UPDATE tbl_order_detail t SET unit_price = 369, unit_price_after_vat=329, shipping_fee = 0, paid_price = 369, paid_price_after_vat = 329 WHERE order_nr = '200023246' AND sku = 'MA746HL69DKOLAVEN-65590';
UPDATE tbl_order_detail t SET unit_price = 4869, unit_price_after_vat=4347, shipping_fee = 0, paid_price = 4869, paid_price_after_vat = 4347 WHERE order_nr = '200023646' AND sku = 'PA708EL81HNULAVEN-68384';
UPDATE tbl_order_detail t SET unit_price = 2272, unit_price_after_vat=2029, shipping_fee = 0, paid_price = 2272, paid_price_after_vat = 2029 WHERE order_nr = '200023946' AND sku = 'CA740HL51DDOLAVEN-65412';
UPDATE tbl_order_detail t SET unit_price = 1542, unit_price_after_vat=1377, shipping_fee = 0, paid_price = 1542, paid_price_after_vat = 1377 WHERE order_nr = '200023946' AND sku = 'CA740HL24IBNLAVEN-68741';
UPDATE tbl_order_detail t SET unit_price = 291, unit_price_after_vat=260, shipping_fee = 0, paid_price = 291, paid_price_after_vat = 260 WHERE order_nr = '200023946' AND sku = 'OS854EL04IJZLAVEN-69008';
UPDATE tbl_order_detail t SET unit_price = 221, unit_price_after_vat=197, shipping_fee = 0, paid_price = 221, paid_price_after_vat = 197 WHERE order_nr = '200024646' AND sku = 'CA780HL19GDSLAVEN-67444';
UPDATE tbl_order_detail t SET unit_price = 245, unit_price_after_vat=219, shipping_fee = 0, paid_price = 245, paid_price_after_vat = 219 WHERE order_nr = '200024646' AND sku = 'CA780HL15GDWLAVEN-67448';
UPDATE tbl_order_detail t SET unit_price = 156, unit_price_after_vat=139, shipping_fee = 0, paid_price = 156, paid_price_after_vat = 139 WHERE order_nr = '200024646' AND sku = 'PH898TB07LEYLAVEN-70904';
UPDATE tbl_order_detail t SET unit_price = 10190, unit_price_after_vat=9098, shipping_fee = 0, paid_price = 10190, paid_price_after_vat = 9098 WHERE order_nr = '200024946' AND sku = 'SA717EL36IITLAVEN-68976';
UPDATE tbl_order_detail t SET unit_price = 167, unit_price_after_vat=149, shipping_fee = 0, paid_price = 167, paid_price_after_vat = 149 WHERE order_nr = '200025646' AND sku = 'TO912TB71LKELAVEN-71075';
UPDATE tbl_order_detail t SET unit_price = 560, unit_price_after_vat=500, shipping_fee = 0, paid_price = 560, paid_price_after_vat = 500 WHERE order_nr = '200025646' AND sku = 'OS854EL96IKHLAVEN-69016';
UPDATE tbl_order_detail t SET unit_price = 8237, unit_price_after_vat=7354, shipping_fee = 0, paid_price = 8237, paid_price_after_vat = 7354 WHERE order_nr = '200025846' AND sku = 'SA717EL28KDFLAVEN-70184';
UPDATE tbl_order_detail t SET unit_price = 74, unit_price_after_vat=66, shipping_fee = 0, paid_price = 74, paid_price_after_vat = 66 WHERE order_nr = '200025946' AND sku = 'AD731EL10GEBLAVEN-67453';
UPDATE tbl_order_detail t SET unit_price = 423, unit_price_after_vat=378, shipping_fee = 0, paid_price = 423, paid_price_after_vat = 378 WHERE order_nr = '200025946' AND sku = 'OS854EL93IKKLAVEN-69019';
UPDATE tbl_order_detail t SET unit_price = 1326, unit_price_after_vat=1184, shipping_fee = 0, paid_price = 1326, paid_price_after_vat = 1184 WHERE order_nr = '200026646' AND sku = 'SA717EL10IFXLAVEN-68902';
UPDATE tbl_order_detail t SET unit_price = 3008, unit_price_after_vat=2686, shipping_fee = 0, paid_price = 3008, paid_price_after_vat = 2686 WHERE order_nr = '200026846' AND sku = 'ZT736EL67CVGLAVEN-65197';
UPDATE tbl_order_detail t SET unit_price = 416, unit_price_after_vat=371, shipping_fee = 0, paid_price = 416, paid_price_after_vat = 371 WHERE order_nr = '200026846' AND sku = 'MO903TB91LFOLAVEN-70920';
UPDATE tbl_order_detail t SET unit_price = 416, unit_price_after_vat=371, shipping_fee = 0, paid_price = 416, paid_price_after_vat = 371 WHERE order_nr = '200026846' AND sku = 'MO903TB91LFOLAVEN-70920';
UPDATE tbl_order_detail t SET unit_price = 875, unit_price_after_vat=781, shipping_fee = 0, paid_price = 875, paid_price_after_vat = 781 WHERE order_nr = '200026846' AND sku = 'PR822HL91HFSLAVEN-68174';
UPDATE tbl_order_detail t SET unit_price = 262, unit_price_after_vat=234, shipping_fee = 0, paid_price = 262, paid_price_after_vat = 234 WHERE order_nr = '200026846' AND sku = 'PR822HL89HFULAVEN-68176';
UPDATE tbl_order_detail t SET unit_price = 2272, unit_price_after_vat=2029, shipping_fee = 0, paid_price = 2272, paid_price_after_vat = 2029 WHERE order_nr = '200026946' AND sku = 'CA740HL51DDOLAVEN-65412';
UPDATE tbl_order_detail t SET unit_price = 277, unit_price_after_vat=247, shipping_fee = 0, paid_price = 277, paid_price_after_vat = 247 WHERE order_nr = '200027646' AND sku = 'DI905TB80LJVLAVEN-71066';
UPDATE tbl_order_detail t SET unit_price = 189, unit_price_after_vat=169, shipping_fee = 0, paid_price = 189, paid_price_after_vat = 169 WHERE order_nr = '200027646' AND sku = 'SP896TB14LERLAVEN-70897';
UPDATE tbl_order_detail t SET unit_price = 1326, unit_price_after_vat=1184, shipping_fee = 0, paid_price = 1326, paid_price_after_vat = 1184 WHERE order_nr = '200027846' AND sku = 'CA740HL48DDRLAVEN-65415';
UPDATE tbl_order_detail t SET unit_price = 5081, unit_price_after_vat=4537, shipping_fee = 0, paid_price = 5081, paid_price_after_vat = 4537 WHERE order_nr = '200027946' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET unit_price = 358, unit_price_after_vat=320, shipping_fee = 0, paid_price = 358, paid_price_after_vat = 320 WHERE order_nr = '200028646' AND sku = 'OS854EL29JRQLAVEN-69883';
UPDATE tbl_order_detail t SET unit_price = 2039, unit_price_after_vat=1821, shipping_fee = 0, paid_price = 2039, paid_price_after_vat = 1821 WHERE order_nr = '200028846' AND sku = 'LG689EL16JVZLAVEN-69996';
UPDATE tbl_order_detail t SET unit_price = 2739, unit_price_after_vat=2446, shipping_fee = 0, paid_price = 2739, paid_price_after_vat = 2446 WHERE order_nr = '200028946' AND sku = 'CO826EL67JUALAVEN-69945';
UPDATE tbl_order_detail t SET unit_price = 3764, unit_price_after_vat=3361, shipping_fee = 0, paid_price = 3764, paid_price_after_vat = 3361 WHERE order_nr = '200028946' AND sku = 'SO722EL34LABLAVEN-70777';
UPDATE tbl_order_detail t SET unit_price = 850, unit_price_after_vat=759, shipping_fee = 0, paid_price = 850, paid_price_after_vat = 759 WHERE order_nr = '200029646' AND sku = 'SO722EL57EICLAVEN-66205';
UPDATE tbl_order_detail t SET unit_price = 8237, unit_price_after_vat=7354, shipping_fee = 0, paid_price = 8237, paid_price_after_vat = 7354 WHERE order_nr = '200029846' AND sku = 'SA717EL29ANELAVEN-63620';
UPDATE tbl_order_detail t SET unit_price = 610, unit_price_after_vat=545, shipping_fee = 0, paid_price = 610, paid_price_after_vat = 545 WHERE order_nr = '200029946' AND sku = 'CA740HL63DDCLAVEN-65400';
UPDATE tbl_order_detail t SET unit_price = 1137, unit_price_after_vat=1015, shipping_fee = 0, paid_price = 1137, paid_price_after_vat = 1015 WHERE order_nr = '200029946' AND sku = 'CA740HL72DCTLAVEN-65391';
UPDATE tbl_order_detail t SET unit_price = 541, unit_price_after_vat=483, shipping_fee = 0, paid_price = 541, paid_price_after_vat = 483 WHERE order_nr = '200029946' AND sku = 'CA740HL81DCKLAVEN-65382';
UPDATE tbl_order_detail t SET unit_price = 473, unit_price_after_vat=422, shipping_fee = 0, paid_price = 473, paid_price_after_vat = 422 WHERE order_nr = '200031246' AND sku = 'TO912TB70LKFLAVEN-71076';
UPDATE tbl_order_detail t SET unit_price = 473, unit_price_after_vat=422, shipping_fee = 0, paid_price = 473, paid_price_after_vat = 422 WHERE order_nr = '200031246' AND sku = 'TO912TB70LKFLAVEN-71076';
UPDATE tbl_order_detail t SET unit_price = 696, unit_price_after_vat=621, shipping_fee = 0, paid_price = 696, paid_price_after_vat = 621 WHERE order_nr = '200031646' AND sku = 'CO826EL45JUWLAVEN-69967';
UPDATE tbl_order_detail t SET unit_price = 609, unit_price_after_vat=544, shipping_fee = 0, paid_price = 609, paid_price_after_vat = 544 WHERE order_nr = '200031946' AND sku = 'CA740HL43DDWLAVEN-65420';
UPDATE tbl_order_detail t SET unit_price = 1326, unit_price_after_vat=1184, shipping_fee = 0, paid_price = 1326, paid_price_after_vat = 1184 WHERE order_nr = '200031946' AND sku = 'CA740HL49DDQLAVEN-65414';
UPDATE tbl_order_detail t SET unit_price = 975, unit_price_after_vat=871, shipping_fee = 0, paid_price = 975, paid_price_after_vat = 871 WHERE order_nr = '200031946' AND sku = 'CA740HL42DDXLAVEN-65421';
UPDATE tbl_order_detail t SET unit_price = 184, unit_price_after_vat=164, shipping_fee = 0, paid_price = 184, paid_price_after_vat = 164 WHERE order_nr = '200032646' AND sku = 'OC741HL12DFBLAVEN-65451';
UPDATE tbl_order_detail t SET unit_price = 191, unit_price_after_vat=171, shipping_fee = 0, paid_price = 191, paid_price_after_vat = 171 WHERE order_nr = '200032646' AND sku = 'OC741HL16DEXLAVEN-65447';
UPDATE tbl_order_detail t SET unit_price = 4222, unit_price_after_vat=3770, shipping_fee = 0, paid_price = 4222, paid_price_after_vat = 3770 WHERE order_nr = '200032646' AND sku = 'SA717EL43JRCLAVEN-69869';
UPDATE tbl_order_detail t SET unit_price = 8237, unit_price_after_vat=7354, shipping_fee = 0, paid_price = 8237, paid_price_after_vat = 7354 WHERE order_nr = '200032846' AND sku = 'SA717EL28KDFLAVEN-70184';
UPDATE tbl_order_detail t SET unit_price = 541, unit_price_after_vat=483, shipping_fee = 0, paid_price = 541, paid_price_after_vat = 483 WHERE order_nr = '200032946' AND sku = 'CA740HL80DCLLAVEN-65383';
UPDATE tbl_order_detail t SET unit_price = 469, unit_price_after_vat=419, shipping_fee = 0, paid_price = 469, paid_price_after_vat = 419 WHERE order_nr = '200033246' AND sku = 'BI873EL26JRTLAVEN-69886';
UPDATE tbl_order_detail t SET unit_price = 8237, unit_price_after_vat=7354, shipping_fee = 0, paid_price = 8237, paid_price_after_vat = 7354 WHERE order_nr = '200033646' AND sku = 'SA717EL29ANELAVEN-63620';
UPDATE tbl_order_detail t SET unit_price = 506, unit_price_after_vat=452, shipping_fee = 0, paid_price = 506, paid_price_after_vat = 452 WHERE order_nr = '200034646' AND sku = 'OS854EL11IJSLAVEN-69001';
UPDATE tbl_order_detail t SET unit_price = 252, unit_price_after_vat=225, shipping_fee = 0, paid_price = 252, paid_price_after_vat = 225 WHERE order_nr = '200034646' AND sku = 'MA914TB65LKKLAVEN-71081';
UPDATE tbl_order_detail t SET unit_price = 313, unit_price_after_vat=279, shipping_fee = 0, paid_price = 313, paid_price_after_vat = 279 WHERE order_nr = '200034646' AND sku = 'SO722EL33LACLAVEN-70778';
UPDATE tbl_order_detail t SET unit_price = 3968, unit_price_after_vat=3543, shipping_fee = 0, paid_price = 3968, paid_price_after_vat = 3543 WHERE order_nr = '200034946' AND sku = 'BL657EL36AMXLAVEN-63613';
UPDATE tbl_order_detail t SET unit_price = 207, unit_price_after_vat=185, shipping_fee = 0, paid_price = 207, paid_price_after_vat = 185 WHERE order_nr = '200035646' AND sku = 'MI911TB77LJYLAVEN-71069';
UPDATE tbl_order_detail t SET unit_price = 473, unit_price_after_vat=422, shipping_fee = 0, paid_price = 473, paid_price_after_vat = 422 WHERE order_nr = '200035646' AND sku = 'TO912TB70LKFLAVEN-71076';
UPDATE tbl_order_detail t SET unit_price = 875, unit_price_after_vat=781, shipping_fee = 0, paid_price = 875, paid_price_after_vat = 781 WHERE order_nr = '200035646' AND sku = 'PR822HL91HFSLAVEN-68174';
UPDATE tbl_order_detail t SET unit_price = 998, unit_price_after_vat=891, shipping_fee = 0, paid_price = 998, paid_price_after_vat = 891 WHERE order_nr = '200035846' AND sku = 'CO826EL60JUHLAVEN-69952';
UPDATE tbl_order_detail t SET unit_price = 189, unit_price_after_vat=169, shipping_fee = 0, paid_price = 189, paid_price_after_vat = 169 WHERE order_nr = '200035846' AND sku = 'HA894TB39LDSLAVEN-70872';
UPDATE tbl_order_detail t SET unit_price = 482, unit_price_after_vat=430, shipping_fee = 0, paid_price = 482, paid_price_after_vat = 430 WHERE order_nr = '200035946' AND sku = 'OS854EL95IKILAVEN-69017';
UPDATE tbl_order_detail t SET unit_price = 77, unit_price_after_vat=69, shipping_fee = 0, paid_price = 77, paid_price_after_vat = 69 WHERE order_nr = '200036646' AND sku = 'PU858EL66ILLLAVEN-69046';
UPDATE tbl_order_detail t SET unit_price = 190, unit_price_after_vat=170, shipping_fee = 0, paid_price = 190, paid_price_after_vat = 170 WHERE order_nr = '200036846' AND sku = 'XM850EL97IGKLAVEN-68915';
UPDATE tbl_order_detail t SET unit_price = 2951, unit_price_after_vat=2635, shipping_fee = 0, paid_price = 2951, paid_price_after_vat = 2635 WHERE order_nr = '200036946' AND sku = 'CA740HL50DDPLAVEN-65413';
UPDATE tbl_order_detail t SET unit_price = 286, unit_price_after_vat=255, shipping_fee = 0, paid_price = 286, paid_price_after_vat = 255 WHERE order_nr = '200037646' AND sku = 'DI899TB05LFALAVEN-70906';
UPDATE tbl_order_detail t SET unit_price = 5081, unit_price_after_vat=4537, shipping_fee = 0, paid_price = 5081, paid_price_after_vat = 4537 WHERE order_nr = '200037846' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET unit_price = 625, unit_price_after_vat=558, shipping_fee = 0, paid_price = 625, paid_price_after_vat = 558 WHERE order_nr = '200037946' AND sku = 'NI704EL74CYVLAVEN-65289';
UPDATE tbl_order_detail t SET unit_price = 625, unit_price_after_vat=558, shipping_fee = 0, paid_price = 625, paid_price_after_vat = 558 WHERE order_nr = '200037946' AND sku = 'NI704EL19LAQLAVEN-70792';
UPDATE tbl_order_detail t SET unit_price = 864, unit_price_after_vat=771, shipping_fee = 0, paid_price = 864, paid_price_after_vat = 771 WHERE order_nr = '200037946' AND sku = 'CA740HL29IBILAVEN-68736';
UPDATE tbl_order_detail t SET unit_price = 1326, unit_price_after_vat=1184, shipping_fee = 0, paid_price = 1326, paid_price_after_vat = 1184 WHERE order_nr = '200038646' AND sku = 'SA717EL10IFXLAVEN-68902';
UPDATE tbl_order_detail t SET unit_price = 1271, unit_price_after_vat=1135, shipping_fee = 0, paid_price = 1271, paid_price_after_vat = 1135 WHERE order_nr = '200038846' AND sku = 'OL750EL85DVMLAVEN-65874';
UPDATE tbl_order_detail t SET unit_price = 1548, unit_price_after_vat=1382, shipping_fee = 0, paid_price = 1548, paid_price_after_vat = 1382 WHERE order_nr = '200038946' AND sku = 'CO826EL59JUILAVEN-69953';
UPDATE tbl_order_detail t SET unit_price = 5570, unit_price_after_vat=4973, shipping_fee = 0, paid_price = 5570, paid_price_after_vat = 4973 WHERE order_nr = '200039646' AND sku = 'BL657EL55BBOLAVEN-63999';
UPDATE tbl_order_detail t SET unit_price = 2289, unit_price_after_vat=2044, shipping_fee = 0, paid_price = 2289, paid_price_after_vat = 2044 WHERE order_nr = '200039846' AND sku = 'PA708EL84HNRLAVEN-68381';
UPDATE tbl_order_detail t SET unit_price = 1424, unit_price_after_vat=1271, shipping_fee = 0, paid_price = 1424, paid_price_after_vat = 1271 WHERE order_nr = '200039946' AND sku = 'AD731EL93OEKLAVEN-72953';
UPDATE tbl_order_detail t SET unit_price = 696, unit_price_after_vat=621, shipping_fee = 0, paid_price = 696, paid_price_after_vat = 621 WHERE order_nr = '200041646' AND sku = 'CO826EL45JUWLAVEN-69967';
UPDATE tbl_order_detail t SET unit_price = 416, unit_price_after_vat=371, shipping_fee = 0, paid_price = 416, paid_price_after_vat = 371 WHERE order_nr = '200041846' AND sku = 'MO903TB91LFOLAVEN-70920';
UPDATE tbl_order_detail t SET unit_price = 2546, unit_price_after_vat=2273, shipping_fee = 0, paid_price = 2546, paid_price_after_vat = 2273 WHERE order_nr = '200041846' AND sku = 'NI704EL41EMOLAVEN-66321';
UPDATE tbl_order_detail t SET unit_price = 8237, unit_price_after_vat=7354, shipping_fee = 0, paid_price = 8237, paid_price_after_vat = 7354 WHERE order_nr = '200041946' AND sku = 'SA717EL29ANELAVEN-63620';
UPDATE tbl_order_detail t SET unit_price = 351, unit_price_after_vat=313, shipping_fee = 0, paid_price = 351, paid_price_after_vat = 313 WHERE order_nr = '200042646' AND sku = 'CO748HL44DTFLAVEN-65815';
UPDATE tbl_order_detail t SET unit_price = 2039, unit_price_after_vat=1821, shipping_fee = 0, paid_price = 2039, paid_price_after_vat = 1821 WHERE order_nr = '200042946' AND sku = 'LG689EL16JVZLAVEN-69996';
UPDATE tbl_order_detail t SET unit_price = 221, unit_price_after_vat=197, shipping_fee = 0, paid_price = 221, paid_price_after_vat = 197 WHERE order_nr = '200043246' AND sku = 'CA780HL19GDSLAVEN-67444';
UPDATE tbl_order_detail t SET unit_price = 156, unit_price_after_vat=139, shipping_fee = 0, paid_price = 156, paid_price_after_vat = 139 WHERE order_nr = '200043246' AND sku = 'PH898TB07LEYLAVEN-70904';
UPDATE tbl_order_detail t SET unit_price = 254, unit_price_after_vat=227, shipping_fee = 0, paid_price = 254, paid_price_after_vat = 227 WHERE order_nr = '200043246' AND sku = 'PR822HL90HFTLAVEN-68175';
UPDATE tbl_order_detail t SET unit_price = 437, unit_price_after_vat=390, shipping_fee = 0, paid_price = 437, paid_price_after_vat = 390 WHERE order_nr = '200043646' AND sku = 'NO758HL61EHYLAVEN-66201';
UPDATE tbl_order_detail t SET unit_price = 482, unit_price_after_vat=430, shipping_fee = 0, paid_price = 482, paid_price_after_vat = 430 WHERE order_nr = '200043646' AND sku = 'OS854EL95IKILAVEN-69017';
UPDATE tbl_order_detail t SET unit_price = 10190, unit_price_after_vat=9098, shipping_fee = 0, paid_price = 10190, paid_price_after_vat = 9098 WHERE order_nr = '200043946' AND sku = 'SA717EL36IITLAVEN-68976';
UPDATE tbl_order_detail t SET unit_price = 289, unit_price_after_vat=258, shipping_fee = 0, paid_price = 289, paid_price_after_vat = 258 WHERE order_nr = '200044646' AND sku = 'OS854EL00IKDLAVEN-69012';
UPDATE tbl_order_detail t SET unit_price = 423, unit_price_after_vat=378, shipping_fee = 0, paid_price = 423, paid_price_after_vat = 378 WHERE order_nr = '200044946' AND sku = 'TA893TB43LDOLAVEN-70868';
UPDATE tbl_order_detail t SET unit_price = 351, unit_price_after_vat=313, shipping_fee = 0, paid_price = 351, paid_price_after_vat = 313 WHERE order_nr = '200044946' AND sku = 'CO748HL41DTILAVEN-65818';
UPDATE tbl_order_detail t SET unit_price = 351, unit_price_after_vat=313, shipping_fee = 0, paid_price = 351, paid_price_after_vat = 313 WHERE order_nr = '200044946' AND sku = 'CO748HL47DTCLAVEN-65812';
UPDATE tbl_order_detail t SET unit_price = 77, unit_price_after_vat=69, shipping_fee = 0, paid_price = 77, paid_price_after_vat = 69 WHERE order_nr = '200045646' AND sku = 'PU858EL66ILLLAVEN-69046';
UPDATE tbl_order_detail t SET unit_price = 99, unit_price_after_vat=88, shipping_fee = 0, paid_price = 99, paid_price_after_vat = 88 WHERE order_nr = '200045646' AND sku = 'PU858EL65ILMLAVEN-69047';
UPDATE tbl_order_detail t SET unit_price = 5570, unit_price_after_vat=4973, shipping_fee = 0, paid_price = 5570, paid_price_after_vat = 4973 WHERE order_nr = '200045846' AND sku = 'BL657EL55BBOLAVEN-63999';
UPDATE tbl_order_detail t SET unit_price = 5081, unit_price_after_vat=4537, shipping_fee = 0, paid_price = 5081, paid_price_after_vat = 4537 WHERE order_nr = '200045846' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET unit_price = 3055, unit_price_after_vat=2728, shipping_fee = 0, paid_price = 3055, paid_price_after_vat = 2728 WHERE order_nr = '200045946' AND sku = 'BL657EL37AMWLAVEN-63612';
UPDATE tbl_order_detail t SET unit_price = 416, unit_price_after_vat=371, shipping_fee = 0, paid_price = 416, paid_price_after_vat = 371 WHERE order_nr = '200046646' AND sku = 'MO903TB92LFNLAVEN-70919';
UPDATE tbl_order_detail t SET unit_price = 8237, unit_price_after_vat=7354, shipping_fee = 0, paid_price = 8237, paid_price_after_vat = 7354 WHERE order_nr = '200046846' AND sku = 'SA717EL29ANELAVEN-63620';
UPDATE tbl_order_detail t SET unit_price = 3764, unit_price_after_vat=3361, shipping_fee = 0, paid_price = 3764, paid_price_after_vat = 3361 WHERE order_nr = '200046946' AND sku = 'SO722EL34LABLAVEN-70777';
UPDATE tbl_order_detail t SET unit_price = 875, unit_price_after_vat=781, shipping_fee = 0, paid_price = 875, paid_price_after_vat = 781 WHERE order_nr = '200047646' AND sku = 'PR822HL91HFSLAVEN-68174';
UPDATE tbl_order_detail t SET unit_price = 1271, unit_price_after_vat=1135, shipping_fee = 0, paid_price = 1271, paid_price_after_vat = 1135 WHERE order_nr = '200047846' AND sku = 'OL750EL85DVMLAVEN-65874';
UPDATE tbl_order_detail t SET unit_price = 5081, unit_price_after_vat=4537, shipping_fee = 0, paid_price = 5081, paid_price_after_vat = 4537 WHERE order_nr = '200047946' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET unit_price = 351, unit_price_after_vat=313, shipping_fee = 0, paid_price = 351, paid_price_after_vat = 313 WHERE order_nr = '200048646' AND sku = 'CO748HL46DTDLAVEN-65813';
UPDATE tbl_order_detail t SET unit_price = 4958, unit_price_after_vat=4427, shipping_fee = 0, paid_price = 4958, paid_price_after_vat = 4427 WHERE order_nr = '200048846' AND sku = 'SO722EL15LAULAVEN-70796';
UPDATE tbl_order_detail t SET unit_price = 2289, unit_price_after_vat=2044, shipping_fee = 0, paid_price = 2289, paid_price_after_vat = 2044 WHERE order_nr = '200048946' AND sku = 'PA708EL84HNRLAVEN-68381';
UPDATE tbl_order_detail t SET unit_price = 221, unit_price_after_vat=197, shipping_fee = 0, paid_price = 221, paid_price_after_vat = 197 WHERE order_nr = '200049646' AND sku = 'CA780HL19GDSLAVEN-67444';
UPDATE tbl_order_detail t SET unit_price = 221, unit_price_after_vat=197, shipping_fee = 0, paid_price = 221, paid_price_after_vat = 197 WHERE order_nr = '200049646' AND sku = 'CA780HL17GDULAVEN-67446';
UPDATE tbl_order_detail t SET unit_price = 184, unit_price_after_vat=164, shipping_fee = 0, paid_price = 184, paid_price_after_vat = 164 WHERE order_nr = '200049646' AND sku = 'OC741HL19DEULAVEN-65444';
UPDATE tbl_order_detail t SET unit_price = 184, unit_price_after_vat=164, shipping_fee = 0, paid_price = 184, paid_price_after_vat = 164 WHERE order_nr = '200049646' AND sku = 'OC741HL19DEULAVEN-65444';
UPDATE tbl_order_detail t SET unit_price = 814, unit_price_after_vat=727, shipping_fee = 0, paid_price = 814, paid_price_after_vat = 727 WHERE order_nr = '200049846' AND sku = 'MO902TB95LFKLAVEN-70916';
UPDATE tbl_order_detail t SET unit_price = 423, unit_price_after_vat=378, shipping_fee = 0, paid_price = 423, paid_price_after_vat = 378 WHERE order_nr = '200049846' AND sku = 'TA893TB42LDPLAVEN-70869';
UPDATE tbl_order_detail t SET unit_price = 423, unit_price_after_vat=378, shipping_fee = 0, paid_price = 423, paid_price_after_vat = 378 WHERE order_nr = '200049846' AND sku = 'TA893TB42LDPLAVEN-70869';
UPDATE tbl_order_detail t SET unit_price = 8237, unit_price_after_vat=7354, shipping_fee = 0, paid_price = 8237, paid_price_after_vat = 7354 WHERE order_nr = '200049946' AND sku = 'SA717EL62ATPLAVEN-63792';
UPDATE tbl_order_detail t SET unit_price = 8237, unit_price_after_vat=7354, shipping_fee = 0, paid_price = 8237, paid_price_after_vat = 7354 WHERE order_nr = '200049946' AND sku = 'SA717EL29ANELAVEN-63620';
UPDATE tbl_order_detail t SET unit_price = 261, unit_price_after_vat=233, shipping_fee = 0, paid_price = 261, paid_price_after_vat = 233 WHERE order_nr = '200051246' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET unit_price = 261, unit_price_after_vat=233, shipping_fee = 0, paid_price = 261, paid_price_after_vat = 233 WHERE order_nr = '200051246' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET unit_price = 504, unit_price_after_vat=450, shipping_fee = 0, paid_price = 504, paid_price_after_vat = 450 WHERE order_nr = '200051246' AND sku = 'MA746HL39DLSLAVEN-65620';
UPDATE tbl_order_detail t SET unit_price = 5081, unit_price_after_vat=4537, shipping_fee = 0, paid_price = 5081, paid_price_after_vat = 4537 WHERE order_nr = '200051646' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET unit_price = 267, unit_price_after_vat=238, shipping_fee = 0, paid_price = 267, paid_price_after_vat = 238 WHERE order_nr = '200051646' AND sku = 'LA857EL82IKVLAVEN-69030';
UPDATE tbl_order_detail t SET unit_price = 3055, unit_price_after_vat=2728, shipping_fee = 0, paid_price = 3055, paid_price_after_vat = 2728 WHERE order_nr = '200051946' AND sku = 'BL657EL37AMWLAVEN-63612';
UPDATE tbl_order_detail t SET unit_price = 588, unit_price_after_vat=525, shipping_fee = 0, paid_price = 588, paid_price_after_vat = 525 WHERE order_nr = '200052646' AND sku = 'PA708EL43KZSLAVEN-70768';
UPDATE tbl_order_detail t SET unit_price = 261, unit_price_after_vat=233, shipping_fee = 0, paid_price = 261, paid_price_after_vat = 233 WHERE order_nr = '200052646' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET unit_price = 261, unit_price_after_vat=233, shipping_fee = 0, paid_price = 261, paid_price_after_vat = 233 WHERE order_nr = '200052646' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET unit_price = 3497, unit_price_after_vat=3122, shipping_fee = 0, paid_price = 3497, paid_price_after_vat = 3122 WHERE order_nr = '200052646' AND sku = 'SA717EL44JRBLAVEN-69868';
UPDATE tbl_order_detail t SET unit_price = 423, unit_price_after_vat=378, shipping_fee = 0, paid_price = 423, paid_price_after_vat = 378 WHERE order_nr = '200052846' AND sku = 'TA893TB43LDOLAVEN-70868';
UPDATE tbl_order_detail t SET unit_price = 423, unit_price_after_vat=378, shipping_fee = 0, paid_price = 423, paid_price_after_vat = 378 WHERE order_nr = '200052846' AND sku = 'TA893TB43LDOLAVEN-70868';
UPDATE tbl_order_detail t SET unit_price = 317, unit_price_after_vat=283, shipping_fee = 0, paid_price = 317, paid_price_after_vat = 283 WHERE order_nr = '200052846' AND sku = 'MI915TB62LKNLAVEN-71084';
UPDATE tbl_order_detail t SET unit_price = 1170, unit_price_after_vat=1045, shipping_fee = 0, paid_price = 1170, paid_price_after_vat = 1045 WHERE order_nr = '200052946' AND sku = 'FR860FA55ILWLAVEN-69057';
UPDATE tbl_order_detail t SET unit_price = 535, unit_price_after_vat=478, shipping_fee = 0, paid_price = 535, paid_price_after_vat = 478 WHERE order_nr = '200052946' AND sku = 'OS854EL28JRRLAVEN-69884';
UPDATE tbl_order_detail t SET unit_price = 351, unit_price_after_vat=313, shipping_fee = 0, paid_price = 351, paid_price_after_vat = 313 WHERE order_nr = '200053246' AND sku = 'CO748HL41DTILAVEN-65818';
UPDATE tbl_order_detail t SET unit_price = 3055, unit_price_after_vat=2728, shipping_fee = 0, paid_price = 3055, paid_price_after_vat = 2728 WHERE order_nr = '200053646' AND sku = 'BL657EL37AMWLAVEN-63612';
UPDATE tbl_order_detail t SET unit_price = 5624, unit_price_after_vat=5021, shipping_fee = 0, paid_price = 5624, paid_price_after_vat = 5021 WHERE order_nr = '200054646' AND sku = 'LE688EL41LLILAVEN-71105';
UPDATE tbl_order_detail t SET unit_price = 261, unit_price_after_vat=233, shipping_fee = 0, paid_price = 261, paid_price_after_vat = 233 WHERE order_nr = '200054646' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET unit_price = 998, unit_price_after_vat=891, shipping_fee = 0, paid_price = 998, paid_price_after_vat = 891 WHERE order_nr = '200054946' AND sku = 'CO826EL60JUHLAVEN-69952';
UPDATE tbl_order_detail t SET unit_price = 753, unit_price_after_vat=672, shipping_fee = 0, paid_price = 753, paid_price_after_vat = 672 WHERE order_nr = '200054946' AND sku = 'BO829EL36IQLLAVEN-69176';
UPDATE tbl_order_detail t SET unit_price = 753, unit_price_after_vat=672, shipping_fee = 0, paid_price = 753, paid_price_after_vat = 672 WHERE order_nr = '200054946' AND sku = 'BO829EL39IQILAVEN-69173';
UPDATE tbl_order_detail t SET unit_price = 449, unit_price_after_vat=401, shipping_fee = 0, paid_price = 449, paid_price_after_vat = 401 WHERE order_nr = '200055646' AND sku = 'CA740HL85DJYLAVEN-65574';
UPDATE tbl_order_detail t SET unit_price = 2546, unit_price_after_vat=2273, shipping_fee = 0, paid_price = 2546, paid_price_after_vat = 2273 WHERE order_nr = '200055846' AND sku = 'NI704EL41EMOLAVEN-66321';
UPDATE tbl_order_detail t SET unit_price = 5710, unit_price_after_vat=5098, shipping_fee = 0, paid_price = 5710, paid_price_after_vat = 5098 WHERE order_nr = '200055946' AND sku = 'SO722EL14LAVLAVEN-70797';
UPDATE tbl_order_detail t SET unit_price = 588, unit_price_after_vat=525, shipping_fee = 0, paid_price = 588, paid_price_after_vat = 525 WHERE order_nr = '200056646' AND sku = 'PA708EL43KZSLAVEN-70768';
UPDATE tbl_order_detail t SET unit_price = 423, unit_price_after_vat=378, shipping_fee = 0, paid_price = 423, paid_price_after_vat = 378 WHERE order_nr = '200056846' AND sku = 'OS854EL93IKKLAVEN-69019';
UPDATE tbl_order_detail t SET unit_price = 423, unit_price_after_vat=378, shipping_fee = 0, paid_price = 423, paid_price_after_vat = 378 WHERE order_nr = '200056846' AND sku = 'OS854EL93IKKLAVEN-69019';
UPDATE tbl_order_detail t SET unit_price = 1326, unit_price_after_vat=1184, shipping_fee = 0, paid_price = 1326, paid_price_after_vat = 1184 WHERE order_nr = '200056846' AND sku = 'SA717EL10IFXLAVEN-68902';
UPDATE tbl_order_detail t SET unit_price = 8237, unit_price_after_vat=7354, shipping_fee = 0, paid_price = 8237, paid_price_after_vat = 7354 WHERE order_nr = '200056946' AND sku = 'SA717EL28KDFLAVEN-70184';
UPDATE tbl_order_detail t SET unit_price = 1139, unit_price_after_vat=1017, shipping_fee = 0, paid_price = 1139, paid_price_after_vat = 1017 WHERE order_nr = '200057646' AND sku = 'LG689EL47KZOLAVEN-70764';
UPDATE tbl_order_detail t SET unit_price = 2063, unit_price_after_vat=1842, shipping_fee = 0, paid_price = 2063, paid_price_after_vat = 1842 WHERE order_nr = '200057846' AND sku = 'GA766EL73FEOLAVEN-66790';
UPDATE tbl_order_detail t SET unit_price = 5081, unit_price_after_vat=4537, shipping_fee = 0, paid_price = 5081, paid_price_after_vat = 4537 WHERE order_nr = '200057946' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET unit_price = 267, unit_price_after_vat=238, shipping_fee = 0, paid_price = 267, paid_price_after_vat = 238 WHERE order_nr = '200057946' AND sku = 'LA857EL82IKVLAVEN-69030';
UPDATE tbl_order_detail t SET unit_price = 267, unit_price_after_vat=238, shipping_fee = 0, paid_price = 267, paid_price_after_vat = 238 WHERE order_nr = '200057946' AND sku = 'LA857EL82IKVLAVEN-69030';
UPDATE tbl_order_detail t SET unit_price = 5570, unit_price_after_vat=4973, shipping_fee = 0, paid_price = 5570, paid_price_after_vat = 4973 WHERE order_nr = '200058646' AND sku = 'BL657EL55BBOLAVEN-63999';
UPDATE tbl_order_detail t SET unit_price = 3055, unit_price_after_vat=2728, shipping_fee = 0, paid_price = 3055, paid_price_after_vat = 2728 WHERE order_nr = '200058846' AND sku = 'BL657EL59KRKLAVEN-70552';
UPDATE tbl_order_detail t SET unit_price = 10190, unit_price_after_vat=9098, shipping_fee = 0, paid_price = 10190, paid_price_after_vat = 9098 WHERE order_nr = '200058946' AND sku = 'SA717EL36IITLAVEN-68976';
UPDATE tbl_order_detail t SET unit_price = 473, unit_price_after_vat=422, shipping_fee = 0, paid_price = 473, paid_price_after_vat = 422 WHERE order_nr = '200059646' AND sku = 'TO912TB70LKFLAVEN-71076';
UPDATE tbl_order_detail t SET unit_price = 369, unit_price_after_vat=329, shipping_fee = 0, paid_price = 369, paid_price_after_vat = 329 WHERE order_nr = '200059646' AND sku = 'MA746HL69DKOLAVEN-65590';
UPDATE tbl_order_detail t SET unit_price = 423, unit_price_after_vat=378, shipping_fee = 0, paid_price = 423, paid_price_after_vat = 378 WHERE order_nr = '200059846' AND sku = 'TA893TB42LDPLAVEN-70869';
UPDATE tbl_order_detail t SET unit_price = 2063, unit_price_after_vat=1842, shipping_fee = 0, paid_price = 2063, paid_price_after_vat = 1842 WHERE order_nr = '200059946' AND sku = 'GA766EL73FEOLAVEN-66790';
UPDATE tbl_order_detail t SET unit_price = 3055, unit_price_after_vat=2728, shipping_fee = 0, paid_price = 3055, paid_price_after_vat = 2728 WHERE order_nr = '200061646' AND sku = 'BL657EL37AMWLAVEN-63612';
UPDATE tbl_order_detail t SET unit_price = 698, unit_price_after_vat=623, shipping_fee = 0, paid_price = 698, paid_price_after_vat = 623 WHERE order_nr = '200061846' AND sku = 'SO722EL77CBQLAVEN-64687';
UPDATE tbl_order_detail t SET unit_price = 5710, unit_price_after_vat=5098, shipping_fee = 0, paid_price = 5710, paid_price_after_vat = 5098 WHERE order_nr = '200061846' AND sku = 'SO722EL14LAVLAVEN-70797';
UPDATE tbl_order_detail t SET unit_price = 3968, unit_price_after_vat=3543, shipping_fee = 0, paid_price = 3968, paid_price_after_vat = 3543 WHERE order_nr = '200061946' AND sku = 'BL657EL36AMXLAVEN-63613';
UPDATE tbl_order_detail t SET unit_price = 850, unit_price_after_vat=759, shipping_fee = 0, paid_price = 850, paid_price_after_vat = 759 WHERE order_nr = '200062646' AND sku = 'SO722EL57EICLAVEN-66205';
UPDATE tbl_order_detail t SET unit_price = 937, unit_price_after_vat=837, shipping_fee = 0, paid_price = 937, paid_price_after_vat = 837 WHERE order_nr = '200062646' AND sku = 'SO722EL10LAZLAVEN-70801';
UPDATE tbl_order_detail t SET unit_price = 8667, unit_price_after_vat=7738, shipping_fee = 0, paid_price = 8667, paid_price_after_vat = 7738 WHERE order_nr = '200062946' AND sku = 'LE688EL64GFVLAVEN-67499';
UPDATE tbl_order_detail t SET unit_price = 340, unit_price_after_vat=304, shipping_fee = 0, paid_price = 340, paid_price_after_vat = 304 WHERE order_nr = '200063246' AND sku = 'HA894TB24LEHLAVEN-70887';
UPDATE tbl_order_detail t SET unit_price = 482, unit_price_after_vat=430, shipping_fee = 0, paid_price = 482, paid_price_after_vat = 430 WHERE order_nr = '200063246' AND sku = 'OS854EL95IKILAVEN-69017';
UPDATE tbl_order_detail t SET unit_price = 5081, unit_price_after_vat=4537, shipping_fee = 0, paid_price = 5081, paid_price_after_vat = 4537 WHERE order_nr = '200063646' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET unit_price = 508, unit_price_after_vat=454, shipping_fee = 0, paid_price = 508, paid_price_after_vat = 454 WHERE order_nr = '200063946' AND sku = 'CA740HL86DJXLAVEN-65573';
UPDATE tbl_order_detail t SET unit_price = 666, unit_price_after_vat=595, shipping_fee = 0, paid_price = 666, paid_price_after_vat = 595 WHERE order_nr = '200063946' AND sku = 'CA740HL31IBGLAVEN-68734';
UPDATE tbl_order_detail t SET unit_price = 3299, unit_price_after_vat=2946, shipping_fee = 0, paid_price = 3299, paid_price_after_vat = 2946 WHERE order_nr = '200064646' AND sku = 'PA708EL82HNTLAVEN-68383';
UPDATE tbl_order_detail t SET unit_price = 906, unit_price_after_vat=809, shipping_fee = 0, paid_price = 906, paid_price_after_vat = 809 WHERE order_nr = '200064946' AND sku = 'CA740HL77DCOLAVEN-65386';
UPDATE tbl_order_detail t SET unit_price = 1827, unit_price_after_vat=1631, shipping_fee = 0, paid_price = 1827, paid_price_after_vat = 1631 WHERE order_nr = '200065646' AND sku = 'CA740HL47DDSLAVEN-65416';
UPDATE tbl_order_detail t SET unit_price = 261, unit_price_after_vat=233, shipping_fee = 0, paid_price = 261, paid_price_after_vat = 233 WHERE order_nr = '200065646' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET unit_price = 3968, unit_price_after_vat=3543, shipping_fee = 0, paid_price = 3968, paid_price_after_vat = 3543 WHERE order_nr = '200065846' AND sku = 'BL657EL36AMXLAVEN-63613';
UPDATE tbl_order_detail t SET unit_price = 134, unit_price_after_vat=120, shipping_fee = 0, paid_price = 134, paid_price_after_vat = 120 WHERE order_nr = '200065846' AND sku = 'PR856HL34IIVLAVEN-68978';
UPDATE tbl_order_detail t SET unit_price = 267, unit_price_after_vat=238, shipping_fee = 0, paid_price = 267, paid_price_after_vat = 238 WHERE order_nr = '200065946' AND sku = 'LA857EL82IKVLAVEN-69030';
UPDATE tbl_order_detail t SET unit_price = 998, unit_price_after_vat=891, shipping_fee = 0, paid_price = 998, paid_price_after_vat = 891 WHERE order_nr = '200065946' AND sku = 'CO826EL60JUHLAVEN-69952';
UPDATE tbl_order_detail t SET unit_price = 77, unit_price_after_vat=69, shipping_fee = 0, paid_price = 77, paid_price_after_vat = 69 WHERE order_nr = '200065946' AND sku = 'PU858EL66ILLLAVEN-69046';
UPDATE tbl_order_detail t SET unit_price = 154, unit_price_after_vat=138, shipping_fee = 0, paid_price = 154, paid_price_after_vat = 138 WHERE order_nr = '200065946' AND sku = 'PU858EL79IKYLAVEN-69033';
UPDATE tbl_order_detail t SET unit_price = 387, unit_price_after_vat=346, shipping_fee = 0, paid_price = 387, paid_price_after_vat = 346 WHERE order_nr = '200066646' AND sku = 'CA740HL62DDDLAVEN-65401';
UPDATE tbl_order_detail t SET unit_price = 8237, unit_price_after_vat=7354, shipping_fee = 0, paid_price = 8237, paid_price_after_vat = 7354 WHERE order_nr = '200066846' AND sku = 'SA717EL62ATPLAVEN-63792';
UPDATE tbl_order_detail t SET unit_price = 696, unit_price_after_vat=621, shipping_fee = 0, paid_price = 696, paid_price_after_vat = 621 WHERE order_nr = '200066946' AND sku = 'CO826EL45JUWLAVEN-69967';
UPDATE tbl_order_detail t SET unit_price = 3055, unit_price_after_vat=2728, shipping_fee = 0, paid_price = 3055, paid_price_after_vat = 2728 WHERE order_nr = '200066946' AND sku = 'BL657EL37AMWLAVEN-63612';
UPDATE tbl_order_detail t SET unit_price = 197, unit_price_after_vat=176, shipping_fee = 0, paid_price = 197, paid_price_after_vat = 176 WHERE order_nr = '200067646' AND sku = 'DI899TB04LFBLAVEN-70907';
UPDATE tbl_order_detail t SET unit_price = 696, unit_price_after_vat=621, shipping_fee = 0, paid_price = 696, paid_price_after_vat = 621 WHERE order_nr = '200067846' AND sku = 'CO826EL45JUWLAVEN-69967';
UPDATE tbl_order_detail t SET unit_price = 152, unit_price_after_vat=136, shipping_fee = 0, paid_price = 152, paid_price_after_vat = 136 WHERE order_nr = '200067946' AND sku = 'HA894TB29LECLAVEN-70882';
UPDATE tbl_order_detail t SET unit_price = 189, unit_price_after_vat=169, shipping_fee = 0, paid_price = 189, paid_price_after_vat = 169 WHERE order_nr = '200067946' AND sku = 'HA894TB39LDSLAVEN-70872';
UPDATE tbl_order_detail t SET unit_price = 1236, unit_price_after_vat=1104, shipping_fee = 0, paid_price = 1236, paid_price_after_vat = 1104 WHERE order_nr = '200067946' AND sku = 'CA740HL25IBMLAVEN-68740';
UPDATE tbl_order_detail t SET unit_price = 1424, unit_price_after_vat=1271, shipping_fee = 0, paid_price = 1424, paid_price_after_vat = 1271 WHERE order_nr = '200068646' AND sku = 'CA740HL67DCYLAVEN-65396';
UPDATE tbl_order_detail t SET unit_price = 261, unit_price_after_vat=233, shipping_fee = 0, paid_price = 261, paid_price_after_vat = 233 WHERE order_nr = '200068646' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET unit_price = 261, unit_price_after_vat=233, shipping_fee = 0, paid_price = 261, paid_price_after_vat = 233 WHERE order_nr = '200068646' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET unit_price = 190, unit_price_after_vat=170, shipping_fee = 0, paid_price = 190, paid_price_after_vat = 170 WHERE order_nr = '200068846' AND sku = 'XM850EL96IGLLAVEN-68916';
UPDATE tbl_order_detail t SET unit_price = 315, unit_price_after_vat=281, shipping_fee = 0, paid_price = 315, paid_price_after_vat = 281 WHERE order_nr = '200068946' AND sku = 'MO917TB57LKSLAVEN-71089';
UPDATE tbl_order_detail t SET unit_price = 483, unit_price_after_vat=431, shipping_fee = 0, paid_price = 483, paid_price_after_vat = 431 WHERE order_nr = '200068946' AND sku = 'BA891TB47LDKLAVEN-70864';
UPDATE tbl_order_detail t SET unit_price = 1424, unit_price_after_vat=1271, shipping_fee = 0, paid_price = 1424, paid_price_after_vat = 1271 WHERE order_nr = '200069646' AND sku = 'CA740HL68DCXLAVEN-65395';
UPDATE tbl_order_detail t SET unit_price = 10190, unit_price_after_vat=9098, shipping_fee = 0, paid_price = 10190, paid_price_after_vat = 9098 WHERE order_nr = '200069846' AND sku = 'SA717EL36IITLAVEN-68976';
UPDATE tbl_order_detail t SET unit_price = 850, unit_price_after_vat=759, shipping_fee = 0, paid_price = 850, paid_price_after_vat = 759 WHERE order_nr = '200069946' AND sku = 'SO722EL57EICLAVEN-66205';
UPDATE tbl_order_detail t SET unit_price = 10190, unit_price_after_vat=9098, shipping_fee = 0, paid_price = 10190, paid_price_after_vat = 9098 WHERE order_nr = '200069946' AND sku = 'SA717EL36IITLAVEN-68976';
UPDATE tbl_order_detail t SET unit_price = 5570, unit_price_after_vat=4973, shipping_fee = 0, paid_price = 5570, paid_price_after_vat = 4973 WHERE order_nr = '200071246' AND sku = 'BL657EL55BBOLAVEN-63999';
UPDATE tbl_order_detail t SET unit_price = 5081, unit_price_after_vat=4537, shipping_fee = 0, paid_price = 5081, paid_price_after_vat = 4537 WHERE order_nr = '200071646' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET unit_price = 245, unit_price_after_vat=219, shipping_fee = 0, paid_price = 245, paid_price_after_vat = 219 WHERE order_nr = '200071846' AND sku = 'CA780HL16GDVLAVEN-67447';
UPDATE tbl_order_detail t SET unit_price = 588, unit_price_after_vat=525, shipping_fee = 0, paid_price = 588, paid_price_after_vat = 525 WHERE order_nr = '200071846' AND sku = 'PA708EL43KZSLAVEN-70768';
UPDATE tbl_order_detail t SET unit_price = 78, unit_price_after_vat=70, shipping_fee = 0, paid_price = 78, paid_price_after_vat = 70 WHERE order_nr = '200071846' AND sku = 'AD731EL92OELLAVEN-72954';
UPDATE tbl_order_detail t SET unit_price = 78, unit_price_after_vat=70, shipping_fee = 0, paid_price = 78, paid_price_after_vat = 70 WHERE order_nr = '200071846' AND sku = 'AD731EL92OELLAVEN-72954';
UPDATE tbl_order_detail t SET unit_price = 78, unit_price_after_vat=70, shipping_fee = 0, paid_price = 78, paid_price_after_vat = 70 WHERE order_nr = '200071846' AND sku = 'AD731EL92OELLAVEN-72954';
UPDATE tbl_order_detail t SET unit_price = 875, unit_price_after_vat=781, shipping_fee = 0, paid_price = 875, paid_price_after_vat = 781 WHERE order_nr = '200071846' AND sku = 'PR822HL91HFSLAVEN-68174';
UPDATE tbl_order_detail t SET unit_price = 886, unit_price_after_vat=791, shipping_fee = 0, paid_price = 886, paid_price_after_vat = 791 WHERE order_nr = '200071846' AND sku = 'CA740HL83DCILAVEN-65380';
UPDATE tbl_order_detail t SET unit_price = 8237, unit_price_after_vat=7354, shipping_fee = 0, paid_price = 8237, paid_price_after_vat = 7354 WHERE order_nr = '200071946' AND sku = 'SA717EL02GMBLAVEN-67661';
UPDATE tbl_order_detail t SET unit_price = 560, unit_price_after_vat=500, shipping_fee = 0, paid_price = 560, paid_price_after_vat = 500 WHERE order_nr = '200072646' AND sku = 'OS854EL96IKHLAVEN-69016';
UPDATE tbl_order_detail t SET unit_price = 261, unit_price_after_vat=233, shipping_fee = 0, paid_price = 261, paid_price_after_vat = 233 WHERE order_nr = '200072646' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET unit_price = 828, unit_price_after_vat=739, shipping_fee = 0, paid_price = 828, paid_price_after_vat = 739 WHERE order_nr = '200072946' AND sku = 'CA740HL45DDULAVEN-65418';
UPDATE tbl_order_detail t SET unit_price = 625, unit_price_after_vat=558, shipping_fee = 0, paid_price = 625, paid_price_after_vat = 558 WHERE order_nr = '200072946' AND sku = 'NI704EL19LAQLAVEN-70792';
UPDATE tbl_order_detail t SET unit_price = 427, unit_price_after_vat=381, shipping_fee = 0, paid_price = 427, paid_price_after_vat = 381 WHERE order_nr = '200073246' AND sku = 'MI915TB63LKMLAVEN-71083';
UPDATE tbl_order_detail t SET unit_price = 134, unit_price_after_vat=120, shipping_fee = 0, paid_price = 134, paid_price_after_vat = 120 WHERE order_nr = '200073246' AND sku = 'PR856HL34IIVLAVEN-68978';
UPDATE tbl_order_detail t SET unit_price = 8237, unit_price_after_vat=7354, shipping_fee = 0, paid_price = 8237, paid_price_after_vat = 7354 WHERE order_nr = '200073646' AND sku = 'SA717EL02GMBLAVEN-67661';
UPDATE tbl_order_detail t SET unit_price = 588, unit_price_after_vat=525, shipping_fee = 0, paid_price = 588, paid_price_after_vat = 525 WHERE order_nr = '200074646' AND sku = 'PA708EL43KZSLAVEN-70768';
UPDATE tbl_order_detail t SET unit_price = 5081, unit_price_after_vat=4537, shipping_fee = 0, paid_price = 5081, paid_price_after_vat = 4537 WHERE order_nr = '200074946' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET unit_price = 1436, unit_price_after_vat=1282, shipping_fee = 0, paid_price = 1436, paid_price_after_vat = 1282 WHERE order_nr = '200075646' AND sku = 'ST878EL02KIBLAVEN-70310';
UPDATE tbl_order_detail t SET unit_price = 291, unit_price_after_vat=260, shipping_fee = 0, paid_price = 291, paid_price_after_vat = 260 WHERE order_nr = '200075646' AND sku = 'OS854EL04IJZLAVEN-69008';
UPDATE tbl_order_detail t SET unit_price = 1139, unit_price_after_vat=1017, shipping_fee = 0, paid_price = 1139, paid_price_after_vat = 1017 WHERE order_nr = '200075646' AND sku = 'LG689EL47KZOLAVEN-70764';
UPDATE tbl_order_detail t SET unit_price = 2063, unit_price_after_vat=1842, shipping_fee = 0, paid_price = 2063, paid_price_after_vat = 1842 WHERE order_nr = '200075846' AND sku = 'GA766EL73FEOLAVEN-66790';
UPDATE tbl_order_detail t SET unit_price = 998, unit_price_after_vat=891, shipping_fee = 0, paid_price = 998, paid_price_after_vat = 891 WHERE order_nr = '200075946' AND sku = 'CO826EL60JUHLAVEN-69952';
UPDATE tbl_order_detail t SET unit_price = 5570, unit_price_after_vat=4973, shipping_fee = 0, paid_price = 5570, paid_price_after_vat = 4973 WHERE order_nr = '200076646' AND sku = 'BL657EL55BBOLAVEN-63999';
UPDATE tbl_order_detail t SET unit_price = 190, unit_price_after_vat=170, shipping_fee = 0, paid_price = 190, paid_price_after_vat = 170 WHERE order_nr = '200076846' AND sku = 'XM850EL97IGKLAVEN-68915';
UPDATE tbl_order_detail t SET unit_price = 861, unit_price_after_vat=769, shipping_fee = 0, paid_price = 861, paid_price_after_vat = 769 WHERE order_nr = '200076946' AND sku = 'CA740HL27IBKLAVEN-68738';
UPDATE tbl_order_detail t SET unit_price = 261, unit_price_after_vat=233, shipping_fee = 0, paid_price = 261, paid_price_after_vat = 233 WHERE order_nr = '200077646' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET unit_price = 261, unit_price_after_vat=233, shipping_fee = 0, paid_price = 261, paid_price_after_vat = 233 WHERE order_nr = '200077646' AND sku = 'KA855HL23IJGLAVEN-68989';
UPDATE tbl_order_detail t SET unit_price = 743, unit_price_after_vat=663, shipping_fee = 0, paid_price = 743, paid_price_after_vat = 663 WHERE order_nr = '200077846' AND sku = 'TO912TB72LKDLAVEN-71074';
UPDATE tbl_order_detail t SET unit_price = 483, unit_price_after_vat=431, shipping_fee = 0, paid_price = 483, paid_price_after_vat = 431 WHERE order_nr = '200077846' AND sku = 'BA891TB47LDKLAVEN-70864';
UPDATE tbl_order_detail t SET unit_price = 812, unit_price_after_vat=725, shipping_fee = 0, paid_price = 812, paid_price_after_vat = 725 WHERE order_nr = '200077946' AND sku = 'FR860FA46IMFLAVEN-69066';
UPDATE tbl_order_detail t SET unit_price = 1073, unit_price_after_vat=958, shipping_fee = 0, paid_price = 1073, paid_price_after_vat = 958 WHERE order_nr = '200077946' AND sku = 'FR860FA17INILAVEN-69095';
UPDATE tbl_order_detail t SET unit_price = 1506, unit_price_after_vat=1345, shipping_fee = 0, paid_price = 1506, paid_price_after_vat = 1345 WHERE order_nr = '200078646' AND sku = 'SO722EL40KZVLAVEN-70771';
UPDATE tbl_order_detail t SET unit_price = 317, unit_price_after_vat=283, shipping_fee = 0, paid_price = 317, paid_price_after_vat = 283 WHERE order_nr = '200078846' AND sku = 'MI915TB62LKNLAVEN-71084';
UPDATE tbl_order_detail t SET unit_price = 380, unit_price_after_vat=339, shipping_fee = 0, paid_price = 380, paid_price_after_vat = 339 WHERE order_nr = '200078946' AND sku = 'CE900TB00LFFLAVEN-70911';
UPDATE tbl_order_detail t SET unit_price = 309, unit_price_after_vat=276, shipping_fee = 0, paid_price = 309, paid_price_after_vat = 276 WHERE order_nr = '200078946' AND sku = 'CA897TB08LEXLAVEN-70903';
UPDATE tbl_order_detail t SET unit_price = 351, unit_price_after_vat=313, shipping_fee = 0, paid_price = 351, paid_price_after_vat = 313 WHERE order_nr = '200079646' AND sku = 'CO748HL05INULAVEN-69107';
UPDATE tbl_order_detail t SET unit_price = 3111, unit_price_after_vat=2778, shipping_fee = 0, paid_price = 3111, paid_price_after_vat = 2778 WHERE order_nr = '200079846' AND sku = 'SA717EL60ATRLAVEN-63794';
UPDATE tbl_order_detail t SET unit_price = 5081, unit_price_after_vat=4537, shipping_fee = 0, paid_price = 5081, paid_price_after_vat = 4537 WHERE order_nr = '200079946' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET unit_price = 358, unit_price_after_vat=320, shipping_fee = 0, paid_price = 358, paid_price_after_vat = 320 WHERE order_nr = '200079946' AND sku = 'OS854EL29JRQLAVEN-69883';
UPDATE tbl_order_detail t SET unit_price = 1271, unit_price_after_vat=1135, shipping_fee = 0, paid_price = 1271, paid_price_after_vat = 1135 WHERE order_nr = '200081646' AND sku = 'OL750EL88DVJLAVEN-65871';
UPDATE tbl_order_detail t SET unit_price = 245, unit_price_after_vat=219, shipping_fee = 0, paid_price = 245, paid_price_after_vat = 219 WHERE order_nr = '200081846' AND sku = 'CA780HL16GDVLAVEN-67447';
UPDATE tbl_order_detail t SET unit_price = 588, unit_price_after_vat=525, shipping_fee = 0, paid_price = 588, paid_price_after_vat = 525 WHERE order_nr = '200081846' AND sku = 'PA708EL43KZSLAVEN-70768';
UPDATE tbl_order_detail t SET unit_price = 850, unit_price_after_vat=759, shipping_fee = 0, paid_price = 850, paid_price_after_vat = 759 WHERE order_nr = '200081846' AND sku = 'SO722EL57EICLAVEN-66205';
UPDATE tbl_order_detail t SET unit_price = 5570, unit_price_after_vat=4973, shipping_fee = 0, paid_price = 5570, paid_price_after_vat = 4973 WHERE order_nr = '200081846' AND sku = 'BL657EL55BBOLAVEN-63999';
UPDATE tbl_order_detail t SET unit_price = 937, unit_price_after_vat=837, shipping_fee = 0, paid_price = 937, paid_price_after_vat = 837 WHERE order_nr = '200081846' AND sku = 'SO722EL09LBALAVEN-70802';
UPDATE tbl_order_detail t SET unit_price = 3122, unit_price_after_vat=2787, shipping_fee = 0, paid_price = 3122, paid_price_after_vat = 2787 WHERE order_nr = '200081846' AND sku = 'NI704EL31NJSLAVEN-72415';
UPDATE tbl_order_detail t SET unit_price = 3122, unit_price_after_vat=2787, shipping_fee = 0, paid_price = 3122, paid_price_after_vat = 2787 WHERE order_nr = '200081846' AND sku = 'NI704EL20LAPLAVEN-70791';
UPDATE tbl_order_detail t SET unit_price = 351, unit_price_after_vat=313, shipping_fee = 0, paid_price = 351, paid_price_after_vat = 313 WHERE order_nr = '200081946' AND sku = 'CO748HL41DTILAVEN-65818';
UPDATE tbl_order_detail t SET unit_price = 351, unit_price_after_vat=313, shipping_fee = 0, paid_price = 351, paid_price_after_vat = 313 WHERE order_nr = '200081946' AND sku = 'CO748HL41DTILAVEN-65818';
UPDATE tbl_order_detail t SET unit_price = 351, unit_price_after_vat=313, shipping_fee = 0, paid_price = 351, paid_price_after_vat = 313 WHERE order_nr = '200081946' AND sku = 'CO748HL41DTILAVEN-65818';
UPDATE tbl_order_detail t SET unit_price = 175, unit_price_after_vat=156, shipping_fee = 0, paid_price = 175, paid_price_after_vat = 156 WHERE order_nr = '200082646' AND sku = 'OC741HL11DFCLAVEN-65452';
UPDATE tbl_order_detail t SET unit_price = 191, unit_price_after_vat=171, shipping_fee = 0, paid_price = 191, paid_price_after_vat = 171 WHERE order_nr = '200082646' AND sku = 'OC741HL17DEWLAVEN-65446';
UPDATE tbl_order_detail t SET unit_price = 262, unit_price_after_vat=234, shipping_fee = 0, paid_price = 262, paid_price_after_vat = 234 WHERE order_nr = '200082646' AND sku = 'PR822HL89HFULAVEN-68176';
UPDATE tbl_order_detail t SET unit_price = 351, unit_price_after_vat=313, shipping_fee = 0, paid_price = 351, paid_price_after_vat = 313 WHERE order_nr = '200082646' AND sku = 'CO748HL41DTILAVEN-65818';
UPDATE tbl_order_detail t SET unit_price = 134, unit_price_after_vat=120, shipping_fee = 0, paid_price = 134, paid_price_after_vat = 120 WHERE order_nr = '200082646' AND sku = 'PR856HL34IIVLAVEN-68978';
UPDATE tbl_order_detail t SET unit_price = 10190, unit_price_after_vat=9098, shipping_fee = 0, paid_price = 10190, paid_price_after_vat = 9098 WHERE order_nr = '200082946' AND sku = 'SA717EL36IITLAVEN-68976';
UPDATE tbl_order_detail t SET unit_price = 504, unit_price_after_vat=450, shipping_fee = 0, paid_price = 504, paid_price_after_vat = 450 WHERE order_nr = '200083246' AND sku = 'MA746HL38DLTLAVEN-65621';
UPDATE tbl_order_detail t SET unit_price = 182, unit_price_after_vat=162, shipping_fee = 0, paid_price = 182, paid_price_after_vat = 162 WHERE order_nr = '200083246' AND sku = 'MA746HL06DQVLAVEN-65753';
UPDATE tbl_order_detail t SET unit_price = 182, unit_price_after_vat=162, shipping_fee = 0, paid_price = 182, paid_price_after_vat = 162 WHERE order_nr = '200083246' AND sku = 'MA746HL06DQVLAVEN-65753';
UPDATE tbl_order_detail t SET unit_price = 182, unit_price_after_vat=162, shipping_fee = 0, paid_price = 182, paid_price_after_vat = 162 WHERE order_nr = '200083246' AND sku = 'MA746HL06DQVLAVEN-65753';
UPDATE tbl_order_detail t SET unit_price = 182, unit_price_after_vat=162, shipping_fee = 0, paid_price = 182, paid_price_after_vat = 162 WHERE order_nr = '200083246' AND sku = 'MA746HL06DQVLAVEN-65753';
UPDATE tbl_order_detail t SET unit_price = 619, unit_price_after_vat=553, shipping_fee = 0, paid_price = 619, paid_price_after_vat = 553 WHERE order_nr = '200083246' AND sku = 'MA746HL90DRLLAVEN-65769';
UPDATE tbl_order_detail t SET unit_price = 5570, unit_price_after_vat=4973, shipping_fee = 0, paid_price = 5570, paid_price_after_vat = 4973 WHERE order_nr = '200083646' AND sku = 'BL657EL33GDELAVEN-67430';
UPDATE tbl_order_detail t SET unit_price = 3055, unit_price_after_vat=2728, shipping_fee = 0, paid_price = 3055, paid_price_after_vat = 2728 WHERE order_nr = '200083646' AND sku = 'BL657EL59KRKLAVEN-70552';
UPDATE tbl_order_detail t SET unit_price = 828, unit_price_after_vat=739, shipping_fee = 0, paid_price = 828, paid_price_after_vat = 739 WHERE order_nr = '200084646' AND sku = 'CA740HL44DDVLAVEN-65419';
UPDATE tbl_order_detail t SET unit_price = 3008, unit_price_after_vat=2686, shipping_fee = 0, paid_price = 3008, paid_price_after_vat = 2686 WHERE order_nr = '200084946' AND sku = 'ZT736EL67CVGLAVEN-65197';
UPDATE tbl_order_detail t SET unit_price = 191, unit_price_after_vat=171, shipping_fee = 0, paid_price = 191, paid_price_after_vat = 171 WHERE order_nr = '200085646' AND sku = 'OC741HL15DEYLAVEN-65448';
UPDATE tbl_order_detail t SET unit_price = 1548, unit_price_after_vat=1382, shipping_fee = 0, paid_price = 1548, paid_price_after_vat = 1382 WHERE order_nr = '200085846' AND sku = 'CO826EL59JUILAVEN-69953';
UPDATE tbl_order_detail t SET unit_price = 3055, unit_price_after_vat=2728, shipping_fee = 0, paid_price = 3055, paid_price_after_vat = 2728 WHERE order_nr = '200085946' AND sku = 'BL657EL59KRKLAVEN-70552';
UPDATE tbl_order_detail t SET unit_price = 3055, unit_price_after_vat=2728, shipping_fee = 0, paid_price = 3055, paid_price_after_vat = 2728 WHERE order_nr = '200085946' AND sku = 'BL657EL37AMWLAVEN-63612';
UPDATE tbl_order_detail t SET unit_price = 850, unit_price_after_vat=759, shipping_fee = 0, paid_price = 850, paid_price_after_vat = 759 WHERE order_nr = '200086646' AND sku = 'SO722EL57EICLAVEN-66205';
UPDATE tbl_order_detail t SET unit_price = 10190, unit_price_after_vat=9098, shipping_fee = 0, paid_price = 10190, paid_price_after_vat = 9098 WHERE order_nr = '200086846' AND sku = 'SA717EL36IITLAVEN-68976';
UPDATE tbl_order_detail t SET unit_price = 10190, unit_price_after_vat=9098, shipping_fee = 0, paid_price = 10190, paid_price_after_vat = 9098 WHERE order_nr = '200086946' AND sku = 'SA717EL36IITLAVEN-68976';
UPDATE tbl_order_detail t SET unit_price = 1424, unit_price_after_vat=1271, shipping_fee = 0, paid_price = 1424, paid_price_after_vat = 1271 WHERE order_nr = '200087646' AND sku = 'CA740HL66DCZLAVEN-65397';
UPDATE tbl_order_detail t SET unit_price = 5081, unit_price_after_vat=4537, shipping_fee = 0, paid_price = 5081, paid_price_after_vat = 4537 WHERE order_nr = '200087846' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET unit_price = 886, unit_price_after_vat=791, shipping_fee = 0, paid_price = 886, paid_price_after_vat = 791 WHERE order_nr = '200087946' AND sku = 'CA740HL82DCJLAVEN-65381';
UPDATE tbl_order_detail t SET unit_price = 358, unit_price_after_vat=320, shipping_fee = 0, paid_price = 358, paid_price_after_vat = 320 WHERE order_nr = '200088646' AND sku = 'OS854EL29JRQLAVEN-69883';
UPDATE tbl_order_detail t SET unit_price = 1907, unit_price_after_vat=1703, shipping_fee = 0, paid_price = 1907, paid_price_after_vat = 1703 WHERE order_nr = '200088846' AND sku = 'CA740HL53DDMLAVEN-65410';
UPDATE tbl_order_detail t SET unit_price = 3055, unit_price_after_vat=2728, shipping_fee = 0, paid_price = 3055, paid_price_after_vat = 2728 WHERE order_nr = '200088946' AND sku = 'BL657EL59KRKLAVEN-70552';
UPDATE tbl_order_detail t SET unit_price = 358, unit_price_after_vat=320, shipping_fee = 0, paid_price = 358, paid_price_after_vat = 320 WHERE order_nr = '200089646' AND sku = 'OS854EL29JRQLAVEN-69883';
UPDATE tbl_order_detail t SET unit_price = 358, unit_price_after_vat=320, shipping_fee = 0, paid_price = 358, paid_price_after_vat = 320 WHERE order_nr = '200089646' AND sku = 'OS854EL29JRQLAVEN-69883';
UPDATE tbl_order_detail t SET unit_price = 2039, unit_price_after_vat=1821, shipping_fee = 0, paid_price = 2039, paid_price_after_vat = 1821 WHERE order_nr = '200089846' AND sku = 'LG689EL16JVZLAVEN-69996';
UPDATE tbl_order_detail t SET unit_price = 1137, unit_price_after_vat=1015, shipping_fee = 0, paid_price = 1137, paid_price_after_vat = 1015 WHERE order_nr = '200089946' AND sku = 'CA740HL70DCVLAVEN-65393';
UPDATE tbl_order_detail t SET unit_price = 886, unit_price_after_vat=791, shipping_fee = 0, paid_price = 886, paid_price_after_vat = 791 WHERE order_nr = '200089946' AND sku = 'CA740HL83DCILAVEN-65380';
UPDATE tbl_order_detail t SET unit_price = 8237, unit_price_after_vat=7354, shipping_fee = 0, paid_price = 8237, paid_price_after_vat = 7354 WHERE order_nr = '200091646' AND sku = 'SA717EL29ANELAVEN-63620';
UPDATE tbl_order_detail t SET unit_price = 473, unit_price_after_vat=422, shipping_fee = 0, paid_price = 473, paid_price_after_vat = 422 WHERE order_nr = '200091846' AND sku = 'TO912TB70LKFLAVEN-71076';
UPDATE tbl_order_detail t SET unit_price = 5081, unit_price_after_vat=4537, shipping_fee = 0, paid_price = 5081, paid_price_after_vat = 4537 WHERE order_nr = '200091846' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET unit_price = 78, unit_price_after_vat=70, shipping_fee = 0, paid_price = 78, paid_price_after_vat = 70 WHERE order_nr = '200091846' AND sku = 'AD731EL92OELLAVEN-72954';
UPDATE tbl_order_detail t SET unit_price = 159, unit_price_after_vat=142, shipping_fee = 0, paid_price = 159, paid_price_after_vat = 142 WHERE order_nr = '200091946' AND sku = 'PR822HL00HFJLAVEN-68165';
UPDATE tbl_order_detail t SET unit_price = 289, unit_price_after_vat=258, shipping_fee = 0, paid_price = 289, paid_price_after_vat = 258 WHERE order_nr = '200092646' AND sku = 'OS854EL00IKDLAVEN-69012';
UPDATE tbl_order_detail t SET unit_price = 1326, unit_price_after_vat=1184, shipping_fee = 0, paid_price = 1326, paid_price_after_vat = 1184 WHERE order_nr = '200092946' AND sku = 'SA717EL10IFXLAVEN-68902';
UPDATE tbl_order_detail t SET unit_price = 841, unit_price_after_vat=751, shipping_fee = 0, paid_price = 841, paid_price_after_vat = 751 WHERE order_nr = '200092946' AND sku = 'NO758HL64EHVLAVEN-66198';
UPDATE tbl_order_detail t SET unit_price = 369, unit_price_after_vat=329, shipping_fee = 0, paid_price = 369, paid_price_after_vat = 329 WHERE order_nr = '200093246' AND sku = 'MA746HL69DKOLAVEN-65590';
UPDATE tbl_order_detail t SET unit_price = 8237, unit_price_after_vat=7354, shipping_fee = 0, paid_price = 8237, paid_price_after_vat = 7354 WHERE order_nr = '200093646' AND sku = 'SA717EL62ATPLAVEN-63792';
UPDATE tbl_order_detail t SET unit_price = 3055, unit_price_after_vat=2728, shipping_fee = 0, paid_price = 3055, paid_price_after_vat = 2728 WHERE order_nr = '200093946' AND sku = 'BL657EL59KRKLAVEN-70552';
UPDATE tbl_order_detail t SET unit_price = 191, unit_price_after_vat=171, shipping_fee = 0, paid_price = 191, paid_price_after_vat = 171 WHERE order_nr = '200094646' AND sku = 'OC741HL14DEZLAVEN-65449';
UPDATE tbl_order_detail t SET unit_price = 454, unit_price_after_vat=405, shipping_fee = 0, paid_price = 454, paid_price_after_vat = 405 WHERE order_nr = '200094946' AND sku = 'ZT736EL71CVCLAVEN-65193';
UPDATE tbl_order_detail t SET unit_price = 1827, unit_price_after_vat=1631, shipping_fee = 0, paid_price = 1827, paid_price_after_vat = 1631 WHERE order_nr = '200094946' AND sku = 'CA740HL46DDTLAVEN-65417';
UPDATE tbl_order_detail t SET unit_price = 351, unit_price_after_vat=313, shipping_fee = 0, paid_price = 351, paid_price_after_vat = 313 WHERE order_nr = '200095646' AND sku = 'CO748HL44DTFLAVEN-65815';
UPDATE tbl_order_detail t SET unit_price = 351, unit_price_after_vat=313, shipping_fee = 0, paid_price = 351, paid_price_after_vat = 313 WHERE order_nr = '200095646' AND sku = 'CO748HL46DTDLAVEN-65813';
UPDATE tbl_order_detail t SET unit_price = 3055, unit_price_after_vat=2728, shipping_fee = 0, paid_price = 3055, paid_price_after_vat = 2728 WHERE order_nr = '200095846' AND sku = 'BL657EL37AMWLAVEN-63612';
UPDATE tbl_order_detail t SET unit_price = 2063, unit_price_after_vat=1842, shipping_fee = 0, paid_price = 2063, paid_price_after_vat = 1842 WHERE order_nr = '200095946' AND sku = 'GA766EL73FEOLAVEN-66790';
UPDATE tbl_order_detail t SET unit_price = 167, unit_price_after_vat=149, shipping_fee = 0, paid_price = 167, paid_price_after_vat = 149 WHERE order_nr = '200096646' AND sku = 'MI911TB76LJZLAVEN-71070';
UPDATE tbl_order_detail t SET unit_price = 2039, unit_price_after_vat=1821, shipping_fee = 0, paid_price = 2039, paid_price_after_vat = 1821 WHERE order_nr = '200096846' AND sku = 'LG689EL16JVZLAVEN-69996';
UPDATE tbl_order_detail t SET unit_price = 5081, unit_price_after_vat=4537, shipping_fee = 0, paid_price = 5081, paid_price_after_vat = 4537 WHERE order_nr = '200096946' AND sku = 'SA717EL68BEXLAVEN-64086';
UPDATE tbl_order_detail t SET unit_price = 3055, unit_price_after_vat=2728, shipping_fee = 0, paid_price = 3055, paid_price_after_vat = 2728 WHERE order_nr = '200096946' AND sku = 'BL657EL37AMWLAVEN-63612';
UPDATE tbl_order_detail t SET unit_price = 403, unit_price_after_vat=360, shipping_fee = 0, paid_price = 403, paid_price_after_vat = 360 WHERE order_nr = '200097646' AND sku = 'MA746HL92DRJLAVEN-65767';
UPDATE tbl_order_detail t SET unit_price = 3055, unit_price_after_vat=2728, shipping_fee = 0, paid_price = 3055, paid_price_after_vat = 2728 WHERE order_nr = '200097846' AND sku = 'BL657EL59KRKLAVEN-70552';
UPDATE tbl_order_detail t SET unit_price = 2063, unit_price_after_vat=1842, shipping_fee = 0, paid_price = 2063, paid_price_after_vat = 1842 WHERE order_nr = '200097946' AND sku = 'GA766EL73FEOLAVEN-66790';
UPDATE tbl_order_detail t SET unit_price = 560, unit_price_after_vat=500, shipping_fee = 0, paid_price = 560, paid_price_after_vat = 500 WHERE order_nr = '200098646' AND sku = 'OS854EL96IKHLAVEN-69016';
UPDATE tbl_order_detail t SET unit_price = 1201, unit_price_after_vat=1072, shipping_fee = 0, paid_price = 1201, paid_price_after_vat = 1072 WHERE order_nr = '200098646' AND sku = 'PR856HL25IJELAVEN-68987';
UPDATE tbl_order_detail t SET unit_price = 625, unit_price_after_vat=558, shipping_fee = 0, paid_price = 625, paid_price_after_vat = 558 WHERE order_nr = '200098846' AND sku = 'NI704EL19LAQLAVEN-70792';
UPDATE tbl_order_detail t SET unit_price = 5710, unit_price_after_vat=5098, shipping_fee = 0, paid_price = 5710, paid_price_after_vat = 5098 WHERE order_nr = '200098946' AND sku = 'SO722EL14LAVLAVEN-70797';
UPDATE tbl_order_detail t SET unit_price = 1506, unit_price_after_vat=1345, shipping_fee = 0, paid_price = 1506, paid_price_after_vat = 1345 WHERE order_nr = '200099646' AND sku = 'SO722EL40KZVLAVEN-70771';
UPDATE tbl_order_detail t SET unit_price = 8237, unit_price_after_vat=7354, shipping_fee = 0, paid_price = 8237, paid_price_after_vat = 7354 WHERE order_nr = '200099846' AND sku = 'SA717EL29ANELAVEN-63620';
UPDATE tbl_order_detail t SET unit_price = 841, unit_price_after_vat=751, shipping_fee = 0, paid_price = 841, paid_price_after_vat = 751 WHERE order_nr = '200099946' AND sku = 'NO758HL64EHVLAVEN-66198';

#Cupones
UPDATE tbl_order_detail t SET coupon_money_value = 0, coupon_money_after_vat=0, shipping_fee = 0, paid_price = 27619, paid_price_after_vat = 24660 WHERE order_nr = '200065896' AND sku = 'LG689EL35MUELAVEN-72011' AND DATE = '2013-02-13' ;
UPDATE tbl_order_detail t SET coupon_money_value = 740, coupon_money_after_vat=661, shipping_fee = 740, paid_price = 1658, paid_price_after_vat = 1481 WHERE order_nr = '200014886' AND sku = 'CA864EL32KDBLAVEN-70180' AND DATE = '2013-02-21' ;
UPDATE tbl_order_detail t SET coupon_money_value = 740, coupon_money_after_vat=661, shipping_fee = 740, paid_price = 1658, paid_price_after_vat = 1481 WHERE order_nr = '200014886' AND sku = 'CA864EL30KDDLAVEN-70182' AND DATE = '2013-02-21' ;
UPDATE tbl_order_detail t SET coupon_money_value = 28, coupon_money_after_vat=25, shipping_fee = 28, paid_price = 541, paid_price_after_vat = 483 WHERE order_nr = '200083886' AND sku = 'CO748HL47DTCLAVEN-65812' AND DATE = '2013-02-22' ;
UPDATE tbl_order_detail t SET coupon_money_value = 437, coupon_money_after_vat=391, shipping_fee = 437, paid_price = 8312, paid_price_after_vat = 7421 WHERE order_nr = '200083886' AND sku = 'SA717EL25QMSLAVEN-74525' AND DATE = '2013-02-22' ;
UPDATE tbl_order_detail t SET coupon_money_value = 521, coupon_money_after_vat=465, shipping_fee = 521, paid_price = 9898, paid_price_after_vat = 8838 WHERE order_nr = '200083886' AND sku = 'AP650EL59SUYLAVEN-76146' AND DATE = '2013-02-22' ;
UPDATE tbl_order_detail t SET coupon_money_value = 495, coupon_money_after_vat=442, shipping_fee = 495, paid_price = 9404, paid_price_after_vat = 8396 WHERE order_nr = '200083886' AND sku = 'AP650EL92NHJLAVEN-72354' AND DATE = '2013-02-22' ;
UPDATE tbl_order_detail t SET coupon_money_value = 1300, coupon_money_after_vat=1171, shipping_fee = 1300, paid_price = 12068, paid_price_after_vat = 10775 WHERE order_nr = '200044186' AND sku = 'TO726EL97QGCLAVEN-74353' AND DATE = '2013-02-25' ;
UPDATE tbl_order_detail t SET coupon_money_value = 810, coupon_money_after_vat=723, shipping_fee = 810, paid_price = 1589, paid_price_after_vat = 1419 WHERE order_nr = '200053586' AND sku = 'CA864EL31KDCLAVEN-70181' AND DATE = '2013-02-25' ;
UPDATE tbl_order_detail t SET coupon_money_value = 63, coupon_money_after_vat=56, shipping_fee = 63, paid_price = 1142, paid_price_after_vat = 1020 WHERE order_nr = '200083176' AND sku = 'SA717EL24HAPLAVEN-68040' AND DATE = '2013-03-08' ;
UPDATE tbl_order_detail t SET coupon_money_value = 62, coupon_money_after_vat=56, shipping_fee = 62, paid_price = 1132, paid_price_after_vat = 1011 WHERE order_nr = '200083176' AND sku = 'OS854EL01IKCLAVEN-69011' AND DATE = '2013-03-08' ;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`lorena.ramirez`@`%`*/ /*!50003 PROCEDURE `KPIs`(IN MonthNum INT)
BEGIN

/*
* KPI's calculations
*/
/*
*  Kpi Group: Main
*  Created By: BI Team
*  Created at: 2013-10-03
*  Lastest Updated: 2013-10-03
*/

/*
*  KPI Table Creation
*/
CREATE TABLE IF NOT EXISTS KPIs
(
  MonthNum    INT NOT NULL,
  SpreadSheet INT NOT NULL,
  `Group`     VARCHAR(100) NOT NULL,
  RowId       INT NOT NULL,
  KPI         TEXT NOT NULL,
  Description TEXT NOT NULL,
  value       DECIMAL (15,5),
  Updated_at  DATETIME,
  PRIMARY KEY ( MonthNum , `Group`, RowId )
)
;

CALL KPI_ORDER_AND_CUSTOMER_DATA_INCL_MARKETPLACE(MonthNum);
CALL KPI_VOLUME_DRIVERS_INCL_MARKETPLACE(MonthNum);
#CALL KPI_BUYING_EFFICIENCY(MonthNum);
CALL KPI_OPERATIONS_EFFICIENCY(MonthNum);
CALL KPI_REVENUE_WATERFALL_INCL_MARKETPLACE(MonthNum);
CALL KPI_PL_ACCOUNTING_VIEW_NOT_CASH_VIEW(MonthNum);
CALL KPI_MARKETPLACE_REVENUE_AND_ORDER_WATERFALL(MonthNum);

CALL KPI_ORDER_REVENUE_DRIVERS(MonthNum);
CALL KPI_NET_PROMOTER_SCORE(MonthNum);

CALL KPI_MARKETING_MAIN();
CALL KPI_EXTRA(MonthNum);
CALL KPI_CONCENTRATION();


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `KPI_BUYING_EFFICIENCY`(IN KPI_MonthNum INT)
BEGIN
/*
*  Kpi Group:  BUYING EFFICIENCY
*  Created By: BI Team
*  Developer:  Carlos Antonio Mondragón Soria
*  Created at: 2013-10-03
*  Lastest Updated: 2013-10-03
*/
SET @KPI_Month=KPI_MonthNum;
SET @KPI_Year=SUBSTR( KPI_MonthNum FROM 1 FOR 4 );
SET @KPI_SpreadSheet = 1;
SET @KPI_UpdatedAt = Now();
SET @KPI_Group = "BUYING EFFICIENCY";

/*
* Sample Table Creations
*/
DROP TEMPORARY TABLE IF EXISTS out_order_tracking_Year;
CREATE TEMPORARY TABLE 	out_order_tracking_Year
SELECT * FROM production_ve.out_order_tracking 
WHERE 
    YEAR( date_shipped ) = @KPI_Year
AND status_wms <> 'cancelado'     
;

DROP TEMPORARY TABLE IF EXISTS A_Master_Year;
CREATE TEMPORARY TABLE A_Master_Year ( INDEX( ItemId ) )
SELECT 
   a.*, 
   b.fulfillment_type_bp ,
   date_shipped        
FROM          A_Master a
   INNER JOIN out_order_tracking_Year b
 ON a.ItemID = b.item_id 
WHERE
   YEAR( date ) = @KPI_Year;

DROP TEMPORARY TABLE IF EXISTS A_Master_Year_Totals;
CREATE TEMPORARY TABLE A_Master_Year_Totals ( INDEX( MonthNum ) )
SELECT MonthNum,
       SUM( IF( OrderAfterCan = 1, Rev , 0  )) AS Net_Rev
FROM          A_Master_Year a
WHERE
   YEAR( date ) = @KPI_Year
GROUP BY MonthNum
;	
   
DROP TEMPORARY TABLE IF EXISTS A_Master_Orders_Year;
CREATE TEMPORARY TABLE A_Master_Orders_Year 
SELECT * FROM Out_SalesReportOrder
WHERE
   YEAR( date ) = @KPI_Year;

DROP TEMPORARY TABLE IF EXISTS out_stock_hist_Year;
CREATE TEMPORARY TABLE out_stock_hist_Year
SELECT * FROM production_ve.out_stock_hist 
WHERE
      YEAR(date_entrance)= @KPI_Year
   OR date_exit IS NULL 
   OR YEAR(date_exit ) = @KPI_Year
;

DROP TEMPORARY TABLE IF EXISTS KPI_inventory_age;
CREATE TEMPORARY TABLE KPI_inventory_age
SELECT * FROM A_Stock
WHERE MonthNum = @KPI_Month
;

DROP TEMPORARY TABLE IF EXISTS out_procurement_tracking_Year;
CREATE TEMPORARY TABLE out_procurement_tracking_Year
SELECT * FROM production_pe.out_procurement_tracking
WHERE
   YEAR(date_goods_received) = @KPI_Year
   AND is_deleted = 0
   AND is_cancelled = 0
;

/*
* KPIs calculations
*/
#Inventory type (share of sales) - Outright buying (in %)
REPLACE KPIs
SELECT 
   A_Master_Year.MonthNum    AS MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group  AS`Group`,
   108         AS RowID,
   "Inventory type (share of sales) - Outright buying (in %)"                                       AS KPI,
   "Gross revenues before cancelations (DWH definition: Net Revenue before cancelations excl. VAT)" AS Description,
   SUM( IF(     A_Master_Year.OrderAfterCan = 1 
            AND A_Master_Year.fulfillment_type_bp="outright buying",
                A_Master_Year.Rev , 0 ) ) / A_Master_Year_Totals.Net_Rev AS Value,
   @KPI_UpdatedAt
FROM 
                A_Master_Year
	INNER JOIN  A_Master_Year_Totals
	     USING  ( MonthNum )
WHERE A_Master_Year.MonthNum = @KPI_Month
GROUP BY MonthNum;


#Inventory type (share of sales) - Consignment - stocked in own warehouse (in %)
REPLACE KPIs
SELECT 
   MonthNum   AS MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group AS `Group`,
   109        AS RowID,
   "Inventory type (share of sales) - Consignment - stocked in own warehouse (in %)"                AS KPI,
   "Gross revenues before cancelations (DWH definition: Net Revenue before cancelations excl. VAT)" AS Description,
   SUM( IF(     OrderAfterCan = 1 
            AND fulfillment_type_bp="consignment",
                A_Master_Year.Rev , 0 ) ) / A_Master_Year_Totals.Net_Rev AS Value,
   @KPI_UpdatedAt
FROM 
                A_Master_Year
	INNER JOIN  A_Master_Year_Totals
	     USING  ( MonthNum )
WHERE A_Master_Year.MonthNum = @KPI_Month
GROUP BY MonthNum;

#Inventory type (share of sales) - Cross-docking (in %)
REPLACE KPIs
SELECT 
   MonthNum    AS MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group  AS `Group`,
   110         AS RowID,
   "Inventory type (share of sales) - Cross-docking (in %)"                                         AS KPI,
   "Gross revenues before cancelations (DWH definition: Net Revenue before cancelations excl. VAT)" AS Description,
   SUM( IF(     OrderAfterCan = 1 
            AND fulfillment_type_bp="crossdocking",
                A_Master_Year.Rev , 0 ) ) / A_Master_Year_Totals.Net_Rev AS Value,
   @KPI_UpdatedAt
FROM 
                A_Master_Year
	INNER JOIN  A_Master_Year_Totals
	     USING  ( MonthNum )
WHERE A_Master_Year.MonthNum = @KPI_Month
GROUP BY MonthNum;

# 111
# Inventory type (share of sales) - Private Label (in %)

#Inventory type (share of sales) - Other (in %)
REPLACE KPIs
SELECT 
   MonthNum   AS MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group AS `Group`,
   112        AS RowID,
   "Inventory type (share of sales) - Other (in %)"                                                 AS KPI,
   "Gross revenues before cancelations (DWH definition: Net Revenue before cancelations excl. VAT)" AS Description,
   SUM( IF(     OrderAfterCan = 1 
            AND fulfillment_type_bp="other",
                A_Master_Year.Rev , 0 ) ) / A_Master_Year_Totals.Net_Rev AS Value,
   @KPI_UpdatedAt
FROM 
                A_Master_Year
	INNER JOIN  A_Master_Year_Totals
	     USING  ( MonthNum )
WHERE A_Master_Year.MonthNum = @KPI_Month
GROUP BY MonthNum;

# COGS by inventory type outright (in %)
REPLACE KPIs
SELECT 
   DATE_FORMAT(date_shipped, "%x%m")                 AS MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   114                                                 AS RowID, 
   "COGS by inventory type - Outright (in %)"          AS KPI,
   "COGS (incl. landed cost, port costs, customs and any "
   "other costs until goods arrive at the warehouse) as a"
   " % of original price (excl. VAT) - based on gross orders" AS Description,
   SUM( IF(     OrderBeforeCan =1
            AND fulfillment_type_bp="outright buying",
	            COGS  , 0))     /
   SUM( IF(     OrderBeforeCan =1
            AND fulfillment_type_bp="outright buying",
	            PriceAfterTax , 0)) AS Value,
   @KPI_UpdatedAt
FROM 
    A_Master_Year 
WHERE
   DATE_FORMAT(date_shipped, "%x%m") = @KPI_Month                 
GROUP BY MonthNum;

#COGS by inventory type - Consignment - stocked in own warehouse (in %)
REPLACE KPIs
SELECT 
   DATE_FORMAT(date_shipped, "%x%m") AS MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                        AS `Group`,
   115                                                                      AS RowID,
   "COGS by inventory type - Consignment - stocked in own warehouse (in %)" AS KPI,
   "COGS (incl. landed cost, port costs, customs and any other costs until "
   "goods arrive at the warehouse) as a % of original price (excl. VAT) - based on gross orders"            AS Description,
   SUM( IF(     OrderBeforeCan =1
            AND fulfillment_type_bp="consignment",
	            COGS  , 0))     /
   SUM( IF(     OrderBeforeCan =1
            AND fulfillment_type_bp="consignment",
                PriceAfterTax , 0)) AS Value,
   @KPI_UpdatedAt
FROM 
    A_Master_Year 
WHERE DATE_FORMAT(date_shipped, "%x%m")  = @KPI_Month
GROUP BY MonthNum;

#COGS by inventory type - Cross-docking (in %)
REPLACE KPIs
SELECT 
   DATE_FORMAT(date_shipped, "%x%m")                   AS MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   116                                                 AS RowID,
   "COGS by inventory type - Cross-docking (in %)"     AS KPI,
   "COGS (incl. landed cost, port costs, customs and "
   "any other costs until goods arrive at the warehouse) "
   "as a % of original price (excl. VAT) - based on gross orders" AS Description,
    SUM( IF(     OrderBeforeCan =1
            AND fulfillment_type_bp="crossdocking",
	            COGS  , 0))     /
   SUM( IF(     OrderBeforeCan =1
            AND fulfillment_type_bp="crossdocking",
                PriceAfterTax , 0)) AS Value,		  
   @KPI_UpdatedAt
FROM 
    A_Master_Year 
WHERE
    DATE_FORMAT(date_shipped, "%x%m") = @KPI_Month
GROUP BY MonthNum;

#COGS by inventory type - Private Label (in %)
REPLACE KPIs
SELECT 
   DATE_FORMAT(date_shipped, "%x%m")                   AS MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   117                                                 AS RowID,
   "COGS by inventory type - Private Label (in %)"     AS KPI,
   "COGS (incl. landed cost, port costs, customs and "
   "any other costs until goods arrive at the warehouse)"
   " as a % of original price (excl. VAT) - based on "
   "gross orders"  AS Description,
    SUM( IF(     OrderBeforeCan =1
            AND fulfillment_type_bp="",
	            COGS  , 0))     /
    SUM( IF(     OrderBeforeCan =1
            AND fulfillment_type_bp="",
                PriceAfterTax , 0)) AS Value,		  
   @KPI_UpdatedAt
FROM 
    A_Master_Year 
WHERE
   DATE_FORMAT(date_shipped, "%x%m") = @KPI_Month                   
GROUP BY MonthNum;

#COGS by inventory type - Other (in %)
REPLACE KPIs
SELECT 
   MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   118                                                 AS RowID,
   "COGS by inventory type - Other (in %)"             AS KPI,
   "COGS (incl. landed cost, port costs, customs and "
   "any other costs until goods arrive at the warehouse) "
   "as a % of original price (excl. VAT) - based on gross orders" AS Description,
    SUM( IF(     OrderBeforeCan =1
            AND fulfillment_type_bp="other",
	            COGS  , 0))     /
    SUM( IF(     OrderBeforeCan =1
            AND fulfillment_type_bp="other",
                PriceAfterTax , 0)) AS Value,		  
   @KPI_UpdatedAt
FROM 
    A_Master_Year 
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;


#Discount rate (in %)
REPLACE KPIs
SELECT 
   MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   119                                                 AS RowID,
   "Discount rate (in %)"             AS KPI,
   "Discounts / Net Revenues before discounts "
   "(see DWH definition below); Where applicable "
   "use Recommended Retail Price (RRP) as starting "
   "point for discount calculation" AS Description,
   ( ( SUM(OriginalPrice) - SUM(Price) ) ) / SUM(OriginalPrice) AS Value,
   @KPI_UpdatedAt
FROM 
    A_Master_Year 
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;


#Revenue per category (in k local) - 
SET @index=121;
SET @indexIni=121;
SET @monthNumAct=0;
SET @KPI = "COGS by category - ";
SET @KPI_Description = "COGS (incl. landed cost, port costs, customs and "
                       "any other costs until goods arrive at the warehouse) "
                       "as a % of original price (excl. VAT)";

REPLACE KPIs
SELECT
   MonthNum,
   SpreadSheet,
   `Group`,
   RowId,
   KPI,
   descripcion,
   Value,
   KPI_UpdatedAt
FROM
(
SELECT
   KPI.MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group               AS `Group`,
   @index := IF( @monthNumAct != KPI.MonthNum , @indexIni ,
                 @index + 1 ) AS RowId,
   @monthNumAct := KPI.MonthNum ,
   CONCAT( @KPI , KPI.CatKPI )  AS KPI,
   @KPI_Description  AS descripcion,
   KPI.Value,
   @KPI_UpdatedAt AS KPI_UpdatedAt
FROM
(
   SELECT
      @KPI_Month AS MonthNum,
      development_mx.M_CategoryKPI.CatKPI AS CatKPI,
      COALESCE(TMP.Value,0) AS Value,
      KPIorder 
   FROM
      development_mx.M_CategoryKPI
      LEFT JOIN
      (
         SELECT 
            MonthNum, 
            CatKPI,
            SUM( IF( OrderBeforeCan =1,
	                 COGS  , 0))     /
            SUM( IF( OrderBeforeCan =1,
                     PriceAfterTax , 0)) AS Value		 
        FROM 
           A_Master_Year 
        WHERE 
           MonthNum = @KPI_Month
        GROUP BY MonthNum, CatKPI
      ) AS TMP 
      ON     TMP.CatKPI = development_mx.M_CategoryKPI.CatKPI
   GROUP BY MonthNum , CONCAT( @KPI , TMP.CatKPI )
   ORDER BY
      MonthNum, development_mx.M_CategoryKPI.KPIorder
) AS KPI
ORDER BY
   MonthNum, KPIorder
) AS Final
;

# Payment terms per FT Outright
REPLACE KPIs
SELECT 
   @KPI_Month, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   137                                                 AS RowID,
   "Payment terms - Outright (in days)"                    AS KPI,
   "Actual days payable (i.e. not negotiated) that include"
   " shipping, internal processes (e.g. production process),"
   " etc."                                                 AS Description,
   sum( TMP.producto)/sum(TMP.suma)     AS Value,

   @KPI_UpdatedAt
FROM (
         SELECT
            date_format(date_goods_received, "%x%m") as MonthNum,
            fulfillment_type_bp,
            supplier_name,
            sum(cost_oms)*avg(payment_terms) as producto,
            sum(cost_oms) as suma 
         FROM
            production_ve.out_procurement_tracking
         WHERE
                fulfillment_type_bp = "consignment" 
            AND date_format(date_goods_received, "%x%m") = @KPI_Month
         GROUP BY MonthNum
      ) AS TMP
GROUP BY MonthNum

;

#Payment terms - Consignment (in days)
REPLACE KPIs
SELECT 
   @KPI_Month, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   138                                                 AS RowID,
   "Payment terms - Consignment (in days)"             AS KPI,
   "Actual days payable (i.e. not negotiated) that "
   "include shipping, internal processes (e.g. production "
   "process), etc." AS Description,
   sum( TMP.producto)/sum(TMP.suma)     AS Value,

   @KPI_UpdatedAt

FROM (
         SELECT
            date_format(date_goods_received, "%x%m") as MonthNum,
            fulfillment_type_bp,
            supplier_name,
            sum(cost_oms)*avg(payment_terms) as producto,
            sum(cost_oms) as suma 
         FROM
            production_ve.out_procurement_tracking
         WHERE
                fulfillment_type_bp="consignment" 
            AND date_format(date_goods_received, "%x%m") = @KPI_Month
         GROUP BY MonthNum
      ) AS TMP
GROUP BY MonthNum

;

#Payment terms - Cross-docking (in days)
REPLACE KPIs
SELECT 
   @KPI_Month, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   139                                                 AS RowID,
   "Payment terms - Cross-docking (in days)"           AS KPI,
   "Actual days payable (i.e. not negotiated) that "
   "include shipping, internal processes (e.g. production "
   "process), etc." AS Description,
   sum( TMP.producto)/sum(TMP.suma)     AS Value,
   @KPI_UpdatedAt

FROM (
         SELECT
            date_format(date_goods_received, "%x%m") as MonthNum,
            fulfillment_type_bp,
            supplier_name,
            sum(cost_oms)*avg(payment_terms) as producto,
            sum(cost_oms) as suma 
         FROM
            production_ve.out_procurement_tracking
         WHERE
                fulfillment_type_bp="crossdocking"
            AND date_format(date_goods_received, "%x%m") = @KPI_Month
         GROUP BY MonthNum
      ) AS TMP
GROUP BY MonthNum

;

#Payment terms - Private Label (in days)

# Payment terms per FT otherREPLACE KPIs
REPLACE KPIs
SELECT 
   @KPI_Month, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   141                                                 AS RowID,
   "Payment terms - Other (in days)"                   AS KPI,
   "Actual days payable (i.e. not negotiated) that "
   "include shipping, internal processes (e.g. production "
   "process), etc."                                    AS Description,
   sum( TMP.producto)/sum(TMP.suma)     AS Value,

   @KPI_UpdatedAt
FROM (
         SELECT
            date_format(out_order_tracking_Year.date_ordered, "%x%m") as MonthNum,
            out_order_tracking_Year.fulfillment_type_bp,
            out_procurement_tracking_Year.supplier_name,
            sum(out_procurement_tracking_Year.cost_oms)*avg(datediff(out_procurement_tracking_Year.date_paid, out_order_tracking_Year.date_delivered)) as producto,
            sum(out_procurement_tracking_Year.cost_oms) as suma 
         FROM
                       production_ve.out_procurement_tracking AS out_procurement_tracking_Year
            INNER JOIN production_ve.out_order_tracking AS out_order_tracking_Year
							   USING  ( item_id )
         WHERE
                out_order_tracking_Year.fulfillment_type_bp="other"
            AND date_format(out_order_tracking_Year.date_ordered, "%x%m") = @KPI_Month
         GROUP BY MonthNum

      ) AS TMP

GROUP BY MonthNum
;

# Inventory ageing - Share of owned inventory < 30 days old
REPLACE KPIs
SELECT 
   MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   143                                                 AS RowID,
   "Inventory ageing - Share of owned inventory < 30 days old"           AS KPI,
   "Purchase value of outright inventory added to warehouse "
   "less than 30 days ago / total purchase value of outright "
   "inventory" AS Description,
   SUM(IF( Days_inStock <= 30 ,1,0) )/COUNT(*)     AS Value,
   @KPI_UpdatedAt
FROM 
   KPI_inventory_age
WHERE
   MonthNum = @KPI_Month
GROUP BY MonthNum;

#Inventory ageing - Share of owned  inventory between 31 and 60 days old
REPLACE KPIs
SELECT 
   MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   144                                                 AS RowID,
   "Inventory ageing - Share of owned  inventory between 31 and 60 days old"           AS KPI,
   "Purchase value of outright inventory added to warehouse less than 30 "
   "days ago / total purchase value of outright inventory" AS Description,
   SUM(IF( Days_inStock BETWEEN 31 AND 60 ,1,0) )/COUNT(*)     AS Value,
   @KPI_UpdatedAt
FROM 
   KPI_inventory_age
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;

#Inventory ageing - Share of owned  inventory between 61 and 90 days old
REPLACE KPIs
SELECT 
   MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   145                                                 AS RowID,
   "Inventory ageing - Share of owned  inventory between 91 and 120 days old"           AS KPI,
   "Purchase value of outright inventory added to warehouse "
   "less than 30 days ago / total purchase value of outright "
   "inventory" AS Description,
   SUM(IF( Days_inStock BETWEEN 61 AND 90 ,1,0) )/COUNT(*)     AS Value,
   @KPI_UpdatedAt
FROM 
   KPI_inventory_age
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;

#Inventory ageing - Share of owned  inventory between 91 and 120 days old
REPLACE KPIs
SELECT 
   MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   146                                                 AS RowID,
   "Inventory ageing - Share of owned  inventory between 91 and 120 days old"           AS KPI,
   "Purchase value of outright inventory added to warehouse less than 30 "
   "days ago / total purchase value of outright inventory" AS Description,
   SUM(IF( Days_inStock BETWEEN 91 AND 120 ,1,0) )/COUNT(*)     AS Value,
   @KPI_UpdatedAt
FROM 
   KPI_inventory_age
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;

#Inventory ageing - Share of owned  inventory more than 120 days old
REPLACE KPIs
SELECT 
   MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   147                                                 AS RowID,
   "Inventory ageing - Share of owned  inventory more than 120 days old"           AS KPI,
   "Purchase value of outright inventory added to warehouse less than 30 "
   "days ago / total purchase value of outright inventory" AS Description,
   SUM(IF( Days_inStock > 120 ,1,0) )/COUNT(*)     AS Value,
   @KPI_UpdatedAt
FROM 
   KPI_inventory_age
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;


REPLACE KPIs
SELECT 
   @MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                          AS `Group`,
   152                                                 AS RowID,
   "Share of live SKU configs on sale/discount (in %)"           AS KPI,
   "Number of SKUs on sale / total # of SKUs"	AS Description,
   DISCOUNTS / DISTINCTS     AS Value,
   @KPI_UpdatedAt
FROM 
    (SELECT 
        count(DISTINCT CONFIG) DISTINCTS
    FROM
        (SELECT 
        `A_Master_Catalog`.`sku_config` AS CONFIG,
            (`A_Master_Catalog`.`isvisible`) VISIBLE,
            `A_Master_Catalog`.`special_from_date`,
            `A_Master_Catalog`.`special_to_date`,
            IF((`A_Master_Catalog`.`special_to_date`) >= CAST(DATE_FORMAT(CURDATE() - INTERVAL 1 MONTH, '%Y-%m-01')
                as DATE), 1, 0)
    FROM
        A_Master_Catalog) R
    WHERE
        VISIBLE = 1) T
        INNER JOIN
    (SELECT 
        count(DISTINCT CONFIG) as DISCOUNTS
    FROM
        (SELECT 
        `A_Master_Catalog`.`sku_config` AS CONFIG,
            (`A_Master_Catalog`.`isvisible`) VISIBLE,
            `A_Master_Catalog`.`special_from_date`,
            `A_Master_Catalog`.`special_to_date`,
            IF((`A_Master_Catalog`.`special_to_date`) >= CAST(DATE_FORMAT(CURDATE() - INTERVAL 1 MONTH, '%Y-%m-01')
                as DATE), 1, 0) DISC
    FROM
        A_Master_Catalog) R
    WHERE
        VISIBLE = 1 AND DISC = 1) Y
#WHERE MonthNum = @KPI_Month 
;



END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `KPI_CONCENTRATION`()
BEGIN

set @i=0;

replace into KPIs select  a.yrmonth, 3,'CONCENTRATION ANALYSES', 13, 'Top 10 best selling SKUs', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, sku, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, sku, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, sku order by yrmonth, net_revenue desc)a group by yrmonth, sku order by yrmonth, net_revenue desc)b where concat<=10 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into KPIs select  a.yrmonth, 3,'CONCENTRATION ANALYSES', 14, 'Top 11-30 best selling SKUs', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, sku, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, sku, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, sku order by yrmonth, net_revenue desc)a group by yrmonth, sku order by yrmonth, net_revenue desc)b where concat between 11 and 30 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into KPIs select  a.yrmonth, 3,'CONCENTRATION ANALYSES', 15, 'Top 31-50 best selling SKUs', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, sku, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, sku, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, sku order by yrmonth, net_revenue desc)a group by yrmonth, sku order by yrmonth, net_revenue desc)b where concat between 31 and 50 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into KPIs select  a.yrmonth, 3,'CONCENTRATION ANALYSES', 16, 'Top 51-100 best selling SKUs', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, sku, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, sku, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, sku order by yrmonth, net_revenue desc)a group by yrmonth, sku order by yrmonth, net_revenue desc)b where concat between 51 and 100 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into KPIs select  a.yrmonth, 3,'CONCENTRATION ANALYSES', 17, 'Top 101-1000 best selling SKUs', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, sku, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, sku, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, sku order by yrmonth, net_revenue desc)a group by yrmonth, sku order by yrmonth, net_revenue desc)b where concat between 101 and 1000 group by yrmonth)b where a.yrmonth=b.yrmonth;


replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 18, 'Top >1000 best selling SKUs', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, sku, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, sku, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, sku order by yrmonth, net_revenue desc)a group by yrmonth, sku order by yrmonth, net_revenue desc)b where concat >1000 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 21, 'Top 10 best selling SKUs', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, sku, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, sku, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, sku order by yrmonth, net_revenue desc)a group by yrmonth, sku order by yrmonth, net_revenue desc)b where concat<=10 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 22, 'Top 20 best selling SKUs', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, sku, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, sku, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, sku order by yrmonth, net_revenue desc)a group by yrmonth, sku order by yrmonth, net_revenue desc)b where concat<=20 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 23, 'Top 30 best selling SKUs', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, sku, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, sku, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, sku order by yrmonth, net_revenue desc)a group by yrmonth, sku order by yrmonth, net_revenue desc)b where concat<=30 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 24, 'Top 50 best selling SKUs', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, sku, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, sku, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, sku order by yrmonth, net_revenue desc)a group by yrmonth, sku order by yrmonth, net_revenue desc)b where concat<=50 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 25, 'Top 100 best selling SKUs', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, sku, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, sku, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, sku order by yrmonth, net_revenue desc)a group by yrmonth, sku order by yrmonth, net_revenue desc)b where concat<=100 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 26, 'Top 1000 best selling SKUs', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, sku, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, sku, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, sku order by yrmonth, net_revenue desc)a group by yrmonth, sku order by yrmonth, net_revenue desc)b where concat<=1000 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 28, 'Top 5 best selling brands', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, brand, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, brand, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, brand order by yrmonth, net_revenue desc)a group by yrmonth, brand order by yrmonth, net_revenue desc)b where concat<=5 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 29, 'Top 6-10 best selling brands', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, brand, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, brand, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, brand order by yrmonth, net_revenue desc)a group by yrmonth, brand order by yrmonth, net_revenue desc)b where concat between 6 and 10 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 30, 'Top 11-30 best selling brands', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, brand, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, brand, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, brand order by yrmonth, net_revenue desc)a group by yrmonth, brand order by yrmonth, net_revenue desc)b where concat between 11 and 30 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 31, 'Top 31-50 best selling brands', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, brand, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, brand, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, brand order by yrmonth, net_revenue desc)a group by yrmonth, brand order by yrmonth, net_revenue desc)b where concat between 31 and 50 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 32, 'Top >50 best selling brands', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, brand, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, brand, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, brand order by yrmonth, net_revenue desc)a group by yrmonth, brand order by yrmonth, net_revenue desc)b where concat>50 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 35, 'Top 5 best selling brands', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, brand, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, brand, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, brand order by yrmonth, net_revenue desc)a group by yrmonth, brand order by yrmonth, net_revenue desc)b where concat<=5 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 36, 'Top 10 best selling brands', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, brand, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, brand, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, brand order by yrmonth, net_revenue desc)a group by yrmonth, brand order by yrmonth, net_revenue desc)b where concat<=10 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 37, 'Top 20 best selling brands', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, brand, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, brand, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, brand order by yrmonth, net_revenue desc)a group by yrmonth, brand order by yrmonth, net_revenue desc)b where concat<=20 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 38, 'Top 30 best selling brands', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, brand, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, brand, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, brand order by yrmonth, net_revenue desc)a group by yrmonth, brand order by yrmonth, net_revenue desc)b where concat<=30 group by yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 39, 'Top 50 best selling brands', '% of total net revenue generated by SKUs', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, brand, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, brand, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, brand order by yrmonth, net_revenue desc)a group by yrmonth, brand order by yrmonth, net_revenue desc)b where concat<=50 group by yrmonth)b where a.yrmonth=b.yrmonth;

create table  temporary_kpi as
select yrmonth, round(count(distinct custid)*0.1) as percent1, round(count(distinct custid)*0.2) as percent2, round(count(distinct custid)*0.3) as percent3, round(count(distinct custid)*0.4) as percent4, round(count(distinct custid)*0.5) as percent5 from production_ve.tbl_order_detail t where oac=1 group by yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 41, 'Top 10% of best purchasing customers', '% of total net revenue generated by customer group', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select b.yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, custid, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, custid, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, custid order by yrmonth, net_revenue desc)a group by yrmonth, custid order by yrmonth, net_revenue desc)b,  temporary_kpi t where concat<=t.percent1 and t.yrmonth=b.yrmonth group by b.yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 42, 'Next 10% of best purchasing customers', '% of total net revenue generated by customer group', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select b.yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, custid, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, custid, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, custid order by yrmonth, net_revenue desc)a group by yrmonth, custid order by yrmonth, net_revenue desc)b,  temporary_kpi t where concat between t.percent1 and percent2 and t.yrmonth=b.yrmonth group by b.yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 43, 'Next 10% of best purchasing customers', '% of total net revenue generated by customer group', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select b.yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, custid, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, custid, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, custid order by yrmonth, net_revenue desc)a group by yrmonth, custid order by yrmonth, net_revenue desc)b,  temporary_kpi t where concat between t.percent2 and percent3 and t.yrmonth=b.yrmonth group by b.yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 44, 'Next 10% of best purchasing customers', '% of total net revenue generated by customer group', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select b.yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, custid, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, custid, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, custid order by yrmonth, net_revenue desc)a group by yrmonth, custid order by yrmonth, net_revenue desc)b,  temporary_kpi t where concat between t.percent3 and percent4 and t.yrmonth=b.yrmonth group by b.yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 45, 'Rest', '% of total net revenue generated by customer group', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select b.yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, custid, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, custid, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, custid order by yrmonth, net_revenue desc)a group by yrmonth, custid order by yrmonth, net_revenue desc)b,  temporary_kpi t where concat>percent4 and t.yrmonth=b.yrmonth group by b.yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 48, 'Top 10% of best purchasing customers', '% of total net revenue generated by customer group', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select b.yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, custid, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, custid, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, custid order by yrmonth, net_revenue desc)a group by yrmonth, custid order by yrmonth, net_revenue desc)b,  temporary_kpi t where concat<=percent1 and t.yrmonth=b.yrmonth group by b.yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 49, 'Top 20% of best purchasing customers', '% of total net revenue generated by customer group', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select b.yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, custid, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, custid, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, custid order by yrmonth, net_revenue desc)a group by yrmonth, custid order by yrmonth, net_revenue desc)b,  temporary_kpi t where concat<=percent2 and t.yrmonth=b.yrmonth group by b.yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 50, 'Top 30% of best purchasing customers', '% of total net revenue generated by customer group', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select b.yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, custid, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, custid, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, custid order by yrmonth, net_revenue desc)a group by yrmonth, custid order by yrmonth, net_revenue desc)b,  temporary_kpi t where concat<=percent3 and t.yrmonth=b.yrmonth group by b.yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 51, 'Top 40% of best purchasing customers', '% of total net revenue generated by customer group', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select b.yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, custid, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, custid, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, custid order by yrmonth, net_revenue desc)a group by yrmonth, custid order by yrmonth, net_revenue desc)b,  temporary_kpi t where concat<=percent4 and t.yrmonth=b.yrmonth group by b.yrmonth)b where a.yrmonth=b.yrmonth;

replace into  KPIs select  a.yrmonth, 3, 'CONCENTRATION ANALYSES', 52, 'Top 50% of best purchasing customers', '% of total net revenue generated by customer group', (b.net_revenue/a.net_revenue), now() from (select yrmonth, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth)a, (select b.yrmonth, sum(net_revenue) as net_revenue from (select yrmonth, custid, sum(net_revenue) as net_revenue, IF( yrmonth != @concen , @i := 1, @i := @i + 1 ) AS concat, @concen := yrmonth from (select yrmonth, custid, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) as net_revenue from production_ve.tbl_order_detail where oac=1 group by yrmonth, custid order by yrmonth, net_revenue desc)a group by yrmonth, custid order by yrmonth, net_revenue desc)b,  temporary_kpi t where concat<=percent5 and t.yrmonth=b.yrmonth group by b.yrmonth)b where a.yrmonth=b.yrmonth;

drop table  temporary_kpi;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`lorena.ramirez`@`%`*/ /*!50003 PROCEDURE `KPI_CONCENTRATION_ANALYSES`( IN KPI_MonthNum INT)
BEGIN
SET @KPI_Month=KPI_MonthNum;
SET @KPI_SpreadSheet = 3;
SET @KPI_UpdatedAt = Now();
SET @KPI_Group = 'CONCENTRATION ANALYSES';

/****************************************/
-- SKUs separate buckets
/****************************************/
REPLACE KPIs (MonthNum,`Group`,RowId,KPI,Description,value,Updated_at, SpreadSheet)
SELECT
@KPI_Month,
 @KPI_Group,
CASE WHEN rango = 10 THEN 13
     WHEN rango = 11 THEN 14
     WHEN rango = 31 THEN 15
     WHEN rango = 51 THEN 16
     WHEN rango = 101 THEN 17
     WHEN rango = 1000 THEN 18
END AS rowId,
CASE WHEN rango = 10 THEN 'Top 10 best selling SKUs'
     WHEN rango = 11 THEN 'Top 11-30 best selling SKUs'
     WHEN rango = 31 THEN 'Top 31-50 best selling SKUs'
     WHEN rango = 51 THEN 'Top 51-100 best selling SKUs'
     WHEN rango = 101 THEN 'Top 101-1000 best selling SKUs'
     WHEN rango = 1000 THEN 'Top >1000 best selling SKUs'
END as KPI,
'% of total net revenue generated by SKUs' AS description, 
SUM(ventas)/total AS value,
 @KPI_UpdatedAt,
@KPI_SpreadSheet
FROM
(
SELECT SKUConfig, ventas, @rank := @rank +1 AS rank, 
       CASE WHEN @rank BETWEEN 1 AND 10 THEN 10 
            WHEN @rank BETWEEN 11 AND 30 THEN 11
            WHEN @rank BETWEEN 31 AND 50 THEN 31
            WHEN @rank BETWEEN 51 AND 100 THEN 51
            WHEN @rank BETWEEN 101 AND 1000 THEN 101
       ELSE 1000
       END AS rango, c.total
FROM 
(select SKUConfig, sum(Rev) ventas
from A_Master 
where OrderAfterCan = 1 and MonthNum = @KPI_Month
group by SKUConfig
order by ventas desc) a, (SELECT @rank:=0) b, 
(SELECT sum(Rev) AS total from A_Master 
where OrderAfterCan = 1 and MonthNum = @KPI_Month) c
) d
GROUP BY rango;

/****************************************/
-- SKUs cumulative buckets
/****************************************/
REPLACE KPIs (MonthNum,`Group`,RowId,KPI,Description,value,Updated_at, SpreadSheet)
SELECT
@KPI_Month,
@KPI_Group,
CASE WHEN rango = 10 THEN 21
     WHEN rango = 11 THEN 22
     WHEN rango = 31 THEN 23
     WHEN rango = 51 THEN 24
     WHEN rango = 101 THEN 25
     WHEN rango = 1000 THEN 26
END AS rowId,
CASE WHEN rango = 10 THEN 'Top 10 best selling SKUs'
     WHEN rango = 11 THEN 'Top 20 best selling SKUs'
     WHEN rango = 31 THEN 'Top 30 best selling SKUs'
     WHEN rango = 51 THEN 'Top 50 best selling SKUs'
     WHEN rango = 101 THEN 'Top 100 best selling SKUs'
     WHEN rango = 1000 THEN 'Top 1000 best selling SKUs'
END as KPI,
'% of total net revenue generated by SKUs' AS description, 
kpi as value,
 @KPI_UpdatedAt,
@KPI_SpreadSheet 
FROM 
(
SELECT rango, ventas, @cummulative:=@cummulative+ventas AS cummulative, @cummulative/total as kpi
FROM (
SELECT rango, SUM(ventas) as ventas, total
FROM
(
SELECT SKUConfig, ventas, @rank := @rank +1 AS rank, 
       CASE WHEN @rank BETWEEN 1 AND 10 THEN 10 
            WHEN @rank BETWEEN 11 AND 20 THEN 11
            WHEN @rank BETWEEN 21 AND 30 THEN 31
            WHEN @rank BETWEEN 31 AND 50 THEN 51
            WHEN @rank BETWEEN 51 AND 100 THEN 101
            WHEN @rank BETWEEN 101 AND 1000 THEN 1000
       END AS rango, c.total
FROM 
(select SKUConfig, sum(Rev) ventas
from A_Master 
where OrderAfterCan = 1 and MonthNum = @KPI_Month
group by SKUConfig
order by ventas desc) a, (SELECT @rank:=0) b, 
(SELECT sum(Rev) AS total from A_Master 
where OrderAfterCan = 1 and MonthNum = @KPI_Month) c
) d
WHERE rango IS NOT NULL
GROUP BY rango) e, (SELECT @cummulative:=0) f) g;

/****************************************/
-- BRANDS
/****************************************/
REPLACE KPIs (MonthNum,`Group`,RowId,KPI,Description,value,Updated_at, SpreadSheet)
SELECT
@KPI_Month,
@KPI_Group,
CASE WHEN rango = 1 THEN 28
     WHEN rango = 6 THEN 29
     WHEN rango = 11 THEN 30
     WHEN rango = 31 THEN 31
     WHEN rango = 50 THEN 32
END AS rowId,
CASE WHEN rango = 1 THEN 'Top 5 best selling brands'
     WHEN rango = 6 THEN 'Top 6-10 best selling brands'
     WHEN rango = 11 THEN 'Top 11-30 best selling brands'
     WHEN rango = 31 THEN 'Top 31-50 best selling brands'
     WHEN rango = 50 THEN 'Top >50 best selling brands'
END as KPI,
'% of total net revenue generated by SKUs' AS description, 
SUM(ventas)/total as value,
 @KPI_UpdatedAt,
@KPI_SpreadSheet
FROM
(
SELECT brand, ventas, @rank := @rank +1 AS rank, 
       CASE WHEN @rank BETWEEN 1 AND 5 THEN 1 
            WHEN @rank BETWEEN 6 AND 10 THEN 6
            WHEN @rank BETWEEN 11 AND 30 THEN 11
            WHEN @rank BETWEEN 31 AND 50 THEN 31
       ELSE 50
       END AS rango, c.total
FROM (
select brand, sum(Rev) ventas
from A_Master 
where OrderAfterCan = 1 and MonthNum = @KPI_Month
group by brand
order by ventas desc) a, (SELECT @rank:=0) b, 
(SELECT sum(Rev) AS total from A_Master 
where OrderAfterCan = 1 and MonthNum = @KPI_Month) c
) d
GROUP BY rango;

/****************************************/
-- BRANDS cummulative
/****************************************/
REPLACE KPIs (MonthNum,`Group`,RowId,KPI,Description,value,Updated_at, SpreadSheet)
SELECT
@KPI_Month,
@KPI_Group,
CASE WHEN rango = 1 THEN 35
     WHEN rango = 6 THEN 36
     WHEN rango = 11 THEN 37
     WHEN rango = 21 THEN 38
     WHEN rango = 31 THEN 39
END AS rowId,
CASE WHEN rango = 1 THEN 'Top 5 best selling brands'
     WHEN rango = 6 THEN 'Top 10 best selling brands'
     WHEN rango = 11 THEN 'Top 20 best selling brands'
     WHEN rango = 21 THEN 'Top 30 best selling brands'
     WHEN rango = 31 THEN 'Top 50 best selling brands'
END as KPI,
'% of total net revenue generated by SKUs' AS description,
kpi as value,
 @KPI_UpdatedAt,
@KPI_SpreadSheet
FROM 
(
SELECT rango, ventas, @cummulative:=@cummulative+ventas AS cummulative, @cummulative/total as kpi
FROM (
SELECT rango, SUM(ventas) AS ventas, total
FROM
(
SELECT brand, ventas, @rank := @rank +1 AS rank, 
       CASE WHEN @rank BETWEEN 1 AND 5 THEN 1 
            WHEN @rank BETWEEN 6 AND 10 THEN 6
            WHEN @rank BETWEEN 11 AND 20 THEN 11
            WHEN @rank BETWEEN 21 AND 30 THEN 21
            WHEN @rank BETWEEN 31 AND 50 THEN 31
       END AS rango, c.total
FROM (
select brand, sum(Rev) ventas
from A_Master 
where OrderAfterCan = 1 and MonthNum = @KPI_Month
group by brand
order by ventas desc) a, (SELECT @rank:=0) b, 
(SELECT sum(Rev) AS total from A_Master 
where OrderAfterCan = 1 and MonthNum = @KPI_Month) c
) d
WHERE rango IS NOT NULL
GROUP BY rango) e, (SELECT @cummulative:=0) f) g;

/****************************************/
-- CUSTOMERS
/****************************************/
REPLACE KPIs (MonthNum,`Group`,RowId,KPI,Description,value,Updated_at, SpreadSheet)
SELECT
@KPI_Month,
@KPI_Group,
CASE WHEN rango = 10 THEN 41
     WHEN rango = 20 THEN 42
     WHEN rango = 30 THEN 43
     WHEN rango = 40 THEN 44
     WHEN rango = 50 THEN 45
END AS rowId,
CASE WHEN rango = 10 THEN 'Top 10% of best purchasing customers'
     WHEN rango = 20 THEN 'Next 10% of best purchasing customers'
     WHEN rango = 30 THEN 'Next 10% of best purchasing customers'
     WHEN rango = 40 THEN 'Next 10% of best purchasing customers'
     WHEN rango = 50 THEN 'Rest'
END as KPI,
'% of total net revenue generated by SKUs' AS description,
SUM(ventas)/total AS value,
 @KPI_UpdatedAt,
@KPI_SpreadSheet
FROM
(
SELECT CustomerNum, ventas, @rank := @rank +1 AS rank, 
       CASE WHEN @rank BETWEEN 1 AND ROUND(c.total/10) THEN 10 
            WHEN @rank BETWEEN ROUND((c.total/10))+1 AND ROUND((c.total/10))*2 THEN 20
            WHEN @rank BETWEEN ROUND((c.total/10))*2+1 AND ROUND((c.total/10))*3 THEN 30
            WHEN @rank BETWEEN ROUND((c.total/10))*3+1 AND ROUND((c.total/10))*4 THEN 40
       ELSE 50
       END AS rango, d.total
FROM (
select CustomerNum, sum(Rev) ventas
from A_Master 
where OrderAfterCan = 1 
and MonthNum = @KPI_Month
group by CustomerNum
order by ventas desc) a, (SELECT @rank:=0) b,
(SELECT COUNT(DISTINCT CustomerNum) AS total from A_Master 
where OrderAfterCan = 1 and MonthNum = @KPI_Month) c,
(SELECT sum(Rev) AS total from A_Master 
where OrderAfterCan = 1 and MonthNum = @KPI_Month) d
) e
GROUP BY rango;

/****************************************/
-- CUSTOMERS cummulative
/****************************************/
REPLACE KPIs (MonthNum,`Group`,RowId,KPI,Description,value,Updated_at, SpreadSheet)
SELECT
@KPI_Month,
@KPI_Group,
CASE WHEN rango = 10 THEN 48
     WHEN rango = 20 THEN 49
     WHEN rango = 30 THEN 50
     WHEN rango = 40 THEN 51
     WHEN rango = 50 THEN 52
END AS rowId,
CASE WHEN rango = 10 THEN 'Top 10% of best purchasing customers'
     WHEN rango = 20 THEN 'Top 20% of best purchasing customers'
     WHEN rango = 30 THEN 'Top 30% of best purchasing customers'
     WHEN rango = 40 THEN 'Top 40% of best purchasing customers'
     WHEN rango = 50 THEN 'Top 50% of best purchasing customers'
END as KPI,
'% of total net revenue generated by SKUs' AS description,
kpi as value,
 @KPI_UpdatedAt,
@KPI_SpreadSheet
FROM 
(
SELECT rango, ventas, @cummulative:=@cummulative+ventas AS cummulative, @cummulative/total as kpi
FROM (
SELECT rango, SUM(ventas) AS ventas, total
FROM
(
SELECT CustomerNum, ventas, @rank := @rank +1 AS rank, 
       CASE WHEN @rank BETWEEN 1 AND ROUND(c.total/10) THEN 10 
            WHEN @rank BETWEEN ROUND((c.total/10))+1 AND ROUND((c.total/10))*2 THEN 20
            WHEN @rank BETWEEN ROUND((c.total/10))*2+1 AND ROUND((c.total/10))*3 THEN 30
            WHEN @rank BETWEEN ROUND((c.total/10))*3+1 AND ROUND((c.total/10))*4 THEN 40
            WHEN @rank BETWEEN ROUND((c.total/10))*4+1 AND ROUND((c.total/10))*5 THEN 50
       END AS rango, d.total
FROM (
select CustomerNum, sum(Rev) ventas
from A_Master 
where OrderAfterCan = 1 
and MonthNum = @KPI_Month
group by CustomerNum
order by ventas desc) a, (SELECT @rank:=0) b,
(SELECT COUNT(DISTINCT CustomerNum) AS total from A_Master 
where OrderAfterCan = 1 and MonthNum = @KPI_Month) c,
(SELECT sum(Rev) AS total from A_Master 
where OrderAfterCan = 1 and MonthNum = @KPI_Month) d
) e
WHERE rango IS NOT NULL
GROUP BY rango) f, (SELECT @cummulative:=0) g) h;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `KPI_EXTRA`(in KPI_MonthNum int)
BEGIN

SET @KPI_Month:=KPI_MonthNum;
SET @KPI_SpreadSheet:=2;
SET @KPI_UpdatedAt:=now();

replace into   KPIs 
select 
concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), 2,
'PRODUCT PERFORMANCE KPIs', 
13, 
'Branded Traffic (in k #)', 
'Number of visits via direct URL typing or direct brand Search (SEM and SEO)', 
sum(visits)/1000, 
now() 
from 
SEM.campaign_ad_group_ve 
where 
 channel in ('Directo / Typed', 'Linio.com Referral', 'SEM Branded') group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));

replace into   KPIs 
select 
yrmonth, 2,'PRODUCT PERFORMANCE KPIs', 
15, 
'thereof SEM', 
'Total traffic via channel', 
sum(visits)/1000, 
now() 
from 
SEM.google_optimization_ad_group_ve 
where channel = 'SEM' group by yrmonth;

replace into   KPIs 
select 
concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))),
2, 
'PRODUCT PERFORMANCE KPIs', 
16, 
'thereof Affiliate', 
'Total traffic via channel', 
sum(visits)/1000, now() from SEM.campaign_ad_group_ve where (source_medium like '%affiliate%' or source_medium in ('pampanetwork.com / referral',
'pampa / siteunder',
'dscuento.com.mx / referral',
'promodescuentos.com / referral',
'ck.solocpm.com / referral',
'parentesis.com / referral',
'promodescuentos / affilaite',
'mainadv?utm_source=pampanetwork / Affiliates',
'cuponesmagicos.com.mx / referral',
'pampa / tablets',
'pampanetwork / (not set)'
)) group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));

replace into   KPIs select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))),2, 'PRODUCT PERFORMANCE KPIs', 
18, 
'thereof Re-targeting (e.g. Sociomantic)', 'Total traffic via channel', sum(visits)/1000, now() from SEM.campaign_ad_group_ve where (source_medium like '%retargeting%' or source_medium in ('adroll / banner',
'cas.sv.us.criteo.com / referral',
'cas.ny.us.criteo.com / referral',
'cas.criteo.com / referral',
'static.ny.us.criteo.net / referral',
'info.criteo.com / referral',
'Criteo / (not set)',
'triggit / 1',
'us-sonar.sociomantic.com / referral',
'vizury&utm_medium=retargeting&utm_content= / (not set)',
'fb.rtb.criteo.com / referral',
'queretarocity.olx.com.mx / referral',
'sociomantic / (not set)',
'sociomantic / retar',
'sociomantic / retarge'
)) group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));

replace into   KPIs select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))),2, 
'PRODUCT PERFORMANCE KPIs', 
22, 'thereof Other sources', 'Total traffic via channel', sum(visits)/1000, now() from SEM.campaign_ad_group_ve where ((source!='google' and medium!='cpc') and source not like '%bing%') AND campaign not like 'brandb%' and
channel != 'SEM' and (source_medium not like '%affiliate%' and source_medium not in ('pampanetwork.com / referral',
'pampa / siteunder',
'dscuento.com.mx / referral',
'promodescuentos.com / referral',
'ck.solocpm.com / referral',
'parentesis.com / referral',
'promodescuentos / affilaite',
'mainadv?utm_source=pampanetwork / Affiliates',
'cuponesmagicos.com.mx / referral',
'pampa / tablets',
'pampanetwork / (not set)'
)) and (source_medium not like '%retargeting%' or source_medium not in ('adroll / banner',
'cas.sv.us.criteo.com / referral',
'cas.ny.us.criteo.com / referral',
'cas.criteo.com / referral',
'static.ny.us.criteo.net / referral',
'info.criteo.com / referral',
'Criteo / (not set)',
'triggit / 1',
'us-sonar.sociomantic.com / referral',
'vizury&utm_medium=retargeting&utm_content= / (not set)',
'fb.rtb.criteo.com / referral',
'queretarocity.olx.com.mx / referral',
'sociomantic / (not set)',
'sociomantic / retar',
'sociomantic / retarge'
)) group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));

replace into   KPIs select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))),2, 'PRODUCT PERFORMANCE KPIs', 19, 'thereof Display w/o re-targeting', 'Total traffic via channel', sum(visits)/1000, now() from SEM.campaign_ad_group_ve where source_medium = 'google / cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%') group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));

replace into   KPIs select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), 2,'PRODUCT PERFORMANCE KPIs', 20, 'thereof Social Media Paid', 'Total traffic via channel', sum(visits)/1000, now() from SEM.campaign_ad_group_ve where source_medium = 'facebook / socialmedia' group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));


replace into   KPIs select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))),2, 'PRODUCT PERFORMANCE KPIs', 21, 'thereof CRM', 'Total traffic via channel', sum(visits)/1000, now() from SEM.campaign_ad_group_ve where source_medium = 'postal / crm' or source_medium like '%veinteractive%' or source_medium='%newsletter%' group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));


REPLACE KPIs (`Group`,KPI,MonthNum,value,RowId, SpreadSheet, Updated_at)
select 'PRODUCT PERFORMANCE KPIs' KPI_Group,
'Page impressions per visit (#)' KPI,
yrmonth, 
sum(impressions)/sum(visits) value,
26 RowId,
@KPI_SpreadSheet,
@KPI_UpdatedAt
from ga_mobile
where yrmonth = KPI_MonthNum
group by yrmonth desc;

REPLACE KPIs (`Group`,KPI,MonthNum,value,RowId, SpreadSheet, Updated_at)
select 'PRODUCT PERFORMANCE KPIs' KPI_Group,
'Page impressions per mobile visit (#)' KPI,
yrmonth, 
sum(impressions)/sum(visits) value,
27 RowId,
@KPI_SpreadSheet,
@KPI_UpdatedAt
from ga_mobile
where is_mobile = 1 and  yrmonth = KPI_MonthNum
group by yrmonth desc;
REPLACE KPIs (`Group`,KPI,MonthNum,value,RowId, SpreadSheet, Updated_at)
select 'PRODUCT PERFORMANCE KPIs' KPI_Group,
'CR mobile visits (%)' KPI,
yrmonth, 
sum(transactions)/sum(visits) value,
28 RowId,
@KPI_SpreadSheet,
@KPI_UpdatedAt
from ga_mobile
where is_mobile = 1
and  yrmonth = KPI_MonthNum
group by yrmonth desc;

REPLACE KPIs (`Group`,KPI,MonthNum,value,RowId, SpreadSheet, Updated_at)
select 'PRODUCT PERFORMANCE KPIs' KPI_Group,
'# of visits where on-site search was used' KPI,
yrmonth, 
sum(visits) value,
29 RowId,
@KPI_SpreadSheet,
@KPI_UpdatedAt
from ga_search_usage
where search_used = 'Visits With Site Search'
and  yrmonth = KPI_MonthNum
group by yrmonth desc;

REPLACE KPIs (`Group`,KPI,MonthNum,value,RowId, SpreadSheet, Updated_at)
#CR for visits with on-site search (%)
select 'PRODUCT PERFORMANCE KPIs' KPI_Group,
'CR for visits with on-site search (%)' KPI,
yrmonth, 
sum(transactions)/sum(visits) value,
31 RowId,
@KPI_SpreadSheet,
@KPI_UpdatedAt
from ga_search_usage
where search_used = 'Visits With Site Search'
and  yrmonth = KPI_MonthNum
group by yrmonth desc;

REPLACE KPIs (`Group`,KPI,MonthNum,value,RowId, SpreadSheet, Updated_at)
#CR for visits with on-site search (%)
select 'PRODUCT PERFORMANCE KPIs' KPI_Group,
'CR for visits without on-site search (%)' KPI,
yrmonth, 
sum(transactions)/sum(visits) value,
32 RowId,
@KPI_SpreadSheet,
@KPI_UpdatedAt
from ga_search_usage
where search_used = 'Visits Without Site Search'
and  yrmonth = KPI_MonthNum
group by yrmonth desc;

REPLACE KPIs (`Group`,KPI,MonthNum,value,RowId, SpreadSheet, Updated_at)
#CR for visits with on-site search (%)
select 'PRODUCT PERFORMANCE KPIs' KPI_Group,
'% of on site search w/o result' KPI,
yrmonth, 
sum(search_refinements)/sum(search_result_views) value,
34 RowId,
@KPI_SpreadSheet,
@KPI_UpdatedAt
from ga_search_usage
where search_used = 'Visits With Site Search'
and  yrmonth = KPI_MonthNum
group by yrmonth desc;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `KPI_MARKETING_MAIN`()
BEGIN

REPLACE INTO  KPIs 
SELECT
	concat(YEAR (date),	IF (MONTH (date) < 10,concat(0, MONTH(date)),MONTH (date)	)	) AS MonthNum,
  1 AS SpreadSheet,
	'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE' AS ´GROUP´,
	84 AS RowId,
	'# of total visits (in k)' AS KPI,
	'Number of total visits  incl. mobile' AS Descripcion,
	sum(visits)/1000 AS Value,
	now() AS UpdatedAt
FROM
	SEM.campaign_ad_group_ve
GROUP BY
	concat(YEAR (date),	IF (MONTH (date) < 10,concat(0, MONTH(date)),MONTH (date)));

/*REPLACE INTO  KPIs 
SELECT
	concat(YEAR (date),IF (MONTH (date) < 10,concat(0, MONTH(date)),		MONTH (date))) AS MonthNum,
  0 AS SpreadSheet,
	'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE' AS Value,
	85 AS RowId,
	'thereof # of total mobile visits (in k)' AS KPI,
	'Number of total mobile visits' AS Description,
	sum(visits) AS Value,
	now()
FROM
	production.mobileapp_campaign
GROUP BY
	concat(
		YEAR (date),

	IF (
		MONTH (date) < 10,
		concat(0, MONTH(date)),
		MONTH (date)
	)
	);*/

REPLACE INTO  KPIs 
SELECT
	b.yrmonth,
  1 AS SpreadSheet,
	'VOLUME DRIVERS - INCL. MARKETPLACE',
	56,
	'Volume share of orders from paid channels',
	'Volume share of gross orders generated from paid channels (online, offline)',
	sum(gross_orders) / a.total_orders,
	now()
FROM
	marketing_report.global_report b,
	(
		SELECT
			yrmonth,
			sum(gross_orders) AS total_orders
		FROM
			marketing_report.global_report
		WHERE
			country = 'Venezuela'
		GROUP BY
			yrmonth
	) a
WHERE
	b.yrmonth = a.yrmonth
AND b.country = 'Venezuela'
AND (b.channel_group IN (
	'Affiliates',
	'Branded',
	'CAC Deals',
	'Facebook Ads',
	'Google Display Network',
	'NewsLetter',
	'Offline Marketing',
	'Retargeting',
	'Search Engine Marketing',
	'Social Media',
	'Mobile App',
	'Display'
) or b.channel like '%CAC%')
GROUP BY
	a.yrmonth;

REPLACE INTO  KPIs SELECT
	b.yrmonth,
  1 AS SpreadSheet,
	'VOLUME DRIVERS - INCL. MARKETPLACE',
	57,
	'Volume share of orders from free channels',
	'Volume share of gross orders generated from free channels (online, offline)',
	sum(gross_orders) / a.total_orders,
	now()
FROM
	marketing_report.global_report b,
	(
		SELECT
			yrmonth,
			sum(gross_orders) AS total_orders
		FROM
			marketing_report.global_report
		WHERE
			country = 'Venezuela'
		GROUP BY
			yrmonth
	) a
WHERE
	b.yrmonth = a.yrmonth
AND b.country = 'Venezuela'
AND b.channel_group NOT IN (
	'Affiliates',
	'Branded',
	'CAC Deals',
	'Facebook Ads',
	'Google Display Network',
	'NewsLetter',
	'Offline Marketing',
	'Retargeting',
	'Search Engine Marketing',
	'Social Media',
'Mobile App',
	'Display'
) AND b.channel not like '%CAC%'
GROUP BY
	a.yrmonth;

REPLACE INTO  KPIs SELECT
	yrmonth,
  1 AS SpreadSheet,
	'MARKETING EFFICIENCY - INCL. MARKETPLACE',
	199,
	'New customers from "free" channels ',
	'New customers (based on on net orders post rejections) generated from free traffic (direct, seo, mailings, etc.)',
	sum(b.new_customers),
	now()
FROM
	marketing_report.global_report b
WHERE
	b.channel_group NOT IN (
		'Affiliates',
		'Branded',
		'Facebook Ads',
		'Google Display Network',
		'NewsLetter',
		'Offline Marketing',
		'Retargeting',
		'Search Engine Marketing',
		'CAC Deals',
		'Social Media',
'Mobile App',
	'Display'
	)
AND country = 'Venezuela'
GROUP BY
	yrmonth;

REPLACE INTO  KPIs SELECT
	concat(
		YEAR (date),

	IF (
		MONTH (date) < 10,
		concat(0, MONTH(date)),
		MONTH (date)
	)
	),
  1 AS SpreadSheet,
	'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE',
	96,
	'% basket used',
	'Share of total visitors that have used the basket',
	sum(cart) / sum(visits),
	now()
FROM
	SEM.campaign_ad_group_ve
GROUP BY
	concat(
		YEAR (date),

	IF (
		MONTH (date) < 10,
		concat(0, MONTH(date)),
		MONTH (date)
	)
	);

REPLACE INTO  KPIs SELECT
	concat(
		YEAR (b.date),

	IF (
		MONTH (b.date) < 10,
		concat(0, MONTH(b.date)),
		MONTH (b.date)
	)
	),
  1 AS SpreadSheet,
	'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE',
	102,
	'% converted (step 4)',
	'Share of total visitors that have converted',
	TRANSACTION / sum(visits),
	now()
FROM
	SEM.campaign_ad_group_ve b,
	(
		SELECT
			concat(
				YEAR (date),

			IF (
				MONTH (date) < 10,
				concat(0, MONTH(date)),
				MONTH (date)
			)
			) AS yrmonth,
			count(*) AS TRANSACTION
		FROM
			SEM.transaction_id_ve
		GROUP BY
			concat(
				YEAR (date),

			IF (
				MONTH (date) < 10,
				concat(0, MONTH(date)),
				MONTH (date)
			)
			)
	) a
WHERE
	a.yrmonth = concat(
		YEAR (b.date),

	IF (
		MONTH (b.date) < 10,
		concat(0, MONTH(b.date)),
		MONTH (b.date)
	)
	)
GROUP BY
	concat(
		YEAR (b.date),

	IF (
		MONTH (b.date) < 10,
		concat(0, MONTH(b.date)),
		MONTH (b.date)
	)
	);

REPLACE INTO  KPIs SELECT
	concat(
		YEAR (date),

	IF (
		MONTH (date) < 10,
		concat(0, MONTH(date)),
		MONTH (date)
	)
	),
  1 AS SpreadSheet,
	'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE',
	93,
	'Bounce rate',
	'Share of immediate exits after landing on site',
	sum(bounce)/sum(visits),
	now()
FROM
	SEM.campaign_ad_group_ve
GROUP BY
	concat(
		YEAR (date),

	IF (
		MONTH (date) < 10,
		concat(0, MONTH(date)),
		MONTH (date)
	)
	);

-- Not Yet Active
/*
REPLACE INTO  KPIs SELECT
	concat(
		YEAR (date),

	IF (
		MONTH (date) < 10,
		concat(0, MONTH(date)),
		MONTH (date)
	)
	),
  0 AS SpreadSheet,
	'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE',
	89,
	'# of unique visitors (in k)',
	'Number of total unique visitors',
	sum(unique_visits),
	now()
FROM
	development_ve.kpi_automation_1
GROUP BY
	concat(
		YEAR (date),

	IF (
		MONTH (date) < 10,
		concat(0, MONTH(date)),
		MONTH (date)
	)
	);

REPLACE INTO  KPIs SELECT
	concat(
		YEAR (date),

	IF (
		MONTH (date) < 10,
		concat(0, MONTH(date)),
		MONTH (date)
	)
	),
	'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE',
  0 AS SpreadSheet,
	90,
	'# of unique mobile visitors (in k) - subset of above',
	'Number of total unique mobile visitors',
	sum(unique_visits),
	now()
FROM
	development_ve.kpi_automation_2
GROUP BY
	concat(
		YEAR (date),

	IF (
		MONTH (date) < 10,
		concat(0, MONTH(date)),
		MONTH (date)
	)
	);

REPLACE INTO  KPIs SELECT
	concat(
		YEAR (date),

	IF (
		MONTH (date) < 10,
		concat(0, MONTH(date)),
		MONTH (date)
	)
	),
  0 AS SpreadSheet,
	'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE',
	94,
	'% catalogue page seen',
	'Share of total visitors that have seen the catalogue page',
	sum(page_views),
	now()
FROM
	development_ve.kpi_automation_1
GROUP BY
	concat(
		YEAR (date),

	IF (
		MONTH (date) < 10,
		concat(0, MONTH(date)),
		MONTH (date)
	)
	);*/

replace into KPIs select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, 1, 'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE', 89, '# of unique visitors (in k)', 'Number of total unique visitors', sum(visitors)/1000, now() from development_ve.kpi_automation_6 where is_mobile='No' group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));

replace into KPIs select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, 1, 'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE', 90, '# of unique mobile visitors (in k) - subset of above', 'Number of total unique mobile visitors', sum(visitors)/1000, now() from development_ve.kpi_automation_6 where is_mobile='Yes' group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));

replace into KPIs select a.yrmonth, 1, 'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE', 97, '%  started checkout (step 1)', 'Share of total visitors that have started the checkout', checkout/visits, now() from (select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, sum(checkout) as checkout from development_ve.kpi_automation_5 group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))))b, (select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, sum(visits) as visits from SEM.campaign_ad_group_ve group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))))a where a.yrmonth=b.yrmonth;

replace into KPIs select a.yrmonth, 1, 'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE', 98, '% seen payment selection page (step 2) - not for single page checkout', 'Share of total visitors that have seen the payment selection page', checkout/visits, now() from (select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, sum(checkout) as checkout from development_ve.kpi_automation_5 group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))))b, (select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, sum(visits) as visits from SEM.campaign_ad_group_ve group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))))a where a.yrmonth=b.yrmonth;

replace into KPIs select a.yrmonth, 1, 'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE', 102, '% converted (step 4)', 'Share of total visitors that have converted', order_nr/visits, now() from (select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, count(distinct order_nr) as order_nr from production_ve.tbl_order_detail where oac=1 group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))))b, (select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, sum(visits) as visits from SEM.campaign_ad_group_ve group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))))a where a.yrmonth=b.yrmonth;

replace into KPIs select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) as yrmonth, 1, 'PRODUCT PERFORMANCE KPIs - INCL. MARKETPLACE', 85, 'thereof # of total mobile visits (in k)', 'Number of total mobile visits', sum(visits)/1000, now() from development_ve.kpi_automation_4 where is_mobile='Yes' group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `KPI_MARKETPLACE_REVENUE_AND_ORDER_WATERFALL`(IN KPI_MonthNum INT)
BEGIN
/*
*  Kpi Group:  MARKETPLACE REVENUE AND ORDER WATERFALL
*  Created By: BI Team
*  Developer:  Carlos Antonio Mondragón Soria
*  Created at: 2013-10-03
*  Lastest Updated: 2013-10-03
*/
SET @KPI_Month=KPI_MonthNum;
SET @KPI_Year=SUBSTR( KPI_MonthNum FROM 1 FOR 4 );

SET @KPI_SpreadSheet = 1;
SET @KPI_UpdatedAt = Now();
SET @KPI_Group = "MARKETPLACE REVENUE AND ORDER WATERFALL";

/*
* Sample Table Creations
*/ 
DROP TEMPORARY TABLE IF EXISTS KPI_MPlace;
CREATE TEMPORARY TABLE KPI_MPlace ( INDEX ( MonthNum, ItemId ), INDEX( OrderNum ) )
SELECT 
   A_Master.*
FROM A_Master
  INNER JOIN bob_live_ve.sales_order_item AS soi 
          ON A_Master.ItemId = soi.id_sales_order_item 

WHERE
       YEAR( Date ) = @KPI_Year
   and soi.is_option_marketplace = 1
;

DROP TABLE IF EXISTS KPI_MPlace_OSRO;
CREATE TABLE KPI_MPlace_OSRO LIKE Out_SalesReportOrder;
INSERT KPI_MPlace_OSRO 
SELECT 
   Out_SalesReportOrder.* 
FROM          Out_SalesReportOrder 	
   INNER JOIN KPI_MPlace 
        USING ( OrderNum )
GROUP BY OrderNum
;
/*
* KPIs Calculations
*/

# 539 Gross valid merchandise orders
REPLACE KPIs
SELECT 
   Month_Num        AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   539         AS RowID,
   "Gross valid merchandise orders" AS KPI,
   "" AS Description,
   SUM( IF( KPI_MPlace_OSRO.OrderBeforeCan = 1,
            1 , 0 ) ) AS Value,
   @KPI_UpdatedAt
FROM 
   KPI_MPlace_OSRO
WHERE  Month_Num  = @KPI_Month      
GROUP BY MonthNum
;	

#540 Gross valid merchandise revenues = Marketplace GMV (in k local)
REPLACE KPIs 
SELECT
   MonthNum    AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   540,
   "Gross valid merchandise revenues = Marketplace GMV (in k local)" ,
   "" AS Description,
   sum( IF( OrderBeforeCan = 1, REV, 0 )) / 1000 AS Value,
   @KPI_UpdatedAt
FROM 
   KPI_MPlace
WHERE  MonthNum  = @KPI_Month      
GROUP BY MonthNum
;

#541 Gross valid venture marketplace revenues (in k local)
REPLACE KPIs 
SELECT
   MonthNum    AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   541,
   "Gross valid venture marketplace revenues (in k local)" ,
   "Gross valid merchandise revenues * commission" AS Description,
   sum( IF( OrderBeforeCan = 1 , PriceAfterTax - CostAfterTax, 0  ) ) /1000 ,
   @KPI_UpdatedAt
FROM 
   KPI_MPlace
WHERE  MonthNum  = @KPI_Month      
GROUP BY MonthNum
;

#542 Cancelled merchandise orders
REPLACE KPIs
SELECT
   MonthNum    AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   542,
   "Cancelled merchandise orders" ,
   "",
   -1 * count( distinct OrderNum ) ,
   @KPI_UpdatedAt
FROM 
   KPI_MPlace
WHERE
   /*NET KPI*/
       OrderBeforeCan = 1 
   AND Cancellations  = 1 
   AND MonthNum  = @KPI_Month      
GROUP BY MonthNum   
;

#   543 Cancelled merchandise revenues (in k local)
REPLACE KPIs
SELECT
   MonthNum    AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   543,
   "Cancelled merchandise revenues (in k local)" ,
   "" AS Description,
   COALESCE( SUM( PriceAfterTax - CostAfterTax  ) , 0 ) / -1000,
   @KPI_UpdatedAt
FROM
   KPI_MPlace
WHERE
   /*NET KPI*/
       OrderBeforeCan = 1 
   AND Cancellations  = 1 
   AND MonthNum  = @KPI_Month      
GROUP BY MonthNum
;

#  546   "Net venture marketplace revenues (in k local)" ,
REPLACE KPIs
SELECT
   #MonthNum    AS MonthNum, 
   @KPI_Month,
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   546,
   "Net venture marketplace revenues (in k local)" ,
   "Net merchandise revenues * commission" AS Description,
   sum( IF(     Cancellations  = 0
            AND OrderBeforeCan = 1
 , Rev * MPlaceFee , 0 )) / 1000  AS Value,
   #0 AS Value,
   @KPI_UpdatedAt
FROM 
   KPI_MPlace
WHERE
   #Supplier in ( SELECT Bob_Supplier_Name FROM A_E_BI_Marketplace_Commission WHERE isRocketVenture = 1 )
   #AND 
   MonthNum  = @KPI_Month      
GROUP BY MonthNuM
;

#551 Total active listings
REPLACE KPIs
SELECT
   @KPI_Month AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   551,
   "Total active listings" ,
   "Number of online listings at the end of the month" AS Description,
   count( distinct sku_config ),
   @KPI_UpdatedAt
FROM 
   A_Master_Catalog
WHERE 
    isMarketPlace       = 1
AND isVisible     = 1
AND isActive_SKUConfig  = 1

;

#   552 Total active listings Of which from Rocket Internet ventures
REPLACE KPIs
SELECT
   @KPI_Month AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   552,
   "Total active listings Of which from Rocket Internet ventures" ,
   "" AS Descripcion,
   #count( distinct sku_config ),
   0 AS Value,
   @KPI_UpdatedAt
#FROM 
#   A_Master_Catalog
#WHERE 
#    A_Master_Catalog.isMarketPlace       = 1
#AND A_Master_Catalog.isActive_SKUConfig  = 1
#AND date_format( A_Master_Catalog.isMarketPlace_Since , "%Y%m" ) <= @KPI_Month
#AND A_Master_Catalog.supplier in ( SELECT A_E_BI_Marketplace_Commission.Bob_Supplier_Name FROM A_E_BI_Marketplace_Commission WHERE A_E_BI_Marketplace_Commission.isRocketVenture = 1 )

;

# 553 Merchant base at the end of the month
REPLACE KPIs
SELECT
   @KPI_Month AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   553,
   "Merchant base at the end of the month" ,
   "Total number of merchants at the end of the month" AS Description,
   count( DISTINCT supplier ),
   @KPI_UpdatedAt
FROM 
   A_Master_Catalog
WHERE 
    A_Master_Catalog.isMarketPlace       = 1
AND isVisible     = 1
AND isActive_SKUConfig  = 1
AND date_format( A_Master_Catalog.isMarketPlace_Since , "%Y%m" ) <= @KPI_Month

;
#   554 Merchant base at the end of the month Of which active" 
REPLACE KPIs
SELECT
   @KPI_Month AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   554,
   "Merchant base at the end of the month Of which active" ,
   "At least 5 net merchandise transactions per month" AS Description,
   COUNT(*),
   @KPI_UpdatedAt
FROM
   (
   SELECT 
      Supplier
   FROM 
      KPI_MPlace
   WHERE
          MonthNum = @KPI_Month 
      AND Cancellations  = 0
      AND OrderBeforeCan = 1
   GROUP BY Supplier
   HAVING count( DISTINCT OrderNum ) >= 5
   ) AS OrdersSupplier
;

REPLACE KPIs
SELECT
   @KPI_Month AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   555,
   "Merchants delisted this month" ,
   "",
   0,
   @KPI_UpdatedAt
;

REPLACE KPIs
SELECT
   @KPI_Month  AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   556,
   "New merchants this month" ,
   "",
   count( DISTINCT supplier ),
   @KPI_UpdatedAt
FROM 
   A_Master_Catalog
WHERE 
    A_Master_Catalog.isMarketPlace       = 1
AND isVisible     = 1
AND isActive_SKUConfig  = 1
AND date_format( A_Master_Catalog.isMarketPlace_Since , "%Y%m" ) = @KPI_Month
;

#   557 "New gross customers this month" ,
REPLACE KPIs
SELECT
   Month_Num  AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   557,
   "New gross customers this month" ,
   "Additional customers that placed at least one gross valid merchandise order",
   count(*),
   @KPI_UpdatedAt
FROM
   KPI_MPlace_OSRO
WHERE
       OrderBeforeCan = 1 
   AND First_Gross_Order = KPI_MPlace_OSRO.OrderNum
   AND Month_Num  = @KPI_Month      
GROUP BY MonthNum   
;

# 558 New net customers this month
REPLACE KPIs
SELECT
   Month_Num  AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   558,
   "New net customers this month" ,
   "Additional customers that placed at least one net merchandise order",
   count(*),
   @KPI_UpdatedAt
FROM
   KPI_MPlace_OSRO
WHERE
       KPI_MPlace_OSRO.OrderAfterCan = 1 
   AND KPI_MPlace_OSRO.First_Net_Order = KPI_MPlace_OSRO.OrderNum
   AND Month_Num = @KPI_Month
GROUP BY MonthNum
;

REPLACE KPIs 
SELECT
   MonthNum  AS MonthNum,
   @KPI_SpreadSheet AS SpreadSheet, 
   @KPI_Group  AS`Group`,
   561,
   "Total net merchandise items"  AS KPI,
   "In net merchandise orders" AS Description,
   count(*),
   @KPI_UpdatedAt
FROM 
   KPI_MPlace
WHERE
   Cancellations  = 0
AND OrderBeforeCan = 1
AND MonthNum = @KPI_Month
GROUP BY MonthNum
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `KPI_NET_PROMOTER_SCORE`(IN KPI_MonthNum INT)
BEGIN
/*
*  Kpi Group:  NET PROMOTER SCORE
*  Created By: BI Team
*  Developer:  Eduardo Martinez
*  Created at: 2013-11-01
*  Lastest Updated: 2013-11-01
* Modified by: Paula Mendoza (2014-07-05)
*/

/*
*  Global Variables
*/
SET @KPI_Month=KPI_MonthNum;
SET @KPI_Year=SUBSTR( KPI_MonthNum FROM 1 FOR 4 );
SET @KPI_UpdatedAt = Now();
SET @KPI_SpreadSheet = 1;
SET @KPI_Group='NET PROMOTER SCORE';

Drop TEMPORARY TABLE if EXISTS NPS;
create TEMPORARY TABLE NPS
SELECT  
`Response ID`,
`IP Address`,
`Date Submitted`,
`Duplicate`,
`Time Taken to Complete (Seconds)`,
`Response Status`,
`NPS`
from  customer_service.NPSMailVE
where date_format(`Date Submitted`, "%x%m") = @KPI_Month
union all
select
`Response ID`,
`IP Address`,
`Date Submitted`,
`Duplicate`,
`Time Taken to Complete (Seconds)`,
`Response Status`,
`NPS`
from customer_service.NPSMailVE2
where date_format(`Date Submitted`, "%x%m") = @KPI_Month
union all
select
`Response ID`,
`IP Address`,
`Dare Submitted`,
`Duplicate`,
`Time Taken to Complete (Seconds)`,
`Response Status`,
`NPS`
from nps.NPSMarketPlaceVE
where date_format(`Dare Submitted`, "%x%m") = @KPI_Month;


replace KPIs
SELECT 
   date_format(`Date Submitted`, "%x%m")    AS MonthNum, 
   @KPI_SpreadSheet,
   @KPI_Group  AS`Group`,
   155         AS RowID,
   "Net promoter score" AS KPI,
   "Promoters (9 and 10) - detractors (0 through 6); as per net promoter score survey done to new customers" AS Description,
   round( (sum(if(NPS in(11,10),1,0))-
sum(if(NPS in(1,2,3,4,5,6,7),1,0)))/count(NPS) * 100  ) AS Value,
   @KPI_UpdatedAt
FROM 
   NPS
WHERE
   date_format(`Date Submitted`, "%x%m") = @KPI_Month
GROUP BY MonthNum
; 

#Cambio la encuesta, no se pueden medir estos npúmeros

/*
replace KPIs
SELECT 
   date_format(`Date Submitted`, "%x%m")    AS MonthNum, 
   @KPI_SpreadSheet,
   @KPI_Group  AS`Group`,
   156         AS RowID,
   "Satisfaction - website" AS KPI,
   "1 minimum, 5 maximum; as per net promoter score survey done to new customers" AS Description,
   (sum(`Facilidad de uso`)
+SUM(`Facilidad para navegar y filtrar`)+sum(`Ahorro del tiempo`)
+sum(`Diseos inspiradores`)+sum(`Facilidad en el proceso de pago`)
)
/(
sum(if(`Facilidad de uso`<>'',1,0))
+sum(if(`Facilidad para navegar y filtrar`<>'',1,0))+SUM(if(`Ahorro del tiempo`<>'',1,0))
+sum(if(`Diseos inspiradores`,1,0))
+sum(if(`Facilidad en el proceso de pago`<>'',1,0))
)
AS Value,
   @KPI_UpdatedAt
FROM 
   NPSMX
WHERE
   date_format(`Date Submitted`, "%x%m") = @KPI_Month
GROUP BY MonthNum
; 

replace KPIs
SELECT 
   date_format(`Date Submitted`, "%x%m")    AS MonthNum, 
   @KPI_SpreadSheet,
   @KPI_Group  AS`Group`,
   157         AS RowID,
   "Satisfaction - products" AS KPI,
   "1 minimum, 5 maximum; as per net promoter score survey done to new customers" AS Description,
   (sum(`Cantidad de productos`)+SUM(`Calidad de los productos`)+sum(`Calidad de las marcas`)
+sum(`Disponibilidad de tamaos de los productos`)+sum(`Informacin de tamaos/tallas`))/
(sum(if(`Cantidad de productos`<>'',1,0))+sum(if(`Calidad de los productos`<>'',1,0))
+sum(if(`Calidad de las marcas`<>'',1,0))
+sum(if(`Disponibilidad de tamaos de los productos`<>'',1,0))+sum(if(`Informacin de tamaos/tallas`<>'',1,0))
) AS Value,
   @KPI_UpdatedAt
FROM 
   NPSMX
WHERE
   date_format(`Date Submitted`, "%x%m") = @KPI_Month
GROUP BY MonthNum
; 

replace KPIs
SELECT 
   date_format(`Date Submitted`, "%x%m")    AS MonthNum, 
   @KPI_SpreadSheet,
   @KPI_Group  AS`Group`,
   158         AS RowID,
   "Satisfaction - delivery" AS KPI,
   "1 minimum, 5 maximum; as per net promoter score survey done to new customers" AS Description,
   (sum(`Informacin clara frente a la entrega`)+sum(`Servicio por parte de nuestros mensajeros`)
+SUM(`Tiempo de entrega`)+sum(`Calidad del empaque`)+sum(`Exactitud de los productos y documentos entregados`))
/
 (sum(if(`Informacin clara frente a la entrega`<>'',1,0))
+sum(if(`Servicio por parte de nuestros mensajeros`<>'',1,0))
+sum(if(`Tiempo de entrega`<>'',1,0))
+sum(if(`Calidad del empaque`<>'',1,0))
+sum(if(`Exactitud de los productos y documentos entregados`<>'',1,0))
) AS Value,
   @KPI_UpdatedAt
FROM 
   NPSMX
WHERE
   date_format(`Date Submitted`, "%x%m") = @KPI_Month

GROUP BY MonthNum
; 

replace KPIs
SELECT 
   date_format(`Date Submitted`, "%x%m")    AS MonthNum, 
   @KPI_SpreadSheet,
   @KPI_Group  AS`Group`,
   159         AS RowID,
   "Satisfaction - customer care" AS KPI,
   "1 minimum, 5 maximum; as per net promoter score survey done to new customers" AS Description,
   (sum(`Capacidad para resolver el problema`)+sum(`Aptitudes`)+SUM(`Velocidad de respuesta`)
+sum(`Amabilidad`))
/
(
sum(if(`Capacidad para resolver el problema`<>'',1,0))
+sum(if(`Aptitudes`<>'',1,0))
+sum(if(`Velocidad de respuesta`<>'',1,0))
+sum(if(`Amabilidad`<>'',1,0))
) AS Value,
   @KPI_UpdatedAt
FROM 
   NPSMX
WHERE
   date_format(`Date Submitted`, "%x%m") = @KPI_Month

GROUP BY MonthNum
; 

replace KPIs
SELECT 
   date_format(`Date Submitted`, "%x%m")    AS MonthNum, 
   @KPI_SpreadSheet,
   @KPI_Group  AS`Group`,
   160         AS RowID,
   "Satisfaction - returns & refunds" AS KPI,
   "1 minimum, 5 maximum; as per net promoter score survey done to new customers" AS Description,
   (sum(`Devolver un producto`)+sum(`Obtencin del reembolso`)+SUM(`Interaccin con atencin al cliente`))
/
(sum(if(`Devolver un producto`<>'',1,0))
+sum(if(`Obtencin del reembolso`<>'',1,0))
+sum(if(`Interaccin con atencin al cliente`<>'',1,0))
)AS Value,
   @KPI_UpdatedAt
FROM 
   NPSMX
WHERE
   date_format(`Date Submitted`, "%x%m") = @KPI_Month

GROUP BY MonthNum
;
*/

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 PROCEDURE `KPI_OPERATIONS_EFFICIENCY`(IN KPI_MonthNum INT)
BEGIN
/*
*  Kpi Group:  OPERATIONS EFFICIENCY
*  Created By: BI Team
*  Developer:  Carlos Antonio Mondragón Soria
*  Created_at: 2013-10-03
*  Lastest Updated: 2013-10-03
*/
SET @KPI_Month=KPI_MonthNum;
SET @KPI_Year=SUBSTR( KPI_MonthNum FROM 1 FOR 4 );

SET @KPI_UpdatedAt = Now();
SET @KPI_SpreadSheet = 1;
SET @KPI_Group = "OPERATIONS EFFICIENCY";

/*
* Sample Table Creations
*/

DROP TEMPORARY TABLE IF EXISTS KPI_CUSTOMER_CARE;
CREATE TEMPORARY TABLE KPI_CUSTOMER_CARE
SELECT * FROM customer_service.tbl_general_cc
WHERE YEAR(Fecha)=@KPI_Year;


DROP TEMPORARY TABLE IF EXISTS KPI_ZENDESK_CARE;
CREATE TEMPORARY TABLE KPI_ZENDESK_CARE
SELECT date(Created_at) as Created_at,
SUM(IF(Group_ IN('B.O. Cancelaciones','BackOffice - Cancelación'),Resolution_Time,0))/SUM(IF(Group_ IN('BackOffice - Devolución','B.O. Devoluciones'),summation_column,0)) AVGTIMEdev,
SUM(IF(Group_ IN('BackOffice - Devolución','B.O. Devoluciones'),Resolution_Time,0))/SUM(IF(Group_ IN('BackOffice - Devolución','B.O. Devoluciones'),summation_column,0)) AS AVGTIMEree,
SUM(IF( Resolution_Time>75,1,0))
/
SUM(summation_column)
AS TOTALTK 
FROM customer_service.tbl_zendesk 
WHERE YEAR(Created_at)=@KPI_Year
#WHERE Group_ IN('BackOffice - Devolución','B.O. Devoluciones')
GROUP BY date(Created_at)
;


DROP TEMPORARY TABLE IF EXISTS KPI_CUSTOMER_in20;
CREATE TEMPORARY TABLE KPI_CUSTOMER_in20
SELECT *  FROM customer_service.bd_distribucion_llamadas_ve
#WHERE YEAR(Fecha)=@KPI_Year
;
DROP TEMPORARY TABLE IF EXISTS KPI_CUSTOMER_ZEN;
CREATE TEMPORARY TABLE KPI_CUSTOMER_ZEN
SELECT `Created_at`,`Resolution time`  FROM customer_service.tbl_zendesk_ve
where `Solved at` is not null and `Resolution time`<120
#WHERE YEAR(Fecha)=@KPI_Year
;


DROP TEMPORARY TABLE IF EXISTS KPI_ZENDESK_CARE;
CREATE TEMPORARY TABLE KPI_ZENDESK_CARE

SELECT date(`Created_at`) as `Created_at`,
SUM(IF(`Group` IN('Back Office'),`Resolution Time`,0))/SUM(IF(`Group` IN('Back Office'),`summation column`,0)) AVGTIMEdev,
SUM(IF(`Group` IN('Back Office'),`Resolution Time`,0))/SUM(IF(`Group` IN('Back Office'),`summation column`,0)) AS AVGTIMEree,
SUM(IF( `Resolution Time`>3,1,0))
/
SUM(`summation column`)
AS TOTALTK 
FROM customer_service.tbl_zendesk_ve
WHERE YEAR(`Created_at`)=@KPI_Year
#WHERE `Group` IN('BackOffice - Devolución','B.O. Devoluciones')
GROUP BY date(`Created_at`)
;

DROP TEMPORARY TABLE IF EXISTS KPI_MASTER_CAT;
CREATE TEMPORARY TABLE KPI_MASTER_CAT
SELECT * FROM A_Master
WHERE YEAR(Date)=@KPI_Year;

DROP TEMPORARY TABLE IF EXISTS KPI_Master_Catalog;
CREATE TEMPORARY TABLE KPI_Master_Catalog
SELECT * FROM A_Master_Catalog
WHERE YEAR(created_at_Simple)=@KPI_Year;

DROP TEMPORARY TABLE IF EXISTS KPI_Catalog_history;
CREATE TEMPORARY TABLE KPI_Catalog_history
SELECT * FROM production_ve.catalog_history 
where 
		YEAR( date ) = @KPI_Year;

DROP TEMPORARY TABLE IF EXISTS out_order_tracking_Year;
CREATE TEMPORARY TABLE 	out_order_tracking_Year
SELECT * FROM production_ve.out_order_tracking 
WHERE 
   YEAR( date_ordered ) = @KPI_Year
AND status_wms <> 'cancelado'     
;

DROP TEMPORARY TABLE IF EXISTS out_order_tracking_totals;
CREATE TEMPORARY TABLE 	out_order_tracking_totals
SELECT  
   date_format(date_shipped, "%x%m") AS MonthNum,
   SUM( IF( status_wms NOT IN ('Quebrado','quebra tratada','Cancelado'), 1, 0 ) ) AS tot_items,
   SUM( IF( date_shipped IS NOT NULL, 1, 0 ) ) AS tot_delivers
FROM out_order_tracking_Year 
WHERE 
    YEAR( date_ordered ) = @KPI_Year
AND status_wms <> 'cancelado'     
GROUP BY MonthNum
;

DROP TEMPORARY TABLE IF EXISTS A_SKUsVisible_Sample;
CREATE TEMPORARY TABLE A_SKUsVisible_Sample LIKE A_SKUsVisible;
INSERT A_SKUsVisible_Sample SELECT * FROM A_SKUsVisible WHERE MonthNum = @KPI_Month;

DROP TEMPORARY TABLE IF EXISTS A_Stock_Sample;
CREATE TEMPORARY TABLE A_Stock_Sample LIKE A_Stock;
INSERT A_Stock_Sample SELECT * FROM A_Stock WHERE MonthNum = @KPI_Month;
	
/*
* 
*/
#164 Production - Total number of SKUs online 
REPLACE KPIs
SELECT 
   @KPI_Month                AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   164                       AS RowId ,
   "Production - Total number of SKUs online" AS KPI,
   "Total number of SKU configs online that "
   "are in own warehouse (outright or consignment)" AS Description ,
   COUNT( DISTINCT A_Stock.SKU_Config ) AS Value,
   @KPI_UpdatedAt  
FROM 
           A_SKUsVisible_Sample AS A_SKUsVisible 
INNER JOIN A_Stock_Sample       AS A_Stock
     USING (SKU_Config, MonthNum )
WHERE 
   MonthNum = @KPI_Month
GROUP BY MonthNum 
;

#165 Total number of SKUs in warehouse
REPLACE KPIs
SELECT 
   @KPI_Month                AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   165                       AS RowId ,
   "Total number of SKUs in warehouse" AS KPI,
   "Total number of SKU configs that are "
   "in own warehouse (outright or consignment)" AS Description ,
   COUNT( DISTINCT A_Stock.SKU_Config ) AS Value,
   @KPI_UpdatedAt  
FROM 
A_Stock
WHERE 
   MonthNum = @KPI_Month
GROUP BY MonthNum 
;

#167 Production - SKU configs created (#)
REPLACE KPIs
SELECT 
   date_format(created_at_Config, "%x%m") AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   167                       AS RowId ,
   "Production - SKU configs created (#)" AS KPI,
   "Number of SKU configs added online in this month"
   "(incl. production, content-writing, quality check)" AS Description ,
   count(distinct(sku_config)) AS Value,
   @KPI_UpdatedAt  
FROM 
   KPI_Master_Catalog
WHERE date_format(created_at_Config, "%x%m") = @KPI_Month
GROUP BY date_format(created_at_Config, "%x%m")
;

#171 Logistics - 24h shipments (%) - customer perspective
REPLACE KPIs
SELECT 
   c.month_s as MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   171                       AS RowId ,
   "Logistics - 24h shipments (%) - customer perspective" AS KPI,
   "Orders (or items) shipped within 24h from date and time "
   "of order / total valid orders within period. Shipment = "
   "when the parcel is picked up by the carrier (either in "
   "house or third party)." AS Description ,   
   c.24hr/tot_items AS Value,
   @KPI_UpdatedAt     
FROM
(
   SELECT 
      date_format(out_order_tracking_Year.date_shipped, "%x%m") as month_s, 
	  count(*) as 24hr
   FROM 
      out_order_tracking_Year
   WHERE 
          out_order_tracking_Year.date_shipped <= production.workday(out_order_tracking_Year.date_ordered,1)
      AND out_order_tracking_Year.date_shipped is not null
      AND out_order_tracking_Year.status_wms NOT IN ('Quebrado','quebra tratada','Cancelado')
   GROUP BY 
      date_format(date_shipped, "%x%m")
) AS c
INNER JOIN out_order_tracking_totals
           ON c.month_s = out_order_tracking_totals.MonthNum
WHERE c.month_s = @KPI_Month
GROUP BY out_order_tracking_totals.MonthNum
;

#172 Logistics - 24h shipments (%) - warehouse perspective
REPLACE KPIs
SELECT 
   c.month_s as MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   172                       AS RowId ,
   "Logistics - 24h shipments (%) - warehouse perspective" AS KPI,
   "Measures warehouse performance: orders (or items) shipped "
   "within 24 hours of receiving the pick request or receiving "
   "the cross docked items / total orders (items) within period "
   "(in OMS 'item received'). It disregards the time it takes to "
   "validate the payment and / or receive the cross docked items. "
   "Shipment = when the parcel is picked up by the carrier (either "
   "in house or third party)." AS Description ,   
   c.24hr/tot_items AS Value,
   @KPI_UpdatedAt     
FROM
(
   SELECT 
      date_format(out_order_tracking_Year.date_shipped, "%x%m") as month_s, 
	  count(*) as 24hr
   FROM 
      out_order_tracking_Year
   WHERE 
          out_order_tracking_Year.date_shipped <= production.workday(out_order_tracking_Year.date_ready_to_pick,1)
      AND out_order_tracking_Year.date_shipped is not null
   GROUP BY 
      date_format(date_shipped, "%x%m")
) AS c
INNER JOIN out_order_tracking_totals
           ON c.month_s = out_order_tracking_totals.MonthNum
WHERE c.month_s = @KPI_Month
GROUP BY out_order_tracking_totals.MonthNum
;

#173 Logistics - On time delivery (or shipment) %
REPLACE KPIs
SELECT 
   c.MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   173                       AS RowId ,
   "Logistics - On time delivery (or shipment) %" AS KPI,
   "Deliveries (shipments) within promise to customer (e.g. on website) "
   "/ total deliveries. If delivery information not available / reliable, "
   "shipment date are to be taken discounting an average delivery time (e.g."
   "5 days delivery promise and 2 days average delivery time = on time "
   "shipment up to and including day 3)" AS Description ,   
   c.Value/Total AS Value,
   @KPI_UpdatedAt     
FROM
(
SELECT
   date_format(date_delivered_promised, "%x%m") as MonthNum, 
   SUM( IF(  on_time_total_1st_attempt = 1 , 1, 0 ) ) AS Value,
   COUNT(*) AS Total
 FROM   
   out_order_tracking_Year
WHERE
    is_presale = 0
AND status_wms in( 
                        'aguardando estoque', 
                        'aguardando separacao', 
                        'analisando quebra', 
                        'DS estoque reservado', 
                        'Estoque reservado' ,
                        'expedido',
                        'faturado',
                        'separando'
                      )
AND date_format(date_delivered_promised, "%x%m") = @KPI_Month
GROUP BY MonthNum
) as c
;

#174 Logistics - Average order delivery lead time (days)
REPLACE KPIs
SELECT 
   MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   174                       AS RowId ,
   "Logistics - Average order delivery lead time (days)" AS KPI,
   "Lead time from order to customer delivery (first trial). "
   "If delivery not available, shipment + estimated delivery "
   "lead time to be taken" AS Description ,   
   Value AS Value,
   @KPI_UpdatedAt     
FROM   
   (
     SELECT
	    DATE_FORMAT(date_delivered_promised, "%x%m") as MonthNum, 
		avg(
           case 
              when date_exported is null then null 
	         else production.calcworkdays(date_exported, 
			                                             case when date_1st_attempt is null then curdate() 
														 else date_1st_attempt end)end
            ) as Value 
	 FROM  out_order_tracking_Year
     WHERE     check_dates = 'correct'
           AND status_wms <> 'cancelado'
           AND DATE_FORMAT(date_delivered_promised, "%x%m") = @KPI_Month
     GROUP BY MonthNum
    ) AS TMP
;

#175 Logistics - Unable to fulfil (%)
REPLACE KPIs
SELECT 
   date_format(date_procured_promised, "%x%m") AS MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   175                       AS RowId ,
   "Logistics - Unable to fulfil (%)" AS KPI,
   "items cancelled due to out of stock, "
   "non availability at supplier or quality "
   "issues / total items validated (after "
   "payment and CS validation or alternatively "
   "items shipped)" AS Description ,   
   sum(is_stockout)/SUM( Item_counter ) AS Value,
   @KPI_UpdatedAt     
FROM   
	out_order_tracking_Year
where 
    is_presale = 0
AND fulfillment_type_real = "crossdocking"
AND status_wms not in('cancelado', 'quebrado', 'exportado','quebra_tratada', 'ac_analisando_quebra', 'ac_estoque_reservado','precancelado','aguardando confirmacion' )
AND date_format(date_procured_promised, "%x%m") = @KPI_Month
GROUP BY MonthNum;


#178	Customer Service - Inbound call service level (%)
REPLACE KPIs
SELECT 
   date_format( Fecha , "%Y%m" ) AS MonthNum,
   @KPI_SpreadSheet ,
   @KPI_Group                    AS `Group`,
   178                           AS RowId ,
   "Customer Service - Inbound call service level (%)" AS KPI,
   "Calls received within 20seconds / total calls received during business hours (incl. abandoned)"  AS Description ,
   sum(if(evento in ('COMPLETEAGENT','COMPLETECALLER') and `Dur. Espera`<=20,1,0))/sum(if(evento in ('COMPLETEAGENT','COMPLETECALLER'),1,0))

AS Value,
   @Updated_at  
FROM 
   KPI_CUSTOMER_in20
WHERE date_format( Fecha , "%Y%m" )=@KPI_Month
GROUP BY date_format( Fecha , "%Y%m" )
;



#179	Customer Service - Inbound chat service level (%) (if the case)

REPLACE KPIs
SELECT 
   date_format( chat_start_time , "%Y%m" ) AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                    AS `Group`,
   179                           AS RowId ,
   "Customer Service - Inbound chat service level (%) (if the case)" AS KPI,
   "Chats answered within 20 seconds / total chats received during business hours"  AS Description ,
sum(if(operator_first_response_delay <>'None' AND visitor_messages_sent>0 and operator_first_response_delay<=20,1,0) )
/ 
SUM(if(operator_first_response_delay <>'None' AND visitor_messages_sent>0,1,0) ) AS Value
,
  @Updated_at  
FROM 
   customer_service.tbl_olark_chat_ve
WHERE 
	date_format(chat_start_time ,'%Y%m')=@KPI_Month
GROUP BY date_format( chat_start_time , "%Y%m" )
;





#180	Customer Service - Mail response time (hours)
REPLACE KPIs
SELECT
date_format(Fecha_creacion,'%Y%m'),
1,
'OPERATIONS EFFICIENCY',
180 as RowId,
'Customer Service - Mail response time (hours)' as KPI,
'Average time to answer a customer email (customer perspective, no adjustment for working days)' as Description,
avg(first_reply_time_in_minutes)/60  as value,
now() as Updated_at
from customer_service.tbl_zendesk_general
where pais='VE' and via='Mail'
and Clasificacion not like '%SPAM%'
and date_format(Fecha_creacion,'%Y%m')=@KPI_Month
;



#181	Customer Service - Total inbound calls answered (#)

REPLACE KPIs
SELECT 
   date_format( Fecha , "%Y%m" ) AS MonthNum,
   @KPI_SpreadSheet ,
   @KPI_Group                    AS `Group`,
   181                           AS RowId ,
   "Customer Service - Total inbound calls answered (#)" AS KPI,
   "Total inbound calls answered (i.e. picked up and served customer) incl. live chats"  AS Description ,
   sum(if(evento in ('COMPLETEAGENT','COMPLETECALLER'),1,0)) AS Value,
   @Updated_at
FROM 
customer_service.bd_distribucion_llamadas_ve
WHERE date_format( Fecha , "%Y%m" )=@KPI_Month
GROUP BY date_format( Fecha , "%Y%m" )
;



#182	Customer Service - Total outbound calls made (#)###
REPLACE KPIs
SELECT 
   date_format( Fecha , "%Y%m" ) AS MonthNum,
   @KPI_SpreadSheet ,
   @KPI_Group                    AS `Group`,
   182                           AS RowId ,
   "Customer Service - Total outbound calls made (#)" AS KPI,
   "Total outbound calls made (i.e. called and reached)"  AS Description ,
    count(*)AS Value,
   @Updated_at
FROM 
   customer_service.bd_llamadas_salientes_ve
WHERE
date_format( Fecha , "%Y%m" )=@KPI_Month
GROUP BY date_format( Fecha , "%Y%m" )
;



#183	Customer Service - Total emails received (#)
REPLACE KPIs 
SELECT
date_format(Fecha_creacion,'%Y%m'),
1,
'OPERATIONS EFFICIENCY',
183 as RowId,
'Customer Service - Total emails received (#)' as KPI,
'Total emails received by customers (excl. SPAM)' as Description,
count(1) as value,
now() as Updated_at
from customer_service.tbl_zendesk_general 
where pais='VE' and via='Mail'
and Clasificacion not like '%SPAM%' and
date_format(Fecha_creacion,'%Y%m')=@KPI_Month
;



#184	Customer Service - Total other contacts received (#)###
REPLACE KPIs
SELECT 
   date_format( Fecha , "%Y%m" ) AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                    AS `Group`,
   184                           AS RowId ,
   "Customer Service - Total other contacts received (#)" AS KPI,
   "Social media contacts (e.g. facebook, twitter), chat and similar"  AS Description ,
   #SUM(Todas) AS Value,
   sum(`comment`) AS Value,
   @KPI_UpdatedAt
FROM 
(SELECT Fecha,`comment` from customer_service.tbl_facebook_comments_ve
			UNION ALL
			SELECT Date(chat_start_time) as Fecha,count(*) as `comment` 
			FROM customer_service.tbl_olark_chat_ve GROUP BY Date(chat_start_time))a

WHERE date_format( Fecha , "%Y%m" ) = @KPI_Month
GROUP BY date_format( Fecha , "%Y%m" )
;

#190	Returns and refunds - Return processing lead time (hours)###
DROP   TEMPORARY TABLE IF EXISTS temp_items_returned_hours;
CREATE TEMPORARY TABLE temp_items_returned_hours
SELECT date(date_inbound_il) AS date,
((UNIX_TIMESTAMP(date_refunded_il)-UNIX_TIMESTAMP(date_inbound_il))/60)/60 AS return_hours
FROM operations_ve.out_inverse_logistics_tracking
WHERE is_returned=1
AND date_inbound_il IS NOT NULL
AND date_refunded_il IS NOT NULL
AND date_closed_il IS NULL

GROUP BY date(date_inbound_il);

REPLACE  KPIs
SELECT 
	 date_format( date, "%Y%m" ) AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                    AS `Group`,
   190                           AS RowId ,
   "Returns and refunds - Return processing lead time (hours)" AS KPI,
   "Average lead time from arrival of parcels at warehouse dock to return confirmation to customer (can be an estimate, no adjustment for working hours)"  AS Description ,
    avg(return_hours) AS Value,
 @Updated_at
FROM 
 temp_items_returned_hours
WHERE
  date_format( date  , "%Y%m" ) = @KPI_Month
GROUP BY date_format( date, "%Y%m" );



#191	Returns and refunds - Refund processing lead time (hours)####

SELECT  'Refund processing lead time (hours)',now();
DROP   TEMPORARY TABLE IF EXISTS temp_items_returned_hours_total;
CREATE TEMPORARY TABLE temp_items_returned_hours_total
SELECT date_format((date_customer_request_il), "%Y%m") AS yrmonth,
avg(if(date_cash_refunded_il is not null,
			(((UNIX_TIMESTAMP(date_cash_refunded_il)-UNIX_TIMESTAMP(date_customer_request_il))/60)/60),
			(((UNIX_TIMESTAMP(date_voucher_refunded_il)-UNIX_TIMESTAMP(date_customer_request_il))/60)/60))) as return_hours_total
FROM operations_ve.out_inverse_logistics_tracking
WHERE is_returned=1
AND date_quality_il IS NOT NULL
AND date_refunded_il IS NOT NULL
GROUP BY yrmonth;

REPLACE KPIs
SELECT 
 yrmonth AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                    AS `Group`,
   191                           AS RowId ,
   "Returns and refunds - Refund processing lead time (hours)" AS KPI,
   "Average lead time from cancellation / return processing to refund confirmation to customer (can be an estimate, no adjustment for working time)"  
		AS Description ,
 return_hours_total AS Value,
 @Updated_at
FROM 
 temp_items_returned_hours_total
WHERE yrmonth = @KPI_Month;



#192 Returns and refunds - Refund tickets / requests older than 72h (%)####
REPLACE KPIs
SELECT 
   date_format( `Created_at` , "%Y%m" ) AS MonthNum,
   @KPI_SpreadSheet              AS SpreadSheet,
   @KPI_Group                    AS `Group`,
   192                           AS RowId ,
   "Returns and refunds - Refund tickets / requests older than 72h (%)" AS KPI,
   "Refund tickets or requests older than 72h / Total refund tickets or requests open at the end of the month"  
		AS Description ,
   avg(TOTALTK) AS Value,
   @Updated_at
FROM 
   KPI_ZENDESK_CARE
WHERE date_format( `Created_at` , "%Y%m" ) = @KPI_Month

GROUP BY date_format( `Created_at` , "%Y%m" )
;
#193 Own last mile (if the case) - Delivery lead time (hours)
REPLACE KPIs
SELECT 
   date_format(date_delivered_promised, "%x%m")                   AS MonthNum,  
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                                                     AS `Group`,   
   193                                                            AS RowId ,
   "Own last mile (if the case) - Delivery lead time (hours)"     AS KPI,
   "Average lead time from ready to pick up at main warehouse to delivered to customer. E.g. order ready at warehouse for pick up at 15.30, final delivery  next day at 15.30 = 24hours."                                                        AS Description ,
   AVG(TIME_TO_SEC(  TIMEDIFF(date_delivered + interval 12 hour, datetime_ready_to_ship ) ) / 3600) AS Value,
   @KPI_UpdatedAt
FROM 
   out_order_tracking_Year
WHERE 
    shipping_carrier LIKE "%MENSAJERO%INTERNO%"
AND date_format(date_delivered_promised, "%x%m") = @KPI_Month
GROUP BY MonthNum;

# 194 Own last mile (if the case) - Delivery lead time (hours)
REPLACE KPIs
SELECT
  date_format(date_delivered_promised, "%x%m")                   AS MonthNum, 
  @KPI_SpreadSheet          AS SpreadSheet,
  @KPI_Group                                                     AS `Group`,  
  194                                                            AS RowId ,
  "Own last mile (if the case) - Delivery lead time (hours)"     AS KPI,
  "Average lead time from ready to pick up at main warehouse to"
  " delivered to customer. E.g. order ready at warehouse for pick "
  "up at 15.30, final delivery next day at 15.30 = 24hours." AS Description ,
  AVG(TIME_TO_SEC(  TIMEDIFF(date_delivered + interval 12 hour, datetime_ready_to_ship ) ) / 3600)                                                         AS Value,
  @Updated_at
FROM
   out_order_tracking_Year
WHERE
          shipping_carrier LIKE "%MENSAJERO%INTERNO%"
      AND date_format(date_delivered_promised, "%x%m") = @KPI_Month                  
GROUP BY MonthNum;


#195 Own last mile (if the case) - First delivery attempt (%)
REPLACE KPIs
SELECT 
   date_format(date_procured_promised, "%x%m") AS MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   195                       AS RowId ,
   "Own last mile (if the case) - First delivery attempt (%)" AS KPI,
   "Packages delivered at first attempt / total packages" AS Description ,   
   SUM(effective_1st_attempt)/SUM(item_counter) AS Value,
   @KPI_UpdatedAt     
FROM   
	out_order_tracking_Year
WHERE	
        shipping_carrier LIKE "%MENSAJERO%INTERNO%"
    AND is_presale = 0
    AND status_wms not in('cancelado', 'quebrado', 'quebra_tratada', 'ac_analisando_quebra', 'ac_estoque_reservado' )
	  AND cat_2 NOT LIKE "%CINE%"
    AND date_format(date_procured_promised, "%x%m") = @KPI_Month
GROUP BY MonthNum;

#196 Own last mile (if the case) - Coverage (%)
REPLACE KPIs
SELECT 
   MonthNum, 
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   196                       AS RowId ,
   "Own last mile (if the case) - Coverage (%)" AS KPI,
   "Orders delivered with own last mile / total orders delivered (Packages can be taken if orders split)" AS Description ,   
   Own_Delivery/out_order_tracking_totals.tot_delivers AS Value,
   @KPI_UpdatedAt     
FROM
   (
   SELECT   
      date_format(date_shipped, "%x%m") AS MonthNum,
      COUNT(*) AS Own_Delivery
   FROM   
   	  out_order_tracking_Year
   WHERE	
        shipping_carrier LIKE "%MENSAJERO%INTERNO%"
    AND is_presale = 0
    AND status_wms not in('cancelado', 'quebrado', 'quebra_tratada', 'ac_analisando_quebra', 'ac_estoque_reservado' )
  	AND cat_2 NOT LIKE "%CINE%"
    AND date_format(date_shipped, "%x%m") = @KPI_Month
   GROUP BY MonthNum
   ) AS TMP
INNER JOIN 	out_order_tracking_totals
     USING ( MonthNum )
;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `KPI_ORDER_AND_CUSTOMER_DATA_INCL_MARKETPLACE`(IN KPI_MonthNum INT)
BEGIN
/*
*  Kpi Group:  ORDER AND CUSTOMER DATA - INCL. MARKETPLACE
*  Created By: BI Team
*  Developer:  Carlos Antonio Mondragón Soria
*  Created at: 2013-10-03
*  Lastest Updated: 2013-10-03
*/

/*
*  Global Variables
*/
SET @KPI_Month=KPI_MonthNum;
SET @KPI_Year=SUBSTR( KPI_MonthNum FROM 1 FOR 4 );
SET @KPI_SpreadSheet=1;

SET @KPI_UpdatedAt = Now();
SET @KPI_Group = "ORDER AND CUSTOMER DATA - INCL. MARKETPLACE";

/*
* Sample Table Creations
*/																					
DROP TEMPORARY TABLE IF EXISTS A_Master_Year;
CREATE TEMPORARY TABLE A_Master_Year ( INDEX( ItemId ), INDEX( OrderNum ) )
SELECT * FROM A_Master
WHERE
   YEAR( date ) = @KPI_Year;

DROP TEMPORARY TABLE IF EXISTS A_Master_Orders_Year;
CREATE TEMPORARY TABLE A_Master_Orders_Year  ( INDEX (OrderNum ) )
SELECT * FROM Out_SalesReportOrder
WHERE
   YEAR( date ) = @KPI_Year;

DROP TEMPORARY TABLE IF EXISTS TMP;
CREATE TEMPORARY TABLE TMP ( PRIMARY KEY ( OrderNum ) )
SELECT OrderNum , SUM( Returns ) FROM A_Master_Year WHERE Returns = 1 GROUP BY OrderNum;

UPDATE A_Master_Orders_Year SET Returns = 0;
UPDATE 
              A_Master_Orders_Year 
   INNER JOIN TMP
        USING ( OrderNum )
SET
   A_Master_Orders_Year.Returns = 1
WHERE   
   A_Master_Orders_Year.OrderAfterCan = 0 
;

DROP TEMPORARY TABLE IF EXISTS out_order_tracking_Year;
CREATE TEMPORARY TABLE 	out_order_tracking_Year
SELECT * FROM production_ve.out_order_tracking 
WHERE 
(
   YEAR( date_ordered  ) = @KPI_Year
OR YEAR( date_shipped  ) = @KPI_Year
OR YEAR( date_exported ) = @KPI_Year
OR YEAR( date_procured ) = @KPI_Year
OR YEAR( date_ready_to_pick ) = @KPI_Year
OR YEAR( date_ready_to_ship ) = @KPI_Year
OR YEAR( date_1st_attempt )   = @KPI_Year
OR YEAR( date_delivered )     = @KPI_Year
OR YEAR( date_procured_promised      ) = @KPI_Year
OR YEAR( date_ready_to_ship_promised ) = @KPI_Year
OR YEAR( date_delivered_promised     ) = @KPI_Year
)

AND status_wms <> 'cancelado'     
;


/*Orders Shipped*/
DROP   TEMPORARY TABLE IF EXISTS item_tracking;
CREATE TEMPORARY TABLE item_tracking
SELECT 
   order_number, 
   item_id, 
   date_format(max(date_shipped), "%x%m") as month_shipped, 
   0 AS ItemsInOrder, 
   SUM( IF( date_shipped is not null, 1, 0 ) ) AS Shipped,
   SUM( IF( shipping_carrier like '%Mensajero%interno%' , 1, 0 ) ) AS OwnFleet
FROM out_order_tracking_Year  
WHERE
   status_wms <> 'cancelado'
GROUP BY order_number, item_id;

DROP   TEMPORARY TABLE IF EXISTS order_tracking;
CREATE TEMPORARY TABLE order_tracking
SELECT
	order_number,
	count(item_id) AS items_in_order,
	sum(shipped) AS items_shipped,
	month_shipped,
  IF( count(item_id) = sum(shipped) , 1 , 0 ) AS Shipped,
  IF( sum(OwnFleet) > 0  , 1 , 0 ) AS OwnFleet
FROM item_tracking  
GROUP BY order_number;


/*PerMonth Totals*/
DROP TEMPORARY TABLE IF EXISTS A_Master_Totals_PerMonth;
CREATE TEMPORARY TABLE A_Master_Totals_PerMonth
SELECT 
   MonthNum, 

   COUNT( Distinct ItemId )   AS Items,
	 COUNT( Distinct OrderNum ) AS Orders,
   SUM( Rev)                  AS Rev,   


   SUM( IF( OrderBeforeCan = 1, 1, 0 ) )     AS Gross_Items,
   0                                         AS Gross_Orders,
   SUM( IF( OrderBeforeCan = 1, Rev, 0 ) )   AS Gross_Rev,
   

   SUM( IF( OrderAfterCan = 1, 1, 0 ) )   AS Net_Items,
   0                                      AS Net_Orders,
   SUM( IF( OrderAfterCan = 1, Rev, 0 ) ) AS Net_Rev

FROM 
   A_Master_Year
GROUP BY MonthNum
;

DROP TEMPORARY TABLE IF EXISTS A_Master_Orders_PerMonth;
CREATE TEMPORARY TABLE A_Master_Orders_PerMonth
SELECT
   Month_Num as MonthNum,  
   COUNT(*)                                  AS Orders,

   SUM( OrderBeforeCan )                     AS Gross,
   SUM( IF( OrderBeforeCan = 1, Rev, 0 ) )   AS Gross_Rev,
   
   SUM( OrderAfterCan )                      AS Net,
   SUM( IF( OrderAfterCan = 1, Rev, 0 ) )    AS Net_Rev

FROM
   A_Master_Orders_Year
GROUP BY Month_Num;

UPDATE        A_Master_Totals_PerMonth 
   INNER JOIN A_Master_Orders_PerMonth
        USING ( MonthNum )
SET
   A_Master_Totals_PerMonth.Gross_Orders = A_Master_Orders_PerMonth.Gross,
   A_Master_Totals_PerMonth.Net_Orders   = A_Master_Orders_PerMonth.Net
;

/*
* KPIs calculations
*/
# Total Orders received
REPLACE KPIs
SELECT 
   MonthNum                  AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   12                        AS RowId ,
   "Total # orders received" AS KPI,
   "Orders received in BOB"  AS Description ,
   COUNT( DISTINCT OrderNum) ,
   @KPI_UpdatedAt
FROM 
   A_Master_Year
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum
;


# - thereof invalid
REPLACE KPIs
SELECT 
   Month_Num                 AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   13                        AS RowId ,
   "- thereof invalid"      AS KPI,
   "Orders invalidated (failed first fraud filter by Adyen or similar)"  AS Description ,
   COUNT(*)                 AS Value,
   @KPI_UpdatedAt  
FROM 
   A_Master_Orders_Year
WHERE
    OrderBeforeCan = 0
AND Month_Num = @KPI_Month
GROUP BY Month_Num
;

# - thereof canceled
REPLACE KPIs
SELECT 
  Month_Num                 AS MonthNum,
  @KPI_SpreadSheet          AS SpreadSheet,
  @KPI_Group                AS `Group`,
  15                        AS RowId ,
  "- thereof canceled"      AS KPI,
  "Orders cancelled or similar by customer or internally "
  "(for instance payslip system in some LatAm markets) "
  "(of the orders received in the period); only fully canceled orders"  AS Description ,
  SUM( IF( Cancelled = 1, 1, 0 ) ) AS Value,  
  @KPI_UpdatedAt  
FROM 
   A_Master_Orders_Year
WHERE Month_Num = @KPI_Month
GROUP BY Month_Num  
;

# - thereof rejected
REPLACE KPIs
SELECT 
  Month_Num                 AS MonthNum,
  @KPI_SpreadSheet          AS SpreadSheet,
  @KPI_Group                AS `Group`,
  17                        AS RowId ,
  "- thereof rejected"      AS KPI,
  "Orders refused on the day of "
  " shipment or post shipment,  "
  " for instance cash on delivery (no payment taken)"  AS Description ,
  SUM( IF( Rejected = 1, 1 , 0 ) ) AS Value,
  @KPI_UpdatedAt  
FROM 
   A_Master_Orders_Year 
WHERE Month_Num = @KPI_Month
GROUP BY Month_Num
;

# CHECK - thereof B2B
REPLACE KPIs
SELECT 
  MonthNum                  AS MonthNum,
  @KPI_SpreadSheet          AS SpreadSheet,
  @KPI_Group                AS `Group`,
  19                        AS RowId ,
  "- thereof B2B"           AS KPI,
  "B2B orders starting the "
  "fulfillment process minus "
  "canceled and rejected orders "
  "(before returned orders)"  AS Description ,
  COUNT( DISTINCT OrderNum ) AS Value,
  @KPI_UpdatedAt  
FROM 
   A_Master_Year
WHERE  
    ( OrderNum in   ( SELECT OrderNum FROM M_CorporativeOrders ) OR
      PrefixCode in ( "CDEAL" ) 
    )
AND OrderAfterCan = 1
AND MonthNum = @KPI_Month
GROUP BY MonthNum;


# Total # items in gross orders
REPLACE KPIs
SELECT 
  Month_Num                 AS MonthNum,
  @KPI_SpreadSheet          AS SpreadSheet,
  @KPI_Group                AS `Group`,
  20                        AS RowId ,
  "Total # items in gross orders" AS KPI,
  "Total number of items in gross orders (valid orders)"  AS Description ,
  SUM( IF( OrderBeforeCan = 1 , ItemsInOrder , 0 ) ) AS Value,
  @KPI_UpdatedAt  
FROM 
   A_Master_Orders_Year
WHERE Month_Num = @KPI_Month
GROUP BY Month_Num;

#Total # items in net orders post rejections
REPLACE KPIs
SELECT 
   
  MonthNum                 AS MonthNum,
  @KPI_SpreadSheet         AS SpreadSheet,
  @KPI_Group               AS `Group`,
  21                       AS RowId ,
  "Total # items in net orders post rejections" AS KPI,
  "Total number of items in net orders post rejections (before returns)"  AS Description ,
  SUM( IF(     OrderAfterCan = 1 
           AND     Cancelled = 0 
           AND      Rejected = 0, 1 , 0 ) ) AS Value,
  @KPI_UpdatedAt  
FROM 
   A_Master_Year
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;

# Total # orders shipped
REPLACE KPIs
SELECT 
  month_shipped AS MonthNum,
  @KPI_SpreadSheet         AS SpreadSheet,
  @KPI_Group               AS `Group`,
  24                       AS RowId ,
  "Total # orders shipped" AS KPI,
  "Total number of orders "
  "shipped in the period"          AS Description, 
  SUM( IF( Shipped = 1 , 1 , 0 ) ) AS Value,
  @KPI_UpdatedAt
FROM
   order_tracking
WHERE month_shipped = @KPI_Month
GROUP BY month_shipped;

# - thereof own delivery fleet
REPLACE KPIs
SELECT 
  month_shipped AS MonthNum,
  @KPI_SpreadSheet         AS SpreadSheet,
  @KPI_Group               AS `Group`,
  25                       AS RowId ,
  "- thereof own delivery fleet" AS KPI,
  "Total number of items shipped in the period"  AS Description, 
  SUM( IF( OwnFleet = 1, 1 , 0 ) ) AS Value,
  @KPI_UpdatedAt
FROM
   order_tracking
WHERE month_shipped = @KPI_Month
GROUP BY month_shipped;

# Total # items shipped
REPLACE KPIs
SELECT 
  month_shipped                                    AS MonthNum,  
  #@KPI_Month,
  @KPI_SpreadSheet         AS SpreadSheet,
  @KPI_Group                                       AS `Group`,   
  26                                               AS RowId ,
  "Total # items shipped"                          AS KPI,
  "Total number of items shipped in the period"    AS Description ,
  SUM( IF( Shipped , 1 , 0 ) )     AS Value,
  # 0 AS Value,
   @KPI_UpdatedAt  
FROM 
   item_tracking
WHERE month_shipped = @KPI_Month
GROUP BY MonthNum;

#27 Total # items returned
#Total # items returned
REPLACE KPIs
SELECT 
  DATE_FORMAT(date_inbound_il,'%x%m')           AS MonthNum,  
  #@KPI_Month,
  @KPI_SpreadSheet         AS SpreadSheet,
  @KPI_Group                                       AS `Group`,   
  27                                               AS RowId ,
  "Total # items returned"                         AS KPI,
  "Total number of items returned during the period"   AS Description ,
   sum(is_returned)                                   AS Value,
   @KPI_UpdatedAt  
FROM
   operations_ve.out_inverse_logistics_tracking
WHERE DATE_FORMAT(date_inbound_il,'%x%m') = @KPI_Month
GROUP BY MonthNum
;

#Total # items returned (definition 2)

REPLACE KPIs
SELECT 
  #DATE_FORMAT(date_ordered,'%x%m')                AS MonthNum,  
  @KPI_Month AS MonthNum,  
  @KPI_SpreadSheet         AS SpreadSheet,
  @KPI_Group                                       AS `Group`,   
  28                                               AS RowId ,
  "Total # items returned (definition 2)"          AS KPI,
  "Total number of items returned for the period in which order was placed (to be adjusted retroactively)"   AS Description ,
   sum(is_returned)                                   AS Value,

   @KPI_UpdatedAt  
FROM
   operations_ve.out_inverse_logistics_tracking
WHERE
   DATE_FORMAT(date_ordered,'%x%m') = @KPI_Month
GROUP BY
   MonthNum;


#28 Total # items returned (definition 2)

# of new customers (based on gross orders)
DROP TEMPORARY TABLE IF EXISTS TMP_KPI_Customers;
CREATE TEMPORARY TABLE TMP_KPI_Customers ( Id INT AUTO_INCREMENT, 
                                           PRIMARY KEY( CustomerNum, Id ) 
                                         )
SELECT
   CustomerNum,
   NULL AS Id,
   Date,
   Month_num,
   OrderNum,
   Rejected,
   Returns
FROM
   A_Master_Orders_Year  
WHERE
    OrderBeforeCan = 1
ORDER BY CustomerNum,Date
;
DELETE FROM TMP_KPI_Customers WHERE Id > 1;

REPLACE KPIs
SELECT 
  Month_Num                AS MonthNum,  
  @KPI_SpreadSheet         AS SpreadSheet,
  @KPI_Group              AS `Group`,   
  31                                               AS RowId ,
  "Total # items shipped"                          AS KPI,
  "Total number of items shipped in the period"    AS Description ,
  COUNT( DISTINCT CustomerNum ) AS Value,
  @KPI_UpdatedAt
FROM 
  TMP_KPI_Customers
WHERE Month_Num = @KPI_Month
GROUP BY MonthNum;


# of new customers (based on gross orders)
DROP TEMPORARY TABLE IF EXISTS TMP_KPI_Customers;
CREATE TEMPORARY TABLE TMP_KPI_Customers ( Id INT AUTO_INCREMENT, 
                                           PRIMARY KEY( CustomerNum, Id ) 
                                         )
SELECT
   CustomerNum,
   NULL AS Id,
   Date,
   Month_num,
   OrderNum,
   Rejected,
   Returns
FROM
   A_Master_Orders_Year  
WHERE
        OrderBeforeCan = 1
    AND Cancelled = 0
    AND Rejected  = 0
ORDER BY CustomerNum,Date
;
DELETE FROM TMP_KPI_Customers WHERE Id > 1;

# of new customers (based on net orders post rejection)
REPLACE KPIs
SELECT 
  Month_Num  AS MonthNum,  
  @KPI_SpreadSheet         AS SpreadSheet,
  @KPI_Group AS `Group`,   
  32                                               AS RowId ,
  "of new customers (based on net orders post rejection)" AS KPI,
  "Number of new customers with at least one valid purchase (based on net orders post rejections)"    AS Description ,
  COUNT( DISTINCT CustomerNum ) AS Value,
  @KPI_UpdatedAt
FROM 
  TMP_KPI_Customers
WHERE     Month_Num = @KPI_Month
group by Month_Num
;

# of new customers (based on net orders post rejections and returns)
DROP TEMPORARY TABLE IF EXISTS TMP_KPI_Orders;
CREATE TEMPORARY TABLE TMP_KPI_Orders
SELECT  
   MonthNum,
   CustomerNum,
   OrderNum,
   date
FROM
   A_Master
WHERE  
       MonthNum <= @KPI_Month
   AND Date > '2012-05-08'
   AND OrderBeforeCan = 1 
   AND Cancellations = 0 
   AND Rejected = 0
GROUP BY OrderNum
;

DROP TEMPORARY TABLE IF EXISTS TMP_KPI_Customers;
CREATE TEMPORARY TABLE TMP_KPI_Customers ( id int auto_increment, PRIMARY KEY ( CustomerNum, id ) )
SELECT  
   MonthNum AS MonthNum,
   CustomerNum,
   null as id,
   date
FROM
  TMP_KPI_Orders
ORDER BY OrderNum ASC, date ASC 
;

# of new customers (based on gross orders)
DROP TEMPORARY TABLE IF EXISTS TMP_KPI_Customers;
CREATE TEMPORARY TABLE TMP_KPI_Customers ( Id INT AUTO_INCREMENT, 
                                           PRIMARY KEY( CustomerNum, Id ) 
                                         )
SELECT
   CustomerNum,
   NULL AS Id,
   Date,
   Month_num,
   OrderNum,
   Rejected,
   Returns
FROM
   A_Master_Orders_Year  
WHERE
        OrderBeforeCan = 1
    AND Cancelled = 0
    AND Rejected  = 0
    AND Returns   = 0
ORDER BY CustomerNum,Date
;

REPLACE KPIs
SELECT 
  @KPI_Month              AS MonthNum,  
  @KPI_SpreadSheet        AS SpreadSheet,
  @KPI_Group              AS `Group`,   
  33                      AS RowId ,
  "of new customers (based on net orders post rejection and returns)" AS KPI,
  "Number of new customers with at least one valid purchase (based on net orders post rejections and returns)"    AS Description ,
  COUNT( DISTINCT CustomerNum ) AS Value,
  @KPI_UpdatedAt
FROM 
  TMP_KPI_Customers
WHERE  Month_Num = @KPI_Month
   AND id = 1
GROUP BY MonthNum
;


# of repeat customers (based on net orders post rejections)
REPLACE KPIs
SELECT 
  Month_Num                AS MonthNum,  
  @KPI_SpreadSheet         AS SpreadSheet,
  @KPI_Group              AS `Group`,   
  34                                               AS RowId ,
  "of repeat customers (based on net orders post rejections)" AS KPI,
  "Number of existing customers with at least one valid "
  "purchase in period (based on net orders post rejections)"    AS Description ,
  COUNT( DISTINCT CustomerNum ) AS Value,
  @KPI_UpdatedAt
FROM 
  TMP_KPI_Customers
WHERE  Month_Num = @KPI_Month
   AND id != 1
GROUP BY MonthNum
;

REPLACE KPIs
SELECT 
  @KPI_Month              AS MonthNum,  
  @KPI_SpreadSheet        AS SpreadSheet,
  @KPI_Group              AS `Group`,   
  36                                               AS RowId ,
  "Total customer accounts per end of month (#)" AS KPI,
  "Number of customers in the database with at least one valid purchase post rejections (cumulated)"    AS Description ,
  COUNT( DISTINCT CustomerNum ) AS Value,
  @KPI_UpdatedAt
FROM 
  TMP_KPI_Customers
WHERE  Month_Num <= @KPI_Month
GROUP BY MonthNum
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `KPI_ORDER_REVENUE_DRIVERS`(IN KPI_MonthNum INT)
BEGIN
/*
*  Kpi Group:  ORDER REVENUE DRIVERS
*  Created By: BI Team
*  Developer:  Carlos Antonio Mondragón Soria
*  Created at: 2013-10-03
*  Lastest Updated: 2013-10-03
*/
SET @KPI_Month=KPI_MonthNum;
SET @KPI_Year=SUBSTR( KPI_MonthNum FROM 1 FOR 4 );

SET @KPI_SpreadSheet = 1;
SET @KPI_UpdatedAt = Now();
SET @KPI_Group = "ORDER REVENUE DRIVERS";

/*
* SAMPLE TABLE
*/

/*
* KPI Calculations
*/ 
SET @index=576;
SET @indexIni=576;
SET @monthNumAct=0;
SET @KPI = "Active listings by category - ";
SET @KPI_Description = "Number of online listings at the end of the month by category";

REPLACE KPIs
SELECT
   MonthNum,
   SpreadSheet,
   `Group`,
   RowId,
   KPI,
   descripcion,
   Value ,
   KPI_UpdatedAt
FROM
(
SELECT
   KPI.MonthNum,
   @KPI_SpreadSheet AS SpreadSheet, 
   @KPI_Group               AS `Group`,
   @index := IF( @monthNumAct != KPI.MonthNum , @indexIni ,
                 @index + 1 ) AS RowId,
   @monthNumAct := KPI.MonthNum ,
   CONCAT( @KPI , KPI.CatKPI )  AS KPI,
   @KPI_Description  AS descripcion,
   KPI.Value,
   @KPI_UpdatedAt AS KPI_UpdatedAt
FROM
(
   SELECT
      @KPI_Month AS MonthNum, 
      M_CategoryKPI.CatKPI,
      COALESCE( TMP.Value, 0)  AS Value,
      KPIorder 
   FROM
      development_mx.M_CategoryKPI
      LEFT JOIN
      (
         SELECT 
            @KPI_Month AS MonthNum, 
            Cat_KPI AS CatKPI,
            COUNT( DISTINCT sku_config ) AS Value		 
        FROM 
           A_Master_Catalog
        WHERE
               isVisible     = 1
           AND isMarketPlace = 1

        GROUP BY MonthNum, CatKPI
      ) AS TMP 
      ON     TMP.CatKPI = development_mx.M_CategoryKPI.CatKPI
   GROUP BY MonthNum , CONCAT( @KPI , TMP.CatKPI )
   ORDER BY
      MonthNum, development_mx.M_CategoryKPI.KPIorder
) AS KPI
ORDER BY
   MonthNum, KPIorder
) AS Final
;


SET @index=592;
SET @indexIni=592;
SET @monthNumAct=0;
SET @KPI = "Gross valid merchandise orders by category - ";
SET @KPI_Description = "";

REPLACE KPIs
SELECT
   MonthNum,
   SpreadSheet,
   `Group`,
   RowId,
   KPI,
   descripcion,
   Value ,
   KPI_UpdatedAt
FROM
(
SELECT
   KPI.MonthNum,
   @KPI_SpreadSheet         AS SpreadSheet,
   @KPI_Group               AS `Group`,
   @index := IF( @monthNumAct != KPI.MonthNum , @indexIni ,
                 @index + 1 ) AS RowId,
   @monthNumAct := KPI.MonthNum ,
   CONCAT( @KPI , KPI.CatKPI )  AS KPI,
   @KPI_Description  AS descripcion,
   KPI.Value,
   @KPI_UpdatedAt AS KPI_UpdatedAt
FROM
(
   SELECT
      @KPI_Month AS MonthNum,
      M_CategoryKPI.CatKPI,
      COALESCE(TMP.Value,0) AS Value,
      KPIorder 
   FROM
      development_mx.M_CategoryKPI
      LEFT JOIN
      (
         SELECT 
            MonthNum, 
            CatKPI,
            COUNT( DISTINCT OrderNum ) AS Value		 
        FROM 
                      A_Master
           INNER JOIN bob_live_ve.sales_order_item AS soi 
                   ON A_Master.ItemId = soi.id_sales_order_item 
        WHERE
               YEAR( Date ) = @KPI_Year
           and soi.is_option_marketplace = 1
           AND OrderBeforeCan = 1
           AND MonthNum = @KPI_Month
           AND MonthNum = @KPI_Month
        GROUP BY MonthNum, CatKPI
      ) AS TMP 
      ON     TMP.CatKPI = development_mx.M_CategoryKPI.CatKPI
   GROUP BY MonthNum , CONCAT( @KPI , TMP.CatKPI )
   ORDER BY
      MonthNum, development_mx.M_CategoryKPI.KPIorder
) AS KPI
ORDER BY
   MonthNum, KPIorder
) AS Final
;

SET @index=608;
SET @indexIni=608;
SET @monthNumAct=0;
SET @KPI = "Net merchandise orders by category - ";
SET @KPI_Description = "";

REPLACE KPIs
SELECT
   MonthNum,
   SpreadSheet,
   `Group`,
   RowId,
   KPI,
   descripcion,
   Value ,
   KPI_UpdatedAt
FROM
(
SELECT
   KPI.MonthNum,
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group               AS `Group`,
   @index := IF( @monthNumAct != KPI.MonthNum , @indexIni ,
                 @index + 1 ) AS RowId,
   @monthNumAct := KPI.MonthNum ,
   CONCAT( @KPI , KPI.CatKPI )  AS KPI,
   @KPI_Description  AS descripcion,
   KPI.Value,
   @KPI_UpdatedAt AS KPI_UpdatedAt
FROM
(
   SELECT
      @KPI_Month AS MonthNum,
      M_CategoryKPI.CatKPI,
      COALESCE(TMP.Value,0) AS Value,
      KPIorder 
   FROM
      development_mx.M_CategoryKPI
      LEFT JOIN
      (
         SELECT 
            MonthNum, 
            CatKPI,
            COUNT( DISTINCT OrderNum ) AS Value		 
        FROM 
                      A_Master
           INNER JOIN bob_live_ve.sales_order_item AS soi 
                   ON A_Master.ItemId = soi.id_sales_order_item 
        WHERE
               YEAR( Date ) = @KPI_Year
           and soi.is_option_marketplace = 1
           and Cancellations  = 0
           AND OrderBeforeCan = 1
           AND MonthNum = @KPI_Month

        GROUP BY MonthNum, CatKPI
      ) AS TMP 
      ON     TMP.CatKPI = development_mx.M_CategoryKPI.CatKPI
   GROUP BY MonthNum , CONCAT( @KPI , TMP.CatKPI )
   ORDER BY
      MonthNum, development_mx.M_CategoryKPI.KPIorder
) AS KPI
ORDER BY
   MonthNum, KPIorder
) AS Final
;

SET @index=624;
SET @indexIni=624;
SET @monthNumAct=0;
SET @KPI = "Net merchandise revenues by category - ";
SET @KPI_Description = "";

REPLACE KPIs
SELECT
   MonthNum,
   SpreadSheet,
   `Group`,
   RowId,
   KPI,
   descripcion,
   Value ,
   KPI_UpdatedAt
FROM
(
SELECT
   KPI.MonthNum,
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group               AS `Group`,
   @index := IF( @monthNumAct != KPI.MonthNum , @indexIni ,
                 @index + 1 ) AS RowId,
   @monthNumAct := KPI.MonthNum ,
   CONCAT( @KPI , KPI.CatKPI )  AS KPI,
   @KPI_Description  AS descripcion,
   KPI.Value,
   @KPI_UpdatedAt AS KPI_UpdatedAt
FROM
(
   SELECT
      TMP.MonthNum,
      TMP.CatKPI,
      TMP.Value,
      KPIorder 
   FROM
      development_mx.M_CategoryKPI
      INNER JOIN
      (
         SELECT 
            MonthNum AS MonthNum, 
            CatKPI,
            SUM( Rev ) / 1000 AS Value		 
         FROM          A_Master
            INNER JOIN bob_live_ve.sales_order_item AS soi 
                    ON A_Master.ItemId = soi.id_sales_order_item 
         WHERE
                YEAR( Date ) = @KPI_Year
           AND soi.is_option_marketplace = 1
           AND Cancellations  = 0
           AND Rejected       = 0        
           AND OrderBeforeCan = 1
           AND MonthNum = @KPI_Month

        GROUP BY MonthNum, CatKPI
      ) AS TMP 
      ON     TMP.CatKPI = development_mx.M_CategoryKPI.CatKPI
   GROUP BY MonthNum , CONCAT( @KPI , TMP.CatKPI )
   ORDER BY
      MonthNum, development_mx.M_CategoryKPI.KPIorder
) AS KPI
ORDER BY
   MonthNum, KPIorder
) AS Final
;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `KPI_PL_ACCOUNTING_VIEW_NOT_CASH_VIEW`(IN KPI_MonthNum INT)
BEGIN
/*
*  Kpi Group:  NET PROMOTER SCORE
*  Created By: BI Team
*  Developer:  Eduardo Martinez
*  Created at: 2013-11-01
*  Lastest Updated: 2013-11-01
*/

/*
*  Global Variables
*/
SET @KPI_Month=KPI_MonthNum;
SET @KPI_Year=SUBSTR( KPI_MonthNum FROM 1 FOR 4 );

SET @KPI_UpdatedAt = Now();
SET @KPI_SpreadSheet = 1;
SET @KPI_Group='P&L (ACCOUNTING VIEW - NOT CASH VIEW)';


#Row Id - 262
#Net revenue from merchandise (in k local)
#Net sales as per accounting rules (usually sales recognition as per shipped order or, for COD, upon proof of delivery) after VAT, discounts, coupons, returns and cancelations; includes provision for returns
REPLACE KPIs
select 
   a.Yearmonth               AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   262                       AS RowId ,
   "Net revenue from merchandise (in k local)" AS KPI,
   "Net sales as per accounting rules (usually "
   "sales recognition as per shipped order or, "
   "for COD, upon proof of delivery) after VAT,"
   " discounts, coupons, returns and cancelations; "
   "includes provision for returns"  AS Description ,
   COALESCE(a.DelSales,0) - COALESCE(b.RetSales,0)   AS Value,
   @KPI_UpdatedAt
# from (select date_format(DateDelivered,'%Y%m') as 'Yearmonth', sum(PriceAfterTax - CouponValueAfterTax)/1000 as 'DelSales'
 from (select date_format(DateDelivered,'%Y%m') as 'Yearmonth', sum(Rev-ShippingFee-Interest)/1000 as 'DelSales'

			from A_Master
			where     DateDelivered >= 20130101
            and isMPlace = 0
            and not ( OrderNum in   ( SELECT OrderNum FROM M_CorporativeOrders ) OR PrefixCode in ( "CDEAL" ) )
            and date_format(DateDelivered,'%Y%m') <= @KPI_Month
				#and Delivered = 1
			group by date_format(DateDelivered,'%Y%m')
			order by date_format(DateDelivered,'%Y%m') asc) a
#LEFT join (select date_format(DateReturned,'%Y%m') as 'Yearmonth', sum(PriceAfterTax - CouponValueAfterTax)/1000 as 'RetSales'
LEFT join (select date_format(DateReturned,'%Y%m') as 'Yearmonth', sum(Rev-ShippingFee-Interest)/1000 as 'RetSales'

						from A_Master
						where     DateReturned >= 20130101
                  and isMPlace = 0
                  and not ( OrderNum in   ( SELECT OrderNum FROM M_CorporativeOrders ) OR PrefixCode in ( "CDEAL" ) )
                  and date_format(DateReturned,'%Y%m') <= @KPI_Month
							#and Returned = 1
						group by date_format(DateReturned,'%Y%m')
						order by date_format(DateReturned,'%Y%m') asc) b
on a.Yearmonth = b.Yearmonth
where a.Yearmonth <> (select Month_Num									
											from Out_SalesReportOrder
											order by Month_Num desc
											limit 1)
;

#Row 263
#Net revenue from B2B merchandise sales (in k local) 
#Net sales as per accounting rules (usually sales recognition 
#as per shipped order or, for COD, upon proof of delivery); after VAT, discounts, coupons, 
#returns and cancelations; includes provision for returns
REPLACE KPIs
select 
   a.Yearmonth               AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   263                       AS RowId ,
   "Net revenue from B2B merchandise sales (in k local) " AS KPI,
   "Net sales as per accounting rules (usually sales recognition "
   "as per shipped order or, for COD, upon proof of delivery); after VAT, discounts, coupons, "
   "returns and cancelations; includes provision for returns" AS Description ,
   COALESCE(a.DelSales,0) - COALESCE(b.RetSales,0)   AS Value,
   @KPI_UpdatedAt
# from (select date_format(DateDelivered,'%Y%m') as 'Yearmonth', sum(PriceAfterTax - CouponValueAfterTax)/1000 as 'DelSales'
 from (select date_format(DateDelivered,'%Y%m') as 'Yearmonth', sum(Rev-ShippingFee-Interest)/1000 as 'DelSales'

			from A_Master
			where DateDelivered >= 20130101
				#and Delivered = 1
             and ( OrderNum in   ( SELECT OrderNum FROM M_CorporativeOrders ) OR PrefixCode in ( "CDEAL" ) )
            and date_format(DateDelivered,'%Y%m') <= @KPI_Month
			group by date_format(DateDelivered,'%Y%m')
			order by date_format(DateDelivered,'%Y%m') asc) a
#LEFT  join (select date_format(DateReturned,'%Y%m') as 'Yearmonth', sum(PriceAfterTax - CouponValueAfterTax)/1000 as 'RetSales'
LEFT  join (select date_format(DateReturned,'%Y%m') as 'Yearmonth', sum(Rev-ShippingFee-Interest)/1000 as 'RetSales'

						from A_Master
						where DateReturned >= 20130101
							#and Returned = 1
             and ( OrderNum in   ( SELECT OrderNum FROM M_CorporativeOrders ) OR PrefixCode in ( "CDEAL" ) )
                  and date_format(DateReturned,'%Y%m') <= @KPI_Month
						group by date_format(DateReturned,'%Y%m')
						order by date_format(DateReturned,'%Y%m') asc) b
on a.Yearmonth = b.Yearmonth
where a.Yearmonth <> (select Month_Num									
											from Out_SalesReportOrder
											order by Month_Num desc
											limit 1)
;



#Row Id - 265
#Shipping net revenues (in k local)
#Revenues derived from charging customers for shipping
REPLACE KPIs
select 
   a.Yearmonth               AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   265                       AS RowId ,
   "Shipping net revenues (in k local)" AS KPI,
   "Shipping net revenues (in k local)"
   "Revenues derived from charging customers for shipping" AS Description ,
   COALESCE(a.DelShipRev,0) - COALESCE(b.RetShipRev,0) as Value,
   @KPI_UpdatedAt
  
from (select date_format(DateDelivered,'%Y%m') as 'Yearmonth', sum(ShippingFee)/1000 as 'DelShipRev'
			from A_Master
			where DateDelivered >= 20130101
				and Delivered = 1
            and date_format(DateDelivered,'%Y%m') <= @KPI_Month
			group by date_format(DateDelivered,'%Y%m')
			order by date_format(DateDelivered,'%Y%m') asc) a
LEFT  join (select date_format(DateReturned,'%Y%m') as 'Yearmonth', sum(ShippingFee)/1000 as 'RetShipRev'
						from A_Master
						where DateReturned >= 20130101
                  and date_format(DateReturned,'%Y%m') <= @KPI_Month
							and Returns = 1
						group by date_format(DateReturned,'%Y%m')
						order by date_format(DateReturned,'%Y%m') asc) b
on a.Yearmonth = b.Yearmonth
where a.Yearmonth <> (select Month_Num									
											from Out_SalesReportOrder
											order by Month_Num desc
											limit 1)
;

#Row Id - 267
#Revenues derived from charging customers for shipping
REPLACE KPIs
SELECT
   a.Yearmonth               AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   267                       AS RowId ,
   "Net venture marketplace revenues (in k local)" AS KPI,
   "Net merchandise revenues * commission" AS Description,
   COALESCE(a.DelSales,0) - COALESCE(b.RetSales,0) AS Value,
   @KPI_UpdatedAt
#from (select date_format(DateDelivered,'%Y%m') as 'Yearmonth', ( sum(PriceAfterTax - CouponValueAfterTax ) )/1000 as 'DelSales',
from (select date_format(DateDelivered,'%Y%m') as 'Yearmonth', ( sum(Rev-ShippingFee-Interest ) )/1000 as 'DelSales',

       SUM( COGS )/1000 AS DelCOGS
			from A_Master
			where     DateDelivered >= 20130101
            and isMPlace = 1
            and date_format(DateDelivered,'%Y%m') <= @KPI_Month
				#and Delivered = 1
			group by date_format(DateDelivered,'%Y%m')
			order by date_format(DateDelivered,'%Y%m') asc) a
#LEFT  join (select date_format(DateReturned,'%Y%m') as 'Yearmonth', ( sum(PriceAfterTax - CouponValueAfterTax )  )/1000 as 'RetSales',
LEFT  join (select date_format(DateReturned,'%Y%m') as 'Yearmonth', ( sum(Rev-ShippingFee-Interest )  )/1000 as 'RetSales',

            SUM( COGS )/1000 AS RetCOGS
						from A_Master
						where DateReturned >= 20130101
                  and date_format(DateReturned,'%Y%m') <= @KPI_Month
            and isMPlace = 1

							#and Returned = 1
						group by date_format(DateReturned,'%Y%m')
						order by date_format(DateReturned,'%Y%m') asc) b
on a.Yearmonth = b.Yearmonth
where a.Yearmonth <> (select Month_Num									
											from development_mx.Out_SalesReportOrder
											order by Month_Num desc
											limit 1)
;


#Row Id - 270
#Net financial revenue (in k local)
#Revenue from sale of financing solutions to customers (also if in cooperation with financial institution)
REPLACE KPIs
select 
   a.Yearmonth               AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   270                       AS RowId ,
   "Shipping net revenues (in k local)" AS KPI,
   "Shipping net revenues (in k local)"
   "Revenues derived from charging customers for shipping" AS Description ,
   a.DelInterest - b.RetInterest as Value,
   @KPI_UpdatedAt
from (select date_format(DateDelivered,'%Y%m') as 'Yearmonth', sum(Interest)/1000 as 'DelInterest'
			from A_Master
			where DateDelivered >= 20130101
				and Delivered = 1
            and date_format(DateDelivered,'%Y%m') <= @KPI_Month
			group by date_format(DateDelivered,'%Y%m')
			order by date_format(DateDelivered,'%Y%m') asc) a
LEFT  join (select date_format(DateReturned,'%Y%m') as 'Yearmonth', sum(Interest)/1000 as 'RetInterest'
						from A_Master
						where DateReturned >= 20130101
                  and date_format(DateReturned,'%Y%m') <= @KPI_Month
							#and Returned = 1
						group by date_format(DateReturned,'%Y%m')
						order by date_format(DateReturned,'%Y%m') asc) b
on a.Yearmonth = b.Yearmonth
where a.Yearmonth <> (select Month_Num									
											from Out_SalesReportOrder
											order by Month_Num desc
											limit 1)
;





#Row Id - 273
#Basic COGS
#Cost of Goods Sold (net of returns) - reductions in purchase price + cost of inbound logistics if possible, FIFO
REPLACE KPIs
SELECT
   a.Yearmonth               AS MonthNum,
   @KPI_SpreadSheet          AS SpreadSheet,
   @KPI_Group                AS `Group`,
   273                       AS RowId ,
   "Basic COGS"              AS KPI,

   "Cost of Goods Sold (net of returns) - "
   "reductions in purchase price + cost of "
   "inbound logistics if possible, FIFO   " AS Description,
   ( COALESCE(a.DelCOGS,0) - COALESCE(b.RetCOGS,0) ) *-1 as Value,
   @KPI_UpdatedAt
from (select date_format(DateDelivered,'%Y%m') as 'Yearmonth', sum(COGS)/1000 as 'DelCOGS'
			from A_Master
			where DateDelivered >= 20130101
				#and Delivered = 1
            and isMPlace = 0
            and date_format(DateDelivered,'%Y%m') <= @KPI_Month
			group by date_format(DateDelivered,'%Y%m')
			order by date_format(DateDelivered,'%Y%m') asc) a
LEFT join (select date_format(DateReturned,'%Y%m') as 'Yearmonth', sum(COGS)/1000 as 'RetCOGS'
						from A_Master
						where DateReturned >= 20130101
							#and Returned = 1
                  and isMPlace = 0

                  and date_format(DateReturned,'%Y%m') <= @KPI_Month
						group by date_format(DateReturned,'%Y%m')
						order by date_format(DateReturned,'%Y%m') asc) b
on a.Yearmonth = b.Yearmonth
where a.Yearmonth <> (select Month_Num									
											from Out_SalesReportOrder
											order by Month_Num desc
											limit 1)
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `KPI_REVENUE_WATERFALL_INCL_MARKETPLACE`(IN KPI_MonthNum INT)
BEGIN
/*
*  Kpi Group:  REVENUE WATERFALL INCL MARKETPLACE
*  Created By: BI Team
*  Developer:  Carlos Antonio Mondragón Soria
*  Created at: 2013-10-03
*  Lastest Updated: 2014-4-21
*/
SET @KPI_Month=KPI_MonthNum;
SET @KPI_Year=SUBSTR( KPI_MonthNum FROM 1 FOR 4 );

SET @KPI_SpreadSheet = 1;
SET @KPI_UpdatedAt = Now();
SET @KPI_Group = "REVENUE WATERFALL INCL MARKETPLACE";

/*
* SAMPLE TABLES
*/
DROP TEMPORARY TABLE IF EXISTS A_Master_Year;
CREATE TEMPORARY TABLE A_Master_Year ( INDEX( ItemId ) )
SELECT * FROM development_ve.A_Master
WHERE YEAR( date ) = @KPI_Year;

/*
*  KPIs Calculation
*/

# Gross revenue before validation (in k local)
REPLACE KPIs
SELECT 
   A_Master_Year.MonthNum    AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   240         AS RowID,
   "Gross revenue before validation (in k local)" AS KPI,
   "Revenue of all orders received in BOB (before VAT); including marketplace" AS Description,
   sum(OriginalPrice)/1000 AS Value,
   @KPI_UpdatedAt
FROM 
   A_Master_Year
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;


# VAT (or similar) (in k local)
REPLACE KPIs
SELECT 
   A_Master_Year.MonthNum    AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   241         AS RowID,
   "VAT (or similar) (in k local)" AS KPI,
   "VAT (or similar)" AS Description,
   sum(OriginalPrice*(TaxPercent/(100+TaxPercent)))/-1000 AS Value,
   @KPI_UpdatedAt
FROM 
   A_Master_Year
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;

#Invalid net revenue (in k local)
REPLACE KPIs
SELECT 
   A_Master_Year.MonthNum    AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   243         AS RowID,
   "Invalid net revenue (in k local)" AS KPI,
   "Revenue lost due to invalid orders (after VAT); including marketplace" AS Description,
   sum( IF( OrderBeforeCan = 0 , OriginalPrice/((100+TaxPercent)/100), 0 ) )/-1000 AS Value,
   @KPI_UpdatedAt
FROM 
   A_Master_Year
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;

#Cancelation (in k local)
REPLACE KPIs
SELECT 
   A_Master_Year.MonthNum    AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   245         AS RowID,
   "Cancelation (in k local)" AS KPI,
   "Revenue lost due to cancelations "
   "(initiated by customer and internally, "
   "for instance payslip system in some LatAm "
   "markets) (after VAT); including marketplace" AS Description,
   SUM(IF( 	OrderBeforeCan = 1 AND Cancelled  = 1, OriginalPrice/((100+TaxPercent)/100) , 0 ) ) / -1000 AS Value,
   @KPI_UpdatedAt
FROM 
   A_Master_Year
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;

# Discounts (in k local)
REPLACE KPIs
SELECT 
   A_Master_Year.MonthNum    AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   247         AS RowID,
   "Discounts (in k local)" AS KPI,
   "On-site discounts on the sales price (after VAT if applicable); including marketplace" AS Description,
   -1 * ( SUM(if(OrderBeforeCan = 1 AND Cancelled = 0, OriginalPrice/((100+TaxPercent)/100)- PriceAfterTax,0))/1000) AS Value,
   @KPI_UpdatedAt
FROM 
   A_Master_Year
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;



#249 Coupons (in k local)
REPLACE KPIs
SELECT 
   A_Master_Year.MonthNum    AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   249         AS RowID,
   "Coupons (in k local)" AS KPI,
   "Revenue lost due to coupons and redemptions "
   "(after VAT if applicable); including marketplace"
   AS Description,
   SUM(IF( OrderBeforeCan = 1 AND NOT ( Cancellations  = 1 AND Rejected = 0) , CouponValueAfterTax, 0 ) ) / -1000 AS Value,
   @KPI_UpdatedAt
FROM 
   A_Master_Year
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;

# Special discounts

# Rejections (in k local)
REPLACE KPIs
SELECT 
   A_Master_Year.MonthNum    AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   253         AS RowID,
   "Rejections (in k local)" AS KPI,
   "Revenue lost due to rejections /"
   "refusals by customer (customer cannot "
   "be reached by the carrier) (after VAT); "
   "including marketplace" AS Description,
   SUM(IF( OrderBeforeCan = 1 AND NOT ( Cancellations  = 1 AND Rejected = 0) AND Rejected = 1 , PriceAfterTax - CouponValueAfterTax , 0 ) ) / -1000 AS Value,
   @KPI_UpdatedAt
FROM 
   A_Master_Year
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;

#255 Returns (in k local)
REPLACE KPIs
SELECT 
   A_Master_Year.MonthNum    AS MonthNum, 
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group  AS`Group`,
   255         AS RowID,
   "Returns (in k local)" AS KPI,
   "Revenue lost due to customer returns (after VAT); "
   "including marketplace" AS Description,
   #SUM(IF(     OrderBeforeCan = 1
	 #        and Cancelled = 0
	 #        and Rejected = 0
	 #        and `Returns` = 1
   #        , PriceAfterTax - CouponValueAfterTax , 0 ) ) / -1000 AS Value,
   SUM(IF( OrderBeforeCan = 1 AND NOT ( Cancellations  = 1 AND Rejected = 0) AND Rejected = 0
	         and `Returns`     = 1
           , PriceAfterTax - CouponValueAfterTax , 0 ) ) / -1000 AS Value,

   @KPI_UpdatedAt
FROM 
   A_Master_Year
WHERE MonthNum = @KPI_Month
GROUP BY MonthNum;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `KPI_VOLUME_DRIVERS_INCL_MARKETPLACE`(IN KPI_MonthNum INT)
BEGIN
/*
*  Kpi Group:  VOLUME DRIVERS - INCL. MARKETPLACE
*  Created By: BI Team
*  Developer:  Carlos Antonio Mondragón Soria
*  Created at: 2013-10-03
*  Lastest Updated: 2013-10-03
*/
SET @KPI_Month=KPI_MonthNum;
SET @KPI_Year=SUBSTR( KPI_MonthNum FROM 1 FOR 4 );
#SET @KPI_Month=201309;
#SET @KPI_Year=SUBSTR( 201309 FROM 1 FOR 4 );

SET @KPI_UpdatedAt = Now();
SET @KPI_SpreadSheet = 1;
SET @KPI_Group = "VOLUME DRIVERS - INCL. MARKETPLACE";

/*
* Sample Table Creations
*/
DROP TEMPORARY TABLE IF EXISTS A_Master_Year;
CREATE TEMPORARY TABLE A_Master_Year ( INDEX( ItemId ) )
SELECT * FROM A_Master
WHERE
   YEAR( date ) = @KPI_Year;

DROP TEMPORARY TABLE IF EXISTS A_Master_Orders_Year;
CREATE TEMPORARY TABLE A_Master_Orders_Year 
SELECT * FROM Out_SalesReportOrder
WHERE
   YEAR( date ) = @KPI_Year;

/*
* KPIs calculations
*/

# of gross items per category
SET @index=40;
SET @indexIni=40;
SET @monthNumAct=0;
SET @KPI = "# of gross items per category - ";
SET @KPI_Description = "Gross items as per gross orders; easier to pull out "
   "from BOB / DWH; should correlate with net items (after "
   "cancelations and returns)";

REPLACE KPIs
SELECT
   MonthNum,
   SpreadSheet,
   `Group`,
   RowId,
   KPI,
   descripcion,
   Value,
   KPI_UpdatedAt
FROM
(
SELECT
   @KPI_Month AS MonthNum,
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group               AS `Group`,
   @index := IF( @monthNumAct != KPI.MonthNum , @indexIni ,
                 @index + 1 ) AS RowId,
   @monthNumAct := KPI.MonthNum ,
   CONCAT( @KPI , KPI.CatKPI )  AS KPI,
   @KPI_Description  AS descripcion,
   KPI.Value,
   @KPI_UpdatedAt AS KPI_UpdatedAt
FROM
(
   SELECT
      @KPI_Month AS MonthNum,
      development_mx.M_CategoryKPI.CatKPI AS CatKPI,
      COALESCE(TMP.Value, 0 ) as Value,
      KPIorder 
   FROM
      development_mx.M_CategoryKPI
      LEFT JOIN
      (
         SELECT
            MonthNum,
            CatKPI,
            SUM( IF( OrderBeforeCan = 1 , 1 , 0 ) ) AS Value
         FROM 
            A_Master_Year
         WHERE 
            MonthNum = @KPI_Month
         GROUP BY MonthNum, CatKPI

      ) AS TMP 
      ON     TMP.CatKPI = development_mx.M_CategoryKPI.CatKPI
   GROUP BY MonthNum , CONCAT( @KPI , TMP.CatKPI )
   ORDER BY
      MonthNum, development_mx.M_CategoryKPI.KPIorder
) AS KPI
ORDER BY
   MonthNum, KPIorder
) AS Final
;

#Revenue per category (in k local) - 
SET @index=59;
SET @indexIni=59;
SET @monthNumAct=0;
SET @KPI = "Revenue per category (in k local) - ";
SET @KPI_Description = "Gross revenues before cancelations "
                       "(DWH definition: Net Revenue before cancelations excl. VAT)";

REPLACE KPIs
SELECT
   MonthNum,
   SpreadSheet,
   `Group`,
   RowId,
   KPI,
   descripcion,
   Value / 1000,
   KPI_UpdatedAt
FROM
(
SELECT
   KPI.MonthNum,
   @KPI_SpreadSheet AS SpreadSheet,
   @KPI_Group               AS `Group`,
   @index := IF( @monthNumAct != KPI.MonthNum , @indexIni ,
                 @index + 1 ) AS RowId,
   @monthNumAct := KPI.MonthNum ,
   CONCAT( @KPI , KPI.CatKPI )  AS KPI,
   @KPI_Description  AS descripcion,
   KPI.Value,
   @KPI_UpdatedAt AS KPI_UpdatedAt
FROM
(
   SELECT
      @KPI_Month AS MonthNum,
      development_mx.M_CategoryKPI.CatKPI AS CatKPI,
      COALESCE(TMP.Value, 0 ) AS Value,
      KPIorder 
   FROM
      development_mx.M_CategoryKPI
      LEFT JOIN
      (
         SELECT
            MonthNum,
            CatKPI,
            SUM( IF( OrderBeforeCan = 1 , Rev , 0 ) ) AS Value
         FROM 
            A_Master_Year
         WHERE
            MonthNum = @KPI_Month  
         GROUP BY MonthNum, CatKPI

      ) AS TMP 
      ON     TMP.CatKPI = development_mx.M_CategoryKPI.CatKPI
   GROUP BY MonthNum , CONCAT( @KPI , TMP.CatKPI )
   ORDER BY
      MonthNum, development_mx.M_CategoryKPI.KPIorder
) AS KPI
ORDER BY
   MonthNum, KPIorder
) AS Final
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `mom_net_sales_per_channel`()
BEGIN

#Fecha inicio mes actual
select @start_current_month:=cast(date_add(now(), interval - day(now())+1 day) as date);

#Fecha fin mes actual
select @end_current_month:=cast(date_add(now(), interval - 1 day) as date);

#Fecha inicio mes anterior
select @start_previous_month:=date_add(cast(date_add(now(), interval - day(now())+1 day) as date), interval - 1 month);

#Fecha fin mes anterior
select @end_previous_month:=date_add(cast(date_add(now(), interval - 1 day) as date), interval - 1 month);

delete from tbl_mom_net_sales_per_channel;

insert into tbl_mom_net_sales_per_channel(date, channel_group, netSalesPreviousMonth, netSalesCurrentMonth,month_over_month)
select @end_current_month, currentMonth.channel_group, sum(NetSalesPreviousMonth) NetSalesPreviousMonth, sum(NetSalesCurrentMonth) NetSalesCurrentMonth, 
(sum(NetSalesCurrentMonth)/sum(NetSalesPreviousMonth))-1
from
(select channel_group, sum(paid_price_after_vat) +sum(if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat)) as NetSalesCurrentMonth
from tbl_order_detail
where oac = 1 and returned = 0 and date >= @start_current_month and date <= @end_current_month
group by channel_group) currentMonth inner join
(select channel_group, sum(paid_price_after_vat) +sum(if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat)) as NetSalesPreviousMonth
from tbl_order_detail
where oac = 1 and returned = 0 and date >= @start_previous_month and date <= @end_previous_month
group by channel_group) previousMonth on
currentMonth.channel_group = previousMonth.channel_group
group by month(@start_current_month), currentMonth.channel_group;

insert into tbl_mom_net_sales_per_channel(date, channel_group, netSalesPreviousMonth, netSalesCurrentMonth,month_over_month)
select date, 'MTD',sum(NetSalesPreviousMonth), sum(NetSalesCurrentMonth), (sum(NetSalesCurrentMonth)/sum(NetSalesPreviousMonth))-1
from (
select @end_current_month date, sum(NetSalesPreviousMonth) NetSalesPreviousMonth, sum(NetSalesCurrentMonth) NetSalesCurrentMonth, 
(sum(NetSalesCurrentMonth)/sum(NetSalesPreviousMonth))-1
from
(select channel_group, sum(paid_price_after_vat) +sum(if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat)) as NetSalesCurrentMonth
from tbl_order_detail
where oac = 1 and returned = 0 and date >= @start_current_month and date <= @end_current_month
group by channel_group) currentMonth inner join
(select channel_group, sum(paid_price_after_vat) +sum(if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat)) as NetSalesPreviousMonth
from tbl_order_detail
where oac = 1 and returned = 0 and date >= @start_previous_month and date <= @end_previous_month
group by channel_group) previousMonth on
currentMonth.channel_group = previousMonth.channel_group
group by month(@start_current_month), currentMonth.channel_group) p group by date;

update tbl_mom_net_sales_per_channel
set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) where date =  @end_previous_month;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `monthly_marketing`()
BEGIN

#MONTH

DELETE FROM tbl_monthly_marketing WHERE MONTH=MONTH(NOW());

INSERT INTO tbl_monthly_marketing (month,channel) 
SELECT DISTINCT MONTH(NOW()), channel_group FROM tbl_order_detail WHERE MONTH=MONTH(NOW());
#VISITS	

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT MONTH(date) month, reporting_channel, sum(visits) visits
FROM daily_costs_visits_per_channel
GROUP BY MONTH(date), reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.month=tbl_monthly_marketing.month 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.visits=daily_costs_visits_per_channel.visits;

#GROSS ORDERS

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT MONTH(date) month, channel_group, sum(gross_orders) gross_orders
FROM tbl_order_detail WHERE obc = 1
GROUP BY MONTH(date), channel_group) tbl_order_detail
ON tbl_order_detail.month=tbl_monthly_marketing.month 
AND tbl_monthly_marketing.channel=tbl_order_detail.channel_group
SET tbl_monthly_marketing.gross_orders=tbl_order_detail.gross_orders;

#NET ORDERS

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT MONTH(date) month, channel_group, sum(net_orders) net_orders
FROM tbl_order_detail WHERE oac = 1 and returned = 0
GROUP BY MONTH(date), channel_group) tbl_order_detail
ON tbl_order_detail.month=tbl_monthly_marketing.month 
AND tbl_monthly_marketing.channel=tbl_order_detail.channel_group
SET tbl_monthly_marketing.net_orders=tbl_order_detail.net_orders;

#ORDERS NET EXPECTED

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT MONTH(date) month, reporting_channel, sum(net_orders_e) net_orders_e
FROM daily_costs_visits_per_channel
GROUP BY MONTH(date), reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.month=tbl_monthly_marketing.month 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.orders_net_expected=daily_costs_visits_per_channel.net_orders_e;

#CONVERSION RATE
UPDATE tbl_monthly_marketing SET conversion_rate=IF(gross_orders/visits IS NULL, 0, gross_orders/visits);
#PENDING REVENUE

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT MONTH(date) month, channel_group, 
(SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat))/15 pending_sales
FROM tbl_order_detail WHERE tbl_order_detail.pending=1 
GROUP BY MONTH(date), channel_group) tbl_order_detail
ON tbl_order_detail.month=tbl_monthly_marketing.month 
SET tbl_monthly_marketing.pending_revenue=tbl_order_detail.pending_sales
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group;

#GROSS REVENUE
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT MONTH(date) month, channel_group, 
(SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat))/15 gross_sales
FROM tbl_order_detail WHERE tbl_order_detail.obc=1 
GROUP BY MONTH(date), channel_group) tbl_order_detail
ON tbl_order_detail.month=tbl_monthly_marketing.month 
SET tbl_monthly_marketing.gross_revenue=tbl_order_detail.gross_sales
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group;

#NET REVENUE
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT MONTH(date) month, channel_group, 
(SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat))/15 net_sales
FROM tbl_order_detail WHERE tbl_order_detail.oac=1 AND tbl_order_detail.returned=0  
GROUP BY MONTH(date), channel_group) tbl_order_detail
ON tbl_order_detail.month=tbl_monthly_marketing.month 
SET tbl_monthly_marketing.net_revenue=tbl_order_detail.net_sales
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group;

#NET REVENUE EXPECTED

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT MONTH(date) month, 
reporting_channel, sum(net_rev_e)/15 net_rev_e
FROM daily_costs_visits_per_channel
GROUP BY MONTH(date), reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.month=tbl_monthly_marketing.month 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.net_revenue_expected=daily_costs_visits_per_channel.net_rev_e;
UPDATE tbl_monthly_marketing SET net_revenue_expected = net_revenue WHERE net_revenue_expected=0;

#MONTHLY TARGET

UPDATE tbl_monthly_marketing  
INNER JOIN (SELECT MONTH(date) month, channel_group, sum(target_net_sales) monthly_target
FROM daily_targets_per_channel
GROUP BY MONTH(date), channel_group) daily_targets_per_channel
ON daily_targets_per_channel.month=tbl_monthly_marketing.month 
AND tbl_monthly_marketing.channel=daily_targets_per_channel.channel_group
SET tbl_monthly_marketing.monthly_target=daily_targets_per_channel.monthly_target;

#MONTHLY DEVIATION
UPDATE tbl_monthly_marketing 
SET 
deviation=if((net_revenue_expected/(monthly_target/(SELECT DAYOFMONTH(LAST_DAY(NOW())))*(SELECT DAY(NOW())-1))-1) IS NULL,
0,(net_revenue_expected/(monthly_target/(SELECT DAYOFMONTH(LAST_DAY(NOW())))*(SELECT DAY(NOW())-1))-1));

#AVG TICKET
UPDATE tbl_monthly_marketing SET avg_ticket=net_revenue/net_orders;

#MARKETING COST

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT MONTH(date) month, 
reporting_channel, sum(cost_local)/15 cost
FROM daily_costs_visits_per_channel
GROUP BY MONTH(date), reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.month=tbl_monthly_marketing.month 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.marketing_cost=daily_costs_visits_per_channel.cost;

#COST/REV
UPDATE tbl_monthly_marketing SET cost_rev=IF(marketing_cost/net_revenue_expected IS NULL, 0, marketing_cost/net_revenue_expected);

#CPO
UPDATE tbl_monthly_marketing SET cost_per_order=IF(marketing_cost/net_orders IS NULL, 0, marketing_cost/net_orders);

#CPO TARGET

UPDATE tbl_monthly_marketing  
INNER JOIN (SELECT MONTH(date) month, channel_group, MAX(target_cpo) target_cpo
FROM daily_targets_per_channel
GROUP BY MONTH(date), channel_group) daily_targets_per_channel
ON daily_targets_per_channel.month=tbl_monthly_marketing.month 
AND tbl_monthly_marketing.channel=daily_targets_per_channel.channel_group
SET tbl_monthly_marketing.cost_per_order_target=daily_targets_per_channel.target_cpo;

#CPO VS TARGET
UPDATE tbl_monthly_marketing SET cpo_vs_target_cpo=(cost_per_order/cost_per_order_target)-1;

#NEW CLIENTS
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT MONTH(date) month, channel_group, sum(new_customers) nc
FROM tbl_order_detail
GROUP BY MONTH(date), channel_group) tbl_order_detail
ON tbl_order_detail.month=tbl_monthly_marketing.month 
AND tbl_monthly_marketing.channel=tbl_order_detail.channel_group
SET tbl_monthly_marketing.new_clients=tbl_order_detail.nc;

#NEW CLIENTS EXPECTED
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT MONTH(date) month, 
reporting_channel, sum(new_customers_e) nc_e
FROM daily_costs_visits_per_channel
GROUP BY MONTH(date), reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.month=tbl_monthly_marketing.month 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.new_clientes_expected=daily_costs_visits_per_channel.nc_e;
UPDATE tbl_monthly_marketing SET new_clientes_expected=new_clients WHERE new_clientes_expected=0;

#CAC
UPDATE tbl_monthly_marketing SET client_acquisition_cost=marketing_cost/new_clientes_expected;

#PC2_COST

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT MONTH(date) month, channel_group, 
(SUM(costo_after_vat)+SUM(delivery_cost_supplier)+SUM(wh)+SUM(cs)+SUM(payment_cost)+SUM(shipping_cost))/15 pc2_costs
FROM tbl_order_detail WHERE tbl_order_detail.oac=1 AND tbl_order_detail.returned=0  
GROUP BY MONTH(date), channel_group) tbl_order_detail
ON tbl_order_detail.month=tbl_monthly_marketing.month 
SET tbl_monthly_marketing.pc2_costs=tbl_order_detail.pc2_costs
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group;

#PC2
UPDATE tbl_monthly_marketing SET profit_contribution_2=1-(pc2_costs/net_revenue);

#MOM
CALL mom_net_sales_per_channel();

UPDATE tbl_monthly_marketing INNER JOIN tbl_mom_net_sales_per_channel 
ON tbl_monthly_marketing.channel = tbl_mom_net_sales_per_channel.channel_group
AND tbl_monthly_marketing.month = month(tbl_mom_net_sales_per_channel.date)
SET tbl_monthly_marketing.month_over_month = tbl_mom_net_sales_per_channel.month_over_month;

INSERT IGNORE INTO tbl_monthly_marketing (month, channel) 
VALUES(month(date_add(now(), interval -1 day)), 'MTD');

UPDATE tbl_monthly_marketing INNER JOIN tbl_mom_net_sales_per_channel 
ON tbl_monthly_marketing.channel = tbl_mom_net_sales_per_channel.channel_group
AND tbl_monthly_marketing.month = month(tbl_mom_net_sales_per_channel.date)
SET tbl_monthly_marketing.month_over_month = tbl_mom_net_sales_per_channel.month_over_month
where channel_group = 'MTD';


#Adjusted revenue
#Net
update tbl_monthly_marketing m left join 
(
select t1.month, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select month(t.date) month, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by month(t.date) desc,l.about_linio, t.channel_group) t1 inner join
(select month(t.date) month, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by month(t.date) desc,l.about_linio) t2 on t1.month = t2.month and t1.about_linio = t2.about_linio
inner join
(select month(date) month, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 
group by month(date) desc, t.about_linio) t3 on t3.month = t2.month and t3.about_linio = t2.about_linio
group by t1.month, t1.channel_group) t 
on m.month = t.month and m.channel = t.channel_group set m.adjusted_net_revenue = ifnull(m.net_revenue + t.adjusted_net_revenue/15, m.net_revenue)
where m.channel <> 'Tele Sales' ;

update tbl_monthly_marketing m left join 
(
select t1.month, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select month(t.date) month, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by month(t.date) desc,l.about_linio, t.channel_group) t1 inner join
(select month(t.date) month, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by month(t.date) desc,l.about_linio) t2 on t1.month = t2.month and t1.about_linio = t2.about_linio
inner join
(select month(date) month, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 
group by month(date) desc, t.about_linio) t3 on t3.month = t2.month and t3.about_linio = t2.about_linio
group by t1.month, t1.channel_group) t 
on m.month = t.month and m.channel = t.channel_group set m.adjusted_net_revenue = ifnull( t.adjusted_net_revenue/15,0)
where m.channel = 'Tele Sales' ;

#Gross
update tbl_monthly_marketing m left join 
(
select t1.month, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select month(t.date) month, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by month(t.date) desc,l.about_linio, t.channel_group) t1 inner join
(select month(t.date) month, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by month(t.date) desc,l.about_linio) t2 on t1.month = t2.month and t1.about_linio = t2.about_linio
inner join
(select month(date) month, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and obc = 1 
group by month(date) desc, t.about_linio) t3 on t3.month = t2.month and t3.about_linio = t2.about_linio
group by t1.month, t1.channel_group) t 
on m.month = t.month and m.channel = t.channel_group set m.adjusted_gross_revenue = ifnull(m.gross_revenue + t.adjusted_net_revenue/15, m.gross_revenue)
where m.channel <> 'Tele Sales' ;

update tbl_monthly_marketing m left join 
(
select t1.month, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select month(t.date) month, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by month(t.date) desc,l.about_linio, t.channel_group) t1 inner join
(select month(t.date) month, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by month(t.date) desc,l.about_linio) t2 on t1.month = t2.month and t1.about_linio = t2.about_linio
inner join
(select month(date) month, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and obc = 1 
group by month(date) desc, t.about_linio) t3 on t3.month = t2.month and t3.about_linio = t2.about_linio
group by t1.month, t1.channel_group) t 
on m.month = t.month and m.channel = t.channel_group set m.adjusted_gross_revenue = ifnull( t.adjusted_net_revenue/15,0)
where m.channel = 'Tele Sales' ;

#Pending
update tbl_monthly_marketing m left join 
(
select t1.month, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select month(t.date) month, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by month(t.date) desc,l.about_linio, t.channel_group) t1 inner join
(select month(t.date) month, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by month(t.date) desc,l.about_linio) t2 on t1.month = t2.month and t1.about_linio = t2.about_linio
inner join
(select month(date) month, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and pending = 1 
group by month(date) desc, t.about_linio) t3 on t3.month = t2.month and t3.about_linio = t2.about_linio
group by t1.month, t1.channel_group) t 
on m.month = t.month and m.channel = t.channel_group set m.adjusted_pending_revenue = ifnull(m.pending_revenue + t.adjusted_net_revenue/15, m.pending_revenue)
where m.channel <> 'Tele Sales' ;

update tbl_monthly_marketing m left join 
(
select t1.month, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select month(t.date) month, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by month(t.date) desc,l.about_linio, t.channel_group) t1 inner join
(select month(t.date) month, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by month(t.date) desc,l.about_linio) t2 on t1.month = t2.month and t1.about_linio = t2.about_linio
inner join
(select month(date) month, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and pending = 1 
group by month(date) desc, t.about_linio) t3 on t3.month = t2.month and t3.about_linio = t2.about_linio
group by t1.month, t1.channel_group) t 
on m.month = t.month and m.channel = t.channel_group set m.adjusted_pending_revenue = ifnull( t.adjusted_net_revenue/15,0)
where m.channel = 'Tele Sales' ;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `monthly_marketing_2`()
BEGIN

#yrmonth

select @yrmonth := concat(year(curdate()-1),if(month(curdate()-1)<10,concat(0,month(curdate()-1)),month(curdate()-1)));

DELETE FROM tbl_monthly_marketing WHERE yrmonth=@yrmonth;

INSERT IGNORE INTO tbl_monthly_marketing (month,channel) 
SELECT DISTINCT month(now()), channel_group FROM tbl_order_detail WHERE channel_group is not null and yrmonth=@yrmonth;

update tbl_monthly_marketing
set yrmonth = concat(year(curdate()-1),if(month(curdate()-1)<10,concat(0,month(curdate()-1)),month(curdate()-1)))
where month = month (curdate()-1);

#VISITS	

UPDATE tbl_monthly_marketing  left JOIN (SELECT yrmonth yrmonth, reporting_channel, sum(visits) visits
FROM daily_costs_visits_per_channel where daily_costs_visits_per_channel.date < cast(curdate() as date)
GROUP BY yrmonth, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.visits=daily_costs_visits_per_channel.visits 
WHERE tbl_monthly_marketing.yrmonth = @yrmonth
;

#GROSS ORDERS

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, sum(gross_orders) gross_orders
FROM tbl_order_detail WHERE OBC = 1 and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=tbl_order_detail.channel_group
SET tbl_monthly_marketing.gross_orders=tbl_order_detail.gross_orders
WHERE tbl_monthly_marketing.yrmonth = @yrmonth
;
#NET ORDERS

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, sum(net_orders) net_orders
FROM tbl_order_detail WHERE oac = 1 and returned = 0 and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=tbl_order_detail.channel_group
SET tbl_monthly_marketing.net_orders=tbl_order_detail.net_orders
WHERE tbl_monthly_marketing.yrmonth = @yrmonth
;

#ORDERS NET EXPECTED

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, reporting_channel, sum(net_orders_e) net_orders_e
FROM daily_costs_visits_per_channel where daily_costs_visits_per_channel.date < cast(curdate() as date)
GROUP BY yrmonth, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.orders_net_expected=daily_costs_visits_per_channel.net_orders_e
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#CONVERSION RATE
UPDATE tbl_monthly_marketing SET conversion_rate=IF(gross_orders/visits IS NULL, 0, gross_orders/visits);
#PENDING REVENUE

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, 
(SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat))/30.92 pending_sales
FROM tbl_order_detail WHERE tbl_order_detail.pending=1  and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
SET tbl_monthly_marketing.pending_revenue=tbl_order_detail.pending_sales
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group and tbl_monthly_marketing.yrmonth = @yrmonth;

#GROSS REVENUE
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, 
(SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat))/30.92 gross_sales
FROM tbl_order_detail WHERE tbl_order_detail.obc=1  and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
SET tbl_monthly_marketing.gross_revenue=tbl_order_detail.gross_sales
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group and tbl_monthly_marketing.yrmonth = @yrmonth;

#NET REVENUE
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth, channel_group, 
(SUM(paid_price_after_vat)+SUM(shipping_fee_after_vat))/30.92 net_sales
FROM tbl_order_detail WHERE tbl_order_detail.oac=1 AND tbl_order_detail.returned=0  and tbl_order_detail.date < cast(curdate() as date) 
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
SET tbl_monthly_marketing.net_revenue=tbl_order_detail.net_sales
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group and tbl_monthly_marketing.yrmonth = @yrmonth
;

#NET REVENUE EXPECTED

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, 
reporting_channel, sum(net_rev_e)/30.92 net_rev_e
FROM daily_costs_visits_per_channel where daily_costs_visits_per_channel.date < cast(curdate() as date)
GROUP BY yrmonth, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.net_revenue_expected=daily_costs_visits_per_channel.net_rev_e and tbl_monthly_marketing.yrmonth = @yrmonth
;

UPDATE tbl_monthly_marketing SET net_revenue_expected = net_revenue WHERE net_revenue_expected=0;
UPDATE tbl_monthly_marketing SET net_revenue_expected = net_revenue WHERE net_revenue_expected<net_revenue;

#MONTHLY TARGET

UPDATE tbl_monthly_marketing  
INNER JOIN (SELECT yrmonth yrmonth, channel_group, sum(target_net_sales) monthly_target
FROM daily_targets_per_channel
GROUP BY yrmonth, channel_group) daily_targets_per_channel
ON daily_targets_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_targets_per_channel.channel_group
SET tbl_monthly_marketing.monthly_target=daily_targets_per_channel.monthly_target and tbl_monthly_marketing.yrmonth = @yrmonth;

#MONTHLY DEVIATION
UPDATE tbl_monthly_marketing 
SET 
deviation=if((net_revenue_expected/(monthly_target/(SELECT DAYOFMONTH(LAST_DAY(NOW())))*(SELECT DAY(NOW())-1))-1) IS NULL,
0,(net_revenue_expected/(monthly_target/(SELECT DAYOFMONTH(LAST_DAY(NOW())))*(SELECT DAY(NOW())-1))-1))
where tbl_monthly_marketing.yrmonth = @yrmonth;

#AVG TICKET
UPDATE tbl_monthly_marketing SET avg_ticket=net_revenue/net_orders;

#MARKETING COST

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, 
reporting_channel, sum(cost_local)/30.92 cost
FROM daily_costs_visits_per_channel where daily_costs_visits_per_channel.date < cast(curdate() as date)
GROUP BY yrmonth, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.marketing_cost=daily_costs_visits_per_channel.cost
WHERE tbl_monthly_marketing.yrmonth = @yrmonth
;

#COST/REV
UPDATE tbl_monthly_marketing SET cost_rev=IF(marketing_cost/net_revenue_expected IS NULL, 0, marketing_cost/net_revenue_expected)
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#CPO
UPDATE tbl_monthly_marketing SET cost_per_order=IF(marketing_cost/net_orders IS NULL, 0, marketing_cost/net_orders)
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#CPO TARGET

UPDATE tbl_monthly_marketing  
INNER JOIN (SELECT yrmonth yrmonth, channel_group, MAX(target_cpo) target_cpo
FROM daily_targets_per_channel
GROUP BY yrmonth, channel_group) daily_targets_per_channel
ON daily_targets_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_targets_per_channel.channel_group
SET tbl_monthly_marketing.cost_per_order_target=daily_targets_per_channel.target_cpo
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#CPO VS TARGET
UPDATE tbl_monthly_marketing SET cpo_vs_target_cpo=(cost_per_order/cost_per_order_target)-1 WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#NEW CLIENTS
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, sum(new_customers) nc
FROM tbl_order_detail where oac = 1 and returned = 0 and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=tbl_order_detail.channel_group
SET tbl_monthly_marketing.new_clients=tbl_order_detail.nc WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#NEW CLIENTS EXPECTED
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, 
reporting_channel, sum(new_customers_e) nc_e
FROM daily_costs_visits_per_channel 
where daily_costs_visits_per_channel.date < cast(curdate() as date)
GROUP BY yrmonth, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.new_clientes_expected=daily_costs_visits_per_channel.nc_e WHERE tbl_monthly_marketing.yrmonth = @yrmonth;



UPDATE tbl_monthly_marketing SET new_clientes_expected=new_clients WHERE new_clientes_expected=0 AND tbl_monthly_marketing.yrmonth = @yrmonth;

#CAC
UPDATE tbl_monthly_marketing SET client_acquisition_cost=marketing_cost/new_clientes_expected WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#PC2_COST

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, 
(SUM(costo_after_vat)+SUM(delivery_cost_supplier)+SUM(wh)+SUM(cs)+SUM(payment_cost)+SUM(shipping_cost))/30.92 pc2_costs
FROM tbl_order_detail WHERE tbl_order_detail.oac=1 AND tbl_order_detail.returned=0  and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
SET tbl_monthly_marketing.pc2_costs=tbl_order_detail.pc2_costs
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group AND tbl_monthly_marketing.yrmonth = @yrmonth;

#PC2
UPDATE tbl_monthly_marketing SET profit_contribution_2=1-(pc2_costs/net_revenue) WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#MOM

UPDATE tbl_monthly_marketing INNER JOIN tbl_mom_net_sales_per_channel 
ON tbl_monthly_marketing.channel = tbl_mom_net_sales_per_channel.channel_group
AND tbl_monthly_marketing.yrmonth = tbl_mom_net_sales_per_channel.yrmonth
SET tbl_monthly_marketing.month_over_month = tbl_mom_net_sales_per_channel.month_over_month WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

INSERT IGNORE INTO tbl_monthly_marketing (month, channel) 
VALUES(month(date_add(now(), interval -1 day)), 'MTD');

update tbl_monthly_marketing
set yrmonth = concat(year(curdate()-1),if(month(curdate()-1)<10,concat(0,month(curdate()-1)),month(curdate()-1)))
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

UPDATE tbl_monthly_marketing INNER JOIN tbl_mom_net_sales_per_channel 
ON tbl_monthly_marketing.channel = tbl_mom_net_sales_per_channel.channel_group
AND tbl_monthly_marketing.yrmonth = tbl_mom_net_sales_per_channel.yrmonth
SET tbl_monthly_marketing.month_over_month = tbl_mom_net_sales_per_channel.month_over_month
where channel_group = 'MTD' AND tbl_monthly_marketing.yrmonth = @yrmonth;

#Adjusted revenue
#Net
update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion,
 sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_net_revenue = ifnull(m.net_revenue + t.adjusted_net_revenue/30.92, m.net_revenue)
where m.channel <> 'Tele Sales' AND m.yrmonth = @yrmonth;

update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_net_revenue = ifnull( t.adjusted_net_revenue/30.92,0)
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth ;

#Gross
update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and obc = 1 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_gross_revenue = ifnull(m.gross_revenue + t.adjusted_net_revenue/30.92, m.gross_revenue)
where m.channel <> 'Tele Sales'  AND m.yrmonth = @yrmonth;

update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and obc = 1 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_gross_revenue = ifnull( t.adjusted_net_revenue/30.92,0)
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth;

#Pending
update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and pending = 1 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_pending_revenue = ifnull(m.pending_revenue + t.adjusted_net_revenue/30.92, m.pending_revenue)
where m.channel <> 'Tele Sales' AND m.yrmonth = @yrmonth;

update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and pending = 1 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_pending_revenue = ifnull( t.adjusted_net_revenue/30.92,0)
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth;

#Net ORders
update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, 
sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  count(distinct orderID) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, count(distinct orderID) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, count(distinct orderID) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_net_orders = ifnull(m.net_orders + t.adjusted_net_revenue, m.net_orders)
where m.channel <> 'Tele Sales' AND m.yrmonth = @yrmonth;

update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, 
sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  count(distinct orderID) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, count(distinct orderID) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, count(distinct orderID) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_net_orders = t.adjusted_net_revenue
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth;

#New clients
update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, 
sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(new_customers) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio,  sum(new_customers) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(new_customers) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_new_clients = ifnull(m.new_clients + t.adjusted_net_revenue, m.new_clients)
where m.channel <> 'Tele Sales' AND m.yrmonth = @yrmonth;

update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, 
sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group, sum(new_customers) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio,  sum(new_customers) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(new_customers) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_new_clients = t.adjusted_net_revenue
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `top_20`()
begin

delete from production_ve.top_20_weekly;

insert into production_ve.top_20_weekly(Category_English, Category, Sku, Product_name, Freq, AVGPrice, PC2)
(select *
from (select 'Appliances', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_ve.tbl_order_detail t
where production_ve.week_exit(date) = production_ve.week_exit(curdate())-1 and oac = 1 and n1 = 'electrodomésticos'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Photography', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_ve.tbl_order_detail t
where production_ve.week_exit(date) = production_ve.week_exit(curdate())-1 and oac = 1 and n1 = 'cámaras y fotografía'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Audio Video', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_ve.tbl_order_detail t
where production_ve.week_exit(date) = production_ve.week_exit(curdate())-1 and oac = 1 and n1 = 'tv, audio y video'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Books', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_ve.tbl_order_detail t
where production_ve.week_exit(date) = production_ve.week_exit(curdate())-1 and oac = 1 and n1 like 'libros%'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Video Games', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_ve.tbl_order_detail t
where production_ve.week_exit(date) = production_ve.week_exit(curdate())-1 and oac = 1 and n1 = 'videojuegos'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Fashion', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_ve.tbl_order_detail t
where production_ve.week_exit(date) = production_ve.week_exit(curdate())-1 and oac = 1 and (n1 = 'accesorios de moda' or n1 = 'ropa, calzado y accesorios')
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Health and Beauty', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_ve.tbl_order_detail t
where production_ve.week_exit(date) = production_ve.week_exit(curdate())-1 and oac = 1 and (n1 like '%cuidado personal%' or n1 = 'secadores')
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Cellphones', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_ve.tbl_order_detail t
where production_ve.week_exit(date) = production_ve.week_exit(curdate())-1 and oac = 1 and (n1 = 'celulares, teléfonos y gps' or n1 = 'ipad 100')
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Computing', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_ve.tbl_order_detail t
where production_ve.week_exit(date) = production_ve.week_exit(curdate())-1 and oac = 1 and n1 = 'computadoras y tabletas'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Home', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_ve.tbl_order_detail t
where production_ve.week_exit(date) = production_ve.week_exit(curdate())-1 and oac = 1 and (n1 = 'línea blanca' or n1 = 'comerdor y cocina' or n1 = 'hogar')
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Kids and Babies', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_ve.tbl_order_detail t
where production_ve.week_exit(date) = production_ve.week_exit(curdate())-1 and oac = 1 and n1 like 'juguetes%'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Outdoor', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production_ve.tbl_order_detail t
where production_ve.week_exit(date) = production_ve.week_exit(curdate())-1 and oac = 1 and n1 ='camping y exteriores'
group by t.sku) n
order by Freq desc limit 20);

update  production_ve.top_20_weekly set AVGPrice = AVGPrice/15, PC2 = (PC2/15)/Freq;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `update_tbl_order_detail`()
begin

declare itemid integer;

select @last_date:=max(date)       from development_ve.tbl_order_detail;
select @last_date_fa:=max(date)    from development_ve.ga_cost_campaign where source = 'facebook';
select @last_date_socio:=max(date) from development_ve.ga_cost_campaign where source = 'Sociomantic';

select  'update tbl_order_detail: start',now();

#update status
update      development_ve.tbl_order_detail 
       join bob_live_ve.sales_order_item 
            on id_sales_order_item=item 
       join bob_live_ve.sales_order_item_status 
            on fk_sales_order_item_status=id_sales_order_item_status
set 
   status_item= sales_order_item_status.name;

#update obc, oac, pending, cancel, returned
 
update      development_ve.tbl_order_detail 
       join development_ve.status_bob 
            on     tbl_order_detail.payment_method=status_bob.payment_method 
               and tbl_order_detail.status_item=status_bob.status_bob
set 
   tbl_order_detail.obc      = status_bob.obc, 
   tbl_order_detail.oac      = status_bob.oac,
   tbl_order_detail.pending  = status_bob.pending,
   tbl_order_detail.cancel   = status_bob.cancel,
   tbl_order_detail.returned = status_bob.returned;

#update categories
update development_ve.tbl_order_detail join development_ve.tbl_catalog_product_v2 
on tbl_order_detail.sku=tbl_catalog_product_v2.sku 
set n1=cat1,n2=cat2,n3=cat3;

#insert facebookads and sociomantic

insert into development_ve.ga_cost_campaign(date, source, medium, impressions, adclicks, adcost) 
select date(date_value), 'facebook', 
'socialmedia', sum(if(impressions is null, 0, impressions)) impressions, sum(if(clicks is null, 0, clicks)) clicks, sum(if(cost_mxn is null, 0.,cost_mxn)) cost 
from development_ve.mkt_facebook_ad_cost where date(date_value) > @last_date_fa
and category = 'Social Media' group by date_value, channel;

insert into development_ve.ga_cost_campaign(date, source, medium, impressions, adclicks, adcost) 
select date(date_value), 'facebook', 
'socialmediaads', sum(if(impressions is null, 0, impressions)) impressions, sum(if(clicks is null, 0, clicks)) clicks, sum(if(cost_mxn is null, 0.,cost_mxn)) cost 
from development_ve.mkt_facebook_ad_cost where date(date_value) > @last_date_fa
and category <> 'Social Media' group by date_value, channel;

insert into development_ve.ga_cost_campaign(date, source, medium, campaign,impressions, adclicks, adcost) 
select date(date_value), 'Sociomantic', 
'Retargeting', '(not set)', sum(if(impressions is null, 0, impressions)) impressions,
 sum(if(clicks is null, 0, clicks)) clicks, sum(if(cost_mxn is null, 0.,cost_mxn)) cost 
from development_ve.mkt_channel_sociomantic where date(date_value) > @last_date_socio
group by date(date_value);

#insert new data
set itemid = (select item from development_ve.tbl_order_detail order by item desc limit 1);

insert into development_ve.tbl_order_detail (custid,orderid,order_nr,payment_method,unit_price,paid_price,
coupon_money_value,coupon_code,date,hour,sku,item,
status_item,obc,pending,cancel,oac,returned,n1,n2,n3,
tax_percent,unit_price_after_vat,
paid_price_after_vat,coupon_money_after_vat,cost_pet,costo_oferta,costo_after_vat,delivery_cost_supplier,
buyer,brand,product_name,peso,proveedor,ciudad,region,ordershippingfee, about_linio)
(select sales_order.fk_customer as custid,sales_order.id_sales_order as orderid,sales_order.order_nr as order_nr,sales_order.payment_method as payment_method,
sales_order_item.unit_price as unit_price,sales_order_item.paid_price as paid_price,sales_order_item.coupon_money_value as coupon_money_value,sales_order.coupon_code as coupon_code,
cast(sales_order.created_at as date) as date,concat(hour(sales_order.created_at),':',minute(sales_order.created_at),':',second(sales_order.created_at)) as hour,sales_order_item.sku as sku,
sales_order_item.id_sales_order_item as item,
sales_order_item_status.name as status_item,
(select status_bob.obc from development_ve.status_bob 
where ((status_bob.payment_method = sales_order.payment_method) and (sales_order_item_status.name = status_bob.status_bob)) limit 1) as obc,
(select status_bob.pending from development_ve.status_bob where ((status_bob.payment_method = sales_order.payment_method) and 
(sales_order_item_status.name = status_bob.status_bob)) limit 1) as pending,
(select status_bob.cancel from development_ve.status_bob where ((status_bob.payment_method = sales_order.payment_method) and (sales_order_item_status.name = status_bob.status_bob)) limit 1) as cancel,
(select status_bob.oac from development_ve.status_bob where ((status_bob.payment_method = sales_order.payment_method) and (sales_order_item_status.name = status_bob.status_bob)) limit 1) as oac,
(select status_bob.returned from development_ve.status_bob where ((status_bob.payment_method = sales_order.payment_method)
and (sales_order_item_status.name = status_bob.status_bob)) limit 1) as returned,
tbl_catalog_product_v2.cat1 as n1,
tbl_catalog_product_v2.cat2 as n2,
tbl_catalog_product_v2.cat3 as n3,
tbl_catalog_product_v2.tax_percent as tax_percent,
(sales_order_item.unit_price / (1 + (tbl_catalog_product_v2.tax_percent / 100))) as unit_price_after_vat,
(sales_order_item.paid_price / (1 + (tbl_catalog_product_v2.tax_percent / 100))) as paid_price_after_vat,
(sales_order_item.coupon_money_value / (1 + (tbl_catalog_product_v2.tax_percent / 100))) as coupon_money_after_vat,
tbl_catalog_product_v2.cost as cost_pet,
if(tbl_catalog_product_v2.cost<sales_order_item.cost,sales_order_item.cost,0) as costo_oferta,
sales_order_item.cost/1.12 as costo_after_vat,
if(isnull(sales_order_item.delivery_cost_supplier),tbl_catalog_product_v2.inbound,
sales_order_item.delivery_cost_supplier) as delivery_cost_supplier,
tbl_catalog_product_v2.buyer as buyer,
tbl_catalog_product_v2.brand as brand,
tbl_catalog_product_v2.product_name as product_name,
tbl_catalog_product_v2.product_weight as peso,
tbl_catalog_product_v2.supplier as proveedor,
if(isnull(sales_order_address.municipality),sales_order_address.city,sales_order_address.municipality) as ciudad,
sales_order_address.region as region,
sales_order.shipping_amount as ordershippingfee,
sales_order.about_linio as about_linio
from ((((bob_live_ve.sales_order_item join bob_live_ve.sales_order_item_status) join bob_live_ve.sales_order) join development_ve.tbl_catalog_product_v2) join bob_live_ve.sales_order_address) 
where
((sales_order_item.fk_sales_order_item_status = sales_order_item_status.id_sales_order_item_status) 
and (sales_order_item.id_sales_order_item > itemid) and (sales_order.id_sales_order = sales_order_item.fk_sales_order) 
and (sales_order.fk_sales_order_address_shipping = sales_order_address.id_sales_order_address)
and (sales_order_item.sku = tbl_catalog_product_v2.sku)
));

update development_ve.tbl_order_detail set costo_after_vat = cost_pet/1.12
where costo_after_vat is null;

update development_ve.tbl_order_detail
set year = year(date) , month = month(date) , yrmonth = concat(year,if(month<10,concat(0,month),month))
where date >=@last_date;

#update source_medium
update development_ve.tbl_order_detail inner join development_ve.ga_order on tbl_order_detail.order_nr=ga_order.order_nr set source_medium= concat(ga_order.source,' / ',ga_order.medium),tbl_order_detail.campaign=ga_order.campaign where tbl_order_detail.item>=itemid;

update development_ve.tbl_order_detail
set year = year(date) , month = month(date) , yrmonth = concat(year,if(month<10,concat(0,month),month))
where date >=@last_date;

-- call extra_queries_2012();
-- call extra_queries_2013();

select  'start channel_report: start',now();

call development_ve.channel_report();

select  'start channel_report: end',now();

select  'start channel_report_no_voucher: start',now();

call development_ve.channel_report_no_voucher();

select  'start channel_report_no_voucher: end',now();

#Cupones de devolución
UPDATE tbl_order_detail t SET paid_price = unit_price, paid_price_after_vat = unit_price/1.12
WHERE coupon_code like 'CCE%' 
or coupon_code like 'PRCcre%' 
or coupon_code like 'OPScre%'
or coupon_Code like 'DEP%';

#pc2 
select 'start pc2',now();
truncate development_ve.tbl_group_order_detail;
insert into development_ve.tbl_group_order_detail (select orderid, count(order_nr) as items,
sum(greatest(cast(replace(if(isnull(peso),1,peso),',','.') as decimal(21,6)),1.0))as pesototal,
sum(paid_price) as grandtotal
from development_ve.tbl_order_detail where  oac='1' and returned='0' group by order_nr);	

truncate development_ve.tbl_group_order_detail_gross;
insert into development_ve.tbl_group_order_detail_gross (select orderid, count(order_nr) as items,
sum(greatest(cast(replace(if(isnull(peso),1,peso),',','.') as decimal(21,6)),1.0))as pesototal,
sum(paid_price) as grandtotal
from development_ve.tbl_order_detail where  obc='1' group by order_nr);	


update   development_ve.tbl_order_detail as t 
left join development_ve.tbl_group_order_detail as e 
on e.orderid = t.orderid 
set t.orderpeso = e.pesototal , t.nr_items=e.items , t.ordertotal=e.grandtotal
where t.oac='1' and t.returned='0';

update   development_ve.tbl_order_detail as t 
left join development_ve.tbl_group_order_detail_gross as e 
on e.orderid = t.orderid 
set t.orderpeso_gross = e.pesototal , t.gross_items=e.items , t.ordertotal_gross=e.grandtotal
where t.obc='1';

update development_ve.tbl_order_detail as t
set gross_orders = if(gross_items is null,0,1/gross_items) where obc = '1';

update development_ve.tbl_order_detail as t
set net_orders = if(nr_items is null, 0,1/nr_items) where oac = '1 'and returned = '0';

#pc2: shipping_fee
update development_ve.tbl_order_detail set ordershippingfee=0.0 where ordershippingfee is null;
update development_ve.tbl_order_detail set shipping_fee=ordershippingfee/cast(nr_items as decimal) where oac='1' and returned='0';
update tbl_order_detail set shipping_fee_2=ordershippingfee/cast(nr_items as decimal);

update development_ve.tbl_order_detail set shipping_fee_after_vat=if(shipping_fee is null, 0, shipping_fee/1.12);
update tbl_order_detail set shipping_fee_after_vat_2=if(shipping_fee_2 is null, 0, shipping_fee_2/1.12);
update tbl_order_detail set grand_total_after_vat=shipping_fee_after_vat_2+paid_price_after_vat;
update tbl_order_detail set gross_grand_total_after_vat=shipping_fee_after_vat_2+paid_price_after_vat where obc = '1';
update tbl_order_detail set net_grand_total_after_vat=shipping_fee_after_vat+paid_price_after_vat 
where (oac = 1 and returned = 0);

#pc2: cs and wh
update development_ve.tbl_order_detail set cs=13.52/cast(nr_items as decimal),wh=16.64/cast(nr_items as decimal) where date>='2012-08-01' and oac='1' and returned='0';
update development_ve.tbl_order_detail set cs=37.5/cast(nr_items as decimal),wh=40.5/cast(nr_items as decimal) where date>='2013-03-01' and oac='1' and returned='0';

update development_ve.tbl_order_detail set shipping_cost=(select  bogota from bob_live_ve.shipping_cost where peso=least(ceil(orderpeso),200))/cast(nr_items as decimal) where ciudad like '%bogota%' and oac='1' and returned='0' and date<='2012-12-11';
update development_ve.tbl_order_detail set shipping_cost=(select  resto_pais from bob_live_ve.shipping_cost where peso=least(ceil(orderpeso),200))/cast(nr_items as decimal) where ciudad not like '%bogota%' and oac='1' and returned='0'and date<='2012-12-11';
update development_ve.tbl_order_detail set shipping_cost= ordershippingfee/cast(nr_items as decimal) where oac='1' and returned='0' and date>'2012-12-11'; 

#shipping_cost
update development_ve.tbl_order_detail set shipping_cost = 0.04 * (paid_price_after_vat+if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat));

#pc2: payment_cost

update development_ve.tbl_order_detail set payment_cost=(ordertotal+ordershippingfee)*0.03/cast(nr_items as decimal) where payment_method ='Novared_Creditcard_Redirect' and oac='1' and returned='0';
update development_ve.tbl_order_detail set payment_cost=(ordertotal+ordershippingfee)*0.0371/cast(nr_items as decimal) where payment_method ='Sitef_Creditcard' and oac='1' and returned='0';

#wh shipping_cost para drop shipping
update development_ve.tbl_order_detail set wh =0.0, shipping_cost=0.0 where sku in (select sku from bob_live_ve.catalog_simple where fk_catalog_shipment_type=2);

#proveedor entrega:
-- update  development_ve.tbl_order_detail set delivery_cost_supplier=0.0 where proveedor='izc mayorista s.a.s';

#null fiedls
update development_ve.tbl_order_detail set shipping_fee=0.0 where shipping_fee is null;
update development_ve.tbl_order_detail set delivery_cost_supplier=0.0 where delivery_cost_supplier is null;
update development_ve.tbl_order_detail set wh=0.0 where wh is null;
update development_ve.tbl_order_detail set cs=0.0 where cs is null;
update development_ve.tbl_order_detail set payment_cost=0.0 where payment_cost is null;
update development_ve.tbl_order_detail set shipping_cost=0.0 where shipping_cost is null;

#campaign
-- update development_ve.tbl_order_detail set campaign = 'lizamoran' where campaign = 'liza.moran';

#shipping_cost
-- call shipping_cost_order_city();

update development_ve.tbl_order_detail join(select date, t2.orderid, t2.sku_config, city, shipment_zone, t1.shipping_cost as shipping_cost_menor, t2.shipping_cost, 
if(t1.shipping_cost > 0 and t1.shipping_cost < t2.shipping_cost, '1','0') as r 
from 
development_ve.tbl_shipment_cost_calculado t1
right join development_ve.tbl_shipping_cost_order_city t2
on t1.sku_config = t2.sku_config and fk_shipment_zone = shipment_zone
where month(date) >= 2 and year(date) >= 2013
group by  date, t2.orderid, t2.sku_config, city, shipment_zone) tabla
on tbl_order_detail.orderid = tabla.orderid
set tbl_order_detail.shipping_cost = tabla.shipping_cost_menor where r = '1'
and month(tbl_order_detail.date) >= 2 and year(tbl_order_detail.date) >= 2013; 

#daily report

call daily_report();


#tbl_monthly_cohort_sin_groupon
truncate development_ve.tbl_monthly_cohort_sin_groupon;
insert into tbl_monthly_cohort_sin_groupon(custid, netsales, orders, firstorder, lastorder, idfirstorder, idlastorder, frecuencia,
coupon_code,  channel, channel_group)
select t1.*, t2.coupon_code, t2.channel,t2.channel_group from
(select 
        tbl_order_detail.custid as custid,
        sum(tbl_order_detail.paid_price_after_vat) as netsales,
        count(distinct tbl_order_detail.order_nr) as orders,
        min(tbl_order_detail.date) as firstorder,
        max(tbl_order_detail.date) as lastorder,
        min(tbl_order_detail.orderid) as idfirstorder,
        max(tbl_order_detail.orderid) as idlastorder,
        count(distinct tbl_order_detail.date) as frecuencia
    from
        development_ve.tbl_order_detail
    where
        ((tbl_order_detail.oac = '1')
            and (tbl_order_detail.returned = '0'))
    group by tbl_order_detail.custid
    order by sum(tbl_order_detail.paid_price_after_vat) desc) as t1
inner join (select  orderid,coupon_code,channel,channel_group from development_ve.tbl_order_detail where oac = '1' and returned = '0'and coupon_code not like 'gr%')
as t2 on t1.idfirstorder = t2.orderid
group by t1.custid, t2.coupon_code
order by sum(t1.netsales) desc;

update development_ve.tbl_order_detail inner join  
(select firstOrder as date,custID as newCustomer, idFirstOrder from development_ve.view_cohort ) as tbl_nc on tbl_nc.date=tbl_order_detail.date and tbl_nc.newCustomer = tbl_order_detail.custID and tbl_nc.idFirstOrder = orderID
set tbl_order_detail.new_customers=1/nr_items;


#tbl_monthly_cohort
truncate development_ve.tbl_monthly_cohort;
insert into tbl_monthly_cohort(custid,cohort,idfirstorder,idlastorder)
select custid,
cast(concat(year(firstorder),if(length(month(firstorder)) < 2,concat('0', month(firstorder)),month(firstorder)))as signed) as cohort,
idfirstorder,idlastorder from development_ve.view_cohort;

update development_ve.tbl_monthly_cohort inner join development_ve.tbl_order_detail on idFirstOrder = orderID and tbl_monthly_cohort.CustID = tbl_order_detail.CustID
set tbl_monthly_cohort.coupon_code = tbl_order_detail.coupon_code;

update development_ve.tbl_monthly_cohort set coupon_code = '' where coupon_code is null;

select  'update tbl_order_detail: end',now();

#New_customers
update development_ve.tbl_order_detail inner join  
(select firstOrder as date,custID as newCustomer, idFirstOrder from development_ve.view_cohort_gross ) as tbl_nc on tbl_nc.date=tbl_order_detail.date and tbl_nc.newCustomer = tbl_order_detail.custID and tbl_nc.idFirstOrder = orderID
set tbl_order_detail.new_customers_gross=1/gross_items;

update development_ve.tbl_order_detail set PC1 = ifnull(unit_price_after_vat,0)-ifnull(coupon_money_value,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0);

update development_ve.tbl_order_detail set PC2 = PC1-ifnull(payment_cost,0)-ifnull(shipping_cost,0)-ifnull(wh,0)-ifnull(cs,0);

#channel clv
truncate development_ve.tbl_monthly_cohort_channel;
insert into development_ve.tbl_monthly_cohort_channel (fk_customer,cohort,idfirstorder,channel_group)
select tbl_monthly_cohort.custid as fk_customer,cohort,idfirstorder,tbl_order_detail.channel_group as channel
from development_ve.tbl_monthly_cohort left join development_ve.tbl_order_detail on orderid=idfirstorder
group by tbl_monthly_cohort.custid;


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `visits_costs_channel`()
begin

delete from production_ve.vw_visits_costs_channel;
delete from production_ve.daily_costs_visits_per_channel;

insert into production_ve.vw_visits_costs_channel (date, channel_group, sum_visits, sum_adcost) 
select v.*, if(c.adCost is null, 0, c.adCost) adCost from (
select date, channel_group, sum(if(adCost is null, 0, adCost)) adCost from production_ve.ga_cost_campaign
group by date , channel_group) c right join (
select date, channel_group, sum(visits) visits from production_ve.ga_visits_cost_source_medium
group by date , channel_group) v on c.channel_group = v.channel_group and c.date = v.date
order by c.date desc;

insert into production_ve.daily_costs_visits_per_channel (date, cost_local, reporting_channel, visits) select date, sum_adcost, channel_group, sum_visits from production_ve.vw_visits_costs_channel;

update production_ve.daily_costs_visits_per_channel
set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));

update production_ve.daily_costs_visits_per_channel set month = month(date);

update production_ve.daily_costs_visits_per_channel set reporting_channel = 'Non identified' where reporting_channel is null;

delete from production_ve.media_rev_orders;

insert into production_ve.media_rev_orders(date, channel_group, net_rev, gross_rev, net_orders, gross_orders, new_customers_gross, new_customers) select date, channel_group, sum(net_grand_total_after_vat), sum(gross_grand_total_after_vat), sum(net_orders), sum(gross_orders), sum(new_customers_gross), sum(new_customers) from production_ve.tbl_order_detail group by date, channel_group;

update production_ve.daily_costs_visits_per_channel a inner join production_ve.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.gross_rev = b.gross_rev;

update production_ve.daily_costs_visits_per_channel a inner join production_ve.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.gross_orders = b.gross_orders;

update production_ve.daily_costs_visits_per_channel a inner join production_ve.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.new_customers_gross = b.new_customers_gross;

update production_ve.daily_costs_visits_per_channel a set net_rev_e = (select (avg(net_rev/gross_rev)) from production_ve.media_rev_orders b where b.date between date_sub(a.date, interval 21 day) and date_sub(a.date,interval 7 day) and a.reporting_channel=b.channel_group group by reporting_channel);

update production_ve.daily_costs_visits_per_channel a set net_orders_e = (select (avg(net_orders/gross_orders)) from production_ve.media_rev_orders b where b.date between date_sub(a.date, interval 21 day) and date_sub(a.date,interval 7 day) and a.reporting_channel=b.channel_group group by reporting_channel);

update production_ve.daily_costs_visits_per_channel a set new_customers_e = (select (avg(new_customers/new_customers_gross)) from production_ve.media_rev_orders b where b.date between date_sub(a.date, interval 21 day) and date_sub(a.date,interval 7 day) and a.reporting_channel=b.channel_group group by reporting_channel);

update production_ve.daily_costs_visits_per_channel set net_rev_e = net_rev_e*gross_rev, net_orders_e = net_orders_e*gross_orders, new_customers_e = new_customers_e*new_customers_gross;

update production_ve.daily_costs_visits_per_channel a inner join production_ve.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.net_rev_e = b.net_rev, a.net_orders_e = b.net_orders, a.new_customers_e = b.new_customers where a.date < date_sub(curdate(), interval 14 day);

update production_ve.daily_costs_visits_per_channel a inner join production_ve.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.net_rev_e = case when b.net_rev>a.net_rev_e then b.net_rev else a.net_rev_e end, a.net_orders_e = case when b.net_orders>a.net_orders_e then b.net_orders else a.net_orders_e end, a.new_customers_e = case when b.new_customers>a.new_customers_e then b.new_customers else a.new_customers_e end;

update production_ve.daily_costs_visits_per_channel a inner join production_ve.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.net_rev_e = case when a.net_rev_e is null then b.net_rev else a.net_rev_e end, a.net_orders_e = case when a.net_orders_e is null then b.net_orders else a.net_orders_e end, a.new_customers_e = case when a.new_customers_e is null then b.new_customers else a.new_customers_e end;

update production_ve.daily_costs_visits_per_channel a inner join production_ve.daily_targets_per_channel b 
on a.reporting_channel=b.channel_group and a.date = b.date set a.target_net_sales = b.target_net_sales, a.target_cpo = b.target_cpo;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:03:58
