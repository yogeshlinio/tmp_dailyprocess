-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: price_mapping
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'price_mapping'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`angel.m`@`%`*/ /*!50003 FUNCTION `friendly_name`(entrada varchar(200)) RETURNS varchar(200) CHARSET latin1
BEGIN

declare salida varchar(200);
set salida = entrada;

set salida = case when entrada in ('canceled',
									'canceled_in_exported',
									'cancelled_and_refund')
					   then 'Cancelada' 
				  when entrada in ('exportable','exported')
			           then 'Confirmada' 
				  when entrada in ('returned')
			           then 'Devuelta' 
				  when entrada in ('auto_fraud_check_pending')
			           then 'En revisión' 
			      when entrada in ('delivered')
			           then 'Entregada' 
				  when entrada in ('clarify_not_stock_updated',
									'shipped',
									'shipped_and_stock_updated',
									'shipped_item_reserved')
					   then 'Enviada' 
				  when entrada in ('invalid')
					   then 'Inválida'
				  when entrada in ('clarify_not_shipped',
									'ready_to_ship')
			           then 'Listo para enviar' 
				  when entrada in ('clarify_not_returned',
									'waiting_for_return')
			           then 'Pendiente de confirmar devolución'
        		  when entrada in ('payment_confirmation_pending',
									'payment_confirmation_pending_reminded_1',
									'payment_confirmation_pending_reminded_2',
									'payment_pending')
					   then 'Pendiente de pago' 
				  when entrada in ('clarify_refund_not_processed',
									'manual_refund_needed',
									'refund_needed')
					   then 'Pendiente de reembolso' 
			      when entrada in ('clarify_confirm_call',
									'clarify_manual_fraud_check',
									'clarify_not_paid',
									'clarify_payment_error',
									'clarify_payment_pending',
									'confirm_call_1',
									'confirm_call_2',
									'confirm_call_3',
									'confirm_call_4',
									'fraud_check_pending',
									'manual_fraud_check_pending',
									'pending_email_answer')
					   then 'Por confirmar' 
				  when entrada in ('refunded','refunded_after_return','cash_refunded')
			           then 'Reembolsada'
			      when entrada in ('closed')
			           then 'Terminada' end;


RETURN salida;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`alexander.c`@`%`*/ /*!50003 PROCEDURE `full_updates`()
BEGIN



SET @items_del_mes = (select count(*) from price_mapping.tbl_order_detail where month='2' and year='2013' and oac='1');
update price_mapping.tbl_order_detail 
set delivery_cost_supplier= round(15919.2/ @items_del_mes )   
where month='2' and year='2013' and oac='1';

SET @items_del_mes=(select count(*) from price_mapping.tbl_order_detail where month='3' and year='2013' and oac='1');
update price_mapping.tbl_order_detail 
set delivery_cost_supplier= round(31046.4/ @items_del_mes)   
where month='3' and year='2013' and oac='1';

SET @items_del_mes= (select count(*) from price_mapping.tbl_order_detail where month='4' and year='2013' and oac='1');   
update price_mapping.tbl_order_detail 
set delivery_cost_supplier= round(25146/ @items_del_mes)
where month='4' and year='2013' and oac='1';

SET @items_del_mes= (select count(*) from price_mapping.tbl_order_detail where month='5' and year='2013' and oac='1');   
update price_mapping.tbl_order_detail 
set delivery_cost_supplier= round(22826.1/ @items_del_mes)
where month='5' and year='2013' and oac='1';

SET @items_del_mes= (select count(*) from price_mapping.tbl_order_detail where month='6' and year='2013' and oac='1');
update price_mapping.tbl_order_detail 
set delivery_cost_supplier= round(11784.3/ @items_del_mes)
where month='6' and year='2013' and oac='1';








END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`alexander.c`@`%`*/ /*!50003 PROCEDURE `pinche_charles`()
BEGIN

drop table price_mapping.tbl_order_detail;
create table price_mapping.tbl_order_detail 
select * from production_pe.tbl_order_detail;

truncate price_mapping.cat_new_category;
insert into price_mapping.cat_new_category
   select  A.id_catalog_config,
     cast(concat(COALESCE(nullif(A.fk_catalog_attribute_option_global_category,''),''),',',
         COALESCE(nullif(A.fk_catalog_attribute_option_global_sub_category,''),''),',',
         COALESCE(nullif(A.fk_catalog_attribute_option_global_sub_sub_category,''),'')) AS CHAR(10000)) new_cat_number,
     concat(B.name,',',C.name,',',D.name) new_cat_name
   from bob_live_pe.catalog_config A 
   left join bob_live_pe.catalog_attribute_option_global_category B 
       on A.fk_catalog_attribute_option_global_category = B.id_catalog_attribute_option_global_category
   left join bob_live_pe.catalog_attribute_option_global_sub_category  C 
          on A.fk_catalog_attribute_option_global_sub_category= C.id_catalog_attribute_option_global_sub_category
   left join bob_live_pe.catalog_attribute_option_global_sub_sub_category  D 
       on A.fk_catalog_attribute_option_global_sub_sub_category = D.id_catalog_attribute_option_global_sub_sub_category;

truncate price_mapping.cat_old_category;
insert into price_mapping.cat_old_category
   select A.id_catalog_config ,GROUP_CONCAT(B.fk_catalog_category) old_cat_number, GROUP_CONCAT(C.name) old_cat_name
   from bob_live_pe.catalog_config A
   left join bob_live_pe.catalog_config_has_catalog_category B on A.id_catalog_config=B.fk_catalog_config
   left join bob_live_pe.catalog_category C on C.id_catalog_category=B.fk_catalog_category
   group by A.id_catalog_config;

truncate price_mapping.tbl_content;
insert into price_mapping.tbl_content 
select 
	A.id_catalog_simple,A.fk_catalog_config,A.fk_catalog_import,A.sku,B.name config_name,A.status,
	A.status_supplier_simple, A.created_at_external,A.updated_at_external,A.created_at,
	A.created_by_simple,A.updated_at,A.updated_by_simple,A.price,A.special_price, 
	A.special_from_date,A.special_to_date,A.fk_catalog_tax_class,A.original_price,
	A.fk_catalog_shipment_type, A.sku_supplier_simple, A.creation_source_simple, A.supplier_simple,
	A.barcode_ean,A.cost, A.delivery_cost_supplier, A.fk_catalog_attribute_option_global_package_type, 
	A.package_pieces,A.min_delivery_time,A.max_delivery_time,A.delivery_time_supplier, 
	A.storage_place,A.shipment_cost_item,A.shipment_cost_order,A.stock_master, A.eligible_free_shipping,
	D.name content_name,C.name QA_name,E.name creation_name,B.fk_catalog_attribute_option_global_category,
	G.name buyer_name,B.category_string,B.product_weight,B.package_weight,B.model,B.sku buyer_sku,
	A.sku catalog_sku,A.supplier_simple catalog_suplier_simple,F.old_cat_number,F.old_cat_name,
	H.new_cat_number,H.new_cat_name,year(A.created_at) `ano`,month(A.created_at) `mes`
from bob_live_pe.catalog_simple A
left join bob_live_pe.catalog_config B on A.fk_catalog_config = B.id_catalog_config
left join bob_live_pe.catalog_attribute_option_global_quality C on B.fk_catalog_attribute_option_global_quality = C.id_catalog_attribute_option_global_quality
left join bob_live_pe.catalog_attribute_option_global_content D on B.fk_catalog_attribute_option_global_content =  D.id_catalog_attribute_option_global_content
left join bob_live_pe.catalog_attribute_option_global_creation E on B.fk_catalog_attribute_option_global_creation= E.id_catalog_attribute_option_global_creation 
left join bob_live_pe.catalog_attribute_option_global_buyer G on B.fk_catalog_attribute_option_global_buyer= G.id_catalog_attribute_option_global_buyer
left join price_mapping.cat_old_category F on A.id_catalog_simple= F.id_catalog_config 
left join price_mapping.cat_new_category H on A.id_catalog_simple= H.id_catalog_config and new_cat_number != ',,';



Insert into price_mapping.tbl_sms_order_completNew

select od.order_nr, concat('51',ca.mobile_phone) Telefono, ot.dep Departamento,
		("") mensaje, 
		("De ser necesario tenga su pago exacto listo y dejar la copia de su DNI en caso no este presente para recibir producto") mensaje_dni,
		("")Operador 
from  production_pe.tbl_order_detail od 
left join  bob_live_pe.customer_address ca on ca.fk_customer = od.custid
left join  production_pe.out_order_tracking ot on ot.order_number = od.order_nr
where date= date('2013-08-19')

group by order_nr;

truncate price_mapping.app_price_mapper_sms_comlete;

SET @rank= 0;
insert into price_mapping.app_price_mapper_sms_comlete
select @rank:=@rank+1 AS id,A.* from price_mapping.tbl_sms_order_completNew A;


update price_mapping.app_price_mapper_sms_comlete A
set A.Telefono = ''
where A.telefono is null;

update price_mapping.app_price_mapper_sms_comlete A
set A.departamento = ''
where A.departamento is null;

update price_mapping.app_price_mapper_sms_comlete A
set A.mensaje = ''
where A.mensaje is null;

update price_mapping.app_price_mapper_sms_comlete A
set A.mensaje_dni = ''
where A.mensaje_dni is null;

Update price_mapping.app_price_mapper_sms_comlete
set operador = 'Linio' where operador is null;


call price_mapping.full_updates();

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:04:11
