-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: commercial_pe
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'commercial_pe'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`marco.lazo`@`%`*/ /*!50003 PROCEDURE `new_procedure1`()
BEGIN

drop table if  exists commercial_pe.test;
create table commercial_pe.test(
Select B.uniqueid,
FROM_UNIXTIME(A.timestamp,'%Y-%m-%d %H:%i:%s')call_datetime,
FROM_UNIXTIME(A.timestamp,'%Y-%m-%d')call_date,
C.name agent_name,
(case when A.agent<1000 then SUBSTR(A.agent,5,3) else A.agent end)agent_ext,
FROM_UNIXTIME(A.timestamp,'%H:%i:%s')call_time,
A.info2 call_duration,
B.phone callerid,
((case when B.id_quest_1=0 then 0 else 1 end)+(case when B.id_quest_2=0 then 0 else 1 end)+(case when B.id_quest_3=0 then 0 else 1 end)) respuestas,
((case when B.id_quest_1=0 then 1 else 0 end)+(case when B.id_quest_2=0 then 1 else 0 end)+(case when B.id_quest_3=0 then 1 else 0 end)) abandon_in,

(case when B.sel_opt_1=1 then 1 else 0 end) answer1_option_1,
(case when B.sel_opt_1=2 then 1 else 0 end) answer1_option_2,
(case when B.id_quest_1=0 then 1 else 0 end) answer1_abandon,

(case when B.sel_opt_2=1 then 1 else 0 end) answer2_option_1,
(case when B.sel_opt_2=2 then 1 else 0 end) answer2_option_2,
(case when B.sel_opt_2=3 then 1 else 0 end) answer2_option_3,
(case when B.id_quest_2=0 then 1 else 0 end) answer2_abandon,

(case when B.sel_opt_3=1 then 1 else 0 end) answer3_option_1,
(case when B.sel_opt_3=2 then 1 else 0 end) answer3_option_2,
(case when B.sel_opt_3=3 then 1 else 0 end) answer3_option_3,
(case when B.id_quest_3=0 then 1 else 0 end) answer3_abandon,

(case when B.id_quest_1=0 then 0 else 1 end) answer_1,
(case when B.id_quest_2=0 then 0 else 1 end) answer_2,
(case when B.id_quest_3=0 then 0 else 1 end) answer_3

from customer_service_pe.queuelog A
left join customer_service_pe.reports B on A.callid=B.callid
left join customer_service_pe.users C on SUBSTR(A.agent,5,3)=C.extension
where qname='1002'
and action in ('COMPLETEAGENT','COMPLETECALLER') and FROM_UNIXTIME(A.timestamp,'%Y-%m-%d')>='2014-05-14'
and B.status is not null

);

alter table customer_service_pe.test
ADD id int NOT NULL AUTO_INCREMENT primary key FIRST
 
 ;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`daniel.h`@`%`*/ /*!50003 PROCEDURE `rout_A_MasterALL`()
BEGIN

##creacion de un A_Master de todos los paises
drop table if exists commercial_pe.A_MasterALL;
create table commercial_pe.A_MasterALL
select 'PER' as Country,A.id_catalog_simple,A.sku_simple,B.MonthNum,B.Date,B.Time,B.OrderNum,B.IdSalesOrder,
B.ItemID,B.IdSupplier,B.Supplier,B.SupplierType,B.Buyer,B.HeadBuyer,B.Brand,B.StoreId,B.CatKPI,B.CatBP,B.Cat1,	
B.Cat2,	B.Cat3,	B.SKUConfig,B.SKUSimple,B.SKUName,B.isActiveSKUConfig,B.isActiveSKUSimple,B.isVisible,B.isMPlace,B.isB2B,B.isMPlaceSince,	
B.isMPlaceUntil,B.MPlaceFee,B.ProductWeight,B.PackageWeight,B.VolumeWeight,B.PackageHeight,B.PackageLength,B.PackageWidth,B.OrderWeight,	
B.ItemsInOrder,B.NetItemsInOrder,B.OriginalPrice,B.OriginalPriceAfterTax,B.TaxPercent,B.Tax,B.Price,
B.PriceAfterTax,B.PrefixCode,B.CouponCode,B.CouponCodeDescription,	
B.CouponValue,B.CouponValueAfterTax,B.NonMKTCouponAfterTax,	B.PaidPrice,
(B.PaidPriceAfterTax/2.7) as PaidPriceAfterTax,B.Fees,B.ExtraCharge,B.Min_Value,B.GrandTotal,	
B.Cost,	B.CostAfterTax,B.WHCost,B.FLWHCost,B.CSCost,B.FLCSCost,B.fk_courier,B.Courier,B.ShipmentType,B.FulfillmentTypeReal,B.DeliveryCostSupplier,B.ShipmentCost,	
B.ShippingCost,B.ShippingFee,
(B.ShippingFeeAfterTax/2.7) as ShippingFeeAfterTax ,B.PackagingCost,B.PaymentMethod,B.IssuingBank,B.Bin,B.Installment,B.PaymentFees,B.TransactionFee,	
B.TransactionFeeAfterTax,B.InstallmentFee,B.InstallmentFeeAfterTax,B.Interest,	B.NetInterest,B.AssistedSalesOperator,B.CustomerNum,	
B.CustomerEmail,B.FirstName,B.LastName,B.fk_shipment_zone,B.fk_shipment_zone_mapping,B.PostCode,B.State,B.City,B.Status,	
B.OrderBeforeCan,B.OrderAfterCan,B.Cancellations,B.Pending,B.Returns,B.DateReturned,B.Rejected,B.Cancelled,B.DateDelivered,	
B.Delivered,B.DeliveredReturn,B.NetDelivered,B.DateExported,B.Exported,B.DateCollected,B.Collected,B.DateRefunded,B.Refunded,B.Exportable,	
B.Shipped,B.DateShipped,B.ConfirmCall1,B.FraudCheckPending,B.MCI,B.NetMCI,B.MSI,B.MarketingChannel,B.CategoryAd,	
B.PaidFreeChannel,B.CohortMonthNum,B.FirstOrderNum,B.NewReturning,B.NewReturningGross,B.Source,B.Source_medium,	
B.Campaign,B.Ownership,B.Channel,B.ChannelType,B.ChannelGroup,B.CohortChannel,B.ChannelGroupAnterior,B.CACCustomer,B.CreditNotes,	
B.COGS,	B.PCOne,B.PCOnePFive,B.PCTwo,B.DeliveryType,B.Rev,B.OtherRev
from development_pe.A_Master_Catalog A
inner join (select * from development_pe.A_Master 
where year(Date)>='2014' and CatBP='3,1 Fashion') B on 
A.sku_simple=B.SKUSimple
where A.Cat_BP='3,1 Fashion'
union
select 'COL' as Country,A.id_catalog_simple,A.sku_simple,B.MonthNum,B.Date,B.Time,B.OrderNum,B.IdSalesOrder,
B.ItemID,B.IdSupplier,B.Supplier,B.SupplierType,B.Buyer,B.HeadBuyer,B.Brand,B.StoreId,B.CatKPI,B.CatBP,B.Cat1,	
B.Cat2,	B.Cat3,	B.SKUConfig,B.SKUSimple,B.SKUName,B.isActiveSKUConfig,B.isActiveSKUSimple,B.isVisible,B.isMPlace,B.isB2B,B.isMPlaceSince,	
B.isMPlaceUntil,B.MPlaceFee,B.ProductWeight,B.PackageWeight,B.VolumeWeight,B.PackageHeight,B.PackageLength,B.PackageWidth,B.OrderWeight,	
B.ItemsInOrder,B.NetItemsInOrder,B.OriginalPrice,B.OriginalPriceAfterTax,B.TaxPercent,B.Tax,B.Price,B.PriceAfterTax,B.PrefixCode,B.CouponCode,B.CouponCodeDescription,	
B.CouponValue,B.CouponValueAfterTax,B.NonMKTCouponAfterTax,	B.PaidPrice,
(B.PaidPriceAfterTax/1900) as PaidPriceAfterTax,B.Fees,B.ExtraCharge,B.Min_Value,B.GrandTotal,	
B.Cost,	B.CostAfterTax,B.WHCost,B.FLWHCost,B.CSCost,B.FLCSCost,B.fk_courier,B.Courier,B.ShipmentType,B.FulfillmentTypeReal,B.DeliveryCostSupplier,B.ShipmentCost,	
B.ShippingCost,B.ShippingFee,
(B.ShippingFeeAfterTax/1900) as ShippingFeeAfterTax,B.PackagingCost,B.PaymentMethod,B.IssuingBank,B.Bin,B.Installment,B.PaymentFees,B.TransactionFee,	
B.TransactionFeeAfterTax,B.InstallmentFee,B.InstallmentFeeAfterTax,B.Interest,	B.NetInterest,B.AssistedSalesOperator,B.CustomerNum,	
B.CustomerEmail,B.FirstName,B.LastName,B.fk_shipment_zone,B.fk_shipment_zone_mapping,B.PostCode,B.State,B.City,B.Status,	
B.OrderBeforeCan,B.OrderAfterCan,B.Cancellations,B.Pending,B.Returns,B.DateReturned,B.Rejected,B.Cancelled,B.DateDelivered,	
B.Delivered,B.DeliveredReturn,B.NetDelivered,B.DateExported,B.Exported,B.DateCollected,B.Collected,B.DateRefunded,B.Refunded,B.Exportable,	
B.Shipped,B.DateShipped,B.ConfirmCall1,B.FraudCheckPending,B.MCI,B.NetMCI,B.MSI,B.MarketingChannel,B.CategoryAd,	
B.PaidFreeChannel,B.CohortMonthNum,B.FirstOrderNum,B.NewReturning,B.NewReturningGross,B.Source,B.Source_medium,	
B.Campaign,B.Ownership,B.Channel,B.ChannelType,B.ChannelGroup,B.CohortChannel,B.ChannelGroupAnterior,B.CACCustomer,B.CreditNotes,	
B.COGS,	B.PCOne,B.PCOnePFive,B.PCTwo,B.DeliveryType,B.Rev,B.OtherRev
from development_co_project.A_Master_Catalog A
inner join (select * from development_co_project.A_Master 
where year(Date)>='2014' and CatBP='3,1 Fashion') B on 
A.sku_simple=B.SKUSimple
where A.Cat_BP='3,1 Fashion' 
union
select 'MEX' as Country,A.id_catalog_simple,A.sku_simple,B.MonthNum,B.Date,B.Time,B.OrderNum,B.IdSalesOrder,
B.ItemID,B.IdSupplier,B.Supplier,B.SupplierType,B.Buyer,B.HeadBuyer,B.Brand,B.StoreId,B.CatKPI,B.CatBP,B.Cat1,	
B.Cat2,	B.Cat3,	B.SKUConfig,B.SKUSimple,B.SKUName,B.isActiveSKUConfig,B.isActiveSKUSimple,B.isVisible,B.isMPlace,B.isB2B,B.isMPlaceSince,	
B.isMPlaceUntil,B.MPlaceFee,B.ProductWeight,B.PackageWeight,B.VolumeWeight,B.PackageHeight,B.PackageLength,B.PackageWidth,B.OrderWeight,	
B.ItemsInOrder,B.NetItemsInOrder,B.OriginalPrice,B.OriginalPriceAfterTax,B.TaxPercent,B.Tax,B.Price,B.PriceAfterTax,B.PrefixCode,B.CouponCode,B.CouponCodeDescription,	
B.CouponValue,B.CouponValueAfterTax,B.NonMKTCouponAfterTax,	B.PaidPrice,
B.PaidPriceAfterTax/13 as PaidPriceAfterTax ,B.Fees,B.ExtraCharge,B.Min_Value,B.GrandTotal,	
B.Cost,	B.CostAfterTax,B.WHCost,B.FLWHCost,B.CSCost,B.FLCSCost,B.fk_courier,B.Courier,B.ShipmentType,B.FulfillmentTypeReal,B.DeliveryCostSupplier,B.ShipmentCost,	
B.ShippingCost,B.ShippingFee,
B.ShippingFeeAfterTax/13 as ShippingFeeAfterTax,B.PackagingCost,B.PaymentMethod,B.IssuingBank,B.Bin,B.Installment,B.PaymentFees,B.TransactionFee,	
B.TransactionFeeAfterTax,B.InstallmentFee,B.InstallmentFeeAfterTax,B.Interest,	B.NetInterest,B.AssistedSalesOperator,B.CustomerNum,	
B.CustomerEmail,B.FirstName,B.LastName,B.fk_shipment_zone,B.fk_shipment_zone_mapping,B.PostCode,B.State,B.City,B.Status,	
B.OrderBeforeCan,B.OrderAfterCan,B.Cancellations,B.Pending,B.Returns,B.DateReturned,B.Rejected,B.Cancelled,B.DateDelivered,	
B.Delivered,B.DeliveredReturn,B.NetDelivered,B.DateExported,B.Exported,B.DateCollected,B.Collected,B.DateRefunded,B.Refunded,B.Exportable,	
B.Shipped,B.DateShipped,B.ConfirmCall1,B.FraudCheckPending,B.MCI,B.NetMCI,B.MSI,B.MarketingChannel,B.CategoryAd,	
B.PaidFreeChannel,B.CohortMonthNum,B.FirstOrderNum,B.NewReturning,B.NewReturningGross,B.Source,B.Source_medium,	
B.Campaign,B.Ownership,B.Channel,B.ChannelType,B.ChannelGroup,B.CohortChannel,B.ChannelGroupAnterior,B.CACCustomer,B.CreditNotes,	
B.COGS,	B.PCOne,B.PCOnePFive,B.PCTwo,B.DeliveryType,B.Rev,B.OtherRev
from development_mx.A_Master_Catalog A
inner join (select * from development_mx.A_Master 
where year(Date)>='2014' and CatBP='3,1 Fashion') B on 
A.sku_simple=B.SKUSimple
where A.Cat_BP='3,1 Fashion'
union
select 'VEN' as Country,A.id_catalog_simple,A.sku_simple,B.MonthNum,B.Date,B.Time,B.OrderNum,B.IdSalesOrder,
B.ItemID,B.IdSupplier,B.Supplier,B.SupplierType,B.Buyer,B.HeadBuyer,B.Brand,B.StoreId,B.CatKPI,B.CatBP,B.Cat1,	
B.Cat2,	B.Cat3,	B.SKUConfig,B.SKUSimple,B.SKUName,B.isActiveSKUConfig,B.isActiveSKUSimple,B.isVisible,B.isMPlace,B.isB2B,B.isMPlaceSince,	
B.isMPlaceUntil,B.MPlaceFee,B.ProductWeight,B.PackageWeight,B.VolumeWeight,B.PackageHeight,B.PackageLength,B.PackageWidth,B.OrderWeight,	
B.ItemsInOrder,B.NetItemsInOrder,B.OriginalPrice,B.OriginalPriceAfterTax,B.TaxPercent,B.Tax,B.Price,B.PriceAfterTax,B.PrefixCode,B.CouponCode,B.CouponCodeDescription,	
B.CouponValue,B.CouponValueAfterTax,B.NonMKTCouponAfterTax,	B.PaidPrice,
B.PaidPriceAfterTax/54 as PaidPriceAfterTax,B.Fees,B.ExtraCharge,B.Min_Value,B.GrandTotal,	
B.Cost,	B.CostAfterTax,B.WHCost,B.FLWHCost,B.CSCost,B.FLCSCost,B.fk_courier,B.Courier,B.ShipmentType,B.FulfillmentTypeReal,B.DeliveryCostSupplier,B.ShipmentCost,	
B.ShippingCost,B.ShippingFee,
B.ShippingFeeAfterTax/54 as ShippingFeeAfterTax,B.PackagingCost,B.PaymentMethod,B.IssuingBank,B.Bin,B.Installment,B.PaymentFees,B.TransactionFee,	
B.TransactionFeeAfterTax,B.InstallmentFee,B.InstallmentFeeAfterTax,B.Interest,	B.NetInterest,B.AssistedSalesOperator,B.CustomerNum,	
B.CustomerEmail,B.FirstName,B.LastName,B.fk_shipment_zone,B.fk_shipment_zone_mapping,B.PostCode,B.State,B.City,B.Status,	
B.OrderBeforeCan,B.OrderAfterCan,B.Cancellations,B.Pending,B.Returns,B.DateReturned,B.Rejected,B.Cancelled,B.DateDelivered,	
B.Delivered,B.DeliveredReturn,B.NetDelivered,B.DateExported,B.Exported,B.DateCollected,B.Collected,B.DateRefunded,B.Refunded,B.Exportable,	
B.Shipped,B.DateShipped,B.ConfirmCall1,B.FraudCheckPending,B.MCI,B.NetMCI,B.MSI,B.MarketingChannel,B.CategoryAd,	
B.PaidFreeChannel,B.CohortMonthNum,B.FirstOrderNum,B.NewReturning,B.NewReturningGross,B.Source,B.Source_medium,	
B.Campaign,B.Ownership,B.Channel,B.ChannelType,B.ChannelGroup,B.CohortChannel,B.ChannelGroupAnterior,B.CACCustomer,B.CreditNotes,	
B.COGS,	B.PCOne,B.PCOnePFive,B.PCTwo,B.DeliveryType,B.Rev,B.OtherRev
from development_ve.A_Master_Catalog A
inner join (select * from development_ve.A_Master 
where year(Date)>='2014' and CatBP='3,1 Fashion') B on 
A.sku_simple=B.SKUSimple
where A.Cat_BP='3,1 Fashion';

ALTER TABLE commercial_pe.A_MasterALL
ADD INDEX `id_simple` (`id_catalog_simple` ASC) ;

##creacion de los unit sold and cancelled
drop table if exists commercial_pe.F_R_FashionSales_prueba;
create table commercial_pe.F_R_FashionSales_prueba(
select
A.Country ,A.id_catalog_simple,A.SKUSimple, 
sum(if(week(A.Date) = week(curdate())-4  and A.OrderAfterCan=1,A.PaidPriceAfterTax+A.ShippingFeeAfterTax , 0)) as 'R4lastWeek',
sum(if(week(A.Date) = week(curdate())-1  and A.OrderAfterCan=1, A.PaidPriceAfterTax+A.ShippingFeeAfterTax , 0)) as 'RlastWeek',
sum(if(week(A.Date) = week(curdate())-2  and A.OrderAfterCan=1, A.PaidPriceAfterTax+A.ShippingFeeAfterTax , 0)) as 'R2lastWeek',
sum(if(A.OriginalPrice = A.Price  and week(A.Date) = week(curdate())-1   and A.OrderAfterCan=1, A.PaidPriceAfterTax+A.ShippingFeeAfterTax , 0)) as 'RPriceNODiscountlastweek',
sum(if(week(A.Date) = week(curdate())-1  and A.OrderAfterCan=1, 1 , 0)) as 'NetItemlastweek',
sum(if(week(A.Date) = week(curdate())-1  and A.OriginalPrice = A.Price   and A.OrderAfterCan=1,1,0 )) as 'UnitSoldNODiscountlastweek',
sum(if(week(A.Date) = week(curdate())-1  and A.OrderBeforeCan =1, 1 , 0)) as 'GrossItemlastweek',
sum(if(week(A.Date) = week(curdate())-1  and A.Cancellations =1, 1 , 0)) as 'CancelledItemlastweek',
sum(if(week(A.Date) = week(curdate())-1  and A.Returns =1, 1 , 0)) as 'ReturnedItemlastweek',
sum(if(week(A.Date) = week(curdate())-1  and A.Rejected  =1, 1 , 0)) as 'RejectedItemlastweek',
sum(if(week(A.Date) = week(curdate())  and A.OrderAfterCan=1, 1 , 0)) as 'UnitSoldWeek',
sum(if(week(A.Date) = week(curdate())-1  and A.OrderAfterCan=1, 1 , 0)) as 'UnitSoldLastWeek',
sum(if(week(A.Date) = week(curdate())-2  and A.OrderAfterCan=1, 1 , 0)) as 'UnitSold2Week',
sum(if(week(A.Date) = week(curdate())-3  and A.OrderAfterCan=1, 1 , 0)) as 'UnitSold3Week',
sum(if(week(A.Date) = week(curdate())-4  and A.OrderAfterCan=1, 1 , 0)) as 'UnitSold4Week',
sum(if(week(A.Date) = week(curdate())-5  and A.OrderAfterCan=1, 1 , 0)) as 'UnitSold5Week',
sum(if(week(A.Date) = week(curdate())-6  and A.OrderAfterCan=1, 1 , 0)) as 'UnitSold6Week',
sum(if(week(A.Date) = week(curdate())-7  and A.OrderAfterCan=1, 1 , 0)) as 'UnitSold7Week',
sum(if(week(A.Date) = week(curdate())-8  and A.OrderAfterCan=1, 1 , 0)) as 'UnitSold8Week',
#MTD
sum(if( month(A.Date) = month(curdate())  and A.OrderAfterCan=1, A.PaidPriceAfterTax+A.ShippingFeeAfterTax , 0)) as 'RFullPriceMTD',
sum(if( A.OriginalPrice = A.Price  and month(A.Date) = month(curdate()) and A.OrderAfterCan=1, A.PaidPriceAfterTax+A.ShippingFeeAfterTax , 0)) as 'RPriceNODiscountMTD',
sum(if( month(A.Date) = month(now())    and A.OrderAfterCan=1, 1 , 0)) as 'NetItemMTD',
sum(if( A.OriginalPrice = A.Price  and month(A.Date) = month(curdate()) and A.OrderAfterCan=1,1,0 )) as 'UnitSoldNODiscountMTD',
sum(if( month(A.Date) = month(now())   and A.OrderBeforeCan =1, 1 , 0)) as 'GrossItemMTD',
sum(if( month(A.Date) = month(now())   and A.Cancellations =1, 1 , 0)) as 'CancelledItemMTD',
sum(if( month(A.Date) = month(now())   and A.Returns =1, 1 , 0)) as 'ReturnedItemMTD',
sum(if( month(A.Date) = month(now())   and A.Rejected  =1, 1 , 0)) as 'RejectedItemMTD',
#YTD
sum(if( A.OrderAfterCan=1, A.PaidPriceAfterTax+A.ShippingFeeAfterTax , 0)) as 'RFullPriceYTD',
sum(if(A.OriginalPrice = A.Price and A.OrderAfterCan=1, A.PaidPriceAfterTax+A.ShippingFeeAfterTax , 0)) as 'RPriceNODiscountYTD',
sum(if( A.OrderAfterCan=1, 1 , 0)) as 'NetItemYTD',
sum(if( A.OriginalPrice = A.Price  and A.OrderAfterCan=1,1,0 )) as 'UnitSoldNODiscountYTD',
sum(if(  A.OrderBeforeCan =1, 1 , 0)) as 'GrossItemYTD',
sum(if( A.Cancellations =1, 1 , 0)) as 'CancelledItemYTD',
sum(if(  A.Returns =1, 1 , 0)) as 'ReturnedItemYTD',
sum(if( A.Rejected  =1, 1 , 0)) as 'RejectedItemYTD'

from
commercial_pe.A_MasterALL A
where
year(A.Date)>='2014'
group by A.Country,A.SKUSimple);

ALTER TABLE commercial_pe.F_R_FashionSales_prueba
ADD INDEX `id_simple` (`id_catalog_simple` ASC) ;

ALTER TABLE commercial_pe.F_R_FashionSales_prueba
ADD INDEX `SKUSimple` (`SKUSimple` ASC) ;

ALTER TABLE commercial_pe.F_R_FashionSales_prueba
ADD INDEX `Country` (`Country` ASC) ;

## creancion de una tabla para los pc and discount rate
drop table if exists commercial_pe.PC_DR;
create table commercial_pe.PC_DR
select Country,id_catalog_simple,sku_simple,
avg(if(CatBP='3,1 Fashion' and OrderAfterCan='1' and week(Date)=week(curdate())-1 and year(Date)=year(curdate()),PCOne/Rev,0)) as PCOne_last_week,
avg(if(CatBP='3,1 Fashion' and OrderAfterCan='1' and month(Date)=month(curdate())and year(Date)=year(curdate()),PCOne/Rev,0)) as PCOne_month,
avg(if(CatBP='3,1 Fashion' and OrderAfterCan='1' and year(Date)=year(curdate()),PCOne/Rev,0)) as PCOne_year,
avg(if(CatBP='3,1 Fashion' and OrderAfterCan='1' and week(Date)=week(curdate())-4 and year(Date)=year(curdate()),PCOne/Rev,0)) as PCOne_4last_week,
avg(if(CatBP='3,1 Fashion' and OrderAfterCan='1' and week(Date)=week(curdate())-4 and year(Date)=year(curdate()),(Price-PaidPrice)/Price,0)) as discount_rate_4last_week,
avg(if(CatBP='3,1 Fashion' and OrderAfterCan='1' and week(Date)=week(curdate())-1 and year(Date)=year(curdate()),(Price-PaidPrice)/Price,0)) as discount_rate_last_week,
avg(if(CatBP='3,1 Fashion' and OrderAfterCan='1' and month(Date)=month(curdate()) and year(Date)=year(curdate()),(Price-PaidPrice)/Price,0)) as discount_rate_month,
avg(if(CatBP='3,1 Fashion' and OrderAfterCan='1'and year(Date)=year(curdate()),(Price-PaidPrice)/Price,0)) as discount_rate_year
from commercial_pe.A_MasterALL
group by 2,3;

ALTER TABLE commercial_pe.PC_DR
ADD INDEX `id_simple` (`id_catalog_simple` ASC) ;

ALTER TABLE commercial_pe.PC_DR
ADD INDEX `SKUSimple` (`sku_simple` ASC) ;

ALTER TABLE commercial_pe.PC_DR
ADD INDEX `Country` (`Country` ASC) ;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`daniel.h`@`%`*/ /*!50003 PROCEDURE `rout_BDFashion`()
BEGIN
#borrar tabla de skus fashion regional
drop table if exists commercial_pe.F_R_FashionSKUs;
#crear tabla de skus fashion regional
create table commercial_pe.F_R_FashionSKUs
select
'PER' as 'Country' ,B.sku_config,A.fk_catalog_simple,B.sku_simple ,B.sku_name, date(B.created_at_simple), date(B.created_at_config)  ,B.Cat1, B.Cat2, B.Cat3, B.Brand, B.Buyer,B.Supplier, E.name as 'Gender', F.name as  'Size', 
B.isVisible, B.isMarketPlace, concat('www.linio.com.pe/',cast(B.id_catalog_config as char(100)),'.html') as 'Url'
from
bob_live_pe.catalog_simple_fashion A
left join development_pe.A_Master_Catalog B
on A.fk_catalog_simple =B.id_catalog_simple
left join bob_live_pe.catalog_config C
on B.id_catalog_config =C.id_catalog_config
left join bob_live_pe.catalog_config_fashion D
on C.id_catalog_config= D.fk_catalog_config
left join bob_live_pe.catalog_attribute_option_fashion_gender E
on E.id_catalog_attribute_option_fashion_gender= D.fk_catalog_attribute_option_fashion_gender
left join bob_live_pe.catalog_attribute_option_fashion_size F
on F.id_catalog_attribute_option_fashion_size= A.fk_catalog_attribute_option_fashion_size
union
select
'MEX' as 'Country' ,B.sku_config,A.fk_catalog_simple, B.sku_simple ,B.sku_name, date(B.created_at_simple), date(B.created_at_config)  ,B.Cat1, B.Cat2, B.Cat3, B.Brand, B.Buyer,B.Supplier, E.name as 'Gender', F.name as  'Size', 
B.isVisible, B.isMarketPlace, concat('www.linio.com.mx/',cast(B.id_catalog_config as char(100)),'.html') as 'Url'
from
bob_live_mx.catalog_simple_fashion A
left join development_mx.A_Master_Catalog B
on A.fk_catalog_simple =B.id_catalog_simple
left join bob_live_mx.catalog_config C
on B.id_catalog_config =C.id_catalog_config
left join bob_live_mx.catalog_config_fashion D
on C.id_catalog_config= D.fk_catalog_config
left join bob_live_mx.catalog_attribute_option_fashion_gender E
on E.id_catalog_attribute_option_fashion_gender= D.fk_catalog_attribute_option_fashion_gender
left join bob_live_mx.catalog_attribute_option_fashion_size F
on F.id_catalog_attribute_option_fashion_size= A.fk_catalog_attribute_option_fashion_size
union
select
'VEN' as 'Country' ,B.sku_config,A.fk_catalog_simple, B.sku_simple ,B.sku_name,date(B.created_at_simple), date(B.created_at_config)  ,B.Cat1, B.Cat2, B.Cat3, B.Brand, B.Buyer,B.Supplier, E.name as 'Gender', F.name as  'Size', 
B.isVisible, B.isMarketPlace, concat('www.linio.com.ve/',cast(B.id_catalog_config as char(100)),'.html') as 'Url'
from
bob_live_ve.catalog_simple_fashion A
left join development_ve.A_Master_Catalog B
on A.fk_catalog_simple =B.id_catalog_simple
left join bob_live_ve.catalog_config C
on B.id_catalog_config =C.id_catalog_config
left join bob_live_ve.catalog_config_fashion D
on C.id_catalog_config= D.fk_catalog_config
left join bob_live_ve.catalog_attribute_option_fashion_gender E
on E.id_catalog_attribute_option_fashion_gender= D.fk_catalog_attribute_option_fashion_gender
left join bob_live_ve.catalog_attribute_option_fashion_size F
on F.id_catalog_attribute_option_fashion_size= A.fk_catalog_attribute_option_fashion_size
union
select
'COL' as 'Country' ,B.sku_config,A.fk_catalog_simple, B.sku_simple ,B.sku_name, date(B.created_at_simple), date(B.created_at_config)  ,B.Cat1, B.Cat2, B.Cat3, B.Brand, B.Buyer,B.Supplier, E.name as 'Gender', F.name as  'Size', 
B.isVisible, B.isMarketPlace, concat('www.linio.com.co/',cast(B.id_catalog_config as char(100)),'.html') as 'Url'
from
bob_live_co.catalog_simple_fashion A
left join development_co_project.A_Master_Catalog B
on A.fk_catalog_simple =B.id_catalog_simple
left join bob_live_co.catalog_config C
on B.id_catalog_config =C.id_catalog_config
left join bob_live_co.catalog_config_fashion D
on C.id_catalog_config= D.fk_catalog_config
left join bob_live_co.catalog_attribute_option_fashion_gender E
on E.id_catalog_attribute_option_fashion_gender= D.fk_catalog_attribute_option_fashion_gender
left join bob_live_co.catalog_attribute_option_fashion_size F
on F.id_catalog_attribute_option_fashion_size= A.fk_catalog_attribute_option_fashion_size
;

alter table commercial_pe.F_R_FashionSKUs  add INDEX `sku_simple` (`sku_simple` ASC) ;
alter table commercial_pe.F_R_FashionSKUs  add INDEX `fk_catalog_simple` (`fk_catalog_simple` ASC) ;
alter table commercial_pe.F_R_FashionSKUs  add INDEX `Country` (`Country` ASC) ;
update commercial_pe.F_R_FashionSKUs A
set A.Gender='Other'
where A.Gender is null;

ALTER TABLE commercial_pe.F_R_FashionSKUs 
ADD INDEX `sku_config` (`sku_config` ASC);

/*#borrar tabla Fashion Sales Regional
drop table if exists commercial_pe.F_R_FashionSales;
#Crear tabla Fashion Sales Regional
create table commercial_pe.F_R_FashionSales (
select
A.Country ,A.SKUSimple, 
sum(if( week(A.Date) = week(curdate())-1  and A.OrderAfterCan=1, A.PaidPriceAfterTax+A.ShippingFeeAfterTax , 0)) as 'RlastWeek',
sum(if( week(A.Date) = week(curdate())-1  and A.OrderAfterCan=1, 1 , 0)) as 'UnitSoldLastWeek',
sum(if( week(A.Date) = week(curdate())-2  and A.OrderAfterCan=1, 1 , 0)) as 'UnitSold2Week',
sum(if( week(A.Date) = week(curdate())-3  and A.OrderAfterCan=1, 1 , 0)) as 'UnitSold3Week',
sum(if( week(A.Date) = week(curdate())-4  and A.OrderAfterCan=1, 1 , 0)) as 'UnitSold4Week',
sum(if( week(A.Date) = week(curdate())-5  and A.OrderAfterCan=1, 1 , 0)) as 'UnitSold5Week',
sum(if( week(A.Date) = week(curdate())-6  and A.OrderAfterCan=1, 1 , 0)) as 'UnitSold6Week',
sum(if( week(A.Date) = week(curdate())-7  and A.OrderAfterCan=1, 1 , 0)) as 'UnitSold7Week',
sum(if( week(A.Date) = week(curdate())-8  and A.OrderAfterCan=1, 1 , 0)) as 'UnitSold8Week',
#MTD
sum(if( month(A.Date) = month(curdate())  and A.OrderAfterCan=1, A.PaidPriceAfterTax+A.ShippingFeeAfterTax , 0)) as 'RFullPriceMTD',
sum(if( A.OriginalPrice = A.Price  and month(A.Date) = month(curdate()) and A.OrderAfterCan=1, A.PaidPriceAfterTax+A.ShippingFeeAfterTax , 0)) as 'RPriceNODiscountMTD',
sum(if( month(A.Date) = month(now())    and A.OrderAfterCan=1, 1 , 0)) as 'NetItemMTD',
sum(if( A.OriginalPrice = A.Price  and month(A.Date) = month(curdate()) and A.OrderAfterCan=1,1,0 )) as 'UnitSoldNODiscountMTD',
sum(if( month(A.Date) = month(now())   and A.OrderBeforeCan =1, 1 , 0)) as 'GrossItemMTD',
sum(if( month(A.Date) = month(now())   and A.Cancellations =1, 1 , 0)) as 'CancelledItemMTD',
sum(if( month(A.Date) = month(now())   and A.Returns =1, 1 , 0)) as 'ReturnedItemMTD',
sum(if( month(A.Date) = month(now())   and A.Rejected  =1, 1 , 0)) as 'RejectedItemMTD',
#YTD
sum(if( A.OrderAfterCan=1, A.PaidPriceAfterTax+A.ShippingFeeAfterTax , 0)) as 'RFullPriceYTD',
sum(if( A.OrderAfterCan=1, A.PaidPriceAfterTax+A.ShippingFeeAfterTax , 0)) as 'RPriceNODiscountYTD',
sum(if( A.OrderAfterCan=1, 1 , 0)) as 'NetItemYTD',
sum(if( A.OrderAfterCan=1,1,0 )) as 'UnitSoldNODiscountYTD',
sum(if(  A.OrderBeforeCan =1, 1 , 0)) as 'GrossItemYTD',
sum(if( A.Cancellations =1, 1 , 0)) as 'CancelledItemYTD',
sum(if(  A.Returns =1, 1 , 0)) as 'ReturnedItemYTD',
sum(if( A.Rejected  =1, 1 , 0)) as 'RejectedItemYTD'

from
test_linio.A_Master A
where
year(A.Date)>='2014'
group by A.Country,A.SKUSimple
);*/





END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`sthip.blas`@`%`*/ /*!50003 PROCEDURE `rout_BDPet_New`()
BEGIN

create table commercial_pe.campos(
select B.sku,C.sku_config,B.fk_catalog_config,B.cost,
B.price,B.special_price,B.special_from_date,B.special_to_date,C.Brand,C.Buyer,
C.id_catalog_attribute_option_global_category,
C.id_catalog_attribute_option_global_sub_category
,C.Cat_KPI,C.Cat_BP
,C.Cat1,C.Cat2,C.Cat3,C.sku_name,C.Package_Weight,C.Product_weight,C.Supplier,D.model,
if (C.Supplier like '%(MP%)%',1,0) as isMarketPlace,
C.isVisible,B.id_catalog_simple,C.barcode_ean,
B.status,B.shipment_cost_item,B.shipment_cost_order,
B.size_supplier,
C.min_delivery_time,C.max_delivery_time,C.original_price,
if(B.special_price is null,B.price,if(B.special_to_date>now() or B.special_to_date is null,
B.special_price,B.price)) as real_price
,concat('www.linio.com.pe/',cast(fk_catalog_config as char(100)),'.html') as url
from bob_live_pe.catalog_simple B
left join  development_pe.A_Master_Catalog C  on B.id_catalog_simple=C.id_catalog_simple
left join bob_live_pe.catalog_config D on D.id_catalog_config=B.fk_catalog_config);

alter table commercial_pe.campos
ADD INDEX `idxxx` (`id_catalog_simple` ASC) ;


create table commercial_pe.stock_supplier(
select C.id_catalog_simple,A.stock_supplier
from  bob_live_pe.catalog_simple  C 
inner join (select fk_catalog_simple,sum(quantity) as stock_supplier
from  bob_live_pe.catalog_supplier_stock 
group by fk_catalog_simple) 
A on A.fk_catalog_simple=C.id_catalog_simple);

alter table commercial_pe.stock_supplier
ADD INDEX `id` (`id_catalog_simple` ASC) ;


create table commercial_pe.stock_warehouse(
select C.id_catalog_simple,B.stock_warehouse
from  bob_live_pe.catalog_simple  C 
inner join (select fk_catalog_simple,sum(quantity) as stock_warehouse
from  bob_live_pe.catalog_warehouse_stock 
group by fk_catalog_simple) 
B on B.fk_catalog_simple=C.id_catalog_simple);

alter table commercial_pe.stock_warehouse
ADD INDEX `idx` (`id_catalog_simple` ASC) ;


create table commercial_pe.stock(
select A.id_catalog_simple,A.stock_supplier,
if(B.stock_warehouse is null,0,B.stock_warehouse) as stock_warehouse
,(A.stock_supplier + B.stock_warehouse ) as total_stock
from commercial_pe.stock_supplier A
inner join commercial_pe.stock_warehouse B on A.id_catalog_simple=B.id_catalog_simple);

alter table commercial_pe.stock
ADD INDEX `idx` (`id_catalog_simple` ASC) ;

drop table commercial_pe.BDPet_New;
create table commercial_pe.BDPet_New(
select A.*,B.stock_supplier,B.stock_warehouse,
if(B.total_stock is null,0,B.total_stock) as total_stock 
from commercial_pe.campos A
left join commercial_pe.stock B on B.id_catalog_simple=A.id_catalog_simple);

alter table commercial_pe.BDPet_New
ADD INDEX `idxz` (`id_catalog_simple` ASC) ,
add index `idxzz` (`sku` ASC) ;

drop table commercial_pe.campos;
drop table commercial_pe.stock_supplier;
drop table commercial_pe.stock_warehouse;
drop table commercial_pe.stock;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`sthip.blas`@`%`*/ /*!50003 PROCEDURE `rout_cliente_BMC`()
BEGIN
create table commercial_pe.sku_intervalos(
select SKUSimple,min(PaidPrice) as min,max(PaidPrice) as max,
(max(PaidPrice)-min(PaidPrice)) as rango,
(max(PaidPrice)-min(PaidPrice))/3 as amplitud,min(PaidPrice) as barato,
min(PaidPrice)+(max(PaidPrice)-min(PaidPrice))/3 as medio,
min(PaidPrice)+2*(max(PaidPrice)-min(PaidPrice))/3 as caro
from development_pe.A_Master
where OrderAfterCan=1 and year(Date)>='2013' #and SKUSimple='SK993EL83WSAPEAMZ-42357'
group by SKUSimple);

alter table commercial_pe.sku_intervalos
ADD INDEX `idxxx` (`SKUSimple` ASC) ;

create table commercial_pe.sku_b_m_c(
select A.CustomerNum,A.SKUSimple,A.PaidPrice,D.min,D.barato,D.medio,D.caro,D.max
from development_pe.A_Master A
inner join bob_live_pe.customer C on C.id_customer=A.CustomerNum
left join commercial_pe.sku_intervalos D on D.SKUSimple=A.SKUSimple
where  year(A.Date)='2013'  and OrderAfterCan='1'
and year(C.created_at)='2013');

drop table commercial_pe.clientes_sku_BMC;
create table commercial_pe.clientes_sku_BMC(
select Z.CustomerNum,
count(case when Z.PaidPrice<Z.medio then Z.SKUSimple end) as num_sku_baratos,
count(case when Z.PaidPrice>=Z.medio and Z.PaidPrice<Z.caro then Z.SKUSimple end) as num_sku_medios,
count(case when Z.PaidPrice>=Z.caro and Z.PaidPrice<=Z.max then Z.SKUSimple end) as num_sku_caros
from commercial_pe.sku_b_m_c Z
group by Z.CustomerNum);

drop table commercial_pe.sku_intervalos;
drop table commercial_pe.sku_b_m_c;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`sthip.blas`@`%`*/ /*!50003 PROCEDURE `rout_compra_venta`()
BEGIN
create table commercial_pe.sku_comprados(
SELECT B.sku,A.fk_catalog_simple, max(A.created_at) as fecha_compra 
FROM procurement_live_pe.procurement_order_item A
inner join commercial_pe.BDPet_New B on B.id_catalog_simple=A.fk_catalog_simple
where year(A.created_at)>='2013' 
group by A.fk_catalog_simple);

alter table commercial_pe.sku_comprados
ADD INDEX `id` (`fk_catalog_simple` ASC) ;

create table commercial_pe.sku_vendidos(
select SKUSimple,max(date) as datee,mid(SKUSimple,19,6) as id_catalog_simple 
from development_pe.A_Master
where OrderAfterCan='1' and year(date)>='2013'
group by SKUSimple);

alter table commercial_pe.sku_vendidos
ADD INDEX `idxxx` (`id_catalog_simple` ASC) ;

create table commercial_pe.sku_inventario(
select A.sku,A.fecha_compra,B.datee as fecha_venta from commercial_pe.sku_comprados A
left join commercial_pe.sku_vendidos B on B.id_catalog_simple=A.fk_catalog_simple);

drop table commercial_pe.sku_comprados;
drop table commercial_pe.sku_vendidos;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`daniel.h`@`%`*/ /*!50003 PROCEDURE `rout_content`()
BEGIN

DROP TABLE IF EXISTS commercial_pe.Table_ContentCOV;

create table commercial_pe.Table_ContentCOV(
SELECT
	'COL' Country,
	IFNULL(SUBSTR(amc.Cat_BP,5),"Other")   Cat_BP ,
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 4 week,'%Y%u')
			then ch.sku_config else null end) 'WV-4',
	count( distinct
			case when  date_format(ch.date,'%Y%u')=date_format(curdate()-interval 3 week,'%Y%u')
			then ch.sku_config else null end) 'WV-3',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 2 week,'%Y%u')
			then ch.sku_config else null end) 'WV2',
	count( distinct
			case when  date_format(ch.date,'%Y%u')=date_format(curdate()-interval 1 week,'%Y%u')
			then ch.sku_config else null end) 'WV-1',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate(),'%Y%u')
			then ch.sku_config else null end) 'WV-0'
FROM
	development_co_project.A_Master_Catalog AS amc
JOIN development_co_project.catalog_history AS ch ON amc.sku_config = ch.sku_config
JOIN development_co_project.catalog_visible AS cv ON ch.sku_config = cv.sku_config
where ch.visible=1 and date_format(ch.date,'%Y%u') >= date_format(curdate()-interval 4 week,'%Y%u')
GROUP BY Cat_BP) ;

DROP TABLE IF EXISTS commercial_pe.Table_ContentPEV;

create table commercial_pe.Table_ContentPEV(
SELECT
	'PER' Country,
	IFNULL(SUBSTR(amc.Cat_BP,5),"Other")   Cat_BP ,
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 4 week,'%Y%u')
			then ch.sku_config else null end) 'WV-4',
	count( distinct
			case when  date_format(ch.date,'%Y%u')=date_format(curdate()-interval 3 week,'%Y%u')
			then ch.sku_config else null end) 'WV-3',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 2 week,'%Y%u')
			then ch.sku_config else null end) 'WV2',
	count( distinct
			case when  date_format(ch.date,'%Y%u')=date_format(curdate()-interval 1 week,'%Y%u')
			then ch.sku_config else null end) 'WV-1',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate(),'%Y%u')
			then ch.sku_config else null end) 'WV-0'
FROM
	development_pe.A_Master_Catalog AS amc
JOIN development_pe.catalog_history AS ch ON amc.sku_config = ch.sku_config
JOIN development_pe.catalog_visible AS cv ON ch.sku_config = cv.sku_config
where ch.visible=1 and date_format(ch.date,'%Y%u') >= date_format(curdate()-interval 4 week,'%Y%u')
GROUP BY Cat_BP); 

DROP TABLE IF EXISTS commercial_pe.Table_ContentMXV;

create table commercial_pe.Table_ContentMXV(
SELECT
	'MEX' Country,
	IFNULL(SUBSTR(amc.Cat_BP,5),"Other")   Cat_BP ,
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 4 week,'%Y%u')
			then ch.sku_config else null end) 'WV-4',
	count( distinct
			case when  date_format(ch.date,'%Y%u')=date_format(curdate()-interval 3 week,'%Y%u')
			then ch.sku_config else null end) 'WV-3',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 2 week,'%Y%u')
			then ch.sku_config else null end) 'WV2',
	count( distinct
			case when  date_format(ch.date,'%Y%u')=date_format(curdate()-interval 1 week,'%Y%u')
			then ch.sku_config else null end) 'WV-1',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate(),'%Y%u')
			then ch.sku_config else null end) 'WV-0'
FROM
	development_mx.A_Master_Catalog AS amc
JOIN development_mx.catalog_history AS ch ON amc.sku_config = ch.sku_config
JOIN development_mx.catalog_visible AS cv ON ch.sku_config = cv.sku_config
where ch.visible=1 and date_format(ch.date,'%Y%u') >= date_format(curdate()-interval 4 week,'%Y%u')
GROUP BY Cat_BP); 

DROP TABLE IF EXISTS commercial_pe.Table_ContentVEV;

create table commercial_pe.Table_ContentVEV(
SELECT
	'VEN' Country,
	IFNULL(SUBSTR(amc.Cat_BP,5),"Other")   Cat_BP ,
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 4 week,'%Y%u')
			then ch.sku_config else null end) 'WV-4',
	count( distinct
			case when  date_format(ch.date,'%Y%u')=date_format(curdate()-interval 3 week,'%Y%u')
			then ch.sku_config else null end) 'WV-3',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 2 week,'%Y%u')
			then ch.sku_config else null end) 'WV2',
	count( distinct
			case when  date_format(ch.date,'%Y%u')=date_format(curdate()-interval 1 week,'%Y%u')
			then ch.sku_config else null end) 'WV-1',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate(),'%Y%u')
			then ch.sku_config else null end) 'WV-0'
FROM
	development_ve.A_Master_Catalog AS amc
JOIN development_ve.catalog_history AS ch ON amc.sku_config = ch.sku_config
JOIN development_ve.catalog_visible AS cv ON ch.sku_config = cv.sku_config
where ch.visible=1 and date_format(ch.date,'%Y%u') >= date_format(curdate()-interval 4 week,'%Y%u')
GROUP BY Cat_BP);

drop table if exists commercial_pe.Table_ContentV ;

create table commercial_pe.Table_ContentV(
select*from commercial_pe.Table_ContentCOV)
union all
(select*from commercial_pe.Table_ContentPEV)
union all
(select*from commercial_pe.Table_ContentMXV)
union all
(select*from commercial_pe.Table_ContentVEV);

DROP TABLE commercial_pe.Table_ContentCOV;
DROP TABLE commercial_pe.Table_ContentPEV;
DROP TABLE commercial_pe.Table_ContentMXV;
DROP TABLE commercial_pe.Table_ContentVEV;


DROP TABLE IF EXISTS commercial_pe.Table_content_NV_CO;

create table commercial_pe.Table_content_NV_CO(
SELECT
	'COL' Country,
	IFNULL(SUBSTR(amc.Cat_BP,5),"Other")   Cat_BP ,
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 4 week,'%Y%u')
			then ch.sku_config else null end) 'WV-4',
	count( distinct
			case when  date_format(ch.date,'%Y%u')=date_format(curdate()-interval 3 week,'%Y%u')
			then ch.sku_config else null end) 'WV-3',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 2 week,'%Y%u')
			then ch.sku_config else null end) 'WV2',
	count( distinct
			case when  date_format(ch.date,'%Y%u')=date_format(curdate()-interval 1 week,'%Y%u')
			then ch.sku_config else null end) 'WV-1',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate(),'%Y%u')
			then ch.sku_config else null end) 'WV-0'
FROM
	development_co_project.A_Master_Catalog AS amc
JOIN development_co_project.catalog_history AS ch ON amc.sku_config = ch.sku_config
JOIN development_co_project.catalog_visible AS cv ON ch.sku_config = cv.sku_config
where ch.visible=0 and ch.quantity<>0  and date_format(ch.date,'%Y%u') >= date_format(curdate()-interval 4 week,'%Y%u')
GROUP BY Cat_BP );

DROP TABLE IF EXISTS commercial_pe.Table_content_NV_MX;

create table commercial_pe.Table_content_NV_MX(SELECT
	'MEX' Country,
	IFNULL(SUBSTR(amc.Cat_BP,5),"Other")   Cat_BP ,
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 4 week,'%Y%u')
			then ch.sku_config else null end) 'WV-4',
	count( distinct
			case when  date_format(ch.date,'%Y%u')=date_format(curdate()-interval 3 week,'%Y%u')
			then ch.sku_config else null end) 'WV-3',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 2 week,'%Y%u')
			then ch.sku_config else null end) 'WV2',
	count( distinct
			case when  date_format(ch.date,'%Y%u')=date_format(curdate()-interval 1 week,'%Y%u')
			then ch.sku_config else null end) 'WV-1',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate(),'%Y%u')
			then ch.sku_config else null end) 'WV-0'
FROM
	development_mx.A_Master_Catalog AS amc
JOIN development_mx.catalog_history AS ch ON amc.sku_config = ch.sku_config
JOIN development_mx.catalog_visible AS cv ON ch.sku_config = cv.sku_config
where ch.visible=0 and ch.quantity<>0  and date_format(ch.date,'%Y%u') >= date_format(curdate()-interval 4 week,'%Y%u')
GROUP BY Cat_BP );

DROP TABLE IF EXISTS commercial_pe.Table_content_NV_PE;

create table commercial_pe.Table_content_NV_PE(SELECT
	'PER' Country,
	IFNULL(SUBSTR(amc.Cat_BP,5),"Other")   Cat_BP ,
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 4 week,'%Y%u')
			then ch.sku_config else null end) 'WV-4',
	count( distinct
			case when  date_format(ch.date,'%Y%u')=date_format(curdate()-interval 3 week,'%Y%u')
			then ch.sku_config else null end) 'WV-3',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 2 week,'%Y%u')
			then ch.sku_config else null end) 'WV2',
	count( distinct
			case when  date_format(ch.date,'%Y%u')=date_format(curdate()-interval 1 week,'%Y%u')
			then ch.sku_config else null end) 'WV-1',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate(),'%Y%u')
			then ch.sku_config else null end) 'WV-0'
FROM
	development_pe.A_Master_Catalog AS amc
JOIN development_pe.catalog_history AS ch ON amc.sku_config = ch.sku_config
JOIN development_pe.catalog_visible AS cv ON ch.sku_config = cv.sku_config
where ch.visible=0 and ch.quantity<>0  and date_format(ch.date,'%Y%u') >= date_format(curdate()-interval 4 week,'%Y%u')
GROUP BY Cat_BP );

DROP TABLE IF EXISTS commercial_pe.Table_content_NV_VE;

create table commercial_pe.Table_content_NV_VE(SELECT
	'VEN' Country,
	IFNULL(SUBSTR(amc.Cat_BP,5),"Other")   Cat_BP ,
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 4 week,'%Y%u')
			then ch.sku_config else null end) 'WV-4',
	count( distinct
			case when  date_format(ch.date,'%Y%u')=date_format(curdate()-interval 3 week,'%Y%u')
			then ch.sku_config else null end) 'WV-3',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 2 week,'%Y%u')
			then ch.sku_config else null end) 'WV2',
	count( distinct
			case when  date_format(ch.date,'%Y%u')=date_format(curdate()-interval 1 week,'%Y%u')
			then ch.sku_config else null end) 'WV-1',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate(),'%Y%u')
			then ch.sku_config else null end) 'WV-0'
FROM
	development_ve.A_Master_Catalog AS amc
JOIN development_ve.catalog_history AS ch ON amc.sku_config = ch.sku_config
JOIN development_ve.catalog_visible AS cv ON ch.sku_config = cv.sku_config
where ch.visible=0 and ch.quantity<>0  and date_format(ch.date,'%Y%u') >= date_format(curdate()-interval 4 week,'%Y%u')
GROUP BY Cat_BP );

drop table if exists commercial_pe.Table_content_NV ;

create table commercial_pe.Table_content_NV(
select*from commercial_pe.Table_content_NV_CO)
union all
(select*from commercial_pe.Table_content_NV_PE)
union all
(select*from commercial_pe.Table_content_NV_MX)
union all
(select*from commercial_pe.Table_content_NV_VE);

DROP TABLE commercial_pe.Table_content_NV_CO;
DROP TABLE commercial_pe.Table_content_NV_PE;

DROP TABLE commercial_pe.Table_content_NV_MX;
DROP TABLE commercial_pe.Table_content_NV_VE;

DROP TABLE commercial_pe.Table_content_FC_CO;

create table commercial_pe.Table_content_FC_CO(
SELECT
	'COL' Country,
	IFNULL(SUBSTR(amc.Cat_BP,5),"Other")   Cat_BP ,
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 4 week,'%Y%u')
			then ch.sku_config else null end) 'W-4',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 3 week,'%Y%u')
			then ch.sku_config else null end) 'W-3',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 2 week,'%Y%u')
			then ch.sku_config else null end) 'W-2',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 1 week,'%Y%u')
			then ch.sku_config else null end) 'WV-1',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate(),'%Y%u')
			then ch.sku_config else null end) 'W-0'
FROM
	development_co_project.A_Master_Catalog AS amc
JOIN development_co_project.catalog_history AS ch ON amc.sku_config = ch.sku_config
JOIN development_co_project.catalog_visible AS cv ON ch.sku_config = cv.sku_config
where ch.quantity<>0 and cv.pet_status<>"creation,edited,images" and ch.status_config<>"deleted" 
and date_format(ch.date,'%Y%u') >= date_format(curdate()-interval 4 week,'%Y%u')
GROUP BY Cat_BP);


DROP TABLE IF EXISTS commercial_pe.Table_content_FC_PE;
create table commercial_pe.Table_content_FC_PE(SELECT
	'PER' Country,
	IFNULL(SUBSTR(amc.Cat_BP,5),"Other")   Cat_BP ,
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 4 week,'%Y%u')
			then ch.sku_config else null end) 'W-4',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 3 week,'%Y%u')
			then ch.sku_config else null end) 'W-3',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 2 week,'%Y%u')
			then ch.sku_config else null end) 'W-2',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 1 week,'%Y%u')
			then ch.sku_config else null end) 'WV-1',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate(),'%Y%u')
			then ch.sku_config else null end) 'W-0'
FROM
	development_pe.A_Master_Catalog AS amc
JOIN development_pe.catalog_history AS ch ON amc.sku_config = ch.sku_config
JOIN development_pe.catalog_visible AS cv ON ch.sku_config = cv.sku_config
where ch.quantity<>0 and cv.pet_status<>"creation,edited,images" and ch.status_config<>"deleted" 
and date_format(ch.date,'%Y%u') >= date_format(curdate()-interval 4 week,'%Y%u')
GROUP BY Cat_BP);

DROP TABLE IF EXISTS commercial_pe.Table_content_FC_MX;

create table commercial_pe.Table_content_FC_MX(SELECT
	'MEX' Country,
	IFNULL(SUBSTR(amc.Cat_BP,5),"Other")   Cat_BP ,
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 4 week,'%Y%u')
			then ch.sku_config else null end) 'W-4',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 3 week,'%Y%u')
			then ch.sku_config else null end) 'W-3',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 2 week,'%Y%u')
			then ch.sku_config else null end) 'W-2',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 1 week,'%Y%u')
			then ch.sku_config else null end) 'WV-1',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate(),'%Y%u')
			then ch.sku_config else null end) 'W-0'
FROM
	development_mx.A_Master_Catalog AS amc
JOIN development_mx.catalog_history AS ch ON amc.sku_config = ch.sku_config
JOIN development_mx.catalog_visible AS cv ON ch.sku_config = cv.sku_config
where ch.quantity<>0 and cv.pet_status<>"creation,edited,images" and ch.status_config<>"deleted" 
and date_format(ch.date,'%Y%u') >= date_format(curdate()-interval 4 week,'%Y%u')
GROUP BY Cat_BP); 



DROP TABLE IF EXISTS commercial_pe.Table_content_FC_VE;

create table commercial_pe.Table_content_FC_VE(SELECT
	'VEN' Country,
	IFNULL(SUBSTR(amc.Cat_BP,5),"Other")   Cat_BP ,
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 4 week,'%Y%u')
			then ch.sku_config else null end) 'W-4',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 3 week,'%Y%u')
			then ch.sku_config else null end) 'W-3',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 2 week,'%Y%u')
			then ch.sku_config else null end) 'W-2',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 1 week,'%Y%u')
			then ch.sku_config else null end) 'WV-1',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate(),'%Y%u')
			then ch.sku_config else null end) 'W-0'
FROM
	development_ve.A_Master_Catalog AS amc
JOIN development_ve.catalog_history AS ch ON amc.sku_config = ch.sku_config
JOIN development_ve.catalog_visible AS cv ON ch.sku_config = cv.sku_config
where ch.quantity<>0 and cv.pet_status<>"creation,edited,images" and ch.status_config<>"deleted" 
and date_format(ch.date,'%Y%u') >= date_format(curdate()-interval 4 week,'%Y%u')
GROUP BY Cat_BP);

drop table if exists commercial_pe.Table_content_FC;

 CREATE TABLE commercial_pe.Table_content_FC(

 select*from commercial_pe.Table_content_FC_CO)
union all
(select*from commercial_pe.Table_content_FC_MX)
union all
(select*from commercial_pe.Table_content_FC_PE)
union all
(select*from commercial_pe.Table_content_FC_VE);

drop table commercial_pe.Table_content_FC_CO;
drop table commercial_pe.Table_content_FC_MX;
drop table commercial_pe.Table_content_FC_PE;
drop table commercial_pe.Table_content_FC_VE;

DROP TABLE IF EXISTS commercial_pe.Table_content_NVFC_CO;

create table commercial_pe.Table_content_NVFC_CO(
SELECT
	'COL' Country,
	IFNULL(SUBSTR(amc.Cat_BP,5),"Other")   Cat_BP ,
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 4 week,'%Y%u')
			then ch.sku_config else null end) 'W-4',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 3 week,'%Y%u')
			then ch.sku_config else null end) 'W-3',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 2 week,'%Y%u')
			then ch.sku_config else null end) 'W-2',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 1 week,'%Y%u')
			then ch.sku_config else null end) 'WV-1',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate(),'%Y%u')
			then ch.sku_config else null end) 'W-0'
FROM
	development_co_project.A_Master_Catalog AS amc
JOIN development_co_project.catalog_history AS ch ON amc.sku_config = ch.sku_config
JOIN development_co_project.catalog_visible AS cv ON ch.sku_config = cv.sku_config
where ch.visible=0 and ch.quantity<>0 and cv.pet_status<>"creation,edited,images" and ch.status_config<>"deleted" 
and date_format(ch.date,'%Y%u') >= date_format(curdate()-interval 4 week,'%Y%u')
GROUP BY Cat_BP);

DROP TABLE IF EXISTS commercial_pe.Table_content_NVFC_PE;

CREATE TABLE commercial_pe.Table_content_NVFC_PE(
SELECT
	'PER' Country,
	IFNULL(SUBSTR(amc.Cat_BP,5),"Other")   Cat_BP ,
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 4 week,'%Y%u')
			then ch.sku_config else null end) 'W-4',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 3 week,'%Y%u')
			then ch.sku_config else null end) 'W-3',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 2 week,'%Y%u')
			then ch.sku_config else null end) 'W-2',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 1 week,'%Y%u')
			then ch.sku_config else null end) 'WV-1',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate(),'%Y%u')
			then ch.sku_config else null end) 'W-0'
FROM
	development_pe.A_Master_Catalog AS amc
JOIN development_pe.catalog_history AS ch ON amc.sku_config = ch.sku_config
JOIN development_pe.catalog_visible AS cv ON ch.sku_config = cv.sku_config
where ch.visible=0 and ch.quantity<>0 and cv.pet_status<>"creation,edited,images" and ch.status_config<>"deleted" 
and date_format(ch.date,'%Y%u') >= date_format(curdate()-interval 4 week,'%Y%u')
GROUP BY Cat_BP );

DROP TABLE IF EXISTS commercial_pe.Table_content_NVFC_MX;

CREATE TABLE commercial_pe.Table_content_NVFC_MX(
SELECT
	'MEX' Country,
	IFNULL(SUBSTR(amc.Cat_BP,5),"Other")   Cat_BP ,
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 4 week,'%Y%u')
			then ch.sku_config else null end) 'W-4',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 3 week,'%Y%u')
			then ch.sku_config else null end) 'W-3',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 2 week,'%Y%u')
			then ch.sku_config else null end) 'W-2',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 1 week,'%Y%u')
			then ch.sku_config else null end) 'WV-1',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate(),'%Y%u')
			then ch.sku_config else null end) 'W-0'
FROM
	development_mx.A_Master_Catalog AS amc
JOIN development_mx.catalog_history AS ch ON amc.sku_config = ch.sku_config
JOIN development_mx.catalog_visible AS cv ON ch.sku_config = cv.sku_config
where ch.visible=0 and ch.quantity<>0 and cv.pet_status<>"creation,edited,images" and ch.status_config<>"deleted" 
and date_format(ch.date,'%Y%u') >= date_format(curdate()-interval 4 week,'%Y%u')
GROUP BY Cat_BP );

DROP TABLE IF EXISTS commercial_pe.Table_content_NVFC_VE;

CREATE TABLE commercial_pe.Table_content_NVFC_VE(

SELECT
	'VEN' Country,
	IFNULL(SUBSTR(amc.Cat_BP,5),"Other")   Cat_BP ,
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 4 week,'%Y%u')
			then ch.sku_config else null end) 'W-4',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 3 week,'%Y%u')
			then ch.sku_config else null end) 'W-3',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 2 week,'%Y%u')
			then ch.sku_config else null end) 'W-2',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate()-interval 1 week,'%Y%u')
			then ch.sku_config else null end) 'WV-1',
	count( distinct
			case when date_format(ch.date,'%Y%u')=date_format(curdate(),'%Y%u')
			then ch.sku_config else null end) 'W-0'
FROM
	development_ve.A_Master_Catalog AS amc
JOIN development_ve.catalog_history AS ch ON amc.sku_config = ch.sku_config
JOIN development_ve.catalog_visible AS cv ON ch.sku_config = cv.sku_config
where ch.visible=0 and ch.quantity<>0 and cv.pet_status<>"creation,edited,images" and ch.status_config<>"deleted" 
and date_format(ch.date,'%Y%u') >= date_format(curdate()-interval 4 week,'%Y%u')
GROUP BY Cat_BP );


DROP TABLE IF EXISTS commercial_pe.Table_content_NVFC;

CREATE TABLE commercial_pe.Table_content_NVFC(

 select*from commercial_pe.Table_content_NVFC_CO)
union all
(select*from commercial_pe.Table_content_NVFC_MX)
union all
(select*from commercial_pe.Table_content_NVFC_PE)
union all
(select*from commercial_pe.Table_content_NVFC_VE);

DROP TABLE commercial_pe.Table_content_NVFC_CO;
DROP TABLE commercial_pe.Table_content_NVFC_MX;
DROP TABLE commercial_pe.Table_content_NVFC_PE;
DROP TABLE commercial_pe.Table_content_NVFC_VE;



drop table if exists commercial_pe.Table_skus_Creados;

create table commercial_pe.Table_skus_Creados(
SELECT
'COL' Country,
IFNULL(SUBSTR(amc.Cat_BP,5),"Other") as Cat_BP,
count( distinct
case when date_format(cc.created_at,'%Y%m')=date_format(curdate()- interval 4 month,'%Y%m')
	then cc.sku else null end) 'M-4',
count( distinct
case when date_format(cc.created_at,'%Y%m')=date_format(curdate()- interval 3 month,'%Y%m')
	then cc.sku else null end) 'M-3',
count( distinct
case when date_format(cc.created_at,'%Y%m')=date_format(curdate()- interval 2 month,'%Y%m')
	then cc.sku else null end) 'M-2',
count( distinct
case when date_format(cc.created_at,'%Y%m')=date_format(curdate()- interval 1 month,'%Y%m')
	then cc.sku else null end) 'M-1',
count( distinct
case when date_format(cc.created_at,'%Y%m')=date_format(curdate(),'%Y%m')
	then cc.sku else null end) 'M-0',
count( distinct
case when date_format(cc.created_at,'%Y%u')=date_format(curdate()-interval 4 week,'%Y%u')
	then cc.sku else null end) 'W-4',
count( distinct
case when date_format(cc.created_at,'%Y%u')=date_format(curdate()-interval 3 week,'%Y%u')
	then cc.sku else null end) 'W-3',
count( distinct
case when date_format(cc.created_at,'%Y%u')=date_format(curdate()-interval 2 week,'%Y%u')
	then cc.sku else null end) 'W-2',
count( distinct
case when date_format(cc.created_at,'%Y%u')=date_format(curdate()-interval 1 week,'%Y%u')
	then cc.sku else null end) 'W-1',
count( distinct
case when date_format(cc.created_at,'%Y%u')=date_format(curdate(),'%Y%u')
	then cc.sku else null end) 'W-0'
FROM
bob_live_co.catalog_config as cc
LEFT JOIN
development_co_project.A_Master_Catalog as amc on cc.id_catalog_config = amc.id_catalog_config
where date_format(cc.created_at,'%Y%m') >= date_format(curdate()-interval 4 month,'%Y%m')
and cc.pet_status in ('creation','creation,edited','creation,edited,images','creation,images')
group by Cat_BP)
union all
(SELECT
'PER' Country,
IFNULL(SUBSTR(amc.Cat_BP,5),"Other") as Cat_BP,
count( distinct
case when date_format(cc.created_at,'%Y%m')=date_format(curdate()- interval 4 month,'%Y%m')
	then cc.sku else null end) 'M-4',
count( distinct
case when date_format(cc.created_at,'%Y%m')=date_format(curdate()- interval 3 month,'%Y%m')
	then cc.sku else null end) 'M-3',
count( distinct
case when date_format(cc.created_at,'%Y%m')=date_format(curdate()- interval 2 month,'%Y%m')
	then cc.sku else null end) 'M-2',
count( distinct
case when date_format(cc.created_at,'%Y%m')=date_format(curdate()- interval 1 month,'%Y%m')
	then cc.sku else null end) 'M-1',
count( distinct
case when date_format(cc.created_at,'%Y%m')=date_format(curdate(),'%Y%m')
	then cc.sku else null end) 'M-0',
count( distinct
case when date_format(cc.created_at,'%Y%u')=date_format(curdate()-interval 4 week,'%Y%u')
	then cc.sku else null end) 'W-4',
count( distinct
case when date_format(cc.created_at,'%Y%u')=date_format(curdate()-interval 3 week,'%Y%u')
	then cc.sku else null end) 'W-3',
count( distinct
case when date_format(cc.created_at,'%Y%u')=date_format(curdate()-interval 2 week,'%Y%u')
	then cc.sku else null end) 'W-2',
count( distinct
case when date_format(cc.created_at,'%Y%u')=date_format(curdate()-interval 1 week,'%Y%u')
	then cc.sku else null end) 'W-1',
count( distinct
case when date_format(cc.created_at,'%Y%u')=date_format(curdate(),'%Y%u')
	then cc.sku else null end) 'W-0'
FROM
bob_live_pe.catalog_config as cc
LEFT JOIN
development_pe.A_Master_Catalog as amc on cc.id_catalog_config = amc.id_catalog_config
where date_format(cc.created_at,'%Y%m') >= date_format(curdate()-interval 4 month,'%Y%m')
and cc.pet_status in ('creation','creation,edited','creation,edited,images','creation,images')
group by Cat_BP)
union all

(SELECT
'MEX' Country,
IFNULL(SUBSTR(amc.Cat_BP,5),"Other") as Cat_BP,
count( distinct
case when date_format(cc.created_at,'%Y%m')=date_format(curdate()- interval 4 month,'%Y%m')
	then cc.sku else null end) 'M-4',
count( distinct
case when date_format(cc.created_at,'%Y%m')=date_format(curdate()- interval 3 month,'%Y%m')
	then cc.sku else null end) 'M-3',
count( distinct
case when date_format(cc.created_at,'%Y%m')=date_format(curdate()- interval 2 month,'%Y%m')
	then cc.sku else null end) 'M-2',
count( distinct
case when date_format(cc.created_at,'%Y%m')=date_format(curdate()- interval 1 month,'%Y%m')
	then cc.sku else null end) 'M-1',
count( distinct
case when date_format(cc.created_at,'%Y%m')=date_format(curdate(),'%Y%m')
	then cc.sku else null end) 'M-0',
count( distinct
case when date_format(cc.created_at,'%Y%u')=date_format(curdate()-interval 4 week,'%Y%u')
	then cc.sku else null end) 'W-4',
count( distinct
case when date_format(cc.created_at,'%Y%u')=date_format(curdate()-interval 3 week,'%Y%u')
	then cc.sku else null end) 'W-3',
count( distinct
case when date_format(cc.created_at,'%Y%u')=date_format(curdate()-interval 2 week,'%Y%u')
	then cc.sku else null end) 'W-2',
count( distinct
case when date_format(cc.created_at,'%Y%u')=date_format(curdate()-interval 1 week,'%Y%u')
	then cc.sku else null end) 'W-1',
count( distinct
case when date_format(cc.created_at,'%Y%u')=date_format(curdate(),'%Y%u')
	then cc.sku else null end) 'W-0'
FROM
bob_live_mx.catalog_config as cc
LEFT JOIN
development_mx.A_Master_Catalog as amc on cc.id_catalog_config = amc.id_catalog_config
where date_format(cc.created_at,'%Y%m') >= date_format(curdate()-interval 4 month,'%Y%m')
and cc.pet_status in ('creation','creation,edited','creation,edited,images','creation,images')
group by Cat_BP)
union all
(
SELECT
'VEN' Country,
IFNULL(SUBSTR(amc.Cat_BP,5),"Other") as Cat_BP,
count( distinct
case when date_format(cc.created_at,'%Y%m')=date_format(curdate()- interval 4 month,'%Y%m')
	then cc.sku else null end) 'M-4',
count( distinct
case when date_format(cc.created_at,'%Y%m')=date_format(curdate()- interval 3 month,'%Y%m')
	then cc.sku else null end) 'M-3',
count( distinct
case when date_format(cc.created_at,'%Y%m')=date_format(curdate()- interval 2 month,'%Y%m')
	then cc.sku else null end) 'M-2',
count( distinct
case when date_format(cc.created_at,'%Y%m')=date_format(curdate()- interval 1 month,'%Y%m')
	then cc.sku else null end) 'M-1',
count( distinct
case when date_format(cc.created_at,'%Y%m')=date_format(curdate(),'%Y%m')
	then cc.sku else null end) 'M-0',
count( distinct
case when date_format(cc.created_at,'%Y%u')=date_format(curdate()-interval 4 week,'%Y%u')
	then cc.sku else null end) 'W-4',
count( distinct
case when date_format(cc.created_at,'%Y%u')=date_format(curdate()-interval 3 week,'%Y%u')
	then cc.sku else null end) 'W-3',
count( distinct
case when date_format(cc.created_at,'%Y%u')=date_format(curdate()-interval 2 week,'%Y%u')
	then cc.sku else null end) 'W-2',
count( distinct
case when date_format(cc.created_at,'%Y%u')=date_format(curdate()-interval 1 week,'%Y%u')
	then cc.sku else null end) 'W-1',
count( distinct
case when date_format(cc.created_at,'%Y%u')=date_format(curdate(),'%Y%u')
	then cc.sku else null end) 'W-0'
FROM
bob_live_ve.catalog_config as cc
LEFT JOIN
development_ve.A_Master_Catalog as amc on cc.id_catalog_config = amc.id_catalog_config
where date_format(cc.created_at,'%Y%m') >= date_format(curdate()-interval 4 month,'%Y%m')
and cc.pet_status in ('creation','creation,edited','creation,edited,images','creation,images')
group by Cat_BP);

DROP TABLE IF EXISTS commercial_pe.CONTENT_REPORT;

create table commercial_pe.CONTENT_REPORT(
select A.*,E.`M-4`,E.`M-3`,E.`M-2`,E.`M-1`,E.`M-0`,
E.`W-4`,E.`W-3`,E.`W-2`,E.`W-1`,E.`W-0` ,B.`WV-0` NV_0,B.`WV-1` NV_1,B.`WV2` NV_2,B.`WV-3` NV_3,B.`WV-4` NV_4,
C.`W-0` FC_O,C.`WV-1` FC_1,C.`W-2` FC_2,
C.`W-3` FC_3,C.`W-4` FC_4,D.`W-0` NVFC_0,D.`WV-1` NVFC_1,D.`W-2` NVFC_2,D.`W-3` NVFC_3,D.`W-4` NVFC_4  
from commercial_pe.Table_ContentV A
left join commercial_pe.Table_content_NV B on
A.Country=B.Country and A.Cat_BP=B.Cat_BP
left join commercial_pe.Table_content_FC C on
A.Country=C.Country and A.Cat_BP=C.Cat_BP
left join commercial_pe.Table_content_NVFC D on
A.Country=D.Country and A.Cat_BP=D.Cat_BP
left join commercial_pe.Table_skus_Creados E on
E.Country=A.Country and A.Cat_BP=E.Cat_BP );

DROP TABLE commercial_pe.Table_ContentV;
DROP TABLE commercial_pe.Table_content_NV;
DROP TABLE commercial_pe.Table_content_FC;
DROP TABLE commercial_pe.Table_content_NVFC;
DROP TABLE commercial_pe.Table_skus_Creados;

DROP TABLE IF EXISTS commercial_pe.ContentFI;

create table commercial_pe.ContentFI(
SELECT
'COL' Country,
cc.created_by_config,
cc.pet_status,
cc.`status`,
EXTRACT(YEAR_MONTH FROM cc.created_at) as month_created,
DATE(cc.created_at) as date_created,
YEARWEEK(cc.created_at,3) as week_created,
cc.sku as sku_config,
amc.Supplier,
IFNULL(SUBSTR(amc.Cat_BP,5),"(en blanco)") as Cat_BP
FROM
bob_live_co.catalog_config as cc
LEFT JOIN
development_co_project.A_Master_Catalog as amc on cc.id_catalog_config = amc.id_catalog_config
group by cc.id_catalog_config) union all
(
SELECT
'MEX' Country,
cc.created_by_config,
cc.pet_status,
cc.`status`,
EXTRACT(YEAR_MONTH FROM cc.created_at) as month_created,
DATE(cc.created_at) as date_created,
YEARWEEK(cc.created_at,3) as week_created,
cc.sku as sku_config,
amc.Supplier,
IFNULL(SUBSTR(amc.Cat_BP,5),"(en blanco)") as Cat_BP
FROM
bob_live_mx.catalog_config as cc
LEFT JOIN
development_mx.A_Master_Catalog as amc on cc.id_catalog_config = amc.id_catalog_config
group by cc.id_catalog_config) union all
(
SELECT
'PER' Country,
cc.created_by_config,
cc.pet_status,
cc.`status`,
EXTRACT(YEAR_MONTH FROM cc.created_at) as month_created,
DATE(cc.created_at) as date_created,
YEARWEEK(cc.created_at,3) as week_created,
cc.sku as sku_config,
amc.Supplier,
IFNULL(SUBSTR(amc.Cat_BP,5),"(en blanco)") as Cat_BP
FROM
bob_live_pe.catalog_config as cc
LEFT JOIN
development_pe.A_Master_Catalog as amc on cc.id_catalog_config = amc.id_catalog_config
group by cc.id_catalog_config) union all
(
SELECT
'VEN' Country,
cc.created_by_config,
cc.pet_status,
cc.`status`,
EXTRACT(YEAR_MONTH FROM cc.created_at) as month_created,
DATE(cc.created_at) as date_created,
YEARWEEK(cc.created_at,3) as week_created,
cc.sku as sku_config,
amc.Supplier,
IFNULL(SUBSTR(amc.Cat_BP,5),"(en blanco)") as Cat_BP
FROM
bob_live_ve.catalog_config as cc
LEFT JOIN
development_ve.A_Master_Catalog as amc on cc.id_catalog_config = amc.id_catalog_config
group by cc.id_catalog_config)
;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`daniel.h`@`%`*/ /*!50003 PROCEDURE `rout_FashionReport`()
BEGIN

drop table commercial_pe.A_Master;

create table commercial_pe.A_Master
select
*
from test_linio.A_Master A
where
A.Cat1='Fashion' and A.OrderAfterCan=1
;

ALTER TABLE `commercial_pe`.`A_Master` 
ADD COLUMN `RevM2` DOUBLE NOT NULL   , 
ADD COLUMN `RevM1` DOUBLE NOT NULL   , 
ADD COLUMN `RevMTD` DOUBLE NOT NULL  , 
ADD COLUMN `PC1M2` DOUBLE NOT NULL   , 
ADD COLUMN `PC1M1` DOUBLE NOT NULL   , 
ADD COLUMN `PC1MMTD` DOUBLE NOT NULL , 
ADD COLUMN `RevW1` DOUBLE NOT NULL   , 
ADD COLUMN `W1OW5` DOUBLE NOT NULL   , 
ADD COLUMN `RevW2` DOUBLE NOT NULL   , 
ADD COLUMN `RevW3` DOUBLE NOT NULL   , 
ADD COLUMN `RevW4` DOUBLE NOT NULL   , 
ADD COLUMN `PC1W1` DOUBLE NOT NULL   , 
ADD COLUMN `PC1W2` DOUBLE NOT NULL   , 
ADD COLUMN `PC1W3` DOUBLE NOT NULL   , 
ADD COLUMN `PC1W4` DOUBLE NOT NULL   , 
ADD COLUMN `STRW1` DOUBLE NOT NULL   , 
ADD COLUMN `STRW2` DOUBLE NOT NULL   , 
ADD COLUMN `STRW3` DOUBLE NOT NULL   , 
ADD COLUMN `STRW4` DOUBLE NOT NULL   ,
ADD COLUMN `RevW5` DOUBLE NOT NULL   

; 

update  commercial_pe.A_Master A
set RevM2= if( A.MonthNum >= DATE_FORMAT(date_sub(curdate()- interval day(curdate()) day, interval 1 month ),'%Y%m') , A.Rev, 0);
/*
update  commercial_pe.A_Master A
set RevM1= if( A.Date>=(now() - INTERVAL 1 MONTH) , A.Rev, 0);
*/

update  commercial_pe.A_Master A
set RevM1= if( A.MonthNum >= DATE_FORMAT(date_sub(curdate(), interval day(curdate()) day  ),'%Y%m') , A.Rev, 0);

update  commercial_pe.A_Master A
set RevMTD= if( Month(A.Date) = month(now()) , A.Rev, 0);

update  commercial_pe.A_Master A
set PC1M2= if( A.MonthNum >= DATE_FORMAT(date_sub(curdate()- interval day(curdate()) day, interval 1 month ),'%Y%m') , A.PCOne, 0);

update  commercial_pe.A_Master A
set PC1M1= if( A.MonthNum >= DATE_FORMAT(date_sub(curdate(), interval day(curdate()) day  ),'%Y%m') , A.PCOne, 0);

update  commercial_pe.A_Master A
set PC1MMTD= if( Month(A.Date) = month(now()) , A.PCOne, 0);

update  commercial_pe.A_Master A
set RevW1= if( yearweek(A.Date) = yearweek( ( curdate() - INTERVAL 1 WEEK - interval 1 day  ) ), A.Rev, 0);

update  commercial_pe.A_Master A
set RevW2= if( yearweek(A.Date) = yearweek( ( curdate() - INTERVAL 2 WEEK - interval 1 day  ) ), A.Rev, 0);


update  commercial_pe.A_Master A
set RevW3= if( yearweek(A.Date) = yearweek( ( curdate() - INTERVAL 3 WEEK - interval 1 day  ) ), A.Rev, 0);

update  commercial_pe.A_Master A
set RevW4= if( yearweek(A.Date) = yearweek( ( curdate() - INTERVAL 4 WEEK - interval 1 day  ) ), A.Rev, 0);

update  commercial_pe.A_Master A
set RevW5= if( yearweek(A.Date) = yearweek( ( curdate() - INTERVAL 5 WEEK - interval 1 day  ) ), A.Rev, 0);

update  commercial_pe.A_Master A
set PC1W1= if( yearweek(A.Date) = yearweek( ( curdate() - INTERVAL 1 WEEK - interval 1 day  ) ), A.PCOne, 0);

update  commercial_pe.A_Master A
set PC1W2= if( yearweek(A.Date) = yearweek( ( curdate() - INTERVAL 2 WEEK - interval 1 day  ) ), A.PCOne, 0);


update  commercial_pe.A_Master A
set PC1W3= if( yearweek(A.Date) = yearweek( ( curdate() - INTERVAL 3 WEEK - interval 1 day  ) ), A.PCOne, 0);

update  commercial_pe.A_Master A
set PC1W4= if( yearweek(A.Date) = yearweek( ( curdate() - INTERVAL 4 WEEK - interval 1 day  ) ), A.PCOne, 0);



/*

select
yearweek((max(A.Date) - INTERVAL 1 WEEK - interval 1 day ))
from development_pe.A_Master A;

select
yearweek(('2014-02-16' - INTERVAL 1 WEEK -1 ));

select
yearweek(('2014-02-10'- INTERVAL 1 WEEK));


select
yearweek(max(A.Date ))
from development_pe.A_Master A;
*/

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`daniel.h`@`%`*/ /*!50003 PROCEDURE `rout_FashionStock`()
BEGIN

drop table if exists commercial_pe.F_R_FashionStock;
create table commercial_pe.F_R_FashionStock (select 'PER' Country,
    A.id_catalog_simple,A.sku_simple,
    A.stock_sku,
    B.avg_sales_week,
    case
        when B.avg_sales_week = 0 then A.stock_sku
        else A.stock_sku / B.avg_sales_week
    end stock_covert_week,
    A.Inventory_Value_at_Retail,
    A.stock_supplier,
    A.Inventory_cogs from
    (select 
        A.sku sku_simple,
            C.quantity stock_sku,
            C.quantity * A.price / 2.7 Inventory_Value_at_Retail,
            E.quantity stock_supplier,
            A.cost * C.quantity/2.7 Inventory_cogs,A.id_catalog_simple
    from
        bob_live_pe.catalog_simple A
    left join bob_live_pe.catalog_source B ON A.id_catalog_simple = B.fk_catalog_simple
    left join bob_live_pe.catalog_stock C ON C.fk_catalog_source = B.id_catalog_source
    left join development_pe.A_Master_Catalog D ON D.id_catalog_simple = A.id_catalog_simple
    left join bob_live_pe.catalog_supplier_stock E ON E.fk_catalog_simple = A.id_catalog_simple
    where
        D.Cat_BP like '%Fashion%') A
        left join
    (select 
        A.SKUSimple sku_simple,
            avg(A.unidades_vendidas) avg_sales_week
    from
        (select 
        date_format(Date, '%Y-%u') week,
            A.SKUSimple,
            count(*) unidades_vendidas
    from
        development_pe.A_Master A
    where
        OrderAfterCan = 1
            and CatBP like '%Fashion%'
            and date_format(Date, '%Y-%m') = date_format(curdate(), '%Y-%m')
    group by week , SKUSimple) A
    group by A.SKUSimple) B ON A.sku_simple = B.sku_simple) union all (select 
    'COL' Country,A.id_catalog_simple,
    A.sku_simple,
    A.stock_sku,
    B.avg_sales_week,
    case
        when B.avg_sales_week = 0 then A.stock_sku
        else A.stock_sku / B.avg_sales_week
    end stock_covert_week,
    A.Inventory_Value_at_Retail,
    A.stock_supplier,
    A.Inventory_cogs
from
    (select 
        A.sku sku_simple,
            C.quantity stock_sku,
            C.quantity * A.price /1900 Inventory_Value_at_Retail,
            E.quantity stock_supplier,
            A.cost * C.quantity/1900 Inventory_cogs,A.id_catalog_simple
    from
        bob_live_co.catalog_simple A
    left join bob_live_co.catalog_source B ON A.id_catalog_simple = B.fk_catalog_simple
    left join bob_live_co.catalog_stock C ON C.fk_catalog_source = B.id_catalog_source
    left join development_co_project.A_Master_Catalog D ON D.id_catalog_simple = A.id_catalog_simple
    left join bob_live_co.catalog_supplier_stock E ON E.fk_catalog_simple = A.id_catalog_simple
    where
        D.Cat_BP like '%Fashion%') A
        left join
    (select 
        A.SKUSimple sku_simple,
            avg(A.unidades_vendidas) avg_sales_week
    from
        (select 
        date_format(Date, '%Y-%u') week,
            A.SKUSimple,
            count(*) unidades_vendidas
    from
        development_co_project.A_Master A
    where
        OrderAfterCan = 1
            and CatBP like '%Fashion%'
            and date_format(Date, '%Y-%m') = date_format(curdate(), '%Y-%m')
    group by week , SKUSimple) A
    group by A.SKUSimple) B ON A.sku_simple = B.sku_simple) union all (select 
    'VEN' Country,A.id_catalog_simple,
    A.sku_simple,
    A.stock_sku,
    B.avg_sales_week,
    case
        when B.avg_sales_week = 0 then A.stock_sku
        else A.stock_sku / B.avg_sales_week
    end stock_covert_week,
    A.Inventory_Value_at_Retail,
    A.stock_supplier,
    A.Inventory_cogs
from
    (select 
        A.sku sku_simple,
            C.quantity stock_sku,
            C.quantity * A.price /54 Inventory_Value_at_Retail,
            E.quantity stock_supplier,
            A.cost * C.quantity/54 Inventory_cogs,A.id_catalog_simple
    from
        bob_live_ve.catalog_simple A
    left join bob_live_ve.catalog_source B ON A.id_catalog_simple = B.fk_catalog_simple
    left join bob_live_ve.catalog_stock C ON C.fk_catalog_source = B.id_catalog_source
    left join development_ve.A_Master_Catalog D ON D.id_catalog_simple = A.id_catalog_simple
    left join bob_live_ve.catalog_supplier_stock E ON E.fk_catalog_simple = A.id_catalog_simple
    where
        D.Cat_BP like '%Fashion%') A
        left join
    (select 
        A.SKUSimple sku_simple,
            avg(A.unidades_vendidas) avg_sales_week
    from
        (select 
        date_format(Date, '%Y-%u') week,
            A.SKUSimple,
            count(*) unidades_vendidas
    from
        development_ve.A_Master A
    where
        OrderAfterCan = 1
            and CatBP like '%Fashion%'
            and date_format(Date, '%Y-%m') = date_format(curdate(), '%Y-%m')
    group by week , SKUSimple) A
    group by A.SKUSimple) B ON A.sku_simple = B.sku_simple) union all (select 
    'MEX' Country,A.id_catalog_simple,
    A.sku_simple,
    A.stock_sku,
    B.avg_sales_week,
    case
        when B.avg_sales_week = 0 then A.stock_sku
        else A.stock_sku / B.avg_sales_week
    end stock_covert_week,
    A.Inventory_Value_at_Retail,
    A.stock_supplier,
    A.Inventory_cogs
from
    (select 
        A.sku sku_simple,
            C.quantity stock_sku,
            C.quantity * A.price /13 Inventory_Value_at_Retail,
            E.quantity stock_supplier,
            A.cost * C.quantity/13 Inventory_cogs,A.id_catalog_simple
    from
        bob_live_mx.catalog_simple A
    left join bob_live_mx.catalog_source B ON A.id_catalog_simple = B.fk_catalog_simple
    left join bob_live_mx.catalog_stock C ON C.fk_catalog_source = B.id_catalog_source
    left join development_mx.A_Master_Catalog D ON D.id_catalog_simple = A.id_catalog_simple
    left join bob_live_mx.catalog_supplier_stock E ON E.fk_catalog_simple = A.id_catalog_simple
    where
        D.Cat_BP like '%Fashion%') A
        left join
    (select 
        A.SKUSimple sku_simple,
            avg(A.unidades_vendidas) avg_sales_week
    from
        (select 
        date_format(Date, '%Y-%u') week,
            A.SKUSimple,
            count(*) unidades_vendidas
    from
        development_mx.A_Master A
    where
        OrderAfterCan = 1
            and CatBP like '%Fashion%'
            and date_format(Date, '%Y-%m') = date_format(curdate(), '%Y-%m')
    group by week , SKUSimple) A
    group by A.SKUSimple) B ON A.sku_simple = B.sku_simple);

ALTER TABLE commercial_pe.F_R_FashionStock 
ADD INDEX `idxx` (`id_catalog_simple` ASC) ;

ALTER TABLE commercial_pe.F_R_FashionStock 
ADD INDEX `sku_simple` (`sku_simple` ASC) ;

ALTER TABLE commercial_pe.F_R_FashionStock 
ADD INDEX `idx` (`Country` ASC) ;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`sthip.blas`@`%`*/ /*!50003 PROCEDURE `rout_handling_freight`()
BEGIN

#handling cost
#STHIP
call commercial_pe.rout_sku_visible;

drop table if exists commercial_pe.handling_cost2;
create table commercial_pe.handling_cost2(
select D.id_catalog_simple,D.sku_simple,
min(C.handling_cost) as handling_cost
from bob_live_pe.catalog_config_has_catalog_category A
inner join commercial_pe.sku_visible D on D.id_catalog_config=A.fk_catalog_config
left join bob_live_pe.shipment_handling_cost C on C.fk_catalog_category=A.fk_catalog_category
group by D.id_catalog_simple,D.sku_simple
);

alter table commercial_pe.handling_cost2
ADD INDEX `id` (`id_catalog_simple` ASC) ;


#Daniel
/*drop table commercial_pe.handling_cost;
create table commercial_pe.handling_cost(
select B.id_catalog_simple, B.sku_simple,
min(C.handling_cost) as handling_cost
from bob_live_pe.catalog_config_has_catalog_category A
left join development_pe.A_Master_Catalog B on B.id_catalog_config=A.fk_catalog_config
left join bob_live_pe.shipment_handling_cost C on C.fk_catalog_category=A.fk_catalog_category
group by B.id_catalog_simple,B.sku_simple);*/




# freight cost  category
drop table if exists commercial_pe.freight_cost2;
#Sthip
create table commercial_pe.freight_cost2(
select Z.id_catalog_config,Z.id_catalog_simple,Z.sku_simple,Z.fk_shipment_zone,
Z.package_weight,Z.eligible_free_shipping,min(Z.Real_Freight_Cost) as Real_Freight_Cost 
from (
         select E.id_catalog_config,E.id_catalog_simple,E.sku_simple,C.fk_catalog_category,
                C.fk_shipment_zone,C.cost_structure,B.eligible_free_shipping,
				min(C.freight_cost) as freight_cost,D.package_weight,
				if(C.cost_structure='w',min(C.freight_cost)*D.package_weight,min(C.freight_cost)) 
				as Real_Freight_Cost
				from commercial_pe.sku_visible E 
				inner join bob_live_pe.catalog_config_has_catalog_category A on E.id_catalog_config=A.fk_catalog_config
				left join development_pe.A_Master_Catalog B on B.id_catalog_config=E.id_catalog_config
				left join bob_live_pe.shipment_freight_cost C on C.fk_catalog_category=A.fk_catalog_category 
				left join bob_live_pe.catalog_config D on D.id_catalog_config=E.id_catalog_config
				group by E.id_catalog_config,E.id_catalog_simple,E.sku_simple,C.fk_catalog_category,
				C.fk_shipment_zone,C.cost_structure,B.eligible_free_shipping 
													) Z
group by  Z.id_catalog_config,Z.id_catalog_simple,Z.sku_simple,
Z.fk_shipment_zone,
Z.package_weight,Z.eligible_free_shipping);

alter table commercial_pe.freight_cost2
ADD INDEX `idx` (`id_catalog_simple` ASC);

drop table if exists  commercial_pe.freight_cost22;
create table commercial_pe.freight_cost22(
select A.id_catalog_config,A.id_catalog_simple,A.sku_simple,A.package_weight,
A.eligible_free_shipping,
sum(case when A.fk_shipment_zone= 1 then A.Real_Freight_Cost end) Real_Freight_Cost_zone1,
sum(case when A.fk_shipment_zone= 2 then A.Real_Freight_Cost end) Real_Freight_Cost_zone2,
sum(case when A.fk_shipment_zone= 3 then A.Real_Freight_Cost end) Real_Freight_Cost_zone3,
sum(case when A.fk_shipment_zone= 4 then A.Real_Freight_Cost end) Real_Freight_Cost_zone4,
sum(case when A.fk_shipment_zone= 5 then A.Real_Freight_Cost end) Real_Freight_Cost_zone5,
sum(case when A.fk_shipment_zone= 6 then A.Real_Freight_Cost end) Real_Freight_Cost_zone6,
sum(case when A.fk_shipment_zone= 7 then A.Real_Freight_Cost end) Real_Freight_Cost_zone7,
sum(case when A.fk_shipment_zone= 8 then A.Real_Freight_Cost end) Real_Freight_Cost_zone8,
sum(case when A.fk_shipment_zone= 9 then A.Real_Freight_Cost end) Real_Freight_Cost_zone9,
sum(case when A.fk_shipment_zone= 13 then A.Real_Freight_Cost end) Real_Freight_Cost_zone10
from 
(select * from commercial_pe.freight_cost2
) A
group by  A.id_catalog_config,A.id_catalog_simple,
A.sku_simple,A.package_weight,A.eligible_free_shipping);

alter table commercial_pe.freight_cost22
ADD INDEX `idh` (`id_catalog_simple` ASC);

drop table  if exists commercial_pe.freight_cost2;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`sthip.blas`@`%`*/ /*!50003 PROCEDURE `rout_handling_freight_catalog`()
BEGIN

#handling cost
#STHIP
#drop table commercial_pe.handling_cost_catalog;
create table commercial_pe.handling_cost_catalog(
select D.id_catalog_simple,D.sku,
min(C.handling_cost) as handling_cost
from bob_live_pe.catalog_config_has_catalog_category A
inner join commercial_pe.BDPet_New D on D.fk_catalog_config=A.fk_catalog_config
left join bob_live_pe.shipment_handling_cost C on C.fk_catalog_category=A.fk_catalog_category
group by D.id_catalog_simple,D.sku
);

alter table commercial_pe.handling_cost_catalog
ADD INDEX `id` (`id_catalog_simple` ASC) ;

#creando freight temporal
create table commercial_pe.freight_catalog_temporal
(select Z.fk_catalog_config,Z.id_catalog_simple,Z.sku,Z.fk_shipment_zone,
Z.package_weight,Z.eligible_free_shipping,min(Z.Real_Freight_Cost) as Real_Freight_Cost 
from (
         select E.fk_catalog_config,E.id_catalog_simple,E.sku,C.fk_catalog_category,
                C.fk_shipment_zone,C.cost_structure,B.eligible_free_shipping,
    min(C.freight_cost) as freight_cost,D.package_weight,
    if(C.cost_structure='w',min(C.freight_cost)*D.package_weight,min(C.freight_cost)) 
    as Real_Freight_Cost
    from commercial_pe.BDPet_New E 
    inner join bob_live_pe.catalog_config_has_catalog_category A on E.fk_catalog_config=A.fk_catalog_config
    left join development_pe.A_Master_Catalog B on B.id_catalog_config=E.fk_catalog_config
    left join bob_live_pe.shipment_freight_cost C on C.fk_catalog_category=A.fk_catalog_category 
    left join bob_live_pe.catalog_config D on D.id_catalog_config=E.fk_catalog_config
#where E.sku ='EL008EL48BCXPEAMZ-753'    
group by E.fk_catalog_config,E.id_catalog_simple,E.sku,C.fk_catalog_category,
    C.fk_shipment_zone,C.cost_structure,B.eligible_free_shipping 
            ) Z
group by  Z.fk_catalog_config,Z.id_catalog_simple,Z.sku,
Z.fk_shipment_zone,
Z.package_weight,Z.eligible_free_shipping );

alter table commercial_pe.freight_catalog_temporal
ADD INDEX `idx` (`id_catalog_simple` ASC);


# freight cost  category
#drop table commercial_pe.freight_cost22;
#Sthip
create table commercial_pe.freight_cost_catalog(
select A.fk_catalog_config,A.id_catalog_simple,A.sku,A.package_weight,A.eligible_free_shipping,
sum(case when A.fk_shipment_zone= 1 then A.Real_Freight_Cost end) Real_Freight_Cost_zone1,
sum(case when A.fk_shipment_zone= 2 then A.Real_Freight_Cost end) Real_Freight_Cost_zone2,
sum(case when A.fk_shipment_zone= 3 then A.Real_Freight_Cost end) Real_Freight_Cost_zone3,
sum(case when A.fk_shipment_zone= 4 then A.Real_Freight_Cost end) Real_Freight_Cost_zone4,
sum(case when A.fk_shipment_zone= 5 then A.Real_Freight_Cost end) Real_Freight_Cost_zone5,
sum(case when A.fk_shipment_zone= 6 then A.Real_Freight_Cost end) Real_Freight_Cost_zone6,
sum(case when A.fk_shipment_zone= 7 then A.Real_Freight_Cost end) Real_Freight_Cost_zone7,
sum(case when A.fk_shipment_zone= 8 then A.Real_Freight_Cost end) Real_Freight_Cost_zone8,
sum(case when A.fk_shipment_zone= 9 then A.Real_Freight_Cost end) Real_Freight_Cost_zone9,
sum(case when A.fk_shipment_zone= 13 then A.Real_Freight_Cost end) Real_Freight_Cost_zone10

from 
(select * from commercial_pe.freight_catalog_temporal
) A
group by  A.fk_catalog_config,A.id_catalog_simple,A.sku,A.package_weight,A.eligible_free_shipping);

alter table commercial_pe.freight_cost_catalog
ADD INDEX `idx` (`id_catalog_simple` ASC);

#create table commercial_pe.hand_freight_catalog(
#select A.*,B.handling_cost
#from commercial_pe.freight_cost_catalog A
#left join commercial_pe.handling_cost_catalog B on A.id_catalog_simple=B.id_catalog_simple);

#drop table commercial_pe.handling_cost_catalog;
#drop table commercial_pe.freight_cost_catalog;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`sthip.blas`@`%`*/ /*!50003 PROCEDURE `rout_lista_pedidos_sin_factura`()
BEGIN
create table commercial_pe.lista_ordenes_compra2(
SELECT concat( cast(A.venture_code as char(100)), 
           lpad(cast(A.id_procurement_order as char(100)), 7, 0),
           cast(A.check_digit as char(100))) as id_orden_compra,
A.id_procurement_order,B.name_company,
B.days_credit,B.line_credit,B.payday,'' as condiciones_pago,
A.payment_status,C.unit_price as total_pagado,D.total_de_la_orden ,
A.created_at,'' as acciones
 FROM procurement_live_pe.procurement_order A
inner join bob_live_pe.supplier B on A.fk_catalog_supplier=B.id_supplier
inner join procurement_live_pe.procurement_order_item C on C.fk_procurement_order=A.id_procurement_order
inner join (SELECT A.id_procurement_order,sum(C.unit_price) as total_de_la_orden
 FROM procurement_live_pe.procurement_order A
inner join bob_live_pe.supplier B on A.fk_catalog_supplier=B.id_supplier
inner join procurement_live_pe.procurement_order_item C on C.fk_procurement_order=A.id_procurement_order
where year(A.created_at)='2014'
group by A.id_procurement_order) D on A.id_procurement_order=D.id_procurement_order
where year(A.created_at)='2014'  and is_cancelled='0'
order by A.created_at);

alter table commercial_pe.lista_ordenes_compra2
ADD INDEX `id` (`id_orden_compra` ASC) ;

create table commercial_pe.lista_facturas2(
SELECT A.invoice_nr,B.name_company,B.days_credit,B.line_credit,B.payday,
sum(D.unit_price)total_relacionado,A.numero_cuenta,A.issue_date,A.fecha_pago,
A.numero_legal,A.bank,A.bank_name,concat( cast(E.venture_code as char(100)), 
           lpad(cast(E.id_procurement_order as char(100)), 7, 0),
           cast(if(E.check_digit is null,'A',E.check_digit) as char(100))) as POS
 FROM procurement_live_pe.invoice A
inner join bob_live_pe.supplier B on A.fk_catalog_supplier=B.id_supplier
inner join procurement_live_pe.invoice_item C on A.id_invoice=C.fk_invoice 
inner join procurement_live_pe.procurement_order_item D on 
D.id_procurement_order_item=C.fk_procurement_order_item
INNER JOIN procurement_live_pe.procurement_order E on D.fk_procurement_order=E.id_procurement_order
where year(A.fecha_pago)='2014'
group by A.invoice_nr,concat( cast(E.venture_code as char(100)), 
           lpad(cast(E.id_procurement_order as char(100)), 7, 0),
           cast(E.check_digit as char(100)))
order by A.issue_date);

alter table commercial_pe.lista_facturas2
ADD INDEX `idx` (`POS` ASC) ;

drop table commercial_pe.lista_pedidos_sin_factura;
CREATE TABLE commercial_pe.lista_pedidos_sin_factura
(select A.*,B.invoice_nr from commercial_pe.lista_ordenes_compra2 A
left join commercial_pe.lista_facturas2 B on A.id_orden_compra=B.POS);


drop table commercial_pe.lista_ordenes_compra2;
drop table commercial_pe.lista_facturas2;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`sthip.blas`@`%`*/ /*!50003 PROCEDURE `rout_mercado`()
BEGIN
create table commercial_pe.stud_mercado_final
(select  A.CustomerNum,concat(A.FirstName,' ',A.LastName) as nombre,C.gender,
(year(now())-year(C.birthday)) as edad,A.City,max(A.Date) as ultima_fecha_compra,
count(distinct A.OrderNum) as num_ordenes_creadas,
B.num_ordenes_cerradas,
count(case when A.OrderAfterCan='1' then A.OrderNum end) as item_compr,
avg(case when A.OrderAfterCan='1' then A.PackageWeight end) as peso_prom ,
avg(case when A.OrderAfterCan='1' then A.ItemsInOrder end) as avg_item_order,
count(case when A.Cat1='Electrónicos' and A.OrderAfterCan='1 'then A.Cat1 end) as compras_electronicos,
count(case when A.Cat1='Fashion' and A.OrderAfterCan='1 'then A.Cat1 end) as compras_Fashion,
count(case when A.Cat1='Belleza' and A.OrderAfterCan='1 'then A.Cat1 end) as compras_Belleza,
count(case when A.Cat1='Home and Living' and A.OrderAfterCan='1 'then A.Cat1 end) as compras_Home_Living,
count(case when A.Cat1='Entretenimiento' and A.OrderAfterCan='1 'then A.Cat1 end) as compras_Entretenimiento,
count(case when A.Cat1='Juguetes, niños y bebes' and A.OrderAfterCan='1 'then A.Cat1 end) as compras_juguetes,
count(case when A.Cat1='Deportes' and A.OrderAfterCan='1 'then A.Cat1 end) as compras_Deportes,
count(case when A.Cat1='Salud' and A.OrderAfterCan='1 'then A.Cat1 end) as compras_Salud,
count(case when A.Cat1='Intangibles' and A.OrderAfterCan='1 'then A.Cat1 end) as compras_Intangibles,
count(case when A.Cat1='Gourmet, Vinos y Licores' and A.OrderAfterCan='1 'then A.Cat1 end) as compras_gourmet,
count(case when A.Cat1='Mascotas' and A.OrderAfterCan='1 'then A.Cat1 end) as compras_Mascotas,
count(case when A.Cat1='DefaultSC' and A.OrderAfterCan='1 'then A.Cat1 end) as compras_DefaultSC,
sum(case when A.Cat1='Electrónicos' and A.OrderAfterCan='1 'then A.Rev end) as soles_electronicos,
sum(case when A.Cat1='Fashion' and A.OrderAfterCan='1 'then A.Rev end) as soles_Fashion,
sum(case when A.Cat1='Belleza' and A.OrderAfterCan='1 'then A.Rev end) as soles_Belleza,
sum(case when A.Cat1='Home and Living' and A.OrderAfterCan='1 'then A.Rev end) as soles_Home_Living,
sum(case when A.Cat1='Entretenimiento' and A.OrderAfterCan='1 'then A.Rev end) as soles_Entretenimiento,
sum(case when A.Cat1='Juguetes, niños y bebes' and A.OrderAfterCan='1 'then A.Rev end) as soles_juguetes,
sum(case when A.Cat1='Deportes' and A.OrderAfterCan='1 'then A.Rev end) as soles_Deportes,
sum(case when A.Cat1='Salud' and A.OrderAfterCan='1 'then A.Rev end) as soles_Salud,
sum(case when A.Cat1='Intangibles' and A.OrderAfterCan='1 'then A.Rev end) as soles_Intangibles,
sum(case when A.Cat1='Gourmet, Vinos y Licores' and A.OrderAfterCan='1 'then A.Rev end) as soles_gourmet,
sum(case when A.Cat1='Mascotas' and A.OrderAfterCan='1 'then A.Rev end) as soles_Mascotas,
sum(case when A.Cat1='DefaultSC' and A.OrderAfterCan='1 'then A.Rev end) as soles_DefaultSC,
sum(case when A.OrderAfterCan='1' then A.Rev end) as total_gastado,
count(case when A.PrefixCode<>'' and A.OrderAfterCan='1' then A.PrefixCode end) as total_cupones_usados,
sum(case when A.OrderAfterCan='1' then A.CouponValueAfterTax end) as total_soles_cupones,
sum(case when A.OrderAfterCan='1' then A.PaymentFees END) as costo_envio,
B.tipos_pago_utilizado,
B.tipos_envio_utilizado,
if(B.num_ordenes_cerradas=1,'NEW',if(B.num_ordenes_cerradas is null,'NO-COMPRO','RETORNO')) AS status_customer
from development_pe.A_Master A
left join (select  A.CustomerNum,
count(distinct A.OrderNum) as num_ordenes_cerradas,
count(distinct A.Cat1) as num_compras_distintas_category,
count(distinct A.PaymentMethod) as tipos_pago_utilizado,
count(distinct A.ShipmentType) as tipos_envio_utilizado
from development_pe.A_Master A
where year(A.Date)='2013'  and A.OrderAfterCan='1' #and A.CustomerNum='7'
group by A.CustomerNum) B on B.CustomerNum=A.CustomerNum
left join bob_live_pe.customer C on C.id_customer=A.CustomerNum
where year(A.Date)='2013' #and A.CustomerNum='7'
group by A.CustomerNum );

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`sthip.blas`@`%`*/ /*!50003 PROCEDURE `rout_sku_fijos`()
BEGIN
create table commercial_pe.sku_fijos(
select distinct A.id_catalog_simple,A.id_catalog_config,
 trim(upper(A.brand)) brand,
A.supplier,A.sku_config, 
A.sku_simple,A.buyer
from development_pe.A_Master_Catalog A
where trim(upper(A.brand)) in ('SONY','LG ','SAMSUNG','BEATS','AOC') 
and A.buyer='B9 - Ray');

insert into commercial_pe.sku_fijos(
select distinct A.id_catalog_simple,A.id_catalog_config,
 trim(upper(A.brand)) brand,
A.supplier,A.sku_config, 
A.sku_simple,A.buyer
from development_pe.A_Master_Catalog A
where trim(upper(A.brand))  in ('Continental','Drimer',
'Estilo y Hogar','KOMFORT','Mamiboo','Paraíso','Technodream',
'Vive','Rosen','Dolce Gusto','Mobo'));


select * from commercial_pe.sku_fijos;

drop table commercial_pe.sku_fijos;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`sthip.blas`@`%`*/ /*!50003 PROCEDURE `rout_sku_live`()
BEGIN
#crear tabla SKU_Live
#drop table development_pe.`sku_live`;
#CREATE TABLE development_pe.`sku_live`(
#`date` date,
#`Cat2`varchar(50) not null,
#`Num_sku_config` int (10),
#`Num_sku_simple` int (10) );

insert into commercial_pe.sku_live
(SELECT now() as date,A.Cat2,count(A.sku_config) as Num_sku_config,
count(A.sku_simple) as Num_sku_simple
FROM development_pe.A_Master_Catalog A
where A.isVisible=1
group by A.Cat2
order by count(A.sku_config) desc);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`daniel.h`@`%`*/ /*!50003 PROCEDURE `rout_sku_visible`()
BEGIN

drop table commercial_pe.sku_visible;

create table commercial_pe.sku_visible
SELECT
	c.id_catalog_config as id_catalog_config,
	s.id_catalog_simple as id_catalog_simple ,
    c.sku AS sku_config,
    s.sku AS sku_simple
    #c.pet_status,
    #c.pet_approved,
    #c.`status` AS status_config,
    #s.`status` AS status_simple,
    #c.`name`,
    #c.display_if_out_of_stock,
    #s.price,
    #b.`status` AS `brand_status`,
    #p.`status` AS `supplier_status`

   FROM bob_live_pe.catalog_config c
   INNER JOIN bob_live_pe.catalog_simple s ON c.id_catalog_config = s.fk_catalog_config
   INNER JOIN bob_live_pe.catalog_brand b ON c.fk_catalog_brand = b.id_catalog_brand
   #INNER JOIN bob_live_pe.supplier p ON c.id_supplier = p.id_supplier

   WHERE c.pet_status=('creation,edited,images') 
    AND c.pet_approved = 1
    AND c.status='active' 
    AND s.status='active'
    AND s.price > 0
    AND b.status='active';
    #AND p.status='active';

ALTER TABLE `commercial_pe`.`sku_visible` ADD INDEX `id_catalog_config` (`id_catalog_config` ASC) ;
ALTER TABLE `commercial_pe`.`sku_visible` ADD INDEX `id_catalog_simple` (`id_catalog_simple` ASC) ;
ALTER TABLE `commercial_pe`.`sku_visible` ADD INDEX `sku_simple` (`sku_simple` ASC) ;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`daniel.h`@`%`*/ /*!50003 PROCEDURE `rout_webtrek_PO`()
BEGIN
drop table if exists commercial_pe.PO;
create table commercial_pe.PO 
select 'MEX' as Country,A.id_catalog_simple,sku_simple,if(A.special_price is null,A.price,if(A.special_to_date>now() or A.special_to_date is null,
A.special_price,A.price))/B.unit_price as PO,
if(A.special_price is null,A.price,if(A.special_to_date>curdate() or A.special_to_date is null,
A.special_price,A.price))-B.unit_price as D_margin_last_week
 from development_mx.A_Master_Catalog A
left join (select Z.id_catalog_simple,avg(A.unit_price)  as unit_price
					from  development_mx.A_Master_Catalog Z
					inner join procurement_live_mx.procurement_order_item B on Z.id_catalog_simple=B.fk_catalog_simple
					inner join procurement_live_mx.invoice_item A on B.id_procurement_order_item=A.fk_procurement_order_item
					where Z.Cat_BP='3,1 Fashion' and week(A.created_at)=week(curdate())-1 and year(curdate())=year(A.created_at)
					group by Z.sku_simple) B on A.id_catalog_simple=B.id_catalog_simple
where A.Cat_BP='3,1 Fashion'
union
select 'COL' as Country,A.id_catalog_simple,sku_simple,if(A.special_price is null,A.price,if(A.special_to_date>now() or A.special_to_date is null,
A.special_price,A.price))/B.unit_price as PO,
if(A.special_price is null,A.price,if(A.special_to_date>curdate() or A.special_to_date is null,
A.special_price,A.price))-B.unit_price as D_margin_last_week
 from development_co_project.A_Master_Catalog A
left join (select Z.id_catalog_simple,avg(A.unit_price)  as unit_price
					from  development_co_project.A_Master_Catalog Z
					inner join procurement_live_co.procurement_order_item B on Z.id_catalog_simple=B.fk_catalog_simple
					inner join procurement_live_co.invoice_item A on B.id_procurement_order_item=A.fk_procurement_order_item
					where Z.Cat_BP='3,1 Fashion' and week(A.created_at)=week(curdate())-1 and year(now())=year(A.created_at)
					group by Z.sku_simple) B on A.id_catalog_simple=B.id_catalog_simple
where A.Cat_BP='3,1 Fashion' 
union
select 'VEN' as Country,A.id_catalog_simple,sku_simple,if(A.special_price is null,A.price,if(A.special_to_date>now() or A.special_to_date is null,
A.special_price,A.price))/B.unit_price as PO,
if(A.special_price is null,A.price,if(A.special_to_date>curdate() or A.special_to_date is null,
A.special_price,A.price))-B.unit_price as D_margin_last_week
 from development_ve.A_Master_Catalog A
left join (select Z.id_catalog_simple,avg(A.unit_price)  as unit_price
					from  development_ve.A_Master_Catalog Z
					inner join procurement_live_ve.procurement_order_item B on Z.id_catalog_simple=B.fk_catalog_simple
					inner join procurement_live_ve.invoice_item A on B.id_procurement_order_item=A.fk_procurement_order_item
					where Z.Cat_BP='3,1 Fashion' and week(A.created_at)=week(curdate())-1 and year(curdate())=year(A.created_at)
					group by Z.sku_simple) B on A.id_catalog_simple=B.id_catalog_simple
where A.Cat_BP='3,1 Fashion' 
union
select 'PER' as Country,A.id_catalog_simple,sku_simple,if(A.special_price is null,A.price,if(A.special_to_date>now() or A.special_to_date is null,
A.special_price,A.price))/B.unit_price as PO,
if(A.special_price is null,A.price,if(A.special_to_date>curdate() or A.special_to_date is null,
A.special_price,A.price))-B.unit_price as D_margin_last_week
 from development_pe.A_Master_Catalog A
left join (select Z.id_catalog_simple,avg(A.unit_price)  as unit_price
					from  development_pe.A_Master_Catalog Z
					inner join procurement_live_pe.procurement_order_item B on Z.id_catalog_simple=B.fk_catalog_simple
					inner join procurement_live_pe.invoice_item A on B.id_procurement_order_item=A.fk_procurement_order_item
					where Z.Cat_BP='3,1 Fashion' and week(A.created_at)=week(curdate())-1 and year(curdate())=year(A.created_at)
					group by Z.sku_simple) B on A.id_catalog_simple=B.id_catalog_simple
where A.Cat_BP='3,1 Fashion' ;


alter table commercial_pe.PO
ADD INDEX `idx` (`id_catalog_simple` ASC) ;

alter table commercial_pe.PO
ADD INDEX `idxxx` (`sku_simple` ASC) ;

alter table commercial_pe.PO
ADD INDEX `idxx` (`Country` ASC) ;

drop table if exists commercial_pe.sell_throught_last_week;
create table commercial_pe.sell_throught_last_week
select 'MEX' as Country,A.id_catalog_simple,A.sku_simple,
		C.items_vendidos/B.items_comprados as sell_throught_last_week  
from development_mx.A_Master_Catalog A
left join (select Z.id_catalog_simple,Z.sku_simple,count(Z.sku_simple) as items_comprados from development_mx.A_Master_Catalog Z 
			left join procurement_live_mx.procurement_order_item B on Z.id_catalog_simple=B.fk_catalog_simple
			left join procurement_live_mx.invoice_item A on B.id_procurement_order_item=A.fk_procurement_order_item
			where Z.Cat_BP='3,1 Fashion' and week(A.created_at)=week(curdate())-1 and year(curdate())=year(A.created_at)
			group by Z.sku_simple)B on  B.id_catalog_simple=A.id_catalog_simple
left join (select SKUConfig,SKUSimple,count(SKUsimple) as items_vendidos from development_mx.A_Master
						where OrderAfterCan='1' and CatBP='3,1 Fashion' and week(Date)=week(curdate())-1 and year(curdate())=year(Date)
						group by SKUSimple) C on C.SKUSimple=A.sku_simple
where A.Cat_BP='3,1 Fashion'
union
select 'COL' as Country,A.id_catalog_simple,A.sku_simple,
		C.items_vendidos/B.items_comprados as sell_throught_last_week  
from development_co_project.A_Master_Catalog A
left join (select Z.id_catalog_simple,Z.sku_simple,count(Z.sku_simple) as items_comprados from development_co_project.A_Master_Catalog Z 
			left join procurement_live_co.procurement_order_item B on Z.id_catalog_simple=B.fk_catalog_simple
			left join procurement_live_co.invoice_item A on B.id_procurement_order_item=A.fk_procurement_order_item
			where Z.Cat_BP='3,1 Fashion' and week(A.created_at)=week(curdate())-1 and year(curdate())=year(A.created_at)
			group by Z.sku_simple)B on  B.id_catalog_simple=A.id_catalog_simple
left join (select SKUConfig,SKUSimple,count(SKUsimple) as items_vendidos from development_co_project.A_Master
						where OrderAfterCan='1' and CatBP='3,1 Fashion' and week(Date)=week(curdate())-1 and year(curdate())=year(Date)
						group by SKUSimple) C on C.SKUSimple=A.sku_simple
where A.Cat_BP='3,1 Fashion'
union
select 'VEN' as Country,A.id_catalog_simple,A.sku_simple,
		C.items_vendidos/B.items_comprados as sell_throught_last_week  
from development_ve.A_Master_Catalog A
left join (select Z.id_catalog_simple,Z.sku_simple,count(Z.sku_simple) as items_comprados from development_ve.A_Master_Catalog Z 
			left join procurement_live_ve.procurement_order_item B on Z.id_catalog_simple=B.fk_catalog_simple
			left join procurement_live_ve.invoice_item A on B.id_procurement_order_item=A.fk_procurement_order_item
			where Z.Cat_BP='3,1 Fashion' and week(A.created_at)=week(curdate())-1 and year(now())=year(A.created_at)
			group by Z.sku_simple)B on  B.id_catalog_simple=A.id_catalog_simple
left join (select SKUConfig,SKUSimple,count(SKUsimple) as items_vendidos from development_ve.A_Master
						where OrderAfterCan='1' and CatBP='3,1 Fashion' and week(Date)=week(curdate())-1 and year(curdate())=year(Date)
						group by SKUSimple) C on C.SKUSimple=A.sku_simple
where A.Cat_BP='3,1 Fashion'
union
select 'PER' as Country,A.id_catalog_simple,A.sku_simple,
		C.items_vendidos/B.items_comprados as sell_throught_last_week  
from development_pe.A_Master_Catalog A
left join (select Z.id_catalog_simple,Z.sku_simple,count(Z.sku_simple) as items_comprados from development_pe.A_Master_Catalog Z 
			left join procurement_live_pe.procurement_order_item B on Z.id_catalog_simple=B.fk_catalog_simple
			left join procurement_live_pe.invoice_item A on B.id_procurement_order_item=A.fk_procurement_order_item
			where Z.Cat_BP='3,1 Fashion' and week(A.created_at)=week(curdate())-1 and year(curdate())=year(A.created_at)
			group by Z.sku_simple)B on  B.id_catalog_simple=A.id_catalog_simple
left join (select SKUConfig,SKUSimple,count(SKUsimple) as items_vendidos from development_pe.A_Master
						where OrderAfterCan='1' and CatBP='3,1 Fashion' and week(Date)=week(curdate())-1 and year(now())=year(Date)
						group by SKUSimple) C on C.SKUSimple=A.sku_simple
where A.Cat_BP='3,1 Fashion';


alter table commercial_pe.sell_throught_last_week
ADD INDEX `idx` (`id_catalog_simple` ASC) ;

alter table commercial_pe.sell_throught_last_week
ADD INDEX `idxxx` (`sku_simple` ASC) ;

alter table commercial_pe.sell_throught_last_week
ADD INDEX `idxx` (`Country` ASC) ;

drop table if exists commercial_pe.webtrek_PO;
create table commercial_pe.webtrek_PO(
select A.Country,A.id_catalog_simple,A.sku_simple,A.PO,A.D_margin_last_week,
B.sell_throught_last_week
from commercial_pe.PO A
left join commercial_pe.sell_throught_last_week B 
on A.id_catalog_simple=B.id_catalog_simple and A.Country=B.Country);

alter table commercial_pe.webtrek_PO
ADD INDEX `idx` (`id_catalog_simple` ASC) ;

alter table commercial_pe.webtrek_PO
ADD INDEX `idxxx` (`sku_simple` ASC) ;

alter table commercial_pe.webtrek_PO
ADD INDEX `idxx` (`Country` ASC) ;

drop table if exists commercial_pe.view_car_abandoment;
create table commercial_pe.view_car_abandoment
(select 'MEX' as Country,A.id_catalog_simple,A.sku_simple,B.view_4last_week,B.view_last_week,B.view_2last_week,B.view_month,B.view_year,
B.car_4last_week,B.car_last_week,B.car_month,B.car_year,
B.abandoment_4last_week,B.abandoment_last_week,B.abandoment_month,B.abandoment_year 
from development_mx.A_Master_Catalog A
inner join (
select sku_simple,
sum(if(week(A.date)=week(curdate())-4 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),visitas,0)) as view_4last_week,
sum(if(week(A.date)=week(curdate())-1 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),visitas,0)) as view_last_week,
sum(if(week(A.date)=week(curdate())-2 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),visitas,0)) as view_2last_week,
sum(if(month(A.date)=month(curdate()) and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),visitas,0)) as view_month,
sum(if(A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),visitas,0)) as view_year,
sum(if(week(A.date)=week(curdate())-4 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito,0)) as car_4last_week,
sum(if(week(A.date)=week(curdate())-1 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito,0)) as car_last_week,
sum(if(month(A.date)=month(curdate()) and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito,0)) as car_month,
sum(if(A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito,0)) as car_year,
sum(if(week(A.date)=week(curdate())-4 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito-comprado,0)) as abandoment_4last_week,
sum(if(week(A.date)=week(curdate())-1 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito-comprado,0)) as abandoment_last_week,
sum(if(month(A.date)=month(curdate()) and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito-comprado,0)) as abandoment_month,
sum(if(A.category='Fashion'and year(curdate())=year(A.date),carrito-comprado,0)) as abandoment_year
 			from david_dana.mx_cmr_prc_tbl_views_visits_add_to_car_comprado_simple_webtrekk A
			group by sku_simple) B on A.sku_simple=B.sku_simple where A.Cat_BP='3,1 Fashion') 
union
(select 'PER' as Country,A.id_catalog_simple,A.sku_simple,B.view_4last_week,B.view_2last_week,B.view_last_week,B.view_month,B.view_year,
B.car_4last_week,B.car_last_week,B.car_month,B.car_year,
B.abandoment_4last_week,B.abandoment_last_week,B.abandoment_month,B.abandoment_year 
from development_pe.A_Master_Catalog A
inner join (
select sku_simple,
sum(if(week(A.date)=week(curdate())-4 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),visitas,0)) as view_4last_week,
sum(if(week(A.date)=week(curdate())-1 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),visitas,0)) as view_last_week,
sum(if(week(A.date)=week(curdate())-2 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),visitas,0)) as view_2last_week,
sum(if(month(A.date)=month(curdate()) and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),visitas,0)) as view_month,
sum(if(A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),visitas,0)) as view_year,
sum(if(week(A.date)=week(curdate())-4 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito,0)) as car_4last_week,
sum(if(week(A.date)=week(curdate())-1 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito,0)) as car_last_week,
sum(if(month(A.date)=month(curdate()) and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito,0)) as car_month,
sum(if(A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito,0)) as car_year,
sum(if(week(A.date)=week(curdate())-4 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito-comprado,0)) as abandoment_4last_week,
sum(if(week(A.date)=week(curdate())-1 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito-comprado,0)) as abandoment_last_week,
sum(if(month(A.date)=month(curdate()) and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito-comprado,0)) as abandoment_month,
sum(if(A.category='Fashion'and year(curdate())=year(A.date),carrito-comprado,0)) as abandoment_year
 			from david_dana.pe_cmr_prc_tbl_views_visits_add_to_car_comprado_simple_webtrekk A
			group by sku_simple) B on A.sku_simple=B.sku_simple where A.Cat_BP='3,1 Fashion')
union
(select 'COL' as Country,A.id_catalog_simple,A.sku_simple,B.view_4last_week,B.view_2last_week,B.view_last_week,B.view_month,B.view_year,
B.car_4last_week,B.car_last_week,B.car_month,B.car_year,
B.abandoment_4last_week,B.abandoment_last_week,B.abandoment_month,B.abandoment_year 
from development_co_project.A_Master_Catalog A
inner join (
select sku_simple,
sum(if(week(A.date)=week(curdate())-4 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),visitas,0)) as view_4last_week,
sum(if(week(A.date)=week(curdate())-1 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),visitas,0)) as view_last_week,
sum(if(week(A.date)=week(curdate())-2 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),visitas,0)) as view_2last_week,
sum(if(month(A.date)=month(curdate()) and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),visitas,0)) as view_month,
sum(if(A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),visitas,0)) as view_year,
sum(if(week(A.date)=week(curdate())-4 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito,0)) as car_4last_week,
sum(if(week(A.date)=week(curdate())-1 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito,0)) as car_last_week,
sum(if(month(A.date)=month(curdate()) and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito,0)) as car_month,
sum(if(A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito,0)) as car_year,
sum(if(week(A.date)=week(curdate())-4 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito-comprado,0)) as abandoment_4last_week,
sum(if(week(A.date)=week(curdate())-1 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito-comprado,0)) as abandoment_last_week,
sum(if(month(A.date)=month(curdate()) and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito-comprado,0)) as abandoment_month,
sum(if(A.category='Fashion'and year(curdate())=year(A.date),carrito-comprado,0)) as abandoment_year
 			from david_dana.co_cmr_prc_tbl_views_visits_add_to_car_comprado_simple_webtrekk A
			group by sku_simple) B on A.sku_simple=B.sku_simple where A.Cat_BP='3,1 Fashion')
union
(select 'VEN' as Country,A.id_catalog_simple,A.sku_simple,B.view_4last_week,B.view_2last_week,B.view_last_week,B.view_month,B.view_year,
B.car_4last_week,B.car_last_week,B.car_month,B.car_year,
B.abandoment_4last_week,B.abandoment_last_week,B.abandoment_month,B.abandoment_year 
from development_ve.A_Master_Catalog A
inner join (
select sku_simple,
sum(if(week(A.date)=week(curdate())-4 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),visitas,0)) as view_4last_week,
sum(if(week(A.date)=week(curdate())-1 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),visitas,0)) as view_last_week,
sum(if(week(A.date)=week(curdate())-2 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),visitas,0)) as view_2last_week,
sum(if(month(A.date)=month(curdate()) and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),visitas,0)) as view_month,
sum(if(A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),visitas,0)) as view_year,
sum(if(week(A.date)=week(curdate())-4 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito,0)) as car_4last_week,
sum(if(week(A.date)=week(curdate())-1 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito,0)) as car_last_week,
sum(if(month(A.date)=month(curdate()) and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito,0)) as car_month,
sum(if(A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito,0)) as car_year,
sum(if(week(A.date)=week(curdate())-4 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito-comprado,0)) as abandoment_4last_week,
sum(if(week(A.date)=week(curdate())-1 and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito-comprado,0)) as abandoment_last_week,
sum(if(month(A.date)=month(curdate()) and A.Cat_BP='3,1 Fashion'and year(curdate())=year(A.date),carrito-comprado,0)) as abandoment_month,
sum(if(A.category='Fashion'and year(curdate())=year(A.date),carrito-comprado,0)) as abandoment_year
 			from david_dana.ve_cmr_prc_tbl_views_visits_add_to_car_comprado_simple_webtrekk A
			group by sku_simple) B on A.sku_simple=B.sku_simple where A.Cat_BP='3,1 Fashion');

alter table commercial_pe.view_car_abandoment
ADD INDEX `idx` (`id_catalog_simple` ASC) ;

alter table commercial_pe.view_car_abandoment
ADD INDEX `idxxx` (`sku_simple` ASC) ;

alter table commercial_pe.view_car_abandoment
ADD INDEX `idxx` (`Country` ASC) ;

drop table commercial_pe.PO;
drop table commercial_pe.sell_throught_last_week;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`miguel.echeverre`@`%`*/ /*!50003 PROCEDURE `sp_perfomance_mp`()
BEGIN

drop table if exists commercial_pe.MarketPlace_Performance;
drop table if exists commercial_pe.Items_to_return;

create table commercial_pe.Items_to_return(

select 
    A.date_exported,
	B.date_register_to_return,
	B.date_confirmed_to_return,
	A.order_number,	
    A.item_id,
    A.sku_simple,
	A.supplier_name,
    B.item_to_return,
    B.is_returned,
    B.return_in_alice
from
    (select 
        concat(order_number, sku_simple) Group_id,supplier_name,
            date_exported,
            item_id,
            sku_simple,
            order_number
    from
        operations_pe.out_order_tracking
    where
        date_exported >= date_sub(curdate(), interval 2 month)
    group by Group_id) A
        inner join
    (select 
        `Fecha de registro` date_register_to_return,`FIN DE REVISION` date_confirmed_to_return,
            `Numero de orden` order_number,
            SKU sku_simple,
            1 item_to_return,
            if(ucase(`Procede`) = 'SI', 1, 0) is_returned,
            if(ucase(`Devuelto en ALICE`) = 'SI', 1, 0) return_in_alice
    from
        commercial_pe.RETURNS_CC_PE) B ON A.Group_id = concat(B.order_number, B.sku_simple));

alter table commercial_pe.Items_to_return 
add index `item_id`(`item_id` asc) ;

create table commercial_pe.MarketPlace_Performance(
select 
    date_format(A.date_exported, '%Y-%u') week,date_format(A.date_exported, '%Y-%m') month,A.item_id,
    A.date_exported,
    A.supplier_name,
    A.order_number,
    A.sku_simple,
    A.date_ready_to_ship_promised,
    A.date_shipped,
    A.date_delivered_promised,
    A.date_delivered,
    case
        when A.date_ready_to_ship_promised <= A.date_exported + interval 2 day then A.item_counter
        else 0
    end Item_ship_primised_48hours,
    case
        when A.date_shipped <= A.date_exported + interval 2 day then A.item_counter
        else 0
    end Item_shipped_48hours,
    A.is_stockout,
    A.workdays_to_ship,
    A.workdays_to_1st_attempt,
    A.on_time_total_1st_attempt,
    A.workdays_to_deliver,
    case
        when
            A.date_delivered is null
        then
            case
                when datediff(curdate(), A.date_exported) > 7 then 1
                else 0
            end
        else 0
    end 'items pending to deliver more than a week' ,
	if(B.item_to_return is null , 0 , 1) item_to_return  , 
	if(B.is_returned is not null,1,0) is_returned , 
	if(B.return_in_alice is not null ,1,0) return_in_alice, 
	B.date_register_to_return,
	B.date_confirmed_to_return,
	C.is_fail_delivery
from
    operations_pe.out_order_tracking A left join commercial_pe.Items_to_return B
	on A.item_id=B.item_id left join (
select item_id,is_fail_delivery from operations_pe.out_inverse_logistics_tracking
where  date_exported >= date_sub(curdate(), interval 2 month)) C

on A.item_id=C.item_id
where
    A.date_exported >= date_sub(curdate(), interval 2 month)
        and ucase(A.supplier_name) like '%(MP%)%');

alter table commercial_pe.MarketPlace_Performance
add index `item_id`(`item_id` asc);

drop table commercial_pe.Items_to_return;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:03:35
