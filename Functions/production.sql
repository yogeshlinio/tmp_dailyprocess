-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: production
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'production'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 FUNCTION `calcworkdays`(sdate varchar(50), edate varchar(50)) RETURNS int(11)
begin
 declare wdays, tdiff, counter, thisday smallint;
 declare newdate date;
 set newdate := sdate;
 set wdays = 0;
 if sdate is null then return 0; end if;
 if edate is null then return 0; end if;
 if edate like '1%' then return 0; end if;
 if edate like '0%' then return 0; end if;
 if sdate like '1%' then return 0; end if;
 if sdate like '0%' then return 0; end if;
    if datediff(edate, sdate) <= 0 then return 0; end if;
         label1: loop
          set thisday = dayofweek(newdate);
            if thisday in(1,2,3,4,5) then set wdays := wdays + 1; end if;
             set newdate = date_add(newdate, interval 1 day);
            if datediff(edate, newdate) <= 0 then leave label1; end if;
       end loop label1;
      return wdays;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 FUNCTION `maximum`(procure integer, ready integer, to_ship integer,  attempt integer ) RETURNS int(11)
begin
declare compare integer; 
		set compare = procure;
		if compare < ready then set compare = ready; end if;
		if compare < to_ship then set compare = to_ship; end if;
		if compare < attempt then set compare = attempt; end if;
 return compare;
 end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 FUNCTION `Val`(`value` varchar(50) ) RETURNS decimal(26,5)
BEGIN
 #Routine body goes here...
  DECLARE weight DECIMAL(15,5);
 
  select cast( REPLACE( value , " " , ""  )   AS DECIMAL(15,5) ) into weight;

  #select if(  abs( weight ) < 1  , 0 , weight ) into weight;   

 RETURN weight;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 FUNCTION `week_exit`(edate date) RETURNS varchar(20) CHARSET latin1
begin
 declare sdate varchar(20);
 if year(edate) = 2012 then set sdate = week(edate, 5)+1; else set sdate = date_format(edate, "%v"); end if;
      return sdate;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 FUNCTION `week_iso`(edate date) RETURNS varchar(20) CHARSET latin1
begin
 declare sdate varchar(20);
 if year(edate) = 2012 then set sdate = concat(year(edate), '-',(week(edate, 5))+1); else set sdate = date_format(edate, "%x-%v"); end if;
      return sdate;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 FUNCTION `workday`(edate date, workdays integer ) RETURNS date
begin
 declare wdays, thisday, thisday0, timer smallint;
 declare newdate, middate date;
 set wdays = 0;
 set newdate :=  edate;
 if edate is null then set newdate = '2012-05-01'; end if;
 if workdays is null then return '2012-05-01'; end if;
 set thisday0 = dayofweek(edate);
 if workdays in (0) and thisday0 between 2 and 6 then return newdate; end if;
 if (workdays in (0) and thisday0 in (7)) then return date(adddate(newdate, 2)); end if;
 if (workdays in (0) and thisday0 in (1)) then return date(adddate(newdate, 1)); end if;
	label2: loop
		set newdate = date(adddate(newdate, 1));
        set thisday = dayofweek(newdate);
		if thisday between 2 and 6 then 
			set wdays = wdays + 1;
			if wdays >= workdays then 
				leave label2; 
			end if;
		else 
			set wdays = wdays;
		end if;
	end loop label2;
return newdate;
 end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `catalog_creation`()
begin

delete from production.catalog_creation;

insert into production.catalog_creation
select z.cat1, a201204.number as '201204', a201205.number as '201205', a201206.number as '201206', a201207.number as '201207', a201208.number as '201208', a201209.number as '201209', a201210.number as '201210', a201211.number as '201211', a201212.number as '201212', a201301.number as '201301', a201302.number as '201302', a201303.number as '201303', a201304.number as '201304' from (select cat1 from production.tbl_catalog_product_v2 c where cat1 is not null group by cat1)z left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production.tbl_catalog_product_v2 c, bob_live_mx.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) = 201204 group by cat1)a201204 on z.cat1=a201204.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production.tbl_catalog_product_v2 c, bob_live_mx.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201205 group by cat1)a201205 on z.cat1=a201205.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production.tbl_catalog_product_v2 c, bob_live_mx.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201206 group by cat1)a201206 on z.cat1=a201206.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production.tbl_catalog_product_v2 c, bob_live_mx.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201207 group by cat1)a201207 on z.cat1=a201207.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production.tbl_catalog_product_v2 c, bob_live_mx.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201208 group by cat1)a201208 on z.cat1=a201208.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production.tbl_catalog_product_v2 c, bob_live_mx.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201209 group by cat1)a201209 on z.cat1=a201209.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production.tbl_catalog_product_v2 c, bob_live_mx.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201210 group by cat1)a201210 on z.cat1=a201210.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production.tbl_catalog_product_v2 c, bob_live_mx.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201211 group by cat1)a201211 on z.cat1=a201211.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production.tbl_catalog_product_v2 c, bob_live_mx.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201212 group by cat1)a201212 on z.cat1=a201212.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production.tbl_catalog_product_v2 c, bob_live_mx.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201301 group by cat1)a201301 on z.cat1=a201301.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production.tbl_catalog_product_v2 c, bob_live_mx.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201302 group by cat1)a201302 on z.cat1=a201302.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production.tbl_catalog_product_v2 c, bob_live_mx.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201303 group by cat1)a201303 on z.cat1=a201303.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production.tbl_catalog_product_v2 c, bob_live_mx.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201304 group by cat1)a201304 on z.cat1=a201304.cat1 order by cat1;

update production.catalog_creation set `201204` = 0  where `201204` is null;

update production.catalog_creation set `201205` = 0  where `201205` is null;

update production.catalog_creation set `201206` = 0  where `201206` is null;

update production.catalog_creation set `201207` = 0  where `201207` is null;

update production.catalog_creation set `201208` = 0  where `201208` is null;

update production.catalog_creation set `201209` = 0  where `201209` is null;

update production.catalog_creation set `201210` = 0  where `201210` is null;

update production.catalog_creation set `201211` = 0  where `201211` is null;

update production.catalog_creation set `201212` = 0  where `201212` is null;

update production.catalog_creation set `201301` = 0  where `201301` is null;

update production.catalog_creation set `201302` = 0  where `201302` is null;

update production.catalog_creation set `201303` = 0  where `201303` is null;

update production.catalog_creation set `201304` = 0  where `201304` is null;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `catalog_history`()
begin

call production.catalog_visible;

insert into production.catalog_history(date, sku_config, sku_simple, product_name, status_config, status_simple, quantity, price) 
select curdate(), catalog_config.sku as sku_config, catalog_simple.sku as sku_simple, catalog_config.name, catalog_config.status, catalog_simple.status, catalog_stock.quantity, catalog_simple.price
from (bob_live_mx.catalog_config inner join bob_live_mx.catalog_simple on catalog_config.id_catalog_config = catalog_simple.fk_catalog_config) left join bob_live_mx.catalog_stock on catalog_simple.id_catalog_simple = catalog_stock.fk_catalog_simple;

update production.catalog_history set quantity = 0  where quantity is null and date = curdate();

update production.catalog_history c inner join production.catalog_visible v on c.sku_simple=v.sku_simple set visible = 1 where date = curdate();

update production.catalog_history set visible = 0  where visible is null and date = curdate();

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `catalog_history_regional`()
begin

select  'Catalog History Regional: start',now();

call production.catalog_history;
call production_pe.catalog_history;
call production_ve.catalog_history;
call production_co.catalog_history;

select  'Catalog History Regional: end',now();

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `catalog_visible`()
begin

delete from production.catalog_visible;

insert into production.catalog_visible(sku_config, sku_simple, pet_status, pet_approved, status_config, status_simple, name, display_if_out_of_stock, updated_at, activated_at, price) 
select catalog_config.sku as sku_config, catalog_simple.sku as sku_simple, catalog_config.pet_status, catalog_config.pet_approved, catalog_config.status as status_config, catalog_simple.status as status_simple, catalog_config.name, catalog_config.display_if_out_of_stock, catalog_config.updated_at, catalog_config.activated_at, catalog_simple.price
from (bob_live_mx.catalog_config inner join bob_live_mx.catalog_simple on catalog_config.id_catalog_config = catalog_simple.fk_catalog_config)
where (((catalog_config.pet_status)="creation,edited,images") and ((catalog_config.pet_approved)=1) and ((catalog_config.status)="active") and ((catalog_simple.status)="active") and ((catalog_config.display_if_out_of_stock)=0) and ((catalog_simple.price)>0)) or (((catalog_config.pet_status)="creation,edited,images") and ((catalog_config.pet_approved)=1) and ((catalog_config.status)="active") and ((catalog_simple.status)="active") and ((catalog_config.display_if_out_of_stock)=1) and ((catalog_simple.price)>0));

create table production.temporary_visible(
sku varchar(255),
catalog_warehouse_stock int,
catalog_supplier_stock int,
item_reserveed int,
stock_available int
);
create index sku on production.temporary_visible(sku);

insert into production.temporary_visible
SELECT `catalog_simple`.`sku`, IFNULL((SELECT CAST(quantity AS SIGNED INT) FROM bob_live_mx.catalog_warehouse_stock WHERE catalog_warehouse_stock.fk_catalog_simple = catalog_simple.id_catalog_simple),0) AS `catalog_warehouse_stock`, IFNULL((SELECT CAST(quantity AS SIGNED INT) FROM bob_live_mx.catalog_supplier_stock WHERE catalog_supplier_stock.fk_catalog_simple = catalog_simple.id_catalog_simple),0) AS `catalog_supplier_stock`, ( (SELECT COUNT(*) FROM bob_live_mx.sales_order_item JOIN bob_live_mx.sales_order ON fk_sales_order = id_sales_order WHERE fk_sales_order_process IN (33,5,27,26,31,8,23,28,32,34,29,9,30) AND is_reserved = 1 AND sales_order_item.sku = catalog_simple.sku) + IFNULL((SELECT SUM(catalog_stock_shared.reserved) FROM bob_live_mx.catalog_stock_shared WHERE catalog_stock_shared.sku = catalog_simple.sku GROUP BY catalog_stock_shared.sku), 0)) AS `item_reserved`, GREATEST((IFNULL((SELECT CAST(quantity AS SIGNED INT) FROM bob_live_mx.catalog_stock WHERE catalog_stock.fk_catalog_simple = catalog_simple.id_catalog_simple),0) - ( (SELECT COUNT(*) FROM bob_live_mx.sales_order_item JOIN bob_live_mx.sales_order ON fk_sales_order = id_sales_order WHERE fk_sales_order_process IN (33,5,27,26,31,8,23,28,32,34,29,9,30) AND is_reserved = 1 AND sales_order_item.sku = catalog_simple.sku) + IFNULL((SELECT SUM(catalog_stock_shared.reserved) FROM bob_live_mx.catalog_stock_shared WHERE catalog_stock_shared.sku = catalog_simple.sku GROUP BY catalog_stock_shared.sku), 0))),0) AS `stock_available` FROM bob_live_mx.`catalog_simple` ORDER BY `catalog_simple`.`id_catalog_simple` DESC;

update production.catalog_visible v inner join production.temporary_visible t on t.sku=v.sku_simple set v.quantity=t.stock_available;

delete from production.catalog_visible where quantity=0 or quantity is null;

drop table production.temporary_visible;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `channel_marketing`()
BEGIN
#Channel Report MX---

#Define channel---
#Voucher is priority statemnt

#NEW REGISTERS
UPDATE SEM.campaign_ad_group_mx SET channel='New Register' 
WHERE (source_medium like 'NL%' OR source_medium like 'NR%' OR source_medium like
'COM%' OR source_medium like 'BNR%') AND source_medium is null;



#BLOG
-- UPDATE SEM.campaign_ad_group_mx SET channel='Blog Linio' WHERE source_medium like 'blog%';

#PAMPA
UPDATE SEM.campaign_ad_group_mx set channel='Pampa Network' where source_medium like '%pampa%';

#REFERRAL
UPDATE SEM.campaign_ad_group_mx SET channel='Referral Sites' WHERE source_medium like '%referral' 
AND source_medium<>'facebook.com / referral'
AND source_medium<>'m.facebook.com / referral'
AND source_medium<>'linio.com.co / referral'
AND source_medium<>'linio.com / referral'
AND source_medium<>'linio.com.mx / referral'
AND source_medium<>'google.co.ve / referral'
AND source_medium<>'linio.com.ve / referral'
AND source_medium<>'linio.com.pe / referral'
AND source_medium<>'www-staging.linio.com.ve / referral'
AND source_medium<>'google.com / referral'
AND source_medium<>'.com.ve / referral'
AND source_medium<>'blog.linio.com.ve / referral' AND source_medium<>'t.co / referral'
AND source_medium not like '%google%' AND source_medium not like '%blog%' and source_medium not like '%pampa%'
and source_medium not like '%buscape%';


UPDATE SEM.campaign_ad_group_mx SET channel='Buscape' 
WHERE source_medium='Buscape / Price Comparison' OR source_medium like 'buscape%' or source_medium='tracker.buscape.com.mx / referral';

#FB ADS
UPDATE SEM.campaign_ad_group_mx SET channel='Facebook Ads' WHERE source_medium='facebook / socialmediaads' OR
source_medium like '%book%ads%';

#SEM 
UPDATE SEM.campaign_ad_group_mx SET channel='SEM' WHERE source_medium='google / cpc' AND (campaign not like '%linio%' AND 
campaign not like 'er.%' AND campaign not like 'brandb%');
UPDATE SEM.campaign_ad_group_mx SET channel='SEM Branded' WHERE source_medium='google / cpc' AND campaign like 'brandb%';

#GDN
UPDATE SEM.campaign_ad_group_mx SET channel='Google Display Network' WHERE source_medium='google / cpc' AND 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%'
or campaign like '|R%'
or campaign like '|C%');

#RETARGETING
UPDATE SEM.campaign_ad_group_mx SET channel='Sociomantic' WHERE source_medium='sociomantic / (not set)' 
OR source_medium='sociomantic / retargeting' OR source_medium 
like '%socioman%';
UPDATE SEM.campaign_ad_group_mx SET channel='Cart Recovery' WHERE source_medium='Cart_Recovery / CRM';
UPDATE SEM.campaign_ad_group_mx SET channel='Vizury' WHERE source_medium='vizury / retargeting' OR source_medium like '%vizury%';
UPDATE SEM.campaign_ad_group_mx SET channel='Facebook R.' WHERE source_medium='facebook / retargeting';
UPDATE SEM.campaign_ad_group_mx SET channel='VEInteractive' WHERE source_medium like '%VEInteractive%';
UPDATE SEM.campaign_ad_group_mx SET channel='Triggit' WHERE source_medium like '%Triggit / Retargeting%';


#Criteo
UPDATE SEM.campaign_ad_group_mx set channel='Criteo' where source_medium like '%criteo%';

#SOCIAL MEDIA
UPDATE SEM.campaign_ad_group_mx SET channel='Facebook Referral' WHERE 
(source_medium='facebook.com / referral' OR source_medium='m.facebook.com / referral' OR source_medium='Banamex-deb / socialmedia' OR 
source_medium='SocialMedia / FacebookVoucher') AND 
(source_medium<>'facebook / socialmediaads' AND source_medium<>'facebook / retargeting');
UPDATE SEM.campaign_ad_group_mx SET channel='Twitter Referral' WHERE source_medium='t.co / referral' OR source_medium='twitter / socialmedia';
UPDATE SEM.campaign_ad_group_mx SET channel='FB Posts'
 WHERE source_medium='facebook / socialmedia' OR source_medium='facebook / (not set)';
UPDATE SEM.campaign_ad_group_mx SET channel='March 10 FB Video' WHERE source_medium='MKT1SWKug';
UPDATE SEM.campaign_ad_group_mx SET channel='March 22 FB Video' WHERE source_medium='MKT1IIuzu' OR source_medium='MKT1WBY1T';

UPDATE SEM.campaign_ad_group_mx
set channel='FB MP' 
WHERE campaign like '%!_MP' escape '!' and source_medium='facebook / socialmedia';

UPDATE SEM.campaign_ad_group_mx 
set channel='FBF MP' 
WHERE campaign like '%!_MP' escape '!' and source_medium='facebook / socialmedia_fashion';

UPDATE SEM.campaign_ad_group_mx
set channel='FB Ads MP' 
WHERE campaign like '%!_MP' escape '!' and source_medium='facebook / socialmediaads';

UPDATE SEM.campaign_ad_group_mx 
set channel='NL MP' 
WHERE campaign like '%!_MP' escape '!' and source_medium='postal / CRM';

UPDATE SEM.campaign_ad_group_mx 
set channel='SEM Branded MP' 
WHERE campaign like 'brandb.%!_MP' escape '!' and source_medium='google / cpc';

#SERVICIO AL CLIENTE
UPDATE SEM.campaign_ad_group_mx SET channel='Inbound' WHERE source_medium like 'Tele%' or source_medium like '%inbound%';
UPDATE SEM.campaign_ad_group_mx SET channel='OutBound' WHERE source_medium like 'REC%' or source_medium like '%outbound%';
UPDATE SEM.campaign_ad_group_mx SET channel='CCEMP Vouchers' WHERE source_medium like 'CCEMP%';

#SEO
UPDATE SEM.campaign_ad_group_mx SET channel='Search Organic' WHERE source_medium like '%organic' OR source_medium='google.com.co / referral';

#NEWSLETTER
UPDATE SEM.campaign_ad_group_mx 
set Channel='Newsletter' 
WHERE (source_medium like '%CRM' 
OR source_medium like '%email'
OR source_medium like 'Email Marketing%' 
OR source_medium like 'postal%') 
AND (source_medium<>'TeleSales / CRM' 
AND source_medium<>'CDEAL / email' 
AND source_medium<>'Cart_Recovery / CRM');

UPDATE SEM.campaign_ad_group_mx 
set Channel='CAC Campaign' WHERE
source_medium='MKTNfue3n' OR
source_medium='MKT0i1Gq2' OR
source_medium='MKT0xfgVK' OR
source_medium='MKT1eDvI7' OR
source_medium='MKTB0TZfz' OR
source_medium='MKTxmsjvs' OR
source_medium='MKTB0TZfz' OR
source_medium='MKT3TcOf8' OR
source_medium='MKT063HRE' OR
source_medium='MKTqqU8KR' OR
source_medium='MKT7lSkUd' OR
source_medium='MKT1pFyS6' OR
source_medium='MKTNfue3n' OR
source_medium like 'CAC%';

update SEM.campaign_ad_group_mx
set channel = 'NL CAC'
where source_medium like '%CRM'
and campaign like '%cac%';

#OTHER
UPDATE SEM.campaign_ad_group_mx SET channel='Exchange' WHERE source_medium like 'CCE%';
UPDATE SEM.campaign_ad_group_mx SET channel='Content Mistake' WHERE source_medium like 'CON%';
UPDATE SEM.campaign_ad_group_mx SET channel='Procurement Mistake' WHERE source_medium like 'PRC%';
UPDATE SEM.campaign_ad_group_mx SET channel='Devoluciones' WHERE source_medium like 'OPS%';
UPDATE SEM.campaign_ad_group_mx SET channel='IT Vouchers' WHERE source_medium like 'IT%';
UPDATE SEM.campaign_ad_group_mx SET channel='Employee Vouchers' WHERE (source_medium like 'TE%' AND source_medium<>'TeleSales / CRM')  
OR source_medium like 'EA%' OR source_medium like 'LIN%';

UPDATE SEM.campaign_ad_group_mx set Channel='AMEX' WHERE source_medium like 'AMEX%';

UPDATE SEM.campaign_ad_group_mx SET channel='PAR Voucher' WHERE source_medium like 'PAR%';

#DIRECT
UPDATE SEM.campaign_ad_group_mx SET channel='Linio.com Referral' WHERE  
source_medium='linio.com.co / referral' OR
source_medium='linio.com / referral' OR
source_medium='linio.com.mx / referral' OR
source_medium='linio.com.ve / referral' OR
source_medium='linio.com.pe / referral' OR
source_medium='www-staging.linio.com.ve / referral';

UPDATE SEM.campaign_ad_group_mx SET channel='Directo / Typed' WHERE source_medium='(direct) / (none)' OR 
source_medium like '%direct%none%';

#OFFLINE
UPDATE SEM.campaign_ad_group_mx SET channel='PR vouchers' WHERE source_medium like 'PR%';
UPDATE SEM.campaign_ad_group_mx SET channel='Flyers' WHERE source_medium='regalolinio200' OR
source_medium='regalo200' OR source_medium='descuento20';
UPDATE SEM.campaign_ad_group_mx SET channel='SMS' WHERE source_medium='madre';
UPDATE SEM.campaign_ad_group_mx SET channel='Other Offline' Where
source_medium='CATALOGO' or source_medium='descuento20' or source_medium like '%metro%' 
or source_medium like '%sms%';

#AFFILIATES
UPDATE SEM.campaign_ad_group_mx SET channel='Descuentomx' WHERE source_medium like 'dscuentomx / affiliates';
UPDATE SEM.campaign_ad_group_mx SET channel='Mercado Libre Affliates' WHERE source_medium like 'Mercadolibre / Affiliates';
UPDATE SEM.campaign_ad_group_mx SET channel='Glg' WHERE source_medium='glg / Affiliates';
UPDATE SEM.campaign_ad_group_mx SET channel='Clickmagic' where source_medium='Clickmagic / Affiliates';
UPDATE SEM.campaign_ad_group_mx SET channel='Trade Tracker' where source_medium='tradetracker / Affiliates';
UPDATE SEM.campaign_ad_group_mx SET channel='Dscuento' where source_medium='MKTDscuento' or source_medium='dscuento.com.mx / referral';
UPDATE SEM.campaign_ad_group_mx SET channel='Referral P.' where source_medium LIKE 'cbit%';
UPDATE SEM.campaign_ad_group_mx SET channel='Cupones Magicos' where source_medium='cuponesmagicos.com.mx / referral';

#CORPORATE SALES
UPDATE SEM.campaign_ad_group_mx SET channel='Corporate Sales' WHERE source_medium like 'CDEAL%';

#OFFLINE

#N/A
UPDATE 
SEM.campaign_ad_group_mx SET channel='Unknown-No voucher' 
WHERE channel is null;
UPDATE SEM.campaign_ad_group_mx SET 
channel='Unknown-Voucher' WHERE channel='' AND source_medium<>'' ;

#Channel Report Extra Stuff to define Channel---
#Venta corporativa 

UPDATE SEM.campaign_ad_group_mx SET channel='Partnerships' where source_medium like '%partnership%';

#Partnerships Channel Extra

UPDATE SEM.campaign_ad_group_mx SET channel='Mercado Libre Voucher' WHERE channel like 'MKTMeLi%';

#Mercado libre Orders 

UPDATE SEM.campaign_ad_group_mx SET channel='Mercado Libre Voucher' WHERE channel like 'MKTMeLi%';
#OFFLINE
UPDATE SEM.campaign_ad_group_mx SET channel='PR vouchers' WHERE channel like 'PR%';

#DEFINE CHANNEL GROUP

#Facebook Ads Group
UPDATE SEM.campaign_ad_group_mx SET channel_group='Facebook Ads' 
WHERE channel='Facebook Ads' or channel = 'FB Ads MP';

#SEM Group
UPDATE SEM.campaign_ad_group_mx SET channel_group='Search Engine Marketing' WHERE channel='SEM';

#Social Media Group
UPDATE SEM.campaign_ad_group_mx SET 
channel_group='Social Media' WHERE channel='Facebook Referral' 
OR channel='Twitter Referral' or channel='FB Posts'
 or channel='March 10 FB Video' OR channel='March 22 FB Video'
or channel = 'FB MP' or channel = 'FBF MP';

#Retargeting Group
UPDATE SEM.campaign_ad_group_mx SET channel_group='Retargeting' WHERE channel='Sociomantic' or source_medium like '%retargeting' OR 
channel='Cart Recovery' OR channel='Vizury' OR 'Facebook R.' OR channel = 'VEInteractive' OR channel = 'Criteo' or channel='Triggit';

#GDN group
UPDATE SEM.campaign_ad_group_mx SET channel_group='Google Display Network' WHERE channel like 'Google Display Network'; 

#NewsLetter Group
UPDATE SEM.campaign_ad_group_mx SET channel_group='NewsLetter'
 WHERE channel='Newsletter' OR channel='CAC Campaign' or channel = 'NL CAC'
or channel = 'NL MP';

#SEO Group
UPDATE SEM.campaign_ad_group_mx SET channel_group='Search Engine Optimization' WHERE channel='Search Organic';

#Partnerships Group

UPDATE SEM.campaign_ad_group_mx SET channel_group='Other (identified)' WHERE channel='Santander' OR channel='PAR Voucher' OR 
channel='AMEX' or channel='Banamex' OR channel='Dafiti' OR channel='Bancomer' OR channel='American Express' OR
channel='Banorte' or channel='PayPal' or channel='Partnerships';


#Corporate Sales Group
UPDATE SEM.campaign_ad_group_mx SET channel_group='Other (identified)' WHERE channel='Corporate Sales';

#Offline Group
UPDATE SEM.campaign_ad_group_mx SET channel_group='Offline Marketing' WHERE channel='PR vouchers' or channel='Flyers' 
or channel='SMS' or channel='Other Offline';

#Tele Sales Group
UPDATE SEM.campaign_ad_group_mx SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales' OR channel='CCEMP Vouchers';

#Branded Group
UPDATE SEM.campaign_ad_group_mx SET channel_group='Branded' 
WHERE channel='Linio.com Referral' OR channel='Directo / Typed' OR
channel='SEM Branded' or channel = 'SEM Branded MP';

#ML Group
UPDATE SEM.campaign_ad_group_mx SET channel_group='Mercado Libre' WHERE channel='Mercado Libre Referral' OR channel='Mercado Libre Voucher' OR channel='Otros - Mercado Libre';

#Blog Group
-- UPDATE SEM.campaign_ad_group_mx SET channel_group='Blogs' WHERE channel='Blog Linio';

#Affiliates Group
UPDATE SEM.campaign_ad_group_mx SET channel_group='Affiliates' WHERE channel='Buscape' OR channel='Descuentomx' 
OR channel='Mercado Libre Affliates' OR channel='Glg' or channel='Pampa Network' OR channel='Clickmagic' OR 
channel='Trade Tracker' OR channel='Dscuento' OR channel='Referral P.' OR channel='Cupones Magicos';

#Other (identified) Group
UPDATE SEM.campaign_ad_group_mx SET channel_group='Other (identified)' 
WHERE channel='Referral Sites' OR channel='Reactivation Letters' 
OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' OR
channel='Exchange' OR
channel='Content Mistake' OR
channel='Procurement Mistake' OR
channel='Employee Voucher' OR channel='IT Vouchers';

#Extra Channel Group

update SEM.campaign_ad_group_mx set channel_group='Social Media' where source_medium = 'FB / FB';
update SEM.campaign_ad_group_mx set channel_group='Social Media' where source_medium = 'Facebook / social media';
update SEM.campaign_ad_group_mx set channel_group='Google Display Network' where source_medium = 'google / Display';
update SEM.campaign_ad_group_mx set channel_group='Newsletter' where source_medium = 'Postal / (not set)';
update SEM.campaign_ad_group_mx set channel_group='Other (identified)' where source_medium = 'sclaberinto.blogspot.mx / referral';
update SEM.campaign_ad_group_mx set channel_group='Other (identified)' where source_medium = 'plus.url.google.com / referral';
update SEM.campaign_ad_group_mx set channel_group='Social Media' where source_medium = 'SocialMedia / FacebookVoucher';
update SEM.campaign_ad_group_mx set channel_group='Partnerships' where source_medium = 'Publimetro / Partnership';
update SEM.campaign_ad_group_mx set channel_group='Google Display Network' where source_medium = 'MetrosCubicos / Retargeting';
update SEM.campaign_ad_group_mx set channel_group='Other (identified)' where source_medium = 'mail.google.com / referral';
update SEM.campaign_ad_group_mx set channel_group='Affiliates' where source_medium = 'Affiliates / Mercadolibre';
update SEM.campaign_ad_group_mx set channel_group='Affiliates' where source_medium = 'Cuponzote / Affiliates';
update SEM.campaign_ad_group_mx set channel_group='Affiliates' where source_medium = 'Google_Shopping / Price Comparison';
update SEM.campaign_ad_group_mx set channel_group='Tele Sales' where source_medium = 'CS / Inbound';
update SEM.campaign_ad_group_mx set channel_group='Other (identified)' where source_medium = 'youtube / socialmedia';
update SEM.campaign_ad_group_mx set channel_group='Tele Sales' where source_medium = 'CS / Outbound';
update SEM.campaign_ad_group_mx set channel_group='Other (identified)' where source_medium = 'pagead2.googlesyndication.com / referral';
update SEM.campaign_ad_group_mx set channel_group='Affiliates' where source_medium = 'tradetracker / Affiliates';
update SEM.campaign_ad_group_mx set channel_group='Affiliates' where source_medium = 'Clickmagic / Affiliates';

#Unknown Group
UPDATE SEM.campaign_ad_group_mx SET channel_group='Non identified' WHERE channel='Unknown-No voucher' OR channel='Unknown-Voucher';

#Define Channel Type---
#TO BE DONE

#Ownership

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `channel_report`()
BEGIN
#Channel Report MX---

update production.tbl_order_detail t inner join SEM.transaction_id_mx c on t.order_nr=c.transaction_id set t.source_medium= concat(c.source,' / ',c.medium),t.campaign=c.campaign where source_medium is null;

#Define channel---
##UPDATE tbl_order_detail SET source='',channel='', channel_group='';
#Voucher is priority statemnt
UPDATE tbl_order_detail SET source='' where source is null;
UPDATE tbl_order_detail SET campaign='' where campaign is null;
UPDATE tbl_order_detail SET source=coupon_code WHERE coupon_code is not null;
UPDATE tbl_order_detail SET source=source_medium WHERE (coupon_code is null or coupon_code = '');
UPDATE tbl_order_detail SET source=source_medium WHERE
coupon_code like 'NL%' OR coupon_code like 'NR%' OR coupon_code like
'COM%' OR coupon_code like 'BNR%';
UPDATE tbl_order_detail SET source=source_medium where channel_group='Non Identified';
#If voucher is empty use GA source


#NEW REGISTERS
UPDATE tbl_order_detail SET channel='New Register' 
WHERE (source like 'NL%' OR source like 'NR%' OR source like
'COM%' OR source like 'BNR%') AND source_medium is null;



#BLOG
-- UPDATE tbl_order_detail SET channel='Blog Linio' WHERE source like 'blog%';

#REFERRAL
UPDATE tbl_order_detail SET channel='Referral Sites' WHERE source like '%referral' 
AND source<>'facebook.com / referral'
AND source<>'m.facebook.com / referral'
AND source<>'linio.com.co / referral'
AND source<>'linio.com / referral'
AND source<>'linio.com.mx / referral'
AND source<>'google.co.ve / referral'
AND source<>'linio.com.ve / referral'
AND source<>'linio.com.pe / referral'
AND source<>'www-staging.linio.com.ve / referral'
AND source<>'google.com / referral'
AND source<>'.com.ve / referral'
AND source<>'blog.linio.com.ve / referral' AND source<>'t.co / referral'
AND source not like '%google%' AND source not like '%blog%' and source not like '%pampa%'
and source not like '%buscape%';


UPDATE tbl_order_detail SET channel='Buscape' 
WHERE source='Buscape / Price Comparison' OR source like 'buscape%' or source='tracker.buscape.com.mx / referral';

#FB ADS
UPDATE tbl_order_detail SET channel='Facebook Ads' WHERE source='facebook / socialmediaads' OR
source like '%book%ads%' or source='facebook / (not set)';

UPDATE tbl_order_detail set channel= 'Dark Post' where campaign like '%darkpost%';

#SEM 
UPDATE tbl_order_detail SET channel='SEM' WHERE source='google / cpc' AND (campaign not like '%linio%' AND 
campaign not like 'er.%' AND campaign not like 'brandb%');
UPDATE tbl_order_detail SET channel='Bing' where source like '%bing%';
UPDATE tbl_order_detail SET channel='SEM Branded' WHERE (source='google / cpc' or source like '%bing%') AND campaign like 'brandb%';
UPDATE tbl_order_detail SET channel='Other Branded' WHERE source = '201.151.86.164' or source ='10.0.0.9' or source = 'www-staging.linio.com.mx' or source='blog.linio.com.mx' or source='213.229.186.15';

#GDN
UPDATE tbl_order_detail SET channel='Google Display Network' WHERE source='google / cpc' AND 
(campaign like 'r.%' or campaign like '[D%' 
or campaign like 'er.%' or campaign like '%d.%'
or campaign like '|R%'
or campaign like '|C%');

#RETARGETING
UPDATE tbl_order_detail set channel='Other Retargeting' where source like '%retargeting%';
UPDATE tbl_order_detail SET channel='Sociomantic' WHERE source='sociomantic / (not set)' 
OR source='sociomantic / retargeting' OR source 
like '%socioman%' or source='facebook / retargeting';
UPDATE tbl_order_detail SET channel='Cart Recovery' WHERE source='Cart_Recovery / CRM';
UPDATE tbl_order_detail SET channel='Vizury' WHERE source='vizury / retargeting' OR source like '%vizury%';
#UPDATE tbl_order_detail SET channel='Facebook R.' WHERE source='facebook / retargeting';
UPDATE tbl_order_detail SET channel='VEInteractive' WHERE source like '%VEInteractive%';
UPDATE tbl_order_detail SET channel='Main ADV' where source = 'retargeting / mainadv' or source like '%mainadv%' or source like '%solocpm%';
UPDATE tbl_order_detail SET channel='AdRoll' where source like '%adroll%';
UPDATE tbl_order_detail SET channel='My Things' where source like '%mythings%';
UPDATE tbl_order_detail SET channel='Avazu' where source like '%avazu%';
UPDATE tbl_order_detail SET channel='Triggit' where source like '%triggit / retargeting%';


#Criteo
UPDATE tbl_order_detail set channel='Criteo' where source like '%criteo%';

#SOCIAL MEDIA
UPDATE tbl_order_detail SET channel='Facebook Referral' WHERE 
(source='facebook.com / referral' OR source='m.facebook.com / referral' OR source='Banamex-deb / socialmedia' OR 
source='SocialMedia / FacebookVoucher') AND 
(source<>'facebook / socialmediaads' AND source<>'facebook / retargeting');
UPDATE tbl_order_detail SET channel='Twitter Referral' WHERE source='t.co / referral';
UPDATE tbl_order_detail SET channel='FB Posts' WHERE source='facebook / socialmedia' or source='twitter / socialmedia';
UPDATE tbl_order_detail SET channel='March 10 FB Video' WHERE source='MKT1SWKug';
UPDATE tbl_order_detail SET channel='March 22 FB Video' WHERE source='MKT1IIuzu' OR SOURCE='MKT1WBY1T';
UPDATE tbl_order_detail SET channel='Youtube' where source='youtube / socialmedia';

#SERVICIO AL CLIENTE
UPDATE tbl_order_detail set channel='Other Tele Sales' where source = 'CS /%' or source = '%/ CS' or source like 'telesales /%' or source like '%/ telesales'; 
UPDATE tbl_order_detail SET channel='Inbound' WHERE source like 'Tele%' or source like '%inbound%';
UPDATE tbl_order_detail SET channel='OutBound' WHERE source like 'REC%' or source like '%outbound%';
UPDATE tbl_order_detail SET channel='CCEMP Vouchers' WHERE source like 'CCEMP%';

#SEO
UPDATE tbl_order_detail SET channel='Search Organic' WHERE source like '%organic' OR source='google.com.mx / referral';

UPDATE tbl_order_detail SET channel='Search Organic' where source like 'google.%' and source like '%referral';

UPDATE tbl_order_detail SET channel='Yahoo Answers' where source like '%mx.answers.yahoo.com%';

#NEWSLETTER
UPDATE tbl_order_detail set Channel='Newsletter' WHERE 
(source like '%CRM' OR source like '%email' OR source like 'Email Marketing%' or source like 'postal%') AND 
(source<>'TeleSales / CRM' AND source<>'CDEAL / email' AND source<>'Cart_Recovery / CRM');
UPDATE tbl_order_detail set Channel='NL CAC' WHERE
source='MKTNfue3n' OR
source='MKT0i1Gq2' OR
source='MKT0xfgVK' OR
source='MKT1eDvI7' OR
source='MKTB0TZfz' OR
source='MKTxmsjvs' OR
source='MKTB0TZfz' OR
source='MKT3TcOf8' OR
source='MKT063HRE' OR
source='MKTqqU8KR' OR
source='MKT7lSkUd' OR
source='MKT1pFyS6' OR
source='MKT7tavt8' OR
source='MKT0uJXni' OR
source='MKT0pTUbn' OR
source='MKT1ORfvU' OR
source='MKT1Z54Lh' OR
source='MKTf4A6Sk' OR
source='MKT0xfgVK' OR
source='MKT1eDvI7' OR
source='MKTxmsjvs' OR
source='MKTB0TZfz' OR
source='MKT3TcOf8' OR
source='MKT063HRE' OR
source='MKTqqU8KR' OR
source='MKTaudifonosfb1' OR
source='MKTfDc7gLi' OR
source='MKTJ97' OR
source='MKTM36' OR
source='MKTM50' OR
source='MKTM90' OR
source like 'CAC%';

UPDATE tbl_order_detail set channel='FB Ads CAC' WHERE (coupon_code like 'CAC%') and source_medium='facebook / socialmediaads';
UPDATE tbl_order_detail set channel='FB CAC' WHERE (coupon_code like 'CAC%') and source_medium='facebook / socialmedia';
UPDATE tbl_order_detail set channel='NL CAC' WHERE coupon_code like 'CAC%' and source_medium='postal / crm';
UPDATE tbl_order_detail 
set channel='FB MP' 
WHERE campaign like '%_!MP' escape '!' and source_medium='facebook / socialmedia';

UPDATE tbl_order_detail 
set channel='FBF MP' 
WHERE campaign like '%_!MP' escape '!' and source_medium='facebook / socialmedia_fashion';

UPDATE tbl_order_detail 
set channel='FB Ads MP' 
WHERE campaign like '%_!MP' escape '!' and source_medium='facebook / socialmediaads';

UPDATE tbl_order_detail 
set channel='NL MP' 
WHERE campaign like '%_!MP' escape '!' and source_medium='postal / CRM';

UPDATE tbl_order_detail 
set channel='SEM Branded MP' 
WHERE campaign like 'brandb.%_!MP' escape '!' and source_medium='google / cpc';



#OTHER
UPDATE tbl_order_detail SET channel='Exchange' WHERE source like 'CCE%';
UPDATE tbl_order_detail SET channel='Content Mistake' WHERE source like 'CON%';
UPDATE tbl_order_detail SET channel='Procurement Mistake' WHERE source like 'PRC%';
UPDATE tbl_order_detail SET channel='Devoluciones' WHERE source like 'OPS%';
UPDATE tbl_order_detail SET channel='IT Vouchers' WHERE source like 'IT%';
UPDATE tbl_order_detail SET channel='Employee Vouchers' WHERE (source like 'TE%' AND source<>'TeleSales / CRM')  
OR source like 'EA%' OR source like 'LIN%';

#PARTNERSHIPS

UPDATE tbl_order_detail SET channel='Partnerships' where source like '%Partnership%' or coupon_code like 'PAR%';

UPDATE tbl_order_detail SET channel='Other Partnerships' where source like '%PARTNERSHIP%' or coupon_code like '%PARTNERSHIP%';

UPDATE tbl_order_detail SET channel='Buen Fin 2013' where campaign like '%BuenFin2013%';

UPDATE tbl_order_detail SET channel='Banorte' WHERE source like '%banorte%' or (description_voucher like '%banorte%'
and coupon_code=source) or source like '%Banorteixe%' or coupon_code in ('COM1gn4kh', 
'COMOeznfM', 
'COM1aJa4q', 
'COM0E6Dlp', 
'COMqSU7M5',
'COM0t9PWA',
'COM1ssnpB', 
'COM0A22dO',
'COM4FkI2i');
UPDATE tbl_order_detail SET channel='Santander' WHERE source='MKTExFiiv' OR source like '%Santander-deb%' OR
source='MKTsaHAD2' OR
source='MKT6bucMW' OR
source='MKT0v8y8X' OR
source='MKTulPG5T' OR
source='MKTx6OGcF' OR
source='MKT0bhnLc' OR
source='MKT0iZjZS' OR
source='MKT0Akg6y' OR
source='MKTn6RcCs' OR
source='MKT1ft96n' OR
source='MKTnwM00A' OR
source='MKTph4cQn' OR
source='MKTHs9mG1' OR
source='MKTL38fvT' OR
source='MKT111GV9' OR
source='MKT59kWnR' OR
source='MKTXukY1p' OR source like '%Santander%' OR source='MKT1H8uB8' OR
source='MKT1A3rIA' OR 
source='MKT1RwDN6' OR 
source='MKTJ5YgZz' OR 
source='MKTyJLtQA' OR 
source='MKT0LcYyg' OR 
source='MKT15sqy8' OR 
source='MKT1H8uB8' OR 
source='MKTHyUt98' OR 
source='MKT1VK5uv' OR 
source='MKTsCydnL' OR 
source='MKT1aTNQc' OR 
source='MKT1aTCNt' OR 
source='MKT17v5KB' OR 
source='MKT0StMG8' OR 
source='MKTxQGoWi'
OR (description_voucher like '%santander%' and coupon_code=source);
UPDATE tbl_order_detail set Channel='AMEX' WHERE source like 'AMEX%';

UPDATE tbl_order_detail SET channel='PAR Voucher' WHERE source like 'PAR%';
UPDATE tbl_order_detail SET channel='Banamex' WHERE 
source='Banamex / Partnership' or source like '%Banamex-deb%' OR source='MKTfXPuRa' OR source='MKT1KzeNi' OR source='MKT0plcnf' OR
source='MKT0YW4bI' OR
source='MKT1kgmm6' OR
source='MKTTLQ8QP' OR
source='MKTpgBEQv' OR
source='MKTevO4UN' OR
source='MKT0yvpr6' OR
source='MKTgUOaDm' OR
source='MKT1KzeNi' OR
source='MKT0plcnf' OR
source='MKTfXPuRa' OR source like '%banamex%'
 or (description_voucher like '%banamex%' and coupon_code=source) or coupon_code in ('COMWAnHqk',
'COMj6VdXm',
'COMleM6yw',
'COMvacYUl',
'COM0quw9T',
'COM1od8oi',
'COM03A4rf');
UPDATE tbl_order_detail SET channel='Dafiti' WHERE source='Dafiti / Partnership' OR
(description_voucher like '%dafiti%' and coupon_code=source);
UPDATE tbl_order_detail SET channel='Bancomer' WHERE 
source='MKT0qnqj3' OR
source='MKT1mR3we' OR
source='MKTHGWq1T' OR
source='MKT1LPgtD' OR
source='MKTQzGnNQ' OR
source='MKT1mlElQ' OR source='MKT0qnqj3' OR source like '%bancomer%' or 
(description_voucher like '%bancomer%' and coupon_code=source) or coupon_code in ('COMbB6AKv',
'COMQV1fvj',
'COMk26IZN',
'COMl3tBhE',
'COM0bKAkK',
'COMwmfJAH',
'COMyIDag2',
'COMKXInFy',
'COMpfewAF',
'COM9ESnZw',
'COM0SrC75',
'COMQTbq9s',
'COMDaXNVy',
'COMq62YER',
'COM1HTYw1',
'COM1tqcJ4',
'COMKwqYxd',
'COMBqk10S',
'COMVRMweB',
'COMsd1D5W',
'COM08NaXX',
'COMTHhcfh',
'COMF5vDPA',
'COM09ykHd',
'COMtgSSOc',
'COMeOoRqS',
'COMo8FaTg',
'COMDfv9ii',
'COM0reEj7',
'COMMrwW8l',
'COMekuAx9',
'COM1ipcME',
'COMmmJv5p',
'COMYIIvGh');

UPDATE tbl_order_detail SET channel='American Express' WHERE 
source='AMEX1cqUNp' OR
source='AMEX0JmHIq' OR
source='AMEX4xvQ53' OR
source='AMEX7lwlWV' OR
source='AMEXSoybiO' or 
(description_voucher like '%american%express' and coupon_code=source);

UPDATE tbl_order_detail SET channel='PayPal' WHERE source like '%paypal%' or 
(description_voucher like '%paypal%' and coupon_code=source);



UPDATE tbl_order_detail set channel='Corporate Deal' where coupon_code in ('Intercam','CDEALBW','CDEALGS','CDEALcrecer','CDEALMV','LYTtTtn8Q','CDEALFUNKY','CDEALTW','CDEALGH','CDEALSMG', 'CDEALP001');

UPDATE tbl_order_detail set channel='Corporate Deal' where coupon_code LIKE 'CDEAL%' or source_medium like '%CORPORATESALES%';

UPDATE tbl_order_detail set channel='Bank' where coupon_code in('MKT0wnkzE',
'MKTkkyQnk',
'MKT1BY8UY',
'MKTMblVuX',
'MKT0tnUuL',
'MKT1mFB5t',
'MKTM84jXQ',
'MKT0iQEbB',
'MKTOTOgvc',
'MKTOggLi5',
'MKTYMZaRd',
'MKT0N6Uy6',
'MKT0ThhQd',
'MKTiuuHiD',
'MKTUb9lgI',
'MKTzNa59o',
'MKT0aF3I6',
'MKTxmmIGM',
'MKTpaypal6',
'MKT010yoY',
'MKTFQsEjD',
'MKTUbO0ZH',
'MKTxYYAFX',
'MKT0UC4gA',
'MKT6MveDC',
'MKT154hDI',
'MKTeCmqxS',
'COMtNBgtx',
'COM1XeXow',
'COM1yS69N',
'COM12vTGP',
'COM0Szfz1',
'COMsq3XPZ',
'COMOhGprT',
'COMAtSA4A',
'COMTacDRh',
'COMoW24Lj',
'COM02XwgL',
'MKTFM9Ojf',
'MKT078Io6',
'MKTE4UpIP',
'MKT1uaPuh',
'MKT1KOf1T',
'MKT1FtOWs',
'MKTpiP20I',
'MKTNX01Vo',
'MKT5A0vss',
'COMq2yXkS',
'COMKImnUu',
'COMrwMzLv',
'COMLbrM8B',
'COMe6sShP',
'COM1PQNCd',
'MKTFQReMz',
'MKTCPDLKa',
'MKTxmfcd7',
'MKT0CMpgL',
'MKT1R2DZ0',
'MKT00fpoM',
'COMtNBgtx',
'COM1XeXow',
'COM1yS69N',
'COM12vTGP',
'COM0Szfz1',
'COMsq3XPZ',
'COMOhGprT',
'COMAtSA4A',
'COMTacDRh',
'COMoW24Lj',
'COM02XwgL',
'COM0u62rt',
'COM0jwWC4',
'COMEtA71y',
'COMvIVok2',
'COM0iGdhI',
'COMrrk8t6',
'COMa1U08u',
'COM0FQm0F',
'COM12PSUn',
'COMIONFTE',
'COM0u3tio',
'COM1nejSI',
'COM1LQ2c9');

#DIRECT
UPDATE tbl_order_detail SET channel='Linio.com Referral' WHERE  
source='linio.com.co / referral' OR
source='linio.com / referral' OR
source='linio.com.mx / referral' OR
source='linio.com.ve / referral' OR
source='linio.com.pe / referral' OR
source='www-staging.linio.com.ve / referral';

UPDATE tbl_order_detail SET channel='Directo / Typed' WHERE source='(direct) / (none)' OR 
source like '%direct%none%';

#OFFLINE
UPDATE tbl_order_detail SET channel='PR vouchers' WHERE source like 'PR%';
UPDATE tbl_order_detail SET channel='Flyers' WHERE source='regalolinio200' OR
source='regalo200' OR source='descuento20';
UPDATE tbl_order_detail SET channel='SMS' WHERE source='madre' or source like '%Partnership/sms%';
UPDATE tbl_order_detail SET channel='Other Offline' Where
source='CATALOGO' or source='descuento20' or source like '%metro%' 
or source like '%sms%';
UPDATE tbl_order_detail set channel='Linio Flyers' where coupon_code in ('descuento20','regalo200');
UPDATE tbl_order_detail set channel='Bank Flyers' where coupon_code in ('BANIXE01','PAYPAL150');
UPDATE tbl_order_detail set channel='SMS' where coupon_code in ('PADRE','REGRESA','REGRESO','REGRESA1');
UPDATE tbl_order_detail set channel='Partnership Flyers' where coupon_code in ('taxi','regalolinio100','regalolinio150');

UPDATE tbl_order_detail set channel='NUEVO' where coupon_code='NUEVO';
UPDATE tbl_order_detail set channel='EXCLUSIVIDAD' where coupon_code='EXCLUSIVIDAD';
UPDATE tbl_order_detail set channel='regalolinio100' where coupon_code='regalolinio100';
UPDATE tbl_order_detail set channel='regalolinio150' where coupon_code='regalolinio150';
UPDATE tbl_order_detail set channel='regreso' where coupon_code='regreso';
UPDATE tbl_order_detail set channel='regresa' where coupon_code='regresa';
UPDATE tbl_order_detail set channel='regresa1' where coupon_code='regresa1';
UPDATE tbl_order_detail set channel='taxi' where coupon_code='taxi';
UPDATE tbl_order_detail set channel='BANIXE01' where coupon_code='BANIXE01';
UPDATE tbl_order_detail set channel='BANIXE02' where coupon_code='BANIXE02';
UPDATE tbl_order_detail set channel='BANIXE03' where coupon_code='BANIXE03';
UPDATE tbl_order_detail set channel='BANIXE04' where coupon_code='BANIXE04';
UPDATE tbl_order_detail set channel='PAYPAL150' where coupon_code='PAYPAL150';
UPDATE tbl_order_detail set channel='SAN200' where coupon_code='SAN200';
UPDATE tbl_order_detail set channel='LIN120' where coupon_code='LIN120';
UPDATE tbl_order_detail set channel='regalo150' where coupon_code='regalo150';
UPDATE tbl_order_detail set channel='linio150' where coupon_code='linio150';
UPDATE tbl_order_detail set channel='linio5' where coupon_code='linio5';
UPDATE tbl_order_detail set channel='linio220' where coupon_code='linio220';
UPDATE tbl_order_detail set channel='linio10' where coupon_code='linio10';
UPDATE tbl_order_detail set channel='Bridgestone150' where coupon_code='Bridgestone150';
UPDATE tbl_order_detail set channel='N80' where coupon_code='N80';
UPDATE tbl_order_detail set channel='N120' where coupon_code='N120';
UPDATE tbl_order_detail set channel='N200' where coupon_code='N200';
UPDATE tbl_order_detail set channel='soriana120' where coupon_code='soriana120';
UPDATE tbl_order_detail set channel='soriana100' where coupon_code='soriana100';
UPDATE tbl_order_detail set channel='BMX100' where coupon_code='BMX100';
UPDATE tbl_order_detail set channel='BMX150' where coupon_code='BMX150';
UPDATE tbl_order_detail set channel='BANIXE04' where coupon_code='BANIXE04';
UPDATE tbl_order_detail set channel='BBVA150' where coupon_code='BBVA150';
UPDATE tbl_order_detail set channel='FINDE' where coupon_code='FINDE';
UPDATE tbl_order_detail set channel='VUELA' where coupon_code='VUELA';
UPDATE tbl_order_detail set channel='OFERTA150' where coupon_code='OFERTA150';
UPDATE tbl_order_detail set channel='DIC150' where coupon_code='DIC150';
UPDATE tbl_order_detail set channel='SMS100' where coupon_code='SMS100';
UPDATE tbl_order_detail set channel='SMS200' where coupon_code='SMS200';
UPDATE tbl_order_detail set channel='SMS300' where coupon_code='SMS300';
UPDATE tbl_order_detail set channel='LIN1' where coupon_code='LIN1';
UPDATE tbl_order_detail set channel='LIN2' where coupon_code='LIN2';
UPDATE tbl_order_detail set channel='LIN3' where coupon_code='LIN3';
UPDATE tbl_order_detail set channel='MODA100' where coupon_code='MODA100';
UPDATE tbl_order_detail set channel='MODA200' where coupon_code='MODA200';
UPDATE tbl_order_detail set channel='MODA300' where coupon_code='MODA300';


#AFFILIATES
UPDATE tbl_order_detail SET channel='Other Affiliates' where source like '%affiliate%' or source like '%afiliate%';
UPDATE tbl_order_detail SET channel='Descuentomx' WHERE source like 'dscuentomx / affiliates' or source like 'dscuento.com.mx%';
UPDATE tbl_order_detail SET channel='Mercado Libre Affliates' WHERE source like 'Mercadolibre / Affiliates';
UPDATE tbl_order_detail SET channel='Glg' WHERE source='glg / Affiliates';
UPDATE tbl_order_detail SET channel='Clickmagic' where source='Clickmagic / Affiliates';
UPDATE tbl_order_detail SET channel='Trade Tracker' where source='tradetracker / Affiliates';
UPDATE tbl_order_detail SET channel='Dscuento' where source='MKTDscuento' or source='dscuento.com.mx / referral';
UPDATE tbl_order_detail SET channel='Cupones Magicos' where source='cuponesmagicos.com.mx / referral';
UPDATE tbl_order_detail set channel='Pampa Network' where source like '%pampa%';
UPDATE tbl_order_detail set channel='SoloCPM' where source like '%solocpm%';
UPDATE tbl_order_detail set channel='Promodescuentos' where source like '%promodescuentos%';

#Pago Movil
UPDATE tbl_order_detail SET channel='Pago Movil' where source like '%Pagomovil/partnership%' or campaign like '%pagomovil2013%';

#El Universal
UPDATE tbl_order_detail SET channel='El Universal' where source like '%el-universal%';

#CORPORATE SALES
UPDATE tbl_order_detail SET channel='Corporate Sales' WHERE source like 'CDEAL%';

#N/A
UPDATE 
tbl_order_detail SET channel='Unknown-No voucher' 
WHERE source_medium='' or source_medium is null;
UPDATE tbl_order_detail SET 
channel='Unknown-Voucher' WHERE channel='' AND source<>'';

#Channel Report Extra Stuff to define Channel---
#Venta corporativa 



#Partnerships Channel Extra


#Mercado libre Orders 

UPDATE tbl_order_detail SET channel='Mercado Libre Voucher' WHERE channel like 'MKTMeLi%';
#OFFLINE
UPDATE tbl_order_detail SET channel='PR vouchers' WHERE channel like 'PR%';

#CAC Vouchers
##UPDATE tbl_order_detail set channel = 'CAC Deals' where (coupon_code like 'CAC%') and campaign not in ('MEX_fanpage_m_21-50_saludbelleza_20130606NauticaClassic_23_48', 'BlancaChida', '20130115.BibliotecaCool', '20130103.VacacionesEnLasMontañas');

UPDATE tbl_order_detail set channel='FB Ads CAC' WHERE (coupon_code like 'CAC%') and source_medium='facebook / socialmediaads';
UPDATE tbl_order_detail set channel='FB CAC' WHERE (coupon_code like 'CAC%') and source_medium='facebook / socialmedia';
UPDATE tbl_order_detail set channel='NL CAC' WHERE coupon_code like 'CAC%' and source_medium='postal / crm';

#Curebit
UPDATE tbl_order_detail set channel = 'Curebit' where coupon_code like '%cbit%' or source_medium like '%curebit%' or description_voucher like 'curebit%';

UPDATE tbl_order_detail t inner join mobileapp_transaction_id m on t.order_nr=m.transaction_id set channel = 'Mobile App';

#DEFINE CHANNEL GROUP

#Mobile App
UPDATE tbl_order_detail SET channel_group='Mobile App' WHERE channel='Mobile App';

#Facebook Ads Group
UPDATE tbl_order_detail SET channel_group='Facebook Ads'
 WHERE channel='Facebook Ads' or channel = 'Dark Post' 
or channel='Facebook Referral' or channel='FB Ads CAC'
or channel = 'FB Ads MP';

#Referal Platform
UPDATE tbl_order_detail set channel_group = 'Referal Platform' where channel='Curebit';

#SEM Group
UPDATE tbl_order_detail SET channel_group='Search Engine Marketing' WHERE channel='SEM' or channel='Bing' or channel='Other Branded';

#Social Media Group
UPDATE tbl_order_detail SET 
channel_group='Social Media' WHERE channel='Twitter Referral' 
or channel='FB Posts' or channel='March 10 FB Video' 
OR channel='March 22 FB Video' or channel='Youtube' or channel='FB CAC'
or channel = 'FB MP' or channel = 'FBF MP';

#Retargeting Group
UPDATE tbl_order_detail SET channel_group='Retargeting' WHERE channel='Sociomantic' or source like '%retargeting' OR 
channel='Cart Recovery' OR channel='Vizury' OR 'Facebook R.' OR channel = 'VEInteractive' OR channel = 'Criteo' or channel='Main ADV' or channel='AdRoll' or channel='Other Retargeting' or channel='My Things' or channel ='Avazu' or channel='Promosdescuentos' or channel='Triggit';

#GDN group
UPDATE tbl_order_detail SET channel_group='Google Display Network' WHERE channel like 'Google Display Network'; 

#NewsLetter Group
UPDATE tbl_order_detail SET channel_group='NewsLetter'
 WHERE channel='Newsletter' or channel='NL CAC' or channel = 'NL MP';

#CAC Deals Group
##UPDATE tbl_order_detail set channel_group='CAC Deals' where channel='CAC Deals' or channel='FB Ads CAC' or channel='FB CAC' or channel='NL CAC';

#SEO Group
UPDATE tbl_order_detail SET channel_group='Search Engine Optimization' WHERE channel='Search Organic' or channel='Yahoo Answers';

#Partnerships Group

UPDATE tbl_order_detail SET channel_group='Other (identified)' WHERE channel='Santander' OR channel='PAR Voucher' OR 
channel='AMEX' or channel='Banamex' OR channel='Dafiti' OR channel='Bancomer' OR channel='American Express' OR
channel='Banorte' or channel='PayPal' or channel='Bank' or channel='Corporate Deal' or channel='Other Partnerships' or channel='Pago Movil' or channel='El Universal' or channel='Buen Fin 2013' or channel='Partnerships';


#Corporate Sales Group
##UPDATE tbl_order_detail SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';

#Offline Group
UPDATE tbl_order_detail SET channel_group='Offline Marketing' WHERE channel='PR vouchers' or channel='Flyers' 
or channel='SMS' or channel='Other Offline' or channel='Linio Flyers' or channel='Bank Flyers' or channel='Partnership Flyers' or channel='NUEVO' or channel='EXCLUSIVIDAD' or channel='regalolinio100' or channel='regalolinio150' or channel='regreso' or channel='regresa'  or channel='regresa1' or channel='taxi' or channel='BANIXE01' or channel='BANIXE02' or channel='BANIXE03' or channel='PAYPAL150' or channel='SAN200' or channel='LIN120' or channel='regalo150' or channel='linio150' or channel='linio5' or channel='linio220' or channel='linio10' or channel='Bridgestone150' or channel='N80' or channel='N120' or channel='N200' or channel='soriana120' or channel='soriana100' or channel='BMX100' or channel='BMX150' or channel='BANIXE04' or channel='BBVA150' or channel='FINDE' or channel='VUELA' or channel='OFERTA150' or channel='DIC150' or channel='DEPORTE150' or channel='SMS100' or channel='SMS200' or channel='SMS300' or channel='LIN1' or channel='LIN2' or channel='LIN3' or channel='MODA100' or channel='MODA200' or channel='MODA300';

#Tele Sales Group
UPDATE tbl_order_detail SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales' OR channel='CCEMP Vouchers' or channel='Other Tele Sales';

#Branded Group
UPDATE tbl_order_detail SET channel_group='Branded' 
WHERE channel='Linio.com Referral' OR channel='Directo / Typed' OR
channel='SEM Branded';

#ML Group
UPDATE tbl_order_detail SET channel_group='Mercado Libre' WHERE channel='Mercado Libre Referral' OR channel='Mercado Libre Voucher' OR channel='Otros - Mercado Libre';

#Blog Group
-- UPDATE tbl_order_detail SET channel_group='Blogs' WHERE channel='Blog Linio';

#Affiliates Group
UPDATE tbl_order_detail SET channel_group='Affiliates' WHERE channel='Buscape' OR channel='Descuentomx' 
OR channel='Mercado Libre Affliates' OR channel='Glg' or channel='Pampa Network' OR channel='Clickmagic' OR 
channel='Trade Tracker' OR channel='Dscuento' OR channel='Referral P.' OR channel='Cupones Magicos' or channel='Other Affiliates' or channel='SoloCPM' or channel='PromoDescuentos';

#Other (identified) Group
UPDATE tbl_order_detail SET channel_group='Other (identified)' 
WHERE channel='Referral Sites' OR channel='Reactivation Letters' 
OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' OR
channel='Exchange' OR
channel='Content Mistake' OR
channel='Procurement Mistake' OR
channel='Employee Voucher' OR channel='IT Vouchers' or channel='Corporate Sales';

#Extra Channel Group

update tbl_order_detail set channel_group='Social Media'
 where source_medium = 'FB / FB';
update tbl_order_detail set channel_group='Social Media' where source_medium = 'Facebook / social media';
update tbl_order_detail set channel_group='Google Display Network' where source_medium = 'google / Display';
update tbl_order_detail set channel_group='Newsletter' where source_medium = 'Postal / (not set)';
update tbl_order_detail set channel_group='Other (identified)' where source_medium = 'sclaberinto.blogspot.mx / referral';
update tbl_order_detail set channel_group='Other (identified)' where source_medium = 'plus.url.google.com / referral';
update tbl_order_detail set channel_group='Social Media' where source_medium = 'SocialMedia / FacebookVoucher';
update tbl_order_detail set channel_group='Other (identified)' where source_medium = 'Publimetro / Partnership';
update tbl_order_detail set channel_group='Google Display Network' where source_medium = 'MetrosCubicos / Retargeting';
update tbl_order_detail set channel_group='Other (identified)' where source_medium = 'mail.google.com / referral';
update tbl_order_detail set channel_group='Affiliates' where source_medium = 'Affiliates / Mercadolibre';
update tbl_order_detail set channel_group='Affiliates' where source_medium = 'Cuponzote / Affiliates';
update tbl_order_detail set channel_group='Affiliates' where source_medium = 'Google_Shopping / Price Comparison';
update tbl_order_detail set channel_group='Tele Sales' where source_medium = 'CS / Inbound';
update tbl_order_detail set channel_group='Tele Sales' where source_medium = 'CS / Outbound';
update tbl_order_detail set channel_group='Other (identified)' where source_medium = 'pagead2.googlesyndication.com / referral';
update tbl_order_detail set channel_group='Affiliates' where source_medium = 'tradetracker / Affiliates';
update tbl_order_detail set channel_group='Affiliates' where source_medium = 'Clickmagic / Affiliates';


#Unknown Group
UPDATE tbl_order_detail SET channel_group='Non identified' WHERE channel='Unknown-No voucher' OR channel='Unknown-Voucher';

#Define Channel Type---
#TO BE DONE

#Ownership

UPDATE tbl_order_detail set Ownership='' where channel_group='Social Media';
UPDATE tbl_order_detail set Ownership='' where channel_group='NewsLetter';
UPDATE tbl_order_detail set Ownership='' where channel_group='Search Engine Optimization';
UPDATE tbl_order_detail set Ownership='' where channel_group='Blogs';
UPDATE tbl_order_detail set Ownership='' where channel_group='Offline Marketing';
UPDATE tbl_order_detail set Ownership='' where channel_group='Partnerships';
UPDATE tbl_order_detail set Ownership='' where channel_group='Mercado Libre';
UPDATE tbl_order_detail set Ownership='' where channel_group='Tele Sales';


-- SELECT 'Start: channel_report_no_voucher ', now();
-- call channel_report_no_voucher();

-- SELECT 'Start: visits_costs_channel ', now();
-- call visits_costs_channel();

-- SELECT 'Start: daily markting ', now();
-- call daily_marketing();

-- SELECT 'Start: monthly marketing ', now();
-- CALL monthly_marketing_2();
-- SELECT 'End: channel_report', now();

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `channel_report_no_voucher`()
BEGIN
#Channel Report MX---

#ga_cost_Campaign

update ga_visits_cost_source_medium set source_medium = concat( source, ' / ', medium) where source_medium is null;
update ga_cost_campaign set source_medium = concat( source, ' / ', medium) where source_medium is null;

#START#

#BLOG
UPDATE ga_visits_cost_source_medium SET channel='Blog Linio' where source_medium like 'blog%';

#PAMPA
UPDATE ga_visits_cost_source_medium set channel='Pampa Network' where source_medium like '%pampa%';

#REFERRAL
UPDATE ga_visits_cost_source_medium SET channel='Referral Sites' where source_medium like '%referral' 
AND source<>'facebook.com / referral'
AND source<>'m.facebook.com / referral'
AND source<>'linio.com.co / referral'
AND source<>'linio.com / referral'
AND source<>'linio.com.mx / referral'
AND source<>'google.co.ve / referral'
AND source<>'linio.com.ve / referral'
AND source<>'linio.com.pe / referral'
AND source<>'www-staging.linio.com.ve / referral'
AND source<>'google.com / referral'
AND source<>'.com.ve / referral'
AND source<>'blog.linio.com.ve / referral' AND source<>'t.co / referral'
AND source not like '%google%' AND source not like '%blog%' and source not like '%pampa%'
and source not like '%buscape%';


UPDATE ga_visits_cost_source_medium SET channel='Buscape' 
where source_medium='Buscape / Price Comparison' OR source like 'buscape%' or source='tracker.buscape.com.mx / referral';

#FB ADS
UPDATE ga_visits_cost_source_medium SET channel='Facebook Ads' where source_medium='facebook / socialmediaads' OR
source like '%book%ads%';

#SEM 
UPDATE ga_visits_cost_source_medium SET channel='SEM' where source_medium='google / cpc' AND (campaign not like '%linio%' AND 
campaign not like 'er.%' AND campaign not like 'brandb%');
UPDATE ga_visits_cost_source_medium SET channel='SEM Branded' where source_medium='google / cpc' AND campaign like 'brandb%';

#GDN
UPDATE ga_visits_cost_source_medium SET channel='Google Display Network' where source_medium='google / cpc' AND 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%');

#RETARGETING
UPDATE ga_visits_cost_source_medium SET channel='Sociomantic' where source_medium='sociomantic / (not set)' 
OR source='sociomantic / retargeting' OR source 
like '%socioman%';
UPDATE ga_visits_cost_source_medium SET channel='Cart Recovery' where source_medium='Cart_Recovery / CRM';
UPDATE ga_visits_cost_source_medium SET channel='Vizury' where source_medium='vizury / retargeting' OR source like '%vizury%';
UPDATE ga_visits_cost_source_medium SET channel='Facebook R.' where source_medium='facebook / retargeting';
UPDATE ga_visits_cost_source_medium SET channel='VEInteractive' where source_medium like '%VEInteractive%';


#Criteo
UPDATE ga_visits_cost_source_medium set channel='Criteo' where source_medium like '%criteo%';

#SOCIAL MEDIA
UPDATE ga_visits_cost_source_medium SET channel='Facebook Referral' WHERE 
(source='facebook.com / referral' OR source='m.facebook.com / referral' OR source='Banamex-deb / socialmedia' OR 
source='SocialMedia / FacebookVoucher') AND 
(source<>'facebook / socialmediaads' AND source<>'facebook / retargeting');
UPDATE ga_visits_cost_source_medium SET channel='Twitter Referral' where source_medium='t.co / referral' OR source='twitter / socialmedia';
UPDATE ga_visits_cost_source_medium SET channel='FB Posts' where source_medium='facebook / socialmedia' OR source='facebook / (not set)';

#SERVICIO AL CLIENTE
UPDATE ga_visits_cost_source_medium SET channel='Inbound' where source_medium like 'Tele%' or source like '%inbound%';
UPDATE ga_visits_cost_source_medium SET channel='OutBound' where source_medium like 'REC%' or source like '%outbound%';
UPDATE ga_visits_cost_source_medium SET channel='CCEMP Vouchers' where source_medium like 'CCEMP%';

#SEO
UPDATE ga_visits_cost_source_medium SET channel='Search Organic' where source_medium like '%organic' OR source='google.com.co / referral';

#NEWSLETTER
UPDATE ga_visits_cost_source_medium set Channel='Newsletter' WHERE 
(source like '%CRM' OR source like '%email' OR source like 'Email Marketing%' or source like 'postal%') AND 
(source<>'TeleSales / CRM' AND source<>'Cart_Recovery / CRM');


#PARTNERSHIPS
UPDATE ga_visits_cost_source_medium SET channel='Banorte' 
where source_medium like '%banorte%';
UPDATE ga_visits_cost_source_medium SET channel='Santander' where source_medium 
like '%Santander%';
UPDATE ga_visits_cost_source_medium set Channel='AMEX' where source_medium like '%AMEX%';

UPDATE ga_visits_cost_source_medium SET channel='Banamex' WHERE 
source='Banamex / Partnership' OR source_medium like '%banamez%';
UPDATE ga_visits_cost_source_medium SET channel='Dafiti' where source_medium='Dafiti / Partnership' 
or source_medium like '%dafiti%';
UPDATE ga_visits_cost_source_medium SET channel='Bancomer' WHERE 
source_medium like '%bancomer%' or source_medium like '%partnership%';


UPDATE ga_visits_cost_source_medium SET channel='PayPal' where source_medium like '%paypal%';

#DIRECT
UPDATE ga_visits_cost_source_medium SET channel='Linio.com Referral' WHERE  
source_medium='linio.com.co / referral' OR
source_medium='linio.com / referral' OR
source_medium='linio.com.mx / referral' OR
source_medium='linio.com.ve / referral' OR
source_medium='linio.com.pe / referral' OR
source_medium='www-staging.linio.com.ve / referral';

UPDATE ga_visits_cost_source_medium SET channel='Directo / Typed' where source_medium='(direct) / (none)' OR 
source_medium like '%direct%none%';


#AFFILIATES
UPDATE ga_visits_cost_source_medium SET channel='Descuentomx' where source_medium like 'dscuentomx / affiliates';
UPDATE ga_visits_cost_source_medium SET 
channel='Mercado Libre Affliates' where source_medium like 'Mercadolibre / Affiliates';
UPDATE ga_visits_cost_source_medium SET channel='Glg' where source_medium='glg / Affiliates';
UPDATE ga_visits_cost_source_medium SET channel='Clickmagic' where source_medium='Clickmagic / Affiliates';
UPDATE ga_visits_cost_source_medium SET channel='Tradetracker' where source_medium='tradetracker / Affiliates';
UPDATE ga_visits_cost_source_medium SET channel='Dscuento' where source_medium='MKTDscuento' 
or source_medium='dscuento.com.mx / referral';
UPDATE ga_visits_cost_source_medium SET channel='Referral P.' where source_medium LIKE 'cbit%';
UPDATE ga_visits_cost_source_medium SET channel='Cupones Magicos' where source_medium='cuponesmagicos.com.mx / referral';

#CORPORATE SALES

#OFFLINE

#N/A
UPDATE 
ga_visits_cost_source_medium SET channel='Unknown-No voucher' 
where source_medium='';
UPDATE ga_visits_cost_source_medium SET 
channel='Unknown-Voucher' WHERE channel='' AND source<>'';

#DEFINE CHANNEL GROUP

#Facebook Ads Group
UPDATE ga_visits_cost_source_medium SET channel_group='Facebook Ads' WHERE channel='Facebook Ads';

#SEM Group
UPDATE ga_visits_cost_source_medium SET channel_group='Search Engine Marketing' WHERE channel='SEM';

#Social Media Group
UPDATE ga_visits_cost_source_medium SET 
channel_group='Social Media' WHERE channel='Facebook Referral' 
OR channel='Twitter Referral' or channel='FB Posts' or channel='March 10 FB Video' OR channel='March 22 FB Video';

#Retargeting Group
UPDATE ga_visits_cost_source_medium SET channel_group='Retargeting' WHERE channel='Sociomantic' or source like '%retargeting' OR 
channel='Cart Recovery' OR channel='Vizury' OR 'Facebook R.' OR channel = 'VEInteractive' OR channel = 'Criteo';

#GDN group
UPDATE ga_visits_cost_source_medium SET channel_group='Google Display Network' WHERE channel like 'Google Display Network'; 

#NewsLetter Group
UPDATE ga_visits_cost_source_medium SET channel_group='NewsLetter' WHERE channel='Newsletter' OR channel='CAC Campaign';

#SEO Group
UPDATE ga_visits_cost_source_medium SET channel_group='Search Engine Optimization' WHERE channel='Search Organic';

#Partnerships Group

UPDATE ga_visits_cost_source_medium SET channel_group='Partnerships' WHERE channel='Santander' OR channel='PAR Voucher' OR 
channel='AMEX' or channel='Banamex' OR channel='Dafiti' OR channel='Bancomer' OR channel='American Express' OR
channel='Banorte' or channel='PayPal';


#Corporate Sales Group
UPDATE ga_visits_cost_source_medium SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';

#Offline Group
UPDATE ga_visits_cost_source_medium SET channel_group='Offline Marketing' WHERE channel='PR vouchers' or channel='Flyers' 
or channel='SMS' or channel='Other Offline';

#Tele Sales Group
UPDATE ga_visits_cost_source_medium SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales' OR channel='CCEMP Vouchers';

#Branded Group
UPDATE ga_visits_cost_source_medium SET channel_group='Branded' 
WHERE channel='Linio.com Referral' OR channel='Directo / Typed' OR
channel='SEM Branded';

#ML Group
UPDATE ga_visits_cost_source_medium SET channel_group='Mercado Libre' WHERE channel='Mercado Libre Referral' OR channel='Mercado Libre Voucher' OR channel='Otros - Mercado Libre';

#Blog Group
UPDATE ga_visits_cost_source_medium SET channel_group='Blogs' WHERE channel='Blog Linio';

#Affiliates Group
UPDATE ga_visits_cost_source_medium SET channel_group='Affiliates' WHERE channel='Buscape' OR channel='Descuentomx' 
OR channel='Mercado Libre Affliates' OR channel='Glg' or channel='Pampa Network' OR channel='Clickmagic' OR 
channel='Tradetracker' OR channel='Dscuento' OR channel='Referral P.' OR channel='Cupones Magicos';

#Other (identified) Group
UPDATE ga_visits_cost_source_medium SET channel_group='Other (identified)' 
WHERE channel='Referral Sites' OR channel='Reactivation Letters' 
OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' OR
channel='Exchange' OR
channel='Content Mistake' OR
channel='Procurement Mistake' OR
channel='Employee Voucher' OR channel='IT Vouchers';

#Unknown Group
UPDATE ga_visits_cost_source_medium SET channel_group='Non identified' WHERE channel='Unknown-No voucher' OR channel='Unknown-Voucher';









# GA Cost campaign
update ga_cost_campaign set source_medium = concat( source, ' / ', medium) where source_medium is null;
update ga_cost_campaign set source_medium = concat( source, ' / ', medium) where source_medium is null;
#Define channel---
#BLOG
UPDATE ga_cost_campaign SET channel='Blog Linio' where source_medium like 'blog%';

#PAMPA
UPDATE ga_cost_campaign set channel='Pampa Network' where source_medium like '%pampa%';

#REFERRAL
UPDATE ga_cost_campaign SET channel='Referral Sites' where source_medium like '%referral' 
AND source<>'facebook.com / referral'
AND source<>'m.facebook.com / referral'
AND source<>'linio.com.co / referral'
AND source<>'linio.com / referral'
AND source<>'linio.com.mx / referral'
AND source<>'google.co.ve / referral'
AND source<>'linio.com.ve / referral'
AND source<>'linio.com.pe / referral'
AND source<>'www-staging.linio.com.ve / referral'
AND source<>'google.com / referral'
AND source<>'.com.ve / referral'
AND source<>'blog.linio.com.ve / referral' AND source<>'t.co / referral'
AND source not like '%google%' AND source not like '%blog%' and source not like '%pampa%'
and source not like '%buscape%';


UPDATE ga_cost_campaign SET channel='Buscape' 
where source_medium='Buscape / Price Comparison' OR source like 'buscape%' or source='tracker.buscape.com.mx / referral';

#FB ADS
UPDATE ga_cost_campaign SET channel='Facebook Ads' where source_medium='facebook / socialmediaads' OR
source like '%book%ads%';

#SEM 
UPDATE ga_cost_campaign SET channel='SEM' where source_medium='google / cpc' AND (campaign not like '%linio%' AND 
campaign not like 'er.%' AND campaign not like 'brandb%');
UPDATE ga_cost_campaign SET channel='SEM Branded' where source_medium='google / cpc' AND campaign like 'brandb%';

#GDN
UPDATE ga_cost_campaign SET channel='Google Display Network' where source_medium='google / cpc' AND 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%');

#RETARGETING
UPDATE ga_cost_campaign SET channel='Sociomantic' where source_medium='sociomantic / (not set)' 
OR source='sociomantic / retargeting' OR source 
like '%socioman%';
UPDATE ga_cost_campaign SET channel='Cart Recovery' where source_medium='Cart_Recovery / CRM';
UPDATE ga_cost_campaign SET channel='Vizury' where source_medium='vizury / retargeting' OR source like '%vizury%';
UPDATE ga_cost_campaign SET channel='Facebook R.' where source_medium='facebook / retargeting';
UPDATE ga_cost_campaign SET channel='VEInteractive' where source_medium like '%VEInteractive%';


#Criteo
UPDATE ga_cost_campaign set channel='Criteo' where source_medium like '%criteo%';

#SOCIAL MEDIA
UPDATE ga_cost_campaign SET channel='Facebook Referral' WHERE 
(source='facebook.com / referral' OR source='m.facebook.com / referral' OR source='Banamex-deb / socialmedia' OR 
source='SocialMedia / FacebookVoucher') AND 
(source<>'facebook / socialmediaads' AND source<>'facebook / retargeting');
UPDATE ga_cost_campaign SET channel='Twitter Referral' where source_medium='t.co / referral' OR source='twitter / socialmedia';
UPDATE ga_cost_campaign SET channel='FB Posts' where source_medium='facebook / socialmedia' OR source='facebook / (not set)';

#SERVICIO AL CLIENTE
UPDATE ga_cost_campaign SET channel='Inbound' where source_medium like 'Tele%' or source like '%inbound%';
UPDATE ga_cost_campaign SET channel='OutBound' where source_medium like 'REC%' or source like '%outbound%';
UPDATE ga_cost_campaign SET channel='CCEMP Vouchers' where source_medium like 'CCEMP%';

#SEO
UPDATE ga_cost_campaign SET channel='Search Organic' where source_medium like '%organic' OR source='google.com.co / referral';

#NEWSLETTER
UPDATE ga_cost_campaign set Channel='Newsletter' WHERE 
(source like '%CRM' OR source like '%email' OR source like 'Email Marketing%' or source like 'postal%') AND 
(source<>'TeleSales / CRM' AND source<>'Cart_Recovery / CRM');


#PARTNERSHIPS
UPDATE ga_cost_campaign SET channel='Banorte' 
where source_medium like '%banorte%';
UPDATE ga_cost_campaign SET channel='Santander' where source_medium 
like '%Santander%';
UPDATE ga_cost_campaign set Channel='AMEX' where source_medium like '%AMEX%';

UPDATE ga_cost_campaign SET channel='Banamex' WHERE 
source='Banamex / Partnership' OR source_medium like '%banamez%';
UPDATE ga_cost_campaign SET channel='Dafiti' where source_medium='Dafiti / Partnership' 
or source_medium like '%dafiti%';
UPDATE ga_cost_campaign SET channel='Bancomer' WHERE 
source_medium like '%bancomer%' or source_medium like '%partnership%';


UPDATE ga_cost_campaign SET channel='PayPal' where source_medium like '%paypal%';

#DIRECT
UPDATE ga_cost_campaign SET channel='Linio.com Referral' WHERE  
source_medium='linio.com.co / referral' OR
source_medium='linio.com / referral' OR
source_medium='linio.com.mx / referral' OR
source_medium='linio.com.ve / referral' OR
source_medium='linio.com.pe / referral' OR
source_medium='www-staging.linio.com.ve / referral';

UPDATE ga_cost_campaign SET channel='Directo / Typed' where source_medium='(direct) / (none)' OR 
source_medium like '%direct%none%';


#AFFILIATES
UPDATE ga_cost_campaign SET channel='Descuentomx' where source_medium like 'dscuentomx / affiliates';
UPDATE ga_cost_campaign SET 
channel='Mercado Libre Affliates' where source_medium like 'Mercadolibre / Affiliates';
UPDATE ga_cost_campaign SET channel='Glg' where source_medium='glg / Affiliates';
UPDATE ga_cost_campaign SET channel='Clickmagic' where source_medium='Clickmagic / Affiliates';
UPDATE ga_cost_campaign SET channel='Tradetracker' where source_medium='tradetracker / Affiliates';
UPDATE ga_cost_campaign SET channel='Dscuento' where source_medium='MKTDscuento' 
or source_medium='dscuento.com.mx / referral';
UPDATE ga_cost_campaign SET channel='Referral P.' where source_medium LIKE 'cbit%';
UPDATE ga_cost_campaign SET channel='Cupones Magicos' where source_medium='cuponesmagicos.com.mx / referral';

#CORPORATE SALES

#OFFLINE

#N/A
UPDATE 
ga_cost_campaign SET channel='Unknown-No voucher' 
where source_medium='';
UPDATE ga_cost_campaign SET 
channel='Unknown-Voucher' WHERE channel='' AND source<>'';

#DEFINE CHANNEL GROUP

#Facebook Ads Group
UPDATE ga_cost_campaign SET channel_group='Facebook Ads' WHERE channel='Facebook Ads';

#SEM Group
UPDATE ga_cost_campaign SET channel_group='Search Engine Marketing' WHERE channel='SEM';

#Social Media Group
UPDATE ga_cost_campaign SET 
channel_group='Social Media' WHERE channel='Facebook Referral' 
OR channel='Twitter Referral' or channel='FB Posts' or channel='March 10 FB Video' OR channel='March 22 FB Video';

#Retargeting Group
UPDATE ga_cost_campaign SET channel_group='Retargeting' WHERE channel='Sociomantic' or source like '%retargeting' OR 
channel='Cart Recovery' OR channel='Vizury' OR 'Facebook R.' OR channel = 'VEInteractive' OR channel = 'Criteo';

#GDN group
UPDATE ga_cost_campaign SET channel_group='Google Display Network' WHERE channel like 'Google Display Network'; 

#NewsLetter Group
UPDATE ga_cost_campaign SET channel_group='NewsLetter' WHERE channel='Newsletter' OR channel='CAC Campaign';

#SEO Group
UPDATE ga_cost_campaign SET channel_group='Search Engine Optimization' WHERE channel='Search Organic';

#Partnerships Group

UPDATE ga_cost_campaign SET channel_group='Partnerships' WHERE channel='Santander' OR channel='PAR Voucher' OR 
channel='AMEX' or channel='Banamex' OR channel='Dafiti' OR channel='Bancomer' OR channel='American Express' OR
channel='Banorte' or channel='PayPal';


#Corporate Sales Group
UPDATE ga_cost_campaign SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';

#Offline Group
UPDATE ga_cost_campaign SET channel_group='Offline Marketing' WHERE channel='PR vouchers' or channel='Flyers' 
or channel='SMS' or channel='Other Offline';

#Tele Sales Group
UPDATE ga_cost_campaign SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales' OR channel='CCEMP Vouchers';

#Branded Group
UPDATE ga_cost_campaign SET channel_group='Branded' 
WHERE channel='Linio.com Referral' OR channel='Directo / Typed' OR
channel='SEM Branded';

#ML Group
UPDATE ga_cost_campaign SET channel_group='Mercado Libre' WHERE channel='Mercado Libre Referral' OR channel='Mercado Libre Voucher' OR channel='Otros - Mercado Libre';

#Blog Group
UPDATE ga_cost_campaign SET channel_group='Blogs' WHERE channel='Blog Linio';

#Affiliates Group
UPDATE ga_cost_campaign SET channel_group='Affiliates' WHERE channel='Buscape' OR channel='Descuentomx' 
OR channel='Mercado Libre Affliates' OR channel='Glg' or channel='Pampa Network' OR channel='Clickmagic' OR 
channel='Tradetracker' OR channel='Dscuento' OR channel='Referral P.' OR channel='Cupones Magicos';

#Other (identified) Group
UPDATE ga_cost_campaign SET channel_group='Other (identified)' 
WHERE channel='Referral Sites' OR channel='Reactivation Letters' 
OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' OR
channel='Exchange' OR
channel='Content Mistake' OR
channel='Procurement Mistake' OR
channel='Employee Voucher' OR channel='IT Vouchers';

#Unknown Group
UPDATE ga_cost_campaign SET channel_group='Non identified' WHERE channel='Unknown-No voucher' OR channel='Unknown-Voucher';



call visits_costs_channel();


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `cheating_list`()
begin

select  'Cheating List: start',now();

delete from production.cheating_list;

insert into production.cheating_list(date, custid, first_name, last_name, email, order_nr, coupon_code, address1, address2, street_number, city, postcode, phone_number) select t.date, t.custid, c.first_name, c.last_name, c.email, t.order_nr, t.coupon_code, a.address1, a.address2, a.street_number, a.city, a.postcode, a.phone from production.tbl_order_detail t, bob_live_mx.customer c, bob_live_mx.customer_address a where t.custid=c.id_customer and t.custid=a.fk_customer and t.obc=1 and t.coupon_code like 'CAC%' group by order_nr, phone, address1, address2, street_number, city order by custid;

update production.cheating_list c set actual_paid_price = (select sum(actual_paid_price) from production.tbl_order_detail t where t.order_nr=c.order_nr);

update production.cheating_list c inner join bob_live_mx.sales_order o on c.order_nr=o.order_nr set c.ip_address=o.ip;

update production.cheating_list c inner join bob_live_mx.sales_order o on c.order_nr=o.order_nr inner join bob_live_mx.sales_order_accertify_log g on o.id_sales_order=g.fk_sales_order set credit_card=ExtractValue(g.message,'/transaction/paymentInformation/cardNumber');

-- IP Address

create table production.temporary_cheat(
ip_address varchar(255),
custid int
);

create index temporary_cheat on production.temporary_cheat(ip_address, custid);

insert into production.temporary_cheat select ip, fk_customer from bob_live_mx.sales_order;

update production.cheating_list c inner join production.temporary_cheat o on c.ip_address=o.ip_address set c.ip_address_cheat = 1 where c.custid!=o.custid; 

drop table production.temporary_cheat;

-- Address

create table production.temporary_cheat(
address1 varchar(255),
address2 varchar(255),
street_number varchar(255),
city varchar(255),
custid int
);

create index temporary_cheat on production.temporary_cheat(address1, address2, city, custid);

insert into production.temporary_cheat select address1, address2, street_number, city, fk_customer from bob_live_mx.customer_address;

update production.cheating_list c inner join production.temporary_cheat o on c.address1=o.address1 and c.address2=o.address2 and c.street_number=o.street_number and c.city=o.city set c.address_cheat = 1 where c.custid!=o.custid;

drop table production.temporary_cheat;

-- Phone Number

create table production.temporary_cheat(
phone_number varchar(255),
custid int
);

create index temporary_cheat on production.temporary_cheat(phone_number, custid);

insert into production.temporary_cheat select phone, fk_customer from bob_live_mx.customer_address;

update production.cheating_list c inner join production.temporary_cheat o on c.phone_number=o.phone_number set c.phone_number_cheat = 1 where c.custid!=o.custid; 

drop table production.temporary_cheat;

-- Same Name

create table production.temporary_cheat(
first_name varchar(255),
last_name varchar(255),
postcode int,
custid int
);

create index temporary_cheat on production.temporary_cheat(first_name, last_name, custid, postcode);

insert into production.temporary_cheat select c.first_name, c.last_name, a.postcode, c.id_customer from bob_live_mx.customer c, bob_live_mx.customer_address a where c.id_customer=a.fk_customer;

update production.cheating_list c inner join production.temporary_cheat o on c.first_name=o.first_name and c.last_name=o.last_name and c.postcode=o.postcode set c.same_name_cheat = 1 where c.custid!=o.custid; 

drop table production.temporary_cheat;

-- Credit Card

create table production.temporary_cheat(
credit_card varchar(255),
custid int
);

create index temporary_cheat on production.temporary_cheat(credit_card, custid);

insert into production.temporary_cheat select ExtractValue(g.message,'/transaction/paymentInformation/cardNumber'), fk_customer from bob_live_mx.sales_order o, bob_live_mx.sales_order_accertify_log g where o.id_sales_order=g.fk_sales_order;

update production.cheating_list c inner join production.temporary_cheat o on c.credit_card=o.credit_card set c.credit_card_cheat = 1 where c.custid!=o.custid; 

drop table production.temporary_cheat;

-- Linio Mail

update production.cheating_list set linio_mail_cheat = 1 where email like '%@linio%';

-- delete from production.cheating_list where ip_address_cheat is null and address_cheat is null and phone_number_cheat is null and same_name_cheat is null and credit_card_cheat is null and linio_mail_cheat is null;

update production.cheating_list set ip_address_cheat=0 where ip_address_cheat is null;

update production.cheating_list set address_cheat=0 where address_cheat is null;

update production.cheating_list set phone_number_cheat=0 where phone_number_cheat is null;

update production.cheating_list set same_name_cheat=0 where same_name_cheat is null;

update production.cheating_list set credit_card_cheat=0 where credit_card_cheat is null;

update production.cheating_list set linio_mail_cheat=0 where linio_mail_cheat is null;

select  'Cheating List: stop',now();

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `cheating_list2`()
begin

select  'Cheating List: start',now();

delete from production.cheating_list;

insert into production.cheating_list(date, custid, first_name, last_name, email, order_nr, coupon_code, address1, address2, street_number, city, postcode, phone_number) select t.date, t.custid, c.first_name, c.last_name, c.email, t.order_nr, t.coupon_code, a.address1, a.address2, a.street_number, a.city, a.postcode, a.phone from production.tbl_order_detail t, bob_live_mx.customer c, bob_live_mx.customer_address a where t.custid=c.id_customer and t.custid=a.fk_customer and t.obc=1 and (t.coupon_code = 'CACbex' or t.coupon_code like 'NL%') group by order_nr, phone, address1, address2, street_number, city order by custid;

update production.cheating_list c set actual_paid_price = (select sum(actual_paid_price) from production.tbl_order_detail t where t.order_nr=c.order_nr);

update production.cheating_list c inner join bob_live_mx.sales_order o on c.order_nr=o.order_nr set c.ip_address=o.ip;

update production.cheating_list c inner join bob_live_mx.sales_order o on c.order_nr=o.order_nr inner join bob_live_mx.sales_order_accertify_log g on o.id_sales_order=g.fk_sales_order set credit_card=ExtractValue(g.message,'/transaction/paymentInformation/cardNumber');

-- IP Address

create table production.temporary_cheat(
ip_address varchar(255),
custid int
);

create index temporary_cheat on production.temporary_cheat(ip_address, custid);

insert into production.temporary_cheat select ip, fk_customer from bob_live_mx.sales_order;

update production.cheating_list c inner join production.temporary_cheat o on c.ip_address=o.ip_address set c.ip_address_cheat = 1 where c.custid!=o.custid; 

drop table production.temporary_cheat;

-- Address

create table production.temporary_cheat(
address1 varchar(255),
address2 varchar(255),
street_number varchar(255),
city varchar(255),
custid int
);

create index temporary_cheat on production.temporary_cheat(address1, address2, city, custid);

insert into production.temporary_cheat select address1, address2, street_number, city, fk_customer from bob_live_mx.customer_address;

update production.cheating_list c inner join production.temporary_cheat o on c.address1=o.address1 and c.address2=o.address2 and c.street_number=o.street_number and c.city=o.city set c.address_cheat = 1 where c.custid!=o.custid;

drop table production.temporary_cheat;

-- Phone Number

create table production.temporary_cheat(
phone_number varchar(255),
custid int
);

create index temporary_cheat on production.temporary_cheat(phone_number, custid);

insert into production.temporary_cheat select phone, fk_customer from bob_live_mx.customer_address;

update production.cheating_list c inner join production.temporary_cheat o on c.phone_number=o.phone_number set c.phone_number_cheat = 1 where c.custid!=o.custid; 

drop table production.temporary_cheat;

-- Same Name

create table production.temporary_cheat(
first_name varchar(255),
last_name varchar(255),
postcode int,
custid int
);

create index temporary_cheat on production.temporary_cheat(first_name, last_name, custid, postcode);

insert into production.temporary_cheat select c.first_name, c.last_name, a.postcode, c.id_customer from bob_live_mx.customer c, bob_live_mx.customer_address a where c.id_customer=a.fk_customer;

update production.cheating_list c inner join production.temporary_cheat o on c.first_name=o.first_name and c.last_name=o.last_name and c.postcode=o.postcode set c.same_name_cheat = 1 where c.custid!=o.custid; 

drop table production.temporary_cheat;

-- Credit Card

create table production.temporary_cheat(
credit_card varchar(255),
custid int
);

create index temporary_cheat on production.temporary_cheat(credit_card, custid);

insert into production.temporary_cheat select ExtractValue(g.message,'/transaction/paymentInformation/cardNumber'), fk_customer from bob_live_mx.sales_order o, bob_live_mx.sales_order_accertify_log g where o.id_sales_order=g.fk_sales_order;

update production.cheating_list c inner join production.temporary_cheat o on c.credit_card=o.credit_card set c.credit_card_cheat = 1 where c.custid!=o.custid; 

drop table production.temporary_cheat;

-- Linio Mail

update production.cheating_list set linio_mail_cheat = 1 where email like '%@linio%';

-- delete from production.cheating_list where ip_address_cheat is null and address_cheat is null and phone_number_cheat is null and same_name_cheat is null and credit_card_cheat is null and linio_mail_cheat is null;

update production.cheating_list set ip_address_cheat=0 where ip_address_cheat is null;

update production.cheating_list set address_cheat=0 where address_cheat is null;

update production.cheating_list set phone_number_cheat=0 where phone_number_cheat is null;

update production.cheating_list set same_name_cheat=0 where same_name_cheat is null;

update production.cheating_list set credit_card_cheat=0 where credit_card_cheat is null;

update production.cheating_list set linio_mail_cheat=0 where linio_mail_cheat is null;

select  'Cheating List: stop',now();

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `cohort_channel_category`()
begin

delete from production.cohort_channel_category;

insert into production.cohort_channel_category(country, custid, yrmonth, date, channel_group, channel, category, voucher, net_revenue, PC15, nr_items, `% discount`, region) select 'Mexico', custid, yrmonth, date, channel_group, channel,(select n1 from production.tbl_order_detail x where actual_paid_price = (select max(actual_paid_price) from production.tbl_order_detail y where x.order_nr=y.order_nr group by order_nr) and z.order_nr=x.order_nr group by order_nr), coupon_code, sum(actual_paid_price), sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)) as PC15, count(order_nr), (select (1-(avg(i.unit_price)/avg(i.original_unit_price))) from bob_live_mx.sales_order o inner join bob_live_mx.sales_order_item i on i.fk_sales_order = o.id_sales_order where z.order_nr=o.order_nr group by o.order_nr), region from production.tbl_order_detail z where oac=1 and new_customers is not null group by custid;

update production.cohort_channel_category z inner join bob_live_mx.customer c on z.custid=c.id_customer set z.age = year(curdate()) - year(c.birthday), z.sex = c.gender;

-- Same Month

update production.cohort_channel_category b set nr_unique_repurcharse_same_month = (select case when count(distinct t.order_nr)>=1 then 1 else 0 end from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and b.yrmonth=t.yrmonth and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set transactions_same_month = (select count(distinct t.order_nr) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and b.yrmonth=t.yrmonth and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set nr_items_same_month = (select count(t.order_nr) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and b.yrmonth=t.yrmonth and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set net_revenue_same_month = (select sum(t.actual_paid_price) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and b.yrmonth=t.yrmonth and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set PC15_same_month = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and b.yrmonth=t.yrmonth and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set nr_dif_categories_same_month = (select count(distinct t.n1) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and b.yrmonth=t.yrmonth and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set nr_dif_sub_categories_same_month = (select count(distinct t.n2) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and b.yrmonth=t.yrmonth and t.new_customers is null group by b.custid);

update production.cohort_channel_category set nr_unique_repurcharse_same_month=0 where nr_unique_repurcharse_same_month is null;

-- 30 days

update production.cohort_channel_category b set nr_unique_repurcharse_30 = (select case when count(distinct t.order_nr)>=1 and nr_unique_repurcharse_same_month=0 then 1 else 0 end from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set transactions_30 = (select count(distinct t.order_nr) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set nr_items_30 = (select count(t.order_nr) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set net_revenue_30 = (select sum(t.actual_paid_price) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set PC15_30 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set nr_dif_categories_30 = (select count(distinct t.n1) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set nr_dif_sub_categories_30 = (select count(distinct t.n2) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category set nr_unique_repurcharse_30=0 where nr_unique_repurcharse_30 is null;

-- 60 days

update production.cohort_channel_category b set nr_unique_repurcharse_60 = (select case when count(distinct t.order_nr)>=1 and nr_unique_repurcharse_same_month=0 and nr_unique_repurcharse_30=0 then 1 else 0 end from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set transactions_60 = (select count(distinct t.order_nr) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set nr_items_60 = (select count(t.order_nr) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set net_revenue_60 = (select sum(t.actual_paid_price) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set PC15_60 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set nr_dif_categories_60 = (select count(distinct t.n1) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set nr_dif_sub_categories_60 = (select count(distinct t.n2) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category set nr_unique_repurcharse_60=0 where nr_unique_repurcharse_60 is null;

-- 90 days

update production.cohort_channel_category b set nr_unique_repurcharse_90 = (select case when count(distinct t.order_nr)>=1 and nr_unique_repurcharse_same_month=0 and nr_unique_repurcharse_30=0 and nr_unique_repurcharse_60=0 then 1 else 0 end from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 90 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set transactions_90 = (select count(distinct t.order_nr) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 90 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set nr_items_90 = (select count(t.order_nr) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 90 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set net_revenue_90 = (select sum(t.actual_paid_price) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 90 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set PC15_90 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 90 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set nr_dif_categories_90 = (select count(distinct t.n1) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 90 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set nr_dif_sub_categories_90 = (select count(distinct t.n2) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 90 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category set nr_unique_repurcharse_90=0 where nr_unique_repurcharse_90 is null;

-- 120 days

update production.cohort_channel_category b set nr_unique_repurcharse_120 = (select case when count(distinct t.order_nr)>=1 and nr_unique_repurcharse_same_month=0 and nr_unique_repurcharse_30=0 and nr_unique_repurcharse_60=0 and nr_unique_repurcharse_90=0 then 1 else 0 end from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 120 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set transactions_120 = (select count(distinct t.order_nr) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 120 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set nr_items_120 = (select count(t.order_nr) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 120 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set net_revenue_120 = (select sum(t.actual_paid_price) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 120 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set PC15_120 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 120 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set nr_dif_categories_120 = (select count(distinct t.n1) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 120 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set nr_dif_sub_categories_120 = (select count(distinct t.n2) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 120 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category set nr_unique_repurcharse_120=0 where nr_unique_repurcharse_120 is null;

-- 150 days

update production.cohort_channel_category b set nr_unique_repurcharse_150 = (select case when count(distinct t.order_nr)>=1 and nr_unique_repurcharse_same_month=0 and nr_unique_repurcharse_30=0 and nr_unique_repurcharse_60=0 and nr_unique_repurcharse_90=0 and nr_unique_repurcharse_120=0 then 1 else 0 end from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 150 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set transactions_150 = (select count(distinct t.order_nr) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 150 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set nr_items_150 = (select count(t.order_nr) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 150 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set net_revenue_150 = (select sum(t.actual_paid_price) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 150 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set PC15_150 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 150 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set nr_dif_categories_150 = (select count(distinct t.n1) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 150 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set nr_dif_sub_categories_150 = (select count(distinct t.n2) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 150 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category set nr_unique_repurcharse_150=0 where nr_unique_repurcharse_150 is null;

-- 180 days

update production.cohort_channel_category b set nr_unique_repurcharse_180 = (select case when count(distinct t.order_nr)>=1 and nr_unique_repurcharse_same_month=0 and nr_unique_repurcharse_30=0 and nr_unique_repurcharse_60=0 and nr_unique_repurcharse_90=0 and nr_unique_repurcharse_120=0 and nr_unique_repurcharse_150=0 then 1 else 0 end from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 180 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set transactions_180 = (select count(distinct t.order_nr) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 180 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set nr_items_180 = (select count(t.order_nr) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 180 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set net_revenue_180 = (select sum(t.actual_paid_price) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 180 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set PC15_180 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 180 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set nr_dif_categories_180 = (select count(distinct t.n1) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 180 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category b set nr_dif_sub_categories_180 = (select count(distinct t.n2) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 180 day) and t.new_customers is null group by b.custid);

update production.cohort_channel_category set nr_unique_repurcharse_180=0 where nr_unique_repurcharse_180 is null;

update production.cohort_channel_category c inner join development_mx.A_E_BI_ExchangeRate_USD u on c.yrmonth=u.Month_Num set net_revenue=net_revenue/xr, net_revenue_same_month=net_revenue_same_month/xr, net_revenue_30=net_revenue_30/xr, net_revenue_60=net_revenue_60/xr, net_revenue_90=net_revenue_90/xr, net_revenue_120=net_revenue_120/xr, net_revenue_150=net_revenue_150/xr, net_revenue_180=net_revenue_180/xr, PC15=PC15/xr, PC15_same_month=PC15_same_month/xr, PC15_30=PC15_30/xr, PC15_60=PC15_60/xr, PC15_90=PC15_90/xr, PC15_120=PC15_120/xr, PC15_150=PC15_150/xr, PC15_180=PC15_180/xr where u.country='MEX';

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`gustavo.miranda`@`%`*/ /*!50003 PROCEDURE `commercial_scorecard_regional`()
begin

-- Mexico

insert into production.commercial_scorecard_regional(week, country, revenue, PC2) select production.week_iso(date_sub(curdate(), interval 7 day)), 'Mexico', sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0))/16.35 as revenue, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0))/16.35 as PC2 from production.tbl_order_detail where oac=1 and production.week_exit(date) = production.week_exit(curdate())-1 and year(date)=year(curdate());

update production.commercial_scorecard_regional c set share_non_electronics= (select (sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0))/16.35)/c.revenue from production.tbl_order_detail t where t.oac=1 and production.week_exit(date) = production.week_exit(curdate())-1 and year(date)=year(curdate()) and t.n1 not in ('Computación', 'Celulares, Telefonía y GPS', 'Cámaras y Fotografía', 'TV, Audio y Video', 'Videojuegos', 'Electrónicos')) where c.country='Mexico' and c.week=production.week_iso(date_sub(curdate(), interval 7 day));

update production.commercial_scorecard_regional c set share_inventory = (select a.inventory/b.total from (select count(order_number) as inventory from production.out_order_tracking where fullfilment_type_real='inventory' and production.week_exit(date_shipped) = production.week_exit(curdate())-1 and year(date_shipped)=year(curdate()))a, (select count(order_number) as total from production.out_order_tracking where production.week_exit(date_shipped) = production.week_exit(curdate())-1 and year(date_shipped)=year(curdate()))b) where country='Mexico' and c.week=production.week_iso(date_sub(curdate(), interval 7 day));

update production.commercial_scorecard_regional c set share_sell_list = (select b.outlet/a.hist from (select count(*) as hist from production.out_stock_hist where in_stock=1)a, (select count(*) as outlet from production.out_stock_hist where in_stock=1 and reserved=0 and fullfilment_type_bob in ('Crossdocking', 'Dropshipping', 'Own Warehouse') and max_days_in_stock>30 and average_remaining_days>90)b) where country='Mexico' and c.week=production.week_iso(date_sub(curdate(), interval 7 day));

update production.commercial_scorecard_regional c set diff_sku_sold= (select count(distinct sku) from production.tbl_order_detail t where t.oac=1 and production.week_exit(date) = production.week_exit(curdate())-1 and year(date)=year(curdate())) where c.country='Mexico' and c.week=production.week_iso(date_sub(curdate(), interval 7 day));

update production.commercial_scorecard_regional c set sku_visibles = (select count(sku_simple) from production.catalog_history where visible = 1 and date = date_sub(curdate(), interval 7 day)) where c.country='Mexico' and c.week=production.week_iso(date_sub(curdate(), interval 7 day));

-- Colombia

insert into production.commercial_scorecard_regional(week, country, revenue, PC2) select production.week_iso(date_sub(curdate(), interval 7 day)), 'Colombia', sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0))/2350 as revenue, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0))/2350 as PC2 from production_co.tbl_order_detail where oac=1 and production.week_exit(date) = production.week_exit(curdate())-1 and year(date)=year(curdate());

update production.commercial_scorecard_regional c set share_non_electronics= (select (sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0))/2350)/c.revenue from production_co.tbl_order_detail t where t.oac=1 and production.week_exit(date) = production.week_exit(curdate())-1 and year(date)=year(curdate()) and t.n1 not in ('Tecnología') ) where c.country='Colombia' and c.week=production.week_iso(date_sub(curdate(), interval 7 day));

update production.commercial_scorecard_regional c set diff_sku_sold= (select count(distinct sku) from production_co.tbl_order_detail t where t.oac=1 and production.week_exit(t.date) = production.week_exit(curdate())-1 and year(t.date)=year(curdate())) where c.country='Colombia' and c.week=production.week_iso(date_sub(curdate(), interval 7 day));

update production.commercial_scorecard_regional c set sku_visibles = (select count(sku_simple) from production_co.catalog_history where visible = 1 and date = date_sub(curdate(), interval 7 day)) where c.country='Colombia' and c.week=production.week_iso(date_sub(curdate(), interval 7 day));

-- Peru

insert into production.commercial_scorecard_regional(week, country, revenue, PC2) select production.week_iso(date_sub(curdate(), interval 7 day)), 'Peru', sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0))/3.3 as revenue, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0))/3.3 as PC2 from production_pe.tbl_order_detail where oac=1 and production.week_exit(date) = production.week_exit(curdate())-1 and year(date)=year(curdate());

update production.commercial_scorecard_regional c set share_non_electronics= (select (sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0))/3.3)/c.revenue from production_pe.tbl_order_detail t where t.oac=1 and production.week_exit(date) = production.week_exit(curdate())-1 and year(date)=year(curdate()) and t.n1 not in ('Computadoras', 'Celulares, Telefonía y GPS', 'Cámaras y Fotografía', 'TV, Audio y Video', 'Videojuegos') ) where c.country='Peru' and c.week=production.week_iso(date_sub(curdate(), interval 7 day));

update production.commercial_scorecard_regional c set share_inventory = (select a.inventory/b.total from (select count(order_number) as inventory from production_pe.out_order_tracking where fullfilment_type_real='inventory' and production.week_exit(date_shipped) = production.week_exit(curdate())-1 and year(date_shipped)=year(curdate()))a, (select count(order_number) as total from production_pe.out_order_tracking where production.week_exit(date_shipped) = production.week_exit(curdate())-1 and year(date_shipped)=year(curdate()))b) where country='Peru' and c.week=production.week_iso(date_sub(curdate(), interval 7 day));

update production.commercial_scorecard_regional c set diff_sku_sold= (select count(distinct sku) from production_pe.tbl_order_detail t where t.oac=1 and production.week_exit(t.date) = production.week_exit(curdate())-1 and year(date)=year(curdate())) where c.country='Peru' and c.week=production.week_iso(date_sub(curdate(), interval 7 day));

update production.commercial_scorecard_regional c set sku_visibles = (select count(sku_simple) from production_pe.catalog_history where visible = 1 and date = date_sub(curdate(), interval 7 day)) where c.country='Peru' and c.week=production.week_iso(date_sub(curdate(), interval 7 day));

-- Venezuela

insert into production.commercial_scorecard_regional(week, country, revenue, PC2) select production.week_iso(date_sub(curdate(), interval 7 day)), 'Venezuela', sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0))/39 as revenue, sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0))/39 as PC2 from production_ve.tbl_order_detail where oac=1 and production.week_exit(date) = production.week_exit(curdate())-1 and year(date)=year(curdate());

update production.commercial_scorecard_regional c set share_non_electronics= (select (sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0))/39)/c.revenue from production_ve.tbl_order_detail t where t.oac=1 and production.week_exit(date) = production.week_exit(curdate())-1 and year(date)=year(curdate()) and t.n1 not in ('Computadoras y Tabletas', 'Celulares, Teléfonos y GPS', 'Cámaras y Fotografía', 'TV, Audio y Video', 'Videojuegos') ) where c.country='Venezuela' and c.week=production.week_iso(date_sub(curdate(), interval 7 day));

update production.commercial_scorecard_regional c set share_inventory = (select a.inventory/b.total from (select count(order_number) as inventory from production_ve.out_order_tracking where fullfilment_type_real='inventory' and production.week_exit(date_shipped) = production.week_exit(curdate())-1 and year(date_shipped)=year(curdate()))a, (select count(order_number) as total from production_ve.out_order_tracking where production.week_exit(date_shipped) = production.week_exit(curdate())-1 and year(date_shipped)=year(curdate()))b) where country='Venezuela' and c.week=production.week_iso(date_sub(curdate(), interval 7 day));

update production.commercial_scorecard_regional c set diff_sku_sold= (select count(distinct sku) from production_ve.tbl_order_detail t where t.oac=1 and production.week_exit(t.date) = production.week_exit(curdate())-1 and year(date)=year(curdate())) where c.country='Venezuela' and c.week=production.week_iso(date_sub(curdate(), interval 7 day));

update production.commercial_scorecard_regional c set sku_visibles = (select count(sku_simple) from production_ve.catalog_history where visible = 1 and date = date_sub(curdate(), interval 7 day)) where c.country='Venezuela' and c.week=production.week_iso(date_sub(curdate(), interval 7 day));

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `content_management`()
begin

delete from production.content_management;

call production.catalog_visible;

insert into production.content_management(sku_config, sku_simple, status_config, status_simple, product_name, brand, model, color, product_weight, package_weight) select c.sku, s.sku, c.status, s.status, c.name, b.name, c.model, c.color, c.product_weight, c.package_weight from bob_live_mx.catalog_config c, bob_live_mx.catalog_simple s, bob_live_mx.catalog_brand b where c.id_catalog_config = s.fk_catalog_config and c.fk_catalog_brand=b.id_catalog_brand;

update production.content_management p inner join bob_live_mx.catalog_config conf on p.sku_config=conf.sku inner join bob_live_mx.catalog_attribute_option_global_category c on c.id_catalog_attribute_option_global_category=conf.fk_catalog_attribute_option_global_category set p.main_category = c.name;

update production.content_management p inner join bob_live_mx.catalog_config conf on p.sku_config=conf.sku inner join bob_live_mx.catalog_attribute_option_global_sub_category c on c.id_catalog_attribute_option_global_sub_category=conf.fk_catalog_attribute_option_global_sub_category set p.sub_category = c.name;

update production.content_management p inner join bob_live_mx.catalog_config conf on p.sku_config=conf.sku inner join bob_live_mx.catalog_attribute_option_global_sub_sub_category c on c.id_catalog_attribute_option_global_sub_sub_category=conf.fk_catalog_attribute_option_global_sub_sub_category set p.sub_sub_category = c.name;

update production.content_management m inner join production.catalog_visible v on m.sku_simple=v.sku_simple set visible = 1;

update production.content_management m set visible = 0 where visible is null;

update production.content_management m set sku_online_wh = (select case when sum(o.in_stock)>0 then 1 else 0 end from production.out_stock_hist o where o.sku_simple=m.sku_simple) where visible=1;

update production.content_management m set sku_online_wh = 0 where sku_online_wh is null;

update production.content_management m inner join bob_live_mx.catalog_config c on m.sku_config=c.sku set sku_pending_images = 1 where c.pet_status not like '%images%' or c.pet_status is null;

update production.content_management m set sku_pending_images = 0 where sku_pending_images is null; 

update production.content_management m inner join bob_live_mx.catalog_config c on m.sku_config=c.sku set sku_pending_edited = 1 where c.pet_status not like '%edited%' or c.pet_status is null;

update production.content_management m set sku_pending_edited = 0 where sku_pending_edited is null; 

update production.content_management m inner join bob_live_mx.catalog_config c on m.sku_config=c.sku set m.sku_pending_QC = case when c.pet_approved=0 then 1 else 0 end;

update production.content_management m set sku_pending_QC = 0 where sku_pending_QC is null;

update production.content_management m set sku_pending_all_wh_over_5_days = (select case when max_days_in_stock>5 and sum(o.in_stock)>0 then 1 else 0 end from production.out_stock_hist o inner join bob_live_mx.catalog_config c on o.sku_config=c.sku where o.sku_simple=m.sku_simple and c.pet_status is null group by o.sku_config);

update production.content_management m set sku_pending_all_wh_over_5_days = 0 where sku_pending_all_wh_over_5_days is null;

update production.content_management m set sku_pending_all_wh_less_5_days = (select case when max_days_in_stock<5 and sum(o.in_stock)>0 then 1 else 0 end from production.out_stock_hist o inner join bob_live_mx.catalog_config c on o.sku_config=c.sku where o.sku_simple=m.sku_simple and c.pet_status is null group by o.sku_config);

update production.content_management m set sku_pending_all_wh_less_5_days = 0 where sku_pending_all_wh_less_5_days is null;

update production.content_management m set sku_pending_images_wh_over_5_days = (select case when max_days_in_stock>5 and sum(o.in_stock)>0 then 1 else 0 end from production.out_stock_hist o inner join bob_live_mx.catalog_config c on o.sku_config=c.sku where o.sku_simple=m.sku_simple and (c.pet_status not like '%images%' or c.pet_status is null) group by o.sku_config);

update production.content_management m set sku_pending_images_wh_over_5_days = 0 where sku_pending_images_wh_over_5_days is null;

update production.content_management m set sku_pending_images_wh_less_5_days = (select case when max_days_in_stock<5 and sum(o.in_stock)>0 then 1 else 0 end from production.out_stock_hist o inner join bob_live_mx.catalog_config c on o.sku_config=c.sku where o.sku_simple=m.sku_simple and (c.pet_status not like '%images%' or c.pet_status is null) group by o.sku_config);

update production.content_management m set sku_pending_images_wh_less_5_days = 0 where sku_pending_images_wh_less_5_days is null;

update production.content_management m set sku_pending_content_wh_over_5_days = (select case when max_days_in_stock>5 and sum(o.in_stock)>0 then 1 else 0 end from production.out_stock_hist o inner join bob_live_mx.catalog_config c on o.sku_config=c.sku where o.sku_simple=m.sku_simple and (c.pet_status not like '%edited%' or c.pet_status is null) group by o.sku_config);

update production.content_management m set sku_pending_content_wh_over_5_days = 0 where sku_pending_content_wh_over_5_days is null;

update production.content_management m set sku_pending_content_wh_less_5_days = (select case when max_days_in_stock<5 and sum(o.in_stock)>0 then 1 else 0 end from production.out_stock_hist o inner join bob_live_mx.catalog_config c on o.sku_config=c.sku where o.sku_simple=m.sku_simple and (c.pet_status not like '%edited%' or c.pet_status is null) group by o.sku_config);

update production.content_management m set sku_pending_content_wh_less_5_days = 0 where sku_pending_content_wh_less_5_days is null;

update production.content_management m set sku_pending_to_go_live = (select case when c.pet_status = 'creation,edited,images' and sum(o.in_stock)>0 and c.pet_approved=0 then 1 else 0 end from production.out_stock_hist o inner join bob_live_mx.catalog_config c on o.sku_config=c.sku where o.sku_simple=m.sku_simple group by o.sku_config);

update production.content_management m set sku_pending_to_go_live = 0 where sku_pending_to_go_live is null;

update production.content_management m inner join bob_live_mx.catalog_simple s on m.sku_simple=s.sku set sku_without_upc = case when s.barcode_ean is null then 1 else 0 end; 

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `content_sharing_tool`()
begin

delete from production.catalog_config;

alter table production.catalog_config AUTO_INCREMENT = 1;

insert into production.catalog_config(country, id_catalog_config, fk_catalog_brand, sku_config, status_supplier_config, description, name, pet_status, pet_approved, created_at, updated_at, updated_by_config, activated_at, sku_supplier_config, creation_source_config, model, imported, product_measures, product_weight, package_height, package_length, package_width, package_weight, short_description, color, product_warranty, fk_supplier, status, sku_simple) select 'Mexico', c.id_catalog_config, c.fk_catalog_brand, c.sku, c.status_supplier_config, c.description, c.name, c.pet_status, c.pet_approved, c.created_at, c.updated_at, c.updated_by_config, c.activated_at, c.sku_supplier_config, c.creation_source_config, c.model, c.imported, c.product_measures, c.product_weight, c.package_height, c.package_length, c.package_width, c.package_weight, c.short_description, c.color, c.product_warranty, c.fk_supplier, c.status, s.sku from bob_live_mx.catalog_config c, bob_live_mx.catalog_simple s where s.fk_catalog_config=c.id_catalog_config;

insert into production.catalog_config(country, id_catalog_config, fk_catalog_brand, sku_config, status_supplier_config, description, name, pet_status, pet_approved, created_at, updated_at, updated_by_config, activated_at, sku_supplier_config, creation_source_config, model, imported, product_measures, product_weight, package_height, package_length, package_width, package_weight, short_description, color, product_warranty, fk_supplier, status, sku_simple) select 'Colombia', c.id_catalog_config, c.fk_catalog_brand, c.sku, c.status_supplier_config, c.description, c.name, c.pet_status, c.pet_approved, c.created_at, c.updated_at, c.updated_by_config, c.activated_at, c.sku_supplier_config, c.creation_source_config, c.model, c.imported, c.product_measures, c.product_weight, c.package_height, c.package_length, c.package_width, c.package_weight, c.short_description, c.color, c.product_warranty, c.fk_supplier, c.status, s.sku from bob_live_co.catalog_config c, bob_live_co.catalog_simple s where s.fk_catalog_config=c.id_catalog_config;

insert into production.catalog_config(country, id_catalog_config, fk_catalog_brand, sku_config, status_supplier_config, description, name, pet_status, pet_approved, created_at, updated_at, updated_by_config, activated_at, sku_supplier_config, creation_source_config, model, imported, product_measures, product_weight, package_height, package_length, package_width, package_weight, short_description, color, product_warranty, fk_supplier, status, sku_simple) select 'Peru', c.id_catalog_config, c.fk_catalog_brand, c.sku, c.status_supplier_config, c.description, c.name, c.pet_status, c.pet_approved, c.created_at, c.updated_at, c.updated_by_config, c.activated_at, c.sku_supplier_config, c.creation_source_config, c.model, c.imported, c.product_measures, c.product_weight, c.package_height, c.package_length, c.package_width, c.package_weight, c.short_description, c.color, c.product_warranty, c.fk_supplier, c.status, s.sku from bob_live_pe.catalog_config c, bob_live_pe.catalog_simple s where s.fk_catalog_config=c.id_catalog_config;

insert into production.catalog_config(country, id_catalog_config, fk_catalog_brand, sku_config, status_supplier_config, description, name, pet_status, pet_approved, created_at, updated_at, updated_by_config, activated_at, sku_supplier_config, creation_source_config, model, imported, product_measures, product_weight, package_height, package_length, package_width, package_weight, short_description, color, product_warranty, fk_supplier, status, sku_simple) select 'Venezuela', c.id_catalog_config, c.fk_catalog_brand, c.sku, c.status_supplier_config, c.description, c.name, c.pet_status, c.pet_approved, c.created_at, c.updated_at, c.updated_by_config, c.activated_at, c.sku_supplier_config, c.creation_source_config, c.model, c.imported, c.product_measures, c.product_weight, c.package_height, c.package_length, c.package_width, c.package_weight, c.short_description, c.color, c.product_warranty, c.fk_supplier, c.status, s.sku from bob_live_ve.catalog_config c, bob_live_ve.catalog_simple s where s.fk_catalog_config=c.id_catalog_config;

update production.catalog_config c inner join bob_live_mx.catalog_simple s on c.id_catalog_config=s.fk_catalog_config set c.id_catalog_simple=s.id_catalog_simple, c.barcode_ean=s.barcode_ean where country = 'Mexico';

update production.catalog_config c inner join bob_live_co.catalog_simple s on c.id_catalog_config=s.fk_catalog_config set c.id_catalog_simple=s.id_catalog_simple, c.barcode_ean=s.barcode_ean where country = 'Colombia';

update production.catalog_config c inner join bob_live_pe.catalog_simple s on c.id_catalog_config=s.fk_catalog_config set c.id_catalog_simple=s.id_catalog_simple, c.barcode_ean=s.barcode_ean where country = 'Peru';

update production.catalog_config c inner join bob_live_ve.catalog_simple s on c.id_catalog_config=s.fk_catalog_config set c.id_catalog_simple=s.id_catalog_simple, c.barcode_ean=s.barcode_ean where country = 'Venezuela';

update production.catalog_config c inner join bob_live_mx.catalog_brand b on c.fk_catalog_brand=b.id_catalog_brand set c.brand_name=b.name where country = 'Mexico';

update production.catalog_config c inner join bob_live_co.catalog_brand b on c.fk_catalog_brand=b.id_catalog_brand set c.brand_name=b.name where country = 'Colombia';

update production.catalog_config c inner join bob_live_pe.catalog_brand b on c.fk_catalog_brand=b.id_catalog_brand set c.brand_name=b.name where country = 'Peru';

update production.catalog_config c inner join bob_live_ve.catalog_brand b on c.fk_catalog_brand=b.id_catalog_brand set c.brand_name=b.name where country = 'Venezuela';

call production.catalog_visible;

update production.catalog_config c inner join production.catalog_visible v on c.sku_simple=v.sku_simple set visible = 1 where country = 'Mexico';

update production.catalog_config  set visible = 0 where visible is null and country='Mexico';

call production_co.catalog_visible;

update production.catalog_config c inner join production_co.catalog_visible v on c.sku_simple=v.sku_simple set visible = 1 where country = 'Colombia';

update production.catalog_config  set visible = 0 where visible is null and country='Colombia';

call production_pe.catalog_visible;

update production.catalog_config c inner join production_pe.catalog_visible v on c.sku_simple=v.sku_simple set visible = 1 where country = 'Peru';

update production.catalog_config  set visible = 0 where visible is null and country='Peru';

call production_ve.catalog_visible;

update production.catalog_config c inner join production_ve.catalog_visible v on c.sku_simple=v.sku_simple set visible = 1 where country = 'Venezuela';

update production.catalog_config  set visible = 0 where visible is null and country='Venezuela';

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `customers_per_category_item`()
begin

delete from production.customers_per_category_item;

insert into production.customers_per_category_item(first_name, last_name, email, Appliances, Photography, Audio_Video, Books, Video_Games, Fashion, Health_and_Beauty, Cellphones, Computing, Home, Kids_and_Babies, Sports, Total)
select z.first_name, z.last_name, z.email, ifnull(a.Appliances,0) as Appliances, ifnull(b.Photography,0) as Photography, ifnull(c.Audio_Video,0) as Audio_Video, ifnull(d.Books,0) as Books, ifnull(e.Video_Games,0) as Video_Games, ifnull(f.Fashion,0) as Fashion, ifnull(g.Health_and_Beauty,0) as Health_and_Beauty, ifnull(h.Cellphones,0) as Cellphones, ifnull(i.Computing,0) as Computing, ifnull(j.Home,0) as Home, ifnull(k.Kids_and_Babies,0) as Kids_and_Babies, ifnull(l.Sports,0) as Sports, ifnull(a.Appliances+b.Photography+c.Audio_Video+d.Books+e.Video_Games+f.Fashion+g.Health_and_Beauty+h.Cellphones+i.Computing+j.Home+k.Kids_and_Babies+l.Sports,0) as Total from (select custid, first_name, last_name, email from production.tbl_order_detail, bob_live_mx.customer where customer.id_customer=tbl_order_detail.custid and oac=1 group by custid)z left join (select custid, first_name, last_name, email, sum(oac) as Appliances from production.tbl_order_detail, bob_live_mx.customer where customer.id_customer=tbl_order_detail.custid and n1='electrodomésticos' and oac=1 group by custid)a on z.custid=a.custid left join (select custid, first_name, last_name, email, sum(oac) as Photography from production.tbl_order_detail, bob_live_mx.customer where customer.id_customer=tbl_order_detail.custid and n1='cámaras y fotografía' and oac=1 group by custid)b on z.custid=b.custid left join (select custid, first_name, last_name, email, sum(oac) as Audio_Video from production.tbl_order_detail, bob_live_mx.customer where customer.id_customer=tbl_order_detail.custid and n1='tv, audio y video' and oac=1 group by custid)c on z.custid=c.custid left join (select custid, first_name, last_name, email, sum(oac) as Books from production.tbl_order_detail, bob_live_mx.customer where customer.id_customer=tbl_order_detail.custid and n1 like 'libros%' and oac=1 group by custid)d on z.custid=d.custid left join (select custid, first_name, last_name, email, sum(oac) as Video_Games from production.tbl_order_detail, bob_live_mx.customer where customer.id_customer=tbl_order_detail.custid and n1='videojuegos' and oac=1 group by custid)e on z.custid=e.custid left join (select custid, first_name, last_name, email, sum(oac) as Fashion from production.tbl_order_detail, bob_live_mx.customer where customer.id_customer=tbl_order_detail.custid and n1='ropa, calzado y accesorios' and oac=1 group by custid)f on z.custid=f.custid left join (select custid, first_name, last_name, email, sum(oac) as Health_and_Beauty from production.tbl_order_detail, bob_live_mx.customer where customer.id_customer=tbl_order_detail.custid and n1 like '%cuidado personal%' and oac=1 group by custid)g on z.custid=g.custid left join (select custid, first_name, last_name, email, sum(oac) as Cellphones from production.tbl_order_detail, bob_live_mx.customer where customer.id_customer=tbl_order_detail.custid and n1 like 'celulares%' and oac=1 group by custid)h on z.custid=h.custid left join (select custid, first_name, last_name, email, sum(oac) as Computing from production.tbl_order_detail, bob_live_mx.customer where customer.id_customer=tbl_order_detail.custid and n1 like 'compu%' and oac=1 group by custid)i on z.custid=i.custid left join (select custid, first_name, last_name, email, sum(oac) as Home from production.tbl_order_detail, bob_live_mx.customer where customer.id_customer=tbl_order_detail.custid and n1='hogar' or n1 like '%muebles' and oac=1 group by custid)j on z.custid=j.custid left join (select custid, first_name, last_name, email, sum(oac) as Kids_and_Babies from production.tbl_order_detail, bob_live_mx.customer where customer.id_customer=tbl_order_detail.custid and n1='juguetes, niños y bebés' and oac=1 group by custid)k on z.custid=k.custid left join (select custid, first_name, last_name, email, sum(oac) as Sports from production.tbl_order_detail, bob_live_mx.customer where customer.id_customer=tbl_order_detail.custid and n1='deportes' and oac=1 group by custid)l on z.custid=l.custid group by z.custid order by total desc;

update production.customers_per_category_item set Total = Appliances+Photography+Audio_Video+Books+Video_Games+Fashion+Health_and_Beauty+Cellphones+Computing+Home+Kids_and_Babies+Sports;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 PROCEDURE `daily_execution_ops`()
begin

 

select  'daily execution ops: start',now();

 

-- Operaciones

-- Macro A

 

-- A200

delete production.pro_sku_con_categorias.* from production.pro_sku_con_categorias;

 

-- A201

insert into production.pro_sku_con_categorias ( sku, catalog_config_name, catalog_category_name, lft, rgt )

select catalog_config.sku, catalog_config.name, catalog_category.name, catalog_category.lft, catalog_category.rgt

from (bob_live_mx.catalog_config inner join bob_live_mx.catalog_config_has_catalog_category on catalog_config.id_catalog_config = catalog_config_has_catalog_category.fk_catalog_config) inner join bob_live_mx.catalog_category on catalog_config_has_catalog_category.fk_catalog_category = catalog_category.id_catalog_category

where catalog_category.lft<>1;

 

-- A202

update production.pro_sku_con_categorias set pro_sku_con_categorias.dif = rgt-lft;

 

-- A211

delete production.pro_category_tree.* from production.pro_category_tree;

 

delete from production.cat1_step_1_test;

delete from production.cat1_final_test;

delete from production.cat2_step_1_test;

delete from production.cat2_step_2_test;

delete from production.cat2_final_test;

delete from production.cat3_step_1_test;

delete from production.cat3_step_2_test;

delete from production.cat3_final_test;

 

insert into production.cat1_step_1_test (sku, catalog_config_name, maxofdif) select pro_sku_con_categorias.sku, pro_sku_con_categorias.catalog_config_name, max(pro_sku_con_categorias.dif) as maxofdif from production.pro_sku_con_categorias group by pro_sku_con_categorias.sku, pro_sku_con_categorias.catalog_config_name;

 

insert into production.cat1_final_test (sku, catalog_config_name, catalog_category_name) select cat1_step_1_test.sku, cat1_step_1_test.catalog_config_name, pro_sku_con_categorias.catalog_category_name

from production.cat1_step_1_test inner join production.pro_sku_con_categorias on (cat1_step_1_test.maxofdif = pro_sku_con_categorias.dif) and (cat1_step_1_test.sku = pro_sku_con_categorias.sku);

 

 

insert into production.cat2_step_1_test (sku, catalog_config_name, catalog_category_name, dif) select pro_sku_con_categorias.sku, pro_sku_con_categorias.catalog_config_name, pro_sku_con_categorias.catalog_category_name, pro_sku_con_categorias.dif from production.pro_sku_con_categorias inner join production.cat1_step_1_test on pro_sku_con_categorias.sku = cat1_step_1_test.sku where (((pro_sku_con_categorias.dif)<maxofdif));

 

insert into production.cat2_step_2_test (sku, catalog_config_name, maxofdif) select cat2_step_1_test.sku, cat2_step_1_test.catalog_config_name, max(cat2_step_1_test.dif) as maxofdif from production.cat2_step_1_test group by cat2_step_1_test.sku, cat2_step_1_test.catalog_config_name;

 

insert into production.cat2_final_test (sku, catalog_config_name, catalog_category_name) select cat2_step_2_test.sku, cat2_step_2_test.catalog_config_name, pro_sku_con_categorias.catalog_category_name

from production.cat2_step_2_test inner join production.pro_sku_con_categorias on (cat2_step_2_test.sku = pro_sku_con_categorias.sku) and (cat2_step_2_test.maxofdif = pro_sku_con_categorias.dif);

 

 

insert into production.cat3_step_1_test (sku, catalog_config_name, catalog_category_name, dif) select pro_sku_con_categorias.sku, pro_sku_con_categorias.catalog_config_name, pro_sku_con_categorias.catalog_category_name, pro_sku_con_categorias.dif

from production.pro_sku_con_categorias inner join production.cat2_step_2_test on pro_sku_con_categorias.sku = cat2_step_2_test.sku

where (((pro_sku_con_categorias.dif)<maxofdif));

 

insert into production.cat3_step_2_test (sku, catalog_config_name, maxofdif) select cat3_step_1_test.sku, cat3_step_1_test.catalog_config_name, max(cat3_step_1_test.dif) as maxofdif

from production.cat3_step_1_test

group by cat3_step_1_test.sku, cat3_step_1_test.catalog_config_name;

 

insert into production.cat3_final_test (sku, catalog_config_name, catalog_category_name) select pro_sku_con_categorias.sku, pro_sku_con_categorias.catalog_config_name,  pro_sku_con_categorias.catalog_category_name

from production.cat3_step_2_test inner join production.pro_sku_con_categorias on (cat3_step_2_test.sku = pro_sku_con_categorias.sku) and (cat3_step_2_test.maxofdif = pro_sku_con_categorias.dif);

 

-- A212

insert into production.pro_category_tree ( sku, catalog_config_name, cat1, cat2, cat3 )

select cat1_final_test.sku, cat1_final_test.catalog_config_name, cat1_final_test.catalog_category_name as cat1, cat2_final_test.catalog_category_name as cat2, cat3_final_test.catalog_category_name as cat3 from (production.cat1_final_test left join production.cat2_final_test on cat1_final_test.sku = cat2_final_test.sku) left join production.cat3_final_test on cat2_final_test.sku = cat3_final_test.sku;

 

-- Operaciones

-- Macro B

 

-- B100 A

delete production.out_order_tracking.* from production.out_order_tracking;

 

-- B101 A

insert into production.out_order_tracking ( order_item_id, order_number, sku_simple ) select itens_venda.item_id, itens_venda.numero_order, itens_venda.sku from wmsprod.itens_venda;

 

-- B102 U

update ((bob_live_mx.sales_order_address right join (production.out_order_tracking inner join bob_live_mx.sales_order on out_order_tracking.order_number = sales_order.order_nr) on sales_order_address.id_sales_order_address = sales_order.fk_sales_order_address_billing) left join bob_live_mx.customer_address_region on sales_order_address.fk_customer_address_region = customer_address_region.id_customer_address_region) inner join bob_live_mx.sales_order_item on out_order_tracking.order_item_id = sales_order_item.id_sales_order_item set out_order_tracking.date_ordered = date(sales_order.created_at), date_time_ordered = sales_order.created_at, out_order_tracking.payment_method = sales_order.payment_method, out_order_tracking.ship_to_state = customer_address_region.code, out_order_tracking.min_delivery_time = sales_order_item.min_delivery_time, out_order_tracking.max_delivery_time = sales_order_item.max_delivery_time, out_order_tracking.supplier_leadtime = sales_order_item.delivery_time_supplier;

 

-- B103 U

update production.out_order_tracking inner join bob_live_mx.catalog_simple on out_order_tracking.sku_simple = catalog_simple.sku inner join bob_live_mx.catalog_config on catalog_simple.fk_catalog_config = catalog_config.id_catalog_config inner join bob_live_mx.supplier on catalog_config.fk_supplier = supplier.id_supplier set out_order_tracking.sku_config = catalog_config.sku, out_order_tracking.sku_name = catalog_config.name, out_order_tracking.supplier_id = supplier.id_supplier, out_order_tracking.supplier_name = supplier.name, out_order_tracking.min_delivery_time = case when out_order_tracking.min_delivery_time is null then catalog_simple.min_delivery_time else out_order_tracking.min_delivery_time end, out_order_tracking.max_delivery_time = case when out_order_tracking.max_delivery_time is null then catalog_simple.max_delivery_time else out_order_tracking.max_delivery_time end, out_order_tracking.package_height = catalog_config.package_height, out_order_tracking.package_length = catalog_config.package_length, out_order_tracking.package_width = catalog_config.package_width, out_order_tracking.package_weight = catalog_config.package_weight;

 

-- B103b U

update production.out_order_tracking inner join bob_live_mx.catalog_simple on out_order_tracking.sku_simple = catalog_simple.sku inner join bob_live_mx.catalog_config on catalog_simple.fk_catalog_config = catalog_config.id_catalog_config set out_order_tracking.supplier_leadtime = case when out_order_tracking.supplier_leadtime is null then (case when catalog_config.supplier_lead_time is null then 0 else catalog_config.supplier_lead_time end) else out_order_tracking.supplier_leadtime end;

 

-- B103c U extra

update production.out_order_tracking set out_order_tracking.fullfilment_type_real = "inventory";

 

-- B104 U

update production.out_order_tracking inner join (wmsprod.itens_venda inner join wmsprod.status_itens_venda on itens_venda.itens_venda_id = status_itens_venda.itens_venda_id) on out_order_tracking.order_item_id = itens_venda.item_id set out_order_tracking.fullfilment_type_real = "crossdock" where status_itens_venda.status="analisando quebra" or status_itens_venda.status="aguardando estoque";

 

-- B104 U Extra

update (production.out_order_tracking inner join wmsprod.itens_venda on out_order_tracking.order_item_id = itens_venda.item_id) inner join wmsprod.status_itens_venda on itens_venda.itens_venda_id = status_itens_venda.itens_venda_id set out_order_tracking.fullfilment_type_real = "dropshipping"

where (((status_itens_venda.status)="ds estoque reservado"));

 

-- B105 U

update production.out_order_tracking inner join production.pro_category_tree on out_order_tracking.sku_config = pro_category_tree.sku set out_order_tracking.category = cat1, out_order_tracking.cat2 = pro_category_tree.cat2;

 

-- B105a U

update production.out_order_tracking inner join production.exceptions_supplier_categories on out_order_tracking.supplier_name = exceptions_supplier_categories.supplier set out_order_tracking.category = "ropa, calzado y accesorios"

where out_order_tracking.category="deportes";

 

-- B105b U

update production.out_order_tracking set out_order_tracking.category = "salud y cuidado personal" where out_order_tracking.category="ropa, calzado y accesorios" and out_order_tracking.cat2="salud y cuidado personal";

 

-- B106 U

update production.out_order_tracking inner join production.buyer_category on out_order_tracking.category = buyer_category.category set out_order_tracking.category_english = buyer_category.category_english;

 

-- B110 D

delete production.pro_min_date_exported.* from production.pro_min_date_exported;

 

-- B111 A

insert into production.pro_min_date_exported ( fk_sales_order_item, name, min_of_created_at )

select sales_order_item_status_history.fk_sales_order_item, sales_order_item_status.name, min(sales_order_item_status_history.created_at) as min_of_created_at from bob_live_mx.sales_order_item_status inner join bob_live_mx.sales_order_item_status_history on sales_order_item_status.id_sales_order_item_status = sales_order_item_status_history.fk_sales_order_item_status group by sales_order_item_status_history.fk_sales_order_item, sales_order_item_status.name having (((sales_order_item_status.name)="exported"));

 

-- B112 U

update production.out_order_tracking inner join production.pro_min_date_exported on out_order_tracking.order_item_id = pro_min_date_exported.fk_sales_order_item set out_order_tracking.date_exported = date(min_of_created_at), out_order_tracking.date_time_exported = min_of_created_at;

 

-- B120 U

update production.out_order_tracking inner join wmsprod.itens_venda on out_order_tracking.order_item_id = itens_venda.item_id inner join wmsprod.status_itens_venda on itens_venda.itens_venda_id = status_itens_venda.itens_venda_id set out_order_tracking.date_procured = date(data), out_order_tracking.date_time_procured = data where status_itens_venda.status="estoque reservado";

 

-- B120b U

update production.out_order_tracking set out_order_tracking.date_ready_to_pick = case when dayofweek(date_procured)=1 then production.workday(date_procured,1) else (case when dayofweek(date_procured)=7 then production.workday(date_procured,1) else date_procured end)end;

 

-- B120b2 U

update production.out_order_tracking set out_order_tracking.date_time_ready_to_pick = case when dayofweek(date_time_procured)=1 then production.workday(date_time_procured,1) else (case when dayofweek(date_time_procured)=7 then production.workday(date_time_procured,1) else date_procured end)end;

 

-- B120c U

update production.out_order_tracking set out_order_tracking.date_ready_to_pick = case when date_ready_to_pick in('2012-11-19','2012-12-25','2013-1-1','2013-2-4','2013-3-18','2013-05-01') then production.workday(date_ready_to_pick,1) when date_ready_to_pick = '2013-3-28' then production.workday(date_ready_to_pick,2) else date_ready_to_pick end,

out_order_tracking.date_time_ready_to_pick = case when date_ready_to_pick in('2012-11-19','2012-12-25','2013-1-1','2013-2-4','2013-3-18','2013-05-01') then production.workday(date_time_ready_to_pick,1) when date_ready_to_pick = '2013-3-28' then production.workday(date_time_ready_to_pick,2) else date_time_ready_to_pick end;

 

-- B121a D

delete production.pro_max_date_ready_to_ship.* from production.pro_max_date_ready_to_ship;

 

-- B121b A

insert into production.pro_max_date_ready_to_ship ( date_ready, order_item_id )

select min(date(data)) as date_ready, out_order_tracking.order_item_id

from production.out_order_tracking inner join wmsprod.itens_venda on out_order_tracking.order_item_id = itens_venda.item_id inner join wmsprod.status_itens_venda on itens_venda.itens_venda_id = status_itens_venda.itens_venda_id

where status_itens_venda.status="faturado"

group by out_order_tracking.order_item_id;

 

-- B121c U

update production.out_order_tracking inner join production.pro_max_date_ready_to_ship on out_order_tracking.order_item_id = pro_max_date_ready_to_ship.order_item_id set out_order_tracking.date_ready_to_ship = pro_max_date_ready_to_ship.date_ready;

 

-- B122 U

update production.out_order_tracking inner join (wmsprod.itens_venda b inner join wmsprod.status_itens_venda d on b.itens_venda_id = d.itens_venda_id) on out_order_tracking.order_item_id = b.item_id set out_order_tracking.date_shipped = (select max(date(data)) from wmsprod.itens_venda c, wmsprod.status_itens_venda e where e.status="expedido" and b.itens_venda_id = c.itens_venda_id and e.itens_venda_id = d.itens_venda_id),

out_order_tracking.date_time_shipped = (select max(data) from wmsprod.itens_venda c, wmsprod.status_itens_venda e where e.status="expedido" and b.itens_venda_id = c.itens_venda_id and e.itens_venda_id = d.itens_venda_id);

 

-- B123 U

update production.out_order_tracking inner join wmsprod.vw_itens_venda_entrega vw on out_order_tracking.order_item_id = vw.item_id inner join wmsprod.tms_status_delivery del on vw.cod_rastreamento = del.cod_rastreamento set out_order_tracking.date_delivered = (select case when tms.date < '2012-05-01' then null else max(tms.date) end from wmsprod.vw_itens_venda_entrega itens, wmsprod.tms_status_delivery tms where itens.cod_rastreamento = vw.cod_rastreamento and tms.cod_rastreamento = del.cod_rastreamento and tms.id_status = 4);

 

-- B123a U

update production.out_order_tracking inner join wmsprod.itens_venda on out_order_tracking.order_item_id = itens_venda.item_id set out_order_tracking.wh_time = tempo_armazem where tempo_armazem >1;

 

-- B124 U

update production.out_order_tracking set out_order_tracking.date_procured_promised = production.workday(date_exported, supplier_leadtime), out_order_tracking.date_delivered_promised = production.workday(date_exported,max_delivery_time)

where out_order_tracking.date_exported is not null;

 

-- B124a U

update bob_live_mx.sales_order_item inner join production.out_order_tracking on sales_order_item.id_sales_order_item = out_order_tracking.order_item_id set out_order_tracking.is_linio_promise = sales_order_item.is_linio_promise;

 

-- B124b U

update production.out_order_tracking set is_linio_promise = 0 where is_linio_promise is null;

 

-- B124c U

update production.out_order_tracking set date_delivered_promised = case when payment_method in('Banorte_PagoReferenciado','Oxxo_Direct') then production.workday(date_exported,2) else production.workday(date_ordered,2) end where is_linio_promise = 1;

 

-- B124b1 U

update production.out_order_tracking set out_order_tracking.date_procured_promised = production.workday(out_order_tracking.date_procured_promised,1)

where out_order_tracking.date_procured_promised>='2012-11-19' and out_order_tracking.date_exported<'2012-11-19';

 

-- B124b2 U

update production.out_order_tracking set out_order_tracking.date_delivered_promised = production.workday(out_order_tracking.date_delivered_promised,1)

where out_order_tracking.date_delivered_promised>='2012-11-19' and out_order_tracking.date_exported<'2012-11-19';

 

-- B124c1 U

update production.out_order_tracking set out_order_tracking.date_procured_promised = production.workday(out_order_tracking.date_procured_promised,1)

where out_order_tracking.date_procured_promised>='2012-12-25' and out_order_tracking.date_exported<'2012-12-25';

 

-- B124c2 U

update production.out_order_tracking set out_order_tracking.date_delivered_promised = production.workday(out_order_tracking.date_delivered_promised,1)

where out_order_tracking.date_delivered_promised>='2012-12-25' and out_order_tracking.date_exported<'2012-12-25';

 

-- B124d1 U

update production.out_order_tracking set out_order_tracking.date_procured_promised = production.workday(out_order_tracking.date_procured_promised,1)

where out_order_tracking.date_procured_promised>='2013-1-1' and out_order_tracking.date_exported<'2013-1-1';

 

-- B124d2 U

update production.out_order_tracking set out_order_tracking.date_delivered_promised = production.workday(out_order_tracking.date_delivered_promised,1) where out_order_tracking.date_delivered_promised>='2013-1-1' and out_order_tracking.date_exported<'2013-1-1';

 

-- B124d4 U

update production.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production.workday(out_order_tracking.date_ready_to_ship_promised,1) where out_order_tracking.date_ready_to_ship_promised>='2013-1-1' and out_order_tracking.date_exported<'2013-1-1';

 

-- B124e1 U

update production.out_order_tracking set out_order_tracking.date_procured_promised = production.workday(out_order_tracking.date_procured_promised,1)

where out_order_tracking.date_procured_promised>='2013-2-4' and out_order_tracking.date_exported<'2013-2-4';

 

-- B124e2 U

update production.out_order_tracking set out_order_tracking.date_delivered_promised = production.workday(out_order_tracking.date_delivered_promised,1) where out_order_tracking.date_delivered_promised>='2013-2-4' and out_order_tracking.date_exported<'2013-2-4';

 

-- B124e4 U

update production.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production.workday(out_order_tracking.date_ready_to_ship_promised,1) where out_order_tracking.date_ready_to_ship_promised>='2013-2-4' and out_order_tracking.date_exported<'2013-2-4';

 

-- B124f1 U

update production.out_order_tracking set out_order_tracking.date_procured_promised = production.workday(out_order_tracking.date_procured_promised,1)

where out_order_tracking.date_procured_promised>='2013-3-18' and out_order_tracking.date_exported<'2013-3-18';

 

-- B124f2 U

update production.out_order_tracking set out_order_tracking.date_delivered_promised = production.workday(out_order_tracking.date_delivered_promised,1)

where out_order_tracking.date_delivered_promised>='2013-3-18' and out_order_tracking.date_exported<'2013-3-18';

 

-- B124f4 U

update production.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production.workday(out_order_tracking.date_ready_to_ship_promised,1) where out_order_tracking.date_ready_to_ship_promised>='2013-3-18' and out_order_tracking.date_exported<'2013-3-18';

 

-- B124g1 U

update production.out_order_tracking set out_order_tracking.date_procured_promised = production.workday(out_order_tracking.date_procured_promised,1)

where out_order_tracking.date_procured_promised>='2013-3-28' and out_order_tracking.date_exported<'2013-3-28';

 

-- B124g2 U

update production.out_order_tracking set out_order_tracking.date_delivered_promised = production.workday(out_order_tracking.date_delivered_promised,1)

where out_order_tracking.date_delivered_promised>='2013-3-28' and out_order_tracking.date_exported<'2013-3-28';

 

-- B124g4 U

update production.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production.workday(out_order_tracking.date_ready_to_ship_promised,1) where out_order_tracking.date_ready_to_ship_promised>='2013-3-28' and out_order_tracking.date_exported<'2013-3-28';

 

-- B124h1 U

update production.out_order_tracking set out_order_tracking.date_procured_promised = production.workday(out_order_tracking.date_procured_promised,1)

where out_order_tracking.date_procured_promised>='2013-3-29' and out_order_tracking.date_exported<'2013-3-29';

 

-- B124h2 U

update production.out_order_tracking set out_order_tracking.date_delivered_promised = production.workday(out_order_tracking.date_delivered_promised,1)

where out_order_tracking.date_delivered_promised>='2013-3-29' and out_order_tracking.date_exported<'2013-3-29';

 

-- B124h4 U

update production.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production.workday(out_order_tracking.date_ready_to_ship_promised,1) where out_order_tracking.date_ready_to_ship_promised>='2013-3-29' and out_order_tracking.date_exported<'2013-3-29';

 

-- B124i1 U

update production.out_order_tracking set out_order_tracking.date_procured_promised = production.workday(out_order_tracking.date_procured_promised,1)

where out_order_tracking.date_procured_promised>='2013-05-01' and out_order_tracking.date_exported<'2013-05-01';

 

-- B124i2 U

update production.out_order_tracking set out_order_tracking.date_delivered_promised = production.workday(out_order_tracking.date_delivered_promised,1)

where out_order_tracking.date_delivered_promised>='2013-05-01' and out_order_tracking.date_exported<'2013-05-01';

 

-- B124i4 U

update production.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production.workday(out_order_tracking.date_ready_to_ship_promised,1) where out_order_tracking.date_ready_to_ship_promised>='2013-05-01' and out_order_tracking.date_exported<'2013-05-01';

 

-- B124f U

update production.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production.workday(date_procured_promised, wh_time);

 

-- B124f U Extra

update production.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production.workday(out_order_tracking.date_ready_to_ship_promised,1)

where (((out_order_tracking.date_ready_to_ship_promised)>='2013-03-18') and ((out_order_tracking.date_procured_promised)<'2013-03-18'));

 

-- B124g U Extra

update production.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production.workday(out_order_tracking.date_ready_to_ship_promised,1)

where (((out_order_tracking.date_ready_to_ship_promised)>='2013-03-28') and ((out_order_tracking.date_procured_promised)<'2013-03-28'));

 

-- B124g U Extra

update production.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production.workday(out_order_tracking.date_ready_to_ship_promised,1)

where (((out_order_tracking.date_ready_to_ship_promised)>='2013-05-01') and ((out_order_tracking.date_procured_promised)<'2013-05-01'));

 

-- B124h U Extra

update production.out_order_tracking set out_order_tracking.date_ready_to_ship_promised = production.workday(out_order_tracking.date_ready_to_ship_promised,1)

where (((out_order_tracking.date_ready_to_ship_promised)>='2013-03-29') and ((out_order_tracking.date_procured_promised)<'2013-03-29'));

 

-- B125 U

update production.out_order_tracking inner join bob_live_mx.sales_order_item on out_order_tracking.sku_simple = sales_order_item.sku inner join bob_live_mx.catalog_shipment_type on sales_order_item.fk_catalog_shipment_type = catalog_shipment_type.id_catalog_shipment_type set out_order_tracking.fullfilment_type_bob = catalog_shipment_type.name;

 

-- B130 D

delete production.pro_1st_attempt.* from production.pro_1st_attempt;

 

-- B131 A

insert into production.pro_1st_attempt ( order_item_id, min_of_date )

select out_order_tracking.order_item_id, min(tms_status_delivery.date) as min_of_date from (production.out_order_tracking inner join wmsprod.

vw_itens_venda_entrega on out_order_tracking.order_item_id = vw_itens_venda_entrega.item_id) inner join wmsprod.

tms_status_delivery on vw_itens_venda_entrega.cod_rastreamento = tms_status_delivery.cod_rastreamento group by out_order_tracking.order_item_id, tms_status_delivery.id_status

having tms_status_delivery.id_status=5;

 

-- B132 U

update production.out_order_tracking inner join production.pro_1st_attempt on out_order_tracking.order_item_id = pro_1st_attempt.order_item_id set out_order_tracking.date_1st_attempt = min_of_date;

 

-- B133 U

update production.out_order_tracking inner join wmsprod.vw_itens_venda_entrega vw on out_order_tracking.order_item_id = vw.item_id inner join wmsprod.tms_status_delivery del on vw.cod_rastreamento = del.cod_rastreamento set out_order_tracking.date_1st_attempt = (select max(tms.date) from wmsprod.tms_status_delivery tms, wmsprod.vw_itens_venda_entrega itens where del.cod_rastreamento = tms.cod_rastreamento and vw.item_id = itens.item_id and tms.id_status=4) where out_order_tracking.date_1st_attempt is null or out_order_tracking.date_1st_attempt < '2011-05-01';

 

-- B140 U

update production.out_order_tracking set out_order_tracking.week_exported = production.week_iso(date_exported), out_order_tracking.week_procured_promised = production.week_iso(date_procured_promised), out_order_tracking.week_delivered_promised = production.week_iso(date_delivered_promised), out_order_tracking.month_num_delivered_promised = date_format(date_delivered_promised, "%x-%m"), out_order_tracking.month_num_procured_promised = date_format(date_procured_promised, "%x-%m"), out_order_tracking.week_ready_to_ship_promised = production.week_iso(date_ready_to_ship_promised), out_order_tracking.week_ready_to_pick = production.week_iso(date_ready_to_pick), production.out_order_tracking.week_ordered = production.week_iso(date_ordered),month_num_ready_to_pick = date_format(date_ready_to_pick, "%x-%m"), out_order_tracking.week_shipped = production.week_iso(date_shipped);

 

-- B141a U

update production.out_order_tracking set out_order_tracking.month_procured_promised = monthname(date_procured_promised);

 

-- B141a1 U

update production.out_order_tracking set out_order_tracking.month_num_procured_promised = date_format(date_procured_promised, "%x-%m");

 

-- B141b U

update production.out_order_tracking set out_order_tracking.month_delivered_promised = monthname(date_delivered_promised);

 

-- B141b1 U

update production.out_order_tracking set out_order_tracking.month_num_delivered_promised = date_format(date_delivered_promised, "%x-%m");

 

-- B141c U

update production.out_order_tracking set month_num_procured_promised = '2012-12' where date_procured_promised = '2012-12-31';


-- B141c2 U

update production.out_order_tracking set month_num_delivered_promised = '2012-12' where date_delivered_promised = '2012-12-31';

 

-- B150 U

update production.out_order_tracking inner join wmsprod.itens_venda on out_order_tracking.order_item_id = itens_venda.item_id inner join wmsprod.romaneio on itens_venda.romaneio_id = romaneio.romaneio_id inner join wmsprod.transportadoras on romaneio.transportadora_id = transportadoras.transportadoras_id set out_order_tracking.shipping_carrier = nome_transportadora;

 

-- B150a U

update production.out_order_tracking ot inner join bob_live_mx.sales_order_item soi on ot.order_item_id = soi.id_sales_order_item inner join bob_live_mx.shipment_carrier sc on soi.fk_shipment_carrier = sc.id_shipment_carrier set ot.shipping_carrier_srt = sc.name;

 

-- B151 U

update production.out_order_tracking set out_order_tracking.year_procured_promised = date_format(date_procured_promised, "%x");

 

-- B152 U

update production.out_order_tracking set out_order_tracking.year_delivered_promised = date_format(date_delivered_promised,"%x");

 

-- B153 U

update production.out_order_tracking set out_order_tracking.sl0 = 1

where (((out_order_tracking.supplier_leadtime)=0));

 

-- B190 U

update wmsprod.itens_venda inner join production.out_order_tracking on itens_venda.item_id = out_order_tracking.order_item_id set out_order_tracking.status_wms = status;

 

-- B191 U

update production.out_order_tracking set out_order_tracking.stockout = "1"

where out_order_tracking.status_wms="quebra tratada" or out_order_tracking.status_wms="quebrado";

 

-- B201 U

update production.out_order_tracking set out_order_tracking.workdays_to_export = case when date_ordered is null then null else production.calcworkdays(date_ordered,case when date_exported is null then curdate() else date_exported end)end, out_order_tracking.workdays_to_procure = case when stockout = 1 then null else (case when date_exported is null then null else production.calcworkdays(date_exported,case when date_procured is null then curdate() else date_procured end)end)end, out_order_tracking.workdays_to_ready = case when date_ready_to_pick is null then null else production.calcworkdays(date_ready_to_pick, case when date_ready_to_ship is null then curdate() else date_ready_to_ship end)end, out_order_tracking.workdays_to_ship = case when date_ready_to_ship is null then null else production.calcworkdays(date_ready_to_ship, case when date_shipped is null then curdate() else date_shipped end)end, out_order_tracking.workdays_to_1st_attempt = case when date_shipped is null then null else production.calcworkdays(date_shipped, case when date_1st_attempt is null then curdate() else date_1st_attempt end)end, out_order_tracking.workdays_to_deliver = case when date_shipped is null then null else production.calcworkdays(date_shipped,case when date_delivered is null then curdate() else date_delivered end)end, out_order_tracking.workdays_total_1st_attempt = case when date_shipped is null then null else production.calcworkdays(date_exported, case when date_1st_attempt is null then curdate() else date_1st_attempt end)end, out_order_tracking.workdays_total_delivery = case when date_shipped is null then null else production.calcworkdays(date_exported, case when date_delivered is null then curdate() else date_delivered end)end;

 

-- B201a U

update production.out_order_tracking set days_to_export = case when date_ordered is null then null else (case when date_exported is null then datediff(curdate(),date_ordered) else datediff(date_exported, date_ordered) end)end, days_to_procure = case when stockout = 1 then null else (case when date_procured < date_exported then 0 else (case when date_procured is null then datediff(curdate(),date_exported) else datediff(date_procured, date_exported) end)end) end, days_to_ready = case when date_ready_to_pick is null then null else (case when date_ready_to_ship is null then datediff(curdate(),date_ready_to_pick) else datediff(date_ready_to_ship, date_ready_to_pick) end)end, days_to_ship  = case when date_ready_to_ship is null then null else (case when date_shipped is null then datediff(curdate(),date_ready_to_ship) else datediff(date_shipped, date_ready_to_ship) end)end, days_to_1st_attempt = case when date_shipped is null then null else (case when date_1st_attempt is null then datediff(curdate(),date_shipped) else datediff(date_1st_attempt,date_shipped) end)end, out_order_tracking.days_total_delivery = case when date_shipped is null then null else (case when date_delivered is null then datediff(curdate(),date_ordered) else datediff(date_delivered,date_ordered) end)end;

 

-- B201b U

update production.out_order_tracking set date_delivered_errors = case when days_total_delivery < 0 then 1 else 0 end;

 

-- B202 U

update production.out_order_tracking set out_order_tracking.delay_to_procure = case when date_procured is null then (case when curdate()>date_procured_promised then 1 else 0 end) else (case when date_procured>date_procured_promised then 1 else 0 end)end, out_order_tracking.delay_to_ready = case when workdays_to_ready>1 then 1 else 0 end, out_order_tracking.delay_to_ship = case when workdays_to_ship>1 then 1 else 0 end, out_order_tracking.delay_to_1st_attempt = case when workdays_to_1st_attempt>2 then 1 else 0 end, out_order_tracking.delay_to_deliver = case when workdays_to_deliver> 2 then 1 else 0 end, out_order_tracking.delay_total_1st_attempt = case when date_1st_attempt is null then (case when curdate()>date_delivered_promised then 1 else 0 end) else (case when date_1st_attempt>date_delivered_promised then 1 else 0 end)end, out_order_tracking.delay_total_delivery = case when date_delivered is null then (case when curdate()>date_delivered_promised then 1 else 0 end) else (case when date_delivered>date_delivered_promised then 1 else 0 end)end, out_order_tracking.workdays_delay_to_procure = case when workdays_to_procure-supplier_leadtime<0 then 0 else workdays_to_procure-supplier_leadtime end, out_order_tracking.workdays_delay_to_ready = case when workdays_to_ready-1<0 then 0 else workdays_to_ready-1 end, out_order_tracking.workdays_delay_to_ship = case when workdays_to_ship-1<0 then 0 else workdays_to_ship-1 end, out_order_tracking.workdays_delay_to_1st_attempt = case when workdays_to_1st_attempt-3<0 then 0 else workdays_to_1st_attempt-3 end, out_order_tracking.workdays_delay_to_deliver = case when workdays_to_deliver-3<0 then 0 else workdays_to_deliver-3 end, out_order_tracking.on_time_to_procure = case when date_procured<=date_procured_promised then 1 else 0 end, out_order_tracking.on_time_total_1st_attempt = case when date_1st_attempt<=date_delivered_promised then 1 else 0 end, out_order_tracking.on_time_total_delivery = case when date_delivered<=date_delivered_promised then 1 when date_delivered is null then 0 else 0 end;

 

-- B203 U

update production.out_order_tracking inner join bob_live_mx.catalog_config on out_order_tracking.sku_config = catalog_config.sku set out_order_tracking.presale = "1" where catalog_config.name like "preventa%";

 

-- B204 U

update production.out_order_tracking inner join bob_live_mx.catalog_config on out_order_tracking.sku_config = catalog_config.sku inner join bob_live_mx.catalog_attribute_option_global_buyer_name on catalog_attribute_option_global_buyer_name.id_catalog_attribute_option_global_buyer_name = catalog_config.fk_catalog_attribute_option_global_buyer_name inner join bob_live_mx.catalog_attribute_option_global_head_buyer_name on catalog_attribute_option_global_head_buyer_name.id_catalog_attribute_option_global_head_buyer_name = catalog_config.fk_catalog_attribute_option_global_head_buyer_name set out_order_tracking.buyer = catalog_attribute_option_global_buyer_name.name, out_order_tracking.head_buyer = catalog_attribute_option_global_head_buyer_name.name;

 

-- B206 U

update production.out_order_tracking inner join bob_live_mx.sales_order_item on out_order_tracking.order_item_id = sales_order_item.id_sales_order_item inner join bob_live_mx.sales_order_item_status on sales_order_item.fk_sales_order_item_status = sales_order_item_status.id_sales_order_item_status set out_order_tracking.status_bob = sales_order_item_status.name;

 

-- B207 U

update production.out_order_tracking inner join wmsprod.vw_itens_venda_entrega on out_order_tracking.order_item_id = vw_itens_venda_entrega.item_id set out_order_tracking.wms_tracking_code = cod_rastreamento;

 

-- B208 U

update production.out_order_tracking inner join wmsprod.tms_tracks on out_order_tracking.wms_tracking_code = tms_tracks.cod_rastreamento set out_order_tracking.shipping_carrier_tracking_code = track;

 

-- B210 U

update production.out_order_tracking inner join production.status_match_bob_wms on out_order_tracking.status_wms = status_match_bob_wms.status_wms and out_order_tracking.status_bob = status_match_bob_wms.status_bob set out_order_tracking.status_match_bob_wms = status_match_bob_wms.correct;

 

-- B211 U

update production.out_order_tracking set out_order_tracking.check_dates = case when date_ordered>date_exported then "date ordered > date exported" else (case when date_exported>date_procured then "date exported > date procured" else (case when date_procured>date_ready_to_ship then "date procured > date ready to ship" else (case when date_ready_to_ship>date_shipped then "date ready to ship > date shipped" else (case when date_shipped>date_1st_attempt then "date shipped > date 1st attempt" else (case when date_1st_attempt>date_delivered then "date shipped > date 1st attempt" else "correct" end)end)end)end)end)end;

 

-- B211a U

update production.out_order_tracking set out_order_tracking.check_date_1st_attempt = case when date_1st_attempt is null then (case when date_delivered is null then 0 else 1 end) else (case when date_1st_attempt>date_delivered then 1 else 0 end)end;

 

-- B211b U

update production.out_order_tracking set out_order_tracking.check_date_shipped = case when date_shipped is null then (case when date_1st_attempt is null and date_delivered is null then 0 else 1 end) else (case when date_shipped>date_1st_attempt then 1 else 0 end)end;

 

-- B211c U

update production.out_order_tracking set out_order_tracking.check_date_ready_to_ship = case when date_ready_to_ship is null then (case when date_shipped is null and date_1st_attempt is null and date_delivered is null then 0 else 1 end) else (case when date_ready_to_ship>date_shipped then 1 else 0 end)end;

 

-- B211d U

update production.out_order_tracking set out_order_tracking.check_date_procured = case when date_procured is null then (case when date_ready_to_ship is null and date_shipped is null and date_1st_attempt is null and date_delivered is null then 0 else 1 end) else (case when date_procured>date_ready_to_ship then 1 else 0 end)end;

 

-- B211e U

update production.out_order_tracking set out_order_tracking.check_date_exported = case when date_exported is null then (case when date_procured is null and date_ready_to_ship is null and date_shipped is null and date_1st_attempt is null and date_delivered is null then 0 else 1 end) else (case when date_exported>date_procured then 1 else 0 end)end;

 

-- B211f U

update production.out_order_tracking set out_order_tracking.check_date_ordered = case when date_ordered is null then (case when date_exported is null and date_procured is null and date_ready_to_ship is null and date_shipped is null and date_1st_attempt is null and date_delivered is null then 0 else 1 end) else (case when date_ordered>date_exported then 1 else 0 end)end;

 

-- B214 U

update production.out_order_tracking set out_order_tracking.date_procured = null

where out_order_tracking.status_wms="aguardando estoque" or out_order_tracking.status_wms="analisando quebra";

 

-- B216 U

update production.out_order_tracking set out_order_tracking.still_to_procure = 1

where out_order_tracking.date_procured is null;

 

-- B216b U

update production.out_order_tracking set out_order_tracking.still_to_procure = 0

where out_order_tracking.status_wms="quebrado" or out_order_tracking.status_wms="quebra tratada";

 

-- B218 U

update production.out_order_tracking set out_order_tracking.vol_weight = package_height*package_length*package_width/5000;

 

-- B219 U

update production.out_order_tracking set out_order_tracking.max_vol_w_vs_w= case when vol_weight>package_weight then vol_weight else package_weight end;

 

-- B220 U

update production.out_order_tracking set out_order_tracking.package_measure = case when max_vol_w_vs_w>45.486 then "large" else (case when max_vol_w_vs_w>2.277 then "medium" else "small" end)end;

 

-- B221 U

update production.out_order_tracking inner join bob_live_mx.sales_order on out_order_tracking.order_number = sales_order.order_nr inner join bob_live_mx.sales_order_address on sales_order.fk_sales_order_address_billing = sales_order_address.id_sales_order_address set out_order_tracking.ship_to_zip = postcode;

 

-- B222 U

update production.out_order_tracking set out_order_tracking.delay_carrier_shipment = case when package_measure="large" then (case when workdays_to_1st_attempt-5>0 then 1 else 0 end) else (case when workdays_to_1st_attempt-3>0 then 1 else 0 end)end;

 

-- B223 U

update production.out_order_tracking set out_order_tracking.days_left_to_procure = case when date_procured is null then (case when datediff(date_procured_promised,curdate())<0 then 0 else datediff(date_procured_promised,curdate()) end) else 555 end;

 

-- B224 U

update production.out_order_tracking set out_order_tracking.deliv_within_3_days_of_order = case when datediff(date_delivered,date_ordered)<=5 then (case when production.calcworkdays(date_ordered,date_delivered)<=3 then 1 else 0 end) else 0 end, out_order_tracking.first_att_within_3_days_of_order = case when datediff(date_1st_attempt,date_ordered)<=5 then (case when production.calcworkdays(date_ordered,date_1st_attempt)<=3 then 1 else 0 end) else 0 end;

 

-- B225 U

update production.out_order_tracking set out_order_tracking.shipped_same_day_as_order = case when date_ordered=date_shipped then 1 else 0 end;

 

-- B226 U

update production.out_order_tracking set out_order_tracking.shipped_same_day_as_procured = case when date_procured=date_shipped then 1 else 0 end;

 

-- B227 U extra

update production.out_order_tracking set out_order_tracking.reason_for_delay = "on time 1st attempt";

 

-- B227 U

update production.out_order_tracking set out_order_tracking.reason_for_delay = "procurement" where out_order_tracking.delay_to_procure=1 and out_order_tracking.delay_total_1st_attempt=1;

 

-- B228 U

update production.out_order_tracking set out_order_tracking.reason_for_delay = "preparing item for shipping" where out_order_tracking.reason_for_delay="on time 1st attempt" and out_order_tracking.delay_to_ready=1 and out_order_tracking.delay_total_1st_attempt=1;

 

-- B229 U

update production.out_order_tracking set out_order_tracking.reason_for_delay = "preparing item for shipping"

where out_order_tracking.reason_for_delay="on time 1st attempt" and out_order_tracking.delay_to_ready=1 and out_order_tracking.delay_total_1st_attempt=1;

 

-- B230 U

update production.out_order_tracking set out_order_tracking.reason_for_delay = "carrier late delivering" where out_order_tracking.reason_for_delay="on time 1st attempt" and out_order_tracking.delay_total_1st_attempt=1;

 

-- B231 U

update production.out_order_tracking set out_order_tracking.early_to_procure = case when (case when date_procured is null then datediff(date_procured_promised,curdate()) else datediff(date_procured_promised,date_procured) end)>0 then 1 else 0 end, out_order_tracking.early_to_1st_attempt = case when (case when date_1st_attempt is null then datediff(date_delivered_promised,curdate()) else datediff(date_delivered_promised,date_1st_attempt) end)>0 then 1 else 0 end;

 

-- B232 U

update production.out_order_tracking set out_order_tracking.delay_reason_maximum_value = production.maximum(workdays_delay_to_procure,workdays_delay_to_ready,workdays_delay_to_ship,workdays_delay_to_1st_attempt);

 

-- B233 U

update production.out_order_tracking set out_order_tracking.delay_reason = case when delay_total_1st_attempt=1 then (case when delay_reason_maximum_value=workdays_delay_to_procure then "procurement" else (case when delay_reason_maximum_value=workdays_delay_to_ready then "preparing item for shipping" else (case when delay_reason_maximum_value=workdays_delay_to_ship then "carrier late to pick up item" else (case when delay_reason_maximum_value=workdays_delay_to_1st_attempt then "carrier late delivering" else "on time 1st attempt" end)end)end)end) else "on time 1st attempt" end;

 

-- B234 U

update production.out_order_tracking set out_order_tracking.on_time_to_ship = case when date_shipped<= date_ready_to_ship_promised then 1 else 0 end, out_order_tracking.on_time_r2s = case when date_ready_to_ship<=date_ready_to_ship_promised then 1 else 0 end;

 

-- B235 D

delete production.pro_order_tracking_num_items_per_order.* from production.pro_order_tracking_num_items_per_order;

 

-- B236 A

insert into production.pro_order_tracking_num_items_per_order ( order_number, count_of_order_item_id ) select out_order_tracking.order_number, count(out_order_tracking.order_item_id) as countoforder_item_id from production.out_order_tracking group by out_order_tracking.order_number;

 

-- B237 U

update production.out_order_tracking inner join production.pro_order_tracking_num_items_per_order on out_order_tracking.order_number = pro_order_tracking_num_items_per_order.order_number set out_order_tracking.num_items_per_order = 1/pro_order_tracking_num_items_per_order.count_of_order_item_id;

 

-- B238 U

update production.out_order_tracking set out_order_tracking.procurement_actual_time = case when date_exported is null then null else (case when date_procured is null then production.calcworkdays(date_exported,curdate()) else production.calcworkdays(date_exported,date_procured)end)end;

 

-- B239 U

update production.out_order_tracking set out_order_tracking.procurement_delay= case when date_exported>=curdate() then 0 else (case when date_sub(procurement_actual_time, interval supplier_leadtime day)>0 then date_sub(procurement_actual_time, interval supplier_leadtime day) else 0 end)end;

 

-- B240 U

update production.out_order_tracking set out_order_tracking.procurement_delay_counter = 1

where out_order_tracking.procurement_delay>0;

 

-- B241 U

update production.out_order_tracking set out_order_tracking.warehouse_actual_time = case when date_ready_to_pick is null then null else (case when date_ready_to_ship is null then production.calcworkdays(date_ready_to_pick,curdate()) else production.calcworkdays(date_ready_to_pick,date_ready_to_ship)end)end;

 

-- B242 U

update production.out_order_tracking set out_order_tracking.warehouse_delay = case when date_ready_to_ship_promised>curdate() then 0 else (case when date_ready_to_ship is null then production.calcworkdays(date_ready_to_ship_promised,curdate()) else production.calcworkdays(date_ready_to_ship_promised,date_ready_to_ship)end)end;

 

-- B243 U

update production.out_order_tracking set out_order_tracking.warehouse_delay_counter = 1

where out_order_tracking.warehouse_delay>0;

 

-- B244 U

update production.out_order_tracking set out_order_tracking.carrier_time = max_delivery_time-wh_time-supplier_leadtime;

 

-- B245 U

update production.out_order_tracking set out_order_tracking.actual_carrier_time = case when date_ready_to_ship is null then null else (case when date_delivered is null then production.calcworkdays(date_ready_to_ship,curdate()) else production.calcworkdays(date_ready_to_ship,date_delivered)end)end;

 

-- B246 U

update production.out_order_tracking set out_order_tracking.delivery_delay = case when date_ready_to_ship>=curdate() then 0 else (case when actual_carrier_time>carrier_time then actual_carrier_time-carrier_time else 0 end)end;

 

-- B247 U

update production.out_order_tracking set out_order_tracking.delivery_delay_counter = 1

where out_order_tracking.delivery_delay>0 and out_order_tracking.date_delivered_promised<=curdate();

 

-- B248 U

update production.out_order_tracking set out_order_tracking.delay_exceptions = case when out_order_tracking.supplier_leadtime>=out_order_tracking.max_delivery_time then 1 else 0 end;

 

-- B250 U

update production.out_order_tracking set out_order_tracking.ready_to_ship = 1 where out_order_tracking.date_ready_to_ship is not null;

 

-- B251 U

update production.out_order_tracking set out_order_tracking.wh_workdays_to_r2s = case when date_ready_to_ship is null then null else (case when date_ready_to_pick is null then null else production.calcworkdays(date_ready_to_pick, date_ready_to_ship)end)end;

 

-- B251b U

update production.out_order_tracking set out_order_tracking.week_r2s_promised = production.week_iso(date_ready_to_ship_promised), out_order_tracking.month_num_r2s_promised = date_format(date_ready_to_ship_promised, "%x-%m");

 

-- B252 U

update production.out_order_tracking set out_order_tracking.r2s_workday_0 = case when production.calcworkdays(date_ready_to_pick,curdate())<0 then 0 else (case when wh_workdays_to_r2s=0 then 1 else 0 end)end, out_order_tracking.r2s_workday_1 = case when production.calcworkdays(date_ready_to_pick,curdate())<1 then 0 else (case when wh_workdays_to_r2s<=1 then 1 else 0 end)end, out_order_tracking.r2s_workday_2 = case when production.calcworkdays(date_ready_to_pick,curdate())<2 then 0 else (case when wh_workdays_to_r2s<=2 then 1 else 0 end)end, out_order_tracking.r2s_workday_3 = case when production.calcworkdays(date_ready_to_pick,curdate())<3 then 0 else (case when wh_workdays_to_r2s<=3 then 1 else 0 end)end, out_order_tracking.r2s_workday_4 = case when production.calcworkdays(date_ready_to_pick,curdate())<4 then 0 else (case when wh_workdays_to_r2s<=4 then 1 else 0 end)end, out_order_tracking.r2s_workday_5 = case when production.calcworkdays(date_ready_to_pick,curdate())<5 then 0 else (case when wh_workdays_to_r2s<=5 then 1 else 0 end)end, out_order_tracking.r2s_workday_6 = case when production.calcworkdays(date_ready_to_pick,curdate())<6 then 0 else (case when wh_workdays_to_r2s<=6 then 1 else 0 end)end, out_order_tracking.r2s_workday_7 = case when production.calcworkdays(date_ready_to_pick,curdate())<7 then 0 else (case when wh_workdays_to_r2s<=7 then 1 else 0 end)end, out_order_tracking.r2s_workday_8 = case when production.calcworkdays(date_ready_to_pick,curdate())<8 then 0 else (case when wh_workdays_to_r2s<=8 then 1 else 0 end)end, out_order_tracking.r2s_workday_9 = case when production.calcworkdays(date_ready_to_pick,curdate())<9 then 0 else (case when wh_workdays_to_r2s<=9 then 1 else 0 end)end, out_order_tracking.r2s_workday_10 = case when production.calcworkdays(date_ready_to_pick,curdate())<10 then 0 else (case when wh_workdays_to_r2s<=10 then 1 else 0 end)end, out_order_tracking.r2s_workday_superior_10 = case when production.calcworkdays(date_ready_to_pick,curdate())<10 then 0 else (case when wh_workdays_to_r2s>10 then 1 else 0 end)end;

 

-- B253 U

update production.out_order_tracking set out_order_tracking.ready_to_pick = 1

where out_order_tracking.date_ready_to_pick is not null;

 

-- B255 U

update ((procurement_live_mx.procurement_order inner join procurement_live_mx.catalog_supplier_attributes on procurement_order.fk_catalog_supplier = catalog_supplier_attributes.fk_catalog_supplier) inner join (bob_live_mx.catalog_simple inner join procurement_live_mx.procurement_order_item on catalog_simple.id_catalog_simple = procurement_order_item.fk_catalog_simple) on procurement_order.id_procurement_order = procurement_order_item.fk_procurement_order) inner join production.out_order_tracking on catalog_simple.sku = out_order_tracking.sku_simple set out_order_tracking.oms_payment_terms = catalog_supplier_attributes.payment_terms, out_order_tracking.oms_payment_event = catalog_supplier_attributes.payment_event, out_order_tracking.date_procurement_order = procurement_order.created_at;


-- B256 U

update production.out_order_tracking set out_order_tracking.date_payable_promised = date_procured

where out_order_tracking.oms_payment_event="entrega";


-- B257 U

update production.out_order_tracking set out_order_tracking.date_payable_promised = date_procured

where out_order_tracking.oms_payment_event="pedido";


-- B258 U

update production.out_order_tracking set out_order_tracking.date_payable_promised = date_procured-supplier_leadtime

where out_order_tracking.oms_payment_event="factura";

 

-- B259 U

update production.out_order_tracking set out_order_tracking.delay_linio_promise = case when is_linio_promise = 1 and workdays_to_1st_attempt > 2 then 1 else 0 end;

 

-- B260

update production.out_order_tracking inner join wmsprod.entrega on out_order_tracking.wms_tracking_code = entrega.cod_rastreamento set out_order_tracking.delivery_fullfilment = entrega.delivery_fulfillment;


-- B261

update production.out_order_tracking set out_order_tracking.split_order = case when delivery_fullfilment = 'partial' then 1 else 0 end;

 

-- B262

update production.out_order_tracking set out_order_tracking.effective_1st_attempt = case when date_delivered = date_1st_attempt then 1 else 0 end;

 

-- B263

delete from production.pro_value_from_order;

 

-- B264

insert into production.pro_value_from_order

select order_number, 1/count(order_item_id) as value_from_order from production.out_order_tracking t group by order_number order by date_ordered desc;

 

-- B265

update production.out_order_tracking inner join production.pro_value_from_order on out_order_tracking.order_number=pro_value_from_order.order_number set out_order_tracking.value_from_order=pro_value_from_order.value_from_order;

 

-- B267

update production.out_order_tracking set out_order_tracking.shipped = case when date_shipped is not null then 1 else 0 end;

 

-- B268

update production.out_order_tracking set out_order_tracking.delivered = case when date_delivered is not null then 1 else 0 end;


-- B269

update production.out_order_tracking set out_order_tracking.first_attempt = 1 where date_1st_attempt is not null;

 

-- B270 U

update production.out_stock_hist set out_stock_hist.entrance_last_30= case when datediff(curdate(),date_entrance)<30 then 1 else 0 end, out_stock_hist.oms_po_last_30= case when datediff(curdate(),date_procurement_order)<=30 and datediff(curdate(),date_procurement_order)>0 then 1 else 0 end;


-- B271 U

update production.out_order_tracking set out_order_tracking.procured_promised_last_30 = case when datediff(curdate(),date_procured_promised)<=30 and datediff(curdate(),date_procured_promised)>0 then 1 else 0 end;


-- B272 U

update production.out_order_tracking set out_order_tracking.shipped_last_30 = case when datediff(curdate(),date_shipped)<=30 and datediff(curdate(),date_shipped)>0 then 1 else 0 end;


-- B273 U

update production.out_order_tracking set out_order_tracking.delivered_promised_last_30 = case when datediff(curdate(),date_delivered_promised)<=30 and datediff(curdate(),date_delivered_promised)>0 then 1 else 0 end;

 

-- B274 U

update production.out_order_tracking set out_order_tracking.pending_first_attempt = case when date_shipped is not null and date_1st_attempt is null then 1 else 0 end;

 

-- B275 U

update production.out_order_tracking set ship_to_met_area  = case when ship_to_zip between  01000 and 16999 or ship_to_zip between 53000 and 53970 or ship_to_zip between 54000 and 54198 or ship_to_zip between 54600 and 54658 or ship_to_zip between 54700 and 54769 or ship_to_zip between 54900 and 54959 or ship_to_zip between 54960 and 54990 or ship_to_zip between 52760 and 52799 or ship_to_zip between 52900 and 52998 or ship_to_zip between 55000 and 55549 or ship_to_zip between 55700 and 55739 or ship_to_zip between 57000 and 57950 then 1 else 0 end;

 

-- B276 U

update production.out_order_tracking o inner join production.tbl_order_detail t on o.order_item_id=t.item set o.actual_paid_price_after_tax=t.actual_paid_price_after_tax, o.costo_after_tax=t.costo_after_vat;

 

-- B277 U

update production.out_order_tracking a inner join bob_live_mx.catalog_config b on a.sku_config = b.sku inner join bob_live_mx.catalog_attribute_option_global_category c on b.fk_catalog_attribute_option_global_category = c.id_catalog_attribute_option_global_category inner join bob_live_mx.catalog_attribute_option_global_sub_category d on b.fk_catalog_attribute_option_global_sub_category = d.id_catalog_attribute_option_global_sub_category inner join bob_live_mx.catalog_attribute_option_global_sub_sub_category e on b.fk_catalog_attribute_option_global_sub_sub_category = e.id_catalog_attribute_option_global_sub_sub_category set a.category_com_main = c.name, a.category_com_sub = d.name, a.category_com_sub_sub = e.name;

 

-- B278 U

update production.out_order_tracking set ship_to_zip2 = left(ship_to_zip,2), ship_to_zip3 = left(ship_to_zip,3);

 

-- B279 U

update production.out_order_tracking a inner join production.tbl_order_detail b on a.order_number = b.order_nr set a.coupon_code = b.coupon_code;


-- B280 U

update production.out_order_tracking set is_corporate_sale = 1 where coupon_code like'CDEAL%';

 

-- B281

update production.out_order_tracking a inner join development_mx.A_E_M1_New_CategoryBP b on a.category_com_sub = b.cat2 set a.category_bp = b.CatBP;

 

-- B282

update production.out_order_tracking a inner join development_mx.A_E_M1_New_CategoryBP b on a.category_com_main = b.cat1 set a.category_bp = b.CatBP where a.category_bp is null;

 

-- B283

update production.out_order_tracking set fullfilment_type_bp =

case when fullfilment_type_real = 'crossdock' then 'crossdocking'

when fullfilment_type_real = 'inventory' and fullfilment_type_bob = 'consignment' then 'consignment'

when fullfilment_type_real = 'inventory' and fullfilment_type_bob <> 'consignment' then 'outright buying'

when fullfilment_type_real = 'dropshipping' then 'other' end;

 

-- Operaciones

-- Macro C

 

-- C001 D

delete production.out_stock_hist.* from production.out_stock_hist;

 

-- C002 A

insert into production.out_stock_hist ( stock_item_id, barcode_wms, date_entrance, barcode_bob_duplicated, in_stock, wh_location )

select estoque.estoque_id, estoque.cod_barras, case when data_criacao is null then null else date(data_criacao) end as expr1, estoque.minucioso, posicoes.participa_estoque, estoque.endereco from (wmsprod.estoque left join wmsprod.itens_recebimento on estoque.itens_recebimento_id = itens_recebimento.itens_recebimento_id) left join wmsprod.posicoes on estoque.endereco = posicoes.posicao;

 

-- C002 a

update production.out_stock_hist inner join wmsprod.movimentacoes on out_stock_hist.stock_item_id = movimentacoes.estoque_id set out_stock_hist.date_entrance = date(movimentacoes.data_criacao)

where movimentacoes.de_endereco='retorno de cancelamento' or movimentacoes.de_endereco='vendidos';

 

-- C003 D

delete production.pro_unique_ean_bob.* from production.pro_unique_ean_bob;

 

-- C004 A

insert into production.pro_unique_ean_bob ( ean ) select produtos.ean as ean from bob_live_mx.catalog_simple inner join wmsprod.produtos on catalog_simple.sku = produtos.sku where catalog_simple.status not like "deleted"

group by produtos.ean

having count(produtos.ean=1);

 

-- C102 U

update production.out_stock_hist inner join wmsprod.traducciones_producto on out_stock_hist.barcode_wms = traducciones_producto.identificador set out_stock_hist.sku_simple = sku;

 

-- C104 U

update production.out_stock_hist inner join wmsprod.estoque on out_stock_hist.stock_item_id = estoque.estoque_id set out_stock_hist.date_exit = date(data_ultima_movimentacao), out_stock_hist.exit_type = "sold" where out_stock_hist.wh_location="vendidos";

 

-- C105 U

update production.out_stock_hist inner join wmsprod.estoque on out_stock_hist.stock_item_id = estoque.estoque_id set out_stock_hist.date_exit = date(data_ultima_movimentacao), out_stock_hist.exit_type = "error" where out_stock_hist.wh_location="error";

 

-- C106 U

update (((production.out_stock_hist inner join bob_live_mx.

catalog_simple on out_stock_hist.sku_simple = catalog_simple.sku) inner join bob_live_mx.

catalog_config on catalog_simple.fk_catalog_config = catalog_config.id_catalog_config) inner join bob_live_mx.

catalog_shipment_type on catalog_simple.fk_catalog_shipment_type = catalog_shipment_type.id_catalog_shipment_type) inner join bob_live_mx.supplier on catalog_config.fk_supplier = supplier.id_supplier set out_stock_hist.barcode_bob = barcode_ean, out_stock_hist.sku_config = catalog_config.sku, out_stock_hist.sku_name = catalog_config.name, out_stock_hist.sku_supplier_config = catalog_config.sku_supplier_config, out_stock_hist.model = catalog_config.model, out_stock_hist.cost = catalog_simple.cost, out_stock_hist.supplier_name = supplier.name, out_stock_hist.supplier_id = supplier.id_supplier, out_stock_hist.fullfilment_type_bob = catalog_shipment_type.name, out_stock_hist.price = catalog_simple.price;

 

-- C106a

update production.out_stock_hist a inner join wmsprod.estoque b on a.stock_item_id= b.estoque_id inner join procurement_live_mx.wms_received_item c on b.itens_recebimento_id = c.id_wms inner join procurement_live_mx.procurement_order_item d on c.fk_procurement_order_item = d.id_procurement_order_item set a.cost = d.unit_price;

 

-- C107 U

update production.out_stock_hist inner join production.pro_category_tree on out_stock_hist.sku_config = pro_category_tree.sku set out_stock_hist.category = cat1, out_stock_hist.cat2 = pro_category_tree.cat2;

 

-- C107a U

update production.out_stock_hist inner join production.exceptions_supplier_categories on out_stock_hist.supplier_name = exceptions_supplier_categories.supplier set out_stock_hist.category = "ropa, calzado y accesorios"

where out_stock_hist.category="deportes";

 

-- C107b U

update production.out_stock_hist set out_stock_hist.category = "salud y cuidado personal"

where out_stock_hist.category="ropa, calzado y accesorios" and out_stock_hist.cat2="salud y cuidado personal";

 

-- C107c U

update production.out_stock_hist inner join bob_live_mx.catalog_config on out_stock_hist.sku_config = catalog_config.sku inner join bob_live_mx.catalog_attribute_option_global_buyer_name on catalog_attribute_option_global_buyer_name.id_catalog_attribute_option_global_buyer_name = catalog_config.fk_catalog_attribute_option_global_buyer_name inner join bob_live_mx.catalog_attribute_option_global_head_buyer_name on catalog_attribute_option_global_head_buyer_name.id_catalog_attribute_option_global_head_buyer_name = catalog_config.fk_catalog_attribute_option_global_head_buyer_name set out_stock_hist.buyer = catalog_attribute_option_global_buyer_name.name, out_stock_hist.head_buyer = catalog_attribute_option_global_head_buyer_name.name;

 

-- C107d U

update production.out_stock_hist inner join production.buyer_category on out_stock_hist.category = buyer_category.category set out_stock_hist.category_english = buyer_category.category_english;

 

-- C108 U

update production.out_stock_hist set out_stock_hist.sold_last_30= case when datediff(curdate(),date_exit)<30 then 1 else 0 end, out_stock_hist.sold_yesterday = case when datediff(curdate(),(date_sub(date_exit, interval 1 day)))=0 then 1 else 0 end, out_stock_hist.sold_last_10 = case when datediff(curdate(),date_exit)<10 then 1 else 0 end, out_stock_hist.sold_last_7 = case when datediff(curdate(),date_exit)<7 then 1 else 0 end where out_stock_hist.exit_type="sold";

 

-- C108a U

update production.out_stock_hist set out_stock_hist.entrance_last_30= case when datediff(curdate(),date_entrance)<30 then 1 else 0 end, out_stock_hist.oms_po_last_30= case when datediff(curdate(),date_procurement_order)<30 then 1 else 0 end;

 

-- C109 U

update production.out_stock_hist set out_stock_hist.days_in_stock = case when date_exit is null then datediff(curdate(),date_entrance) else datediff(date_exit,date_entrance) end;

 

-- Extra

update production.out_stock_hist set out_stock_hist.fullfilment_type_real = "inventory";

 

-- C110 U

update production.out_stock_hist inner join (wmsprod.itens_venda inner join wmsprod.status_itens_venda on itens_venda.itens_venda_id = status_itens_venda.itens_venda_id) on out_stock_hist.stock_item_id = itens_venda.estoque_id set out_stock_hist.fullfilment_type_real = "crossdock"

where status_itens_venda.status="analisando quebra" or status_itens_venda.status="aguardando estoque";

 

-- C110 U Extra

update (production.out_stock_hist inner join wmsprod.itens_venda on out_stock_hist.stock_item_id = itens_venda.estoque_id) inner join wmsprod.status_itens_venda on itens_venda.itens_venda_id = status_itens_venda.itens_venda_id set out_stock_hist.fullfilment_type_real = "dropshipping"

where (((status_itens_venda.status)="ds estoque reservado"));

 

-- C110 c

update production.out_stock_hist inner join wmsprod.movimentacoes on out_stock_hist.stock_item_id = movimentacoes.estoque_id set out_stock_hist.fullfilment_type_real = 'inventory'

where movimentacoes.de_endereco='retorno de cancelamento' or movimentacoes.de_endereco='vendidos';

 

-- C111 U

update production.out_stock_hist inner join bob_live_mx.catalog_simple on out_stock_hist.sku_simple = catalog_simple.sku inner join bob_live_mx.catalog_tax_class on catalog_simple.fk_catalog_tax_class = catalog_tax_class.id_catalog_tax_class set out_stock_hist.tax_percentage = tax_percent, out_stock_hist.cost_w_o_vat = out_stock_hist.cost/(1+tax_percent/100);

 

-- C111b U

update production.out_stock_hist set out_stock_hist.in_stock_cost_w_o_vat = out_stock_hist.cost_w_o_vat where out_stock_hist.in_stock=1;

 

-- C111c U

update production.out_stock_hist set out_stock_hist.sold_last_30_cost_w_o_vat = out_stock_hist.cost_w_o_vat, out_stock_hist.sold_last_30_price = out_stock_hist.price

where out_stock_hist.sold_last_30=1;

 

-- C112 D

delete production.pro_stock_hist_sku_count.* from production.pro_stock_hist_sku_count;

 

-- C113 A

insert into production.pro_stock_hist_sku_count ( sku_simple, sku_count )

select out_stock_hist.sku_simple, sum(out_stock_hist.item_counter) as sumofitem_counter from production.out_stock_hist

group by out_stock_hist.sku_simple, out_stock_hist.in_stock having out_stock_hist.in_stock=1;

 

-- C114 U

update production.out_stock_hist inner join production.pro_stock_hist_sku_count on out_stock_hist.sku_simple = pro_stock_hist_sku_count.sku_simple set out_stock_hist.sku_counter= 1/pro_stock_hist_sku_count.sku_count;

 

-- C114b U

update production.out_stock_hist set out_stock_hist.reserved = '0';

 

-- C115 U

update production.out_stock_hist inner join wmsprod.estoque on out_stock_hist.stock_item_id = estoque.estoque_id set out_stock_hist.reserved = "1"

where (((estoque.almoxarifado)="separando" or (estoque.almoxarifado)="estoque reservado" or (estoque.almoxarifado)="aguardando separacao"));

 

-- C116 U

update production.out_stock_hist inner join bob_live_mx.catalog_simple on out_stock_hist.sku_simple = catalog_simple.sku set out_stock_hist.supplier_leadtime = catalog_simple.delivery_time_supplier;

 

-- C117 U

update production.out_stock_hist inner join ((bob_live_mx.catalog_config inner join bob_live_mx.catalog_simple on catalog_config.id_catalog_config = catalog_simple.fk_catalog_config) inner join bob_live_mx.catalog_stock on catalog_simple.id_catalog_simple = catalog_stock.fk_catalog_simple) on out_stock_hist.sku_config = catalog_config.sku set out_stock_hist.visible = 1

where (((catalog_config.pet_status)="creation,edited,images") and ((catalog_config.pet_approved)=1) and ((catalog_config.status)="active") and ((catalog_simple.status)="active") and ((catalog_simple.price)>0) and ((catalog_config.display_if_out_of_stock)=0) and ((catalog_stock.quantity)>0) and ((catalog_simple.cost)>0)) or (((catalog_config.pet_status)="creation,edited,images") and ((catalog_config.pet_approved)=1) and ((catalog_config.status)="active") and ((catalog_simple.status)="active") and ((catalog_simple.price)>0) and ((catalog_config.display_if_out_of_stock)=1) and ((catalog_stock.quantity)>0) and ((catalog_simple.cost)>0));

 

-- C118 U

update production.out_stock_hist set out_stock_hist.week_exit = production.week_iso(date_exit);

 

-- C119a D

delete production.pro_stock_hist_sold_last_30_count.* from production.pro_stock_hist_sold_last_30_count;

 

-- C119b A

insert into production.pro_stock_hist_sold_last_30_count ( sold_last_30, sku_simple, count_of_item_counter )

select out_stock_hist.sold_last_30, out_stock_hist.sku_simple, count(out_stock_hist.item_counter) as count_of_item_counter

from production.out_stock_hist group by out_stock_hist.sold_last_30, out_stock_hist.sku_simple having out_stock_hist.sold_last_30=1;

 

-- C119c U

update production.out_stock_hist inner join production.pro_stock_hist_sold_last_30_count on out_stock_hist.sku_simple = pro_stock_hist_sold_last_30_count.sku_simple set out_stock_hist.sold_last_30_counter = 1/pro_stock_hist_sold_last_30_count.count_of_item_counter;

 

-- C120 U

update production.out_stock_hist set out_stock_hist.sku_simple_blank = 1

where out_stock_hist.sku_simple is null;

 

-- C121 U

update (wmsprod.itens_recebimento inner join wmsprod.estoque on itens_recebimento.itens_recebimento_id = estoque.itens_recebimento_id) inner join production.out_stock_hist on estoque.estoque_id = out_stock_hist.stock_item_id set out_stock_hist.quarantine = 1 where itens_recebimento.endereco="cuarentena" or itens_recebimento.endereco="cuarenta2";

 

-- C124 D

delete production.pro_sum_last_5_out_of_6_wks.* from production.pro_sum_last_5_out_of_6_wks;

 

-- C125 A

insert into production.pro_sum_last_5_out_of_6_wks ( sku_simple, expr1 )

select max_last_6_wks.sku_simple, sumofcountofdate_exit-maxofcountofdate_exit as expr1 from production.max_last_6_wks

group by max_last_6_wks.sku_simple, sumofcountofdate_exit-maxofcountofdate_exit;

 

-- C126 U

update production.out_stock_hist inner join production.pro_sum_last_5_out_of_6_wks on out_stock_hist.sku_simple = pro_sum_last_5_out_of_6_wks.sku_simple set out_stock_hist.sum_sold_last_5_out_of_6_wks = pro_sum_last_5_out_of_6_wks.expr1;

 

-- C127 U

update production.out_stock_hist inner join bob_live_mx.catalog_config on out_stock_hist.sku_config = catalog_config.sku set out_stock_hist.package_height = catalog_config.package_height, out_stock_hist.package_length = catalog_config.package_length, out_stock_hist.package_width = catalog_config.package_width;

 

-- C128 U

update production.out_stock_hist set out_stock_hist.vol_weight = package_height*package_length*package_width/5000;

 

-- C129 U

update production.out_stock_hist inner join bob_live_mx.catalog_config on out_stock_hist.sku_config = catalog_config.sku set out_stock_hist.package_weight = catalog_config.package_weight;

 

-- C130 U

update production.out_stock_hist set out_stock_hist.max_vol_w_vs_w = case when vol_weight>package_weight then vol_weight else package_weight end;

 

-- C131 U

update production.out_stock_hist set out_stock_hist.package_measure = case when max_vol_w_vs_w>45.486 then "large" else (case when max_vol_w_vs_w>2.277 then "medium" else "small" end)end;

 

-- C132 U

update production.out_stock_hist inner join wmsprod.itens_inventario on out_stock_hist.barcode_wms = itens_inventario.cod_barras set out_stock_hist.sub_position = sub_endereco;

 

-- C133 U

update wmsprod.posicoes inner join production.out_stock_hist on posicoes.posicao = out_stock_hist.wh_location set out_stock_hist.position_type= posicoes.tipo_posicao;

 

-- C135 U

update production.out_stock_hist set out_stock_hist.position_item_size_type = case when position_type="safe" then "small" else (case when position_type="mezanine" then "small" else (case when position_type="muebles" then "large" else "tbd" end)end)end;

 

-- C136 D

delete from production.items_procured_in_transit;

 

-- C136 A

insert into production.items_procured_in_transit ( sku_simple, number_ordered, unit_price )

select catalog_simple.sku, count(procurement_order_item.id_procurement_order_item) as countofid_procurement_order_item, avg(procurement_order_item.unit_price) as avgofunit_price

from bob_live_mx.catalog_simple inner join (procurement_live_mx.procurement_order_item inner join procurement_live_mx.procurement_order on procurement_order_item.fk_procurement_order = procurement_order.id_procurement_order) on catalog_simple.id_catalog_simple = procurement_order_item.fk_catalog_simple

where (((procurement_order_item.is_deleted)=0) and ((procurement_order.is_cancelled)=0) and ((procurement_order_item.sku_received)=0))

group by catalog_simple.sku;

 

-- C137 U

update production.out_stock_hist inner join production.items_procured_in_transit on out_stock_hist.sku_simple = items_procured_in_transit.sku_simple set out_stock_hist.items_procured_in_transit = number_ordered, out_stock_hist.procurement_price = unit_price;

 

-- C138 U

update (procurement_live_mx.procurement_order inner join ((bob_live_mx.catalog_simple inner join production.out_stock_hist on catalog_simple.sku = out_stock_hist.sku_simple) inner join procurement_live_mx.procurement_order_item on catalog_simple.id_catalog_simple = procurement_order_item.fk_catalog_simple) on procurement_order.id_procurement_order = procurement_order_item.fk_procurement_order) set out_stock_hist.date_procurement_order = procurement_order.created_at;

 

-- C138a U

update production.out_stock_hist inner join procurement_live_mx.catalog_supplier_attributes on out_stock_hist.supplier_id = catalog_supplier_attributes.fk_catalog_supplier set out_stock_hist.oms_payment_terms = catalog_supplier_attributes.payment_terms, out_stock_hist.oms_payment_event = catalog_supplier_attributes.payment_event;

 

-- C139 U

update production.out_stock_hist set out_stock_hist.date_payable_promised = adddate(date_procurement_order, interval oms_payment_terms day) where out_stock_hist.oms_payment_event in("factura","pedido");

 

-- C140 U

update production.out_stock_hist set out_stock_hist.date_payable_promised = adddate(date_procurement_order, interval oms_payment_terms + supplier_leadtime day)

where out_stock_hist.oms_payment_event="entrega";


/*-- C139 U

update production.out_stock_hist set out_stock_hist.date_payable_promised = date_entrance-supplier_leadtime

where out_stock_hist.oms_payment_event="factura";


-- C140 U

update production.out_stock_hist set out_stock_hist.date_payable_promised = date_entrance

where out_stock_hist.oms_payment_event="entrega";


-- C141 U

update production.out_stock_hist set out_stock_hist.date_payable_promised = date_entrance

where out_stock_hist.oms_payment_event="pedido";*/


-- C142 U

update production.out_stock_hist set out_stock_hist.sold = 1

where out_stock_hist.exit_type="sold";

 

-- C143 D

delete from production.sell_rate_simple;


-- C144 A

insert into production.sell_rate_simple(sku_simple,num_items,num_sales,average_days_in_stock)

select sku_simple, sum(item_counter),sum(sold),avg(days_in_stock)from production.out_stock_hist group by sku_simple;


-- C144b U

update production.sell_rate_simple srs set srs.num_items_available = ( select sum(in_stock) from production.out_stock_hist osh where reserved = '0' and srs.sku_simple = osh.sku_simple group by sku_simple);


-- C145 U

update production.sell_rate_simple set sell_rate_simple.average_sell_rate = case when average_days_in_stock=0 then 0 else num_sales/average_days_in_stock end;

 

-- C145b U

update production.sell_rate_simple set sell_rate_simple.remaining_days = case when average_sell_rate=0 then 0 else num_items_available/average_sell_rate end;

 

-- C145c U

update production.out_stock_hist inner join production.sell_rate_simple on out_stock_hist.sku_simple = sell_rate_simple.sku_simple set out_stock_hist.average_remaining_days = sell_rate_simple.remaining_days;

 

-- C146 D

delete from production.pro_max_days_in_stock;

 

-- C147 A

insert into production.pro_max_days_in_stock select * from (select sku_simple, days_in_stock from production.out_stock_hist where in_stock=1 order by sku_simple, days_in_stock desc)n group by sku_simple;

 

-- C148 U 

update production.out_stock_hist inner join production.pro_max_days_in_stock on out_stock_hist.sku_simple= pro_max_days_in_stock.sku_simple set out_stock_hist.max_days_in_stock = pro_max_days_in_stock.max_days_in_stock;

 

-- C149 U

update production.out_stock_hist set out_stock_hist.week_payable_promised = production.week_iso(date_payable_promised);

 

-- C150 U

update production.out_stock_hist set out_stock_hist. week_procurement_order = production.week_iso(date_procurement_order);

 

-- C151 U

update production.out_stock_hist inner join bob_live_mx.catalog_simple on out_stock_hist.sku_simple = catalog_simple.sku set out_stock_hist.delivery_cost_supplier = catalog_simple.delivery_cost_supplier;

 

-- C152 U

update production.out_stock_hist set cogs = cost_w_o_vat + delivery_cost_supplier;

 

-- C153

update production.out_stock_hist set out_stock_hist.days_payable_since_entrance = case when date_entrance is null then null else (case when oms_payment_event in('factura','pedido') then oms_payment_terms - supplier_leadtime else (case when oms_payment_event = 'entrega' then oms_payment_terms end)end)end;

 

-- C154

update production.out_stock_hist a inner join bob_live_mx.catalog_config b on a.sku_config = b.sku inner join bob_live_mx.catalog_attribute_option_global_category c on b.fk_catalog_attribute_option_global_category = c.id_catalog_attribute_option_global_category inner join bob_live_mx.catalog_attribute_option_global_sub_category d on b.fk_catalog_attribute_option_global_sub_category = d.id_catalog_attribute_option_global_sub_category inner join bob_live_mx.catalog_attribute_option_global_sub_sub_category e on b.fk_catalog_attribute_option_global_sub_sub_category = e.id_catalog_attribute_option_global_sub_sub_category set a.category_com_main = c.name, a.category_com_sub = d.name, a.category_com_sub_sub = e.name;

 

-- C155

update production.out_stock_hist a inner join development_mx.A_E_M1_New_CategoryBP b on a.category_com_sub = b.cat2 set a.category_bp = b.CatBP;


-- C156

update production.out_stock_hist a inner join development_mx.A_E_M1_New_CategoryBP b on a.category_com_main = b.cat1 set a.category_bp = b.CatBP where a.category_bp is null;

 

-- C157

update production.out_stock_hist set fullfilment_type_bp =

case when fullfilment_type_real = 'crossdock' then 'crossdocking'

when fullfilment_type_real = 'inventory' and fullfilment_type_bob = 'consignment' then 'consignment'

when fullfilment_type_real = 'inventory' and fullfilment_type_bob <> 'consignment' then 'outright buying'

when fullfilment_type_real = 'dropshipping' then 'other' end;

 

-- Operaciones

-- Macro D

 

-- D001 D


delete production.out_catalog_tracking.* from production.out_catalog_tracking;

-- D101 I


insert into production.out_catalog_tracking (sku_simple, delivery_time_supplier, delivery_cost_supplier, sku_supplier_simple, cost, price, special_price, special_to_date, special_from_date,barcode_ean, eligible_free_shipping) select sku, delivery_time_supplier, delivery_cost_supplier, sku_supplier_simple, cost, price, special_price, special_to_date, special_from_date, barcode_ean, eligible_free_shipping from bob_live_mx.catalog_simple where status <> 'deleted';


-- D102 U

update production.out_catalog_tracking a inner join bob_live_mx.catalog_simple b on a.sku_simple = b.sku inner join bob_live_mx.catalog_config c on b.fk_catalog_config = c.id_catalog_config set a.sku_config = c.sku, a.sku_name = c.name, a.package_height = c.package_height, a.package_length = c.package_length, a.package_width = c.package_width, a.package_weight = c.package_weight, a.id_supplier = c.fk_supplier, a.sku_supplier_config = c.sku_supplier_config, a.model = c.model, a.pet_approved = c.pet_approved;


-- D103 U

update production.out_catalog_tracking a inner join bob_live_mx.catalog_simple b on a.sku_simple = b.sku inner join bob_live_mx.catalog_shipment_type c on b.fk_catalog_shipment_type = c.id_catalog_shipment_type set a.shipment_type = c.name;


-- D104 U

update production.out_catalog_tracking a inner join bob_live_mx.catalog_config b on a.sku_config = b.sku inner join bob_live_mx.catalog_attribute_option_global_inbound_shipment_type c on b.fk_catalog_attribute_option_global_inbound_shipment_type = c.id_catalog_attribute_option_global_inbound_shipment_type set a.inbound_shipment_type = c.name;


-- D105 U

update production.out_catalog_tracking a inner join bob_live_mx.supplier b on a.id_supplier = b.id_supplier set a.supplier = b.name;


-- D106 U

update production.out_catalog_tracking a inner join bob_live_mx.catalog_config b on a.sku_config = b.sku inner join bob_live_mx.catalog_brand c on b.fk_catalog_brand = c.id_catalog_brand set a.brand = c.name;


-- D107 U

update production.out_catalog_tracking a inner join bob_live_mx.catalog_config b on a.sku_config = b.sku inner join bob_live_mx.catalog_attribute_option_global_buyer_name c on b.fk_catalog_attribute_option_global_buyer_name = c.id_catalog_attribute_option_global_buyer_name set a.buyer = c.name;


-- D108 U

update production.out_catalog_tracking a inner join bob_live_mx.catalog_config b on a.sku_config = b.sku inner join bob_live_mx.catalog_attribute_option_global_head_buyer_name c on b.fk_catalog_attribute_option_global_head_buyer_name = c.id_catalog_attribute_option_global_head_buyer_name set a.head_buyer = c.name;


-- D109 U

update production.out_catalog_tracking a set a.catalog_warehouse_stock = (select sum(item_counter) from production.out_stock_hist b where b.in_stock = 1 and b.reserved = 0 and a.sku_simple = b.sku_simple group by b.sku_simple);


-- D110 U

update production.out_catalog_tracking a inner join bob_live_mx.catalog_simple b on a.sku_simple = b.sku inner join bob_live_mx.catalog_supplier_stock c on b.id_catalog_simple = c.fk_catalog_simple set a.catalog_supplier_stock = c.quantity;

 

-- Operaciones

-- Macro R

 

-- R100 D

/*delete production.pro_leadtime_track_ow.*

from production.pro_leadtime_track_ow;

 

-- R101 D

delete production.pro_leadtime_track_crossd.*

from production.pro_leadtime_track_crossd;

 

-- R102 D

delete production.out_leadtime_tracking.*

from production.out_leadtime_tracking;

 

-- R103 A

insert into production.pro_leadtime_track_crossd ( category, sku_config, sku_simple, supplier_leadtime, max_delivery_time, min_delivery_time, name, visible, max_delivery_time_published, min_delivery_time_published )

select categories_no_duplicates.maxofcat1, catalog_config.sku as sku_config, catalog_simple.sku as sku, sales_order_item.delivery_time_supplier, delivery_time_crossdocking.max_delivery_time as max_delivery_time, delivery_time_crossdocking.min_delivery_time as min_delivery_time, catalog_shipment_type.name, case when sku_simple is null then 0 else 1 end as visible, catalog_simple.max_delivery_time, catalog_simple.min_delivery_time

from (production.items_visible right join ((((bob_live_mx.catalog_config inner join bob_live_mx.catalog_simple on catalog_config.id_catalog_config = catalog_simple.fk_catalog_config) inner join production.categories_no_duplicates on catalog_config.sku = categories_no_duplicates.sku) inner join production.delivery_time_crossdocking on catalog_simple.delivery_time_supplier = delivery_time_crossdocking.supplier_leadtime) inner join bob_live_mx.catalog_shipment_type on catalog_simple.fk_catalog_shipment_type = catalog_shipment_type.id_catalog_shipment_type) on items_visible.sku_config = catalog_config.sku) inner join bob_live_mx.sales_order_item on catalog_simple.sku = sales_order_item.sku

group by categories_no_duplicates.maxofcat1, catalog_config.sku, catalog_simple.sku, sales_order_item.delivery_time_supplier, delivery_time_crossdocking.max_delivery_time, delivery_time_crossdocking.min_delivery_time, catalog_shipment_type.name, case when sku_simple is null then 0 else 1 end, catalog_simple.max_delivery_time, catalog_simple.min_delivery_time, sales_order_item.delivery_time_supplier, catalog_config.status

having (((catalog_shipment_type.name) like "crossdocking") and ((catalog_config.status) like "active"))

order by categories_no_duplicates.maxofcat1;

 

-- R104 U

update production.pro_leadtime_track_crossd set pro_leadtime_track_crossd.error_max_delivery = case when  max_delivery_time=max_delivery_time_published then 0 else 1 end, pro_leadtime_track_crossd.error_min_delivery = case when min_delivery_time=min_delivery_time_published then 0 else 1 end, pro_leadtime_track_crossd.error = case when error_max_delivery+error_min_delivery>0 then 1 else 0 end;

 

-- R105 U

update production.pro_leadtime_track_crossd inner join (bob_live_mx.catalog_config inner join bob_live_mx.catalog_attribute_option_global_buyer_name on catalog_config.fk_catalog_attribute_option_global_buyer_name = catalog_attribute_option_global_buyer_name.id_catalog_attribute_option_global_buyer_name) on pro_leadtime_track_crossd.sku_config = catalog_config.sku set pro_leadtime_track_crossd.buyer = catalog_attribute_option_global_buyer_name.name;

 

-- R106 U

update production.pro_leadtime_track_crossd inner join (bob_live_mx.catalog_config inner join bob_live_mx.catalog_attribute_option_global_head_buyer_name on catalog_config.fk_catalog_attribute_option_global_head_buyer_name = catalog_attribute_option_global_head_buyer_name.id_catalog_attribute_option_global_head_buyer_name) on pro_leadtime_track_crossd.sku_config = catalog_config.sku set pro_leadtime_track_crossd.head_buyer = catalog_attribute_option_global_head_buyer_name.name;

 

update production.pro_leadtime_track_crossd set item_counter = 1;

 

-- R107 A

insert into production.pro_leadtime_track_ow ( category, sku_config, sku_simple, max_delivery_time, min_delivery_time, name, package_weight, status, visible, max_delivery_time_published, min_delivery_time_published )

select categories_no_duplicates.maxofcat1, catalog_config.sku as sku_config, catalog_simple.sku as sku, case when catalog_config.package_weight<21 then (case when catalog_config.package_height<41 then (case when catalog_config.package_length<41 then (case when catalog_config.package_width<41 then 5 else 7 end) else 7 end) else 7 end) else 7 end as max_delivery_time, case when catalog_config.package_weight<21 then ( case when catalog_config.package_height<41 then (case when catalog_config.package_length<41 then (case when catalog_config.package_width<41 then 3 else 4 end) else 4 end) else 4 end) else 4 end as min_delivery_time, catalog_shipment_type.name, catalog_config.package_weight, catalog_config.status, case when sku_simple is null then 0 else 1 end as visible, catalog_simple.max_delivery_time as max_delivery_time_published, catalog_simple.min_delivery_time as min_delivery_time_published

from production.items_visible right join ((((bob_live_mx.catalog_config inner join bob_live_mx.catalog_simple on catalog_config.id_catalog_config = catalog_simple.fk_catalog_config) inner join production.categories_no_duplicates on catalog_config.sku = categories_no_duplicates.sku) inner join production.delivery_time_crossdocking on catalog_simple.delivery_time_supplier = delivery_time_crossdocking.supplier_leadtime) inner join bob_live_mx.catalog_shipment_type on catalog_simple.fk_catalog_shipment_type = catalog_shipment_type.id_catalog_shipment_type) on items_visible.sku_config = catalog_config.sku

where (((catalog_shipment_type.name) like "own warehouse") and ((catalog_config.status) like "active")) or (((catalog_shipment_type.name) like "consignment") and ((catalog_config.status) like "active"))

order by catalog_config.package_weight desc;

 

-- R108 U

update production.pro_leadtime_track_ow set pro_leadtime_track_ow.error_max_delivery = case when max_delivery_time=max_delivery_time_published then 0 else 1 end, pro_leadtime_track_ow.error_min_delivery = case when min_delivery_time=min_delivery_time_published then 0 else 1 end, pro_leadtime_track_ow.error = case when error_max_delivery+error_min_delivery>0 then 1 else 0 end;

 

-- R109 U

update production.pro_leadtime_track_ow inner join (bob_live_mx.catalog_config inner join bob_live_mx.catalog_attribute_option_global_buyer_name on catalog_config.fk_catalog_attribute_option_global_buyer_name = catalog_attribute_option_global_buyer_name.id_catalog_attribute_option_global_buyer_name) on pro_leadtime_track_ow.sku_config = catalog_config.sku set pro_leadtime_track_ow.buyer = catalog_attribute_option_global_buyer_name.name;

 

-- R110 U

update production.pro_leadtime_track_ow inner join (bob_live_mx.catalog_config inner join bob_live_mx.catalog_attribute_option_global_head_buyer_name on catalog_config.fk_catalog_attribute_option_global_head_buyer_name = catalog_attribute_option_global_head_buyer_name.id_catalog_attribute_option_global_head_buyer_name) on pro_leadtime_track_ow.sku_config = catalog_config.sku set pro_leadtime_track_ow.head_buyer = catalog_attribute_option_global_head_buyer_name.name;

 

update production.pro_leadtime_track_ow set item_counter = 1;

 

-- R111 A

insert into production.out_leadtime_tracking ( category, sku_config, sku_simple, supplier_leadtime, max_delivery_time, max_delivery_time_published, error_max_delivery, min_delivery_time, min_delivery_time_published, error_min_delivery, error, fullfilment_type_bob, visible, buyer, head_buyer, item_counter )

select pro_leadtime_track_crossd.category, pro_leadtime_track_crossd.sku_config, pro_leadtime_track_crossd.sku_simple, pro_leadtime_track_crossd.supplier_leadtime, pro_leadtime_track_crossd.max_delivery_time, pro_leadtime_track_crossd.max_delivery_time_published, pro_leadtime_track_crossd.error_max_delivery, pro_leadtime_track_crossd.min_delivery_time, pro_leadtime_track_crossd.min_delivery_time_published, pro_leadtime_track_crossd.error_min_delivery, pro_leadtime_track_crossd.error, pro_leadtime_track_crossd.name, pro_leadtime_track_crossd.visible, pro_leadtime_track_crossd.buyer, pro_leadtime_track_crossd.head_buyer, pro_leadtime_track_crossd.item_counter

from production.pro_leadtime_track_crossd;

 

-- R112 A

insert into production.out_leadtime_tracking ( category, sku_config, sku_simple, max_delivery_time, max_delivery_time_published, error_max_delivery, min_delivery_time, error_min_delivery, error, fullfilment_type_bob, visible, buyer, head_buyer, package_weight, status, item_counter )

select pro_leadtime_track_ow.category, pro_leadtime_track_ow.sku_config, pro_leadtime_track_ow.sku_simple, pro_leadtime_track_ow.max_delivery_time, pro_leadtime_track_ow.max_delivery_time_published, pro_leadtime_track_ow.error_max_delivery, pro_leadtime_track_ow.min_delivery_time, pro_leadtime_track_ow.error_min_delivery, pro_leadtime_track_ow.error, pro_leadtime_track_ow.name, pro_leadtime_track_ow.visible, pro_leadtime_track_ow.buyer, pro_leadtime_track_ow.head_buyer, pro_leadtime_track_ow.package_weight, pro_leadtime_track_ow.status, pro_leadtime_track_ow.item_counter

from production.pro_leadtime_track_ow;

 

-- R113 U

update production.out_leadtime_tracking inner join ((bob_live_mx.catalog_simple inner join bob_live_mx.catalog_config on catalog_simple.fk_catalog_config = catalog_config.id_catalog_config) inner join bob_live_mx.supplier on catalog_config.fk_supplier = supplier.id_supplier) on out_leadtime_tracking.sku_simple = catalog_simple.sku set out_leadtime_tracking.supplier_id = id_supplier, out_leadtime_tracking.supplier_name = supplier.name;*/

 

-- Extra

/*delete from rafael.out_order_tracking;

 

insert into rafael.out_order_tracking select * from production.out_order_tracking;*/

 

/*delete from rafael.out_stock_hist;

 

insert into rafael.out_stock_hist select * from production.out_stock_hist;*/

 

delete from rafael.out_leadtime_tracking;

 

insert into rafael.out_leadtime_tracking select * from production.out_leadtime_tracking;

 

select  'daily execution ops: end',now();

 

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `daily_marketing`()
BEGIN

#date

DELETE FROM tbl_daily_marketing WHERE date=CAST(CURDATE()-1 as date);

INSERT INTO tbl_daily_marketing (date, channel) 
SELECT DISTINCT date, channel_group FROM tbl_order_detail WHERE date=CAST(CURDATE()-1 as date);

#VISITS	
UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, reporting_channel, sum(visits) visits
FROM daily_costs_visits_per_channel
GROUP BY date, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_daily_marketing.visits=daily_costs_visits_per_channel.visits;

#GROSS ORDERS
UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, channel_group, sum(gross_orders) gross_orders
FROM tbl_order_detail
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=tbl_order_detail.channel_group
SET tbl_daily_marketing.gross_orders=tbl_order_detail.gross_orders;

#NET ORDERS
UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, channel_group, sum(net_orders) net_orders
FROM tbl_order_detail
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=tbl_order_detail.channel_group
SET tbl_daily_marketing.net_orders=tbl_order_detail.net_orders;

#ORDERS NET EXPECTED

UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, reporting_channel, sum(net_orders_e) net_orders_e
FROM daily_costs_visits_per_channel
GROUP BY date, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_daily_marketing.orders_net_expected=daily_costs_visits_per_channel.net_orders_e;

#CONVERSION RATE
UPDATE tbl_daily_marketing SET conversion_rate=IF(gross_orders/visits IS NULL, 0, gross_orders/visits);

#PENDING REVENUE
UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, channel_group, 
(SUM(actual_paid_price_after_tax)+SUM(shipping_fee_after_vat))/16.35 pending_sales
FROM tbl_order_detail WHERE tbl_order_detail.pending=1 
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing.date 
SET tbl_daily_marketing.pending_revenue=tbl_order_detail.pending_sales
WHERE tbl_daily_marketing.channel=tbl_order_detail.channel_group;

#GROSS REVENUE
UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, channel_group, 
(SUM(actual_paid_price_after_tax)+SUM(shipping_fee_after_vat))/16.35 gross_sales
FROM tbl_order_detail WHERE tbl_order_detail.obc=1 
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing.date 
SET tbl_daily_marketing.gross_revenue=tbl_order_detail.gross_sales
WHERE tbl_daily_marketing.channel=tbl_order_detail.channel_group;

#NET REVENUE
UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, channel_group, 
(SUM(actual_paid_price_after_tax)+SUM(shipping_fee_after_vat))/16.35 net_sales
FROM tbl_order_detail WHERE tbl_order_detail.oac=1 AND tbl_order_detail.returned=0  
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing.date 
SET tbl_daily_marketing.net_revenue=tbl_order_detail.net_sales
WHERE tbl_daily_marketing.channel=tbl_order_detail.channel_group;

#NET REVENUE EXPECTED

UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, 
reporting_channel, sum(net_rev_e)/16.35 net_rev_e
FROM daily_costs_visits_per_channel
GROUP BY date, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_daily_marketing.net_revenue_expected=daily_costs_visits_per_channel.net_rev_e;
UPDATE tbl_daily_marketing SET net_revenue_expected = net_revenue WHERE net_revenue_expected=0;

#MONTHLY TARGET

UPDATE tbl_daily_marketing  
INNER JOIN (SELECT date, channel_group, sum(target_net_sales) daily_target
FROM daily_targets_per_channel
GROUP BY date, channel_group) daily_targets_per_channel
ON daily_targets_per_channel.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=daily_targets_per_channel.channel_group
SET tbl_daily_marketing.daily_target=daily_targets_per_channel.daily_target;

#COST VS REV
UPDATE tbl_daily_marketing SET target_vs_rev=(net_revenue_expected/daily_target)-1;

#AVG TICKET
UPDATE tbl_daily_marketing SET avg_ticket=net_revenue/net_orders;

#MARKETING COST

UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, 
reporting_channel, sum(cost_local)/16.35 cost
FROM daily_costs_visits_per_channel
GROUP BY date, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_daily_marketing.marketing_cost=daily_costs_visits_per_channel.cost;

#COST/REV
UPDATE tbl_daily_marketing SET cost_rev=IF(marketing_cost/net_revenue_expected IS NULL, 0, marketing_cost/net_revenue_expected);

#CPO
UPDATE tbl_daily_marketing SET cost_per_order=IF(marketing_cost/net_orders IS NULL, 0, marketing_cost/net_orders);



#CPO TARGET

UPDATE tbl_daily_marketing  
INNER JOIN (SELECT date, channel_group, MAX(target_cpo) target_cpo
FROM daily_targets_per_channel
GROUP BY date, channel_group) daily_targets_per_channel
ON daily_targets_per_channel.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=daily_targets_per_channel.channel_group
SET tbl_daily_marketing.cost_per_order_target=daily_targets_per_channel.target_cpo;

#CPO VS TARGET
UPDATE tbl_daily_marketing SET cpo_vs_target_cpo=(cost_per_order/cost_per_order_target)-1;

#NEW CLIENTS
UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, channel_group, sum(new_customers) nc
FROM tbl_order_detail
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=tbl_order_detail.channel_group
SET tbl_daily_marketing.new_clients=tbl_order_detail.nc;

#NEW CLIENTS EXPECTED
UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, 
reporting_channel, sum(new_customers_e) nc_e
FROM daily_costs_visits_per_channel
GROUP BY date, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.date=tbl_daily_marketing.date 
AND tbl_daily_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_daily_marketing.new_clientes_expected=daily_costs_visits_per_channel.nc_e;
UPDATE tbl_daily_marketing SET new_clientes_expected=new_clients WHERE new_clientes_expected=0;

#CAC
UPDATE tbl_daily_marketing SET client_acquisition_cost=marketing_cost/new_clientes_expected;

#PC2_COST

UPDATE tbl_daily_marketing  INNER JOIN (SELECT date, channel_group, 
(SUM(costo_after_vat)+SUM(delivery_cost_supplier)+SUM(wh)+SUM(cs)+SUM(payment_cost)+SUM(shipping_cost))/16.35 pc2_costs
FROM tbl_order_detail WHERE tbl_order_detail.oac=1 AND tbl_order_detail.returned=0  
GROUP BY date, channel_group) tbl_order_detail
ON tbl_order_detail.date=tbl_daily_marketing.date 
SET tbl_daily_marketing.pc2_costs=tbl_order_detail.pc2_costs
WHERE tbl_daily_marketing.channel=tbl_order_detail.channel_group;

#PC2
UPDATE tbl_daily_marketing SET profit_contribution_2= 1-(pc2_costs/net_revenue);

#MOM
select 'Month over month: ', now();
CALL mom_net_sales_per_channel();

UPDATE tbl_daily_marketing INNER JOIN tbl_mom_net_sales_per_channel 
ON tbl_daily_marketing.channel = tbl_mom_net_sales_per_channel.channel_group
AND tbl_daily_marketing.date = tbl_mom_net_sales_per_channel.date
SET tbl_daily_marketing.month_over_month = tbl_mom_net_sales_per_channel.month_over_month;

INSERT IGNORE INTO tbl_daily_marketing (date, channel) 
VALUES(date_add(now(), interval -1 day), 'MTD');

UPDATE tbl_daily_marketing INNER JOIN tbl_mom_net_sales_per_channel 
ON tbl_daily_marketing.channel = tbl_mom_net_sales_per_channel.channel_group
AND tbl_daily_marketing.date = tbl_mom_net_sales_per_channel.date
SET tbl_daily_marketing.month_over_month = tbl_mom_net_sales_per_channel.month_over_month
where channel_group = 'MTD';

#Adjusted revenue
select 'Adjusted revenue: ', now();
#Net
update tbl_daily_marketing m left join 
(
select t1.*, t2.totalAbout, t1.totalChannel/t2.totalAbout as proporcion, t3.netSales, 
t1.totalChannel/t2.totalAbout*t3.netSales adjusted_net_revenue
from
(select t.date, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by t.date desc,l.about_linio, t.channel_group) t1 inner join
(select t.date, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by t.date desc,l.about_linio) t2 on t1.date = t2.date and t1.about_linio = t2.about_linio
inner join
(select date, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0
group by date desc, t.about_linio) t3 on t3.date = t2.date and t3.about_linio = t2.about_linio) t 
on m.date = t.date and m.channel = t.channel_group 
set m.adjusted_net_revenue = ifnull(m.net_revenue + t.adjusted_net_revenue/16.35,m.net_revenue)
where m.channel <> 'Tele Sales';

update tbl_daily_marketing m left join 
(
select t1.*, t2.totalAbout, t1.totalChannel/t2.totalAbout as proporcion, t3.netSales, 
t1.totalChannel/t2.totalAbout*t3.netSales adjusted_net_revenue
from
(select t.date, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by t.date desc,l.about_linio, t.channel_group) t1 inner join
(select t.date, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0
group by t.date desc,l.about_linio) t2 on t1.date = t2.date and t1.about_linio = t2.about_linio
inner join
(select date, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0
group by date desc, t.about_linio) t3 on t3.date = t2.date and t3.about_linio = t2.about_linio) t 
on m.date = t.date and m.channel = t.channel_group set m.adjusted_net_revenue = ifnull(t.adjusted_net_revenue/16.35,0)
where m.channel = 'Tele Sales';

#Gross
update tbl_daily_marketing m left join 
(
select t1.*, t2.totalAbout, t1.totalChannel/t2.totalAbout as proporcion, t3.netSales, 
t1.totalChannel/t2.totalAbout*t3.netSales adjusted_revenue
from
(select t.date, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where obc  = 1
group by t.date desc,l.about_linio, t.channel_group) t1 inner join
(select t.date, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where obc  = 1
group by t.date desc,l.about_linio) t2 on t1.date = t2.date and t1.about_linio = t2.about_linio
inner join
(select date, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and obc  = 1
group by date desc, t.about_linio) t3 on t3.date = t2.date and t3.about_linio = t2.about_linio) t 
on m.date = t.date and m.channel = t.channel_group 
set m.adjusted_gross_revenue = ifnull(m.gross_revenue + t.adjusted_revenue/16.35,m.gross_revenue)
where m.channel <> 'Tele Sales';

update tbl_daily_marketing m left join 
(
select t1.*, t2.totalAbout, t1.totalChannel/t2.totalAbout as proporcion, t3.netSales, t1.totalChannel/t2.totalAbout*t3.netSales adjusted_revenue
from
(select t.date, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where obc  = 1
group by t.date desc,l.about_linio, t.channel_group) t1 inner join
(select t.date, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where obc  = 1
group by t.date desc,l.about_linio) t2 on t1.date = t2.date and t1.about_linio = t2.about_linio
inner join
(select date, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and obc  = 1
group by date desc, t.about_linio) t3 on t3.date = t2.date and t3.about_linio = t2.about_linio) t 
on m.date = t.date and m.channel = t.channel_group set m.adjusted_gross_revenue = ifnull(t.adjusted_revenue/16.35,0)
where m.channel = 'Tele Sales';

#Pending
update tbl_daily_marketing m left join 
(
select t1.*, t2.totalAbout, t1.totalChannel/t2.totalAbout as proporcion, t3.netSales, t1.totalChannel/t2.totalAbout*t3.netSales adjusted_revenue
from
(select t.date, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where pending  = 1
group by t.date desc,l.about_linio, t.channel_group) t1 inner join
(select t.date, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where pending  = 1
group by t.date desc,l.about_linio) t2 on t1.date = t2.date and t1.about_linio = t2.about_linio
inner join
(select date, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and pending  = 1
group by date desc, t.about_linio) t3 on t3.date = t2.date and t3.about_linio = t2.about_linio) t 
on m.date = t.date and m.channel = t.channel_group 
set m.adjusted_pending_revenue = ifnull(m.pending_revenue + t.adjusted_revenue/16.35,m.pending_revenue)
where m.channel <> 'Tele Sales';

update tbl_daily_marketing m left join 
(
select t1.*, t2.totalAbout, t1.totalChannel/t2.totalAbout as proporcion, t3.netSales, t1.totalChannel/t2.totalAbout*t3.netSales adjusted_revenue
from
(select t.date, l.about_linio,t.channel_group,  sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where pending  = 1
group by t.date desc,l.about_linio, t.channel_group) t1 inner join
(select t.date, l.about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where pending  = 1
group by t.date desc,l.about_linio) t2 on t1.date = t2.date and t1.about_linio = t2.about_linio
inner join
(select date, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(paid_price_after_vat) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and pending  = 1
group by date desc, t.about_linio) t3 on t3.date = t2.date and t3.about_linio = t2.about_linio) t 
on m.date = t.date and m.channel = t.channel_group set m.adjusted_pending_revenue = ifnull(t.adjusted_revenue/16.35,0)
where m.channel = 'Tele Sales';




END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `daily_report`()
begin

truncate production.tbl_dailyReport;
insert into production.tbl_dailyReport (date) select dt from production.calendar where dt>'2012-07-31' and dt<now();

update production.tbl_dailyReport
set year = year(date) , month = month(date) , yrmonth = concat(year,if(month<10,concat(0,month),month));

#gross
update production.tbl_dailyReport join 
(select date,sum(unit_price) as grossrev, 
count(distinct order_nr,oac,returned,pending) as grossorders, 
count(order_nr) as grossitems,
sum(unit_price_after_vat) as grosssalesprecancel
from production.tbl_order_detail where   (obc = 1)
group by date) as tbl_gross on tbl_gross.date=tbl_dailyReport.date
set tbl_dailyReport.grossrev=tbl_gross.grossrev ,
 tbl_dailyReport.grossorders=tbl_gross.grossorders, 
tbl_dailyReport.grossitems=tbl_gross.grossitems,
tbl_dailyReport.grosssalesprecancel=tbl_gross.grosssalesprecancel;

#cancel
update production.tbl_dailyReport join 
(select date,sum(unit_price_after_vat) as cancelrev, 
count(distinct order_nr) as cancelorders
from production.tbl_order_detail 
where   (cancel = 1)
group by date) as tbl_cancel on tbl_cancel.date=tbl_dailyReport.date
set tbl_dailyReport.cancellations=tbl_cancel.cancelrev , 
tbl_dailyReport.cancelorders=tbl_cancel.cancelorders;


#pending
update production.tbl_dailyReport join 
(select date,sum(unit_price_after_vat) as pendingrev, 
count(distinct order_nr) as pendingorders
from production.tbl_order_detail 
where   (pending = 1)
group by date) as tbl_pending on tbl_pending.date=tbl_dailyReport.date
set tbl_dailyReport.pendingcop=tbl_pending.pendingrev , 
tbl_dailyReport.pendingorders=tbl_pending.pendingorders;


#cogs_gross_post_cancel
update production.tbl_dailyReport join 
(select date,(sum(costo_after_vat) + sum(delivery_cost_supplier)) as cogs_gross_post_cancel
from production.tbl_order_detail 
where   (oac = 1)
group by date) as tbl_cogs_post_cancel 
on tbl_cogs_post_cancel.date=tbl_dailyReport.date
set tbl_dailyReport.cogs_gross_post_cancel=tbl_cogs_post_cancel.cogs_gross_post_cancel;

#pending
update production.tbl_dailyReport join 
(select date,sum(unit_price_after_vat) as returnrev, count(distinct order_nr) as returnorders
from production.tbl_order_detail 
where   (returned = 1)
group by date) as tbl_returned on tbl_returned.date=tbl_dailyReport.date
set tbl_dailyReport.returnedcop=tbl_returned.returnrev , tbl_dailyReport.returnedorders=tbl_returned.returnorders;

#net
update production.tbl_dailyReport join 
(select date,sum(unit_price_after_vat) as netsalesprevoucher, 
sum(coupon_money_after_vat) as vouchermktcost,
sum(paid_price_after_vat) as netsales,
count(distinct order_nr) as netorders,
 (sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
count(order_nr)/count(distinct order_nr) as averageitemsperbasket,
count(distinct custid) as customers,
sum(shipping_fee/(1+tax_percent/100)) as shippingfee, 
sum(shipping_cost)as shippingcost, 
sum(payment_cost) as paymentcost
from production.tbl_order_detail 
where   (returned = 0) and (oac = 1)
group by date) as tbl_net on tbl_net.date=tbl_dailyReport.date
set tbl_dailyReport.netsalesprevoucher=tbl_net.netsalesprevoucher ,
 tbl_dailyReport.vouchermktcost=tbl_net.vouchermktcost,
tbl_dailyReport.netsales=tbl_net.netsales,tbl_dailyReport.netorders=tbl_net.netorders,
tbl_dailyReport.cogs_on_net_sales=tbl_net.cogs_on_net_sales,tbl_dailyReport.averageitemsperbasket=tbl_net.averageitemsperbasket,
tbl_dailyReport.customers=tbl_net.customers,
tbl_dailyReport.shippingfee=tbl_net.shippingfee,
tbl_dailyReport.shippingcost=tbl_net.shippingcost,tbl_dailyReport.paymentcost=tbl_net.paymentcost;

 
#categorías
#electrodomésticos
update production.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/(1+tax_percent/100)) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production.tbl_order_detail 
where  (oac = 1) and (returned = 0) and (n1 = 'electrodomésticos')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_appiliances=tbl_cat.netsales , tbl_dailyReport.cogs_appiliances=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_appliances=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_appliances=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_appliances=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_appliances=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_appliances=tbl_cat.payment_fee;

#cámaras y fotografía
update production.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/(1+tax_percent/100)) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production.tbl_order_detail 
where  (oac = 1) and (returned = 0) and(n1 = 'cámaras y fotografía')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_photography=tbl_cat.netsales , tbl_dailyReport.cogs_photography=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_photography=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_photography=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_photography=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_photography=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_photography=tbl_cat.payment_fee;


#tv, video y audio
update production.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/(1+tax_percent/100)) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production.tbl_order_detail 
where  (oac = 1) and (returned = 0) and(n1 ='tv, audio y video')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_tv_audio_video=tbl_cat.netsales , tbl_dailyReport.cogs_tv_audio_video=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_tv_audio_video=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_tv_audio_video=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_tv_audio_video=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_tv_audio_video=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_tv_audio_video=tbl_cat.payment_fee;

#libros
update production.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/(1+tax_percent/100)) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production.tbl_order_detail 
where  (oac = 1) and (returned = 0) and(n1 like 'libros%')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_books=tbl_cat.netsales , tbl_dailyReport.cogs_books=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_books=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_books=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_books=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_books=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_books=tbl_cat.payment_fee;

#videojuegos
update production.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/(1+tax_percent/100)) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production.tbl_order_detail 
where  (oac = 1) and (returned = 0) and(n1 = 'videojuegos')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_videogames=tbl_cat.netsales , tbl_dailyReport.cogs_videogames=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_videogames=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_videogames=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_videogames=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_videogames=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_videogames=tbl_cat.payment_fee;

#fashion
update production.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/(1+tax_percent/100)) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production.tbl_order_detail 
where  (oac = 1) and (returned = 0) and(n1 = 'ropa, calzado y accesorios')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_fashion=tbl_cat.netsales , tbl_dailyReport.cogs_fashion=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_fashion=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_fashion=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_fashion=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_fashion=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_fashion=tbl_cat.payment_fee;


#cuidado personal
update production.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/(1+tax_percent/100)) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production.tbl_order_detail 
where  (oac = 1) and (returned = 0) and(n1 like '%cuidado personal%')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_health_beauty=tbl_cat.netsales , tbl_dailyReport.cogs_health_beauty=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_health_beauty=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_health_beauty=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_health_beauty=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_health_beauty=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_health_beauty=tbl_cat.payment_fee;


#teléfonos y gps
update production.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/(1+tax_percent/100)) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production.tbl_order_detail 
where  (oac = 1) and (returned = 0) and(n1 like 'celulares%')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_cellphones=tbl_cat.netsales , tbl_dailyReport.cogs_cellphones=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_cellphones=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_cellphones=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_cellphones=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_cellphones=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_cellphones=tbl_cat.payment_fee;


 
#computadores y tablets    
update production.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/(1+tax_percent/100)) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production.tbl_order_detail 
where  (oac = 1) and (returned = 0) and(n1 like 'compu%')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_computing=tbl_cat.netsales , tbl_dailyReport.cogs_computing=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_computing=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_computing=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_computing=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_computing=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_computing=tbl_cat.payment_fee;


#hogar y muebles   
update production.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/(1+tax_percent/100)) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from production.tbl_order_detail 
where  (oac = 1) and (returned = 0) and(n1 = 'hogar' or n1 like '%muebles')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_hogar=tbl_cat.netsales , tbl_dailyReport.cogs_hogar=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_hogar=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_hogar=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_hogar=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_hogar=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_hogar=tbl_cat.payment_fee;


#juguetes, niños y bebés 
update production.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as netsales,
(sum(costo_after_vat) + sum(delivery_cost_supplier))as cogs_on_net_sales,
sum(shipping_fee/(1+tax_percent/100)) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(wh) as wh_cost,
sum(cs) as cs_cost,
sum(payment_cost) as payment_fee
from tbl_order_detail 
where  (oac = 1) and (returned = 0) and(n1 ='juguetes, niños y bebés')
group by date) as tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_kids_babies=tbl_cat.netsales , tbl_dailyReport.cogs_kids_babies=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.shippingfee_kids_babies=tbl_cat.shipping_fee, tbl_dailyReport.shippingcost_kids_babies=tbl_cat.shipping_cost,
tbl_dailyReport.wh_cost_kids_babies=tbl_cat.wh_cost, tbl_dailyReport.cs_cost_kids_babies=tbl_cat.cs_cost,
tbl_dailyReport.payment_fees_kids_babies=tbl_cat.payment_fee;

#paidchannels
 update production.tbl_dailyReport join 
(select date,sum(paid_price_after_vat) as net_sales_paid_channel,
sum(coupon_money_after_vat) as voucher_paid_channel
from production.tbl_order_detail 
where  (oac = 1) and (returned = 0) 
and   ((source_medium like '%price comparison%')
  or (source_medium like '%socialmediaads%')
  or (source_medium like '%cpc%')
  or (source_medium like '%sociomantic%'))
group by date) as tbl_paid on tbl_paid.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_paid_channel=tbl_paid.net_sales_paid_channel
 , tbl_dailyReport.voucher_paid_channel=tbl_paid.voucher_paid_channel;

# new customers
update production.tbl_dailyReport join  
(select firstorder as date,count(*) as newcustomers from production.view_cohort 
group by firstorder) as tbl_nc on tbl_nc.date=tbl_dailyReport.date
set tbl_dailyReport.newcustomers=tbl_nc.newcustomers;


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `daily_report_EUR`()
BEGIN

truncate tbl_dailyReport_EUR;
insert into tbl_dailyReport_EUR (date) select dt from calendar where dt>'2012-07-31' and dt<now();

update tbl_dailyReport_EUR
set year = year(date) , month = month(date) , yrmonth = concat(year,if(month<10,concat(0,month),month));

#Visits
UPDATE tbl_dailyReport_EUR t INNER JOIN tbl_dailyReport_EUR g 
ON t.date = g.date
SET t.Visits = g.visits;

#Visits
UPDATE tbl_dailyReport_EUR t INNER JOIN tbl_dailyReport_EUR g 
ON t.date = g.date
SET t.UniqueVisits = g.UniqueVisits;

#Mobile Visits
UPDATE tbl_dailyReport_EUR t INNER JOIN tbl_dailyReport_EUR g 
ON t.date = g.date
SET t.MobileVisits = if(g.MobileVisits is null, 0, g.MobileVisits);

#EUR
update tbl_dailyReport_EUR e join 
tbl_dailyReport_local l on e.date = l.date
join tbl_exchange_rate 
on tbl_exchange_rate.yrmonth = cast(concat(year(e.date),if(month(e.date)<10, concat(0,month(e.date)),month(e.date))) as unsigned )
set e.GrossRev = l.GrossRev/rate,
e.GrossSalesPreCancel = l.GrossSalesPreCancel/rate,
e.Cancellations = l.Cancellations/rate,
e.Pending = l.PendingCOP/rate,
e.cogs_gross_post_cancel = l.cogs_gross_post_cancel/rate,
e.Returned = l.ReturnedCop/rate,
e.NetSales = l.NetSales/rate,
e.NetSalesPreVoucher = l.NetSalesPreVoucher/rate,
e.VoucherMktCost = l.VoucherMktCost/rate,
e.cogs_on_net_sales = l.cogs_on_net_sales/rate,
e.marketing_spend = l.marketing_spend/rate,
e.net_sales_paid_channel = l.net_sales_paid_channel/rate,
e.voucher_paid_channel = l.voucher_paid_channel/rate,
e.ShippingFee = l.ShippingFee/rate,
e.ShippingCost = l.ShippingCost/rate,
e.ShippingCostPerOrder = l.ShippingCostPerOrder/rate,
e.PaymentCost = l.PaymentCost/rate,
e.PaymentCostPerOrder = l.PaymentCostPerOrder/rate,
e.WHCostPerOrder = l.WHCostPerOrder/rate,
e.CSCostPerOrder = l.CSCostPerOrder,
e.net_sales_Appliances = l.net_sales_Appliances/rate,
e.costs_Appliances = l.costs_Appliances/rate,
e.inboundCosts_Appliances = l.inboundCosts_Appliances/rate,
e.cogs_Appliances = l.cogs_Appliances/rate,
e.ShippingFee_Appliances = l.ShippingFee_Appliances/rate,
e.ShippingCost_Appliances = l.ShippingCost_Appliances/rate,
e.WH_Cost_Appliances = l.WH_Cost_Appliances/rate,
e.CS_Cost_Appliances = l.CS_Cost_Appliances/rate,
e.Payment_Fees_Appliances = l.Payment_Fees_Appliances/rate,
e.net_sales_Photography = l.net_sales_Photography/rate,
e.costs_Photography = l.costs_Photography/rate,
e.inboundCosts_Photography = l.inboundCosts_Photography/rate,
e.cogs_Photography = l.cogs_Photography/rate,
e.ShippingFee_Photography = l.ShippingFee_Photography/rate,
e.ShippingCost_Photography = l.ShippingCost_Photography/rate,
e.WH_Cost_Photography = l.WH_Cost_Photography/rate,
e.CS_Cost_Photography = l.CS_Cost_Photography/rate,
e.Payment_Fees_Photography = l.Payment_Fees_Photography/rate,
e.net_sales_TV_Audio_Video = l.net_sales_TV_Audio_Video/rate,
e.costs_TV_Audio_Video = l.costs_TV_Audio_Video/rate,
e.inboundCosts_TV_Audio_Video = l.inboundCosts_TV_Audio_Video/rate,
e.cogs_TV_Audio_Video = l.cogs_TV_Audio_Video/rate,
e.ShippingFee_TV_Audio_Video = l.ShippingFee_TV_Audio_Video/rate,
e.ShippingCost_TV_Audio_Video = l.ShippingCost_TV_Audio_Video/rate,
e.WH_Cost_TV_Audio_Video = l.WH_Cost_TV_Audio_Video/rate,
e.CS_Cost_TV_Audio_Video = l.CS_Cost_TV_Audio_Video/rate,
e.Payment_Fees_TV_Audio_Video = l.Payment_Fees_TV_Audio_Video/rate,
e.net_sales_Books = l.net_sales_Books/rate,
e.costs_Books = l.costs_Books/rate,
e.inboundCosts_Books = l.inboundCosts_Books/rate,
e.cogs_Books = l.cogs_Books/rate,
e.ShippingFee_Books = l.ShippingFee_Books/rate,
e.ShippingCost_Books = l.ShippingCost_Books/rate,
e.WH_Cost_Books = l.WH_Cost_Books/rate,
e.CS_Cost_Books = l.CS_Cost_Books/rate,
e.Payment_Fees_Books = l.Payment_Fees_Books/rate,
e.net_sales_Videogames = l.net_sales_Videogames/rate,
e.costs_Videogames = l.costs_Videogames/rate,
e.inboundCosts_Videogames = l.inboundCosts_Videogames/rate,
e.cogs_Videogames = l.cogs_Videogames/rate,
e.ShippingFee_Videogames = l.ShippingFee_Videogames/rate,
e.ShippingCost_Videogames = l.ShippingCost_Videogames/rate,
e.WH_Cost_Videogames = l.WH_Cost_Videogames/rate,
e.CS_Cost_Videogames = l.CS_Cost_Videogames/rate,
e.Payment_Fees_Videogames = l.Payment_Fees_Videogames/rate,
e.net_sales_Fashion = l.net_sales_Fashion/rate,
e.costs_Fashion = l.costs_Fashion/rate,
e.inboundCosts_Fashion = l.inboundCosts_Fashion/rate,
e.cogs_Fashion = l.cogs_Fashion/rate,
e.ShippingFee_Fashion = l.ShippingFee_Fashion/rate,
e.ShippingCost_Fashion = l.ShippingCost_Fashion/rate,
e.WH_Cost_Fashion = l.WH_Cost_Fashion/rate,
e.CS_Cost_Fashion = l.CS_Cost_Fashion/rate,
e.Payment_Fees_Fashion = l.Payment_Fees_Fashion/rate,
e.net_sales_Health_Beauty = l.net_sales_Health_Beauty/rate,
e.costs_Health_Beauty = l.costs_Health_Beauty/rate,
e.inboundCosts_Health_Beauty = l.inboundCosts_Health_Beauty/rate,
e.cogs_Health_Beauty = l.cogs_Health_Beauty/rate,
e.ShippingFee_Health_Beauty = l.ShippingFee_Health_Beauty/rate,
e.ShippingCost_Health_Beauty = l.ShippingCost_Health_Beauty/rate,
e.WH_Cost_Health_Beauty = l.WH_Cost_Health_Beauty/rate,
e.CS_Cost_Health_Beauty = l.CS_Cost_Health_Beauty/rate,
e.Payment_Fees_Health_Beauty = l.Payment_Fees_Health_Beauty/rate,
e.net_sales_Cellphones = l.net_sales_Cellphones/rate,
e.costs_Cellphones = l.costs_Cellphones/rate,
e.inboundCosts_Cellphones = l.inboundCosts_Cellphones/rate,
e.cogs_Cellphones = l.cogs_Cellphones/rate,
e.ShippingFee_Cellphones = l.ShippingFee_Cellphones/rate,
e.ShippingCost_Cellphones = l.ShippingCost_Cellphones/rate,
e.WH_Cost_Cellphones = l.WH_Cost_Cellphones/rate,
e.CS_Cost_Cellphones = l.CS_Cost_Cellphones/rate,
e.Payment_Fees_Cellphones = l.Payment_Fees_Cellphones/rate,
e.net_sales_Home = l.net_sales_Home/rate,
e.costs_Home = l.costs_Home/rate,
e.inboundCosts_Home = l.inboundCosts_Home/rate,
e.cogs_Home = l.cogs_Home/rate,
e.ShippingFee_Home = l.ShippingFee_Home/rate,
e.ShippingCost_Home = l.ShippingCost_Home/rate,
e.WH_Cost_Home = l.WH_Cost_Home/rate,
e.CS_Cost_Home = l.CS_Cost_Home/rate,
e.Payment_Fees_Home = l.Payment_Fees_Home/rate,
e.net_sales_Kids_Babies = l.net_sales_Kids_Babies/rate,
e.costs_Kids_Babies = l.costs_Kids_Babies/rate,
e.inboundCosts_Kids_Babies = l.inboundCosts_Kids_Babies/rate,
e.cogs_Kids_Babies = l.cogs_Kids_Babies/rate,
e.ShippingFee_Kids_Babies = l.ShippingFee_Kids_Babies/rate,
e.ShippingCost_Kids_Babies = l.ShippingCost_Kids_Babies/rate,
e.WH_Cost_Kids_Babies = l.WH_Cost_Kids_Babies/rate,
e.CS_Cost_Kids_Babies = l.CS_Cost_Kids_Babies/rate,
e.Payment_Fees_Kids_Babies = l.Payment_Fees_Kids_Babies/rate,
e.net_sales_Sports = l.net_sales_Sports/rate,
e.costs_Sports = l.costs_Sports/rate,
e.inboundCosts_Sports = l.inboundCosts_Sports/rate,
e.cogs_Sports = l.cogs_Sports/rate,
e.ShippingFee_Sports = l.ShippingFee_Sports/rate,
e.ShippingCost_Sports = l.ShippingCost_Sports/rate,
e.WH_Cost_Sports = l.WH_Cost_Sports/rate,
e.CS_Cost_Sports = l.CS_Cost_Sports/rate,
e.Payment_Fees_Sports = l.Payment_Fees_Sports/rate,
e.GrossRev_Appliances = l.GrossRev_Appliances/rate,
e.GrossRev_Photography = l.GrossRev_Photography/rate,
e.GrossRev_TV_Audio_Video = l.GrossRev_TV_Audio_Video/rate,
e.GrossRev_Books = l.GrossRev_Books/rate,
e.GrossRev_Videogames = l.GrossRev_Videogames/rate,
e.GrossRev_Fashion = l.GrossRev_Fashion/rate,
e.GrossRev_Health_Beauty = l.GrossRev_Health_Beauty/rate,
e.GrossRev_Cellphones = l.GrossRev_Cellphones/rate,
e.GrossRev_Computing = l.GrossRev_Computing/rate,
e.GrossRev_Home = l.GrossRev_Home/rate,
e.GrossRev_Kids_Babies = l.GrossRev_Kids_Babies/rate,
e.GrossRev_Sports = l.GrossRev_Sports/rate;

update tbl_dailyReport_EUR e join 
tbl_dailyReport_local l on e.date = l.date
set e.Visits = l.Visits,
e.UniqueVisits = l.UniqueVisits,
e.MobileVisits = l.MobileVisits,
e.Visits = l.Visits,
e.GrossOrders = l.GrossOrders,
e.CancelOrders = l.CancelOrders,
e.PendingOrders = l.PendingOrders,
e.ReturnedOrders = l.ReturnedOrders,
e.NetOrders = l.NetOrders,
e.PromisedDeliveries = l.PromisedDeliveries,
e.DeliveriesOnTime = l.DeliveriesOnTime,
e.AverageItemsPerBasket = l.AverageItemsPerBasket,
e.WHIdleCapacity = l.WHIdleCapacity,
e.newCustomers = l.newCustomers,
e.itemsInStock = l.itemsInStock,
e.brandsInStock = l.brandsInStock;




END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `daily_report_local`()
BEGIN

truncate tbl_dailyReport_local;
insert into tbl_dailyReport_local (date) select dt from calendar where dt>'2012-07-31' and dt<now();

update tbl_dailyReport_local
set year = year(date) , month = month(date) , yrmonth = concat(year,if(month<10,concat(0,month),month));

#Visits
UPDATE tbl_dailyReport_local t INNER JOIN ga_daily_visits g 
ON t.date = g.date
SET t.Visits = g.visits;

#Visits
UPDATE tbl_dailyReport_local t INNER JOIN ga_daily_visits g 
ON t.date = g.date
SET t.UniqueVisits = g.unique_visits;

#Mobile Visits
UPDATE tbl_dailyReport_local t INNER JOIN ga_daily_visits g 
ON t.date = g.date
SET t.MobileVisits = if(g.mobile_visits is null, 0, g.mobile_visits);

#Gross
update tbl_dailyReport_local join 
(select date,sum(`unit_price`) as grossrev, count(distinct `order_nr`,`OAC`,`RETURNED`,`PENDING`) as grossorders, 
	sum(`unit_price_after_vat`) as GrossSalesPreCancel
            from `tbl_order_detail` where   (`OBC` = '1')
            group by `date`) AS tbl_gross on tbl_gross.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.GrossRev=tbl_gross.grossrev , tbl_dailyReport_local.GrossOrders=tbl_gross.grossorders, 
tbl_dailyReport_local.GrossSalesPreCancel=tbl_gross.GrossSalesPreCancel;

#Cancel
update tbl_dailyReport_local join 
(select date,sum(`unit_price_after_vat`) as cancelrev, count(distinct `order_nr`) as cancelorders
from `tbl_order_detail` 
where   (`CANCEL` = '1')
group by `date`) AS tbl_cancel on tbl_cancel.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.Cancellations=tbl_cancel.cancelrev , tbl_dailyReport_local.CancelOrders=tbl_cancel.cancelorders;


#Pending
update tbl_dailyReport_local join 
(select date,sum(if(`unit_price_after_vat` is null, 0, unit_price_after_vat))  as pendingrev, if( count(distinct `order_nr`) is null, 0,  count(distinct `order_nr`)) as pendingorders
from `tbl_order_detail` 
where   (`PENDING` = '1')
group by `date`) AS tbl_pending on tbl_pending.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.PendingCOP=tbl_pending.pendingrev , tbl_dailyReport_local.PendingOrders=tbl_pending.pendingorders;

#cogs_gross_post_cancel
update tbl_dailyReport_local join 
(select date,(sum(`costo_after_vat`) + sum(`delivery_cost_supplier`)) as cogs_gross_post_cancel
from `tbl_order_detail` 
where   (`OAC` = '1')
group by `date`) AS tbl_cogs_post_cancel on tbl_cogs_post_cancel.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.cogs_gross_post_cancel=tbl_cogs_post_cancel.cogs_gross_post_cancel;

#Returned
update tbl_dailyReport_local join 
(select date,sum(`unit_price_after_vat`) as returnrev, count(distinct `order_nr`) as returnorders
from `tbl_order_detail` 
where   (`RETURNED` = '1')
group by `date`) AS tbl_returned on tbl_returned.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.ReturnedCOP=tbl_returned.returnrev , tbl_dailyReport_local.ReturnedOrders=tbl_returned.returnorders;

#Net
update tbl_dailyReport_local join 
(select date,sum(`unit_price_after_vat`) as NetSalesPreVoucher, SUM(coupon_money_after_vat) as VoucherMktCost,
SUM(`actual_paid_price_after_tax`) as NetSales,count(distinct `order_nr`) as NetOrders,
 (sum(`costo_after_vat`) + sum(`delivery_cost_supplier`))as cogs_on_net_sales,
count(`order_nr`)/count(distinct `order_nr`) as AverageItemsPerBasket,
count(distinct `CustID`) as customers,
SUM(`shipping_fee`) as ShippingFee, 
SUM(`shipping_cost`)as ShippingCost, 
SUM(`payment_cost`) as PaymentCost
from `tbl_order_detail` 
where   (`RETURNED` = '0') AND (`OAC` = '1')
group by `date`) AS tbl_net on tbl_net.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.NetSalesPreVoucher=tbl_net.NetSalesPreVoucher ,
tbl_dailyReport_local.VoucherMktCost=tbl_net.VoucherMktCost,
tbl_dailyReport_local.NetSales=tbl_net.NetSales,
tbl_dailyReport_local.NetOrders=tbl_net.NetOrders,
tbl_dailyReport_local.cogs_on_net_sales=tbl_net.cogs_on_net_sales,
tbl_dailyReport_local.AverageItemsPerBasket=tbl_net.AverageItemsPerBasket,
tbl_dailyReport_local.customers=tbl_net.customers,
tbl_dailyReport_local.ShippingFee=tbl_net.ShippingFee/1.16,
tbl_dailyReport_local.ShippingCost=tbl_net.ShippingCost,
tbl_dailyReport_local.PaymentCost=tbl_net.PaymentCost;

#Marketing spend
UPDATE tbl_dailyReport_local t
INNER JOIN (select date, sum(adCost) adCost from ga_cost_campaign group by date) g 
ON t.date = g.date SET t.marketing_spend = adCost;

# New Customers
update tbl_dailyReport_local join  
(select firstOrder as date,COUNT(*) as newCustomers from `view_cohort` 
group by firstOrder) as tbl_nc on tbl_nc.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.newcustomers=tbl_nc.newCustomers,
tbl_dailyReport_local.ShippingCostPerOrder=tbl_dailyReport_local.ShippingCost/tbl_nc.newCustomers,
tbl_dailyReport_local.PaymentCostPerOrder=tbl_dailyReport_local.PaymentCost/tbl_nc.newCustomers;

#PaidChannels
 update production.tbl_dailyReport_local join 
(select date,sum(actual_paid_price_after_tax) as net_sales_paid_channel,
sum(coupon_money_after_vat) as voucher_paid_channel
from production.tbl_order_detail 
where  (oac = '1') and (returned = '0') 
and   ((source_medium like '%price comparison%')
  or (source_medium like '%socialmediaads%')
  or (source_medium like '%cpc%')
  or (source_medium like '%sociomantic%'))
group by date) as tbl_paid on tbl_paid.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_paid_channel=tbl_paid.net_sales_paid_channel
 , tbl_dailyReport_local.voucher_paid_channel=tbl_paid.voucher_paid_channel;

 
#Categorías
#Electrodomésticos
update tbl_dailyReport_local join 
(select date,sum(`actual_paid_price_after_tax`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.16) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` = 'electrodomésticos')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Appliances=tbl_cat.netsales , tbl_dailyReport_local.cogs_Appliances=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Appliances=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Appliances=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Appliances=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Appliances=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Appliances=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Appliances = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Appliances = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Appliances= tbl_cat.grossRev;

#Cámaras y Fotografía
update tbl_dailyReport_local join 
(select date,sum(`actual_paid_price_after_tax`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.16) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` = 'cámaras y fotografía')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Photography=tbl_cat.netsales , tbl_dailyReport_local.cogs_Photography=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Photography=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Photography=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Photography=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Photography=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Photography=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Photography = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Photography = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Photography= tbl_cat.grossRev ;


#TV, Video y Audio
update tbl_dailyReport_local join 
(select date,sum(`actual_paid_price_after_tax`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.16) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` ='tv, audio y video')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_TV_Audio_Video=tbl_cat.netsales , tbl_dailyReport_local.cogs_TV_Audio_Video=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_TV_Audio_Video=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_TV_Audio_Video=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_TV_Audio_Video=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_TV_Audio_Video=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_TV_Audio_Video=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_TV_Audio_Video = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_TV_Audio_Video = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_TV_Audio_Video= tbl_cat.grossRev ;

#Libros
update tbl_dailyReport_local join 
(select date,sum(`actual_paid_price_after_tax`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.16) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` like 'libros%')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Books=tbl_cat.netsales , tbl_dailyReport_local.cogs_Books=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Books=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Books=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Books=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Books=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Books=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Books = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Books = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Books= tbl_cat.grossRev  ;
#Videojuegos
update tbl_dailyReport_local join 
(select date,sum(`actual_paid_price_after_tax`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.16) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` = 'videojuegos')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Videogames=tbl_cat.netsales , tbl_dailyReport_local.cogs_Videogames=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Videogames=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Videogames=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Videogames=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Videogames=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Videogames=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Videogames = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Videogames = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Videogames= tbl_cat.grossRev ;

#Fashion
update tbl_dailyReport_local join 
(select date,sum(`actual_paid_price_after_tax`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.16) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 = 'ropa, calzado y accesorios')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Fashion=tbl_cat.netsales , tbl_dailyReport_local.cogs_Fashion=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Fashion=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Fashion=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Fashion=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Fashion=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Fashion=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Fashion = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Fashion = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Fashion= tbl_cat.grossRev  ;


#Cuidado Personal
update tbl_dailyReport_local join 
(select date,sum(`actual_paid_price_after_tax`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.16) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 like '%cuidado personal%')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Health_Beauty=tbl_cat.netsales , tbl_dailyReport_local.cogs_Health_Beauty=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Health_Beauty=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Health_Beauty=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Health_Beauty=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Health_Beauty=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Health_Beauty=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Health_Beauty = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Health_Beauty = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Health_Beauty= tbl_cat.grossRev  ;


#Teléfonos y GPS
update tbl_dailyReport_local join 
(select date,sum(`actual_paid_price_after_tax`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.16) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND( n1 like 'celulares%')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Cellphones=tbl_cat.netsales , tbl_dailyReport_local.cogs_Cellphones=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Cellphones=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Cellphones=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Cellphones=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Cellphones=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Cellphones=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Cellphones = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Cellphones = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Cellphones= tbl_cat.grossRev   ;


 
#Computadores y Tablets    
update tbl_dailyReport_local join 
(select date,sum(`actual_paid_price_after_tax`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.16) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 like 'compu%')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Computing=tbl_cat.netsales , tbl_dailyReport_local.cogs_Computing=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Computing=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Computing=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Computing=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Computing=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Computing=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Computing = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Computing = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Computing = tbl_cat.grossRev  ;


#Home y Muebles   
update tbl_dailyReport_local join 
(select date,sum(`actual_paid_price_after_tax`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.16) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 = 'hogar' or n1 like '%muebles')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Home=tbl_cat.netsales , 
tbl_dailyReport_local.cogs_Home=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Home=tbl_cat.shipping_fee,
 tbl_dailyReport_local.ShippingCost_Home=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Home=tbl_cat.WH_cost, 
tbl_dailyReport_local.CS_Cost_Home=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Home=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Home = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Home = tbl_cat.inboundCosts,
tbl_dailyReport_local.GrossRev_Home = tbl_cat.grossRev ;


#Juguetes, Niños y Bebés 
 update tbl_dailyReport_local join 
(select date,sum(`actual_paid_price_after_tax`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.16) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(n1 ='juguetes, niños y bebés')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Kids_Babies=tbl_cat.netsales , tbl_dailyReport_local.cogs_Kids_Babies=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Kids_Babies=tbl_cat.shipping_fee, tbl_dailyReport_local.ShippingCost_Kids_Babies=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Kids_Babies=tbl_cat.WH_cost, tbl_dailyReport_local.CS_Cost_Kids_Babies=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Kids_Babies=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Kids_Babies = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Kids_Babies = tbl_cat.inboundCosts , 
tbl_dailyReport_local.GrossRev_Kids_Babies = tbl_cat.grossRev ;

#Deportes
 update tbl_dailyReport_local join 
(select date,sum(`actual_paid_price_after_tax`) as netsales,
sum(`unit_price_after_vat`) as grossRev,
sum(`costo_after_vat`) as costs, 
 sum(`delivery_cost_supplier`) as inboundCosts,
 (sum(`costo_after_vat`)+sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.16) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` ='Deportes')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport_local.date
set tbl_dailyReport_local.net_sales_Sports=tbl_cat.netsales , 
tbl_dailyReport_local.cogs_Sports=tbl_cat.cogs_on_net_sales,
tbl_dailyReport_local.ShippingFee_Sports=tbl_cat.shipping_fee, 
tbl_dailyReport_local.ShippingCost_Sports=tbl_cat.shipping_cost,
tbl_dailyReport_local.WH_Cost_Sports=tbl_cat.WH_cost, 
tbl_dailyReport_local.CS_Cost_Sports=tbl_cat.CS_cost,
tbl_dailyReport_local.Payment_Fees_Sports=tbl_cat.payment_fee,
tbl_dailyReport_local.costs_Sports = tbl_cat.costs,
tbl_dailyReport_local.inboundCosts_Sports = tbl_cat.inboundCosts , 
tbl_dailyReport_local.GrossRev_Sports = tbl_cat.grossRev ;



END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `daily_routine`()
begin
select  'daily routine: start',now();

select  'daily routine: tbl_catalog_product start',now();
#table catalog_product_v2
drop table if exists production.simple_variation;
create table production.simple_variation as (select * from production.simple_variation_view);
alter table production.simple_variation add primary key (fk_catalog_simple) ;

#tbl_catalog_stock
truncate production.tbl_catalog_product_stock;
insert into production.tbl_catalog_product_stock(fk_catalog_simple,sku,reservedbob,stockbob,availablebob)
select catalog_simple.id_catalog_simple,catalog_simple.sku,
if(sum(is_reserved) is null, 0, sum(is_reserved))  as reservedbob ,catalog_stock.quantity as availablebob,
catalog_stock.quantity-if(sum(is_reserved) is null, 0, sum(is_reserved)) as disponible
from 
(bob_live_mx.catalog_simple join bob_live_mx.catalog_stock on catalog_simple.id_catalog_simple=catalog_stock.fk_catalog_simple) left join bob_live_mx.sales_order_item
 on sales_order_item.sku=catalog_simple.sku
group by catalog_simple.sku
order by catalog_stock.quantity desc;
drop table if exists tbl_catalog_product_v3;
create table production.tbl_catalog_product_v3 as (select * from production.catalog_product);
update production.tbl_catalog_product_v3  set visible='no' where cat1 is null;

#actualiza lo recientamente reservado, desde el 01 de enero
update production.tbl_catalog_product_stock inner join (select tbl_catalog_product_stock.fk_catalog_simple,
if(sum(is_reserved) is null, 0, sum(is_reserved))  as recently_reserved
from 
production.tbl_catalog_product_stock inner join bob_live_mx.sales_order_item on tbl_catalog_product_stock.sku=sales_order_item.sku
where date(sales_order_item.created_at)>'2013-01-01' group by fk_catalog_simple) as a 
on  tbl_catalog_product_stock.fk_catalog_simple = a.fk_catalog_simple
set tbl_catalog_product_stock.recently_reservedbob = a.recently_reserved;


alter table production.tbl_catalog_product_v3 add primary key (sku) ;

drop table if exists production.tbl_catalog_product_v2;
alter table production.tbl_catalog_product_v3 rename to  production.tbl_catalog_product_v2 ;

alter table production.tbl_catalog_product_v2 
add index (sku_config ) ;

select  'daily routine: tbl_catalog_product ok',now();

#special cost
#call special_cost();
select  'daily routine: tbl_order_detail start',now();

call production.update_tbl_order_detail();

select  'daily routine: tbl_order_detail end',now();

#ventas netas mes corrido
replace into production.tbl_sales_growth_mom_cat
select date(now() - interval 1 day) date, tab.n1, ventas_netas_t, ventas_netas_t_1, 
cast(((ventas_netas_t/ventas_netas_t_1)-1) as decimal(30,10)) mom from (
select n1,sum(paid_price_after_vat) + sum(if(shipping_fee_after_vat is null, shipping_fee_after_vat,0)) ventas_netas_t
 from production.tbl_order_detail where date>= date(now() - interval day(now()) day) and date <= now() 
 and month(date) = month(now() - interval 1 day) and oac = 1 and returned = 0 group by n1) tab inner join 
(select n1,sum(paid_price_after_vat) + sum(if(shipping_fee_after_vat is null, shipping_fee_after_vat,0)) ventas_netas_t_1
 from production.tbl_order_detail where date<= date(now() - interval 1 month)
 and date >= date(now() - interval 1 month - interval day(now()) day ) 
and month(date) = month(now() - interval 1 day) - 1
 and oac = 1 and returned = 0 group by n1) tab2
on tab.n1 = tab2.n1;

replace into production.tbl_sales_growth_mom_buyer
select date(now() - interval 1 day) date, tab.buyer, ventas_netas_t, ventas_netas_t_1, 
cast(((ventas_netas_t/ventas_netas_t_1)-1) as decimal(30,10)) mom from (
select buyer,sum(paid_price_after_vat) + sum(if(shipping_fee_after_vat is null, shipping_fee_after_vat,0)) ventas_netas_t
 from production.tbl_order_detail where date>= date(now() - interval day(now()) day) and date <= now() 
 and month(date) = month(now() - interval 1 day) and oac = 1 and returned = 0 group by buyer) tab inner join 
(select buyer,sum(paid_price_after_vat) + sum(if(shipping_fee_after_vat is null, shipping_fee_after_vat,0)) ventas_netas_t_1
 from production.tbl_order_detail where date<= date(now() - interval 1 month)
 and date >= date(now() - interval 1 month - interval day(now()) day ) 
and month(date) =  month(now() - interval 1 day) - 1
 and oac = 1 and returned = 0 group by buyer) tab2
on tab.buyer = tab2.buyer;

select  'bireporting: start',now();

#sales cube
-- call bireporting.etl_fact_sales_comercial();

select  'bireporting: ok',now();


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `employee_vouchers`()
begin

delete from production.employee_vouchers;

delete from production.employee_vouchers_cohort;

insert into production.employee_vouchers select custid, date, yrmonth, order_nr, sum(actual_paid_price) as net_revenue, avg(actual_paid_price) as avg_ticket, avg(nr_items) as avg_items, avg(ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as avg_PC2, coupon_code, sum(new_customers) from production.tbl_order_detail, production.employee_info where oac=1 and (new_vouchers=coupon_code or old_vouchers=coupon_code) group by order_nr order by date desc;

create table production.temporary_cohort(
date date,
yrmonth int,
custid int,
first_name varchar(255),
last_name varchar(255),
order_nr varchar(255),
first_total_purcharse varchar(255),
avg_ticket float,
avg_items float,
avg_PC2 float,
coupon_code varchar(255),
oac_transactions_cohort int,
avg_ticket_cohort float,
avg_items_cohort float,
avg_PC2_cohort float,
new_customers float,
status varchar(255)
);

insert into production.temporary_cohort(yrmonth, custid, date, order_nr, avg_ticket, avg_items, avg_PC2, coupon_code, new_customers) select yrmonth, custid, date, order_nr, avg_ticket, avg_items, avg_PC2, coupon_code, new_customers from production.employee_vouchers;

update production.temporary_cohort t inner join bob_live_mx.customer c on t.custid=c.id_customer set t.first_name=c.first_name, t.last_name=c.last_name;
 
update production.temporary_cohort b set oac_transactions_cohort = (select count(distinct t.order_nr) from production.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date > adddate(b.date, interval 1 minute) group by b.order_nr, b.custid order by b.custid);

update production.temporary_cohort b set avg_ticket_cohort = (select avg(avg) from (select sum(actual_paid_price) as avg, t.custid, date from production.tbl_order_detail t where t.oac=1 group by t.order_nr, t.custid order by t.custid)t where t.date > adddate(b.date, interval 1 minute) and t.custid=b.custid);

update production.temporary_cohort b set avg_items_cohort = (select avg(avg) from (select sum(nr_items) as avg, t.custid, date from production.tbl_order_detail t where t.oac=1 group by t.order_nr, t.custid order by t.custid)t where t.date > adddate(b.date, interval 1 minute) and t.custid=b.custid);

update production.temporary_cohort b set avg_PC2_cohort = (select avg(avg) from (select sum(ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as avg, t.custid, date from production.tbl_order_detail t where t.oac=1 group by t.order_nr, t.custid order by t.custid)t where t.date > adddate(b.date, interval 1 minute) and t.custid=b.custid);

update production.temporary_cohort b set first_total_purcharse = (select net_revenue from production.employee_vouchers t where t.order_nr=b.order_nr);

update production.temporary_cohort b inner join production.employee_info i on b.first_name=i.first_name and b.last_name=i.last_name set status = 'Employee';

insert into production.employee_vouchers_cohort(date, yrmonth, transactions, avg_ticket, avg_items, avg_PC2, new_customers, status, coupon_code, first_total_purcharse) select date, yrmonth, oac_transactions_cohort, avg_ticket_cohort, avg_items_cohort, avg_PC2_cohort, case when new_customers is not null then 1 else 0 end, case when status is null then 'Friend/Other' else status end, coupon_code, first_total_purcharse from production.temporary_cohort;

update production.employee_vouchers_cohort set transactions = 0 where transactions is null;

update production.employee_vouchers_cohort e inner join production.employee_info i on e.coupon_code=i.old_vouchers set e.coupon_code = i.new_vouchers where i.new_vouchers is not null;

drop table production.temporary_cohort;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `extra_queries_osri`()
begin

-- 5_M1_Supplier
/*UPDATE (bob_live_mx.catalog_config INNER JOIN bob_live_mx.supplier ON catalog_config.fk_supplier = supplier.id_supplier)
 INNER JOIN production.tbl_order_detail ON catalog_config.sku = tbl_order_detail.sku_config
 SET tbl_order_detail.proveedor = supplier.name;*/
 
 /*-- M1_buyers
 UPDATE ((production.tbl_order_detail INNER JOIN bob_live_mx.catalog_config ON tbl_order_detail.sku_config = catalog_config.sku) 
 INNER JOIN bob_live_mx.catalog_attribute_option_global_head_buyer_name 
 ON catalog_config.fk_catalog_attribute_option_global_head_buyer_name 
 = catalog_attribute_option_global_head_buyer_name.id_catalog_attribute_option_global_head_buyer_name)
 INNER JOIN bob_live_mx.catalog_attribute_option_global_buyer_name ON catalog_config.fk_catalog_attribute_option_global_buyer_name
 = catalog_attribute_option_global_buyer_name.id_catalog_attribute_option_global_buyer_name 
 SET tbl_order_detail.Buyer = catalog_attribute_option_global_buyer_name.name, 
 tbl_order_detail.HeadBuyer = catalog_attribute_option_global_head_buyer_name.name;*/
 
-- 1_M1_CouponMoneyValue+ShippingCost
UPDATE production.tbl_order_detail 
SET tbl_order_detail.coupon_money_value = tbl_order_detail.coupon_money_value+tbl_order_detail.shipping_cost
WHERE (((tbl_order_detail.coupon_code)="MKT0xfgVK" Or (tbl_order_detail.coupon_code)="MKT1eDvI7"));

-- 2 M1_ShippingCost_0
UPDATE production.tbl_order_detail SET tbl_order_detail.shipping_cost = 0
WHERE (((tbl_order_detail.coupon_code)="MKT0xfgVK" Or (tbl_order_detail.coupon_code)="MKT1eDvI7"));

-- M1_U_voucherMKT
UPDATE production.tbl_order_detail
 SET tbl_order_detail.code_prefix = "MKT" where coupon_code in (
'ANIVERSARIO', 'banamex100', 'descuento20', 'EscalaLINIO4', 'Intercam', 'LINIOMADRID', 'MetrobusLINIO21', 'MetrobusLINIO23', 'MetroLinio01','PAMPAMX', 'regalo200', 'regalolinio200','SMS','SMS1');

-- 4_M1_MKTVoucherFin
UPDATE production.tbl_order_detail 
SET tbl_order_detail.Actual_Paid_price = tbl_order_detail.Actual_Paid_price+tbl_order_detail.coupon_money_value,
 tbl_order_detail.Actual_Paid_price_after_tax = tbl_order_detail.Actual_Paid_price_after_tax+tbl_order_detail.coupon_money_after_vat
WHERE ((tbl_order_detail.code_prefix)="MKT") 
AND (tbl_order_detail.coupon_code) Not In ("MKT7tavt8","MKT0uJXni","MKT0pTUbn"," MKT1ORfvU","MKT1Z54Lh","MKTf4A6Sk","MKT0xfgVK","MKT1eDvI7",
"MKTxmsjvs","MKTB0TZfz","MKT3TcOf8","MKT063HRE","MKTqqU8KR");

-- 11_M1_LNVoucher
UPDATE production.tbl_order_detail SET tbl_order_detail.Actual_Paid_price = tbl_order_detail.Actual_Paid_price+tbl_order_detail.coupon_money_value,
 tbl_order_detail.Actual_Paid_price_after_tax = tbl_order_detail.Actual_Paid_price_after_tax+tbl_order_detail.coupon_money_after_vat
WHERE (((tbl_order_detail.code_prefix)="NL"));

-- M1_CACVoucher
UPDATE production.tbl_order_detail 
SET tbl_order_detail.Actual_Paid_price = tbl_order_detail.Actual_Paid_price+tbl_order_detail.coupon_money_value, 
tbl_order_detail.Actual_Paid_price_after_tax = tbl_order_detail.Actual_Paid_price_after_tax+tbl_order_detail.coupon_money_after_vat
WHERE (((tbl_order_detail.code_prefix)="CAC"));

-- M1_ShipFee_CAC
UPDATE production.tbl_order_detail SET tbl_order_detail.shipping_fee_after_vat = 55
WHERE (((tbl_order_detail.coupon_code) In ("CAC0xY3Iz","CACs4tyXP")));

-- M1_CostoCorporativo
UPDATE production.tbl_order_detail INNER JOIN production.eq_ordenes_corporativas ON tbl_order_detail.order_nr = eq_ordenes_corporativas.Order 
SET tbl_order_detail.shipping_fee_after_vat = eq_ordenes_corporativas.ShippingFee, 
tbl_order_detail.wh = eq_ordenes_corporativas.WHcost, 
tbl_order_detail.cs = eq_ordenes_corporativas.CScost, 
tbl_order_detail.shipping_cost = eq_ordenes_corporativas.ShippingCost, 
tbl_order_detail.payment_cost = eq_ordenes_corporativas.PaymentFee;

-- 7_M1_Costo0Revistas
UPDATE production.tbl_order_detail INNER JOIN production.eq_costos_revistas ON tbl_order_detail.sku = eq_costos_revistas.`sku simple`
 SET tbl_order_detail.shipping_cost = 0, tbl_order_detail.wh = 0, tbl_order_detail.delivery_cost_supplier = 0;

-- 8_M1_Exported
UPDATE production.tbl_order_detail SET tbl_order_detail.Exported = 1
WHERE (((tbl_order_detail.Status_item)="Exported"));

-- 10_M1_DateExported
UPDATE (bob_live_mx.sales_order_item_status_history INNER JOIN bob_live_mx.sales_order_item_status 
ON sales_order_item_status_history.fk_sales_order_item_status = sales_order_item_status.id_sales_order_item_status)
 INNER JOIN (bob_live_mx.sales_order_item INNER JOIN production.tbl_order_detail ON sales_order_item.fk_sales_order = tbl_order_detail.id_sales_order)
 ON sales_order_item_status_history.fk_sales_order_item = sales_order_item.id_sales_order_item 
 SET tbl_order_detail.DateExported = sales_order_item_status_history.created_at
WHERE (((tbl_order_detail.Exported)=1));

-- 12_M1_email
UPDATE bob_live_mx.sales_order INNER JOIN production.tbl_order_detail ON sales_order.id_sales_order = tbl_order_detail.orderid SET tbl_order_detail.customerEmail = sales_order.customer_email;

-- M1_binocularTelescopio
UPDATE production.tbl_order_detail SET tbl_order_detail.category_bp = "1,2 Photography"
WHERE (((tbl_order_detail.sku)="CE320EL58OPLLMX-30801" Or (tbl_order_detail.sku)="CE320EL22KWRLMX-74175" Or (tbl_order_detail.sku)="CE320EL25KWOLMX-74172"));

-- M1_Aniversario
UPDATE production.tbl_order_detail INNER JOIN production.category_bob_bp ON tbl_order_detail.Cat2 = category_bob_bp.cat1 SET tbl_order_detail.category_bp = category_bob_bp.category_bp
WHERE (((tbl_order_detail.n1)="Aniversario"));

-- M1_Aniversario_H&B
UPDATE production.tbl_order_detail SET tbl_order_detail.category_bp = "4,1 Health & Beauty"
WHERE (((tbl_order_detail.n1)="Aniversario") AND ((tbl_order_detail.sku)="CA260HB29MXILMX-52206"));

-- M1_COGS_PC
UPDATE production.tbl_order_detail SET tbl_order_detail.COGS = ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0), tbl_order_detail.PC1 = ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0), tbl_order_detail.PC2 = ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0);

-- M1_PC2Adjust
UPDATE production.tbl_order_detail SET tbl_order_detail.PC2Adjust = ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0);

-- M1_PC2Adjust_Paso2
UPDATE production.tbl_order_detail SET tbl_order_detail.PC2Adjust = tbl_order_detail.PC2
WHERE (tbl_order_detail.date)<'2013-04-18';

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `extra_query`()
begin

delete production.o.* from production.tbl_order_detail o 
inner join bob_live_mx.customer c on o.custid = c.id_customer 
where o.order_nr in ('200064328', '200011128', '200091128', '200047828', 
'200066579', '200091679', '200055526', '200001711', '200002361', '200059986', 
'200036442', '200003711', '200002781', '200008781', '200001751', '200003751', 
'200002551', '200001281', '200008911', '200046249', '200004511', '200003511', 
'200074126', '200066366', '200005611', '200019589', '200028986', '200034949', 
'200019456', '200013576', '200005841', '200024636', '200013236', '200032579', 
'200089858', '200039858', '200098858', '200078858', '200038858', '200047858') 
or c.first_name like 'test%' or c.first_name regexp '[[:<:]]test[[:>:]]*'
or c.last_name regexp '[[:<:]]test[[:>:]]*' 
or c.last_name regexp '[[:<:]]prueba[[:>:]]*'
 or c.first_name regexp '[[:<:]]prueba[[:>:]]*';

#Cupones to Zero + VISA
update tbl_order_detail
set coupon_money_value = 0, coupon_money_after_vat = 0
where code_prefix in ('CCE','PRCcre','OPScre','DEP');

update tbl_order_detail t inner join bob_live_mx.sales_order 
on orderID = id_sales_order 
inner join bob_live_mx.sales_order_item s 
on fk_sales_order = id_sales_order and s.sku = t.sku
set t.coupon_money_value = s.coupon_money_value,
 t.coupon_money_after_vat = s.coupon_money_value/((100 + s.tax_percent)/100)
where code_prefix in ('VISA','VISA3');

update tbl_order_detail
set coupon_money_value = coupon_money_value-unit_price*0.2,
coupon_money_after_vat = coupon_money_after_vat-unit_price_after_vat*0.2
where code_prefix in ('VISA');

update tbl_order_detail
set coupon_money_value = coupon_money_value-unit_price*0.1,
coupon_money_after_vat = coupon_money_after_vat-unit_price_after_vat*0.1
where code_prefix in ('VISA3');

#Subsidio Visa
UPDATE (tbl_order_detail INNER JOIN eq_visa_promotion
 ON (tbl_order_detail.coupon_code = eq_visa_promotion.coupon_code)
 AND (tbl_order_detail.sku_config = eq_visa_promotion.sku_config))
 INNER JOIN bob_live_mx.sales_order_item ON (tbl_order_detail.sku = bob_live_mx.sales_order_item.sku)
 AND (tbl_order_detail.orderID = bob_live_mx.sales_order_item.fk_sales_order)
 SET tbl_order_detail.Actual_Paid_price = tbl_order_detail.paid_price+eq_visa_promotion.subsidio_visa, 
 tbl_order_detail.Actual_Paid_price_after_tax = tbl_order_detail.Paid_price_after_vat +eq_visa_promotion.subsidio_visa_after_vat,
 tbl_order_detail.coupon_money_value = bob_live_mx.sales_order_item.coupon_money_value
-eq_visa_promotion.subsidio_visa, 
 tbl_order_detail.coupon_money_after_vat = 
(bob_live_mx.sales_order_item.coupon_money_value/((100+bob_live_mx.sales_order_item.tax_percent)/100))-
 eq_visa_promotion.subsidio_visa_after_vat,
 tbl_order_detail.shipping_fee = sales_order_item.shipping_amount+
 (eq_visa_promotion.shipping_fee_charged_visa_after_vat/tbl_order_detail.nr_items);
 
 #Wrong vouchers used by CC
UPDATE tbl_order_detail INNER JOIN eq_wrong_vouchers_used_by_cc
 ON (tbl_order_detail.sku = eq_wrong_vouchers_used_by_cc.sku)
 AND (tbl_order_detail.order_nr = eq_wrong_vouchers_used_by_cc.order_nr) 
 SET tbl_order_detail.Actual_Paid_price = eq_wrong_vouchers_used_by_cc.Actual_paid_price, 
 tbl_order_detail.Actual_Paid_price_after_tax = eq_wrong_vouchers_used_by_cc.Actual_paid_price_after_vat,
 tbl_order_detail.coupon_money_value = eq_wrong_vouchers_used_by_cc.Coupon_money_value,
 tbl_order_detail.coupon_money_after_vat = eq_wrong_vouchers_used_by_cc.After_vat_coupon;
 
 #Coupon money value + shipping cost 
 update tbl_order_detail t inner join bob_live_mx.sales_order 
on orderID = id_sales_order 
inner join bob_live_mx.sales_order_item s 
on fk_sales_order = id_sales_order and s.sku = t.sku
set t.coupon_money_value = s.coupon_money_value,
 t.coupon_money_after_vat = s.coupon_money_value/((100 + s.tax_percent)/100)
where t.coupon_code in ('MKT0xfgVK','MKT1eDvI7');

  UPDATE tbl_order_detail 
 SET tbl_order_detail.coupon_money_value = tbl_order_detail.coupon_money_value+tbl_order_detail.shipping_cost,
 tbl_order_detail.coupon_money_after_vat = (tbl_order_detail.coupon_money_value+tbl_order_detail.shipping_cost)/((100 + tbl_order_detail.tax_percent)/100)
WHERE ((coupon_code in ('MKT0xfgVK','MKT1eDvI7')));

UPDATE tbl_order_detail SET tbl_order_detail.shipping_cost = 0
WHERE (coupon_code in ('MKT0xfgVK','MKT1eDvI7'));

#Paypal Vouchers
UPDATE tbl_order_detail 
SET tbl_order_detail.actual_paid_price = tbl_order_detail.unit_price, 
tbl_order_detail.actual_paid_price_after_tax = tbl_order_detail.unit_price_after_vat,
 tbl_order_detail.coupon_money_value = 0, tbl_order_detail.coupon_money_after_vat = 0, 
 tbl_order_detail.payment_cost = 0
WHERE (((tbl_order_detail.coupon_code)='COM1Tw3Bz'
 Or (tbl_order_detail.coupon_code)='COM1QuGJR'
 Or (tbl_order_detail.coupon_code)='COMq517VW' 
 Or (tbl_order_detail.coupon_code)='COMc4jiTb' 
 Or (tbl_order_detail.coupon_code)='COM57OMUy'));

 #Non-MKT Voucher (CCemp)
  UPDATE tbl_order_detail 
 SET tbl_order_detail.coupon_money_value = tbl_order_detail.coupon_money_value+tbl_order_detail.shipping_cost,
 tbl_order_detail.coupon_money_after_vat = tbl_order_detail.coupon_money_value/((100 + tbl_order_detail.tax_percent)/100)
WHERE ((code_prefix in ('CCemp'))); 

UPDATE tbl_order_detail
SET tbl_order_detail.Actual_Paid_price = tbl_order_detail.paid_price+tbl_order_detail.coupon_money_value, 
tbl_order_detail.Actual_Paid_price_after_tax = tbl_order_detail.Paid_price_after_vat+tbl_order_detail.coupon_money_after_vat, 
tbl_order_detail.coupon_money_value = 0, tbl_order_detail.coupon_money_after_vat = 0 
where tbl_order_detail.code_prefix='CCemp';

#Adjust price nomina sales
UPDATE tbl_order_detail INNER JOIN eq_adjust_price_nomina_sales
 ON (tbl_order_detail.sku_config = eq_adjust_price_nomina_sales.sku_config)
 AND (tbl_order_detail.coupon_code = eq_adjust_price_nomina_sales.coupon_code) 
 SET tbl_order_detail.Actual_Paid_price = eq_adjust_price_nomina_sales.actual_paid_price,
 tbl_order_detail.Actual_Paid_price_after_tax = eq_adjust_price_nomina_sales.actual_paid_price_after_vat, 
 tbl_order_detail.coupon_money_value = eq_adjust_price_nomina_sales.coupon_money_value, 
tbl_order_detail.coupon_money_after_vat = eq_adjust_price_nomina_sales.coupon_money_after_vat;
 

#Costo after tax/Delivery cost supplier
  UPDATE (tbl_order_detail INNER JOIN bob_live_mx.catalog_simple 
ON tbl_order_detail.sku = bob_live_mx.catalog_simple.sku) 
 INNER JOIN bob_live_mx.catalog_config ON bob_live_mx.catalog_simple.fk_catalog_config = catalog_config.id_catalog_config
 SET tbl_order_detail.costo_after_vat = bob_live_mx.catalog_simple.cost/((100+tbl_order_detail.tax_percent)/100)
  where yrmonth = 201208;

 UPDATE tbl_order_detail INNER JOIN bob_live_mx.sales_order_item
ON tbl_order_detail.item = id_sales_order_item
 set  tbl_order_detail.Costo_after_vat = (bob_live_mx.sales_order_item.cost)/((100+sales_order_item.tax_percent)/100)
 where yrmonth > 201208;
 
 UPDATE (tbl_order_detail INNER JOIN bob_live_mx.catalog_simple 
ON tbl_order_detail.sku = bob_live_mx.catalog_simple.sku) 
 INNER JOIN bob_live_mx.catalog_config ON bob_live_mx.catalog_simple.fk_catalog_config = catalog_config.id_catalog_config
 SET tbl_order_detail.Delivery_cost_supplier = bob_live_mx.catalog_simple.delivery_cost_supplier;
 
  #Adjust wrong costs on daily report
  UPDATE tbl_order_detail INNER JOIN eq_adjust_wrong_costs 
 ON (tbl_order_detail.order_nr = eq_adjust_wrong_costs.order_nr)
 AND (tbl_order_detail.sku = eq_adjust_wrong_costs.sku) 
 SET  tbl_order_detail.costo_after_vat = eq_adjust_wrong_costs.Cost_after_tax;
 
 #Notas de crédito
 
 update tbl_order_detail t inner join 
(select items.yrmonth, items.proveedor, cat1, cat2,costo_after_vat, items 
from (select yrmonth, proveedor, n1, n2, count(sku) items
from tbl_order_detail where oac = 1 and returned = 0
group by yrmonth, proveedor, n1, n2) items
inner join eq_notas_de_credito e on items.yrmonth = e.yrmonth
and cat1 = n1 and cat2 = n2 and e.proveedor = items.proveedor) p 
on t.yrmonth = p.yrmonth and cat1 = n1 and cat2 = n2 and p.proveedor = t.proveedor 
set t.costo_after_vat = t.costo_after_vat - p.costo_after_vat/items;

update tbl_order_detail t inner join 
(select items.yrmonth, items.proveedor, cat1, costo_after_vat, items 
from (select yrmonth, proveedor, n1,  count(sku) items
from tbl_order_detail where oac = 1 and returned = 0
group by yrmonth, proveedor, n1) items
inner join eq_notas_de_credito e on items.yrmonth = e.yrmonth
and cat1 = n1 and e.proveedor = items.proveedor and cat2 = '') p 
on t.yrmonth = p.yrmonth and cat1 = n1 and p.proveedor = t.proveedor 
set t.costo_after_vat = t.costo_after_vat - p.costo_after_vat/items;

update tbl_order_detail t inner join 
(select items.yrmonth, items.proveedor, cat1, costo_after_vat, items 
from (select yrmonth, proveedor, count(sku) items
from tbl_order_detail where oac = 1 and returned = 0
group by yrmonth, proveedor) items
inner join eq_notas_de_credito e on items.yrmonth = e.yrmonth
 and e.proveedor = items.proveedor and cat1 = '' and cat2 = '') p 
on t.yrmonth = p.yrmonth and p.proveedor = t.proveedor 
set t.costo_after_vat = t.costo_after_vat -  p.costo_after_vat/items;




end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `extra_query_actual_paid_price`(itemid integer)
begin

update production.tbl_order_detail set actual_paid_price = paid_price;

update (production.tbl_order_detail left join bob_live_mx.sales_rule on tbl_order_detail.coupon_code = sales_rule.code) left join bob_live_mx.sales_rule_set on sales_rule.fk_sales_rule_set = sales_rule_set.id_sales_rule_set set tbl_order_detail.code_prefix = sales_rule_set.code_prefix, tbl_order_detail.actual_paid_price = case when sales_rule_set.code_prefix="cce" then tbl_order_detail.paid_price +tbl_order_detail.coupon_money_value when sales_rule_set.code_prefix="prccre" then tbl_order_detail.paid_price + tbl_order_detail.coupon_money_value when sales_rule_set.code_prefix="opscre" then tbl_order_detail.paid_price + tbl_order_detail.coupon_money_value when sales_rule_set.code_prefix="dep" then tbl_order_detail.paid_price + tbl_order_detail.coupon_money_value else tbl_order_detail.paid_price end, tbl_order_detail.actual_paid_price_after_tax = case when sales_rule_set.code_prefix="cce" then tbl_order_detail.paid_price_after_vat + tbl_order_detail.coupon_money_after_vat when sales_rule_set.code_prefix="prccre" then tbl_order_detail.paid_price_after_vat + tbl_order_detail.coupon_money_after_vat when sales_rule_set.code_prefix="opscre" then tbl_order_detail.paid_price_after_vat + tbl_order_detail.coupon_money_after_vat when sales_rule_set.code_prefix="dep" then tbl_order_detail.paid_price_after_vat + tbl_order_detail.coupon_money_after_vat else tbl_order_detail.paid_price/((100+tbl_order_detail.tax_percent)/100) end where tbl_order_detail.actual_paid_price_after_tax is null;

/*update production.tbl_order_detail set tbl_order_detail.payment_cost = (tbl_order_detail.paid_price/((100+tbl_order_detail.tax_percent)/100)*((tbl_order_detail.fees)/100)+tbl_order_detail.extra_charge_mxn);*/

update (production.tbl_order_detail left join bob_live_mx.sales_rule on tbl_order_detail.coupon_code = sales_rule.code) left join bob_live_mx.sales_rule_set on sales_rule.fk_sales_rule_set = sales_rule_set.id_sales_rule_set set tbl_order_detail.actual_paid_price = case when sales_rule_set.code_prefix="visa" then tbl_order_detail.paid_price+(0.2*tbl_order_detail.unit_price) when sales_rule_set.code_prefix="visa3" then (tbl_order_detail.paid_price+(0.1*tbl_order_detail.unit_price)) else tbl_order_detail.actual_paid_price end, tbl_order_detail.actual_paid_price_after_tax = case when sales_rule_set.code_prefix="visa" then (tbl_order_detail.paid_price/((100+tbl_order_detail.tax_percent)/100))+((tbl_order_detail.unit_price*0.2)/((100+tbl_order_detail.tax_percent)/100)) when sales_rule_set.code_prefix="visa3" then (tbl_order_detail.paid_price+((0.1*(tbl_order_detail.unit_price))))/((100+tbl_order_detail.tax_percent)/100) else tbl_order_detail.actual_paid_price_after_tax end where tbl_order_detail.actual_paid_price_after_tax is null;

-- A118 D
delete production.items_per_order.* from production.items_per_order;

-- A119 A
insert into production.items_per_order ( order_num, countofsku_simple )
select tbl_order_detail.order_nr, count(tbl_order_detail.sku) as countofsku_simple from production.tbl_order_detail group by tbl_order_detail.order_nr;

update (production.tbl_order_detail inner join production.com_visa_promotion_product on (tbl_order_detail.coupon_code = com_visa_promotion_product.coupon_code) and (tbl_order_detail.sku_config = com_visa_promotion_product.sku_config)) inner join bob_live_mx.sales_order_item on (tbl_order_detail.sku = sales_order_item.sku) and (tbl_order_detail.order_nr = sales_order_item.fk_sales_order) left join production.items_per_order on tbl_order_detail.order_nr = items_per_order.order_num set tbl_order_detail.actual_paid_price = tbl_order_detail.paid_price+com_visa_promotion_product.subsidio_visa, tbl_order_detail.actual_paid_price_after_tax = tbl_order_detail.paid_price_after_vat+com_visa_promotion_product.subsidio_visa_after_vat, tbl_order_detail.coupon_money_value = sales_order_item.coupon_money_value-com_visa_promotion_product.subsidio_visa, tbl_order_detail.coupon_money_after_vat = (sales_order_item.coupon_money_value/((100+sales_order_item.tax_percent)/100))-com_visa_promotion_product.subsidio_visa_after_vat, tbl_order_detail.shipping_fee_after_vat = tbl_order_detail.shipping_fee_after_vat+(com_visa_promotion_product.shipping_fee_charged_to_visa_after_vat/items_per_order.countofsku_simple) where tbl_order_detail.actual_paid_price_after_tax is null;

update production.tbl_order_detail set tbl_order_detail.actual_paid_price = case when tbl_order_detail.code_prefix="ccemp" then tbl_order_detail.paid_price+tbl_order_detail.coupon_money_value else tbl_order_detail.actual_paid_price end, tbl_order_detail.actual_paid_price_after_tax = case when tbl_order_detail.code_prefix="ccemp" then tbl_order_detail.paid_price_after_vat+tbl_order_detail.coupon_money_after_vat else tbl_order_detail.actual_paid_price_after_tax end, tbl_order_detail.coupon_money_value = case when tbl_order_detail.code_prefix="ccemp" then 0 else tbl_order_detail.coupon_money_value end, tbl_order_detail.coupon_money_after_vat = case when tbl_order_detail.code_prefix="ccemp" then 0 else tbl_order_detail.coupon_money_after_vat end where tbl_order_detail.actual_paid_price_after_tax is null;

update production.tbl_order_detail inner join production.ciber_santa on (tbl_order_detail.sku_config = ciber_santa.sku) and (tbl_order_detail.date = ciber_santa.date) set tbl_order_detail.paid_price = tbl_order_detail.unit_price-ciber_santa.subsidio, tbl_order_detail.paid_price_after_vat = (tbl_order_detail.unit_price-ciber_santa.subsidio)*0.86, tbl_order_detail.actual_paid_price = tbl_order_detail.paid_price+ciber_santa.subsidio, tbl_order_detail.actual_paid_price_after_tax = (tbl_order_detail.actual_paid_price_after_tax+ciber_santa.subsidio)*0.86 where tbl_order_detail.actual_paid_price_after_tax is null;

update (production.tbl_order_detail left join bob_live_mx.sales_rule on tbl_order_detail.coupon_code = sales_rule.code) left join bob_live_mx.sales_rule_set on sales_rule_set.fk_sales_rule_set_refund = sales_rule_set.id_sales_rule_set set tbl_order_detail.code_prefix = sales_rule_set.code_prefix, tbl_order_detail.actual_paid_price = tbl_order_detail.paid_price+tbl_order_detail.coupon_money_value, tbl_order_detail.actual_paid_price_after_tax = tbl_order_detail.paid_price_after_vat+tbl_order_detail.coupon_money_after_vat where tbl_order_detail.code_prefix="opscred" and tbl_order_detail.date>='2012-12-01' and tbl_order_detail.actual_paid_price_after_tax is null;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `extra_query_once`()
begin

update production.tbl_order_detail set tbl_order_detail.paid_price = tbl_order_detail.paid_price+tbl_order_detail.coupon_money_after_vat*1.16, tbl_order_detail.paid_price_after_vat = tbl_order_detail.paid_price_after_vat+tbl_order_detail.coupon_money_after_vat
where (tbl_order_detail.coupon_code) like "mkt%" and (tbl_order_detail.coupon_code) not in ('MKTNfue3n', 'MKT0i1Gq2', 'MKT0xfgVK', 'MKT1eDvI7', 'MKTB0TZfz', 'MKTxmsjvs', 'MKTB0TZfz', 'MKT3TcOf8', 'MKT063HRE', 'MKTqqU8KR', 'MKT7lSkUd', 'MKT1pFyS6', 'MKTNfue3n');

update production.tbl_order_detail set tbl_order_detail.paid_price = tbl_order_detail.paid_price+tbl_order_detail.coupon_money_after_vat, tbl_order_detail.paid_price_after_vat = tbl_order_detail.paid_price_after_vat+tbl_order_detail.coupon_money_after_vat*0.86
where tbl_order_detail.coupon_code="MKT7tavt8" or tbl_order_detail.coupon_code="MKT0uJXni" or tbl_order_detail.coupon_code="MKT0pTUbn" or tbl_order_detail.coupon_code=" MKT1orfvU" or tbl_order_detail.coupon_code="MKT1Z54Lh" or tbl_order_detail.coupon_code="MKTf4A6Sk" or tbl_order_detail.coupon_code="MKT0xfgVK" or tbl_order_detail.coupon_code="MKT1eDvI7" or tbl_order_detail.coupon_code="MKTxmsjvs" or tbl_order_detail.coupon_code="MKTB0TZfz" or tbl_order_detail.coupon_code="MKT3TcOf8" or tbl_order_detail.coupon_code="MKT063HRE" or tbl_order_detail.coupon_code="MKTqqU8KR";

update production.tbl_order_detail set tbl_order_detail.paid_price = tbl_order_detail.paid_price+tbl_order_detail.coupon_money_value, tbl_order_detail.paid_price_after_vat = tbl_order_detail.paid_price_after_vat+tbl_order_detail.coupon_money_after_vat
where (((tbl_order_detail.coupon_code) like "nl%"));

update production.tbl_order_detail set tbl_order_detail.coupon_money_value = tbl_order_detail.coupon_money_value+tbl_order_detail.shipping_cost
where (((tbl_order_detail.coupon_code)="mkt0xfgvk" or (tbl_order_detail.coupon_code)="mkt1edvi7"));

update production.tbl_order_detail set tbl_order_detail.shipping_cost = 0
where (((tbl_order_detail.coupon_code)="mkt0xfgvk" or (tbl_order_detail.coupon_code)="mkt1edvi7"));

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`daniel.palacios`@`%`*/ /*!50003 PROCEDURE `insert_table_monitor_log`(`country` varchar(255),`table_name` varchar(255),`step` varchar(255),`updated_at` datetime,`key_date` datetime,`total_rows` int(11),`total_rows_check` int(11))
BEGIN
	#Routine body goes here...
	INSERT INTO table_monitoring_log (country,table_namE,step,updated_at,key_date,total_rows,total_rows_check)
	VALUES (country,table_namE,step,updated_at,key_date,total_rows,total_rows_check);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `mom_net_sales_per_channel`()
BEGIN

#Fecha inicio mes actual
select @start_current_month:=cast(date_add(now(), interval - day(now())+1 day) as date);

#Fecha fin mes actual
select @end_current_month:=cast(date_add(now(), interval - 1 day) as date);

#Fecha inicio mes anterior
select @start_previous_month:=date_add(cast(date_add(now(), interval - day(now())+1 day) as date), interval - 1 month);

#Fecha fin mes anterior
select @end_previous_month:=date_add(cast(date_add(now(), interval - 1 day) as date), interval - 1 month);

delete from tbl_mom_net_sales_per_channel;

insert into tbl_mom_net_sales_per_channel(date, channel_group, netSalesPreviousMonth, netSalesCurrentMonth,month_over_month)
select @end_current_month, currentMonth.channel_group, sum(NetSalesPreviousMonth) NetSalesPreviousMonth, sum(NetSalesCurrentMonth) NetSalesCurrentMonth, 
if((sum(NetSalesCurrentMonth)/sum(NetSalesPreviousMonth))-1 is null, 0,(sum(NetSalesCurrentMonth)/sum(NetSalesPreviousMonth))-1)
from
(select channel_group, sum(actual_paid_price_after_tax) +sum(if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat)) as NetSalesCurrentMonth
from tbl_order_detail
where oac = 1 and returned = 0 and date >= @start_current_month and date <= @end_current_month
group by channel_group) currentMonth left join
(select channel_group, sum(actual_paid_price_after_tax) +sum(if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat)) as NetSalesPreviousMonth
from tbl_order_detail
where oac = 1 and returned = 0 and date >= @start_previous_month and date <= @end_previous_month
group by channel_group) previousMonth on
currentMonth.channel_group = previousMonth.channel_group
group by month(@start_current_month), currentMonth.channel_group;

insert into tbl_mom_net_sales_per_channel(date, channel_group, netSalesPreviousMonth, netSalesCurrentMonth,month_over_month)
select date, 'MTD',sum(NetSalesPreviousMonth), sum(NetSalesCurrentMonth), (sum(NetSalesCurrentMonth)/sum(NetSalesPreviousMonth))-1
from (
select @end_current_month date, sum(NetSalesPreviousMonth) NetSalesPreviousMonth, sum(NetSalesCurrentMonth) NetSalesCurrentMonth, 
(sum(NetSalesCurrentMonth)/sum(NetSalesPreviousMonth))-1
from
(select channel_group, sum(actual_paid_price_after_tax) +sum(if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat)) as NetSalesCurrentMonth
from tbl_order_detail
where oac = 1 and returned = 0 and date >= @start_current_month and date <= @end_current_month
group by channel_group) currentMonth inner join
(select channel_group, sum(actual_paid_price_after_tax) +sum(if(shipping_fee_after_vat is null, 0, shipping_fee_after_vat)) as NetSalesPreviousMonth
from tbl_order_detail
where oac = 1 and returned = 0 and date >= @start_previous_month and date <= @end_previous_month
group by channel_group) previousMonth on
currentMonth.channel_group = previousMonth.channel_group
group by month(@start_current_month), currentMonth.channel_group) p group by date;

update tbl_mom_net_sales_per_channel
set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) where date =  @end_previous_month;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `monitoring`(IN `in_table_name` varchar(255),IN `in_country` varchar(255), IN `attempts` int)
BEGIN
	
	DECLARE rows     INT DEFAULT 0;

	#DECLARE attempts INT DEFAULT 12;
	 
	monitoring: WHILE rows = 0 DO

		SELECT COUNT(*) INTO rows FROM production.table_monitoring_log 
		WHERE country = in_country AND table_name = in_table_name
					AND updated_at  >= curdate() - interval 1 hour 
          AND Step = "finish";

		IF rows     > 0 
		THEN
			 LEAVE monitoring;
		END IF;

		IF attempts = 0 
		THEN
			 LEAVE monitoring;
		END IF;

		SET attempts = attempts - 1 ;
		SELECT SLEEP(30);

	END WHILE;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `monitoring_step`(IN `in_table_name` varchar(255),IN `in_country` varchar(255), IN `in_step` varchar(50), IN `attempts` int)
BEGIN
	
	DECLARE rows     INT DEFAULT 0;

	#DECLARE attempts INT DEFAULT 12;
	 
	monitoring: WHILE rows = 0 DO

		SELECT COUNT(*) INTO rows FROM production.table_monitoring_log 
		WHERE country = in_country AND table_name = in_table_name
          AND updated_at  >= curdate() - interval 1 hour 
          AND Step = in_step ;

		IF rows     > 0 
		THEN
			 LEAVE monitoring;
		END IF;

		IF attempts = 0 
		THEN
			 LEAVE monitoring;
		END IF;

		SET attempts = attempts - 1 ;
		SELECT SLEEP(30);

	END WHILE;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `monthly_marketing_2`()
BEGIN

#yrmonth

select @yrmonth := concat(year(date_sub(curdate(), interval 1 day)),if(month(date_sub(curdate(), interval 1 day))<10,concat(0,month(date_sub(curdate(), interval 1 day))),month(date_sub(curdate(), interval 1 day))));

DELETE FROM tbl_monthly_marketing WHERE yrmonth=@yrmonth;

INSERT IGNORE INTO tbl_monthly_marketing (month,channel) 
SELECT DISTINCT month(now()), channel_group FROM tbl_order_detail WHERE channel_group is not null and yrmonth=@yrmonth;

update tbl_monthly_marketing
set yrmonth = concat(year(date_sub(curdate(), interval 1 day)),if(month(date_sub(curdate(), interval 1 day))<10,concat(0,month(date_sub(curdate(), interval 1 day))),month(date_sub(curdate(), interval 1 day))))
where month = month (date_sub(curdate(), interval 1 day));

#VISITS	

UPDATE tbl_monthly_marketing  left JOIN (SELECT yrmonth yrmonth, reporting_channel, sum(visits) visits
FROM daily_costs_visits_per_channel where daily_costs_visits_per_channel.date < cast(curdate() as date)
GROUP BY yrmonth, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.visits=daily_costs_visits_per_channel.visits 
WHERE tbl_monthly_marketing.yrmonth = @yrmonth
;

#GROSS ORDERS

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, ceil(sum(gross_orders)) gross_orders
FROM tbl_order_detail WHERE OBC = 1 and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=tbl_order_detail.channel_group
SET tbl_monthly_marketing.gross_orders=tbl_order_detail.gross_orders
WHERE tbl_monthly_marketing.yrmonth = @yrmonth
;
#NET ORDERS

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, ceil(sum(net_orders)) net_orders
FROM tbl_order_detail WHERE oac = 1 and returned = 0 and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=tbl_order_detail.channel_group
SET tbl_monthly_marketing.net_orders=tbl_order_detail.net_orders
WHERE tbl_monthly_marketing.yrmonth = @yrmonth
;

#ORDERS NET EXPECTED

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, reporting_channel, sum(net_orders_e) net_orders_e
FROM daily_costs_visits_per_channel where daily_costs_visits_per_channel.date < cast(curdate() as date)
GROUP BY yrmonth, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.orders_net_expected=daily_costs_visits_per_channel.net_orders_e
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#CONVERSION RATE
UPDATE tbl_monthly_marketing SET conversion_rate=IF(gross_orders/visits IS NULL, 0, gross_orders/visits);

#PENDING REVENUE
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, 
(SUM(actual_paid_price_after_tax)+SUM(shipping_fee_after_vat))/15.9 pending_sales
FROM tbl_order_detail WHERE tbl_order_detail.pending=1  and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
SET tbl_monthly_marketing.pending_revenue=tbl_order_detail.pending_sales
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group and tbl_monthly_marketing.yrmonth = @yrmonth;

#GROSS REVENUE
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, 
(SUM(actual_paid_price_after_tax)+SUM(shipping_fee_after_vat))/15.9 gross_sales
FROM tbl_order_detail WHERE tbl_order_detail.obc=1  and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
SET tbl_monthly_marketing.gross_revenue=tbl_order_detail.gross_sales
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group and tbl_monthly_marketing.yrmonth = @yrmonth;

#NET REVENUE
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth, channel_group, 
(SUM(actual_paid_price_after_tax)+SUM(shipping_fee_after_vat))/15.9 net_sales
FROM tbl_order_detail WHERE tbl_order_detail.oac=1 AND tbl_order_detail.returned=0  and tbl_order_detail.date < cast(curdate() as date) 
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
SET tbl_monthly_marketing.net_revenue=tbl_order_detail.net_sales
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group and tbl_monthly_marketing.yrmonth = @yrmonth
;

#NET REVENUE EXPECTED

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, 
reporting_channel, sum(net_rev_e)/15.9 net_rev_e
FROM daily_costs_visits_per_channel where daily_costs_visits_per_channel.date < cast(curdate() as date)
GROUP BY yrmonth, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.net_revenue_expected=daily_costs_visits_per_channel.net_rev_e and tbl_monthly_marketing.yrmonth = @yrmonth
;

UPDATE tbl_monthly_marketing SET net_revenue_expected = net_revenue WHERE net_revenue_expected=0;
UPDATE tbl_monthly_marketing SET net_revenue_expected = net_revenue WHERE net_revenue_expected<net_revenue;

#MONTHLY TARGET

UPDATE tbl_monthly_marketing  
INNER JOIN (SELECT yrmonth yrmonth, channel_group, sum(target_net_sales) monthly_target
FROM daily_targets_per_channel
GROUP BY yrmonth, channel_group) daily_targets_per_channel
ON daily_targets_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_targets_per_channel.channel_group
SET tbl_monthly_marketing.monthly_target=daily_targets_per_channel.monthly_target and tbl_monthly_marketing.yrmonth = @yrmonth;

#MONTHLY DEVIATION
UPDATE tbl_monthly_marketing 
SET 
deviation=if((net_revenue_expected/(monthly_target/(SELECT DAYOFMONTH(LAST_DAY(NOW())))*(SELECT DAY(NOW())-1))-1) IS NULL,
0,(net_revenue_expected/(monthly_target/(SELECT DAYOFMONTH(LAST_DAY(NOW())))*(SELECT DAY(NOW())-1))-1))
where tbl_monthly_marketing.yrmonth = @yrmonth;

#AVG TICKET
UPDATE tbl_monthly_marketing SET avg_ticket=net_revenue/net_orders;

#MARKETING COST

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, 
reporting_channel, sum(cost_local)/15.9 cost
FROM daily_costs_visits_per_channel where daily_costs_visits_per_channel.date < cast(curdate() as date)
GROUP BY yrmonth, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.marketing_cost=daily_costs_visits_per_channel.cost
WHERE tbl_monthly_marketing.yrmonth = @yrmonth
;

#COST/REV
UPDATE tbl_monthly_marketing SET cost_rev=IF(marketing_cost/net_revenue_expected IS NULL, 0, marketing_cost/net_revenue_expected)
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#CPO
UPDATE tbl_monthly_marketing SET cost_per_order=IF(marketing_cost/net_orders IS NULL, 0, marketing_cost/net_orders)
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#CPO TARGET

UPDATE tbl_monthly_marketing  
INNER JOIN (SELECT yrmonth yrmonth, channel_group, MAX(target_cpo) target_cpo
FROM daily_targets_per_channel
GROUP BY yrmonth, channel_group) daily_targets_per_channel
ON daily_targets_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_targets_per_channel.channel_group
SET tbl_monthly_marketing.cost_per_order_target=daily_targets_per_channel.target_cpo
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#CPO VS TARGET
UPDATE tbl_monthly_marketing SET cpo_vs_target_cpo=(cost_per_order/cost_per_order_target)-1 WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#NEW CLIENTS
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, sum(new_customers) nc
FROM tbl_order_detail where oac = 1 and returned = 0 and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=tbl_order_detail.channel_group
SET tbl_monthly_marketing.new_clients=tbl_order_detail.nc WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#NEW CLIENTS EXPECTED
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, 
reporting_channel, sum(new_customers_e) nc_e
FROM daily_costs_visits_per_channel 
where daily_costs_visits_per_channel.date < cast(curdate() as date)
GROUP BY yrmonth, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.new_clientes_expected=daily_costs_visits_per_channel.nc_e WHERE tbl_monthly_marketing.yrmonth = @yrmonth;



UPDATE tbl_monthly_marketing SET new_clientes_expected=new_clients WHERE new_clientes_expected=0 AND tbl_monthly_marketing.yrmonth = @yrmonth;

#CAC
UPDATE tbl_monthly_marketing SET client_acquisition_cost=marketing_cost/new_clientes_expected WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#PC2_COST

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, 
(SUM(ifnull(costo_after_vat,0))+
SUM(ifnull(delivery_cost_supplier,0))+SUM(ifnull(wh,0))+SUM(ifnull(cs,0))+
SUM(ifnull(payment_cost,0))+SUM(ifnull(shipping_cost,0)))/15.9 
pc2_costs
FROM production.tbl_order_detail WHERE tbl_order_detail.oac=1 AND tbl_order_detail.returned=0  and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
SET tbl_monthly_marketing.pc2_costs=tbl_order_detail.pc2_costs
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group AND tbl_monthly_marketing.yrmonth = @yrmonth;

#PC2
UPDATE tbl_monthly_marketing SET profit_contribution_2= 1-(pc2_costs/net_revenue) WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#MOM

UPDATE tbl_monthly_marketing INNER JOIN tbl_mom_net_sales_per_channel 
ON tbl_monthly_marketing.channel = tbl_mom_net_sales_per_channel.channel_group
AND tbl_monthly_marketing.yrmonth = tbl_mom_net_sales_per_channel.yrmonth
SET tbl_monthly_marketing.month_over_month = tbl_mom_net_sales_per_channel.month_over_month WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

INSERT IGNORE INTO tbl_monthly_marketing (month, channel) 
VALUES(month(date_add(now(), interval -1 day)), 'MTD');

update tbl_monthly_marketing
set yrmonth = concat(year(date_sub(curdate(), interval 1 day)),if(month(date_sub(curdate(), interval 1 day))<10,concat(0,month(date_sub(curdate(), interval 1 day))),month(date_sub(curdate(), interval 1 day))))
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

UPDATE tbl_monthly_marketing INNER JOIN tbl_mom_net_sales_per_channel 
ON tbl_monthly_marketing.channel = tbl_mom_net_sales_per_channel.channel_group
AND tbl_monthly_marketing.yrmonth = tbl_mom_net_sales_per_channel.yrmonth
SET tbl_monthly_marketing.month_over_month = tbl_mom_net_sales_per_channel.month_over_month
where channel_group = 'MTD' AND tbl_monthly_marketing.yrmonth = @yrmonth;

#Adjusted revenue
#Net
update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion,
 sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth, l.about_linio,t.channel_group,  sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_net_revenue = ifnull(m.net_revenue + t.adjusted_net_revenue/15.9, m.net_revenue)
where m.channel <> 'Tele Sales' AND m.yrmonth = @yrmonth;

update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_net_revenue = ifnull( t.adjusted_net_revenue/15.9,0)
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth ;

#Gross
update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and obc = 1 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_gross_revenue = ifnull(m.gross_revenue + t.adjusted_net_revenue/15.9, m.gross_revenue)
where m.channel <> 'Tele Sales'  AND m.yrmonth = @yrmonth;

update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and obc = 1 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_gross_revenue = ifnull( t.adjusted_net_revenue/15.9,0)
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth;

#Pending
update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and pending = 1 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_pending_revenue = ifnull(m.pending_revenue + t.adjusted_net_revenue/15.9, m.pending_revenue)
where m.channel <> 'Tele Sales' AND m.yrmonth = @yrmonth;

update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and pending = 1 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_pending_revenue = ifnull( t.adjusted_net_revenue/15.9,0)
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth;

#Net ORders
update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, 
sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  count(distinct orderID) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, count(distinct orderID) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, count(distinct orderID) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_net_orders = ifnull(m.net_orders + t.adjusted_net_revenue, m.net_orders)
where m.channel <> 'Tele Sales' AND m.yrmonth = @yrmonth;

update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, 
sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  count(distinct orderID) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, count(distinct orderID) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, count(distinct orderID) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_net_orders = t.adjusted_net_revenue
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth;

#New clients
update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, 
sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(new_customers) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio,  sum(new_customers) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(new_customers) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_new_clients = ifnull(m.new_clients + t.adjusted_net_revenue, m.new_clients)
where m.channel <> 'Tele Sales' AND m.yrmonth = @yrmonth;

update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, 
sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group, sum(new_customers) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio,  sum(new_customers) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(new_customers) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_new_clients = t.adjusted_net_revenue
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `out_outlet_history`()
begin
 
select  'Outlet History: start',now();
 
insert into production.out_outlet_history (date_inserted, yrweek, stock_item_id, barcode_wms, hash_wms, barcode_bob, sku_config, sku_simple, sku_name, sku_supplier_config, model, category, category_english, date_entrance, date_exit, week_exit, exit_type, days_in_stock, fullfilment_type_bob, fullfilment_type_real, supplier_id, supplier_name, cost, tax_percentage, cost_w_o_vat, buyer, head_buyer, in_stock, in_stock_cost_w_o_vat, price, wh_location, barcode_bob_duplicated, item_counter, supplier_pay_time, sku_counter, reserved, supplier_leadtime, visible, sku_simple_blank, quarantine, cat2, procurement_price, sold, average_remaining_days, max_days_in_stock, delivery_cost_supplier, cogs, entrance_last_30, days_payable_since_entrance, items_procured_in_transit, category_com_main, category_com_sub)
 
select curdate(), production.week_iso(curdate()), stock_item_id, barcode_wms, hash_wms, barcode_bob, sku_config, sku_simple, sku_name, sku_supplier_config, model, category, category_english, date_entrance, date_exit, week_exit, exit_type, days_in_stock, fullfilment_type_bob, fullfilment_type_real, supplier_id, supplier_name, cost, tax_percentage, cost_w_o_vat, buyer, head_buyer, in_stock, in_stock_cost_w_o_vat, price, wh_location, barcode_bob_duplicated, item_counter, supplier_pay_time, sku_counter, reserved, supplier_leadtime, visible, sku_simple_blank, quarantine, cat2, procurement_price, sold, average_remaining_days, max_days_in_stock, delivery_cost_supplier, cogs, entrance_last_30, days_payable_since_entrance, items_procured_in_transit, category_com_main, category_com_sub from production.out_stock_hist where in_stock=1 and reserved=0 and fullfilment_type_bob != 'consignment';
 
update production.out_outlet_history set in_outlet = case when category = 'outlet' then 1 else 0 end where date_inserted = curdate();
 
-- D001 U
update production.out_outlet_history inner join production.tbl_catalog_product_v2 on out_outlet_history.sku_simple = tbl_catalog_product_v2.sku set out_outlet_history.special_price = tbl_catalog_product_v2.special_price where date_inserted = curdate();
 
-- D002 U
update production.out_outlet_history inner join production.tbl_order_detail x on out_outlet_history.sku_simple = x.sku set out_outlet_history.avg_shipping_fee_after_vat = (select avg(y.shipping_fee_after_vat) from production.tbl_order_detail y where x.sku = y.sku group by sku) where date_inserted = curdate();
 
-- D003 U
update production.out_outlet_history inner join production.tbl_order_detail x on out_outlet_history.sku_simple = x.sku set out_outlet_history.avg_shipping_cost_per_sku = (select avg(y.shipping_cost_per_sku) from production.tbl_order_detail y where x.sku = y.sku group by sku) where date_inserted = curdate();
 
-- D004 U
update production.out_outlet_history inner join production.tbl_order_detail x on out_outlet_history.sku_simple = x.sku set out_outlet_history.avg_customer_care_cost = (select avg(y.cs) from production.tbl_order_detail y where x.sku = y.sku group by sku) where date_inserted = curdate();
 
-- D005 U
update production.out_outlet_history inner join production.tbl_order_detail x on out_outlet_history.sku_simple = x.sku set out_outlet_history.avg_wh_cost = (select avg(y.wh) from production.tbl_order_detail y where x.sku = y.sku group by sku) where date_inserted = curdate();
 
-- D006 U
update production.out_outlet_history inner join production.tbl_order_detail x on out_outlet_history.sku_simple = x.sku set out_outlet_history.avg_payment_cost = (select avg(y.payment_cost) from production.tbl_order_detail y where x.sku = y.sku group by sku) where date_inserted = curdate();
 
-- D006 U
update production.out_outlet_history inner join bob_live_mx.catalog_simple s on out_outlet_history.sku_simple = s.sku inner join bob_live_mx.catalog_supplier_stock on s.id_catalog_simple = catalog_supplier_stock.fk_catalog_simple set  out_outlet_history.supplier_stock = (select sum(quantity) from bob_live_mx.catalog_supplier_stock x where s.id_catalog_simple = x.fk_catalog_simple group by fk_catalog_simple) where date_inserted = curdate();
 
update production.out_outlet_history h inner join production.pro_outlet_outputs o on h.yrweek=o.yrweek and h.sku_simple=o.sku_simple set h.sell_list_comments=o.sell_list_comments, h.keep_in_outlet=o.keep_in_outlet, h.take_out_of_outlet=o.take_out_of_outlet, h.include_in_outlet=o.include_in_outlet, h.outlet_price = o.outlet_price, h.outlet_discount = o.outlet_discount, h.action=o.action;
 
update production.out_outlet_history set avg_remaining_days_threshold = case when average_remaining_days >= 45 then 45 else (case when average_remaining_days >= 30 then 30 else (case when average_remaining_days >= 15 then 15 else 0 end) end) end, days_in_stock_threshold = case when days_in_stock >= 180 then 180 else (case when days_in_stock >= 120 then 120 else (case when days_in_stock >= 90 then 90 else (case when days_in_stock >= 60 then 60 else 0 end) end) end) end where date_inserted = curdate();
 
update production.out_outlet_history a inner join production.pricing_rules b on (a.category_com_sub = b.category_com_sub) and (a.avg_remaining_days_threshold = b.avg_remaining_days) and (a.days_in_stock_threshold = b.days_in_stock) set a.forced_discount = b.discount where date_inserted = curdate();
 
update production.out_outlet_history set discounted_price = price*(1 - forced_discount) where date_inserted = curdate();
 
update production.out_outlet_history x set avg_price_last_30 = (select avg(price) from production.catalog_history c where datediff(curdate(),date)<=30 and x.sku_simple=c.sku_simple) where date_inserted = curdate();

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `out_safety_stock`()
begin

select  'Safety Stock: start',now();

delete  from production.out_safety_stock;

insert into production.out_safety_stock(date, item_id, order_num, product_name, sku_simple, supplier, obc, oac, status) select date, item, order_nr, product_name, sku, proveedor, obc, oac, status_item from production.tbl_order_detail where status_item in ('confirm_call_1', 'manual_fraud_check_pending', 'payment_pending');

update production.out_safety_stock s set s.in_stock = (select sum(h.in_stock) from production.out_stock_hist h where s.sku_simple=h.sku_simple and reserved=0 group by h.sku_simple); 

/*update production.out_safety_stock s inner join production.out_stock_hist h on s.sku_simple=h.sku_simple set s.items_procured_in_transit=h.items_procured_in_transit, s.fullfilment_type_bob=h.fullfilment_type_bob, s.fullfilment_type_real=h.fullfilment_type_real;*/

update production.out_safety_stock s inner join production.out_stock_hist h on s.sku_simple=h.sku_simple set s.fullfilment_type_bob=h.fullfilment_type_bob, s.fullfilment_type_real=h.fullfilment_type_real; 

update production.out_safety_stock s inner join production.items_procured_in_transit ipit on s.sku_simple=ipit.sku_simple set s.items_procured_in_transit=ipit.number_ordered;

update production.out_safety_stock inner join bob_live_mx.sales_order_item on out_safety_stock.sku_simple = sales_order_item.sku inner join bob_live_mx.catalog_shipment_type on sales_order_item.fk_catalog_shipment_type = catalog_shipment_type.id_catalog_shipment_type set out_safety_stock.fullfilment_type_bob = catalog_shipment_type.name where out_safety_stock.fullfilment_type_bob is null;

update production.out_safety_stock set in_stock=0 where in_stock is null;

update production.out_safety_stock set items_procured_in_transit=0 where items_procured_in_transit is null;

delete from production.temporary_safety_stock;

insert into production.temporary_safety_stock select order_num, sku_simple from production.out_safety_stock;

update production.out_safety_stock s set count_order= (select count(distinct order_num)/count(order_num) from production.temporary_safety_stock t where s.order_num=t.order_num group by t.order_num);

update production.out_safety_stock s set count_sku= (select count(distinct sku_simple)/count(sku_simple) from production.temporary_safety_stock t where s.sku_simple=t.sku_simple and s.order_num=t.order_num group by t.sku_simple, t.order_num);

/*update wmsprod.itens_venda inner join production.out_safety_stock on itens_venda.sku = out_safety_stock.sku_simple set out_safety_stock.items_pending_procurement = 1 where itens_venda.status in ('aguardando estoque', 'analisando quebra');*/

update production.out_safety_stock set items_pending_procurement = (select count(itens_venda_id) from wmsprod.itens_venda where itens_venda.sku = out_safety_stock.sku_simple and itens_venda.status in ('aguardando estoque', 'analisando quebra') group by itens_venda.sku);

update production.out_safety_stock set items_pending_procurement = 0 where items_pending_procurement is null;

select  'Safety Stock: stop',now();

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `overlapping_sku`()
begin

delete from production.overlapping_sku;

call production.catalog_visible;

call production_co.catalog_visible;

call production_pe.catalog_visible;

call production_ve.catalog_visible;

-- Mexico

insert into production.overlapping_sku(country, sku_config, sku_simple, product_name, brand, model, upc, barcode_ean) select 'Mexico', c.sku, s.sku, c.name, b.name, c.model, case when s.barcode_ean is not null then 1 else 0 end, s.barcode_ean from bob_live_mx.catalog_config c, bob_live_mx.catalog_simple s, bob_live_mx.catalog_brand b where c.id_catalog_config = s.fk_catalog_config and c.fk_catalog_brand=b.id_catalog_brand;

update production.overlapping_sku p inner join bob_live_mx.catalog_config conf on p.sku_config=conf.sku inner join bob_live_mx.catalog_attribute_option_global_category c on c.id_catalog_attribute_option_global_category=conf.fk_catalog_attribute_option_global_category set p.main_category = c.name where country='Mexico';

update production.overlapping_sku p inner join bob_live_mx.catalog_config conf on p.sku_config=conf.sku inner join bob_live_mx.catalog_attribute_option_global_sub_category c on c.id_catalog_attribute_option_global_sub_category=conf.fk_catalog_attribute_option_global_sub_category set p.sub_category = c.name where country='Mexico';

update production.overlapping_sku p inner join bob_live_mx.catalog_config conf on p.sku_config=conf.sku inner join bob_live_mx.catalog_attribute_option_global_sub_sub_category c on c.id_catalog_attribute_option_global_sub_sub_category=conf.fk_catalog_attribute_option_global_sub_sub_category set p.sub_sub_category = c.name where country='Mexico';

update production.overlapping_sku m inner join production.catalog_visible v on m.sku_simple=v.sku_simple set visible = 1 where country='Mexico';

update production.overlapping_sku m set visible = 0 where visible is null and country='Mexico';

-- Colombia

insert into production.overlapping_sku(country, sku_config, sku_simple, product_name, brand, model, upc, barcode_ean) select 'Colombia', c.sku, s.sku, c.name, b.name, c.model, case when s.barcode_ean is not null then 1 else 0 end, s.barcode_ean from bob_live_co.catalog_config c, bob_live_co.catalog_simple s, bob_live_co.catalog_brand b where c.id_catalog_config = s.fk_catalog_config and c.fk_catalog_brand=b.id_catalog_brand;

update production.overlapping_sku p inner join bob_live_co.catalog_config conf on p.sku_config=conf.sku inner join bob_live_co.catalog_attribute_option_global_category c on c.id_catalog_attribute_option_global_category=conf.fk_catalog_attribute_option_global_category set p.main_category = c.name where country='Colombia';

update production.overlapping_sku p inner join bob_live_co.catalog_config conf on p.sku_config=conf.sku inner join bob_live_co.catalog_attribute_option_global_sub_category c on c.id_catalog_attribute_option_global_sub_category=conf.fk_catalog_attribute_option_global_sub_category set p.sub_category = c.name where country='Colombia';

update production.overlapping_sku p inner join bob_live_co.catalog_config conf on p.sku_config=conf.sku inner join bob_live_co.catalog_attribute_option_global_sub_sub_category c on c.id_catalog_attribute_option_global_sub_sub_category=conf.fk_catalog_attribute_option_global_sub_sub_category set p.sub_sub_category = c.name where country='Colombia';

update production.overlapping_sku m inner join production_co.catalog_visible v on m.sku_simple=v.sku_simple set visible = 1 where country='Colombia';

update production.overlapping_sku m set visible = 0 where visible is null and country='Colombia';

-- Peru

insert into production.overlapping_sku(country, sku_config, sku_simple, product_name, brand, model, upc, barcode_ean) select 'Peru', c.sku, s.sku, c.name, b.name, c.model, case when s.barcode_ean is not null then 1 else 0 end, s.barcode_ean from bob_live_pe.catalog_config c, bob_live_pe.catalog_simple s, bob_live_pe.catalog_brand b where c.id_catalog_config = s.fk_catalog_config and c.fk_catalog_brand=b.id_catalog_brand;

update production.overlapping_sku p inner join bob_live_pe.catalog_config conf on p.sku_config=conf.sku inner join bob_live_pe.catalog_attribute_option_global_category c on c.id_catalog_attribute_option_global_category=conf.fk_catalog_attribute_option_global_category set p.main_category = c.name where country='Peru';

update production.overlapping_sku p inner join bob_live_pe.catalog_config conf on p.sku_config=conf.sku inner join bob_live_pe.catalog_attribute_option_global_sub_category c on c.id_catalog_attribute_option_global_sub_category=conf.fk_catalog_attribute_option_global_sub_category set p.sub_category = c.name where country='Peru';

update production.overlapping_sku p inner join bob_live_pe.catalog_config conf on p.sku_config=conf.sku inner join bob_live_pe.catalog_attribute_option_global_sub_sub_category c on c.id_catalog_attribute_option_global_sub_sub_category=conf.fk_catalog_attribute_option_global_sub_sub_category set p.sub_sub_category = c.name where country='Peru';

update production.overlapping_sku m inner join production_pe.catalog_visible v on m.sku_simple=v.sku_simple set visible = 1 where country='Peru';

update production.overlapping_sku m set visible = 0 where visible is null and country='Peru';

-- Venezuela

insert into production.overlapping_sku(country, sku_config, sku_simple, product_name, brand, model, upc, barcode_ean) select 'Venezuela', c.sku, s.sku, c.name, b.name, c.model, case when s.barcode_ean is not null then 1 else 0 end, s.barcode_ean from bob_live_ve.catalog_config c, bob_live_ve.catalog_simple s, bob_live_ve.catalog_brand b where c.id_catalog_config = s.fk_catalog_config and c.fk_catalog_brand=b.id_catalog_brand;

update production.overlapping_sku p inner join bob_live_ve.catalog_config conf on p.sku_config=conf.sku inner join bob_live_ve.catalog_attribute_option_global_category c on c.id_catalog_attribute_option_global_category=conf.fk_catalog_attribute_option_global_category set p.main_category = c.name where country='Venezuela';

update production.overlapping_sku p inner join bob_live_ve.catalog_config conf on p.sku_config=conf.sku inner join bob_live_ve.catalog_attribute_option_global_sub_category c on c.id_catalog_attribute_option_global_sub_category=conf.fk_catalog_attribute_option_global_sub_category set p.sub_category = c.name where country='Venezuela';

update production.overlapping_sku p inner join bob_live_ve.catalog_config conf on p.sku_config=conf.sku inner join bob_live_ve.catalog_attribute_option_global_sub_sub_category c on c.id_catalog_attribute_option_global_sub_sub_category=conf.fk_catalog_attribute_option_global_sub_sub_category set p.sub_sub_category = c.name where country='Venezuela';

update production.overlapping_sku m inner join production_ve.catalog_visible v on m.sku_simple=v.sku_simple set visible = 1 where country='Venezuela';

update production.overlapping_sku m set visible = 0 where visible is null and country='Venezuela';

-- Overlapping Product
-- Mexico

delete from production.overlapping_sku_lookup;

insert into production.overlapping_sku_lookup(country, product_name, brand, model, barcode_ean) select country, product_name, brand, model, barcode_ean from production.overlapping_sku;

update production.overlapping_sku set mexico_product=1 where country='Mexico';

update production.overlapping_sku o inner join production.overlapping_sku_lookup l on o.product_name=l.product_name and o.brand=l.brand and o.model=l.model set colombia_product=1 where o.country='Mexico' and l.country='Colombia';

update production.overlapping_sku set colombia_product = 0 where colombia_product is null and country='Mexico';

update production.overlapping_sku o inner join production.overlapping_sku_lookup l on o.product_name=l.product_name and o.brand=l.brand and o.model=l.model set peru_product=1 where o.country='Mexico' and l.country='Peru';

update production.overlapping_sku set peru_product = 0 where peru_product is null and country='Mexico';

update production.overlapping_sku o inner join production.overlapping_sku_lookup l on o.product_name=l.product_name and o.brand=l.brand and o.model=l.model set venezuela_product=1 where o.country='Mexico' and l.country='Venezuela';

update production.overlapping_sku set venezuela_product = 0 where venezuela_product is null and country='Mexico';

-- Colombia

update production.overlapping_sku set colombia_product=1 where country='Colombia';

update production.overlapping_sku o inner join production.overlapping_sku_lookup l on o.product_name=l.product_name and o.brand=l.brand and o.model=l.model set mexico_product=1 where o.country='Colombia' and l.country='Mexico';

update production.overlapping_sku set mexico_product = 0 where mexico_product is null and country='Colombia';

update production.overlapping_sku o inner join production.overlapping_sku_lookup l on o.product_name=l.product_name and o.brand=l.brand and o.model=l.model set peru_product=1 where o.country='Colombia' and l.country='Peru';

update production.overlapping_sku set peru_product = 0 where peru_product is null and country='Colombia';

update production.overlapping_sku o inner join production.overlapping_sku_lookup l on o.product_name=l.product_name and o.brand=l.brand and o.model=l.model set venezuela_product=1 where o.country='Colombia' and l.country='Venezuela';

update production.overlapping_sku set venezuela_product = 0 where venezuela_product is null and country='Colombia';

-- Peru

update production.overlapping_sku set peru_product=1 where country='Peru';

update production.overlapping_sku o inner join production.overlapping_sku_lookup l on o.product_name=l.product_name and o.brand=l.brand and o.model=l.model set mexico_product=1 where o.country='Peru' and l.country='Mexico';

update production.overlapping_sku set mexico_product = 0 where mexico_product is null and country='Peru';

update production.overlapping_sku o inner join production.overlapping_sku_lookup l on o.product_name=l.product_name and o.brand=l.brand and o.model=l.model set colombia_product=1 where o.country='Peru' and l.country='Colombia';

update production.overlapping_sku set colombia_product = 0 where colombia_product is null and country='Peru';

update production.overlapping_sku o inner join production.overlapping_sku_lookup l on o.product_name=l.product_name and o.brand=l.brand and o.model=l.model set venezuela_product=1 where o.country='Peru' and l.country='Venezuela';

update production.overlapping_sku set venezuela_product = 0 where venezuela_product is null and country='Peru';

-- Vemezuela

update production.overlapping_sku set venezuela_product=1 where country='Venezuela';

update production.overlapping_sku o inner join production.overlapping_sku_lookup l on o.product_name=l.product_name and o.brand=l.brand and o.model=l.model set mexico_product=1 where o.country='Venezuela' and l.country='Mexico';

update production.overlapping_sku set mexico_product = 0 where mexico_product is null and country='Venezuela';

update production.overlapping_sku o inner join production.overlapping_sku_lookup l on o.product_name=l.product_name and o.brand=l.brand and o.model=l.model set colombia_product=1 where o.country='Venezuela' and l.country='Colombia';

update production.overlapping_sku set colombia_product = 0 where colombia_product is null and country='Venezuela';

update production.overlapping_sku o inner join production.overlapping_sku_lookup l on o.product_name=l.product_name and o.brand=l.brand and o.model=l.model set peru_product=1 where o.country='Venezuela' and l.country='Peru';

update production.overlapping_sku set peru_product = 0 where peru_product is null and country='Venezuela';

-- Overlapping UPC
-- Mexico

update production.overlapping_sku set mexico_upc=1 where country='Mexico';

update production.overlapping_sku o inner join production.overlapping_sku_lookup l on o.barcode_ean=l.barcode_ean set colombia_upc=1 where o.country='Mexico' and l.country='Colombia';

update production.overlapping_sku set colombia_upc = 0 where colombia_upc is null and country='Mexico';

update production.overlapping_sku o inner join production.overlapping_sku_lookup l on o.barcode_ean=l.barcode_ean set peru_upc=1 where o.country='Mexico' and l.country='Peru';

update production.overlapping_sku set peru_upc = 0 where peru_upc is null and country='Mexico';

update production.overlapping_sku o inner join production.overlapping_sku_lookup l on o.barcode_ean=l.barcode_ean set venezuela_upc=1 where o.country='Mexico' and l.country='Venezuela';

update production.overlapping_sku set venezuela_upc = 0 where venezuela_upc is null and country='Mexico';

-- Colombia

update production.overlapping_sku set colombia_upc=1 where country='Colombia';

update production.overlapping_sku o inner join production.overlapping_sku_lookup l on o.barcode_ean=l.barcode_ean set mexico_upc=1 where o.country='Colombia' and l.country='Mexico';

update production.overlapping_sku set mexico_upc = 0 where mexico_upc is null and country='Colombia';

update production.overlapping_sku o inner join production.overlapping_sku_lookup l on o.barcode_ean=l.barcode_ean set peru_upc=1 where o.country='Colombia' and l.country='Peru';

update production.overlapping_sku set peru_upc = 0 where peru_upc is null and country='Colombia';

update production.overlapping_sku o inner join production.overlapping_sku_lookup l on o.barcode_ean=l.barcode_ean set venezuela_upc=1 where o.country='Colombia' and l.country='Venezuela';

update production.overlapping_sku set venezuela_upc = 0 where venezuela_upc is null and country='Colombia';

-- Peru

update production.overlapping_sku set peru_upc=1 where country='Peru';

update production.overlapping_sku o inner join production.overlapping_sku_lookup l on o.barcode_ean=l.barcode_ean set mexico_upc=1 where o.country='Peru' and l.country='Mexico';

update production.overlapping_sku set mexico_upc = 0 where mexico_upc is null and country='Peru';

update production.overlapping_sku o inner join production.overlapping_sku_lookup l on o.barcode_ean=l.barcode_ean set colombia_upc=1 where o.country='Peru' and l.country='Colombia';

update production.overlapping_sku set colombia_upc = 0 where colombia_upc is null and country='Peru';

update production.overlapping_sku o inner join production.overlapping_sku_lookup l on o.barcode_ean=l.barcode_ean set venezuela_upc=1 where o.country='Peru' and l.country='Venezuela';

update production.overlapping_sku set venezuela_upc = 0 where venezuela_upc is null and country='Peru';

-- Vemezuela

update production.overlapping_sku set venezuela_upc=1 where country='Venezuela';

update production.overlapping_sku o inner join production.overlapping_sku_lookup l on o.barcode_ean=l.barcode_ean set mexico_upc=1 where o.country='Venezuela' and l.country='Mexico';

update production.overlapping_sku set mexico_upc = 0 where mexico_upc is null and country='Venezuela';

update production.overlapping_sku o inner join production.overlapping_sku_lookup l on o.barcode_ean=l.barcode_ean set colombia_upc=1 where o.country='Venezuela' and l.country='Colombia';

update production.overlapping_sku set colombia_upc = 0 where colombia_upc is null and country='Venezuela';

update production.overlapping_sku o inner join production.overlapping_sku_lookup l on o.barcode_ean=l.barcode_ean set peru_upc=1 where o.country='Venezuela' and l.country='Peru';

update production.overlapping_sku set peru_upc = 0 where peru_upc is null and country='Venezuela';

update production.overlapping_sku o set num_approved_rating = (select count(r.status) from bob_live_mx.catalog_config c, bob_live_mx.rating_review r where c.id_catalog_config=r.fk_catalog_config and c.sku=o.sku_config and r.status = 'APPROVED') where country='Mexico';

update production.overlapping_sku o set num_approved_rating = (select count(r.status) from bob_live_co.catalog_config c, bob_live_co.rating_review r where c.id_catalog_config=r.fk_catalog_config and c.sku=o.sku_config and r.status = 'APPROVED') where country='Colombia';

update production.overlapping_sku o set num_approved_rating = (select count(r.status) from bob_live_pe.catalog_config c, bob_live_pe.rating_review r where c.id_catalog_config=r.fk_catalog_config and c.sku=o.sku_config and r.status = 'APPROVED') where country='Peru';

update production.overlapping_sku o set num_approved_rating = (select count(r.status) from bob_live_ve.catalog_config c, bob_live_ve.rating_review r where c.id_catalog_config=r.fk_catalog_config and c.sku=o.sku_config and r.status = 'APPROVED') where country='Venezuela';


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `product_pricing_history`()
begin

-- delete from production.product_pricing_history;

call production.catalog_visible;

insert into production.product_pricing_history(date, sku_config, sku_simple, product_name, price) select date_sub(curdate(), interval 1 day), sku_config, sku_simple, name, price from production.catalog_visible;

update production.product_pricing_history p inner join bob_live_mx.catalog_config conf on p.sku_config=conf.sku inner join bob_live_mx.catalog_attribute_option_global_category c on c.id_catalog_attribute_option_global_category=conf.fk_catalog_attribute_option_global_category set p.category = c.name where date=date_sub(curdate(), interval 1 day);

update production.product_pricing_history p inner join bob_live_mx.catalog_config conf on p.sku_config=conf.sku inner join bob_live_mx.catalog_attribute_option_global_sub_category c on c.id_catalog_attribute_option_global_sub_category=conf.fk_catalog_attribute_option_global_sub_category set p.sub_category = c.name where date=date_sub(curdate(), interval 1 day);

update production.product_pricing_history p inner join bob_live_mx.catalog_config conf on p.sku_config=conf.sku inner join bob_live_mx.catalog_attribute_option_global_sub_sub_category c on c.id_catalog_attribute_option_global_sub_sub_category=conf.fk_catalog_attribute_option_global_sub_sub_category set p.sub_sub_category = c.name where date=date_sub(curdate(), interval 1 day);

update production.product_pricing_history p inner join bob_live_mx.catalog_config conf on p.sku_config=conf.sku inner join bob_live_mx.catalog_attribute_option_global_head_buyer_name c on c.id_catalog_attribute_option_global_head_buyer_name=conf.fk_catalog_attribute_option_global_head_buyer_name set p.head_buyer = c.name where date=date_sub(curdate(), interval 1 day);

update production.product_pricing_history p inner join bob_live_mx.catalog_config conf on p.sku_config=conf.sku inner join bob_live_mx.catalog_attribute_option_global_buyer_name c on c.id_catalog_attribute_option_global_buyer_name=conf.fk_catalog_attribute_option_global_buyer_name set p.buyer = c.name where date=date_sub(curdate(), interval 1 day);

update production.product_pricing_history p inner join bob_live_mx.catalog_simple s on p.sku_simple=s.sku set p.original_price=s.original_price where date=date_sub(curdate(), interval 1 day);

update production.product_pricing_history p inner join bob_live_mx.catalog_simple s on p.sku_simple=s.sku set p.special_price=s.special_price where curdate() between s.special_from_date and s.special_to_date and date=date_sub(curdate(), interval 1 day); 

update production.product_pricing_history set `% discount (price)` = 1-(special_price/price) where date=date_sub(curdate(), interval 1 day);

update production.product_pricing_history set `% discount (RRP)` =  1-(price/original_price) where date=date_sub(curdate(), interval 1 day);

update production.product_pricing_history p set PC1 = (select sum(PC1) from production.tbl_order_detail t where oac=1 and p.sku_simple=t.sku and t.date=date_sub(curdate(), interval 1 day) group by t.sku) where date=date_sub(curdate(), interval 1 day);

update production.product_pricing_history p set PC2 = (select sum(PC2) from production.tbl_order_detail t where oac=1 and p.sku_simple=t.sku and t.date=date_sub(curdate(), interval 1 day) group by t.sku) where date=date_sub(curdate(), interval 1 day);

update production.product_pricing_history p set number_sales = (select count(sku) from production.tbl_order_detail t where oac=1 and p.sku_simple=t.sku and t.date=date_sub(curdate(), interval 1 day) group by t.sku) where date=date_sub(curdate(), interval 1 day); 

update production.product_pricing_history p inner join bob_live_mx.catalog_config c on c.sku=p.sku_config inner join bob_live_mx.catalog_brand b on b.id_catalog_brand=c.fk_catalog_brand set p.brand=b.name, p.date_created=c.created_at where date=date_sub(curdate(), interval 1 day);

update production.product_pricing_history p inner join bob_live_mx.catalog_simple c on c.sku=p.sku_simple set p.cost=c.cost where date=date_sub(curdate(), interval 1 day);

update production.product_pricing_history p set net_revenue = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production.tbl_order_detail t where oac=1 and p.sku_simple=t.sku and t.date=date_sub(curdate(), interval 1 day) group by t.sku) where date=date_sub(curdate(), interval 1 day);

update production.product_pricing_history  set `% PC1` = PC1/net_revenue, `% PC2` = PC2/net_revenue where date=date_sub(curdate(), interval 1 day);

update production.product_pricing_history p set num_in_stock = (select sum(in_stock) from rafael.out_stock_hist o where o.sku_simple=p.sku_simple) where date=date_sub(curdate(), interval 1 day); 

update production.subcategory_visits set sub_category = substr(substr(substr(substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1),locate('.', substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1))+1) ,locate('.', substr(substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1),locate('.', substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1))+1))+1) , locate('.', substr(substr(substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1),locate('.', substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1))+1) ,locate('.', substr(substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1),locate('.', substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1))+1))+1))+1) where sub_category is null;

update production.subcategory_product_clicks set sub_category = substr(substr(substr(substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1),locate('.', substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1))+1) ,locate('.', substr(substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1),locate('.', substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1))+1))+1) , locate('.', substr(substr(substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1),locate('.', substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1))+1) ,locate('.', substr(substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1),locate('.', substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1))+1))+1))+1) where sub_category is null;

update production.subcategory_product_clicks set sku_config = substring_index(substr(events, locate(':',events)+1), '.', 1) where sku_config is null;

delete from production.webtrekk_pricing;

insert into production.webtrekk_pricing(sku_config, sku_visits, impressions, micro_conversion_rate) select sku_config, visits, impressions, conversion_rate/100 from production.products_pi_visits_cr where date=date_sub(curdate(), interval 1 day); 

update production.webtrekk_pricing w set subcategory = (select sub_category from production.subcategory_product_clicks c where w.sku_config=c.sku_config and c.date=date_sub(curdate(), interval 1 day) group by w.sku_config), clicks = (select sum(clicks) from production.subcategory_product_clicks c where w.sku_config=c.sku_config and c.date=date_sub(curdate(), interval 1 day));

update production.webtrekk_pricing w set subcategory_visits = (select sum(visits) from production.subcategory_visits c where w.subcategory=c.sub_category and c.date=date_sub(curdate(), interval 1 day));

update production.webtrekk_pricing set CTR= case when clicks/subcategory_visits>1 then 1 else clicks/subcategory_visits end;

update production.webtrekk_pricing set macro_conversion_rate = micro_conversion_rate*CTR;

update production.product_pricing_history p inner join production.webtrekk_pricing w on p.sku_config=w.sku_config set p.sku_visits=w.sku_visits, p.subcategory_visits=w.subcategory_visits, p.clicks=w.clicks, p.CTR=w.CTR, p.impressions=w.impressions, p.micro_conversion_rate=w.micro_conversion_rate, p.macro_conversion_rate=w.macro_conversion_rate where p.date=date_sub(curdate(), interval 1 day);

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `retargeting_affiliates`()
begin

select  'Retargeting Affiliates Optimization Tool: start',now();

call production.retargeting_affiliates_mx;

call production.retargeting_affiliates_co;

call production.retargeting_affiliates_pe;

call production.retargeting_affiliates_ve;

select  'Retargeting Affiliates Optimization Tool: end',now();

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `retargeting_affiliates_co`()
begin

set @days:=30;

delete from production.retargeting_affiliates_co where datediff(curdate(), date)<@days;

-- FBX

insert into production.retargeting_affiliates_co(channel, date, source, medium, campaign, visits, bounce, cart) select 'FBX', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_co c where source='facebook' and medium='retargeting' and datediff(curdate(), date)<@days group by c.date, c.source, c.medium, c.campaign;

create table production.temporary_retargeting_affiliates_co(
date date,
impressions int,
clicks int,
cost float);

create index date on production.temporary_retargeting_affiliates_co(date);

create table production.final_co(
date date,
source varchar(255),
medium varchar(255),
campaign varchar(255),
times float,
visits int,
day_visits int);

create index channel on production.final_co(date, source, medium, campaign);

insert into production.temporary_retargeting_affiliates_co(date) select date from production.retargeting_affiliates_co where channel = 'FBX' and datediff(curdate(), date)<@days group by date;

update production.temporary_retargeting_affiliates_co r set cost = (select sum(a.cost)+sum(b.cost) from marketing_report.sociomantic_fbx_co a, marketing_report.sociomantic_fbx_loyalty_co b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_co r set impressions = (select sum(a.impressions)+sum(b.impressions) from marketing_report.sociomantic_fbx_co a, marketing_report.sociomantic_fbx_loyalty_co b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_co r set clicks = (select sum(a.clicks)+sum(b.clicks) from marketing_report.sociomantic_fbx_co a, marketing_report.sociomantic_fbx_loyalty_co b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

insert into production.final_co(date, source, medium, campaign, visits) select date, source, medium, campaign, visits from production.retargeting_affiliates_co where channel='FBX' and datediff(curdate(), date)<@days;

update production.final_co f set day_visits = (select sum(visits) from production.retargeting_affiliates_co r where r.date=f.date group by date) where datediff(curdate(), date)<@days;

update production.final_co f set times = visits/day_visits;

update production.retargeting_affiliates_co r set cost = (select t.cost*x.times from production.temporary_retargeting_affiliates_co t, production.final_co x where t.date=r.date and x.date=t.date and r.channel='FBX' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='FBX' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co r set impressions = (select t.impressions*x.times from production.temporary_retargeting_affiliates_co t, production.final_co x where t.date=r.date and x.date=t.date and r.channel='FBX' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='FBX' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co r set clicks = (select t.clicks*x.times from production.temporary_retargeting_affiliates_co t, production.final_co x where t.date=r.date and x.date=t.date and r.channel='FBX' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='FBX' and datediff(curdate(), date)<@days;

delete from production.temporary_retargeting_affiliates_co;

delete from production.final_co;

-- Sociomantic

insert into production.retargeting_affiliates_co(channel, date, source, medium, campaign, visits, bounce, cart) select 'Sociomantic', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_co c where source like '%socioman%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

insert into production.temporary_retargeting_affiliates_co(date) select date from production.retargeting_affiliates_co where channel = 'Sociomantic' and datediff(curdate(), date)<@days group by date;

update production.temporary_retargeting_affiliates_co r set cost = (select sum(a.cost)+sum(b.cost) from marketing_report.sociomantic_basket_co a, marketing_report.sociomantic_loyalty_co b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_co r set impressions = (select sum(a.impressions)+sum(b.impressions) from marketing_report.sociomantic_basket_co a, marketing_report.sociomantic_loyalty_co b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_co r set clicks = (select sum(a.clicks)+sum(b.clicks) from marketing_report.sociomantic_basket_co a, marketing_report.sociomantic_loyalty_co b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

insert into production.final_co(date, source, medium, campaign, visits) select date, source, medium, campaign, visits from production.retargeting_affiliates_co where channel='Sociomantic' and datediff(curdate(), date)<@days;

update production.final_co f set day_visits = (select sum(visits) from production.retargeting_affiliates_co r where r.channel='Sociomantic' and r.date=f.date group by date) where datediff(curdate(), date)<@days;

update production.final_co f set times = visits/day_visits;

update production.retargeting_affiliates_co r set cost = (select t.cost*x.times from production.temporary_retargeting_affiliates_co t, production.final_co x where t.date=r.date and x.date=t.date and r.channel='Sociomantic' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='Sociomantic' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co r set impressions = (select t.impressions*x.times from production.temporary_retargeting_affiliates_co t, production.final_co x where t.date=r.date and x.date=t.date and r.channel='Sociomantic' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='Sociomantic' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co r set clicks = (select t.clicks*x.times from production.temporary_retargeting_affiliates_co t, production.final_co x where t.date=r.date and x.date=t.date and r.channel='Sociomantic' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='Sociomantic' and datediff(curdate(), date)<@days;

-- Criteo

insert into production.retargeting_affiliates_co(channel, date, source, medium, campaign, visits, bounce, cart) select 'Criteo', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_co c where source like '%criteo%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

insert into production.temporary_retargeting_affiliates_co(date) select date from production.retargeting_affiliates_co where channel = 'Criteo' and datediff(curdate(), date)<@days group by date;

update production.temporary_retargeting_affiliates_co r set cost = (select sum(a.cost) from marketing_report.criteo_co a where a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_co r set impressions = (select sum(a.impressions) from marketing_report.criteo_co a where a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_co r set clicks = (select sum(a.clicks) from marketing_report.criteo_co a where a.date=r.date) where datediff(curdate(), date)<@days;

insert into production.final_co(date, source, medium, campaign, visits) select date, source, medium, campaign, visits from production.retargeting_affiliates_co where channel='Criteo' and datediff(curdate(), date)<@days;

update production.final_co f set day_visits = (select sum(visits) from production.retargeting_affiliates_co r where r.channel='Criteo' and r.date=f.date group by date) where datediff(curdate(), date)<@days;

update production.final_co f set times = visits/day_visits;

update production.retargeting_affiliates_co r set cost = (select t.cost*x.times from production.temporary_retargeting_affiliates_co t, production.final_co x where t.date=r.date and x.date=t.date and r.channel='Criteo' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='Criteo' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co r set impressions = (select t.impressions*x.times from production.temporary_retargeting_affiliates_co t, production.final_co x where t.date=r.date and x.date=t.date and r.channel='Criteo' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='Criteo' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co r set clicks = (select t.clicks*x.times from production.temporary_retargeting_affiliates_co t, production.final_co x where t.date=r.date and x.date=t.date and r.channel='Criteo' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='Criteo' and datediff(curdate(), date)<@days;

drop table production.temporary_retargeting_affiliates_co;

drop table production.final_co;

-- Vizury

insert into production.retargeting_affiliates_co(channel, date, source, medium, campaign, visits, bounce, cart) select 'Vizury', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_co c where source like '%vizury%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_co r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_co.tbl_order_detail t, SEM.transaction_id_co x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Vizury' and datediff(curdate(), date)<@days;

-- Veinteractive

insert into production.retargeting_affiliates_co(channel, date, source, medium, campaign, visits, bounce, cart) select 'Veinteractive', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_co c where source like '%veinteractive%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_co r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_co.tbl_order_detail t, SEM.transaction_id_co x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Veinteractive' and datediff(curdate(), date)<@days;

-- GLG

insert into production.retargeting_affiliates_co(channel, date, source, medium, campaign, visits, bounce, cart) select 'GLG', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_co c where source like '%glg%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_co r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_co.tbl_order_detail t, SEM.transaction_id_co x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='GLG' and datediff(curdate(), date)<@days;

-- Pampa Network

insert into production.retargeting_affiliates_co(channel, date, source, medium, campaign, visits, bounce, cart) select 'Pampa Network', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_co c where source like '%pampa%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_co r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_co.tbl_order_detail t, SEM.transaction_id_co x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Pampa Network' and datediff(curdate(), date)<@days;

-- SOLOCPM/Main ADV

insert into production.retargeting_affiliates_co(channel, date, source, medium, campaign, visits, bounce, cart) select 'Main ADV', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_co c where (source like '%maindadv%' or source like '%solocpm%') and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_co r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_co.tbl_order_detail t, SEM.transaction_id_co x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Main ADV' and datediff(curdate(), date)<@days;

-- MyThings

insert into production.retargeting_affiliates_co(channel, date, source, medium, campaign, visits, bounce, cart) select 'My Things', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_co c where source like '%mythings%' and medium like '%retargeting%' and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_co r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_co.tbl_order_detail t, SEM.transaction_id_co x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='My Things' and datediff(curdate(), date)<@days;

-- Trade Tracker

insert into production.retargeting_affiliates_co(channel, date, source, medium, campaign, visits, bounce, cart) select 'Trade Tracker', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_co c where (source like '%tradetracker%' or source = 'Trade Tracker') and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_co r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_co.tbl_order_detail t, SEM.transaction_id_co x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Trade Tracker' and datediff(curdate(), date)<@days;

-- Buscape

insert into production.retargeting_affiliates_co(channel, date, source, medium, campaign, visits, bounce, cart) select 'Buscape', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_co c where source like '%buscape%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

-- Mercado Libre

insert into production.retargeting_affiliates_co(channel, date, source, medium, campaign, visits, bounce, cart) select 'Mercado Libre', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_co c where source like '%mercadolibre%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

-- Promodescuentos

insert into production.retargeting_affiliates_co(channel, date, source, medium, campaign, visits, bounce, cart) select 'Promodescuentos', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_co c where source like '%promodescuentos%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

-- ILoveCPA

insert into production.retargeting_affiliates_co(channel, date, source, medium, campaign, visits, bounce, cart) select 'ILoveCPA', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_co c where source like '%ilovecpa%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_co r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_co.tbl_order_detail t, SEM.transaction_id_co x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='ILoveCPA' and datediff(curdate(), date)<@days;

-- BOB 

update production.retargeting_affiliates_co r set net_transactions = (select count(distinct transaction_id) from production_co.tbl_order_detail t, SEM.transaction_id_co x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co r set gross_transactions = (select count(distinct transaction_id) from production_co.tbl_order_detail t, SEM.transaction_id_co x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.obc=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co r set net_revenue = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_co.tbl_order_detail t, SEM.transaction_id_co x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co r set gross_revenue = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_co.tbl_order_detail t, SEM.transaction_id_co x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.obc=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co r set PC2 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production_co.tbl_order_detail t, SEM.transaction_id_co x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co r set new_customers = (select sum(new_customers) from production_co.tbl_order_detail t, SEM.transaction_id_co x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co r set new_customers_gross = (select sum(new_customers_gross) from production_co.tbl_order_detail t, SEM.transaction_id_co x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.obc=1) where datediff(curdate(), date)<@days;

-- Cohort

create table production.temporary_co(
date date,
source varchar(255),
medium varchar(255),
campaign varchar(255),
custid int,
transaction_id varchar(255)
);

create index temporary on production.temporary_co(date, source, medium, campaign);
create index transaction_id on production.temporary_co(transaction_id);

insert into production.temporary_co select distinct x.date, x.source, x.medium, x.campaign, t.custid, x.transaction_id from SEM.transaction_id_co x, production_co.tbl_order_detail t where x.transaction_id=t.order_nr and t.new_customers !=0 and t.oac=1 and datediff(curdate(), t.date)<@days;

update production.retargeting_affiliates_co a set net_transactions_30 = (select count(distinct t.order_nr) from production_co.tbl_order_detail t, production.temporary_co x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co a set net_revenue_30 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_co.tbl_order_detail t, production.temporary_co x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co a set PC2_30 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production_co.tbl_order_detail t, production.temporary_co x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co a set net_transactions_60 = (select count(distinct t.order_nr) from production_co.tbl_order_detail t, production.temporary_co x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co a set net_revenue_60 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_co.tbl_order_detail t, production.temporary_co x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co a set PC2_60 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production_co.tbl_order_detail t, production.temporary_co x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co a set net_transactions_90 = (select count(distinct t.order_nr) from production_co.tbl_order_detail t, production.temporary_co x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co a set net_revenue_90 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_co.tbl_order_detail t, production.temporary_co x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers = 0 and t.oac=1);

update production.retargeting_affiliates_co a set PC2_90 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production_co.tbl_order_detail t, production.temporary_co x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co a set net_transactions_120 = (select count(distinct t.order_nr) from production_co.tbl_order_detail t, production.temporary_co x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co a set net_revenue_120 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_co.tbl_order_detail t, production.temporary_co x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co a set PC2_120 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production_co.tbl_order_detail t, production.temporary_co x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co a set net_transactions_150 = (select count(distinct t.order_nr) from production_co.tbl_order_detail t, production.temporary_co x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co a set net_revenue_150 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_co.tbl_order_detail t, production.temporary_co x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co a set PC2_150 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production_co.tbl_order_detail t, production.temporary_co x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co a set net_transactions_180 = (select count(distinct t.order_nr) from production_co.tbl_order_detail t, production.temporary_co x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co a set net_revenue_180 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_co.tbl_order_detail t, production.temporary_co x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co a set PC2_180 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production_co.tbl_order_detail t, production.temporary_co x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days;

drop table production.temporary_co;

-- Date

update production.retargeting_affiliates_co set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=SEM.week_iso(date) where datediff(curdate(), date)<@days;

-- Exchange Rate

update production.retargeting_affiliates_co s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, net_revenue_30=net_revenue_30/xr, net_revenue_60=net_revenue_60/xr, net_revenue_90=net_revenue_90/xr, net_revenue_120=net_revenue_120/xr, net_revenue_150=net_revenue_150/xr, net_revenue_180=net_revenue_180/xr, PC2=PC2/xr, PC2_30=PC2_30/xr, PC2_60=PC2_60/xr, PC2_90=PC2_90/xr, PC2_120=PC2_120/xr, PC2_150=PC2_150/xr, PC2_180=PC2_180/xr where r.country='COL' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co set cost = cost/1.23 where channel = 'Criteo' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co set cost = cost/2200 where yrmonth = 201304 and channel in ('Vizury', 'Veinteractive', 'GLG', 'Pampa Network', 'ILoveCPA', 'Trade Tracker', 'Main ADV', 'My Things') and datediff(curdate(), date)<@days; 

update production.retargeting_affiliates_co set cost = cost/2350 where yrmonth between 201305 and 201309 and channel in ('Vizury', 'Veinteractive', 'GLG', 'Pampa Network', 'ILoveCPA', 'Trade Tracker', 'Main ADV', 'My Things') and datediff(curdate(), date)<@days; 

update production.retargeting_affiliates_co set cost = cost/2200 where yrmonth >= 201310 and channel in ('Vizury', 'Veinteractive', 'GLG', 'Pampa Network', 'ILoveCPA', 'Trade Tracker', 'Main ADV', 'My Things') and datediff(curdate(), date)<@days; 

-- Cost

update production.retargeting_affiliates_co set cost = cost*0.08*2.5 where channel = 'Vizury' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co set cost = cost*0.1 where channel = 'Trade Tracker' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co set cost = cost*0.1 where channel = 'Main ADV' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co set cost = cost*0.1 where channel = 'My Things' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co set cost = cost*0.08*2.5 where channel = 'Veinteractive' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co set cost = cost*0.1*2.5 where channel = 'GLG' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co set cost = cost*0.15*2.5 where channel = 'Pampa Network' and date <= '2013-05-12' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co set cost = cost*0.1*2.5 where channel = 'Pampa Network' and date > '2013-05-12' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_co set cost = cost*0.1*2.5 where channel = 'ILoveCPA' and datediff(curdate(), date)<@days;


-- NULL

update production.retargeting_affiliates_co  set gross_revenue=0 where gross_revenue is null;

update production.retargeting_affiliates_co  set net_revenue=0 where net_revenue is null;

update production.retargeting_affiliates_co  set new_customers=0 where new_customers is null;

update production.retargeting_affiliates_co  set new_customers_gross=0 where new_customers_gross is null;

update production.retargeting_affiliates_co set net_revenue_30=0 where net_revenue_30 is null;

update production.retargeting_affiliates_co set net_revenue_60=0 where net_revenue_60 is null;

update production.retargeting_affiliates_co set net_revenue_90=0 where net_revenue_90 is null;

update production.retargeting_affiliates_co set net_revenue_120=0 where net_revenue_120 is null;

update production.retargeting_affiliates_co set net_revenue_150=0 where net_revenue_150 is null;

update production.retargeting_affiliates_co set net_revenue_180=0 where net_revenue_180 is null;


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `retargeting_affiliates_mx`()
begin

set @days:=30;

delete from production.retargeting_affiliates_mx where datediff(curdate(), date)<@days;

-- FBX

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'FBX', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source='facebook' and medium='retargeting' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

create table production.temporary_retargeting_affiliates_mx(
date date,
impressions int,
clicks int,
cost float);

create index date on production.temporary_retargeting_affiliates_mx(date);

create table production.final_mx(
date date,
source varchar(255),
medium varchar(255),
campaign varchar(255),
times float,
visits int,
day_visits int);

create index channel on production.final_mx(date, source, medium, campaign);

insert into production.temporary_retargeting_affiliates_mx(date) select date from production.retargeting_affiliates_mx where channel = 'FBX' and datediff(curdate(), date)<@days group by date;

update production.temporary_retargeting_affiliates_mx r set cost = (select sum(a.cost)+sum(b.cost) from marketing_report.sociomantic_fbx_retargeting_mx a, marketing_report.sociomantic_fbx_loyalty_mx b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_mx r set impressions = (select sum(a.impressions)+sum(b.impressions) from marketing_report.sociomantic_fbx_retargeting_mx a, marketing_report.sociomantic_fbx_loyalty_mx b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_mx r set clicks = (select sum(a.clicks)+sum(b.clicks) from marketing_report.sociomantic_fbx_retargeting_mx a, marketing_report.sociomantic_fbx_loyalty_mx b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

insert into production.final_mx(date, source, medium, campaign, visits) select date, source, medium, campaign, visits from production.retargeting_affiliates_mx where channel='FBX' and datediff(curdate(), date)<@days;

update production.final_mx f set day_visits = (select sum(visits) from production.retargeting_affiliates_mx r where r.date=f.date group by date) where datediff(curdate(), date)<@days;

update production.final_mx f set times = visits/day_visits;

update production.retargeting_affiliates_mx r set cost = (select t.cost*x.times from production.temporary_retargeting_affiliates_mx t, production.final_mx x where t.date=r.date and x.date=t.date and r.channel='FBX' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='FBX' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx r set impressions = (select t.impressions*x.times from production.temporary_retargeting_affiliates_mx t, production.final_mx x where t.date=r.date and x.date=t.date and r.channel='FBX' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='FBX' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx r set clicks = (select t.clicks*x.times from production.temporary_retargeting_affiliates_mx t, production.final_mx x where t.date=r.date and x.date=t.date and r.channel='FBX' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='FBX' and datediff(curdate(), date)<@days;

delete from production.temporary_retargeting_affiliates_mx;

delete from production.final_mx;

-- Sociomantic

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Sociomantic', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%socioman%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

insert into production.temporary_retargeting_affiliates_mx(date) select date from production.retargeting_affiliates_mx where channel = 'Sociomantic' and datediff(curdate(), date)<@days group by date;

update production.temporary_retargeting_affiliates_mx r set cost = (select sum(a.cost)+sum(b.cost) from marketing_report.sociomantic_basket_mx a, marketing_report.sociomantic_loyalty_mx b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_mx r set impressions = (select sum(a.impressions)+sum(b.impressions) from marketing_report.sociomantic_basket_mx a, marketing_report.sociomantic_loyalty_mx b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_mx r set clicks = (select sum(a.clicks)+sum(b.clicks) from marketing_report.sociomantic_basket_mx a, marketing_report.sociomantic_loyalty_mx b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

insert into production.final_mx(date, source, medium, campaign, visits) select date, source, medium, campaign, visits from production.retargeting_affiliates_mx where channel='Sociomantic' and datediff(curdate(), date)<@days;

update production.final_mx f set day_visits = (select sum(visits) from production.retargeting_affiliates_mx r where r.channel='Sociomantic' and r.date=f.date group by date) where datediff(curdate(), date)<@days;

update production.final_mx f set times = visits/day_visits;

update production.retargeting_affiliates_mx r set cost = (select t.cost*x.times from production.temporary_retargeting_affiliates_mx t, production.final_mx x where t.date=r.date and x.date=t.date and r.channel='Sociomantic' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='Sociomantic' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx r set impressions = (select t.impressions*x.times from production.temporary_retargeting_affiliates_mx t, production.final_mx x where t.date=r.date and x.date=t.date and r.channel='Sociomantic' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='Sociomantic' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx r set clicks = (select t.clicks*x.times from production.temporary_retargeting_affiliates_mx t, production.final_mx x where t.date=r.date and x.date=t.date and r.channel='Sociomantic' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='Sociomantic' and datediff(curdate(), date)<@days;

delete from production.temporary_retargeting_affiliates_mx;

delete from production.final_mx;

-- Criteo

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Criteo', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%criteo%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

insert into production.temporary_retargeting_affiliates_mx(date) select date from production.retargeting_affiliates_mx where channel = 'Criteo' and datediff(curdate(), date)<@days group by date;

update production.temporary_retargeting_affiliates_mx r set cost = (select sum(a.cost) from marketing_report.criteo_mx a where a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_mx r set impressions = (select sum(a.impressions) from marketing_report.criteo_mx a where a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_mx r set clicks = (select sum(a.clicks) from marketing_report.criteo_mx a where a.date=r.date) where datediff(curdate(), date)<@days;

insert into production.final_mx(date, source, medium, campaign, visits) select date, source, medium, campaign, visits from production.retargeting_affiliates_mx where channel='Criteo' and datediff(curdate(), date)<@days;

update production.final_mx f set day_visits = (select sum(visits) from production.retargeting_affiliates_mx r where r.channel='Criteo' and r.date=f.date group by date) where datediff(curdate(), date)<@days;

update production.final_mx f set times = visits/day_visits;

update production.retargeting_affiliates_mx r set cost = (select t.cost*x.times from production.temporary_retargeting_affiliates_mx t, production.final_mx x where t.date=r.date and x.date=t.date and r.channel='Criteo' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='Criteo' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx r set impressions = (select t.impressions*x.times from production.temporary_retargeting_affiliates_mx t, production.final_mx x where t.date=r.date and x.date=t.date and r.channel='Criteo' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='Criteo' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx r set clicks = (select t.clicks*x.times from production.temporary_retargeting_affiliates_mx t, production.final_mx x where t.date=r.date and x.date=t.date and r.channel='Criteo' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='Criteo' and datediff(curdate(), date)<@days;

drop table production.temporary_retargeting_affiliates_mx;

drop table production.final_mx;

-- Vizury

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Vizury', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%vizury%' and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Vizury' and datediff(curdate(), date)<@days;

-- Veinteractive

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Veinteractive', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%veinteractive%' and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Veinteractive' and datediff(curdate(), date)<@days;

-- GLG

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'GLG', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%glg%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='GLG' and datediff(curdate(), date)<@days;

-- Pampa Network

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Pampa Network', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%pampa%' and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Pampa Network' and datediff(curdate(), date)<@days;

-- SOLOCPM/Main ADV

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Main ADV', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where (source like '%maindadv%' or source like '%solocpm%') and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Main ADV' and datediff(curdate(), date)<@days;

-- Mercado Libre

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Mercado Libre', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%mercadolibre%' and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

-- Metros Cubicos

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Metros Cubicos', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where (source like '%metroscubiocs%' or source like '%Metros_Cubicos%' or source like '%Metros+Cubicos%') and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

-- Promodescuentos

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Promodescuentos', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%promodescuentos%' and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Promodescuentos' and datediff(curdate(), date)<@days;

-- My Things

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'My Things', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%mythings%' and medium='retargeting' and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='My Things' and datediff(curdate(), date)<@days;

-- Avazu

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Avazu', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%avazu%' and medium='retargeting' and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Avazu' and datediff(curdate(), date)<@days;

-- Adjal

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Adjal', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%adjal%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Adjal' and datediff(curdate(), date)<@days;

-- ClickMagic

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'ClickMagic', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%clickmagic%' and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='ClickMagic' and datediff(curdate(), date)<@days;

-- Commission Junction

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Commission Junction', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%cj%' and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Commission Junction' and datediff(curdate(), date)<@days;

-- DiverselyDigital

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'DiverselyDigital', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where (source like '%diverselydigital%' or source like '%diverslydigital%') and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='DiverselyDigital' and datediff(curdate(), date)<@days;

-- ILoveCPA

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'ILoveCPA', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%ilovecpa%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='ILoveCPA' and datediff(curdate(), date)<@days;

-- Lomadee

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Lomadee', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%lomadee%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Lomadee' and datediff(curdate(), date)<@days;

-- PubliciIdeas

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'PubliciIdeas', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%publiciideas%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='PubliciIdeas' and datediff(curdate(), date)<@days;

-- Soicos

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Soicos', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%soicos%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Soicos' and datediff(curdate(), date)<@days;

-- Trade Tracker

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Trade Tracker', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where (source like '%tradetracker%' or source = 'Trade Tracker') and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_mx r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Trade Tracker' and datediff(curdate(), date)<@days;

-- Buscape

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Buscape', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%buscape%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

-- Freewoo

insert into production.retargeting_affiliates_mx(channel, date, source, medium, campaign, visits, bounce, cart) select 'Freewoo', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_mx c where source like '%freewoo%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

-- BOB 

update production.retargeting_affiliates_mx r set net_transactions = (select count(distinct transaction_id) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx r set gross_transactions = (select count(distinct transaction_id) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.obc=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx r set net_revenue = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx r set gross_revenue = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.obc=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx r set PC2 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx r set new_customers = (select sum(new_customers) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx r set new_customers_gross = (select sum(new_customers_gross) from production.tbl_order_detail t, SEM.transaction_id_mx x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.obc=1) where datediff(curdate(), date)<@days;

-- Cohort

create table production.temporary_mx(
date date,
source varchar(255),
medium varchar(255),
campaign varchar(255),
custid int,
transaction_id varchar(255)
);

create index temporary on production.temporary_mx(date, source, medium, campaign);
create index transaction_id on production.temporary_mx(transaction_id);

insert into production.temporary_mx select distinct x.date, x.source, x.medium, x.campaign, t.custid, x.transaction_id from SEM.transaction_id_mx x, production.tbl_order_detail t where x.transaction_id=t.order_nr and t.new_customers is not null and t.oac=1 and datediff(curdate(), t.date)<@days;

update production.retargeting_affiliates_mx a set net_transactions_30 = (select count(distinct t.order_nr) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set net_revenue_30 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set PC2_30 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set net_transactions_60 = (select count(distinct t.order_nr) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set net_revenue_60 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set PC2_60 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set net_transactions_90 = (select count(distinct t.order_nr) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set net_revenue_90 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set PC2_90 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set net_transactions_120 = (select count(distinct t.order_nr) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set net_revenue_120 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set PC2_120 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set net_transactions_150 = (select count(distinct t.order_nr) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set net_revenue_150 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set PC2_150 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set net_transactions_180 = (select count(distinct t.order_nr) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set net_revenue_180 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx a set PC2_180 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production.tbl_order_detail t, production.temporary_mx x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

drop table production.temporary_mx;

-- Date

update production.retargeting_affiliates_mx set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=SEM.week_iso(date) where datediff(curdate(), date)<@days;

-- Exchange Rate

update production.retargeting_affiliates_mx s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, net_revenue_30=net_revenue_30/xr, net_revenue_60=net_revenue_60/xr, net_revenue_90=net_revenue_90/xr, net_revenue_120=net_revenue_120/xr, net_revenue_150=net_revenue_150/xr, net_revenue_180=net_revenue_180/xr, PC2=PC2/xr, PC2_30=PC2_30/xr, PC2_60=PC2_60/xr, PC2_90=PC2_90/xr, PC2_120=PC2_120/xr, PC2_150=PC2_150/xr, PC2_180=PC2_180/xr where r.country='MEX' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost/1.23 where channel = 'Criteo' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost/16.35 where channel in ('Vizury', 'Veinteractive', 'GLG', 'Pampa Network', 'Adjal', 'ClickMagic', 'Commission Junction', 'DiverselyDigital', 'ILoveCPA', 'Lomadee', 'PubliciIdeas', 'Soicos', 'Trade Tracker', 'Main ADV', 'Promodescuentos', 'My Things', 'Avazu') and datediff(curdate(), date)<@days; 

-- Cost

update production.retargeting_affiliates_mx set cost = cost*0.08*2.5 where channel = 'Vizury' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1 where channel = 'Main ADV' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1 where channel = 'Promodescuentos' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1 where channel = 'My Things' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1 where channel = 'Avazu' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.08*2.5 where channel = 'Veinteractive' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1 where channel = 'GLG' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.15*2.5 where channel = 'Pampa Network' and date <= '2013-05-12' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1*2.5 where channel = 'Pampa Network' and date > '2013-05-12' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1*2.5 where channel = 'Adjal' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1*2.5 where channel = 'ClickMagic' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1 where channel = 'Commission Junction' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1 where channel = 'DiverselyDigital' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1 where channel = 'ILoveCPA' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1*2.5 where channel = 'Lomadee' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1*2.5 where channel = 'PubliciIdeas' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1*2.5 where channel = 'Soicos' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_mx set cost = cost*0.1*2.5 where channel = 'Trade Tracker' and datediff(curdate(), date)<@days;

-- NULL

update production.retargeting_affiliates_mx  set gross_revenue=0 where gross_revenue is null;

update production.retargeting_affiliates_mx  set net_revenue=0 where net_revenue is null;

update production.retargeting_affiliates_mx  set new_customers=0 where new_customers is null;

update production.retargeting_affiliates_mx  set new_customers_gross=0 where new_customers_gross is null;

update production.retargeting_affiliates_mx set net_revenue_30=0 where net_revenue_30 is null;

update production.retargeting_affiliates_mx set net_revenue_60=0 where net_revenue_60 is null;

update production.retargeting_affiliates_mx set net_revenue_90=0 where net_revenue_90 is null;

update production.retargeting_affiliates_mx set net_revenue_120=0 where net_revenue_120 is null;

update production.retargeting_affiliates_mx set net_revenue_150=0 where net_revenue_150 is null;

update production.retargeting_affiliates_mx set net_revenue_180=0 where net_revenue_180 is null;


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `retargeting_affiliates_pe`()
begin

set @days:=30;

delete from production.retargeting_affiliates_pe where datediff(curdate(), date)<@days;

-- FBX

insert into production.retargeting_affiliates_pe(channel, date, source, medium, campaign, visits, bounce, cart) select 'FBX', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_pe c where source='facebook' and medium='retargeting' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

create table production.temporary_retargeting_affiliates_pe(
date date,
impressions int,
clicks int,
cost float);

create index date on production.temporary_retargeting_affiliates_pe(date);

create table production.final_pe(
date date,
source varchar(255),
medium varchar(255),
campaign varchar(255),
times float,
visits int,
day_visits int);

create index channel on production.final_pe(date, source, medium, campaign);

insert into production.temporary_retargeting_affiliates_pe(date) select date from production.retargeting_affiliates_pe where channel = 'FBX' and datediff(curdate(), date)<@days group by date;

update production.temporary_retargeting_affiliates_pe r set cost = (select sum(a.cost)+sum(b.cost) from marketing_report.sociomantic_fbx_pe a, marketing_report.sociomantic_fbx_loyalty_pe b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_pe r set impressions = (select sum(a.impressions)+sum(b.impressions) from marketing_report.sociomantic_fbx_pe a, marketing_report.sociomantic_fbx_loyalty_pe b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_pe r set clicks = (select sum(a.clicks)+sum(b.clicks) from marketing_report.sociomantic_fbx_pe a, marketing_report.sociomantic_fbx_loyalty_pe b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

insert into production.final_pe(date, source, medium, campaign, visits) select date, source, medium, campaign, visits from production.retargeting_affiliates_pe where channel='FBX' and datediff(curdate(), date)<@days;

update production.final_pe f set day_visits = (select sum(visits) from production.retargeting_affiliates_pe r where r.date=f.date group by date) where datediff(curdate(), date)<@days;

update production.final_pe f set times = visits/day_visits;

update production.retargeting_affiliates_pe r set cost = (select t.cost*x.times from production.temporary_retargeting_affiliates_pe t, production.final_pe x where t.date=r.date and x.date=t.date and r.channel='FBX' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='FBX' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe r set impressions = (select t.impressions*x.times from production.temporary_retargeting_affiliates_pe t, production.final_pe x where t.date=r.date and x.date=t.date and r.channel='FBX' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='FBX' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe r set clicks = (select t.clicks*x.times from production.temporary_retargeting_affiliates_pe t, production.final_pe x where t.date=r.date and x.date=t.date and r.channel='FBX' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='FBX' and datediff(curdate(), date)<@days;

delete from production.temporary_retargeting_affiliates_pe;

delete from production.final_pe;

-- Sociomantic

insert into production.retargeting_affiliates_pe(channel, date, source, medium, campaign, visits, bounce, cart) select 'Sociomantic', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_pe c where source like '%socioman%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

insert into production.temporary_retargeting_affiliates_pe(date) select date from production.retargeting_affiliates_pe where channel = 'Sociomantic' and datediff(curdate(), date)<@days group by date;

update production.temporary_retargeting_affiliates_pe r set cost = (select sum(a.cost)+sum(b.cost) from marketing_report.sociomantic_retargeting_pe a, marketing_report.sociomantic_loyalty_pe b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_pe r set impressions = (select sum(a.impressions)+sum(b.impressions) from marketing_report.sociomantic_retargeting_pe a, marketing_report.sociomantic_loyalty_pe b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_pe r set clicks = (select sum(a.clicks)+sum(b.clicks) from marketing_report.sociomantic_retargeting_pe a, marketing_report.sociomantic_loyalty_pe b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

insert into production.final_pe(date, source, medium, campaign, visits) select date, source, medium, campaign, visits from production.retargeting_affiliates_pe where channel='Sociomantic' and datediff(curdate(), date)<@days;

update production.final_pe f set day_visits = (select sum(visits) from production.retargeting_affiliates_pe r where r.channel='Sociomantic' and r.date=f.date group by date) where datediff(curdate(), date)<@days;

update production.final_pe f set times = visits/day_visits;

update production.retargeting_affiliates_pe r set cost = (select t.cost*x.times from production.temporary_retargeting_affiliates_pe t, production.final_pe x where t.date=r.date and x.date=t.date and r.channel='Sociomantic' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='Sociomantic' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe r set impressions = (select t.impressions*x.times from production.temporary_retargeting_affiliates_pe t, production.final_pe x where t.date=r.date and x.date=t.date and r.channel='Sociomantic' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='Sociomantic' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe r set clicks = (select t.clicks*x.times from production.temporary_retargeting_affiliates_pe t, production.final_pe x where t.date=r.date and x.date=t.date and r.channel='Sociomantic' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='Sociomantic' and datediff(curdate(), date)<@days;

drop table production.temporary_retargeting_affiliates_pe;

drop table production.final_pe;

-- Vizury

insert into production.retargeting_affiliates_pe(channel, date, source, medium, campaign, visits, bounce, cart) select 'Vizury', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_pe c where source like '%vizury%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_pe r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_pe.tbl_order_detail t, SEM.transaction_id_pe x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Vizury' and datediff(curdate(), date)<@days;

-- SOLOCPM/Main ADV

insert into production.retargeting_affiliates_pe(channel, date, source, medium, campaign, visits, bounce, cart) select 'Main ADV', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_pe c where (source like '%maindadv%' or source like '%solocpm%') and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_pe r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_pe.tbl_order_detail t, SEM.transaction_id_pe x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Main ADV' and datediff(curdate(), date)<@days;

-- Trade Tracker

insert into production.retargeting_affiliates_pe(channel, date, source, medium, campaign, visits, bounce, cart) select 'Trade Tracker', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_pe c where (source like '%tradetracker%' or source = 'Trade Tracker') and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_pe r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_pe.tbl_order_detail t, SEM.transaction_id_pe x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Trade Tracker' and datediff(curdate(), date)<@days;

-- Veinteractive

insert into production.retargeting_affiliates_pe(channel, date, source, medium, campaign, visits, bounce, cart) select 'Veinteractive', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_pe c where source like '%veinteractive%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_pe r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_pe.tbl_order_detail t, SEM.transaction_id_pe x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Veinteractive' and datediff(curdate(), date)<@days;

-- GLG

insert into production.retargeting_affiliates_pe(channel, date, source, medium, campaign, visits, bounce, cart) select 'GLG', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_pe c where source like '%glg%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_pe r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_pe.tbl_order_detail t, SEM.transaction_id_pe x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='GLG' and datediff(curdate(), date)<@days;

-- Pampa Network

insert into production.retargeting_affiliates_pe(channel, date, source, medium, campaign, visits, bounce, cart) select 'Pampa Network', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_pe c where source like '%pampa%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_pe r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_pe.tbl_order_detail t, SEM.transaction_id_pe x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Pampa Network' and datediff(curdate(), date)<@days;

-- Buscape

insert into production.retargeting_affiliates_pe(channel, date, source, medium, campaign, visits, bounce, cart) select 'Buscape', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_pe c where source like '%buscape%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

-- Others

insert into production.retargeting_affiliates_pe(channel, date, source, medium, campaign, visits, bounce, cart) select 'Others', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_pe c where (source like '%Retargeting RM%' or source like '%googleads.g.doubleclick.net%' or source like '%peruhardware.net%' or source like '%mx.answers.yahoo.com%' or source like '%google.com.pe%') and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

-- BOB 

update production.retargeting_affiliates_pe r set net_transactions = (select count(distinct transaction_id) from production_pe.tbl_order_detail t, SEM.transaction_id_pe x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe r set gross_transactions = (select count(distinct transaction_id) from production_pe.tbl_order_detail t, SEM.transaction_id_pe x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.obc=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe r set net_revenue = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_pe.tbl_order_detail t, SEM.transaction_id_pe x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe r set gross_revenue = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_pe.tbl_order_detail t, SEM.transaction_id_pe x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.obc=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe r set PC2 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production_pe.tbl_order_detail t, SEM.transaction_id_pe x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe r set new_customers = (select sum(new_customers) from production_pe.tbl_order_detail t, SEM.transaction_id_pe x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe r set new_customers_gross = (select sum(new_customers_gross) from production_pe.tbl_order_detail t, SEM.transaction_id_pe x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.obc=1) where datediff(curdate(), date)<@days;

-- Cohort

create table production.temporary_pe(
date date,
source varchar(255),
medium varchar(255),
campaign varchar(255),
custid int,
transaction_id varchar(255)
);

create index temporary on production.temporary_pe(date, source, medium, campaign);
create index transaction_id on production.temporary_pe(transaction_id);

insert into production.temporary_pe select distinct x.date, x.source, x.medium, x.campaign, t.custid, x.transaction_id from SEM.transaction_id_pe x, production_pe.tbl_order_detail t where x.transaction_id=t.order_nr and t.new_customers is not null and t.oac=1 and datediff(curdate(), t.date)<@days;

update production.retargeting_affiliates_pe a set net_transactions_30 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t, production.temporary_pe x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe a set net_revenue_30 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_pe.tbl_order_detail t, production.temporary_pe x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe a set PC2_30 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production_pe.tbl_order_detail t, production.temporary_pe x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe a set net_transactions_60 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t, production.temporary_pe x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe a set net_revenue_60 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_pe.tbl_order_detail t, production.temporary_pe x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe a set PC2_60 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production_pe.tbl_order_detail t, production.temporary_pe x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe a set net_transactions_90 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t, production.temporary_pe x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe a set net_revenue_90 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_pe.tbl_order_detail t, production.temporary_pe x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe a set PC2_90 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production_pe.tbl_order_detail t, production.temporary_pe x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe a set net_transactions_120 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t, production.temporary_pe x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe a set net_revenue_120 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_pe.tbl_order_detail t, production.temporary_pe x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe a set PC2_120 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production_pe.tbl_order_detail t, production.temporary_pe x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe a set net_transactions_150 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t, production.temporary_pe x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe a set net_revenue_150 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_pe.tbl_order_detail t, production.temporary_pe x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe a set PC2_150 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production_pe.tbl_order_detail t, production.temporary_pe x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe a set net_transactions_180 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t, production.temporary_pe x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe a set net_revenue_180 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_pe.tbl_order_detail t, production.temporary_pe x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe a set PC2_180 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production_pe.tbl_order_detail t, production.temporary_pe x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

drop table production.temporary_pe;

-- Date

update production.retargeting_affiliates_pe set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=SEM.week_iso(date) where datediff(curdate(), date)<@days;

-- Exchange Rate

update production.retargeting_affiliates_pe s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, net_revenue_30=net_revenue_30/xr, net_revenue_60=net_revenue_60/xr, net_revenue_90=net_revenue_90/xr, net_revenue_120=net_revenue_120/xr, net_revenue_150=net_revenue_150/xr, net_revenue_180=net_revenue_180/xr, PC2=PC2/xr, PC2_30=PC2_30/xr, PC2_60=PC2_60/xr, PC2_90=PC2_90/xr, PC2_120=PC2_120/xr, PC2_150=PC2_150/xr, PC2_180=PC2_180/xr where r.country='PER' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe set cost = cost/3.3 where channel in ('Vizury', 'Veinteractive', 'GLG', 'Pampa Network', 'Main ADV', 'Trade Tracker') and datediff(curdate(), date)<@days; 

-- Cost

update production.retargeting_affiliates_pe set cost = cost*0.08*2.5 where channel = 'Vizury' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe set cost = cost*0.1 where channel = 'Main ADV' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe set cost = cost*0.1 where channel = 'Trade Tracker' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe set cost = cost*0.08*2.5 where channel = 'Veinteractive' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe set cost = cost*0.1*2.5 where channel = 'GLG' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe set cost = cost*0.15*2.5 where channel = 'Pampa Network' and date <= '2013-05-12' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_pe set cost = cost*0.1*2.5 where channel = 'Pampa Network' and date > '2013-05-12' and datediff(curdate(), date)<@days;

-- NULL

update production.retargeting_affiliates_pe  set gross_revenue=0 where gross_revenue is null;

update production.retargeting_affiliates_pe  set net_revenue=0 where net_revenue is null;

update production.retargeting_affiliates_pe  set new_customers=0 where new_customers is null;

update production.retargeting_affiliates_pe  set new_customers_gross=0 where new_customers_gross is null;

update production.retargeting_affiliates_pe set net_revenue_30=0 where net_revenue_30 is null;

update production.retargeting_affiliates_pe set net_revenue_60=0 where net_revenue_60 is null;

update production.retargeting_affiliates_pe set net_revenue_90=0 where net_revenue_90 is null;

update production.retargeting_affiliates_pe set net_revenue_120=0 where net_revenue_120 is null;

update production.retargeting_affiliates_pe set net_revenue_150=0 where net_revenue_150 is null;

update production.retargeting_affiliates_pe set net_revenue_180=0 where net_revenue_180 is null;


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `retargeting_affiliates_ve`()
begin

set @days:=30;

delete from production.retargeting_affiliates_ve where datediff(curdate(), date)<@days;

-- FBX

insert into production.retargeting_affiliates_ve(channel, date, source, medium, campaign, visits, bounce, cart) select 'FBX', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_ve c where source='facebook' and medium='retargeting' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

create table production.temporary_retargeting_affiliates_ve(
date date,
impressions int,
clicks int,
cost float);

create index date on production.temporary_retargeting_affiliates_ve(date);

create table production.final_ve(
date date,
source varchar(255),
medium varchar(255),
campaign varchar(255),
times float,
visits int,
day_visits int);

create index channel on production.final_ve(date, source, medium, campaign);

insert into production.temporary_retargeting_affiliates_ve(date) select date from production.retargeting_affiliates_ve where channel = 'FBX' and datediff(curdate(), date)<@days group by date;

update production.temporary_retargeting_affiliates_ve r set cost = (select sum(a.cost)+sum(b.cost) from marketing_report.sociomantic_fbx_ve a, marketing_report.sociomantic_fbx_loyalty_ve b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_ve r set impressions = (select sum(a.impressions)+sum(b.impressions) from marketing_report.sociomantic_fbx_ve a, marketing_report.sociomantic_fbx_loyalty_ve b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_ve r set clicks = (select sum(a.clicks)+sum(b.clicks) from marketing_report.sociomantic_fbx_ve a, marketing_report.sociomantic_fbx_loyalty_ve b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

insert into production.final_ve(date, source, medium, campaign, visits) select date, source, medium, campaign, visits from production.retargeting_affiliates_ve where channel='FBX' and datediff(curdate(), date)<@days;

update production.final_ve f set day_visits = (select sum(visits) from production.retargeting_affiliates_ve r where r.date=f.date group by date) where datediff(curdate(), date)<@days;

update production.final_ve f set times = visits/day_visits;

update production.retargeting_affiliates_ve r set cost = (select t.cost*x.times from production.temporary_retargeting_affiliates_ve t, production.final_ve x where t.date=r.date and x.date=t.date and r.channel='FBX' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='FBX' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve r set impressions = (select t.impressions*x.times from production.temporary_retargeting_affiliates_ve t, production.final_ve x where t.date=r.date and x.date=t.date and r.channel='FBX' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='FBX' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve r set clicks = (select t.clicks*x.times from production.temporary_retargeting_affiliates_ve t, production.final_ve x where t.date=r.date and x.date=t.date and r.channel='FBX' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='FBX' and datediff(curdate(), date)<@days;

delete from production.temporary_retargeting_affiliates_ve;

delete from production.final_ve;

-- Sociomantic

insert into production.retargeting_affiliates_ve(channel, date, source, medium, campaign, visits, bounce, cart) select 'Sociomantic', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_ve c where source like '%socioman%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

insert into production.temporary_retargeting_affiliates_ve(date) select date from production.retargeting_affiliates_ve where channel = 'Sociomantic' and datediff(curdate(), date)<@days group by date;

update production.temporary_retargeting_affiliates_ve r set cost = (select sum(a.cost)+sum(b.cost) from marketing_report.sociomantic_basket_ve a, marketing_report.sociomantic_loyalty_ve b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_ve r set impressions = (select sum(a.impressions)+sum(b.impressions) from marketing_report.sociomantic_basket_ve a, marketing_report.sociomantic_loyalty_ve b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

update production.temporary_retargeting_affiliates_ve r set clicks = (select sum(a.clicks)+sum(b.clicks) from marketing_report.sociomantic_basket_ve a, marketing_report.sociomantic_loyalty_ve b where a.date=b.date and a.date=r.date) where datediff(curdate(), date)<@days;

insert into production.final_ve(date, source, medium, campaign, visits) select date, source, medium, campaign, visits from production.retargeting_affiliates_ve where channel='Sociomantic' and datediff(curdate(), date)<@days;

update production.final_ve f set day_visits = (select sum(visits) from production.retargeting_affiliates_ve r where r.channel='Sociomantic' and r.date=f.date group by date) where datediff(curdate(), date)<@days;

update production.final_ve f set times = visits/day_visits;

update production.retargeting_affiliates_ve r set cost = (select t.cost*x.times from production.temporary_retargeting_affiliates_ve t, production.final_ve x where t.date=r.date and x.date=t.date and r.channel='Sociomantic' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='Sociomantic' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve r set impressions = (select t.impressions*x.times from production.temporary_retargeting_affiliates_ve t, production.final_ve x where t.date=r.date and x.date=t.date and r.channel='Sociomantic' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='Sociomantic' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve r set clicks = (select t.clicks*x.times from production.temporary_retargeting_affiliates_ve t, production.final_ve x where t.date=r.date and x.date=t.date and r.channel='Sociomantic' and x.source=r.source and x.medium=r.medium and x.campaign=r.campaign and x.date=r.date group by r.date) where r.channel='Sociomantic' and datediff(curdate(), date)<@days;

drop table production.temporary_retargeting_affiliates_ve;

drop table production.final_ve;

-- Vizury

insert into production.retargeting_affiliates_ve(channel, date, source, medium, campaign, visits, bounce, cart) select 'Vizury', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_ve c where source like '%vizury%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_ve r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_ve.tbl_order_detail t, SEM.transaction_id_ve x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Vizury' and datediff(curdate(), date)<@days;

-- SOLOCPM/Main ADV

insert into production.retargeting_affiliates_ve(channel, date, source, medium, campaign, visits, bounce, cart) select 'Main ADV', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_ve c where (source like '%maindadv%' or source like '%solocpm%') and datediff(curdate(), c.date)<@days  group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_ve r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_ve.tbl_order_detail t, SEM.transaction_id_ve x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Main ADV' and datediff(curdate(), date)<@days;

-- Trade Tracker

insert into production.retargeting_affiliates_pe(channel, date, source, medium, campaign, visits, bounce, cart) select 'Trade Tracker', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_pe c where (source like '%tradetracker%' or source = 'Trade Tracker') and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_pe r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_pe.tbl_order_detail t, SEM.transaction_id_pe x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Trade Tracker' and datediff(curdate(), date)<@days;

-- Veinteractive

insert into production.retargeting_affiliates_ve(channel, date, source, medium, campaign, visits, bounce, cart) select 'Veinteractive', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_ve c where source like '%veinteractive%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_ve r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_ve.tbl_order_detail t, SEM.transaction_id_ve x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Veinteractive' and datediff(curdate(), date)<@days;

-- GLG

insert into production.retargeting_affiliates_ve(channel, date, source, medium, campaign, visits, bounce, cart) select 'GLG', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_ve c where source like '%glg%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_ve r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_ve.tbl_order_detail t, SEM.transaction_id_ve x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='GLG' and datediff(curdate(), date)<@days;

-- Pampa Network

insert into production.retargeting_affiliates_ve(channel, date, source, medium, campaign, visits, bounce, cart) select 'Pampa Network', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_ve c where source like '%pampa%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

update production.retargeting_affiliates_ve r set cost = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_ve.tbl_order_detail t, SEM.transaction_id_ve x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where r.channel='Pampa Network' and datediff(curdate(), date)<@days;

-- Others

insert into production.retargeting_affiliates_ve(channel, date, source, medium, campaign, visits, bounce, cart) select 'Others', c.date, c.source, c.medium, c.campaign, c.visits, c.bounce, c.cart from SEM.campaign_ad_group_ve c where source like '%contigovenezuela%' and datediff(curdate(), c.date)<@days group by c.date, c.source, c.medium, c.campaign;

-- BOB 

update production.retargeting_affiliates_ve r set net_transactions = (select count(distinct transaction_id) from production_ve.tbl_order_detail t, SEM.transaction_id_ve x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve r set gross_transactions = (select count(distinct transaction_id) from production_ve.tbl_order_detail t, SEM.transaction_id_ve x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.obc=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve r set net_revenue = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_ve.tbl_order_detail t, SEM.transaction_id_ve x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve r set gross_revenue = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_ve.tbl_order_detail t, SEM.transaction_id_ve x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.obc=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve r set PC2 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production_ve.tbl_order_detail t, SEM.transaction_id_ve x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve r set new_customers = (select sum(new_customers) from production_ve.tbl_order_detail t, SEM.transaction_id_ve x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.oac=1) where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve r set new_customers_gross = (select sum(new_customers_gross) from production_ve.tbl_order_detail t, SEM.transaction_id_ve x where t.order_nr=x.transaction_id and r.date=x.date and r.source=x.source and r.medium=x.medium and r.campaign=x.campaign and t.obc=1) where datediff(curdate(), date)<@days;

-- Cohort

create table production.temporary_ve(
date date,
source varchar(255),
medium varchar(255),
campaign varchar(255),
custid int,
transaction_id varchar(255)
);

create index temporary on production.temporary_ve(date, source, medium, campaign);
create index transaction_id on production.temporary_ve(transaction_id);

insert into production.temporary_ve select distinct x.date, x.source, x.medium, x.campaign, t.custid, x.transaction_id from SEM.transaction_id_ve x, production_ve.tbl_order_detail t where x.transaction_id=t.order_nr and t.new_customers is not null and t.oac=1 and datediff(curdate(), t.date)<@days;

update production.retargeting_affiliates_ve a set net_transactions_30 = (select count(distinct t.order_nr) from production_ve.tbl_order_detail t, production.temporary_ve x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) 
where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve a set net_revenue_30 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_ve.tbl_order_detail t, production.temporary_ve x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) 
where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve a set PC2_30 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production_ve.tbl_order_detail t, production.temporary_ve x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) 
where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve a set net_transactions_60 = (select count(distinct t.order_nr) from production_ve.tbl_order_detail t, production.temporary_ve x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) 
where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve a set net_revenue_60 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_ve.tbl_order_detail t, production.temporary_ve x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) 
where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve a set PC2_60 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production_ve.tbl_order_detail t, production.temporary_ve x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) 
where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve a set net_transactions_90 = (select count(distinct t.order_nr) from production_ve.tbl_order_detail t, production.temporary_ve x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) 
where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve a set net_revenue_90 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_ve.tbl_order_detail t, production.temporary_ve x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) 
where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve a set PC2_90 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production_ve.tbl_order_detail t, production.temporary_ve x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) 
where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve a set net_transactions_120 = (select count(distinct t.order_nr) from production_ve.tbl_order_detail t, production.temporary_ve x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) 
where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve a set net_revenue_120 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_ve.tbl_order_detail t, production.temporary_ve x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) 
where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve a set PC2_120 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production_ve.tbl_order_detail t, production.temporary_ve x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) 
where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve a set net_transactions_150 = (select count(distinct t.order_nr) from production_ve.tbl_order_detail t, production.temporary_ve x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) 
where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve a set net_revenue_150 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_ve.tbl_order_detail t, production.temporary_ve x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) 
where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve a set PC2_150 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production_ve.tbl_order_detail t, production.temporary_ve x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) 
where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve a set net_transactions_180 = (select count(distinct t.order_nr) from production_ve.tbl_order_detail t, production.temporary_ve x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) 
where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve a set net_revenue_180 = (select sum(ifnull(t.unit_price_after_vat,0)-ifnull(t.coupon_money_after_vat,0)+ifnull(t.shipping_fee_after_vat,0)) from production_ve.tbl_order_detail t, production.temporary_ve x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) 
where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve a set PC2_180 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) from production_ve.tbl_order_detail t, production.temporary_ve x where t.custid=x.custid and a.source=x.source and a.medium=x.medium and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) 
where datediff(curdate(), date)<@days;

drop table production.temporary_ve;

-- Date

update production.retargeting_affiliates_ve set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end 
where datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=SEM.week_iso(date) 
where datediff(curdate(), date)<@days;

-- Exchange Rate

update production.retargeting_affiliates_ve s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, net_revenue_30=net_revenue_30/xr, net_revenue_60=net_revenue_60/xr, net_revenue_90=net_revenue_90/xr, net_revenue_120=net_revenue_120/xr, net_revenue_150=net_revenue_150/xr, net_revenue_180=net_revenue_180/xr, PC2=PC2/xr, PC2_30=PC2_30/xr, PC2_60=PC2_60/xr, PC2_90=PC2_90/xr, PC2_120=PC2_120/xr, PC2_150=PC2_150/xr, PC2_180=PC2_180/xr where r.country='VEN' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve set cost = cost/32.468 where yrmonth = 201304 and channel in ('Vizury', 'Veinteractive', 'GLG', 'Pampa Network', 'Trade Tracker', 'Main ADV') and datediff(curdate(), date)<@days; 

update production.retargeting_affiliates_ve set cost = cost/32.92 where yrmonth = 201305 and channel in ('Vizury', 'Veinteractive', 'GLG', 'Pampa Network', 'Trade Tracker', 'Main ADV') and datediff(curdate(), date)<@days; 

update production.retargeting_affiliates_ve set cost = cost/36.12 where yrmonth = 201306 and channel in ('Vizury', 'Veinteractive', 'GLG', 'Pampa Network', 'Trade Tracker', 'Main ADV') and datediff(curdate(), date)<@days; 

update production.retargeting_affiliates_ve set cost = cost/39 where yrmonth >= 201307 and channel in ('Vizury', 'Veinteractive', 'GLG', 'Pampa Network', 'Trade Tracker', 'Main ADV') and datediff(curdate(), date)<@days;

-- Cost

update production.retargeting_affiliates_ve set cost = cost*0.08*2.5 where channel = 'Vizury' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve set cost = cost*0.1 where channel = 'Trade Tracker' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve set cost = cost*0.1 where channel = 'Main ADV' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve set cost = cost*0.08*2.5 where channel = 'Veinteractive' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve set cost = cost*0.1*2.5 where channel = 'GLG' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve set cost = cost*0.15*2.5 where channel = 'Pampa Network' and date <= '2013-05-12' and datediff(curdate(), date)<@days;

update production.retargeting_affiliates_ve set cost = cost*0.05*2.5 where channel = 'Pampa Network' and date > '2013-05-12' and datediff(curdate(), date)<@days;

-- NULL

update production.retargeting_affiliates_ve  set gross_revenue=0 where gross_revenue is null;

update production.retargeting_affiliates_ve  set net_revenue=0 where net_revenue is null;

update production.retargeting_affiliates_ve  set new_customers=0 where new_customers is null;

update production.retargeting_affiliates_ve  set new_customers_gross=0 where new_customers_gross is null;

update production.retargeting_affiliates_ve set net_revenue_30=0 where net_revenue_30 is null;

update production.retargeting_affiliates_ve set net_revenue_60=0 where net_revenue_60 is null;

update production.retargeting_affiliates_ve set net_revenue_90=0 where net_revenue_90 is null;

update production.retargeting_affiliates_ve set net_revenue_120=0 where net_revenue_120 is null;

update production.retargeting_affiliates_ve set net_revenue_150=0 where net_revenue_150 is null;

update production.retargeting_affiliates_ve set net_revenue_180=0 where net_revenue_180 is null;


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `status_bob`()
begin

delete from production.status_bob;

insert into production.status_bob(payment_method, status_bob, obc, oac, cancel, returned, pending) select payment_method, status, orderbeforecan, orderaftercan, cancellations, returns, pending from development_mx.M_Bob_Order_Status_Definition;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `table_monitoring`()
begin

delete from production.table_monitoring;

insert into production.table_monitoring(table_name, Mexico) select 'BOB', created_at from bob_live_mx.sales_order order by created_at desc limit 1;	

update production.table_monitoring set Colombia = (select created_at from bob_live_co.sales_order order by created_at desc limit 1 ) where table_name = 'BOB';	

update production.table_monitoring set Peru = (select created_at from bob_live_pe.sales_order order by created_at desc limit 1 ) where table_name = 'BOB';	

update production.table_monitoring set Venezuela = (select created_at from bob_live_ve.sales_order order by created_at desc limit 1 ) where table_name = 'BOB';

insert into production.table_monitoring(table_name, Mexico) select 'WMS', data_criacao from wmsprod.itens_venda order by data_criacao desc limit 1;	

update production.table_monitoring set Colombia = (select data_criacao from wmsprod_co.itens_venda order by data_criacao desc limit 1 ) where table_name = 'WMS';	

update production.table_monitoring set Peru = (select data_criacao from wmsprod_pe.itens_venda order by data_criacao desc limit 1 ) where table_name = 'WMS';	

update production.table_monitoring set Venezuela = (select data_criacao from wmsprod_ve.itens_venda order by data_criacao desc limit 1 ) where table_name = 'WMS';	

insert into production.table_monitoring(table_name, Mexico) select 'OMS', updated_at from procurement_live_mx.procurement_order_item order by updated_at desc limit 1;	

update production.table_monitoring set Colombia = (select updated_at from procurement_live_co.procurement_order_item order by updated_at desc limit 1 ) where table_name = 'OMS';	

update production.table_monitoring set Peru = (select updated_at from procurement_live_pe.procurement_order_item order by updated_at desc limit 1 ) where table_name = 'OMS';	

update production.table_monitoring set Venezuela = (select updated_at from procurement_live_ve.procurement_order_item order by updated_at desc limit 1 ) where table_name = 'OMS';

insert into production.table_monitoring(table_name, Mexico) select 'Global GA', date from SEM.campaign_ad_group_mx order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from SEM.campaign_ad_group_co order by date desc limit 1 ) where table_name = 'Global GA';	

update production.table_monitoring set Peru = (select date from SEM.campaign_ad_group_pe order by date desc limit 1 ) where table_name = 'Global GA';

update production.table_monitoring set Venezuela = (select date from SEM.campaign_ad_group_ve order by date desc limit 1 ) where table_name = 'Global GA';

--

insert into production.table_monitoring(table_name, Mexico) select 'Keyword GA', date from SEM.campaign_keyword_mx order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from SEM.campaign_keyword_co order by date desc limit 1 ) where table_name = 'Keyword GA';	

update production.table_monitoring set Peru = (select date from SEM.campaign_keyword_pe order by date desc limit 1 ) where table_name = 'Keyword GA';

update production.table_monitoring set Venezuela = (select date from SEM.campaign_keyword_ve order by date desc limit 1 ) where table_name = 'Keyword GA';

--

insert into production.table_monitoring(table_name, Colombia) select 'Global Fashion GA', date from SEM.campaign_ad_group_fashion_co order by date desc limit 1;	

insert into production.table_monitoring(table_name, Mexico) select 'Facebook GA', date from facebook.ga_facebook_ads_region_mx order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from facebook.ga_facebook_ads_region_co order by date desc limit 1 ) where table_name = 'Facebook GA';	

update production.table_monitoring set Peru = (select date from facebook.ga_facebook_ads_region_pe order by date desc limit 1 ) where table_name = 'Facebook GA';

insert into production.table_monitoring(table_name, Mexico) select 'Mobile App GA', date from production.mobileapp_transaction_id order by date desc limit 1;

update production.table_monitoring set Colombia = (select date from production_co.mobileapp_transaction_id order by date desc limit 1 ) where table_name = 'Mobile App GA';

update production.table_monitoring set Peru = (select date from production_pe.mobileapp_transaction_id order by date desc limit 1 ) where table_name = 'Mobile App GA';

update production.table_monitoring set Venezuela = (select date from production_ve.mobileapp_transaction_id order by date desc limit 1 ) where table_name = 'Mobile App GA';

--

insert into production.table_monitoring(table_name, Mexico) select 'Facebook Keyword GA', date from facebook.ga_facebook_ads_keyword_mx order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from facebook.ga_facebook_ads_keyword_co order by date desc limit 1 ) where table_name = 'Facebook Keyword GA';	

update production.table_monitoring set Peru = (select date from facebook.ga_facebook_ads_keyword_pe order by date desc limit 1 ) where table_name = 'Facebook Keyword GA';

--

insert into production.table_monitoring(table_name, Mexico) select 'Facebook Cost', date from facebook.facebook_campaign_mx order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from facebook.facebook_campaign_co order by date desc limit 1 ) where table_name = 'Facebook Cost';	

update production.table_monitoring set Peru = (select date from facebook.facebook_campaign_pe order by date desc limit 1 ) where table_name = 'Facebook Cost';

update production.table_monitoring set Venezuela = (select date from facebook.facebook_campaign_ve order by date desc limit 1 ) where table_name = 'Facebook Cost';

insert into production.table_monitoring(table_name, Colombia) select 'Facebook Fashion Cost', date from facebook.facebook_campaign_fashion_co order by date desc limit 1;	

--

insert into production.table_monitoring(table_name, Mexico) select 'Criteo Cost', date from marketing_report.criteo where country='MEX' order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from marketing_report.criteo where country='COL' order by date desc limit 1 ) where table_name = 'Criteo Cost';	

--

insert into production.table_monitoring(table_name, Mexico) select 'Sociomantic Cost', date from marketing_report.sociomantic where country='MEX' order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from marketing_report.sociomantic where country='COL' order by date desc limit 1 ) where table_name = 'Sociomantic Cost';	

update production.table_monitoring set Peru = (select date from marketing_report.sociomantic where country='PER' order by date desc limit 1 ) where table_name = 'Sociomantic Cost';	

update production.table_monitoring set Venezuela = (select date from marketing_report.sociomantic where country='VEN' order by date desc limit 1 ) where table_name = 'Sociomantic Cost';	

--

insert into production.table_monitoring(table_name, Mexico) select 'Triggit Cost', report_start_date from marketing_report.triggit_mx order by report_start_date desc limit 1;	

update production.table_monitoring set Colombia = (select report_start_date from marketing_report.triggit_co order by report_start_date desc limit 1 ) where table_name = 'Triggit Cost';	

update production.table_monitoring set Peru = (select report_start_date from marketing_report.triggit_pe order by report_start_date desc limit 1 ) where table_name = 'Triggit Cost';	

update production.table_monitoring set Venezuela = (select report_start_date from marketing_report.triggit_ve order by report_start_date desc limit 1 ) where table_name = 'Triggit Cost';	

--

insert into production.table_monitoring(table_name, Mexico) select 'Vizury Cost', date from marketing_report.vizury_mx order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from marketing_report.vizury_co order by date desc limit 1 ) where table_name = 'Vizury Cost';

update production.table_monitoring set Peru = (select date from marketing_report.vizury_pe order by date desc limit 1 ) where table_name = 'Vizury Cost';

update production.table_monitoring set Venezuela = (select date from marketing_report.vizury_ve order by date desc limit 1 ) where table_name = 'Vizury Cost';

--

insert into production.table_monitoring(table_name, Mexico) select 'Trade Tracker Cost', date from marketing_report.affiliates_mx where network='TradeTracker' order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from marketing_report.affiliates_co where network='TradeTracker' order by date desc limit 1 ) where table_name = 'Trade Tracker Cost';

update production.table_monitoring set Peru = (select date from marketing_report.affiliates_pe where network='TradeTracker' order by date desc limit 1 ) where table_name = 'Trade Tracker Cost';

update production.table_monitoring set Venezuela = (select date from marketing_report.affiliates_ve where network='TradeTracker' order by date desc limit 1 ) where table_name = 'Trade Tracker Cost';

--

insert into production.table_monitoring(table_name, Mexico) select 'GLG Cost', date from marketing_report.affiliates_mx where network='GlobalLeadsGroup' order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from marketing_report.affiliates_co where network='GlobalLeadsGroup' order by date desc limit 1 ) where table_name = 'GLG Cost';

update production.table_monitoring set Peru = (select date from marketing_report.affiliates_pe where network='GlobalLeadsGroup' order by date desc limit 1 ) where table_name = 'GLG Cost';

update production.table_monitoring set Venezuela = (select date from marketing_report.affiliates_ve where network='GlobalLeadsGroup' order by date desc limit 1 ) where table_name = 'GLG Cost';

--

insert into production.table_monitoring(table_name, Mexico) select 'Pampa Network Cost', date from marketing_report.affiliates_mx where network='PampaNetwork' order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from marketing_report.affiliates_co where network='PampaNetwork' order by date desc limit 1 ) where table_name = 'Pampa Network Cost';

update production.table_monitoring set Peru = (select date from marketing_report.affiliates_pe where network='PampaNetwork' order by date desc limit 1 ) where table_name = 'Pampa Network Cost';

update production.table_monitoring set Venezuela = (select date from marketing_report.affiliates_ve where network='PampaNetwork' order by date desc limit 1 ) where table_name = 'Pampa Network Cost';

--

insert into production.table_monitoring(table_name, Mexico) select 'Experian Cost', date from development_mx.M_Other_Cost where country='MEX' and type='experian' order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from development_mx.M_Other_Cost where country='COL' and type='experian' order by date desc limit 1 ) where table_name = 'Experian Cost';

update production.table_monitoring set Peru = (select date from development_mx.M_Other_Cost where country='PER' and type='experian' order by date desc limit 1 ) where table_name = 'Experian Cost';

update production.table_monitoring set Venezuela = (select date from development_mx.M_Other_Cost where country='VEN' and type='experian' order by date desc limit 1 ) where table_name = 'Experian Cost';

--

insert into production.table_monitoring(table_name, Colombia) select 'ICCK Cost', date from marketing_report.icck where country='Colombia' order by date desc limit 1;	

--

insert into production.table_monitoring(table_name, Colombia) select 'ExoClick Cost', date from marketing_report.exoclick where country='Colombia' order by date desc limit 1;	

--

insert into production.table_monitoring(table_name, Mexico) select 'tbl_order_detail', date from production.tbl_order_detail order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from production_co.tbl_order_detail order by date desc limit 1 ) where table_name = 'tbl_order_detail';	

update production.table_monitoring set Peru = (select date from production_pe.tbl_order_detail order by date desc limit 1 ) where table_name = 'tbl_order_detail';	

update production.table_monitoring set Venezuela = (select date from production_ve.tbl_order_detail order by date desc limit 1 ) where table_name = 'tbl_order_detail';

--

insert into production.table_monitoring(table_name, Mexico) select 'A_Master', concat(date,' ', time)  from development_mx.A_Master order by date desc, time desc limit 1;	

update production.table_monitoring set Colombia = (select concat(date,' ', time) from development_co_project.A_Master order by date desc, time desc limit 1 ) where table_name = 'A_Master';	

update production.table_monitoring set Peru = (select concat(date,' ', time) from development_pe.A_Master order by date desc, time desc limit 1 ) where table_name = 'A_Master';	

update production.table_monitoring set Venezuela = (select concat(date,' ', time) from development_ve.A_Master order by date desc, time desc limit 1 ) where table_name = 'A_Master';

--

insert into production.table_monitoring(table_name, Mexico) select 'global_report', date  from marketing_report.global_report where country='Mexico' order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date  from marketing_report.global_report where country='Colombia' order by date desc limit 1 ) where table_name = 'global_report';	

update production.table_monitoring set Peru = (select date  from marketing_report.global_report where country='Peru' order by date desc limit 1 ) where table_name = 'global_report';	

update production.table_monitoring set Venezuela = (select date  from marketing_report.global_report where country='Venezuela' order by date desc limit 1 ) where table_name = 'global_report';	

--

insert into production.table_monitoring(table_name, Mexico) select 'Attribution Campaign', date  from attribution_testing.getfullcampaign_mx order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from attribution_testing.getfullcampaign_co order by date  desc limit 1 ) where table_name = 'Attribution Campaign';	

update production.table_monitoring set Peru = (select date from attribution_testing.getfullcampaign_pe order by date  desc limit 1 ) where table_name = 'Attribution Campaign';	

update production.table_monitoring set Venezuela = (select date from attribution_testing.getfullcampaign_ve order by date  desc limit 1 ) where table_name = 'Attribution Campaign';	

insert into production.table_monitoring(table_name, Mexico) select 'Attribution Campaign Config', date  from attribution_testing.campaigns_config_mx order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from attribution_testing.campaigns_config_co order by date  desc limit 1 ) where table_name = 'Attribution Campaign Config';	

update production.table_monitoring set Peru = (select date from attribution_testing.campaigns_config_pe order by date  desc limit 1 ) where table_name = 'Attribution Campaign Config';	

update production.table_monitoring set Venezuela = (select date from attribution_testing.campaigns_config_ve order by date  desc limit 1 ) where table_name = 'Attribution Campaign Config';	

insert into production.table_monitoring(table_name, Mexico) select 'Attribution Orders', date  from attribution_testing.getfullorders_mx order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from attribution_testing.getfullorders_co order by date  desc limit 1 ) where table_name = 'Attribution Orders';

update production.table_monitoring set Peru = (select date from attribution_testing.getfullorders_pe order by date  desc limit 1 ) where table_name = 'Attribution Orders';

update production.table_monitoring set Venezuela = (select date from attribution_testing.getfullorders_ve order by date  desc limit 1 ) where table_name = 'Attribution Orders';

insert into production.table_monitoring(table_name, Mexico) select 'Attribution Visitors', date  from attribution_testing.getfullvisitors_mx order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from attribution_testing.getfullvisitors_co order by date  desc limit 1 ) where table_name = 'Attribution Visitors';

update production.table_monitoring set Peru = (select date from attribution_testing.getfullvisitors_pe order by date  desc limit 1 ) where table_name = 'Attribution Visitors';

update production.table_monitoring set Venezuela = (select date from attribution_testing.getfullvisitors_ve order by date  desc limit 1 ) where table_name = 'Attribution Visitors';

--

insert into production.table_monitoring(table_name, Mexico) select 'Attribution Basket', date  from attribution_testing.getfullbasket_mx order by date desc limit 1;

update production.table_monitoring set Colombia = (select date from attribution_testing.getfullbasket_co order by date  desc limit 1 ) where table_name = 'Attribution Basket';

update production.table_monitoring set Peru = (select date from attribution_testing.getfullbasket_pe order by date  desc limit 1 ) where table_name = 'Attribution Basket';

update production.table_monitoring set Venezuela = (select date from attribution_testing.getfullbasket_ve order by date  desc limit 1 ) where table_name = 'Attribution Basket';

--

insert into production.table_monitoring(table_name, Mexico) select 'Attribution Clicks', date  from attribution_testing.getfullclicks_mx order by date desc limit 1;

update production.table_monitoring set Colombia = (select date from attribution_testing.getfullclicks_co order by date  desc limit 1 ) where table_name = 'Attribution Clicks';

update production.table_monitoring set Peru = (select date from attribution_testing.getfullclicks_pe order by date  desc limit 1 ) where table_name = 'Attribution Clicks';

update production.table_monitoring set Venezuela = (select date from attribution_testing.getfullclicks_ve order by date  desc limit 1 ) where table_name = 'Attribution Clicks';

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `top_20`()
begin

delete from production.top_20_weekly;

insert into production.top_20_weekly(Category_English, Category, Sku, Product_name, Freq, AVGPrice, PC2)
(select *
from (select 'Appliances', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production.tbl_order_detail t
where production.week_exit(date) = production.week_exit(curdate())-1 and oac = 1 and n1 = 'electrodomésticos'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Photography', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production.tbl_order_detail t
where production.week_exit(date) = production.week_exit(curdate())-1 and oac = 1 and n1 = 'cámaras y fotografía'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Audio Video', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production.tbl_order_detail t
where production.week_exit(date) = production.week_exit(curdate())-1 and oac = 1 and n1 = 'tv, audio y video'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Books', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production.tbl_order_detail t
where production.week_exit(date) = production.week_exit(curdate())-1 and oac = 1 and n1 like 'libros%'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Video Games', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production.tbl_order_detail t
where production.week_exit(date) = production.week_exit(curdate())-1 and oac = 1 and n1 = 'videojuegos'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Fashion', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production.tbl_order_detail t
where production.week_exit(date) = production.week_exit(curdate())-1 and oac = 1 and n1 = 'ropa, calzado y accesorios'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Health and Beauty', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production.tbl_order_detail t
where production.week_exit(date) = production.week_exit(curdate())-1 and oac = 1 and n1 like '%cuidado personal%'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Cellphones', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production.tbl_order_detail t
where production.week_exit(date) = production.week_exit(curdate())-1 and oac = 1 and n1 like 'celulares%'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Computing', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production.tbl_order_detail t
where production.week_exit(date) = production.week_exit(curdate())-1 and oac = 1 and n1 like 'compu%'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Home', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production.tbl_order_detail t
where production.week_exit(date) = production.week_exit(curdate())-1 and oac = 1 and (n1 = 'hogar' or n1 like '%muebles')
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Kids and Babies', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0)) as PC2
from production.tbl_order_detail t
where production.week_exit(date) = production.week_exit(curdate())-1 and oac = 1 and n1 = 'juguetes, niños y bebés'
group by t.sku) n
order by Freq desc limit 20);

update  production.top_20_weekly set AVGPrice = AVGPrice/16.35, PC2 = (PC2/16.35)/Freq;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `top_20_total_revenue`()
begin

delete from production.top_20_total_revenue;

insert into production.top_20_total_revenue(country, category, net_rev) select Country, Category_English, sum(Net_Revenue) from production.top_20_weekly_regional group by Country, Category_English;

-- Mexico

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Mexico' as country, 'Appliances' as category, sum(actual_paid_price_after_tax)/16.35 as net_revenue from production.out_sales_report_item i where order_after_can=1 and production.week_exit(date) = production.week_exit(curdate())-1 and year(date)=year(curdate()) and cat1 = 'electrodomésticos')i ) where country='Mexico' and category='Appliances';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Mexico' as country, 'Photography' as category, sum(actual_paid_price_after_tax)/16.35 as net_revenue from production.out_sales_report_item i where order_after_can=1 and production.week_exit(date) = production.week_exit(curdate())-1 and year(date)=year(curdate()) and cat1 = 'cámaras y fotografía')i ) where country='Mexico' and category='Photography';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Mexico' as country, 'Audio Video' as category, sum(actual_paid_price_after_tax)/16.35 as net_revenue from production.out_sales_report_item i where order_after_can=1 and production.week_exit(date) = production.week_exit(curdate())-1 and year(date)=year(curdate()) and cat1 = 'tv, audio y video')i ) where country='Mexico' and category='Audio Video';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Mexico' as country, 'Books' as category, sum(actual_paid_price_after_tax)/16.35 as net_revenue from production.out_sales_report_item i where order_after_can=1 and production.week_exit(date) = production.week_exit(curdate())-1 and year(date)=year(curdate()) and cat1 like 'libros%')i ) where country='Mexico' and category='Books';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Mexico' as country, 'Video Games' as category, sum(actual_paid_price_after_tax)/16.35 as net_revenue from production.out_sales_report_item i where order_after_can=1 and production.week_exit(date) = production.week_exit(curdate())-1 and year(date)=year(curdate()) and cat1 = 'videojuegos')i ) where country='Mexico' and category='Video Games';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Mexico' as country, 'Fashion' as category, sum(actual_paid_price_after_tax)/16.35 as net_revenue from production.out_sales_report_item i where order_after_can=1 and production.week_exit(date) = production.week_exit(curdate())-1 and year(date)=year(curdate()) and (cat1 = 'ropa, calzado y accesorios' or cat1 = 'moda'))i ) where country='Mexico' and category='Fashion';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Mexico' as country, 'Health and Beauty' as category, sum(actual_paid_price_after_tax)/16.35 as net_revenue from production.out_sales_report_item i where order_after_can=1 and production.week_exit(date) = production.week_exit(curdate())-1 and year(date)=year(curdate()) and cat1 like '%cuidado personal%')i ) where country='Mexico' and category='Health and Beauty';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Mexico' as country, 'Cellphones' as category, sum(actual_paid_price_after_tax)/16.35 as net_revenue from production.out_sales_report_item i where order_after_can=1 and production.week_exit(date) = production.week_exit(curdate())-1 and year(date)=year(curdate()) and cat1 like 'celulares%')i ) where country='Mexico' and category='Cellphones';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Mexico' as country, 'Computing' as category, sum(actual_paid_price_after_tax)/16.35 as net_revenue from production.out_sales_report_item i where order_after_can=1 and production.week_exit(date) = production.week_exit(curdate())-1 and year(date)=year(curdate()) and cat1 like 'compu%')i ) where country='Mexico' and category='Computing';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Mexico' as country, 'Home' as category, sum(actual_paid_price_after_tax)/16.35 as net_revenue from production.out_sales_report_item i where order_after_can=1 and production.week_exit(date) = production.week_exit(curdate())-1 and year(date)=year(curdate()) and (cat1 = 'hogar' or cat1 like '%muebles'))i ) where country='Mexico' and category='Home';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Mexico' as country, 'Kids and Babies' as category, sum(actual_paid_price_after_tax)/16.35 as net_revenue from production.out_sales_report_item i where order_after_can=1 and production.week_exit(date) = production.week_exit(curdate())-1 and year(date)=year(curdate()) and cat1 = 'juguetes, niños y bebés')i ) where country='Mexico' and category='Kids and Babies';

-- Peru

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Peru' as country, 'Appliances' as category, sum(actual_paid_price_after_tax)/3.3 as net_revenue from production_pe.out_sales_report_item i where oac=1 and production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and year(date)=year(curdate()) and cat1 = 'electrodomésticos')i ) where country='Peru' and category='Appliances';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Peru' as country, 'Photography' as category, sum(actual_paid_price_after_tax)/3.3 as net_revenue from production_pe.out_sales_report_item i where oac=1 and production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and year(date)=year(curdate()) and cat1 = 'cámaras y fotografía')i ) where country='Peru' and category='Photography';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Peru' as country, 'Audio Video' as category, sum(actual_paid_price_after_tax)/3.3 as net_revenue from production_pe.out_sales_report_item i where oac=1 and production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and year(date)=year(curdate()) and (cat1 = 'tv, audio y video' or cat1 = 'altavoces'))i ) where country='Peru' and category='Audio Video';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Peru' as country, 'Books' as category, sum(actual_paid_price_after_tax)/3.3 as net_revenue from production_pe.out_sales_report_item i where oac=1 and production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and year(date)=year(curdate()) and cat1 like 'libros%')i ) where country='Peru' and category='Books';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Peru' as country, 'Video Games' as category, sum(actual_paid_price_after_tax)/3.3 as net_revenue from production_pe.out_sales_report_item i where oac=1 and production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and year(date)=year(curdate()) and cat1 = 'videojuegos')i ) where country='Peru' and category='Video Games';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Peru' as country, 'Fashion' as category, sum(actual_paid_price_after_tax)/3.3 as net_revenue from production_pe.out_sales_report_item i where oac=1 and production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and year(date)=year(curdate()) and cat1 = 'ropa, calzado y accesorios')i ) where country='Peru' and category='Fashion';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Peru' as country, 'Health and Beauty' as category, sum(actual_paid_price_after_tax)/3.3 as net_revenue from production_pe.out_sales_report_item i where oac=1 and production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and year(date)=year(curdate()) and (cat1 like '%cuidado personal%' or cat1 = 'rizadoras' or cat1 = 'cabello' or cat1 = 'secadores'))i ) where country='Peru' and category='Health and Beauty';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Peru' as country, 'Cellphones' as category, sum(actual_paid_price_after_tax)/3.3 as net_revenue from production_pe.out_sales_report_item i where oac=1 and production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and year(date)=year(curdate()) and cat1 = 'celulares, telefonía y gps')i ) where country='Peru' and category='Cellphones';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Peru' as country, 'Computing' as category, sum(actual_paid_price_after_tax)/3.3 as net_revenue from production_pe.out_sales_report_item i where oac=1 and production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and year(date)=year(curdate()) and cat1 = 'computadoras')i ) where country='Peru' and category='Computing';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Peru' as country, 'Home' as category, sum(actual_paid_price_after_tax)/3.3 as net_revenue from production_pe.out_sales_report_item i where oac=1 and production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and year(date)=year(curdate()) and (cat1 = 'hogar' or cat1 = 'línea blanca'))i ) where country='Peru' and category='Home';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Peru' as country, 'Kids and Babies' as category, sum(actual_paid_price_after_tax)/3.3 as net_revenue from production_pe.out_sales_report_item i where oac=1 and production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and year(date)=year(curdate()) and cat1 = 'niños y bebés')i ) where country='Peru' and category='Kids and Babies';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Peru' as country, 'Fitness' as category, sum(actual_paid_price_after_tax)/3.3 as net_revenue from production_pe.out_sales_report_item i where oac=1 and production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and year(date)=year(curdate()) and cat1 = 'deportes')i ) where country='Peru' and category='Fitness';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Peru' as country, 'Office Supplies' as category, sum(actual_paid_price_after_tax)/3.3 as net_revenue from production_pe.out_sales_report_item i where oac=1 and production_pe.week_exit(date) = production_pe.week_exit(curdate())-1 and year(date)=year(curdate()) and cat1 = 'oficina y útiles')i ) where country='Peru' and category='Office Supplies';

-- Venezuela

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Venezuela' as country, 'Appliances' as category, sum(actual_paid_price_after_tax)/39 as net_revenue from production_ve.out_sales_report_item i where OrderAfterCan = 'Y' and production_ve.week_exit(date) = production_ve.week_exit(curdate())-1 and year(date)=year(curdate()) and cat2 = 'Electrodomésticos')i ) where country='Venezuela' and category='Appliances';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Venezuela' as country, 'Photography' as category, sum(actual_paid_price_after_tax)/39 as net_revenue from production_ve.out_sales_report_item i where OrderAfterCan = 'Y' and production_ve.week_exit(date) = production_ve.week_exit(curdate())-1 and year(date)=year(curdate()) and cat2 = 'Cámaras')i ) where country='Venezuela' and category='Photography';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Venezuela' as country, 'Audio Video' as category, sum(actual_paid_price_after_tax)/39 as net_revenue from production_ve.out_sales_report_item i where OrderAfterCan = 'Y' and production_ve.week_exit(date) = production_ve.week_exit(curdate())-1 and year(date)=year(curdate()) and cat2 = 'TV y Video')i ) where country='Venezuela' and category='Audio Video';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Venezuela' as country, 'Books' as category, sum(actual_paid_price_after_tax)/39 as net_revenue from production_ve.out_sales_report_item i where OrderAfterCan = 'Y' and production_ve.week_exit(date) = production_ve.week_exit(curdate())-1 and year(date)=year(curdate()) and cat1 like 'libros%')i ) where country='Venezuela' and category='Books';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Venezuela' as country, 'Video Games' as category, sum(actual_paid_price_after_tax)/39 as net_revenue from production_ve.out_sales_report_item i where OrderAfterCan = 'Y' and production_ve.week_exit(date) = production_ve.week_exit(curdate())-1 and year(date)=year(curdate()) and cat2 = 'Video Juegos')i ) where country='Venezuela' and category='Video Games';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Venezuela' as country, 'Fashion' as category, sum(actual_paid_price_after_tax)/39 as net_revenue from production_ve.out_sales_report_item i where OrderAfterCan = 'Y' and production_ve.week_exit(date) = production_ve.week_exit(curdate())-1 and year(date)=year(curdate()) and cat1= 'Fashion')i ) where country='Venezuela' and category='Fashion';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Venezuela' as country, 'Health and Beauty' as category, sum(actual_paid_price_after_tax)/39 as net_revenue from production_ve.out_sales_report_item i where OrderAfterCan = 'Y' and production_ve.week_exit(date) = production_ve.week_exit(curdate())-1 and year(date)=year(curdate()) and cat1 = 'Belleza')i ) where country='Venezuela' and category='Health and Beauty';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Venezuela' as country, 'Cellphones' as category, sum(actual_paid_price_after_tax)/39 as net_revenue from production_ve.out_sales_report_item i where OrderAfterCan = 'Y' and production_ve.week_exit(date) = production_ve.week_exit(curdate())-1 and year(date)=year(curdate()) and cat2 = 'Teléfonos y GPS')i ) where country='Venezuela' and category='Cellphones';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Venezuela' as country, 'Computing' as category, sum(actual_paid_price_after_tax)/39 as net_revenue from production_ve.out_sales_report_item i where OrderAfterCan = 'Y' and production_ve.week_exit(date) = production_ve.week_exit(curdate())-1 and year(date)=year(curdate()) and cat2 = 'Cómputo')i ) where country='Venezuela' and category='Computing';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Venezuela' as country, 'Home' as category, sum(actual_paid_price_after_tax)/39 as net_revenue from production_ve.out_sales_report_item i where OrderAfterCan = 'Y' and production_ve.week_exit(date) = production_ve.week_exit(curdate())-1 and year(date)=year(curdate()) and cat1 = 'Home and Living')i ) where country='Venezuela' and category='Home';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Venezuela' as country, 'Kids and Babies' as category, sum(actual_paid_price_after_tax)/39 as net_revenue from production_ve.out_sales_report_item i where OrderAfterCan = 'Y' and production_ve.week_exit(date) = production_ve.week_exit(curdate())-1 and year(date)=year(curdate()) and cat1 like 'juguetes%')i ) where country='Venezuela' and category='Kids and Babies';

-- Colombia

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Colombia' as country, 'Appliances' as category, sum(paid_price_after_vat)/2350 as net_revenue from production_co.tbl_order_detail i where oac=1 and production_co.week_exit(date) = production_co.week_exit(curdate())-1 and year(date)=year(curdate()) and (n2 = 'Electrodomésticos'))i ) where country='Colombia' and category='Appliances';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Colombia' as country, 'Photography' as category, sum(paid_price_after_vat)/2350 as net_revenue from production_co.tbl_order_detail i where oac=1 and production_co.week_exit(date) = production_co.week_exit(curdate())-1 and year(date)=year(curdate()) and (n2 = 'Cámaras y Fotografía'))i ) where country='Colombia' and category='Photography';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Colombia' as country, 'Audio Video' as category, sum(paid_price_after_vat)/2350 as net_revenue from production_co.tbl_order_detail i where oac=1 and production_co.week_exit(date) = production_co.week_exit(curdate())-1 and year(date)=year(curdate()) and (n1 = 'Música y Películas' or n2 = 'TV, Video y Audio'))i ) where country='Colombia' and category='Audio Video';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Colombia' as country, 'Books' as category, sum(paid_price_after_vat)/2350 as net_revenue from production_co.tbl_order_detail i where oac=1 and production_co.week_exit(date) = production_co.week_exit(curdate())-1 and year(date)=year(curdate()) and n1 = 'Libros')i ) where country='Colombia' and category='Books';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Colombia' as country, 'Video Games' as category, sum(paid_price_after_vat)/2350 as net_revenue from production_co.tbl_order_detail i where oac=1 and production_co.week_exit(date) = production_co.week_exit(curdate())-1 and year(date)=year(curdate()) and n2 = 'Videojuegos')i ) where country='Colombia' and category='Video Games';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Colombia' as country, 'Fashion' as category, sum(paid_price_after_vat)/2350 as net_revenue from production_co.tbl_order_detail i where oac=1 and production_co.week_exit(date) = production_co.week_exit(curdate())-1 and year(date)=year(curdate()) and (n1='linio_fashion'))i ) where country='Colombia' and category='Fashion';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Colombia' as country, 'Health and Beauty' as category, sum(paid_price_after_vat)/2350 as net_revenue from production_co.tbl_order_detail i where oac=1 and production_co.week_exit(date) = production_co.week_exit(curdate())-1 and year(date)=year(curdate()) and n1 like '%cuidado personal%')i ) where country='Colombia' and category='Health and Beauty';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Colombia' as country, 'Cellphones' as category, sum(paid_price_after_vat)/2350 as net_revenue from production_co.tbl_order_detail i where oac=1 and production_co.week_exit(date) = production_co.week_exit(curdate())-1 and year(date)=year(curdate()) and n2 = 'teléfonos y gps')i ) where country='Colombia' and category='Cellphones';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Colombia' as country, 'Computing' as category, sum(paid_price_after_vat)/2350 as net_revenue from production_co.tbl_order_detail i where oac=1 and production_co.week_exit(date) = production_co.week_exit(curdate())-1 and year(date)=year(curdate()) and n2 = 'computadores y tablets')i ) where country='Colombia' and category='Computing';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Colombia' as country, 'Home' as category, sum(paid_price_after_vat)/2350 as net_revenue from production_co.tbl_order_detail i where oac=1 and production_co.week_exit(date) = production_co.week_exit(curdate())-1 and year(date)=year(curdate()) and (n1 = 'Hogar' and n2 != 'Electrodomésticos'))i ) where country='Colombia' and category='Home';

update production.top_20_total_revenue t set total_net_rev = (select i.net_revenue from (select 'Colombia' as country, 'Kids and Babies' as category, sum(paid_price_after_vat)/2350 as net_revenue from production_co.tbl_order_detail i where oac=1 and production_co.week_exit(date) = production_co.week_exit(curdate())-1 and year(date)=year(curdate()) and n1 = 'Juguetes, Niños y Bebés')i ) where country='Colombia' and category='Kids and Babies';

update production.top_20_total_revenue set diff_net_rev=total_net_rev-net_rev; 

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `top_20_weekly_regional`()
begin

select  'Top 20 Weekly Regional: start',now();

delete from production.top_20_weekly_regional;

insert into production.top_20_weekly_regional(Country, Category_English, Category, Sku, Product_name, Freq, AVGPrice, `PC1.5`) select 'Mexico', w.* from production.top_20_weekly w;

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Appliances';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Photography';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Audio Video';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Books';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Video Games';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Fashion';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Health and Beauty';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Cellphones';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Computing';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Home';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Kids and Babies';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Fitness';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Office Supplies';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Outdoor';

insert into production.top_20_weekly_regional(Country, Category_English, Category, Sku, Product_name, Freq, AVGPrice, `PC1.5`) select 'Colombia', w.* from production_co.top_20_weekly w;

update production.top_20_weekly_regional set Category_English = 'Kids and Babies' where Category_English = 'Kids & Babies';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Appliances';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Photography';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Audio Video';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Books';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Video Games';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Fashion';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Health and Beauty';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Cellphones';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Computing';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Home';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Kids and Babies';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Fitness';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Office Supplies';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Outdoor';

insert into production.top_20_weekly_regional(Country, Category_English, Category, Sku, Product_name, Freq, AVGPrice, `PC1.5`) select 'Peru', w.* from production_pe.top_20_weekly w;

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Appliances';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Photography';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Audio Video';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Books';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Video Games';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Fashion';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Health and Beauty';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Cellphones';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Computing';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Home';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Kids and Babies';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Fitness';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Office Supplies';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Outdoor';

insert into production.top_20_weekly_regional(Country, Category_English, Category, Sku, Product_name, Freq, AVGPrice, `PC1.5`) select 'Venezuela', w.* from production_ve.top_20_weekly w;

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Appliances';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Photography';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Audio Video';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Books';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Video Games';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Fashion';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Health and Beauty';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Cellphones';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Computing';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Home';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Kids and Babies';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Fitness';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Office Supplies';

set @counter=0;

update production.top_20_weekly_regional set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Outdoor';

update production.top_20_weekly_regional set Net_Revenue = Freq*AVGPrice;

update production.top_20_weekly_regional set `Sum_of_PC1.5` = Freq*`PC1.5`;

select  'Top 20 Weekly Regional: stop',now();

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `top_20_weekly_regional_revenue`()
begin

select  'Top 20 Weekly Regional: start',now();

delete from production.top_20_weekly_regional_revenue;

insert into production.top_20_weekly_regional_revenue(Country, Category_English, Category, Sku, Product_name, Freq, SUMPrice, `PC1.5`) select 'Mexico', w.* from production.top_20_weekly_revenue w order by SUMPrice desc;

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Appliances';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Photography';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Audio Video';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Books';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Video Games';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Fashion';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Health and Beauty';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Cellphones';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Computing';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Home';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Kids and Babies';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Fitness';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Office Supplies';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Mexico' and Category_English='Outdoor';

insert into production.top_20_weekly_regional_revenue(Country, Category_English, Category, Sku, Product_name, Freq, SUMPrice, `PC1.5`) select 'Colombia', w.* from production_co.top_20_weekly_revenue w order by SUMPrice desc;

update production.top_20_weekly_regional_revenue set Category_English = 'Kids and Babies' where Category_English = 'Kids & Babies';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Appliances';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Photography';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Audio Video';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Books';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Video Games';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Fashion';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Health and Beauty';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Cellphones';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Computing';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Home';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Kids and Babies';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Fitness';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Office Supplies';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Colombia' and Category_English='Outdoor';

insert into production.top_20_weekly_regional_revenue(Country, Category_English, Category, Sku, Product_name, Freq, SUMPrice, `PC1.5`) select 'Peru', w.* from production_pe.top_20_weekly_revenue w order by SUMPrice desc;

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Appliances';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Photography';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Audio Video';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Books';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Video Games';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Fashion';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Health and Beauty';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Cellphones';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Computing';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Home';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Kids and Babies';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Fitness';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Office Supplies';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Peru' and Category_English='Outdoor';

insert into production.top_20_weekly_regional_revenue(Country, Category_English, Category, Sku, Product_name, Freq, SUMPrice, `PC1.5`) select 'Venezuela', w.* from production_ve.top_20_weekly_revenue w order by SUMPrice desc;

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Appliances';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Photography';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Audio Video';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Books';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Video Games';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Fashion';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Health and Beauty';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Cellphones';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Computing';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Home';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Kids and Babies';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Fitness';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Office Supplies';

set @counter=0;

update production.top_20_weekly_regional_revenue set rating=@counter:=@counter+1 where Country='Venezuela' and Category_English='Outdoor';

update production.top_20_weekly_regional_revenue set Net_Revenue = Freq*SUMPrice;

update production.top_20_weekly_regional_revenue set `Sum_of_PC1.5` = Freq*`PC1.5`;

select  'Top 20 Weekly Regional: stop',now();

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `update_tbl_order_detail`()
begin

declare itemid integer;

select @last_date:=max(date) from production.tbl_order_detail;
select @last_date_fa:=max(date) from production.ga_cost_campaign where source = 'facebook';
select @last_date_socio:=max(date) from production.ga_cost_campaign where source = 'Sociomantic';

select  'update tbl_order_detail: start',now();

#update status
update production.tbl_order_detail join bob_live_mx.sales_order_item on id_sales_order_item=item join bob_live_mx.sales_order_item_status on fk_sales_order_item_status=id_sales_order_item_status
set status_item= sales_order_item_status.name;

#update obc, oac, pending, cancel, returned
update production.tbl_order_detail join production.status_bob 
on tbl_order_detail.payment_method=status_bob.payment_method 
and tbl_order_detail.status_item=status_bob.status_bob
set tbl_order_detail.obc=status_bob.obc, 
tbl_order_detail.oac=status_bob.oac,
tbl_order_detail.pending=status_bob.pending,
tbl_order_detail.cancel=status_bob.cancel,
tbl_order_detail.returned=status_bob.returned;

#update categories
update production.tbl_order_detail join production.tbl_catalog_product_v2 
on tbl_order_detail.sku=tbl_catalog_product_v2.sku 
set n1=cat1,n2=cat2,n3=cat3;

#insert facebookads and sociomantic
insert into production.ga_cost_campaign(date, source, medium, impressions, adclicks, adcost) 
select date(date_value), 'facebook', 
'socialmedia', sum(if(impressions is null, 0, impressions)) impressions, sum(if(clicks is null, 0, clicks)) clicks, sum(if(cost_mxn is null, 0.,cost_mxn)) cost
 from production.mkt_facebook_ad_cost where date(date_value) > @last_date_fa
and category = 'Social Media' group by date_value, channel;

insert into production.ga_cost_campaign(date, source, medium, impressions, adclicks, adcost) 
select date(date_value), 'facebook', 
'socialmediaads', sum(if(impressions is null, 0, impressions)) impressions, sum(if(clicks is null, 0, clicks)) clicks, sum(if(cost_mxn is null, 0.,cost_mxn)) cost 
from production.mkt_facebook_ad_cost where date(date_value) > @last_date_fa
and category <> 'Social Media' group by date_value, channel;

insert into production.ga_cost_campaign(date, source, medium, campaign,impressions, adclicks, adcost) 
select date(date_value), 'Sociomantic', 
'Retargeting', '(not set)', sum(if(impressions is null, 0, impressions)) impressions,
 sum(if(clicks is null, 0, clicks)) clicks, sum(if(cost_mxn is null, 0.,cost_mxn)) cost 
from production.mkt_channel_sociomantic where date(date_value) > @last_date_socio
group by date(date_value);

#insert new data
set itemid = (select item from production.tbl_order_detail order by item desc limit 1);

insert into production.tbl_order_detail (custid,orderid,order_nr,payment_method,unit_price,paid_price,
coupon_money_value,coupon_code,date,hour,sku,item,
status_item,obc,pending,cancel,oac,returned,n1,n2,n3,
tax_percent,unit_price_after_vat,
paid_price_after_vat,coupon_money_after_vat,cost_pet,costo_oferta,costo_after_vat,delivery_cost_supplier,
buyer,head_buyer,brand,product_name,peso,proveedor,ciudad,region,ordershippingfee, about_linio)
(select sales_order.fk_customer as custid,sales_order.id_sales_order as orderid,sales_order.order_nr as order_nr,sales_order.payment_method as payment_method,
sales_order_item.unit_price as unit_price,sales_order_item.paid_price as paid_price,sales_order_item.coupon_money_value as coupon_money_value,sales_order.coupon_code as coupon_code,
cast(sales_order.created_at as date) as date,concat(hour(sales_order.created_at),':',minute(sales_order.created_at),':',second(sales_order.created_at)) as hour,sales_order_item.sku as sku,
sales_order_item.id_sales_order_item as item,
sales_order_item_status.name as status_item,
(select status_bob.obc from production.status_bob 
where ((status_bob.payment_method = sales_order.payment_method) and (sales_order_item_status.name = status_bob.status_bob)) limit 1) as obc,
(select status_bob.pending from production.status_bob where ((status_bob.payment_method = sales_order.payment_method) and 
(sales_order_item_status.name = status_bob.status_bob)) limit 1) as pending,
(select status_bob.cancel from production.status_bob where ((status_bob.payment_method = sales_order.payment_method) and (sales_order_item_status.name = status_bob.status_bob)) limit 1) as cancel,
(select status_bob.oac from production.status_bob where ((status_bob.payment_method = sales_order.payment_method) and (sales_order_item_status.name = status_bob.status_bob)) limit 1) as oac,
(select status_bob.returned from production.status_bob where ((status_bob.payment_method = sales_order.payment_method)
and (sales_order_item_status.name = status_bob.status_bob)) limit 1) as returned,
tbl_catalog_product_v2.cat1 as n1,
tbl_catalog_product_v2.cat2 as n2,
tbl_catalog_product_v2.cat3 as n3,
tbl_catalog_product_v2.tax_percent as tax_percent,
(sales_order_item.unit_price / (1 + (tbl_catalog_product_v2.tax_percent / 100))) as unit_price_after_vat,
(sales_order_item.paid_price / (1 + (tbl_catalog_product_v2.tax_percent / 100))) as paid_price_after_vat,
(sales_order_item.coupon_money_value / (1 + (tbl_catalog_product_v2.tax_percent / 100))) as coupon_money_after_vat,
tbl_catalog_product_v2.cost as cost_pet,
if(tbl_catalog_product_v2.cost<sales_order_item.cost,sales_order_item.cost,0) as costo_oferta,
sales_order_item.cost/((100+ sales_order_item.tax_percent)/100) as costo_after_vat,
if(isnull(sales_order_item.delivery_cost_supplier),tbl_catalog_product_v2.inbound,
sales_order_item.delivery_cost_supplier) as delivery_cost_supplier,
tbl_catalog_product_v2.buyer as buyer,
tbl_catalog_product_v2.head_buyer as head_buyer,
tbl_catalog_product_v2.brand as brand,
tbl_catalog_product_v2.product_name as product_name,
tbl_catalog_product_v2.product_weight as peso,
tbl_catalog_product_v2.supplier as proveedor,
if(isnull(sales_order_address.municipality),sales_order_address.city,sales_order_address.municipality) as ciudad,
sales_order_address.region as region,
sales_order.shipping_amount as ordershippingfee,
sales_order.about_linio as about_linio
from ((((bob_live_mx.sales_order_item join bob_live_mx.sales_order_item_status) 
join bob_live_mx.sales_order) join production.tbl_catalog_product_v2) join bob_live_mx.sales_order_address) 
where
((sales_order_item.fk_sales_order_item_status = sales_order_item_status.id_sales_order_item_status) 
and (sales_order_item.id_sales_order_item > itemid) and (sales_order.id_sales_order = sales_order_item.fk_sales_order) 
and (sales_order.fk_sales_order_address_shipping = sales_order_address.id_sales_order_address)
and (sales_order_item.sku = tbl_catalog_product_v2.sku)
));

update production.tbl_order_detail inner join bob_live_mx.catalog_simple on tbl_order_detail.sku=catalog_simple.sku set tbl_order_detail.delivery_cost_supplier = catalog_simple.delivery_cost_supplier;

update production.tbl_order_detail set costo_after_vat = cost_pet/1.16 
where costo_after_vat is null;

update production.tbl_order_detail set tbl_order_detail.costo_after_vat = tbl_order_detail.costo_after_vat-(94641.06/60)
where tbl_order_detail.oac=1 and month(tbl_order_detail.date)=2 and tbl_order_detail.proveedor='canon mexicana s de rl de cv' and item>itemid;

update production.tbl_order_detail set tbl_order_detail.costo_after_vat = tbl_order_detail.costo_after_vat-(229203.9/195)
where tbl_order_detail.oac=1 and month(tbl_order_detail.date)=3 and tbl_order_detail.proveedor='canon mexicana s de rl de cv' and item>itemid;

update production.tbl_order_detail set tbl_order_detail.costo_after_vat = tbl_order_detail.costo_after_vat-(12931.04/117)
where tbl_order_detail.oac=1 and month(tbl_order_detail.date)=3 and tbl_order_detail.proveedor='marcas de renombre, s.a. de c.v.' and tbl_order_detail.n1='deportes' and item>itemid;

update production.tbl_order_detail
set year = year(date) , month = month(date) , yrmonth = concat(year,if(month<10,concat(0,month),month))
where date >=@last_date;

select  'start extra_query_actual_paid_price: start',now();

call production.extra_query_actual_paid_price(@itemid);

select  'start extra_query_actual_paid_price: end',now();

#shipping cost
update production.tbl_order_detail 
inner join bob_live_mx.customer_address 
on tbl_order_detail.custid = customer_address.fk_customer 
inner join bob_live_mx.customer_address_region 
on customer_address.fk_customer_address_region = customer_address_region.id_customer_address_region 
set state =  customer_address_region.code;

update production.tbl_order_detail set sku_config = left(sku, 15);

update production.tbl_order_detail o set 
o.package_weight = 
(select catalog_config.package_weight from bob_live_mx.catalog_config where o.sku_config = catalog_config.sku);

update production.tbl_order_detail inner join production.ops_average_weight_per_category on tbl_order_detail.n1 = ops_average_weight_per_category.category set tbl_order_detail.package_weight = average_weight_per_category where tbl_order_detail.package_weight is null;

delete from production.tbl_order_detail_lookup;

insert into production.tbl_order_detail_lookup (select * from production.tbl_order_detail);

update production.tbl_order_detail t inner join bob_live_mx.catalog_config c on t.sku_config=c.sku set t.product_weight=c.product_weight;

update production.tbl_order_detail a 
set peso = (select case when package_weight=0 
then ceil(sum(product_weight)) else ceil(sum(package_weight)) 
end from production.tbl_order_detail_lookup b where a.order_nr = b.order_nr group by a.order_nr);

/* update production.tbl_order_detail inner join production.ops_shipping_cost_per_area on tbl_order_detail.state = ops_shipping_cost_per_area.state and tbl_order_detail.peso = ops_shipping_cost_per_area.kg set shipping_cost = case when date <= '2012-09-30' then shipping_cost_until_sept when date between '2012-10-01'
and '2012-11-30' then shipping_cost_from_oct when date >= '2012-12-01' then shipping_cost_from_dec end where peso <=150; */

update production.tbl_order_detail inner join production.ops_shipping_cost_per_area on tbl_order_detail.state = ops_shipping_cost_per_area.state and tbl_order_detail.peso = ops_shipping_cost_per_area.kg set shipping_cost = case when package_weight=0 then shipping_cost_until_sept else shipping_cost_from_dec end where peso <=150;

/*update production.tbl_order_detail inner join production.ops_shipping_cost_per_area on tbl_order_detail.state = ops_shipping_cost_per_area.state set shipping_cost = case when date <= '2012-09-30' then (peso/150)*shipping_cost_until_sept when date between '2012-10-01'
and '2012-11-30' then (peso/150)*shipping_cost_from_oct when date >= '2012-12-01' then (peso/150)*shipping_cost_from_dec end where peso >=150;*/

update production.tbl_order_detail inner join production.ops_shipping_cost_per_area on 
tbl_order_detail.state = ops_shipping_cost_per_area.state 
and 150 = ops_shipping_cost_per_area.kg set shipping_cost = (peso/150)*shipping_cost_from_dec where peso >=150;

delete from production.tbl_order_detail_lookup;

insert into production.tbl_order_detail_lookup (select * from production.tbl_order_detail);

update production.tbl_order_detail o set 
shipping_cost_per_sku = case when o.package_weight=0 
then round((o.product_weight/o.peso)*o.shipping_cost) else round((o.package_weight/o.peso)*o.shipping_cost) end;

-- update production.tbl_order_detail o set shipping_cost_per_sku = (select t.package_weight/sum(package_weight)*shipping_cost from production.tbl_order_detail_lookup t where o.order_nr=t.order_nr group by o.order_nr);

#update source_medium
update production.tbl_order_detail t inner join SEM.transaction_id_mx c on t.order_nr=c.transaction_id set t.source_medium= concat(c.source,' / ',c.medium),t.campaign=c.campaign where t.item>=itemid;

update production.tbl_order_detail inner join bob_live_mx.sales_rule on code = coupon_code
inner join bob_live_mx.sales_rule_set on id_sales_rule_set = fk_sales_rule_set
set description_voucher = description 
where coupon_code is not null;

-- Category BP

update production.tbl_order_detail t inner join development_mx.A_E_M1_New_CategoryBP m on m.Cat1=t.new_cat1 set t.category_bp=m.catbp;

update production.tbl_order_detail t inner join development_mx.A_E_M1_New_CategoryBP m on m.Cat2=t.new_cat2 set t.category_bp=m.catbp where (t.new_cat1   =  "Entretenimiento" or t.new_cat1 like "Electr%nicos");

-- call extra_queries_2012();
-- call extra_queries_2013();
call extra_query();

select  'start channel_report: start',now();

call production.channel_report();

select  'start channel_report: end',now();

select  'start channel_report_no_voucher: start',now();

call production.channel_report_no_voucher();

select  'start channel_report_no_voucher: end',now();

#pc2 
select 'start pc2',now();
truncate production.tbl_group_order_detail;
insert into production.tbl_group_order_detail (select orderid, count(order_nr) as items,
sum(greatest(cast(replace(if(isnull(peso),1,peso),',','.') as decimal(21,6)),1.0))as pesototal,
sum(paid_price) as grandtotal
from production.tbl_order_detail where  oac='1' and returned='0' group by order_nr);	

truncate production.tbl_group_order_detail_gross;
insert into production.tbl_group_order_detail_gross (select orderid, count(order_nr) as items,
sum(greatest(cast(replace(if(isnull(peso),1,peso),',','.') as decimal(21,6)),1.0))as pesototal,
sum(paid_price) as grandtotal
from production.tbl_order_detail where  obc='1' group by order_nr);	


update   production.tbl_order_detail as t 
left join production.tbl_group_order_detail as e 
on e.orderid = t.orderid 
set t.orderpeso = e.pesototal , t.nr_items=e.items , t.ordertotal=e.grandtotal
where t.oac='1' and t.returned='0';

update   production.tbl_order_detail as t 
left join production.tbl_group_order_detail_gross as e 
on e.orderid = t.orderid 
set t.orderpeso_gross = e.pesototal , t.gross_items=e.items , t.ordertotal_gross=e.grandtotal
where t.obc='1';

update production.tbl_order_detail as t
set gross_orders = if(gross_items is null,0,1/gross_items) where obc = '1';

##update production.tbl_order_detail as t
##set net_orders = if(nr_items is null, 0,1/nr_items) where oac = '1' and returned=0;


-- New Net Orders

/*
create table production.temporary_net_orders as select order_nr, count(*) as items from production.tbl_order_detail where oac=1 group by order_nr;

create index order_nr on production.temporary_net_orders(order_nr);

update production.tbl_order_detail t inner join production.temporary_net_orders n on t.order_nr=n.order_nr set t.net_orders=1/n.items;

drop table production.temporary_net_orders;*/

#pc2: shipping_fee
/*update production.tbl_order_detail set ordershippingfee=0.0 where ordershippingfee is null;
update production.tbl_order_detail set shipping_fee=ordershippingfee/cast(nr_items as decimal) 
where oac='1' and returned='0';
update tbl_order_detail set shipping_fee_2=ordershippingfee/cast(nr_items as decimal);*/



update production.tbl_order_detail o set 
shipping_fee= 
case when o.package_weight=0 
then round((o.product_weight/o.peso)*o.ordershippingfee) 
else round((o.package_weight/o.peso)*o.ordershippingfee) end 
where yrmonth>=201305;
update production.tbl_order_detail set shipping_fee_after_vat=if(shipping_fee is null, 0, shipping_fee/1.16);
update production.tbl_order_detail 
inner join production.out_sales_report_item on 
out_sales_report_item.id_sales_order=tbl_order_detail.orderid and
out_sales_report_item.sku_simple=tbl_order_detail.sku
set shipping_fee_after_vat=shipping_fee_charged where
yrmonth<>201305;


update tbl_order_detail set shipping_fee_after_vat_2=if(shipping_fee_2 is null, 0, shipping_fee_2/1.16);
update tbl_order_detail set grand_total_after_vat=shipping_fee_after_vat_2+paid_price_after_vat;
update tbl_order_detail set gross_grand_total_after_vat=shipping_fee_after_vat_2+paid_price_after_vat where obc = '1';
update tbl_order_detail set net_grand_total_after_vat=shipping_fee_after_vat+paid_price_after_vat 
where (oac = 1 and returned = 0);

#SHIPPING FEE EXTRA

UPDATE production.tbl_order_detail SET tbl_order_detail.shipping_fee_after_vat=55
where production.tbl_order_detail.coupon_code='CAC0xY3Iz' OR production.tbl_order_detail.coupon_code='CACs4tyXP';

UPDATE production.tbl_order_detail SET tbl_order_detail.shipping_fee_after_vat=0 WHERE
order_nr='200149366' OR
order_nr='200211346' OR
order_nr='200252946' OR
order_nr='200268446' OR
order_nr='200274246' OR
order_nr='200278526' OR
order_nr='200288526' OR
order_nr='200299832' OR
order_nr='200322426' OR
order_nr='200348226' OR
order_nr='200352832' OR
order_nr='200369746' OR
order_nr='200384246' OR
order_nr='200472766' OR
order_nr='200482346' OR
order_nr='200486332' OR
order_nr='200493312' OR
order_nr='200517366' OR
order_nr='200557726' OR
order_nr='200569746' OR
order_nr='200569832' OR
order_nr='200588266' OR
order_nr='200588526' OR
order_nr='200596496' OR
order_nr='200668446' OR
order_nr='200698446' OR
order_nr='200728446' OR
order_nr='200728766' OR
order_nr='200857632' OR
order_nr='200867226' OR
order_nr='200868446';



#END SHIPPING FEE EXTRA
update production.tbl_order_detail inner join production.com_visa_promotion_product on tbl_order_detail.sku_config=com_visa_promotion_product.sku_config set shipping_fee_after_vat = shipping_fee_after_vat+com_visa_promotion_product.shipping_fee_charged_to_visa_after_vat;

#pc2: cs and wh
update production.tbl_order_detail 
inner join production.ops_wh_cs_per_order_for_pc2 on 
ops_wh_cs_per_order_for_pc2.yrmonth=tbl_order_detail.yrmonth set 
cs=ops_wh_cs_per_order_for_pc2.cs_cost_per_order_mxn/cast(nr_items as decimal),
wh=ops_wh_cs_per_order_for_pc2.wh_cost_per_order_mxn/cast(nr_items as decimal);


/* update production.tbl_order_detail set SELECT order_nr,month_num, yrmonth, ordershippingfee,shipping_fee, SUM(shipping_fee_charged),sum(shipping_fee_after_vat) as shipping_sql
from production.out_sales_report_item
inner join production.tbl_order_detail on tbl_order_detail.orderid=production.out_sales_report_item.id_sales_order
where order_after_can=1 
group by month_num, order_nrshipping_cost=(select  bogota from bob_live_mx.shipping_cost where peso=least(ceil(orderpeso),200))/cast(nr_items as decimal) where ciudad like '%bogota%' and oac='1' and returned='0' and date<='2012-12-11';
update production.tbl_order_detail set shipping_cost=(select  resto_pais from bob_live_mx.shipping_cost where peso=least(ceil(orderpeso),200))/cast(nr_items as decimal) where ciudad not like '%bogota%' and oac='1' and returned='0'and date<='2012-12-11';
update production.tbl_order_detail set shipping_cost= ordershippingfee/cast(nr_items as decimal) where oac='1' and returned='0' and date>'2012-12-11'; */

#pc2: payment_cost;

/*update production.tbl_order_detail inner join bob_live_mx.sales_order on tbl_order_detail.order_nr = sales_order.order_nr inner join production.com_fees_per_pm on sales_order.payment_method = com_fees_per_pm.payment_method inner join bob_live_mx.sales_order_item on sales_order_item.fk_sales_order = sales_order.id_sales_order inner join (select fk_sales_order, installment_months from bob_live_mx.payment_sales_banortepayworks union select fk_sales_order, installment_months from bob_live_mx.payment_sales_amex)banc on banc.fk_sales_order=sales_order.id_sales_order set payment_cost = (sales_order_item.paid_price/((100+sales_order_item.tax_percent)/100)*((com_fees_per_pm.fee)/100)+com_fees_per_pm.extra_charge) where banc.installment_months=com_fees_per_pm.installment;*/

update production.tbl_order_detail inner join bob_live_mx.sales_order on tbl_order_detail.order_nr = sales_order.order_nr inner join bob_live_mx.payment_sales_banortepayworks on payment_sales_banortepayworks.fk_sales_order=sales_order.id_sales_order set tbl_order_detail.installment =  payment_sales_banortepayworks.installment_months; 

update production.tbl_order_detail inner join bob_live_mx.sales_order on tbl_order_detail.order_nr = sales_order.order_nr inner join bob_live_mx.payment_sales_amex on payment_sales_amex.fk_sales_order=sales_order.id_sales_order set tbl_order_detail.installment =  payment_sales_amex.installment_months; 

update production.tbl_order_detail set tbl_order_detail.installment = '0' where tbl_order_detail.installment is null;

update production.tbl_order_detail set tbl_order_detail.extra_charge = '0' where tbl_order_detail.extra_charge is null;

update production.tbl_order_detail set tbl_order_detail.fee = '0' where tbl_order_detail.fee is null;

update production.tbl_order_detail left join production.com_fees_per_pm on (tbl_order_detail.installment = com_fees_per_pm.installment) and (tbl_order_detail.payment_method = com_fees_per_pm.payment_method) set tbl_order_detail.fee = com_fees_per_pm.fee, tbl_order_detail.extra_charge = com_fees_per_pm.extra_charge;

update production.tbl_order_detail set payment_cost = (tbl_order_detail.paid_price/((100+tbl_order_detail.tax_percent)/100)*((tbl_order_detail.fee)/100)+tbl_order_detail.extra_charge);

update production.tbl_order_detail set payment_cost=0 where coupon_code in('com1tw3bz', 'com1qugjr', 'comq517vw', 'comc4jitb', 'com57omuy');

#wh shipping_cost para drop shipping
/*update production.tbl_order_detail set wh =0.0, shipping_cost=0.0 where sku in (select sku from bob_live_mx.catalog_simple where fk_catalog_shipment_type=2);*/

#proveedor entrega:
-- update  production.tbl_order_detail set delivery_cost_supplier=0.0 where proveedor='izc mayorista s.a.s';

#null fiedls
update production.tbl_order_detail set shipping_fee=0.0 where shipping_fee is null;
update production.tbl_order_detail set delivery_cost_supplier=0.0 where delivery_cost_supplier is null;
update production.tbl_order_detail set wh=0.0 where wh is null;
update production.tbl_order_detail set cs=0.0 where cs is null;
update production.tbl_order_detail set payment_cost=0.0 where payment_cost is null;
update production.tbl_order_detail set shipping_cost=0.0 where shipping_cost is null;

#campaign
-- update production.tbl_order_detail set campaign = 'lizamoran' where campaign = 'liza.moran';

#shipping_cost
-- call shipping_cost_order_city();

/*update production.tbl_order_detail join(select date, t2.orderid, t2.sku_config, city, shipment_zone, t1.shipping_cost as shipping_cost_menor, t2.shipping_cost, 
if(t1.shipping_cost > 0 and t1.shipping_cost < t2.shipping_cost, '1','0') as r 
from 
production.tbl_shipment_cost_calculado t1
right join production.tbl_shipping_cost_order_city t2
on t1.sku_config = t2.sku_config and fk_shipment_zone = shipment_zone
where month(date) >= 2 and year(date) >= 2013
group by  date, t2.orderid, t2.sku_config, city, shipment_zone) tabla
on tbl_order_detail.orderid = tabla.orderid
set tbl_order_detail.shipping_cost = tabla.shipping_cost_menor where r = '1'
and month(tbl_order_detail.date) >= 2 and year(tbl_order_detail.date) >= 2013; */

#daily report
-- call daily_report();


#tbl_monthly_cohort_sin_groupon
truncate production.tbl_monthly_cohort_sin_groupon;
insert into tbl_monthly_cohort_sin_groupon(custid, netsales, orders, firstorder, lastorder, idfirstorder, idlastorder, frecuencia,
coupon_code,  channel, channel_group)
select t1.*, t2.coupon_code, t2.channel,t2.channel_group from
(select 
        tbl_order_detail.custid as custid,
        sum(tbl_order_detail.paid_price_after_vat) as netsales,
        count(distinct tbl_order_detail.order_nr) as orders,
        min(tbl_order_detail.date) as firstorder,
        max(tbl_order_detail.date) as lastorder,
        min(tbl_order_detail.orderid) as idfirstorder,
        max(tbl_order_detail.orderid) as idlastorder,
        count(distinct tbl_order_detail.date) as frecuencia
    from
        production.tbl_order_detail
    where
        ((tbl_order_detail.oac = '1')
            and (tbl_order_detail.returned = '0'))
    group by tbl_order_detail.custid
    order by sum(tbl_order_detail.paid_price_after_vat) desc) as t1
inner join (select  orderid,coupon_code,channel,channel_group from production.tbl_order_detail where oac = '1' and returned = '0'and coupon_code not like 'gr%')
as t2 on t1.idfirstorder = t2.orderid
group by t1.custid, t2.coupon_code
order by sum(t1.netsales) desc;

update production.tbl_order_detail inner join  
(select firstOrder as date,custID as newCustomer, idFirstOrder from production.view_cohort ) as tbl_nc on tbl_nc.date=tbl_order_detail.date and tbl_nc.newCustomer = tbl_order_detail.custID and tbl_nc.idFirstOrder = orderID
set tbl_order_detail.new_customers=1/nr_items;

update production.tbl_order_detail p inner join bob_live_mx.catalog_config conf on p.sku_config=conf.sku inner join bob_live_mx.catalog_attribute_option_global_category c on c.id_catalog_attribute_option_global_category=conf.fk_catalog_attribute_option_global_category set p.new_cat1 = c.name where p.new_cat1 is null;

update production.tbl_order_detail p inner join bob_live_mx.catalog_config conf on p.sku_config=conf.sku inner join bob_live_mx.catalog_attribute_option_global_sub_category c on c.id_catalog_attribute_option_global_sub_category=conf.fk_catalog_attribute_option_global_sub_category set p.new_cat2 = c.name where p.new_cat2 is null;

update production.tbl_order_detail p inner join bob_live_mx.catalog_config conf on p.sku_config=conf.sku inner join bob_live_mx.catalog_attribute_option_global_sub_sub_category c on c.id_catalog_attribute_option_global_sub_sub_category=conf.fk_catalog_attribute_option_global_sub_sub_category set p.new_cat3 = c.name where p.new_cat3 is null;

#Tbl_monthly_cohort
truncate tbl_monthly_cohort;
insert into tbl_monthly_cohort(CustID,cohort,idFirstOrder,idLastOrder)
select CustId,
cast(concat(year(firstOrder),if(length(month(firstOrder)) < 2,concat('0', month(firstOrder)),month(firstOrder)))as signed) as cohort,
idFirstOrder,idLastOrder from view_cohort;

update production.tbl_monthly_cohort inner join production.tbl_order_detail on idFirstOrder = orderID and tbl_monthly_cohort.CustID = tbl_order_detail.CustID
set tbl_monthly_cohort.coupon_code = tbl_order_detail.coupon_code;

update production.tbl_monthly_cohort set coupon_code = '' where coupon_code is null;

#New_customers
update production.tbl_order_detail inner join  
(select firstOrder as date,custID as newCustomer, idFirstOrder from production.view_cohort_gross ) as tbl_nc on tbl_nc.date=tbl_order_detail.date and tbl_nc.newCustomer = tbl_order_detail.custID and tbl_nc.idFirstOrder = orderID
set tbl_order_detail.new_customers_gross=1/gross_items;

update production.tbl_order_detail set PC1 = ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0);

update production.tbl_order_detail set PC2 = PC1-ifnull(payment_cost,0)-ifnull(shipping_cost_per_sku,0)-ifnull(wh,0)-ifnull(cs,0);

update production.tbl_order_detail t inner join(select s.code, t.name, t.description from bob_live_mx.sales_rule s, bob_live_mx.sales_rule_set t where t.id_sales_rule_set=s.fk_sales_rule_set)a  on t.coupon_code=a.code set t.description_voucher=a.name where t.item>=itemid;

select  'update tbl_order_detail: end',now();

#channel clv
truncate production.tbl_monthly_cohort_channel;
insert into production.tbl_monthly_cohort_channel (fk_customer,cohort,idfirstorder,channel_group)
select tbl_monthly_cohort.custid as fk_customer,cohort,idfirstorder,tbl_order_detail.channel_group as channel
from production.tbl_monthly_cohort left join production.tbl_order_detail on orderid=idfirstorder
group by tbl_monthly_cohort.custid;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Mexico', 
  'tbl_order_detail',
  NOW(),
  max(date),
  count(*),
  count(*)
FROM
  production.tbl_order_detail;


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `visible_pc2`()
begin

call production.catalog_visible;

delete from production.visible_pc2;

insert into production.visible_pc2(sku_simple, product_name, original_price, price, special_price, `% Discount`) select v.sku_simple, v.name, s.original_price, s.price, s.special_price, (1-(s.price/s.original_price))*100 from production.catalog_visible v inner join bob_live_mx.catalog_simple s on s.sku=v.sku_simple group by v.sku_simple; 

update production.visible_pc2 v set `% PC2` = (select (avg(ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0))/avg(unit_price_after_vat))*100 from production.tbl_order_detail t where t.sku=v.sku_simple and t.oac=1 and t.date > date_sub(curdate(), interval 90 day) group by t.sku);

update production.visible_pc2 v set `% PC2Adjust` = (select (avg(ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0))/avg(unit_price_after_vat))*100 from production.tbl_order_detail t where t.sku=v.sku_simple and t.oac=1 and t.date > date_sub(curdate(), interval 90 day) group by t.sku);

create table production.temporary_visible(
sku_simple varchar(255),
n2 varchar(255),
avg_pc2 float,
avg_pc2adjusted float
);

create index temporary on production.temporary_visible(n2, sku_simple);

insert into production.temporary_visible(sku_simple, n2) select sku_simple, cat2 from production.visible_pc2 v, production.tbl_catalog_product_v2 c where v.sku_simple = c.sku; 

create table production.temporary_visible2 as select n2, (avg(ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0))/avg(unit_price_after_vat))*100 as pc2, (avg(ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0))/avg(unit_price_after_vat))*100 as pc2adjusted from production.tbl_order_detail t where oac=1 and t.date > date_sub(curdate(), interval 90 day) group by n2;

create index n2 on production.temporary_visible2(n2);

update production.temporary_visible x inner join production.temporary_visible2 y on x.n2=y.n2 set x.avg_pc2=y.pc2, x.avg_pc2adjusted=y.pc2adjusted;

drop table production.temporary_visible2;

/*
update production.temporary tbl set avg_pc2 = (select (avg(ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0)-ifnull(wh,0)-ifnull(cs,0))/avg(unit_price_after_vat))*100 from production.tbl_order_detail t where tbl.n2=t.n2 and t.oac=1 and t.date > date_sub(curdate(), interval 90 day) group by tbl.n2);

update production.temporary tbl set avg_pc2adjusted = (select (avg(ifnull(actual_paid_price_after_tax,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(t.delivery_cost_supplier,0)-ifnull(shipping_cost_per_sku,0)-ifnull(payment_cost,0))/avg(unit_price_after_vat))*100 from production.tbl_order_detail t where tbl.n2=t.n2 and t.oac=1 and t.date > date_sub(curdate(), interval 90 day) group by tbl.n2);*/

update production.visible_pc2 v inner join production.temporary_visible t on t.sku_simple=v.sku_simple set `% PC2` = avg_pc2, `% PC2Adjust` = avg_pc2adjusted where `% PC2` is null;

drop table production.temporary_visible;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `visits_costs_channel`()
begin

delete from production.vw_visits_costs_channel;
delete from production.daily_costs_visits_per_channel;

insert into production.vw_visits_costs_channel (date, channel_group, sum_visits, sum_adcost) 
select v.*, if(c.adCost is null, 0, c.adCost) adCost from (
select date, channel_group, sum(if(adCost is null, 0, adCost)) adCost from production.ga_cost_campaign
group by date , channel_group) c right join (
select date, channel_group, sum(visits) visits from production.ga_visits_cost_source_medium
group by date , channel_group) v on c.channel_group = v.channel_group and c.date = v.date
order by c.date desc;

insert into production.daily_costs_visits_per_channel (date, cost_local, reporting_channel, visits) select date, sum_adcost, channel_group, sum_visits from production.vw_visits_costs_channel;

update production.daily_costs_visits_per_channel
set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)));

update production.daily_costs_visits_per_channel set month = month(date);

update production.daily_costs_visits_per_channel set reporting_channel = 'Non identified' where reporting_channel is null;

delete from production.media_rev_orders;

insert into production.media_rev_orders(date, channel_group, net_rev, gross_rev, net_orders, gross_orders, new_customers_gross, new_customers) select date, channel_group, sum(net_grand_total_after_vat), sum(gross_grand_total_after_vat), sum(net_orders), sum(gross_orders), sum(new_customers_gross), sum(new_customers) from production.tbl_order_detail group by date, channel_group;

update production.daily_costs_visits_per_channel a inner join production.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.gross_rev = b.gross_rev;

update production.daily_costs_visits_per_channel a inner join production.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.gross_orders = b.gross_orders;

update production.daily_costs_visits_per_channel a inner join production.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.new_customers_gross = b.new_customers_gross;

update production.daily_costs_visits_per_channel a set net_rev_e = (select (avg(net_rev/gross_rev)) from production.media_rev_orders b where b.date between date_sub(a.date, interval 21 day) and date_sub(a.date,interval 7 day) and a.reporting_channel=b.channel_group group by reporting_channel);

update production.daily_costs_visits_per_channel a set net_orders_e = (select (avg(net_orders/gross_orders)) from production.media_rev_orders b where b.date between date_sub(a.date, interval 21 day) and date_sub(a.date,interval 7 day) and a.reporting_channel=b.channel_group group by reporting_channel);

update production.daily_costs_visits_per_channel a set new_customers_e = (select (avg(new_customers/new_customers_gross)) from production.media_rev_orders b where b.date between date_sub(a.date, interval 21 day) and date_sub(a.date,interval 7 day) and a.reporting_channel=b.channel_group group by reporting_channel);

update production.daily_costs_visits_per_channel set net_rev_e = net_rev_e*gross_rev, net_orders_e = net_orders_e*gross_orders, new_customers_e = new_customers_e*new_customers_gross;

update production.daily_costs_visits_per_channel a inner join production.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.net_rev_e = b.net_rev, a.net_orders_e = b.net_orders, a.new_customers_e = b.new_customers where a.date < date_sub(curdate(), interval 14 day);

update production.daily_costs_visits_per_channel a inner join production.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.net_rev_e = case when b.net_rev>a.net_rev_e then b.net_rev else a.net_rev_e end, a.net_orders_e = case when b.net_orders>a.net_orders_e then b.net_orders else a.net_orders_e end, a.new_customers_e = case when b.new_customers>a.new_customers_e then b.new_customers else a.new_customers_e end;

update production.daily_costs_visits_per_channel a inner join production.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.net_rev_e = case when a.net_rev_e is null then b.net_rev else a.net_rev_e end, a.net_orders_e = case when a.net_orders_e is null then b.net_orders else a.net_orders_e end, a.new_customers_e = case when a.new_customers_e is null then b.new_customers else a.new_customers_e end;

update production.daily_costs_visits_per_channel a inner join production.daily_targets_per_channel b 
on a.reporting_channel=b.channel_group and a.date = b.date set a.target_net_sales = b.target_net_sales, a.target_cpo = b.target_cpo;


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`danielbarrera`@`%`*/ /*!50003 PROCEDURE `warranty_extended`()
begin

delete from production.warranty_extended;

insert into production.warranty_extended(date_ordered, order_nr, warranty_sku_simple, warranty_name, warranty_retail_price, product_sku_simple) select i.created_at, o.order_nr, i.sku, i.name, i.unit_price, i.sku_parent_supplementary from bob_live_mx.sales_order o, bob_live_mx.sales_order_item i where o.id_sales_order=i.fk_sales_order and sku_parent_supplementary is not null and (sku like 'EW%' or sku like 'RW%');

update production.warranty_extended set yrmonth = concat(year(date_ordered),if(month(date_ordered)<10,concat(0,month(date_ordered)),month(date_ordered))), week=production.week_iso(date_ordered);

update production.warranty_extended w inner join production.tbl_order_detail t on w.product_sku_simple=t.sku and w.order_nr=t.order_nr set w.product_sku_config = t.sku_config, w.product_name=t.product_name, w.product_retail_price=t.unit_price, w.product_cost=t.cost_pet, w.status_item=t.status_item;

update production.warranty_extended p inner join bob_live_mx.catalog_config conf on p.product_sku_config=conf.sku inner join bob_live_mx.catalog_attribute_option_global_category c on c.id_catalog_attribute_option_global_category=conf.fk_catalog_attribute_option_global_category set p.main_category = c.name;

update production.warranty_extended p inner join bob_live_mx.catalog_config conf on p.product_sku_config=conf.sku inner join bob_live_mx.catalog_attribute_option_global_sub_category c on c.id_catalog_attribute_option_global_sub_category=conf.fk_catalog_attribute_option_global_sub_category set p.sub_category = c.name;

update production.warranty_extended p inner join bob_live_mx.catalog_config conf on p.product_sku_config=conf.sku inner join bob_live_mx.catalog_attribute_option_global_sub_sub_category c on c.id_catalog_attribute_option_global_sub_sub_category=conf.fk_catalog_attribute_option_global_sub_sub_category set p.sub_sub_category = c.name;

update production.warranty_extended w inner join rafael.out_order_tracking t on w.order_nr=t.order_number and w.product_sku_simple=t.sku_simple set w.date_delivered=t.date_delivered; 

update production.warranty_extended w set warranty_category = 'Extended Warranty' where warranty_sku_simple like 'EW%';

update production.warranty_extended w set warranty_category = 'Replacement Warranty' where warranty_sku_simple like 'RW%';

update production.warranty_extended w set order_total = (select sum(actual_paid_price) from production.tbl_order_detail t where t.order_nr=w.order_nr group by order_nr);

update production.warranty_extended w inner join bob_live_mx.catalog_simple s on w.warranty_sku_simple=s.sku set warranty_cost= s.cost; 

update production.category_visits_and_pi set main_category = substr(substr(substr(substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1),locate('.', substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1))+1) ,locate('.', substr(substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1),locate('.', substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1))+1))+1) , locate('.', substr(substr(substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1),locate('.', substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1))+1) ,locate('.', substr(substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1),locate('.', substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1))+1))+1))+1) where main_category is null;

update production.category_product_clicks set category = substr(substr(substr(substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1),locate('.', substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1))+1) ,locate('.', substr(substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1),locate('.', substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1))+1))+1) , locate('.', substr(substr(substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1),locate('.', substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1))+1) ,locate('.', substr(substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1),locate('.', substr(substr(pages, locate('.',pages)+1) ,locate('.' ,substr(pages, locate('.',pages)+1))+1))+1))+1))+1) where category is null;

update production.category_product_clicks set sku_config = substring_index(substr(events, locate(':',events)+1), '.', 1) where sku_config is null;

delete from production.webtrekk_warranty;

insert into production.webtrekk_warranty(date, sku_config, main_category, clicks) select date, sku_config, category, clicks from production.category_product_clicks;

update production.webtrekk_warranty w inner join production.category_visits_and_pi c on w.main_category=c.main_category set w.category_impressions=c.impressions, w.category_visits=c.visits;

update production.warranty_extended w inner join production.webtrekk_warranty t on w.product_sku_config=t.sku_config and w.date_ordered=t.date set w.category_impressions=t.category_impressions, w.category_visits=t.category_visits;

update production.warranty_extended w set w.clicks = (select sum(t.clicks) from production.webtrekk_warranty t where t.sku_config=w.product_sku_config and t.date=w.date_ordered);

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `_Navicat_Temp_Stored_Proc`()
begin

delete from production.table_monitoring;

insert into production.table_monitoring(table_name, Mexico) select 'BOB', created_at from bob_live_mx.sales_order order by created_at desc limit 1;	

update production.table_monitoring set Colombia = (select created_at from bob_live_co.sales_order order by created_at desc limit 1 ) where table_name = 'BOB';	

update production.table_monitoring set Peru = (select created_at from bob_live_pe.sales_order order by created_at desc limit 1 ) where table_name = 'BOB';	

update production.table_monitoring set Venezuela = (select created_at from bob_live_ve.sales_order order by created_at desc limit 1 ) where table_name = 'BOB';

insert into production.table_monitoring(table_name, Mexico) select 'WMS', data_criacao from wmsprod.itens_venda order by data_criacao desc limit 1;	

update production.table_monitoring set Colombia = (select data_criacao from wmsprod_co.itens_venda order by data_criacao desc limit 1 ) where table_name = 'WMS';	

update production.table_monitoring set Peru = (select data_criacao from wmsprod_pe.itens_venda order by data_criacao desc limit 1 ) where table_name = 'WMS';	

update production.table_monitoring set Venezuela = (select data_criacao from wmsprod_ve.itens_venda order by data_criacao desc limit 1 ) where table_name = 'WMS';	

insert into production.table_monitoring(table_name, Mexico) select 'OMS', updated_at from procurement_live_mx.procurement_order_item order by updated_at desc limit 1;	

update production.table_monitoring set Colombia = (select updated_at from procurement_live_co.procurement_order_item order by updated_at desc limit 1 ) where table_name = 'OMS';	

update production.table_monitoring set Peru = (select updated_at from procurement_live_pe.procurement_order_item order by updated_at desc limit 1 ) where table_name = 'OMS';	

update production.table_monitoring set Venezuela = (select updated_at from procurement_live_ve.procurement_order_item order by updated_at desc limit 1 ) where table_name = 'OMS';

insert into production.table_monitoring(table_name, Mexico) select 'Global GA', date from SEM.campaign_ad_group_mx order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from SEM.campaign_ad_group_co order by date desc limit 1 ) where table_name = 'Global GA';	

update production.table_monitoring set Peru = (select date from SEM.campaign_ad_group_pe order by date desc limit 1 ) where table_name = 'Global GA';

update production.table_monitoring set Venezuela = (select date from SEM.campaign_ad_group_ve order by date desc limit 1 ) where table_name = 'Global GA';

--

insert into production.table_monitoring(table_name, Mexico) select 'Keyword GA', date from SEM.campaign_keyword_mx order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from SEM.campaign_keyword_co order by date desc limit 1 ) where table_name = 'Keyword GA';	

update production.table_monitoring set Peru = (select date from SEM.campaign_keyword_pe order by date desc limit 1 ) where table_name = 'Keyword GA';

update production.table_monitoring set Venezuela = (select date from SEM.campaign_keyword_ve order by date desc limit 1 ) where table_name = 'Keyword GA';

--

insert into production.table_monitoring(table_name, Colombia) select 'Global Fashion GA', date from SEM.campaign_ad_group_fashion_co order by date desc limit 1;	

insert into production.table_monitoring(table_name, Mexico) select 'Facebook GA', date from facebook.ga_facebook_ads_region_mx order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from facebook.ga_facebook_ads_region_co order by date desc limit 1 ) where table_name = 'Facebook GA';	

update production.table_monitoring set Peru = (select date from facebook.ga_facebook_ads_region_pe order by date desc limit 1 ) where table_name = 'Facebook GA';

insert into production.table_monitoring(table_name, Mexico) select 'Mobile App GA', date from production.mobileapp_transaction_id order by date desc limit 1;

update production.table_monitoring set Colombia = (select date from production_co.mobileapp_transaction_id order by date desc limit 1 ) where table_name = 'Mobile App GA';

update production.table_monitoring set Peru = (select date from production_pe.mobileapp_transaction_id order by date desc limit 1 ) where table_name = 'Mobile App GA';

update production.table_monitoring set Venezuela = (select date from production_ve.mobileapp_transaction_id order by date desc limit 1 ) where table_name = 'Mobile App GA';

--

insert into production.table_monitoring(table_name, Mexico) select 'Facebook Keyword GA', date from facebook.ga_facebook_ads_keyword_mx order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from facebook.ga_facebook_ads_keyword_co order by date desc limit 1 ) where table_name = 'Facebook Keyword GA';	

update production.table_monitoring set Peru = (select date from facebook.ga_facebook_ads_keyword_pe order by date desc limit 1 ) where table_name = 'Facebook Keyword GA';

--

insert into production.table_monitoring(table_name, Mexico) select 'Facebook Cost', date from facebook.facebook_campaign_mx order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from facebook.facebook_campaign_co order by date desc limit 1 ) where table_name = 'Facebook Cost';	

update production.table_monitoring set Peru = (select date from facebook.facebook_campaign_pe order by date desc limit 1 ) where table_name = 'Facebook Cost';

update production.table_monitoring set Venezuela = (select date from facebook.facebook_campaign_ve order by date desc limit 1 ) where table_name = 'Facebook Cost';

insert into production.table_monitoring(table_name, Colombia) select 'Facebook Fashion Cost', date from facebook.facebook_campaign_fashion_co order by date desc limit 1;	

--

insert into production.table_monitoring(table_name, Mexico) select 'Criteo Cost', date from marketing_report.criteo where country='MEX' order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from marketing_report.criteo where country='COL' order by date desc limit 1 ) where table_name = 'Criteo Cost';	

--

insert into production.table_monitoring(table_name, Mexico) select 'Sociomantic Cost', date from marketing_report.sociomantic where country='MEX' order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from marketing_report.sociomantic where country='COL' order by date desc limit 1 ) where table_name = 'Sociomantic Cost';	

update production.table_monitoring set Peru = (select date from marketing_report.sociomantic where country='PER' order by date desc limit 1 ) where table_name = 'Sociomantic Cost';	

update production.table_monitoring set Venezuela = (select date from marketing_report.sociomantic where country='VEN' order by date desc limit 1 ) where table_name = 'Sociomantic Cost';	

--

insert into production.table_monitoring(table_name, Mexico) select 'Triggit Cost', report_start_date from marketing_report.triggit_mx order by report_start_date desc limit 1;	

update production.table_monitoring set Colombia = (select report_start_date from marketing_report.triggit_co order by report_start_date desc limit 1 ) where table_name = 'Triggit Cost';	

update production.table_monitoring set Peru = (select report_start_date from marketing_report.triggit_pe order by report_start_date desc limit 1 ) where table_name = 'Triggit Cost';	

update production.table_monitoring set Venezuela = (select report_start_date from marketing_report.triggit_ve order by report_start_date desc limit 1 ) where table_name = 'Triggit Cost';	

--

insert into production.table_monitoring(table_name, Mexico) select 'Vizury Cost', date from marketing_report.vizury_mx order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from marketing_report.vizury_co order by date desc limit 1 ) where table_name = 'Vizury Cost';

update production.table_monitoring set Peru = (select date from marketing_report.vizury_pe order by date desc limit 1 ) where table_name = 'Vizury Cost';

update production.table_monitoring set Venezuela = (select date from marketing_report.vizury_ve order by date desc limit 1 ) where table_name = 'Vizury Cost';

--

insert into production.table_monitoring(table_name, Mexico) select 'Trade Tracker Cost', date from marketing_report.affiliates_mx where network='TradeTracker' order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from marketing_report.affiliates_co where network='TradeTracker' order by date desc limit 1 ) where table_name = 'Trade Tracker Cost';

update production.table_monitoring set Peru = (select date from marketing_report.affiliates_pe where network='TradeTracker' order by date desc limit 1 ) where table_name = 'Trade Tracker Cost';

update production.table_monitoring set Venezuela = (select date from marketing_report.affiliates_ve where network='TradeTracker' order by date desc limit 1 ) where table_name = 'Trade Tracker Cost';

--

insert into production.table_monitoring(table_name, Mexico) select 'GLG Cost', date from marketing_report.affiliates_mx where network='GlobalLeadsGroup' order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from marketing_report.affiliates_co where network='GlobalLeadsGroup' order by date desc limit 1 ) where table_name = 'GLG Cost';

update production.table_monitoring set Peru = (select date from marketing_report.affiliates_pe where network='GlobalLeadsGroup' order by date desc limit 1 ) where table_name = 'GLG Cost';

update production.table_monitoring set Venezuela = (select date from marketing_report.affiliates_ve where network='GlobalLeadsGroup' order by date desc limit 1 ) where table_name = 'GLG Cost';

--

insert into production.table_monitoring(table_name, Mexico) select 'Pampa Network Cost', date from marketing_report.affiliates_mx where network='PampaNetwork' order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from marketing_report.affiliates_co where network='PampaNetwork' order by date desc limit 1 ) where table_name = 'Pampa Network Cost';

update production.table_monitoring set Peru = (select date from marketing_report.affiliates_pe where network='PampaNetwork' order by date desc limit 1 ) where table_name = 'Pampa Network Cost';

update production.table_monitoring set Venezuela = (select date from marketing_report.affiliates_ve where network='PampaNetwork' order by date desc limit 1 ) where table_name = 'Pampa Network Cost';

--

insert into production.table_monitoring(table_name, Mexico) select 'Experian Cost', date from development_mx.M_Other_Cost where country='MEX' and type='experian' order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from development_mx.M_Other_Cost where country='COL' and type='experian' order by date desc limit 1 ) where table_name = 'Experian Cost';

update production.table_monitoring set Peru = (select date from development_mx.M_Other_Cost where country='PER' and type='experian' order by date desc limit 1 ) where table_name = 'Experian Cost';

update production.table_monitoring set Venezuela = (select date from development_mx.M_Other_Cost where country='VEN' and type='experian' order by date desc limit 1 ) where table_name = 'Experian Cost';

--

insert into production.table_monitoring(table_name, Colombia) select 'ICCK Cost', date from marketing_report.icck where country='Colombia' order by date desc limit 1;	

--

insert into production.table_monitoring(table_name, Colombia) select 'ExoClick Cost', date from marketing_report.exoclick where country='Colombia' order by date desc limit 1;	

--

insert into production.table_monitoring(table_name, Mexico) select 'tbl_order_detail', date from production.tbl_order_detail order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from production_co.tbl_order_detail order by date desc limit 1 ) where table_name = 'tbl_order_detail';	

update production.table_monitoring set Peru = (select date from production_pe.tbl_order_detail order by date desc limit 1 ) where table_name = 'tbl_order_detail';	

update production.table_monitoring set Venezuela = (select date from production_ve.tbl_order_detail order by date desc limit 1 ) where table_name = 'tbl_order_detail';

--

insert into production.table_monitoring(table_name, Mexico) select 'A_Master', concat(date,' ', time)  from development_mx.A_Master order by date desc, time desc limit 1;	

update production.table_monitoring set Colombia = (select concat(date,' ', time) from development_co_project.A_Master order by date desc, time desc limit 1 ) where table_name = 'A_Master';	

update production.table_monitoring set Peru = (select concat(date,' ', time) from development_pe.A_Master order by date desc, time desc limit 1 ) where table_name = 'A_Master';	

update production.table_monitoring set Venezuela = (select concat(date,' ', time) from development_ve.A_Master order by date desc, time desc limit 1 ) where table_name = 'A_Master';

--

insert into production.table_monitoring(table_name, Mexico) select 'global_report', date  from marketing_report.global_report where country='Mexico' order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date  from marketing_report.global_report where country='Colombia' order by date desc limit 1 ) where table_name = 'global_report';	

update production.table_monitoring set Peru = (select date  from marketing_report.global_report where country='Peru' order by date desc limit 1 ) where table_name = 'global_report';	

update production.table_monitoring set Venezuela = (select date  from marketing_report.global_report where country='Venezuela' order by date desc limit 1 ) where table_name = 'global_report';	

--

insert into production.table_monitoring(table_name, Mexico) select 'Attribution Campaign', date  from attribution_testing.getfullcampaign_mx order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from attribution_testing.getfullcampaign_co order by date  desc limit 1 ) where table_name = 'Attribution Campaign';	

update production.table_monitoring set Peru = (select date from attribution_testing.getfullcampaign_pe order by date  desc limit 1 ) where table_name = 'Attribution Campaign';	

update production.table_monitoring set Venezuela = (select date from attribution_testing.getfullcampaign_ve order by date  desc limit 1 ) where table_name = 'Attribution Campaign';	

insert into production.table_monitoring(table_name, Mexico) select 'Attribution Campaign Config', date  from attribution_testing.campaigns_config_mx order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from attribution_testing.campaigns_config_co order by date  desc limit 1 ) where table_name = 'Attribution Campaign Config';	

update production.table_monitoring set Peru = (select date from attribution_testing.campaigns_config_pe order by date  desc limit 1 ) where table_name = 'Attribution Campaign Config';	

update production.table_monitoring set Venezuela = (select date from attribution_testing.campaigns_config_ve order by date  desc limit 1 ) where table_name = 'Attribution Campaign Config';	

insert into production.table_monitoring(table_name, Mexico) select 'Attribution Orders', date  from attribution_testing.getfullorders_mx order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from attribution_testing.getfullorders_co order by date  desc limit 1 ) where table_name = 'Attribution Orders';

update production.table_monitoring set Peru = (select date from attribution_testing.getfullorders_pe order by date  desc limit 1 ) where table_name = 'Attribution Orders';

update production.table_monitoring set Venezuela = (select date from attribution_testing.getfullorders_ve order by date  desc limit 1 ) where table_name = 'Attribution Orders';

insert into production.table_monitoring(table_name, Mexico) select 'Attribution Visitors', date  from attribution_testing.getfullvisitors_mx order by date desc limit 1;	

update production.table_monitoring set Colombia = (select date from attribution_testing.getfullvisitors_co order by date  desc limit 1 ) where table_name = 'Attribution Visitors';

update production.table_monitoring set Peru = (select date from attribution_testing.getfullvisitors_pe order by date  desc limit 1 ) where table_name = 'Attribution Visitors';

update production.table_monitoring set Venezuela = (select date from attribution_testing.getfullvisitors_ve order by date  desc limit 1 ) where table_name = 'Attribution Visitors';

--

insert into production.table_monitoring(table_name, Mexico) select 'Attribution Basket', date  from attribution_testing.getfullbasket_mx order by date desc limit 1;

update production.table_monitoring set Colombia = (select date from attribution_testing.getfullbasket_co order by date  desc limit 1 ) where table_name = 'Attribution Basket';

update production.table_monitoring set Peru = (select date from attribution_testing.getfullbasket_pe order by date  desc limit 1 ) where table_name = 'Attribution Basket';

update production.table_monitoring set Venezuela = (select date from attribution_testing.getfullbasket_ve order by date  desc limit 1 ) where table_name = 'Attribution Basket';

--

insert into production.table_monitoring(table_name, Mexico) select 'Attribution Clicks', date  from attribution_testing.getfullclicks_mx order by date desc limit 1;

update production.table_monitoring set Colombia = (select date from attribution_testing.getfullclicks_co order by date  desc limit 1 ) where table_name = 'Attribution Clicks';

update production.table_monitoring set Peru = (select date from attribution_testing.getfullclicks_pe order by date  desc limit 1 ) where table_name = 'Attribution Clicks';

update production.table_monitoring set Venezuela = (select date from attribution_testing.getfullclicks_ve order by date  desc limit 1 ) where table_name = 'Attribution Clicks';

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:04:16
