-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: finance_ve
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'finance_ve'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`leonardo.fraile`@`%`*/ /*!50003 PROCEDURE `cargar_kpi`()
BEGIN
DECLARE tasa_euro, month_num, temporal, rent_wh, depreciation_equipment_wh, wh_logistics_f_l_cost, customer_care_f_l_cost, global_mkt_cost, online_mkt_cost, offline_mkt_cost INT;


SET tasa_euro = 70;																						
SET month_num = 201312;																				
SET rent_wh = 180000; 
SET depreciation_equipment_wh = 2250; 
SET wh_logistics_f_l_cost = 524892; 
SET customer_care_f_l_cost = 332867; 


UPDATE data_kpi_p_l set is_b2b="VERDADERO" WHERE UPPER(code_prefix) like '%CDEAL%';

SELECT sum(actual_paid_price_after_tax) into temporal from data_kpi_p_l where is_b2b="FALSO";
INSERT INTO kpi_p_l VALUES (month_num,1,"P&L (ACCOUNTING VIEW - NOT CASH VIEW)",262,"Net revenue from merchandise (in k local)","Net sales as per accounting rules (usually sales recognition as per shipped order or, for COD, upon proof of delivery); after VAT, discounts, coupons, returns and cancelations; includes provision for returns",temporal/1000,NOW());

SELECT sum(actual_paid_price_after_tax) into temporal from data_kpi_p_l where is_b2b="VERDADERO";
IF temporal is NULL THEN 
	SET temporal = 0;
END IF;
INSERT INTO kpi_p_l VALUES (month_num,1,"P&L (ACCOUNTING VIEW - NOT CASH VIEW)",263,"Net revenue from B2B merchandise sales (in k local)","Net sales as per accounting rules (usually sales recognition as per shipped order or, for COD, upon proof of delivery); after VAT, discounts, coupons, returns and cancelations; includes provision for returns",temporal/1000,NOW());

INSERT INTO kpi_p_l VALUES (month_num,1,"P&L (ACCOUNTING VIEW - NOT CASH VIEW)",264,"Net revenue from offline merchandise sales (in k local)","Offline net sales as per accounting rules",0,NOW());

SELECT sum(shipping_fee_charged) into temporal from data_kpi_p_l;
IF temporal is NULL THEN 
	SET temporal = 0;
END IF;
INSERT INTO kpi_p_l VALUES (month_num,1,"P&L (ACCOUNTING VIEW - NOT CASH VIEW)",265,"Shipping net revenues (in k local)","Revenues derived from charging customers for shipping",temporal/1000,NOW());

INSERT INTO kpi_p_l VALUES (month_num,1,"P&L (ACCOUNTING VIEW - NOT CASH VIEW)",266,"Wrapping net revenues (in k local)","Revenues derived from charging customers for wrapping",0,NOW());

SELECT sum(price_after_tax) - sum(cost_after_tax) into temporal from data_kpi_p_l where is_market_place="VERDADERO";
IF temporal is NULL THEN 
	SET temporal = 0;
END IF;
INSERT INTO kpi_p_l VALUES (month_num,1,"P&L (ACCOUNTING VIEW - NOT CASH VIEW)",267,"Net venture marketplace revenues (in k local)","Net merchandise revenues * commission",temporal/1000,NOW());

INSERT INTO kpi_p_l VALUES (month_num,1,"P&L (ACCOUNTING VIEW - NOT CASH VIEW)",268,"Net venture marketplace service revenues (in k local)","Service revenue from shop in shop, marketing, fulfillment, etc",0,NOW());
INSERT INTO kpi_p_l VALUES (month_num,1,"P&L (ACCOUNTING VIEW - NOT CASH VIEW)",269,"Marketing/advertising net revenues (in k local)","Revenue from sale of platform reach (if invoiced)",0,NOW());
INSERT INTO kpi_p_l VALUES (month_num,1,"P&L (ACCOUNTING VIEW - NOT CASH VIEW)",270,"Net financial revenue (in k local)","Revenue from sale of financing solutions to customers (also if in cooperation with financial institution)",0,NOW());
INSERT INTO kpi_p_l VALUES (month_num,1,"P&L (ACCOUNTING VIEW - NOT CASH VIEW)",271,"Net other revenue (in k local)","Other revenue from non-merchandise (after VAT), e.g. offering logistics services to third parties, non-redeemed prepaid vouchers",0,NOW());

SELECT sum(cost_after_tax) into temporal from data_kpi_p_l;
IF temporal is NULL THEN 
	SET temporal = 0;
END IF;
INSERT INTO kpi_p_l VALUES (month_num,1,"P&L (ACCOUNTING VIEW - NOT CASH VIEW)",273,"Basic COGS","Cost of Goods Sold (net of returns) - reductions in purchase price + cost of inbound logistics; if possible, FIFO",(temporal/1000)*-1,NOW());

INSERT INTO kpi_p_l VALUES (month_num,1,"P&L (ACCOUNTING VIEW - NOT CASH VIEW)",274,"Other COGS","All costs incurred to bring product to warehouse such as inbound logistics & customs",0,NOW());

INSERT INTO kpi_p_l VALUES (month_num,1,"P&L (ACCOUNTING VIEW - NOT CASH VIEW)",278,"Warehouse rent","Warehouse rent",(rent_wh/1000)*-1,NOW());
INSERT INTO kpi_p_l VALUES (month_num,1,"P&L (ACCOUNTING VIEW - NOT CASH VIEW)",279,"Other warehouse cost","Depreciation (as per legal requirement or 3 to 5 years useful live for equipment, 20 years for buildings), utilities etc.",(depreciation_equipment_wh/1000)*-1,NOW());
INSERT INTO kpi_p_l VALUES (month_num,1,"P&L (ACCOUNTING VIEW - NOT CASH VIEW)",280,"Warehouse handling cost","Warehouse handling cost (attributable to 3PL cost and directly employed personnel) and packaging cost",((wh_logistics_f_l_cost - (depreciation_equipment_wh + rent_wh))/1000)*-1,NOW());

SELECT sum(shipping_cost) into temporal from data_kpi_p_l;
IF temporal is NULL THEN 
	SET temporal = 0;
END IF;
INSERT INTO kpi_p_l VALUES (month_num,1,"P&L (ACCOUNTING VIEW - NOT CASH VIEW)",281,"Delivery cost","Outbound logistics cost excl. COD fees, attributable to 3PL and directly employed personnel (in case of own last mile)",(temporal/1000)*-1,NOW());

SELECT sum(shipping_cost) into temporal from data_kpi_p_l where is_return="VERDADERO";
IF temporal is NULL THEN 
	SET temporal = 0;
END IF;
INSERT INTO kpi_p_l VALUES (month_num,1,"P&L (ACCOUNTING VIEW - NOT CASH VIEW)",282,"Return cost","Logistic costs for returns, attributable to 3PL and directly employed personnel (in case of own last mile)",(temporal/1000)*-1,NOW());

INSERT INTO kpi_p_l VALUES (month_num,1,"P&L (ACCOUNTING VIEW - NOT CASH VIEW)",283,"Service/customer care cost (in k local)","Total cost (incl. idle capacity) related to customer care (3PL cost, infrastructure cost, total costs of service centers, salaries for directly employed personnel, depreciation, etc.)",(customer_care_f_l_cost/1000)*-1,NOW());

SELECT sum(payment_fees) into temporal from data_kpi_p_l;
IF temporal is NULL THEN 
	SET temporal = 0;
END IF;
INSERT INTO kpi_p_l VALUES (month_num,1,"P&L (ACCOUNTING VIEW - NOT CASH VIEW)",284,"Payment costs (in k local)","Credit card fees and other payment fees incl. COD fees; should also include charge back costs",(temporal/1000)*-1,NOW());

INSERT INTO kpi_p_l VALUES (month_num,1,"P&L (ACCOUNTING VIEW - NOT CASH VIEW)",285,"Other direct order fulfillment costs (in k local)","Costs directly related to fulfillment, e.g. hunters sourcing dedicated to cross docking and cross listing --> all costs directly attributable to the fulfillment of a customer order",0,NOW());
INSERT INTO kpi_p_l VALUES (month_num,1,"P&L (ACCOUNTING VIEW - NOT CASH VIEW)",287,"Comarketing contributions","Contributions to comarketing outside the platform (eg shared banners, shared costs) (if separately invoiced and not already implicitly part of COGS)",0,NOW());



SELECT SUM(cost) into global_mkt_cost FROM marketing_report.global_report WHERE country="Venezuela" AND yrmonth = month_num;
IF global_mkt_cost is NULL THEN 
	SET global_mkt_cost = 0;
END IF;

SELECT sum(cost) into offline_mkt_cost from marketing_report.offline_ve where EXTRACT(YEAR_MONTH FROM date) = month_num;
IF offline_mkt_cost is NULL THEN 
	SET offline_mkt_cost = 0;
END IF;

SET online_mkt_cost = global_mkt_cost - offline_mkt_cost;


INSERT INTO kpi_p_l VALUES (month_num,1,"P&L (ACCOUNTING VIEW - NOT CASH VIEW)",289,"Total online marketing cost","Total online marketing cost (e.g. SEM, FB paid etc) NOT including discounts and vouchers",((online_mkt_cost*tasa_euro)/1000)*-1,NOW());

INSERT INTO kpi_p_l VALUES (month_num,1,"P&L (ACCOUNTING VIEW - NOT CASH VIEW)",290,"Total TV marketing cost","Total TV costs (incl. Media production & buying)",0,NOW());

INSERT INTO kpi_p_l VALUES (month_num,1,"P&L (ACCOUNTING VIEW - NOT CASH VIEW)",291,"Total offline marketing cost","Total offline marketing cost (e.g. radio, print etc.; production & media buying) NOT including discounts and vouchers (print costs for discounts and vouchers to be included!)",((offline_mkt_cost*tasa_euro)/1000)*-1,NOW());

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`leonardo.fraile`@`%`*/ /*!50003 PROCEDURE `Q_A_Master`()
BEGIN

	DECLARE DESDE_FECHA DATE;

	SET DESDE_FECHA = "2013-12-31";

	/*DELETE FROM finance_ve.sku_categorias;

	INSERT INTO finance_ve.sku_categorias
		SELECT
			catalog.id_catalog_config,
			catalog.sku_config,
			catalog.cat1, 
			catalog.cat2,
			catalog.cat3,
			bp.CatBP Cat_BP,
			catalog.buyer,
			catalog.cat4
		FROM
			vista_sku_categorias AS catalog
		LEFT JOIN finance_ve.A_E_M1_New_CategoryBP AS bp ON catalog.cat1 = bp.cat1;*/

	DELETE FROM finance_ve.inventario_oms;
	
	INSERT INTO finance_ve.inventario_oms 
		SELECT 
		b.item_id,
		c.estoque_id, 
		f.unit_price as cost_oms, 
		f.price_before_tax as cost_after_tax_oms, 
		s.`name` as supplier, 
		s.id_supplier,
		CONCAT("LIN000",g.id_procurement_order,g.check_digit) as po
		FROM
						wmsprod_ve.itens_venda b
						RIGHT JOIN wmsprod_ve.estoque c 
										ON b.estoque_id = c.estoque_id 
						INNER JOIN wmsprod_ve.itens_recebimento d
										ON c.itens_recebimento_id = d.itens_recebimento_id 
						INNER JOIN procurement_live_ve.wms_received_item e 
										ON c.itens_recebimento_id=e.id_wms 
						INNER JOIN procurement_live_ve.procurement_order_item f
										ON e.fk_procurement_order_item=f.id_procurement_order_item 
						INNER JOIN procurement_live_ve.procurement_order g
										ON f.fk_procurement_order=g.id_procurement_order
						INNER JOIN bob_live_ve.supplier s
										ON g.fk_catalog_supplier = s.id_supplier
		WHERE f.is_deleted = 0;

	DELETE FROM finance_ve.A_Master;
	
	INSERT INTO finance_ve.A_Master SELECT * FROM development_ve.A_Master as amc;# WHERE amc.Date>DESDE_FECHA;
	
	#Borra el Supplier Rocket Internet
	DELETE FROM finance_ve.A_Master WHERE IdSupplier = 2744;

	UPDATE
		finance_ve.A_Master as amc JOIN
		finance_ve.modificacion_costos mc ON amc.ItemID = mc.id_sales_order_item
		SET
		amc.Cost = mc.Cost,
		amc.CostAfterTax = mc.CostAfterTax;

	UPDATE
		finance_ve.A_Master as amc JOIN
		finance_ve.inventario_oms oms ON amc.ItemID = oms.item_id
		SET
		amc.Cost = oms.cost_oms,
		amc.CostAfterTax = oms.cost_after_tax_oms,
		amc.Supplier = oms.supplier;
	
	UPDATE finance_ve.A_Master SET PaymentFees=((Fees/100)*PaidPriceAfterTax);

	UPDATE finance_ve.A_Master SET
		PaidPrice=PaidPrice+CouponValue,
		PaidPriceAfterTax=PaidPriceAfterTax+CouponValueAfterTax
		WHERE
			UPPER(PrefixCode) in ("CCE","XMAS-LINIO","XMAS-LINIO-","XMAX-LINIO","JOHN","BONO");

	UPDATE finance_ve.A_Master SET
		CouponValue=0,
		CouponValueAfterTax=0
		WHERE
			UPPER(PrefixCode) in ("CCE","XMAS-LINIO","XMAS-LINIO-","XMAX-LINIO","JOHN","BONO");

	UPDATE finance_ve.A_Master amc JOIN
		bob_live_ve.sales_order so ON amc.OrderNum = so.order_nr
	SET	
		amc.ShippingFee = so.shipping_amount / amc.ItemsInOrder, 
		amc.ShippingFeeAfterTax = (so.shipping_amount / amc.ItemsInOrder)/1.12;

	UPDATE finance_ve.A_Master SET Rev=PriceAfterTax-CouponValueAfterTax+ShippingFeeAfterTax;
	UPDATE finance_ve.A_Master SET PCOne=PriceAfterTax-CostAfterTax-CouponValueAfterTax+ShippingFeeAfterTax;
	UPDATE finance_ve.A_Master SET PCOnePFive=PCOne-ShippingCost-PaymentFees;
	
	UPDATE
		A_Master as amc JOIN 
		(SELECT
		mi.Date,
		fl.CS_Cost_per_Day/mi.items as FLCS,
		fl.WH_Cost_per_Day/mi.items as FLWH
		FROM
		development_ve.OPS_WH_CS_per_Day_for_PC2_Fully_Loaded as fl
		JOIN
		(
		SELECT
		MonthNum, Date, Count(ItemID) as items
		FROM
		A_Master
		WHERE
		OrderAfterCan=1
		GROUP BY
		MonthNum, Date
		) as mi ON fl.`Month` = mi.MonthNum) flcost ON amc.Date = flcost.Date
		set
		amc.FLCSCost = flcost.FLCS, amc.FLWHCost = flcost.FLWH
		WHERE
		amc.OrderAfterCan=1;

	UPDATE finance_ve.A_Master SET PCTwo=PCOnePFive-(FLWHCost+FLCSCost);

	UPDATE
		finance_ve.A_Master
		SET `Status` = NULL;

	UPDATE
		finance_ve.A_Master am 
		JOIN
		bob_live_ve.sales_order_item soi ON am.ItemID = soi.id_sales_order_item
		JOIN
		bob_live_ve.sales_order_item_status sois ON soi.fk_sales_order_item_status = sois.id_sales_order_item_status
		SET
		am.`Status` = sois.`name`;

	UPDATE
		finance_ve.A_Master am
		JOIN
		finance_ve.state_machine_returns smr ON am.`Status` = smr.Status AND am.PaymentMethod = smr.PaymentMethod
		SET
		am.`Returns` = smr.`Return`;

	UPDATE
		finance_ve.A_Master am JOIN
		bob_live_ve.sales_order_item soi ON am.ItemId = soi.id_sales_order_item JOIN
		bob_live_ve.catalog_shipment_type cst ON soi.fk_catalog_shipment_type = cst.id_catalog_shipment_type 
		SET
		am.ShipmentType = cst.`name`;

	/*UPDATE
		A_Master as amc JOIN 
		(
		SELECT
		soi.id_sales_order_item,
		soi.fk_supplier,
		cst.name
		FROM
		bob_live_ve.sales_order_item as soi
		JOIN
		bob_live_ve.catalog_shipment_type as cst ON soi.fk_catalog_shipment_type = cst.id_catalog_shipment_type
		WHERE
		soi.created_at >= DESDE_FECHA
		) as sois on amc.ItemID = sois.id_sales_order_item
		SET
		amc.IdSupplier = sois.fk_supplier, amc.ShipmentType = sois.name;

	UPDATE
		A_Master as amc JOIN
		bob_live_ve.supplier as su ON amc.idSupplier = su.id_supplier
		set amc.Supplier = su.`name`;

	UPDATE
		finance_ve.A_Master
		SET
		Installment= SUBSTR(SKUSimple,LENGTH(SUBSTRING_INDEX(SKUSimple,"-",1))+2);

	UPDATE
		finance_ve.A_Master as amc
		JOIN
		bob_live_ve.catalog_simple as cs ON amc.Installment = cs.id_catalog_simple
		SET
		amc.Installment=cs.fk_catalog_config;

	UPDATE
		finance_ve.A_Master as amc JOIN
		sku_categorias as cat on amc.Installment = cat.id_catalog_config
		SET amc.Cat1=cat.cat1, amc.Cat2=cat.cat2, amc.Cat3=cat.cat3, amc.CatBP=cat.Cat_BP, amc.Buyer = cat.buyer;

	UPDATE
		finance_ve.A_Master as amc SET amc.Installment = 0;

	UPDATE finance_ve.A_Master am JOIN
		A_E_M1_New_CategoryBP cat ON am.Cat1 = cat.Cat1
		SET am.CatBP = cat.catbp;

	UPDATE finance_ve.A_Master am JOIN
		A_E_M1_New_CategoryBP cat ON am.Cat2 = cat.Cat2
		SET am.CatBP = cat.catbp;*/

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`leonardo.fraile`@`%`*/ /*!50003 PROCEDURE `Q_a_master_catalog`()
BEGIN
	DELETE FROM finance_ve.A_Master_Catalog;

	INSERT INTO finance_ve.A_Master_Catalog 
		SELECT * FROM development_ve.A_Master_Catalog;

	UPDATE finance_ve.A_Master_Catalog as amc JOIN
		A_E_M1_New_CategoryBP cat ON amc.Cat1 = cat.Cat1
		SET amc.Cat_BP = cat.CatBP;

	UPDATE finance_ve.A_Master_Catalog as amc JOIN
		A_E_M1_New_CategoryBP cat ON amc.Cat2 = cat.Cat2
		SET amc.cat_bp = cat.CatBP;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`leonardo.fraile`@`%`*/ /*!50003 PROCEDURE `Q_A_Master_Local`()
BEGIN

DECLARE DESDE_FECHA DATE;

SET DESDE_FECHA = "2014-04-01";

DELETE FROM finance_ve.A_Master_Local;

INSERT INTO finance_ve.A_Master_Local
	SELECT
		EXTRACT(YEAR_MONTH FROM sord.created_at) MonthNum,
		DATE(sord.created_at) Date,
		sord.order_nr OrderNum,
		sord.id_sales_order IdSalesOrder,
		soi.id_sales_order_item ItemId,
		soi.fk_supplier IdSupplier,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		soi.sku SKUSimple,
		soi.name SKUName,
		soi.is_option_marketplace isMPlace,
		items_in_order.n_items ItemsInOrder,
		soi.unit_price Price,
		soi.unit_price/1.12 PriceAfterTax,
		sord.coupon_code CouponCode,
		NULL,
		soi.coupon_money_value CouponValue,
		soi.coupon_money_value/1.12 CouponValueAfterTax,
		soi.paid_price PaidPrice,
		soi.paid_price/1.12 PaidPriceAfterTax,
		NULL,
		sord.grand_total GrandTotal,
		soi.cost Cost,
		soi.cost/1.12 CostAfterTax,
		NULL,
		NULL,
		cst.`name` ShipmentType,
		NULL,
		(sord.shipping_amount/items_in_order.n_items) ShippingFee,
		(sord.shipping_amount/items_in_order.n_items)/1.12 ShippingFeeAfterTax,
		sord.payment_method PaymentMethod,
		NULL,
		sord.fk_customer CustomerNum,
		sord.customer_email CustomerEmail,
		sord.customer_first_name FirstName,
		sord.customer_last_name LastName,
		NULL,
		NULL,
		sois.`name` Status,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		NULL,
		sord.fk_sales_order_address_shipping
	FROM
		bob_live_ve.sales_order_item soi
	JOIN bob_live_ve.sales_order sord ON soi.fk_sales_order = sord.id_sales_order
	JOIN bob_live_ve.sales_order_item_status sois ON sois.id_sales_order_item_status = soi.fk_sales_order_item_status
	JOIN bob_live_ve.catalog_shipment_type cst ON soi.fk_catalog_shipment_type = cst.id_catalog_shipment_type
	JOIN 
	(
	SELECT 
	order_nr, count(soi.id_sales_order_item) as n_items
	FROM 
	bob_live_ve.sales_order sor
	JOIN bob_live_ve.sales_order_item soi ON soi.fk_sales_order = sor.id_sales_order
	GROUP BY sor.order_nr 
	) items_in_order ON items_in_order.order_nr = sord.order_nr
	WHERE sord.created_at >= DESDE_FECHA;

	UPDATE
		finance_ve.A_Master_Local
		SET id_catalog_simple= SUBSTR(SKUSimple,LENGTH(SUBSTRING_INDEX(SKUSimple,"-",1))+2),
		SKUConfig = SUBSTRING_INDEX(SKUSimple,"-",1);
	
	UPDATE
		finance_ve.A_Master_Local aml
		JOIN bob_live_ve.catalog_simple cs ON aml.id_catalog_simple = cs.id_catalog_simple
		SET aml.id_catalog_config = cs.fk_catalog_config;
	
	UPDATE
		finance_ve.A_Master_Local aml 
		JOIN bob_live_ve.supplier su ON aml.IdSupplier = su.id_supplier
		SET aml.Supplier = su.`name`;
	
	UPDATE
		finance_ve.A_Master_Local aml
		JOIN bob_live_ve.catalog_config cc ON aml.id_catalog_config = cc.id_catalog_config
		JOIN bob_live_ve.catalog_brand cb ON cc.fk_catalog_brand = cb.id_catalog_brand
		SET aml.Brand = cb.`name`;

	UPDATE
		finance_ve.A_Master_Local aml
		JOIN bob_live_ve.sales_rule sru ON aml.CouponCode = sru.`code`
    JOIN bob_live_ve.sales_rule_set sruse ON sru.fk_sales_rule_set = sruse.id_sales_rule_set
		SET aml.PrefixCode = sruse.code_prefix;

	UPDATE
		finance_ve.A_Master_Local as aml
		JOIN development_ve.M_Bob_Order_Status_Definition bobsta 
		ON aml.PaymentMethod = bobsta.Payment_Method AND aml.`Status` = bobsta.`Status`
		SET 
		aml.OrderBeforeCan = bobsta.OrderBeforeCan, 
		aml.OrderAfterCan = bobsta.OrderAfterCan,
		aml.Cancelled = bobsta.Cancellations,
		aml.Pending = bobsta.Pending;

	UPDATE
		finance_ve.A_Master_Local as aml
		JOIN finance_ve.state_machine_returns smr 
		ON aml.PaymentMethod = smr.PaymentMethod AND aml.`Status` = smr.`Status`
		SET aml.`Returns` = smr.Return;

	DELETE FROM finance_ve.inventario_oms;
	
	INSERT INTO finance_ve.inventario_oms 
		SELECT 
		b.item_id,
		c.estoque_id, 
		f.unit_price as cost_oms, 
		f.price_before_tax as cost_after_tax_oms, 
		s.`name` as supplier, 
		s.id_supplier,
		CONCAT("LIN000",g.id_procurement_order,g.check_digit) as po
		FROM
						wmsprod_ve.itens_venda b
						RIGHT JOIN wmsprod_ve.estoque c 
										ON b.estoque_id = c.estoque_id 
						INNER JOIN wmsprod_ve.itens_recebimento d
										ON c.itens_recebimento_id = d.itens_recebimento_id 
						INNER JOIN procurement_live_ve.wms_received_item e 
										ON c.itens_recebimento_id=e.id_wms 
						INNER JOIN procurement_live_ve.procurement_order_item f
										ON e.fk_procurement_order_item=f.id_procurement_order_item 
						INNER JOIN procurement_live_ve.procurement_order g
										ON f.fk_procurement_order=g.id_procurement_order
						INNER JOIN bob_live_ve.supplier s
										ON g.fk_catalog_supplier = s.id_supplier
		WHERE f.is_deleted = 0;

	#Borra el Supplier Rocket Internet
	DELETE FROM finance_ve.A_Master_Local WHERE IdSupplier = 2744;

	#Borra Test
	DELETE FROM finance_ve.A_Master_Local WHERE UPPER(FirstName) like "TEST%" or UPPER(LastName) like "TEST%";

	UPDATE
		finance_ve.A_Master_Local aml 
		JOIN finance_ve.modificacion_costos mc ON aml.ItemID = mc.id_sales_order_item
		SET	aml.Cost = mc.Cost,
		aml.CostAfterTax = mc.CostAfterTax;

	UPDATE
		finance_ve.A_Master_Local aml 
		JOIN finance_ve.inventario_oms oms ON aml.ItemID = oms.item_id
		SET	aml.Cost = oms.cost_oms,
		aml.CostAfterTax = oms.cost_after_tax_oms;

	UPDATE
		finance_ve.A_Master_Local aml 
		JOIN development_ve.M_PaymentMethod_Fee pmf
		ON aml.PaymentMethod = pmf.PaymentMethod AND aml.MonthNum = pmf.MonthNum 
		SET aml.Fees = pmf.Fee;

	UPDATE
		finance_ve.A_Master_Local aml
		SET aml.Fees = 5 WHERE PaymentMethod = "CashOnDelivery_Payment";

	UPDATE finance_ve.A_Master_Local SET PaymentFees=((Fees/100)*PaidPriceAfterTax);

	UPDATE finance_ve.A_Master_Local SET
		PaidPrice=PaidPrice+CouponValue,
		PaidPriceAfterTax=PaidPriceAfterTax+CouponValueAfterTax
		WHERE
			UPPER(PrefixCode) in ("UNI","CCE","PRCCRE","OPSCRE","SC","XMAS-LINIO","XMAS-LINIO-","XMAX-LINIO","ZOOM","IPSOS","JOHN","BONO");

	UPDATE finance_ve.A_Master_Local SET
		CouponValue=0,
		CouponValueAfterTax=0
		WHERE
			UPPER(PrefixCode) in ("UNI","CCE","PRCCRE","OPSCRE","SC","XMAS-LINIO","XMAS-LINIO-","XMAX-LINIO","ZOOM","IPSOS","JOHN","BONO");

	/*DELETE FROM finance_ve.sku_categorias;

	INSERT INTO finance_ve.sku_categorias
		SELECT
			catalog.id_catalog_config,
			catalog.sku_config,
			catalog.cat1, 
			catalog.cat2,
			catalog.cat3,
			bp.CatBP Cat_BP,
			catalog.buyer,
			catalog.cat4
		FROM
			vista_sku_categorias AS catalog
		LEFT JOIN finance_ve.A_E_M1_New_CategoryBP AS bp ON catalog.cat1 = bp.cat1;

	UPDATE finance_ve.A_Master_Local aml
		JOIN finance_ve.sku_categorias skucat ON aml.id_catalog_config = skucat.id_catalog_config
		SET aml.Cat1 = skucat.cat1,
		aml.Cat2 = skucat.cat2,
		aml.Cat3 = skucat.cat3,
		aml.CatBP = skucat.cat_bp,
		aml.Buyer = skucat.buyer;

	UPDATE finance_ve.A_Master_Local aml
		JOIN development_ve.M_Monthly_ShippingCost msc ON aml.CatBP = msc.CatBP AND aml.MonthNum = msc.MonthNum
		SET aml.ShippingCost = msc.ShippingCostAVG;
		
	UPDATE finance_ve.A_Master_Local aml
		SET aml.CatBP = SUBSTR(aml.CatBP,5);
*/
	UPDATE finance_ve.A_Master_Local SET Rev=PriceAfterTax-CouponValueAfterTax+ShippingFeeAfterTax;
	UPDATE finance_ve.A_Master_Local SET PCOne=PriceAfterTax-CostAfterTax-CouponValueAfterTax+ShippingFeeAfterTax;
	UPDATE finance_ve.A_Master_Local SET PCOnePFive=PCOne-ShippingCost-PaymentFees;
	
	UPDATE
		finance_ve.A_Master_Local as amc JOIN 
		(SELECT
		mi.Date,
		fl.CS_Cost_per_Day/mi.items as FLCS,
		fl.WH_Cost_per_Day/mi.items as FLWH
		FROM
		development_ve.OPS_WH_CS_per_Day_for_PC2_Fully_Loaded as fl
		JOIN
		(
		SELECT
		aml.MonthNum, aml.Date, Count(aml.ItemID) items
		FROM
		finance_ve.A_Master_Local aml
		WHERE
		OrderAfterCan=1
		GROUP BY
		aml.MonthNum, aml.Date
		) as mi ON fl.`Month` = mi.MonthNum) flcost ON amc.Date = flcost.Date
		set
		amc.FLCSCost = flcost.FLCS, amc.FLWHCost = flcost.FLWH
		WHERE
		amc.OrderAfterCan=1;

	UPDATE finance_ve.A_Master_Local SET PCTwo=PCOnePFive-(FLWHCost+FLCSCost);

	UPDATE
		finance_ve.A_Master_Local aml 
		JOIN bob_live_ve.sales_order_address soa ON aml.id_sales_order_address_shipping = soa.id_sales_order_address
		SET aml.State = soa.region, aml.City = soa.city;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`leonardo.fraile`@`%`*/ /*!50003 PROCEDURE `Q_out_order_tracking`()
BEGIN
	DELETE FROM finance_ve.out_order_tracking;

	INSERT INTO finance_ve.out_order_tracking 
		SELECT * FROM operations_ve.out_order_tracking;

	UPDATE finance_ve.out_order_tracking as oot JOIN
		A_E_M1_New_CategoryBP cat ON oot.cat_1 = cat.cat1 
		SET oot.category_bp = cat.CatBP;
	
	UPDATE finance_ve.out_order_tracking as oot JOIN
		A_E_M1_New_CategoryBP cat ON oot.cat_2 = cat.cat2
		SET oot.category_bp = cat.CatBP;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`leonardo.fraile`@`%`*/ /*!50003 PROCEDURE `Q_out_stock_hist`()
BEGIN
	DELETE FROM finance_ve.inventario_oms;
	
	INSERT INTO finance_ve.inventario_oms 
		SELECT 
		b.item_id,
		c.estoque_id, 
		f.unit_price as cost_oms, 
		f.price_before_tax as cost_after_tax_oms, 
		s.`name` as supplier, 
		s.id_supplier,
		CONCAT("LIN000",g.id_procurement_order,g.check_digit) as po
		FROM
						wmsprod_ve.itens_venda b
						RIGHT JOIN wmsprod_ve.estoque c 
										ON b.estoque_id = c.estoque_id 
						INNER JOIN wmsprod_ve.itens_recebimento d
										ON c.itens_recebimento_id = d.itens_recebimento_id 
						INNER JOIN procurement_live_ve.wms_received_item e 
										ON c.itens_recebimento_id=e.id_wms 
						INNER JOIN procurement_live_ve.procurement_order_item f
										ON e.fk_procurement_order_item=f.id_procurement_order_item 
						INNER JOIN procurement_live_ve.procurement_order g
										ON f.fk_procurement_order=g.id_procurement_order
						INNER JOIN bob_live_ve.supplier s
										ON g.fk_catalog_supplier = s.id_supplier
		WHERE f.is_deleted = 0;

	DELETE FROM finance_ve.out_stock_hist;
	
	INSERT INTO finance_ve.out_stock_hist 
		SELECT * FROM operations_ve.out_stock_hist as osh;

	UPDATE
		finance_ve.out_stock_hist as osh JOIN
		wmsprod_ve.estoque as e ON osh.stock_item_id = e.estoque_id
		SET osh.wh_location = e.endereco, osh.sub_location = e.sub_endereco;
	
	UPDATE
		finance_ve.out_stock_hist as osh JOIN
		bob_live_ve.catalog_simple as cs
		on osh.sku_simple = cs.sku
		SET osh.price = cs.price, osh.cost = cs.cost, osh.cost_w_o_vat = cs.cost / 1.12;
/*
	UPDATE
		finance_ve.out_stock_hist as osh JOIN
		bob_live_ve.catalog_config as cc
		on osh.sku_config = cc.id_catalog_config
		SET osh.sku_config = cc.sku, osh.sku_name = cc.`name`;

	UPDATE
		finance_ve.out_stock_hist as osh JOIN
		finance_ve.sku_categorias as skucat ON osh.sku_config = skucat.sku_config
		SET osh.category_com_main = skucat.cat1, osh.category_com_sub = skucat.cat2, osh.category_com_sub_sub = skucat.cat3, osh.category_bp = skucat.cat_bp, osh.buyer = skucat.buyer;

	UPDATE
		finance_ve.out_stock_hist as osh 
		JOIN
		bob_live_ve.catalog_source as cso 
			ON osh.hash_wms = cso.fk_catalog_simple
		JOIN
		bob_live_ve.supplier as su
			ON cso.fk_supplier = su.id_supplier
		JOIN
		bob_live_ve.catalog_shipment_type as cst 
			ON cso.fk_catalog_shipment_type = cst.id_catalog_shipment_type
		SET osh.supplier_id = cso.fk_supplier, osh.supplier_name = su.`name`, osh.fullfilment_type_bob = cst.`name`, osh.barcode_bob = cso.barcode_ean;
*/

	UPDATE
		finance_ve.out_stock_hist as osh JOIN
		finance_ve.inventario_oms as oms on osh.stock_item_id = oms.estoque_id
		SET 
		osh.cost = oms.cost_oms, 
		osh.cost_w_o_vat=oms.cost_after_tax_oms, 
		osh.supplier_id = oms.id_supplier, 
		osh.supplier_name = oms.supplier,
		osh.purchase_order = oms.po;

	UPDATE
		finance_ve.out_stock_hist SET position_type=NULL, in_stock=NULL;

	UPDATE
		finance_ve.out_stock_hist as osh JOIN
		wmsprod_ve.posicoes as pos ON osh.wh_location = pos.posicao
		SET
		osh.position_type = pos.tipo_posicao, osh.in_stock = pos.participa_estoque;

	UPDATE finance_ve.out_stock_hist as osh JOIN
		A_E_M1_New_CategoryBP cat ON osh.category_1 = cat.cat1 
		SET osh.category_bp = cat.CatBP;
	
	UPDATE finance_ve.out_stock_hist as osh JOIN
		A_E_M1_New_CategoryBP cat ON osh.category_2 = cat.Cat2
		SET osh.category_bp = cat.catbp;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`leonardo.fraile`@`%`*/ /*!50003 PROCEDURE `Reporte_Contenido`()
BEGIN
DECLARE fecha_reporte DATE;

#SET fecha_reporte = "2014-05-25";
SET fecha_reporte = subdate(CURDATE(), 1);

INSERT INTO finance_ve.Reporte_Contenido 
SELECT
	fecha_reporte,
	IFNULL(SUBSTR(amc.Cat_BP,5),"(en blanco)") AS Cat_BP,
	count(DISTINCT(amc.sku_config)) as Cantidad,
	"V" as Tipo
FROM
	development_ve.A_Master_Catalog AS amc
WHERE
	amc.isVisible = 1
GROUP BY
	amc.Cat_BP;

INSERT INTO finance_ve.Reporte_Contenido
SELECT
	fecha_reporte,
	IFNULL(SUBSTR(osh.category_bp,5),"(en blanco)") AS Cat_BP,
	count(DISTINCT(osh.sku_config)) as Cantidad, 
	"NV" as Tipo
FROM
	operations_ve.out_stock_hist AS osh
WHERE
	osh.visible = 0
AND osh.in_stock = 1
AND osh.reserved = 0
GROUP BY
	osh.category_bp;

INSERT INTO finance_ve.Reporte_Contenido
SELECT
	fecha_reporte,
	IFNULL(SUBSTR(osh.category_bp,5),"(en blanco)") AS Cat_BP,
	count(DISTINCT(osh.sku_config)) AS Cantidad,
	"NVFC" AS Tipo
FROM
	operations_ve.out_stock_hist AS osh
JOIN (
	SELECT
		bcc.sku
	FROM
		bob_live_ve.catalog_config AS bcc
	WHERE
		bcc.`status` <> "deleted"
	AND bcc.pet_status <> "creation,edited,images"
) AS skufc ON osh.sku_config = skufc.sku
WHERE
	osh.visible = 0
AND osh.in_stock = 1
AND osh.reserved = 0
GROUP BY
	osh.category_bp;

INSERT INTO finance_ve.Reporte_Contenido
SELECT
	fecha_reporte,
	IFNULL(SUBSTR(amc.Cat_BP,5),"(en blanco)") AS Cat_BP,
	count(DISTINCT(amc.sku_config)) AS Cantidad,
"FC" as Tipo
FROM
	development_ve.A_Master_Catalog AS amc
JOIN (
	SELECT
		bcc.sku
	FROM
		bob_live_ve.catalog_config AS bcc
	WHERE
		bcc.`status` <> "deleted"
	AND bcc.pet_status <> "creation,edited,images"
) as skufc on amc.sku_config = skufc.sku
GROUP BY
	amc.Cat_BP;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:04:00
