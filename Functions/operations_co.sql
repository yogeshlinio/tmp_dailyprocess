-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: operations_co
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'operations_co'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 FUNCTION `calcworkdays`(sdate varchar(50), edate varchar(50)) RETURNS int(11)
begin
 declare wdays, tdiff, counter, thisday smallint;
 declare newdate date;
 set newdate := sdate;
 set wdays = 0;
 if sdate is null then return 0; end if;
 if edate is null then return 0; end if;
 if edate like '1%' then return 0; end if;
 if edate like '0%' then return 0; end if;
 if sdate like '1%' then return 0; end if;
 if sdate like '0%' then return 0; end if;
    if datediff(edate, sdate) <= 0 then return 0; end if;
         label1: loop
          set thisday = dayofweek(newdate);
            if thisday in(1,2,3,4,5) then set wdays := wdays + 1; end if;
             set newdate = date_add(newdate, interval 1 day);
            if datediff(edate, newdate) <= 0 then leave label1; end if;
       end loop label1;
      return wdays;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`natali.serrano`@`%`*/ /*!50003 FUNCTION `dias_habiles`(fecha_inicio DATE, fecha_final DATE) RETURNS int(11)
BEGIN	
	DECLARE dias_habiles_fixed INT;
	SET dias_habiles_fixed = (SELECT COUNT(*) FROM calendar WHERE (dt BETWEEN fecha_inicio AND fecha_final) AND isweekday=1 AND isholiday=0);
	IF (weekday(fecha_inicio)<>6 AND weekday(fecha_inicio)<>5)
		THEN SET dias_habiles_fixed = (dias_habiles_fixed-1);
	END IF;
	IF (fecha_inicio>=fecha_final)
		THEN SET dias_habiles_fixed=0;
	END IF;
RETURN dias_habiles_fixed;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 FUNCTION `dias_habiles_negativos`(fecha_min_delivery DATE, fecha_stc DATE) RETURNS int(11)
BEGIN	
	DECLARE dias_habiles_fixed INT;
	DECLARE fecha_inicio DATE;
	DECLARE fecha_final DATE;

	IF (fecha_min_delivery < fecha_stc) THEN SET fecha_final = fecha_stc;
	SET fecha_inicio = fecha_min_delivery;
	END IF;

	IF (fecha_min_delivery > fecha_stc) THEN SET fecha_inicio = fecha_stc;
	SET fecha_final = fecha_min_delivery;
	END IF;

	SET dias_habiles_fixed = (SELECT COUNT(*) FROM calendar WHERE (dt BETWEEN fecha_inicio AND fecha_final) AND isweekday=1 AND isholiday=0);
	IF (weekday(fecha_inicio)<>6 AND weekday(fecha_inicio)<>5)
		THEN SET dias_habiles_fixed = (dias_habiles_fixed-1);
	END IF;

	IF (fecha_min_delivery = fecha_final)
	THEN SET dias_habiles_fixed = dias_habiles_fixed *-1;
	END IF;
	
RETURN dias_habiles_fixed;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 FUNCTION `dia_habil_anterior`(fecha date) RETURNS date
BEGIN

set @dias := -1;

set @dia_habil_anterior:= workday_bi(fecha,@dias);

while @dia_habil_anterior = fecha do

set @dias = @dias -1 ;
set @dia_habil_anterior = workday_bi(fecha,@dias);

end while;



RETURN @dia_habil_anterior;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 FUNCTION `maximum`(procure integer, ready integer, to_ship integer,  attempt integer ) RETURNS int(11)
begin
declare compare integer; 
		set compare = procure;
		if compare < ready then set compare = ready; end if;
		if compare < to_ship then set compare = to_ship; end if;
		if compare < attempt then set compare = attempt; end if;
 return compare;
 end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 FUNCTION `week_iso`(edate date) RETURNS varchar(20) CHARSET latin1
begin
 declare sdate varchar(20);
 if year(edate) = 2012 then set sdate = concat(year(edate), '-',(week(edate, 5))+1); else set sdate = date_format(edate, "%x-%v"); end if;
      return sdate;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 FUNCTION `workday`(edate date, workdays integer) RETURNS date
begin
 declare wdays, thisday, thisday0, timer smallint;
 declare newdate, middate date;
 set wdays = 0;
 set newdate :=  edate;
 if edate is null then set newdate = '2012-05-01'; end if;
 if workdays is null then return '2012-05-01'; end if;
 set thisday0 = dayofweek(edate);
 if workdays in (0) and thisday0 between 2 and 6 then return newdate; end if;
 if (workdays in (0) and thisday0 in (7)) then return date(adddate(newdate, 2)); end if;
 if (workdays in (0) and thisday0 in (1)) then return date(adddate(newdate, 1)); end if;
	label2: loop
		set newdate = date(adddate(newdate, 1));
        set thisday = dayofweek(newdate);
		if thisday between 2 and 6 then 
			set wdays = wdays + 1;
			if wdays >= workdays then 
				leave label2; 
			end if;
		else 
			set wdays = wdays;
		end if;
	end loop label2;
return newdate;
 end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`natali.serrano`@`%`*/ /*!50003 PROCEDURE `bi_ops_tracking_code`()
BEGIN
SELECT  'Insert item_id y coperations_co.odigo de rastreamento',now();
TRUNCATE TABLE operations_co.tbl_bi_ops_guiasbyitem2;

SELECT  'Insert item_id itens_venda',now();
INSERT INTO operations_co.tbl_bi_ops_guiasbyitem2 (item_id,cod_rastreamento)
SELECT out_order_tracking_sample.item_id,out_order_tracking_sample.wms_tracking_code
FROM operations_co.out_order_tracking_sample;

SELECT  'Actualizar tbl_bi_ops_tms_tracks2 segun tabla de wms tms_tracks',now();
TRUNCATE TABLE operations_co.tbl_bi_ops_tms_tracks2;

INSERT INTO operations_co.tbl_bi_ops_tms_tracks2 SELECT * FROM  wmsprod_co.tms_tracks;

SELECT  'GUIA 1',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2 b INNER JOIN operations_co.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia1=a.track;

DELETE FROM operations_co.tbl_bi_ops_tms_tracks2
WHERE track IN (SELECT guia1 FROM operations_co.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 2',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2 b
INNER JOIN operations_co.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia2=a.track;

DELETE FROM operations_co.tbl_bi_ops_tms_tracks2
WHERE track IN (SELECT guia2 FROM tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 3',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2 b
INNER JOIN operations_co.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia3=a.track;

DELETE FROM operations_co.tbl_bi_ops_tms_tracks2
WHERE track IN (SELECT guia3 FROM operations_co.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 4',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2 b
INNER JOIN operations_co.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia4=a.track;	

DELETE FROM operations_co.tbl_bi_ops_tms_tracks2
WHERE track IN (SELECT guia4 FROM operations_co.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 5',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2 b
INNER JOIN operations_co.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia5=a.track;	

DELETE FROM operations_co.tbl_bi_ops_tms_tracks2
WHERE track IN (SELECT guia5 FROM operations_co.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 6',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2 b
inner join operations_co.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia6=a.track;	

DELETE from operations_co.tbl_bi_ops_tms_tracks2
where track in (select guia6 from operations_co.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 7',now();
UPDATE tbl_bi_ops_guiasbyitem2 b
inner join operations_co.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia7=a.track;

DELETE from operations_co.tbl_bi_ops_tms_tracks2
where track in (select guia7 from operations_co.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 8',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2 b
inner join operations_co.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia8=a.track;

DELETE from operations_co.tbl_bi_ops_tms_tracks2
where track in (select guia8 from operations_co.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 9',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2 b
inner join operations_co.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia9=a.track;

DELETE from operations_co.tbl_bi_ops_tms_tracks2
where track in (select guia9 from operations_co.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 10',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2 b
inner join operations_co.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia10=a.track;

DELETE from operations_co.tbl_bi_ops_tms_tracks2 where track in (select guia10 from operations_co.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 11',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2 b inner join operations_co.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia11=a.track;

DELETE from operations_co.tbl_bi_ops_tms_tracks2 where track in (select guia11 from operations_co.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 12',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2 b
inner join operations_co.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia12=a.track;

DELETE from operations_co.tbl_bi_ops_tms_tracks2 where track in (select guia12 from operations_co.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 13',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2 b
inner join operations_co.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia13=a.track;

DELETE from operations_co.tbl_bi_ops_tms_tracks2 where track in (select guia13 from operations_co.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 14',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2 b inner join operations_co.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia14=a.track;

DELETE from operations_co.tbl_bi_ops_tms_tracks2 where track in (select guia14 from operations_co.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 15',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2 b inner join operations_co.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia15=a.track;

DELETE from operations_co.tbl_bi_ops_tms_tracks2 where track in (select guia15 from operations_co.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 16',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2 b inner join operations_co.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia16=a.track;

DELETE from operations_co.tbl_bi_ops_tms_tracks2 where track in (select guia16 from operations_co.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 17',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2 b inner join operations_co.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia17=a.track;

DELETE from operations_co.tbl_bi_ops_tms_tracks2 where track in (select guia17 from operations_co.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 18',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2 b
inner join operations_co.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia18=a.track;

DELETE from operations_co.tbl_bi_ops_tms_tracks2 where track in (select guia18 from operations_co.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 19',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2 b
inner join operations_co.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia19=a.track;

DELETE from operations_co.tbl_bi_ops_tms_tracks2 where track in (select guia19 from operations_co.tbl_bi_ops_guiasbyitem2);

SELECT  'GUIA 20',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2 b inner join operations_co.vw_tracks_wms  a
ON b.cod_rastreamento=a.cod_rastreamento
SET b.guia20=a.track;

DELETE from operations_co.tbl_bi_ops_tms_tracks2 where track in (select guia20 from operations_co.tbl_bi_ops_guiasbyitem2);

SELECT  'concatenar',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET total_guias = NULL;

SELECT  'concatenar1',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET total_guias = guia1
WHERE guia1 is not null
AND guia2 is null;

SELECT  'concatenar2',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2)
WHERE guia2 is not null
AND guia3 is null;

SELECT  'concatenar3',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3)
WHERE guia3 is not null
AND guia4 is null;

SELECT  'concatenar4',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4)
WHERE guia4 is not null
AND guia5 is null;

SELECT  'concatenar5',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5)
WHERE guia5 is not null
AND guia6 is null;

SELECT  'concatenar6',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6)
WHERE guia6 is not null
AND guia7 is null;

SELECT  'concatenar7',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7)
WHERE guia7 is not null
AND guia8 is null;

SELECT  'concatenar8',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8)
WHERE guia8 is not null
AND guia9 is null;

SELECT  'concatenar9',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8,'-',guia9)
WHERE guia9 is not null
AND guia10 is null;

SELECT  'concatenar10',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8,'-',guia9,'-',guia10)
WHERE guia10 is not null
AND guia11 is null;

SELECT  'concatenar11',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8,'-',guia9,'-',guia10,'-',guia11)
WHERE guia11 is not null
AND guia12 is null;

SELECT  'concatenar12',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8,'-',guia9,'-',guia10,'-',guia11,'-',guia12)
WHERE guia12 is not null
AND guia13 is null;

SELECT  'concatenar13',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8,'-',guia9,'-',guia10,'-',guia11
,'-',guia12,'-',guia13)
WHERE guia13 is not null
AND guia14 is null;

SELECT  'concatenar14',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8,'-',guia9,'-',guia10,'-',guia11
,'-',guia12,'-',guia13,'-',guia14)
WHERE guia14 is not null
AND guia15 is null;

SELECT  'concatenar15',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8,'-',guia9,'-',guia10,'-',guia11
,'-',guia12,'-',guia13,'-',guia14,'-',guia15)
WHERE guia15 is not null
AND guia16 is null;

SELECT  'concatenar16',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8,'-',guia9,'-',guia10,'-',guia11
,'-',guia12,'-',guia13,'-',guia14,'-',guia15,'-',guia16)
WHERE guia16 is not null
AND guia17 is null;

SELECT  'concatenar17',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8,'-',guia9,'-',guia10,'-',guia11
,'-',guia12,'-',guia13,'-',guia14,'-',guia15,'-',guia16,'-',guia17)
WHERE guia17 is not null
AND guia18 is null;

SELECT  'concatenar18',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8,'-',guia9,'-',guia10,'-',guia11
,'-',guia12,'-',guia13,'-',guia14,'-',guia15,'-',guia16,'-',guia17,'-',guia18)
WHERE guia18 is not null
AND guia19 is null;

SELECT  'concatenar19',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8,'-',guia9,'-',guia10,'-',guia11
,'-',guia12,'-',guia13,'-',guia14,'-',guia15,'-',guia16,'-',guia17,'-',guia18,'-',guia19)
WHERE guia19 is not null
AND guia20 is null;

SELECT  'concatenar20',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET total_guias = CONCAT(guia1,'-',guia2,'-',guia3,'-',guia4,'-',guia5,'-',guia6,'-',guia7,'-',guia8,'-',guia9,'-',guia10,'-',guia11
,'-',guia12,'-',guia13,'-',guia14,'-',guia15,'-',guia16,'-',guia17,'-',guia18,'-',guia19,'-',guia20)
WHERE guia20 is not null;

SELECT  'buscar y reemplazar --',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET total_guias = REPLACE(total_guias,'--','-');

SELECT  'guia1 -',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET
guia1 = if(instr(guia1,'-')=0,guia1,LEFT(total_guias,instr(total_guias,'-')-1)),
guia2 = if(instr(guia1,'-')=0,guia2,left(right(total_guias,length(total_guias)-instr(total_guias,'-')),instr(right(total_guias,length(total_guias)-instr(total_guias,'-')),'-')-1)),
guia3 = if(instr(guia1,'-')=0,guia3,left(right(total_guias,length(total_guias)-instr(total_guias,'-')-instr(total_guias,'-')),instr(right(total_guias,length(total_guias)-instr(total_guias,'-')),'-')-1));

SELECT  'buscar y reemplazar doble guion por un guion--',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET total_guias = REPLACE(total_guias,'--','-');

SELECT  'buscar y reemplazar espacios por vacios--',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
SET total_guias = REPLACE(total_guias,' ','');

SELECT  'validacion igual a total guias',now();
TRUNCATE TABLE operations_co.validacion_track;

SELECT  'validacion igual a total guias',now();
INSERT INTO operations_co.validacion_track SELECT item_id,total_guias FROM operations_co.tbl_bi_ops_guiasbyitem2;

SELECT  'actualizar guia1',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia1 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia1',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia2',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia2 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia2',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia3',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia3 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia3',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia4',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia4 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia4',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia5',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia5 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia5',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia6',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia6 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia6',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia7',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia7 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia7',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia8',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia8 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia8',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia9',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia9 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia9',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia10',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia10 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia10',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia11',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia11 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia11',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia12',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia12 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia12',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia13',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia13 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia13',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia14',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia14 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia14',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia15',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia15 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia15',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia16',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia16 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia16',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia17',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia17 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia17',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia18',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia18 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia18',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia19',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia19 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia19',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'actualizar guia20',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
SET guia20 = if(instr(validacion_track.total_guias,'-')=0,validacion_track.total_guias,(LEFT(validacion_track.total_guias,instr(validacion_track.total_guias,'-')-1)))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'borrar guia20',now();
UPDATE operations_co.tbl_bi_ops_guiasbyitem2
inner join operations_co.validacion_track
on tbl_bi_ops_guiasbyitem2.item_id=validacion_track.item_id
set validacion_track.total_guias=if(instr(validacion_track.total_guias,'-')=0,null,right(validacion_track.total_guias,length(validacion_track.total_guias)-instr(validacion_track.total_guias,'-')))
where validacion_track.total_guias is not null and validacion_track.total_guias <> '';

SELECT  'Rutina finalizada bi_ops_tracking_code',now();

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`ops_co`@`%`*/ /*!50003 PROCEDURE `calcworkdays`()
begin

DECLARE bDone, x INT;
DECLARE pasado INT;
DECLARE futuro INT;

DECLARE vsDate date;


DECLARE curs_dt	date;
DECLARE curs_isweekday	bit(1);
DECLARE curs_isholiday	bit(1);
DECLARE curs_isweekend	bit(1);
DECLARE curs_yr	smallint(6);
DECLARE curs_qtr	tinyint(4);
DECLARE curs_mt	tinyint(4);
DECLARE curs_dy	tinyint(4);
DECLARE curs_dyw	tinyint(4);
DECLARE curs_wk	tinyint(4);

#SELECT 180,60  INTO pasado, futuro ;
DECLARE curs CURSOR FOR SELECT * FROM calendar WHERE dt BETWEEN dt - INTERVAL 180 DAY AND dt ORDER BY dt ASC;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET bDone = 1;

#drop table if exists calcworkdays;
create table if not exists  calcworkdays(
date_first date,
date_last date,
workdays int,

isweekday int,
isholiday int,
isweekend int,

isweekday_first int,
isholiday_first int,
isweekend_first int,

primary key ( date_first ,date_last ),
index date (date_first, date_last),
index date_first  ( date_first  ),
index date_last   ( date_last  ),
index date_last_wd  ( date_last  , workdays),
index date_first_wd ( date_first , workdays)
);


SET pasado=180;
SET futuro = 180;

OPEN curs;
SET bDone = 0;
REPEAT
FETCH curs INTO curs_dt,
                       curs_isweekday,
                       curs_isholiday,
                       curs_isweekend,
                       curs_yr,
                       curs_qtr,
                       curs_mt,
                       curs_dy,
                       curs_dyw,
                       curs_wk	
                       ;

#    IF whatever_filtering_desired
       -- here for whatever_transformation_may_be_desired
   SET x =0;
   SET vsDate = curs_dt;
   WHILE x<=pasado DO
 
    REPLACE INTO calcworkdays ( date_last , date_first , workdays )
    SELECT vsDate , curs_dt , sum( IF( dt != curs_dt , isweekday - if( isweekday = 1 , isholiday , 0 ), 0  ) ) 
    FROM calendar 
    where dt between curs_dt and vsDate;
		set x =x+1; 
		set vsDate = curs_dt - interval x day;
	end while;

   SET x =0;
   SET vsDate = curs_dt;
   WHILE x<=futuro DO
 
    REPLACE INTO calcworkdays ( date_last , date_first , workdays )
    SELECT vsDate , curs_dt , sum( IF( dt != curs_dt , isweekday - if( isweekday = 1 , isholiday , 0 ), 0  ) ) 
    FROM calendar 
    where dt between curs_dt and vsDate;
		set x =x+1; 
		set vsDate = curs_dt + interval x day;
	end while;


UNTIL bDone END REPEAT;
CLOSE curs;

UPDATE        calcworkdays 
   INNER JOIN calendar 
        ON calcworkdays.date_last = calendar.dt
SET
   calcworkdays.isweekday = calendar.isweekday,
   calcworkdays.isholiday = calendar.isholiday,
   calcworkdays.isweekend = calendar.isweekend
;

UPDATE        calcworkdays 
   INNER JOIN calendar 
        ON calcworkdays.date_first = calendar.dt
SET
   calcworkdays.isweekday_first = calendar.isweekday,
   calcworkdays.isholiday_first = calendar.isholiday,
   calcworkdays.isweekend_first = calendar.isweekend
;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`ops_co`@`%`*/ /*!50003 PROCEDURE `calcworkdays_inventory`()
begin

DECLARE bDone, x INT;
DECLARE pasado INT;
DECLARE futuro INT;

DECLARE vsDate date;


DECLARE curs_dt	date;
DECLARE curs_isweekday	bit(1);
DECLARE curs_isholiday	bit(1);
DECLARE curs_isweekend	bit(1);
DECLARE curs_yr	smallint(6);
DECLARE curs_qtr	tinyint(4);
DECLARE curs_mt	tinyint(4);
DECLARE curs_dy	tinyint(4);
DECLARE curs_dyw	tinyint(4);
DECLARE curs_wk	tinyint(4);

#SELECT 180,60  INTO pasado, futuro ;
DECLARE curs CURSOR FOR SELECT * FROM calendar WHERE dt BETWEEN dt - INTERVAL 180 DAY AND dt ORDER BY dt ASC;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET bDone = 1;

#drop table if exists calcworkdays;
create table if not exists  calcworkdays_inventory(
date_first date,
date_last date,
workdays int,

isweekday int,
isholiday int,
isweekend int,

isweekday_first int,
isholiday_first int,
isweekend_first int,

primary key ( date_first ,date_last ),
index date (date_first, date_last),
index date_first  ( date_first  ),
index date_last   ( date_last  ),
index date_last_wd  ( date_last  , workdays),
index date_first_wd ( date_first , workdays)
);


SET pasado=180;
SET futuro = 180;

OPEN curs;
SET bDone = 0;
REPEAT
FETCH curs INTO curs_dt,
                       curs_isweekday,
                       curs_isholiday,
                       curs_isweekend,
                       curs_yr,
                       curs_qtr,
                       curs_mt,
                       curs_dy,
                       curs_dyw,
                       curs_wk	
                       ;

#    IF whatever_filtering_desired
       -- here for whatever_transformation_may_be_desired
   SET x =0;
   SET vsDate = curs_dt;
   WHILE x<=pasado DO
 
    REPLACE INTO calcworkdays_inventory ( date_last , date_first , workdays )
    SELECT vsDate , curs_dt , sum(isweekday - if( isweekday = 1 , isholiday , 0 )) 
    FROM calendar 
    where dt between curs_dt and vsDate;
		set x =x+1; 
		set vsDate = curs_dt - interval x day;
	end while;

   SET x =0;
   SET vsDate = curs_dt;
   WHILE x<=futuro DO
 
    REPLACE INTO calcworkdays_inventory ( date_last , date_first , workdays )
    SELECT vsDate , curs_dt , sum(isweekday - if( isweekday = 1 , isholiday , 0 )) 
    FROM calendar 
    where dt between curs_dt and vsDate;
		set x =x+1; 
		set vsDate = curs_dt + interval x day;
	end while;


UNTIL bDone END REPEAT;
CLOSE curs;

UPDATE        calcworkdays_inventory 
   INNER JOIN calendar 
        ON calcworkdays_inventory.date_last = calendar.dt
SET
   calcworkdays_inventory.isweekday = calendar.isweekday,
   calcworkdays_inventory.isholiday = calendar.isholiday,
   calcworkdays_inventory.isweekend = calendar.isweekend
;

UPDATE        calcworkdays_inventory 
   INNER JOIN calendar 
        ON calcworkdays_inventory.date_first = calendar.dt
SET
   calcworkdays_inventory.isweekday_first = calendar.isweekday,
   calcworkdays_inventory.isholiday_first = calendar.isholiday,
   calcworkdays_inventory.isweekend_first = calendar.isweekend
;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 PROCEDURE `daily_execution_ops`()
    MODIFIES SQL DATA
BEGIN

CALL operations_ve.out_stock_hist;

CALL operations_ve.out_order_tracking;

#CALL operations_ve.out_procurement_tracking;

CALL operations_ve.out_inverse_logistics_tracking;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`laura.forero`@`%`*/ /*!50003 PROCEDURE `days_inventory_per_day`()
BEGIN

SELECT  'Inicio rutina days_inventory_per_day',now();

SELECT  'Tabla dias de inventario',now();



SELECT date_first INTO @dia_habil_anterior FROM operations_co.calcworkdays
WHERE date_last = curdate() - interval  1 day 
     AND workdays = 1 
     AND isholiday_first = 0
     AND isweekend_first = 0
     AND isweekday_first = 1;

set @max_day=cast((select max(date) from tbl_days_of_inventory_per_day) as date);

#delete from tbl_days_of_inventory_per_day 
#where date=@dia_habil_anterior;

INSERT INTO operations_co.tbl_days_of_inventory_per_day 
			(date,buyer,new_cat1,new_cat2,new_cat3,sku_simple,sku_config,
				items_in_stock, costo_after_vat_in_stock, costo_after_vat_Consignation, 
					category_bp, pc_15_percentage, visible)
SELECT * FROM 
			(SELECT @dia_habil_anterior AS date,buyer,
					category_1,category_2,category_3,sku_simple,sku_config,
					SUM(in_stock) AS 'items_in_stock',
					SUM(IF(fulfillment_type_real like '%Own Warehouse%' OR fulfillment_type_real like '%Crossdock%',
					cost_w_o_vat*in_stock,0)) AS 'Inventory_cost',
					SUM(IF(fulfillment_type_real like '%Consignment%',
					cost_w_o_vat*in_stock,0)) AS 'Consignation_cost',
					category_bp AS 'Category_BP',
					pc_15_percentage as 'PC_1_5',
					visible as 'Is_Visible'
				FROM production_co.out_stock_hist WHERE in_stock=1 AND reserved = 0
GROUP BY sku_simple) AS a
where date<>@max_day;

UPDATE operations_co.tbl_days_of_inventory_per_day d
INNER JOIN 
		(SELECT sku_simple, 
		SUM(sold_last_30_order) sold_last_30_order,
		SUM(sold_last_30_order_cost_w_o_vat) sold_last_30_order_cost_w_o_vat,
		SUM(sold_last_15_order) sold_last_15_order,
		SUM(sold_last_15_order_cost_w_o_vat) sold_last_15_order_cost_w_o_vat,		
		SUM(sold_last_7_order) sold_last_7_order,
		SUM(sold_last_7_order_cost_w_o_vat) sold_last_7_order_cost_w_o_vat
		FROM operations_co.out_stock_hist 
		WHERE 
			sku_in_stock = 1 GROUP BY sku_simple) s 
ON 
	d.sku_simple = s.sku_simple
SET 
	d.items_sold_last_30_days = s.sold_last_30_order,
	d.costo_after_vat_sold_last_30_days = s.sold_last_30_order_cost_w_o_vat,	
	d.items_sold_last_15_days = s.sold_last_15_order,
	d.costo_after_vat_sold_last_15_days = s.sold_last_15_order_cost_w_o_vat,
	d.items_sold_last_7_days = s.sold_last_7_order,
	d.costo_after_vat_sold_last_7_days = s.sold_last_7_order_cost_w_o_vat
where date<>@max_day ;

SET @yrmonth := CONCAT(year(@dia_habil_anterior),IF(MONTH(@dia_habil_anterior)<10,CONCAT(0,MONTH(@dia_habil_anterior)),MONTH(@dia_habil_anterior)));

UPDATE tbl_days_of_inventory_per_day 
SET
	yrmonth=@yrmonth,
	week=weekofyear(@dia_habil_anterior)
WHERE yrmonth IS NULL;

UPDATE tbl_days_of_inventory_per_day 
SET
	inv_days_by_items=
					IF(items_sold_last_30_days=0,NULL,items_in_stock/(items_sold_last_30_days/30)),
	inv_days_by_value=
					IF(costo_after_vat_sold_last_30_days=0,NULL,costo_after_vat_in_stock/(costo_after_vat_sold_last_30_days/30))
;

SELECT  'Actualizar sell list',now();

UPDATE operations_co.tbl_days_of_inventory_per_day b 
INNER JOIN
		(SELECT sku_simple,SUM(is_sell_list) AS 'items_sell_list',
			SUM(is_sell_list*cost_w_o_vat) AS 'value_sell_list'
			FROM operations_co.out_stock_hist
		WHERE in_stock=1 AND reserved=0
						AND fulfillment_type_real not IN ('Consignment')
		GROUP BY sku_simple) AS a
ON 
	b.sku_simple=a.sku_simple
SET 
	b.items_sell_list=a.items_sell_list,
	b.value_sell_list=a.value_sell_list
WHERE date=@dia_habil_anterior
AND date<>@max_day;

UPDATE tbl_days_of_inventory_per_day b 
INNER JOIN
		(SELECT sku_simple,SUM(is_sell_list_30) AS 'items_sell_list_30',
				SUM(is_sell_list_30*cost_w_o_vat) AS 'value_sell_list_30'
			FROM operations_co.out_stock_hist
			WHERE in_stock=1 AND reserved=0
						AND fulfillment_type_real not IN ('Consignation')
			GROUP BY sku_simple) AS a
ON 
	b.sku_simple=a.sku_simple
SET 
	b.items_sell_list_30=a.items_sell_list_30,
	b.value_sell_list_30=a.value_sell_list_30
WHERE date=@dia_habil_anterior
AND date<>@max_day;


update tbl_days_of_inventory_per_day set buyer = trim(both ' ' from replace(replace(replace(buyer, '\t', ''), '\n',''),'\r',''));


SELECT  'Fin rutina days_inventory_per_day',now();

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `monthly_kpi_finance`()
BEGIN

DECLARE MonthNum INT;
SELECT date_format( CURRENT_DATE() - INTERVAL 1 month ,"%Y%m" )  INTO MonthNum;

DROP TABLE IF EXISTS bazayaco.monthly_kpi_finance;

CREATE TABLE IF NOT EXISTS bazayaco.monthly_kpi_finance
(
   MonthNum int,
   stock_item_id int,
   SKU_Simple  varchar(25),
   SKU_Config  varchar(25),   
   CostAfterTax decimal(15,2),
   Fulfillment varchar(65),
   Brand varchar(65),
   UpdatedAt  datetime,
   PRIMARY KEY ( MonthNum, stock_item_id ),
   KEY( SKU_Simple )
)
;

DELETE FROM bazayaco.monthly_kpi_finance WHERE MonthNum = date_format( CURRENT_DATE() -INTERVAL 1 month ,"%Y%m" );

INSERT INTO bazayaco.monthly_kpi_finance
SELECT
  MonthNum   AS MonthNum,
  stock_item_id AS stock_item_id,
  sku_simple  AS SKU_Simple,
  sku_config  AS SKU_Config,
  cost_stock_wms,
  fullfilment_type, 
  brand,
  now()       AS UpdatedAt
FROM
  bazayaco.tbl_bi_ops_stock 
WHERE
  in_stock_real = 1 and reserved = 0
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`ops_co`@`%`*/ /*!50003 PROCEDURE `out_catalog_product_stock`()
BEGIN

SELECT 'Start: catalog_product_v2', now();

/*
# Simple Variation
DROP TABLE IF EXISTS simple_variation;

CREATE TABLE operations_co.simple_variation AS
(
	SELECT
		`bob_live_co`.`catalog_simple_books`.`fk_catalog_simple` AS `fk_catalog_simple`,
		`bob_live_co`.`catalog_simple_books`.`variation` AS `variation`
	FROM
		`bob_live_co`.`catalog_simple_books`
	WHERE	`bob_live_co`.`catalog_simple_books`.`variation` IS NOT NULL
	AND		`bob_live_co`.`catalog_simple_books`.`variation` <> '...'
	AND 	`bob_live_co`.`catalog_simple_books`.`variation` <> '…'
	AND 	`bob_live_co`.`catalog_simple_books`.`variation` <> '?'
	AND 	`bob_live_co`.`catalog_simple_books`.`variation` <> ',,,'
)
UNION
	(
		SELECT
			`bob_live_co`.`catalog_simple_electronics`.`fk_catalog_simple` AS `fk_catalog_simple`,
			`bob_live_co`.`catalog_simple_electronics`.`variation` AS `variation`
		FROM
			`bob_live_co`.`catalog_simple_electronics`
		WHERE	`bob_live_co`.`catalog_simple_electronics`.`variation` IS NOT NULL
		AND 	`bob_live_co`.`catalog_simple_electronics`.`variation` <> '...'
		AND 	`bob_live_co`.`catalog_simple_electronics`.`variation` <> '…'
		AND 	`bob_live_co`.`catalog_simple_electronics`.`variation` <> '?'
		AND 	`bob_live_co`.`catalog_simple_electronics`.`variation` <> ',,,'
	)
UNION
	(
		SELECT
			`bob_live_co`.`catalog_simple_toys_baby`.`fk_catalog_simple` AS `fk_catalog_simple`,
			`bob_live_co`.`catalog_simple_toys_baby`.`variation` AS `variation`
		FROM
			`bob_live_co`.`catalog_simple_toys_baby`
		WHERE	`bob_live_co`.`catalog_simple_toys_baby`.`variation` IS NOT NULL
		AND		`bob_live_co`.`catalog_simple_toys_baby`.`variation` <> '...'
		AND		`bob_live_co`.`catalog_simple_toys_baby`.`variation` <> '…'
		AND		`bob_live_co`.`catalog_simple_toys_baby`.`variation` <> '?'
		AND		`bob_live_co`.`catalog_simple_toys_baby`.`variation` <> ',,,'
	)
UNION
	(
		SELECT
			`bob_live_co`.`catalog_simple_health_beauty`.`fk_catalog_simple` AS `fk_catalog_simple`,
			`bob_live_co`.`catalog_simple_health_beauty`.`variation` AS `variation`
		FROM
			`bob_live_co`.`catalog_simple_health_beauty`
		WHERE	`bob_live_co`.`catalog_simple_health_beauty`.`variation` IS NOT NULL
		AND		`bob_live_co`.`catalog_simple_health_beauty`.`variation` <> '...'
		AND		`bob_live_co`.`catalog_simple_health_beauty`.`variation` <> '…'
		AND		`bob_live_co`.`catalog_simple_health_beauty`.`variation` <> '?'
		AND		`bob_live_co`.`catalog_simple_health_beauty`.`variation` <> ',,,'
	)
UNION
	(
		SELECT
			`bob_live_co`.`catalog_simple_home_living`.`fk_catalog_simple` AS `fk_catalog_simple`,
			`bob_live_co`.`catalog_simple_home_living`.`variation` AS `variation`
		FROM
			`bob_live_co`.`catalog_simple_home_living`
		WHERE	`bob_live_co`.`catalog_simple_home_living`.`variation` IS NOT NULL
		AND		`bob_live_co`.`catalog_simple_home_living`.`variation` <> '...'
		AND		`bob_live_co`.`catalog_simple_home_living`.`variation` <> '…'
		AND		`bob_live_co`.`catalog_simple_home_living`.`variation` <> '?'
		AND		`bob_live_co`.`catalog_simple_home_living`.`variation` <> ',,,'
	)
UNION
	(
		SELECT
			`bob_live_co`.`catalog_simple_media`.`fk_catalog_simple` AS `fk_catalog_simple`,
			`bob_live_co`.`catalog_simple_media`.`variation` AS `variation`
		FROM
			`bob_live_co`.`catalog_simple_media`
		WHERE	`bob_live_co`.`catalog_simple_media`.`variation` IS NOT NULL
		AND		`bob_live_co`.`catalog_simple_media`.`variation` <> '...'
		AND		`bob_live_co`.`catalog_simple_media`.`variation` <> '…'
		AND		`bob_live_co`.`catalog_simple_media`.`variation` <> '?'
		AND		`bob_live_co`.`catalog_simple_media`.`variation` <> ',,,'
	)
UNION
	(
		SELECT
			`bob_live_co`.`catalog_simple_other`.`fk_catalog_simple` AS `fk_catalog_simple`,
			`bob_live_co`.`catalog_simple_other`.`variation` AS `variation`
		FROM
			`bob_live_co`.`catalog_simple_other`
		WHERE	`bob_live_co`.`catalog_simple_other`.`variation` IS NOT NULL
		AND		`bob_live_co`.`catalog_simple_other`.`variation` <> '...'
		AND		`bob_live_co`.`catalog_simple_other`.`variation` <> '…'
		AND		`bob_live_co`.`catalog_simple_other`.`variation` <> '?'
		AND		`bob_live_co`.`catalog_simple_other`.`variation` <> ',,,'
	)
UNION
	(
		SELECT
			`bob_live_co`.`catalog_simple_sports`.`fk_catalog_simple` AS `fk_catalog_simple`,
			`bob_live_co`.`catalog_simple_sports`.`variation` AS `variation`
		FROM
			`bob_live_co`.`catalog_simple_sports`
		WHERE	`bob_live_co`.`catalog_simple_sports`.`variation` IS NOT NULL
		AND		`bob_live_co`.`catalog_simple_sports`.`variation` <> '...'
		AND		`bob_live_co`.`catalog_simple_sports`.`variation` <> '…'
		AND		`bob_live_co`.`catalog_simple_sports`.`variation` <> '?'
		AND		`bob_live_co`.`catalog_simple_sports`.`variation` <> ',,,'
	)
UNION
	(
		SELECT
			`bob_live_co`.`catalog_simple_fashion`.`fk_catalog_simple` AS `fk_catalog_simple`,
			`bob_live_co`.`catalog_attribute_option_fashion_size`.`name` AS `name`
		FROM
			(
				`bob_live_co`.`catalog_simple_fashion`
				JOIN `bob_live_co`.`catalog_attribute_option_fashion_size`
			)
		WHERE `bob_live_co`.`catalog_simple_fashion`.`fk_catalog_attribute_option_fashion_size` = `bob_live_co`.`catalog_attribute_option_fashion_size`.`id_catalog_attribute_option_fashion_size`
		AND		`bob_live_co`.`catalog_attribute_option_fashion_size`.`name` IS NOT NULL
	);

ALTER TABLE operations_co.simple_variation ADD PRIMARY KEY (`fk_catalog_simple`) ;
*/

# out_catalog_product_stock
TRUNCATE operations_co.out_catalog_product_stock;

INSERT INTO out_catalog_product_stock(
	fk_catalog_simple,
	sku,
	reservedbob,
	stockbob,
	availablebob)
SELECT 
	catalog_simple.id_catalog_simple,
	catalog_simple.sku,
	IF(sum(is_reserved) IS NULL, 0, sum(is_reserved))  AS reservedBOB,
	ifnull(catalog_stock.quantity,0) AS stockbob,
	ifnull(catalog_stock.quantity,0)-IF(sum(sales_order_item.is_reserved) IS NULL, 0, sum(is_reserved)) AS availablebob
FROM 
(bob_live_co.catalog_simple
INNER JOIN bob_live_co.catalog_source
 ON catalog_simple.id_catalog_simple = catalog_source.fk_catalog_simple
INNER JOIN bob_live_co.catalog_stock
 ON catalog_source.id_catalog_source = catalog_stock.fk_catalog_source) 
	LEFT JOIN bob_live_co.sales_order_item
		ON catalog_simple.sku = sales_order_item.sku
GROUP BY catalog_simple.sku
ORDER BY catalog_stock.quantity DESC;

UPDATE operations_co.out_catalog_product_stock 
INNER JOIN bob_live_co.catalog_warehouse_stock 
	ON out_catalog_product_stock.fk_catalog_simple = catalog_warehouse_stock.fk_catalog_simple
SET
	out_catalog_product_stock.ownstock = catalog_warehouse_stock.quantity;

UPDATE operations_co.out_catalog_product_stock 
INNER JOIN bob_live_co.catalog_supplier_stock 
	ON out_catalog_product_stock.fk_catalog_simple = catalog_supplier_stock.fk_catalog_simple
SET 
 out_catalog_product_stock.supplierstock = catalog_supplier_stock.quantity;

UPDATE operations_co.out_catalog_product_stock 
SET availablebob = 0 
WHERE availablebob < 0;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 PROCEDURE `out_inverse_logistics_tracking`()
BEGIN
#Test ...	
INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'out_inverse_logistics_tracking',
  'start',
  NOW(),
  max(date_ordered),
  count(*),
  count(item_counter)
FROM
  production_co.out_inverse_logistics_tracking;


DELETE operations_co.pro_inverse_logistics_WMS.*
FROM
	operations_co.pro_inverse_logistics_WMS;


INSERT INTO operations_co.pro_inverse_logistics_WMS (
	id_inverse_logistics_docs,
	order_nr,
	item_id,
	sku_simple,
	shipping_carrier_return,
	dhl_tracking_code,
	reason,
	ilwh_1st_step,
	comments,
	ilwh_position,
	date_cancelled,
	comments_main
) SELECT
	id,
	order_number,
	item_id,
	sku,
	carrier,
	carrier_tracking_code,
	reason_id,
	action_id,
	COMMENT,
	STATUS,
	created_at,
	comments_il
FROM
	wmsprod_co.inverselogistics_devolucion;


UPDATE operations_co.pro_inverse_logistics_WMS a
INNER JOIN (
	SELECT
		b.return_id,
		b. STATUS,
		b.changed_at,
		c.item_id
	FROM
		wmsprod_co.inverselogistics_status_history b
	JOIN wmsprod_co.inverselogistics_devolucion c ON b.return_id = c.id
) d ON a.item_id = d.item_id
SET a.date_entrance_ilwh = d.changed_at
WHERE
	d. STATUS = 'retornado';


INSERT INTO operations_co.pro_inverse_logistics_WMS (
	id_inverse_logistics_docs,
	wms_tracking_code,
	reason,
	date_cancelled
) SELECT
	id,
	cod_rastreamento,
	id_status,
	date
FROM
	wmsprod_co.tms_status_delivery
WHERE
	tms_status_delivery.id_status = 10;


UPDATE operations_co.pro_inverse_logistics_WMS a
SET a. STATUS = CASE
WHEN a.reason = 10 THEN
	'Entrega no Exitosa'
ELSE
	'Devolucion'
END;


UPDATE operations_co.pro_inverse_logistics_WMS a
SET a.damaged_item = CASE
WHEN a.reason = 7 THEN
	1
ELSE
	NULL
END;


UPDATE operations_co.pro_inverse_logistics_WMS a
SET a.damaged_package = CASE
WHEN a.reason = 1 THEN
	1
ELSE
	NULL
END;


UPDATE operations_co.pro_inverse_logistics_WMS a
SET a.return_accepted = CASE
WHEN a.ilwh_position = 'retornar_stock'
OR a.ilwh_position = 'enviar_cuarentena' THEN
	1
ELSE
	NULL
END;


UPDATE operations_co.pro_inverse_logistics_WMS a
SET a.create_new_order = CASE
WHEN a.ilwh_1st_step = 9
OR a.ilwh_1st_step = 10
AND a.return_accepted = 1 THEN
	1
ELSE
	NULL
END;


UPDATE operations_co.pro_inverse_logistics_WMS a
INNER JOIN wmsprod_co.inverselogistics_devolucion b ON a.item_id = b.item_id
SET a.date_exit_ilwh = CASE
WHEN b. STATUS = 'retornar_stock'
OR b. STATUS = 'enviar_cuarentena' THEN
	modified_at
ELSE
	NULL
END;


UPDATE operations_co.pro_inverse_logistics_WMS a
INNER JOIN wmsprod_co.inverselogistics_devolucion b ON a.item_id = b.item_id
SET a.ilwh_exit_to = CASE
WHEN b. STATUS = 'retornar_stock' THEN
	'A'
WHEN b. STATUS = 'enviar_cuarentena' THEN
	'V'
WHEN b. STATUS = 'reenvio_cliente' THEN
	'C'
ELSE
	NULL
END;


UPDATE operations_co.pro_inverse_logistics_WMS a
INNER JOIN (
	SELECT
		b.user_id,
		b.return_id,
		b. STATUS,
		b.changed_at,
		c.item_id
	FROM
		wmsprod_co.inverselogistics_status_history b
	JOIN wmsprod_co.inverselogistics_devolucion c ON b.return_id = c.id
) d ON a.item_id = d.item_id
SET a.entered_to_ilwh_by = d.user_id
WHERE
	d. STATUS = 'retornado';


UPDATE operations_co.pro_inverse_logistics_WMS a
INNER JOIN wmsprod_co.tms_status_delivery b ON b.cod_rastreamento = a.wms_tracking_code
SET a.entered_to_ilwh_by = b.id_user
WHERE
	a.reason = 10;


UPDATE operations_co.pro_inverse_logistics_WMS a
INNER JOIN wmsprod_co.ws_log_lista_embarque b ON b.cod_rastreamento = a.wms_tracking_code
SET a.reason = b.observaciones
WHERE
 a.reason = 10;

UPDATE operations_co.pro_inverse_logistics_WMS a
SET a.reason = CASE
WHEN a.reason = 1 THEN
 "D-Producto Dañado"
WHEN a.reason = 2 THEN
 "D-Producto Incorrecto"
WHEN a.reason = 3 THEN
 "D-Producto duplicado"
WHEN a.reason = 4 THEN
 "D-No se quiere el producto"
WHEN a.reason = 5 THEN
 "D-Piezas faltantes"
WHEN a.reason = 6 THEN
 "D.Retraso en entrega de pedido"
WHEN a.reason = 7 THEN
 "D-Producto usado"
WHEN a.reason = 8 THEN
 "D-Otro"
ELSE
a.reason
END;

UPDATE operations_co.pro_inverse_logistics_WMS a
SET a.ilwh_2nd_step = a.ilwh_1st_step
WHERE
	a.ilwh_position = 'retornar_stock'
OR a.ilwh_position = 'enviar_cuarentena';


UPDATE operations_co.pro_inverse_logistics_WMS a
SET a.ilwh_1st_step = CASE
WHEN a.ilwh_1st_step = 9 THEN
	"Otro producto igual"
WHEN a.ilwh_1st_step = 10 THEN
	"Otro producto diferente"
WHEN a.ilwh_1st_step = 11 THEN
	"Reembolso en crédito Linio"
WHEN a.ilwh_1st_step = 12 THEN
	"Rembolso a cuenta (2 -5 días)"
WHEN a.ilwh_1st_step = 13 THEN
	"Reverso a tarjeta (10-15 días)"
ELSE
	NULL
END;


UPDATE operations_co.pro_inverse_logistics_WMS a
SET a.ilwh_2nd_step = CASE
WHEN a.ilwh_2nd_step = 9 THEN
	"Otro producto igual"
WHEN a.ilwh_2nd_step = 10 THEN
	"Otro producto diferente"
WHEN a.ilwh_2nd_step = 11 THEN
	"Reembolso en crédito Linio"
WHEN a.ilwh_1st_step = 12 THEN
	"Rembolso a cuenta (2 -5 días)"
WHEN a.ilwh_1st_step = 13 THEN
	"Reverso a tarjeta (10-15 días)"
ELSE
	NULL
END;

INSERT INTO operations_co.pro_inverse_logistics_WMS (
	date_entrance_ilwh,
	order_nr,
	item_id,
	description,
	sku_simple,
	reason,
  ilwh_2nd_step,
  comments,
  STATUS
) SELECT
	date_entrance_ilwh,
	order_nr,
	item_id,
	description,
	sku_simple,
	reason,
  ilwh_2nd_step,
  comments,
  STATUS 
FROM
	(
		SELECT
			A.*
		FROM
			operations_co.pro_inverse_logistics_docs A
		LEFT JOIN operations_co.pro_inverse_logistics_WMS B ON A.item_id = B.item_id
		WHERE
			B.item_id IS NULL
	) C;





DELETE operations_co.out_inverse_logistics_tracking.*
FROM
	operations_co.out_inverse_logistics_tracking;


INSERT INTO operations_co.out_inverse_logistics_tracking (
	item_id,
	order_number,
	sku_config,
	sku_simple,
	sku_name,
	supplier_id,
	supplier_name,
	buyer,
	head_buyer,
	package_height,
	package_length,
	package_width,
	package_weight,
	date_ordered,
	fullfilment_type_bob,
	fullfilment_type_real,
	shipping_carrier,
	date_shipped,
	date_delivered,
	date_delivered_promised,
	week_delivered_promised,
	month_num_delivered_promised,
	on_time_to_procure,
	presale,
	payment_method,
	ship_to_state,
	ship_to_zip,
	ship_to_zip2,
	ship_to_zip3,
	wms_tracking_code,
	shipping_carrier_tracking_code,
	status_bob,
	status_wms,
	item_counter,
	package_measure,
	shipped_last_30,
	delivered_promised_last_30,
	ship_to_met_area,
	category_com_main,
	category_com_sub,
	category_com_sub_sub,
	category_bp,
	fullfilment_type_bp
) SELECT
	order_item_id,
	order_number,
	sku_config,
	sku_simple,
	sku_name,
	supplier_id,
	supplier_name,
	buyer,
	head_buyer,
	package_height,
	package_length,
	package_width,
	package_weight,
	date_ordered,
	fullfilment_type_bob,
	fullfilment_type_real,
	shipping_carrier,
	date_shipped,
	date_delivered,
	date_delivered_promised,
	week_delivered_promised,
	month_num_delivered_promised,
	on_time_to_procure,
	presale,
	payment_method,
	ship_to_state,
	ship_to_zip,
	ship_to_zip2,
	ship_to_zip3,
	wms_tracking_code,
	shipping_carrier_tracking_code,
	status_bob,
	status_wms,
	item_counter,
	package_measure,
	shipped_last_30,
	delivered_promised_last_30,
	ship_to_met_area,
	category_com_main,
	category_com_sub,
	category_com_sub_sub,
	category_bp,
	fullfilment_type_bp
  
FROM
	operations_co.out_order_tracking;


UPDATE operations_co.out_inverse_logistics_tracking a
INNER JOIN operations_co.pro_inverse_logistics_WMS b ON a.order_item_id = b.item_id
SET a.date_entrance_ilwh = date(b.date_entrance_ilwh),
 a.id_ilwh = b.id_inverse_logistics_docs,
 a.previous_ilwh_entrance = b.entrance_again,
 a.tracking_number_returns = b.dhl_tracking_code,
 a.reason_delivery_fail_return = b.reason,
 a.damaged_item = b.damaged_item,
 a.damaged_package = b.damaged_package,
 a.comments = b.comments_main,
 a.return_accepted = b.return_accepted,
 a.ilwh_position = b.ilwh_position,
 a.ilwh_cancellation_date = date(b.date_entrance_ilwh),
 a.ilwh_exit_date = date(b.date_exit_ilwh),
 a.entered_to_ilwh = b.entered_to_ilwh_by,
 a.payment_method_type = b.payment_method;


UPDATE operations_co.out_inverse_logistics_tracking a
JOIN operations_co.pro_inverse_logistics_WMS b ON a.wms_tracking_code = b.wms_tracking_code
SET a.ilwh_status = 'Delivery Fail',
 a.reason_delivery_fail_return = b.reason,
 a.id_ilwh = b.id_inverse_logistics_docs,
 a.ilwh_cancellation_date = b.date_cancelled,
 a.date_entrance_ilwh = b.date_cancelled
WHERE
 b. STATUS = 'Entrega no Exitosa';

UPDATE operations_co.out_inverse_logistics_tracking a
INNER JOIN operations_co.pro_inverse_logistics_WMS b ON a.order_item_id = b.item_id
SET a.ilwh_status = CASE
WHEN b. STATUS = 'Bloqueo Interno' THEN
	'Internal block'
WHEN b. STATUS = 'Entrega no Exitosa' THEN
	'Delivery Fail'
WHEN b. STATUS LIKE 'Devol%' THEN
	'Return'
ELSE
	NULL
END;


UPDATE operations_co.out_inverse_logistics_tracking a
INNER JOIN operations_co.pro_inverse_logistics_WMS b ON a.order_item_id = b.item_id
SET a.ilwh_1st_step = CASE
WHEN b.ilwh_1st_step = 'Cancelado' THEN
	'Cancelled'
WHEN b.ilwh_1st_step LIKE 'Re-Env%' THEN
	'Re-Delivery'
WHEN b.ilwh_1st_step LIKE 'Recolecci%' THEN
	'Split recolection'
ELSE
	b.ilwh_1st_step
END;


UPDATE operations_co.out_inverse_logistics_tracking a
INNER JOIN operations_co.pro_inverse_logistics_WMS b ON a.order_item_id = b.item_id
SET a.ilwh_2nd_step = CASE
WHEN b.ilwh_2nd_step LIKE 'Cancelado por L%' THEN
	'Cancelled by Logistics'
WHEN b.ilwh_2nd_step = 'Cancelado por el cliente' THEN
	'Cancelled by Client'
WHEN b.ilwh_2nd_step = 'Cancelado' THEN
	'Cancelled by Logistics'
WHEN b.ilwh_2nd_step LIKE 'Re-Env%' THEN
	'Re-Delivery'
WHEN b.ilwh_2nd_step LIKE 'cliente soli%' THEN
	'Re-Delivery'
ELSE
	b.ilwh_2nd_step
END;


UPDATE operations_co.out_inverse_logistics_tracking a
INNER JOIN operations_co.pro_inverse_logistics_WMS b ON a.order_item_id = b.item_id
SET a.ilwh_exit_to = CASE
WHEN b.ilwh_exit_to LIKE '%V%' THEN
	'Sale'
WHEN b.ilwh_exit_to LIKE 'A%' THEN
	'Stock'
WHEN b.ilwh_exit_to LIKE 'P%' THEN
	'Supplier'
WHEN b.ilwh_exit_to LIKE 'C%' THEN
	'Client'
ELSE
	NULL
END;


UPDATE operations_co.out_inverse_logistics_tracking a
SET a.`RETURN` =
IF (a.ilwh_status = 'Return', 1, 0);


UPDATE operations_co.out_inverse_logistics_tracking a
SET a.failed_delivery =
IF (
	a.ilwh_status = 'Delivery Fail',
	1,
	0
);


UPDATE operations_co.out_inverse_logistics_tracking a
SET a.internal_block = 1
WHERE
	a.ilwh_status = 'Internal Block';


UPDATE operations_co.out_inverse_logistics_tracking a
SET a.entered_to_ilwh = 1
WHERE
	a.id_ilwh IS NOT NULL;


UPDATE operations_co.out_inverse_logistics_tracking a
SET a.1st_cancelled =
IF (
	ilwh_1st_step = 'Cancelled',
	1,
	0
),
 a.1st_partial_retrieval =
IF (
	ilwh_1st_step = 'Split Recolection',
	1,
	0
),
 a.1st_re_delivery =
IF (
	ilwh_1st_step = 'Re-Delivery',
	1,
	0
),
 a.1st_non_applicable =
IF (ilwh_1st_step IS NULL, 1, 0);


UPDATE operations_co.out_inverse_logistics_tracking a
SET a.2nd_cancelled =
IF (
	ilwh_2nd_step = 'Cancelled',
	1,
	0
),
 a.2nd_cancelled_by_client =
IF (
	ilwh_2nd_step = 'Cancelled by Client',
	1,
	0
),
 a.2nd_cancelled_by_logistics =
IF (
	ilwh_2nd_step = 'Cancellled by Logistics',
	1,
	0
),
 a.2nd_re_delivery =
IF (
	ilwh_2nd_step = 'Re-Delivery',
	1,
	0
);


UPDATE operations_co.out_inverse_logistics_tracking a
SET a.exit_to_client_failed_delivery =
IF (ilwh_exit_to = 'Client', 1, 0),
 a.exit_to_stock_failed_delivery =
IF (ilwh_exit_to = 'Stock', 1, 0),
 a.exit_to_supplier_failed_delivery =
IF (
	ilwh_exit_to = 'Supplier',
	1,
	0
),
 a.exit_to_resale_failed_delivery =
IF (ilwh_exit_to = 'Sale', 1, 0)
WHERE
	a.failed_delivery = 1;


UPDATE operations_co.out_inverse_logistics_tracking a
SET a.exit_to_client_returns =
IF (ilwh_exit_to = 'Client', 1, 0),
 a.exit_to_stock_returns =
IF (ilwh_exit_to = 'Stock', 1, 0),
 a.exit_to_supplier_returns =
IF (
	ilwh_exit_to = 'Supplier',
	1,
	0
),
 a.exit_to_resale_returns =
IF (ilwh_exit_to = 'Sale', 1, 0)
WHERE
	a. `RETURN` = 1;


UPDATE operations_co.out_inverse_logistics_tracking a
SET a.exit_to_client_internal_block =
IF (ilwh_exit_to = 'Client', 1, 0),
 a.exit_to_stock_internal_block =
IF (ilwh_exit_to = 'Stock', 1, 0),
 a.exit_to_supplier_internal_block =
IF (
	ilwh_exit_to = 'Supplier',
	1,
	0
),
 a.exit_to_resale_internal_block =
IF (ilwh_exit_to = 'Sale', 1, 0)
WHERE
	a.internal_block = 1;


UPDATE operations_co.out_inverse_logistics_tracking a
SET a.reintegrate_to_stock =
IF (ilwh_position = 'IN' OR ilwh_position = 'retornar_stock', 1, 0);


UPDATE operations_co.out_inverse_logistics_tracking a
SET a.succesfull_redelivery =
IF (ilwh_position = 'Client', 1, 0);


UPDATE operations_co.out_inverse_logistics_tracking a
SET a.pending_exits =
IF (a.ilwh_exit_date IS NULL, 1, 0);


UPDATE operations_co.out_inverse_logistics_tracking
SET out_inverse_logistics_tracking.last_30 = CASE
WHEN datediff(curdate(), date_entrance_ilwh) <= 45
AND datediff(curdate(), date_entrance_ilwh) >= 15 THEN
	1
ELSE
	0
END;



UPDATE operations_co.out_inverse_logistics_tracking
SET item_counter = 1;

DROP TABLE IF EXISTS production_co.out_inverse_logistics_tracking;

CREATE TABLE production_co.out_inverse_logistics_tracking LIKE operations_co.out_inverse_logistics_tracking;

INSERT INTO production_co.out_inverse_logistics_tracking SELECT * FROM operations_co.out_inverse_logistics_tracking;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'out_inverse_logistics_tracking',
  'finish',
  NOW(),
  max(date_ordered),
  count(*),
  count(item_counter)
FROM
  production_co.out_inverse_logistics_tracking;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`natali.serrano`@`%`*/ /*!50003 PROCEDURE `out_order_tracking`()
    MODIFIES SQL DATA
BEGIN
DECLARE rows     INT DEFAULT 0;
DECLARE attempts INT DEFAULT 12;

###Monitoring_log
SELECT  'Monitoring_log',now();
INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'out_order_tracking',
  'start',
  NOW(),
  max(date_time_ordered),
  count(*),
  count(item_counter)
FROM
  production.out_order_tracking;

DROP TABLE IF EXISTS operations_co.tbl_purchase_order;

CREATE TABLE operations_co.tbl_purchase_order LIKE bazayaco.tbl_purchase_order;

INSERT INTO operations_co.tbl_purchase_order SELECT * FROM bazayaco.tbl_purchase_order;

SELECT  'Create out_order_tracking_sample',now();
DROP TABLE IF EXISTS operations_co.out_order_tracking_sample;

CREATE TABLE operations_co.out_order_tracking_sample LIKE operations_co.out_order_tracking;

TRUNCATE operations_co.out_order_tracking_sample;

SELECT  'Insert Itens_venda',now();
INSERT INTO operations_co.out_order_tracking_sample (
	item_id,
	order_number,
	sku_simple,
	status_wms,
	order_id,
	min_delivery_time,
	max_delivery_time,
#Fecha
	date_exported,
	datetime_exported,
	supplier_leadtime,
	is_linio_promise
) SELECT
	a.item_id,
	a.numero_order,
	a.sku,
	a.STATUS,
	a.order_id,
	a.tempo_de_entrega_minimo,
	a.tempo_de_entrega_maximo,
	date(a.data_exportable),
	a.data_exportable,
	a.tempo_de_entrega_minimo - 2,
	if(a.linio_promise IS NULL, 0, a.linio_promise)
FROM
	wmsprod_co.itens_venda AS a
WHERE DATE_FORMAT(a.data_criacao,'%Y%m') >= (DATE_FORMAT(curdate()- INTERVAL 4 MONTH,'%Y%m'));

SELECT  'transportadoras',now();
UPDATE operations_co.out_order_tracking_sample AS a
INNER JOIN wmsprod_co.itens_venda AS b
	ON a.item_id = b.item_id
INNER JOIN wmsprod_co.romaneio AS c
	ON b.romaneio_id = c.romaneio_id
INNER JOIN wmsprod_co.transportadoras AS d
	ON c.transportadora_id = d.transportadoras_id
SET a.shipping_carrier = d.nome_transportadora;

#Nota: Tenemos que avisar a A_Master

SELECT  'A_Master_Catalog',now();

CALL production.monitoring( "A_Master_Catalog" , "Colombia_Project",  240 );


UPDATE operations_co.out_order_tracking_sample AS a
INNER JOIN development_co_project.A_Master_Catalog AS b 
 ON a.sku_simple = b.sku_simple
SET
	a.cat_1 = b.Cat1,
	a.cat_2 = b.Cat2,
	a.cat_3 = b.Cat3,
	a.category_bp = Cat_BP,
	a.category_kpi = Cat_KPI,
	a.head_buyer = b.Head_Buyer,
	a.buyer = b.Buyer,
	a.sku_config = b.sku_config,
	a.sku_name = b.sku_name,
 a.supplier_id = b.id_supplier,
 	a.supplier_name = b.Supplier,
 	a.package_height = b.package_height,
 	a.package_length = b.package_length,
 	a.package_width = b.package_width,
 	a.package_weight = b.package_weight;

SELECT  'Update out_order_tracking_sample',now();	
#vol_weight_carrier aplica solo para CO
UPDATE operations_co.out_order_tracking_sample AS a
SET 
	a.vol_weight = a.package_height * a.package_length * a.package_width / 5000,
	a.vol_weight_carrier= IF(shipping_carrier='DESPACHOS SERVIENTREGAS',
													(((a.package_width*a.package_length*a.package_height)*222)/1000000),
												IF(a.shipping_carrier='DESPACHO DESPRISA',
													(((a.package_width*a.package_length*a.package_height)*400)/1000000),
													(((a.package_width*a.package_length*a.package_height)*222)/1000000)));

SELECT  'Update out_order_tracking_sample2',now();	
UPDATE operations_co.out_order_tracking_sample AS a
SET
a.max_vol_w_vs_w = 	CASE
											WHEN a.vol_weight > a.package_weight 
											THEN a.vol_weight
											ELSE a.package_weight
										END;

SELECT  'Update out_order_tracking_sample3',now();	
UPDATE operations_co.out_order_tracking_sample AS a
SET
a.package_measure_new = CASE
													WHEN a.max_vol_w_vs_w > 35 
													THEN 'oversized'
													WHEN a.max_vol_w_vs_w > 5 
													THEN 'large'
													WHEN a.max_vol_w_vs_w > 2 
													THEN 'medium'
													ELSE 'small'
												END;

SELECT  'Update Inventory',now();
UPDATE operations_co.out_order_tracking_sample a
INNER JOIN wmsprod_co.itens_venda b
	ON a.item_id = b.item_id
INNER JOIN wmsprod_co.status_itens_venda c
	ON b.itens_venda_id = c.itens_venda_id
SET a.fulfillment_type_real = "Own Warehouse"
WHERE c.STATUS IN ( "Estoque reservado", "ac_estoque_reservado");

SELECT  'Update dropshipping',now();
UPDATE operations_co.out_order_tracking_sample a
INNER JOIN wmsprod_co.itens_venda b
	ON a.item_id = b.item_id
INNER JOIN wmsprod_co.status_itens_venda c
	ON b.itens_venda_id = c.itens_venda_id
SET a.fulfillment_type_real = "Dropshipping"
WHERE	c.STATUS IN ('DS estoque reservado',
										'Waiting dropshipping'
										'dropshipping notified',
										'Electronic good'
									);

SELECT  'Update crossdocking',now();
UPDATE operations_co.out_order_tracking_sample a
INNER JOIN wmsprod_co.itens_venda b
	ON a.item_id = b.item_id
INNER JOIN wmsprod_co.status_itens_venda c
	ON b.itens_venda_id = c.itens_venda_id
SET a.fulfillment_type_real = "Crossdocking"
WHERE	c.STATUS IN("analisando quebra",
									"aguardando estoque",
									"ac_analisando_quebra");

########## Queries Fechas

CALL operations_co.calcworkdays;

TRUNCATE operations_co.pro_order_tracking_dates;

INSERT INTO operations_co.pro_order_tracking_dates (
	item_id,
	order_number,
	order_id,
  sku_simple,
  status_wms,
  date_exported,
	datetime_exported,
  supplier_leadtime,
  min_delivery_time,
	max_delivery_time
) SELECT
	a.item_id,
	a.numero_order,
	a.order_id,
  a.sku,
  a.STATUS,
  date(a.data_exportable),
	a.data_exportable,
  a.tempo_de_entrega_minimo - 2,
  a.tempo_de_entrega_minimo,
	a.tempo_de_entrega_maximo
FROM
	wmsprod_co.itens_venda AS a
WHERE DATE_FORMAT(a.data_criacao,'%Y%m') >= (DATE_FORMAT(curdate()- INTERVAL 4 MONTH,'%Y%m'));


UPDATE operations_co.pro_order_tracking_dates AS a
INNER JOIN bob_live_co.sales_order_item AS b
	ON a.item_id = b.id_sales_order_item
INNER JOIN bob_live_co.catalog_shipment_type AS c
	ON b.fk_catalog_shipment_type = c.id_catalog_shipment_type
SET a.fulfillment_type_bob = c.NAME;



UPDATE operations_co.pro_order_tracking_dates a
INNER JOIN wmsprod_co.itens_venda b
	ON a.item_id = b.item_id
INNER JOIN wmsprod_co.status_itens_venda c
	ON b.itens_venda_id = c.itens_venda_id
SET a.fulfillment_type_real = "Own Warehouse"
WHERE c.STATUS IN ( "Estoque reservado", "ac_estoque_reservado");



UPDATE 	(operations_co.pro_order_tracking_dates AS a
	INNER JOIN wmsprod_co.itens_venda AS b
	ON a.item_id = b.item_id)
INNER JOIN wmsprod_co.status_itens_venda AS c
ON b.itens_venda_id = c.itens_venda_id
SET a.fulfillment_type_real = "Dropshipping"
WHERE
	c.STATUS IN ("DS estoque reservado",
				'Waiting dropshipping'
				'dropshipping notified',
				'Electronic good'
				);


UPDATE operations_co.pro_order_tracking_dates a
INNER JOIN wmsprod_co.itens_venda b
	ON a.item_id = b.item_id
INNER JOIN wmsprod_co.status_itens_venda c
	ON b.itens_venda_id = c.itens_venda_id
SET a.fulfillment_type_real = "Crossdocking"
WHERE	c.STATUS IN("analisando quebra",
									"aguardando estoque",
									"ac_analisando_quebra");


UPDATE operations_co.pro_order_tracking_dates AS a
INNER JOIN development_co_project.A_Master_Catalog AS b 
 ON a.sku_simple = b.sku_simple
SET
 a.supplier_id = b.id_supplier;

UPDATE 
((bob_live_co.sales_order_address AS a
RIGHT JOIN (operations_co.pro_order_tracking_dates AS b
				INNER JOIN bob_live_co.sales_order AS c
				ON b.order_number = c.order_nr)
ON a.id_sales_order_address = c.fk_sales_order_address_billing)
LEFT JOIN bob_live_co.customer_address_region AS d
ON a.fk_customer_address_region = d.id_customer_address_region)
INNER JOIN bob_live_co.sales_order_item AS e
ON b.item_id = e.id_sales_order_item
SET 
 b.date_ordered = date(c.created_at),
 b.datetime_ordered = c.created_at
;


UPDATE operations_co.pro_order_tracking_dates  AS a
INNER JOIN wmsprod_co.vw_itens_venda_entrega AS b
ON a.item_id = b.item_id
INNER JOIN wmsprod_co.tms_status_delivery AS c
ON b.cod_rastreamento = c.cod_rastreamento
SET a.date_delivered = (
	SELECT
		CASE
	WHEN tms.date < '2012-05-01' THEN
		NULL
	ELSE
		max(tms.date)
	END
	FROM
		wmsprod_co.vw_itens_venda_entrega AS itens,
		wmsprod_co.tms_status_delivery AS tms
	WHERE
		itens.cod_rastreamento = b.cod_rastreamento
	AND tms.cod_rastreamento = c.cod_rastreamento
	AND tms.id_status = 4
);


UPDATE 
((bob_live_co.sales_order_address AS a
RIGHT JOIN (operations_co.pro_order_tracking_dates AS b
				INNER JOIN bob_live_co.sales_order AS c
				ON b.order_number = c.order_nr)
ON a.id_sales_order_address = c.fk_sales_order_address_billing)
LEFT JOIN bob_live_co.customer_address_region AS d
ON a.fk_customer_address_region = d.id_customer_address_region)
INNER JOIN bob_live_co.sales_order_item AS e
ON b.item_id = e.id_sales_order_item
SET 
 b.date_ordered = date(c.created_at),
 b.datetime_ordered = c.created_at
;


UPDATE operations_co.pro_order_tracking_dates AS a
	INNER JOIN wmsprod_co.itens_venda AS b
	ON a.item_id=b.item_id
	INNER JOIN
	(SELECT itens_venda_id,max(data) date
	FROM wmsprod_co.status_itens_venda WHERE status = 'backorder'
	GROUP BY itens_venda_id) AS c
ON b.itens_venda_id=c.itens_venda_id
SET a.date_backorder = c.date,
a.is_backorder = 1;



UPDATE operations_co.pro_order_tracking_dates AS a
	INNER JOIN wmsprod_co.itens_venda AS b
	ON a.item_id=b.item_id
	INNER JOIN
	(SELECT itens_venda_id,max(data) date
	FROM wmsprod_co.status_itens_venda WHERE status = 'backorder_tratada'
	GROUP BY itens_venda_id) AS c
ON b.itens_venda_id=c.itens_venda_id
SET a.date_backorder_tratada = c.date;



UPDATE  operations_co.pro_order_tracking_dates AS a
INNER JOIN wmsprod_co.itens_venda AS b
ON a.item_id=b.item_id
INNER JOIN wmsprod_co.estoque AS c
ON b.estoque_id=c.estoque_id
INNER JOIN wmsprod_co.itens_recebimento AS d
ON c.itens_recebimento_id=d.itens_recebimento_id
INNER JOIN procurement_live_co.wms_received_item AS e 
ON d.itens_recebimento_id = e.id_wms
INNER JOIN operations_co.out_procurement_tracking AS f
ON e.fk_procurement_order_item = f.id_procurement_order_item
SET 
a.date_po_created = date(f.date_po_created),
a.datetime_po_created = f.date_po_created,
a.date_po_updated  = date(f.date_po_created),
a.datetime_po_updated = f.date_po_updated,
a.date_po_issued = date(f.date_po_issued),
a.datetime_po_issued = f.date_po_issued,
a.date_po_confirmed = date(f.date_po_confirmed),
a.datetime_po_confirmed = f.date_po_confirmed;

UPDATE operations_co.pro_order_tracking_dates AS a
INNER JOIN operations_co.tbl_purchase_order AS b
ON a.item_id=b.itemid
SET a.date_po_created = date(b.fecha_creacion),
a.datetime_po_created = b.fecha_creacion;

UPDATE ((operations_co.pro_order_tracking_dates AS a
INNER JOIN wmsprod_co.itens_venda AS b
ON a.item_id = b.item_id)
INNER JOIN wmsprod_co.status_itens_venda AS c
ON b.itens_venda_id = c.itens_venda_id) 
SET a.date_sent_tracking_code = date(c.data),
a.datetime_sent_tracking_code = c.data
WHERE ((c.status)="Expedido");


UPDATE operations_co.pro_order_tracking_dates AS a
INNER JOIN wmsprod_co.itens_venda AS b
ON a.item_id = b.item_id
INNER JOIN wmsprod_co.status_itens_venda AS c
ON b.itens_venda_id = c.itens_venda_id
SET 
a.date_procured = date(DATA),
a.datetime_procured = DATA
WHERE c.STATUS = "estoque reservado";

UPDATE operations_co.pro_order_tracking_dates
SET date_ready_to_pick = 	CASE
														WHEN dayofweek(date_procured) = 1 
														THEN operations_co.workday (date_procured, 1)
														WHEN dayofweek(date_procured) = 7 
														THEN operations_co.workday (date_procured, 1)
														ELSE date_procured
													END;


UPDATE operations_co.pro_order_tracking_dates
SET pro_order_tracking_dates.datetime_ready_to_pick = 	CASE
														WHEN dayofweek(datetime_procured) = 1 
														THEN operations_co.workday (datetime_procured, 1)
														WHEN dayofweek(datetime_procured) = 7 
														THEN operations_co.workday (datetime_procured, 1)
														ELSE datetime_procured
													END;


UPDATE operations_co.pro_order_tracking_dates
   INNER JOIN operations_co.calcworkdays
           ON  date( date_ready_to_pick ) = calcworkdays.date_first 
              AND workdays = 1
              AND isweekday = 1 
              AND isholiday = 1
SET 
   date_ready_to_pick = calcworkdays.date_last;

TRUNCATE operations_co.pro_max_date_ready_to_ship;

INSERT INTO operations_co.pro_max_date_ready_to_ship (date_ready, order_item_id) SELECT
	min(DATA) AS date_ready,
	pro_order_tracking_dates.item_id
FROM
	operations_co.pro_order_tracking_dates
INNER JOIN wmsprod_co.itens_venda ON pro_order_tracking_dates.item_id = itens_venda.item_id
INNER JOIN wmsprod_co.status_itens_venda ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
WHERE
	status_itens_venda. STATUS = "faturado"
GROUP BY
	pro_order_tracking_dates.item_id;


UPDATE operations_co.pro_order_tracking_dates
INNER JOIN operations_co.pro_max_date_ready_to_ship ON pro_order_tracking_dates.item_id = pro_max_date_ready_to_ship.order_item_id
SET pro_order_tracking_dates.date_ready_to_ship = date(
	pro_max_date_ready_to_ship.date_ready
),
 pro_order_tracking_dates.datetime_ready_to_ship = pro_max_date_ready_to_ship.date_ready;

UPDATE operations_co.pro_order_tracking_dates a
INNER JOIN wmsprod_co.itens_venda b
	ON a.item_id = b.item_id
INNER JOIN wmsprod_co.status_itens_venda d 
	ON b.itens_venda_id = d.itens_venda_id
SET
 a.date_shipped = (SELECT
												max(date(DATA))
											FROM
												wmsprod_co.itens_venda c,
												wmsprod_co.status_itens_venda e
											WHERE
												e. STATUS = "expedido"
											AND b.itens_venda_id = c.itens_venda_id
											AND e.itens_venda_id = d.itens_venda_id
										),
  a.date_time_shipped = (	SELECT
														max(DATA)
													FROM
														wmsprod_co.itens_venda c,
														wmsprod_co.status_itens_venda e
													WHERE
														e. STATUS = "expedido"
													AND b.itens_venda_id = c.itens_venda_id
													AND e.itens_venda_id = d.itens_venda_id
												);

TRUNCATE operations_co.pro_1st_attempt;


INSERT INTO operations_co.pro_1st_attempt (order_item_id, min_of_date) SELECT
	pro_order_tracking_dates.item_id,
	min(tms_status_delivery.date) AS min_of_date
FROM
	(
		operations_co.pro_order_tracking_dates
		INNER JOIN wmsprod_co.vw_itens_venda_entrega ON pro_order_tracking_dates.item_id = vw_itens_venda_entrega.item_id
	)
INNER JOIN wmsprod_co.tms_status_delivery ON vw_itens_venda_entrega.cod_rastreamento = tms_status_delivery.cod_rastreamento
GROUP BY
	pro_order_tracking_dates.item_id,
	tms_status_delivery.id_status
HAVING
	tms_status_delivery.id_status = 5;

UPDATE operations_co.pro_order_tracking_dates 
INNER JOIN operations_co.pro_1st_attempt ON pro_order_tracking_dates.item_id = pro_1st_attempt.order_item_id
SET pro_order_tracking_dates.date_1st_attempt = pro_1st_attempt.min_of_date;

UPDATE operations_co.pro_order_tracking_dates
INNER JOIN wmsprod_co.vw_itens_venda_entrega vw 
	ON pro_order_tracking_dates.item_id = vw.item_id
INNER JOIN wmsprod_co.tms_status_delivery del 
	ON vw.cod_rastreamento = del.cod_rastreamento
SET operations_co.pro_order_tracking_dates.date_1st_attempt = (
	SELECT
		max(tms.date)
	FROM
		wmsprod_co.tms_status_delivery tms,
		wmsprod_co.vw_itens_venda_entrega itens
	WHERE
		del.cod_rastreamento = tms.cod_rastreamento
	AND vw.item_id = itens.item_id
	AND tms.id_status = 4
)
WHERE
	pro_order_tracking_dates.date_1st_attempt IS NULL
OR pro_order_tracking_dates.date_1st_attempt < '2011-05-01';

UPDATE operations_co.pro_order_tracking_dates AS a
INNER JOIN wmsprod_co.vw_itens_venda_entrega AS b
ON a.item_id = b.item_id
INNER JOIN wmsprod_co.tms_status_delivery AS c
ON b.cod_rastreamento = c.cod_rastreamento
SET a.date_failed_delivery = c.date
WHERE c.id_status=10;


UPDATE operations_co.pro_order_tracking_dates as a
INNER JOIN bazayaco.tbl_bi_ops_ingreso_bodega_devolucion as b
ON a.item_id=b.item_id
SET 
a.date_returned=b.date_ingreso_bodega_devolucion;


UPDATE        operations_co.pro_order_tracking_dates 
   INNER JOIN operations_co.calcworkdays
           ON     date( date_exported )           = calcworkdays.date_first 
              AND workdays = supplier_leadtime
              AND isweekday = 1 
              AND isholiday = 0
SET 
   date_procured_promised = calcworkdays.date_last
WHERE
	pro_order_tracking_dates.date_exported IS NOT NULL;

UPDATE operations_co.pro_order_tracking_dates AS a
INNER JOIN operations_co.out_order_tracking_sample b ON a.item_id = b.item_id
SET 
a.wh_time=if((b.package_measure_new='Small' OR b.package_measure_new='Medium'),1,2);

UPDATE operations_co.pro_order_tracking_dates 
INNER JOIN operations_co.calcworkdays
ON date(date_procured_promised) = calcworkdays.date_first 
AND workdays = wh_time
AND isweekday = 1
AND isholiday = 0
SET 
	date_ready_to_ship_promised = calcworkdays.date_last
WHERE
	operations_co.pro_order_tracking_dates.date_procured_promised IS NOT NULL;


UPDATE operations_co.pro_order_tracking_dates AS a
INNER JOIN 
(SELECT sku_simple,max(date_update) as date_update FROM operations_co.tbl_bi_ops_stock_update GROUP BY sku_simple) as t
ON a.sku_simple=t.sku_simple
SET a.date_stock_updated = t.date_update;

UPDATE  operations_co.pro_order_tracking_dates 
   INNER JOIN calcworkdays
		ON     date( date_exported ) = calcworkdays.date_first 
			AND workdays = if((max_delivery_time is null or max_delivery_time =''  or max_delivery_time=0),7,max_delivery_time)
			AND isweekday = 1
			AND isholiday = 0
SET 
   date_delivered_promised = calcworkdays.date_last
WHERE
	pro_order_tracking_dates.date_exported IS NOT NULL;

DROP TEMPORARY TABLE IF EXISTS tmp_order_tracking_stockout_declared;
 
CREATE TEMPORARY TABLE tmp_order_tracking_stockout_declared (INDEX (itens_venda_id))
SELECT
                a.itens_venda_id,
                a.numero_order,
                a.item_id,
                b.date
FROM
                wmsprod_co.itens_venda a
INNER JOIN (  SELECT
								itens_venda_id,
								max(DATA) date
							FROM
								wmsprod_co.status_itens_venda
							WHERE STATUS = 'Quebrado'
							GROUP BY itens_venda_id) b
ON  a.itens_venda_id = b.itens_venda_id;
 
UPDATE operations_co.pro_order_tracking_dates d
INNER JOIN tmp_order_tracking_stockout_declared t
                ON d.item_id = t.item_id
SET d.date_declared_stockout = t.date;


UPDATE operations_co.pro_order_tracking_dates d
INNER JOIN (
	SELECT
		a.item_id,
		a.tempo_de_entrega,
		a.tempo_entrega_fornecedor,
		a.data_criacao,
		a.estimated_delivery_date,
		a.estimated_delivery_days,
		a.estimated_days_on_wh,
		b.days_back,
		b.date_created
	FROM
		wmsprod.itens_venda a
	JOIN wmsprod.item_backorder b ON a.itens_venda_id = b.itens_venda_id
) c ON d.item_id = c.item_id
SET d.date_declared_backorder = c.date_created;



UPDATE operations_co.pro_order_tracking_dates AS a
INNER JOIN
(SELECT  d.item_id,b.status,b.data FROM
(select itens_venda_id,status, max(data) as data
from
(SELECT itens_venda_id,status,data
FROM wmsprod_co.status_itens_venda
ORDER BY data desc) a
GROUP BY itens_venda_id) AS b
INNER JOIN wmsprod_co.itens_venda AS d
ON b.itens_venda_id=d.itens_venda_id) AS t1
ON a.item_id=t1.item_id
SET a.date_status_value_chain =t1.data,
status_value_chain=t1.status;

UPDATE operations_co.pro_order_tracking_dates AS a
SET a.date_status_value_chain=date_1st_attempt,
status_value_chain='Intentado'
WHERE status_wms='Expedido'
AND date_1st_attempt IS NOT NULL;

UPDATE operations_co.pro_order_tracking_dates AS a
SET a.date_status_value_chain=date_delivered,
status_value_chain='Entregado'
WHERE status_wms='Expedido'
AND date_delivered IS NOT NULL;

UPDATE operations_co.pro_order_tracking_dates AS a
INNER JOIN wmsprod_co.inverselogistics_devolucion AS b
ON a.item_id=b.item_id
SET a.date_status_value_chain=b.modified_at,
status_value_chain=b.status;

#workdays to create po
UPDATE operations_co.pro_order_tracking_dates
INNER JOIN operations_co.calcworkdays
	ON pro_order_tracking_dates.date_exported = calcworkdays.date_first
		AND pro_order_tracking_dates.date_po_created = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_po =	calcworkdays.workdays;

UPDATE operations_co.pro_order_tracking_dates
INNER JOIN operations_co.calcworkdays
	ON pro_order_tracking_dates.date_exported = calcworkdays.date_first
		AND curdate() = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_po =	calcworkdays.workdays
WHERE date_exported IS NULL;

#Workdays to confirm
UPDATE operations_co.pro_order_tracking_dates
INNER JOIN operations_co.calcworkdays
	ON pro_order_tracking_dates.date_po_created = calcworkdays.date_first
		AND pro_order_tracking_dates.date_po_confirmed = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_confirm =	calcworkdays.workdays;

UPDATE operations_co.pro_order_tracking_dates
INNER JOIN operations_co.calcworkdays
	ON pro_order_tracking_dates.date_po_created = calcworkdays.date_first
		AND curdate() = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_confirm =	calcworkdays.workdays
WHERE date_exported IS NULL;

#Workdays to send po
UPDATE operations_co.pro_order_tracking_dates
INNER JOIN operations_co.calcworkdays
	ON pro_order_tracking_dates.date_po_confirmed = calcworkdays.date_first
		AND pro_order_tracking_dates.date_po_issued = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_send_po =	calcworkdays.workdays;

UPDATE operations_co.pro_order_tracking_dates
INNER JOIN operations_co.calcworkdays
	ON pro_order_tracking_dates.date_po_confirmed = calcworkdays.date_first
		AND curdate() = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_send_po =	calcworkdays.workdays
WHERE date_exported IS NULL;

#Workdays to export
UPDATE operations_co.pro_order_tracking_dates
INNER JOIN operations_co.calcworkdays
	ON pro_order_tracking_dates.date_ordered = calcworkdays.date_first
		AND pro_order_tracking_dates.date_exported = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_export =	calcworkdays.workdays;

UPDATE operations_co.pro_order_tracking_dates
INNER JOIN operations_co.calcworkdays
	ON pro_order_tracking_dates.date_ordered = calcworkdays.date_first
		AND curdate() = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_export =	calcworkdays.workdays
WHERE date_exported IS NULL;

#Workdays to ready
UPDATE operations_co.pro_order_tracking_dates
INNER JOIN operations_co.calcworkdays
	ON pro_order_tracking_dates.date_ready_to_pick	= calcworkdays.date_first
		AND pro_order_tracking_dates.date_ready_to_ship 	= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_ready 	=	calcworkdays.workdays
;

UPDATE operations_co.pro_order_tracking_dates
INNER JOIN operations_co.calcworkdays
	ON pro_order_tracking_dates.date_ready_to_pick	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_ready 	=	calcworkdays.workdays
WHERE date_ready_to_ship IS NULL;

#Workdays to ship
UPDATE operations_co.pro_order_tracking_dates
INNER JOIN operations_co.calcworkdays
	ON pro_order_tracking_dates.date_ready_to_ship	= calcworkdays.date_first
		AND pro_order_tracking_dates.date_shipped 		= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_ship 		=	calcworkdays.workdays
;

UPDATE operations_co.pro_order_tracking_dates
INNER JOIN operations_co.calcworkdays
	ON pro_order_tracking_dates.date_ready_to_ship	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_ship 	=	calcworkdays.workdays
WHERE date_shipped IS NULL;

#workdays_to_1st_attempt
UPDATE operations_co.pro_order_tracking_dates
INNER JOIN operations_co.calcworkdays
	ON pro_order_tracking_dates.date_shipped	= calcworkdays.date_first
		AND pro_order_tracking_dates.date_1st_attempt 		= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_1st_attempt 		=	calcworkdays.workdays
;

UPDATE operations_co.pro_order_tracking_dates
INNER JOIN operations_co.calcworkdays
	ON pro_order_tracking_dates.date_shipped	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_1st_attempt 	=	calcworkdays.workdays
WHERE date_1st_attempt IS NULL;

#workdays_to_deliver
UPDATE operations_co.pro_order_tracking_dates
INNER JOIN operations_co.calcworkdays
	ON pro_order_tracking_dates.date_shipped	= calcworkdays.date_first
		AND pro_order_tracking_dates.date_delivered 		= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_deliver 		=	calcworkdays.workdays
;

UPDATE operations_co.pro_order_tracking_dates
INNER JOIN operations_co.calcworkdays
	ON pro_order_tracking_dates.date_shipped	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_deliver 	=	calcworkdays.workdays
WHERE date_delivered IS NULL;

#workdays to request recollection

DROP TEMPORARY TABLE IF EXISTS tmp_order_tracking_stockout_declared;
 
CREATE TEMPORARY TABLE tmp_order_tracking_stockout_declared (INDEX (itens_venda_id))
SELECT
                a.itens_venda_id,
                a.numero_order,
                a.item_id,
                b.date
FROM
                wmsprod_co.itens_venda a
INNER JOIN ( 	SELECT
								itens_venda_id,
								max(DATA) date
							FROM
								wmsprod_co.status_itens_venda
							WHERE STATUS = 'Quebrado'
							GROUP BY itens_venda_id) b
ON  a.itens_venda_id = b.itens_venda_id;
 
UPDATE operations_co.pro_order_tracking_dates d
INNER JOIN tmp_order_tracking_stockout_declared t
                ON d.item_id = t.item_id
SET d.date_declared_stockout = t.date;

#workdays to stockout declared
UPDATE operations_co.pro_order_tracking_dates
INNER JOIN operations_co.calcworkdays
 ON pro_order_tracking_dates.date_ordered = calcworkdays.date_first
  AND pro_order_tracking_dates.date_declared_stockout  = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_stockout_declared = calcworkdays.workdays
WHERE date_declared_stockout is not null ;

#workdays to create tracking code
UPDATE operations_co.pro_order_tracking_dates
INNER JOIN operations_co.calcworkdays
 ON date(pro_order_tracking_dates.date_po_confirmed) = calcworkdays.date_first
  AND date(pro_order_tracking_dates.date_sent_tracking_code)  = calcworkdays.date_last
SET pro_order_tracking_dates.workdays_to_create_tracking_code = calcworkdays.workdays
WHERE date_po_confirmed is not null AND  date_sent_tracking_code  is not null ;

#workdays_total_1st_attempt
UPDATE operations_co.pro_order_tracking_dates
INNER JOIN operations_co.calcworkdays
	ON pro_order_tracking_dates.date_exported	= calcworkdays.date_first
		AND pro_order_tracking_dates.date_1st_attempt 		= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_total_1st_attempt 		=	calcworkdays.workdays
WHERE date_shipped IS NOT NULL;

UPDATE operations_co.pro_order_tracking_dates
INNER JOIN operations_co.calcworkdays
	ON pro_order_tracking_dates.date_exported	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_total_1st_attempt 	=	calcworkdays.workdays
WHERE date_shipped IS NOT NULL
AND date_1st_attempt IS NULL;

#workdays_total_delivery
UPDATE operations_co.pro_order_tracking_dates
INNER JOIN operations_co.calcworkdays
	ON pro_order_tracking_dates.date_exported	= calcworkdays.date_first
		AND pro_order_tracking_dates.date_delivered 		= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_total_delivery 		=	calcworkdays.workdays
WHERE date_shipped IS NOT NULL;

UPDATE operations_co.pro_order_tracking_dates
INNER JOIN operations_co.calcworkdays
	ON pro_order_tracking_dates.date_exported	= calcworkdays.date_first
		AND curdate() 	= calcworkdays.date_last
SET pro_order_tracking_dates.workdays_total_delivery 	=	calcworkdays.workdays
WHERE date_shipped IS NOT NULL
AND date_1st_attempt IS NULL;

UPDATE operations_co.pro_order_tracking_dates
SET 
 week_ordered = operations_co.week_iso (date_ordered),
 week_procured_promised = operations_co.week_iso (date_procured_promised),
 week_r2s_promised = operations_co.week_iso (date_ready_to_ship_promised),
 week_delivered_promised = operations_co.week_iso (date_delivered_promised),
 week_shipped = operations_co.week_iso (date_shipped),
 month_procured_promised = date_format(date_procured_promised,"%Y-%m"),
 month_delivered_promised = date_format(date_delivered_promised,"%Y-%m"),
 month_r2s_promised = date_format(date_ready_to_ship_promised,"%Y-%m");

UPDATE        operations_co.pro_order_tracking_dates
   INNER JOIN operations_co.calcworkdays
           ON     pro_order_tracking_dates.date_ready_to_pick = calcworkdays.date_first
              AND calcworkdays.date_last = curdate() 
SET 
   r2s_workday_0  = IF( workdays_to_ready  = 0 AND workdays >= 0, 1 , r2s_workday_0 ),
   r2s_workday_1  = IF( workdays_to_ready <= 1 AND workdays >= 1, 1 , r2s_workday_1 ),
   r2s_workday_2  = IF( workdays_to_ready <= 2 AND workdays >= 2, 1 , r2s_workday_2 ),
   r2s_workday_3  = IF( workdays_to_ready <= 3 AND workdays >= 3, 1 , r2s_workday_3 ),
   r2s_workday_4  = IF( workdays_to_ready <= 4 AND workdays >= 4, 1 , r2s_workday_4 ),
   r2s_workday_5  = IF( workdays_to_ready <= 5 AND workdays >= 5, 1 , r2s_workday_5 ),
   r2s_workday_6  = IF( workdays_to_ready <= 6 AND workdays >= 6, 1 , r2s_workday_6 ), 
   r2s_workday_7  = IF( workdays_to_ready <= 7 AND workdays >= 7, 1 , r2s_workday_7 ), 
   r2s_workday_8  = IF( workdays_to_ready <= 8 AND workdays >= 8, 1 , r2s_workday_8 ),  
   r2s_workday_9  = IF( workdays_to_ready <= 9 AND workdays >= 9, 1 , r2s_workday_9 ),  
   r2s_workday_10 = IF( workdays_to_ready  >10 AND workdays >=10, 1 , r2s_workday_10);

UPDATE operations_co.pro_order_tracking_dates
SET check_dates =
								 IF (date_ordered > date_exported,"Date ordered > date exported",
								 IF (date_exported > date_procured,"Date exported > Date procured",
								 IF (date_procured > date_ready_to_ship,"Date procured > Date ready to ship",
								 IF (date_ready_to_ship > date_shipped,"Date ready to ship > Date shipped",
								 IF (date_shipped > date_1st_attempt,"Date shipped > Date 1st attempt",
								 IF (date_1st_attempt > date_delivered,"Date 1st atteempt> Date delivered",
																 "Correct"))))));

UPDATE operations_co.pro_order_tracking_dates
SET
check_date_exported = CASE
												WHEN date_exported IS NULL
												THEN (CASE
																WHEN date_procured IS NULL
																AND date_ready_to_ship IS NULL
																AND date_shipped IS NULL
																AND date_1st_attempt IS NULL
																AND date_delivered IS NULL
																THEN 0
																ELSE 1
															END )
												ELSE (CASE
															 WHEN date_exported > date_procured
															 THEN 1
															 ELSE 0
															END)
												END,
check_date_ordered = CASE
											WHEN date_ordered IS NULL
											THEN (  CASE
															 WHEN date_exported IS NULL
															 AND date_procured IS NULL
															 AND date_ready_to_ship IS NULL
															 AND date_shipped IS NULL
															 AND date_1st_attempt IS NULL
															 AND date_delivered IS NULL
															 THEN 0
															 ELSE 1
															 END )
											ELSE (  CASE
															 WHEN date_ordered > date_exported
															 THEN 1
															 ELSE 0
															 END )
															END,
check_date_procured = CASE
												WHEN date_procured IS NULL
												THEN (  CASE
																 WHEN date_ready_to_ship IS NULL
																 AND date_shipped IS NULL
																 AND date_1st_attempt IS NULL
																 AND date_delivered IS NULL
																 THEN 0
																 ELSE 1
																 END )
												ELSE (  CASE
																 WHEN date_procured > date_ready_to_ship
																 THEN 1
																 ELSE 0
																 END )
																END,
check_date_ready_to_ship = CASE
														 WHEN date_ready_to_ship IS NULL
														 THEN (  CASE
																			 WHEN date_shipped IS NULL
																			 AND date_1st_attempt IS NULL
																			 AND date_delivered IS NULL
																			 THEN 0
																			 ELSE 1
																			 END )
														 ELSE (    CASE
																				WHEN date_ready_to_ship > date_shipped
																				THEN 1
																				ELSE 0
																				END)
														 END,
check_date_shipped = CASE
											WHEN date_shipped IS NULL
											THEN (  CASE
																											WHEN date_1st_attempt IS NULL
																											AND date_delivered IS NULL
																											THEN 0
																											ELSE 1
																											END )
											ELSE (    CASE
																											WHEN date_shipped > date_1st_attempt
																											THEN 1
																											ELSE 0
																											END)
											END,
check_date_1st_attempt = CASE
													WHEN date_1st_attempt IS NULL
													THEN (  CASE
																												 WHEN date_delivered IS NULL
																												 THEN 0
																												 ELSE 1
																												 END )
													ELSE (    CASE
																												 WHEN date_1st_attempt > date_delivered
																												 THEN 1
																												 ELSE 0
																												 END )
													END;

UPDATE operations_co.pro_order_tracking_dates
SET
 check_date_stockout_over_bo = IF(min_delivery_time>=max_delivery_time,1,0);


UPDATE operations_co.pro_order_tracking_dates
SET 
pro_order_tracking_dates.time_to_po = If(datetime_exported is Null,null,If(datetime_po_created is Null, TIMEDIFF(CURDATE(),datetime_exported), TIMEDIFF(datetime_po_created,datetime_exported))),
pro_order_tracking_dates.time_to_confirm = If(datetime_po_issued is Null,null,If(datetime_po_confirmed is Null, TIMEDIFF(CURDATE(),datetime_po_issued), TIMEDIFF(datetime_po_confirmed,datetime_po_issued))),
pro_order_tracking_dates.time_to_send_po = If(datetime_po_created is Null,null,If(datetime_po_issued is Null, TIMEDIFF(CURDATE(),datetime_po_created), TIMEDIFF(datetime_po_issued,datetime_po_created))),
pro_order_tracking_dates.time_to_procure = If(datetime_exported is Null,null,If(datetime_procured is Null, TIMEDIFF(CURDATE(),datetime_exported), TIMEDIFF(datetime_procured,datetime_exported))),
pro_order_tracking_dates.time_to_export = If(datetime_ordered is Null,null,If(datetime_exported is Null, TIMEDIFF(CURDATE(),datetime_ordered), TIMEDIFF(datetime_exported,datetime_ordered))),
pro_order_tracking_dates.time_to_ready = If(datetime_ready_to_pick is Null,null,If(datetime_ready_to_ship is Null, TIMEDIFF(CURDATE(),datetime_ready_to_pick), TIMEDIFF(datetime_ready_to_ship,datetime_ready_to_pick))),
pro_order_tracking_dates.time_to_ship = If(datetime_ready_to_ship is Null,null,If(date_time_shipped is Null, TIMEDIFF(CURDATE(),datetime_ready_to_ship), TIMEDIFF(date_time_shipped,datetime_ready_to_ship))),
pro_order_tracking_dates.time_to_1st_attempt = If(date_time_shipped is Null,null,If(date_1st_attempt is Null, TIMEDIFF(CURDATE(),date_time_shipped), TIMEDIFF(date_1st_attempt,date_time_shipped))),
pro_order_tracking_dates.time_to_deliver = If(date_time_shipped is Null,null,If(date_delivered is Null, TIMEDIFF(CURDATE(),date_time_shipped), TIMEDIFF(date_delivered,date_time_shipped))),
pro_order_tracking_dates.time_to_request_recollection = If(date_time_shipped is Null,null,If(date_delivered is Null, TIMEDIFF(CURDATE(),date_time_shipped), TIMEDIFF(date_delivered,date_time_shipped))),
pro_order_tracking_dates.time_to_stockout_declared = If(datetime_ordered is Null,null,If(date_declared_stockout is Null, TIMEDIFF(CURDATE(),datetime_ordered), TIMEDIFF(date_declared_stockout,datetime_ordered))),
pro_order_tracking_dates.time_to_createPO = If(datetime_exported is Null,null,If(datetime_po_created is Null, TIMEDIFF(CURDATE(),datetime_exported), TIMEDIFF(datetime_po_created,datetime_exported))),
pro_order_tracking_dates.time_to_create_tracking_code = If(datetime_po_confirmed is Null,null,If(datetime_sent_tracking_code is Null, TIMEDIFF(CURDATE(),datetime_po_confirmed), TIMEDIFF(datetime_sent_tracking_code,datetime_po_confirmed))),
pro_order_tracking_dates.time_total_1st_attempt = If(datetime_exported is Null,null,If(date_1st_attempt is Null, TIMEDIFF(CURDATE(),datetime_exported), TIMEDIFF(date_1st_attempt,datetime_exported))),
pro_order_tracking_dates.time_total_delivery = If(datetime_exported is Null,null,If(date_delivered is Null, TIMEDIFF(CURDATE(),datetime_exported), TIMEDIFF(date_delivered,datetime_exported)));

UPDATE operations_co.pro_order_tracking_dates
SET 
#ontime_to_po = If(date(date_po_created)<=date_po_created_promised,1,0),
#on_time_to_confirm = If(date(date_po_confirmed)<=date_po_confirmed_promised,1,0),
#on_time_to_send_po = If(date(date_po_issued)<=date_po_issued_promised,1,0),
on_time_to_procure = If(date(date_procured)<=date_procured_promised,1,0),
#ontime_to_export = If(date(date_exported)<=date_exported_promised,1,0),
on_time_to_ready = If(date_ready_to_ship is not null,(if(date_ready_to_ship<=date_ready_to_ship_promised,1,0)),(if(date_ready_to_ship_promised<=curdate(),1,0))),
on_time_to_ship = If(date_shipped is not null,(if(date_shipped<=date_ready_to_ship_promised,1,0)),(if(date_ready_to_ship_promised<=curdate(),1,0))),
on_time_to_1st_attempt = If(date_1st_attempt is not null,(if(date_1st_attempt <=date_delivered_promised,1,0)),(if(date_delivered_promised<=curdate(),1,0))),
on_time_to_deliver = If(date_delivered is not null,(if(date_delivered<=date_delivered_promised,1,0)),(if(date_delivered_promised<=curdate(),1,0))),
#on_time_to_request_recollection = If(date_first_attempt<=promised_delivery_date,1,0),
#on_time_to_stockout_declared = If(date_shipped is not null,(if(date_shipped<=date_ready_to_ship_promised,1,0)),(if(date_ready_to_ship_promised<=curdate(),1,0))),
#on_time_to_create_po = If(date_shipped is not null,(if(date_shipped<=date_ready_to_ship_promised,1,0)),(if(date_ready_to_ship_promised<=curdate(),1,0))),
#on_time_to_create_tracking_code = If(date_first_attempt<=promised_delivery_date,1,0),
on_time_total_1st_attempt = If(date_delivered<=date_delivered_promised,1,0),
on_time_total_delivery = If(date_delivered<=date_delivered_promised,1,0);


UPDATE operations_co.pro_order_tracking_dates
SET 
pro_order_tracking_dates.days_to_po = If(date_exported is Null,null,If(date_po_created is Null, DATEDIFF(CURDATE(),date_exported), DATEDIFF(date_po_created,date_exported))),
pro_order_tracking_dates.days_to_confirm = If(date_po_issued is Null,null,If(date_po_confirmed is Null, DATEDIFF(CURDATE(),date_po_issued), DATEDIFF(date_po_confirmed,date_po_issued))),
pro_order_tracking_dates.days_to_send_po = If(date_po_created is Null,null,If(date_po_issued is Null, DATEDIFF(CURDATE(),date_po_created), DATEDIFF(date_po_issued,date_po_created))),
pro_order_tracking_dates.days_to_procure = If(date_exported is Null,null,If(date_procured is Null, DATEDIFF(CURDATE(),date_exported), DATEDIFF(date_procured,date_exported))),
pro_order_tracking_dates.days_to_export = If(date_ordered is Null,null,If(date_exported is Null, DATEDIFF(CURDATE(),date_ordered), DATEDIFF(date_exported,date_ordered))),
pro_order_tracking_dates.days_to_ready = If(date_ready_to_pick is Null,null,If(date_ready_to_ship is Null, DATEDIFF(CURDATE(),date_ready_to_pick), DATEDIFF(date_ready_to_ship,date_ready_to_pick))),
pro_order_tracking_dates.days_to_ship = If(date_ready_to_ship is Null,null,If(date_shipped is Null, DATEDIFF(CURDATE(),date_ready_to_ship), DATEDIFF(date_shipped,date_ready_to_ship))),
pro_order_tracking_dates.days_to_1st_attempt = If(date_shipped is Null,null,If(date_1st_attempt is Null, DATEDIFF(CURDATE(),date_shipped), DATEDIFF(date_1st_attempt,date_shipped))),
pro_order_tracking_dates.days_to_deliver = If(date_shipped is Null,null,If(date_delivered is Null, DATEDIFF(CURDATE(),date_shipped), DATEDIFF(date_delivered,date_shipped))),
pro_order_tracking_dates.days_to_request_recollection = If(date_shipped is Null,null,If(date_delivered is Null, DATEDIFF(CURDATE(),date_shipped), DATEDIFF(date_delivered,date_shipped))),
pro_order_tracking_dates.days_to_stockout_declared = If(date_ordered is Null,null,If(date_declared_stockout is Null, DATEDIFF(CURDATE(),date_ordered), DATEDIFF(date_declared_stockout,date_ordered))),
pro_order_tracking_dates.days_to_createPO = If(date_exported is Null,null,If(date_po_created is Null, DATEDIFF(CURDATE(),date_exported), DATEDIFF(date_po_created,date_exported))),
pro_order_tracking_dates.days_to_create_tracking_code = If(date_po_confirmed is Null,null,If(date_sent_tracking_code is Null, DATEDIFF(CURDATE(),date_po_confirmed), DATEDIFF(date_sent_tracking_code,date_po_confirmed))),
pro_order_tracking_dates.days_total_1st_attempt = If(date_exported is Null,null,If(date_1st_attempt is Null, DATEDIFF(CURDATE(),date_exported), DATEDIFF(date_1st_attempt,date_exported))),
pro_order_tracking_dates.days_total_delivery = If(date_exported is Null,null,If(date_delivered is Null, DATEDIFF(CURDATE(),date_exported), DATEDIFF(date_delivered,date_exported)));


UPDATE operations_co.pro_order_tracking_dates
SET 
pro_order_tracking_dates.shipped_same_day_as_procured = If (date_procured = date_shipped,1,0), 
pro_order_tracking_dates.shipped_same_day_as_order = If (date_ordered = date_shipped ,1,0);

UPDATE operations_co.pro_order_tracking_dates
SET 
24hr_shipments_Cust_persp = If(date_ordered is Null,null,If(date_shipped is Null,if( Datediff(CURDATE(),date_ordered)= 1,1,0), if( Datediff(date_shipped,date_ordered)=1,1,0))),
24hr_shipmnets_WH_persp = If(date_ready_to_pick is Null,null,If(date_shipped is Null, if( Datediff(CURDATE(),date_ready_to_pick)= 1,1,0), if( Datediff(date_shipped,date_ready_to_pick)=1,1,0)));

UPDATE operations_co.out_order_tracking_sample a
INNER JOIN operations_co.pro_order_tracking_dates AS b
ON a.item_id = b.item_id
SET
a.sku_simple = b.sku_simple,
a.status_wms = b.status_wms,
a.fulfillment_type_bob = b.fulfillment_type_bob,
a.fulfillment_type_real = b.fulfillment_type_real,
a.supplier_leadtime = b.supplier_leadtime,
a.supplier_id = b.supplier_id,
a.min_delivery_time = b.min_delivery_time,
a.max_delivery_time = b.max_delivery_time,
a.wh_time = b.wh_time,
a.date_ordered = b.date_ordered,
a.date_exported = b.date_exported,
a.date_backorder = b.date_backorder,
a.date_backorder_tratada = b.date_backorder_tratada,
a.date_backorder_accepted = b.date_backorder_accepted,
a.date_po_created = b.date_po_created,
a.date_po_updated = b.date_po_updated,
a.date_po_issued = b.date_po_issued,
a.date_po_confirmed = b.date_po_confirmed,
a.date_sent_tracking_code = b.date_sent_tracking_code,
a.date_procured = b.date_procured,
a.date_assigned_to_stock = b.date_assigned_to_stock,
a.date_ready_to_pick = b.date_ready_to_pick,
a.date_ready_to_ship = b.date_ready_to_ship,
a.date_shipped = b.date_shipped,
a.date_1st_attempt = b.date_1st_attempt,
a.date_delivered = b.date_delivered,
a.date_failed_delivery = b.date_failed_delivery,
a.date_returned = b.date_returned,
a.date_procured_promised = b.date_procured_promised,
a.date_ready_to_ship_promised = b.date_ready_to_ship_promised,
a.date_delivered_promised = b.date_delivered_promised,
a.status_value_chain = b.status_value_chain,
a.date_status_value_chain = b.date_status_value_chain,
a.date_stock_updated = b.date_stock_updated,
a.date_declared_stockout = b.date_declared_stockout,
a.date_declared_backorder = b.date_declared_backorder,
a.datetime_ordered = b.datetime_ordered,
a.datetime_exported = b.datetime_exported,
a.datetime_po_created = b.datetime_po_created,
a.datetime_po_updated = b.datetime_po_updated,
a.datetime_po_issued = b.datetime_po_issued,
a.datetime_po_confirmed = b.datetime_po_confirmed,
a.datetime_sent_tracking_code = b.datetime_sent_tracking_code,
a.datetime_procured = b.datetime_procured,
a.datetime_assigned_to_stock = b.datetime_assigned_to_stock,
a.datetime_ready_to_pick = b.datetime_ready_to_pick,
a.datetime_ready_to_ship = b.datetime_ready_to_ship,
a.date_time_shipped = b.date_time_shipped,
a.week_ordered = b.week_ordered,
a.week_procured_promised = b.week_procured_promised,
a.week_delivered_promised = b.week_delivered_promised,
a.week_r2s_promised = b.week_r2s_promised,
a.week_shipped = b.week_shipped,
a.month_procured_promised = b.month_procured_promised,
a.month_delivered_promised = b.month_delivered_promised,
a.month_r2s_promised = b.month_r2s_promised,
a.check_dates = b.check_dates,
a.check_date_exported = b.check_date_exported,
a.check_date_ordered = b.check_date_ordered,
a.check_date_procured = b.check_date_procured,
a.check_date_ready_to_ship = b.check_date_ready_to_ship,
a.check_date_shipped = b.check_date_shipped,
a.check_date_1st_attempt = b.check_date_1st_attempt,
a.check_date_stockout_over_bo = b.check_date_stockout_over_bo,
a.check_min_max = b.check_min_max,
a.workdays_to_po = b.workdays_to_po,
a.workdays_to_confirm = b.workdays_to_confirm,
a.workdays_to_send_po = b.workdays_to_send_po,
a.workdays_to_procure = b.workdays_to_procure,
a.workdays_to_export = b.workdays_to_export,
a.workdays_to_ready = b.workdays_to_ready,
a.workdays_to_ship = b.workdays_to_ship,
a.workdays_to_1st_attempt = b.workdays_to_1st_attempt,
a.workdays_to_deliver = b.workdays_to_deliver,
a.workdays_to_request_recollection = b.workdays_to_request_recollection,
a.workdays_to_stockout_declared = b.workdays_to_stockout_declared,
a.workdays_to_createPO = b.workdays_to_createPO,
a.workdays_to_create_tracking_code = b.workdays_to_create_tracking_code,
a.workdays_total_1st_attempt = b.workdays_total_1st_attempt,
a.workdays_total_delivery = b.workdays_total_delivery,
a.is_backorder = b.is_backorder,
a.workdays_to_backlog = b.workdays_to_backlog,
a.days_to_po = b.days_to_po,
a.days_to_confirm = b.days_to_confirm,
a.days_to_send_po = b.days_to_send_po, 
a.days_to_procure = b.days_to_procure,
a.days_to_export = b.days_to_export, 
a.days_to_ready = b.days_to_ready, 
a.days_to_ship = b.days_to_ship, 
a.days_to_1st_attempt = b.days_to_1st_attempt,
a.days_to_deliver = b.days_to_deliver,
a.days_to_request_recollection = b.days_to_request_recollection,
a.days_to_stockout_declared = b.days_to_stockout_declared,
a.days_to_createPO = b.days_to_createPO, 
a.days_to_create_tracking_code = b.days_to_create_tracking_code,
a.days_total_1st_attempt = b.days_total_1st_attempt,
a.days_total_delivery = b.days_total_delivery,
#a.days_to_backlog = b.days_to_backlog, 
a.days_since_stock_updated = b.days_since_stock_updated, 
a.time_to_po = b.time_to_po,
a.time_to_confirm = b.time_to_confirm, 
a.time_to_send_po = b.time_to_send_po,
a.time_to_procure = b.time_to_procure, 
a.time_to_export = b.time_to_export,
a.time_to_ready = b.time_to_ready, 
a.time_to_ship = b.time_to_ship,
a.time_to_1st_attempt = b.time_to_1st_attempt, 
a.time_to_deliver = b.time_to_deliver,
a.time_to_request_recollection = b.time_to_request_recollection, 
a.time_to_stockout_declared = b.time_to_stockout_declared,
a.time_to_createPO = b.time_to_createPO,
a.time_to_create_tracking_code = b.time_to_create_tracking_code, 
a.time_total_1st_attempt = b.time_total_1st_attempt, 
a.time_total_delivery = b.time_total_delivery,
#a.time_to_backlog = b.time_to_backlog, 
a.on_time_to_po = b.on_time_to_po,
a.on_time_to_confirm = b.on_time_to_confirm,
a.on_time_to_send_po = b.on_time_to_send_po, 
a.on_time_to_procure = b.on_time_to_procure, 
a.on_time_to_export = b.on_time_to_export, 
a.on_time_to_ready = b.on_time_to_ready, 
a.on_time_to_ship = b.on_time_to_ship, 
a.on_time_to_1st_attempt = b.on_time_to_1st_attempt,
a.on_time_to_deliver = b.on_time_to_deliver, 
a.on_time_to_request_recollection = b.on_time_to_request_recollection,
a.on_time_to_stockout_declared = b.on_time_to_stockout_declared, 
a.on_time_to_create_po = b.on_time_to_create_po, 
a.on_time_to_create_tracking_code = b.on_time_to_create_tracking_code, 
a.on_time_total_1st_attempt = b.on_time_total_1st_attempt, 
a.on_time_total_delivery = b.on_time_total_delivery, 
a.r2s_workday_0 = b.r2s_workday_0,
a.r2s_workday_1 = b.r2s_workday_1, 
a.r2s_workday_2 = b.r2s_workday_2, 
a.r2s_workday_3 = b.r2s_workday_3, 
a.r2s_workday_4 = b.r2s_workday_4, 
a.r2s_workday_5 = b.r2s_workday_5,
a.r2s_workday_6 = b.r2s_workday_6, 
a.r2s_workday_7 = b.r2s_workday_7, 
a.r2s_workday_8 = b.r2s_workday_8, 
a.r2s_workday_9 = b.r2s_workday_9, 
a.r2s_workday_10 = b.r2s_workday_10,
a.shipped_same_day_as_procured = b.shipped_same_day_as_procured, 
a.shipped_same_day_as_order = b.shipped_same_day_as_order, 
a.24hr_shipments_Cust_persp = b.24hr_shipments_Cust_persp,
a.24hr_shipmnets_WH_persp = b.24hr_shipmnets_WH_persp;

########## Queries Dates

SELECT  'vw_itens_venda_entrega',now();
UPDATE operations_co.out_order_tracking_sample AS a
INNER JOIN wmsprod_co.vw_itens_venda_entrega AS b
ON a.item_id = b.item_id
SET a.wms_tracking_code = b.cod_rastreamento;

SELECT  'CALL bi_ops_tracking_code',now();
CALL bi_ops_tracking_code;
#2.
SELECT  'Update tms_tracks',now();
UPDATE operations_co.out_order_tracking_sample
INNER JOIN operations_co.tbl_bi_ops_guiasbyitem2
	ON operations_co.out_order_tracking_sample.item_id = operations_co.tbl_bi_ops_guiasbyitem2.item_id
SET operations_co.out_order_tracking_sample.shipping_carrier_tracking_code = operations_co.tbl_bi_ops_guiasbyitem2.total_guias;

#### Insert para A_Master
INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia_Project', 
  'out_order_tracking',
  'shipping_carrier_tracking_code',
  NOW(),
  MAX(date_ordered),
  count(*),
  count(*)
FROM
operations_co.out_order_tracking_sample
;

SELECT  'Update days_since_stock_updated',now();
UPDATE operations_co.out_order_tracking_sample AS a
SET a.days_since_stock_updated =
IF(a.date_stock_updated IS NULL,NULL,TIMESTAMPDIFF(DAY, date(a.date_stock_updated), CURDATE()));


SELECT  'Update sales_order_item_status',now();
UPDATE operations_co.out_order_tracking_sample AS a
INNER JOIN bob_live_co.sales_order_item AS b
	ON a.item_id = b.id_sales_order_item
INNER JOIN bob_live_co.sales_order_item_status AS c
	ON b.fk_sales_order_item_status = c.id_sales_order_item_status
SET a.status_bob = c. NAME;

UPDATE operations_co.out_order_tracking_sample
INNER JOIN operations_co.status_match_bob_wms 
	ON out_order_tracking_sample.status_wms = status_match_bob_wms.status_wms
	AND out_order_tracking_sample.status_bob = status_match_bob_wms.status_bob
SET 
	out_order_tracking_sample.status_match_bob_wms = status_match_bob_wms.correct;

SELECT  'Update catalog_shipment_type',now();
UPDATE operations_co.out_order_tracking_sample AS a
INNER JOIN bob_live_co.sales_order_item AS b
	ON a.item_id = b.id_sales_order_item
INNER JOIN bob_live_co.catalog_shipment_type AS c
	ON b.fk_catalog_shipment_type = c.id_catalog_shipment_type
SET a.fulfillment_type_bob = c.NAME;

SELECT  'Calculo wh_time',now();
UPDATE operations_co.out_order_tracking_sample AS a
SET 
a.wh_time=if((a.package_measure_new='Small' OR a.package_measure_new='Medium'),1,2);

/*
version para MX
UPDATE operations_co.out_order_tracking_sample
INNER JOIN wmsprod_co.itens_venda ON out_order_tracking_sample.item_id = itens_venda.item_id
SET out_order_tracking_sample.wh_time = tempo_armazem
WHERE
tempo_armazem > 1;
*/

UPDATE operations_co.out_order_tracking_sample
SET
is_stockout = If(status_wms  IN(	'Quebrado',
																	'quebra tratada'
																	'backorder_tratada'),1,0),
is_backorder = if(date_backorder is not null,1,0),
is_ready_to_pick = If(date_ready_to_pick is not null,1,0),
is_ready_to_ship = If(date_ready_to_ship is not null,1,0),
is_procured = If(date_procured is not null,1,0),
is_shipped = If(date_shipped is not null,1,0),
is_first_attempt = If(date_1st_attempt is not null,1,0),
is_delivered = If(date_delivered is not null,1,0),
is_cancelled = If(status_wms in ('Cancelado','quebra tratada','Quebrado') is not null,1,0),
backlog_procurement = If((date_procured is null AND status_wms in ('Aguardando estoque','Analisando quebra') AND delay_to_procure=1 ),1,0),
backlog_delivery = If((status_wms='Expedido' AND delay_total_delivery = 1 AND date_delivered is null),1,0),
still_to_procure = if(date_procured is null AND	status_wms not in ("quebrado", "quebra tratada") AND fulfillment_type_real = 'Crossdocking',1,0);

UPDATE operations_co.out_order_tracking_sample a
INNER JOIN bob_live_co.catalog_config b
	ON a.sku_config = b.sku
SET 
	a.is_presale = IF(b. NAME LIKE "%preventa%",1,0),
	a.is_gift_card = IF(a.supplier_name like '%E-Gift Card%',1,0)
;

#Workdays to procure
UPDATE operations_co.out_order_tracking_sample
INNER JOIN operations_co.calcworkdays
	ON out_order_tracking_sample.date_exported 			= calcworkdays.date_first
		AND out_order_tracking_sample.date_procured 	= calcworkdays.date_last
SET out_order_tracking_sample.workdays_to_procure 	=	calcworkdays.workdays
WHERE is_stockout = 0
AND fulfillment_type_real not like 'Dropshipping';

UPDATE operations_co.out_order_tracking_sample
INNER JOIN operations_co.calcworkdays
	ON out_order_tracking_sample.date_exported 			= calcworkdays.date_first
		AND curdate() 																= calcworkdays.date_last
SET out_order_tracking_sample.workdays_to_procure =	calcworkdays.workdays
WHERE is_stockout = 0
AND date_procured IS NULL
AND fulfillment_type_real not like 'Dropshipping';

UPDATE operations_co.pro_order_tracking_dates a
INNER JOIN operations_co.out_order_tracking_sample b
	ON a.item_id = b.item_id
SET a.workdays_to_procure = b.workdays_to_procure;

UPDATE operations_co.out_order_tracking_sample AS a
INNER JOIN bob_live_co.sales_order_item_status_history AS b
ON a.item_id=b.fk_sales_order_item
SET
a.stockout_order_nr = 
 (SUBSTR(b.note,(SELECT INSTR(b.note, '200')),9))
WHERE a.is_stockout=1
AND b.fk_sales_order_item_status=9;

UPDATE operations_co.out_order_tracking_sample
SET 
	delay_to_procure = CASE
													WHEN date_procured IS NULL 
													THEN(	CASE
																	WHEN curdate() > date_procured_promised 
																	THEN 1
																	ELSE 0
																END)
													ELSE(	CASE
																	WHEN date_procured > date_procured_promised 
																	THEN 1
																	ELSE 0
																END)
												END,
	delay_to_ready = 	CASE
											WHEN workdays_to_ready > 1 
											THEN 1
											ELSE 0
										END,
 delay_to_ship = 	CASE
										WHEN workdays_to_ship > 1 
										THEN 1
										ELSE 0
									END,
 delay_to_1st_attempt = CASE
													WHEN workdays_to_1st_attempt > 2 
													THEN 1
													ELSE 0
												END,
 delay_to_delivery = CASE
											WHEN workdays_to_deliver > 2 
											THEN 1
											ELSE 0
										END,
 delay_total_1st_attempt = CASE
														WHEN date_1st_attempt IS NULL 
														THEN	(	CASE
																			WHEN curdate() > date_delivered_promised 
																			THEN 1
																			ELSE 0
																		END)
														ELSE	( CASE
																			WHEN date_1st_attempt > date_delivered_promised 
																			THEN 1
																			ELSE 0
																		END)
														END,
														 delay_total_delivery = CASE
														WHEN date_delivered IS NULL 
														THEN	(	CASE
																			WHEN curdate() > date_delivered_promised 
																			THEN 1
																			ELSE 0
																		END)
														ELSE	(	CASE
																			WHEN date_delivered > date_delivered_promised 
																			THEN 1
																			ELSE 0
																		END)
														END,
 workdays_delay_to_procure = CASE
															WHEN workdays_to_procure - supplier_leadtime < 0 
															THEN 0
															ELSE workdays_to_procure - supplier_leadtime
														END,
 workdays_delay_to_ready = 	CASE
															WHEN workdays_to_ready - 1 < 0 
															THEN 0
															ELSE workdays_to_ready - 1
														END,
 workdays_delay_to_ship = CASE
														WHEN workdays_to_ship - 1 < 0 
														THEN 0
														ELSE workdays_to_ship - 1
													END,
 workdays_delay_to_1st_attempt = 	CASE
																		WHEN workdays_to_1st_attempt - 3 < 0 
																		THEN 0
																		ELSE workdays_to_1st_attempt - 3
																	END,
 workdays_delay_to_delivery = 	CASE
																WHEN workdays_to_deliver - 3 < 0 
																THEN 0
																ELSE workdays_to_deliver - 3
															END,
 on_time_to_procure = CASE
												WHEN date_procured <= date_procured_promised 
												THEN 1
												ELSE 0
											END,
 on_time_total_1st_attempt = 	CASE
																WHEN date_1st_attempt <= date_delivered_promised 
																THEN 1
																ELSE 0
															END,
 on_time_total_delivery = CASE
														WHEN date_delivered <= date_delivered_promised 
														THEN 1
														WHEN date_delivered IS NULL 
														THEN 0
														ELSE 0
													END;




UPDATE operations_co.out_order_tracking_sample AS a
INNER JOIN development_co_project.A_Master AS b
ON a.stockout_order_nr=b.OrderNum
SET
a.stockout_recovered = 1
WHERE (stockout_order_nr <> '')
and status_wms in ('Quebrado','quebra tratada')
AND b.OrderAfterCan=1;

UPDATE operations_co.out_order_tracking_sample AS a
SET
a.stockout_real = 1
WHERE a.is_stockout=1
AND a.stockout_recovered is null;

UPDATE operations_co.out_order_tracking_sample AS a
INNER JOIN tbl_orders_reo_rep_inv AS b
ON a.item_id=b.item_original
SET
a.stockout_recovered_voucher = b.coupon_code,
a.stockout_item_id = b.item_nueva
WHERE b.coupon_code like 'REO%';


# Revisar llevar a rutina de Inverse Logistics
SELECT  'inverselogistics_agendamiento',now();
UPDATE operations_co.out_order_tracking_sample AS a
INNER JOIN wmsprod_co.inverselogistics_agendamiento AS b
	ON a.item_id=b.item_id
SET a.shipping_carrier_tracking_code_inverse = b.carrier_tracking_code;


/*
No aplica para CO - por que en OMS no hay dropshipping - Para los otros paises si.
UPDATE operations_co.out_order_tracking_sample 
INNER JOIN procurement_live_mx.wms_imported_dropship_orders
	ON out_order_tracking_sample.item_id = wms_imported_dropship_orders.fk_sales_order_item
INNER JOIN procurement_live_mx.procurement_order_item_dropshipping 
	ON wms_imported_dropship_orders.id_wms_imported_dropship_order = procurement_order_item_dropshipping.fk_wms_imported_dropship_order
INNER JOIN procurement_live_mx.procurement_order_item
	ON procurement_order_item_dropshipping.fk_procurement_order_item = procurement_order_item.id_procurement_order_item
INNER JOIN procurement_live_mx.procurement_order
	ON procurement_order_item.fk_procurement_order = procurement_order.id_procurement_order
SET 
 out_order_tracking_sample.date_po_created = procurement_order.created_at,
 out_order_tracking_sample.date_po_issued = procurement_order.sent_at
WHERE operations_co.out_order_tracking_sample.fullfilment_type_real = "dropshipping";
*/

UPDATE operations_co.out_order_tracking_sample a
INNER JOIN wmsprod_co.itens_venda b
ON a.item_id=b.item_id
INNER JOIN wmsprod_co.estoque c
ON b.estoque_id=c.estoque_id
INNER JOIN wmsprod_co.itens_recebimento d
ON c.itens_recebimento_id=d.itens_recebimento_id
INNER JOIN wmsprod_co.recebimento e
ON d.recebimento_id=e.recebimento_id
SET a.purchase_order = e.inbound_document_identificator
WHERE e.inbound_document_type_id = 4;


UPDATE operations_co.out_order_tracking_sample a
INNER JOIN wmsprod_co.entrega b
ON a.wms_tracking_code = b.cod_rastreamento
SET a.delivery_fullfilment = b.delivery_fulfillment;


SELECT  'status_itens_venda expedido',now();
#Fecha
TRUNCATE	operations_co.pro_1st_attempt;

INSERT INTO operations_co.pro_1st_attempt (
	order_item_id, 
	min_of_date) 
SELECT
	out_order_tracking_sample.item_id,
	min(tms_status_delivery.date) AS min_of_date
FROM operations_co.out_order_tracking_sample
INNER JOIN wmsprod_co.vw_itens_venda_entrega 
	ON out_order_tracking_sample.item_id = vw_itens_venda_entrega.item_id
INNER JOIN wmsprod_co.tms_status_delivery 
	ON vw_itens_venda_entrega.cod_rastreamento = tms_status_delivery.cod_rastreamento
GROUP BY
	out_order_tracking_sample.item_id,
	tms_status_delivery.id_status
HAVING
	tms_status_delivery.id_status = 5;

UPDATE operations_co.out_order_tracking_sample
INNER JOIN operations_co.pro_1st_attempt 
	ON out_order_tracking_sample.item_id = pro_1st_attempt.order_item_id
SET out_order_tracking_sample.date_1st_attempt = pro_1st_attempt.min_of_date;

UPDATE operations_co.out_order_tracking_sample
INNER JOIN wmsprod_co.vw_itens_venda_entrega vw 
	ON out_order_tracking_sample.item_id = vw.item_id
INNER JOIN wmsprod_co.tms_status_delivery del 
	ON vw.cod_rastreamento = del.cod_rastreamento
SET out_order_tracking_sample.date_1st_attempt = (
	SELECT
		max(tms.date)
	FROM
		wmsprod_co.tms_status_delivery tms,
		wmsprod_co.vw_itens_venda_entrega itens
	WHERE
		del.cod_rastreamento = tms.cod_rastreamento
	AND vw.item_id = itens.item_id
	AND tms.id_status = 4
)
WHERE
#Fecha
	out_order_tracking_sample.date_1st_attempt IS NULL
OR out_order_tracking_sample.date_1st_attempt < '2011-05-01';

SELECT  'date_ready_to_pick',now();	
UPDATE operations_co.out_order_tracking_sample
#Fecha
SET out_order_tracking_sample.date_ready_to_pick = 	CASE
																											WHEN dayofweek(date_procured) = 1 
																											THEN operations_co.workday (date_procured, 1)
																											WHEN dayofweek(date_procured) = 7 
																											THEN operations_co.workday (date_procured, 1)
																											ELSE date_procured
																										END;

UPDATE operations_co.out_order_tracking_sample
SET out_order_tracking_sample.datetime_ready_to_pick = 	CASE
																													WHEN dayofweek(datetime_procured) = 1 
																													THEN operations_co.workday (datetime_procured, 1)
																													WHEN dayofweek(datetime_procured) = 7 
																													THEN operations_co.workday (datetime_procured, 1)
																													ELSE datetime_procured
																												END;

#Fecha pendientes 

UPDATE operations_co.out_order_tracking_sample
INNER JOIN bob_live_co.sales_order 
	ON out_order_tracking_sample.order_number = sales_order.order_nr
INNER JOIN bob_live_co.sales_order_address 
	ON sales_order.fk_sales_order_address_shipping = sales_order_address.id_sales_order_address
SET 
	out_order_tracking_sample.ship_to_zip = postcode,
	out_order_tracking_sample.ship_to_zip2 = mid(postcode, 3, 2),
	out_order_tracking_sample.ship_to_zip3 = RIGHT (postcode, 3),
	out_order_tracking_sample.ship_to_zip4 = LEFT (postcode, 4)
	;

/*
# Pendiente agregar para CO
UPDATE operations_co.out_order_tracking_sample
SET ship_to_met_area = CASE
WHEN ship_to_zip BETWEEN 01000
AND 16999
OR ship_to_zip BETWEEN 53000
AND 53970
OR ship_to_zip BETWEEN 54000
AND 54198
OR ship_to_zip BETWEEN 54600
AND 54658
OR ship_to_zip BETWEEN 54700
AND 54769
OR ship_to_zip BETWEEN 54900
AND 54959
OR ship_to_zip BETWEEN 54960
AND 54990
OR ship_to_zip BETWEEN 52760
AND 52799
OR ship_to_zip BETWEEN 52900
AND 52998
OR ship_to_zip BETWEEN 55000
AND 55549
OR ship_to_zip BETWEEN 55700
AND 55739
OR ship_to_zip BETWEEN 57000
AND 57950 THEN
	1
ELSE
	0
END;
*/

UPDATE operations_co.out_order_tracking_sample a
INNER JOIN wmsprod_co.pedidos_venda b
ON a.order_number = b.numero_pedido
SET 
a.payment_method = b.metodo_de_pagamento,
a.ship_to_region = b.estado_cliente,
a.ship_to_city = b.cidade_cliente;

###############


UPDATE operations_co.out_order_tracking_sample AS a
INNER JOIN production_co.calendar AS b
ON DATE(a.date_shipped)=b.dt
AND b.isweekend=1
SET a.shipped_is_weekend=1;

UPDATE operations_co.out_order_tracking_sample AS a
LEFT JOIN wmsprod_co.itens_venda AS b
ON a.item_id=b.item_id
SET
a.note_tracking = b.obs;

UPDATE operations_co.out_order_tracking_sample AS a
SET
a.tracking_stockout= 
CASE 
   WHEN INSTR(note_tracking,'OOS')!=0 THEN "OOS" 
   WHEN INSTR(note_tracking,'OOT')!=0 THEN "OOT" 
   WHEN INSTR(note_tracking,'PNP')!=0 THEN "PNP" 
   WHEN INSTR(note_tracking,'DSE')!=0 THEN "DSE" 
   WHEN INSTR(note_tracking,'POL')!=0 THEN "POL"  
   WHEN INSTR(note_tracking,'DTE')!=0 THEN "DTE"  
   WHEN INSTR(note_tracking,'SOE')!=0 THEN "SOE"  
   WHEN INSTR(note_tracking,'CBC')!=0 THEN "CBC"  
   WHEN INSTR(note_tracking,'DTL')!=0 THEN "DTL"
   WHEN INSTR(note_tracking,'OSW')!=0 THEN "OSW"
   WHEN INSTR(note_tracking,'UNS')!=0 THEN "UNS"
   WHEN INSTR(note_tracking,'OSM')!=0 THEN "OSM"
   WHEN INSTR(note_tracking,'SWC')!=0 THEN "SWC"
   WHEN INSTR(note_tracking,'OOA')!=0 THEN "OOA"
   WHEN INSTR(note_tracking,'IWL')!=0 THEN "IWL"
   WHEN INSTR(note_tracking,'ETR')!=0 THEN "ETR"
   WHEN INSTR(note_tracking,'CSW')!=0 THEN "CSW"
   WHEN INSTR(note_tracking,'STE')!=0 THEN "STE"
   ELSE "NA"
END;

UPDATE operations_co.out_order_tracking_sample AS d
SET d.is_quebra_tratada = 1
WHERE d.date_declared_stockout IS NOT NULL
AND d.last_quebra_tratada_date IS NOT NULL
AND d.date_declared_stockout < d.last_quebra_tratada_date;

UPDATE operations_co.out_order_tracking_sample
SET sac_backlog = dias_habiles_negativos(curdate(), date_declared_stockout)
WHERE last_quebra_tratada_date IS NULL
AND date_declared_stockout IS NOT NULL;

UPDATE operations_co.out_order_tracking_sample AS a
INNER JOIN calcworkdays AS b
ON date(a.date_exported) = b.date_last 
 AND b.workdays =a.min_delivery_time
 AND b.isweekday = 1 
    AND b.isholiday = 0
SET 
 a.min_delivery_date = b.date_first ;

UPDATE operations_co.out_order_tracking_sample AS d
INNER JOIN
(SELECT a.itens_venda_id, a.numero_order, a.item_id,b.date 
from
(select itens_venda_id, numero_order, item_id from wmsprod_co.itens_venda) a inner join
(select itens_venda_id,max(data) date
from wmsprod_co.status_itens_venda
where status = 'quebra tratada'
group by itens_venda_id order by date desc) b
on a.itens_venda_id = b.itens_venda_id) t 
on d.item_id = t.item_id
set d.last_quebra_tratada_date = t.date;

UPDATE operations_co.out_order_tracking_sample
SET customer_backlog = dias_habiles_negativos(curdate(),date_delivered_promised)
WHERE last_quebra_tratada_date IS NULL
AND date_declared_stockout IS NOT NULL;

UPDATE operations_co.out_order_tracking_sample AS a
SET a.procured_promised_last_30 = CASE
WHEN datediff(curdate(),date_procured_promised) <= 30
AND datediff(curdate(),date_procured_promised) > 0 
THEN 1
ELSE 0
END,
a.shipped_last_30 = CASE
WHEN datediff(curdate(), date_shipped) <= 30
AND datediff(curdate(), date_shipped) > 0 
THEN 1
ELSE 0
END,
a.delivered_promised_last_30 = CASE
WHEN datediff(curdate(),date_delivered_promised) <= 30
AND datediff(curdate(),date_delivered_promised) > 0 
THEN 1
ELSE 0
END;

UPDATE operations_co.out_order_tracking_sample AS a
SET
a.quebra_sugerido = IF(a.note_tracking regexp 'LACOL|SUGERIR', 'SI', 'NO')
WHERE a.date_declared_stockout IS NOT NULL;

UPDATE operations_co.out_order_tracking_sample AS a
SET
a.quebra_sugerido = IF(a.note_tracking NOT regexp 'LACOL' = 1, 'NO', 'SI')
WHERE a.date_declared_stockout IS NOT NULL;


SELECT  'A_Master',now();
CALL production.monitoring( "A_Master" , "Colombia_Project",  240 );

UPDATE operations_co.out_order_tracking_sample AS a
INNER JOIN development_co_project.A_Master AS b
ON a.item_id=b.ItemID
SET
a.tax_percent = b.Tax,
a.cost = b.Cost,
a.cost_w_o_vat = b.CostAfterTax,
a.price = b.Price,
a.price_w_o_vat = b.PriceAfterTax,
a.is_marketplace = b.isMPlace,
a.corporate_sale = if(b.CouponCode LIKE 'VC%',1,0),
a.coupon_value = if(b.CouponCode like 'REP%',b.CouponValue,0), 
a.customer_first_name = b.FirstName,
a.customer_last_name = b.LastName,
a.payment_method = b.PaymentMethod;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 PROCEDURE `out_procurement_tracking`()
BEGIN

-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------


SELECT  'Calculating WorkDays',now();
call calcworkdays;
SELECT  'Finished Calculating WorkDays',now();


SELECT  'OPS Oms Report',now();

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'operations_co.out_procurement_tracking',
  'start',
  NOW(),
  MAX(created_date),
  count(*),
  count(*)
FROM
  operations_co.out_procurement_tracking
;


TRUNCATE operations_co.out_procurement_tracking ; 

INSERT INTO operations_co.out_procurement_tracking (
		id_procurement_order_item,
		id_procurement_order,
		id_catalog_simple,
		procurement_carrier_id,
		transport_type,
		is_deleted,
		is_confirmed,
		is_receipt,
		date_payment_scheduled,
		date_payment_estimation,
		date_payment_promised,
		cost_oms,
		tax,
		cost_oms_after_tax,
		inbound_type
) 
SELECT
		id_procurement_order_item,
		fk_procurement_order,
		fk_catalog_simple,
		fk_shipment_carrier,
		transport_type,
		is_deleted,
		is_confirm,
		sku_received,
		date(schedule_payment_date),
		date(estimated_payment_date),
		date(promised_payment_date),
		price_before_tax,
		tax,
		unit_price,
		if(transport_type = 2, 'FOB', 'CIF')
	FROM
		procurement_live_co.procurement_order_item 
#LIMIT 50
; 

UPDATE operations_co.out_procurement_tracking a
INNER JOIN procurement_live_co.procurement_order b 
	ON a.id_procurement_order = b.id_procurement_order
INNER JOIN procurement_live_co.procurement_order_type c 
	ON b.fk_procurement_order_type = c.id_procurement_order_type
SET 		
  a.purchase_order = concat(	b.venture_code, 
											lpad(b.id_procurement_order, 7, 0),
											b.check_digit),
	a.fk_procurement_order_type = b.fk_procurement_order_type,
	a.procurement_payment_terms = b.procurement_payment_terms,
	a.procurement_payment_event = b.procurement_payment_event,
	a.procurement_payment_status = b.payment_status,
	a.is_cancelled = b.is_cancelled,
	a.date_po_created = date(b.created_at),
	a.date_po_updated = date(b.updated_at),
	a.date_po_issued = date(b.sent_at)
	#, a.procurement_order_type = c.procurement_order_type
; 

SELECT  'Type PO',now();
UPDATE operations_co.out_procurement_tracking
SET out_procurement_tracking.purchase_order_type='Crossdocking'
WHERE out_procurement_tracking.fk_procurement_order_type=6;

SELECT  'Type PO',now();
UPDATE operations_co.out_procurement_tracking
SET out_procurement_tracking.purchase_order_type='Stock'
WHERE out_procurement_tracking.fk_procurement_order_type=1;

SELECT  'Type PO',now();
UPDATE operations_co.out_procurement_tracking
SET out_procurement_tracking.purchase_order_type='Consignment'
WHERE out_procurement_tracking.fk_procurement_order_type=7;

SELECT  'Type PO',now();
UPDATE operations_co.out_procurement_tracking
SET out_procurement_tracking.purchase_order_type='Materia Prima'
WHERE out_procurement_tracking.fk_procurement_order_type=8;

SELECT  'Type PO',now();
UPDATE operations_co.out_procurement_tracking
SET out_procurement_tracking.purchase_order_type='Invoice'
WHERE out_procurement_tracking.fk_procurement_order_type=9;

SELECT  'Type PO',now();
UPDATE operations_co.out_procurement_tracking
SET out_procurement_tracking.purchase_order_type='Inbound'
WHERE out_procurement_tracking.fk_procurement_order_type=10;


UPDATE operations_co.out_procurement_tracking a
INNER JOIN development_co_project.A_Master_Catalog b 
	ON a.id_catalog_simple = b.id_catalog_simple
SET
	a.sku_simple = b.sku_simple,
	a.sku_config = b.sku_config,
	a.sku_name = b.sku_name,
	a.supplier_id = b.id_supplier,
	a.supplier_name = b.Supplier,
	a.brand = b.brand,
	a.fulfillment_type_bob = b.fulfillment_type,
	a.cat_1 = b.Cat1,
	a.cat_2 = b.Cat2,
	a.cat_3 = b.Cat3,
	a.buyer = b.Buyer,
	a.head_buyer = b.Head_Buyer;

UPDATE operations_co.out_procurement_tracking a
INNER JOIN procurement_live_co.catalog_supplier_attributes b 
	ON a.supplier_id = b.fk_catalog_supplier
SET 
 a.supplier_name = b.company_name,
 a.supplier_tax_id = b.nit,
 a.catalog_payment_type = b.payment_type,
 a.catalog_payment_terms = b.payment_terms,
 a.procurement_analyst = b.tracker,
# a.pick_at_zip = b.NA,
# a.pick_at_zip2 = b.NA,
# a.pick_at_zip3 = b.NA,
 a.pick_at_city = b.city,
 a.pick_at_region = b.zone,
 a.credit_limit = b.credit_limit; 

UPDATE operations_co.out_procurement_tracking a
INNER JOIN procurement_live_co.procurement_order_item_date_history b 
	ON a.id_procurement_order_item = b.fk_procurement_order_item
SET 
	a.date_goods_received = b.delivery_real_date,
	a.date_po_confirmed = b.confirm_date;

UPDATE operations_co.out_procurement_tracking a
INNER JOIN procurement_live_co.wms_received_item b 
	ON a.id_procurement_order_item = b.fk_procurement_order_item
INNER JOIN wmsprod_co.itens_recebimento c 
	ON b.id_wms = c.itens_recebimento_id
INNER JOIN wmsprod_co.estoque d 
	ON c.itens_recebimento_id = d.itens_recebimento_id
INNER JOIN wmsprod_co.movimentacoes e
ON d.estoque_id = e.estoque_id 
SET 
 a.stock_item_id = d.estoque_id,
 a.wh_location = d.endereco,
 a.exit_type = 	CASE WHEN ( d.endereco = 'vendidos' AND e.para_endereco = "vendidos" )
										THEN 'sold'
										WHEN ( d.endereco LIKE '%error%' AND e.para_endereco  LIKE '%error%' )
										THEN 'error'
										ELSE NULL
								END;

UPDATE operations_co.out_procurement_tracking a
INNER JOIN wmsprod_co.itens_venda b 
	ON a.stock_item_id = b.estoque_id
SET 
	a.item_id = b.item_id; 


UPDATE operations_co.out_procurement_tracking a
INNER JOIN procurement_live_co.invoice_item b ON a.id_procurement_order_item = b.fk_procurement_order_item
INNER JOIN procurement_live_co.invoice c ON b.fk_invoice = c.id_invoice
SET 
 a.invoice_number = c.invoice_nr,
 a.date_invoice_issued = date(c.emission_date),
 a.date_invoice_created = date(c.created_at),
 a.date_invoice_receipt = date(c.invoice_date); 

DROP TEMPORARY TABLE IF EXISTS operations_co.tmp_stock_ft;

CREATE TEMPORARY TABLE operations_co.tmp_stock_ft (INDEX (stock_item_id))
SELECT
	stock_item_id,
	fulfillment_type_real,
	fulfillment_type_bp
FROM operations_co.out_stock_hist;

UPDATE operations_co.out_procurement_tracking a
INNER JOIN operations_co.tmp_stock_ft b 
	ON a.stock_item_id = b.stock_item_id
SET 
 a.fulfillment_type_real = b.fulfillment_type_real,
 a.fulfillment_type_bp = b.fulfillment_type_bp ;

UPDATE operations_co.out_procurement_tracking a
INNER JOIN procurement_live_co.procurement_order_item_date_history b 
	ON a.id_procurement_order_item = b.fk_procurement_order_item
SET 
 a.date_collection_scheduled = date(collect_scheduled_date),
 a.date_collection_negotiated = date(collect_negotiated_date),
 a.date_delivery_calculated_bob = date(delivery_bob_calculated_date),
 a.date_delivery_scheduled = date(delivery_scheduled_date);


UPDATE operations_co.out_procurement_tracking a
INNER JOIN bob_live_co.sales_order_item b 
ON a.item_id = b.id_sales_order_item
SET 
 a.date_ordered = date(b.created_at),
 a.cost_bob = b.cost ; 

UPDATE operations_co.out_procurement_tracking a
INNER JOIN bob_live_co.catalog_simple b 
ON a.id_catalog_simple = b.id_catalog_simple
SET a.cost_bob = b.cost
WHERE
	a.cost_bob = 0 ;


UPDATE operations_co.out_procurement_tracking
SET 
	week_po_created = operations_co.week_iso (date_po_created),
# week_payment = operations_co.week_iso (date_paid),
	week_goods_received = operations_co.week_iso (date_goods_received),
	month_po_created = date_format(date_po_created, "%x-%m"),
# month_payment = date_format(date_paid, "%x-%m"),
	month_goods_received = date_format(date_goods_received,"%x-%m"),
	goods_received_last_15 = IF(datediff(curdate(),date_goods_received) <= 15,1,0),
	goods_received_last_30 = IF(datediff(curdate(),date_goods_received) <= 30,1,0),
	goods_received_last_45 = IF(datediff(curdate(),date_goods_received) <= 45,1,0)
;

UPDATE operations_co.out_procurement_tracking
SET 
 cost_oms_gt_bob = 1
WHERE
 cost_oms > cost_bob ;
 
DROP TEMPORARY TABLE IF EXISTS operations_co.tmp_po_totals;
CREATE TEMPORARY TABLE operations_co.tmp_po_totals (INDEX (purchase_order))
SELECT 
	purchase_order,
	count(*) AS num_items
FROM operations_co.out_procurement_tracking
WHERE is_deleted = 0
AND		is_cancelled = 0
GROUP BY purchase_order;

UPDATE operations_co.out_procurement_tracking a
INNER JOIN operations_co.tmp_po_totals b
ON a.purchase_order = b.purchase_order
SET 
	a.items_in_po = b.num_items,
	a.perc_items_in_po = 1/b.num_items;

UPDATE operations_co.out_procurement_tracking
SET item_counter = 1;

DROP TABLE IF EXISTS production_co.out_procurement_tracking;

CREATE TABLE production_co.out_procurement_tracking LIKE operations_co.out_procurement_tracking;

INSERT INTO production_co.out_procurement_tracking SELECT * FROM operations_co.out_procurement_tracking;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Venezuela', 
  'out_procurement_tracking',
  'finish',
  NOW(),
  max(date_created),
  count(*),
  count(item_counter)
FROM
  production_co.out_procurement_tracking;

/*
Revisar cómo registrar esto si los pagos no se hacen en OMS
UPDATE operations_co.out_procurement_tracking a
INNER JOIN procurement_live_co.procurement_order_payment_items b 
	ON a.id_procurement_order_item = b.fk_procurement_order_item
INNER JOIN procurement_live_co.procurement_order_payment c 
	ON b.fk_procurement_order_payment = c.id_procurement_order_payment
SET 
 a.procurement_payment_type = c.payment_type,
 a.date_paid = date(c.operation_date) ; 

UPDATE operations_co.out_procurement_tracking
SET 
 is_paid = 1,
 amount_paid = cost_oms
WHERE
	date_paid IS NOT NULL ;

UPDATE operations_co.out_procurement_tracking
SET 
 is_invoiced = 1
WHERE
 date_paid IS NOT NULL ; 

UPDATE operations_co.out_procurement_tracking
SET 
 payment_terms_real = CASE
												WHEN date_paid IS NULL 
												THEN NULL
												ELSE
											 (CASE
													WHEN date_goods_received IS NULL 
													THEN datediff(date_paid, curdate())
													ELSE datediff(date_paid, date_goods_received)
												END)
											END,
payment_terms_scheduled = CASE
																WHEN date_payment_scheduled IS NULL 
																THEN NULL
																ELSE (	CASE
																					WHEN date_goods_received IS NULL 
																					THEN datediff(date_payment_scheduled, curdate())
																					ELSE datediff(date_payment_scheduled,	date_goods_received)
																				END)
																END,
payment_terms_expected = CASE
														WHEN date_goods_received IS NULL 
														THEN datediff(date_payment_estimation,curdate())
													ELSE (	CASE
																		WHEN date_payment_promised IS NULL 
																		THEN datediff(date_payment_estimation,date_goods_received)
																	ELSE datediff(date_payment_promised,date_goods_received)
																		END)
													END ; 

UPDATE operations_co.out_procurement_tracking
SET 
 paid_last_15 = IF(datediff(curdate(),date_paid) <= 15,1,0),
 paid_last_30 = IF(datediff(curdate(),date_paid) <= 30,1,0),
 paid_last_45 = IF(datediff(curdate(),date_paid) <= 45,1,0)
;

UPDATE operations_co.out_procurement_tracking a
SET a.date_payment_programmed = CASE
																	WHEN a.is_confirmed = 1 
																	THEN date_payment_scheduled
																	ELSE (	CASE
																						WHEN a.is_received = 1 
																						THEN date_payment_promised
																						ELSE date_payment_estimation
																					END)
																	END
WHERE
	procurement_payment_event = "pedido" ; 

UPDATE operations_co.out_procurement_tracking a
SET 
 a.date_payment_programmed = CASE
															WHEN a.is_invoiced = 1 
															THEN (CASE
																			WHEN a.is_confirmed = 1 
																			THEN date_payment_scheduled
																			ELSE date_payment_promised
																		END)
															ELSE (CASE
																			WHEN a.is_received = 1 
																			THEN date_payment_promised
																			ELSE date_payment_estimation
																		END)
															END
WHERE
	procurement_payment_event = "factura" ; 

UPDATE operations_co.out_procurement_tracking a
SET 
 a.date_payment_programmed = CASE
															WHEN a.is_received = 1 
															THEN (CASE
																			WHEN a.is_confirmed = 1
																			AND a.is_invoiced = 1 
																			THEN date_payment_scheduled
																			ELSE date_payment_promised
																		END)
															ELSE date_payment_estimation
														 END
WHERE
	procurement_payment_event = "entrega" ; 


UPDATE operations_co.out_procurement_tracking
SET payment_terms_programmed = datediff(date_payment_programmed,date_goods_received) ; 

UPDATE operations_co.out_procurement_tracking
SET payment_terms = payment_terms_real ; 

UPDATE operations_co.out_procurement_tracking
SET payment_terms = payment_terms_programmed
WHERE
	payment_terms IS NULL ; 

UPDATE operations_co.out_procurement_tracking
SET payment_terms = payment_terms_scheduled
WHERE
	payment_terms IS NULL ; 

UPDATE operations_co.out_procurement_tracking
SET payment_terms = payment_terms_scheduled
WHERE
	payment_terms IS NULL ; 

UPDATE operations_co.out_procurement_tracking
SET payment_terms = payment_terms_expected
WHERE
	payment_terms IS NULL ;
*/

#1.
UPDATE operations_co.out_procurement_tracking
INNER JOIN calcworkdays
ON date(date_delivery_bob_original_calculated) = calcworkdays.date_last 
              AND date_first = date_po_created
              AND isweekday = 1 
              AND isholiday = 0
SET workdays_to_receipt_shedule = calcworkdays.workdays;
#2.
SELECT  'Calculo promised procurement date stock1',now();
UPDATE operations_co.out_procurement_tracking
INNER JOIN calcworkdays
ON date(date_delivery_bob_original_calculated) = calcworkdays.date_last 
              AND workdays = 10
              AND isweekday = 1 
              AND isholiday = 0
SET date_promised_procurement = calcworkdays.date_first
WHERE workdays_to_receipt_shedule<=5
AND purchase_order_type='Stock';
#3.
SELECT  'Calculo promised procurement date stock2',now();
UPDATE operations_co.out_procurement_tracking
INNER JOIN calcworkdays
ON date(date_delivery_bob_original_calculated) = calcworkdays.date_last 
              AND workdays = 5
              AND isweekday = 1 
              AND isholiday = 0
SET date_promised_procurement = calcworkdays.date_first
WHERE workdays_to_receipt_shedule>5
AND purchase_order_type='Stock';
#4.
SELECT  'Calculo promised procurement date otros',now();
UPDATE operations_co.out_procurement_tracking
SET date_promised_procurement = date_delivery_bob_original_calculated
WHERE purchase_order_type<>'Stock';
#5.
SELECT  'Calculo promised procurement date otros',now();
UPDATE operations_co.out_procurement_tracking
SET  pending_to_receipt=1
WHERE date_promised_procurement<=CURDATE()
AND is_cancelled IS NULL
AND is_deleted IS NULL
AND is_receipt IS NULL;


INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'operations_co.out_procurement_tracking',
  'finish',
  NOW(),
  MAX(created_date),
  count(*),
  count(*)
FROM
  operations_co.out_procurement_tracking
;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 PROCEDURE `out_stock_hist`()
BEGIN


-- --------------------------------------------------------------------------------
-- New Out Stock History Routine Colombia
-- --------------------------------------------------------------------------------

INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  UPDATEd_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'out_stock_hist',
  'start',
  NOW(),
  max(date_exit),
  count(*),
  count(item_counter)
FROM
  production_co.out_stock_hist;


TRUNCATE operations_co.out_stock_hist;

INSERT INTO operations_co.out_stock_hist (
	stock_item_id,
	barcode_wms,
	date_entrance,
	week_entrance,
	barcode_bob_duplicated,
	in_stock,
	wh_location,
	sub_location,
	position_type,
    reserved
) 
SELECT
	estoque.estoque_id,
	estoque.cod_barras,
	date(data_criacao),
	operations_co.week_iso(data_criacao),
	estoque.minucioso,
	posicoes.participa_estoque,
	estoque.endereco,
	estoque.sub_endereco,
	posicoes.tipo_posicao,
    IF(estoque.almoxarifado = "separando"
			OR estoque.almoxarifado = "estoque reservado"
			OR estoque.almoxarifado = "aguardando separacao", 1, 0)
FROM	(wmsprod_co.estoque
			LEFT JOIN wmsprod_co.itens_recebimento 
				ON estoque.itens_recebimento_id = itens_recebimento.itens_recebimento_id)
			LEFT JOIN wmsprod_co.posicoes 
				ON estoque.endereco = posicoes.posicao;



#REVISAR SI SE USA
/*DELETE operations_co.pro_unique_ean_bob.*
FROM
	operations_co.pro_unique_ean_bob;


INSERT INTO operations_co.pro_unique_ean_bob (ean) SELECT
	produtos.ean AS ean
FROM
	bob_live_co.catalog_simple
 INNER JOIN wmsprod_co.produtos ON catalog_simple.sku = produtos.sku
WHERE
	catalog_simple. STATUS NOT LIKE "deleted"
GROUP BY
	produtos.ean
HAVING
	count(produtos.ean = 1);*/

UPDATE operations_co.out_stock_hist
 INNER JOIN wmsprod_co.traducciones_producto 
ON out_stock_hist.barcode_wms = traducciones_producto.identificador
SET out_stock_hist.sku_simple = sku;

UPDATE operations_co.out_stock_hist 
SET 
	sku_simple_blank= IF(sku_simple IS NULL,1,0);

#Fecha y datos de cuando sale vendido un producto
UPDATE operations_co.out_stock_hist 
 INNER JOIN wmsprod_co.movimentacoes 
ON out_stock_hist.stock_item_id = movimentacoes.estoque_id 
SET 
	out_stock_hist.date_exit = data_criacao,
	out_stock_hist.week_exit = operations_co.week_iso(data_criacao),
	out_stock_hist.exit_type = "sold",
	out_stock_hist.sold = 1
WHERE 
		out_stock_hist.wh_location ="vendidos"
	AND movimentacoes.para_endereco="vendidos";

#Fecha y datos de cuando sale errado un producto
UPDATE operations_co.out_stock_hist 
 INNER JOIN wmsprod_co.movimentacoes 
ON out_stock_hist.stock_item_id = movimentacoes.estoque_id 
SET 
	out_stock_hist.date_exit = data_criacao,
	out_stock_hist.week_exit = operations_co.week_iso(data_criacao),
	out_stock_hist.exit_type = "error"
WHERE 
		out_stock_hist.wh_location ="Error_entrada"
	AND movimentacoes.para_endereco="Error_entrada";


UPDATE operations_co.out_stock_hist 
 INNER JOIN wmsprod_co.movimentacoes 
	ON out_stock_hist.stock_item_id = movimentacoes.estoque_id 
SET out_stock_hist.wh_location_before_sold = de_endereco
WHERE movimentacoes.para_endereco="vendidos";


CALL production.monitoring( "A_Master_Catalog" , "Colombia_Project",  240 );

#Hacerlo desde A_MASTERCATALOG
UPDATE operations_co.out_stock_hist
 INNER JOIN development_co_project.A_Master_Catalog 
	ON out_stock_hist.sku_simple = development_co_project.A_Master_Catalog.sku_simple
SET 
 out_stock_hist.barcode_bob = 						A_Master_Catalog.barcode_ean,
 out_stock_hist.sku_config = 						A_Master_Catalog.sku_config,
 out_stock_hist.sku_name = 							A_Master_Catalog.sku_name,
 out_stock_hist.sku_color = 						A_Master_Catalog.color,
 out_stock_hist.sku_attribute = 					A_Master_Catalog.attribute,
 out_stock_hist.brand = 							A_Master_Catalog.Brand,
 out_stock_hist.visible = 							A_Master_Catalog.isVisible,
 out_stock_hist.is_marketplace = 					A_Master_Catalog.isMarketPlace,
 #out_stock_hist.stock_wms =						out_catalog_product_stock.TBD,
 #out_stock_hist.reserved_wms = 					out_catalog_product_stock.TBD,
 #out_stock_hist.sku_supplier_config = 				catalog_config.sku_supplier_config,
 out_stock_hist.model = 							A_Master_Catalog.model,
 out_stock_hist.supplier_name = 					A_Master_Catalog.Supplier,
 out_stock_hist.supplier_id = 						A_Master_Catalog.id_supplier,
 out_stock_hist.supplier_leadtime = 				A_Master_Catalog.delivery_time_supplier,
 out_stock_hist.fulfillment_type_bob = 				A_Master_Catalog.fulfillment_type,
 out_stock_hist.cost_w_o_vat = ifnull(
	IF (A_Master_Catalog.special_purchase_price IS NULL,A_Master_Catalog.cost,
	IF (curdate() > A_Master_Catalog.special_to_date,A_Master_Catalog.cost,
	IF (curdate() < A_Master_Catalog.special_from_date,A_Master_Catalog.cost,
	IF (A_Master_Catalog.special_purchase_price < A_Master_Catalog.cost,
				A_Master_Catalog.special_purchase_price,
				A_Master_Catalog.cost)))),0),
 out_stock_hist.price = ifnull(
	IF (A_Master_Catalog.special_price IS NULL,A_Master_Catalog.price,
	IF (curdate() > A_Master_Catalog.special_to_date,A_Master_Catalog.price,
	IF (curdate() < A_Master_Catalog.special_from_date,A_Master_Catalog.price,
	IF (A_Master_Catalog.special_price < A_Master_Catalog.price,
				A_Master_Catalog.special_price,
				A_Master_Catalog.price)))),0),
 out_stock_hist.special_from_date = 				A_Master_Catalog.special_from_date,
 out_stock_hist.special_to_date = 					A_Master_Catalog.special_to_date,
 out_stock_hist.status_simple = 					A_Master_Catalog.status_simple,
 out_stock_hist.status_config = 					A_Master_Catalog.status_config,
 out_stock_hist.pet_status = 						A_Master_Catalog.pet_status,
 out_stock_hist.category_bp = A_Master_Catalog.Cat_BP ;

UPDATE operations_co.out_stock_hist a
 INNER JOIN bob_live_co.catalog_simple b
 ON a.sku_simple = b.sku
 INNER JOIN operations_co.simple_variation c
 ON b.id_catalog_simple = c.fk_catalog_simple
SET
 a.sku_variation =  c.variation;

UPDATE operations_co.out_stock_hist a
 INNER JOIN bob_live_co.catalog_config b ON a.sku_config = b.sku
 INNER JOIN bob_live_co.catalog_attribute_option_global_category c ON b.fk_catalog_attribute_option_global_category = c.id_catalog_attribute_option_global_category
 INNER JOIN bob_live_co.catalog_attribute_option_global_sub_category d ON b.fk_catalog_attribute_option_global_sub_category = d.id_catalog_attribute_option_global_sub_category
 INNER JOIN bob_live_co.catalog_attribute_option_global_sub_sub_category e ON b.fk_catalog_attribute_option_global_sub_sub_category = e.id_catalog_attribute_option_global_sub_sub_category
SET a.category_1 = c. NAME,
 a.category_2 = d. NAME,
 a.category_3 = e. NAME;


call operations_co.out_catalog_product_stock;

# Indicadores de Stock para consulta
UPDATE operations_co.out_catalog_product_stock
SET share_reserved_bob = 	IF (	reservedbob = 0,0,
													IF (	(reservedbob - stock_wms_reserved) / stock_wms > 1,1,(reservedbob - stock_wms_reserved) / stock_wms)), share_stock_bob = availablebob / stock_wms;

SELECT
	'Actualizando niveles de stock de BOB',
	now();
	
UPDATE operations_co.out_catalog_product_stock a
         INNER JOIN
    operations_co.vw_stock_wms b ON a.sku = b.sku_simple 
SET 
    a.stock_wms = b.stock_wms
WHERE
    b.stock_wms > 0;



UPDATE operations_co.out_catalog_product_stock a
         INNER JOIN
    operations_co.vw_stock_wms_reserved b ON a.sku = b.sku_simple 
SET 
    a.stock_wms_reserved = b.stock_wms_reserved;
	

UPDATE operations_co.out_catalog_product_stock 
SET 
    stock_wms_reserved = 0
WHERE
    stock_wms_reserved IS NULL;


UPDATE operations_co.out_catalog_product_stock 
SET 
    share_reserved_bob = IF(reservedbob = 0,
        0,
        IF((reservedbob - stock_wms_reserved) / stock_wms > 1,
            1,
            (reservedbob - stock_wms_reserved) / stock_wms)),
    share_stock_bob = availablebob / stock_wms;

UPDATE operations_co.out_stock_hist
 INNER JOIN operations_co.out_catalog_product_stock 
	ON out_stock_hist.sku_simple = out_catalog_product_stock.sku
SET 
 out_stock_hist.stock_bob = out_catalog_product_stock.stockbob,
 out_stock_hist.own_stock_bob = out_catalog_product_stock.ownstock,
 out_stock_hist.supplier_stock_bob = out_catalog_product_stock.supplierstock,
 out_stock_hist.reserved_bob = out_catalog_product_stock.reservedbob,
 out_stock_hist.stock_available_bob = out_catalog_product_stock.availablebob,
 out_stock_hist.reserved_bob = out_catalog_product_stock.share_reserved_bob;

#Se actualizan costos de oms de acuerdo a las notas a credito
UPDATE operations_co.out_stock_hist od
JOIN operations_co.tbl_cambios_costos cc
ON od.sku_simple=cc.sku
SET od.cost_w_o_vat = cc.cost
WHERE od.date_ordered BETWEEN cc.from_date AND cc.to_date;


UPDATE operations_co.out_stock_hist
 INNER JOIN bob_live_co.catalog_simple ON out_stock_hist.sku_simple = catalog_simple.sku
 INNER JOIN bob_live_co.catalog_tax_class ON catalog_simple.fk_catalog_tax_class = catalog_tax_class.id_catalog_tax_class
SET 
 out_stock_hist.tax_percentage = tax_percent,
 out_stock_hist.cost = out_stock_hist.cost_w_o_vat*(1 + tax_percent / 100);

UPDATE operations_co.out_stock_hist
SET cogs = cost_w_o_vat + delivery_cost_supplier;

UPDATE operations_co.out_stock_hist
 INNER JOIN bob_live_co.catalog_config ON out_stock_hist.sku_config = catalog_config.sku
 INNER JOIN bob_live_co.catalog_attribute_option_global_buyer ON catalog_attribute_option_global_buyer.id_catalog_attribute_option_global_buyer = catalog_config.fk_catalog_attribute_option_global_buyer
SET out_stock_hist.buyer = catalog_attribute_option_global_buyer. NAME;

#Parche del catálogo quitar despues
UPDATE operations_co.out_stock_hist
SET buyer = trim(BOTH ' ' FROM REPLACE(REPLACE(REPLACE(buyer, '\t', ''), '\n',''),'\r',''));

UPDATE operations_co.out_stock_hist
SET planner =
IF (	out_stock_hist.buyer IN (
		"B0 - Jessica",
		"B3 - Maria Fernanda",
		"B4 - Freddy",
		"B5 - Katherine",
		"B6 - Ximena",
		"B8 - Guillermo",
		"B8 - Cristina"
	),
	"HOGAR",

IF (
	out_stock_hist.buyer IN (
		"B2 - Alexander",
		"B1 - Maritza"
	),
	"ELECTRONICA",
	"FASHION"
)
);


UPDATE operations_co.out_stock_hist 
 INNER JOIN wmsprod_co.itens_venda 
	ON out_stock_hist.stock_item_id = itens_venda.estoque_id
 INNER JOIN wmsprod_co.status_itens_venda 
	ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
SET out_stock_hist.fulfillment_type_real = "Crossdock"
WHERE status_itens_venda.status IN ("Analisando quebra","Aguardando estoque");

UPDATE operations_co.out_stock_hist 
SET out_stock_hist.fulfillment_type_real = "Consignation"
WHERE wh_location like '%CONSI%';

UPDATE operations_co.out_stock_hist
SET out_stock_hist.days_in_stock = 	CASE
																			WHEN 	date_exit IS NULL 
																			THEN	datediff(curdate(), date_entrance)
																			ELSE	datediff(date_exit, date_entrance)
																		END;
TRUNCATE operations_co.skus_age;

INSERT INTO operations_co.skus_age
select * from operations_co.vw_inv_avg_ageing;

UPDATE operations_co.out_stock_hist 
INNER JOIN operations_co.skus_age 
ON 
	out_stock_hist.sku_simple= skus_age.sku_simple 
SET 
	out_stock_hist.sku_age = truncate(skus_age.avg_age,0);


UPDATE operations_co.out_stock_hist 
 INNER JOIN operations_co.tbl_bi_ops_tracking_suppliers
	ON out_stock_hist.supplier_id = tbl_bi_ops_tracking_suppliers.id
SET out_stock_hist.tracker = tbl_bi_ops_tracking_suppliers.tracker_name;

/*
# Pendiente a revisar implementacion A_Master_Catalog
#Actualizar datos para pet status como DV wants
UPDATE operations_co.out_stock_hist
 INNER JOIN production_co.A_Master_Catalog 
	ON out_stock_hist.sku_simple 	= A_Master_Catalog.SkuSimple
SET 
	out_stock_hist.content_qc 			= A_Master_Catalog.QC,
	out_stock_hist.status_config 		= A_Master_Catalog.status_config,
	out_stock_hist.status_simple 		= A_Master_Catalog.status_simple,
	out_stock_hist.status_pet = IF(A_Master_Catalog.status_pet IS NULL, 'Pending All', 
IF(A_Master_Catalog.status_pet = "creation,edited", "Pending Images", 
IF(A_Master_Catalog.status_pet = "creation,images", "Pending Edited", 
IF(A_Master_Catalog.status_pet = "creation", "Pending EnI", 
IF(A_Master_Catalog.status_pet = "creation,edited,images", "Ok", "Error")))));

#Actualizar el sku status
UPDATE operations_co.out_stock_hist
SET out_stock_hist.status_sku = 
		IF(out_stock_hist.status_config="deleted" or out_stock_hist.status_simple="deleted", "Deleted", 
		IF(out_stock_hist.status_config="inactive" or out_stock_hist.status_simple="inactive", "Inactive", 
		IF(out_stock_hist.status_config="active" and out_stock_hist.status_simple="active", "Active", NULL)));

#Actualizar la razón de no visibilidad
UPDATE operations_co.out_stock_hist
SET out_stock_hist.reason_not_visible = 
		IF(out_stock_hist.status_pet <> "OK", out_stock_hist.status_pet, 
		IF(out_stock_hist.status_sku <> "active", out_stock_hist.status_sku, 
		IF(out_stock_hist.content_qc = 0, "Quality Check Failed", "Stock levels")))
WHERE visible = 0;

*/

UPDATE operations_co.out_stock_hist
SET 
 out_stock_hist.quarantine = 1,
 quarantine_type  = 	wh_location
WHERE	wh_location LIKE '%DEF%'
OR		wh_location LIKE '%REP%'
OR		wh_location LIKE '%GRT%'
OR		wh_location LIKE '%AVE%'
OR		wh_location LIKE '%cuerentena%'
OR		wh_location LIKE '%Cuarentena_Inverse%'
;

/*
# Cambié este query por el de abajo
UPDATE operations_co.out_stock_hist t 
  INNER JOIN (	SELECT 
								estoque_id, 
								para_endereco, 
								min(data_movimentacao) AS data_movimentacao 
							FROM
									(	SELECT 
											estoque_id, 
											para_endereco, 
											data_criacao AS data_movimentacao 
									 FROM wmsprod_co.movimentacoes 
										WHERE para_endereco LIKE '%DEF%' 
										OR 		para_endereco LIKE '%REP%' 
										OR		para_endereco LIKE '%GRT%' 
										OR 		para_endereco LIKE '%AVE%' 
										OR 		para_endereco LIKE '%cuerentena%') AS a
							GROUP BY estoque_id) AS t2
ON 	t.stock_item_id = t2.estoque_id
SET t.quarantine_date = t2.data_movimentacao;
*/



DROP TEMPORARY TABLE IF EXISTS tmp_movimentacoes;
CREATE TEMPORARY TABLE tmp_movimentacoes (PRIMARY KEY (estoque_id))
SELECT 
								estoque_id, 
								para_endereco, 
								min(date(data_criacao)) AS data_movimentacao 
							FROM wmsprod_co.movimentacoes 
							WHERE para_endereco LIKE '%DEF%' 
							OR 		para_endereco LIKE '%REP%' 
							OR		para_endereco LIKE '%GRT%' 
							OR 		para_endereco LIKE '%AVE%' 
							OR 		para_endereco LIKE '%cuerentena%'
							GROUP BY estoque_id;

UPDATE operations_co.out_stock_hist t 
  INNER JOIN tmp_movimentacoes t2
ON 	t.stock_item_id = t2.estoque_id
SET t.quarantine_date = t2.data_movimentacao;

UPDATE operations_co.out_stock_hist
SET 
 quarantine_date=date_entrance,
 quarantine_inbound = 1
WHERE quarantine=1 
and quarantine_date IS NULL;

UPDATE operations_co.out_stock_hist
 INNER JOIN bob_live_co.catalog_config 
	ON out_stock_hist.sku_config = catalog_config.sku
SET 
out_stock_hist.product_measures = catalog_config.product_measures, 
out_stock_hist.package_width = greatest(cast(replace(IF(isnull(catalog_config.package_width),1,catalog_config.package_width),',','.') as DECIMAL(6,2)),1.0), 
out_stock_hist.package_length = greatest(cast(replace(IF(isnull(catalog_config.package_length),1,catalog_config.package_length),',','.') as DECIMAL(6,2)),1.0), 
out_stock_hist.package_height = greatest(cast(replace(IF(isnull(catalog_config.package_height),1,catalog_config.package_height),',','.') as DECIMAL(6,2)),1.0),
out_stock_hist.package_weight = greatest(cast(replace(IF(isnull(catalog_config.package_weight),1,catalog_config.package_weight),',','.') as DECIMAL(6,2)),1.0)
;


UPDATE operations_co.out_stock_hist
SET out_stock_hist.vol_weight = package_height * package_length * package_width / 5000;

UPDATE operations_co.out_stock_hist
SET out_stock_hist.max_vol_w_vs_w = CASE
WHEN vol_weight > package_weight THEN
	vol_weight
ELSE
	package_weight
END;

/*
UPDATE operations_co.out_stock_hist
SET out_stock_hist.package_measure = CASE
WHEN max_vol_w_vs_w > 45.486 THEN
	"large"
ELSE
	(
		CASE
		WHEN max_vol_w_vs_w > 2.277 THEN
			"medium"
		ELSE
			"small"
		END
	)
END;
*/


UPDATE operations_co.out_stock_hist
SET out_stock_hist.package_measure_new = 	CASE
																						WHEN max_vol_w_vs_w > 35 
																						THEN 'oversized'
																					ELSE
																					(	CASE
																							WHEN max_vol_w_vs_w > 5 
																							THEN 'large'
																							ELSE
																							(	CASE
																									WHEN max_vol_w_vs_w > 2 
																									THEN 'medium'
																									ELSE 'small'
																								END)
																							END)
																					END;

																					
UPDATE operations_co.out_stock_hist
SET 
 out_stock_hist.sold_last_60 = CASE
																		WHEN datediff(curdate(), date_exit) < 60 
																		THEN 1
																		ELSE 0
																	END,
 out_stock_hist.sold_last_30 = CASE
																		WHEN datediff(curdate(), date_exit) < 30 
																		THEN 1
																		ELSE 0
																	END,
 out_stock_hist.sold_last_7 = CASE
																		WHEN datediff(curdate(), date_exit) < 7 
																		THEN 1
																		ELSE 0
																	END,
 out_stock_hist.sold_yesterday = 	CASE
																		WHEN datediff(curdate(),(date_sub(date_exit, INTERVAL 1 DAY))) = 0 
																		THEN 1
																		ELSE 0
																	END,
 out_stock_hist.entrance_last_30 = CASE
																		WHEN datediff(curdate(), date_entrance) < 30 
																		THEN 1
																		ELSE 0
																	END;

UPDATE operations_co.out_stock_hist
 INNER JOIN wmsprod_co.itens_venda 
 ON out_stock_hist.stock_item_id = itens_venda.estoque_id
 INNER JOIN wmsprod_co.status_itens_venda 
 ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
SET out_stock_hist.fulfillment_type_real = 'dropshipping'
WHERE status_itens_venda. STATUS IN ( 'ds estoque reservado', 'Waiting dropshipping');

UPDATE operations_co.out_stock_hist
 INNER JOIN wmsprod_co.itens_venda
 ON out_stock_hist.stock_item_id = itens_venda.estoque_id
 INNER JOIN wmsprod_co.status_itens_venda ON itens_venda.itens_venda_id = status_itens_venda.itens_venda_id
SET 
 out_stock_hist.fulfillment_type_real = 'Crossdocking'
WHERE status_itens_venda. STATUS IN ("analisando quebra","aguardando estoque");


UPDATE operations_co.out_stock_hist
 INNER JOIN wmsprod_co.movimentacoes ON out_stock_hist.stock_item_id = movimentacoes.estoque_id
SET out_stock_hist.fulfillment_type_real = 'Inventory'
WHERE
	movimentacoes.de_endereco = 'retorno de cancelamento'
OR movimentacoes.de_endereco = 'vendidos';

UPDATE operations_co.out_stock_hist
SET out_stock_hist.in_stock_cost_w_o_vat = out_stock_hist.cost_w_o_vat
WHERE
	out_stock_hist.in_stock = 1;

UPDATE operations_co.out_stock_hist
SET out_stock_hist.sold_last_30_cost_w_o_vat = out_stock_hist.cost_w_o_vat,
 out_stock_hist.sold_last_30_price = out_stock_hist.price
WHERE
	out_stock_hist.sold_last_30 = 1;


TRUNCATE operations_co.pro_stock_hist_sku_count;

INSERT INTO operations_co.pro_stock_hist_sku_count (
	sku_simple, 
	sku_count) 
SELECT
	out_stock_hist.sku_simple,
	sum(out_stock_hist.item_counter) AS sumofitem_counter
FROM
	operations_co.out_stock_hist
GROUP BY
	out_stock_hist.sku_simple,
	out_stock_hist.in_stock
HAVING
	out_stock_hist.in_stock = 1;

UPDATE operations_co.out_stock_hist
 INNER JOIN operations_co.pro_stock_hist_sku_count 
 ON out_stock_hist.sku_simple = pro_stock_hist_sku_count.sku_simple
SET out_stock_hist.sku_counter = 1 / pro_stock_hist_sku_count.sku_count;

TRUNCATE 
	operations_co.pro_stock_hist_sold_last_30_count;


INSERT INTO operations_co.pro_stock_hist_sold_last_30_count (
	sold_last_30,
	sku_simple,
	count_of_item_counter
) SELECT
	out_stock_hist.sold_last_30,
	out_stock_hist.sku_simple,
	count(out_stock_hist.item_counter) AS count_of_item_counter
FROM
	operations_co.out_stock_hist
GROUP BY
	out_stock_hist.sold_last_30,
	out_stock_hist.sku_simple
HAVING
	out_stock_hist.sold_last_30 = 1;


UPDATE operations_co.out_stock_hist
 INNER JOIN operations_co.pro_stock_hist_sold_last_30_count 
	ON out_stock_hist.sku_simple = pro_stock_hist_sold_last_30_count.sku_simple
SET out_stock_hist.sold_last_30_counter = 1 / pro_stock_hist_sold_last_30_count.count_of_item_counter;


UPDATE operations_co.out_stock_hist
SET out_stock_hist.position_item_size_type = CASE
WHEN position_type = "safe" THEN
	"small"
ELSE
	(
		CASE
		WHEN position_type = "mezanine" THEN
			"small"
		ELSE
			(
				CASE
				WHEN position_type = "muebles" THEN
					"large"
				ELSE
					"tbd"
				END
			)
		END
	)
END;


TRUNCATE	operations_co.items_procured_in_transit;

INSERT INTO operations_co.items_procured_in_transit (
	sku_simple,
	number_ordered,
	unit_price
) SELECT
	catalog_simple.sku,
	count(procurement_order_item.id_procurement_order_item) AS countofid_procurement_order_item,
	avg(procurement_order_item.unit_price) AS avgofunit_price
FROM
	bob_live_co.catalog_simple
 INNER JOIN procurement_live_co.procurement_order_item
ON catalog_simple.id_catalog_simple = procurement_order_item.fk_catalog_simple
	 INNER JOIN procurement_live_co.procurement_order 
ON procurement_order_item.fk_procurement_order = procurement_order.id_procurement_order
WHERE	procurement_order_item.is_deleted = 0
AND		procurement_order.is_cancelled = 0
AND		procurement_order_item.sku_received = 0
GROUP BY
	catalog_simple.sku;


UPDATE operations_co.out_stock_hist
 INNER JOIN operations_co.items_procured_in_transit 
	ON out_stock_hist.sku_simple = items_procured_in_transit.sku_simple
SET 
 out_stock_hist.items_procured_in_transit = number_ordered,
 out_stock_hist.procurement_price = unit_price;

/*
# Sacar de tabla de procurement
UPDATE operations_co.out_stock_hist
 INNER JOIN bob_live_co.catalog_simple
	ON out_stock_hist.sku_simple = catalog_simple.sku
 INNER JOIN procurement_live_co.procurement_order_item 
	ON catalog_simple.id_catalog_simple = procurement_order_item.fk_catalog_simple
 INNER JOIN  procurement_live_co.procurement_order
	ON procurement_order.id_procurement_order = procurement_order_item.fk_procurement_order
SET 
 out_stock_hist.date_procurement_order = procurement_order.created_at,
 out_stock_hist.week_procurement_order = operations_co.week_iso(procurement_order.created_at);
*/

/*
UPDATE operations_co.out_stock_hist
SET out_stock_hist.date_payable_promised = adddate(date_procurement_order,INTERVAL oms_payment_terms DAY)
WHERE	out_stock_hist.oms_payment_event IN ("factura", "pedido");


UPDATE operations_co.out_stock_hist
SET 
 out_stock_hist.date_payable_promised = adddate(date_procurement_order,INTERVAL oms_payment_terms + supplier_leadtime DAY),
 out_stock_hist.week_payable_promised = operations_co.week_iso(adddate(date_procurement_order,INTERVAL oms_payment_terms + supplier_leadtime DAY))
WHERE
	out_stock_hist.oms_payment_event = "entrega";
*/

UPDATE operations_co.out_stock_hist a
 INNER JOIN wmsprod_co.estoque b 
	ON a.stock_item_id = b.estoque_id
 INNER JOIN wmsprod_co.itens_recebimento c 
	ON b.itens_recebimento_id = c.itens_recebimento_id
 INNER JOIN wmsprod_co.recebimento d 
	ON c.recebimento_id = d.recebimento_id
SET 
	purchase_order = d.inbound_document_identificator;

CALL production.monitoring( "operations_co.out_procurement_tracking" , "Colombia",  240 );

UPDATE operations_co.out_stock_hist a 
 INNER JOIN operations_co.out_procurement_tracking b 
ON a.purchase_order=b.purchase_order and a.sku_simple=b.sku_simple 
SET
	a.cost_w_o_vat=b.cost_oms,
	a.purchase_order_type=b.purchase_order_type,
	a.payment_terms=b.payment_terms
WHERE b.is_cancelled=0 and b.is_deleted=0;


UPDATE operations_co.out_stock_hist a 
 INNER JOIN operations_co.tbl_purchase_order_stock b 
	ON a.purchase_order = b.oc 
	AND a.sku_simple = b.sku
SET
	a.cost_w_o_vat = b.costo,
	a.purchase_order_type = tipo_orden,
	a.payment_terms = b.tipo_pago;


UPDATE operations_co.out_stock_hist a
 INNER JOIN operations_co.tbl_purchase_order b 
	ON a.purchase_order = b.oc 
	AND a.sku_simple = b.sku
SET
	a.cost_w_o_vat = b.costo,
	a.purchase_order_type = 'Crossdock',
	a.payment_terms = b.tipo_pago;


/*
# Pendiente agregar columnas desde procurement:
*/

# Sales order item DATA

/*
Este también lo cambié por el de abajo
UPDATE
operations_co.out_stock_hist a 
 INNER JOIN 
(	SELECT 
		t.itens_venda_id,
		t.item_id,
		t.order_id,
		t.numero_order,
		t.data_pedido,
		t.estoque_id,
		t.data_criacao,
		t.status 
	FROM wmsprod_co.itens_venda t 
JOIN (SELECT 
				estoque_id, 
				max(data_criacao) AS max 
			FROM wmsprod_co.itens_venda 
			GROUP BY estoque_id) t2 
ON t.data_criacao=t2.max 
AND t.estoque_id=t2.estoque_id ) b  
ON a.stock_item_id = b.estoque_id
SET 
	a.order_nr = b.numero_order,
	a.date_ordered = b.data_pedido,
	a.status_item = b.status,
	a.item_id = b.item_id;
*/

DROP TEMPORARY TABLE IF EXISTS tmp_sales_for_stock;
CREATE TEMPORARY TABLE tmp_sales_for_stock ( INDEX (estoque_id))
SELECT 
		t.itens_venda_id,
		t.item_id,
		t.order_id,
		t.numero_order,
		t.data_pedido,
		t.estoque_id,
		t.data_criacao,
		t.status 
	FROM wmsprod_co.itens_venda t 
JOIN (SELECT 
				estoque_id, 
				max(data_criacao) AS max 
			FROM wmsprod_co.itens_venda 
			GROUP BY estoque_id) t2 
ON t.data_criacao=t2.max 
AND t.estoque_id=t2.estoque_id 
;

UPDATE	operations_co.out_stock_hist a 
 INNER JOIN tmp_sales_for_stock b
	ON a.stock_item_id = b.estoque_id
SET 
	a.order_nr = b.numero_order,
	a.date_ordered = b.data_pedido,
	a.status_item = b.status,
	a.item_id = b.item_id;

UPDATE operations_co.out_stock_hist 
SET 
	cancelled= 1,
	fulfillment_type_real='Inventory'
WHERE status_item IN (	'Cancelado', 
												'quebra tratada', 
												'quebrado', 
												'Precancelado', 
												'backorder_tratada') 
AND in_stock = 1 
AND reserved = 0;


#Próximos 3 queries chequean si el sku está en stock, cuando hay al menos un item disponible
TRUNCATE TABLE operations_co.skus_in_stock;

INSERT INTO operations_co.skus_in_stock
select distinct(sku_simple) from operations_co.out_stock_hist
WHERE in_stock=1 and reserved=0;

UPDATE operations_co.out_stock_hist
SET out_stock_hist.sku_in_stock=1
WHERE sku_simple in (select sku from operations_co.skus_in_stock);


UPDATE operations_co.out_stock_hist
SET sold_last_30_order = 	CASE
														WHEN datediff(curdate(), date_ordered) <= 30 
														THEN 1
														ELSE 0
													END
WHERE	cancelled <> 1;

UPDATE operations_co.out_stock_hist
SET 
 out_stock_hist.sold_last_30_order_cost_w_o_vat = out_stock_hist.cost_w_o_vat,
 out_stock_hist.sold_last_30_order_price = out_stock_hist.price
WHERE
	out_stock_hist.sold_last_30_order = 1;

# What is on the outlet
TRUNCATE TABLE operations_co.pro_skus_in_outlet;

INSERT INTO operations_co.pro_skus_in_outlet 
SELECT
	sku,
	NAME
FROM
	(
		SELECT
			cc.*, 
			lft,
			rgt,
			rgt - lft,
			c2.sku,
			c. NAME,
			c. STATUS
		FROM
			bob_live_co.catalog_config_has_catalog_category cc
		 INNER JOIN bob_live_co.catalog_category c ON id_catalog_category = fk_catalog_category
		 INNER JOIN bob_live_co.catalog_config c2 ON cc.fk_catalog_config = c2.id_catalog_config
		WHERE
			id_catalog_category > 1
		) AS a
WHERE
	a. NAME LIKE '%liquida%';

UPDATE operations_co.out_stock_hist a
 INNER JOIN operations_co.pro_skus_in_outlet b 
	ON a.sku_config = b.sku
SET outlet = 1
WHERE	a.in_stock = 1
AND 	a.reserved = 0;

#Sell list por config - 42 días de ventas
TRUNCATE operations_co.sell_rate_config;

INSERT INTO operations_co.sell_rate_config (
	sku_config,
	num_sales,
	num_items,
	average_days_in_stock
) SELECT
	sold.sku_config,
	sold.vendidos,
	instockreal.items_total,
	instockreal.days
FROM
	(	SELECT
			sku_config,
			sum(sold) AS vendidos
		FROM
			operations_co.out_stock_hist
		WHERE
			(	in_stock = 1
				OR exit_type = 'sold')
		AND date_exit BETWEEN CURDATE() - INTERVAL 42 DAY
		AND CURDATE()
		GROUP BY
			sku_config
	) AS sold
 INNER JOIN (
	SELECT
		sku_config,
		sum(item_counter) AS items_total,
		avg(days_in_stock) AS days
	FROM
		operations_co.out_stock_hist
	WHERE
		in_stock = 1
	GROUP BY
		sku_config
) AS instockreal ON sold.sku_config = instockreal.sku_config;

UPDATE operations_co.sell_rate_config
SET num_items_available = (
	SELECT
		sum(in_stock)
	FROM
		operations_co.out_stock_hist
	WHERE	reserved = 0
	AND 	sell_rate_config.sku_config = out_stock_hist.sku_config
	GROUP BY
		sku_config
);

UPDATE operations_co.sell_rate_config
SET average_sell_rate =
IF (num_sales = 0,NULL,	CASE
													WHEN average_days_in_stock = 0 
													THEN 0
													ELSE num_sales / 42
												END
);

UPDATE operations_co.sell_rate_config
SET remaining_days =	IF (average_sell_rate IS NULL,NULL,	CASE															
															WHEN average_sell_rate = 0 
															THEN 0
															ELSE num_items_available / average_sell_rate
															END
);


UPDATE operations_co.out_stock_hist
 INNER JOIN operations_co.sell_rate_config 
	ON out_stock_hist.sku_config = sell_rate_config.sku_config
SET out_stock_hist.average_remaining_days = sell_rate_config.remaining_days;

UPDATE operations_co.out_stock_hist 
 INNER JOIN operations_co.sell_rate_config 
	ON out_stock_hist.sku_config = sell_rate_config.sku_config 
SET 
	out_stock_hist.sales_rate_config_42 = ((sell_rate_config.num_sales/42)/sell_rate_config.num_items_available);


#Sell list por config - 30 días de ventas
TRUNCATE operations_co.sell_rate_config_30;

INSERT INTO operations_co.sell_rate_config_30 (
	sku_config,
	num_sales,
	num_items,
	average_days_in_stock
) SELECT
	sold.sku_config,
	sold.vendidos,
	instockreal.items_total,
	instockreal.days
FROM
	(	SELECT
			sku_config,
			sum(sold) AS vendidos
		FROM
			operations_co.out_stock_hist
		WHERE
			(	in_stock = 1
				OR exit_type = 'sold')
		AND date_exit BETWEEN CURDATE() - INTERVAL 30 DAY
		AND CURDATE()
		GROUP BY
			sku_config
	) AS sold
 INNER JOIN (
	SELECT
		sku_config,
		sum(item_counter) AS items_total,
		avg(days_in_stock) AS days
	FROM
		operations_co.out_stock_hist
	WHERE
		in_stock = 1
	GROUP BY
		sku_config
) AS instockreal ON sold.sku_config = instockreal.sku_config;

UPDATE operations_co.sell_rate_config_30
SET num_items_available = (
	SELECT
		sum(in_stock)
	FROM
		operations_co.out_stock_hist
	WHERE	reserved = 0
	AND 	sell_rate_config_30.sku_config = out_stock_hist.sku_config
	GROUP BY
		sku_config
);

UPDATE operations_co.sell_rate_config_30
SET average_sell_rate =
IF (num_sales = 0,NULL,	CASE
													WHEN average_days_in_stock = 0 
													THEN 0
													ELSE num_sales / 30
												END
);

UPDATE operations_co.sell_rate_config_30
SET remaining_days =	IF (average_sell_rate IS NULL,NULL,	CASE
																														WHEN average_sell_rate = 0 
																														THEN 0
																														ELSE num_items_available / average_sell_rate
																													END
																													);

UPDATE operations_co.out_stock_hist
 INNER JOIN operations_co.sell_rate_config_30 
	ON out_stock_hist.sku_config = sell_rate_config_30.sku_config
SET out_stock_hist.average_remaining_days_30 = sell_rate_config_30.remaining_days;

#Sell list por config - 7 días de ventas
TRUNCATE operations_co.sell_rate_config_7;

INSERT INTO operations_co.sell_rate_config_7 (
	sku_config,
	num_sales,
	num_items,
	average_days_in_stock
) SELECT
	sold.sku_config,
	sold.vendidos,
	instockreal.items_total,
	instockreal.days
FROM
	(	SELECT
			sku_config,
			sum(sold) AS vendidos
		FROM
			operations_co.out_stock_hist
		WHERE
			(	in_stock = 1
				OR exit_type = 'sold')
		AND date_exit BETWEEN CURDATE() - INTERVAL 7 DAY
		AND CURDATE()
		GROUP BY
			sku_config
	) AS sold
 INNER JOIN (
	SELECT
		sku_config,
		sum(item_counter) AS items_total,
		avg(days_in_stock) AS days
	FROM
		operations_co.out_stock_hist
	WHERE
		in_stock = 1
	GROUP BY
		sku_config
) AS instockreal ON sold.sku_config = instockreal.sku_config;

UPDATE operations_co.sell_rate_config_7
SET num_items_available = (
	SELECT
		sum(in_stock)
	FROM
		operations_co.out_stock_hist
	WHERE	reserved = 0
	AND 	sell_rate_config_7.sku_config = out_stock_hist.sku_config
	GROUP BY
		sku_config
);

UPDATE operations_co.sell_rate_config_7
SET average_sell_rate =
IF (num_sales = 0,NULL,	CASE
													WHEN average_days_in_stock = 0 
													THEN 0
													ELSE num_sales / 7
												END
);

UPDATE operations_co.sell_rate_config_7
SET remaining_days =	IF (average_sell_rate IS NULL,NULL,	CASE
																														WHEN average_sell_rate = 0 
																														THEN 0
																														ELSE num_items_available / average_sell_rate
																													END
																													);

UPDATE operations_co.out_stock_hist
 INNER JOIN operations_co.sell_rate_config_7 
	ON out_stock_hist.sku_config = sell_rate_config_7.sku_config
SET out_stock_hist.average_remaining_days_7 = sell_rate_config_7.remaining_days;

TRUNCATE	operations_co.pro_max_days_in_stock;

INSERT INTO operations_co.pro_max_days_in_stock(
	sku_simple,
	max_days_in_stock
)
SELECT
	sku_simple,
	max(days_in_stock)
FROM
	operations_co.out_stock_hist
GROUP BY
	sku_simple;


UPDATE operations_co.out_stock_hist
 INNER JOIN operations_co.pro_max_days_in_stock 
	ON out_stock_hist.sku_simple = pro_max_days_in_stock.sku_simple
SET out_stock_hist.max_days_in_stock = pro_max_days_in_stock.max_days_in_stock;

/*
UPDATE operations_co.out_stock_hist
SET out_stock_hist.days_payable_since_entrance = 
	CASE
		WHEN date_entrance IS NULL 
		THEN NULL
		ELSE	(	CASE
							WHEN oms_payment_event IN ('factura', 'pedido') 
							THEN oms_payment_terms - supplier_leadtime
							ELSE	(	CASE
												WHEN oms_payment_event = 'entrega' 
												THEN oms_payment_terms
											END)
						END)
	END;*/


UPDATE operations_co.out_stock_hist SET is_sell_list = 1
WHERE max_days_in_stock > 30
and in_stock= 1
and reserved= 0
and fulfillment_type_bob <> 'Consignment' 
and fulfillment_type_bob is not null
and category_1 = 'Home and Living' 
and (average_remaining_days > 150
or average_remaining_days IS NULL);

UPDATE operations_co.out_stock_hist SET is_sell_list = 1
WHERE max_days_in_stock > 30
and in_stock= 1
and reserved= 0
and fulfillment_type_bob <> 'Consignment' 
and fulfillment_type_bob is not null
and category_1 = 'Fashion' 
and (average_remaining_days > 120
or average_remaining_days IS NULL);

UPDATE operations_co.out_stock_hist SET is_sell_list = 1
WHERE max_days_in_stock > 30
and in_stock= 1
and reserved= 0
and fulfillment_type_bob <> 'Consignment' 
and fulfillment_type_bob is not null
and category_1 = 'Electrónicos' 
and (average_remaining_days > 60
or average_remaining_days IS NULL);

UPDATE operations_co.out_stock_hist SET is_sell_list = 1
WHERE max_days_in_stock > 30
and in_stock= 1
and reserved= 0
and fulfillment_type_bob <> 'Consignment' 
and fulfillment_type_bob is not null
and category_1 not in ('Home and Living','Fashion','Electrónicos') 
and (average_remaining_days > 90
or average_remaining_days IS NULL);

update operations_co.out_stock_hist
set 
out_stock_hist.interval_remaining_days=if(average_remaining_days is null,"Infinito",
if(average_remaining_days<7,"< 8",if(average_remaining_days<15,"8-15", if(average_remaining_days<30,"15-29", 
if(average_remaining_days<45,"30-44",if(average_remaining_days<60,"45-59",
if(average_remaining_days<90,"60-89",if(average_remaining_days<120,"90-119",
if(average_remaining_days<150,"120-149",if(average_remaining_days<180,"150-179",
if(average_remaining_days<210,"180-209",if(average_remaining_days<240,"210-239",
if(average_remaining_days<270,"240-269",if(average_remaining_days<300,"270-299",
if(average_remaining_days<330,"300-229",if(average_remaining_days<=360,"330-360","> 360"))))))))))))))));


SET @i = 0 ; 
DROP TEMPORARY TABLE IF EXISTS TMP ; 
CREATE TEMPORARY TABLE TMP 
SELECT
	sku_config,
	Count(*) AS items_in_stock,
	sum(cost_w_o_vat) AS cost_w_o_vat_in_stock
FROM
	operations_co.out_stock_hist
WHERE
	in_stock = 1
AND reserved = 0
AND (
			fulfillment_type_bob <> 'consignment'
			OR fulfillment_type_bob IS NOT NULL
		)
GROUP BY
	sku_config;

DROP TABLE IF EXISTS operations_co.top_skus_sell_list ; 
CREATE TABLE operations_co.top_skus_sell_list 
SELECT
	sku_config,
	items_in_stock,
	cost_w_o_vat_in_stock,
	@i := @i + 1 AS rkn
FROM
	TMP
ORDER BY
	cost_w_o_vat_in_stock DESC ;

# Sell List

INSERT INTO operations_co.pro_sell_list_hist_totals (
	date_reported,
	category_bp,
	cost_w_o_vat_over_two_items,
	cost_w_o_vat_under_two_items) 
SELECT
	J.date_reported,
	J.Categoria,
	J.Value_more_than_two,
	J.Value_less_than_two
FROM
	(	SELECT
			curdate() AS date_reported,
			B.category_bp AS 'Categoria',
			B.cost_w_o_vat AS Value_more_than_two,
			D.cost_w_o_vat AS Value_less_than_two
		FROM
			(	SELECT
					category_bp,
					sum(cost_w_o_vat) AS cost_w_o_vat
				FROM
					(	SELECT
							category_bp,
							sku_config,
							sum(cost_w_o_vat) AS cost_w_o_vat
						FROM
							operations_co.out_stock_hist
						WHERE
							is_sell_list = 1
						AND category_bp IS NOT NULL
						GROUP BY
							sku_config
						HAVING
							count(sku_config) > 2
					) A
				GROUP BY
					category_bp
			) B
		LEFT JOIN (	SELECT
									category_bp,
									sum(cost_w_o_vat) AS cost_w_o_vat
								FROM
									(	SELECT
											category_bp,
											sum(cost_w_o_vat) AS cost_w_o_vat
										FROM
											operations_co.out_stock_hist
										WHERE
											is_sell_list = 1
										GROUP BY
											sku_config
										HAVING
											count(sku_config) <= 2
									) C
								GROUP BY
									category_bp
									) D 
		ON B.category_bp = D.category_bp
	) J;



	
/*
DROP TABLE IF EXISTS production_co.out_stock_hist;

CREATE TABLE production_co.out_stock_hist LIKE operations_co.out_stock_hist; 

INSERT INTO production_co.out_stock_hist SELECT * FROM operations_co.out_stock_hist;
*/

INSERT INTO production.table_monitoring_log (
  country, 
  table_name,
  step, 
  UPDATEd_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'out_stock_hist',
  'finish',
  NOW(),
  max(date_exit),
  count(*),
  count(item_counter)
FROM
  production_co.out_stock_hist;

/*
DROP TABLE IF EXISTS production_co.items_procured_in_transit;

CREATE TABLE production_co.items_procured_in_transit LIKE operations_co.items_procured_in_transit;

INSERT INTO production_co.items_procured_in_transit SELECT * FROM operations_co.items_procured_in_transit; 

DROP TABLE IF EXISTS production_co.pro_sell_list_hist_totals;

CREATE TABLE production_co.pro_sell_list_hist_totals LIKE operations_co.pro_sell_list_hist_totals;

INSERT INTO production_co.pro_sell_list_hist_totals SELECT * FROM operations_co.pro_sell_list_hist_totals;

DROP TABLE IF EXISTS production_co.top_skus_sell_list;

CREATE TABLE production_co.top_skus_sell_list LIKE operations_co.top_skus_sell_list;

INSERT INTO production_co.top_skus_sell_list SELECT * FROM operations_co.top_skus_sell_list; */

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`rafael`@`%`*/ /*!50003 PROCEDURE `tbl_bi_ops_pc`()
BEGIN

INSERT INTO operations_co.tbl_bi_ops_pc(
SELECT 
	c.sku, 
	((	sh.precio_actual 
		+ IF(c.eligible_free_shipping = 1, 0, IF(sf.shipping_fee IS NULL, 0, sf.shipping_fee)))/(1+(c.tax_percent/100)) 
		- IF(sh.costo_actual IS NULL, 0, sh.costo_actual) 
		-	IF(c.delivery_cost_supplier IS NULL, 0, c.delivery_cost_supplier) 
		- IF(shc.shipping_cost IS NULL, 0, shc.shipping_cost) 
		- IF(sh.costo_actual IS NULL, 0, 0.025*sh.costo_actual)) AS pc,
	((	sh.precio_actual 
		+ IF(c.eligible_free_shipping = 1, 0, IF(sf.shipping_fee IS NULL, 0, sf.shipping_fee)))/(1+(c.tax_percent/100)) 
		- IF(sh.costo_actual IS NULL, 0, sh.costo_actual) 
		-	IF(c.delivery_cost_supplier IS NULL, 0, c.delivery_cost_supplier) 
		- IF(shc.shipping_cost IS NULL, 0, shc.shipping_cost) 
		- IF(sh.precio_actual IS NULL, 0, 0.025*(sh.precio_actual + IF(sf.shipping_fee IS NULL, 0, sf.shipping_fee)))/sh.precio_actual) AS pc_percentage
FROM tbl_catalog_product_v2 c
LEFT JOIN (	SELECT 
							sku_simple, 
							sum(ifnull(in_stock,0)) stock_wms
						FROM tbl_bi_ops_stock 
						WHERE reserved = 0 
						AND in_stock_real = 1 
						GROUP BY sku_simple) i 
ON i.sku_simple = c.sku
LEFT JOIN tbl_sku_precios_costos sh ON sh.sku = c.sku
LEFT JOIN tbl_sku_shipping_cost shc ON shc.sku = c.sku
LEFT JOIN tbl_sku_shipping_fees sf 	ON sf.sku = c.sku);

#Después de tener los datos en la tabla de pc se hace el cruce con stock
UPDATE operations_co.out_stock_hist
INNER JOIN operations_co.tbl_bi_ops_pc 
	ON out_stock_hist.sku_simple = tbl_bi_ops_pc.sku
SET 
	out_stock_hist.pc_15 = tbl_bi_ops_pc.pc_15,
	out_stock_hist.pc_15_percentage = tbl_bi_ops_pc.pc_15_percentage;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:04:07
