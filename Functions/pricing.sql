-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: pricing
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'pricing'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`gustavo.miranda`@`%`*/ /*!50003 PROCEDURE `adada`()
BEGIN
	
CREATE TABLE  pricing.Purchase_user_tm2  (INDEX (sku_simple))(

		SELECT
			m2.`name` AS category,
			m3.`name` AS sub_category,
			m4.`name` AS sub_sub_category,
			m6.`name` AS supplier,
			m5.`name` AS brand,
			m1.`name`,
			m1.model,
			m1.sku AS sku_config,
			m7.sku AS sku_simple,
			m7.price,
			m7.special_price,
			m10.tax_percent / 100 AS tax_percent,  
			m7.cost AS cost_bob,
			m7.cost / ( 1 + m10.tax_percent / 100 ) AS cost_after_tax,
			IF ((m7.delivery_cost_supplier) IS NULL, 0, (m7.delivery_cost_supplier)) AS delivery_cost_supplier,
			m7.fixed_price, #bob_live_mx.catalog_simple.fixed_price,
			m7.sell_list,    #bob_live_mx.catalog_simple.sell_list
			IFNULL( m8.quantity,0) + IFNULL(m9.quantity,0) AS stock,
			IFNULL( m9.quantity,0 ) AS warehouse_stock,
			m7.is_option_marketplace AS Marketplace,
			m1.sku_not_comparable
			
		FROM bob_live_mx.catalog_config m1
		LEFT JOIN bob_live_mx.catalog_simple m7 ON m1.id_catalog_config = m7.fk_catalog_config
		LEFT JOIN bob_live_mx.catalog_attribute_option_global_category m2 ON m1.fk_catalog_attribute_option_global_category = m2.id_catalog_attribute_option_global_category
		LEFT JOIN bob_live_mx.catalog_attribute_option_global_sub_category  m3 ON m1.fk_catalog_attribute_option_global_sub_category = m3.id_catalog_attribute_option_global_sub_category
		LEFT JOIN bob_live_mx.catalog_attribute_option_global_sub_sub_category m4 ON m1.fk_catalog_attribute_option_global_sub_sub_category = m4.id_catalog_attribute_option_global_sub_sub_category
		LEFT JOIN bob_live_mx.catalog_brand m5 ON m1.fk_catalog_brand = m5.id_catalog_brand
		LEFT JOIN bob_live_mx.catalog_supplier_stock m8 ON m7.id_catalog_simple = m8.fk_catalog_simple
		LEFT JOIN bob_live_mx.catalog_warehouse_stock m9 ON m7.id_catalog_simple = m9.fk_catalog_simple
		LEFT JOIN bob_live_mx.catalog_tax_class m10 ON m7.fk_catalog_tax_class = m10.id_catalog_tax_class
		LEFT JOIN bob_live_mx.catalog_source m11 ON m7.id_catalog_simple = m11.fk_catalog_simple
		LEFT JOIN bob_live_mx.supplier m6 ON m11.fk_supplier = m6.id_supplier
);


CREATE TABLE pricing.Purchase_user_tm1_two (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			sku_simple,
			name,
			category,
			brand,
			model,
			barccode,
			quantity,
			cost_per_unit,
			total_cost,
			estimated_selling_price,
			estimated_time_weeks,
			product_bought_before,
			IF (product_bought_before='yes', sku_simple, sku_simple_similar_product_sold) AS sku_simple_similar_product_sold,
			best_competitor_price,
			name_competitor,
			Final_Quantity,
			Final_Total_Cost

	
		FROM pricing.mx_cmr_prc_tbl_Purchase_user
		GROUP BY sku_simple
);

CREATE TABLE pricing.Purchase_user_tm3 (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			b.category, 
			b.sub_category, 
			b.sub_sub_category,
			NULL AS supplier_id,
			NULL AS supplier_name,
			a.sku_simple, 
			a.name,
			a.brand,
			a.model, 
			a.barccode, 
			a.quantity, 
			a.cost_per_unit, 
			a.total_cost, 
			a.estimated_selling_price, 
			a.estimated_time_weeks, 
			a.product_bought_before, 			
			0 AS not_in_restock, 
			a.best_competitor_price, 
			a.name_competitor,
			a.Final_Quantity,
			a.Final_Total_Cost,
			0 AS product_attack, 
			0 AS in_stock, 
			0 AS price_point_last_30, 
			a.sku_simple_similar_product_sold, 
			0 AS items_sku_sold, 
			b.price,
			IF (best_competitor_price > 0, ((best_competitor_price/1.16)*0.0169), IF (estimated_selling_price > 0, ((estimated_selling_price/1.16)*0.0169),((price/1.16)*0.0169))) AS payment_fee,
			IF (best_competitor_price > 0, ((best_competitor_price/1.16)*0.04), IF (estimated_selling_price > 0, ((estimated_selling_price/1.16)*0.04),((price/1.16)*0.04))) AS voucher_cost,
			#0 AS payment_fee,
			#0 AS voucher_cost,
			b.delivery_cost_supplier,
			b.cost_bob,
			b.tax_percent,
			b.warehouse_stock,
			b.Marketplace

		FROM pricing.Purchase_user_tm1_two a 
		LEFT JOIN pricing.Purchase_user_tm2 b ON a.sku_simple_similar_product_sold = b.sku_simple
		GROUP BY a.sku_simple
		);

CREATE TABLE pricing.Purchase_user_tm4 (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			a.category, 
			a.sub_category, 
			a.sub_sub_category,
			a.supplier_id,
			a.supplier_name,
			a.sku_simple, 
			a.name,
			a.brand,
			a.model, 
			a.barccode, 
			a.quantity, 
			a.cost_per_unit, 
			a.total_cost, 
			a.estimated_selling_price, 
			a.estimated_time_weeks, 
			a.product_bought_before, 			
			a.not_in_restock, 
			a.best_competitor_price, 
			a.name_competitor, 
			a.Final_Quantity,
			a.Final_Total_Cost,
			a.product_attack, 
			a.in_stock, 
			a.price_point_last_30, 
			a.sku_simple_similar_product_sold, 
			a.items_sku_sold, 
			a.price,
			a.payment_fee,
			a.voucher_cost,
			a.delivery_cost_supplier,
			a.cost_bob,
			a.tax_percent,
			a.warehouse_stock,
			a.Marketplace,
			b.shipping_fee,
			b.shipping_cost

		FROM pricing.Purchase_user_tm3 a 
		LEFT JOIN david_dana.shipping_fee_per_mx b ON a.sku_simple_similar_product_sold = b.sku_simple
		GROUP BY a.sku_simple
		);

CREATE TABLE pricing.Purchase_user_tm5 (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			category, 
			sub_category, 
			sub_sub_category,
			supplier_id,
			supplier_name,
			sku_simple, 
			name,
			brand,
			model, 
			barccode, 
			quantity, 
			cost_per_unit, 
			total_cost, 
			estimated_selling_price, 
			estimated_time_weeks, 
			product_bought_before, 			
			not_in_restock, 
			best_competitor_price, 
			name_competitor, 
			Final_Quantity,
			Final_Total_Cost,
			product_attack, 
			in_stock, 
			price_point_last_30, 
			sku_simple_similar_product_sold, 
			items_sku_sold, 
			price,
			payment_fee,
			voucher_cost,
			delivery_cost_supplier,
			cost_bob,
			tax_percent,
			warehouse_stock,
			Marketplace,
			IF (shipping_fee IS NULL,0,shipping_fee) AS shipping_fee,
			IF (shipping_cost IS NULL,0, shipping_cost) AS shipping_cost,
			IF (best_competitor_price > 0, best_competitor_price, IF(estimated_selling_price > 0, estimated_selling_price, price)) AS price_f,
			IF (cost_per_unit > 0, cost_per_unit, cost_bob) AS cost_f

		FROM pricing.Purchase_user_tm4 
		GROUP BY sku_simple
);

CREATE TABLE pricing.Purchase_user_tm6 (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			category, 
			sub_category, 
			sub_sub_category,
			supplier_id,
			supplier_name,
			sku_simple, 
			name,
			brand,
			model, 
			barccode, 
			quantity, 
			cost_per_unit, 
			total_cost, 
			estimated_selling_price, 
			estimated_time_weeks, 
			product_bought_before, 			
			not_in_restock, 
			best_competitor_price, 
			name_competitor,
			Final_Quantity,
			Final_Total_Cost,
			product_attack, 
			in_stock, 
			price_point_last_30, 
			sku_simple_similar_product_sold, 
			items_sku_sold, 
			price,
			payment_fee,
			voucher_cost,
			delivery_cost_supplier,
			cost_bob,
			tax_percent,
			warehouse_stock,
			Marketplace,
			shipping_fee,
			shipping_cost,
			price_f,
			cost_f,
			((price_f/1.16) + (shipping_fee/1.16) - (delivery_cost_supplier/1.16) - (cost_f/1.16) - (shipping_cost/1.16) - payment_fee - voucher_cost) AS PC15
			

		FROM pricing.Purchase_user_tm5
		GROUP BY sku_simple
);

CREATE TABLE pricing.Purchase_user_tm7 (INDEX (sku_simple))(

		SELECT
			'Linio' AS Competitor,
			m1.sku AS sku_config,
			m2.sku AS sku_simple,
			m7.`name` AS Brand,
			m1.`name`,
			m6.price_f,
			m3.`name` AS category,
			m4.`name` AS sub_category,
			m5.`name` AS sub_sub_category,
			m8.price_point_tier_1,
			m8.price_point_tier_2,
			m8.price_point_tier_3,
			m8.price_point_tier_4,
			m8.price_point_tier_5,
			m8.price_point_tier_6,
			m8.price_point_tier_7,

				CASE WHEN m6.price_f < m8.price_point_tier_1 THEN 1
				WHEN m6.price_f < m8.price_point_tier_2      THEN 2
				WHEN m6.price_f < m8.price_point_tier_3      THEN 3
				WHEN m6.price_f < m8.price_point_tier_4      THEN 4
				WHEN m6.price_f < m8.price_point_tier_5      THEN 5
				WHEN m6.price_f < m8.price_point_tier_6      THEN 6
				WHEN m6.price_f < m8.price_point_tier_7      THEN 7
				ELSE 8
				END AS price_point_tier

		FROM bob_live_mx.catalog_config m1
		INNER JOIN bob_live_mx.catalog_simple m2 ON m1.id_catalog_config = m2.fk_catalog_config
		INNER JOIN bob_live_mx.catalog_attribute_option_global_category m3 ON m1.fk_catalog_attribute_option_global_category = m3.id_catalog_attribute_option_global_category
		INNER JOIN bob_live_mx.catalog_attribute_option_global_sub_category m4 ON m1.fk_catalog_attribute_option_global_sub_category = m4.id_catalog_attribute_option_global_sub_category
		INNER JOIN bob_live_mx.catalog_attribute_option_global_sub_sub_category m5 ON m1.fk_catalog_attribute_option_global_sub_sub_category = m5.id_catalog_attribute_option_global_sub_sub_category
		INNER JOIN pricing.Purchase_user_tm5 m6 ON m2.sku = m6.sku_simple_similar_product_sold
		INNER JOIN bob_live_mx.catalog_brand m7 ON m1.fk_catalog_brand = m7.id_catalog_brand
		LEFT  JOIN david_dana.`price_points_per_category_key_mx` m8 ON m3.`name` = m8.category AND m4.`name` = m8.sub_category AND m5.`name` = m8.sub_sub_category

		GROUP BY m2.sku
);

DROP TABLE IF EXISTS pricing.warehouse_stock_v1;
CREATE TABLE pricing.warehouse_stock_v1 AS (

				SELECT 
					sku_simple,
					sum(in_stock) AS stock_wms, 
					sum(reserved) AS stock_reserved_wms

				FROM operations_mx.out_stock_hist
				GROUP BY sku_simple
		);

DROP TABLE IF EXISTS pricing.warehouse_stock_v2;
CREATE TABLE pricing.warehouse_stock_v2 AS (

				SELECT 
					sku_simple,
					(stock_wms - stock_reserved_wms) AS in_stock

				FROM pricing.warehouse_stock_v1
				GROUP BY sku_simple
		);

CREATE TABLE pricing.Purchase_user_tm8 (INDEX (sku_simple_similar_product_sold))(

		SELECT

			a.category, 
			a.sub_category, 
			a.sub_sub_category,
			a.supplier_id,
			a.supplier_name,
			a.sku_simple, 
			a.name,
			a.brand,
			a.model, 
			a.barccode, 
			a.quantity, 
			a.cost_per_unit, 
			a.total_cost, 
			a.estimated_selling_price, 
			a.estimated_time_weeks, 
			a.product_bought_before, 			
			a.not_in_restock, 
			a.best_competitor_price, 
			a.name_competitor,
			a.Final_Quantity,
			a.Final_Total_Cost,
			a.product_attack,  
			a.price_point_last_30, 
			a.sku_simple_similar_product_sold, 
			a.items_sku_sold, 
			a.price,
			a.payment_fee,
			a.voucher_cost,
			a.delivery_cost_supplier,
			a.cost_bob,
			a.tax_percent,
			a.warehouse_stock,
			a.Marketplace,
			a.shipping_fee,
			a.shipping_cost,
			a.price_f,
			a.cost_f,
			a.PC15,
			b.price_point_tier
			
		FROM pricing.Purchase_user_tm6 a
		INNER JOIN pricing.Purchase_user_tm7 b ON a.sku_simple_similar_product_sold = b.sku_simple
		GROUP BY a.sku_simple
		
);

CREATE TABLE pricing.Purchase_user_tm9 (INDEX (sku_simple_similar_product_sold))(

		SELECT
			
			b.category, 
			b.sub_category, 
			b.sub_sub_category,
			b.supplier_id,
			b.supplier_name,
			b.sku_simple, 
			b.name,
			b.brand,
			b.model, 
			b.barccode, 
			b.quantity, 
			b.cost_per_unit, 
			b.total_cost, 
			b.estimated_selling_price, 
			b.estimated_time_weeks, 
			b.product_bought_before, 			
			b.not_in_restock, 
			b.best_competitor_price, 
			b.name_competitor,
			b.Final_Quantity,
			b.Final_Total_Cost,
			b.product_attack,  
			b.price_point_last_30, 
			b.sku_simple_similar_product_sold, 
			b.items_sku_sold, 
			b.price,
			b.payment_fee,
			b.voucher_cost,
			b.delivery_cost_supplier,
			b.cost_bob,
			b.tax_percent,
			b.warehouse_stock,
			b.Marketplace,
			b.shipping_fee,
			b.shipping_cost,
			b.price_f,
			b.cost_f,
			b.PC15,
			b.price_point_tier,
			Sum(c.num_sales_30) AS sales_last_30_days_price_point,
			sum(d.in_stock) AS in_stock

FROM pricing.Purchase_user_tm7 a
INNER JOIN pricing.Purchase_user_tm8 b ON b.category = a.category AND b.sub_category = a.sub_category AND b.sub_sub_category = a.sub_sub_category AND b.price_point_tier = a.price_point_tier
LEFT JOIN david_dana.sku_actual_numbers_mx c ON a.sku_simple = c.SKUSimple
LEFT JOIN pricing.warehouse_stock_v2 d ON a.sku_simple = d.sku_simple
GROUP BY  b.category, b.sub_category,b.sub_sub_category, b.price_point_tier
		
);

CREATE TABLE pricing.Purchase_Request_user (INDEX (sku_simple_similar_product_sold))(

		SELECT
				
				
				#b.supplier_id,
				#b.supplier_name,
				b.sku_simple,
				b.`name`,
				b.category,
				b.brand,
				b.model,
				b.barccode,
				b.quantity,
				b.cost_per_unit, 
				b.total_cost,
				b.estimated_selling_price,
				b.estimated_time_weeks,
				b.product_bought_before,
				'' AS nada,
				b.sku_simple_similar_product_sold,
				b.best_competitor_price,
				IF (b.best_competitor_price = 0,'No Competitor',b.name_competitor) AS name_competitor,
				b.Final_Quantity,
				b.Final_Total_Cost,
				b.sub_category,
				b.sub_sub_category,
				b.PC15,
				b.price_point_tier,
				a.in_stock,
				a.sales_last_30_days_price_point AS price_point_last_30,
				b.items_sku_sold
				#b.not_in_restock

		FROM pricing.Purchase_user_tm9 a
		INNER JOIN pricing.Purchase_user_tm8 b ON a.category = b.category AND a.sub_category = b.sub_category AND a.sub_sub_category = b.sub_sub_category AND a.price_point_tier = b.price_point_tier
		
);

DROP TABLE IF EXISTS pricing.sales_SKUSimple;

		CREATE TABLE pricing.sales_SKUSimple (INDEX (sku_simple))(

			SELECT
				SKUSimple AS sku_simple,
				Count(*) AS NumItems,
				Sum(IF(CouponCode NOT IN ('REC%','MKT%','CAC%'),Rev,Rev+CouponValueAfterTax)) AS TotalRev,
				Avg(IF(CouponCode NOT IN ('REC%','MKT%','CAC%'),PCOnePFive,PCOnePFive+CouponValueAfterTax)) AS PCOnePFive,
				Avg (PaidPrice) AS Paid_Price
			
			FROM development_mx.A_Master
			WHERE DATEDIFF(Curdate(),Date) <= 30
				AND OrderAfterCan = 1
			GROUP BY SKUSimple
);

DROP TABLE IF EXISTS pricing.Days_in_stock_p;

		CREATE TABLE pricing.Days_in_stock_p (INDEX (sku_simple))(

			SELECT
				sku_simple,
				avg(days_in_stock) AS Avg_Days_in_WH

			FROM	operations_mx.out_stock_hist
			WHERE in_stock =1
			GROUP BY sku_simple
);

DROP TABLE IF EXISTS pricing.mx_cmr_prc_tbl_Purchase_Request_finaly;

CREATE TABLE pricing.mx_cmr_prc_tbl_Purchase_Request_finaly (INDEX (sku_simple_similar_product_sold))(

		SELECT
		
				b.sku_simple,
				b.`name`,
				b.category,
				b.brand,
				b.model,
				b.barccode,
				b.quantity,
				b.cost_per_unit,
				b.total_cost,
				b.estimated_selling_price,
				b.estimated_time_weeks,
				b.product_bought_before,
				'' AS nada,
				b.sku_simple_similar_product_sold,
				b.best_competitor_price, 
				b.name_competitor,
				b.sub_category,
				b.sub_sub_category,
				b.PC15,
				b.price_point_tier,
				IF (b.in_stock IS NULL,0,b.in_stock) AS in_stock,
				IF (b.price_point_last_30 IS NULL,0,b.price_point_last_30) AS price_point_last_30,
				'' AS nada2,
				#b.items_sku_sold,
				IF (c.NumItems IS NULL,0,c.NumItems) AS NumItems,
				IF (c.TotalRev IS NULL,0,c.TotalRev) AS TotalRev,
				IF (c.PCOnePFive IS NULL,0,c.PCOnePFive) AS PCOnePFive,
				IF (c.Paid_Price IS NULL,0,c.Paid_Price) AS Paid_Price,
				IF (d.Avg_Days_in_WH IS NULL,0,d.Avg_Days_in_WH) AS Avg_Days_in_WH,
				0 AS Real_stock,
				b.Final_Quantity,
				b.Final_Total_Cost

		FROM pricing.Purchase_Request_user b
		LEFT JOIN pricing.sales_SKUSimple c ON b.sku_simple_similar_product_sold=c.sku_simple
		LEFT JOIN pricing.Days_in_stock_p d ON b.sku_simple_similar_product_sold=d.sku_simple
		GROUP BY b.sku_simple
);

UPDATE pricing.mx_cmr_prc_tbl_Purchase_Request_finaly a
				INNER JOIN david_dana.mx_cmr_mp_tbl_real_stock b ON (a.sku_simple = b.sku_simple)
										SET a.Real_stock = b.Real_stock;



DROP TABLE IF EXISTS pricing.Purchase_user_tm1_two;
DROP TABLE IF EXISTS pricing.Purchase_user_tm2;
DROP TABLE IF EXISTS pricing.Purchase_user_tm3;
DROP TABLE IF EXISTS pricing.Purchase_user_tm4;
DROP TABLE IF EXISTS pricing.Purchase_user_tm5;
DROP TABLE IF EXISTS pricing.Purchase_user_tm6;
DROP TABLE IF EXISTS pricing.Purchase_user_tm7;
DROP TABLE IF EXISTS pricing.Purchase_user_tm8;
DROP TABLE IF EXISTS pricing.Purchase_user_tm9;
DROP TABLE IF EXISTS pricing.Purchase_Request_user;

TRUNCATE TABLE pricing.mx_cmr_prc_tbl_Purchase_user;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`gustavo.miranda`@`%`*/ /*!50003 PROCEDURE `co_cmr_prc_sp_cleaning_price_tool`()
BEGIN
	
DROP TABLE IF EXISTS pricing.co_cmr_prc_tbl_price_tool_tmp2;

DROP TABLE IF EXISTS pricing.co_cmr_prc_tbl_price_tool_tmp1;

TRUNCATE TABLE pricing.co_cmr_prc_tbl_catalog_user_price;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`gustavo.miranda`@`%`*/ /*!50003 PROCEDURE `co_cmr_prc_sp_cleaning_Purchase_request`()
BEGIN
	
DROP TABLE IF EXISTS pricing.Purchase_user_tm1_two_co;
DROP TABLE IF EXISTS pricing.Purchase_user_tm2_co;
DROP TABLE IF EXISTS pricing.Purchase_user_tm3_co;
DROP TABLE IF EXISTS pricing.Purchase_user_tm4_co;
DROP TABLE IF EXISTS pricing.Purchase_user_tm5_co;
DROP TABLE IF EXISTS pricing.Purchase_user_tm6_co;
DROP TABLE IF EXISTS pricing.Purchase_user_tm7_co;
DROP TABLE IF EXISTS pricing.Purchase_user_tm8_co;
DROP TABLE IF EXISTS pricing.Purchase_user_tm9_co;
DROP TABLE IF EXISTS pricing.Purchase_Request_user_co;

TRUNCATE TABLE pricing.co_cmr_prc_tbl_Purchase_user;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`gustavo.miranda`@`%`*/ /*!50003 PROCEDURE `co_cmr_prc_sp_price_change_tool`()
BEGIN
	
 DROP TABLE IF EXISTS pricing.co_cmr_prc_tbl_catalog_price_comple;

CREATE TABLE pricing.co_cmr_prc_tbl_catalog_price_comple (INDEX(sku_config), INDEX(sku_simple))(

	SELECT 

		a.sku AS sku_config,
		b.sku AS sku_simple,
		b.fixed_price, # corregir cuando el atributo este en bob
		0 AS mp,
		c.isMarketPlace AS Marketplace,
		IF (b.cost IS NULL, 0, b.cost) AS cost,
		IF (b.special_price IS NULL, 0, b.special_price) AS special_price,
		IF (b.price IS NULL, 0, b.price) AS price
		
		
	FROM bob_live_co.catalog_config a
	INNER JOIN bob_live_co.catalog_simple b ON (a.id_catalog_config = b.fk_catalog_config)
	INNER JOIN development_co_project.A_Master_Catalog c ON (c.sku_simple = b.sku)
	GROUP BY b.sku
);

UPDATE pricing.co_cmr_prc_tbl_catalog_price_comple a
INNER JOIN pricing.co_cmr_prc_tbl_tmp_price_tool b ON (a.sku_config = b.sku_config)
SET a.mp = 1;

TRUNCATE TABLE pricing.co_cmr_prc_tbl_catalog_user_price_tmp1;

		INSERT INTO pricing.co_cmr_prc_tbl_catalog_user_price_tmp1 (

			sku_config,
			sku_simple,
			cost,
			special_price,
			price
)
				SELECT 
					
					sku_config,
					sku_simple,
					IF (cost = ' ', -1, cost) AS cost,
					IF (special_price = ' ', -1, special_price) AS special_price,
					IF (price = ' ', -1, price) AS price


				FROM pricing.co_cmr_prc_tbl_catalog_user_price;


DROP TABLE IF EXISTS pricing.co_cmr_prc_tbl_catalog_price_desicion;

CREATE TABLE pricing.co_cmr_prc_tbl_catalog_price_desicion (INDEX(sku_config), INDEX(sku_simple))(

	SELECT 

		a.sku_config,
		a.sku_simple,
		b.fixed_price,
		b.Marketplace,
		b.mp,
		IF (a.price = -1, b.price, a.price) AS price,
		a.price AS price_new,
		b.price AS price_old,
		IF (a.special_price = -1, b.special_price, a.special_price) AS special_price,
		a.special_price AS special_price_new,
		b.special_price AS special_price_old,
		IF (a.cost = -1, b.cost, a.cost) AS cost,
		a.cost AS cost_new,
		b.cost AS cost_old
		
	FROM pricing.co_cmr_prc_tbl_catalog_user_price_tmp1 a
	LEFT JOIN pricing.co_cmr_prc_tbl_catalog_price_comple b ON (a.sku_simple = b.sku_simple)
	GROUP BY a.sku_simple
);

CREATE TABLE pricing.co_cmr_prc_tbl_price_tool_tmp1 (INDEX(sku_config), INDEX(sku_simple))(

	SELECT 

		sku_config,
		sku_simple,
		fixed_price,
		Marketplace,
		mp,
		cost,
		special_price,
		price,

		IF ( cost = 0 AND price = 0, 'Check, Cost = 0 and Price = 0',
		IF ( cost = 0, 'Check, Cost = 0',
		IF ( price = 0, 'Check, price = 0',
		IF ( special_price = 0, 'Check, special_price = 0',
		IF ( ( (cost_old) * 8 ) < cost AND cost < ( (cost_old) * 1.2 ), 'Check, The cost is very large',
		IF ( price > ( (cost) * 10 ), 'Check, High price', 
		IF ( special_price > ( (cost) * 10 ), 'Check, High special_price',
		IF ( cost <= special_price AND cost <= price AND special_price < price, 'Ok',
		IF ( cost <= special_price AND cost <= price AND special_price >= price, 'Check, special_price >= price',
		IF ( cost <= special_price AND cost > price AND special_price < price, 'Check, price < cost',
		IF ( cost > special_price AND cost <= price AND special_price < price, 'Check, special_price < cost',
		IF ( cost <= special_price AND cost > price AND special_price >= price, 'Check, special_price >= price and price < cost',
		IF ( cost > special_price AND cost <= price AND special_price >= price, 'Check, special_price >= price and special_price < cost',
		IF ( cost > special_price AND cost > price AND special_price < price, 'Check, price < cost and special_price < cost', 'Check, price < cost, special_price < cost and special_price >= price'
		)))))))))))))) AS Action_result,
		
		IF ( Marketplace = 1, 'Marketplace Product',
		IF ( fixed_price = 1, 'Fixed Price',
		IF ( mp = 1, 'Marketing Plan',
		IF ( cost = 0 AND price = 0, 'Pricing',
		IF ( cost = 0, 'Pricing',
		IF ( price = 0, 'Pricing',
		IF ( special_price = 0, 'Pricing',
		IF ( ( (cost_old) * 8 ) < cost AND cost < ( (cost_old) * 1.2 ), 'Pricing',
		IF ( price > ( (cost) * 10 ), 'Pricing', 
		IF ( special_price > ( (cost) * 10 ), 'Pricing',
		IF ( cost <= special_price AND cost <= price AND special_price < price, 'Ok',
		IF ( cost <= special_price AND cost <= price AND special_price >= price, 'Pricing',
		IF ( cost <= special_price AND cost > price AND special_price < price, 'Pricing',
		IF ( cost > special_price AND cost <= price AND special_price < price, 'Pricing',
		IF ( cost <= special_price AND cost > price AND special_price >= price, 'Pricing',
		IF ( cost > special_price AND cost <= price AND special_price >= price, 'Pricing',
		IF ( cost > special_price AND cost > price AND special_price < price, 'Pricing', 'Pricing'
		))))))))))))))))) AS Check_Final
	
	FROM pricing.co_cmr_prc_tbl_catalog_price_desicion
	GROUP BY sku_simple
);

CREATE TABLE pricing.co_cmr_prc_tbl_price_tool_tmp2 (INDEX(sku_config), INDEX(sku_simple))(

	SELECT 

		a.sku_config,
		a.sku_simple,
		IF (b.cost = -1 , ' ', b.cost) AS cost_order,
		IF (b.special_price = -1, ' ', b.special_price) AS special_price_order,
		IF (b.price = -1, ' ', b.price) AS price_order,
		IF (a.cost = 0, ' ', a.cost) AS cost_db,
		IF (a.special_price = 0, ' ', a.special_price) AS special_price_db,
		IF (a.price = 0, ' ', a.price) AS price_db,
		a.Action_result,
		a.Check_Final

	FROM pricing.co_cmr_prc_tbl_price_tool_tmp1 a
	LEFT JOIN pricing.co_cmr_prc_tbl_catalog_user_price b ON (a.sku_simple = b.sku_simple)
	GROUP BY a.sku_simple
);

TRUNCATE TABLE pricing.co_cmr_prc_tbl_price_tool;

		INSERT INTO pricing.co_cmr_prc_tbl_price_tool (

			sku_config,
			sku_simple,
			cost_order,
			special_price_order,
			price_order,
			cost_db,
			special_price_db,
			price_db,
			Action_result,
			Check_Final
)

				SELECT 
					
					sku_config,
					sku_simple,
					cost_order,
					special_price_order,
					price_order,
					cost_db,
					special_price_db,
					price_db,
					Action_result,
					Check_Final

				FROM pricing.co_cmr_prc_tbl_price_tool_tmp2;


DROP TABLE IF EXISTS pricing.co_cmr_prc_tbl_price_tool_tmp2;

DROP TABLE IF EXISTS pricing.co_cmr_prc_tbl_price_tool_tmp1;

TRUNCATE TABLE pricing.co_cmr_prc_tbl_catalog_user_price;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`gustavo.miranda`@`%`*/ /*!50003 PROCEDURE `co_mcr_prc_sp_Purchase_Request`()
BEGIN
	
CREATE TABLE  pricing.Purchase_user_tm2_co  (INDEX (sku_simple))(
	
		SELECT
			m2.`name` AS category,
			m3.`name` AS sub_category,
			m4.`name` AS sub_sub_category,
			m6.`name` AS supplier,
			m5.`name` AS brand,
			m1.`name`,
			m1.model,
			m1.sku AS sku_config,
			m7.sku AS sku_simple,
			m7.price,
			m7.special_price,
			m10.tax_percent / 100 AS tax_percent,  
			m7.cost AS cost_bob,
			m7.cost / ( 1 + m10.tax_percent / 100 ) AS cost_after_tax,
			IF ((m7.delivery_cost_supplier) IS NULL, 0, (m7.delivery_cost_supplier)) AS delivery_cost_supplier,
			0 as fixed_price, #bob_live_mx.catalog_simple.fixed_price, --- corregir m7
			0 AS sell_list,    #bob_live_mx.catalog_simple.sell_list -- corregir m7
			IFNULL( m8.quantity,0) + IFNULL(m9.quantity,0) AS stock,
			IFNULL( m9.quantity,0 ) AS warehouse_stock,
			m7.is_option_marketplace AS Marketplace,
			0 AS sku_not_comparable # corregir m1
			
		FROM bob_live_co.catalog_config m1
		INNER JOIN bob_live_co.catalog_attribute_option_global_category m2 ON m1.fk_catalog_attribute_option_global_category = m2.id_catalog_attribute_option_global_category
		INNER JOIN bob_live_co.catalog_attribute_option_global_sub_category  m3 ON m1.fk_catalog_attribute_option_global_sub_category = m3.id_catalog_attribute_option_global_sub_category
		INNER JOIN bob_live_co.catalog_attribute_option_global_sub_sub_category m4 ON m1.fk_catalog_attribute_option_global_sub_sub_category = m4.id_catalog_attribute_option_global_sub_sub_category
		INNER JOIN bob_live_co.catalog_brand m5 ON m1.fk_catalog_brand = m5.id_catalog_brand
		INNER JOIN bob_live_co.catalog_simple m7 ON m1.id_catalog_config = m7.fk_catalog_config
		LEFT JOIN bob_live_co.catalog_supplier_stock m8 ON m7.id_catalog_simple = m8.fk_catalog_simple
		LEFT JOIN bob_live_co.catalog_warehouse_stock m9 ON m7.id_catalog_simple = m9.fk_catalog_simple
		INNER JOIN bob_live_co.catalog_tax_class m10 ON m7.fk_catalog_tax_class = m10.id_catalog_tax_class
		INNER JOIN bob_live_co.catalog_source m11 ON m7.id_catalog_simple = m11.fk_catalog_simple
		INNER JOIN bob_live_co.supplier m6 ON m11.fk_supplier = m6.id_supplier
		);


CREATE TABLE pricing.Purchase_user_tm1_two_co (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			sku_simple,
			name,
			category,
			brand,
			model,
			barccode,
			quantity,
			cost_per_unit,
			total_cost,
			estimated_selling_price,
			estimated_time_weeks,
			product_bought_before,
			IF (product_bought_before='yes', sku_simple, sku_simple_similar_product_sold) AS sku_simple_similar_product_sold,
			best_competitor_price,
			name_competitor,
			Final_Quantity,
			Final_Total_Cost

	
		FROM pricing.co_cmr_prc_tbl_Purchase_user
		GROUP BY sku_simple
);

CREATE TABLE pricing.Purchase_user_tm3_co (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			b.category, 
			b.sub_category, 
			b.sub_sub_category,
			NULL AS supplier_id,
			NULL AS supplier_name,
			a.sku_simple, 
			a.name,
			a.brand,
			a.model, 
			a.barccode, 
			a.quantity, 
			a.cost_per_unit, 
			a.total_cost, 
			a.estimated_selling_price, 
			a.estimated_time_weeks, 
			a.product_bought_before, 			
			0 AS not_in_restock, 
			a.best_competitor_price, 
			a.name_competitor,
			a.Final_Quantity,
			a.Final_Total_Cost,
			0 AS product_attack, 
			0 AS in_stock, 
			0 AS price_point_last_30, 
			a.sku_simple_similar_product_sold, 
			0 AS items_sku_sold, 
			b.price,
			IF (best_competitor_price > 0, ((best_competitor_price/1.16)*0.0169), IF (estimated_selling_price > 0, ((estimated_selling_price/1.16)*0.0169),((price/1.16)*0.0169))) AS payment_fee,
			IF (best_competitor_price > 0, ((best_competitor_price/1.16)*0.04), IF (estimated_selling_price > 0, ((estimated_selling_price/1.16)*0.04),((price/1.16)*0.04))) AS voucher_cost,
			#0 AS payment_fee,
			#0 AS voucher_cost,
			b.delivery_cost_supplier,
			b.cost_bob,
			b.tax_percent,
			b.warehouse_stock,
			b.Marketplace

		FROM pricing.Purchase_user_tm1_two_co a 
		LEFT JOIN pricing.Purchase_user_tm2_co b ON a.sku_simple_similar_product_sold = b.sku_simple
		GROUP BY a.sku_simple
		);

CREATE TABLE pricing.Purchase_user_tm4_co (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			a.category, 
			a.sub_category, 
			a.sub_sub_category,
			a.supplier_id,
			a.supplier_name,
			a.sku_simple, 
			a.name,
			a.brand,
			a.model, 
			a.barccode, 
			a.quantity, 
			a.cost_per_unit, 
			a.total_cost, 
			a.estimated_selling_price, 
			a.estimated_time_weeks, 
			a.product_bought_before, 			
			a.not_in_restock, 
			a.best_competitor_price, 
			a.name_competitor, 
			a.Final_Quantity,
			a.Final_Total_Cost,
			a.product_attack, 
			a.in_stock, 
			a.price_point_last_30, 
			a.sku_simple_similar_product_sold, 
			a.items_sku_sold, 
			a.price,
			a.payment_fee,
			a.voucher_cost,
			a.delivery_cost_supplier,
			a.cost_bob,
			a.tax_percent,
			a.warehouse_stock,
			a.Marketplace,
			b.shipping_fee,
			b.shipping_cost

		FROM pricing.Purchase_user_tm3_co a 
		LEFT JOIN david_dana.shipping_fee_per_sku_co b ON a.sku_simple_similar_product_sold = b.sku_simple
		GROUP BY a.sku_simple
		);

CREATE TABLE pricing.Purchase_user_tm5_co (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			category, 
			sub_category, 
			sub_sub_category,
			supplier_id,
			supplier_name,
			sku_simple, 
			name,
			brand,
			model, 
			barccode, 
			quantity, 
			cost_per_unit, 
			total_cost, 
			estimated_selling_price, 
			estimated_time_weeks, 
			product_bought_before, 			
			not_in_restock, 
			best_competitor_price, 
			name_competitor, 
			Final_Quantity,
			Final_Total_Cost,
			product_attack, 
			in_stock, 
			price_point_last_30, 
			sku_simple_similar_product_sold, 
			items_sku_sold, 
			price,
			payment_fee,
			voucher_cost,
			delivery_cost_supplier,
			cost_bob,
			tax_percent,
			warehouse_stock,
			Marketplace,
			IF (shipping_fee IS NULL,0,shipping_fee) AS shipping_fee,
			IF (shipping_cost IS NULL,0, shipping_cost) AS shipping_cost,
			IF (best_competitor_price > 0, best_competitor_price, IF(estimated_selling_price > 0, estimated_selling_price, price)) AS price_f,
			IF (cost_per_unit > 0, cost_per_unit, cost_bob) AS cost_f

		FROM pricing.Purchase_user_tm4_co 
		GROUP BY sku_simple
);

CREATE TABLE pricing.Purchase_user_tm6_co (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			category, 
			sub_category, 
			sub_sub_category,
			supplier_id,
			supplier_name,
			sku_simple, 
			name,
			brand,
			model, 
			barccode, 
			quantity, 
			cost_per_unit, 
			total_cost, 
			estimated_selling_price, 
			estimated_time_weeks, 
			product_bought_before, 			
			not_in_restock, 
			best_competitor_price, 
			name_competitor,
			Final_Quantity,
			Final_Total_Cost,
			product_attack, 
			in_stock, 
			price_point_last_30, 
			sku_simple_similar_product_sold, 
			items_sku_sold, 
			price,
			payment_fee,
			voucher_cost,
			delivery_cost_supplier,
			cost_bob,
			tax_percent,
			warehouse_stock,
			Marketplace,
			shipping_fee,
			shipping_cost,
			price_f,
			cost_f,
			((price_f/1.16) + (shipping_fee/1.16) - (delivery_cost_supplier/1.16) - (cost_f/1.16) - (shipping_cost/1.16) - payment_fee - voucher_cost) AS PC15
			

		FROM pricing.Purchase_user_tm5_co
		GROUP BY sku_simple
);

CREATE TABLE pricing.Purchase_user_tm7_co (INDEX (sku_simple))(

		SELECT
			'Linio' AS Competitor,
			m1.sku AS sku_config,
			m2.sku AS sku_simple,
			m7.`name` AS Brand,
			m1.`name`,
			m6.price_f,
			m3.`name` AS category,
			m4.`name` AS sub_category,
			m5.`name` AS sub_sub_category,
			m8.price_point_tier_1,
			m8.price_point_tier_2,
			m8.price_point_tier_3,
			m8.price_point_tier_4,
			m8.price_point_tier_5,
			m8.price_point_tier_6,
			m8.price_point_tier_7,

				CASE WHEN m6.price_f < m8.price_point_tier_1 THEN 1
				WHEN m6.price_f < m8.price_point_tier_2      THEN 2
				WHEN m6.price_f < m8.price_point_tier_3      THEN 3
				WHEN m6.price_f < m8.price_point_tier_4      THEN 4
				WHEN m6.price_f < m8.price_point_tier_5      THEN 5
				WHEN m6.price_f < m8.price_point_tier_6      THEN 6
				WHEN m6.price_f < m8.price_point_tier_7      THEN 7
				ELSE 8
				END AS price_point_tier

		FROM bob_live_co.catalog_config m1
		INNER JOIN bob_live_co.catalog_simple m2 ON m1.id_catalog_config = m2.fk_catalog_config
		INNER JOIN bob_live_co.catalog_attribute_option_global_category m3 ON m1.fk_catalog_attribute_option_global_category = m3.id_catalog_attribute_option_global_category
		INNER JOIN bob_live_co.catalog_attribute_option_global_sub_category m4 ON m1.fk_catalog_attribute_option_global_sub_category = m4.id_catalog_attribute_option_global_sub_category
		INNER JOIN bob_live_co.catalog_attribute_option_global_sub_sub_category m5 ON m1.fk_catalog_attribute_option_global_sub_sub_category = m5.id_catalog_attribute_option_global_sub_sub_category
		INNER JOIN pricing.Purchase_user_tm5_co m6 ON m2.sku = m6.sku_simple_similar_product_sold
		INNER JOIN bob_live_co.catalog_brand m7 ON m1.fk_catalog_brand = m7.id_catalog_brand
		LEFT  JOIN david_dana.`price_points_per_category_key_co` m8 ON m3.`name` = m8.category AND m4.`name` = m8.sub_category AND m5.`name` = m8.sub_sub_category

		GROUP BY m2.sku
);

DROP TABLE IF EXISTS pricing.warehouse_stock_v1_co;
CREATE TABLE pricing.warehouse_stock_v1_co AS (

				SELECT 
					sku_simple,
					sum(in_stock) AS stock_wms, 
					sum(reserved) AS stock_reserved_wms

				FROM operations_co.out_stock_hist
				GROUP BY sku_simple
		);

DROP TABLE IF EXISTS pricing.warehouse_stock_v2_co;
CREATE TABLE pricing.warehouse_stock_v2_co AS (

				SELECT 
					sku_simple,
					(stock_wms - stock_reserved_wms) AS in_stock

				FROM pricing.warehouse_stock_v1_co
				GROUP BY sku_simple
		);

CREATE TABLE pricing.Purchase_user_tm8_co (INDEX (sku_simple_similar_product_sold))(

		SELECT

			a.category, 
			a.sub_category, 
			a.sub_sub_category,
			a.supplier_id,
			a.supplier_name,
			a.sku_simple, 
			a.name,
			a.brand,
			a.model, 
			a.barccode, 
			a.quantity, 
			a.cost_per_unit, 
			a.total_cost, 
			a.estimated_selling_price, 
			a.estimated_time_weeks, 
			a.product_bought_before, 			
			a.not_in_restock, 
			a.best_competitor_price, 
			a.name_competitor,
			a.Final_Quantity,
			a.Final_Total_Cost,
			a.product_attack,  
			a.price_point_last_30, 
			a.sku_simple_similar_product_sold, 
			a.items_sku_sold, 
			a.price,
			a.payment_fee,
			a.voucher_cost,
			a.delivery_cost_supplier,
			a.cost_bob,
			a.tax_percent,
			a.warehouse_stock,
			a.Marketplace,
			a.shipping_fee,
			a.shipping_cost,
			a.price_f,
			a.cost_f,
			a.PC15,
			b.price_point_tier
			
		FROM pricing.Purchase_user_tm6_co a
		INNER JOIN pricing.Purchase_user_tm7_co b ON a.sku_simple_similar_product_sold = b.sku_simple
		GROUP BY a.sku_simple
		
);

CREATE TABLE pricing.Purchase_user_tm9_co (INDEX (sku_simple_similar_product_sold))(

		SELECT
			
			b.category, 
			b.sub_category, 
			b.sub_sub_category,
			b.supplier_id,
			b.supplier_name,
			b.sku_simple, 
			b.name,
			b.brand,
			b.model, 
			b.barccode, 
			b.quantity, 
			b.cost_per_unit, 
			b.total_cost, 
			b.estimated_selling_price, 
			b.estimated_time_weeks, 
			b.product_bought_before, 			
			b.not_in_restock, 
			b.best_competitor_price, 
			b.name_competitor,
			b.Final_Quantity,
			b.Final_Total_Cost,
			b.product_attack,  
			b.price_point_last_30, 
			b.sku_simple_similar_product_sold, 
			b.items_sku_sold, 
			b.price,
			b.payment_fee,
			b.voucher_cost,
			b.delivery_cost_supplier,
			b.cost_bob,
			b.tax_percent,
			b.warehouse_stock,
			b.Marketplace,
			b.shipping_fee,
			b.shipping_cost,
			b.price_f,
			b.cost_f,
			b.PC15,
			b.price_point_tier,
			Sum(c.num_sales_30) AS sales_last_30_days_price_point,
			sum(d.in_stock) AS in_stock

FROM pricing.Purchase_user_tm7_co a
INNER JOIN pricing.Purchase_user_tm8_co b ON b.category = a.category AND b.sub_category = a.sub_category AND b.sub_sub_category = a.sub_sub_category AND b.price_point_tier = a.price_point_tier
LEFT JOIN david_dana.sku_actual_numbers_co c ON a.sku_simple = c.SKUSimple
LEFT JOIN pricing.warehouse_stock_v2_co d ON a.sku_simple = d.sku_simple
GROUP BY  b.category, b.sub_category,b.sub_sub_category, b.price_point_tier
		
);

CREATE TABLE pricing.Purchase_Request_user_co (INDEX (sku_simple_similar_product_sold))(

		SELECT
				
				
				#b.supplier_id,
				#b.supplier_name,
				b.sku_simple,
				b.`name`,
				b.category,
				b.brand,
				b.model,
				b.barccode,
				b.quantity,
				b.cost_per_unit, 
				b.total_cost,
				b.estimated_selling_price,
				b.estimated_time_weeks,
				b.product_bought_before,
				'' AS nada,
				b.sku_simple_similar_product_sold,
				b.best_competitor_price,
				IF (b.best_competitor_price = 0,'No Competitor',b.name_competitor) AS name_competitor,
				b.Final_Quantity,
				b.Final_Total_Cost,
				b.sub_category,
				b.sub_sub_category,
				b.PC15,
				b.price_point_tier,
				a.in_stock,
				a.sales_last_30_days_price_point AS price_point_last_30,
				b.items_sku_sold
				#b.not_in_restock

		FROM pricing.Purchase_user_tm9_co a
		INNER JOIN pricing.Purchase_user_tm8_co b ON a.category = b.category AND a.sub_category = b.sub_category AND a.sub_sub_category = b.sub_sub_category AND a.price_point_tier = b.price_point_tier
		
);

DROP TABLE IF EXISTS pricing.sales_SKUSimple_co;

		CREATE TABLE pricing.sales_SKUSimple_co (INDEX (sku_simple))(

			SELECT
				SKUSimple AS sku_simple,
				Count(*) AS NumItems,
				Sum(IF(CouponCode NOT IN ('REC%','MKT%','CAC%'),Rev,Rev+CouponValueAfterTax)) AS TotalRev,
				Avg(IF(CouponCode NOT IN ('REC%','MKT%','CAC%'),PCOnePFive,PCOnePFive+CouponValueAfterTax)) AS PCOnePFive,
				Avg (PaidPrice) AS Paid_Price
			
			FROM development_co_project.A_Master
			WHERE DATEDIFF(Curdate(),Date) <= 30
				AND OrderAfterCan = 1
			GROUP BY SKUSimple
);

DROP TABLE IF EXISTS pricing.Days_in_stock_p_co;

		CREATE TABLE pricing.Days_in_stock_p_co (INDEX (sku_simple))(

			SELECT
				sku_simple,
				avg(days_in_stock) AS Avg_Days_in_WH

			FROM	operations_co.out_stock_hist
			GROUP BY sku_simple
);

DROP TABLE IF EXISTS pricing.co_cmr_prc_tbl_Purchase_Request_finaly;

CREATE TABLE pricing.co_cmr_prc_tbl_Purchase_Request_finaly (INDEX (sku_simple_similar_product_sold))(

		SELECT
		
				b.sku_simple,
				b.`name`,
				b.category,
				b.brand,
				b.model,
				b.barccode,
				b.quantity,
				b.cost_per_unit,
				b.total_cost,
				b.estimated_selling_price,
				b.estimated_time_weeks,
				b.product_bought_before,
				'' AS nada,
				b.sku_simple_similar_product_sold,
				b.best_competitor_price, 
				b.name_competitor,
				b.sub_category,
				b.sub_sub_category,
				b.PC15,
				b.price_point_tier,
				IF (b.in_stock IS NULL,0,b.in_stock) AS in_stock,
				IF (b.price_point_last_30 IS NULL,0,b.price_point_last_30) AS price_point_last_30,
				'' AS nada2,
				#b.items_sku_sold,
				IF (c.NumItems IS NULL,0,c.NumItems) AS NumItems,
				IF (c.TotalRev IS NULL,0,c.TotalRev) AS TotalRev,
				IF (c.PCOnePFive IS NULL,0,c.PCOnePFive) AS PCOnePFive,
				IF (c.Paid_Price IS NULL,0,c.Paid_Price) AS Paid_Price,
				IF (d.Avg_Days_in_WH IS NULL,0,d.Avg_Days_in_WH) AS Avg_Days_in_WH,
				0 AS Real_stock,
				b.Final_Quantity,
				b.Final_Total_Cost

		FROM pricing.Purchase_Request_user_co b
		LEFT JOIN pricing.sales_SKUSimple_co c ON b.sku_simple_similar_product_sold=c.sku_simple
		LEFT JOIN pricing.Days_in_stock_p_co d ON b.sku_simple_similar_product_sold=d.sku_simple
		GROUP BY b.sku_simple
);

# ver que hago con esta parte

/*UPDATE pricing.co_cmr_prc_tbl_Purchase_Request_finaly a
				INNER JOIN david_dana.mx_cmr_mp_tbl_real_stock b ON (a.sku_simple = b.sku_simple)
										SET a.Real_stock = b.Real_stock;*/


DROP TABLE IF EXISTS pricing.Purchase_user_tm1_two_co;
DROP TABLE IF EXISTS pricing.Purchase_user_tm2_co;
DROP TABLE IF EXISTS pricing.Purchase_user_tm3_co;
DROP TABLE IF EXISTS pricing.Purchase_user_tm4_co;
DROP TABLE IF EXISTS pricing.Purchase_user_tm5_co;
DROP TABLE IF EXISTS pricing.Purchase_user_tm6_co;
DROP TABLE IF EXISTS pricing.Purchase_user_tm7_co;
DROP TABLE IF EXISTS pricing.Purchase_user_tm8_co;
DROP TABLE IF EXISTS pricing.Purchase_user_tm9_co;
DROP TABLE IF EXISTS pricing.Purchase_Request_user_co;

TRUNCATE TABLE pricing.co_cmr_prc_tbl_Purchase_user;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`gustavo.miranda`@`%`*/ /*!50003 PROCEDURE `mx_cmr_prc_sp_cleaning_price_tool`()
BEGIN
	
TRUNCATE TABLE pricing.mx_cmr_prc_tbl_catalog_user_price;

DROP TABLE IF EXISTS pricing.mx_cmr_prc_tbl_price_tool_tmp1;

DROP TABLE IF EXISTS pricing.mx_cmr_prc_tbl_price_tool_tmp2;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`gustavo.miranda`@`%`*/ /*!50003 PROCEDURE `mx_cmr_prc_sp_cleaning_Purchase_request`()
BEGIN
	
DROP TABLE IF EXISTS pricing.Purchase_user_tm1_two;
DROP TABLE IF EXISTS pricing.Purchase_user_tm2;
DROP TABLE IF EXISTS pricing.Purchase_user_tm3;
DROP TABLE IF EXISTS pricing.Purchase_user_tm4;
DROP TABLE IF EXISTS pricing.Purchase_user_tm5;
DROP TABLE IF EXISTS pricing.Purchase_user_tm6;
DROP TABLE IF EXISTS pricing.Purchase_user_tm7;
DROP TABLE IF EXISTS pricing.Purchase_user_tm8;
DROP TABLE IF EXISTS pricing.Purchase_user_tm9;
DROP TABLE IF EXISTS pricing.Purchase_Request_user;

TRUNCATE TABLE pricing.mx_cmr_prc_tbl_Purchase_user;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`gustavo.miranda`@`%`*/ /*!50003 PROCEDURE `mx_cmr_prc_sp_price_change_tool`()
BEGIN
	
 DROP TABLE IF EXISTS pricing.mx_cmr_prc_tbl_catalog_price_comple;

CREATE TABLE pricing.mx_cmr_prc_tbl_catalog_price_comple (INDEX(sku_config), INDEX(sku_simple))(

	SELECT 

		a.sku AS sku_config,
		b.sku AS sku_simple,
		GREATEST(b.fixed_price,b.supplier_negotiated_price) AS fixed_price,
		0 AS mp,
		c.isMarketPlace AS Marketplace,
		IF (b.cost IS NULL, 0, b.cost) AS cost,
		IF (b.special_price IS NULL, 0, b.special_price) AS special_price,
		IF (b.price IS NULL, 0, b.price) AS price
		
		
	FROM bob_live_mx.catalog_config a
	INNER JOIN bob_live_mx.catalog_simple b ON (a.id_catalog_config = b.fk_catalog_config)
	INNER JOIN development_mx.A_Master_Catalog c ON (c.sku_simple = b.sku)
	GROUP BY b.sku
);

UPDATE pricing.mx_cmr_prc_tbl_catalog_price_comple a
INNER JOIN pricing.mx_cmr_prc_tbl_tmp_price_tool b ON (a.sku_config = b.sku_config)
SET a.mp = 1;

TRUNCATE TABLE pricing.mx_cmr_prc_tbl_catalog_user_price_tmp1;

		INSERT INTO pricing.mx_cmr_prc_tbl_catalog_user_price_tmp1 (

			sku_config,
			sku_simple,
			cost,
			special_price,
			price
)
				SELECT 
					
					sku_config,
					sku_simple,
					IF (cost = ' ', -1, cost) AS cost,
					IF (special_price = ' ', -1, special_price) AS special_price,
					IF (price = ' ', -1, price) AS price


				FROM pricing.mx_cmr_prc_tbl_catalog_user_price;


DROP TABLE IF EXISTS pricing.mx_cmr_prc_tbl_catalog_price_desicion;

CREATE TABLE pricing.mx_cmr_prc_tbl_catalog_price_desicion (INDEX(sku_config), INDEX(sku_simple))(

	SELECT 

		a.sku_config,
		a.sku_simple,
		b.fixed_price,
		b.Marketplace,
		b.mp,
		IF (a.price = -1, b.price, a.price) AS price,
		a.price AS price_new,
		b.price AS price_old,
		IF (a.special_price = -1,IF(b.special_price =0,((b.cost + b.price) / 2), b.special_price), a.special_price) AS special_price,
		a.special_price AS special_price_new,
		b.special_price AS special_price_old,
		IF (a.cost = -1, b.cost, a.cost) AS cost,
		a.cost AS cost_new,
		b.cost AS cost_old
		
	FROM pricing.mx_cmr_prc_tbl_catalog_user_price_tmp1 a
	LEFT JOIN pricing.mx_cmr_prc_tbl_catalog_price_comple b ON (a.sku_simple = b.sku_simple)
	GROUP BY a.sku_simple
);

CREATE TABLE pricing.mx_cmr_prc_tbl_price_tool_tmp1 (INDEX(sku_config), INDEX(sku_simple))(

	SELECT 

		sku_config,
		sku_simple,
		fixed_price,
		Marketplace,
		mp,
		price,
		price_new,
		price_old,
		special_price,
		special_price_new,
		special_price_old,
		cost,
		cost_new,
		cost_old,

		IF ( cost = 0 AND price = 0, 'Check, Cost = 0 and Price = 0',
		IF ( cost = 0, 'Check, Cost = 0',
		IF ( price = 0, 'Check, price = 0',
		IF ( special_price = 0, 'Check, special_price = 0',
		IF ( ( (cost_old) * 8 ) < cost AND cost < ( (cost_old) * 1.2 ), 'Check, The cost is very large',
		IF ( price > ( (cost) * 10 ), 'Check, High price', 
		IF ( special_price > ( (cost) * 10 ), 'Check, High special_price',
		IF ( cost <= special_price AND cost <= price AND special_price < price, 'Ok',
		IF ( cost <= special_price AND cost <= price AND special_price >= price, 'Check, special_price >= price',
		IF ( cost <= special_price AND cost > price AND special_price < price, 'Check, price < cost',
		IF ( cost > special_price AND cost <= price AND special_price < price, 'Check, special_price < cost',
		IF ( cost <= special_price AND cost > price AND special_price >= price, 'Check, special_price >= price and price < cost',
		IF ( cost > special_price AND cost <= price AND special_price >= price, 'Check, special_price >= price and special_price < cost',
		IF ( cost > special_price AND cost > price AND special_price < price, 'Check, price < cost and special_price < cost', 'Check, price < cost, special_price < cost and special_price >= price'
		)))))))))))))) AS Action_result,
		
		IF ( Marketplace = 1, 'Marketplace Product',
		IF ( fixed_price = 1, 'Fixed Price',
		IF ( mp = 1, 'Marketing Plan',
		IF ( cost = 0 AND price = 0, 'Pricing',
		IF ( cost = 0, 'Pricing',
		IF ( price = 0, 'Pricing',
		IF ( special_price = 0, 'Pricing',
		IF ( ( (cost_old) * 8 ) < cost AND cost < ( (cost_old) * 1.2 ), 'Pricing',
		IF ( price > ( (cost) * 10 ), 'Pricing', 
		IF ( special_price > ( (cost) * 10 ), 'Pricing',
		IF ( cost <= special_price AND cost <= price AND special_price < price, 'Ok',
		IF ( cost <= special_price AND cost <= price AND special_price >= price, 'Pricing',
		IF ( cost <= special_price AND cost > price AND special_price < price, 'Pricing',
		IF ( cost > special_price AND cost <= price AND special_price < price, 'Pricing',
		IF ( cost <= special_price AND cost > price AND special_price >= price, 'Pricing',
		IF ( cost > special_price AND cost <= price AND special_price >= price, 'Pricing',
		IF ( cost > special_price AND cost > price AND special_price < price, 'Pricing', 'Pricing'
		))))))))))))))))) AS Check_Final
	
	FROM pricing.mx_cmr_prc_tbl_catalog_price_desicion
	GROUP BY sku_simple
);

CREATE TABLE pricing.mx_cmr_prc_tbl_price_tool_tmp2 (INDEX(sku_config), INDEX(sku_simple))(

	SELECT 

		a.sku_config,
		a.sku_simple,
		IF (b.cost = -1 , ' ', b.cost) AS cost_order,
		IF (b.special_price = -1, ' ', b.special_price) AS special_price_order,
		IF (b.price = -1, ' ', b.price) AS price_order,
		IF (a.cost_old = 0, ' ', a.cost_old) AS cost_db,
		IF (a.special_price_old = 0, ' ', a.special_price_old) AS special_price_db,
		IF (a.price_old = 0, ' ', a.price_old) AS price_db,
		a.Action_result,
		a.Check_Final

	FROM pricing.mx_cmr_prc_tbl_price_tool_tmp1 a
	LEFT JOIN pricing.mx_cmr_prc_tbl_catalog_user_price b ON (a.sku_simple = b.sku_simple)
	GROUP BY a.sku_simple
);

TRUNCATE TABLE pricing.mx_cmr_prc_tbl_price_tool;

		INSERT INTO pricing.mx_cmr_prc_tbl_price_tool (

			sku_config,
			sku_simple,
			cost_order,
			special_price_order,
			price_order,
			cost_db,
			special_price_db,
			price_db,
			Action_result,
			Check_Final
)

				SELECT 
					
					sku_config,
					sku_simple,
					cost_order,
					special_price_order,
					price_order,
					cost_db,
					special_price_db,
					price_db,
					Action_result,
					Check_Final

				FROM pricing.mx_cmr_prc_tbl_price_tool_tmp2;


DROP TABLE IF EXISTS pricing.mx_cmr_prc_tbl_price_tool_tmp2;

DROP TABLE IF EXISTS pricing.mx_cmr_prc_tbl_price_tool_tmp1;

TRUNCATE TABLE pricing.mx_cmr_prc_tbl_catalog_user_price;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`gustavo.miranda`@`%`*/ /*!50003 PROCEDURE `mx_mcr_prc_sp_Purchase_Request`()
BEGIN
	
CREATE TABLE  pricing.Purchase_user_tm2  (INDEX (sku_simple))(
	
		SELECT
			m2.`name` AS category,
			m3.`name` AS sub_category,
			m4.`name` AS sub_sub_category,
			m6.`name` AS supplier,
			m5.`name` AS brand,
			m1.`name`,
			m1.model,
			m1.sku AS sku_config,
			m7.sku AS sku_simple,
			m7.price,
			m7.special_price,
			m10.tax_percent / 100 AS tax_percent,  
			m7.cost AS cost_bob,
			m7.cost / ( 1 + m10.tax_percent / 100 ) AS cost_after_tax,
			IF ((m7.delivery_cost_supplier) IS NULL, 0, (m7.delivery_cost_supplier)) AS delivery_cost_supplier,
			m7.fixed_price, #bob_live_mx.catalog_simple.fixed_price,
			m7.sell_list,    #bob_live_mx.catalog_simple.sell_list
			IFNULL( m8.quantity,0) + IFNULL(m9.quantity,0) AS stock,
			IFNULL( m9.quantity,0 ) AS warehouse_stock,
			m7.is_option_marketplace AS Marketplace,
			m1.sku_not_comparable
			
		FROM bob_live_mx.catalog_config m1
		LEFT JOIN bob_live_mx.catalog_simple m7 ON m1.id_catalog_config = m7.fk_catalog_config
		LEFT JOIN bob_live_mx.catalog_attribute_option_global_category m2 ON m1.fk_catalog_attribute_option_global_category = m2.id_catalog_attribute_option_global_category
		LEFT JOIN bob_live_mx.catalog_attribute_option_global_sub_category  m3 ON m1.fk_catalog_attribute_option_global_sub_category = m3.id_catalog_attribute_option_global_sub_category
		LEFT JOIN bob_live_mx.catalog_attribute_option_global_sub_sub_category m4 ON m1.fk_catalog_attribute_option_global_sub_sub_category = m4.id_catalog_attribute_option_global_sub_sub_category
		LEFT JOIN bob_live_mx.catalog_brand m5 ON m1.fk_catalog_brand = m5.id_catalog_brand
		LEFT JOIN bob_live_mx.catalog_supplier_stock m8 ON m7.id_catalog_simple = m8.fk_catalog_simple
		LEFT JOIN bob_live_mx.catalog_warehouse_stock m9 ON m7.id_catalog_simple = m9.fk_catalog_simple
		LEFT JOIN bob_live_mx.catalog_tax_class m10 ON m7.fk_catalog_tax_class = m10.id_catalog_tax_class
		LEFT JOIN bob_live_mx.catalog_source m11 ON m7.id_catalog_simple = m11.fk_catalog_simple
		LEFT JOIN bob_live_mx.supplier m6 ON m11.fk_supplier = m6.id_supplier
);


CREATE TABLE pricing.Purchase_user_tm1_two (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			sku_simple,
			name,
			category,
			brand,
			model,
			barccode,
			quantity,
			cost_per_unit,
			total_cost,
			estimated_selling_price,
			estimated_time_weeks,
			product_bought_before,
			IF (product_bought_before='yes', sku_simple, sku_simple_similar_product_sold) AS sku_simple_similar_product_sold,
			best_competitor_price,
			name_competitor,
			Final_Quantity,
			Final_Total_Cost

	
		FROM pricing.mx_cmr_prc_tbl_Purchase_user
		GROUP BY sku_simple
);

CREATE TABLE pricing.Purchase_user_tm3 (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			b.category, 
			b.sub_category, 
			b.sub_sub_category,
			NULL AS supplier_id,
			NULL AS supplier_name,
			a.sku_simple, 
			a.name,
			a.brand,
			a.model, 
			a.barccode, 
			a.quantity, 
			a.cost_per_unit, 
			a.total_cost, 
			a.estimated_selling_price, 
			a.estimated_time_weeks, 
			a.product_bought_before, 			
			0 AS not_in_restock, 
			a.best_competitor_price, 
			a.name_competitor,
			a.Final_Quantity,
			a.Final_Total_Cost,
			0 AS product_attack, 
			0 AS in_stock, 
			0 AS price_point_last_30, 
			a.sku_simple_similar_product_sold, 
			0 AS items_sku_sold, 
			b.price,
			IF (best_competitor_price > 0, ((best_competitor_price/1.16)*0.0169), IF (estimated_selling_price > 0, ((estimated_selling_price/1.16)*0.0169),((price/1.16)*0.0169))) AS payment_fee,
			IF (best_competitor_price > 0, ((best_competitor_price/1.16)*0.04), IF (estimated_selling_price > 0, ((estimated_selling_price/1.16)*0.04),((price/1.16)*0.04))) AS voucher_cost,
			#0 AS payment_fee,
			#0 AS voucher_cost,
			b.delivery_cost_supplier,
			b.cost_bob,
			b.tax_percent,
			b.warehouse_stock,
			b.Marketplace

		FROM pricing.Purchase_user_tm1_two a 
		LEFT JOIN pricing.Purchase_user_tm2 b ON a.sku_simple_similar_product_sold = b.sku_simple
		GROUP BY a.sku_simple
		);

CREATE TABLE pricing.Purchase_user_tm4 (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			a.category, 
			a.sub_category, 
			a.sub_sub_category,
			a.supplier_id,
			a.supplier_name,
			a.sku_simple, 
			a.name,
			a.brand,
			a.model, 
			a.barccode, 
			a.quantity, 
			a.cost_per_unit, 
			a.total_cost, 
			a.estimated_selling_price, 
			a.estimated_time_weeks, 
			a.product_bought_before, 			
			a.not_in_restock, 
			a.best_competitor_price, 
			a.name_competitor, 
			a.Final_Quantity,
			a.Final_Total_Cost,
			a.product_attack, 
			a.in_stock, 
			a.price_point_last_30, 
			a.sku_simple_similar_product_sold, 
			a.items_sku_sold, 
			a.price,
			a.payment_fee,
			a.voucher_cost,
			a.delivery_cost_supplier,
			a.cost_bob,
			a.tax_percent,
			a.warehouse_stock,
			a.Marketplace,
			b.shipping_fee,
			b.shipping_cost

		FROM pricing.Purchase_user_tm3 a 
		LEFT JOIN david_dana.mx_cmr_prc_tbl_shipping_cost_and_shipping_fee b ON a.sku_simple_similar_product_sold = b.sku_simple
		GROUP BY a.sku_simple
		);

CREATE TABLE pricing.Purchase_user_tm5 (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			category, 
			sub_category, 
			sub_sub_category,
			supplier_id,
			supplier_name,
			sku_simple, 
			name,
			brand,
			model, 
			barccode, 
			quantity, 
			cost_per_unit, 
			total_cost, 
			estimated_selling_price, 
			estimated_time_weeks, 
			product_bought_before, 			
			not_in_restock, 
			best_competitor_price, 
			name_competitor, 
			Final_Quantity,
			Final_Total_Cost,
			product_attack, 
			in_stock, 
			price_point_last_30, 
			sku_simple_similar_product_sold, 
			items_sku_sold, 
			price,
			payment_fee,
			voucher_cost,
			delivery_cost_supplier,
			cost_bob,
			tax_percent,
			warehouse_stock,
			Marketplace,
			IF (shipping_fee IS NULL,0,shipping_fee) AS shipping_fee,
			IF (shipping_cost IS NULL,0, shipping_cost) AS shipping_cost,
			IF (best_competitor_price > 0, best_competitor_price, IF(estimated_selling_price > 0, estimated_selling_price, price)) AS price_f,
			IF (cost_per_unit > 0, cost_per_unit, cost_bob) AS cost_f

		FROM pricing.Purchase_user_tm4 
		GROUP BY sku_simple
);

CREATE TABLE pricing.Purchase_user_tm6 (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			category, 
			sub_category, 
			sub_sub_category,
			supplier_id,
			supplier_name,
			sku_simple, 
			name,
			brand,
			model, 
			barccode, 
			quantity, 
			cost_per_unit, 
			total_cost, 
			estimated_selling_price, 
			estimated_time_weeks, 
			product_bought_before, 			
			not_in_restock, 
			best_competitor_price, 
			name_competitor,
			Final_Quantity,
			Final_Total_Cost,
			product_attack, 
			in_stock, 
			price_point_last_30, 
			sku_simple_similar_product_sold, 
			items_sku_sold, 
			price,
			payment_fee,
			voucher_cost,
			delivery_cost_supplier,
			cost_bob,
			tax_percent,
			warehouse_stock,
			Marketplace,
			shipping_fee,
			shipping_cost,
			price_f,
			cost_f,
			((price_f/1.16) + (shipping_fee/1.16) - (delivery_cost_supplier/1.16) - (cost_f/1.16) - (shipping_cost/1.16) - payment_fee - voucher_cost) AS PC15
			

		FROM pricing.Purchase_user_tm5
		GROUP BY sku_simple
);

/*CREATE TABLE pricing.Purchase_user_tm7 (INDEX (sku_simple))(

		SELECT
			'Linio' AS Competitor,
			m1.sku AS sku_config,
			m2.sku AS sku_simple,
			m7.`name` AS Brand,
			m1.`name`,
			m6.price_f,
			m3.`name` AS category,
			m4.`name` AS sub_category,
			m5.`name` AS sub_sub_category,
			m8.price_point_tier_1,
			m8.price_point_tier_2,
			m8.price_point_tier_3,
			m8.price_point_tier_4,
			m8.price_point_tier_5,
			m8.price_point_tier_6,
			m8.price_point_tier_7,

				CASE WHEN m6.price_f < m8.price_point_tier_1 THEN 1
				WHEN m6.price_f < m8.price_point_tier_2      THEN 2
				WHEN m6.price_f < m8.price_point_tier_3      THEN 3
				WHEN m6.price_f < m8.price_point_tier_4      THEN 4
				WHEN m6.price_f < m8.price_point_tier_5      THEN 5
				WHEN m6.price_f < m8.price_point_tier_6      THEN 6
				WHEN m6.price_f < m8.price_point_tier_7      THEN 7
				ELSE 8
				END AS price_point_tier

		FROM bob_live_mx.catalog_config m1
		INNER JOIN bob_live_mx.catalog_simple m2 ON m1.id_catalog_config = m2.fk_catalog_config
		INNER  JOIN bob_live_mx.catalog_attribute_option_global_category m3 ON m1.fk_catalog_attribute_option_global_category = m3.id_catalog_attribute_option_global_category
		INNER  JOIN bob_live_mx.catalog_attribute_option_global_sub_category m4 ON m1.fk_catalog_attribute_option_global_sub_category = m4.id_catalog_attribute_option_global_sub_category
		INNER  JOIN bob_live_mx.catalog_attribute_option_global_sub_sub_category m5 ON m1.fk_catalog_attribute_option_global_sub_sub_category = m5.id_catalog_attribute_option_global_sub_sub_category
		INNER  JOIN pricing.Purchase_user_tm5 m6 ON m2.sku = m6.sku_simple_similar_product_sold
		INNER  JOIN bob_live_mx.catalog_brand m7 ON m1.fk_catalog_brand = m7.id_catalog_brand
		LEFT  JOIN david_dana.`price_points_per_category_key_mx` m8 ON m3.`name` = m8.category AND m4.`name` = m8.sub_category AND m5.`name` = m8.sub_sub_category

		GROUP BY m2.sku
);*/

DROP TABLE IF EXISTS pricing.warehouse_stock_v1;
CREATE TABLE pricing.warehouse_stock_v1 AS (

				SELECT 
					sku_simple,
					sum(in_stock) AS stock_wms, 
					sum(reserved) AS stock_reserved_wms

				FROM operations_mx.out_stock_hist
				GROUP BY sku_simple
		);

DROP TABLE IF EXISTS pricing.warehouse_stock_v2;
CREATE TABLE pricing.warehouse_stock_v2 AS (

				SELECT 
					sku_simple,
					(stock_wms - stock_reserved_wms) AS in_stock

				FROM pricing.warehouse_stock_v1
				GROUP BY sku_simple
		);

CREATE TABLE pricing.Purchase_user_tm8 (INDEX (sku_simple_similar_product_sold))(

		SELECT

			a.category, 
			a.sub_category, 
			a.sub_sub_category,
			a.supplier_id,
			a.supplier_name,
			a.sku_simple, 
			a.name,
			a.brand,
			a.model, 
			a.barccode, 
			a.quantity, 
			a.cost_per_unit, 
			a.total_cost, 
			a.estimated_selling_price, 
			a.estimated_time_weeks, 
			a.product_bought_before, 			
			a.not_in_restock, 
			a.best_competitor_price, 
			a.name_competitor,
			a.Final_Quantity,
			a.Final_Total_Cost,
			a.product_attack,  
			a.price_point_last_30, 
			a.sku_simple_similar_product_sold, 
			a.items_sku_sold, 
			a.price,
			a.payment_fee,
			a.voucher_cost,
			a.delivery_cost_supplier,
			a.cost_bob,
			a.tax_percent,
			a.warehouse_stock,
			a.Marketplace,
			a.shipping_fee,
			a.shipping_cost,
			a.price_f,
			a.cost_f,
			a.PC15,
			0 AS price_point_tier
			
		FROM pricing.Purchase_user_tm6 a
		#LEFT JOIN pricing.Purchase_user_tm7 b ON a.sku_simple_similar_product_sold = b.sku_simple
		GROUP BY a.sku_simple
		
);

DROP TABLE IF EXISTS david_dana.sku_actual_numbers_mx;

CREATE TABLE david_dana.sku_actual_numbers_mx ( INDEX( SKUSimple) )
	
	SELECT 
			SKUSimple, 
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=7,1,0),0)) AS num_sales_7, 
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=7,paidprice,0),0)) AS price_paid_7, 
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=7,ShippingFee,0),0)) AS shipping_fee_7,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=7,ShippingCost,0),0)) AS shipping_cost_7,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=7,Rev,0),0)) AS net_rev_7,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=7,PCOne,0),0)) AS PC1_7,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=7,PCOnePFive,0),0)) AS PC1_5_7,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=7,CouponValue,0),0)) AS CouponValue_7,

			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 7 AND 14 ,1,0),0)) AS num_sales_7_14, 
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 7 AND 14 ,paidprice,0),0)) AS price_paid_7_14, 
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 7 AND 14 ,ShippingFee,0),0)) AS shipping_fee_7_14,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 7 AND 14 ,ShippingCost,0),0)) AS shipping_cost_7_14,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 7 AND 14 ,Rev,0),0)) AS net_rev_7_14,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 7 AND 14 ,PCOne,0),0)) AS PC1_7_14,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 7 AND 14 ,PCOnePFive,0),0)) AS PC1_5_7_14,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 7 AND 14 ,CouponValue,0),0)) AS CouponValue_7_14,

			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=30,1,0),0)) AS num_sales_30, 
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=30,paidprice,0),0)) AS price_paid_30, 
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=30,ShippingFee,0),0)) AS shipping_fee_30,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=30,ShippingCost,0),0)) AS shipping_cost_30,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=30,Rev,0),0)) AS net_rev_30,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=30,PCOne,0),0)) AS PC1_30,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=30,PCOnePFive,0),0)) AS PC1_5_30,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=30,CouponValue,0),0)) AS CouponValue_30,

			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 30 AND 60 ,1,0),0)) AS num_sales_30_60, 
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 30 AND 60 ,paidprice,0),0)) AS price_paid_30_60, 
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 30 AND 60 ,ShippingFee,0),0)) AS shipping_fee_30_60,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 30 AND 60 ,ShippingCost,0),0)) AS shipping_cost_30_60,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 30 AND 60 ,Rev,0),0)) AS net_rev_30_60,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 30 AND 60 ,PCOne,0),0)) AS PC1_30_60,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 30 AND 60 ,PCOnePFive,0),0)) AS PC1_5_30_60,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 30 AND 60 ,CouponValue,0),0)) AS CouponValue_30_60

	FROM development_mx.A_Master r
  WHERE datediff(curdate(),r.Date)<=60
  GROUP BY r.SKUSimple;


CREATE TABLE pricing.Purchase_user_tm9 (INDEX (sku_simple_similar_product_sold))(

		SELECT
			
			b.category, 
			b.sub_category, 
			b.sub_sub_category,
			b.supplier_id,
			b.supplier_name,
			b.sku_simple, 
			b.name,
			b.brand,
			b.model, 
			b.barccode, 
			b.quantity, 
			b.cost_per_unit, 
			b.total_cost, 
			b.estimated_selling_price, 
			b.estimated_time_weeks, 
			b.product_bought_before, 			
			b.not_in_restock, 
			b.best_competitor_price, 
			b.name_competitor,
			b.Final_Quantity,
			b.Final_Total_Cost,
			b.product_attack,  
			b.price_point_last_30, 
			b.sku_simple_similar_product_sold, 
			b.items_sku_sold, 
			b.price,
			b.payment_fee,
			b.voucher_cost,
			b.delivery_cost_supplier,
			b.cost_bob,
			b.tax_percent,
			b.warehouse_stock,
			b.Marketplace,
			b.shipping_fee,
			b.shipping_cost,
			b.price_f,
			b.cost_f,
			b.PC15,
			b.price_point_tier,
			Sum(c.num_sales_30) AS sales_last_30_days_price_point,
			sum(d.in_stock) AS in_stock

#FROM pricing.Purchase_user_tm7 a
#INNER JOIN pricing.Purchase_user_tm8 b ON b.category = a.category AND b.sub_category = a.sub_category AND b.sub_sub_category = a.sub_sub_category AND b.price_point_tier = a.price_point_tier
FROM  pricing.Purchase_user_tm8 b 
LEFT JOIN david_dana.sku_actual_numbers_mx c ON b.sku_simple = c.SKUSimple
LEFT JOIN pricing.warehouse_stock_v2 d ON b.sku_simple = d.sku_simple
#GROUP BY  b.category, b.sub_category,b.sub_sub_category, b.price_point_tier
GROUP BY b.sku_simple
		
);

CREATE TABLE pricing.Purchase_Request_user (INDEX (sku_simple_similar_product_sold))(

		SELECT
				
				
				#b.supplier_id,
				#b.supplier_name,
				b.sku_simple,
				b.`name`,
				b.category,
				b.brand,
				b.model,
				b.barccode,
				b.quantity,
				b.cost_per_unit, 
				b.total_cost,
				b.estimated_selling_price,
				b.estimated_time_weeks,
				b.product_bought_before,
				'' AS nada,
				b.sku_simple_similar_product_sold,
				b.best_competitor_price,
				IF (b.best_competitor_price = 0,'No Competitor',b.name_competitor) AS name_competitor,
				b.Final_Quantity,
				b.Final_Total_Cost,
				b.sub_category,
				b.sub_sub_category,
				b.PC15,
				b.price_point_tier,
				a.in_stock,
				a.sales_last_30_days_price_point AS price_point_last_30,
				b.items_sku_sold
				#b.not_in_restock

		FROM pricing.Purchase_user_tm9 a
		LEFT JOIN pricing.Purchase_user_tm8 b ON a.category = b.category AND a.sub_category = b.sub_category AND a.sub_sub_category = b.sub_sub_category AND a.price_point_tier = b.price_point_tier
		
);

DROP TABLE IF EXISTS pricing.sales_SKUSimple;

		CREATE TABLE pricing.sales_SKUSimple (INDEX (sku_simple))(

			SELECT
				SKUSimple AS sku_simple,
				Count(*) AS NumItems,
				Sum(IF(CouponCode NOT IN ('REC%','MKT%','CAC%'),Rev,Rev+CouponValueAfterTax)) AS TotalRev,
				Avg(IF(CouponCode NOT IN ('REC%','MKT%','CAC%'),PCOnePFive,PCOnePFive+CouponValueAfterTax)) AS PCOnePFive,
				Avg (PaidPrice) AS Paid_Price
			
			FROM development_mx.A_Master
			WHERE DATEDIFF(Curdate(),Date) <= 30
				AND OrderAfterCan = 1
			GROUP BY SKUSimple
);

DROP TABLE IF EXISTS pricing.Days_in_stock_p;

		CREATE TABLE pricing.Days_in_stock_p (INDEX (sku_simple))(

			SELECT
				sku_simple,
				avg(days_in_stock) AS Avg_Days_in_WH

			FROM	operations_mx.out_stock_hist
			WHERE in_stock =1
			GROUP BY sku_simple
);

DROP TABLE IF EXISTS pricing.mx_cmr_prc_tbl_Purchase_Request_finaly;

CREATE TABLE pricing.mx_cmr_prc_tbl_Purchase_Request_finaly (INDEX (sku_simple_similar_product_sold))(

		SELECT
		
				b.sku_simple,
				b.`name`,
				b.category,
				b.brand,
				b.model,
				b.barccode,
				b.quantity,
				b.cost_per_unit,
				b.total_cost,
				b.estimated_selling_price,
				b.estimated_time_weeks,
				b.product_bought_before,
				'' AS nada,
				b.sku_simple_similar_product_sold,
				b.best_competitor_price, 
				b.name_competitor,
				b.sub_category,
				b.sub_sub_category,
				b.PC15,
				b.price_point_tier,
				IF (b.in_stock IS NULL,0,b.in_stock) AS in_stock,
				IF (b.price_point_last_30 IS NULL,0,b.price_point_last_30) AS price_point_last_30,
				'' AS nada2,
				#b.items_sku_sold,
				IF (c.NumItems IS NULL,0,c.NumItems) AS NumItems,
				IF (c.TotalRev IS NULL,0,c.TotalRev) AS TotalRev,
				IF (c.PCOnePFive IS NULL,0,c.PCOnePFive) AS PCOnePFive,
				IF (c.Paid_Price IS NULL,0,c.Paid_Price) AS Paid_Price,
				IF (d.Avg_Days_in_WH IS NULL,0,d.Avg_Days_in_WH) AS Avg_Days_in_WH,
				0 AS Real_stock,
				b.Final_Quantity,
				b.Final_Total_Cost

		FROM pricing.Purchase_Request_user b
		LEFT JOIN pricing.sales_SKUSimple c ON b.sku_simple_similar_product_sold=c.sku_simple
		LEFT JOIN pricing.Days_in_stock_p d ON b.sku_simple_similar_product_sold=d.sku_simple
		GROUP BY b.sku_simple
);

UPDATE pricing.mx_cmr_prc_tbl_Purchase_Request_finaly a
				INNER JOIN david_dana.mx_cmr_mp_tbl_real_stock b ON (a.sku_simple = b.sku_simple)
										SET a.Real_stock = b.Real_stock;



/*DROP TABLE IF EXISTS pricing.Purchase_user_tm1_two;
DROP TABLE IF EXISTS pricing.Purchase_user_tm2;
DROP TABLE IF EXISTS pricing.Purchase_user_tm3;
DROP TABLE IF EXISTS pricing.Purchase_user_tm4;
DROP TABLE IF EXISTS pricing.Purchase_user_tm5;
DROP TABLE IF EXISTS pricing.Purchase_user_tm6;
DROP TABLE IF EXISTS pricing.Purchase_user_tm7;
DROP TABLE IF EXISTS pricing.Purchase_user_tm8;
DROP TABLE IF EXISTS pricing.Purchase_user_tm9;
DROP TABLE IF EXISTS pricing.Purchase_Request_user;

TRUNCATE TABLE pricing.mx_cmr_prc_tbl_Purchase_user;*/

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`gustavo.miranda`@`%`*/ /*!50003 PROCEDURE `mx_mcr_prc_sp_Purchase_Request_copy`()
BEGIN
	
CREATE TABLE  pricing.Purchase_user_tm2  (INDEX (sku_simple))(
	
		SELECT
			m2.`name` AS category,
			m3.`name` AS sub_category,
			m4.`name` AS sub_sub_category,
			m6.`name` AS supplier,
			m5.`name` AS brand,
			m1.`name`,
			m1.model,
			m1.sku AS sku_config,
			m7.sku AS sku_simple,
			m7.price,
			m7.special_price,
			m10.tax_percent / 100 AS tax_percent,  
			m7.cost AS cost_bob,
			m7.cost / ( 1 + m10.tax_percent / 100 ) AS cost_after_tax,
			IF ((m7.delivery_cost_supplier) IS NULL, 0, (m7.delivery_cost_supplier)) AS delivery_cost_supplier,
			m7.fixed_price, #bob_live_mx.catalog_simple.fixed_price,
			m7.sell_list,    #bob_live_mx.catalog_simple.sell_list
			IFNULL( m8.quantity,0) + IFNULL(m9.quantity,0) AS stock,
			IFNULL( m9.quantity,0 ) AS warehouse_stock,
			m7.is_option_marketplace AS Marketplace,
			m1.sku_not_comparable
			
		FROM bob_live_mx.catalog_config m1
		LEFT JOIN bob_live_mx.catalog_simple m7 ON m1.id_catalog_config = m7.fk_catalog_config
		LEFT JOIN bob_live_mx.catalog_attribute_option_global_category m2 ON m1.fk_catalog_attribute_option_global_category = m2.id_catalog_attribute_option_global_category
		LEFT JOIN bob_live_mx.catalog_attribute_option_global_sub_category  m3 ON m1.fk_catalog_attribute_option_global_sub_category = m3.id_catalog_attribute_option_global_sub_category
		LEFT JOIN bob_live_mx.catalog_attribute_option_global_sub_sub_category m4 ON m1.fk_catalog_attribute_option_global_sub_sub_category = m4.id_catalog_attribute_option_global_sub_sub_category
		LEFT JOIN bob_live_mx.catalog_brand m5 ON m1.fk_catalog_brand = m5.id_catalog_brand
		LEFT JOIN bob_live_mx.catalog_supplier_stock m8 ON m7.id_catalog_simple = m8.fk_catalog_simple
		LEFT JOIN bob_live_mx.catalog_warehouse_stock m9 ON m7.id_catalog_simple = m9.fk_catalog_simple
		LEFT JOIN bob_live_mx.catalog_tax_class m10 ON m7.fk_catalog_tax_class = m10.id_catalog_tax_class
		LEFT JOIN bob_live_mx.catalog_source m11 ON m7.id_catalog_simple = m11.fk_catalog_simple
		LEFT JOIN bob_live_mx.supplier m6 ON m11.fk_supplier = m6.id_supplier
);


CREATE TABLE pricing.Purchase_user_tm1_two (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			sku_simple,
			name,
			category,
			brand,
			model,
			barccode,
			quantity,
			cost_per_unit,
			total_cost,
			estimated_selling_price,
			estimated_time_weeks,
			product_bought_before,
			IF (product_bought_before='yes', sku_simple, sku_simple_similar_product_sold) AS sku_simple_similar_product_sold,
			best_competitor_price,
			name_competitor,
			Final_Quantity,
			Final_Total_Cost

	
		FROM pricing.mx_cmr_prc_tbl_Purchase_user
		GROUP BY sku_simple
);

CREATE TABLE pricing.Purchase_user_tm3 (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			b.category, 
			b.sub_category, 
			b.sub_sub_category,
			NULL AS supplier_id,
			NULL AS supplier_name,
			a.sku_simple, 
			a.name,
			a.brand,
			a.model, 
			a.barccode, 
			a.quantity, 
			a.cost_per_unit, 
			a.total_cost, 
			a.estimated_selling_price, 
			a.estimated_time_weeks, 
			a.product_bought_before, 			
			0 AS not_in_restock, 
			a.best_competitor_price, 
			a.name_competitor,
			a.Final_Quantity,
			a.Final_Total_Cost,
			0 AS product_attack, 
			0 AS in_stock, 
			0 AS price_point_last_30, 
			a.sku_simple_similar_product_sold, 
			0 AS items_sku_sold, 
			b.price,
			IF (best_competitor_price > 0, ((best_competitor_price/1.16)*0.0169), IF (estimated_selling_price > 0, ((estimated_selling_price/1.16)*0.0169),((price/1.16)*0.0169))) AS payment_fee,
			IF (best_competitor_price > 0, ((best_competitor_price/1.16)*0.04), IF (estimated_selling_price > 0, ((estimated_selling_price/1.16)*0.04),((price/1.16)*0.04))) AS voucher_cost,
			#0 AS payment_fee,
			#0 AS voucher_cost,
			b.delivery_cost_supplier,
			b.cost_bob,
			b.tax_percent,
			b.warehouse_stock,
			b.Marketplace

		FROM pricing.Purchase_user_tm1_two a 
		LEFT JOIN pricing.Purchase_user_tm2 b ON a.sku_simple_similar_product_sold = b.sku_simple
		GROUP BY a.sku_simple
		);

CREATE TABLE pricing.Purchase_user_tm4 (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			a.category, 
			a.sub_category, 
			a.sub_sub_category,
			a.supplier_id,
			a.supplier_name,
			a.sku_simple, 
			a.name,
			a.brand,
			a.model, 
			a.barccode, 
			a.quantity, 
			a.cost_per_unit, 
			a.total_cost, 
			a.estimated_selling_price, 
			a.estimated_time_weeks, 
			a.product_bought_before, 			
			a.not_in_restock, 
			a.best_competitor_price, 
			a.name_competitor, 
			a.Final_Quantity,
			a.Final_Total_Cost,
			a.product_attack, 
			a.in_stock, 
			a.price_point_last_30, 
			a.sku_simple_similar_product_sold, 
			a.items_sku_sold, 
			a.price,
			a.payment_fee,
			a.voucher_cost,
			a.delivery_cost_supplier,
			a.cost_bob,
			a.tax_percent,
			a.warehouse_stock,
			a.Marketplace,
			b.shipping_fee,
			b.shipping_cost

		FROM pricing.Purchase_user_tm3 a 
		LEFT JOIN david_dana.mx_cmr_prc_tbl_shipping_cost_and_shipping_fee b ON a.sku_simple_similar_product_sold = b.sku_simple
		GROUP BY a.sku_simple
		);

CREATE TABLE pricing.Purchase_user_tm5 (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			category, 
			sub_category, 
			sub_sub_category,
			supplier_id,
			supplier_name,
			sku_simple, 
			name,
			brand,
			model, 
			barccode, 
			quantity, 
			cost_per_unit, 
			total_cost, 
			estimated_selling_price, 
			estimated_time_weeks, 
			product_bought_before, 			
			not_in_restock, 
			best_competitor_price, 
			name_competitor, 
			Final_Quantity,
			Final_Total_Cost,
			product_attack, 
			in_stock, 
			price_point_last_30, 
			sku_simple_similar_product_sold, 
			items_sku_sold, 
			price,
			payment_fee,
			voucher_cost,
			delivery_cost_supplier,
			cost_bob,
			tax_percent,
			warehouse_stock,
			Marketplace,
			IF (shipping_fee IS NULL,0,shipping_fee) AS shipping_fee,
			IF (shipping_cost IS NULL,0, shipping_cost) AS shipping_cost,
			IF (best_competitor_price > 0, best_competitor_price, IF(estimated_selling_price > 0, estimated_selling_price, price)) AS price_f,
			IF (cost_per_unit > 0, cost_per_unit, cost_bob) AS cost_f

		FROM pricing.Purchase_user_tm4 
		GROUP BY sku_simple
);

CREATE TABLE pricing.Purchase_user_tm6 (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			category, 
			sub_category, 
			sub_sub_category,
			supplier_id,
			supplier_name,
			sku_simple, 
			name,
			brand,
			model, 
			barccode, 
			quantity, 
			cost_per_unit, 
			total_cost, 
			estimated_selling_price, 
			estimated_time_weeks, 
			product_bought_before, 			
			not_in_restock, 
			best_competitor_price, 
			name_competitor,
			Final_Quantity,
			Final_Total_Cost,
			product_attack, 
			in_stock, 
			price_point_last_30, 
			sku_simple_similar_product_sold, 
			items_sku_sold, 
			price,
			payment_fee,
			voucher_cost,
			delivery_cost_supplier,
			cost_bob,
			tax_percent,
			warehouse_stock,
			Marketplace,
			shipping_fee,
			shipping_cost,
			price_f,
			cost_f,
			((price_f/1.16) + (shipping_fee/1.16) - (delivery_cost_supplier/1.16) - (cost_f/1.16) - (shipping_cost/1.16) - payment_fee - voucher_cost) AS PC15
			

		FROM pricing.Purchase_user_tm5
		GROUP BY sku_simple
);

CREATE TABLE pricing.Purchase_user_tm7 (INDEX (sku_simple))(

		SELECT
			'Linio' AS Competitor,
			m1.sku AS sku_config,
			m2.sku AS sku_simple,
			m7.`name` AS Brand,
			m1.`name`,
			m6.price_f,
			m3.`name` AS category,
			m4.`name` AS sub_category,
			m5.`name` AS sub_sub_category,
			m8.price_point_tier_1,
			m8.price_point_tier_2,
			m8.price_point_tier_3,
			m8.price_point_tier_4,
			m8.price_point_tier_5,
			m8.price_point_tier_6,
			m8.price_point_tier_7,

				CASE WHEN m6.price_f < m8.price_point_tier_1 THEN 1
				WHEN m6.price_f < m8.price_point_tier_2      THEN 2
				WHEN m6.price_f < m8.price_point_tier_3      THEN 3
				WHEN m6.price_f < m8.price_point_tier_4      THEN 4
				WHEN m6.price_f < m8.price_point_tier_5      THEN 5
				WHEN m6.price_f < m8.price_point_tier_6      THEN 6
				WHEN m6.price_f < m8.price_point_tier_7      THEN 7
				ELSE 8
				END AS price_point_tier

		FROM bob_live_mx.catalog_config m1
		LEFT  JOIN bob_live_mx.catalog_simple m2 ON m1.id_catalog_config = m2.fk_catalog_config
		LEFT  JOIN bob_live_mx.catalog_attribute_option_global_category m3 ON m1.fk_catalog_attribute_option_global_category = m3.id_catalog_attribute_option_global_category
		LEFT  JOIN bob_live_mx.catalog_attribute_option_global_sub_category m4 ON m1.fk_catalog_attribute_option_global_sub_category = m4.id_catalog_attribute_option_global_sub_category
		LEFT  JOIN bob_live_mx.catalog_attribute_option_global_sub_sub_category m5 ON m1.fk_catalog_attribute_option_global_sub_sub_category = m5.id_catalog_attribute_option_global_sub_sub_category
		LEFT  JOIN pricing.Purchase_user_tm5 m6 ON m2.sku = m6.sku_simple_similar_product_sold
		LEFT  JOIN bob_live_mx.catalog_brand m7 ON m1.fk_catalog_brand = m7.id_catalog_brand
		LEFT  JOIN david_dana.`price_points_per_category_key_mx` m8 ON m3.`name` = m8.category AND m4.`name` = m8.sub_category AND m5.`name` = m8.sub_sub_category

		GROUP BY m2.sku
);

DROP TABLE IF EXISTS pricing.warehouse_stock_v1;
CREATE TABLE pricing.warehouse_stock_v1 AS (

				SELECT 
					sku_simple,
					sum(in_stock) AS stock_wms, 
					sum(reserved) AS stock_reserved_wms

				FROM operations_mx.out_stock_hist
				GROUP BY sku_simple
		);

DROP TABLE IF EXISTS pricing.warehouse_stock_v2;
CREATE TABLE pricing.warehouse_stock_v2 AS (

				SELECT 
					sku_simple,
					(stock_wms - stock_reserved_wms) AS in_stock

				FROM pricing.warehouse_stock_v1
				GROUP BY sku_simple
		);

CREATE TABLE pricing.Purchase_user_tm8 (INDEX (sku_simple_similar_product_sold))(

		SELECT

			a.category, 
			a.sub_category, 
			a.sub_sub_category,
			a.supplier_id,
			a.supplier_name,
			a.sku_simple, 
			a.name,
			a.brand,
			a.model, 
			a.barccode, 
			a.quantity, 
			a.cost_per_unit, 
			a.total_cost, 
			a.estimated_selling_price, 
			a.estimated_time_weeks, 
			a.product_bought_before, 			
			a.not_in_restock, 
			a.best_competitor_price, 
			a.name_competitor,
			a.Final_Quantity,
			a.Final_Total_Cost,
			a.product_attack,  
			a.price_point_last_30, 
			a.sku_simple_similar_product_sold, 
			a.items_sku_sold, 
			a.price,
			a.payment_fee,
			a.voucher_cost,
			a.delivery_cost_supplier,
			a.cost_bob,
			a.tax_percent,
			a.warehouse_stock,
			a.Marketplace,
			a.shipping_fee,
			a.shipping_cost,
			a.price_f,
			a.cost_f,
			a.PC15,
			b.price_point_tier
			
		FROM pricing.Purchase_user_tm6 a
		LEFT JOIN pricing.Purchase_user_tm7 b ON a.sku_simple_similar_product_sold = b.sku_simple
		GROUP BY a.sku_simple
		
);

DROP TABLE IF EXISTS david_dana.sku_actual_numbers_mx;

CREATE TABLE david_dana.sku_actual_numbers_mx ( INDEX( SKUSimple) )
	
	SELECT 
			SKUSimple, 
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=7,1,0),0)) AS num_sales_7, 
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=7,paidprice,0),0)) AS price_paid_7, 
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=7,ShippingFee,0),0)) AS shipping_fee_7,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=7,ShippingCost,0),0)) AS shipping_cost_7,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=7,Rev,0),0)) AS net_rev_7,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=7,PCOne,0),0)) AS PC1_7,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=7,PCOnePFive,0),0)) AS PC1_5_7,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=7,CouponValue,0),0)) AS CouponValue_7,

			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 7 AND 14 ,1,0),0)) AS num_sales_7_14, 
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 7 AND 14 ,paidprice,0),0)) AS price_paid_7_14, 
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 7 AND 14 ,ShippingFee,0),0)) AS shipping_fee_7_14,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 7 AND 14 ,ShippingCost,0),0)) AS shipping_cost_7_14,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 7 AND 14 ,Rev,0),0)) AS net_rev_7_14,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 7 AND 14 ,PCOne,0),0)) AS PC1_7_14,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 7 AND 14 ,PCOnePFive,0),0)) AS PC1_5_7_14,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 7 AND 14 ,CouponValue,0),0)) AS CouponValue_7_14,

			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=30,1,0),0)) AS num_sales_30, 
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=30,paidprice,0),0)) AS price_paid_30, 
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=30,ShippingFee,0),0)) AS shipping_fee_30,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=30,ShippingCost,0),0)) AS shipping_cost_30,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=30,Rev,0),0)) AS net_rev_30,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=30,PCOne,0),0)) AS PC1_30,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=30,PCOnePFive,0),0)) AS PC1_5_30,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date)<=30,CouponValue,0),0)) AS CouponValue_30,

			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 30 AND 60 ,1,0),0)) AS num_sales_30_60, 
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 30 AND 60 ,paidprice,0),0)) AS price_paid_30_60, 
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 30 AND 60 ,ShippingFee,0),0)) AS shipping_fee_30_60,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 30 AND 60 ,ShippingCost,0),0)) AS shipping_cost_30_60,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 30 AND 60 ,Rev,0),0)) AS net_rev_30_60,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 30 AND 60 ,PCOne,0),0)) AS PC1_30_60,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 30 AND 60 ,PCOnePFive,0),0)) AS PC1_5_30_60,
			SUM(if(r.OrderAfterCan=1,if(datediff(curdate(),r.Date) BETWEEN 30 AND 60 ,CouponValue,0),0)) AS CouponValue_30_60

	FROM development_mx.A_Master r
  WHERE datediff(curdate(),r.Date)<=60
  GROUP BY r.SKUSimple;


CREATE TABLE pricing.Purchase_user_tm9 (INDEX (sku_simple_similar_product_sold))(

		SELECT
			
			b.category, 
			b.sub_category, 
			b.sub_sub_category,
			b.supplier_id,
			b.supplier_name,
			b.sku_simple, 
			b.name,
			b.brand,
			b.model, 
			b.barccode, 
			b.quantity, 
			b.cost_per_unit, 
			b.total_cost, 
			b.estimated_selling_price, 
			b.estimated_time_weeks, 
			b.product_bought_before, 			
			b.not_in_restock, 
			b.best_competitor_price, 
			b.name_competitor,
			b.Final_Quantity,
			b.Final_Total_Cost,
			b.product_attack,  
			b.price_point_last_30, 
			b.sku_simple_similar_product_sold, 
			b.items_sku_sold, 
			b.price,
			b.payment_fee,
			b.voucher_cost,
			b.delivery_cost_supplier,
			b.cost_bob,
			b.tax_percent,
			b.warehouse_stock,
			b.Marketplace,
			b.shipping_fee,
			b.shipping_cost,
			b.price_f,
			b.cost_f,
			b.PC15,
			b.price_point_tier,
			Sum(c.num_sales_30) AS sales_last_30_days_price_point,
			sum(d.in_stock) AS in_stock

FROM pricing.Purchase_user_tm7 a
#INNER JOIN pricing.Purchase_user_tm8 b ON b.category = a.category AND b.sub_category = a.sub_category AND b.sub_sub_category = a.sub_sub_category AND b.price_point_tier = a.price_point_tier
LEFT JOIN pricing.Purchase_user_tm8 b ON a.sku_simple = b.sku_simple
LEFT JOIN david_dana.sku_actual_numbers_mx c ON a.sku_simple = c.SKUSimple
LEFT JOIN pricing.warehouse_stock_v2 d ON a.sku_simple = d.sku_simple
#GROUP BY  b.category, b.sub_category,b.sub_sub_category, b.price_point_tier
GROUP BY b.sku_simple
		
);

CREATE TABLE pricing.Purchase_Request_user (INDEX (sku_simple_similar_product_sold))(

		SELECT
				
				
				#b.supplier_id,
				#b.supplier_name,
				b.sku_simple,
				b.`name`,
				b.category,
				b.brand,
				b.model,
				b.barccode,
				b.quantity,
				b.cost_per_unit, 
				b.total_cost,
				b.estimated_selling_price,
				b.estimated_time_weeks,
				b.product_bought_before,
				'' AS nada,
				b.sku_simple_similar_product_sold,
				b.best_competitor_price,
				IF (b.best_competitor_price = 0,'No Competitor',b.name_competitor) AS name_competitor,
				b.Final_Quantity,
				b.Final_Total_Cost,
				b.sub_category,
				b.sub_sub_category,
				b.PC15,
				b.price_point_tier,
				a.in_stock,
				a.sales_last_30_days_price_point AS price_point_last_30,
				b.items_sku_sold
				#b.not_in_restock

		FROM pricing.Purchase_user_tm9 a
		LEFT JOIN pricing.Purchase_user_tm8 b ON a.category = b.category AND a.sub_category = b.sub_category AND a.sub_sub_category = b.sub_sub_category AND a.price_point_tier = b.price_point_tier
		
);

DROP TABLE IF EXISTS pricing.sales_SKUSimple;

		CREATE TABLE pricing.sales_SKUSimple (INDEX (sku_simple))(

			SELECT
				SKUSimple AS sku_simple,
				Count(*) AS NumItems,
				Sum(IF(CouponCode NOT IN ('REC%','MKT%','CAC%'),Rev,Rev+CouponValueAfterTax)) AS TotalRev,
				Avg(IF(CouponCode NOT IN ('REC%','MKT%','CAC%'),PCOnePFive,PCOnePFive+CouponValueAfterTax)) AS PCOnePFive,
				Avg (PaidPrice) AS Paid_Price
			
			FROM development_mx.A_Master
			WHERE DATEDIFF(Curdate(),Date) <= 30
				AND OrderAfterCan = 1
			GROUP BY SKUSimple
);

DROP TABLE IF EXISTS pricing.Days_in_stock_p;

		CREATE TABLE pricing.Days_in_stock_p (INDEX (sku_simple))(

			SELECT
				sku_simple,
				avg(days_in_stock) AS Avg_Days_in_WH

			FROM	operations_mx.out_stock_hist
			WHERE in_stock =1
			GROUP BY sku_simple
);

DROP TABLE IF EXISTS pricing.mx_cmr_prc_tbl_Purchase_Request_finaly;

CREATE TABLE pricing.mx_cmr_prc_tbl_Purchase_Request_finaly (INDEX (sku_simple_similar_product_sold))(

		SELECT
		
				b.sku_simple,
				b.`name`,
				b.category,
				b.brand,
				b.model,
				b.barccode,
				b.quantity,
				b.cost_per_unit,
				b.total_cost,
				b.estimated_selling_price,
				b.estimated_time_weeks,
				b.product_bought_before,
				'' AS nada,
				b.sku_simple_similar_product_sold,
				b.best_competitor_price, 
				b.name_competitor,
				b.sub_category,
				b.sub_sub_category,
				b.PC15,
				b.price_point_tier,
				IF (b.in_stock IS NULL,0,b.in_stock) AS in_stock,
				IF (b.price_point_last_30 IS NULL,0,b.price_point_last_30) AS price_point_last_30,
				'' AS nada2,
				#b.items_sku_sold,
				IF (c.NumItems IS NULL,0,c.NumItems) AS NumItems,
				IF (c.TotalRev IS NULL,0,c.TotalRev) AS TotalRev,
				IF (c.PCOnePFive IS NULL,0,c.PCOnePFive) AS PCOnePFive,
				IF (c.Paid_Price IS NULL,0,c.Paid_Price) AS Paid_Price,
				IF (d.Avg_Days_in_WH IS NULL,0,d.Avg_Days_in_WH) AS Avg_Days_in_WH,
				0 AS Real_stock,
				b.Final_Quantity,
				b.Final_Total_Cost

		FROM pricing.Purchase_Request_user b
		LEFT JOIN pricing.sales_SKUSimple c ON b.sku_simple_similar_product_sold=c.sku_simple
		LEFT JOIN pricing.Days_in_stock_p d ON b.sku_simple_similar_product_sold=d.sku_simple
		GROUP BY b.sku_simple
);

UPDATE pricing.mx_cmr_prc_tbl_Purchase_Request_finaly a
				INNER JOIN david_dana.mx_cmr_mp_tbl_real_stock b ON (a.sku_simple = b.sku_simple)
										SET a.Real_stock = b.Real_stock;



DROP TABLE IF EXISTS pricing.Purchase_user_tm1_two;
DROP TABLE IF EXISTS pricing.Purchase_user_tm2;
DROP TABLE IF EXISTS pricing.Purchase_user_tm3;
DROP TABLE IF EXISTS pricing.Purchase_user_tm4;
DROP TABLE IF EXISTS pricing.Purchase_user_tm5;
DROP TABLE IF EXISTS pricing.Purchase_user_tm6;
DROP TABLE IF EXISTS pricing.Purchase_user_tm7;
DROP TABLE IF EXISTS pricing.Purchase_user_tm8;
DROP TABLE IF EXISTS pricing.Purchase_user_tm9;
DROP TABLE IF EXISTS pricing.Purchase_Request_user;

TRUNCATE TABLE pricing.mx_cmr_prc_tbl_Purchase_user;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`gustavo.miranda`@`%`*/ /*!50003 PROCEDURE `pe_cmr_prc_sp_cleaning_price_tool`()
BEGIN
	
DROP TABLE IF EXISTS pricing.pe_cmr_prc_tbl_price_tool_tmp2;

DROP TABLE IF EXISTS pricing.pe_cmr_prc_tbl_price_tool_tmp1;

TRUNCATE TABLE pricing.pe_cmr_prc_tbl_catalog_user_price;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`gustavo.miranda`@`%`*/ /*!50003 PROCEDURE `pe_cmr_prc_sp_cleaning_Purchase_request`()
BEGIN
	
DROP TABLE IF EXISTS pricing.Purchase_user_tm1_two_pe;
DROP TABLE IF EXISTS pricing.Purchase_user_tm2_pe;
DROP TABLE IF EXISTS pricing.Purchase_user_tm3_pe;
DROP TABLE IF EXISTS pricing.Purchase_user_tm4_pe;
DROP TABLE IF EXISTS pricing.Purchase_user_tm5_pe;
DROP TABLE IF EXISTS pricing.Purchase_user_tm6_pe;
DROP TABLE IF EXISTS pricing.Purchase_user_tm7_pe;
DROP TABLE IF EXISTS pricing.Purchase_user_tm8_pe;
DROP TABLE IF EXISTS pricing.Purchase_user_tm9_pe;
DROP TABLE IF EXISTS pricing.Purchase_Request_user_pe;

TRUNCATE TABLE pricing.pe_cmr_prc_tbl_Purchase_user;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`gustavo.miranda`@`%`*/ /*!50003 PROCEDURE `pe_cmr_prc_sp_price_change_tool`()
BEGIN
	
 DROP TABLE IF EXISTS pricing.pe_cmr_prc_tbl_catalog_price_comple;

CREATE TABLE pricing.pe_cmr_prc_tbl_catalog_price_comple (INDEX(sku_config), INDEX(sku_simple))(

	SELECT 

		a.sku AS sku_config,
		b.sku AS sku_simple,
		b.fixed_price, # corregir cuando el atributo este en bob
		0 AS mp,
		c.isMarketPlace AS Marketplace,
		IF (b.cost IS NULL, 0, b.cost) AS cost,
		IF (b.special_price IS NULL, 0, b.special_price) AS special_price,
		IF (b.price IS NULL, 0, b.price) AS price
		
		
	FROM bob_live_pe.catalog_config a
	INNER JOIN bob_live_pe.catalog_simple b ON (a.id_catalog_config = b.fk_catalog_config)
	INNER JOIN development_pe.A_Master_Catalog c ON (c.sku_simple = b.sku)
	GROUP BY b.sku
);

UPDATE pricing.pe_cmr_prc_tbl_catalog_price_comple a
INNER JOIN pricing.pe_cmr_prc_tbl_tmp_price_tool b ON (a.sku_config = b.sku_config)
SET a.mp = 1;

TRUNCATE TABLE pricing.pe_cmr_prc_tbl_catalog_user_price_tmp1;

		INSERT INTO pricing.pe_cmr_prc_tbl_catalog_user_price_tmp1 (

			sku_config,
			sku_simple,
			cost,
			special_price,
			price
)
				SELECT 
					
					sku_config,
					sku_simple,
					IF (cost = ' ', -1, cost) AS cost,
					IF (special_price = ' ', -1, special_price) AS special_price,
					IF (price = ' ', -1, price) AS price


				FROM pricing.pe_cmr_prc_tbl_catalog_user_price;


DROP TABLE IF EXISTS pricing.pe_cmr_prc_tbl_catalog_price_desicion;

CREATE TABLE pricing.pe_cmr_prc_tbl_catalog_price_desicion (INDEX(sku_config), INDEX(sku_simple))(

	SELECT 

		a.sku_config,
		a.sku_simple,
		b.fixed_price,
		b.Marketplace,
		b.mp,
		IF (a.price = -1, b.price, a.price) AS price,
		a.price AS price_new,
		b.price AS price_old,
		IF (a.special_price = -1, b.special_price, a.special_price) AS special_price,
		a.special_price AS special_price_new,
		b.special_price AS special_price_old,
		IF (a.cost = -1, b.cost, a.cost) AS cost,
		a.cost AS cost_new,
		b.cost AS cost_old
		
	FROM pricing.pe_cmr_prc_tbl_catalog_user_price_tmp1 a
	LEFT JOIN pricing.pe_cmr_prc_tbl_catalog_price_comple b ON (a.sku_simple = b.sku_simple)
	GROUP BY a.sku_simple
);

CREATE TABLE pricing.pe_cmr_prc_tbl_price_tool_tmp1 (INDEX(sku_config), INDEX(sku_simple))(

	SELECT 

		sku_config,
		sku_simple,
		fixed_price,
		Marketplace,
		mp,
		cost,
		special_price,
		price,

		IF ( cost = 0 AND price = 0, 'Check, Cost = 0 and Price = 0',
		IF ( cost = 0, 'Check, Cost = 0',
		IF ( price = 0, 'Check, price = 0',
		IF ( special_price = 0, 'Check, special_price = 0',
		IF ( ( (cost_old) * 8 ) < cost AND cost < ( (cost_old) * 1.2 ), 'Check, The cost is very large',
		IF ( price > ( (cost) * 10 ), 'Check, High price', 
		IF ( special_price > ( (cost) * 10 ), 'Check, High special_price',
		IF ( cost <= special_price AND cost <= price AND special_price < price, 'Ok',
		IF ( cost <= special_price AND cost <= price AND special_price >= price, 'Check, special_price >= price',
		IF ( cost <= special_price AND cost > price AND special_price < price, 'Check, price < cost',
		IF ( cost > special_price AND cost <= price AND special_price < price, 'Check, special_price < cost',
		IF ( cost <= special_price AND cost > price AND special_price >= price, 'Check, special_price >= price and price < cost',
		IF ( cost > special_price AND cost <= price AND special_price >= price, 'Check, special_price >= price and special_price < cost',
		IF ( cost > special_price AND cost > price AND special_price < price, 'Check, price < cost and special_price < cost', 'Check, price < cost, special_price < cost and special_price >= price'
		)))))))))))))) AS Action_result,
		
		IF ( Marketplace = 1, 'Marketplace Product',
		IF ( fixed_price = 1, 'Fixed Price',
		IF ( mp = 1, 'Marketing Plan',
		IF ( cost = 0 AND price = 0, 'Pricing',
		IF ( cost = 0, 'Pricing',
		IF ( price = 0, 'Pricing',
		IF ( special_price = 0, 'Pricing',
		IF ( ( (cost_old) * 8 ) < cost AND cost < ( (cost_old) * 1.2 ), 'Pricing',
		IF ( price > ( (cost) * 10 ), 'Pricing', 
		IF ( special_price > ( (cost) * 10 ), 'Pricing',
		IF ( cost <= special_price AND cost <= price AND special_price < price, 'Ok',
		IF ( cost <= special_price AND cost <= price AND special_price >= price, 'Pricing',
		IF ( cost <= special_price AND cost > price AND special_price < price, 'Pricing',
		IF ( cost > special_price AND cost <= price AND special_price < price, 'Pricing',
		IF ( cost <= special_price AND cost > price AND special_price >= price, 'Pricing',
		IF ( cost > special_price AND cost <= price AND special_price >= price, 'Pricing',
		IF ( cost > special_price AND cost > price AND special_price < price, 'Pricing', 'Pricing'
		))))))))))))))))) AS Check_Final
	
	FROM pricing.pe_cmr_prc_tbl_catalog_price_desicion
	GROUP BY sku_simple
);

CREATE TABLE pricing.pe_cmr_prc_tbl_price_tool_tmp2 (INDEX(sku_config), INDEX(sku_simple))(

	SELECT 

		a.sku_config,
		a.sku_simple,
		IF (b.cost = -1 , ' ', b.cost) AS cost_order,
		IF (b.special_price = -1, ' ', b.special_price) AS special_price_order,
		IF (b.price = -1, ' ', b.price) AS price_order,
		IF (a.cost = 0, ' ', a.cost) AS cost_db,
		IF (a.special_price = 0, ' ', a.special_price) AS special_price_db,
		IF (a.price = 0, ' ', a.price) AS price_db,
		a.Action_result,
		a.Check_Final

	FROM pricing.pe_cmr_prc_tbl_price_tool_tmp1 a
	LEFT JOIN pricing.pe_cmr_prc_tbl_catalog_user_price b ON (a.sku_simple = b.sku_simple)
	GROUP BY a.sku_simple
);

TRUNCATE TABLE pricing.pe_cmr_prc_tbl_price_tool;

		INSERT INTO pricing.pe_cmr_prc_tbl_price_tool (

			sku_config,
			sku_simple,
			cost_order,
			special_price_order,
			price_order,
			cost_db,
			special_price_db,
			price_db,
			Action_result,
			Check_Final
)

				SELECT 
					
					sku_config,
					sku_simple,
					cost_order,
					special_price_order,
					price_order,
					cost_db,
					special_price_db,
					price_db,
					Action_result,
					Check_Final

				FROM pricing.pe_cmr_prc_tbl_price_tool_tmp2;


DROP TABLE IF EXISTS pricing.pe_cmr_prc_tbl_price_tool_tmp2;

DROP TABLE IF EXISTS pricing.pe_cmr_prc_tbl_price_tool_tmp1;

TRUNCATE TABLE pricing.pe_cmr_prc_tbl_catalog_user_price;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`gustavo.miranda`@`%`*/ /*!50003 PROCEDURE `pe_mcr_prc_sp_Purchase_Request`()
BEGIN
	
CREATE TABLE  pricing.Purchase_user_tm2_pe  (INDEX (sku_simple))(
	
		SELECT
			m2.`name` AS category,
			m3.`name` AS sub_category,
			m4.`name` AS sub_sub_category,
			m6.`name` AS supplier,
			m5.`name` AS brand,
			m1.`name`,
			m1.model,
			m1.sku AS sku_config,
			m7.sku AS sku_simple,
			m7.price,
			m7.special_price,
			m10.tax_percent / 100 AS tax_percent,  
			m7.cost AS cost_bob,
			m7.cost / ( 1 + m10.tax_percent / 100 ) AS cost_after_tax,
			IF ((m7.delivery_cost_supplier) IS NULL, 0, (m7.delivery_cost_supplier)) AS delivery_cost_supplier,
			0 AS fixed_price, #bob_live_mx.catalog_simple.fixed_price, --- corregir m7
			0 AS sell_list,    #bob_live_mx.catalog_simple.sell_list ---corregir m7
			IFNULL( m8.quantity,0) + IFNULL(m9.quantity,0) AS stock,
			IFNULL( m9.quantity,0 ) AS warehouse_stock,
			m7.is_option_marketplace AS Marketplace,
			0 AS sku_not_comparable # corregir m1
			
		FROM bob_live_pe.catalog_config m1
		INNER JOIN bob_live_pe.catalog_attribute_option_global_category m2 ON m1.fk_catalog_attribute_option_global_category = m2.id_catalog_attribute_option_global_category
		INNER JOIN bob_live_pe.catalog_attribute_option_global_sub_category  m3 ON m1.fk_catalog_attribute_option_global_sub_category = m3.id_catalog_attribute_option_global_sub_category
		INNER JOIN bob_live_pe.catalog_attribute_option_global_sub_sub_category m4 ON m1.fk_catalog_attribute_option_global_sub_sub_category = m4.id_catalog_attribute_option_global_sub_sub_category
		INNER JOIN bob_live_pe.catalog_brand m5 ON m1.fk_catalog_brand = m5.id_catalog_brand
		INNER JOIN bob_live_pe.catalog_simple m7 ON m1.id_catalog_config = m7.fk_catalog_config
		LEFT JOIN bob_live_pe.catalog_supplier_stock m8 ON m7.id_catalog_simple = m8.fk_catalog_simple
		LEFT JOIN bob_live_pe.catalog_warehouse_stock m9 ON m7.id_catalog_simple = m9.fk_catalog_simple
		INNER JOIN bob_live_pe.catalog_tax_class m10 ON m7.fk_catalog_tax_class = m10.id_catalog_tax_class
		INNER JOIN bob_live_pe.catalog_source m11 ON m7.id_catalog_simple = m11.fk_catalog_simple
		INNER JOIN bob_live_pe.supplier m6 ON m11.fk_supplier = m6.id_supplier
		);


CREATE TABLE pricing.Purchase_user_tm1_two_pe (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			sku_simple,
			name,
			category,
			brand,
			model,
			barccode,
			quantity,
			cost_per_unit,
			total_cost,
			estimated_selling_price,
			estimated_time_weeks,
			product_bought_before,
			IF (product_bought_before='yes', sku_simple, sku_simple_similar_product_sold) AS sku_simple_similar_product_sold,
			best_competitor_price,
			name_competitor,
			Final_Quantity,
			Final_Total_Cost

	
		FROM pricing.pe_cmr_prc_tbl_Purchase_user
		GROUP BY sku_simple
);

CREATE TABLE pricing.Purchase_user_tm3_pe (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			b.category, 
			b.sub_category, 
			b.sub_sub_category,
			NULL AS supplier_id,
			NULL AS supplier_name,
			a.sku_simple, 
			a.name,
			a.brand,
			a.model, 
			a.barccode, 
			a.quantity, 
			a.cost_per_unit, 
			a.total_cost, 
			a.estimated_selling_price, 
			a.estimated_time_weeks, 
			a.product_bought_before, 			
			0 AS not_in_restock, 
			a.best_competitor_price, 
			a.name_competitor,
			a.Final_Quantity,
			a.Final_Total_Cost,
			0 AS product_attack, 
			0 AS in_stock, 
			0 AS price_point_last_30, 
			a.sku_simple_similar_product_sold, 
			0 AS items_sku_sold, 
			b.price,
			IF (best_competitor_price > 0, ((best_competitor_price/1.16)*0.0169), IF (estimated_selling_price > 0, ((estimated_selling_price/1.16)*0.0169),((price/1.16)*0.0169))) AS payment_fee,
			IF (best_competitor_price > 0, ((best_competitor_price/1.16)*0.04), IF (estimated_selling_price > 0, ((estimated_selling_price/1.16)*0.04),((price/1.16)*0.04))) AS voucher_cost,
			#0 AS payment_fee,
			#0 AS voucher_cost,
			b.delivery_cost_supplier,
			b.cost_bob,
			b.tax_percent,
			b.warehouse_stock,
			b.Marketplace

		FROM pricing.Purchase_user_tm1_two_pe a 
		LEFT JOIN pricing.Purchase_user_tm2_pe b ON a.sku_simple_similar_product_sold = b.sku_simple
		GROUP BY a.sku_simple
		);

CREATE TABLE pricing.Purchase_user_tm4_pe (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			a.category, 
			a.sub_category, 
			a.sub_sub_category,
			a.supplier_id,
			a.supplier_name,
			a.sku_simple, 
			a.name,
			a.brand,
			a.model, 
			a.barccode, 
			a.quantity, 
			a.cost_per_unit, 
			a.total_cost, 
			a.estimated_selling_price, 
			a.estimated_time_weeks, 
			a.product_bought_before, 			
			a.not_in_restock, 
			a.best_competitor_price, 
			a.name_competitor, 
			a.Final_Quantity,
			a.Final_Total_Cost,
			a.product_attack, 
			a.in_stock, 
			a.price_point_last_30, 
			a.sku_simple_similar_product_sold, 
			a.items_sku_sold, 
			a.price,
			a.payment_fee,
			a.voucher_cost,
			a.delivery_cost_supplier,
			a.cost_bob,
			a.tax_percent,
			a.warehouse_stock,
			a.Marketplace,
			b.shipping_fee,
			b.shipping_cost

		FROM pricing.Purchase_user_tm3_pe a 
		LEFT JOIN david_dana.shipping_fee_per_sku_pe b ON a.sku_simple_similar_product_sold = b.sku_simple
		GROUP BY a.sku_simple
		);

CREATE TABLE pricing.Purchase_user_tm5_pe (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			category, 
			sub_category, 
			sub_sub_category,
			supplier_id,
			supplier_name,
			sku_simple, 
			name,
			brand,
			model, 
			barccode, 
			quantity, 
			cost_per_unit, 
			total_cost, 
			estimated_selling_price, 
			estimated_time_weeks, 
			product_bought_before, 			
			not_in_restock, 
			best_competitor_price, 
			name_competitor, 
			Final_Quantity,
			Final_Total_Cost,
			product_attack, 
			in_stock, 
			price_point_last_30, 
			sku_simple_similar_product_sold, 
			items_sku_sold, 
			price,
			payment_fee,
			voucher_cost,
			delivery_cost_supplier,
			cost_bob,
			tax_percent,
			warehouse_stock,
			Marketplace,
			IF (shipping_fee IS NULL,0,shipping_fee) AS shipping_fee,
			IF (shipping_cost IS NULL,0, shipping_cost) AS shipping_cost,
			IF (best_competitor_price > 0, best_competitor_price, IF(estimated_selling_price > 0, estimated_selling_price, price)) AS price_f,
			IF (cost_per_unit > 0, cost_per_unit, cost_bob) AS cost_f

		FROM pricing.Purchase_user_tm4_pe 
		GROUP BY sku_simple
);

CREATE TABLE pricing.Purchase_user_tm6_pe (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			category, 
			sub_category, 
			sub_sub_category,
			supplier_id,
			supplier_name,
			sku_simple, 
			name,
			brand,
			model, 
			barccode, 
			quantity, 
			cost_per_unit, 
			total_cost, 
			estimated_selling_price, 
			estimated_time_weeks, 
			product_bought_before, 			
			not_in_restock, 
			best_competitor_price, 
			name_competitor,
			Final_Quantity,
			Final_Total_Cost,
			product_attack, 
			in_stock, 
			price_point_last_30, 
			sku_simple_similar_product_sold, 
			items_sku_sold, 
			price,
			payment_fee,
			voucher_cost,
			delivery_cost_supplier,
			cost_bob,
			tax_percent,
			warehouse_stock,
			Marketplace,
			shipping_fee,
			shipping_cost,
			price_f,
			cost_f,
			((price_f/1.16) + (shipping_fee/1.16) - (delivery_cost_supplier/1.16) - (cost_f/1.16) - (shipping_cost/1.16) - payment_fee - voucher_cost) AS PC15
			

		FROM pricing.Purchase_user_tm5_pe
		GROUP BY sku_simple
);

CREATE TABLE pricing.Purchase_user_tm7_pe (INDEX (sku_simple))(

		SELECT
			'Linio' AS Competitor,
			m1.sku AS sku_config,
			m2.sku AS sku_simple,
			m7.`name` AS Brand,
			m1.`name`,
			m6.price_f,
			m3.`name` AS category,
			m4.`name` AS sub_category,
			m5.`name` AS sub_sub_category,
			m8.price_point_tier_1,
			m8.price_point_tier_2,
			m8.price_point_tier_3,
			m8.price_point_tier_4,
			m8.price_point_tier_5,
			m8.price_point_tier_6,
			m8.price_point_tier_7,

				CASE WHEN m6.price_f < m8.price_point_tier_1 THEN 1
				WHEN m6.price_f < m8.price_point_tier_2      THEN 2
				WHEN m6.price_f < m8.price_point_tier_3      THEN 3
				WHEN m6.price_f < m8.price_point_tier_4      THEN 4
				WHEN m6.price_f < m8.price_point_tier_5      THEN 5
				WHEN m6.price_f < m8.price_point_tier_6      THEN 6
				WHEN m6.price_f < m8.price_point_tier_7      THEN 7
				ELSE 8
				END AS price_point_tier

		FROM bob_live_pe.catalog_config m1
		INNER JOIN bob_live_pe.catalog_simple m2 ON m1.id_catalog_config = m2.fk_catalog_config
		INNER JOIN bob_live_pe.catalog_attribute_option_global_category m3 ON m1.fk_catalog_attribute_option_global_category = m3.id_catalog_attribute_option_global_category
		INNER JOIN bob_live_pe.catalog_attribute_option_global_sub_category m4 ON m1.fk_catalog_attribute_option_global_sub_category = m4.id_catalog_attribute_option_global_sub_category
		INNER JOIN bob_live_pe.catalog_attribute_option_global_sub_sub_category m5 ON m1.fk_catalog_attribute_option_global_sub_sub_category = m5.id_catalog_attribute_option_global_sub_sub_category
		INNER JOIN pricing.Purchase_user_tm5_pe m6 ON m2.sku = m6.sku_simple_similar_product_sold
		INNER JOIN bob_live_pe.catalog_brand m7 ON m1.fk_catalog_brand = m7.id_catalog_brand
		LEFT  JOIN david_dana.`price_points_per_category_key_pe` m8 ON m3.`name` = m8.category AND m4.`name` = m8.sub_category AND m5.`name` = m8.sub_sub_category

		GROUP BY m2.sku
);

DROP TABLE IF EXISTS pricing.warehouse_stock_v1_pe;
CREATE TABLE pricing.warehouse_stock_v1_pe AS (

				SELECT 
					sku_simple,
					sum(in_stock) AS stock_wms, 
					sum(reserved) AS stock_reserved_wms

				FROM operations_pe.out_stock_hist
				GROUP BY sku_simple
		);

DROP TABLE IF EXISTS pricing.warehouse_stock_v2_pe;
CREATE TABLE pricing.warehouse_stock_v2_pe AS (

				SELECT 
					sku_simple,
					(stock_wms - stock_reserved_wms) AS in_stock

				FROM pricing.warehouse_stock_v1_pe
				GROUP BY sku_simple
		);

CREATE TABLE pricing.Purchase_user_tm8_pe (INDEX (sku_simple_similar_product_sold))(

		SELECT

			a.category, 
			a.sub_category, 
			a.sub_sub_category,
			a.supplier_id,
			a.supplier_name,
			a.sku_simple, 
			a.name,
			a.brand,
			a.model, 
			a.barccode, 
			a.quantity, 
			a.cost_per_unit, 
			a.total_cost, 
			a.estimated_selling_price, 
			a.estimated_time_weeks, 
			a.product_bought_before, 			
			a.not_in_restock, 
			a.best_competitor_price, 
			a.name_competitor,
			a.Final_Quantity,
			a.Final_Total_Cost,
			a.product_attack,  
			a.price_point_last_30, 
			a.sku_simple_similar_product_sold, 
			a.items_sku_sold, 
			a.price,
			a.payment_fee,
			a.voucher_cost,
			a.delivery_cost_supplier,
			a.cost_bob,
			a.tax_percent,
			a.warehouse_stock,
			a.Marketplace,
			a.shipping_fee,
			a.shipping_cost,
			a.price_f,
			a.cost_f,
			a.PC15,
			b.price_point_tier
			
		FROM pricing.Purchase_user_tm6_pe a
		INNER JOIN pricing.Purchase_user_tm7_pe b ON a.sku_simple_similar_product_sold = b.sku_simple
		GROUP BY a.sku_simple
		
);

CREATE TABLE pricing.Purchase_user_tm9_pe (INDEX (sku_simple_similar_product_sold))(

		SELECT
			
			b.category, 
			b.sub_category, 
			b.sub_sub_category,
			b.supplier_id,
			b.supplier_name,
			b.sku_simple, 
			b.name,
			b.brand,
			b.model, 
			b.barccode, 
			b.quantity, 
			b.cost_per_unit, 
			b.total_cost, 
			b.estimated_selling_price, 
			b.estimated_time_weeks, 
			b.product_bought_before, 			
			b.not_in_restock, 
			b.best_competitor_price, 
			b.name_competitor,
			b.Final_Quantity,
			b.Final_Total_Cost,
			b.product_attack,  
			b.price_point_last_30, 
			b.sku_simple_similar_product_sold, 
			b.items_sku_sold, 
			b.price,
			b.payment_fee,
			b.voucher_cost,
			b.delivery_cost_supplier,
			b.cost_bob,
			b.tax_percent,
			b.warehouse_stock,
			b.Marketplace,
			b.shipping_fee,
			b.shipping_cost,
			b.price_f,
			b.cost_f,
			b.PC15,
			b.price_point_tier,
			Sum(c.num_sales_30) AS sales_last_30_days_price_point,
			sum(d.in_stock) AS in_stock

FROM pricing.Purchase_user_tm7_pe a
INNER JOIN pricing.Purchase_user_tm8_pe b ON b.category = a.category AND b.sub_category = a.sub_category AND b.sub_sub_category = a.sub_sub_category AND b.price_point_tier = a.price_point_tier
LEFT JOIN david_dana.sku_actual_numbers_pe c ON a.sku_simple = c.SKUSimple
LEFT JOIN pricing.warehouse_stock_v2_pe d ON a.sku_simple = d.sku_simple
GROUP BY  b.category, b.sub_category,b.sub_sub_category, b.price_point_tier
		
);

CREATE TABLE pricing.Purchase_Request_user_pe (INDEX (sku_simple_similar_product_sold))(

		SELECT
				
				
				#b.supplier_id,
				#b.supplier_name,
				b.sku_simple,
				b.`name`,
				b.category,
				b.brand,
				b.model,
				b.barccode,
				b.quantity,
				b.cost_per_unit, 
				b.total_cost,
				b.estimated_selling_price,
				b.estimated_time_weeks,
				b.product_bought_before,
				'' AS nada,
				b.sku_simple_similar_product_sold,
				b.best_competitor_price,
				IF (b.best_competitor_price = 0,'No Competitor',b.name_competitor) AS name_competitor,
				b.Final_Quantity,
				b.Final_Total_Cost,
				b.sub_category,
				b.sub_sub_category,
				b.PC15,
				b.price_point_tier,
				a.in_stock,
				a.sales_last_30_days_price_point AS price_point_last_30,
				b.items_sku_sold
				#b.not_in_restock

		FROM pricing.Purchase_user_tm9_pe a
		INNER JOIN pricing.Purchase_user_tm8_pe b ON a.category = b.category AND a.sub_category = b.sub_category AND a.sub_sub_category = b.sub_sub_category AND a.price_point_tier = b.price_point_tier
		
);

DROP TABLE IF EXISTS pricing.sales_SKUSimple_pe;

		CREATE TABLE pricing.sales_SKUSimple_pe (INDEX (sku_simple))(

			SELECT
				SKUSimple AS sku_simple,
				Count(*) AS NumItems,
				Sum(IF(CouponCode NOT IN ('REC%','MKT%','CAC%'),Rev,Rev+CouponValueAfterTax)) AS TotalRev,
				Avg(IF(CouponCode NOT IN ('REC%','MKT%','CAC%'),PCOnePFive,PCOnePFive+CouponValueAfterTax)) AS PCOnePFive,
				Avg (PaidPrice) AS Paid_Price
			
			FROM development_pe.A_Master
			WHERE DATEDIFF(Curdate(),Date) <= 30
				AND OrderAfterCan = 1
			GROUP BY SKUSimple
);

DROP TABLE IF EXISTS pricing.Days_in_stock_p_pe;

		CREATE TABLE pricing.Days_in_stock_p_pe (INDEX (sku_simple))(

			SELECT
				sku_simple,
				avg(days_in_stock) AS Avg_Days_in_WH

			FROM	operations_pe.out_stock_hist
			GROUP BY sku_simple
);

DROP TABLE IF EXISTS pricing.pe_cmr_prc_tbl_Purchase_Request_finaly;

CREATE TABLE pricing.pe_cmr_prc_tbl_Purchase_Request_finaly (INDEX (sku_simple_similar_product_sold))(

		SELECT
		
				b.sku_simple,
				b.`name`,
				b.category,
				b.brand,
				b.model,
				b.barccode,
				b.quantity,
				b.cost_per_unit,
				b.total_cost,
				b.estimated_selling_price,
				b.estimated_time_weeks,
				b.product_bought_before,
				'' AS nada,
				b.sku_simple_similar_product_sold,
				b.best_competitor_price, 
				b.name_competitor,
				b.sub_category,
				b.sub_sub_category,
				b.PC15,
				b.price_point_tier,
				IF (b.in_stock IS NULL,0,b.in_stock) AS in_stock,
				IF (b.price_point_last_30 IS NULL,0,b.price_point_last_30) AS price_point_last_30,
				'' AS nada2,
				#b.items_sku_sold,
				IF (c.NumItems IS NULL,0,c.NumItems) AS NumItems,
				IF (c.TotalRev IS NULL,0,c.TotalRev) AS TotalRev,
				IF (c.PCOnePFive IS NULL,0,c.PCOnePFive) AS PCOnePFive,
				IF (c.Paid_Price IS NULL,0,c.Paid_Price) AS Paid_Price,
				IF (d.Avg_Days_in_WH IS NULL,0,d.Avg_Days_in_WH) AS Avg_Days_in_WH,
				0 AS Real_stock,
				b.Final_Quantity,
				b.Final_Total_Cost

		FROM pricing.Purchase_Request_user_pe b
		LEFT JOIN pricing.sales_SKUSimple_pe c ON b.sku_simple_similar_product_sold=c.sku_simple
		LEFT JOIN pricing.Days_in_stock_p_pe d ON b.sku_simple_similar_product_sold=d.sku_simple
		GROUP BY b.sku_simple
);

# ver que hago con esta parte

/*UPDATE pricing.co_cmr_prc_tbl_Purchase_Request_finaly a
				INNER JOIN david_dana.mx_cmr_mp_tbl_real_stock b ON (a.sku_simple = b.sku_simple)
										SET a.Real_stock = b.Real_stock;*/


DROP TABLE IF EXISTS pricing.Purchase_user_tm1_two_pe;
DROP TABLE IF EXISTS pricing.Purchase_user_tm2_pe;
DROP TABLE IF EXISTS pricing.Purchase_user_tm3_pe;
DROP TABLE IF EXISTS pricing.Purchase_user_tm4_pe;
DROP TABLE IF EXISTS pricing.Purchase_user_tm5_pe;
DROP TABLE IF EXISTS pricing.Purchase_user_tm6_pe;
DROP TABLE IF EXISTS pricing.Purchase_user_tm7_pe;
DROP TABLE IF EXISTS pricing.Purchase_user_tm8_pe;
DROP TABLE IF EXISTS pricing.Purchase_user_tm9_pe;
DROP TABLE IF EXISTS pricing.Purchase_Request_user_pe;

TRUNCATE TABLE pricing.pe_cmr_prc_tbl_Purchase_user;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`manuel.yrigoyen`@`%`*/ /*!50003 PROCEDURE `prc_aux_price2spy_upload`()
BEGIN

DROP TABLE IF EXISTS pricing.aux_price2spy_upload;

CREATE TEMPORARY TABLE pricing.aux_price2spy_upload (INDEX (SKU_CONFIG))(

SELECT
   a.sku_config,
   e.`name` AS category,
   f.`name` AS sub_category,
   'México' AS supplier,
   b.`name` AS brand,
   c.`name`,
   CONVERT(d.URL USING utf8) as url
   
  FROM pricing.sku_config_input a
  INNER JOIN bob_live_mx.catalog_config c ON a.sku_config = c.sku
  INNER JOIN bob_live_mx.catalog_attribute_option_global_category e ON c.fk_catalog_attribute_option_global_category = e.id_catalog_attribute_option_global_category
  INNER JOIN bob_live_mx.catalog_attribute_option_global_sub_category  f ON c.fk_catalog_attribute_option_global_sub_category = f.id_catalog_attribute_option_global_sub_category
  INNER JOIN bob_live_mx.catalog_brand b ON c.fk_catalog_brand = b.id_catalog_brand
  INNER JOIN david_dana.sku_url d ON d.sku = a.sku_config
);

DROP TABLE IF EXISTS pricing.price2spy_upload;

CREATE TABLE pricing.price2spy_upload AS (

SELECT
   sku_config,
   IF (category = 'Electrónicos',sub_category, category) AS category,
   supplier,
   brand,
   `name`,
   url
FROM pricing.aux_price2spy_upload
);
	

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`gustavo.miranda`@`%`*/ /*!50003 PROCEDURE `ve_cmr_prc_sp_cleaning_price_tool`()
BEGIN
	
DROP TABLE IF EXISTS pricing.ve_cmr_prc_tbl_price_tool_tmp2;

DROP TABLE IF EXISTS pricing.ve_cmr_prc_tbl_price_tool_tmp1;

TRUNCATE TABLE pricing.ve_cmr_prc_tbl_catalog_user_price;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`gustavo.miranda`@`%`*/ /*!50003 PROCEDURE `ve_cmr_prc_sp_cleaning_Purchase_request`()
BEGIN
	
DROP TABLE IF EXISTS pricing.Purchase_user_tm1_two_ve;
DROP TABLE IF EXISTS pricing.Purchase_user_tm2_ve;
DROP TABLE IF EXISTS pricing.Purchase_user_tm3_ve;
DROP TABLE IF EXISTS pricing.Purchase_user_tm4_ve;
DROP TABLE IF EXISTS pricing.Purchase_user_tm5_ve;
DROP TABLE IF EXISTS pricing.Purchase_user_tm6_ve;
DROP TABLE IF EXISTS pricing.Purchase_user_tm7_ve;
DROP TABLE IF EXISTS pricing.Purchase_user_tm8_ve;
DROP TABLE IF EXISTS pricing.Purchase_user_tm9_ve;
DROP TABLE IF EXISTS pricing.Purchase_Request_user_ve;

TRUNCATE TABLE pricing.ve_cmr_prc_tbl_Purchase_user;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`gustavo.miranda`@`%`*/ /*!50003 PROCEDURE `ve_cmr_prc_sp_price_change_tool`()
BEGIN
	
 DROP TABLE IF EXISTS pricing.ve_cmr_prc_tbl_catalog_price_comple;

CREATE TABLE pricing.ve_cmr_prc_tbl_catalog_price_comple (INDEX(sku_config), INDEX(sku_simple))(

	SELECT 

		a.sku AS sku_config,
		b.sku AS sku_simple,
		b.fixed_price, # corregir cuando el atributo este en bob
		0 AS mp,
		c.isMarketPlace AS Marketplace,
		IF (b.cost IS NULL, 0, b.cost) AS cost,
		IF (b.special_price IS NULL, 0, b.special_price) AS special_price,
		IF (b.price IS NULL, 0, b.price) AS price
		
		
	FROM bob_live_ve.catalog_config a
	INNER JOIN bob_live_ve.catalog_simple b ON (a.id_catalog_config = b.fk_catalog_config)
	INNER JOIN development_ve.A_Master_Catalog c ON (c.sku_simple = b.sku)
	GROUP BY b.sku
);

UPDATE pricing.ve_cmr_prc_tbl_catalog_price_comple a
INNER JOIN pricing.ve_cmr_prc_tbl_tmp_price_tool b ON (a.sku_config = b.sku_config)
SET a.mp = 1;

TRUNCATE TABLE pricing.ve_cmr_prc_tbl_catalog_user_price_tmp1;

		INSERT INTO pricing.ve_cmr_prc_tbl_catalog_user_price_tmp1 (

			sku_config,
			sku_simple,
			cost,
			special_price,
			price
)
				SELECT 
					
					sku_config,
					sku_simple,
					IF (cost = ' ', -1, cost) AS cost,
					IF (special_price = ' ', -1, special_price) AS special_price,
					IF (price = ' ', -1, price) AS price


				FROM pricing.ve_cmr_prc_tbl_catalog_user_price;


DROP TABLE IF EXISTS pricing.ve_cmr_prc_tbl_catalog_price_desicion;

CREATE TABLE pricing.ve_cmr_prc_tbl_catalog_price_desicion (INDEX(sku_config), INDEX(sku_simple))(

	SELECT 

		a.sku_config,
		a.sku_simple,
		b.fixed_price,
		b.Marketplace,
		b.mp,
		IF (a.price = -1, b.price, a.price) AS price,
		a.price AS price_new,
		b.price AS price_old,
		IF (a.special_price = -1, b.special_price, a.special_price) AS special_price,
		a.special_price AS special_price_new,
		b.special_price AS special_price_old,
		IF (a.cost = -1, b.cost, a.cost) AS cost,
		a.cost AS cost_new,
		b.cost AS cost_old
		
	FROM pricing.ve_cmr_prc_tbl_catalog_user_price_tmp1 a
	LEFT JOIN pricing.ve_cmr_prc_tbl_catalog_price_comple b ON (a.sku_simple = b.sku_simple)
	GROUP BY a.sku_simple
);

CREATE TABLE pricing.ve_cmr_prc_tbl_price_tool_tmp1 (INDEX(sku_config), INDEX(sku_simple))(

	SELECT 

		sku_config,
		sku_simple,
		fixed_price,
		Marketplace,
		mp,
		cost,
		special_price,
		price,

		IF ( cost = 0 AND price = 0, 'Check, Cost = 0 and Price = 0',
		IF ( cost = 0, 'Check, Cost = 0',
		IF ( price = 0, 'Check, price = 0',
		IF ( special_price = 0, 'Check, special_price = 0',
		IF ( ( (cost_old) * 8 ) < cost AND cost < ( (cost_old) * 1.2 ), 'Check, The cost is very large',
		IF ( price > ( (cost) * 10 ), 'Check, High price', 
		IF ( special_price > ( (cost) * 10 ), 'Check, High special_price',
		IF ( cost <= special_price AND cost <= price AND special_price < price, 'Ok',
		IF ( cost <= special_price AND cost <= price AND special_price >= price, 'Check, special_price >= price',
		IF ( cost <= special_price AND cost > price AND special_price < price, 'Check, price < cost',
		IF ( cost > special_price AND cost <= price AND special_price < price, 'Check, special_price < cost',
		IF ( cost <= special_price AND cost > price AND special_price >= price, 'Check, special_price >= price and price < cost',
		IF ( cost > special_price AND cost <= price AND special_price >= price, 'Check, special_price >= price and special_price < cost',
		IF ( cost > special_price AND cost > price AND special_price < price, 'Check, price < cost and special_price < cost', 'Check, price < cost, special_price < cost and special_price >= price'
		)))))))))))))) AS Action_result,
		
		IF ( Marketplace = 1, 'Marketplace Product',
		IF ( fixed_price = 1, 'Fixed Price',
		IF ( mp = 1, 'Marketing Plan',
		IF ( cost = 0 AND price = 0, 'Pricing',
		IF ( cost = 0, 'Pricing',
		IF ( price = 0, 'Pricing',
		IF ( special_price = 0, 'Pricing',
		IF ( ( (cost_old) * 8 ) < cost AND cost < ( (cost_old) * 1.2 ), 'Pricing',
		IF ( price > ( (cost) * 10 ), 'Pricing', 
		IF ( special_price > ( (cost) * 10 ), 'Pricing',
		IF ( cost <= special_price AND cost <= price AND special_price < price, 'Ok',
		IF ( cost <= special_price AND cost <= price AND special_price >= price, 'Pricing',
		IF ( cost <= special_price AND cost > price AND special_price < price, 'Pricing',
		IF ( cost > special_price AND cost <= price AND special_price < price, 'Pricing',
		IF ( cost <= special_price AND cost > price AND special_price >= price, 'Pricing',
		IF ( cost > special_price AND cost <= price AND special_price >= price, 'Pricing',
		IF ( cost > special_price AND cost > price AND special_price < price, 'Pricing', 'Pricing'
		))))))))))))))))) AS Check_Final
	
	FROM pricing.ve_cmr_prc_tbl_catalog_price_desicion
	GROUP BY sku_simple
);

CREATE TABLE pricing.ve_cmr_prc_tbl_price_tool_tmp2 (INDEX(sku_config), INDEX(sku_simple))(

	SELECT 

		a.sku_config,
		a.sku_simple,
		IF (b.cost = -1 , ' ', b.cost) AS cost_order,
		IF (b.special_price = -1, ' ', b.special_price) AS special_price_order,
		IF (b.price = -1, ' ', b.price) AS price_order,
		IF (a.cost = 0, ' ', a.cost) AS cost_db,
		IF (a.special_price = 0, ' ', a.special_price) AS special_price_db,
		IF (a.price = 0, ' ', a.price) AS price_db,
		a.Action_result,
		a.Check_Final

	FROM pricing.ve_cmr_prc_tbl_price_tool_tmp1 a
	LEFT JOIN pricing.ve_cmr_prc_tbl_catalog_user_price b ON (a.sku_simple = b.sku_simple)
	GROUP BY a.sku_simple
);

TRUNCATE TABLE pricing.ve_cmr_prc_tbl_price_tool;

		INSERT INTO pricing.ve_cmr_prc_tbl_price_tool (

			sku_config,
			sku_simple,
			cost_order,
			special_price_order,
			price_order,
			cost_db,
			special_price_db,
			price_db,
			Action_result,
			Check_Final
)

				SELECT 
					
					sku_config,
					sku_simple,
					cost_order,
					special_price_order,
					price_order,
					cost_db,
					special_price_db,
					price_db,
					Action_result,
					Check_Final

				FROM pricing.ve_cmr_prc_tbl_price_tool_tmp2;


DROP TABLE IF EXISTS pricing.ve_cmr_prc_tbl_price_tool_tmp2;

DROP TABLE IF EXISTS pricing.ve_cmr_prc_tbl_price_tool_tmp1;

TRUNCATE TABLE pricing.ve_cmr_prc_tbl_catalog_user_price;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`gustavo.miranda`@`%`*/ /*!50003 PROCEDURE `ve_mcr_prc_sp_Purchase_Request`()
BEGIN
	
CREATE TABLE  pricing.Purchase_user_tm2_ve  (INDEX (sku_simple))(
	
		SELECT
			m2.`name` AS category,
			m3.`name` AS sub_category,
			m4.`name` AS sub_sub_category,
			m6.`name` AS supplier,
			m5.`name` AS brand,
			m1.`name`,
			m1.model,
			m1.sku AS sku_config,
			m7.sku AS sku_simple,
			m7.price,
			m7.special_price,
			m10.tax_percent / 100 AS tax_percent,  
			m7.cost AS cost_bob,
			m7.cost / ( 1 + m10.tax_percent / 100 ) AS cost_after_tax,
			IF ((m7.delivery_cost_supplier) IS NULL, 0, (m7.delivery_cost_supplier)) AS delivery_cost_supplier,
			0 AS fixed_price, #bob_live_mx.catalog_simple.fixed_price,
			0 AS sell_list,    #bob_live_mx.catalog_simple.sell_list
			IFNULL( m8.quantity,0) + IFNULL(m9.quantity,0) AS stock,
			IFNULL( m9.quantity,0 ) AS warehouse_stock,
			m7.is_option_marketplace AS Marketplace,
			0 AS sku_not_comparable
			
		FROM bob_live_ve.catalog_config m1
		INNER JOIN bob_live_ve.catalog_attribute_option_global_category m2 ON m1.fk_catalog_attribute_option_global_category = m2.id_catalog_attribute_option_global_category
		INNER JOIN bob_live_ve.catalog_attribute_option_global_sub_category  m3 ON m1.fk_catalog_attribute_option_global_sub_category = m3.id_catalog_attribute_option_global_sub_category
		INNER JOIN bob_live_ve.catalog_attribute_option_global_sub_sub_category m4 ON m1.fk_catalog_attribute_option_global_sub_sub_category = m4.id_catalog_attribute_option_global_sub_sub_category
		INNER JOIN bob_live_ve.catalog_brand m5 ON m1.fk_catalog_brand = m5.id_catalog_brand
		#INNER JOIN bob_live_ve.supplier m6 ON m1.fk_supplier = m6.id_supplier 
		INNER JOIN bob_live_ve.catalog_simple m7 ON m1.id_catalog_config = m7.fk_catalog_config
		LEFT JOIN bob_live_ve.catalog_supplier_stock m8 ON m7.id_catalog_simple = m8.fk_catalog_simple
		LEFT JOIN bob_live_ve.catalog_warehouse_stock m9 ON m7.id_catalog_simple = m9.fk_catalog_simple
		INNER JOIN bob_live_ve.catalog_tax_class m10 ON m7.fk_catalog_tax_class = m10.id_catalog_tax_class
		INNER JOIN bob_live_ve.catalog_source m11 ON m7.id_catalog_simple = m11.fk_catalog_simple
		INNER JOIN bob_live_ve.supplier m6 ON m11.fk_supplier = m6.id_supplier
		);


CREATE TABLE pricing.Purchase_user_tm1_two_ve (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			sku_simple,
			name,
			category,
			brand,
			model,
			barccode,
			quantity,
			cost_per_unit,
			total_cost,
			estimated_selling_price,
			estimated_time_weeks,
			product_bought_before,
			IF (product_bought_before='yes', sku_simple, sku_simple_similar_product_sold) AS sku_simple_similar_product_sold,
			best_competitor_price,
			name_competitor,
			Final_Quantity,
			Final_Total_Cost

	
		FROM pricing.ve_cmr_prc_tbl_Purchase_user
		GROUP BY sku_simple
);

CREATE TABLE pricing.Purchase_user_tm3_ve (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			b.category, 
			b.sub_category, 
			b.sub_sub_category,
			NULL AS supplier_id,
			NULL AS supplier_name,
			a.sku_simple, 
			a.name,
			a.brand,
			a.model, 
			a.barccode, 
			a.quantity, 
			a.cost_per_unit, 
			a.total_cost, 
			a.estimated_selling_price, 
			a.estimated_time_weeks, 
			a.product_bought_before, 			
			0 AS not_in_restock, 
			a.best_competitor_price, 
			a.name_competitor,
			a.Final_Quantity,
			a.Final_Total_Cost,
			0 AS product_attack, 
			0 AS in_stock, 
			0 AS price_point_last_30, 
			a.sku_simple_similar_product_sold, 
			0 AS items_sku_sold, 
			b.price,
			IF (best_competitor_price > 0, ((best_competitor_price/1.16)*0.0169), IF (estimated_selling_price > 0, ((estimated_selling_price/1.16)*0.0169),((price/1.16)*0.0169))) AS payment_fee,
			IF (best_competitor_price > 0, ((best_competitor_price/1.16)*0.04), IF (estimated_selling_price > 0, ((estimated_selling_price/1.16)*0.04),((price/1.16)*0.04))) AS voucher_cost,
			#0 AS payment_fee,
			#0 AS voucher_cost,
			b.delivery_cost_supplier,
			b.cost_bob,
			b.tax_percent,
			b.warehouse_stock,
			b.Marketplace

		FROM pricing.Purchase_user_tm1_two_ve a 
		LEFT JOIN pricing.Purchase_user_tm2_ve b ON a.sku_simple_similar_product_sold = b.sku_simple
		GROUP BY a.sku_simple
		);

CREATE TABLE pricing.Purchase_user_tm4_ve (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			a.category, 
			a.sub_category, 
			a.sub_sub_category,
			a.supplier_id,
			a.supplier_name,
			a.sku_simple, 
			a.name,
			a.brand,
			a.model, 
			a.barccode, 
			a.quantity, 
			a.cost_per_unit, 
			a.total_cost, 
			a.estimated_selling_price, 
			a.estimated_time_weeks, 
			a.product_bought_before, 			
			a.not_in_restock, 
			a.best_competitor_price, 
			a.name_competitor, 
			a.Final_Quantity,
			a.Final_Total_Cost,
			a.product_attack, 
			a.in_stock, 
			a.price_point_last_30, 
			a.sku_simple_similar_product_sold, 
			a.items_sku_sold, 
			a.price,
			a.payment_fee,
			a.voucher_cost,
			a.delivery_cost_supplier,
			a.cost_bob,
			a.tax_percent,
			a.warehouse_stock,
			a.Marketplace,
			b.shipping_fee,
			b.shipping_cost

		FROM pricing.Purchase_user_tm3_ve a 
		LEFT JOIN david_dana.shipping_fee_per_sku_ve b ON a.sku_simple_similar_product_sold = b.sku_simple
		GROUP BY a.sku_simple
		);

CREATE TABLE pricing.Purchase_user_tm5_ve (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			category, 
			sub_category, 
			sub_sub_category,
			supplier_id,
			supplier_name,
			sku_simple, 
			name,
			brand,
			model, 
			barccode, 
			quantity, 
			cost_per_unit, 
			total_cost, 
			estimated_selling_price, 
			estimated_time_weeks, 
			product_bought_before, 			
			not_in_restock, 
			best_competitor_price, 
			name_competitor, 
			Final_Quantity,
			Final_Total_Cost,
			product_attack, 
			in_stock, 
			price_point_last_30, 
			sku_simple_similar_product_sold, 
			items_sku_sold, 
			price,
			payment_fee,
			voucher_cost,
			delivery_cost_supplier,
			cost_bob,
			tax_percent,
			warehouse_stock,
			Marketplace,
			IF (shipping_fee IS NULL,0,shipping_fee) AS shipping_fee,
			IF (shipping_cost IS NULL,0, shipping_cost) AS shipping_cost,
			IF (best_competitor_price > 0, best_competitor_price, IF(estimated_selling_price > 0, estimated_selling_price, price)) AS price_f,
			IF (cost_per_unit > 0, cost_per_unit, cost_bob) AS cost_f

		FROM pricing.Purchase_user_tm4_ve 
		GROUP BY sku_simple
);

CREATE TABLE pricing.Purchase_user_tm6_ve (INDEX (sku_simple_similar_product_sold)) (

		SELECT

			category, 
			sub_category, 
			sub_sub_category,
			supplier_id,
			supplier_name,
			sku_simple, 
			name,
			brand,
			model, 
			barccode, 
			quantity, 
			cost_per_unit, 
			total_cost, 
			estimated_selling_price, 
			estimated_time_weeks, 
			product_bought_before, 			
			not_in_restock, 
			best_competitor_price, 
			name_competitor,
			Final_Quantity,
			Final_Total_Cost,
			product_attack, 
			in_stock, 
			price_point_last_30, 
			sku_simple_similar_product_sold, 
			items_sku_sold, 
			price,
			payment_fee,
			voucher_cost,
			delivery_cost_supplier,
			cost_bob,
			tax_percent,
			warehouse_stock,
			Marketplace,
			shipping_fee,
			shipping_cost,
			price_f,
			cost_f,
			((price_f/1.16) + (shipping_fee/1.16) - (delivery_cost_supplier/1.16) - (cost_f/1.16) - (shipping_cost/1.16) - payment_fee - voucher_cost) AS PC15
			

		FROM pricing.Purchase_user_tm5_ve
		GROUP BY sku_simple
);

CREATE TABLE pricing.Purchase_user_tm7_ve (INDEX (sku_simple))(

		SELECT
			'Linio' AS Competitor,
			m1.sku AS sku_config,
			m2.sku AS sku_simple,
			m7.`name` AS Brand,
			m1.`name`,
			m6.price_f,
			m3.`name` AS category,
			m4.`name` AS sub_category,
			m5.`name` AS sub_sub_category,
			m8.price_point_tier_1,
			m8.price_point_tier_2,
			m8.price_point_tier_3,
			m8.price_point_tier_4,
			m8.price_point_tier_5,
			m8.price_point_tier_6,
			m8.price_point_tier_7,

				CASE WHEN m6.price_f < m8.price_point_tier_1 THEN 1
				WHEN m6.price_f < m8.price_point_tier_2      THEN 2
				WHEN m6.price_f < m8.price_point_tier_3      THEN 3
				WHEN m6.price_f < m8.price_point_tier_4      THEN 4
				WHEN m6.price_f < m8.price_point_tier_5      THEN 5
				WHEN m6.price_f < m8.price_point_tier_6      THEN 6
				WHEN m6.price_f < m8.price_point_tier_7      THEN 7
				ELSE 8
				END AS price_point_tier

		FROM bob_live_ve.catalog_config m1
		INNER JOIN bob_live_ve.catalog_simple m2 ON m1.id_catalog_config = m2.fk_catalog_config
		INNER JOIN bob_live_ve.catalog_attribute_option_global_category m3 ON m1.fk_catalog_attribute_option_global_category = m3.id_catalog_attribute_option_global_category
		INNER JOIN bob_live_ve.catalog_attribute_option_global_sub_category m4 ON m1.fk_catalog_attribute_option_global_sub_category = m4.id_catalog_attribute_option_global_sub_category
		INNER JOIN bob_live_ve.catalog_attribute_option_global_sub_sub_category m5 ON m1.fk_catalog_attribute_option_global_sub_sub_category = m5.id_catalog_attribute_option_global_sub_sub_category
		INNER JOIN pricing.Purchase_user_tm5_ve m6 ON m2.sku = m6.sku_simple_similar_product_sold
		INNER JOIN bob_live_ve.catalog_brand m7 ON m1.fk_catalog_brand = m7.id_catalog_brand
		LEFT  JOIN david_dana.`price_points_per_category_key_ve` m8 ON m3.`name` = m8.category AND m4.`name` = m8.sub_category AND m5.`name` = m8.sub_sub_category

		GROUP BY m2.sku
);

DROP TABLE IF EXISTS pricing.warehouse_stock_v1_ve;
CREATE TABLE pricing.warehouse_stock_v1_ve AS (

				SELECT 
					sku_simple,
					sum(in_stock) AS stock_wms, 
					sum(reserved) AS stock_reserved_wms

				FROM operations_ve.out_stock_hist
				GROUP BY sku_simple
		);

DROP TABLE IF EXISTS pricing.warehouse_stock_v2_ve;
CREATE TABLE pricing.warehouse_stock_v2_ve AS (

				SELECT 
					sku_simple,
					(stock_wms - stock_reserved_wms) AS in_stock

				FROM pricing.warehouse_stock_v1_ve
				GROUP BY sku_simple
		);

CREATE TABLE pricing.Purchase_user_tm8_ve (INDEX (sku_simple_similar_product_sold))(

		SELECT

			a.category, 
			a.sub_category, 
			a.sub_sub_category,
			a.supplier_id,
			a.supplier_name,
			a.sku_simple, 
			a.name,
			a.brand,
			a.model, 
			a.barccode, 
			a.quantity, 
			a.cost_per_unit, 
			a.total_cost, 
			a.estimated_selling_price, 
			a.estimated_time_weeks, 
			a.product_bought_before, 			
			a.not_in_restock, 
			a.best_competitor_price, 
			a.name_competitor,
			a.Final_Quantity,
			a.Final_Total_Cost,
			a.product_attack,  
			a.price_point_last_30, 
			a.sku_simple_similar_product_sold, 
			a.items_sku_sold, 
			a.price,
			a.payment_fee,
			a.voucher_cost,
			a.delivery_cost_supplier,
			a.cost_bob,
			a.tax_percent,
			a.warehouse_stock,
			a.Marketplace,
			a.shipping_fee,
			a.shipping_cost,
			a.price_f,
			a.cost_f,
			a.PC15,
			b.price_point_tier
			
		FROM pricing.Purchase_user_tm6_ve a
		INNER JOIN pricing.Purchase_user_tm7_ve b ON a.sku_simple_similar_product_sold = b.sku_simple
		GROUP BY a.sku_simple
		
);

CREATE TABLE pricing.Purchase_user_tm9_ve (INDEX (sku_simple_similar_product_sold))(

		SELECT
			
			b.category, 
			b.sub_category, 
			b.sub_sub_category,
			b.supplier_id,
			b.supplier_name,
			b.sku_simple, 
			b.name,
			b.brand,
			b.model, 
			b.barccode, 
			b.quantity, 
			b.cost_per_unit, 
			b.total_cost, 
			b.estimated_selling_price, 
			b.estimated_time_weeks, 
			b.product_bought_before, 			
			b.not_in_restock, 
			b.best_competitor_price, 
			b.name_competitor,
			b.Final_Quantity,
			b.Final_Total_Cost,
			b.product_attack,  
			b.price_point_last_30, 
			b.sku_simple_similar_product_sold, 
			b.items_sku_sold, 
			b.price,
			b.payment_fee,
			b.voucher_cost,
			b.delivery_cost_supplier,
			b.cost_bob,
			b.tax_percent,
			b.warehouse_stock,
			b.Marketplace,
			b.shipping_fee,
			b.shipping_cost,
			b.price_f,
			b.cost_f,
			b.PC15,
			b.price_point_tier,
			Sum(c.num_sales_30) AS sales_last_30_days_price_point,
			sum(d.in_stock) AS in_stock

FROM pricing.Purchase_user_tm7_ve a
INNER JOIN pricing.Purchase_user_tm8_ve b ON b.category = a.category AND b.sub_category = a.sub_category AND b.sub_sub_category = a.sub_sub_category AND b.price_point_tier = a.price_point_tier
LEFT JOIN david_dana.sku_actual_numbers_ve c ON a.sku_simple = c.SKUSimple
LEFT JOIN pricing.warehouse_stock_v2_ve d ON a.sku_simple = d.sku_simple
GROUP BY  b.category, b.sub_category,b.sub_sub_category, b.price_point_tier
		
);

CREATE TABLE pricing.Purchase_Request_user_ve (INDEX (sku_simple_similar_product_sold))(

		SELECT
				
				
				#b.supplier_id,
				#b.supplier_name,
				b.sku_simple,
				b.`name`,
				b.category,
				b.brand,
				b.model,
				b.barccode,
				b.quantity,
				b.cost_per_unit, 
				b.total_cost,
				b.estimated_selling_price,
				b.estimated_time_weeks,
				b.product_bought_before,
				'' AS nada,
				b.sku_simple_similar_product_sold,
				b.best_competitor_price,
				IF (b.best_competitor_price = 0,'No Competitor',b.name_competitor) AS name_competitor,
				b.Final_Quantity,
				b.Final_Total_Cost,
				b.sub_category,
				b.sub_sub_category,
				b.PC15,
				b.price_point_tier,
				a.in_stock,
				a.sales_last_30_days_price_point AS price_point_last_30,
				b.items_sku_sold
				#b.not_in_restock

		FROM pricing.Purchase_user_tm9_ve a
		INNER JOIN pricing.Purchase_user_tm8_ve b ON a.category = b.category AND a.sub_category = b.sub_category AND a.sub_sub_category = b.sub_sub_category AND a.price_point_tier = b.price_point_tier
		
);

DROP TABLE IF EXISTS pricing.sales_SKUSimple_ve;

		CREATE TABLE pricing.sales_SKUSimple_ve (INDEX (sku_simple))(

			SELECT
				SKUSimple AS sku_simple,
				Count(*) AS NumItems,
				Sum(IF(CouponCode NOT IN ('REC%','MKT%','CAC%'),Rev,Rev+CouponValueAfterTax)) AS TotalRev,
				Avg(IF(CouponCode NOT IN ('REC%','MKT%','CAC%'),PCOnePFive,PCOnePFive+CouponValueAfterTax)) AS PCOnePFive,
				Avg (PaidPrice) AS Paid_Price
			
			FROM development_ve.A_Master
			WHERE DATEDIFF(Curdate(),Date) <= 30
				AND OrderAfterCan = 1
			GROUP BY SKUSimple
);

DROP TABLE IF EXISTS pricing.Days_in_stock_p_ve;

		CREATE TABLE pricing.Days_in_stock_p_ve (INDEX (sku_simple))(

			SELECT
				sku_simple,
				avg(days_in_stock) AS Avg_Days_in_WH

			FROM	operations_ve.out_stock_hist
			GROUP BY sku_simple
);

DROP TABLE IF EXISTS pricing.ve_cmr_prc_tbl_Purchase_Request_finaly;

CREATE TABLE pricing.ve_cmr_prc_tbl_Purchase_Request_finaly (INDEX (sku_simple_similar_product_sold))(

		SELECT
		
				b.sku_simple,
				b.`name`,
				b.category,
				b.brand,
				b.model,
				b.barccode,
				b.quantity,
				b.cost_per_unit,
				b.total_cost,
				b.estimated_selling_price,
				b.estimated_time_weeks,
				b.product_bought_before,
				'' AS nada,
				b.sku_simple_similar_product_sold,
				b.best_competitor_price, 
				b.name_competitor,
				b.sub_category,
				b.sub_sub_category,
				b.PC15,
				b.price_point_tier,
				IF (b.in_stock IS NULL,0,b.in_stock) AS in_stock,
				IF (b.price_point_last_30 IS NULL,0,b.price_point_last_30) AS price_point_last_30,
				'' AS nada2,
				#b.items_sku_sold,
				IF (c.NumItems IS NULL,0,c.NumItems) AS NumItems,
				IF (c.TotalRev IS NULL,0,c.TotalRev) AS TotalRev,
				IF (c.PCOnePFive IS NULL,0,c.PCOnePFive) AS PCOnePFive,
				IF (c.Paid_Price IS NULL,0,c.Paid_Price) AS Paid_Price,
				IF (d.Avg_Days_in_WH IS NULL,0,d.Avg_Days_in_WH) AS Avg_Days_in_WH,
				0 AS Real_stock,
				b.Final_Quantity,
				b.Final_Total_Cost

		FROM pricing.Purchase_Request_user_ve b
		LEFT JOIN pricing.sales_SKUSimple_ve c ON b.sku_simple_similar_product_sold=c.sku_simple
		LEFT JOIN pricing.Days_in_stock_p_ve d ON b.sku_simple_similar_product_sold=d.sku_simple
		GROUP BY b.sku_simple
);

# ver que hago con esta parte

/*UPDATE pricing.co_cmr_prc_tbl_Purchase_Request_finaly a
				INNER JOIN david_dana.mx_cmr_mp_tbl_real_stock b ON (a.sku_simple = b.sku_simple)
										SET a.Real_stock = b.Real_stock;*/


DROP TABLE IF EXISTS pricing.Purchase_user_tm1_two_ve;
DROP TABLE IF EXISTS pricing.Purchase_user_tm2_ve;
DROP TABLE IF EXISTS pricing.Purchase_user_tm3_ve;
DROP TABLE IF EXISTS pricing.Purchase_user_tm4_ve;
DROP TABLE IF EXISTS pricing.Purchase_user_tm5_ve;
DROP TABLE IF EXISTS pricing.Purchase_user_tm6_ve;
DROP TABLE IF EXISTS pricing.Purchase_user_tm7_ve;
DROP TABLE IF EXISTS pricing.Purchase_user_tm8_ve;
DROP TABLE IF EXISTS pricing.Purchase_user_tm9_ve;
DROP TABLE IF EXISTS pricing.Purchase_Request_user_ve;

TRUNCATE TABLE pricing.ve_cmr_prc_tbl_Purchase_user;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:04:11
