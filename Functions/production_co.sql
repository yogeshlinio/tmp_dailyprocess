-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: production_co
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`charles`@`%`*/ /*!50003 TRIGGER `voucher_costs_timestamp` BEFORE INSERT ON `tbl_voucher_costs` FOR EACH ROW SET NEW.created_at = NOW() */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping routines for database 'production_co'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 FUNCTION `week_exit`(edate date) RETURNS varchar(20) CHARSET latin1
begin
 declare sdate varchar(20);
 if year(edate) = 2012 then set sdate = week(edate, 5)+1; else set sdate = date_format(edate, "%v"); end if;
      return sdate;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 FUNCTION `week_iso`(edate date) RETURNS varchar(20) CHARSET latin1
begin
 declare sdate varchar(20);
 if year(edate) = 2012 then set sdate = concat(year(edate), '-',(week(edate, 5))+1); else set sdate = date_format(edate, "%x-%v"); end if;
      return sdate;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`ops_co`@`%`*/ /*!50003 PROCEDURE `calcworkdays`()
begin

DECLARE bDone, x INT;
DECLARE pasado INT;
DECLARE futuro INT;

DECLARE vsDate date;


DECLARE curs_dt	date;
DECLARE curs_isweekday	bit(1);
DECLARE curs_isholiday	bit(1);
DECLARE curs_isweekend	bit(1);
DECLARE curs_yr	smallint(6);
DECLARE curs_qtr	tinyint(4);
DECLARE curs_mt	tinyint(4);
DECLARE curs_dy	tinyint(4);
DECLARE curs_dyw	tinyint(4);
DECLARE curs_wk	tinyint(4);

#SELECT 180,60  INTO pasado, futuro ;
DECLARE curs CURSOR FOR SELECT * FROM calendar 
WHERE 
   dt BETWEEN dt - INTERVAL 180 DAY AND dt 
ORDER BY dt ASC;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET bDone = 1;

#drop table if exists calcworkdays;
create table if not exists  calcworkdays(
date_first date,
date_last date,
workdays int,
isweekday int,
isholiday int,
isweekend int,
primary key ( date_first ,date_last ),
index date (date_first, date_last),
index date_first  ( date_first  ),
index date_last   ( date_last  ),
index date_last_wd  ( date_last  , workdays),
index date_first_wd ( date_first , workdays)
);


SET pasado=180;
SET futuro = 180;

OPEN curs;
SET bDone = 0;
REPEAT
FETCH curs INTO curs_dt,
                       curs_isweekday,
                       curs_isholiday,
                       curs_isweekend,
                       curs_yr,
                       curs_qtr,
                       curs_mt,
                       curs_dy,
                       curs_dyw,
                       curs_wk	
                       ;

#    IF whatever_filtering_desired
       -- here for whatever_transformation_may_be_desired
   SET x =0;
   SET vsDate = curs_dt;
   WHILE x<=futuro DO
 
    REPLACE INTO calcworkdays ( date_first , date_last ,workdays )
    SELECT vsDate , curs_dt , sum( IF( dt != curs_dt , isweekday - if( isweekday = 1 , isholiday , 0 ), 0  ) ) FROM calendar 
    where dt between curs_dt and vsDate;
		set x =x+1; 
		set vsDate = curs_dt + interval x day;
	end while;

UNTIL bDone END REPEAT;
CLOSE curs;

UPDATE        calcworkdays 
   INNER JOIN calendar 
        ON calcworkdays.date_first = calendar.dt
SET
   calcworkdays.isweekday = calendar.isweekday,
   calcworkdays.isholiday = calendar.isholiday,
   calcworkdays.isweekend = calendar.isweekend
;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `catalog_creation`()
begin

delete from production_co.catalog_creation;

insert into production_co.catalog_creation
select z.cat1, a201204.number as '201204', a201205.number as '201205', a201206.number as '201206', a201207.number as '201207', a201208.number as '201208', a201209.number as '201209', a201210.number as '201210', a201211.number as '201211', a201212.number as '201212', a201301.number as '201301', a201302.number as '201302', a201303.number as '201303', a201304.number as '201304' from (select cat1 from production_co.tbl_catalog_product_v2 c where cat1 is not null group by cat1)z left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_co.tbl_catalog_product_v2 c, bob_live_co.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) = 201204 group by cat1)a201204 on z.cat1=a201204.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_co.tbl_catalog_product_v2 c, bob_live_co.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201205 group by cat1)a201205 on z.cat1=a201205.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_co.tbl_catalog_product_v2 c, bob_live_co.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201206 group by cat1)a201206 on z.cat1=a201206.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_co.tbl_catalog_product_v2 c, bob_live_co.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201207 group by cat1)a201207 on z.cat1=a201207.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_co.tbl_catalog_product_v2 c, bob_live_co.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201208 group by cat1)a201208 on z.cat1=a201208.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_co.tbl_catalog_product_v2 c, bob_live_co.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201209 group by cat1)a201209 on z.cat1=a201209.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_co.tbl_catalog_product_v2 c, bob_live_co.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201210 group by cat1)a201210 on z.cat1=a201210.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_co.tbl_catalog_product_v2 c, bob_live_co.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201211 group by cat1)a201211 on z.cat1=a201211.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_co.tbl_catalog_product_v2 c, bob_live_co.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201212 group by cat1)a201212 on z.cat1=a201212.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_co.tbl_catalog_product_v2 c, bob_live_co.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201301 group by cat1)a201301 on z.cat1=a201301.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_co.tbl_catalog_product_v2 c, bob_live_co.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201302 group by cat1)a201302 on z.cat1=a201302.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_co.tbl_catalog_product_v2 c, bob_live_co.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201303 group by cat1)a201303 on z.cat1=a201303.cat1 left join (select cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) as yrmonth, cat1, count(distinct c.sku) as number  from production_co.tbl_catalog_product_v2 c, bob_live_co.catalog_simple s where c.sku=s.sku and c.cat1 is not null and cast(concat(year(s.created_at),if(month(s.created_at)<10,concat(0,month(created_at)),month(s.created_at)))as decimal) <= 201304 group by cat1)a201304 on z.cat1=a201304.cat1 order by cat1;

update production_co.catalog_creation set `201204` = 0  where `201204` is null;

update production_co.catalog_creation set `201205` = 0  where `201205` is null;

update production_co.catalog_creation set `201206` = 0  where `201206` is null;

update production_co.catalog_creation set `201207` = 0  where `201207` is null;

update production_co.catalog_creation set `201208` = 0  where `201208` is null;

update production_co.catalog_creation set `201209` = 0  where `201209` is null;

update production_co.catalog_creation set `201210` = 0  where `201210` is null;

update production_co.catalog_creation set `201211` = 0  where `201211` is null;

update production_co.catalog_creation set `201212` = 0  where `201212` is null;

update production_co.catalog_creation set `201301` = 0  where `201301` is null;

update production_co.catalog_creation set `201302` = 0  where `201302` is null;

update production_co.catalog_creation set `201303` = 0  where `201303` is null;

update production_co.catalog_creation set `201304` = 0  where `201304` is null;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `catalog_history`()
begin

call production_co.catalog_visible;

insert into production_co.catalog_history(date, sku_config, sku_simple, product_name, status_config, status_simple, quantity, price) 
select curdate(), catalog_config.sku as sku_config, catalog_simple.sku as sku_simple, catalog_config.name, catalog_config.status, catalog_simple.status, catalog_stock.quantity, catalog_simple.price
from (bob_live_co.catalog_config inner join bob_live_co.catalog_simple on catalog_config.id_catalog_config = catalog_simple.fk_catalog_config) left join bob_live_co.catalog_stock on catalog_simple.id_catalog_simple = catalog_stock.fk_catalog_simple;

update production_co.catalog_history set yrmonth = concat(year(date), if(month(date)<10, concat('0',month(date)), month(date)));

update production_co.catalog_history set quantity = 0  where quantity is null and date = curdate();

update production_co.catalog_history c inner join production_co.catalog_visible v on c.sku_simple=v.sku_simple set visible = 1 where date = curdate();

update production_co.catalog_history set visible = 0  where visible is null and date = curdate();

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `catalog_visible`()
begin

delete from production_co.catalog_visible;

insert into production_co.catalog_visible(sku_config, sku_simple, pet_status, pet_approved, status_config, status_simple, name, display_if_out_of_stock, updated_at, activated_at, price) 
select catalog_config.sku as sku_config, catalog_simple.sku as sku_simple, catalog_config.pet_status, catalog_config.pet_approved, catalog_config.status as status_config, catalog_simple.status as status_simple, catalog_config.name, catalog_config.display_if_out_of_stock, catalog_config.updated_at, catalog_config.activated_at, catalog_simple.price
from (bob_live_co.catalog_config inner join bob_live_co.catalog_simple on catalog_config.id_catalog_config = catalog_simple.fk_catalog_config)
where (((catalog_config.pet_status)="creation,edited,images") and ((catalog_config.pet_approved)=1) and ((catalog_config.status)="active") and ((catalog_simple.status)="active") and ((catalog_config.display_if_out_of_stock)=0) and ((catalog_simple.price)>0)) or (((catalog_config.pet_status)="creation,edited,images") and ((catalog_config.pet_approved)=1) and ((catalog_config.status)="active") and ((catalog_simple.status)="active") and ((catalog_config.display_if_out_of_stock)=1) and ((catalog_simple.price)>0));

create table production_co.temporary_visible(
sku varchar(255),
catalog_warehouse_stock int,
catalog_supplier_stock int,
item_reserveed int,
stock_available int
);
create index sku on production_co.temporary_visible(sku);

insert into production_co.temporary_visible
SELECT `catalog_simple`.`sku`, IFNULL((SELECT CAST(quantity AS SIGNED INT) FROM bob_live_co.catalog_warehouse_stock WHERE catalog_warehouse_stock.fk_catalog_simple = catalog_simple.id_catalog_simple),0) AS `catalog_warehouse_stock`, IFNULL((SELECT CAST(quantity AS SIGNED INT) FROM bob_live_co.catalog_supplier_stock WHERE catalog_supplier_stock.fk_catalog_simple = catalog_simple.id_catalog_simple),0) AS `catalog_supplier_stock`, ( (SELECT COUNT(*) FROM bob_live_co.sales_order_item JOIN bob_live_co.sales_order ON fk_sales_order = id_sales_order WHERE fk_sales_order_process IN (33,5,27,26,31,8,23,28,32,34,29,9,30) AND is_reserved = 1 AND sales_order_item.sku = catalog_simple.sku) + IFNULL((SELECT SUM(catalog_stock_shared.reserved) FROM bob_live_co.catalog_stock_shared WHERE catalog_stock_shared.sku = catalog_simple.sku GROUP BY catalog_stock_shared.sku), 0)) AS `item_reserved`, GREATEST((IFNULL((SELECT CAST(quantity AS SIGNED INT) FROM bob_live_co.catalog_stock WHERE catalog_stock.fk_catalog_simple = catalog_simple.id_catalog_simple),0) - ( (SELECT COUNT(*) FROM bob_live_co.sales_order_item JOIN bob_live_co.sales_order ON fk_sales_order = id_sales_order WHERE fk_sales_order_process IN (33,5,27,26,31,8,23,28,32,34,29,9,30) AND is_reserved = 1 AND sales_order_item.sku = catalog_simple.sku) + IFNULL((SELECT SUM(catalog_stock_shared.reserved) FROM bob_live_co.catalog_stock_shared WHERE catalog_stock_shared.sku = catalog_simple.sku GROUP BY catalog_stock_shared.sku), 0))),0) AS `stock_available` FROM bob_live_co.`catalog_simple` ORDER BY `catalog_simple`.`id_catalog_simple` DESC;

update production_co.catalog_visible v inner join production_co.temporary_visible t on t.sku=v.sku_simple set v.quantity=t.stock_available;

delete from production_co.catalog_visible where quantity=0 or quantity is null;

drop table production_co.temporary_visible;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`lorena.ramirez`@`%`*/ /*!50003 PROCEDURE `channel_marketing`()
BEGIN

UPDATE SEM.campaign_ad_group_co SET channel='Referral Sites' WHERE source_medium like '%referral'
AND (source_medium not like 'google%' and source_medium not like 'linio%' and source_medium not like 'facebook%' OR source_medium<>'linio.com.co / referral' AND
 source_medium<>'facebook / retargeting');



UPDATE SEM.campaign_ad_group_co SET channel='Buscape' WHERE source_medium like '%buscape%';



UPDATE SEM.campaign_ad_group_co SET channel='Pampa Network' WHERE source_medium like 'pampa%';
UPDATE SEM.campaign_ad_group_co SET channel='Affiliate Program' WHERE source_medium like 'cbit%';

UPDATE SEM.campaign_ad_group_co SET channel='SEM Branded' WHERE source_medium='google / cpc' AND campaign like '%linio%';
UPDATE SEM.campaign_ad_group_co SET channel='SEM (unbranded)' WHERE source_medium='google / cpc' AND campaign not like '%linio%'
and (campaign not like 'er.%' AND campaign not like '[D%' AND campaign not like 'r.%');

UPDATE SEM.campaign_ad_group_co SET channel='Google Display Network' WHERE source_medium='google / cpc' AND 
(campaign like 'er.%' or campaign like '[D%' or campaign like 'r.%' or campaign like '%d.%');


UPDATE SEM.campaign_ad_group_co SET channel='Facebook R.' WHERE source_medium='facebook / retargeting';


UPDATE SEM.campaign_ad_group_co SET channel='Sociomantic' WHERE source_medium like 'socioma%';
UPDATE SEM.campaign_ad_group_co SET channel='Vizury' WHERE source_medium like '%vizury%';
UPDATE SEM.campaign_ad_group_co SET channel='VEInteractive' WHERE source_medium like '%VEInteractive%';
UPDATE SEM.campaign_ad_group_co SET channel='Triggit' WHERE source_medium like '%Triggit / Retargeting%';

UPDATE SEM.campaign_ad_group_co SET channel='Mercado Libre' WHERE source_medium='%mercado%referral%' or source_medium like 'ML%';
UPDATE SEM.campaign_ad_group_co SET channel='Alamaula' WHERE source_medium like 'alamaula%' or source_medium like 'ALA%';



UPDATE SEM.campaign_ad_group_co SET channel='Twitter' 
WHERE source_medium='t.co / referral' OR source_medium='socialmedia / smtweet' OR source_medium like 'Twitter%' OR source_medium like 'TW%';
UPDATE SEM.campaign_ad_group_co SET channel='Linio Fan Page' WHERE (source_medium='FASHION25' OR source_medium like '%facebook%' OR (source_medium like 'FB%' AND source_medium not like 'FBCAC%')
OR source_medium='faebook / socialmedia') AND (source_medium<>'facebook / socialmediaads' AND source_medium<>'facebook / retargeting'); 
UPDATE SEM.campaign_ad_group_co SET channel='FB CAC' 
WHERE source_medium like 'FBCAC%';


UPDATE SEM.campaign_ad_group_co SET channel='Fashion Fan Page' 
WHERE source_medium like 'FBF%';

UPDATE SEM.campaign_ad_group_co SET channel='Linio Fan Page' 
WHERE source_medium='facebook / socialmedia' AND (campaign not like '%fashion%' and campaign is not null);

UPDATE SEM.campaign_ad_group_co SET channel='Fashion Fan Page' 
WHERE source_medium='facebook / socialmedia' AND campaign like '%fashion%';


UPDATE SEM.campaign_ad_group_co SET channel='Fashion Fan Page' 
WHERE source_medium='facebook / socialmedia_fashion';



UPDATE SEM.campaign_ad_group_co SET channel='Facebook Ads' WHERE 
source_medium ='facebook / socialmediaads' and source_medium<>'facebook / retargeting';
UPDATE SEM.campaign_ad_group_co SET channel='Alejandra Arce Voucher' WHERE source_medium like 'voucher5%' OR source_medium like 'voucher2%';
UPDATE SEM.campaign_ad_group_co SET channel='FB Ads CAC Mayo' where source_medium like 'CACFA04%';
UPDATE SEM.campaign_ad_group_co SET channel='FB Ads CAC Junio' where source_medium like 'CACFA05%';

UPDATE SEM.campaign_ad_group_co SET channel='Facebook Retargeting' WHERE source_medium ='facebook / retargeting';

UPDATE SEM.campaign_ad_group_co SET channel='UpSell' WHERE  source_medium like 'UP%' OR source_medium like '%UP%' OR 
(source_medium='CS / call' AND campaign like 'ups%' );
UPDATE SEM.campaign_ad_group_co SET channel='Invalids' WHERE 
(source_medium='CS / call' AND campaign like 'INVALID%') or source_medium='CS / invalid' OR source_medium='OUT1mTG7';
UPDATE SEM.campaign_ad_group_co SET channel='Reactivation' WHERE (source_medium='CS / call' AND campaign like 'reactivation%') OR source_medium like 'ACTIVA%' 
OR source_medium='CS / Reactivation';
UPDATE SEM.campaign_ad_group_co SET channel='Inbound' WHERE (source_medium='CS / call' AND campaign like 'inbou%' ) OR source_medium='CS / Inbound' OR 
source_medium='out%' OR source_medium like 'Telesales%';
UPDATE SEM.campaign_ad_group_co SET channel='OutBound' WHERE source_medium='CS / OutBound' OR source_medium='CS / out' OR source_medium like 'CV%' OR (source_medium='CS / call' AND campaign like 'outbou%');
UPDATE SEM.campaign_ad_group_co SET channel='CrossSales' WHERE source_medium='CS / CrossSale' OR source_medium='CS / cross_sell';
UPDATE SEM.campaign_ad_group_co SET channel='Replace' WHERE source_medium like '%replace%';
UPDATE SEM.campaign_ad_group_co SET channel='ReOrder' WHERE source_medium='CS / reorder';
UPDATE SEM.campaign_ad_group_co SET channel='Invalid-Crossell' WHERE source_medium='CS / invalidcrossell';
UPDATE SEM.campaign_ad_group_co SET channel='CS-Insentivos' WHERE source_medium='CS / Insentivos' OR 
source_medium like 'CSIN%';

UPDATE SEM.campaign_ad_group_co SET channel='Search Organic' WHERE source_medium like '%organic' OR source_medium='google.com.co / referral';

#NEWSLETTER
UPDATE SEM.campaign_ad_group_co set channel='Newsletter' WHERE (source_medium='Postal / CRM' 
OR source_medium='Email Marketing / CRM' OR source_medium='EMM / CRM' 
OR source_medium='Postal / (not set)') or (source_medium like '%CRM%'and source_medium not like 'VEInteractive%') 
OR (source_medium like 'LOCO%' and (source_medium is not null and
 source_medium not like '%blog%')
);
update SEM.campaign_ad_group_co
set channel = 'NL CAC'
where source_medium like '%CRM'
and campaign like '%cac%';

UPDATE SEM.campaign_ad_group_co set Channel='Voucher Navidad' WHERE source_medium like 'NAVIDAD%' 
OR source_medium like 'LINIONAVIDAD%'; 
UPDATE SEM.campaign_ad_group_co set Channel='Voucher Fin de Año' where source_medium='FINDEAÑO31';
UPDATE SEM.campaign_ad_group_co set Channel='Bandeja CAC' where source_medium='BANDEJA0203';
UPDATE SEM.campaign_ad_group_co set Channel='Tapete CAC' where source_medium like 'TAPETE%';
UPDATE SEM.campaign_ad_group_co set Channel='Perfumes CAC' where source_medium like 'PERFUME%';
UPDATE SEM.campaign_ad_group_co set Channel='VoucherBebes-Feb' where source_medium like 'BEBES%';
UPDATE SEM.campaign_ad_group_co set channel='Sartenes CAC' where source_medium like 'SARTENES%';
UPDATE SEM.campaign_ad_group_co set channel='Estufa CAC' where source_medium like 'ESTUFA%';
UPDATE SEM.campaign_ad_group_co set channel='Vuelve CAC' where source_medium like 'VUELVE%';
UPDATE SEM.campaign_ad_group_co set channel='Memoria CAC'  where source_medium like 'MEMORIA%';
UPDATE SEM.campaign_ad_group_co set channel='Audifonos CAC'  where source_medium like 'AUDIFONO%';

UPDATE SEM.campaign_ad_group_co set channel='Reactivación Mayo' where source_medium='Regalo50' OR source_medium='Regalo40' 
OR source_medium='Regalo60' OR source_medium like 'GRITA%' or source_medium='regalo30' or source_medium LIKE 'aqui%';
UPDATE SEM.campaign_ad_group_co set channel='Fashion' where source_medium like 'MODA%' OR source_medium like 'ESTILO%';

UPDATE SEM.campaign_ad_group_co set channel='Venir Reactivation' where source_medium like 'VENIR%';
UPDATE SEM.campaign_ad_group_co set channel='Regresa Reactivation' where source_medium like 'REGRESA%' AND (source_medium<>'Regresas' 
AND source_medium<>'Regresas49' AND source_medium<>'Regresa49');
UPDATE SEM.campaign_ad_group_co set channel='Bienvenido CAC'  where source_medium like 'BIENVENIDO%';
UPDATE SEM.campaign_ad_group_co set channel='Amigo CAC'  where source_medium like 'AMIGO%';
UPDATE SEM.campaign_ad_group_co set channel='Tiramos la Casa por la ventana' where source_medium like 'CASA%' 
AND (channel='' OR channel is null);


-- UPDATE SEM.campaign_ad_group_co SET channel='Blog Linio' WHERE source_medium like 'blog%' ;
-- UPDATE SEM.campaign_ad_group_co SET channel='Blog Linio' WHERE order_nr='200133986' OR order_nr='200882196'
-- OR source_medium='BL%' OR source_medium like 'madres%';
-- UPDATE SEM.campaign_ad_group_co SET channel='Blog Mujer' WHERE source_medium='mujer.linio.com.co / referral';
-- UPDATE SEM.campaign_ad_group_co SET channel='Lector' WHERE source_medium like 'lector%';

UPDATE SEM.campaign_ad_group_co SET channel='Reactivation Letters' WHERE (source_medium like 'REACT%' OR source_medium like 'CLIENTVIP%' OR source_medium like 'LINIOVIP%' OR source_medium='VUELVE' OR source_medium like 'CLIENTEVIP%') AND source_medium<>'CLIENTEVIP1';
UPDATE SEM.campaign_ad_group_co SET channel='Customer Adquisition Letters' WHERE  source_medium like 'CONOCE%' OR source_medium='CLIENTEVIP1';
UPDATE SEM.campaign_ad_group_co SET channel='Internal Sells' WHERE source_medium like 'INTER%';
UPDATE SEM.campaign_ad_group_co SET Channel='Employee Vouchers' 
WHERE source_medium like 'OCT%' OR source_medium like 'SEP%' OR source_medium like 'EMP%' OR (source_medium like 'CS%' AND (channel is null OR channel=''));
UPDATE SEM.campaign_ad_group_co SET channel='Parcel Vouchers' WHERE  source_medium like 'DESCUENTO%' OR source_medium='LINIO0KO' OR source_medium='LINIOiju';
UPDATE SEM.campaign_ad_group_co SET channel='New Register' WHERE  source_medium like 'NL%' OR source_medium like 'NR%';
UPDATE SEM.campaign_ad_group_co SET channel='Disculpas' WHERE source_medium like 'H1yJvf' OR source_medium like 'H87h7C' OR source_medium='KI025EL78LLXLACOL-25324'
OR source_medium='KI025EL85GRQLACOL-4516';
UPDATE SEM.campaign_ad_group_co SET channel='Suppliers Email' WHERE source_medium='suppliers_mail / email';
UPDATE SEM.campaign_ad_group_co SET channel='Out Of Stock' WHERE source_medium like 'NST%' OR source_medium='N0Ctor';
UPDATE SEM.campaign_ad_group_co SET channel='Devoluciones' WHERE source_medium like 'DEV%';
UPDATE SEM.campaign_ad_group_co SET channel='Broken Vouchers' WHERE source_medium like 'HVE%';
UPDATE SEM.campaign_ad_group_co SET channel='Broken Discount' WHERE source_medium  like  'KER%' OR source_medium='D1UqWU';
UPDATE SEM.campaign_ad_group_co SET channel='Voucher Empleados' WHERE source_medium like 'EMPLEADO%';
UPDATE SEM.campaign_ad_group_co SET channel='Gift Cards' WHERE source_medium like 'GC%';
UPDATE SEM.campaign_ad_group_co SET channel='Friends and Family' WHERE source_medium like 'FF%';
UPDATE SEM.campaign_ad_group_co SET channel='Leads' WHERE source_medium like 'LC%';
UPDATE SEM.campaign_ad_group_co SET channel='Nueva Página' WHERE source_medium like 'NUEVA%' and (channel='' OR channel is null);
UPDATE SEM.campaign_ad_group_co SET channel='Promesa Linio' WHERE source_medium like 'PROMESA%';
UPDATE SEM.campaign_ad_group_co SET channel='Creating Happiness' WHERE source_medium like 'FELIZ%' OR
source_medium like 'MUFELIZ%';
UPDATE SEM.campaign_ad_group_co SET channel='NO IVA Abril' WHERE source_medium like 'PAG%' OR source_medium like
'CERO%';
UPDATE SEM.campaign_ad_group_co set channel='Invitados VIP Círculo de la moda' where source_medium like 'CIRCULOREG%';
UPDATE SEM.campaign_ad_group_co set channel='CS-REORDER' where source_medium like 'REO%';
UPDATE SEM.campaign_ad_group_co set channel='CS-CANCEL' where source_medium like 'CAN%';



UPDATE SEM.campaign_ad_group_co SET channel='Banco Helm' WHERE  source_medium like  'HELM%';
UPDATE SEM.campaign_ad_group_co SET channel='Banco de Bogotá' WHERE  source_medium like 'CLIENTEBOG%' OR source_medium like 'MOVISTAR%' 
OR source_medium like 'BOG%' OR source_medium like 'BDB%' OR source_medium='BB42' OR source_medium='BB46' OR source_medium='BBLAV' OR source_medium='BBNEV' OR
source_medium='PA953EL96NHFLACOL' OR
source_medium='PA953EL96NHFLACOL' OR
source_medium='BH859EL51WBWLACOL' OR
source_medium='CU054HL58AXPLACOL' OR
source_medium='SA860TB24WCXLACOL' OR
source_medium='HA997TB17DLQLACOL' OR
source_medium='SA860TB35WCMLACOL' OR
source_medium='SA860TB37WCKLACOL' OR
source_medium='BO944TB20JEPLACOL' OR
source_medium='MG614TB23HTWLACOL' OR
source_medium='BA186TB36HTJLACOL' OR
source_medium='PR506TB63BFCLACOL' OR
source_medium='MI392TB32VRBLACOL' OR
source_medium='MI392TB33VRALACOL' OR
source_medium='BO944TB79JCILACOL' OR
source_medium='BO944TB24JELLACOL' OR
source_medium='HA997TB23DLKLACOL';
UPDATE SEM.campaign_ad_group_co SET channel='Post BDB' WHERE source_medium like 'POSTBD%';

UPDATE SEM.campaign_ad_group_co SET channel='Unknown Partnership March' WHERE source_medium='LG082EL56ECLLACOL-20343' OR
source_medium='SO017EL14ZQRLACOL48' OR
source_medium='LG082EL97RQSLACOL48' OR
source_medium='NI088EL26CJLLACOL48' OR
source_medium='GE008EL90BEZLACOL48' OR
source_medium='BDE862EL18WGZLACOL48' OR
source_medium='LU939HL19XSMLACOL48' OR
source_medium='LU939HL11XSULACOL48' OR
source_medium='LU939HL03XTCLACOL48' OR
source_medium='AR842EL87LLOLACOL48' OR
source_medium='HO107EL04MOPLACOL48' OR
source_medium='TC124EL64NBRLACOL48' OR
source_medium='CO052EL34DGFLACOL48' OR
source_medium='MI392EL71GRCLACOL48' OR
source_medium='GU689FA67MQALACOL48' OR
source_medium='TO692FA65MQCLACOL48' OR
source_medium='GU689FA66MQBLACOL48' OR
source_medium='FR119EL73NBILACOL48' OR
source_medium='HA135EL15KYALACOL48' OR
source_medium='CU054EL66KZXLACOL48' OR
source_medium='IN122TB66NBPLACOL48';

UPDATE SEM.campaign_ad_group_co SET channel='Banco Davivienda' WHERE  source_medium like 'DAV%' OR source_medium='DV_AG' OR source_medium='DVPLANCHA46';
UPDATE SEM.campaign_ad_group_co SET channel='Banco BBVA' WHERE  (source_medium like 'BBVA%' AND source_medium<>'BBVADP') OR
source_medium='SA860TB32WCPLACOL' OR
source_medium='SA860TB43WCELACOL' OR
source_medium='BA186TB35HTKLACOL' OR
source_medium='PR506TB55BFKLACOL' OR
source_medium='PR506TB62BFDLACOL' OR
source_medium='PA953EL97NHELACOL' OR
source_medium='PO916EL07ETSLACOL' OR
source_medium='PO916EL04ETVLACOL' OR
source_medium='LU939HL23XSILACOL' OR
source_medium='LU939HL32XRZLACOL' OR
source_medium='HA997TB15DLSLACOL' OR
source_medium='SA860TB35WCMLACOLACOL' or (source_medium like '%BBVA%' and source_medium<>'FBBBVAS');

UPDATE SEM.campaign_ad_group_co SET channel='Campus Party' WHERE source_medium like 'CP%';
UPDATE SEM.campaign_ad_group_co SET channel='Publimetro' WHERE source_medium like 'PUBLIMETRO%';
UPDATE SEM.campaign_ad_group_co SET channel='El Espectador' WHERE source_medium='ESPECT0yx' OR source_medium like 'FIXED%';
UPDATE SEM.campaign_ad_group_co SET channel='Grupon' WHERE source_medium like 'GR%' AND 
(source_medium not like 'gracias%' AND source_medium not like 'GRI%' and source_medium is not null);
UPDATE SEM.campaign_ad_group_co SET channel='Dafiti' WHERE source_medium='AMIGODAFITI';
UPDATE SEM.campaign_ad_group_co SET channel='Sin IVA 16' WHERE source_medium='SINIVA16';
UPDATE SEM.campaign_ad_group_co SET channel='En Medio' WHERE source_medium='ENMEDIO';
UPDATE SEM.campaign_ad_group_co SET channel='Bancolombia' WHERE source_medium like 'AMEX%' OR
source_medium='ABNSbkpSmM' OR
source_medium='ATR08i1X6D' OR
source_medium='TABAMEX1DA2h' OR source_medium='ATR08i1X6D' OR
source_medium='DEV1sd0c' OR
source_medium='DEVfjFW6' OR
source_medium='DEVbCM2o' OR
source_medium='DEVN6TJN' OR
source_medium='DEVFvwqn' OR
source_medium='DEV0QUfY' OR
source_medium='DEVmhwdB' OR
source_medium='DEV0rCuq' OR
source_medium='DEVlLjUZ' OR
source_medium='DEVzKPuf' OR
source_medium='DEV6IJP6' OR
source_medium='DEV08L8A' OR
source_medium='DEV0Fvg' OR
source_medium='DEVcTttT' OR
source_medium='DEVywRNF' OR
source_medium='DEV0umCc' OR
source_medium='DEVFcXZN' OR
source_medium='AMEXTAB8BSoc';
UPDATE SEM.campaign_ad_group_co SET channel='Citi Bank' WHERE  source_medium like 'CITI%' OR source_medium like 'POSTCITI%';

UPDATE SEM.campaign_ad_group_co SET channel='VISA' WHERE source_medium like 'VISA%';

UPDATE SEM.campaign_ad_group_co SET channel='Linio.com Referral' WHERE source_medium='linio.com / referral' 
OR source_medium='info.linio.com.co / referral' OR source_medium='linio.com.co / referral' or source_medium='r.linio.com.co / referral';
UPDATE SEM.campaign_ad_group_co SET channel='Directo / Typed' WHERE  source_medium='(direct) / (none)';


UPDATE SEM.campaign_ad_group_co SET channel='Liniofashion.com.co Referral' WHERE source_medium='liniofashion.com.co / referral';




UPDATE SEM.campaign_ad_group_co SET channel='Referral Program' WHERE source_medium like 'REF%';

UPDATE SEM.campaign_ad_group_co set channel='Corporate Sales' WHERE source_medium like'VC%' OR source_medium like 'VCJ%' OR source_medium like 'VCH%'OR source_medium like 'CVE%' 
OR source_medium like 'VCA%' OR source_medium='BIENVENIDO10';

UPDATE SEM.campaign_ad_group_co SET channel='Journalist Vouchers' WHERE source_medium='CAMILOCARM50' OR
source_medium='JUANPABVAR50' OR
source_medium='LAURACHARR50' OR
source_medium='MONICAPARA50' OR
source_medium='DIEGONARVA50' OR
source_medium='LUCYBUENO50' OR
source_medium='DIANALEON50' OR
source_medium='SILVIAPARR50' OR
source_medium='LAURAAYALA50' OR
source_medium='LEONARDONI50' OR
source_medium='CYNTHIARUIZ50' OR
source_medium='JAVIERLEAESC50' OR
source_medium='LORENAPULEC50' OR
source_medium='MARIAISABE50' OR
source_medium='PAOLACALLE50' OR
source_medium='ISABELSALAZ50' OR
source_medium='VICTORSOLA50' OR
source_medium='FABIANRAM50' OR
source_medium='ANDRESROD50' OR
source_medium='HENRYGON50' OR
source_medium='JOHANMA50' OR
source_medium='ALFONSOAYA50' OR
source_medium='VICTORSOLANAV' OR source_medium='LUISFERNANDOBOTERO' OR
source_medium='PILARBOLIVAR' OR
source_medium='MARCELAESTRADA' OR
source_medium='LUXLANCHEROS';

UPDATE SEM.campaign_ad_group_co SET channel='ADN' WHERE source_medium like'ADN%';
UPDATE SEM.campaign_ad_group_co SET channel='Metro' WHERE source_medium like'METRO%';
UPDATE SEM.campaign_ad_group_co SET channel='TRANSMILENIO' WHERE source_medium like 'TRANSMILENIO%' OR
source_medium='Calle76' OR
source_medium='Calle100' OR
source_medium='Calle63' OR
source_medium='Calle45' OR
source_medium='Marly' or source_medium='HEROES' OR source_medium='ESCUELAMILITAR';
UPDATE SEM.campaign_ad_group_co SET channel='SMS SanValentin' WHERE source_medium like'SANVALENTIN%';
UPDATE SEM.campaign_ad_group_co SET channel='SuperDescuentos' WHERE source_medium like'SUPERDESCUENTOS%';
UPDATE SEM.campaign_ad_group_co SET channel='Folletos' WHERE source_medium='Regalo';
UPDATE SEM.campaign_ad_group_co SET channel='Mujer' WHERE source_medium like 'MUJER%' AND source_medium<>'mujer.linio.com.co / referral';
UPDATE SEM.campaign_ad_group_co SET channel='DiaEspecial' WHERE source_medium like 'DiaEspecial%';
UPDATE SEM.campaign_ad_group_co SET channel='Hora Loca SMS' WHERE source_medium like 'HORALOCA%';
UPDATE SEM.campaign_ad_group_co SET channel='Calle Vouchers' WHERE source_medium like 'call%';
UPDATE SEM.campaign_ad_group_co SET channel='SMS' WHERE source_medium like 'SMS%' AND source_medium<>'SMSNueva';
UPDATE SEM.campaign_ad_group_co SET channel='BBVADP' WHERE source_medium='BBVADP';
UPDATE SEM.campaign_ad_group_co SET channel='Other PR' WHERE source_medium='LG082EL97RQSLACOLDP' OR
source_medium='SO017EL14ZQRLACOLDP';
UPDATE SEM.campaign_ad_group_co SET channel='Valla Abril' WHERE source_medium='Regalo50';
UPDATE SEM.campaign_ad_group_co SET channel='SMS Nueva pagina NC' WHERE source_medium='RegaloSMS';
UPDATE SEM.campaign_ad_group_co SET channel='SMS Nueva pagina Costumers' WHERE source_medium='SMSNueva';
UPDATE SEM.campaign_ad_group_co SET channel='SMS Más temprano más barato' WHERE source_medium='BaratoSMS';

UPDATE SEM.campaign_ad_group_co set channel='Regresa (React.)'  where source_medium='Regresa' 
OR source_medium='Regresas49' OR source_medium='Regresa49';
UPDATE SEM.campaign_ad_group_co set channel='Te Esperamos (React.)' where source_medium like 'teesperamos%';
UPDATE SEM.campaign_ad_group_co set channel='Gracias (Repurchase)' where source_medium like 'gracias%';
UPDATE SEM.campaign_ad_group_co set channel='Falabella Customers' where source_medium='50Bienvenido' OR source_medium like '50bienvenido%';
UPDATE SEM.campaign_ad_group_co set channel='Cali' where source_medium='Fabian' OR
source_medium='Gloria' OR
source_medium='Miguel' OR source_medium like 'Cali%';
UPDATE SEM.campaign_ad_group_co set channel='CC Avenida Chile' where source_medium like 'PARATI%';
UPDATE SEM.campaign_ad_group_co set channel='Correo Bogota' where source_medium='100Regalo';
UPDATE SEM.campaign_ad_group_co set channel='Feria Libro' where source_medium like 'Feria%';
UPDATE SEM.campaign_ad_group_co set channel='Correo directo bogota' where source_medium='50Regalo' OR source_medium='100Regalo' OR 
source_medium='200Regalo';
UPDATE SEM.campaign_ad_group_co set channel='SMS Madres' where source_medium='MADRE30';
UPDATE SEM.campaign_ad_group_co set channel='CEROIVA' where source_medium like 'CEROIVA%';
UPDATE SEM.campaign_ad_group_co set channel='Circulo de la Moda' where source_medium like 'CIRCULOMODA%';
UPDATE SEM.campaign_ad_group_co set channel='SMS Mas Barato' where source_medium like 'MASBARATO%';
UPDATE SEM.campaign_ad_group_co set channel='Mailing 2' where source_medium='REGALO1'
OR source_medium='REGALO2'
OR source_medium='REGALO3'
OR source_medium='REGALO050'
OR source_medium='REGALO100'
OR source_medium='REGALO200';
UPDATE SEM.campaign_ad_group_co set channel='Reactivation60 dias' where source_medium ='Vuelve50';
UPDATE SEM.campaign_ad_group_co set channel='Reactivation120 dias' where source_medium='vuelve050';
UPDATE SEM.campaign_ad_group_co set channel='Reactivation180 dias' where source_medium='50Vuelve';
UPDATE SEM.campaign_ad_group_co set channel='Valla 2nd stage' where source_medium like 'valla%';
UPDATE SEM.campaign_ad_group_co set channel='Repurchase50' where source_medium='050Gracias';
UPDATE SEM.campaign_ad_group_co set channel='Repurchase20' where source_medium='020Gracias';
UPDATE SEM.campaign_ad_group_co set channel='Catalogo Junio' where source_medium='CATALOGO50';


UPDATE SEM.campaign_ad_group_co SET channel='Unknown' WHERE channel is null;

#Criteo
UPDATE SEM.campaign_ad_group_co set channel='Criteo' where source_medium like '%criteo%';

SELECT 'Start: CHANNEL GROUPING', now();

#FIX OLD PARTNERHIPS TO OTHERS

UPDATE SEM.campaign_ad_group_co SET channel_group='Other (identified)' where channel='Libro Bogota';

UPDATE SEM.campaign_ad_group_co SET channel_group='Facebook Ads' WHERE channel='Facebook Ads' OR channel='Alejandra Arce Voucher'
OR channel='FB Ads CAC Junio' OR channel='FB Ads CAC Mayo';


UPDATE SEM.campaign_ad_group_co SET channel_group='Search Engine Marketing' WHERE channel='SEM (unbranded)';


UPDATE SEM.campaign_ad_group_co SET channel_group='Social Media' WHERE channel='Facebook Referral' OR channel='Twitter'
OR channel='FB Posts' OR channel='FB-Diana Call' OR channel='Muy Feliz' or channel='FB CAC' OR 
channel='Fashion Fan Page' OR channel='Linio Fan Page';


UPDATE SEM.campaign_ad_group_co SET channel_group='Retargeting' WHERE channel='Sociomantic' OR
channel='Vizury' OR channel = 'VEInteractive' or channel = 'GDN Retargeting'
OR channel='Facebook Retargeting' or channel='Criteo' or channel='Triggit';


UPDATE SEM.campaign_ad_group_co 
SET channel_group='Display'
WHERE channel='Google Display Network'
or channel = 'ICKK'
or channel = 'Exo'
or channel = 'MSN'; 


UPDATE SEM.campaign_ad_group_co SET channel_group='NewsLetter' WHERE channel='Newsletter'
OR channel='Voucher Navidad' OR channel='Voucher Fin de Año' 
OR channel='Bandeja CAC' OR channel='Tapete CAC' OR channel='Perfumes CAC'
OR channel='VoucherBebes-Feb' OR channel='Sartenes CAC' OR channel='Estufa CAC' OR channel='Vuelve CAC' OR
channel='Memoria CAC' OR
channel='Audifonos CAC' OR channel='Venir Reactivation' OR 
channel='Regresa Reactivation' OR 
channel='Bienvenido CAC' OR channel='Amigo CAC' OR channel='Aqui Reactivation' OR
channel='Tiramos la Casa por la ventana' OR channel='Creating Happiness' OR channel='Grita Reactivation' OR
channel='CACs Marzo' OR channel='CACs Abril' OR channel='CACs Mayo' OR channel='CACs Junio' OR channel='CACs Julio'
OR channel='Reactivación Mayo' OR channel='Fashion' OR channel='Reactivación Junio';





UPDATE SEM.campaign_ad_group_co SET channel_group='Search Engine Optimization' WHERE channel='Search Organic';


UPDATE SEM.campaign_ad_group_co SET channel_group='Other (identified)' WHERE
channel='Banco Helm' OR channel='Banco de Bogotá' OR channel='Banco Davivienda'
OR channel='Banco Davivienda' OR channel='Banco BBVA'
OR channel='Campus Party' OR 
 channel='El Espectador' OR channel='Grupon'
OR channel='Dafiti' OR channel='Sin IVA 16' OR channel='En Medio' OR channel='Bancolombia' OR channel='AMEX'
OR channel='Citi Bank' OR channel='Lenddo' OR channel='Unknown Partnership March' OR 
channel='Post BDB' OR channel='VISA' OR channel='Coomservi' OR channel='Recuperación - Partnership';


UPDATE SEM.campaign_ad_group_co SET channel_group='Other (identified)' WHERE channel='Corporate Sales';


UPDATE SEM.campaign_ad_group_co SET channel_group='Offline Marketing' WHERE 
channel='ADN' OR channel='Journalist Vouchers' OR channeL='Metro' OR
channel='TRANSMILENIO' OR channel='SMS SanValentin' OR channel='SuperDescuentos' OR
channel='Folletos' OR channel='MUJER' OR channel='DiaEspecial' OR channel='Hora Loca SMS' OR 
channel='Calle Vouchers' OR channel='SMS' OR channel='BBVADP' OR channel='Other PR' OR channel='Valla Abril'
OR channel='SMS Nueva pagina NC' OR channel='SMS Nueva pagina Costumers' OR channel='SMS Más temprano más barato' OR 
channel='Barranquilla' OR channel='Regresa (React.)' OR channel='Te Esperamos (React.)' OR channel='Gracias (Repurchase)'
OR channel='Falabella Customers' OR channel='Cali' OR channel='CC Avenida Chile' OR channel='Correo Bogota' 
OR channel='Feria Libro' OR channel='Correo directo bogota' OR channel='CEROIVA' OR channel='SMS Madres' or
channel='Circulo de la Moda' OR channel='SMS Mas Barato' or channel='Mailing 2' or channel='Publimetro' or
channel='Reactivation60 dias' or
channel='Reactivation120 dias' or
channel='Reactivation180 dias' or channel='Valla 2nd stage' or channel='Repurchase50' or
channel='Repurchase20' OR channel='Catalogo Junio';
 

UPDATE SEM.campaign_ad_group_co SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales' OR channel='Replace' OR channel='ReOrder' 
OR channel='Invalid-Crossell' OR channel='Unknown-CS' OR 
channel='CS-Insentivos' OR channel='CS-REORDER' OR channel='CS-CANCEL';


UPDATE SEM.campaign_ad_group_co SET channel_group='Branded' WHERE channel='Linio.com Referral' 
OR channel='Directo / Typed' OR
channel='SEM Branded' OR channel='Liniofashion.com.co Referral';


UPDATE SEM.campaign_ad_group_co SET channel_group='Mercado Libre - Alamaula' 
WHERE channel='Mercado Libre' OR channel='Otros - Mercado Libre' or channel='Alamaula';


-- UPDATE SEM.campaign_ad_group_co SET channel_group='Blogs' WHERE 
-- channel='Blog Linio' OR channel='Blog Mujer' OR channel='Lector';


UPDATE SEM.campaign_ad_group_co SET channel_group='Affiliates' WHERE channel='Buscape' or 
channel='Affiliate Program';



UPDATE SEM.campaign_ad_group_co SET channel_group='Other (identified)' WHERE channel='Referral Sites' 
OR channel='Reactivation Letters' OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' 
OR channel='Referral Program' OR channel='Nueva Página' OR channel='Promesa Linio' OR channel='NO IVA Abril' OR
channel='Invitados VIP Círculo de la moda';

update SEM.campaign_ad_group_co 
set 
    channel = 'Exo'
where
    source_medium like 'exo%';

update SEM.campaign_ad_group_co 
set 
    channel = 'MSN'
where
    source_medium like 'msn%display%';

update SEM.campaign_ad_group_co 
set 
    channel = 'ICCK'
where
   source_medium not like 'exo%' and source_medium not like 'msn%'
        and (source_medium like '%display' or source_medium like 'ICCK%'
				or source_medium like '%Blu%radio%' or source_medium like '%El%Espectador%'
				or source_medium like '%Shock%'
				or source in (1549193,
								1546958,
								1546959,
								1546960,
								1546956,
								1548963,
								1546961,
								1546861
								));
        
UPDATE SEM.campaign_ad_group_co
SET channel_group = 'Display'
WHERE channel in ('ICCK', 'Exo', 'MSN');

UPDATE SEM.campaign_ad_group_co SET 
channel_group='Non identified' WHERE channel='Unknown' OR channel='Unknown-Voucher' OR channel='Unknown-GA Tracking';


UPDATE SEM.campaign_ad_group_co SET channel_group='Affiliates' where channel='Pampa Network';




END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `channel_marketing_fashion`()
BEGIN

UPDATE SEM.campaign_ad_group_fashion_co SET channel='Referral Sites' WHERE source_medium like '%referral'
AND (source_medium not like 'google%' and source_medium not like 'linio%' and source_medium not like 'facebook%' OR source_medium<>'linio.com.co / referral' AND
 source_medium<>'facebook / retargeting');



UPDATE SEM.campaign_ad_group_fashion_co SET channel='Buscape' WHERE source_medium like '%buscape%';



UPDATE SEM.campaign_ad_group_fashion_co SET channel='Pampa Network' WHERE source_medium like 'pampa%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Affiliate Program' WHERE source_medium like 'cbit%';

UPDATE SEM.campaign_ad_group_fashion_co SET channel='SEM Branded' WHERE source_medium='google / cpc' AND campaign like '%linio%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='SEM (unbranded)' WHERE source_medium='google / cpc' AND campaign not like '%linio%'
and (campaign not like 'er.%' AND campaign not like '[D%' AND campaign not like 'r.%');

UPDATE SEM.campaign_ad_group_fashion_co SET channel='Google Display Network' WHERE source_medium='google / cpc' AND 
(campaign like 'er.%' or campaign like '[D%' or campaign like 'r.%');


UPDATE SEM.campaign_ad_group_fashion_co SET channel='Facebook R.' WHERE source_medium='facebook / retargeting';


UPDATE SEM.campaign_ad_group_fashion_co SET channel='Sociomantic' WHERE source_medium like 'socioma%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Vizury' WHERE source_medium like '%vizury%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='VEInteractive' WHERE source_medium like '%VEInteractive%';

UPDATE SEM.campaign_ad_group_fashion_co SET channel='Mercado Libre' WHERE source_medium='%mercado%referral%' or source_medium like 'ML%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Alamaula' WHERE source_medium like 'alamaula%' or source_medium like 'ALA%';



UPDATE SEM.campaign_ad_group_fashion_co SET channel='Twitter' 
WHERE source_medium='t.co / referral' OR source_medium='socialmedia / smtweet' OR source_medium like 'Twitter%' OR source_medium like 'TW%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Linio Fan Page' WHERE (source_medium='FASHION25' OR source_medium like '%facebook%' OR (source_medium like 'FB%' AND source_medium not like 'FBCAC%')
OR source_medium='faebook / socialmedia') AND (source_medium<>'facebook / socialmediaads' AND source_medium<>'facebook / retargeting'); 
UPDATE SEM.campaign_ad_group_fashion_co SET channel='FB CAC' 
WHERE source_medium like 'FBCAC%';


UPDATE SEM.campaign_ad_group_fashion_co SET channel='Fashion Fan Page' 
WHERE source_medium like 'FBF%';

UPDATE SEM.campaign_ad_group_fashion_co SET channel='Linio Fan Page' 
WHERE source_medium='facebook / socialmedia' AND (campaign not like '%fashion%' and campaign is not null);

UPDATE SEM.campaign_ad_group_fashion_co SET channel='Fashion Fan Page' 
WHERE source_medium='facebook / socialmedia' AND campaign like '%fashion%';


UPDATE SEM.campaign_ad_group_fashion_co SET channel='Fashion Fan Page' 
WHERE source_medium='facebook / socialmedia_fashion';



UPDATE SEM.campaign_ad_group_fashion_co SET channel='Facebook Ads' WHERE 
source_medium ='facebook / socialmediaads' and source_medium<>'facebook / retargeting';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Alejandra Arce Voucher' WHERE source_medium like 'voucher5%' OR source_medium like 'voucher2%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='FB Ads CAC Mayo' where source_medium like 'CACFA04%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='FB Ads CAC Junio' where source_medium like 'CACFA05%';

UPDATE SEM.campaign_ad_group_fashion_co SET channel='Facebook Retargeting' WHERE source_medium ='facebook / retargeting';

UPDATE SEM.campaign_ad_group_fashion_co SET channel='UpSell' WHERE  source_medium like 'UP%' OR source_medium like '%UP%' OR 
(source_medium='CS / call' AND campaign like 'ups%' );
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Invalids' WHERE 
(source_medium='CS / call' AND campaign like 'INVALID%') or source_medium='CS / invalid' OR source_medium='OUT1mTG7';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Reactivation' WHERE (source_medium='CS / call' AND campaign like 'reactivation%') OR source_medium like 'ACTIVA%' 
OR source_medium='CS / Reactivation';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Inbound' WHERE (source_medium='CS / call' AND campaign like 'inbou%' ) OR source_medium='CS / Inbound' OR 
source_medium='out%' OR source_medium like 'Telesales%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='OutBound' WHERE source_medium='CS / OutBound' OR source_medium='CS / out' OR source_medium like 'CV%' OR (source_medium='CS / call' AND campaign like 'outbou%');
UPDATE SEM.campaign_ad_group_fashion_co SET channel='CrossSales' WHERE source_medium='CS / CrossSale' OR source_medium='CS / cross_sell';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Replace' WHERE source_medium like '%replace%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='ReOrder' WHERE source_medium='CS / reorder';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Invalid-Crossell' WHERE source_medium='CS / invalidcrossell';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='CS-Insentivos' WHERE source_medium='CS / Insentivos' OR 
source_medium like 'CSIN%';

UPDATE SEM.campaign_ad_group_fashion_co SET channel='Search Organic' WHERE source_medium like '%organic' OR source_medium='google.com.co / referral';

#NEWSLETTER
UPDATE SEM.campaign_ad_group_fashion_co set channel='Newsletter' WHERE (source_medium='Postal / CRM' 
OR source_medium='Email Marketing / CRM' OR source_medium='EMM / CRM' 
OR source_medium='Postal / (not set)') or (source_medium like '%CRM%'and source_medium not like 'VEInteractive%') 
OR (source_medium like 'LOCO%' and (source_medium is not null and
 source_medium not like '%blog%')
);
UPDATE SEM.campaign_ad_group_fashion_co set Channel='Voucher Navidad' WHERE source_medium like 'NAVIDAD%' 
OR source_medium like 'LINIONAVIDAD%'; 
UPDATE SEM.campaign_ad_group_fashion_co set Channel='Voucher Fin de Año' where source_medium='FINDEAÑO31';
UPDATE SEM.campaign_ad_group_fashion_co set Channel='Bandeja CAC' where source_medium='BANDEJA0203';
UPDATE SEM.campaign_ad_group_fashion_co set Channel='Tapete CAC' where source_medium like 'TAPETE%';
UPDATE SEM.campaign_ad_group_fashion_co set Channel='Perfumes CAC' where source_medium like 'PERFUME%';
UPDATE SEM.campaign_ad_group_fashion_co set Channel='VoucherBebes-Feb' where source_medium like 'BEBES%';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Sartenes CAC' where source_medium like 'SARTENES%';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Estufa CAC' where source_medium like 'ESTUFA%';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Vuelve CAC' where source_medium like 'VUELVE%';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Memoria CAC'  where source_medium like 'MEMORIA%';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Audifonos CAC'  where source_medium like 'AUDIFONO%';

UPDATE SEM.campaign_ad_group_fashion_co set channel='Reactivación Mayo' where source_medium='Regalo50' OR source_medium='Regalo40' 
OR source_medium='Regalo60' OR source_medium like 'GRITA%' or source_medium='regalo30' or source_medium LIKE 'aqui%';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Fashion' where source_medium like 'MODA%' OR source_medium like 'ESTILO%';

UPDATE SEM.campaign_ad_group_fashion_co set channel='Venir Reactivation' where source_medium like 'VENIR%';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Regresa Reactivation' where source_medium like 'REGRESA%' AND (source_medium<>'Regresas' 
AND source_medium<>'Regresas49' AND source_medium<>'Regresa49');
UPDATE SEM.campaign_ad_group_fashion_co set channel='Bienvenido CAC'  where source_medium like 'BIENVENIDO%';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Amigo CAC'  where source_medium like 'AMIGO%';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Tiramos la Casa por la ventana' where source_medium like 'CASA%' 
AND (channel='' OR channel is null);


-- UPDATE SEM.campaign_ad_group_fashion_co SET channel='Blog Linio' WHERE source_medium like 'blog%' ;
-- UPDATE SEM.campaign_ad_group_fashion_co SET channel='Blog Linio' WHERE order_nr='200133986' OR order_nr='200882196'
-- OR source_medium='BL%' OR source_medium like 'madres%';
-- UPDATE SEM.campaign_ad_group_fashion_co SET channel='Blog Mujer' WHERE source_medium='mujer.linio.com.co / referral';
-- UPDATE SEM.campaign_ad_group_fashion_co SET channel='Lector' WHERE source_medium like 'lector%';

UPDATE SEM.campaign_ad_group_fashion_co SET channel='Reactivation Letters' WHERE (source_medium like 'REACT%' OR source_medium like 'CLIENTVIP%' OR source_medium like 'LINIOVIP%' OR source_medium='VUELVE' OR source_medium like 'CLIENTEVIP%') AND source_medium<>'CLIENTEVIP1';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Customer Adquisition Letters' WHERE  source_medium like 'CONOCE%' OR source_medium='CLIENTEVIP1';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Internal Sells' WHERE source_medium like 'INTER%';
UPDATE SEM.campaign_ad_group_fashion_co SET Channel='Employee Vouchers' 
WHERE source_medium like 'OCT%' OR source_medium like 'SEP%' OR source_medium like 'EMP%' OR (source_medium like 'CS%' AND (channel is null OR channel=''));
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Parcel Vouchers' WHERE  source_medium like 'DESCUENTO%' OR source_medium='LINIO0KO' OR source_medium='LINIOiju';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='New Register' WHERE  source_medium like 'NL%' OR source_medium like 'NR%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Disculpas' WHERE source_medium like 'H1yJvf' OR source_medium like 'H87h7C' OR source_medium='KI025EL78LLXLACOL-25324'
OR source_medium='KI025EL85GRQLACOL-4516';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Suppliers Email' WHERE source_medium='suppliers_mail / email';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Out Of Stock' WHERE source_medium like 'NST%' OR source_medium='N0Ctor';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Devoluciones' WHERE source_medium like 'DEV%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Broken Vouchers' WHERE source_medium like 'HVE%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Broken Discount' WHERE source_medium  like  'KER%' OR source_medium='D1UqWU';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Voucher Empleados' WHERE source_medium like 'EMPLEADO%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Gift Cards' WHERE source_medium like 'GC%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Friends and Family' WHERE source_medium like 'FF%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Leads' WHERE source_medium like 'LC%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Nueva Página' WHERE source_medium like 'NUEVA%' and (channel='' OR channel is null);
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Promesa Linio' WHERE source_medium like 'PROMESA%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Creating Happiness' WHERE source_medium like 'FELIZ%' OR
source_medium like 'MUFELIZ%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='NO IVA Abril' WHERE source_medium like 'PAG%' OR source_medium like
'CERO%';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Invitados VIP Círculo de la moda' where source_medium like 'CIRCULOREG%';
UPDATE SEM.campaign_ad_group_fashion_co set channel='CS-REORDER' where source_medium like 'REO%';
UPDATE SEM.campaign_ad_group_fashion_co set channel='CS-CANCEL' where source_medium like 'CAN%';



UPDATE SEM.campaign_ad_group_fashion_co SET channel='Banco Helm' WHERE  source_medium like  'HELM%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Banco de Bogotá' WHERE  source_medium like 'CLIENTEBOG%' OR source_medium like 'MOVISTAR%' 
OR source_medium like 'BOG%' OR source_medium like 'BDB%' OR source_medium='BB42' OR source_medium='BB46' OR source_medium='BBLAV' OR source_medium='BBNEV' OR
source_medium='PA953EL96NHFLACOL' OR
source_medium='PA953EL96NHFLACOL' OR
source_medium='BH859EL51WBWLACOL' OR
source_medium='CU054HL58AXPLACOL' OR
source_medium='SA860TB24WCXLACOL' OR
source_medium='HA997TB17DLQLACOL' OR
source_medium='SA860TB35WCMLACOL' OR
source_medium='SA860TB37WCKLACOL' OR
source_medium='BO944TB20JEPLACOL' OR
source_medium='MG614TB23HTWLACOL' OR
source_medium='BA186TB36HTJLACOL' OR
source_medium='PR506TB63BFCLACOL' OR
source_medium='MI392TB32VRBLACOL' OR
source_medium='MI392TB33VRALACOL' OR
source_medium='BO944TB79JCILACOL' OR
source_medium='BO944TB24JELLACOL' OR
source_medium='HA997TB23DLKLACOL';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Post BDB' WHERE source_medium like 'POSTBD%';

UPDATE SEM.campaign_ad_group_fashion_co SET channel='Unknown Partnership March' WHERE source_medium='LG082EL56ECLLACOL-20343' OR
source_medium='SO017EL14ZQRLACOL48' OR
source_medium='LG082EL97RQSLACOL48' OR
source_medium='NI088EL26CJLLACOL48' OR
source_medium='GE008EL90BEZLACOL48' OR
source_medium='BDE862EL18WGZLACOL48' OR
source_medium='LU939HL19XSMLACOL48' OR
source_medium='LU939HL11XSULACOL48' OR
source_medium='LU939HL03XTCLACOL48' OR
source_medium='AR842EL87LLOLACOL48' OR
source_medium='HO107EL04MOPLACOL48' OR
source_medium='TC124EL64NBRLACOL48' OR
source_medium='CO052EL34DGFLACOL48' OR
source_medium='MI392EL71GRCLACOL48' OR
source_medium='GU689FA67MQALACOL48' OR
source_medium='TO692FA65MQCLACOL48' OR
source_medium='GU689FA66MQBLACOL48' OR
source_medium='FR119EL73NBILACOL48' OR
source_medium='HA135EL15KYALACOL48' OR
source_medium='CU054EL66KZXLACOL48' OR
source_medium='IN122TB66NBPLACOL48';

UPDATE SEM.campaign_ad_group_fashion_co SET channel='Banco Davivienda' WHERE  source_medium like 'DAV%' OR source_medium='DV_AG' OR source_medium='DVPLANCHA46';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Banco BBVA' WHERE  (source_medium like 'BBVA%' AND source_medium<>'BBVADP') OR
source_medium='SA860TB32WCPLACOL' OR
source_medium='SA860TB43WCELACOL' OR
source_medium='BA186TB35HTKLACOL' OR
source_medium='PR506TB55BFKLACOL' OR
source_medium='PR506TB62BFDLACOL' OR
source_medium='PA953EL97NHELACOL' OR
source_medium='PO916EL07ETSLACOL' OR
source_medium='PO916EL04ETVLACOL' OR
source_medium='LU939HL23XSILACOL' OR
source_medium='LU939HL32XRZLACOL' OR
source_medium='HA997TB15DLSLACOL' OR
source_medium='SA860TB35WCMLACOLACOL' or (source_medium like '%BBVA%' and source_medium<>'FBBBVAS');

UPDATE SEM.campaign_ad_group_fashion_co SET channel='Campus Party' WHERE source_medium like 'CP%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Publimetro' WHERE source_medium like 'PUBLIMETRO%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='El Espectador' WHERE source_medium='ESPECT0yx' OR source_medium like 'FIXED%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Grupon' WHERE source_medium like 'GR%' AND 
(source_medium not like 'gracias%' AND source_medium not like 'GRI%' and source_medium is not null);
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Dafiti' WHERE source_medium='AMIGODAFITI';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Sin IVA 16' WHERE source_medium='SINIVA16';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='En Medio' WHERE source_medium='ENMEDIO';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Bancolombia' WHERE source_medium like 'AMEX%' OR
source_medium='ABNSbkpSmM' OR
source_medium='ATR08i1X6D' OR
source_medium='TABAMEX1DA2h' OR source_medium='ATR08i1X6D' OR
source_medium='DEV1sd0c' OR
source_medium='DEVfjFW6' OR
source_medium='DEVbCM2o' OR
source_medium='DEVN6TJN' OR
source_medium='DEVFvwqn' OR
source_medium='DEV0QUfY' OR
source_medium='DEVmhwdB' OR
source_medium='DEV0rCuq' OR
source_medium='DEVlLjUZ' OR
source_medium='DEVzKPuf' OR
source_medium='DEV6IJP6' OR
source_medium='DEV08L8A' OR
source_medium='DEV0Fvg' OR
source_medium='DEVcTttT' OR
source_medium='DEVywRNF' OR
source_medium='DEV0umCc' OR
source_medium='DEVFcXZN' OR
source_medium='AMEXTAB8BSoc';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Citi Bank' WHERE  source_medium like 'CITI%' OR source_medium like 'POSTCITI%';

UPDATE SEM.campaign_ad_group_fashion_co SET channel='VISA' WHERE source_medium like 'VISA%';

UPDATE SEM.campaign_ad_group_fashion_co SET channel='Linio.com Referral' WHERE source_medium='linio.com / referral' 
OR source_medium='info.linio.com.co / referral' OR source_medium='linio.com.co / referral' or source_medium='r.linio.com.co / referral';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Directo / Typed' WHERE  source_medium='(direct) / (none)';


UPDATE SEM.campaign_ad_group_fashion_co SET channel='Liniofashion.com.co Referral' WHERE source_medium='liniofashion.com.co / referral';




UPDATE SEM.campaign_ad_group_fashion_co SET channel='Referral Program' WHERE source_medium like 'REF%';

UPDATE SEM.campaign_ad_group_fashion_co set channel='Corporate Sales' WHERE source_medium like'VC%' OR source_medium like 'VCJ%' OR source_medium like 'VCH%'OR source_medium like 'CVE%' 
OR source_medium like 'VCA%' OR source_medium='BIENVENIDO10';

UPDATE SEM.campaign_ad_group_fashion_co SET channel='Journalist Vouchers' WHERE source_medium='CAMILOCARM50' OR
source_medium='JUANPABVAR50' OR
source_medium='LAURACHARR50' OR
source_medium='MONICAPARA50' OR
source_medium='DIEGONARVA50' OR
source_medium='LUCYBUENO50' OR
source_medium='DIANALEON50' OR
source_medium='SILVIAPARR50' OR
source_medium='LAURAAYALA50' OR
source_medium='LEONARDONI50' OR
source_medium='CYNTHIARUIZ50' OR
source_medium='JAVIERLEAESC50' OR
source_medium='LORENAPULEC50' OR
source_medium='MARIAISABE50' OR
source_medium='PAOLACALLE50' OR
source_medium='ISABELSALAZ50' OR
source_medium='VICTORSOLA50' OR
source_medium='FABIANRAM50' OR
source_medium='ANDRESROD50' OR
source_medium='HENRYGON50' OR
source_medium='JOHANMA50' OR
source_medium='ALFONSOAYA50' OR
source_medium='VICTORSOLANAV' OR source_medium='LUISFERNANDOBOTERO' OR
source_medium='PILARBOLIVAR' OR
source_medium='MARCELAESTRADA' OR
source_medium='LUXLANCHEROS';

UPDATE SEM.campaign_ad_group_fashion_co SET channel='ADN' WHERE source_medium like'ADN%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Metro' WHERE source_medium like'METRO%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='TRANSMILENIO' WHERE source_medium like 'TRANSMILENIO%' OR
source_medium='Calle76' OR
source_medium='Calle100' OR
source_medium='Calle63' OR
source_medium='Calle45' OR
source_medium='Marly' or source_medium='HEROES' OR source_medium='ESCUELAMILITAR';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='SMS SanValentin' WHERE source_medium like'SANVALENTIN%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='SuperDescuentos' WHERE source_medium like'SUPERDESCUENTOS%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Folletos' WHERE source_medium='Regalo';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Mujer' WHERE source_medium like 'MUJER%' AND source_medium<>'mujer.linio.com.co / referral';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='DiaEspecial' WHERE source_medium like 'DiaEspecial%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Hora Loca SMS' WHERE source_medium like 'HORALOCA%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Calle Vouchers' WHERE source_medium like 'call%';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='SMS' WHERE source_medium like 'SMS%' AND source_medium<>'SMSNueva';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='BBVADP' WHERE source_medium='BBVADP';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Other PR' WHERE source_medium='LG082EL97RQSLACOLDP' OR
source_medium='SO017EL14ZQRLACOLDP';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='Valla Abril' WHERE source_medium='Regalo50';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='SMS Nueva pagina NC' WHERE source_medium='RegaloSMS';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='SMS Nueva pagina Costumers' WHERE source_medium='SMSNueva';
UPDATE SEM.campaign_ad_group_fashion_co SET channel='SMS Más temprano más barato' WHERE source_medium='BaratoSMS';

UPDATE SEM.campaign_ad_group_fashion_co set channel='Regresa (React.)'  where source_medium='Regresa' 
OR source_medium='Regresas49' OR source_medium='Regresa49';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Te Esperamos (React.)' where source_medium like 'teesperamos%';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Gracias (Repurchase)' where source_medium like 'gracias%';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Falabella Customers' where source_medium='50Bienvenido' OR source_medium like '50bienvenido%';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Cali' where source_medium='Fabian' OR
source_medium='Gloria' OR
source_medium='Miguel' OR source_medium like 'Cali%';
UPDATE SEM.campaign_ad_group_fashion_co set channel='CC Avenida Chile' where source_medium like 'PARATI%';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Correo Bogota' where source_medium='100Regalo';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Feria Libro' where source_medium like 'Feria%';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Correo directo bogota' where source_medium='50Regalo' OR source_medium='100Regalo' OR 
source_medium='200Regalo';
UPDATE SEM.campaign_ad_group_fashion_co set channel='SMS Madres' where source_medium='MADRE30';
UPDATE SEM.campaign_ad_group_fashion_co set channel='CEROIVA' where source_medium like 'CEROIVA%';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Circulo de la Moda' where source_medium like 'CIRCULOMODA%';
UPDATE SEM.campaign_ad_group_fashion_co set channel='SMS Mas Barato' where source_medium like 'MASBARATO%';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Mailing 2' where source_medium='REGALO1'
OR source_medium='REGALO2'
OR source_medium='REGALO3'
OR source_medium='REGALO050'
OR source_medium='REGALO100'
OR source_medium='REGALO200';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Reactivation60 dias' where source_medium ='Vuelve50';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Reactivation120 dias' where source_medium='vuelve050';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Reactivation180 dias' where source_medium='50Vuelve';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Valla 2nd stage' where source_medium like 'valla%';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Repurchase50' where source_medium='050Gracias';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Repurchase20' where source_medium='020Gracias';
UPDATE SEM.campaign_ad_group_fashion_co set channel='Catalogo Junio' where source_medium='CATALOGO50';


UPDATE SEM.campaign_ad_group_fashion_co SET channel='Unknown' WHERE channel is null;

#Criteo
UPDATE SEM.campaign_ad_group_fashion_co set channel='Criteo' where source_medium like '%criteo%';

SELECT 'Start: CHANNEL GROUPING', now();

#FIX OLD PARTNERHIPS TO OTHERS

UPDATE SEM.campaign_ad_group_fashion_co SET channel_group='Other (identified)' where channel='Libro Bogota';

UPDATE SEM.campaign_ad_group_fashion_co SET channel_group='Facebook Ads' WHERE channel='Facebook Ads' OR channel='Alejandra Arce Voucher'
OR channel='FB Ads CAC Junio' OR channel='FB Ads CAC Mayo';


UPDATE SEM.campaign_ad_group_fashion_co SET channel_group='Search Engine Marketing' WHERE channel='SEM (unbranded)';


UPDATE SEM.campaign_ad_group_fashion_co SET channel_group='Social Media' WHERE channel='Facebook Referral' OR channel='Twitter'
OR channel='FB Posts' OR channel='FB-Diana Call' OR channel='Muy Feliz' or channel='FB CAC' OR 
channel='Fashion Fan Page' OR channel='Linio Fan Page';


UPDATE SEM.campaign_ad_group_fashion_co SET channel_group='Retargeting' WHERE channel='Sociomantic' OR
channel='Vizury' OR channel = 'VEInteractive' or channel = 'GDN Retargeting'
OR channel='Facebook Retargeting' or channel='Criteo';


UPDATE SEM.campaign_ad_group_fashion_co SET channel_group='Google Display Network' WHERE channel='Google Display Network'; 


UPDATE SEM.campaign_ad_group_fashion_co SET channel_group='NewsLetter' WHERE channel='Newsletter'
OR channel='Voucher Navidad' OR channel='Voucher Fin de Año' 
OR channel='Bandeja CAC' OR channel='Tapete CAC' OR channel='Perfumes CAC'
OR channel='VoucherBebes-Feb' OR channel='Sartenes CAC' OR channel='Estufa CAC' OR channel='Vuelve CAC' OR
channel='Memoria CAC' OR
channel='Audifonos CAC' OR channel='Venir Reactivation' OR 
channel='Regresa Reactivation' OR 
channel='Bienvenido CAC' OR channel='Amigo CAC' OR channel='Aqui Reactivation' OR
channel='Tiramos la Casa por la ventana' OR channel='Creating Happiness' OR channel='Grita Reactivation' OR
channel='CACs Marzo' OR channel='CACs Abril' OR channel='CACs Mayo' OR channel='CACs Junio' OR channel='CACs Julio'
OR channel='Reactivación Mayo' OR channel='Fashion' OR channel='Reactivación Junio';





UPDATE SEM.campaign_ad_group_fashion_co SET channel_group='Search Engine Optimization' WHERE channel='Search Organic';


UPDATE SEM.campaign_ad_group_fashion_co SET channel_group='Partnerships' WHERE
channel='Banco Helm' OR channel='Banco de Bogotá' OR channel='Banco Davivienda'
OR channel='Banco Davivienda' OR channel='Banco BBVA'
OR channel='Campus Party' OR 
 channel='El Espectador' OR channel='Grupon'
OR channel='Dafiti' OR channel='Sin IVA 16' OR channel='En Medio' OR channel='Bancolombia' OR channel='AMEX'
OR channel='Citi Bank' OR channel='Lenddo' OR channel='Unknown Partnership March' OR 
channel='Post BDB' OR channel='VISA' OR channel='Coomservi' OR channel='Recuperación - Partnership';


UPDATE SEM.campaign_ad_group_fashion_co SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';


UPDATE SEM.campaign_ad_group_fashion_co SET channel_group='Offline Marketing' WHERE 
channel='ADN' OR channel='Journalist Vouchers' OR channeL='Metro' OR
channel='TRANSMILENIO' OR channel='SMS SanValentin' OR channel='SuperDescuentos' OR
channel='Folletos' OR channel='MUJER' OR channel='DiaEspecial' OR channel='Hora Loca SMS' OR 
channel='Calle Vouchers' OR channel='SMS' OR channel='BBVADP' OR channel='Other PR' OR channel='Valla Abril'
OR channel='SMS Nueva pagina NC' OR channel='SMS Nueva pagina Costumers' OR channel='SMS Más temprano más barato' OR 
channel='Barranquilla' OR channel='Regresa (React.)' OR channel='Te Esperamos (React.)' OR channel='Gracias (Repurchase)'
OR channel='Falabella Customers' OR channel='Cali' OR channel='CC Avenida Chile' OR channel='Correo Bogota' 
OR channel='Feria Libro' OR channel='Correo directo bogota' OR channel='CEROIVA' OR channel='SMS Madres' or
channel='Circulo de la Moda' OR channel='SMS Mas Barato' or channel='Mailing 2' or channel='Publimetro' or
channel='Reactivation60 dias' or
channel='Reactivation120 dias' or
channel='Reactivation180 dias' or channel='Valla 2nd stage' or channel='Repurchase50' or
channel='Repurchase20' OR channel='Catalogo Junio';
 

UPDATE SEM.campaign_ad_group_fashion_co SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales' OR channel='Replace' OR channel='ReOrder' 
OR channel='Invalid-Crossell' OR channel='Unknown-CS' OR 
channel='CS-Insentivos' OR channel='CS-REORDER' OR channel='CS-CANCEL';


UPDATE SEM.campaign_ad_group_fashion_co SET channel_group='Branded' WHERE channel='Linio.com Referral' 
OR channel='Directo / Typed' OR
channel='SEM Branded' OR channel='Liniofashion.com.co Referral';


UPDATE SEM.campaign_ad_group_fashion_co SET channel_group='Mercado Libre - Alamaula' 
WHERE channel='Mercado Libre' OR channel='Otros - Mercado Libre' or channel='Alamaula';


-- UPDATE SEM.campaign_ad_group_fashion_co SET channel_group='Blogs' WHERE 
-- channel='Blog Linio' OR channel='Blog Mujer' OR channel='Lector';


UPDATE SEM.campaign_ad_group_fashion_co SET channel_group='Affiliates' WHERE channel='Buscape' or 
channel='Affiliate Program';



UPDATE SEM.campaign_ad_group_fashion_co SET channel_group='Other (identified)' WHERE channel='Referral Sites' 
OR channel='Reactivation Letters' OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' 
OR channel='Referral Program' OR channel='Nueva Página' OR channel='Promesa Linio' OR channel='NO IVA Abril' OR
channel='Invitados VIP Círculo de la moda';



UPDATE SEM.campaign_ad_group_fashion_co SET 
channel_group='Non identified' WHERE channel='Unknown' OR channel='Unknown-Voucher' OR channel='Unknown-GA Tracking';


UPDATE SEM.campaign_ad_group_fashion_co SET channel_group='Affiliates' where channel='Pampa Network';



END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `channel_report`()
BEGIN

update tbl_order_detail inner join ga_order 
on tbl_order_detail.order_nr=ga_order.order_nr
 set `source/medium`= concat(ga_order.source,' / ',ga_order.medium),
tbl_order_detail.campaign=ga_order.campaign where `source/medium` is null;



##UPDATE tbl_order_detail SET source='',channel='', channel_group='';

UPDATE tbl_order_detail SET source='' where source is null;
UPDATE tbl_order_detail SET campaign='' where campaign is null;

UPDATE tbl_order_detail SET source=coupon_code WHERE coupon_code is not null;

UPDATE tbl_order_detail SET source=`source/medium` WHERE coupon_code is null;

UPDATE tbl_order_detail SET channel='New Register' 
WHERE (source like 'NL%' OR source like 'NR%') AND `source/medium` is null;
UPDATE tbl_order_detail SET source=`source/medium` 
WHERE 
(coupon_code like 'NL%' OR coupon_code='SINIVA' OR coupon_code like 'COM%' 
OR coupon_code like 'NUEVA%' OR coupon_code like 'CASA%' OR coupon_code like 'PAG%' 
OR coupon_code='100COMBO-AR238EL90VPTLACOL-15x' Or coupon_code like
'CERO%' OR coupon_code like 'MADRID%' or coupon_code='100COMBO-AR238EL90VPTLACOL-15x' or coupon_code='comboPR237EL91VPSLACOL1' OR
coupon_code like 'shilher-esmaltescombo%' OR coupon_code like 'dev%') AND `source/medium` is not null;

UPDATE tbl_order_detail SET channel='Referral Sites' WHERE source like '%referral'
AND (source not like 'google%' and source not like 'linio%' and source not like 'facebook%' OR source<>'linio.com.co / referral' AND
 source<>'facebook / retargeting');

UPDATE tbl_order_detail SET source=`source/medium` where channel_group='Non Identified';



UPDATE tbl_order_detail SET channel='Buscape' WHERE source like '%buscape%';

UPDATE tbl_order_detail SET channel='Other Affiliates' where source like '%affiliate%' or source like '%afiliate%';

UPDATE tbl_order_detail SET channel='Pampa Network' WHERE source like 'pampa%';

UPDATE tbl_order_detail set channel='Promodescuentos' where source like '%promodescuentos%';


UPDATE tbl_order_detail SET channel='Bing' where source like '%bing%';
UPDATE tbl_order_detail SET channel='SEM Branded' WHERE (source='google / cpc' or source like '%bing%') AND campaign like '%linio%';
UPDATE tbl_order_detail SET channel='SEM (unbranded)' WHERE source='google / cpc' AND campaign not like '%linio%'
and (campaign not like 'er.%' AND campaign not like '[D%' AND campaign not like 'r.%');
UPDATE tbl_order_detail SET channel='Other Branded' WHERE source = '201.151.86.164' or source ='10.0.0.9' or source = 'www-staging.linio.com.mx' or source='blog.linio.com.mx' or source='213.229.186.15';

UPDATE tbl_order_detail SET channel='Google Display Network' WHERE source='google / cpc' AND 
(campaign like 'er.%' or campaign like '[D%' or campaign like 'r.%' or campaign like '%d.%');

UPDATE tbl_order_detail set channel='Other Retargeting' where source like '%retargeting%';

UPDATE tbl_order_detail SET channel='Facebook R.' WHERE source='facebook / retargeting';


UPDATE tbl_order_detail SET channel='Sociomantic' WHERE source like 'socioma%' or source ='facebook / retargeting';
UPDATE tbl_order_detail SET channel='Vizury' WHERE source like '%vizury%';
UPDATE tbl_order_detail SET channel='VEInteractive' WHERE source like '%VEInteractive%';
UPDATE tbl_order_detail SET channel='Main ADV' where source = 'retargeting / mainadv' or source like '%mainadv%' or source like '%solocpm%';
UPDATE tbl_order_detail SET channel='AdRoll' where source like '%adroll%';
UPDATE tbl_order_detail SET channel='My Things' where source like '%mythings%';
UPDATE tbl_order_detail SET channel='Trade Tracker' where source like '%tradetracker%';
UPDATE tbl_order_detail SET channel='Triggit' where source like '%triggit / retargeting%';

UPDATE tbl_order_detail SET channel='Mercado Libre' WHERE source='%mercado%referral%' or source like 'ML%';
UPDATE tbl_order_detail SET channel='Alamaula' WHERE source like 'alamaula%' or source like 'ALA%';



UPDATE tbl_order_detail SET channel='Twitter' 
WHERE source='t.co / referral' OR source='socialmedia / smtweet' OR source like 'Twitter%' OR source like 'TW%';
UPDATE tbl_order_detail SET channel='Linio Fan Page' WHERE (source='FASHION25' OR source like '%facebook%' OR (source like 'FB%' AND source not like 'FBCAC%')
OR source='faebook / socialmedia') AND (source<>'facebook / socialmediaads' AND source<>'facebook / retargeting'); 
UPDATE tbl_order_detail SET channel='FB CAC' 
WHERE source like 'FBCAC%';

UPDATE tbl_order_detail SET channel='Youtube' where source='youtube / socialmedia';

UPDATE tbl_order_detail SET channel='Fashion Fan Page' 
WHERE source like 'FBF%';

UPDATE tbl_order_detail SET channel='Linio Fan Page' 
WHERE source='facebook / socialmedia' AND (campaign not like '%fashion%' and campaign is not null) or source='twitter / socialmedia';

UPDATE tbl_order_detail SET channel='Linio Fan Page' 
WHERE order_nr='200113276';

UPDATE tbl_order_detail SET channel='Fashion Fan Page' 
WHERE source='facebook / socialmedia' AND campaign like '%fashion%';


UPDATE tbl_order_detail SET channel='Fashion Fan Page' 
WHERE source='facebook / socialmedia_fashion';



UPDATE tbl_order_detail SET channel='Facebook Ads' WHERE 
(source ='facebook / socialmediaads' and source<>'facebook / retargeting') or source='facebook / (not set)';
UPDATE tbl_order_detail SET channel='Alejandra Arce Voucher' WHERE source like 'voucher5%' OR source like 'voucher2%';
UPDATE tbl_order_detail SET channel='FB Ads CAC Mayo' where source like 'CACFA04%';
UPDATE tbl_order_detail SET channel='FB Ads CAC Junio' where source like 'CACFA05%';

#UPDATE tbl_order_detail SET channel='Facebook Retargeting' WHERE source ='facebook / retargeting';

UPDATE tbl_order_detail set channel='Other Tele Sales' where source = 'CS /%' or source = '%/ CS' or source like 'telesales /%' or source like '%/ telesales'; 

UPDATE tbl_order_detail SET channel='UpSell' WHERE  source like 'UP%' OR source like '%UP%' OR 
(source='CS / call' AND campaign like 'ups%' );
UPDATE tbl_order_detail SET channel='Invalids' WHERE 
(source='CS / call' AND campaign like 'INVALID%') or SOURCE='CS / invalid' OR source='OUT1mTG7';
UPDATE tbl_order_detail SET channel='Reactivation' WHERE (source='CS / call' AND campaign like 'reactivation%') OR source like 'ACTIVA%' 
OR source='CS / Reactivation';
UPDATE tbl_order_detail SET channel='Inbound' WHERE (source='CS / call' AND campaign like 'inbou%' ) OR source='CS / Inbound' OR 
source='out%' OR source like 'Telesales%';
UPDATE tbl_order_detail SET channel='OutBound' WHERE source='CS / OutBound' OR source='CS / out' OR source like 'CV%' OR (source='CS / call' AND campaign like 'outbou%');
UPDATE tbl_order_detail SET channel='CrossSales' WHERE source='CS / CrossSale' OR source='CS / cross_sell';
UPDATE tbl_order_detail SET channel='Replace' WHERE source like '%replace%';
UPDATE tbl_order_detail SET channel='ReOrder' WHERE source='CS / reorder';
UPDATE tbl_order_detail SET channel='Invalid-Crossell' WHERE source='CS / invalidcrossell';
UPDATE tbl_order_detail SET channel='Unknown-CS' WHERE (source is null or source='') AND
(assisted_sales_operator<>'' OR  assisted_sales_operator is not null);
UPDATE tbl_order_detail SET channel='CS-Insentivos' WHERE source='CS / Insentivos' OR 
source like 'CSIN%';

UPDATE tbl_order_detail set channel= 'Dark Post' where campaign like '%darkpost%';

UPDATE tbl_order_detail SET channel='Search Organic' WHERE source like '%organic' OR source='google.com.co / referral';

UPDATE tbl_order_detail SET channel='Search Organic' where source like 'google.%' and source like '%referral';

UPDATE tbl_order_detail SET channel='Yahoo Answers' where source like '%answers.yahoo.com%';

#NEWSLETTER
UPDATE tbl_order_detail set channel='Newsletter' WHERE (source='Postal / CRM' 
OR source='Email Marketing / CRM' OR source='EMM / CRM' 
OR source='Postal / (not set)') or (source like '%CRM%'and source not like 'VEInteractive%') 
OR (source like 'LOCO%' and (source is not null and
 source not like '%blog%')
);
UPDATE tbl_order_detail set Channel='Voucher Navidad' WHERE source like 'NAVIDAD%' 
OR source like 'LINIONAVIDAD%'; 
UPDATE tbl_order_detail set Channel='Voucher Fin de Año' where source='FINDEAÑO31';
UPDATE tbl_order_detail set Channel='Bandeja CAC' where source='BANDEJA0203';
UPDATE tbl_order_detail set Channel='Tapete CAC' where source like 'TAPETE%';
UPDATE tbl_order_detail set Channel='Perfumes CAC' where source like 'PERFUME%';
UPDATE tbl_order_detail set Channel='VoucherBebes-Feb' where source like 'BEBES%';
UPDATE tbl_order_detail set channel='Sartenes CAC' where source like 'SARTENES%';
UPDATE tbl_order_detail set channel='Estufa CAC' where source like 'ESTUFA%';
UPDATE tbl_order_detail set channel='Vuelve CAC' where source like 'VUELVE%';
UPDATE tbl_order_detail set channel='Memoria CAC'  where source like 'MEMORIA%';
UPDATE tbl_order_detail set channel='Audifonos CAC'  where source like 'AUDIFONO%';
UPDATE tbl_order_detail set channel='CACs Mayo' where source like 'CAC05%' OR source='CAC0420' 
OR (source='Postal / CRM' AND campaign like '%cac%' and yrmonth=201305);
UPDATE tbl_order_detail set channel='CACs Abril' where (source like 'CAC04%' and source<>'CAC0420') 
OR (source='Postal / CRM' AND campaign like '%cac%' and yrmonth=201304);
UPDATE tbl_order_detail set channel='CACs Marzo' where source like 'CAC03%' 
OR (source='Postal / CRM' AND campaign like '%cac%' and yrmonth=201303);
UPDATE tbl_order_detail set channel='CACs Junio' where source like 'CAC06%'
 OR (source='Postal / CRM' AND campaign like '%cac%' and yrmonth=201306);
UPDATE tbl_order_detail set channel='CACs Julio' where source like 'CAC07%' OR 
(source='Postal / CRM' AND campaign like '%cac%' and yrmonth=201307);
UPDATE tbl_order_detail set channel='Reactivación Mayo' where source='Regalo50' OR source='Regalo40' 
OR source='Regalo60' OR source like 'GRITA%' or Source='regalo30' or source LIKE 'aqui%';
UPDATE tbl_order_detail set channel='Fashion' where source like 'MODA%' OR source like 'ESTILO%';

UPDATE tbl_order_detail set channel='Venir Reactivation' where source like 'VENIR%';
UPDATE tbl_order_detail set channel='Regresa Reactivation' where source like 'REGRESA%' AND (source<>'Regresas' 
AND source<>'Regresas49' AND source<>'Regresa49');
UPDATE tbl_order_detail set channel='Bienvenido CAC'  where source like 'BIENVENIDO%';
UPDATE tbl_order_detail set channel='Amigo CAC'  where source like 'AMIGO%';
UPDATE tbl_order_detail set channel='Tiramos la Casa por la ventana' where source like 'CASA%' 
AND (channel='' OR channel is null);
UPDATE tbl_order_detail set channel='Reactivación Junio' where source='LOCURA50' OR source='LOCURA040' 
OR source='LOCURA40' 
OR source='LOCURA60' OR 
(source='Postal / CRM' AND campaign like '%reactivation%' and yrmonth=201307) OR 
(coupon_code_description like 'CRM%' and coupon_code_description not like'%CAC%' and yrmonth=201307 and
coupon_code=source);

UPDATE tbl_order_detail set channel='Reactivación Julio' where 
(source='Postal / CRM' AND campaign like '%reactivation%' and yrmonth=201307) OR 
(coupon_code_description like 'CRM%' and coupon_code_description not like'%CAC%' and yrmonth=201308 and
coupon_code=source);


update tbl_order_detail SET channel_group='NewsLetter' where
(coupon_code_description like 'CRM%') and coupon_code=source and yrmonth>=201305;






-- UPDATE tbl_order_detail SET channel='Blog Linio' WHERE source like 'blog%' ;
-- UPDATE tbl_order_detail SET channel='Blog Linio' WHERE order_nr='200133986' OR order_nr='200882196'
-- OR source='BL%' OR source like 'madres%';
-- UPDATE tbl_order_detail SET channel='Blog Mujer' WHERE source='mujer.linio.com.co / referral';
-- UPDATE tbl_order_detail SET channel='Lector' WHERE source like 'lector%';

UPDATE tbl_order_detail SET channel='Reactivation Letters' WHERE (source like 'REACT%' OR source like 'CLIENTVIP%' OR source like 'LINIOVIP%' OR source='VUELVE' OR source like 'CLIENTEVIP%') AND source<>'CLIENTEVIP1';
UPDATE tbl_order_detail SET channel='Customer Adquisition Letters' WHERE  source like 'CONOCE%' OR source='CLIENTEVIP1';
UPDATE tbl_order_detail SET channel='Internal Sells' WHERE source like 'INTER%';
UPDATE tbl_order_detail SET Channel='Employee Vouchers' 
WHERE source like 'OCT%' OR source like 'SEP%' OR source like 'EMP%' OR (source like 'CS%' AND (channel is null OR channel=''));
UPDATE tbl_order_detail SET channel='Parcel Vouchers' WHERE  source like 'DESCUENTO%' OR source='LINIO0KO' OR source='LINIOiju';
UPDATE tbl_order_detail SET channel='New Register' WHERE  source like 'NL%' OR source like 'NR%';
UPDATE tbl_order_detail SET channel='Disculpas' WHERE source like 'H1yJvf' OR source like 'H87h7C' OR source='KI025EL78LLXLACOL-25324'
OR source='KI025EL85GRQLACOL-4516';
UPDATE tbl_order_detail SET channel='Suppliers Email' WHERE source='suppliers_mail / email';
UPDATE tbl_order_detail SET channel='Out Of Stock' WHERE source like 'NST%' OR source='N0Ctor';
UPDATE tbl_order_detail SET channel='Devoluciones' WHERE source like 'DEV%';
UPDATE tbl_order_detail SET channel='Broken Vouchers' WHERE source like 'HVE%';
UPDATE tbl_order_detail SET channel='Broken Discount' WHERE source  like  'KER%' OR source='D1UqWU';
UPDATE tbl_order_detail SET channel='Voucher Empleados' WHERE source like 'EMPLEADO%';
UPDATE tbl_order_detail SET channel='Gift Cards' WHERE source like 'GC%';
UPDATE tbl_order_detail SET channel='Friends and Family' WHERE source like 'FF%';
UPDATE tbl_order_detail SET channel='Leads' WHERE source like 'LC%';
UPDATE tbl_order_detail SET channel='Nueva Página' WHERE source like 'NUEVA%' and (channel='' OR channel is null);
UPDATE tbl_order_detail SET channel='Promesa Linio' WHERE source like 'PROMESA%';
UPDATE tbl_order_detail SET channel='Creating Happiness' WHERE source like 'FELIZ%' OR
source like 'MUFELIZ%';
UPDATE tbl_order_detail SET channel='NO IVA Abril' WHERE source like 'PAG%' OR SOURCE like
'CERO%';
UPDATE tbl_order_detail set channel='Invitados VIP Círculo de la moda' where source like 'CIRCULOREG%';
UPDATE tbl_order_detail set channel='CS-REORDER' where source like 'REO%';
UPDATE tbl_order_detail set channel='CS-CANCEL' where source like 'CAN%';

UPDATE tbl_order_detail SET channel='Other Partnerships' where source like '%PARTNERSHIP%' or coupon_code like '%PARTNERSHIP%';

UPDATE tbl_order_detail SET channel='Banco Helm' WHERE  source like  'HELM%';
UPDATE tbl_order_detail SET channel='Banco de Bogotá' WHERE  source like 'CLIENTEBOG%' OR source like 'MOVISTAR%' 
OR source like 'BOG%' OR source like 'BDB%' OR source='BB42' OR source='BB46' OR source='BBLAV' OR source='BBNEV' OR
source='PA953EL96NHFLACOL' OR
source='PA953EL96NHFLACOL' OR
source='BH859EL51WBWLACOL' OR
source='CU054HL58AXPLACOL' OR
source='SA860TB24WCXLACOL' OR
source='HA997TB17DLQLACOL' OR
source='SA860TB35WCMLACOL' OR
source='SA860TB37WCKLACOL' OR
source='BO944TB20JEPLACOL' OR
source='MG614TB23HTWLACOL' OR
source='BA186TB36HTJLACOL' OR
source='PR506TB63BFCLACOL' OR
source='MI392TB32VRBLACOL' OR
source='MI392TB33VRALACOL' OR
source='BO944TB79JCILACOL' OR
source='BO944TB24JELLACOL' OR
source='HA997TB23DLKLACOL';
UPDATE tbl_order_detail SET channel='Post BDB' WHERE source like 'POSTBD%';

UPDATE tbl_order_detail SET channel='Unknown Partnership March' WHERE source='LG082EL56ECLLACOL-20343' OR
SOURCE='SO017EL14ZQRLACOL48' OR
SOURCE='LG082EL97RQSLACOL48' OR
SOURCE='NI088EL26CJLLACOL48' OR
SOURCE='GE008EL90BEZLACOL48' OR
SOURCE='BDE862EL18WGZLACOL48' OR
SOURCE='LU939HL19XSMLACOL48' OR
SOURCE='LU939HL11XSULACOL48' OR
SOURCE='LU939HL03XTCLACOL48' OR
SOURCE='AR842EL87LLOLACOL48' OR
SOURCE='HO107EL04MOPLACOL48' OR
SOURCE='TC124EL64NBRLACOL48' OR
SOURCE='CO052EL34DGFLACOL48' OR
SOURCE='MI392EL71GRCLACOL48' OR
SOURCE='GU689FA67MQALACOL48' OR
SOURCE='TO692FA65MQCLACOL48' OR
SOURCE='GU689FA66MQBLACOL48' OR
SOURCE='FR119EL73NBILACOL48' OR
SOURCE='HA135EL15KYALACOL48' OR
SOURCE='CU054EL66KZXLACOL48' OR
SOURCE='IN122TB66NBPLACOL48';

UPDATE tbl_order_detail SET channel='Banco Davivienda' WHERE  source like 'DAV%' OR source='DV_AG' OR source='DVPLANCHA46';
UPDATE tbl_order_detail SET channel='Banco BBVA' WHERE  (source like 'BBVA%' AND source<>'BBVADP') OR
source='SA860TB32WCPLACOL' OR
source='SA860TB43WCELACOL' OR
source='BA186TB35HTKLACOL' OR
source='PR506TB55BFKLACOL' OR
source='PR506TB62BFDLACOL' OR
source='PA953EL97NHELACOL' OR
source='PO916EL07ETSLACOL' OR
source='PO916EL04ETVLACOL' OR
source='LU939HL23XSILACOL' OR
source='LU939HL32XRZLACOL' OR
source='HA997TB15DLSLACOL' OR
source='SA860TB35WCMLACOLACOL' or (source like '%BBVA%' and source<>'FBBBVAS');

UPDATE tbl_order_detail SET channel='Campus Party' WHERE source like 'CP%';
UPDATE tbl_order_detail SET channel='Publimetro' WHERE source like 'PUBLIMETRO%';
UPDATE tbl_order_detail SET channel='El Espectador' WHERE source='ESPECT0yx' OR source like 'FIXED%';
UPDATE tbl_order_detail SET channel='Grupon' WHERE source like 'GR%' AND 
(source not like 'gracias%' AND source not like 'GRI%' and source is not null);
UPDATE tbl_order_detail SET channel='Dafiti' WHERE source='AMIGODAFITI';
UPDATE tbl_order_detail SET channel='Sin IVA 16' WHERE source='SINIVA16';
UPDATE tbl_order_detail SET channel='En Medio' WHERE source='ENMEDIO';
UPDATE tbl_order_detail SET channel='Bancolombia' WHERE source like 'AMEX%' OR
source='ABNSbkpSmM' OR
source='ATR08i1X6D' OR
source='TABAMEX1DA2h' OR source='ATR08i1X6D' OR
source='DEV1sd0c' OR
source='DEVfjFW6' OR
source='DEVbCM2o' OR
source='DEVN6TJN' OR
source='DEVFvwqn' OR
source='DEV0QUfY' OR
source='DEVmhwdB' OR
source='DEV0rCuq' OR
source='DEVlLjUZ' OR
source='DEVzKPuf' OR
source='DEV6IJP6' OR
source='DEV08L8A' OR
source='DEV0Fvg' OR
source='DEVcTttT' OR
source='DEVywRNF' OR
source='DEV0umCc' OR
source='DEVFcXZN' OR
source='AMEXTAB8BSoc';
UPDATE tbl_order_detail SET channel='Citi Bank' WHERE  source like 'CITI%' OR source like 'POSTCITI%';

UPDATE tbl_order_detail SET channel='Lenddo' where order_nr='200547692' OR
order_nr='200461292' OR
order_nr='200427692' OR
order_nr='200111692' OR
order_nr='200547692' OR source Like 'LEND%' or source like 'LENNDO%';
UPDATE tbl_order_detail SET channel='VISA' WHERE source like 'VISA%';

update tbl_order_detail SET 
channel=substr(substr(coupon_code_description, locate('-',coupon_code_description)+1), 1, 
locate('-', substr(coupon_code_description, locate('-',coupon_code_description)+1))-1)
where coupon_code=source and yrmonth>=201305 and 
coupon_code_description like 'Partnerships-%';

update tbl_order_detail SET channel_group='Partnerships' where
(coupon_code_description like 'Partnerships-%') and coupon_code=source and yrmonth>=201305;


UPDATE tbl_order_detail SET channel='Linio.com Referral' WHERE source='linio.com / referral' 
OR source='info.linio.com.co / referral' OR source='linio.com.co / referral' or source='r.linio.com.co / referral';
UPDATE tbl_order_detail SET channel='Directo / Typed' WHERE  source='(direct) / (none)';


UPDATE tbl_order_detail SET channel='Liniofashion.com.co Referral' WHERE source='liniofashion.com.co / referral';




UPDATE tbl_order_detail SET channel='Referral Program' WHERE source like 'REF%';

UPDATE tbl_order_detail set channel='Corporate Sales' WHERE source like'VC%' OR source like 'VCJ%' OR source like 'VCH%'OR source like 'CVE%' 
OR source like 'VCA%' OR source='BIENVENIDO10';

UPDATE tbl_order_detail SET channel='Journalist Vouchers' WHERE source='CAMILOCARM50' OR
source='JUANPABVAR50' OR
source='LAURACHARR50' OR
source='MONICAPARA50' OR
source='DIEGONARVA50' OR
source='LUCYBUENO50' OR
source='DIANALEON50' OR
source='SILVIAPARR50' OR
source='LAURAAYALA50' OR
source='LEONARDONI50' OR
source='CYNTHIARUIZ50' OR
source='JAVIERLEAESC50' OR
source='LORENAPULEC50' OR
source='MARIAISABE50' OR
source='PAOLACALLE50' OR
source='ISABELSALAZ50' OR
source='VICTORSOLA50' OR
source='FABIANRAM50' OR
source='ANDRESROD50' OR
source='HENRYGON50' OR
source='JOHANMA50' OR
source='ALFONSOAYA50' OR
source='VICTORSOLANAV' OR SOURCE='LUISFERNANDOBOTERO' OR
source='PILARBOLIVAR' OR
source='MARCELAESTRADA' OR
source='LUXLANCHEROS';

UPDATE tbl_order_detail SET channel='ADN' WHERE source like'ADN%';
UPDATE tbl_order_detail SET channel='Metro' WHERE source like'METRO%';
UPDATE tbl_order_detail SET channel='TRANSMILENIO' WHERE source like 'TRANSMILENIO%' OR
SOURCE='Calle76' OR
SOURCE='Calle100' OR
SOURCE='Calle63' OR
SOURCE='Calle45' OR
SOURCE='Marly' or source='HEROES' OR SOURCE='ESCUELAMILITAR';
UPDATE tbl_order_detail SET channel='SMS SanValentin' WHERE source like'SANVALENTIN%';
UPDATE tbl_order_detail SET channel='SuperDescuentos' WHERE source like'SUPERDESCUENTOS%';
UPDATE tbl_order_detail SET channel='Folletos' WHERE source='Regalo';
UPDATE tbl_order_detail SET channel='Mujer' WHERE source like 'MUJER%' AND source<>'mujer.linio.com.co / referral';
UPDATE tbl_order_detail SET channel='DiaEspecial' WHERE source like 'DiaEspecial%';
UPDATE tbl_order_detail SET channel='Hora Loca SMS' WHERE source like 'HORALOCA%';
UPDATE tbl_order_detail SET channel='Calle Vouchers' WHERE source like 'call%';
UPDATE tbl_order_detail SET channel='SMS' WHERE source like 'SMS%' AND source<>'SMSNueva';
UPDATE tbl_order_detail SET channel='BBVADP' WHERE source='BBVADP';
UPDATE tbl_order_detail SET channel='Other PR' WHERE source='LG082EL97RQSLACOLDP' OR
source='SO017EL14ZQRLACOLDP';
UPDATE tbl_order_detail SET channel='Valla Abril' WHERE source='Regalo50';
UPDATE tbl_order_detail SET channel='SMS Nueva pagina NC' WHERE source='RegaloSMS';
UPDATE tbl_order_detail SET channel='SMS Nueva pagina Costumers' WHERE source='SMSNueva';
UPDATE tbl_order_detail SET channel='SMS Más temprano más barato' WHERE source='BaratoSMS';
UPDATE tbl_order_detail SET channel='Barranquilla' WHERE source like '%quilla%' OR source='Karen' OR source='Deiner' OR
source='Jair' OR source='Sergio' OR
order_nr='200973772' OR
order_nr='200254572' OR
order_nr='200674572' OR
order_nr='200633772' OR
order_nr='200312572' OR
order_nr='200978572' OR
order_nr='200217572' OR
order_nr='200625572' OR
order_nr='200583572' OR
order_nr='200973572' OR
order_nr='200333572' OR
order_nr='200544172' OR
order_nr='200486172' OR
order_nr='200316172';
UPDATE tbl_order_detail set channel='Regresa (React.)'  where source='Regresa' 
OR source='Regresas49' OR source='Regresa49';
UPDATE tbl_order_detail set channel='Te Esperamos (React.)' where source like 'teesperamos%';
UPDATE tbl_order_detail set channel='Gracias (Repurchase)' where source like 'gracias%';
UPDATE tbl_order_detail set channel='Falabella Customers' where source='50Bienvenido' OR source like '50bienvenido%';
UPDATE tbl_order_detail set channel='Cali' where source='Fabian' OR
source='Gloria' OR
source='Miguel' OR source like 'Cali%';
UPDATE tbl_order_detail set channel='CC Avenida Chile' where source like 'PARATI%';
UPDATE tbl_order_detail set channel='Correo Bogota' where source='100Regalo';
UPDATE tbl_order_detail set channel='Feria Libro' where source like 'Feria%';
UPDATE tbl_order_detail set channel='Correo directo bogota' where source='50Regalo' OR source='100Regalo' OR 
source='200Regalo';
UPDATE tbl_order_detail set channel='SMS Madres' where source='MADRE30';
UPDATE tbl_order_detail set channel='CEROIVA' where source like 'CEROIVA%';
UPDATE tbl_order_detail set channel='Circulo de la Moda' where source like 'CIRCULOMODA%';
UPDATE tbl_order_detail set channel='SMS Mas Barato' where source like 'MASBARATO%';
UPDATE tbl_order_detail set channel='Mailing 2' where source='REGALO1'
OR source='REGALO2'
OR source='REGALO3'
OR source='REGALO050'
OR source='REGALO100'
OR source='REGALO200';
UPDATE tbl_order_detail set channel='Reactivation60 dias' where source ='Vuelve50';
UPDATE tbl_order_detail set channel='Reactivation120 dias' where source='vuelve050';
UPDATE tbl_order_detail set channel='Reactivation180 dias' where source='50Vuelve';
UPDATE tbl_order_detail set channel='Valla 2nd stage' where source like 'valla%';
UPDATE tbl_order_detail set channel='Repurchase50' where source='050Gracias';
UPDATE tbl_order_detail set channel='Repurchase20' where source='020Gracias';
UPDATE tbl_order_detail set channel='Catalogo Junio' where source='CATALOGO50';



update tbl_order_detail SET 
channel=substr(substr(coupon_code_description, locate('-',coupon_code_description)+1), 1, 
locate('-', substr(coupon_code_description, locate('-',coupon_code_description)+1))-1)
where coupon_code=source and yrmonth>=201305 and 
coupon_code_description like 'offline%';

update tbl_order_detail SET channel_group='Offline Marketing' where
(coupon_code_description like 'offline%') and coupon_code=source and yrmonth>=201305;

#PR OTHER
update tbl_order_detail SET 
channel=substr(substr(coupon_code_description, locate('-',coupon_code_description)+1), 1, 
locate('-', substr(coupon_code_description, locate('-',coupon_code_description)+1))-1)
where coupon_code=source and yrmonth>=201305 and 
coupon_code_description like 'PR-%';

update tbl_order_detail SET channel_group='Other (identified)' where
(coupon_code_description like 'PR-%') and coupon_code=source and yrmonth>=201305;

UPDATE tbl_order_detail set channel='Corporate Deal' where coupon_code LIKE 'CDEAL%' or source like '%CORPORATESALES%';

UPDATE tbl_order_detail set channel='Corporate Sales' WHERE order_nr ='200091496' 
OR order_nr ='200078186' OR order_nr ='200066886' OR order_nr ='200088876' OR order_nr ='200091629'
OR order_nr ='200061629' OR order_nr ='200021629' OR order_nr ='200041629' OR order_nr ='200035629'
OR order_nr ='200015629' OR order_nr ='200075629' OR order_nr ='200085629' OR order_nr ='200065629'
OR order_nr ='200018629' OR order_nr ='200078629' OR order_nr ='200098629' OR order_nr ='200037636'
OR order_nr ='200027356' OR order_nr ='200078656' OR order_nr ='200058656' OR order_nr ='200032829'
OR order_nr ='200051569' OR order_nr ='200097636' OR order_nr ='200023129' OR order_nr ='200025729'
OR order_nr ='200037969' OR order_nr ='200094566' OR order_nr ='200064566' OR order_nr ='200017946'
OR order_nr ='200056446' OR order_nr ='200091732' OR order_nr ='200061732' OR order_nr ='200021732'
OR order_nr ='200067932' OR order_nr ='200012232' OR order_nr ='200062112' OR order_nr ='200027812'
OR order_nr ='200047812' OR order_nr ='200018812' OR order_nr ='200021972' OR order_nr ='200035972'
OR order_nr ='200055972' OR order_nr ='200027672' OR order_nr ='200058672' OR order_nr ='200095272'
OR order_nr ='200096272' OR order_nr ='200066272' OR order_nr ='200046272' OR order_nr ='200012272'
OR order_nr ='200072272' OR order_nr ='200078186' OR order_nr ='200091496' OR order_nr ='200034532'
OR order_nr ='200014532' OR order_nr ='200045272' OR order_nr ='200075862' OR order_nr ='200046796'
OR order_nr ='200013572' OR order_nr ='200066886' OR order_nr ='200088876' OR order_nr ='200027356'
OR order_nr ='200078656' OR order_nr ='200051569' OR order_nr ='200032829' OR order_nr ='200097636'
OR order_nr ='200023129' OR order_nr ='200025729' OR order_nr ='200043322' OR order_nr ='200083356'
OR order_nr ='200031416' OR order_nr ='200014936' OR order_nr ='200047812' OR order_nr ='200027812'
OR order_nr='200026765' OR order_nr='200065495' OR order_nr='200094437' OR
order_nr='200021561' OR
order_nr='200012651' OR
order_nr='200044861' OR
order_nr='200021791' OR
order_nr='200057991' OR
order_nr='200083781' OR
order_nr='200064581' OR
order_nr='200011781' OR
order_nr='200073781' OR
order_nr='200023781' OR
order_nr='200036831' OR
order_nr='200029831' OR
order_nr='200099831' OR
order_nr='200089831' OR
order_nr='200019831' OR
order_nr='200089683' OR
order_nr='200046911' OR
order_nr='200041933' OR
order_nr='200085933' OR
order_nr='200045933' OR
order_nr='200037933' OR
order_nr='200017933' OR
order_nr='200041933' OR
order_nr='200085933' OR
order_nr='200045933' OR
order_nr='200037933' OR
order_nr='200017933' OR
order_nr='200781642' OR
order_nr='200677842' OR
order_nr='200991742' OR
order_nr='200271142' OR
order_nr='200245142' OR
order_nr='200325142' OR
order_nr='200365142' OR
order_nr='200895142' OR
order_nr='200555142' OR
order_nr='200271142' OR
order_nr='200423142' OR
order_nr='200841142' OR
order_nr='200195962' OR
order_nr='200829492' OR
order_nr='200168292' OR
order_nr='200918692' OR order_nr='200853382'
OR order_nr='200532252' or
order_nr='200529252' OR
order_nr='200884952' OR
order_nr='200828952' OR
order_nr='200359852' OR
order_nr='200297852' OR
order_nr='200452752' OR
order_nr='200411152' OR
order_nr='200914352';



update tbl_order_detail set channel='FB-Diana Call'
where (order_nr='200059657' OR
order_nr='200017957' OR
order_nr='200076157' OR
order_nr='200059157' OR
order_nr='200064357' OR
order_nr='200021357' OR
order_nr='200016217' OR
order_nr='200095217' OR
order_nr='200082617' OR
order_nr='200059617' OR
order_nr='200074917' OR
order_nr='200086917' OR
order_nr='200036917' OR
order_nr='200032517' OR
order_nr='200074117' OR
order_nr='200028317' OR
order_nr='200046437' OR
order_nr='200088437' OR
order_nr='200044837' OR
order_nr='200017837' OR
order_nr='200037445' OR
order_nr='200086725' OR
order_nr='200097725' OR
order_nr='200093725' OR
order_nr='200079525' OR
order_nr='200062325' OR
order_nr='200095325' OR
order_nr='200042665' OR
order_nr='200093965' OR
order_nr='200054865' OR
order_nr='200082765' OR
order_nr='200025765' OR
order_nr='200035765' OR
order_nr='200053765' OR
order_nr='200012365' OR
order_nr='200056495' OR
order_nr='200061495' OR
order_nr='200024295' OR
order_nr='200066695' OR
order_nr='200053695' OR
order_nr='200026395' OR
order_nr='200061395' OR
order_nr='200067485' OR
order_nr='200062275' OR
order_nr='200014455' OR
order_nr='200042455' OR
order_nr='200037735' OR
order_nr='200026335' OR
order_nr='200079335' OR
order_nr='200078335' OR
order_nr='200091641' OR
order_nr='200053641' OR
order_nr='200068541' OR
order_nr='200052141' OR
order_nr='200024341' OR
order_nr='200061821' OR
order_nr='200052821' OR
order_nr='200038961' OR
order_nr='200041521' OR
order_nr='200058321' OR
order_nr='200052261' OR
order_nr='200025661' OR
order_nr='200059961' OR
order_nr='200048961' OR
order_nr='200093961' OR
order_nr='200062861' OR
order_nr='200069561' OR
order_nr='200055561' OR
order_nr='200013561' OR
order_nr='200033561' OR
order_nr='200053161' OR
order_nr='200011161' OR
order_nr='200035361' OR
order_nr='200019491' OR
order_nr='200033891' OR
order_nr='200055681' OR
order_nr='200045981' OR
order_nr='200059781' OR
order_nr='200078781' OR
order_nr='200058581' OR
order_nr='200038371' OR
order_nr='200053371' OR
order_nr='200083451' OR
order_nr='200044251' OR
order_nr='200042251' OR
order_nr='200014651' OR
order_nr='200036851' OR
order_nr='200026951' OR
order_nr='200085851' OR
order_nr='200039751' OR
order_nr='200091751' OR
order_nr='200022551' OR
order_nr='200091151' OR
order_nr='200069351' OR
order_nr='200049411' OR
order_nr='200034211' OR
order_nr='200049211' OR
order_nr='200039611' OR
order_nr='200045811' OR
order_nr='200095811' OR
order_nr='200091811' OR
order_nr='200022711' OR
order_nr='200023111' OR
order_nr='200021711' OR
order_nr='200096231' OR
order_nr='200069931' OR
order_nr='200056731' OR
order_nr='200043731' OR
order_nr='200044631' OR
order_nr='200023443' OR
order_nr='200062943' OR
order_nr='200087843' OR
order_nr='200017963' OR
order_nr='200097863' OR
order_nr='200084763' OR
order_nr='200024563' OR
order_nr='200082693' OR
order_nr='200081693' OR
order_nr='200019993' OR
order_nr='200055993' OR
order_nr='200081993' OR
order_nr='200039893' OR
order_nr='200049793' OR
order_nr='200044593' OR
order_nr='200062593' OR
order_nr='200087683' OR
order_nr='200023683' OR
order_nr='200086783' OR
order_nr='200056783' OR
order_nr='200069783' OR
order_nr='200039783' OR
order_nr='200071783' OR
order_nr='200066583' OR
order_nr='200062583' OR
order_nr='200057583' OR
order_nr='200099883' OR
order_nr='200092473' OR
order_nr='200068673' OR
order_nr='200067673' OR
order_nr='200043673' OR
order_nr='200049973' OR
order_nr='200064873' OR
order_nr='200027923' OR
order_nr='200053373' OR
order_nr='200016453' OR
order_nr='200023333' OR
order_nr='200584442' OR
order_nr='200382442' OR
order_nr='200465442' OR
order_nr='200776442' OR
order_nr='200831442' OR
order_nr='200614242' OR
order_nr='200244242' OR
order_nr='200127242' OR
order_nr='200937242' OR
order_nr='200617242' OR
order_nr='200965242' OR
order_nr='200663242' OR
order_nr='200255242' OR
order_nr='200226642' OR
order_nr='200287642' OR
order_nr='200326642' OR
order_nr='2000649642' OR
order_nr='200937642' OR
order_nr='200559842' OR
order_nr='200857842' OR
order_nr='200614742' OR
order_nr='200514742' OR
order_nr='200742742' OR
order_nr='200233742' OR
order_nr='200143542' OR
order_nr='200993542' OR
order_nr='200719542' OR
order_nr='200339142' OR
order_nr='200352342' OR
order_nr='200659422' OR
order_nr='200968462' OR
order_nr='200384962' OR
order_nr='200546662' OR
order_nr='200313362' OR
order_nr='200921292' OR
order_nr='200891292' OR
order_nr='200743292' OR
order_nr='200699692' OR
order_nr='200635692' OR
order_nr='200781692' OR
order_nr='200195592' OR
order_nr='200185592' OR
order_nr='200787192' OR 
order_nr='200225192' OR 
order_nr='200666472' OR 
order_nr='200197472' OR 
order_nr='200277472' OR 
order_nr='200296872' OR 
order_nr='200367872' OR 
order_nr='200941872' OR
order_nr='200219772' OR
order_nr='200548772' OR
order_nr='200273772' OR
order_nr='200739172' OR
order_nr='200855172' OR
order_nr='200915372' OR
order_nr='200841372' OR
order_nr='200734952') AND (channel is null OR channel='');




UPDATE tbl_order_detail SET channel='Banco de Bogotá' WHERE sku like 'PA953EL96NHFLACOL%' AND  
date>='2012-11-26' and date<='2012-12-17' AND paid_price='299900' AND coupon_code is null;
UPDATE tbl_order_detail SET channel='Banco de Bogotá' WHERE sku like 'BH859EL51WBWLACOL%' AND  date>='2012-11-26' and date<='2012-12-17' AND paid_price='989900' AND coupon_code is null;
UPDATE tbl_order_detail SET channel='Banco de Bogotá' WHERE sku like 'CU054HL58AXPLACOL%' AND  date>='2012-11-26' and date<='2012-12-17' AND paid_price='299900' AND coupon_code is null;
UPDATE tbl_order_detail SET channel='Banco de Bogotá' WHERE sku='SA860TB24WCXLACOL-80971' AND  date>='2012-11-26' and date<='2012-12-17' AND paid_price='104900' AND coupon_code is null;
UPDATE tbl_order_detail SET channel='Banco de Bogotá' WHERE sku='HA997TB17DLQLACOL-37681' AND  date>='2012-11-26' and date<='2012-12-17' AND paid_price='94900' AND coupon_code is null;
UPDATE tbl_order_detail SET channel='Banco de Bogotá' WHERE sku='SA860TB35WCMLACOL-80960' AND  date>='2012-11-26' and date<='2012-12-17' AND paid_price='139900' AND coupon_code is null;
UPDATE tbl_order_detail SET channel='Banco de Bogotá' WHERE sku='SA860TB37WCKLACOL-80958' AND  date>='2012-11-26' and date<='2012-12-17' AND paid_price='69900' AND coupon_code is null;
UPDATE tbl_order_detail SET channel='Banco de Bogotá' WHERE sku like 'BO944TB20JEPLACOL%' AND  date>='2012-11-26' and date<='2012-12-17' AND paid_price='97900' AND coupon_code is null;
UPDATE tbl_order_detail SET channel='Banco de Bogotá' WHERE sku='MG614TB23HTWLACOL-61488' AND  date>='2012-11-26' and date<='2012-12-17' AND paid_price='79900' AND coupon_code is null;
UPDATE tbl_order_detail SET channel='Banco de Bogotá' WHERE sku like 'BA186TB36HTJLACOL%' AND  date>='2012-11-26' and date<='2012-12-17' AND paid_price='179900' AND coupon_code is null;
UPDATE tbl_order_detail SET channel='Banco de Bogotá' WHERE sku like 'PR506TB63BFCLACOL%' AND  date>='2012-11-26' and date<='2012-12-17' AND paid_price='549900' AND coupon_code is null;
UPDATE tbl_order_detail SET channel='Banco de Bogotá' WHERE sku like 'MI392TB32VRBLACOL%' AND  date>='2012-11-26' and date<='2012-12-17' AND paid_price='149900' AND coupon_code is null;
UPDATE tbl_order_detail SET channel='Banco de Bogotá' WHERE sku like 'MI392TB33VRALACOL%' AND  date>='2012-11-26' and date<='2012-12-17' AND paid_price='139900' AND coupon_code is null;
UPDATE tbl_order_detail SET channel='Banco de Bogotá' WHERE sku like 'BO944TB79JCILACOL%' AND  date>='2012-11-26' and date<='2012-12-17' AND paid_price='89900' AND coupon_code is null;
UPDATE tbl_order_detail SET channel='Banco de Bogotá' WHERE sku='BO944TB24JELLACOL-23774' AND  date>='2012-11-26' and date<='2012-12-17' AND paid_price='149900' AND coupon_code is null;
UPDATE tbl_order_detail SET channel='Banco de Bogotá' WHERE sku='HA997TB23DLKLACOL-37675' AND  date>='2012-11-26' and date<='2012-12-17' AND paid_price='94900' AND coupon_code is null;
UPDATE tbl_order_detail SET channel='Banco de Bogotá' WHERE order_nr='200995142' or order_nr='200971142' 
or order_nr='200895142' or order_nr='200781142' or order_nr='200695142' or order_nr='200555142' or order_nr='200453142'
 or order_nr='200271142' or order_nr='200245142';

#Recuperacion partnership


update tbl_order_detail set channel='Recuperación - Partnership' WHERE
order_nr='200387876' OR 
order_nr='200626686' OR
order_nr='200387876' OR
order_nr='200754196' OR
order_nr='200938396' OR
order_nr='200915166' OR
order_nr='200281996' OR
order_nr='200242486' or source like 'REODOCK%';




update tbl_order_detail set channel='Otros - Mercado Libre' WHERE order_nr='200016188' 
OR order_nr='200046759' OR order_nr='200046759' OR order_nr='200076448' OR 
order_nr='200077719' OR order_nr='200046519' OR order_nr='200016497' OR order_nr='200082525' OR order_nr='200068765' OR order_nr='200072317' OR
order_nr='200057225' OR order_nr='200017967' OR order_nr='200056697' OR order_nr='200047697'
OR order_nr='200081195' OR order_nr='200051195' OR order_nr='200088578' OR order_nr='200078317'
OR order_nr='200053298' OR 
order_nr='200017717' OR
order_nr='200016757' OR order_nr='200053625' OR order_nr='200089185' OR
order_nr='200087561' OR
order_nr='200056561' OR
order_nr='200019561' OR
order_nr='200038561' OR
order_nr='200554462' OR
order_nr='200511792' OR
order_nr= '200424292' OR
order_nr= '200846792' OR
order_nr= '200971292' OR order_nr='200283182' OR order_nr='200165272' OR order_nr='200177812' OR 
order_nr='200264526' OR order_nr='200296146' OR order_nr='200578432'
OR order_nr='200578432'
OR order_nr='200257166'
OR order_nr='200811276'
OR order_nr='200534676'
;



UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CO224EL19ZJSLACOL%' AND paid_price='249900' AND date>='2013-02-01' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CH964EL45KJILACOL%' AND paid_price='299900' AND date>='2013-02-01' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'HY223EL31VLKLACOL%' AND paid_price='599900' AND date>='2013-02-03' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PA094EL36NLNLACOL%' AND paid_price='199900' AND date>='2013-02-03' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PE976OS21FWWLACOL%' AND paid_price='69900' AND date>='2013-02-03' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'TR606HL42POZLACOL%' AND paid_price='9900' AND date>='2013-02-03' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'EP007EL02GQZLACOL%' AND paid_price='299900' AND date>='2013-02-04' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'TO018EL79AEMLACOL%' AND paid_price='899900' AND date>='2013-02-04' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'NI089EL22EOFLACOL%' AND paid_price='999000' AND date>='2013-02-04' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'NI089EL22EOFLACOL%' AND paid_price='899900' AND date>='2013-02-04' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'XB125EL42ZORLACOL%' AND paid_price='109900' AND date>='2013-02-04' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PL098EL61WJELACOL%' AND paid_price='899000' AND date>='2013-02-04' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CA259EL40JSDLACOL%' AND paid_price='159500' AND date>='2013-02-04' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'NA528EL66JUZLACOL%' AND paid_price='159500' AND date>='2013-02-04' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CO224EL19ZJSLACOL%' AND paid_price='249900' AND date>='2013-02-05' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA197EL88ZKXLACOL%' AND paid_price='549900' AND date>='2013-02-05' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'JE859EL74IFLLACOL%' AND paid_price='79900' AND date>='2013-02-05' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'XB125EL61ANYLACOL%' AND paid_price='449900' AND date>='2013-02-05' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'KA778EL89PFMLACOL%' AND paid_price='33000' AND date>='2013-02-05' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'ZA876EL32XWTLACOL%' AND paid_price='52500' AND date>='2013-02-05' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'EP007EL02GQZLACOL%' AND paid_price='199900' AND date>='2013-02-05' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'RT613HL03TEULACOL%' AND paid_price='69900' AND date>='2013-02-05' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'MA390TB01PTMLACOL%' AND paid_price='179900' AND date>='2013-02-05' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'DR937TB27IDKLACOL%' AND paid_price='109900' AND date>='2013-02-05' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'TI235EL67EKQLACOL%' AND paid_price='229900' AND date>='2013-02-05' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'MI085EL06ISNLACOL%' AND paid_price='99900' AND date>='2013-02-05' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'IN940TB76IJFLACOL%' AND paid_price='329900' AND date>='2013-02-05' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SO621HL91VSQLACOL%' AND paid_price='299900' AND date>='2013-02-05' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'TI235EL39YEELACOL%' AND paid_price='189900' AND date>='2013-02-05' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'TO018EL79AEMLACOL%' AND paid_price='699900' AND date>='2013-02-05' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PA953EL35JLSLACOL%' AND paid_price='69900' AND date>='2013-02-05' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'VI271EL78AMFLACOL%' AND paid_price='99900' AND date>='2013-02-05' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CO224EL19ZJSLACOL%' AND paid_price='249900' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'ZA876EL32XWTLACOL%' AND paid_price='52500' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'VI271EL78AMFLACOL%' AND paid_price='99900' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'EL780HL01ISSLACOL%' AND paid_price='599900' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'DR937TB27IDKLACOL%' AND paid_price='109900' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'KA778EL89PFMLACOL%' AND paid_price='33000' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'ZA876EL32XWTLACOL%' AND paid_price='52500' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SO621HL91VSQLACOL%' AND paid_price='299900' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'TI235EL39YEELACOL%' AND paid_price='189900' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'RT613HL03TEULACOL%' AND paid_price='69900' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BE996EL29XPELACOL%' AND paid_price='149900' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'EP007EL02GQZLACOL%' AND paid_price='199900' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'TO018EL79AEMLACOL%' AND paid_price='699900' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'XB125EL61ANYLACOL%' AND paid_price='489900' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'ME248EL35VWULACOL%' AND paid_price='9900' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PR862EL69BGWLACOL%' AND paid_price='249900' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'TI235EL67EKQLACOL%' AND paid_price='229900' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'EL780HL01ISSLACOL%' AND paid_price='599900' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BE996EL29XPELACOL%' AND paid_price='149900' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'MA390TB01PTMLACOL%' AND paid_price='179900' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'DR937TB27IDKLACOL%' AND paid_price='109900' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'IN940TB76IJFLACOL%' AND paid_price='329900' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'DR922EL51HQYLACOL%' AND paid_price='25900' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BE508TB65JYWLACOL%' AND paid_price='399900' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BE508TB64JYXLACOL%' AND paid_price='399900' AND date>='2013-02-06' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'MI392EL73GRALACOL%' AND paid_price='399900' AND date>='2013-02-07' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'ZO801TB29AGKLACOL%' AND paid_price='9990000' AND date>='2013-02-07' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'ZO801TB34AGFLACOL%' AND paid_price='2990000' AND date>='2013-02-07' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BE996EL29XPELACOL%' AND paid_price='14990000' AND date>='2013-02-07' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'VI271EL78AMFLACOL%' AND paid_price='9990000' AND date>='2013-02-07' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'HY223EL31VLKLACOL%' AND paid_price='59990000' AND date>='2013-02-07' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PR862EL69BGWLACOL%' AND paid_price='24990000' AND date>='2013-02-07' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'TI235EL39YEELACOL%' AND paid_price='18990000' AND date>='2013-02-07' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'GE138EL75AMILACOL%' AND paid_price='9990000' AND date>='2013-02-07' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CO052EL47DFSLACOL%' AND paid_price='5990000' AND date>='2013-02-07' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'IM053HL78ITPLACOL%' AND paid_price='9990000' AND date>='2013-02-07' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'DR937TB27IDKLACOL%' AND paid_price='10990000' AND date>='2013-02-07' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CO224EL19ZJSLACOL%' AND paid_price='24990000' AND date>='2013-02-07' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'RT613HL03TEULACOL%' AND paid_price='69900' AND date>='2013-02-07' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'EL780HL01ISSLACOL%' AND paid_price='599900' AND date>='2013-02-07' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA197EL88ZKXLACOL%' AND paid_price='549900' AND date>='2013-02-07' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PR862EL69BGWLACOL%' AND paid_price='249900' AND date>='2013-02-08' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'EP007EL02GQZLACOL%' AND paid_price='2499000' AND date>='2013-02-08' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA197EL88ZKXLACOL%' AND paid_price='1999000' AND date>='2013-02-08' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'VI271EL78AMFLACOL%' AND paid_price='5499000' AND date>='2013-02-08' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PR862EL69BGWLACOL%' AND paid_price='999000' AND date>='2013-02-08' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'HY223EL31VLKLACOL%' AND paid_price='2499000' AND date>='2013-02-08' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'TI235EL39YEELACOL%' AND paid_price='5999000' AND date>='2013-02-08' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'XB125EL61ANYLACOL%' AND paid_price='1899000' AND date>='2013-02-08' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PA094EL36NLNLACOL%' AND paid_price='4899000' AND date>='2013-02-08' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CO052EL47DFSLACOL%' AND paid_price='1999000' AND date>='2013-02-08' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'LG082EL26NQRLACOL%' AND paid_price='599000' AND date>='2013-02-08' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'RT613HL03TEULACOL%' AND paid_price='7699000' AND date>='2013-02-08' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PA094EL36NLNLACOL%' AND paid_price='699000' AND date>='2013-02-08' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'XB125EL42ZORLACOL%' AND paid_price='1999000' AND date>='2013-02-08' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'QB234EL07WHKLACOL%' AND paid_price='1099000' AND date>='2013-02-08' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SO017EL93VKWLACOL%' AND paid_price='1899000' AND date>='2013-02-08' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PL098EL61WJELACOL%' AND paid_price='789000' AND date>='2013-02-08' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'LG082EL26NQRLACOL%' AND paid_price='769900' AND date>='2013-02-08' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PR862EL69BGWLACOL%' AND paid_price='249900' AND date>='2013-02-08' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA197EL88ZKXLACOL%' AND paid_price='549900' AND date>='2013-02-08' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'QB234EL07WHKLACOL%' AND paid_price='189000' AND date>='2013-02-09' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'MI392EL73GRALACOL%' AND paid_price='399900' AND date>='2013-02-09' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'EP007EL02GQZLACOL%' AND paid_price='199000' AND date>='2013-02-09' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA197EL88ZKXLACOL%' AND paid_price='549900' AND date>='2013-02-09' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PR862EL69BGWLACOL%' AND paid_price='249900' AND date>='2013-02-09' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'EL051HL74KKBLACOL%' AND paid_price='599900' AND date>='2013-02-09' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'HY223EL31VLKLACOL%' AND paid_price='599900' AND date>='2013-02-09' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'TI235EL39YEELACOL%' AND paid_price='189900' AND date>='2013-02-09' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'XB125EL61ANYLACOL%' AND paid_price='489900' AND date>='2013-02-09' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PA094EL36NLNLACOL%' AND paid_price='199900' AND date>='2013-02-09' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CO052EL47DFSLACOL%' AND paid_price='59900' AND date>='2013-02-09' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'LG082EL26NQRLACOL%' AND paid_price='769900' AND date>='2013-02-09' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'RT613HL03TEULACOL%' AND paid_price='69900' AND date>='2013-02-09' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'VI271EL78AMFLACOL%' AND paid_price='99900' AND date>='2013-02-09' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'MI085EL06ISNLACOL%' AND paid_price='139900' AND date>='2013-02-09' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'XB125EL42ZORLACOL%' AND paid_price='109900' AND date>='2013-02-09' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SO017EL93VKWLACOL%' AND paid_price='789000' AND date>='2013-02-09' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PL098EL61WJELACOL%' AND paid_price='789000' AND date>='2013-02-09' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'OD666FA38RQZLACOL%' AND paid_price='153000' AND date>='2013-02-10' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BR683FA76RLRLACOL%' AND paid_price='66400' AND date>='2013-02-10' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CA996FA38FGVLACOL%' AND paid_price='285900' AND date>='2013-02-10' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'JE714HB84OMLLACOL%' AND paid_price='89900' AND date>='2013-02-10' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'QB234EL07WHKLACOL%' AND paid_price='189900' AND date>='2013-02-10' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA015EL82UYVLACOL%' AND paid_price='899900' AND date>='2013-02-10' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BR683FA48RMTLACOL%' AND paid_price='36800' AND date>='2013-02-10' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CA996FA74FFLLACOL%' AND paid_price='278900' AND date>='2013-02-10' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CA748HB00TYDLACOL%' AND paid_price='129900' AND date>='2013-02-10' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'VI271EL78AMFLACOL%' AND paid_price='99900' AND date>='2013-02-10' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'EL051HL74KKBLACOL%' AND paid_price='599900' AND date>='2013-02-10' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'OD666FA62RQBLACOL%' AND paid_price='153000' AND date>='2013-02-10' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'DA669HL23NGELACOL%' AND paid_price='4900' AND date>='2013-02-10' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CO052EL74AMJLACOL%' AND paid_price='59900' AND date>='2013-02-11' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BL041EL11SJKLACOL%' AND paid_price='359900' AND date>='2013-02-11' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'OD666FA33NBYLACOL%' AND paid_price='194900' AND date>='2013-02-11' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA015EL71GVWLACOL%' AND paid_price='1099900' AND date>='2013-02-11' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BL041EL11SJKLACOL%' AND paid_price='359900' AND date>='2013-02-11' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA197EL88ZKXLACOL%' AND paid_price='549900' AND date>='2013-02-11' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'TI235EL39YEELACOL%' AND paid_price='189900' AND date>='2013-02-11' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'XB125EL61ANYLACOL%' AND paid_price='489900' AND date>='2013-02-11' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'AR842EL87LLOLACOL%' AND paid_price='399900' AND date>='2013-02-11' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PH097EL36DUPLACOL%' AND paid_price='89900' AND date>='2013-02-11' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'HO098EL22BBXLACOL%' AND paid_price='127000' AND date>='2013-02-11' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'LG082EL26NQRLACOL%' AND paid_price='769900' AND date>='2013-02-11' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'RT613HL03TEULACOL%' AND paid_price='69900' AND date>='2013-02-11' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'VI271EL78AMFLACOL%' AND paid_price='99900' AND date>='2013-02-11' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'NI089EL41ENMLACOL%' AND paid_price='359900' AND date>='2013-02-11' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'MI085EL06ISNLACOL%' AND paid_price='139900' AND date>='2013-02-11' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'LG082EL97RQSLACOL%' AND paid_price='1199900' AND date>='2013-02-11' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SO621HL91VSQLACOL%' AND paid_price='299900' AND date>='2013-02-11' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'EL051HL74KKBLACOL%' AND paid_price='599900' AND date>='2013-02-11' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BL041EL11SJKLACOL%' AND paid_price='359900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA015EL71GVWLACOL%' AND paid_price='1099900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'TI235EL67EKQLACOL%' AND paid_price='229900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'TI235EL39YEELACOL%' AND paid_price='189900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PR862EL69BGWLACOL%' AND paid_price='249900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'XB125EL61ANYLACOL%' AND paid_price='489900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'VI271EL53KOSLACOL%' AND paid_price='99900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'AR842EL87LLOLACOL%' AND paid_price='399900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PH097EL36DUPLACOL%' AND paid_price='89900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'HO098EL68IQDLACOL%' AND paid_price='149900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'LG082EL26NQRLACOL%' AND paid_price='769900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'RT613HL03TEULACOL%' AND paid_price='69900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SO017EL49NAKLACOL%' AND paid_price='329000' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'NI089EL41ENMLACOL%' AND paid_price='359900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'MI085EL06ISNLACOL%' AND paid_price='139900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'LG082EL97RQSLACOL%' AND paid_price='1199900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SO621HL91VSQLACOL%' AND paid_price='299900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'EL051HL74KKBLACOL%' AND paid_price='599900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BL041EL11SJKLACOL%' AND paid_price='359900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'OD666FA33NBYLACOL%' AND paid_price='194900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA015EL71GVWLACOL%' AND paid_price='1099900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'AR842EL87LLOLACOL%' AND paid_price='399900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'XB125EL61ANYLACOL%' AND paid_price='489900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BE996EL29XPELACOL%' AND paid_price='149900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SO017EL14ZQRLACOL%' AND paid_price='639900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CA705HB43OKELACOL%' AND paid_price='129900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'JE714HB84OMLLACOL%' AND paid_price='89900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'OD666FA62RQBLACOL%' AND paid_price='153000' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'OD666FA38RQZLACOL%' AND paid_price='153000' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CA996FA74FFLLACOL%' AND paid_price='278900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CA996FA38FGVLACOL%' AND paid_price='285900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BR683FA48RMTLACOL%' AND paid_price='36800' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BR683FA76RLRLACOL%' AND paid_price='66400' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SO017EL49NAKLACOL%' AND paid_price='329000' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA015EL82UYVLACOL%' AND paid_price='899900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'EL051HL74KKBLACOL%' AND paid_price='599900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'ME248EL35VWULACOL%' AND paid_price='9900' AND date>='2013-02-12' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'AR842EL87LLOLACOL%' AND paid_price='399900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BE996EL29XPELACOL%' AND paid_price='149900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SO621HL86VSVLACOL%' AND paid_price='299900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BR683FA48RMTLACOL%' AND paid_price='36800' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'EL051HL74KKBLACOL%' AND paid_price='599900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CA705HB43OKELACOL%' AND paid_price='129900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'OL092EL06BFLLACOL%' AND paid_price='199000' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'EL780HL52PSLLACOL%' AND paid_price='499900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'VI271EL53KOSLACOL%' AND paid_price='99900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CU707HB51OJWLACOL%' AND paid_price='24900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'EL052HL82ITLLACOL%' AND paid_price='799900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BL041EL11SJKLACOL%' AND paid_price='359900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA015EL71GVWLACOL%' AND paid_price='1099900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CO224EL19ZJSLACOL%' AND paid_price='249900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'HE009EL05ILYLACOL%' AND paid_price='119900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BR683FA76RLRLACOL%' AND paid_price='66400' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'QB234EL07WHKLACOL%' AND paid_price='189900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA015EL71GVWLACOL%' AND paid_price='1099900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PR862EL69BGWLACOL%' AND paid_price='249900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'NI089EL41ENMLACOL%' AND paid_price='359900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'TI235EL67EKQLACOL%' AND paid_price='229900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'NI088EL08CKDLACOL%' AND paid_price='1279900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'LG082EL64ECDLACOL%' AND paid_price='599900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'LG082EL97RQSLACOL%' AND paid_price='1199900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CH964EL45KJILACOL%' AND paid_price='299900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'EL780HL52PSLLACOL%' AND paid_price='499900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BE996EL05ISOLACOL%' AND paid_price='599000' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'RT613HL87HVGLACOL%' AND paid_price='99900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'HO098EL69IQCLACOL%' AND paid_price='259900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CE048EL37FMSLACOL%' AND paid_price='733000' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PL098EL61WJELACOL%' AND paid_price='899000' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA015EL71GVWLACOL%' AND paid_price='1099900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'QB234EL07WHKLACOL%' AND paid_price='189900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BL041EL11SJKLACOL%' AND paid_price='359900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'EL052HL84ITJLACOL%' AND paid_price='699900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BE996EL29XPELACOL%' AND paid_price='149900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CO224EL19ZJSLACOL%' AND paid_price='249900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'LG082EL64ECDLACOL%' AND paid_price='599900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'JE859EL29YASLACOL%' AND paid_price='24900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'GE138EL03HVSLACOL%' AND paid_price='59900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'HO098EL21BBYLACOL%' AND paid_price='54900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA015EL32FQTLACOL%' AND paid_price='1199900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'RT613HL87HVGLACOL%' AND paid_price='99900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'HA235EL58ZOBLACOL%' AND paid_price='59900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'OL092EL06BFLLACOL%' AND paid_price='199900' AND date>='2013-02-13' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'LG082EL64ECDLACOL%' AND paid_price='599900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'EL052HL84ITJLACOL%' AND paid_price='699900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'VI271EL53KOSLACOL%' AND paid_price='99900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BL041EL11SJKLACOL%' AND paid_price='359900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA015EL71GVWLACOL%' AND paid_price='1099900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'LG082EL64ECDLACOL%' AND paid_price='599900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'EL052HL84ITJLACOL%' AND paid_price='699900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'VI271EL53KOSLACOL%' AND paid_price='99900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BL041EL11SJKLACOL%' AND paid_price='359900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA015EL71GVWLACOL%' AND paid_price='1099900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'DA669HL23NGELACOL%' AND paid_price='1900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'DR921TB97LLELACOL%' AND paid_price='19900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'MU935TB62HYFLACOL%' AND paid_price='49900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'OF556TB34YTTLACOL%' AND paid_price='34900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'OF556TB30YTXLACOL%' AND paid_price='29900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BE508TB65JYWLACOL%' AND paid_price='399900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'DR937TB33IDELACOL%' AND paid_price='199900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'EV923TB60YGHLACOL%' AND paid_price='199900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PR506TB42CSJLACOL%' AND paid_price='129900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'TI235EL39YEELACOL%' AND paid_price='189900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BE996EL29XPELACOL%' AND paid_price='149900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PA953EL96NHFLACOL%' AND paid_price='259900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PR862EL69BGWLACOL%' AND paid_price='249900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'LG082EL26NQRLACOL%' AND paid_price='769900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'LG082EL26NQRLACOL%' AND paid_price='769900' AND date>='2013-02-14' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PR862EL69BGWLACOL%' AND paid_price='249900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA015EL71GVWLACOL%' AND paid_price='1099900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'XB125EL61ANYLACOL%' AND paid_price='489900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'LG082EL81KVILACOL%' AND paid_price='999900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BL041EL10SJLLACOL%' AND paid_price='479900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'NI089EL41ENMLACOL%' AND paid_price='359900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'HE009EL05ILYLACOL%' AND paid_price='119900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'LG082EL97RQSLACOL%' AND paid_price='1099900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'VT210EL17ATMLACOL%' AND paid_price='79900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PA953EL35JLSLACOL%' AND paid_price='69900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA015EL26KEFLACOL%' AND paid_price='909900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'LG082EL26NQRLACOL%' AND paid_price='769900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'HO098EL68IQDLACOL%' AND paid_price='149900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'MA668FA88IPJLACOL%' AND paid_price='51800' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'VI271EL96HNFLACOL%' AND paid_price='89900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SP844EL42XZHLACOL%' AND paid_price='129900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'LG082EL56ECLLACOL%' AND paid_price='2598000' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'XB125EL61ANYLACOL%' AND paid_price='489900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BL041EL10SJLLACOL%' AND paid_price='479900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'EL052HL84ITJLACOL%' AND paid_price='699900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA015EL26KEFLACOL%' AND paid_price='909000' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'JE859EL29YASLACOL%' AND paid_price='24900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'DR937TB33IDELACOL%' AND paid_price='199900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'AL038EL44ASLLACOL%' AND paid_price='89900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'HO098EL21BBYLACOL%' AND paid_price='54900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SO621HL07OPKLACOL%' AND paid_price='499900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BR032EL49RDELACOL%' AND paid_price='218000' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CH964EL46KJHLACOL%' AND paid_price='349900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'MA668FA88IPJLACOL%' AND paid_price='51800' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'DR921TB97LLELACOL%' AND paid_price='19900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'DR937TB33IDELACOL%' AND paid_price='199900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA015EL71GVWLACOL%' AND paid_price='1099900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'QB234EL07WHKLACOL%' AND paid_price='189900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BL041EL11SJKLACOL%' AND paid_price='359900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'EL052HL84ITJLACOL%' AND paid_price='699900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BE996EL29XPELACOL%' AND paid_price='149900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CO224EL19ZJSLACOL%' AND paid_price='249900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'AR842EL87LLOLACOL%' AND paid_price='249900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'GE138EL03HVSLACOL%' AND paid_price='59900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'HO098EL21BBYLACOL%' AND paid_price='54900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA015EL32FQTLACOL%' AND paid_price='1199900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'RT613HL87HVGLACOL%' AND paid_price='99900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'HA235EL58ZOBLACOL%' AND paid_price='59900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'OL092EL06BFLLACOL%' AND paid_price='199900' AND date>='2013-02-15' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'TI235EL39YEELACOL%' AND paid_price='189900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PR862EL69BGWLACOL%' AND paid_price='249900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA015EL71GVWLACOL%' AND paid_price='1099900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BL041EL11SJKLACOL%' AND paid_price='359900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'AL284EL62BWNLACOL%' AND paid_price='69900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'LG082EL26NQRLACOL%' AND paid_price='769900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'NI088EL28CJJLACOL%' AND paid_price='449000' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'JE859EL29YASLACOL%' AND paid_price='24900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'GE138EL03HVSLACOL%' AND paid_price='59900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BE508TB65JYWLACOL%' AND paid_price='399900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BR931TB28ACPLACOL%' AND paid_price='79900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA106EL35VQYLACOL%' AND paid_price='104900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'DR921TB97LLELACOL%' AND paid_price='19900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'VI662HL34MUFLACOL%' AND paid_price='234000' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'IN216EL71IAQLACOL%' AND paid_price='32900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'TI235EL39YEELACOL%' AND paid_price='189900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PR862EL69BGWLACOL%' AND paid_price='249900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'VI271EL53KOSLACOL%' AND paid_price='99900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SO621HL86VSVLACOL%' AND paid_price='299900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'LG082EL64ECDLACOL%' AND paid_price='639900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'AL284EL62BWNLACOL%' AND paid_price='69900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'RT613HL03TEULACOL%' AND paid_price='69900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'NI088EL28CJJLACOL%' AND paid_price='449000' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'AR842EL87LLOLACOL%' AND paid_price='379900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CH964EL45KJILACOL%' AND paid_price='299900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BE996EL29XPELACOL%' AND paid_price='149900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'DR937TB33IDELACOL%' AND paid_price='199900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PA094EL36NLNLACOL%' AND paid_price='199900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'LA016HL92GNJLACOL%' AND paid_price='46900' AND date>='2013-02-16' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'TI235EL39YEELACOL%' AND paid_price='189900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA015EL71GVWLACOL%' AND paid_price='1099900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'JE859EL29YASLACOL%' AND paid_price='24900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'GE138EL26ATDLACOL%' AND paid_price='79900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'VI271EL53KOSLACOL%' AND paid_price='99900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CO052EL74AMJLACOL%' AND paid_price='59900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SO621HL91VSQLACOL%' AND paid_price='369900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'LG082EL26NQRLACOL%' AND paid_price='769900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BR931TB14KUBLACOL%' AND paid_price='79900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BE508TB65JYWLACOL%' AND paid_price='399900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BR931TB28ACPLACOL%' AND paid_price='79900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA106EL35VQYLACOL%' AND paid_price='104900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'DR921TB97LLELACOL%' AND paid_price='19900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'OF556TB33YTULACOL%' AND paid_price='34900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'OF556TB25YUCLACOL%' AND paid_price='29900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'TI235EL39YEELACOL%' AND paid_price='189900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SA015EL71GVWLACOL%' AND paid_price='1099900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'VI271EL53KOSLACOL%' AND paid_price='99900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'SO621HL86VSVLACOL%' AND paid_price='299900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'LG082EL64ECDLACOL%' AND paid_price='639900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'AL284EL62BWNLACOL%' AND paid_price='69900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'RT613HL03TEULACOL%' AND paid_price='69900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'RT613HL03TEULACOL%' AND paid_price='69900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'AR842EL87LLOLACOL%' AND paid_price='379900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CH964EL45KJILACOL%' AND paid_price='299900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BE996EL29XPELACOL%' AND paid_price='149900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BR931TB14KUBLACOL%' AND paid_price='79900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BR931TB28ACPLACOL%' AND paid_price='79900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'DR921TB97LLELACOL%' AND paid_price='19900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CU707HB51OJWLACOL%' AND paid_price='9900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'CU707HB50OJXLACOL%' AND paid_price='9900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'TI235EL39YEELACOL%' AND paid_price='189900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'BR032EL49RDELACOL%' AND paid_price='218000' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'PA094EL36NLNLACOL%' AND paid_price='199900' AND date>='2013-02-17' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'MU492EL27XPGLACOL%' AND paid_price='249900' AND date>='2013-02-18' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'LG082EL81KVILACOL%' AND paid_price='999900' AND date>='2013-02-18' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'MI392EL71GRCLACOL%' AND paid_price='179900' AND date>='2013-02-18' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'MO849TB51VAYLACOL%' AND paid_price='169900' AND date>='2013-02-18' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'MO849TB56VATLACOL%' AND paid_price='99900' AND date>='2013-02-18' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'MO849TB64VALLACOL%' AND paid_price='69900' AND date>='2013-02-18' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'MO849TB14UYNLACOL%' AND paid_price='49900' AND date>='2013-02-18' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'MO849TB63VEILACOL%' AND paid_price='69900' AND date>='2013-02-18' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'MO849TB44VFBLACOL%' AND paid_price='69900' AND date>='2013-02-18' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'MO849TB45VFALACOL%' AND paid_price='79900' AND date>='2013-02-18' AND date<='2013-02-31';
UPDATE tbl_order_detail SET channel='Newsletter' WHERE source='' AND sku like 'MO849TB96VGXLACOL%' AND paid_price='79900' AND date>='2013-02-18' AND date<='2013-02-31';

UPDATE tbl_order_detail SET channel='Newsletter' WHERE item='126154'
OR item='127088'
OR item='127089'
OR item='127272'
OR item='127654'
OR item='127833'
OR item='128630'
OR item='128631'
OR item='129392'
OR item='130515'
OR item='130946'
OR item='132629'
OR item='133038'
OR item='133155'
OR item='133195'
OR item='133196'
OR item='133197'
OR item='133227'
OR item='133228'
OR item='133229'
OR item='135149'
OR item='135326'
OR item='135387'
OR item='135496'
OR item='135594'
OR item='135625'
OR item='136078'
OR item='136141'
OR item='136494'
OR item='136549'
OR item='136766'
OR item='136776'
OR item='137301'
OR item='137517'
OR item='137590'
OR item='137591'
OR item='138163'
OR item='138822'
OR item='139091'
OR item='139127'
OR item='139291'
OR item='139292'
OR item='139568'
OR item='139805'
OR item='139835'
OR item='140711'
OR item='141184'
OR item='141346'
OR item='141350'
OR item='142255'
OR item='142664'
OR item='143030'
OR item='143121'
OR item='143844'
OR item='144016'
OR item='144033'
OR item='144631'
OR item='144648'
OR item='144649'
OR item='144778'
OR item='144789'
OR item='144811'
OR item='144815'
OR item='144817'
OR item='144986'
OR item='145038'
OR item='145140'
OR item='145256'
OR item='145265'
OR item='145270'
OR item='145272'
OR item='145274'
OR item='145762'
OR item='145828'
OR item='145920'
OR item='145999'
OR item='146050'
OR item='146085'
OR item='147028'
OR item='147539'
OR item='147688'
OR item='147839'
OR item='147926'
OR item='147989'
OR item='148359'
OR item='148587';



UPDATE tbl_order_detail SET channel='FB Posts' WHERE item='126256'
OR item='126466'
OR item='127080'
OR item='127113'
OR item='127269'
OR item='127940'
OR item='127956'
OR item='128021'
OR item='128087'
OR item='128424'
OR item='128498'
OR item='128538'
OR item='129124'
OR item='129473'
OR item='129778'
OR item='129894'
OR item='130695'
OR item='131174'
OR item='131770'
OR item='131845'
OR item='132250'
OR item='132426'
OR item='132682'
OR item='133091'
OR item='133104'
OR item='133691'
OR item='133721'
OR item='133788'
OR item='134409'
OR item='134540'
OR item='134559'
OR item='134611'
OR item='134996'
OR item='135697'
OR item='135975'
OR item='136671'
OR item='137365'
OR item='137505'
OR item='137516'
OR item='137876'
OR item='138122'
OR item='138267'
OR item='138824'
OR item='139481'
OR item='139783'
OR item='139838'
OR item='139968'
OR item='140384'
OR item='140609'
OR item='140663'
OR item='141208'
OR item='141212'
OR item='141930'
OR item='142461'
OR item='142577'
OR item='143624'
OR item='143915'
OR item='144017'
OR item='144111'
OR item='144634'
OR item='144635'
OR item='144638'
OR item='144641'
OR item='144645'
OR item='144732'
OR item='144772'
OR item='144776'
OR item='144820'
OR item='144821'
OR item='145030'
OR item='145031'
OR item='145040'
OR item='145127'
OR item='145195'
OR item='145269'
OR item='145728'
OR item='145794'
OR item='145935'
OR item='145946'
OR item='146002'
OR item='146454'
OR item='146918'
OR item='146919'
OR item='147080'
OR item='147100'
OR item='147445'
OR item='147500'
OR item='147822'
OR item='147838';





UPDATE tbl_order_detail SET channel='Unknown' WHERE source='';
UPDATE tbl_order_detail SET channel='Unknown-Voucher' WHERE channel='' AND (source<>''AND coupon_code is not null);
UPDATE tbl_order_detail SET channel='Unknown-GA Tracking' WHERE channel='' 
AND source<>''AND coupon_code is null and `source/medium` is not null;

#CAC Vouchers
UPDATE tbl_order_detail set channel = 'CAC Deals' where (coupon_code like 'CAC%' or campaign like '%CAC%') and campaign not in ('COL_fanpage_f_22-65_deportes_20130330BicicletaElipticaconSillaTwisteryMancuernas__', 'AngelicaCastro');

#Curebit
UPDATE tbl_order_detail set channel = 'Curebit' where coupon_code like '%cbit%' or `source/medium` like '%curebit%';

#Criteo
UPDATE tbl_order_detail set channel='Criteo' where source like '%criteo%';

#Corporate Deals
UPDATE tbl_order_detail set channel='Corporate Deals' where coupon_code LIKE 'CDEAL%' or source like '%CORPORATESALES%';

UPDATE tbl_order_detail t inner join mobileapp_transaction_id m on t.order_nr=m.transaction_id set channel = 'Mobile App';

SELECT 'Start: CHANNEL GROUPING', now();

#FIX OLD PARTNERHIPS TO OTHERS

UPDATE tbl_order_detail SET channel_group='Mobile App' WHERE channel='Mobile App';

UPDATE tbl_order_detail SET channel_group='Other (identified)' where channel='Libro Bogota';

UPDATE tbl_order_detail set channel_group ='Corporate Deals' where channel='Corporate Deals';

UPDATE tbl_order_detail SET channel_group='Facebook Ads' WHERE channel='Facebook Ads' OR channel='Alejandra Arce Voucher'
OR channel='FB Ads CAC Junio' OR channel='FB Ads CAC Mayo' or channel = 'Dark Post';

#Referal Platform
UPDATE tbl_order_detail set channel_group = 'Referal Platform' where channel='Curebit';

UPDATE tbl_order_detail SET channel_group='Search Engine Marketing' WHERE channel='SEM (unbranded)' or channel='Bing' or channel='Other Branded';


UPDATE tbl_order_detail SET channel_group='Social Media' WHERE channel='Twitter'
OR channel='FB Posts' OR channel='FB-Diana Call' OR channel='Muy Feliz' or channel='FB CAC' OR 
channel='Fashion Fan Page' OR channel='Linio Fan Page' or channel='Youtube';


UPDATE tbl_order_detail SET channel_group='Retargeting' WHERE channel='Sociomantic' OR
channel='Vizury' OR channel = 'VEInteractive' or channel = 'GDN Retargeting'
OR channel='Facebook Retargeting' or channel='Criteo' or channel='Main ADV' or channel='AdRoll' or channel='Other Retargeting' or channel='My Things' or channel='Trade Tracker' or channel='Triggit';


UPDATE tbl_order_detail SET channel_group='Google Display Network' WHERE channel='Google Display Network'; 


UPDATE tbl_order_detail SET channel_group='NewsLetter' WHERE channel='Newsletter'
OR channel='Voucher Navidad' OR channel='Voucher Fin de Año' 
OR channel='Bandeja CAC' OR channel='Tapete CAC' OR channel='Perfumes CAC'
OR channel='VoucherBebes-Feb' OR channel='Sartenes CAC' OR channel='Estufa CAC' OR channel='Vuelve CAC' OR
channel='Memoria CAC' OR
channel='Audifonos CAC' OR channel='Venir Reactivation' OR 
channel='Regresa Reactivation' OR 
channel='Bienvenido CAC' OR channel='Amigo CAC' OR channel='Aqui Reactivation' OR
channel='Tiramos la Casa por la ventana' OR channel='Creating Happiness' OR channel='Grita Reactivation' OR
channel='CACs Marzo' OR channel='CACs Abril' OR channel='CACs Mayo' OR channel='CACs Junio' OR channel='CACs Julio'
OR channel='Reactivación Mayo' OR channel='Fashion' OR channel='Reactivación Junio' OR channel='Reactivación Julio';


#CAC Deals Group
UPDATE tbl_order_detail set channel_group='CAC Deals' where channel='CAC Deals';


UPDATE tbl_order_detail SET channel_group='Search Engine Optimization' WHERE channel='Search Organic' or channel='Yahoo Answers';


UPDATE tbl_order_detail SET channel_group='Partnerships' WHERE
channel='Banco Helm' OR channel='Banco de Bogotá' OR channel='Banco Davivienda'
OR channel='Banco Davivienda' OR channel='Banco BBVA'
OR channel='Campus Party' OR 
 channel='El Espectador' OR channel='Grupon'
OR channel='Dafiti' OR channel='Sin IVA 16' OR channel='En Medio' OR channel='Bancolombia' OR channel='AMEX'
OR channel='Citi Bank' OR channel='Lenddo' OR channel='Unknown Partnership March' OR 
channel='Post BDB' OR channel='VISA' OR channel='Coomservi' OR channel='Recuperación - Partnership' or channel='Other Partnerships';


UPDATE tbl_order_detail SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';


UPDATE tbl_order_detail SET channel_group='Offline Marketing' WHERE 
channel='ADN' OR channel='Journalist Vouchers' OR channeL='Metro' OR
channel='TRANSMILENIO' OR channel='SMS SanValentin' OR channel='SuperDescuentos' OR
channel='Folletos' OR channel='MUJER' OR channel='DiaEspecial' OR channel='Hora Loca SMS' OR 
channel='Calle Vouchers' OR channel='SMS' OR channel='BBVADP' OR channel='Other PR' OR channel='Valla Abril'
OR channel='SMS Nueva pagina NC' OR channel='SMS Nueva pagina Costumers' OR channel='SMS Más temprano más barato' OR 
channel='Barranquilla' OR channel='Regresa (React.)' OR channel='Te Esperamos (React.)' OR channel='Gracias (Repurchase)'
OR channel='Falabella Customers' OR channel='Cali' OR channel='CC Avenida Chile' OR channel='Correo Bogota' 
OR channel='Feria Libro' OR channel='Correo directo bogota' OR channel='CEROIVA' OR channel='SMS Madres' or
channel='Circulo de la Moda' OR channel='SMS Mas Barato' or channel='Mailing 2' or channel='Publimetro' or
channel='Reactivation60 dias' or
channel='Reactivation120 dias' or
channel='Reactivation180 dias' or channel='Valla 2nd stage' or channel='Repurchase50' or
channel='Repurchase20' OR channel='Catalogo Junio';
 

UPDATE tbl_order_detail SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales' OR channel='Replace' OR channel='ReOrder' 
OR channel='Invalid-Crossell' OR channel='Unknown-CS' OR 
channel='CS-Insentivos' OR channel='CS-REORDER' OR channel='CS-CANCEL' or channel='Other Tele Sales';


UPDATE tbl_order_detail SET channel_group='Branded' WHERE channel='Linio.com Referral' 
OR channel='Directo / Typed' OR
channel='SEM Branded' OR channel='Liniofashion.com.co Referral';


UPDATE tbl_order_detail SET channel_group='Mercado Libre - Alamaula' 
WHERE channel='Mercado Libre' OR channel='Otros - Mercado Libre' or channel='Alamaula';


-- UPDATE tbl_order_detail SET channel_group='Blogs' WHERE 
-- channel='Blog Linio' OR channel='Blog Mujer' OR channel='Lector';


UPDATE tbl_order_detail SET channel_group='Affiliates' WHERE channel='Buscape' or 
channel='Affiliate Program' or channel='Other Affiliates' or channel='SoloCPM' or channel='PromoDescuentos';



UPDATE tbl_order_detail SET channel_group='Other (identified)' WHERE channel='Referral Sites' 
OR channel='Reactivation Letters' OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' 
OR channel='Referral Program' OR channel='Nueva Página' OR channel='Promesa Linio' OR channel='NO IVA Abril' OR
channel='Invitados VIP Círculo de la moda';



UPDATE tbl_order_detail SET 
channel_group='Non identified' WHERE channel='Unknown' OR channel='Unknown-Voucher' OR channel='Unknown-GA Tracking';




UPDATE tbl_order_detail SET channel_group='NewsLetter', channel='Tablets 12.12 NL' WHERE sku='TI235EL39YEELACOL-83806' AND date='2012-12-12' AND OAC='1' AND RETURNED='0' AND channel_group='Unknown' limit 169;
UPDATE tbl_order_detail SET channel_group='Search Engine Marketing', channel='Tablets 12.12 SEO' WHERE sku='TI235EL39YEELACOL-83806' AND date='2012-12-12' AND OAC='1' AND RETURNED='0' AND channel_group='Unknown' limit 8;
UPDATE tbl_order_detail SET channel_group='Social Media', channel='Tablets 12.12 SM' WHERE sku='TI235EL39YEELACOL-83806' AND date='2012-12-12' AND OAC='1' AND RETURNED='0' AND channel_group='Unknown' limit 8;
UPDATE tbl_order_detail SET channel_group='Mercado Libre', channel='Tablets 12.12 ML' WHERE sku='TI235EL39YEELACOL-83806' AND date='2012-12-12' AND OAC='1' AND RETURNED='0' AND channel_group='Unknown' limit 8;
UPDATE tbl_order_detail SET channel_group='Tele Sales', channel='Tablets 12.12 CS' WHERE sku='TI235EL39YEELACOL-83806' AND date='2012-12-12' AND OAC='1' AND RETURNED='0' AND channel_group='Unknown' limit 8;
-- UPDATE tbl_order_detail SET channel_group='Blogs', channel='Tablets 12.12 Blog' WHERE sku='TI235EL39YEELACOL-83806' AND date='2012-12-12' AND OAC='1' AND -- RETURNED='0' AND channel_group='Unknown' limit 8;


UPDATE tbl_order_detail SET channel_group='Affiliates' where channel='Pampa Network';






UPDATE tbl_order_detail set ChannelType='Marketing CO' 
where channel_group='NewsLetter' OR
channel_group='Social Media' OR
channel_group='Partnerships' OR
channel_group='Search Engine Optimization' OR
channel_group='Corporate Sales' OR
channel_group='Mercado Libre - Alamaula' OR
channel_group='Blogs' OR
channel_group='Branded' OR
channel_group='Other (identified)'
OR channel_group='Tele Sales' OR channel_group='Offline Marketing';


UPDATE tbl_order_detail set ChannelType='Marketing Regional'
WHERE channel_group='Facebook Ads' OR channel_group='Search Engine Marketing'
OR channel_group='Retargeting' OR
channel_group='Affiliates' or channel='Google Display Network' or channel='Facebook Referral';





UPDATE tbl_order_detail set Ownership='Vanessa Socha' where channel_group='Social Media' AND channel<>'Fashion Fan Page';
UPDATE tbl_order_detail set Ownership='Andrés Ponce' where channel='Fashion Fan Page';
UPDATE tbl_order_detail set Ownership='Carolina Lizarazo' where channel_group='NewsLetter';
UPDATE tbl_order_detail set Ownership='Paola & Emilia' where channel_group='Blogs';
UPDATE tbl_order_detail set Ownership='Diana Pantoja' where channel_group='Offline Marketing';
UPDATE tbl_order_detail set Ownership='Natalia Gomez/Cristhian Zambrano' where channel_group='Partnerships';
UPDATE tbl_order_detail set Ownership='Sindy' where channel_group='Mercado Libre';
UPDATE tbl_order_detail set Ownership='Tahani' where channel_group='Tele Sales';
UPDATE tbl_order_detail set Ownership='Paola & Fabián' where channel_group='Corporate Sales';


UPDATE tbl_order_detail SET costo_after_vat=12900 WHERE sku_config='CO052EL27VAYLACOL' and channel like '%CAC%';
UPDATE tbl_order_detail SET costo_after_vat=10000 WHERE sku_config='SO017EL23VSILACOL' and channel like '%CAC%';
UPDATE tbl_order_detail SET costo_after_vat=11200 WHERE sku_config='IN031HL52HXNLACOL' and channel like '%CAC%';
UPDATE tbl_order_detail SET costo_after_vat=16500 WHERE sku_config='CO224EL91ERKLACOL' and channel like '%CAC%';
UPDATE tbl_order_detail SET costo_after_vat=5167 WHERE sku_config='DA669HL23NGELACOL' and channel like '%CAC%';
UPDATE tbl_order_detail SET costo_after_vat=9470 WHERE sku_config='EK495HL72AXBLACOL' and channel like '%CAC%';
UPDATE tbl_order_detail SET costo_after_vat=11797 WHERE sku_config='CU707HB51OJWLACOL' and channel like '%CAC%';
UPDATE tbl_order_detail SET costo_after_vat=12850 WHERE sku_config='RO004OS89SYOLACOL' and channel like '%CAC%';
UPDATE tbl_order_detail SET costo_after_vat=12000 WHERE sku_config='TR606HL42POZLACOL' and channel like '%CAC%';
UPDATE tbl_order_detail SET costo_after_vat=10000 WHERE sku_config='SO017EL23VSILACOL' and channel like '%CAC%';
UPDATE tbl_order_detail SET costo_after_vat=12900 WHERE sku_config='CO052EL27VAYLACOL' and channel like '%CAC%';
UPDATE tbl_order_detail SET costo_after_vat=14900 WHERE sku_config='HA235EL74ZNLLACOL' and channel like '%CAC%';
UPDATE tbl_order_detail SET costo_after_vat=9000 WHERE sku_config='CR610EL90NARLACOL' and channel like '%CAC%';
UPDATE tbl_order_detail SET costo_after_vat=18710 WHERE sku_config='TR606HL45VQOLACOL' and channel like '%CAC%';



UPDATE tbl_order_detail SET costo_after_vat=560344 WHERE sku_config='SO017EL14ZQRLACOL' and channel_group='Mercado Libre' and yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=3193925 WHERE sku_config='LG082EL50ECRLACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=187425 WHERE sku_config='CH049EL57IFALACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=689000 WHERE sku_config='TO018EL78KVLLACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=1600000 WHERE sku_config='SA015EL56IBFLACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=1245110 WHERE sku_config='SA015EL57IBELACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=515000 WHERE sku_config='SA197EL17TXMLACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=255550 WHERE sku_config='SP844EL88VLBLACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=190900 WHERE sku_config='PR862EL69BGWLACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=395890 WHERE sku_config='PR862EL53MIWLACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=190000 WHERE sku_config='TI235EL77XYWLACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=839000 WHERE sku_config='SA015EL92HZVLACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=305000 WHERE sku_config='IV097EL84LKPLACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=212000 WHERE sku_config='TI235EL07EOULACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=150862 WHERE sku_config='FU059EL44LIHLACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=54000 WHERE sku_config='VI271EL96HNFLACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=235759 WHERE sku_config='CA006EL06BBPLACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=271552 WHERE sku_config='AI003EL78HMZLACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=814776 WHERE sku_config='CA006EL09BQWLACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=387853 WHERE sku_config='NI088EL98EWZLACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=162931 WHERE sku_config='SA015EL62NNHLACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=283207 WHERE sku_config='CA006EL58GHDLACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=106000 WHERE sku_config='BE996EL29XPELACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=142000 WHERE sku_config='OL092EL07BFKLACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=200000 WHERE sku_config='SA015EL58LHTLACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=440343 WHERE sku_config='XB125EL92SQTLACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=440343 WHERE sku_config='MI085EL78PVHLACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=891301 WHERE sku_config='XB125EL44ZOPLACOL' and channel_group='Mercado Libre' AND yrmonth=201305;
UPDATE tbl_order_detail SET costo_after_vat=398000 WHERE sku_config='XB125EL61ANYLACOL' and channel_group='Mercado Libre' AND yrmonth=201305;

UPDATE tbl_order_detail set WH=0 where channel_group like '%Corporate Sales%';
UPDATE tbl_order_detail set CS=0 where channel_group like '%Corporate Sales%';


UPDATE tbl_order_detail SET costo_after_vat='1145000' where 
(sku='SA015EL52XDRLACOL-116112' OR sku='SA015EL51XDSLACOL-116113') and channel_group='Partnerships';
UPDATE tbl_order_detail SET costo_after_vat='1390000' 
where sku='HP071EL30YYRLACOL-118003'  and channel_group='Partnerships';



END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `channel_report_no_voucher`()
BEGIN

#channel Report---
#NEW REGISTERS
call cost_campaign();

#ga_cost_Campaign
select @last_date:=max(date) from ga_cost_campaign where source = 'vizury';

insert into ga_cost_campaign(date, source, medium, campaign, adCost)
SELECT date, "Vizury" as source, "Retargeting" as medium, "(not set)" as campaign, 
(sum(paid_price_after_vat) +sum(shipping_fee_after_vat) )*0.08 as cost
from tbl_order_detail
where `source/medium` like 'vizury /%' and obc = 1 and date > @last_date
group by date
order by date desc;

select @last_date:=max(date) from ga_cost_campaign where source like 'VEInteractive%';

SELECT date, "VEInteractive" as source, "Retargeting" as medium, "(not set)" as campaign, (sum(paid_price_after_vat) +sum(shipping_fee_after_vat) )*0.085 as cost
from tbl_order_detail
where `source/medium` like 'VEInteractive%' and obc = 1 and date > @last_date
group by date
order by date desc;


update ga_visits_cost_source_medium set source_medium = concat( source, ' / ', medium) where source_medium is null;
update ga_cost_campaign set source_medium = concat( source, ' / ', medium) where source_medium is null;


#REFERRAL
UPDATE ga_visits_cost_source_medium SET channel='Referral Sites' WHERE source_medium like '%referral' AND source_medium<>'linio.com / referral' AND source_medium<>'info.linio.com.co / referral' AND source_medium<>'facebook.com / referral' 
AND source_medium<>'t.co / referral' AND source_medium<>'mercadolibre.com.co / referral' AND source_medium<>'google.com.co / referral' AND source_medium<>'blog.linio.com.co / referral' AND source_medium<>'mujer.linio.com.co / referral';
#BUSCAPE
UPDATE ga_visits_cost_source_medium SET channel='Buscape' WHERE source_medium='Buscape / Price Comparison';
#FB ADS
UPDATE ga_visits_cost_source_medium SET channel='Facebook Ads' WHERE source_medium='facebook / socialmediaads' AND campaign not like 'COL%fan%';
update ga_visits_cost_source_medium
set channel = 'Facebook Ads', channel_group = 'Facebook Ads' 
where (source like '%book%' or source like '%face%') 
and ((campaign <> '' and campaign not like '%fan%' and campaign like 'COL%') or medium like '%ads%');
update ga_visits_cost_source_medium
set channel = 'Facebook Ads', channel_group = 'Facebook Ads' 
where (source like '%socialmediaads%') ;

#SEM - GDN
UPDATE ga_visits_cost_source_medium 
SET channel='SEM Branded' WHERE source like '%google%' and medium like '%cpc%' 
AND (campaign like 'brand%');
UPDATE ga_visits_cost_source_medium SET channel='SEM (unbranded)' 
WHERE source like '%google%' and medium like '%cpc%'  AND (campaign not like 'brand%');
UPDATE ga_visits_cost_source_medium SET channel='GDN' WHERE source_medium='google / cpc' AND campaign not like 'b.%' AND campaign like 'r.%';
UPDATE ga_visits_cost_source_medium SET channel='GDN' WHERE source='google / cpc' AND campaign like '[D[D%';
#SOCIOMANTIC
UPDATE ga_visits_cost_source_medium SET channel='Sociomantic' WHERE source = 'sociomantic' and medium = 'retargeting';
UPDATE ga_visits_cost_source_medium SET channel='GDN Retargeting' WHERE source='google / cpc' AND campaign like '[D[R%';
UPDATE ga_visits_cost_source_medium SET channel='Vizury' WHERE source like '%vizury%';
UPDATE ga_visits_cost_source_medium SET channel='VEInteractive' WHERE source like '%VEInteractive%';
#MERCADO LIBRE
UPDATE ga_visits_cost_source_medium SET channel='Mercado Libre Referral' WHERE source_medium='mercadolibre.com.co / referral';
UPDATE ga_visits_cost_source_medium SET channel='Mercado Libre Voucher' WHERE source_medium like '%MercadoLibre%' OR source_medium='DT1PyRN' OR source_medium like 'ML%';
#SOCIAL MEDIA
UPDATE ga_visits_cost_source_medium SET channel='Facebook Referral' WHERE 
(source_medium='facebook.com / referral' OR source_medium like '%facebook%' OR source_medium like'FB%' 
OR source_medium='LinioColombia') AND source_medium not like '%socialmediaads%' ;
UPDATE ga_visits_cost_source_medium SET channel='Twitter Referral' WHERE source_medium='t.co / referral' OR source_medium='socialmedia / smtweet';
UPDATE ga_visits_cost_source_medium SET channel='FB Posts' WHERE (source_medium='socialmedia / landingpagefb' 
OR source_medium='socialmedia / smfbpost' OR source_medium='facebook / smfbpost' or source_medium like '%facebook%post') and campaign like 'COL%fan%';
UPDATE ga_visits_cost_source_medium SET channel='FB Posts' WHERE medium like 'smfbpost%';

#SERVICIO AL CLIENTE


UPDATE ga_visits_cost_source_medium SET channel='UpSell' WHERE  source_medium like 'UP%' OR source_medium like '%UP%' OR (source_medium='CS / call' AND campaign like 'ups%');
UPDATE ga_visits_cost_source_medium SET channel='Invalids' WHERE source_medium='CS / call' AND campaign like 'INVALID%';
UPDATE ga_visits_cost_source_medium SET channel='Reactivation' WHERE (source_medium='CS / call' AND campaign like 'reactivation%') OR source_medium like 'ACTIVA%' 
OR source_medium='CS / Reactivation';
UPDATE ga_visits_cost_source_medium SET channel='Inbound' WHERE (source_medium='CS / call' AND campaign like 'inbou%') OR source_medium='CS / Inbound' OR 
source_medium='out%';
UPDATE ga_visits_cost_source_medium SET channel='OutBound' WHERE source_medium='CS / OutBound' OR source_medium like 'CV%' OR (source_medium='CS / call' AND campaign like 'outbou%');
UPDATE ga_visits_cost_source_medium SET channel='CrossSales' WHERE source_medium='CS / CrossSale';
UPDATE ga_visits_cost_source_medium SET channel='Replace' WHERE source_medium like '%replace%';

#SEO
UPDATE ga_visits_cost_source_medium SET channel='Search Organic' WHERE source_medium like '%organic' OR source_medium='google.com.co / referral';
#NEWSLETTER
UPDATE ga_visits_cost_source_medium set channel='Newsletter' WHERE source_medium='Postal / CRM' 
OR source_medium='Email Marketing / CRM' OR source_medium='EMM / CRM' 
OR source_medium='Postal / (not set)' or source_medium like '%CRM%';
UPDATE ga_visits_cost_source_medium set channel='Voucher Navidad' WHERE source_medium like 'NAVIDAD%' 
OR source_medium like 'LINIONAVIDAD%'; 
UPDATE ga_visits_cost_source_medium set channel='Voucher Fin de Año' where source_medium='FINDEAÑO31';
UPDATE ga_visits_cost_source_medium set channel='Bandeja CAC' where source_medium='BANDEJA0203';
UPDATE ga_visits_cost_source_medium set channel='Tapete CAC' where source_medium like 'TAPETE%';
UPDATE ga_visits_cost_source_medium set channel = 'Perfumes CAC' where source_medium like 'PERFUME%';
UPDATE ga_visits_cost_source_medium set channel = 'VoucherBebes-Feb' where source_medium like 'BEBES%';
#BLOG
UPDATE ga_visits_cost_source_medium SET channel='Blog Linio' WHERE source_medium like 'blog%' OR source_medium='blog.linio.com.co / referral' OR source_medium='BLS12C';
UPDATE ga_visits_cost_source_medium SET channel='Blog Mujer' WHERE source_medium='mujer.linio.com.co / referral';
#OTHER
UPDATE ga_visits_cost_source_medium SET channel='Reactivation Letters' WHERE (source_medium like 'REACT%' OR source_medium like 'CLIENTVIP%' OR source_medium like 'LINIOVIP%' OR source_medium='VUELVE' OR source_medium like 'CLIENTEVIP%') AND source_medium<>'CLIENTEVIP1';
UPDATE ga_visits_cost_source_medium SET channel='Customer Adquisition Letters' WHERE  source_medium like 'CONOCE%' OR source_medium='CLIENTEVIP1';
UPDATE ga_visits_cost_source_medium SET channel='Internal Sells' WHERE source_medium like 'INTER%';
UPDATE ga_visits_cost_source_medium SET channel='Employee Vouchers' WHERE source_medium like 'OCT%' OR source_medium like 'SEP%';
UPDATE ga_visits_cost_source_medium SET channel='Parcel Vouchers' WHERE  source_medium like 'DESCUENTO%' OR source_medium='LINIO0KO' OR source_medium='LINIOiju';
UPDATE ga_visits_cost_source_medium SET channel='New Register' WHERE  source_medium like 'NL%' OR source_medium like 'NR%';
UPDATE ga_visits_cost_source_medium SET channel='Disculpas' WHERE source_medium like 'H1yJvf' OR source_medium like 'H87h7C' OR source_medium='KI025EL78LLXLACOL-25324'
OR source_medium='KI025EL85GRQLACOL-4516';
UPDATE ga_visits_cost_source_medium SET channel='Suppliers Email' WHERE source_medium='suppliers_mail / email';
UPDATE ga_visits_cost_source_medium SET channel='Out Of Stock' WHERE source_medium like 'NST%' OR source_medium='N0Ctor';
UPDATE ga_visits_cost_source_medium SET channel='Devoluciones' WHERE source_medium like 'DEV%';
UPDATE ga_visits_cost_source_medium SET channel='Broken Vouchers' WHERE source_medium like 'HVE%';
UPDATE ga_visits_cost_source_medium SET channel='Broken Discount' WHERE source_medium  like  'KER%' OR source_medium='D1UqWU';
UPDATE ga_visits_cost_source_medium SET channel='Voucher Empleados' WHERE source_medium like 'EMPLEADO%';
UPDATE ga_visits_cost_source_medium SET channel='Gift Cards' WHERE source_medium like 'GC%';
UPDATE ga_visits_cost_source_medium SET channel='Friends and Family' WHERE source_medium like 'FF%';
UPDATE ga_visits_cost_source_medium SET channel='Leads' WHERE source_medium like 'LC%';

#PARTNERSHIPS
UPDATE ga_visits_cost_source_medium SET channel='Banco Helm' WHERE  source_medium like  'HELM%';
UPDATE ga_visits_cost_source_medium SET channel='Banco de Bogotá' WHERE  source_medium like 'CLIENTEBOG%' OR source_medium like 'MOVISTAR%' 
OR source_medium like 'BOG%' OR source_medium like 'BDB%' OR source_medium='BB42' OR source_medium='BB46' OR source_medium='BBLAV' OR source_medium='BBNEV' OR
source_medium='PA953EL96NHFLACOL' OR
source_medium='PA953EL96NHFLACOL' OR
source_medium='BH859EL51WBWLACOL' OR
source_medium='CU054HL58AXPLACOL' OR
source_medium='SA860TB24WCXLACOL' OR
source_medium='HA997TB17DLQLACOL' OR
source_medium='SA860TB35WCMLACOL' OR
source_medium='SA860TB37WCKLACOL' OR
source_medium='BO944TB20JEPLACOL' OR
source_medium='MG614TB23HTWLACOL' OR
source_medium='BA186TB36HTJLACOL' OR
source_medium='PR506TB63BFCLACOL' OR
source_medium='MI392TB32VRBLACOL' OR
source_medium='MI392TB33VRALACOL' OR
source_medium='BO944TB79JCILACOL' OR
source_medium='BO944TB24JELLACOL' OR
source_medium='HA997TB23DLKLACOL';

UPDATE ga_visits_cost_source_medium SET channel='Banco Davivienda' WHERE  source_medium like 'DAV%' OR source_medium='DV_AG' OR source_medium='DVPLANCHA46';
UPDATE ga_visits_cost_source_medium SET channel='Banco BBVA' WHERE  source_medium like 'BBVA%' OR
source_medium='SA860TB32WCPLACOL' OR
source_medium='SA860TB43WCELACOL' OR
source_medium='BA186TB35HTKLACOL' OR
source_medium='PR506TB55BFKLACOL' OR
source_medium='PR506TB62BFDLACOL' OR
source_medium='PA953EL97NHELACOL' OR
source_medium='PO916EL07ETSLACOL' OR
source_medium='PO916EL04ETVLACOL' OR
source_medium='LU939HL23XSILACOL' OR
source_medium='LU939HL32XRZLACOL' OR
source_medium='HA997TB15DLSLACOL' OR
source_medium='SA860TB35WCMLACOLACOL';

UPDATE ga_visits_cost_source_medium SET channel='Campus Party' WHERE source_medium like 'CP%';
UPDATE ga_visits_cost_source_medium SET channel='Publimetro' WHERE source_medium='PUBLIMETRO';
UPDATE ga_visits_cost_source_medium SET channel='El Espectador' WHERE source_medium='ESPECT0yx' OR source_medium like 'FIXED%';
UPDATE ga_visits_cost_source_medium SET channel='Grupon' WHERE source_medium like 'GR%';
UPDATE ga_visits_cost_source_medium SET channel='Dafiti' WHERE source_medium='AMIGODAFITI';
UPDATE ga_visits_cost_source_medium SET channel='Sin IVA 16' WHERE source_medium='SINIVA16';
UPDATE ga_visits_cost_source_medium SET channel='En Medio' WHERE source_medium='ENMEDIO';
UPDATE ga_visits_cost_source_medium SET channel='Bancolombia' WHERE source_medium like 'AMEX%' OR
source_medium='ABNSbkpSmM' OR
source_medium='ATR08i1X6D' OR
source_medium='TABAMEX1DA2h' OR source_medium='ATR08i1X6D' OR
source_medium='DEV1sd0c' OR
source_medium='DEVfjFW6' OR
source_medium='DEVbCM2o' OR
source_medium='DEVN6TJN' OR
source_medium='DEVFvwqn' OR
source_medium='DEV0QUfY' OR
source_medium='DEVmhwdB' OR
source_medium='DEV0rCuq' OR
source_medium='DEVlLjUZ' OR
source_medium='DEVzKPuf' OR
source_medium='DEV6IJP6' OR
source_medium='DEV08L8A' OR
source_medium='DEV0Fvg' OR
source_medium='DEVcTttT' OR
source_medium='DEVywRNF' OR
source_medium='DEV0umCc' OR
source_medium='DEVFcXZN' OR
source_medium='AMEXTAB8BSoc';
UPDATE ga_visits_cost_source_medium SET channel='Citi Bank' WHERE  source_medium like 'CITI%';

#DIRECT
UPDATE ga_visits_cost_source_medium SET channel='Linio.com Referral' WHERE  source_medium='linio.com / referral' OR source_medium='info.linio.com.co / referral';
UPDATE ga_visits_cost_source_medium SET channel='Directo / Typed' WHERE  source_medium='(direct) / (none)';


#REFERRAL PROGRAM
UPDATE ga_visits_cost_source_medium SET channel='Referral Program' WHERE source_medium like 'REF%';
#CORPORATE SALES
UPDATE ga_visits_cost_source_medium set channel='Corporate Sales' WHERE source_medium like'VC%' OR source_medium like 'VCJ%' OR source_medium like 'VCH%'OR source_medium like 'CVE%' 
OR source_medium like 'VCA%' OR source_medium='corporatesales / corporatesales' OR source_medium='BIENVENIDO10';
#PR
UPDATE ga_visits_cost_source_medium SET channel='Journalist Vouchers' WHERE source_medium='CAMILOCARM50' OR
source_medium='JUANPABVAR50' OR
source_medium='LAURACHARR50' OR
source_medium='MONICAPARA50' OR
source_medium='DIEGONARVA50' OR
source_medium='LUCYBUENO50' OR
source_medium='DIANALEON50' OR
source_medium='SILVIAPARR50' OR
source_medium='LAURAAYALA50' OR
source_medium='LEONARDONI50' OR
source_medium='CYNTHIARUIZ50' OR
source_medium='JAVIERLEAESC50' OR
source_medium='LORENAPULEC50' OR
source_medium='MARIAISABE50' OR
source_medium='PAOLACALLE50' OR
source_medium='ISABELSALAZ50' OR
source_medium='VICTORSOLA50' OR
source_medium='FABIANRAM50' OR
source_medium='ANDRESROD50' OR
source_medium='HENRYGON50' OR
source_medium='JOHANMA50' OR
source_medium='ALFONSOAYA50' OR
source_medium='VICTORSOLANAV';
UPDATE ga_visits_cost_source_medium SET channel='ADN' WHERE source_medium like'ADN%';
UPDATE ga_visits_cost_source_medium SET channel='Metro' WHERE source_medium like'METRO%';
UPDATE ga_visits_cost_source_medium SET channel='TRANSMILENIO' WHERE source_medium like'TRANSMILENIO%';
UPDATE ga_visits_cost_source_medium SET channel='SMS SanValentin' WHERE source_medium like'SANVALENTIN%';
UPDATE ga_visits_cost_source_medium SET channel='SuperDescuentos' WHERE source_medium like'SUPERDESCUENTOS%';
UPDATE ga_visits_cost_source_medium SET channel='Folletos' WHERE source_medium like'REGALO%';

#channel Report Extra Stuff to define channel---

#N/A
UPDATE ga_visits_cost_source_medium SET channel='Unknown-No voucher' WHERE source_medium='';
UPDATE ga_visits_cost_source_medium SET channel='Unknown-Voucher' WHERE channel='' AND source_medium<>'';


#DEFINE channel GROUP

#Facebook Ads Group
UPDATE ga_visits_cost_source_medium SET channel_group='Facebook Ads' WHERE channel='Facebook Ads';

#SEM Group
UPDATE ga_visits_cost_source_medium SET channel_group='Search Engine Marketing' WHERE channel='SEM (unbranded)';

#Social Media Group
UPDATE ga_visits_cost_source_medium SET channel_group='Social Media' WHERE channel='Facebook Referral' OR channel='Twitter Referral'
OR channel='FB Posts' OR channel='FB-Diana Call';

#Retargeting Group
UPDATE ga_visits_cost_source_medium SET channel_group='Retargeting' WHERE channel='Sociomantic' or channel = 'Vizury'
OR channel = 'VEInteractive' or channel = 'GDN Retargeting';

#GDN group
UPDATE ga_visits_cost_source_medium SET channel_group='Google Display Network' WHERE channel like 'GDN'; 

#NewsLetter Group
UPDATE ga_visits_cost_source_medium SET channel_group='NewsLetter' WHERE channel='Newsletter'
OR channel='Voucher Navidad' OR channel='Voucher Fin de Año' 
OR channel='Bandeja CAC' OR channel='Tapete CAC' OR channel='Perfumes CAC'
OR channel='VoucherBebes-Feb' OR channel='Sartenes CAC' OR channel='Estufa CAC';

#SEO Group
UPDATE ga_visits_cost_source_medium SET channel_group='Search Engine Optimization' WHERE channel='Search Organic';

#Partnerships Group
UPDATE ga_visits_cost_source_medium SET channel_group='Partnerships' WHERE
channel='Banco Helm' OR channel='Banco de Bogotá' OR channel='Banco Davivienda'
OR channel='Banco Davivienda' OR channel='Banco BBVA'
OR channel='Campus Party' OR channel='Publimetro'
OR channel='El Espectador' OR channel='Grupon'
OR channel='Dafiti' OR channel='Sin IVA 16' OR channel='En Medio' OR channel='Bancolombia' OR channel='AMEX'
OR channel='Citi Bank';

#Corporate Sales Group
UPDATE ga_visits_cost_source_medium SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';

#Offline Group
UPDATE ga_visits_cost_source_medium SET channel_group='Offline Marketing' WHERE 
channel='ADN' OR channel='Journalist Vouchers' OR channel='Metro' OR
channel='TRANSMILENIO' OR channel='SMS SanValentin' OR channel='SuperDescuentos' OR
channel='Folletos';

#Tele Sales Group
UPDATE ga_visits_cost_source_medium SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales' OR channel='Replace';

#Branded Group
UPDATE ga_visits_cost_source_medium SET channel_group='Branded' WHERE channel='Linio.com Referral' 
OR channel='Directo / Typed' OR
channel='SEM Branded';

#ML Group
UPDATE ga_visits_cost_source_medium SET channel_group='Mercado Libre' WHERE channel='Mercado Libre Referral' OR channel='Mercado Libre Voucher' OR channel='Otros - Mercado Libre';

#Blog Group
UPDATE ga_visits_cost_source_medium SET channel_group='Blogs' WHERE 
channel='Blog Linio' OR channel='Blog Mujer';

#Affiliates Group
UPDATE ga_visits_cost_source_medium SET channel_group='Affiliates' WHERE channel='Buscape';


#Other (identified) Group
UPDATE ga_visits_cost_source_medium SET channel_group='Other (identified)' WHERE channel='Referral Sites' 
OR channel='Reactivation Letters' OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' 
OR channel='Referral Program';

update ga_visits_cost_source_medium set channel_group = 'Social Media', channel = 'FB Posts' where source = 'facebook' and medium = 'socialmedia';

UPDATE ga_visits_cost_source_medium SET channel='Unknown-No voucher' WHERE channel is null;
UPDATE ga_visits_cost_source_medium SET channel_group='Non identified' WHERE channel='Unknown-No voucher' OR channel='Unknown-Voucher' Or channel is null;


#ga_cost_campaign
UPDATE ga_cost_campaign SET channel='Blog Linio' WHERE source_medium like 'blog%' OR source_medium='blog.linio.com.co / referral' OR source_medium='BLS12C';
UPDATE ga_cost_campaign SET channel='Blog Mujer' WHERE source_medium='mujer.linio.com.co / referral';
#REFERRAL
UPDATE ga_cost_campaign SET channel='Referral Sites' WHERE source_medium like '%referral' AND source_medium<>'linio.com / referral' AND source_medium<>'info.linio.com.co / referral' AND source_medium<>'facebook.com / referral' 
AND source_medium<>'t.co / referral' AND source_medium<>'mercadolibre.com.co / referral' AND source_medium<>'google.com.co / referral' AND source_medium<>'blog.linio.com.co / referral' AND source_medium<>'mujer.linio.com.co / referral';
#BUSCAPE
UPDATE ga_cost_campaign SET channel='Buscape' WHERE source_medium='Buscape / Price Comparison';
#FB ADS
UPDATE ga_cost_campaign SET channel='Facebook Ads' WHERE source_medium='facebook / socialmediaads' AND campaign not like 'COL%fan%';
#SEM - GDN
UPDATE ga_cost_campaign 
SET channel='SEM Branded' WHERE source like '%google%' and medium like '%cpc%' 
AND (campaign like 'brand%');
UPDATE ga_cost_campaign SET channel='SEM (unbranded)' 
WHERE source like '%google%' and medium like '%cpc%'  AND (campaign not like 'brand%');
UPDATE ga_cost_campaign SET channel='GDN' WHERE source_medium='google / cpc' AND campaign not like 'b.%' AND campaign like 'r.%';
UPDATE ga_cost_campaign SET channel='GDN' WHERE source='google / cpc' AND campaign like '[D[D%';
#SOCIOMATIC
UPDATE ga_cost_campaign SET channel='Sociomantic' WHERE source='sociomantic' and medium = 'retargeting';
UPDATE ga_cost_campaign SET channel='Vizury' WHERE source like '%vizury%';
UPDATE ga_cost_campaign SET channel='VEInteractive' WHERE source like '%VEInteractive%';
UPDATE ga_cost_campaign SET channel='GDN Retargeting' WHERE source='google / cpc' AND campaign like '[D[R%';
#MERCADO LIBRE
UPDATE ga_cost_campaign SET channel='Mercado Libre Referral' WHERE source_medium='mercadolibre.com.co / referral';
UPDATE ga_cost_campaign SET channel='Mercado Libre Voucher' WHERE source_medium like '%MercadoLibre%' OR source_medium='DT1PyRN' OR source_medium like 'ML%';
#SOCIAL MEDIA
UPDATE ga_cost_campaign SET channel='Facebook Referral' WHERE 
(source_medium='facebook.com / referral' OR source_medium like '%facebook%' OR source_medium like'FB%' 
OR source_medium='LinioColombia') AND source_medium not like '%socialmediaads%' ;
UPDATE ga_cost_campaign SET channel='Twitter Referral' WHERE source_medium='t.co / referral' OR source_medium='socialmedia / smtweet';
UPDATE ga_cost_campaign SET channel='FB Posts' WHERE (source_medium='socialmedia / landingpagefb' 
OR source_medium='socialmedia / smfbpost' OR source_medium='facebook / smfbpost' or source_medium like '%facebook%post') and campaign like 'COL%fan%';

#SERVICIO AL CLIENTE


UPDATE ga_cost_campaign SET channel='UpSell' WHERE  source_medium like 'UP%' OR source_medium like '%UP%' OR (source_medium='CS / call' AND campaign like 'ups%');
UPDATE ga_cost_campaign SET channel='Invalids' WHERE source_medium='CS / call' AND campaign like 'INVALID%';
UPDATE ga_cost_campaign SET channel='Reactivation' WHERE (source_medium='CS / call' AND campaign like 'reactivation%') OR source_medium like 'ACTIVA%' 
OR source_medium='CS / Reactivation';
UPDATE ga_cost_campaign SET channel='Inbound' WHERE (source_medium='CS / call' AND campaign like 'inbou%') OR source_medium='CS / Inbound' OR 
source_medium='out%';
UPDATE ga_cost_campaign SET channel='OutBound' WHERE source_medium='CS / OutBound' OR source_medium like 'CV%' OR (source_medium='CS / call' AND campaign like 'outbou%');
UPDATE ga_cost_campaign SET channel='CrossSales' WHERE source_medium='CS / CrossSale';
UPDATE ga_cost_campaign SET channel='Replace' WHERE source_medium like '%replace%';

#SEO
UPDATE ga_cost_campaign SET channel='Search Organic' WHERE source_medium like '%organic' OR source_medium='google.com.co / referral';
#NEWSLETTER
UPDATE ga_cost_campaign set channel='Newsletter' WHERE source_medium='Postal / CRM' 
OR source_medium='Email Marketing / CRM' OR source_medium='EMM / CRM' 
OR source_medium='Postal / (not set)' or source_medium like '%CRM%';
UPDATE ga_cost_campaign set channel='Voucher Navidad' WHERE source_medium like 'NAVIDAD%' 
OR source_medium like 'LINIONAVIDAD%'; 
UPDATE ga_cost_campaign set channel='Voucher Fin de Año' where source_medium='FINDEAÑO31';
UPDATE ga_cost_campaign set channel='Bandeja CAC' where source_medium='BANDEJA0203';
UPDATE ga_cost_campaign set channel='Tapete CAC' where source_medium like 'TAPETE%';
UPDATE ga_cost_campaign set channel = 'Perfumes CAC' where source_medium like 'PERFUME%';
UPDATE ga_cost_campaign set channel = 'VoucherBebes-Feb' where source_medium like 'BEBES%';

#OTHER
UPDATE ga_cost_campaign SET channel='Reactivation Letters' WHERE (source_medium like 'REACT%' OR source_medium like 'CLIENTVIP%' OR source_medium like 'LINIOVIP%' OR source_medium='VUELVE' OR source_medium like 'CLIENTEVIP%') AND source_medium<>'CLIENTEVIP1';
UPDATE ga_cost_campaign SET channel='Customer Adquisition Letters' WHERE  source_medium like 'CONOCE%' OR source_medium='CLIENTEVIP1';
UPDATE ga_cost_campaign SET channel='Internal Sells' WHERE source_medium like 'INTER%';
UPDATE ga_cost_campaign SET channel='Employee Vouchers' WHERE source_medium like 'OCT%' OR source_medium like 'SEP%';
UPDATE ga_cost_campaign SET channel='Parcel Vouchers' WHERE  source_medium like 'DESCUENTO%' OR source_medium='LINIO0KO' OR source_medium='LINIOiju';
UPDATE ga_cost_campaign SET channel='New Register' WHERE  source_medium like 'NL%' OR source_medium like 'NR%';
UPDATE ga_cost_campaign SET channel='Disculpas' WHERE source_medium like 'H1yJvf' OR source_medium like 'H87h7C' OR source_medium='KI025EL78LLXLACOL-25324'
OR source_medium='KI025EL85GRQLACOL-4516';
UPDATE ga_cost_campaign SET channel='Suppliers Email' WHERE source_medium='suppliers_mail / email';
UPDATE ga_cost_campaign SET channel='Out Of Stock' WHERE source_medium like 'NST%' OR source_medium='N0Ctor';
UPDATE ga_cost_campaign SET channel='Devoluciones' WHERE source_medium like 'DEV%';
UPDATE ga_cost_campaign SET channel='Broken Vouchers' WHERE source_medium like 'HVE%';
UPDATE ga_cost_campaign SET channel='Broken Discount' WHERE source_medium  like  'KER%' OR source_medium='D1UqWU';
UPDATE ga_cost_campaign SET channel='Voucher Empleados' WHERE source_medium like 'EMPLEADO%';
UPDATE ga_cost_campaign SET channel='Gift Cards' WHERE source_medium like 'GC%';
UPDATE ga_cost_campaign SET channel='Friends and Family' WHERE source_medium like 'FF%';
UPDATE ga_cost_campaign SET channel='Leads' WHERE source_medium like 'LC%';

#PARTNERSHIPS
UPDATE ga_cost_campaign SET channel='Banco Helm' WHERE  source_medium like  'HELM%';
UPDATE ga_cost_campaign SET channel='Banco de Bogotá' WHERE  source_medium like 'CLIENTEBOG%' OR source_medium like 'MOVISTAR%' 
OR source_medium like 'BOG%' OR source_medium like 'BDB%' OR source_medium='BB42' OR source_medium='BB46' OR source_medium='BBLAV' OR source_medium='BBNEV' OR
source_medium='PA953EL96NHFLACOL' OR
source_medium='PA953EL96NHFLACOL' OR
source_medium='BH859EL51WBWLACOL' OR
source_medium='CU054HL58AXPLACOL' OR
source_medium='SA860TB24WCXLACOL' OR
source_medium='HA997TB17DLQLACOL' OR
source_medium='SA860TB35WCMLACOL' OR
source_medium='SA860TB37WCKLACOL' OR
source_medium='BO944TB20JEPLACOL' OR
source_medium='MG614TB23HTWLACOL' OR
source_medium='BA186TB36HTJLACOL' OR
source_medium='PR506TB63BFCLACOL' OR
source_medium='MI392TB32VRBLACOL' OR
source_medium='MI392TB33VRALACOL' OR
source_medium='BO944TB79JCILACOL' OR
source_medium='BO944TB24JELLACOL' OR
source_medium='HA997TB23DLKLACOL';

UPDATE ga_cost_campaign SET channel='Banco Davivienda' WHERE  source_medium like 'DAV%' OR source_medium='DV_AG' OR source_medium='DVPLANCHA46';
UPDATE ga_cost_campaign SET channel='Banco BBVA' WHERE  source_medium like 'BBVA%' OR
source_medium='SA860TB32WCPLACOL' OR
source_medium='SA860TB43WCELACOL' OR
source_medium='BA186TB35HTKLACOL' OR
source_medium='PR506TB55BFKLACOL' OR
source_medium='PR506TB62BFDLACOL' OR
source_medium='PA953EL97NHELACOL' OR
source_medium='PO916EL07ETSLACOL' OR
source_medium='PO916EL04ETVLACOL' OR
source_medium='LU939HL23XSILACOL' OR
source_medium='LU939HL32XRZLACOL' OR
source_medium='HA997TB15DLSLACOL' OR
source_medium='SA860TB35WCMLACOLACOL';

UPDATE ga_cost_campaign SET channel='Campus Party' WHERE source_medium like 'CP%';
UPDATE ga_cost_campaign SET channel='Publimetro' WHERE source_medium='PUBLIMETRO';
UPDATE ga_cost_campaign SET channel='El Espectador' WHERE source_medium='ESPECT0yx' OR source_medium like 'FIXED%';
UPDATE ga_cost_campaign SET channel='Grupon' WHERE source_medium like 'GR%';
UPDATE ga_cost_campaign SET channel='Dafiti' WHERE source_medium='AMIGODAFITI';
UPDATE ga_cost_campaign SET channel='Sin IVA 16' WHERE source_medium='SINIVA16';
UPDATE ga_cost_campaign SET channel='En Medio' WHERE source_medium='ENMEDIO';
UPDATE ga_cost_campaign SET channel='Bancolombia' WHERE source_medium like 'AMEX%' OR
source_medium='ABNSbkpSmM' OR
source_medium='ATR08i1X6D' OR
source_medium='TABAMEX1DA2h' OR source_medium='ATR08i1X6D' OR
source_medium='DEV1sd0c' OR
source_medium='DEVfjFW6' OR
source_medium='DEVbCM2o' OR
source_medium='DEVN6TJN' OR
source_medium='DEVFvwqn' OR
source_medium='DEV0QUfY' OR
source_medium='DEVmhwdB' OR
source_medium='DEV0rCuq' OR
source_medium='DEVlLjUZ' OR
source_medium='DEVzKPuf' OR
source_medium='DEV6IJP6' OR
source_medium='DEV08L8A' OR
source_medium='DEV0Fvg' OR
source_medium='DEVcTttT' OR
source_medium='DEVywRNF' OR
source_medium='DEV0umCc' OR
source_medium='DEVFcXZN' OR
source_medium='AMEXTAB8BSoc';
UPDATE ga_cost_campaign SET channel='Citi Bank' WHERE  source_medium like 'CITI%';

#DIRECT
UPDATE ga_cost_campaign SET channel='Linio.com Referral' WHERE  source_medium='linio.com / referral' OR source_medium='info.linio.com.co / referral';
UPDATE ga_cost_campaign SET channel='Directo / Typed' WHERE  source_medium='(direct) / (none)';


#REFERRAL PROGRAM
UPDATE ga_cost_campaign SET channel='Referral Program' WHERE source_medium like 'REF%';
#CORPORATE SALES
UPDATE ga_cost_campaign set channel='Corporate Sales' WHERE source_medium like'VC%' OR source_medium like 'VCJ%' OR source_medium like 'VCH%'OR source_medium like 'CVE%' 
OR source_medium like 'VCA%' OR source_medium='corporatesales / corporatesales' OR source_medium='BIENVENIDO10';
#PR
UPDATE ga_cost_campaign SET channel='Journalist Vouchers' WHERE source_medium='CAMILOCARM50' OR
source_medium='JUANPABVAR50' OR
source_medium='LAURACHARR50' OR
source_medium='MONICAPARA50' OR
source_medium='DIEGONARVA50' OR
source_medium='LUCYBUENO50' OR
source_medium='DIANALEON50' OR
source_medium='SILVIAPARR50' OR
source_medium='LAURAAYALA50' OR
source_medium='LEONARDONI50' OR
source_medium='CYNTHIARUIZ50' OR
source_medium='JAVIERLEAESC50' OR
source_medium='LORENAPULEC50' OR
source_medium='MARIAISABE50' OR
source_medium='PAOLACALLE50' OR
source_medium='ISABELSALAZ50' OR
source_medium='VICTORSOLA50' OR
source_medium='FABIANRAM50' OR
source_medium='ANDRESROD50' OR
source_medium='HENRYGON50' OR
source_medium='JOHANMA50' OR
source_medium='ALFONSOAYA50' OR
source_medium='VICTORSOLANAV';
UPDATE ga_cost_campaign SET channel='ADN' WHERE source_medium like'ADN%';
UPDATE ga_cost_campaign SET channel='Metro' WHERE source_medium like'METRO%';
UPDATE ga_cost_campaign SET channel='TRANSMILENIO' WHERE source_medium like'TRANSMILENIO%';
UPDATE ga_cost_campaign SET channel='SMS SanValentin' WHERE source_medium like'SANVALENTIN%';
UPDATE ga_cost_campaign SET channel='SuperDescuentos' WHERE source_medium like'SUPERDESCUENTOS%';
UPDATE ga_cost_campaign SET channel='Folletos' WHERE source_medium like'REGALO%';

#channel Report Extra Stuff to define channel---

#N/A
UPDATE ga_cost_campaign SET channel='Unknown-No voucher' WHERE source_medium='';
UPDATE ga_cost_campaign SET channel='Unknown-Voucher' WHERE channel='' AND source_medium<>'';


#DEFINE channel GROUP

#Facebook Ads Group
UPDATE ga_cost_campaign SET channel_group='Facebook Ads' WHERE channel='Facebook Ads';

#SEM Group
UPDATE ga_cost_campaign SET channel_group='Search Engine Marketing' WHERE channel='SEM (unbranded)';

#Social Media Group
UPDATE ga_cost_campaign SET channel_group='Social Media' WHERE channel='Facebook Referral' OR channel='Twitter Referral'
OR channel='FB Posts' OR channel='FB-Diana Call';

#Retargeting Group
UPDATE ga_cost_campaign SET channel_group='Retargeting' WHERE channel='Sociomantic' or channel = 'Vizury'
OR channel = 'VEInteractive' or channel = 'GDN Retargeting';

#GDN group
UPDATE ga_visits_cost_source_medium SET channel_group='Google Display Network' WHERE channel like 'GDN'; 

#NewsLetter Group
UPDATE ga_cost_campaign SET channel_group='NewsLetter' WHERE channel='Newsletter'
OR channel='Voucher Navidad' OR channel='Voucher Fin de Año' 
OR channel='Bandeja CAC' OR channel='Tapete CAC' OR channel='Perfumes CAC'
OR channel='VoucherBebes-Feb' OR channel='Sartenes CAC' OR channel='Estufa CAC';

#SEO Group
UPDATE ga_cost_campaign SET channel_group='Search Engine Optimization' WHERE channel='Search Organic';

#Partnerships Group
UPDATE ga_cost_campaign SET channel_group='Partnerships' WHERE
channel='Banco Helm' OR channel='Banco de Bogotá' OR channel='Banco Davivienda'
OR channel='Banco Davivienda' OR channel='Banco BBVA'
OR channel='Campus Party' OR channel='Publimetro'
OR channel='El Espectador' OR channel='Grupon'
OR channel='Dafiti' OR channel='Sin IVA 16' OR channel='En Medio' OR channel='Bancolombia' OR channel='AMEX'
OR channel='Citi Bank';

#Corporate Sales Group
UPDATE ga_cost_campaign SET channel_group='Corporate Sales' WHERE channel='Corporate Sales';

#Offline Group
UPDATE ga_cost_campaign SET channel_group='Offline Marketing' WHERE 
channel='ADN' OR channel='Journalist Vouchers' OR channel='Metro' OR
channel='TRANSMILENIO' OR channel='SMS SanValentin' OR channel='SuperDescuentos' OR
channel='Folletos';

#Tele Sales Group
UPDATE ga_cost_campaign SET channel_group='Tele Sales' 
WHERE channel='UpSell' OR channel='OutBound' OR channel='Inbound' OR channel='Reactivation' 
OR channel='Invalids' OR channel='CrossSales' OR channel='Replace';

#Branded Group
UPDATE ga_cost_campaign SET channel_group='Branded' WHERE channel='Linio.com Referral' 
OR channel='Directo / Typed' OR
channel='SEM Branded';

#ML Group
UPDATE ga_cost_campaign SET channel_group='Mercado Libre' WHERE channel='Mercado Libre Referral' OR channel='Mercado Libre Voucher' OR channel='Otros - Mercado Libre';

#Blog Group
UPDATE ga_cost_campaign SET channel_group='Blogs' WHERE 
channel='Blog Linio' OR channel='Blog Mujer';

#Affiliates Group
UPDATE ga_cost_campaign SET channel_group='Affiliates' WHERE channel='Buscape';


#Other (identified) Group
UPDATE ga_cost_campaign SET channel_group='Other (identified)' WHERE channel='Referral Sites' 
OR channel='Reactivation Letters' OR channel like 'Customer Adquisition%' OR channel='Internal Sells' OR
channel='Employee Vouchers' OR channel='Parcel Vouchers' OR channel='New Register' OR channel='Disculpas' OR channel='Suppliers Email' OR channel='Out Of Stock' 
OR channel='Broken Discount' OR channel='Broken Vouchers' OR channel='Devoluciones' OR
channel='Voucher Empleados' OR channel='Gift Cards' OR channel='Friends and Family' OR channel='Leads' 
OR channel='Referral Program';

update ga_cost_campaign set channel_group = 'Social Media', channel = 'FB Posts' where source = 'facebook' and medium = 'socialmedia';

UPDATE ga_cost_campaign SET channel='Unknown-No voucher' WHERE channel is null;
UPDATE ga_cost_campaign SET channel_group='Non identified' WHERE channel='Unknown-No voucher' OR channel='Unknown-Voucher' Or channel is null;


SELECT 'Start: visits_costs_channel ', now();
call visits_costs_channel();
call mom_net_sales_per_channel();
call daily_marketing();
call monthly_marketing();

select 'End: channel_report_no_voucher', now();


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `cheating_list`()
begin

select  'Cheating List: start',now();

delete from production_co.cheating_list;

insert into production_co.cheating_list(date, custid, first_name, last_name, email, order_nr, coupon_code, address1, address2, street_number, city, postcode, phone_number) select t.date, t.custid, c.first_name, c.last_name, c.email, t.order_nr, t.coupon_code, a.address1, a.address2, a.street_number, a.city, a.postcode, a.phone from production_co.tbl_order_detail t, bob_live_co.customer c, bob_live_co.customer_address a where t.custid=c.id_customer and t.custid=a.fk_customer and t.obc=1 and t.coupon_code like 'CAC%' group by order_nr, phone, address1, address2, street_number, city order by custid;

update production_co.cheating_list c set actual_paid_price = (select sum(paid_price) from production_co.tbl_order_detail t where t.order_nr=c.order_nr);

update production_co.cheating_list c inner join bob_live_co.sales_order o on c.order_nr=o.order_nr set c.ip_address=o.ip;

update production_co.cheating_list c inner join bob_live_co.sales_order o on c.order_nr=o.order_nr inner join bob_live_co.sales_order_accertify_log g on o.id_sales_order=g.fk_sales_order set credit_card=ExtractValue(g.message,'/transaction/paymentInformation/cardNumber');

-- IP Address

create table production_co.temporary_cheat(
ip_address varchar(255),
custid int
);

create index temporary_cheat on production_co.temporary_cheat(ip_address, custid);

insert into production_co.temporary_cheat select ip, fk_customer from bob_live_co.sales_order;

update production_co.cheating_list c inner join production_co.temporary_cheat o on c.ip_address=o.ip_address set c.ip_address_cheat = 1 where c.custid!=o.custid; 

drop table production_co.temporary_cheat;

-- Address

create table production_co.temporary_cheat(
address1 varchar(255),
address2 varchar(255),
street_number varchar(255),
city varchar(255),
custid int
);

create index temporary_cheat on production_co.temporary_cheat(address1, address2, city, custid);

insert into production_co.temporary_cheat select address1, address2, street_number, city, fk_customer from bob_live_co.customer_address;

update production_co.cheating_list c inner join production_co.temporary_cheat o on c.address1=o.address1 and c.address2=o.address2 and c.street_number=o.street_number and c.city=o.city set c.address_cheat = 1 where c.custid!=o.custid;

drop table production_co.temporary_cheat;

-- Phone Number

create table production_co.temporary_cheat(
phone_number varchar(255),
custid int
);

create index temporary_cheat on production_co.temporary_cheat(phone_number, custid);

insert into production_co.temporary_cheat select phone, fk_customer from bob_live_co.customer_address;

update production_co.cheating_list c inner join production_co.temporary_cheat o on c.phone_number=o.phone_number set c.phone_number_cheat = 1 where c.custid!=o.custid; 

drop table production_co.temporary_cheat;

-- Same Name

create table production_co.temporary_cheat(
first_name varchar(255),
last_name varchar(255),
postcode int,
custid int
);

create index temporary_cheat on production_co.temporary_cheat(first_name, last_name, custid, postcode);

insert into production_co.temporary_cheat select c.first_name, c.last_name, a.postcode, c.id_customer from bob_live_co.customer c, bob_live_co.customer_address a where c.id_customer=a.fk_customer;

update production_co.cheating_list c inner join production_co.temporary_cheat o on c.first_name=o.first_name and c.last_name=o.last_name and c.postcode=o.postcode set c.same_name_cheat = 1 where c.custid!=o.custid; 

drop table production_co.temporary_cheat;

-- Credit Card

create table production_co.temporary_cheat(
credit_card varchar(255),
custid int
);

create index temporary_cheat on production_co.temporary_cheat(credit_card, custid);

insert into production_co.temporary_cheat select ExtractValue(g.message,'/transaction/paymentInformation/cardNumber'), fk_customer from bob_live_co.sales_order o, bob_live_co.sales_order_accertify_log g where o.id_sales_order=g.fk_sales_order;

update production_co.cheating_list c inner join production_co.temporary_cheat o on c.credit_card=o.credit_card set c.credit_card_cheat = 1 where c.custid!=o.custid; 

drop table production_co.temporary_cheat;

-- Linio Mail

update production_co.cheating_list set linio_mail_cheat = 1 where email like '%@linio%';

-- delete from production_co.cheating_list where ip_address_cheat is null and address_cheat is null and phone_number_cheat is null and same_name_cheat is null and credit_card_cheat is null and linio_mail_cheat is null;

update production_co.cheating_list set ip_address_cheat=0 where ip_address_cheat is null;

update production_co.cheating_list set address_cheat=0 where address_cheat is null;

update production_co.cheating_list set phone_number_cheat=0 where phone_number_cheat is null;

update production_co.cheating_list set same_name_cheat=0 where same_name_cheat is null;

update production_co.cheating_list set credit_card_cheat=0 where credit_card_cheat is null;

update production_co.cheating_list set linio_mail_cheat=0 where linio_mail_cheat is null;

select  'Cheating List: stop',now();

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `cohort_channel_category`()
begin

delete from production_co.cohort_channel_category;

##insert into production_co.cohort_channel_category(country, custid, yrmonth, date, channel_group, channel, category, voucher, net_revenue, PC15, nr_items, `% discount`, region) select 'Colombia', custid, yrmonth, date, channel_group, channel,(select n1 from production_co.tbl_order_detail x where paid_price = (select max(paid_price) from production_co.tbl_order_detail y where x.order_nr=y.order_nr group by order_nr) and z.order_nr=x.order_nr group by order_nr), coupon_code, sum(paid_price), sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)) as PC15, count(order_nr), 1-(avg(unit_price)/avg(original_price)), region from production_co.tbl_order_detail z where oac=1 and new_customers !=0 group by custid;

insert into production_co.cohort_channel_category(country, custid, yrmonth, date, channel_group, channel, voucher, net_revenue, PC15, nr_items, `% discount`, region) select 'Colombia', custid, yrmonth, date, channel_group, channel, coupon_code, sum(paid_price), sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)) as PC15, count(order_nr), 1-(avg(unit_price)/avg(original_price)), region from production_co.tbl_order_detail z where oac=1 and new_customers !=0 group by custid;

update production_co.cohort_channel_category z inner join bob_live_co.customer c on z.custid=c.id_customer set z.age = year(curdate()) - year(c.birthday), z.sex = c.gender;

-- Same Month

update production_co.cohort_channel_category b set nr_unique_repurcharse_same_month = (select case when count(distinct t.order_nr)>=1 then 1 else 0 end from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and b.yrmonth=t.yrmonth and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set transactions_same_month = (select count(distinct t.order_nr) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and b.yrmonth=t.yrmonth and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set nr_items_same_month = (select count(t.order_nr) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and b.yrmonth=t.yrmonth and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set net_revenue_same_month = (select sum(t.paid_price) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and b.yrmonth=t.yrmonth and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set PC15_same_month = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and b.yrmonth=t.yrmonth and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set nr_dif_categories_same_month = (select count(distinct t.n1) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and b.yrmonth=t.yrmonth and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set nr_dif_sub_categories_same_month = (select count(distinct t.n2) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and b.yrmonth=t.yrmonth and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category set nr_unique_repurcharse_same_month=0 where nr_unique_repurcharse_same_month is null;

-- 30 days

update production_co.cohort_channel_category b set nr_unique_repurcharse_30 = (select case when count(distinct t.order_nr)>=1 and nr_unique_repurcharse_same_month=0 then 1 else 0 end from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set transactions_30 = (select count(distinct t.order_nr) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set nr_items_30 = (select count(t.order_nr) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set net_revenue_30 = (select sum(t.paid_price) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set PC15_30 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set nr_dif_categories_30 = (select count(distinct t.n1) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set nr_dif_sub_categories_30 = (select count(distinct t.n2) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 30 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category set nr_unique_repurcharse_30=0 where nr_unique_repurcharse_30 is null;

-- 60 days

update production_co.cohort_channel_category b set nr_unique_repurcharse_60 = (select case when count(distinct t.order_nr)>=1 and nr_unique_repurcharse_same_month=0 and nr_unique_repurcharse_30=0 then 1 else 0 end from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set transactions_60 = (select count(distinct t.order_nr) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set nr_items_60 = (select count(t.order_nr) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set net_revenue_60 = (select sum(t.paid_price) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set PC15_60 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set nr_dif_categories_60 = (select count(distinct t.n1) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set nr_dif_sub_categories_60 = (select count(distinct t.n2) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 60 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category set nr_unique_repurcharse_60=0 where nr_unique_repurcharse_60 is null;

-- 90 days

update production_co.cohort_channel_category b set nr_unique_repurcharse_90 = (select case when count(distinct t.order_nr)>=1 and nr_unique_repurcharse_same_month=0 and nr_unique_repurcharse_30=0 and nr_unique_repurcharse_60=0 then 1 else 0 end from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 90 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set transactions_90 = (select count(distinct t.order_nr) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 90 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set nr_items_90 = (select count(t.order_nr) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 90 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set net_revenue_90 = (select sum(t.paid_price) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 90 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set PC15_90 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 90 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set nr_dif_categories_90 = (select count(distinct t.n1) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 90 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set nr_dif_sub_categories_90 = (select count(distinct t.n2) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 90 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category set nr_unique_repurcharse_90=0 where nr_unique_repurcharse_90 is null;

-- 120 days

update production_co.cohort_channel_category b set nr_unique_repurcharse_120 = (select case when count(distinct t.order_nr)>=1 and nr_unique_repurcharse_same_month=0 and nr_unique_repurcharse_30=0 and nr_unique_repurcharse_60=0 and nr_unique_repurcharse_90=0 then 1 else 0 end from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 120 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set transactions_120 = (select count(distinct t.order_nr) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 120 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set nr_items_120 = (select count(t.order_nr) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 120 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set net_revenue_120 = (select sum(t.paid_price) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 120 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set PC15_120 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 120 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set nr_dif_categories_120 = (select count(distinct t.n1) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 120 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set nr_dif_sub_categories_120 = (select count(distinct t.n2) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 120 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category set nr_unique_repurcharse_120=0 where nr_unique_repurcharse_120 is null;

-- 150 days

update production_co.cohort_channel_category b set nr_unique_repurcharse_150 = (select case when count(distinct t.order_nr)>=1 and nr_unique_repurcharse_same_month=0 and nr_unique_repurcharse_30=0 and nr_unique_repurcharse_60=0 and nr_unique_repurcharse_90=0 and nr_unique_repurcharse_120=0 then 1 else 0 end from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 150 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set transactions_150 = (select count(distinct t.order_nr) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 150 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set nr_items_150 = (select count(t.order_nr) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 150 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set net_revenue_150 = (select sum(t.paid_price) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 150 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set PC15_150 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 150 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set nr_dif_categories_150 = (select count(distinct t.n1) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 150 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set nr_dif_sub_categories_150 = (select count(distinct t.n2) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 150 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category set nr_unique_repurcharse_150=0 where nr_unique_repurcharse_150 is null;

-- 180 days

update production_co.cohort_channel_category b set nr_unique_repurcharse_180 = (select case when count(distinct t.order_nr)>=1 and nr_unique_repurcharse_same_month=0 and nr_unique_repurcharse_30=0 and nr_unique_repurcharse_60=0 and nr_unique_repurcharse_90=0 and nr_unique_repurcharse_120=0 and nr_unique_repurcharse_150=0 then 1 else 0 end from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 180 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set transactions_180 = (select count(distinct t.order_nr) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 180 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set nr_items_180 = (select count(t.order_nr) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 180 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set net_revenue_180 = (select sum(t.paid_price) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 180 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set PC15_180 = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0)) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 180 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set nr_dif_categories_180 = (select count(distinct t.n1) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 180 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category b set nr_dif_sub_categories_180 = (select count(distinct t.n2) from production_co.tbl_order_detail t where t.custid=b.custid and t.oac=1 and t.date between b.date and adddate(b.date, interval 180 day) and t.new_customers = 0 group by b.custid);

update production_co.cohort_channel_category set nr_unique_repurcharse_180=0 where nr_unique_repurcharse_180 is null;

update production_co.cohort_channel_category c inner join development_mx.A_E_BI_ExchangeRate_USD u on c.yrmonth=u.Month_Num set net_revenue=net_revenue/xr, net_revenue_same_month=net_revenue_same_month/xr, net_revenue_30=net_revenue_30/xr, net_revenue_60=net_revenue_60/xr, net_revenue_90=net_revenue_90/xr, net_revenue_120=net_revenue_120/xr, net_revenue_150=net_revenue_150/xr, net_revenue_180=net_revenue_180/xr, PC15=PC15/xr, PC15_same_month=PC15_same_month/xr, PC15_30=PC15_30/xr, PC15_60=PC15_60/xr, PC15_90=PC15_90/xr, PC15_120=PC15_120/xr, PC15_150=PC15_150/xr, PC15_180=PC15_180/xr where u.country='COL';


end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `daily_extra_queries`(lastDate DATE)
BEGIN


#Test Orders

SELECT 'START Bob Maestro',NOW();
#BOB vs. Maestro
drop table returnMaestro;
create table returnMaestro as 
(SELECT tbl_order_detail.Item as fk_sales_order_item 
FROM tbl_order_detail join tbl_fact_order_management 
ON tbl_order_detail.Item=tbl_fact_order_management.fk_sales_order_item 
WHERE tbl_fact_order_management.outbound_date is not null AND tbl_fact_order_management.cancel is not null 
AND (RETURNED='0' OR RETURNED IS NULL));
update tbl_order_detail set OAC='1', OBC='1', PENDING='0',RETURNED='1', CANCEL='0' WHERE Item in (select fk_sales_order_item from returnMaesto);
drop table cancelMaestro;
create table cancelMaestro as 
(SELECT tbl_order_detail.Item as fk_sales_order_item 
FROM tbl_order_detail JOIN tbl_fact_order_management
ON tbl_order_detail.Item=tbl_fact_order_management.fk_sales_order_item 
WHERE tbl_fact_order_management.outbound_date is null
AND tbl_fact_order_management.cancel is not null 
AND (OAC='1' OR OAC is null));
update tbl_order_detail set OBC='1', OAC='0', RETURNED='0' , PENDING='0' ,CANCEL='1' WHERE Item in (select fk_sales_order_item from cancelMaestro);
#Netas canceladas por doble clic en bob
update tbl_order_detail set OBC='1', OAC='1', RETURNED='0' , PENDING='0' ,CANCEL='0' WHERE Item in (select fk_sales_order_item from netMaestro);


SELECT 'START INBOUND',NOW();
#Proveedor entrega:
update  tbl_order_detail set delivery_cost_supplier=0.0 where proveedor='IZC MAYORISTA S.A.S';

update tbl_order_detail set costo_after_vat=(select cost from bob_live_co.catalog_simple where catalog_simple.sku=tbl_order_detail.sku) 
where costo_after_vat is null;
update tbl_order_detail set costo_after_vat=unit_price_after_vat where costo_after_vat is null;

#Devoluciones
update tbl_order_detail set unit_price=paid_price,unit_price_after_vat=paid_price_after_vat,coupon_money_value=0,coupon_money_after_vat=0 
WHERE coupon_code like 'INTERN%' and date>= date_add(lastDate, interval -7 day);
update tbl_order_detail set paid_price=unit_price,paid_price_after_vat=unit_price_after_vat,coupon_money_value=0,coupon_money_after_vat=0 
WHERE (coupon_code like 'DEV%' 
OR coupon_code like 'NST%' OR coupon_code like 'TRS%' 
OR coupon_code like 'HVE%' or coupon_code like 'RET%' or coupon_code like 'NST%' 
or coupon_code in (select dev_voucher.voucher from dev_voucher) and date>= date_add(lastDate, interval -7 day));

#WMS Match
UPDATE tbl_order_detail
INNER JOIN `tbl_bi_ops_delivery` ON `tbl_bi_ops_delivery`.`item_id` =`tbl_order_detail`.`Item`
SET OBC='1', OAC='1', RETURNED='1', CANCEL='0', PENDING='0'
WHERE OAC='1' AND RETURNED='0' AND (status_wms='Cancelado' OR  status_wms='quebra tratada' OR status_wms='Quebrado') AND date_shipped is not null;

UPDATE tbl_order_detail
INNER JOIN `tbl_bi_ops_delivery` ON `tbl_bi_ops_delivery`.`item_id` =`tbl_order_detail`.`Item`
SET OBC='1', OAC='0', RETURNED='0', CANCEL='1', PENDING='0'
WHERE OAC='1' AND (status_wms='Cancelado' OR status_wms='quebra tratada' OR status_wms='Quebrado') AND date_shipped is null;

#Gender
update bob_live_co.customer set gender = 'male' where prefix='Sr.';
update bob_live_co.customer set gender = 'female' where prefix='Sra.';

drop table if exists tbl_nombre_mujeres;
create table tbl_nombre_mujeres as (select distinct first_name from bob_live_co.customer  where gender='female' and first_name not like '%Test%') ;
update bob_live_co.customer set gender='female' where first_name in (select first_name from tbl_nombre_mujeres ) and first_name not like '%Test%';

drop table if exists tbl_nombre_hombres;
create table tbl_nombre_hombres as (select distinct first_name from bob_live_co.customer  where gender='male' and first_name not like '%Test%');
update bob_live_co.customer set gender='male' where first_name in (select first_name from tbl_nombre_hombres ) and first_name not like '%Test%';

SELECT 'START Compradores',NOW();

#Nombre Compradores
UPDATE tbl_order_detail SET buyer='Alex' WHERE buyer like '%Alex%';
UPDATE tbl_order_detail SET buyer='Mafe' 

WHERE buyer like '%Maria Fernanda%' 
or n3 like '%Cuidado%Capilar%' 
or n3 like '%Cuidado Corporal%';
UPDATE tbl_order_detail SET buyer='Katherine' WHERE buyer like '%Kathe%';
UPDATE tbl_order_detail SET buyer='Eliana' WHERE buyer like '%Eliana%';
UPDATE tbl_order_detail SET buyer='Cesar' WHERE buyer like '%Cesar%';
UPDATE tbl_order_detail SET buyer='Cristina' WHERE buyer like '%Cristina%';
UPDATE tbl_order_detail SET buyer='Natalia' WHERE buyer like '%Natalia%';
UPDATE tbl_order_detail SET buyer='Freddy' WHERE buyer like '%Freddy%';
UPDATE tbl_order_detail SET buyer = 'Jessica'  
WHERE (n2 like '%Perfumes%' or n3 like '%Bienestar%Sexual%' or n3 like '%Perfumes%' or buyer like '%Lida%' );
UPDATE tbl_order_detail SET buyer='Otros' WHERE product_name like 'Tarjeta%Regalo%';
UPDATE tbl_order_detail SET buyer = 'Otros' WHERE buyer is null;
UPDATE tbl_order_detail SET buyer = 'Otros' WHERE buyer = '';




END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `daily_report`()
BEGIN

truncate tbl_dailyReport;
insert into tbl_dailyReport (date) select dt from calendar where dt>'2012-07-31' and dt<now();

update tbl_dailyReport
set year = year(date) , month = month(date) , yrmonth = concat(year,if(month<10,concat(0,month),month));

#Gross
update tbl_dailyReport join 
(select date,sum(`unit_price`) as grossrev, count(distinct `order_nr`,`OAC`,`RETURNED`,`PENDING`) as grossorders, 
	sum(`unit_price_after_vat`) as GrossSalesPreCancel
            from `tbl_order_detail` where   (`OBC` = '1')
            group by `date`) AS tbl_gross on tbl_gross.date=tbl_dailyReport.date
set tbl_dailyReport.GrossRev=tbl_gross.grossrev , tbl_dailyReport.GrossOrders=tbl_gross.grossorders, 
tbl_dailyReport.GrossSalesPreCancel=tbl_gross.GrossSalesPreCancel;

#Cancel
update tbl_dailyReport join 
(select date,sum(`unit_price_after_vat`) as cancelrev, count(distinct `order_nr`) as cancelorders
from `tbl_order_detail` 
where   (`CANCEL` = '1')
group by `date`) AS tbl_cancel on tbl_cancel.date=tbl_dailyReport.date
set tbl_dailyReport.Cancellations=tbl_cancel.cancelrev , tbl_dailyReport.CancelOrders=tbl_cancel.cancelorders;


#Pending
update tbl_dailyReport join 
(select date,sum(`unit_price_after_vat`) as pendingrev, count(distinct `order_nr`) as pendingorders
from `tbl_order_detail` 
where   (`PENDING` = '1')
group by `date`) AS tbl_pending on tbl_pending.date=tbl_dailyReport.date
set tbl_dailyReport.PendingCOP=tbl_pending.pendingrev , tbl_dailyReport.PendingOrders=tbl_pending.pendingorders;


#cogs_gross_post_cancel
update tbl_dailyReport join 
(select date,(sum(`costo_after_vat`) + sum(`delivery_cost_supplier`)) as cogs_gross_post_cancel
from `tbl_order_detail` 
where   (`OAC` = '1')
group by `date`) AS tbl_cogs_post_cancel on tbl_cogs_post_cancel.date=tbl_dailyReport.date
set tbl_dailyReport.cogs_gross_post_cancel=tbl_cogs_post_cancel.cogs_gross_post_cancel;

#Pending
update tbl_dailyReport join 
(select date,sum(`unit_price_after_vat`) as returnrev, count(distinct `order_nr`) as returnorders
from `tbl_order_detail` 
where   (`RETURNED` = '1')
group by `date`) AS tbl_returned on tbl_returned.date=tbl_dailyReport.date
set tbl_dailyReport.ReturnedCOP=tbl_returned.returnrev , tbl_dailyReport.ReturnedOrders=tbl_returned.returnorders;

#Net
update tbl_dailyReport join 
(select date,sum(`unit_price_after_vat`) as NetSalesPreVoucher, SUM(coupon_money_after_vat) as VoucherMktCost,
SUM(`paid_price_after_vat`) as NetSales,count(distinct `order_nr`) as NetOrders,
 (sum(`costo_after_vat`) + sum(`delivery_cost_supplier`))as cogs_on_net_sales,
count(`order_nr`)/count(distinct `order_nr`) as AverageItemsPerBasket,
count(distinct `CustID`) as customers,
SUM(`shipping_fee`) as ShippingFee, 
SUM(`shipping_cost`)as ShippingCost, 
SUM(`payment_cost`) as PaymentCost
from `tbl_order_detail` 
where   (`RETURNED` = '0') AND (`OAC` = '1')
group by `date`) AS tbl_net on tbl_net.date=tbl_dailyReport.date
set tbl_dailyReport.NetSalesPreVoucher=tbl_net.NetSalesPreVoucher , tbl_dailyReport.VoucherMktCost=tbl_net.VoucherMktCost,
tbl_dailyReport.NetSales=tbl_net.NetSales,tbl_dailyReport.NetOrders=tbl_net.NetOrders,
tbl_dailyReport.cogs_on_net_sales=tbl_net.cogs_on_net_sales,tbl_dailyReport.AverageItemsPerBasket=tbl_net.AverageItemsPerBasket,
tbl_dailyReport.customers=tbl_net.customers,
tbl_dailyReport.ShippingFee=tbl_net.ShippingFee/1.16,
tbl_dailyReport.ShippingCost=tbl_net.ShippingCost,tbl_dailyReport.PaymentCost=tbl_net.PaymentCost;

 
#Categorías
#Electrodomésticos
update tbl_dailyReport join 
(select date,sum(`paid_price_after_vat`) as netsales,
(sum(`costo_after_vat`) + sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.16) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` = 'Electrodomésticos')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_Appiliances=tbl_cat.netsales , tbl_dailyReport.cogs_Appiliances=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.ShippingFee_Appliances=tbl_cat.shipping_fee, tbl_dailyReport.ShippingCost_Appliances=tbl_cat.shipping_cost,
tbl_dailyReport.WH_Cost_Appliances=tbl_cat.WH_cost, tbl_dailyReport.CS_Cost_Appliances=tbl_cat.CS_cost,
tbl_dailyReport.Payment_Fees_Appliances=tbl_cat.payment_fee;

#Cámaras y Fotografía
update tbl_dailyReport join 
(select date,sum(`paid_price_after_vat`) as netsales,
(sum(`costo_after_vat`) + sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.16) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` = 'Cámaras y Fotografía')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_Photography=tbl_cat.netsales , tbl_dailyReport.cogs_Photography=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.ShippingFee_Photography=tbl_cat.shipping_fee, tbl_dailyReport.ShippingCost_Photography=tbl_cat.shipping_cost,
tbl_dailyReport.WH_Cost_Photography=tbl_cat.WH_cost, tbl_dailyReport.CS_Cost_Photography=tbl_cat.CS_cost,
tbl_dailyReport.Payment_Fees_Photography=tbl_cat.payment_fee;


#TV, Video y Audio
update tbl_dailyReport join 
(select date,sum(`paid_price_after_vat`) as netsales,
(sum(`costo_after_vat`) + sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.16) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` ='TV, Video y Audio')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_TV_Audio_Video=tbl_cat.netsales , tbl_dailyReport.cogs_TV_Audio_Video=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.ShippingFee_TV_Audio_Video=tbl_cat.shipping_fee, tbl_dailyReport.ShippingCost_TV_Audio_Video=tbl_cat.shipping_cost,
tbl_dailyReport.WH_Cost_TV_Audio_Video=tbl_cat.WH_cost, tbl_dailyReport.CS_Cost_TV_Audio_Video=tbl_cat.CS_cost,
tbl_dailyReport.Payment_Fees_TV_Audio_Video=tbl_cat.payment_fee;

#Libros
update tbl_dailyReport join 
(select date,sum(`paid_price_after_vat`) as netsales,
(sum(`costo_after_vat`) + sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.16) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` = 'Libros, Oficina y Papelería')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_Books=tbl_cat.netsales , tbl_dailyReport.cogs_Books=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.ShippingFee_Books=tbl_cat.shipping_fee, tbl_dailyReport.ShippingCost_Books=tbl_cat.shipping_cost,
tbl_dailyReport.WH_Cost_Books=tbl_cat.WH_cost, tbl_dailyReport.CS_Cost_Books=tbl_cat.CS_cost,
tbl_dailyReport.Payment_Fees_Books=tbl_cat.payment_fee;

#Videojuegos
update tbl_dailyReport join 
(select date,sum(`paid_price_after_vat`) as netsales,
(sum(`costo_after_vat`) + sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.16) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` = 'Videojuegos')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_Videogames=tbl_cat.netsales , tbl_dailyReport.cogs_Videogames=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.ShippingFee_Videogames=tbl_cat.shipping_fee, tbl_dailyReport.ShippingCost_Videogames=tbl_cat.shipping_cost,
tbl_dailyReport.WH_Cost_Videogames=tbl_cat.WH_cost, tbl_dailyReport.CS_Cost_Videogames=tbl_cat.CS_cost,
tbl_dailyReport.Payment_Fees_Videogames=tbl_cat.payment_fee;

#Fashion
update tbl_dailyReport join 
(select date,sum(`paid_price_after_vat`) as netsales,
(sum(`costo_after_vat`) + sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.16) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` like '%Reloj%'or `n1` like '%Maletas%' or`n1` like '%Accesorios de Moda%')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_Fashion=tbl_cat.netsales , tbl_dailyReport.cogs_Fashion=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.ShippingFee_Fashion=tbl_cat.shipping_fee, tbl_dailyReport.ShippingCost_Fashion=tbl_cat.shipping_cost,
tbl_dailyReport.WH_Cost_Fashion=tbl_cat.WH_cost, tbl_dailyReport.CS_Cost_Fashion=tbl_cat.CS_cost,
tbl_dailyReport.Payment_Fees_Fashion=tbl_cat.payment_fee;


#Cuidado Personal
update tbl_dailyReport join 
(select date,sum(`paid_price_after_vat`) as netsales,
(sum(`costo_after_vat`) + sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.16) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` like '%Cuidado Personal%')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_Health_Beauty=tbl_cat.netsales , tbl_dailyReport.cogs_Health_Beauty=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.ShippingFee_Health_Beauty=tbl_cat.shipping_fee, tbl_dailyReport.ShippingCost_Health_Beauty=tbl_cat.shipping_cost,
tbl_dailyReport.WH_Cost_Health_Beauty=tbl_cat.WH_cost, tbl_dailyReport.CS_Cost_Health_Beauty=tbl_cat.CS_cost,
tbl_dailyReport.Payment_Fees_Health_Beauty=tbl_cat.payment_fee;


#Teléfonos y GPS
update tbl_dailyReport join 
(select date,sum(`paid_price_after_vat`) as netsales,
(sum(`costo_after_vat`) + sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.16) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` = 'Teléfonos y GPS')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_Cellphones=tbl_cat.netsales , tbl_dailyReport.cogs_Cellphones=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.ShippingFee_Cellphones=tbl_cat.shipping_fee, tbl_dailyReport.ShippingCost_Cellphones=tbl_cat.shipping_cost,
tbl_dailyReport.WH_Cost_Cellphones=tbl_cat.WH_cost, tbl_dailyReport.CS_Cost_Cellphones=tbl_cat.CS_cost,
tbl_dailyReport.Payment_Fees_Cellphones=tbl_cat.payment_fee;


 
#Computadores y Tablets    
update tbl_dailyReport join 
(select date,sum(`paid_price_after_vat`) as netsales,
(sum(`costo_after_vat`) + sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.16) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` = 'Computadores y Tablets')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_Computing=tbl_cat.netsales , tbl_dailyReport.cogs_Computing=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.ShippingFee_Computing=tbl_cat.shipping_fee, tbl_dailyReport.ShippingCost_Computing=tbl_cat.shipping_cost,
tbl_dailyReport.WH_Cost_Computing=tbl_cat.WH_cost, tbl_dailyReport.CS_Cost_Computing=tbl_cat.CS_cost,
tbl_dailyReport.Payment_Fees_Computing=tbl_cat.payment_fee;


#Hogar y Muebles   
update tbl_dailyReport join 
(select date,sum(`paid_price_after_vat`) as netsales,
(sum(`costo_after_vat`) + sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.16) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` = 'Hogar y Muebles')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_Hogar=tbl_cat.netsales , tbl_dailyReport.cogs_Hogar=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.ShippingFee_Hogar=tbl_cat.shipping_fee, tbl_dailyReport.ShippingCost_Hogar=tbl_cat.shipping_cost,
tbl_dailyReport.WH_Cost_Hogar=tbl_cat.WH_cost, tbl_dailyReport.CS_Cost_Hogar=tbl_cat.CS_cost,
tbl_dailyReport.Payment_Fees_Hogar=tbl_cat.payment_fee;


#Juguetes, Niños y Bebés 
 update tbl_dailyReport join 
(select date,sum(`paid_price_after_vat`) as netsales,
(sum(`costo_after_vat`) + sum(`delivery_cost_supplier`))as cogs_on_net_sales,
sum(shipping_fee/1.16) as shipping_fee,
sum(shipping_cost) as shipping_cost,
sum(WH) as WH_cost,
sum(CS) as CS_cost,
sum(Payment_Cost) as payment_fee
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') AND(`n1` ='Juguetes, Niños y Bebés')
group by `date`) AS tbl_cat on tbl_cat.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_Kids_Babies=tbl_cat.netsales , tbl_dailyReport.cogs_Kids_Babies=tbl_cat.cogs_on_net_sales,
tbl_dailyReport.ShippingFee_Kids_Babies=tbl_cat.shipping_fee, tbl_dailyReport.ShippingCost_Kids_Babies=tbl_cat.shipping_cost,
tbl_dailyReport.WH_Cost_Kids_Babies=tbl_cat.WH_cost, tbl_dailyReport.CS_Cost_Kids_Babies=tbl_cat.CS_cost,
tbl_dailyReport.Payment_Fees_Kids_Babies=tbl_cat.payment_fee;

#PaidChannels
 update tbl_dailyReport join 
(select date,sum(`paid_price_after_vat`) as net_sales_paid_channel,
sum(coupon_money_after_vat) as voucher_paid_channel
from `tbl_order_detail` 
where  (`OAC` = '1') AND (`RETURNED` = '0') 
AND   ((`source/medium` like '%Price Comparison%')
  or (`source/medium` like '%socialmediaads%')
  or (`source/medium` like '%cpc%')
  or (`source/medium` like '%sociomantic%'))
group by `date`) AS tbl_paid on tbl_paid.date=tbl_dailyReport.date
set tbl_dailyReport.net_sales_paid_channel=tbl_paid.net_sales_paid_channel
 , tbl_dailyReport.voucher_paid_channel=tbl_paid.voucher_paid_channel;

# New Customers
update tbl_dailyReport join  
(select firstOrder as date,COUNT(*) as newCustomers from `view_cohort` 
group by firstOrder) as tbl_nc on tbl_nc.date=tbl_dailyReport.date
set tbl_dailyReport.newcustomers=tbl_nc.newCustomers;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `daily_routine`()
BEGIN
SELECT  'Daily routine: Start',now();

SELECT  'Daily routine: Tbl_catalog_product START',now();
#Table catalog_product_v2
DROP TABLE IF EXISTS simple_variation;
CREATE TABLE simple_variation as (select * from simple_variation_view);
ALTER TABLE `simple_variation` ADD PRIMARY KEY (`fk_catalog_simple`) ;

#tbl_catalog_stock
truncate tbl_catalog_product_stock;
insert into tbl_catalog_product_stock(fk_catalog_simple,sku,reservedbob,stockbob,availablebob)
select bob_live_co.catalog_simple.id_catalog_simple,catalog_simple.sku,
IF(sum(is_reserved) is null, 0, sum(is_reserved))  as reservedBOB ,catalog_stock.quantity as availableBOB,
catalog_stock.quantity-IF(sum(is_reserved) is null, 0, sum(is_reserved)) as disponible
from 
(bob_live_co.catalog_simple join bob_live_co.catalog_stock on catalog_simple.id_catalog_simple=catalog_stock.fk_catalog_simple) left join bob_live_co.sales_order_item
 on sales_order_item.sku=catalog_simple.sku
group by catalog_simple.sku
order by catalog_stock.quantity desc;
DROP TABLE IF EXISTS tbl_catalog_product_v3;
CREATE TABLE tbl_catalog_product_v3 as (select * from catalog_product);
update tbl_catalog_product_v3  set visible='NO' where cat1 is null;

#actualiza lo recientamente reservado, desde el 01 de enero
#UPDATE tbl_catalog_product_stock INNER JOIN (select tbl_catalog_product_stock.fk_catalog_simple,
#IF(sum(is_reserved) is null, 0, sum(is_reserved))  as recently_reserved
#from 
#tbl_catalog_product_stock inner join bob_live_co.sales_order_item on tbl_catalog_product_stock.sku=sales_order_item.sku
#where date(sales_order_item.created_at)>'2013-01-01' group by fk_catalog_simple) as a 
#ON  tbl_catalog_product_stock.fk_catalog_simple = a.fk_catalog_simple
#SET tbl_catalog_product_stock.recently_reservedbob = a.recently_reserved;


ALTER TABLE `tbl_catalog_product_v3` ADD PRIMARY KEY (`sku`) ;

DROP TABLE IF EXISTS tbl_catalog_product_v2;
ALTER TABLE `tbl_catalog_product_v3` RENAME TO  `tbl_catalog_product_v2` ;

ALTER TABLE `tbl_catalog_product_v2` 
ADD INDEX (`sku_config` ) ;

SELECT  'Daily routine: Tbl_catalog_product OK',now();

#Special Cost
#call special_cost();
SELECT  'Daily routine: tbl_order_detail START',now();

CALL update_tbl_order_detail();

SELECT  'Daily routine: tbl_order_detail END',now();

#Ventas netas mes corrido
insert into tbl_sales_growth_mom_cat
select date(now() - interval 1 day) date, tab.n1, Ventas_netas_t, ventas_netas_t_1, 
cast(((ventas_netas_t/ventas_netas_t_1)-1) as decimal(30,10)) MoM from (
select n1,sum(paid_price_after_vat) +
 sum(if(shipping_fee_after_vat is null, shipping_fee_after_vat,0)) Ventas_netas_t
 from tbl_order_detail 
where date>= date(now() - interval 1 day - interval day(now() - interval 1 day) day) 
and date <= now() - interval 1 day 
 and month(date) = month(now() - interval 1 day) and oac = 1 and returned = 0 
group by n1) tab inner join 
(select n1,sum(paid_price_after_vat) + 
sum(if(shipping_fee_after_vat is null, shipping_fee_after_vat,0)) Ventas_netas_t_1
 from tbl_order_detail where date<= date(now() - interval 1 day - interval 1 month)
 and date >= date(now() - interval 1 day- interval 1 month - interval day(now() - interval 1 day) day ) 
and month(date) = month(now() - interval 1 day) - 1
 and oac = 1 and returned = 0 group by n1) tab2
on tab.n1 = tab2.n1;

insert into tbl_sales_growth_mom_buyer
select date(now() - interval 1 day) date, tab.buyer, Ventas_netas_t, ventas_netas_t_1, 
cast(((ventas_netas_t/ventas_netas_t_1)-1) as decimal(30,10)) MoM from (
select buyer,sum(paid_price_after_vat) 
+ sum(if(shipping_fee_after_vat is null, shipping_fee_after_vat,0)) Ventas_netas_t
 from tbl_order_detail 
where date>= date(now() - interval 1 day - interval day(now() - interval 1 day) day) and date <= now() - interval 1 day 
 and month(date) = month(now() - interval 1 day) and oac = 1 and returned = 0
 group by buyer) tab
 inner join 
(select buyer, sum(paid_price_after_vat) + sum(if(shipping_fee_after_vat is null,
 shipping_fee_after_vat,0)) Ventas_netas_t_1
 from tbl_order_detail where date<= date(now() - interval 1 day - interval 1 month)
 and date >= date(now() - interval 1 day - interval 1 month - interval day(now() - interval 1 day) day ) 
and month(date) =  month(now() - interval 1 day) - 1
 and oac = 1 and returned = 0 group by buyer) tab2
on tab.buyer = tab2.buyer;

SELECT  'bireporting: START',now();

#Sales Cube
-- call bireporting.etl_fact_sales_comercial();

SELECT  'bireporting: OK',now();


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `monthly_marketing_2`()
BEGIN

#yrmonth

select @yrmonth := concat(year(date_sub(curdate(), interval 1 day)),if(month(date_sub(curdate(), interval 1 day))<10,concat(0,month(date_sub(curdate(), interval 1 day))),month(date_sub(curdate(), interval 1 day))));

DELETE FROM tbl_monthly_marketing WHERE yrmonth=@yrmonth;

INSERT IGNORE INTO tbl_monthly_marketing (month,channel) 
SELECT DISTINCT month(now()), channel_group FROM tbl_order_detail WHERE channel_group is not null and yrmonth=@yrmonth;

update tbl_monthly_marketing
set yrmonth = concat(year(date_sub(curdate(), interval 1 day)),if(month(date_sub(curdate(), interval 1 day))<10,concat(0,month(date_sub(curdate(), interval 1 day))),month(date_sub(curdate(), interval 1 day))))
where month = month (date_sub(curdate(), interval 1 day));

#VISITS	

UPDATE tbl_monthly_marketing  left JOIN (SELECT yrmonth yrmonth, reporting_channel, sum(visits) visits
FROM daily_costs_visits_per_channel where daily_costs_visits_per_channel.date < cast(curdate() as date)
GROUP BY yrmonth, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.visits=daily_costs_visits_per_channel.visits 
WHERE tbl_monthly_marketing.yrmonth = @yrmonth
;

#GROSS ORDERS

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, ceil(sum(gross_orders)) gross_orders
FROM tbl_order_detail WHERE OBC = 1 and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=tbl_order_detail.channel_group
SET tbl_monthly_marketing.gross_orders=tbl_order_detail.gross_orders
WHERE tbl_monthly_marketing.yrmonth = @yrmonth
;
#NET ORDERS

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, ceil(sum(net_orders)) net_orders
FROM tbl_order_detail WHERE oac = 1 and returned = 0 and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=tbl_order_detail.channel_group
SET tbl_monthly_marketing.net_orders=tbl_order_detail.net_orders
WHERE tbl_monthly_marketing.yrmonth = @yrmonth
;

#ORDERS NET EXPECTED

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, reporting_channel, sum(net_orders_e) net_orders_e
FROM daily_costs_visits_per_channel where daily_costs_visits_per_channel.date < cast(curdate() as date)
GROUP BY yrmonth, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.orders_net_expected=daily_costs_visits_per_channel.net_orders_e
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#CONVERSION RATE
UPDATE tbl_monthly_marketing SET conversion_rate=IF(gross_orders/visits IS NULL, 0, gross_orders/visits);

#PENDING REVENUE
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, 
(SUM(actual_paid_price_after_tax)+SUM(shipping_fee_after_vat))/15.9 pending_sales
FROM tbl_order_detail WHERE tbl_order_detail.pending=1  and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
SET tbl_monthly_marketing.pending_revenue=tbl_order_detail.pending_sales
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group and tbl_monthly_marketing.yrmonth = @yrmonth;

#GROSS REVENUE
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, 
(SUM(actual_paid_price_after_tax)+SUM(shipping_fee_after_vat))/15.9 gross_sales
FROM tbl_order_detail WHERE tbl_order_detail.obc=1  and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
SET tbl_monthly_marketing.gross_revenue=tbl_order_detail.gross_sales
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group and tbl_monthly_marketing.yrmonth = @yrmonth;

#NET REVENUE
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth, channel_group, 
(SUM(actual_paid_price_after_tax)+SUM(shipping_fee_after_vat))/15.9 net_sales
FROM tbl_order_detail WHERE tbl_order_detail.oac=1 AND tbl_order_detail.returned=0  and tbl_order_detail.date < cast(curdate() as date) 
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
SET tbl_monthly_marketing.net_revenue=tbl_order_detail.net_sales
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group and tbl_monthly_marketing.yrmonth = @yrmonth
;

#NET REVENUE EXPECTED

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, 
reporting_channel, sum(net_rev_e)/15.9 net_rev_e
FROM daily_costs_visits_per_channel where daily_costs_visits_per_channel.date < cast(curdate() as date)
GROUP BY yrmonth, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.net_revenue_expected=daily_costs_visits_per_channel.net_rev_e and tbl_monthly_marketing.yrmonth = @yrmonth
;

UPDATE tbl_monthly_marketing SET net_revenue_expected = net_revenue WHERE net_revenue_expected=0;
UPDATE tbl_monthly_marketing SET net_revenue_expected = net_revenue WHERE net_revenue_expected<net_revenue;

#MONTHLY TARGET

UPDATE tbl_monthly_marketing  
INNER JOIN (SELECT yrmonth yrmonth, channel_group, sum(target_net_sales) monthly_target
FROM daily_targets_per_channel
GROUP BY yrmonth, channel_group) daily_targets_per_channel
ON daily_targets_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_targets_per_channel.channel_group
SET tbl_monthly_marketing.monthly_target=daily_targets_per_channel.monthly_target and tbl_monthly_marketing.yrmonth = @yrmonth;

#MONTHLY DEVIATION
UPDATE tbl_monthly_marketing 
SET 
deviation=if((net_revenue_expected/(monthly_target/(SELECT DAYOFMONTH(LAST_DAY(NOW())))*(SELECT DAY(NOW())-1))-1) IS NULL,
0,(net_revenue_expected/(monthly_target/(SELECT DAYOFMONTH(LAST_DAY(NOW())))*(SELECT DAY(NOW())-1))-1))
where tbl_monthly_marketing.yrmonth = @yrmonth;

#AVG TICKET
UPDATE tbl_monthly_marketing SET avg_ticket=net_revenue/net_orders;

#MARKETING COST

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, 
reporting_channel, sum(cost_local)/15.9 cost
FROM daily_costs_visits_per_channel where daily_costs_visits_per_channel.date < cast(curdate() as date)
GROUP BY yrmonth, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.marketing_cost=daily_costs_visits_per_channel.cost
WHERE tbl_monthly_marketing.yrmonth = @yrmonth
;

#COST/REV
UPDATE tbl_monthly_marketing SET cost_rev=IF(marketing_cost/net_revenue_expected IS NULL, 0, marketing_cost/net_revenue_expected)
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#CPO
UPDATE tbl_monthly_marketing SET cost_per_order=IF(marketing_cost/net_orders IS NULL, 0, marketing_cost/net_orders)
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#CPO TARGET

UPDATE tbl_monthly_marketing  
INNER JOIN (SELECT yrmonth yrmonth, channel_group, MAX(target_cpo) target_cpo
FROM daily_targets_per_channel
GROUP BY yrmonth, channel_group) daily_targets_per_channel
ON daily_targets_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_targets_per_channel.channel_group
SET tbl_monthly_marketing.cost_per_order_target=daily_targets_per_channel.target_cpo
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#CPO VS TARGET
UPDATE tbl_monthly_marketing SET cpo_vs_target_cpo=(cost_per_order/cost_per_order_target)-1 WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#NEW CLIENTS
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, sum(new_customers) nc
FROM tbl_order_detail where oac = 1 and returned = 0 and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=tbl_order_detail.channel_group
SET tbl_monthly_marketing.new_clients=tbl_order_detail.nc WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#NEW CLIENTS EXPECTED
UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, 
reporting_channel, sum(new_customers_e) nc_e
FROM daily_costs_visits_per_channel 
where daily_costs_visits_per_channel.date < cast(curdate() as date)
GROUP BY yrmonth, reporting_channel) daily_costs_visits_per_channel
ON daily_costs_visits_per_channel.yrmonth=tbl_monthly_marketing.yrmonth 
AND tbl_monthly_marketing.channel=daily_costs_visits_per_channel.reporting_channel
SET tbl_monthly_marketing.new_clientes_expected=daily_costs_visits_per_channel.nc_e WHERE tbl_monthly_marketing.yrmonth = @yrmonth;



UPDATE tbl_monthly_marketing SET new_clientes_expected=new_clients WHERE new_clientes_expected=0 AND tbl_monthly_marketing.yrmonth = @yrmonth;

#CAC
UPDATE tbl_monthly_marketing SET client_acquisition_cost=marketing_cost/new_clientes_expected WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#PC2_COST

UPDATE tbl_monthly_marketing  INNER JOIN (SELECT yrmonth yrmonth, channel_group, 
(SUM(ifnull(costo_after_vat,0))+
SUM(ifnull(delivery_cost_supplier,0))+SUM(ifnull(wh,0))+SUM(ifnull(cs,0))+
SUM(ifnull(payment_cost,0))+SUM(ifnull(shipping_cost,0)))/15.9 
pc2_costs
FROM production.tbl_order_detail WHERE tbl_order_detail.oac=1 AND tbl_order_detail.returned=0  and tbl_order_detail.date < cast(curdate() as date)
GROUP BY yrmonth, channel_group) tbl_order_detail
ON tbl_order_detail.yrmonth=tbl_monthly_marketing.yrmonth 
SET tbl_monthly_marketing.pc2_costs=tbl_order_detail.pc2_costs
WHERE tbl_monthly_marketing.channel=tbl_order_detail.channel_group AND tbl_monthly_marketing.yrmonth = @yrmonth;

#PC2
UPDATE tbl_monthly_marketing SET profit_contribution_2= 1-(pc2_costs/net_revenue) WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

#MOM

UPDATE tbl_monthly_marketing INNER JOIN tbl_mom_net_sales_per_channel 
ON tbl_monthly_marketing.channel = tbl_mom_net_sales_per_channel.channel_group
AND tbl_monthly_marketing.yrmonth = tbl_mom_net_sales_per_channel.yrmonth
SET tbl_monthly_marketing.month_over_month = tbl_mom_net_sales_per_channel.month_over_month WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

INSERT IGNORE INTO tbl_monthly_marketing (month, channel) 
VALUES(month(date_add(now(), interval -1 day)), 'MTD');

update tbl_monthly_marketing
set yrmonth = concat(year(date_sub(curdate(), interval 1 day)),if(month(date_sub(curdate(), interval 1 day))<10,concat(0,month(date_sub(curdate(), interval 1 day))),month(date_sub(curdate(), interval 1 day))))
WHERE tbl_monthly_marketing.yrmonth = @yrmonth;

UPDATE tbl_monthly_marketing INNER JOIN tbl_mom_net_sales_per_channel 
ON tbl_monthly_marketing.channel = tbl_mom_net_sales_per_channel.channel_group
AND tbl_monthly_marketing.yrmonth = tbl_mom_net_sales_per_channel.yrmonth
SET tbl_monthly_marketing.month_over_month = tbl_mom_net_sales_per_channel.month_over_month
where channel_group = 'MTD' AND tbl_monthly_marketing.yrmonth = @yrmonth;

#Adjusted revenue
#Net
update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion,
 sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth, l.about_linio,t.channel_group,  sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_net_revenue = ifnull(m.net_revenue + t.adjusted_net_revenue/15.9, m.net_revenue)
where m.channel <> 'Tele Sales' AND m.yrmonth = @yrmonth;

update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_net_revenue = ifnull( t.adjusted_net_revenue/15.9,0)
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth ;

#Gross
update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and obc = 1 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_gross_revenue = ifnull(m.gross_revenue + t.adjusted_net_revenue/15.9, m.gross_revenue)
where m.channel <> 'Tele Sales'  AND m.yrmonth = @yrmonth;

update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and obc = 1 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_gross_revenue = ifnull( t.adjusted_net_revenue/15.9,0)
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth;

#Pending
update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and pending = 1 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_pending_revenue = ifnull(m.pending_revenue + t.adjusted_net_revenue/15.9, m.pending_revenue)
where m.channel <> 'Tele Sales' AND m.yrmonth = @yrmonth;

update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(actual_paid_price_after_tax) + sum(shipping_fee_after_vat) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and pending = 1 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_pending_revenue = ifnull( t.adjusted_net_revenue/15.9,0)
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth;

#Net ORders
update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, 
sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  count(distinct orderID) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, count(distinct orderID) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, count(distinct orderID) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_net_orders = ifnull(m.net_orders + t.adjusted_net_revenue, m.net_orders)
where m.channel <> 'Tele Sales' AND m.yrmonth = @yrmonth;

update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, 
sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  count(distinct orderID) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio, count(distinct orderID) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, count(distinct orderID) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_net_orders = t.adjusted_net_revenue
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth;

#New clients
update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, 
sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group,  sum(new_customers) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio,  sum(new_customers) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(new_customers) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_new_clients = ifnull(m.new_clients + t.adjusted_net_revenue, m.new_clients)
where m.channel <> 'Tele Sales' AND m.yrmonth = @yrmonth;

update tbl_monthly_marketing m left join 
(
select t1.yrmonth, t1.channel_group, sum(t1.totalChannel) totalChannel, sum(t1.totalChannel)/sum(t2.totalAbout) as proporcion, 
sum(t3.netSales) netSales, sum(t1.totalChannel)/sum(t2.totalAbout)*sum(t3.netSales) adjusted_net_revenue
from
(select t.yrmonth yrmonth, l.about_linio,t.channel_group, sum(new_customers) as totalChannel
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio, t.channel_group) t1 inner join
(select t.yrmonth yrmonth, l.about_linio,  sum(new_customers) as totalAbout 
from tbl_order_detail t left join tbl_about_linio l on t.channel_group = l.channel_group
where oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by t.yrmonth desc,l.about_linio) t2 on t1.yrmonth = t2.yrmonth and t1.about_linio = t2.about_linio
inner join
(select yrmonth yrmonth, ifnull(t.about_linio, 'Tele Sales') about_linio, sum(new_customers) netSales
from tbl_order_detail t 
where t.channel_Group = 'Tele Sales' and oac = 1 and returned = 0 and t.date < cast(curdate() as date)
group by yrmonth desc, t.about_linio) t3 on t3.yrmonth = t2.yrmonth and t3.about_linio = t2.about_linio
group by t1.yrmonth, t1.channel_group) t 
on m.yrmonth = t.yrmonth and m.channel = t.channel_group set m.adjusted_new_clients = t.adjusted_net_revenue
where m.channel = 'Tele Sales' AND m.yrmonth = @yrmonth;


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `product_pricing_history`()
begin

delete from production_co.product_pricing_history;

insert into production_co.product_pricing_history(date, sku_config, sku_simple, product_name, price) select date, sku_config, sku_simple, product_name, price from production_co.catalog_history where date>= '2013-07-22' and visible=1;

update production_co.product_pricing_history p inner join bob_live_co.catalog_config conf on p.sku_config=conf.sku inner join bob_live_co.catalog_attribute_option_global_category c on c.id_catalog_attribute_option_global_category=conf.fk_catalog_attribute_option_global_category set p.category = c.name;

update production_co.product_pricing_history p inner join bob_live_co.catalog_config conf on p.sku_config=conf.sku inner join bob_live_co.catalog_attribute_option_global_sub_category c on c.id_catalog_attribute_option_global_sub_category=conf.fk_catalog_attribute_option_global_sub_category set p.sub_category = c.name;

update production_co.product_pricing_history p inner join bob_live_co.catalog_config conf on p.sku_config=conf.sku inner join bob_live_co.catalog_attribute_option_global_sub_sub_category c on c.id_catalog_attribute_option_global_sub_sub_category=conf.fk_catalog_attribute_option_global_sub_sub_category set p.sub_sub_category = c.name;

update production_co.product_pricing_history p inner join bob_live_co.catalog_config conf on p.sku_config=conf.sku inner join bob_live_co.catalog_attribute_option_global_buyer c on c.id_catalog_attribute_option_global_buyer=conf.fk_catalog_attribute_option_global_buyer set p.buyer = c.name;

update production_co.product_pricing_history p set PC1 = (select sum(PC1) from production_co.tbl_order_detail t where oac=1 and p.sku_simple=t.sku and t.date=p.date group by t.sku);

update production_co.product_pricing_history p set PC2 = (select sum(PC2) from production_co.tbl_order_detail t where oac=1 and p.sku_simple=t.sku and t.date=p.date group by t.sku);

update production_co.product_pricing_history p set number_sales = (select count(sku) from production_co.tbl_order_detail t where oac=1 and p.sku_simple=t.sku and t.date=p.date group by t.sku); 

update production_co.product_pricing_history p inner join bob_live_co.catalog_config c on c.sku=p.sku_config inner join bob_live_co.catalog_brand b on b.id_catalog_brand=c.fk_catalog_brand set p.brand=b.name, p.date_created=c.created_at;

update production_co.product_pricing_history p set net_revenue = (select sum(ifnull(unit_price_after_vat,0)-ifnull(coupon_money_after_vat,0)+ifnull(shipping_fee_after_vat,0)) from production_co.tbl_order_detail t where oac=1 and p.sku_simple=t.sku and t.date=p.date group by t.sku);

update production_co.product_pricing_history  set `% PC1` = PC1/net_revenue, `% PC2` = PC2/net_revenue;

update test_linio.webtrekk_co w inner join production_co.product_pricing_history p on w.date=p.date and w.sku_config=p.sku_config set p.sku_visits=w.sku_visits;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `shipping_cost_order_city`()
BEGIN

truncate tbl_shipping_cost_order_city;

insert into tbl_shipping_cost_order_city(date, orderID, city, sku_config,shipping_cost)
select date,orderId, ciudad, t2.sku_config,sum(shipping_cost) from tbl_order_detail t 
join tbl_catalog_product_v2  t2 on t.sku = t2.sku
where gross_items = 1 and obc = 1
group by date,orderId,t2.sku_config;

update tbl_shipping_cost_order_city join tbl_shipment_zone on city = ciudad
set shipment_zone = fk_zone;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `top_20`()
begin

delete from top_20_weekly;

insert into top_20_weekly(Category_English, Category, Sku, Product_name, Freq, AVGPrice, `PC1.5`)
(select *
from (select 'Home', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0))
from tbl_order_detail t
where week_exit(date) = week_exit(curdate())-1 and year(date)=year(curdate()) and oac = 1 and (n1 = 'Hogar' and n2 != 'Electrodomésticos')
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Kids & Babies', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0))
from tbl_order_detail t
where week_exit(date) = week_exit(curdate())-1 and year(date)=year(curdate()) and oac = 1 and n1 = 'Juguetes, Niños y Bebés'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Books', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0))
from tbl_order_detail t
where week_exit(date) = week_exit(curdate())-1 and year(date)=year(curdate()) and oac = 1 and n1 = 'Libros'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Fashion', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0))
from tbl_order_detail t
where week_exit(date) = week_exit(curdate())-1 and year(date)=year(curdate()) and oac = 1 and n1 = 'linio_fashion'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Audio Video', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0))
from tbl_order_detail t
where week_exit(date) = week_exit(curdate())-1 and year(date)=year(curdate()) and oac = 1 and (n1 = 'Música y Películas' or n2 = 'TV, Video y Audio')
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Photography', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0))
from tbl_order_detail t
where week_exit(date) = week_exit(curdate())-1 and year(date)=year(curdate()) and oac = 1 and (n2 = 'Cámaras y Fotografía')
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Health and Beauty', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0))
from tbl_order_detail t
where week_exit(date) = week_exit(curdate())-1 and year(date)=year(curdate()) and oac = 1 and n1 like '%cuidado personal%'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Cellphones', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0))
from tbl_order_detail t
where week_exit(date) = week_exit(curdate())-1 and year(date)=year(curdate()) and oac = 1 and n2 = 'teléfonos y gps'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Computing', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0))
from tbl_order_detail t
where week_exit(date) = week_exit(curdate())-1 and year(date)=year(curdate()) and oac = 1 and n2 = 'computadores y tablets'
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Appliances', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0))
from tbl_order_detail t
where week_exit(date) = week_exit(curdate())-1 and year(date)=year(curdate()) and oac = 1 and (n2 = 'Electrodomésticos')
group by t.sku) n
order by Freq desc limit 20)
union
(select *
from (select 'Video Games', t.n1, t.sku, t.product_name, count(t.sku) as Freq, avg(t.paid_price_after_vat) as AVGPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0))
from tbl_order_detail t
where week_exit(date) = week_exit(curdate())-1 and year(date)=year(curdate()) and oac = 1 and n2 ='Videojuegos'
group by t.sku) n
order by Freq desc limit 20);

update top_20_weekly set AVGPrice = AVGPrice/2350, `PC1.5` = (`PC1.5`/2350)/Freq;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `top_20_revenue`()
begin

delete from top_20_weekly_revenue;

insert into top_20_weekly_revenue(Category_English, Category, Sku, Product_name, Freq, SUMPrice, `PC1.5`)
(select *
from (select 'Home', t.n1, t.sku, t.product_name, count(t.sku) as Freq, sum(t.paid_price_after_vat) as SUMPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0))
from tbl_order_detail t
where week_exit(date) = week_exit(curdate())-1 and year(date)=year(curdate()) and oac = 1 and (n1 = 'Hogar' and n2 != 'Electrodomésticos')
group by t.sku) n
order by SUMPrice desc limit 20)
union
(select *
from (select 'Kids & Babies', t.n1, t.sku, t.product_name, count(t.sku) as Freq, sum(t.paid_price_after_vat) as SUMPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0))
from tbl_order_detail t
where week_exit(date) = week_exit(curdate())-1 and year(date)=year(curdate()) and oac = 1 and n1 = 'Juguetes, Niños y Bebés'
group by t.sku) n
order by SUMPrice desc limit 20)
union
(select *
from (select 'Books', t.n1, t.sku, t.product_name, count(t.sku) as Freq, sum(t.paid_price_after_vat) as SUMPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0))
from tbl_order_detail t
where week_exit(date) = week_exit(curdate())-1 and year(date)=year(curdate()) and oac = 1 and n1 = 'Libros'
group by t.sku) n
order by SUMPrice desc limit 20)
union
(select *
from (select 'Fashion', t.n1, t.sku, t.product_name, count(t.sku) as Freq, sum(t.paid_price_after_vat) as SUMPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0))
from tbl_order_detail t
where week_exit(date) = week_exit(curdate())-1 and year(date)=year(curdate()) and oac = 1 and n1 = 'linio_fashion'
group by t.sku) n
order by SUMPrice desc limit 20)
union
(select *
from (select 'Audio Video', t.n1, t.sku, t.product_name, count(t.sku) as Freq, sum(t.paid_price_after_vat) as SUMPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0))
from tbl_order_detail t
where week_exit(date) = week_exit(curdate())-1 and year(date)=year(curdate()) and oac = 1 and (n1 = 'Música y Películas' or n2 = 'TV, Video y Audio')
group by t.sku) n
order by SUMPrice desc limit 20)
union
(select *
from (select 'Photography', t.n1, t.sku, t.product_name, count(t.sku) as Freq, sum(t.paid_price_after_vat) as SUMPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0))
from tbl_order_detail t
where week_exit(date) = week_exit(curdate())-1 and year(date)=year(curdate()) and oac = 1 and (n2 = 'Cámaras y Fotografía')
group by t.sku) n
order by SUMPrice desc limit 20)
union
(select *
from (select 'Health and Beauty', t.n1, t.sku, t.product_name, count(t.sku) as Freq, sum(t.paid_price_after_vat) as SUMPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0))
from tbl_order_detail t
where week_exit(date) = week_exit(curdate())-1 and year(date)=year(curdate()) and oac = 1 and n1 like '%cuidado personal%'
group by t.sku) n
order by SUMPrice desc limit 20)
union
(select *
from (select 'Cellphones', t.n1, t.sku, t.product_name, count(t.sku) as Freq, sum(t.paid_price_after_vat) as SUMPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0))
from tbl_order_detail t
where week_exit(date) = week_exit(curdate())-1 and year(date)=year(curdate()) and oac = 1 and n2 = 'teléfonos y gps'
group by t.sku) n
order by SUMPrice desc limit 20)
union
(select *
from (select 'Computing', t.n1, t.sku, t.product_name, count(t.sku) as Freq, sum(t.paid_price_after_vat) as SUMPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0))
from tbl_order_detail t
where week_exit(date) = week_exit(curdate())-1 and year(date)=year(curdate()) and oac = 1 and n2 = 'computadores y tablets'
group by t.sku) n
order by SUMPrice desc limit 20)
union
(select *
from (select 'Appliances', t.n1, t.sku, t.product_name, count(t.sku) as Freq, sum(t.paid_price_after_vat) as SUMPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0))
from tbl_order_detail t
where week_exit(date) = week_exit(curdate())-1 and year(date)=year(curdate()) and oac = 1 and (n2 = 'Electrodomésticos')
group by t.sku) n
order by SUMPrice desc limit 20)
union
(select *
from (select 'Video Games', t.n1, t.sku, t.product_name, count(t.sku) as Freq, sum(t.paid_price_after_vat) as SUMPrice, sum(ifnull(paid_price_after_vat,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0)-ifnull(shipping_cost,0)-ifnull(payment_cost,0))
from tbl_order_detail t
where week_exit(date) = week_exit(curdate())-1 and year(date)=year(curdate()) and oac = 1 and n2 ='Videojuegos'
group by t.sku) n
order by SUMPrice desc limit 20);

update top_20_weekly_revenue set SUMPrice = SUMPrice/2350, `PC1.5` = (`PC1.5`/2350)/Freq;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `update_tbl_order_detail`()
BEGIN


DECLARE itemID Integer;

SELECT  'Update tbl_order_detail: Start',now();


select @last_date:=max(date) from tbl_order_detail;

#Update Status
update tbl_order_detail join bob_live_co.sales_order_item ON id_sales_order_item=Item join bob_live_co.sales_order_item_status on fk_sales_order_item_status=id_sales_order_item_status
set status_item= sales_order_item_status.name;

#Update OBC, OAC, PENDING, CANCEL, RETURNED
update tbl_order_detail join status_bob 
on tbl_order_detail.payment_method=status_bob.payment_method 
and tbl_order_detail.status_item=status_bob.status_bob
SET tbl_order_detail.OBC=status_bob.OBC, 
tbl_order_detail.OAC=status_bob.OAC,
tbl_order_detail.PENDING=status_bob.PENDING,
tbl_order_detail.CANCEL=status_bob.CANCEL,
tbl_order_detail.RETURNED=status_bob.RETURNED;

#Update Categories
update tbl_order_detail join tbl_catalog_product_v2 
on tbl_order_detail.sku=tbl_catalog_product_v2.sku 
SET n1=cat1,n2=cat2,n3=cat3;

#Insert new data
set itemID = (select item from tbl_order_detail order by Item desc limit 1);

insert into tbl_order_detail (CustID,OrderID,order_nr,payment_method,unit_price,paid_price,
coupon_money_value,coupon_code,date,hour,sku,Item,
status_item,OBC,PENDING,CANCEL,OAC,RETURNED,n1,n2,n3,
tax_percent,unit_price_after_vat,
paid_price_after_vat,coupon_money_after_vat,cost_pet,costo_oferta,costo_after_vat,delivery_cost_supplier,
buyer,brand,Product_name,peso,proveedor,ciudad,region,orderShippingFee)
(select sales_order.fk_customer AS CustID,sales_order.id_sales_order AS OrderID,sales_order.order_nr AS order_nr,sales_order.payment_method AS payment_method,
sales_order_item.unit_price AS unit_price,sales_order_item.paid_price AS paid_price,sales_order_item.coupon_money_value AS coupon_money_value,sales_order.coupon_code AS coupon_code,
cast(sales_order.created_at as date) AS date,concat(hour(sales_order.created_at),':',minute(sales_order.created_at),':',second(sales_order.created_at)) AS hour,sales_order_item.sku AS sku,
sales_order_item.id_sales_order_item AS Item,
sales_order_item_status.name AS status_item,
(select status_bob.OBC from status_bob 
where ((status_bob.payment_method = sales_order.payment_method) and (sales_order_item_status.name = status_bob.status_bob)) limit 1) AS OBC,
(select status_bob.PENDING from status_bob where ((status_bob.payment_method = sales_order.payment_method) and 
(sales_order_item_status.name = status_bob.status_bob)) limit 1) AS PENDING,
(select status_bob.CANCEL from status_bob where ((status_bob.payment_method = sales_order.payment_method) and (sales_order_item_status.name = status_bob.status_bob)) limit 1) AS CANCEL,
(select status_bob.OAC from status_bob where ((status_bob.payment_method = sales_order.payment_method) and (sales_order_item_status.name = status_bob.status_bob)) limit 1) AS OAC,
(select status_bob.RETURNED from status_bob where ((status_bob.payment_method = sales_order.payment_method)
and (sales_order_item_status.name = status_bob.status_bob)) limit 1) AS RETURNED,
tbl_catalog_product_v2.cat1 AS n1,
tbl_catalog_product_v2.cat2 AS n2,
tbl_catalog_product_v2.cat3 AS n3,
tbl_catalog_product_v2.tax_percent AS tax_percent,
(sales_order_item.unit_price / (1 + (tbl_catalog_product_v2.tax_percent / 100))) AS unit_price_after_vat,
(sales_order_item.paid_price / (1 + (tbl_catalog_product_v2.tax_percent / 100))) AS paid_price_after_vat,
(sales_order_item.coupon_money_value / (1 + (tbl_catalog_product_v2.tax_percent / 100))) AS coupon_money_after_vat,
tbl_catalog_product_v2.cost AS cost_pet,
IF(tbl_catalog_product_v2.cost<sales_order_item.cost,sales_order_item.cost,0) AS costo_oferta,
sales_order_item.cost AS costo_after_vat,
if(isnull(sales_order_item.delivery_cost_supplier),tbl_catalog_product_v2.inbound,
sales_order_item.delivery_cost_supplier) AS delivery_cost_supplier,
tbl_catalog_product_v2.Buyer AS buyer,
tbl_catalog_product_v2.Brand AS brand,
tbl_catalog_product_v2.product_name AS Product_name,
tbl_catalog_product_v2.product_weight AS peso,
tbl_catalog_product_v2.Supplier AS proveedor,
if(isnull(sales_order_address.municipality),sales_order_address.city,sales_order_address.municipality) AS ciudad,
sales_order_address.region AS region,
sales_order.shipping_amount as orderShippingFee
from ((((bob_live_co.sales_order_item join bob_live_co.sales_order_item_status) join bob_live_co.sales_order) join tbl_catalog_product_v2) join bob_live_co.sales_order_address) 
where
((sales_order_item.fk_sales_order_item_status = sales_order_item_status.id_sales_order_item_status) 
and (sales_order_item.id_sales_order_item > itemID) and (sales_order.id_sales_order = sales_order_item.fk_sales_order) 
and (sales_order.fk_sales_order_address_shipping = sales_order_address.id_sales_order_address)
and (sales_order_item.sku = tbl_catalog_product_v2.sku)
));

#Update source/medium
update tbl_order_detail inner join ga_order 
on tbl_order_detail.order_nr=ga_order.order_nr set `source/medium`= concat(ga_order.source,' / ',ga_order.medium),
tbl_order_detail.campaign=ga_order.campaign where tbl_order_detail.item>=itemID;

call daily_extra_queries(@last_date);

#PC2 
SELECT 'START PC2',NOW();
TRUNCATE tbl_group_order_detail;
INSERT INTO tbl_group_order_detail (SELECT orderid, count(order_nr) as items,
SUM(greatest(cast(replace(IF(isnull(peso),1,peso),',','.') as DECIMAL(21,6)),1.0))as pesoTotal,
SUM(paid_price) as grandTotal
FROM tbl_order_detail WHERE  OAC='1' and RETURNED='0' group by order_nr);	

TRUNCATE tbl_group_order_detail_gross;
INSERT INTO tbl_group_order_detail_gross (SELECT orderid, count(order_nr) as items,
SUM(greatest(cast(replace(IF(isnull(peso),1,peso),',','.') as DECIMAL(21,6)),1.0))as pesoTotal,
SUM(paid_price) as grandTotal
FROM tbl_order_detail WHERE  OBC='1' group by order_nr);	


UPDATE   tbl_order_detail AS t 
LEFT JOIN tbl_group_order_detail as e 
ON e.orderid = t.orderID 
SET t.orderPeso = e.pesoTotal , t.nr_items=e.items , t.orderTotal=e.grandTotal
where t.OAC='1' and t.RETURNED='0';

UPDATE   tbl_order_detail AS t 
LEFT JOIN tbl_group_order_detail_gross as e 
ON e.orderid = t.orderID 
SET t.orderPeso_gross = e.pesoTotal , t.gross_items=e.items , t.orderTotal_gross=e.grandTotal
where t.OBC='1';

UPDATE tbl_order_detail as t
SET gross_orders = if(gross_items is null,0,1/gross_items) where OBC = '1';

UPDATE tbl_order_detail as t
SET net_orders = if(nr_items is null, 0,1/nr_items) where OAC = '1 'and RETURNED = '0';

#PC2: shipping_fee
/*update tbl_order_detail set orderShippingFee=0.0 where orderShippingFee is null;
update tbl_order_detail set shipping_fee=orderShippingFee/CAST(nr_items AS DECIMAL) where OAC='1' and RETURNED='0';
update tbl_order_detail set shipping_fee_2=orderShippingFee/CAST(nr_items AS DECIMAL);

update tbl_order_detail set shipping_fee_after_vat=if(shipping_fee is null, 0, shipping_fee/1.16);
update tbl_order_detail set shipping_fee_after_vat_2=if(shipping_fee_2 is null, 0, shipping_fee_2/1.16);
update tbl_order_detail set grand_total_after_vat=shipping_fee_after_vat_2+paid_price_after_vat;
update tbl_order_detail set gross_grand_total_after_vat=shipping_fee_after_vat_2+paid_price_after_vat WHERE OBC = '1';
update tbl_order_detail set net_grand_total_after_vat=shipping_fee_after_vat+paid_price_after_vat 
WHERE (OAC = 1 and RETURNED = 0);*/

#PC2: CS and WH
update tbl_order_detail set CS=5434/CAST(nr_items AS DECIMAL),WH=2706/CAST(nr_items AS DECIMAL) where date>='2012-12-01' and OAC='1' and RETURNED='0';
update tbl_order_detail set CS=3476/CAST(nr_items AS DECIMAL),WH=4478/CAST(nr_items AS DECIMAL) where date>='2012-11-01' and date<'2012-12-01'  and OAC='1' and RETURNED='0';
update tbl_order_detail set CS=6402/CAST(nr_items AS DECIMAL),WH=3520/CAST(nr_items AS DECIMAL) where date>='2012-10-01' and date<'2012-11-01' and OAC='1' and RETURNED='0';
update tbl_order_detail set CS=6930.0/CAST(nr_items AS DECIMAL),WH=3520.0/CAST(nr_items AS DECIMAL) where date>='2012-07-01' and date<'2012-10-01' and OAC='1' and RETURNED='0';
update tbl_order_detail set CS=6930.0/CAST(nr_items AS DECIMAL),WH=0.0 where date>='2012-05-01' and date<'2012-07-01'  and OAC='1' and RETURNED='0';

#PC2: shipping_cost
update tbl_order_detail set shipping_cost=(SELECT  bogota from shipping_cost where peso=Least(ceil(orderPeso),200))/CAST(nr_items AS DECIMAL) where ciudad like '%bogota%' and OAC='1' and RETURNED='0' and date<='2012-12-11';
update tbl_order_detail set shipping_cost=(SELECT  resto_pais from shipping_cost where peso=Least(ceil(orderPeso),200))/CAST(nr_items AS DECIMAL) where ciudad not like '%bogota%' and OAC='1' and RETURNED='0'and date<='2012-12-11';
update tbl_order_detail set shipping_cost= orderShippingFee/CAST(nr_items AS DECIMAL) where OAC='1' and RETURNED='0' and date>'2012-12-11'; 

#PC2: Payment_cost
update tbl_order_detail set payment_cost=greatest(0.029*(orderTotal+orderShippingFee)+400,2500)/CAST(nr_items AS DECIMAL) where  payment_method ='Pagosonline_Creditcard' and OAC='1' and RETURNED='0';
update tbl_order_detail set payment_cost=greatest(0.019*(orderTotal+orderShippingFee)+400,2500)/CAST(nr_items AS DECIMAL) where  payment_method ='Pagosonline_Pse' and OAC='1' and RETURNED='0';
update tbl_order_detail set payment_cost=(orderTotal+orderShippingFee)*0.01/CAST(nr_items AS DECIMAL) where ciudad like '%bogota%' and  payment_method ='CashOnDelivery_Payment' and OAC='1' and RETURNED='0';
update tbl_order_detail set payment_cost=(orderTotal+orderShippingFee)*0.015/CAST(nr_items AS DECIMAL) where ciudad not like '%bogota%' and  payment_method ='CashOnDelivery_Payment' and OAC='1' and RETURNED='0';
update tbl_order_detail set payment_cost=2000/CAST(nr_items AS DECIMAL) where payment_method ='Consignacion_Payment' and OAC='1' and RETURNED='0'; 

#WH Shipping_cost para drop Shipping
update tbl_order_detail set WH =0.0, shipping_cost=0.0 where sku in (select sku from bob_live_co.catalog_simple where fk_catalog_shipment_type=2);

#Proveedor entrega:
update  tbl_order_detail set delivery_cost_supplier=0.0 where proveedor='IZC MAYORISTA S.A.S';

#NUll FIEDLS
update tbl_order_detail set shipping_fee=0.0 where shipping_fee is null;
update tbl_order_detail set delivery_cost_supplier=0.0 where delivery_cost_supplier is null;
update tbl_order_detail set WH=0.0 where WH is null;
update tbl_order_detail set CS=0.0 where CS is null;
update tbl_order_detail set payment_cost=0.0 where payment_cost is null;
update tbl_order_detail set shipping_cost=0.0 where shipping_cost is null;

#Campaign
update tbl_order_detail set campaign = 'LizaMoran' where campaign = 'liza.moran';

#Shipping_cost
call shipping_cost_order_city();

update tbl_order_detail join(select date, t2.orderID, t2.sku_config, city, shipment_zone, t1.shipping_cost as shipping_cost_menor, t2.shipping_cost, 
if(t1.shipping_cost > 0 and t1.shipping_cost < t2.shipping_cost, '1','0') as r 
from 
tbl_shipment_cost_calculado t1
right join tbl_shipping_cost_order_city t2
on t1.sku_config = t2.sku_config and fk_shipment_zone = shipment_zone
where month(date) >= 2 and year(date) >= 2013
group by  date, t2.orderID, t2.sku_config, city, shipment_zone) tabla
on tbl_order_detail.orderID = tabla.orderID
set tbl_order_detail.shipping_cost = tabla.shipping_cost_menor where r = '1'
and month(tbl_order_detail.date) >= 2 and year(tbl_order_detail.date) >= 2013;

update tbl_order_detail t left join tbl_shipment_zone t2 on t.ciudad = t2.ciudad
set t.fk_shipment_zone = t2.fk_zone where fk_shipment_zone is null;

#Daily Report
call daily_report();


#Tbl_monthly_cohort_sin_groupon
truncate tbl_monthly_cohort_sin_groupon;
insert into tbl_monthly_cohort_sin_groupon(CustId, netSales, orders, firstOrder, lastOrder, idFirstOrder, idLastOrder, frecuencia,
coupon_code,  channel, channel_group)
select T1.*, T2.coupon_code, T2.channel,T2.channel_group from
(select 
        tbl_order_detail.CustID AS CustID,
        sum(tbl_order_detail.paid_price_after_vat) AS netSales,
        count(distinct tbl_order_detail.order_nr) AS orders,
        min(tbl_order_detail.date) AS firstOrder,
        max(tbl_order_detail.date) AS lastOrder,
        min(tbl_order_detail.OrderID) AS idFirstOrder,
        max(tbl_order_detail.OrderID) AS idLastOrder,
        count(distinct tbl_order_detail.date) AS frecuencia
    from
        tbl_order_detail
    where
        ((tbl_order_detail.OAC = '1')
            and (tbl_order_detail.RETURNED = '0'))
    group by tbl_order_detail.CustID
    order by sum(tbl_order_detail.paid_price_after_vat) desc) AS T1
INNER JOIN (select  orderid,coupon_code,channel,channel_group 
from tbl_order_detail where OAC = '1' and returned = '0'and coupon_Code not like 'GR%')
AS T2 ON T1.idFirstOrder = T2.orderid
group by T1.CustID, T2.coupon_code
order by sum(T1.netSales) desc;


#Tbl_monthly_cohort
truncate tbl_monthly_cohort;
insert into tbl_monthly_cohort(CustID,cohort,idFirstOrder,idLastOrder)
select CustId,
cast(concat(year(firstOrder),if(length(month(firstOrder)) < 2,concat('0', month(firstOrder)),month(firstOrder)))as signed) as cohort,
idFirstOrder,idLastOrder from view_cohort;

update tbl_monthly_cohort inner join tbl_order_detail on idFirstOrder = orderID and tbl_monthly_cohort.CustID = tbl_order_detail.CustID
set tbl_monthly_cohort.coupon_code = tbl_order_detail.coupon_code;

update tbl_monthly_cohort set coupon_code = '' where coupon_code is null;

update tbl_monthly_cohort set groupon_check = 'GR' where coupon_code like 'GR%';

update tbl_monthly_cohort set groupon_check = 'NOTGR' where coupon_code not like 'GR%';

#New_customers
update production_co.tbl_order_detail inner join  
(select firstOrder as date,custID as newCustomer, idFirstOrder from production_co.view_cohort ) as tbl_nc on tbl_nc.date=tbl_order_detail.date and tbl_nc.newCustomer = tbl_order_detail.custID and tbl_nc.idFirstOrder = orderID
set tbl_order_detail.new_customers=1/nr_items;

update production_co.tbl_order_detail inner join  
(select firstOrder as date,custID as newCustomer, idFirstOrder from production_co.view_cohort_gross ) as tbl_nc on tbl_nc.date=tbl_order_detail.date and tbl_nc.newCustomer = tbl_order_detail.custID and tbl_nc.idFirstOrder = orderID
set tbl_order_detail.new_customers_gross=1/gross_items;

update production_co.tbl_order_detail
set year = year(date) , month = month(date) , yrmonth = concat(year,if(month<10,concat(0,month),month))
where date >=@last_date;

update production_co.tbl_order_detail set PC1 = ifnull(unit_price_after_vat,0)-ifnull(coupon_money_value,0)+ifnull(shipping_fee_after_vat,0)-ifnull(costo_after_vat,0)-ifnull(delivery_cost_supplier,0);

update production_co.tbl_order_detail set PC2 = PC1-ifnull(payment_cost,0)-ifnull(shipping_cost,0)-ifnull(wh,0)-ifnull(cs,0);

SELECT  'Update tbl_order_detail: END',now();

#Channel CLV
truncate tbl_monthly_cohort_channel;
insert into tbl_monthly_cohort_channel (fk_customer,cohort,idFirstOrder,channel_group)
select tbl_monthly_cohort.CustId as fk_customer,cohort,idFirstOrder,tbl_order_detail.channel_group as channel
from tbl_monthly_cohort left join tbl_order_detail on orderID=idFirstOrder
group by tbl_monthly_cohort.CustID;



END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `visits_costs_channel`()
begin

delete from production.vw_visits_costs_channel;
delete from production.daily_costs_visits_per_channel;

insert into production.vw_visits_costs_channel (date, channel_group, sum_visits, sum_adcost) 
select v.*, if(c.adCost is null, 0, c.adCost) adCost from (
select date, channel_group, sum(if(adCost is null, 0, adCost)) adCost from production.ga_cost_campaign
group by date , channel_group) c right join (
select date, channel_group, sum(visits) visits from production.ga_visits_cost_source_medium
group by date , channel_group) v on c.channel_group = v.channel_group and c.date = v.date
order by c.date desc;

insert into production.daily_costs_visits_per_channel (date, cost_local, reporting_channel, visits) select date, sum_adcost, channel_group, sum_visits from production.vw_visits_costs_channel;

update production.daily_costs_visits_per_channel set month = month(date);

update production.daily_costs_visits_per_channel set reporting_channel = 'Non identified' where reporting_channel is null;

delete from production.media_rev_orders;

insert into production.media_rev_orders(date, channel_group, net_rev, gross_rev, net_orders, gross_orders, new_customers_gross, new_customers) select date, channel_group, sum(net_grand_total_after_vat), sum(gross_grand_total_after_vat), sum(net_orders), sum(gross_orders), sum(new_customers_gross), sum(new_customers) from production.tbl_order_detail group by date, channel_group;

update production.daily_costs_visits_per_channel a inner join production.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.gross_rev = b.gross_rev;

update production.daily_costs_visits_per_channel a inner join production.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.gross_orders = b.gross_orders;

update production.daily_costs_visits_per_channel a inner join production.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.new_customers_gross = b.new_customers_gross;

update production.daily_costs_visits_per_channel a set net_rev_e = (select (avg(net_rev/gross_rev)) from production.media_rev_orders b where b.date between date_sub(a.date, interval 21 day) and date_sub(a.date,interval 7 day) and a.reporting_channel=b.channel_group group by reporting_channel);

update production.daily_costs_visits_per_channel a set net_orders_e = (select (avg(net_orders/gross_orders)) from production.media_rev_orders b where b.date between date_sub(a.date, interval 21 day) and date_sub(a.date,interval 7 day) and a.reporting_channel=b.channel_group group by reporting_channel);

update production.daily_costs_visits_per_channel a set new_customers_e = (select (avg(new_customers/new_customers_gross)) from production.media_rev_orders b where b.date between date_sub(a.date, interval 21 day) and date_sub(a.date,interval 7 day) and a.reporting_channel=b.channel_group group by reporting_channel);

update production.daily_costs_visits_per_channel set net_rev_e = net_rev_e*gross_rev, net_orders_e = net_orders_e*gross_orders, new_customers_e = new_customers_e*new_customers_gross;

update production.daily_costs_visits_per_channel a inner join production.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.net_rev_e = b.net_rev, a.net_orders_e = b.net_orders, a.new_customers_e = b.new_customers where a.date < date_sub(curdate(), interval 14 day);

update production.daily_costs_visits_per_channel a inner join production.media_rev_orders b on a.reporting_channel=b.channel_group and a.date = b.date set a.net_rev_e = case when b.net_rev>a.net_rev_e then b.net_rev else a.net_rev_e end, a.net_orders_e = case when b.net_orders>a.net_orders_e then b.net_orders else a.net_orders_e end, a.new_customers_e = case when b.new_customers>a.new_customers_e then b.new_customers else a.new_customers_e end;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:04:17
