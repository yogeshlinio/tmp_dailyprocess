-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: customer_service_co
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'customer_service_co'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 FUNCTION `backlog`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id_zendex) from bi_ops_tickets where date_creation<=fecha and (date_solved>fecha or date_solved=0)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 FUNCTION `backlog2`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id_zendex) from bi_ops_tickets where cliente=1 and date_creation<=fecha and (date_solved>fecha or date_solved =0)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 FUNCTION `backlog3`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id_zendex) from bi_ops_tickets where responsable ='BACK OFFICE' and date_creation<=fecha and (date_solved>fecha or date_solved =0)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 FUNCTION `backlog4`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id_zendex) from bi_ops_tickets where responsable='COORDINADORES' and date_creation<=fecha and (date_solved>fecha or date_solved =0)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 FUNCTION `backlog5`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id_zendex) from bi_ops_tickets where responsable='OUTBOUND' and date_creation<=fecha and (date_solved>fecha or date_solved =0)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 FUNCTION `backlog6`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id_zendex) from bi_ops_tickets where responsable='SPAM' and date_creation<=fecha and (date_solved>fecha or date_solved =0)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 FUNCTION `backlog7`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id_zendex) from bi_ops_tickets where responsable='LOGISTICA' and date_creation<=fecha and (date_solved>fecha or date_solved =0)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 FUNCTION `backlog8`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id_zendex) from bi_ops_tickets where responsable='FRONT OFFICE' and date_creation<=fecha and (date_solved>fecha or date_solved =0)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 FUNCTION `backlog_after`(fecha date) RETURNS int(10)
BEGIN
declare backlog_after int(10);
SET backlog_after=(select count(id_zendex) from bi_ops_tickets
where date_creation=fecha
and after_time=1) ;
RETURN backlog_after;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 FUNCTION `backlog_after2`(fecha date) RETURNS int(10)
BEGIN
declare backlog_after int(10);
SET backlog_after=(select count(id_zendex) from bi_ops_tickets
where date_creation=fecha
and cliente=1
and responsable='LOGISTICA'
and after_time=1) ;
RETURN backlog_after;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 FUNCTION `backlog_after3`(fecha date) RETURNS int(10)
BEGIN
declare backlog_after int(10);
SET backlog_after=(select count(id_zendex) from bi_ops_tickets
where date_creation=fecha
and responsable ='BACK OFFICE'
and after_time=1) ;
RETURN backlog_after;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 FUNCTION `backlog_after4`(fecha date) RETURNS int(10)
BEGIN
declare backlog_after int(10);
SET backlog_after=(select count(id_zendex) from bi_ops_tickets
where date_creation=fecha
and responsable='COORDINADORES'
and after_time=1) ;
RETURN backlog_after;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 FUNCTION `backlog_after5`(fecha date) RETURNS int(10)
BEGIN
declare backlog_after int(10);
SET backlog_after=(select count(id_zendex) from bi_ops_tickets
where date_creation=fecha
and responsable='OUTBOUND'
and after_time=1) ;
RETURN backlog_after;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 FUNCTION `backlog_after6`(fecha date) RETURNS int(10)
BEGIN
declare backlog_after int(10);
SET backlog_after=(select count(id_zendex) from bi_ops_tickets
where date_creation=fecha
and responsable='SPAM'
and after_time=1) ;
RETURN backlog_after;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 FUNCTION `backlog_after7`(fecha date) RETURNS int(10)
BEGIN
declare backlog_after int(10);
SET backlog_after=(select count(id_zendex) from bi_ops_tickets
where date_creation=fecha
and responsable='LOGISTICA'
and after_time=1) ;
RETURN backlog_after;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 FUNCTION `backlog_after8`(fecha date) RETURNS int(10)
BEGIN
declare backlog_after int(10);
SET backlog_after=(select count(id_zendex) from bi_ops_tickets
where date_creation=fecha
and responsable='FRONT OFFICE'
and after_time=1) ;
RETURN backlog_after;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_bo_total_co`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_co
where date(created_at)<=fecha and 
(date(`Solved at`)>fecha or `Solved at`=0)
and responsable='BACK OFFICE') ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_bo_total_co_interval1`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_co
where date(created_at)<=fecha and 
(date(`Solved at`)>fecha or `Solved at`=0)
and responsable='BACK OFFICE'
and created_at>=DATE_SUB(fecha, INTERVAL 3 day)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_bo_total_co_interval2`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_co
where date(created_at)<=fecha and 
(date(`Solved at`)>fecha or `Solved at`=0)
and responsable='BACK OFFICE'
and created_at<DATE_SUB(fecha, INTERVAL 3 day)
and created_at>=DATE_SUB(fecha, INTERVAL 15 day)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_bo_total_co_interval3`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_co
where date(created_at)<=fecha and 
(date(`Solved at`)>fecha or `Solved at`=0)
and responsable='BACK OFFICE'
and created_at<DATE_SUB(fecha, INTERVAL 15 day)
and created_at>=DATE_SUB(fecha, INTERVAL 1 month)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_bo_total_co_interval4`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_co
where date(created_at)<=fecha and 
(date(`Solved at`)>fecha or `Solved at`=0)
and responsable='BACK OFFICE'
and created_at<DATE_SUB(fecha, INTERVAL 1 month)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_bo_total_mx`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk
where date(`Created_at`)<=fecha 
and 
(date(`Solved_at`)>fecha or `Solved_at`=0)
and
(`Group_` like '%BO%'
            or `Group_` like '%B.O.%') ) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_bo_total_mx_interval1`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk
where date(`Created_at`)<=fecha 
and 
(date(`Solved_at`)>fecha or `Solved_at`=0)
and
(`Group_` like '%BO%'
            or `Group_` like '%B.O.%')
and `Created_at`>=DATE_SUB(fecha, INTERVAL 3 day) ) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_bo_total_mx_interval2`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk
where date(`Created_at`)<=fecha 
and 
(date(`Solved_at`)>fecha or `Solved_at`=0)
and
(`Group_` like '%BO%'
            or `Group_` like '%B.O.%') 
and `Created_at`<DATE_SUB(fecha, INTERVAL 3 day)
and `Created_at`>=DATE_SUB(fecha, INTERVAL 15 day)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_bo_total_mx_interval3`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk
where date(`Created_at`)<=fecha 
and 
(date(`Solved_at`)>fecha or `Solved_at`=0)
and
(`Group_` like '%BO%'
            or `Group_` like '%B.O.%')
and `Created_at`<DATE_SUB(fecha, INTERVAL 15 day)
and `Created_at`>=DATE_SUB(fecha, INTERVAL 1 month) ) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_bo_total_mx_interval4`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk
where date(`Created_at`)<=fecha 
and 
(date(`Solved_at`)>fecha or `Solved_at`=0)
and
(`Group_` like '%BO%'
            or `Group_` like '%B.O.%')
and `Created_at`<DATE_SUB(fecha, INTERVAL 1 month) ) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_bo_total_pe`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_pe
where date(`created_at`)<=fecha and 
(date(`Solved at`)>fecha or `Solved at`=0)
and         `tbl_zendesk_pe`.`Assignee [list]` in ('Angela Campos' , 'Edison Salazar',
            'Juan Rodriguez',
            'Melisa Alejos',
            'Percy Schwarz',
            'Renzo Cisneros',
            'Ruth Belmont',
            'Vanesa Cachique',
            'Renzo Diaz',
            'Lucia Juarez',
            'Agatha Castro',
            'Danayra Heredia',
            'Alex Ipanaque',
            'Thomas Feijo'));
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_bo_total_pe_interval1`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_pe
where date(`created_at`)<=fecha and 
(date(`Solved at`)>fecha or `Solved at`=0)
and         `tbl_zendesk_pe`.`Assignee [list]` in ('Angela Campos' , 'Edison Salazar',
            'Juan Rodriguez',
            'Melisa Alejos',
            'Percy Schwarz',
            'Renzo Cisneros',
            'Ruth Belmont',
            'Vanesa Cachique',
            'Renzo Diaz',
            'Lucia Juarez',
            'Agatha Castro',
            'Danayra Heredia',
            'Alex Ipanaque',
            'Thomas Feijo')
and `created_at`>=DATE_SUB(fecha, INTERVAL 3 day) );
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_bo_total_pe_interval2`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_pe
where date(`created_at`)<=fecha and 
(date(`Solved at`)>fecha or `Solved at`=0)
and         `tbl_zendesk_pe`.`Assignee [list]` in ('Angela Campos' , 'Edison Salazar',
            'Juan Rodriguez',
            'Melisa Alejos',
            'Percy Schwarz',
            'Renzo Cisneros',
            'Ruth Belmont',
            'Vanesa Cachique',
            'Renzo Diaz',
            'Lucia Juarez',
            'Agatha Castro',
            'Danayra Heredia',
            'Alex Ipanaque',
            'Thomas Feijo')
and `created_at`<DATE_SUB(fecha, INTERVAL 3 day)
and `created_at`>=DATE_SUB(fecha, INTERVAL 15 day));
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_bo_total_pe_interval3`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_pe
where date(`created_at`)<=fecha and 
(date(`Solved at`)>fecha or `Solved at`=0)
and         `tbl_zendesk_pe`.`Assignee [list]` in ('Angela Campos' , 'Edison Salazar',
            'Juan Rodriguez',
            'Melisa Alejos',
            'Percy Schwarz',
            'Renzo Cisneros',
            'Ruth Belmont',
            'Vanesa Cachique',
            'Renzo Diaz',
            'Lucia Juarez',
            'Agatha Castro',
            'Danayra Heredia',
            'Alex Ipanaque',
            'Thomas Feijo')
and `created_at`<DATE_SUB(fecha, INTERVAL 15 day)
and `created_at`>=DATE_SUB(fecha, INTERVAL 1 month));
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_bo_total_pe_interval4`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_pe
where date(`created_at`)<=fecha and 
(date(`Solved at`)>fecha or `Solved at`=0)
and         `tbl_zendesk_pe`.`Assignee [list]` in ('Angela Campos' , 'Edison Salazar',
            'Juan Rodriguez',
            'Melisa Alejos',
            'Percy Schwarz',
            'Renzo Cisneros',
            'Ruth Belmont',
            'Vanesa Cachique',
            'Renzo Diaz',
            'Lucia Juarez',
            'Agatha Castro',
            'Danayra Heredia',
            'Alex Ipanaque',
            'Thomas Feijo')
and `created_at`<DATE_SUB(fecha, INTERVAL 1 month));
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_bo_total_ve`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_ve
where date(`created_at`)<=fecha and 
(date(`Solved at`)>fecha or `Solved at`=0)
and `customer_service`.`tbl_zendesk_ve`.`Group` = 'Back Office') ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_bo_total_ve_interval1`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_ve
where date(`created_at`)<=fecha and 
(date(`Solved at`)>fecha or `Solved at`=0)
and `customer_service`.`tbl_zendesk_ve`.`Group` = 'Back Office'
and `created_at`>=DATE_SUB(fecha, INTERVAL 3 day)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_bo_total_ve_interval2`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_ve
where date(`created_at`)<=fecha and 
(date(`Solved at`)>fecha or `Solved at`=0)
and `customer_service`.`tbl_zendesk_ve`.`Group` = 'Back Office'
and `created_at`<DATE_SUB(fecha, INTERVAL 3 day)
and `created_at`>=DATE_SUB(fecha, INTERVAL 15 day) ) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_bo_total_ve_interval3`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_ve
where date(`created_at`)<=fecha and 
(date(`Solved at`)>fecha or `Solved at`=0)
and `customer_service`.`tbl_zendesk_ve`.`Group` = 'Back Office'
and `created_at`<DATE_SUB(fecha, INTERVAL 15 day)
and `created_at`>=DATE_SUB(fecha, INTERVAL 1 month)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_bo_total_ve_interval4`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_ve
where date(`created_at`)<=fecha and 
(date(`Solved at`)>fecha or `Solved at`=0)
and `customer_service`.`tbl_zendesk_ve`.`Group` = 'Back Office'
and `created_at`<DATE_SUB(fecha, INTERVAL 1 month)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 FUNCTION `backlog_by_agent`(fecha date, agent varchar(45)) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id_zendex) from  bi_ops_tickets where date_creation<=fecha
and (date(date_add(created_at,interval first_resolution_time minute))>fecha or first_resolution_time="")
and asignee=agent) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 FUNCTION `backlog_inc_total_co`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_general
where Fecha_creacion<=fecha and 
(Fecha_solucion>fecha or solved=0)
and es_inc_front_office=1 and despues_de_horario<>1
and Pais='CO') ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_inc_total_co_interval1`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_general
where Fecha_creacion<=fecha and 
(Fecha_solucion>fecha or solved=0)
and es_inc_front_office=1 and despues_de_horario<>1
and Pais='CO'
and created_at>=DATE_SUB(fecha, INTERVAL 3 day)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_inc_total_co_interval2`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_general
where Fecha_creacion<=fecha and 
(Fecha_solucion>fecha or solved=0)
and es_inc_front_office=1 and despues_de_horario<>1
and Pais='CO'
and created_at<DATE_SUB(fecha, INTERVAL 3 day)
and created_at>=DATE_SUB(fecha, INTERVAL 15 day)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_inc_total_co_interval3`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_general
where Fecha_creacion<=fecha and 
(Fecha_solucion>fecha or solved=0)
and es_inc_front_office=1 and despues_de_horario<>1
and Pais='CO'
and created_at<DATE_SUB(fecha, INTERVAL 15 day)
and created_at>=DATE_SUB(fecha, INTERVAL 1 month)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_inc_total_co_interval4`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_general
where Fecha_creacion<=fecha and 
(Fecha_solucion>fecha or solved=0)
and es_inc_front_office=1 and despues_de_horario<>1
and Pais='CO'
and created_at<DATE_SUB(fecha, INTERVAL 1 month)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_inc_total_mx`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_general
where Fecha_creacion<=fecha and 
(Fecha_solucion>fecha or solved=0)
and es_inc_front_office=1 and despues_de_horario<>1
and Pais='MX') ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_inc_total_mx_interval1`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_general
where Fecha_creacion<=fecha and 
(Fecha_solucion>fecha or solved=0)
and es_inc_front_office=1 and despues_de_horario<>1
and Pais='MX'   
and `Created_at`>=DATE_SUB(fecha, INTERVAL 3 day)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_inc_total_mx_interval2`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_general
where Fecha_creacion<=fecha and 
(Fecha_solucion>fecha or solved=0)
and es_inc_front_office=1 and despues_de_horario<>1
and Pais='MX'    
and `Created_at`<DATE_SUB(fecha, INTERVAL 3 day)
and `Created_at`>=DATE_SUB(fecha, INTERVAL 15 day)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_inc_total_mx_interval3`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_general
where Fecha_creacion<=fecha and 
(Fecha_solucion>fecha or solved=0)
and es_inc_front_office=1 and despues_de_horario<>1
and Pais='MX'           
and `Created_at`<DATE_SUB(fecha, INTERVAL 15 day)
and `Created_at`>=DATE_SUB(fecha, INTERVAL 1 month)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_inc_total_mx_interval4`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_general
where Fecha_creacion<=fecha and 
(Fecha_solucion>fecha or solved=0)
and es_inc_front_office=1 and despues_de_horario<>1
and Pais='MX'   
and `Created_at`<DATE_SUB(fecha, INTERVAL 1 month)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_inc_total_pe`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_general
where Fecha_creacion<=fecha and 
(Fecha_solucion>fecha or solved=0)
and es_inc_front_office=1 and despues_de_horario<>1
and Pais='PE'   );
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_inc_total_pe_interval1`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_general
where Fecha_creacion<=fecha and 
(Fecha_solucion>fecha or solved=0)
and es_inc_front_office=1 and despues_de_horario<>1
and Pais='PE'
and `created_at`>=DATE_SUB(fecha, INTERVAL 3 day) ) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_inc_total_pe_interval2`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_general
where Fecha_creacion<=fecha and 
(Fecha_solucion>fecha or solved=0)
and es_inc_front_office=1 and despues_de_horario<>1
and Pais='PE'
and `created_at`<DATE_SUB(fecha, INTERVAL 3 day)
and `created_at`>=DATE_SUB(fecha, INTERVAL 15 day)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_inc_total_pe_interval3`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_general
where Fecha_creacion<=fecha and 
(Fecha_solucion>fecha or solved=0)
and es_inc_front_office=1 and despues_de_horario<>1
and Pais='PE'
and `created_at`<DATE_SUB(fecha, INTERVAL 15 day)
and `created_at`>=DATE_SUB(fecha, INTERVAL 1 month)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_inc_total_pe_interval4`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_general
where Fecha_creacion<=fecha and 
(Fecha_solucion>fecha or solved=0)
and es_inc_front_office=1 and despues_de_horario<>1
and Pais='PE'
and `created_at`<DATE_SUB(fecha, INTERVAL 1 month)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_inc_total_ve`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_general
where Fecha_creacion<=fecha and 
(Fecha_solucion>fecha or solved=0)
and es_inc_front_office=1 and despues_de_horario<>1
and Pais='VE') ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_inc_total_ve_interval1`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_general
where Fecha_creacion<=fecha and 
(Fecha_solucion>fecha or solved=0)
and es_inc_front_office=1 and despues_de_horario<>1
and Pais='VE'
and `created_at`>=DATE_SUB(fecha, INTERVAL 3 day) ) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_inc_total_ve_interval2`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_general
where Fecha_creacion<=fecha and 
(Fecha_solucion>fecha or solved=0)
and es_inc_front_office=1 and despues_de_horario<>1
and Pais='VE'
and `created_at`<DATE_SUB(fecha, INTERVAL 3 day)
and `created_at`>=DATE_SUB(fecha, INTERVAL 15 day) ) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_inc_total_ve_interval3`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_general
where Fecha_creacion<=fecha and 
(Fecha_solucion>fecha or solved=0)
and es_inc_front_office=1 and despues_de_horario<>1
and Pais='VE'
and `created_at`<DATE_SUB(fecha, INTERVAL 15 day)
and `created_at`>=DATE_SUB(fecha, INTERVAL 1 month) ) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 FUNCTION `backlog_inc_total_ve_interval4`(fecha date) RETURNS int(10)
BEGIN
declare backlog int(10);
SET backlog=(select count(id) from customer_service.tbl_zendesk_general
where Fecha_creacion<=fecha and 
(Fecha_solucion>fecha or solved=0)
and es_inc_front_office=1 and despues_de_horario<>1
and Pais='VE'
and `created_at`<DATE_SUB(fecha, INTERVAL 1 month)) ;
RETURN backlog;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 FUNCTION `dia_habil_anterior`(fecha date) RETURNS date
BEGIN

set @dias := -1;

set @dia_habil_anterior:= workday_bi(fecha,@dias);

while @dia_habil_anterior = fecha do

set @dias = @dias -1 ;
set @dia_habil_anterior = workday_bi(fecha,@dias);

end while;



RETURN @dia_habil_anterior;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `agents_schedule`()
BEGIN
-- sss

#Zona horaria de Colombia

set time_zone='-5:00';


truncate table  bi_ops_cc_schedule;

SELECT  'Programacion real agentes',now();
insert into bi_ops_cc_schedule (queue_stats_id,uniqueid,datetime,agent,event)
select queue_stats_id,uniqueid,datetime,agent,event from queue_stats a 
inner join qname b on a.qname=b. queue_id
inner join qagent c on a.qagent=c.agent_id
inner join qevent d on a.qevent=d.event_id
where qevent in (2,24)
and date(datetime)>='2013-08-12'
and queue in ('ivr_cola1','ivr_cola2','ivr_cola3')
group by date(datetime),agent,datetime;


truncate table bi_ops_cc_agents_shifts;

SELECT  'Datos de turnos de agentes',now();
insert into bi_ops_cc_agents_shifts(event_date,agent,min,max)
select 
event_date, agent,
min(event_datetime) as min,
max(event_datetime) as max
from bi_ops_cc a  where agent<>'NONE' and event_date>='2013-07-01' and event_date<'2013-08-12'
group by event_date,agent;

UPDATE
bi_ops_cc_agents_shifts a 
inner join bi_ops_cc b on a.max=b.event_datetime and a.agent=b.agent
SET
a.first_call_time=time(a.min),
a.last_call_time=time(date_add(a.max, interval b.call_duration_seg second))
where b.event<>'CONNECT';

UPDATE
bi_ops_cc_agents_shifts  
SET
first_call_time=time(min),
last_call_time=time(max)
where last_call_time is null;

SELECT  'Datos de turnos de agentes de acuerdo a codigos de login y logout',now();
insert into bi_ops_cc_agents_shifts (event_date,agent,min)
select  date(datetime),agent,datetime from  bi_ops_cc_schedule
where event='ADDMEMBER';

update bi_ops_cc_agents_shifts a inner join (select date(datetime) as date,agent,event,max(datetime) from 
bi_ops_cc_schedule where event='REMOVEMEMBER'
group by date(datetime),agent,event) b on a.event_date=b.date 
and a.agent=b.agent
set a.max=b.date;

update bi_ops_cc_agents_shifts
set 
max=addtime(min,'9:00'),
logout=0
where max is null;

UPDATE
bi_ops_cc_agents_shifts 
SET
first_call_time=time(min),
last_call_time=time(max)
where first_call_time is null;


UPDATE
bi_ops_cc_agents_shifts  
SET
total_shift=addtime(last_call_time,-first_call_time);

UPDATE
bi_ops_cc_agents_shifts  
SET
total_shift_hours=hour(total_shift)+(minute(total_shift)/60)+(second(total_shift)/3600);

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `bi_ops_cc`()
BEGIN


SELECT  'Inicio rutina bi_ops_cc',now();

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'customer_service_co.bi_ops_cc',
  'start',
  NOW(),
  MAX(event_date),
  count(*),
  count(*)
FROM
  customer_service_co.bi_ops_cc;

SELECT  'Actualizar campaign y assisted_sales_operator',now();

truncate table customer_service_co.relacion_agentes;

insert into customer_service_co.relacion_agentes
(cod_agente, campaign, sales_operator, name, identification)
select phone,channel, sales_operator,
name, identification  from bi_ops_matrix_ventas
where sales_operator is not null;

SELECT  'Matriz de servicio al cliente',now();

#Zona horaria de Colombia

set time_zone='-5:00';

#Actualizar datos de la matriz de servicio al cliente..
-- ss
update bi_ops_CS_matrix a 
inner join bi_ops_matrix_sac b on a.identification=b.identification
set 
a.phone=b.phone,
a.name=b.name,
a.coordinator=b.coordinator,
a.process=b.process,
a.shift_pattern=b.shift_pattern,
a.birthday=b.birthday,
a.cellphone=b.cellphone,
a.mail=b.mail,
a.start_date=b.start_date,
a.antique=b.antique,
a.status=b.status,
a.date_change_status=b.date_change_status,
a.temporal=b.temporal
;

update bi_ops_CS_matrix a 
inner join bi_ops_matrix_ventas b on a.identification=b.identification
set 
a.phone=b.phone,
a.name=b.name,
a.coordinator=b.coordinator,
a.process=b.process,
a.shift_pattern=b.shift_pattern,
a.birthday=b.birthday,
a.cellphone=b.cellphone,
a.mail=b.mail,
a.start_date=b.start_date,
a.antique=b.antique,
a.status=b.status,
a.date_change_status=b.date_change_status,
a.temporal=b.temporal
where b.process<>'SAC';

insert into bi_ops_CS_matrix
select a.* from bi_ops_matrix_sac a 
left join bi_ops_CS_matrix b on a.identification=b.identification
where b.phone is null and a.identification<>0;

insert into bi_ops_CS_matrix
select a.phone,
a.identification,
a.name,
a.coordinator,
a.process,
a.shift_pattern,
a.birthday,
a.cellphone,
a.mail,
a.start_date,
a.antique,
a.status,
a.date_change_status,
a.temporal from bi_ops_matrix_ventas a 
left join bi_ops_CS_matrix b on a.identification=b.identification
where b.phone is null and a.identification<>0;

SELECT  'Inicio rutina CC Inbound',now();

truncate table new_queue_stats;

insert into new_queue_stats(queue_stats_id,uniqueid,datetime,qname,qagent,qevent,info1,info2,info3,info4,info5) 
SELECT * FROM queue_stats_2013;

insert into new_queue_stats(queue_stats_id,uniqueid,datetime,qname,qagent,qevent,info1,info2,info3,info4,info5) 
SELECT * FROM queue_stats
where datetime>
(select max(datetime) from customer_service_co.queue_stats_2013);

insert into qname2
select * from qname2
where queue_id not in (select queue_id from qname2);

insert into qagent2
select * from qagent2
where agent_id not in (select agent_id from qagent2);


SELECT  'Registro de datos new_queue_stats',now();
update new_queue_stats a inner join qevent b on a.qevent=b.event_id
inner join qname2 c on a.qname=c.queue_id
set call_time=if(event in ('COMPLETEAGENT','COMPLETECALLER'),info2,IF(event='TRANSFER',info4,null)),
hold_time=if(event in ('COMPLETEAGENT','COMPLETECALLER'),info1,if(event in ('ABANDON','EXITWITHTIMEOUT'),info3,if(event='TRANSFER',info3,null))),
start_position= if(event in ('COMPLETEAGENT','COMPLETECALLER'),info3,IF(event='TRANSFER', right(info4,length(info4)-instr(info4,'|')),if(event in ('ABANDON','EXITWITHTIMEOUT'),info2,null))),
end_position=if(event in ('ABANDON','EXITWITHTIMEOUT'),info1,null)
where b.event in ('ABANDON','COMPLETEAGENT','COMPLETECALLER','CONNECT','TRANSFER','EXITWITHTIMEOUT','RINGNOANSWER')
and c.queue in ('ivr_cola1','ivr_cola2','ivr_cola3');

set @max_day=cast((select max(event_date) from bi_ops_cc) as date);

delete from bi_ops_cc
where event_date>=@max_day;
#truncate table bi_ops_cc;

SELECT  'Insertar datos bi_ops_cc',now();
insert into bi_ops_cc (queue_stats_id, uniqueid, event_datetime, event_date, queue, agent, event, call_duration_seg, hold_duration_seg, start_position, end_position,  event_hour, event_minute, event_weekday, event_week, event_month, event_year) 
SELECT a.queue_stats_id, a.uniqueid, a.datetime,date(a.datetime),c.queue,d.agent,b.event, a.call_time,a.hold_time,a.start_position,a.end_position,hour(a.datetime), minute(a.datetime),
weekday(a.datetime),weekofyear(a.datetime),monthname(a.datetime),year(datetime) FROM new_queue_stats a inner join qevent b on a.qevent=b.event_id
inner join qname2 c on a.qname=c.queue_id inner join qagent2 d on a.qagent=d.agent_id 
where b.event in ('ABANDON','COMPLETEAGENT','COMPLETECALLER','CONNECT','TRANSFER','RINGNOANSWER')
and c.queue in ('ivr_cola1','ivr_cola2','ivr_cola3') and date(a.datetime)>=@max_day
group by uniqueid,event,queue;



SELECT  'Actualizar numeros de clientes',now();

truncate table qnumbers;

insert into qnumbers(uniqueid,number,datetime) select uniqueid, info2,datetime from new_queue_stats where qevent = 15 group by uniqueid;

update bi_ops_cc a inner join qnumbers b on a.uniqueid=b.uniqueid 
set 
a.number=b.number,
a.event_datetime=if(event='ABANDON',b.datetime,event_datetime);


SELECT  'Actualizar datos de espera del agente',now();
truncate table qwait;

insert into qwait(uniqueid,datetime,hold_time_agent) select uniqueid, datetime,info3 from new_queue_stats where qevent = 12 group by uniqueid;

update bi_ops_cc a inner join qwait b on a.uniqueid=b.uniqueid 
set 
a.event_datetime=datetime,
a.agent_hold_time_seg=b.hold_time_agent
where event in ('COMPLETEAGENT','COMPLETECALLER','TRANSFER');

SELECT  'Actualizar grabaciones',now();
update bi_ops_cc a inner join cdr b on a.uniqueid=b.uniqueid
set 
a.record=b.record;


truncate table bi_ops_agent_names;

SELECT  'Phones vs ext',now();
insert into bi_ops_agent_names
select a.agent,if(length=1,concat('200',number),if(length=2,concat('20',number),if(length=3,
concat('2',number),null))) as ext from
(SELECT agent,right(agent,length(agent)-9) as 'number',length(right(agent,length(agent)-9)) 
as 'length' FROM qagent where agent like '%phone%' order by agent asc) as a;

insert into bi_ops_agent_names
select a.agent,right(agent, length(agent)-4) as ext from
(SELECT agent,right(agent,length(agent)-9) as 'number',length(right(agent,length(agent)-9)) 
as 'length' FROM qagent where agent like '%SIP%' and agent not like '%phone%' and agent not like '%panda%' 
order by agent asc) as a;

insert into bi_ops_agent_names
SELECT agent, right(agent, length(agent)-6) FROM qagent where agent like 'Agent%' 
order by agent asc;

SELECT  'Insertar nombres',now();
update bi_ops_cc a inner join bi_ops_agent_names b on a.agent=b.agent inner join 
bi_ops_CS_matrix c on b.ext=c.phone
set a.agent_name=c.name,
a.agent_ext=b.ext,
a.agent_identification=c.identification
where c.status='ACTIVO'
and process in ('SAC','VENTAS','INBOUND')
and event_date>'2013-12-09';

update bi_ops_cc a inner join bi_ops_agent_names b on a.agent=b.agent inner join 
bi_ops_CS_matrix c on b.ext=c.phone
set a.agent_name=c.name,
a.agent_ext=b.ext,
a.agent_identification=c.identification
where a.agent_name is null
and event_date>'2013-12-09';

update bi_ops_cc a inner join bi_ops_agent_names b on a.agent=b.agent inner join 
bi_ops_CS_matrix_antext c on b.ext=c.phone
set a.agent_name=c.name,
a.agent_ext=b.ext,
a.agent_identification=c.identification
where c.status='ACTIVO'
and process in ('SAC','VENTAS','INBOUND')
and a.agent_name is null
and event_date<='2013-12-09';

update bi_ops_cc a inner join bi_ops_agent_names b on a.agent=b.agent inner join 
bi_ops_CS_matrix_antext  c on b.ext=c.phone
set a.agent_name=c.name,
a.agent_ext=b.ext,
a.agent_identification=c.identification
where a.agent_name is null
and event_date<='2013-12-09';

update bi_ops_cc a 
set a.agent_name="OTRO" where a.agent_name is null;

update bi_ops_cc 
set 
event_minute=minute(event_datetime),
event_hour=hour(event_datetime)
where event_minute is null;

SELECT  'Actualizar shifts',now();

update bi_ops_cc 
set event_shift=if(event_minute<30,concat(event_hour,':','00'),concat(event_hour,':','30'))
where event_shift is null;


SELECT  'Actualizar holidays',now();
update bi_ops_cc a inner join operations_co.calendar b on a.event_date=b.dt
set holiday=b.isholiday
where holiday is null;

SELECT  'Actualizar horas laborales',now();
update bi_ops_cc
set workinghour=if(event_weekday=6 or holiday=1,
	if(event_hour>=10 and event_hour<18,
		1,0),
		if(event_weekday=5,
			if(event_hour>=9 and event_hour<21,
			1,0),
			if(event_hour>=8 and event_hour<21,
				1,0)))
where event_date>=@max_day
;


SELECT  'Actualizar net_events',now();
update bi_ops_cc
set net_event=1
where event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON','EXITWITHTIMEOUT') 
and workinghour=1
and event_date>=@max_day
;

update bi_ops_cc 
set net_event=1
where event_date in ('2013-03-10', '2013-03-17')
and event in ('COMPLETECALLER','COMPLETEAGENT','ABANDON','TRANSFER','EXITWITHTIMEOUT');


update bi_ops_cc 
set net_event=if(event_hour=18 and event_minute>5,0,if(event_hour=9 and event_minute<55,0,1))
where net_event=0 and agent<>'NONE' and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER') 
AND (event_weekday=6 or holiday=1) and (event_shift='18:00:00' or event_shift='9:30:00')
;

update bi_ops_cc 
set net_event=if(event_hour=21 and event_minute>5,0,if(event_hour=8 and event_minute<55,0,1))
where net_event=0 and agent<>'NONE' and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER') 
AND event_weekday=5 and holiday=0 and (event_shift='21:00:00' or event_shift='8:30:00')
;

update bi_ops_cc 
set net_event=if(event_hour=21 and event_minute>5,0,if(event_hour=7 and event_minute<55,0,1))
where net_event=0 and agent<>'NONE' and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER') 
AND event_weekday in(0,1,2,3,4) and holiday=0 and (event_shift='21:00:00' or event_shift='7:30:00')
;

SELECT  'Extra queries',now();

#Agosto 25 de 2013: Programación hasta las 9 p.m.
update bi_ops_cc 
set net_event=1
where event_date='2013-08-25' and event_hour>18 and event_hour<21;

#Agosto 29 de 2013: Programación hasta las 11 p.m. por promociones de marketing
update bi_ops_cc 
set net_event=1
where event_date='2013-08-29' and event_hour>21 and event_hour<23
and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON');

#Horario adicional Back Friday y Cyber Monday 2013
update  bi_ops_cc
set
net_event=1,
additional_time=1
where event_date='2013-11-27'
and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON')
and event_hour>=21;

update  bi_ops_cc
set
net_event=1,
additional_time=1
where event_date='2013-11-28'
and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON')
and (event_hour>=21 or event_hour<8);

update  bi_ops_cc
set
net_event=1,
additional_time=1
where event_date='2013-11-29'
and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON')
and (event_hour>=21 or event_hour<8);

update  bi_ops_cc
set
net_event=1,
additional_time=1
where event_date='2013-11-30'
and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON')
and (event_hour>=21 or event_hour<9);

update  bi_ops_cc
set
net_event=1,
additional_time=1
where event_date='2013-12-01'
and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON')
and (event_hour>=18 or (event_hour<10 and event_hour>=6));

update  bi_ops_cc
set
net_event=1,
additional_time=1
where event_date='2013-12-02'
and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON')
and (event_hour>=21 or event_hour<8);

update  bi_ops_cc
set
net_event=1,
additional_time=1
where event_date='2013-12-03'
and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON')
and (event_hour>=21 or event_hour<8);

#Horario adicional Diciembre
update  bi_ops_cc
set
net_event=1,
additional_time=1
where event_date='2013-12-17'
and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON')
and event_hour>=21;

update  bi_ops_cc
set
net_event=1,
additional_time=1
where event_date='2013-12-19'
and event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON')
and event_hour>=21;

#Quitar llamadas en horario adicional de postventa
update bi_ops_cc
set net_event=0
where additional_time=1
and pos=1
and event_date>='2013-11-27'
and event_date<='2013-12-19' ;

#Yuli Guarin contesto con la extension 2040 de enit
update bi_ops_cc 
set
agent='SIP/phone117',
agent_name='YULI YESENIA GUARIN CRUZ ',
agent_ext=2117
where event_date='2013-08-24' 
and agent_ext=2040 AND event_datetime<'2013-08-24 15:58:24';

#Apoyo a Inbound de Ingrid Mendez. Contesta con la extension 2040
update bi_ops_cc
set
agent='SIP/phone27',
agent_name='INGRID YISETH MENDEZ PEREZ',
agent_ext=2027
where event_date='2013-08-24' 
and agent_ext=2040 AND event_datetime>='2013-08-24 15:58:24';

#Apoyo de backoffice a la linea (Ingrid Mendez y Ana Cuervo)
update bi_ops_cc
set
agent='SIP/phone27',
agent_name='INGRID YISETH MENDEZ PEREZ',
agent_ext=2027
where event_date='2013-08-25' 
and agent_ext=2069;

update bi_ops_cc
set
agent='SIP/phone16',
agent_name='ANA MARIA CUERVO TOCUA',
agent_ext=2016
where event_date='2013-08-25' 
and agent_ext=2028;

#Quitar llamadas de Navidad en horario no laboral
update bi_ops_cc
set net_event=0
where event_hour>=18
and event_date='2013-12-24';

update bi_ops_cc
set net_event=0
where event_hour>=16
and event_date='2013-12-25';

#Quitar llamadas de Año Nuevo en horario no laboral
update bi_ops_cc
set net_event=0
where event_hour>=18
and event_date='2013-12-31';

update bi_ops_cc
set net_event=0
where 
event_date='2013-12-25';

SELECT  'Actualizar binarios de tiempos',now();
update bi_ops_cc
set 
unanswered_in_wh=if(event in ('ABANDON'),1,0),
unanswered_in_wh_cust=if(event in ('ABANDON') and hold_duration_seg<=5,1,0),
answered_in_wh=if(event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER'),1,0),
answered_in_wh_20=IF(event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER') and hold_duration_seg<=20,1,0),
no_answer=if(event='RINGNOANSWER',1,0),
agent_q=if(agent<>'NONE',1,0)
where net_event=1
and event_date>=@max_day
;

update bi_ops_cc
SET
no_answer=if(event='RINGNOANSWER',1,0)
where event_date>=@max_day
;

update bi_ops_cc
set 
available_time=0
where net_event=0 or agent_q=0
and event_date>=@max_day
;

SELECT  'Actualizar datos de preventa y posventa',now();
update bi_ops_cc 
set 
pre=if(queue='ivr_cola1',1,0),
pos=if(queue in ('ivr_cola2','ivr_cola3'),1,0),
pre_answ=if(queue='ivr_cola1' and answered_in_wh=1,1,0),
pos_answ=if(queue in ('ivr_cola2','ivr_cola3') and answered_in_wh=1,1,0)
where event_date>=@max_day
;

update bi_ops_cc 
set event_date='2013-06-21',
call_duration_seg=25
where uniqueid='1371851043.10169'
and event='COMPLETEAGENT';


update bi_ops_cc
set agent_name='ABANDONO'
where agent='NONE';

#SELECT  'Start routine bi_ops_cc_occupancy',now();

#call bi_ops_cc_occupancy();

#SELECT  'End routine bi_ops_cc_occupancy',now();

#SELECT  'Start routine agents_schedule',now();

#call agents_schedule();

#SELECT  'End routine agents_schedule',now();


call bi_ops_cc_queue2();

call bi_ops_cc_facebook();

SELECT  'Llamadas transferidas a otra cola',now();

#Eliminar las tabla temporal de llamadas transferidad
DROP   TEMPORARY TABLE IF EXISTS calls_transfered_to_queue;
CREATE TEMPORARY TABLE calls_transfered_to_queue

#Llenar los datos de las llamadas transferidas 
select * from (select event_date,uniqueid,count(distinct(queue)) total,queue as queue_a, "queue_unknown" as queue_b,
"queue_unknown" as queue_c 
from bi_ops_cc where net_event=1 
group by uniqueid) as q
where q.total>=2;

#Llenar los datos de la cola a la que fue transferida
update calls_transfered_to_queue a inner join
bi_ops_cc b on a.uniqueid=b.uniqueid
set a.queue_b=b.queue
where a.queue_a<>b.queue and b.net_event=1;

#Llenar los datos de la cola transferida 2 (en caso que se transfiera dos veces la llamada)
update calls_transfered_to_queue a inner join
bi_ops_cc b on a.uniqueid=b.uniqueid
set a.queue_c=b.queue
where a.queue_a<>b.queue and a.queue_b<>b.queue and b.net_event=1;

#call bi_ops_cc_occupancy();

#Actualizar transferencias en la tabla general de servicio al cliente
SELECT  'Actualizar datos de las transferencias en bi_ops_cc',now();
update bi_ops_cc a 
inner join calls_transfered_to_queue b on a.uniqueid=b.uniqueid
set 
transfered_to_queue=1,
a.initial_queue=b.queue_a,
a.second_queue=b.queue_b,
a.third_queue=b.queue_c,
a.sequence_queue=if(b.total=2,concat(b.queue_a,"-",b.queue_b),concat(b.queue_a,"-",b.queue_b,"-",b.queue_c))
where a.event_date>=@max_day
;

#Cambiar filtro para los datos 
update bi_ops_cc 
set 
transfered_to_queue=0
where transfered_to_queue=1
and queue<>initial_queue
and event_date>=@max_day
;

#Llamadas transferidas desde otra cola
update bi_ops_cc 
set transfered_from_queue=1
where transfered_to_queue=0
and initial_queue is not null
and event_date>=@max_day
;

#Llamadas transferidas dos veces
update bi_ops_cc 
set transfered_to_queue=1
where transfered_to_queue=0
and third_queue is not null
and third_queue<>'queue_unknown'
and queue=second_queue
and event_date>=@max_day
;

#Arreglar extension de los abandonos
update bi_ops_cc 
set
agent_ext=0 where agent='NONE';

call bi_ops_cc_agent_sales;

call bi_ops_cc_agent_times;

call bi_ops_cc_occupancy_2;

SELECT  'Actualizar extensiones de los retirados',now();

update bi_ops_cc
set
agent_ext=if(length(right(agent,length(agent)-9))=3,concat(2,right(agent,length(agent)-9)),
if(length(right(agent,length(agent)-9))=2,concat(20,right(agent,length(agent)-9)),
concat(200,right(agent,length(agent)-9))))
where agent_ext is null;

#Apoyos
update bi_ops_cc
set agent_ext=right(agent,length(agent)-4) 
where agent  like 'SIP/1%' OR 
agent like 'SIP/3%'
 ;

update bi_ops_cc
set agent_ext=right(agent,length(agent)-4) 
where event_date='2013-11-29'
and agent  like 'SIP/1%' OR 
agent like 'SIP/3%'
 ;

update bi_ops_cc
set net_event=0
where additional_time=1
and pos=1
and event_date>='2013-11-27';

#Rutina customer detail
call bi_ops_customer_detail;

#Actualizar extensiones diferentes
update bi_ops_cc
set agent_ext=right(agent,4)
where
agent_ext ='200';

call bi_ops_cc_agents_detail;


INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'customer_service_co.bi_ops_cc',
  'finish',
  NOW(),
  MAX(event_date),
  count(*),
  count(*)
FROM
  customer_service_co.bi_ops_cc;


SELECT  'Fin rutina bi_ops_cc',now();




END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `bi_ops_cc_agents_detail`()
BEGIN

SELECT  'Inicio rutina bi_ops_cc_agents_detail',now();

set @max_day=cast((select max(date) from bi_ops_cc_agents_detail) as date);

delete from bi_ops_cc_agents_detail
where date>=@max_day;

SELECT  'Datos de bi_ops_cc',now();
insert into bi_ops_cc_agents_detail (yrmonth, date, identification, calls_total, calls_pre, calls_pos, other_calls)
select date_format(event_date,'%Y%m'),event_date,agent_identification,sum(net_event),sum(pre),sum(pos), 
sum(net_event)-sum(pre)-sum(pos)
from bi_ops_cc
where agent_identification is not null
and net_event=1
and agent_identification<>""
and event_date>=@max_day
group by event_date, agent_identification;

#Binarios de fechas
update bi_ops_cc_agents_detail
set 
is_weekday=if(weekday(date) in (0,1,2,3,4),1,0),
is_saturday=if(weekday(date)=5,1,0),
is_sunday=if(weekday(date)=6,1,0);

update bi_ops_cc_agents_detail a inner join customer_service_co.calendar b on a.date=b.dt
set is_holiday=b.isholiday
where is_holiday is null;

SELECT  'Datos de activity',now();
update bi_ops_cc_agents_detail a
inner join
(SELECT t.yrmonth, t.date,
t.username, count(1) canti FROM 
(SELECT date_format(date_time,'%Y%m') yrmonth, date(a.date_time) date,
a.date_time,
a.macro_proceso,a.telefono_email,c.username,c.email,c.phone,c.canal 
FROM activities a 
left join users c on a.user_id=c.id where canal='telefono' ) as t
where t.phone is not null and t.phone<>'0000'
group by t.date,t.username) b
on a.date=b.date and a.identification=b.username
set a.activity=b.canti
where a.date>=@max_day;

#Turno
update bi_ops_cc_agents_detail a
inner join bi_ops_CS_matrix b
on a.identification=b.identification
set a.shift=b.shift_pattern
where shift is null;

SELECT  'Datos de survey',now();
update bi_ops_cc_agents_detail a
inner join
(select call_date, agent_identification, count(1) cant from bi_ops_cc_survey
where agent_identification is not null
group by call_date, agent_identification) b
on a.date=b.call_date and a.identification=b.agent_identification
set a.survey=b.cant
where date>=@max_day;

SELECT  'Datos de pausas',now();
update  bi_ops_cc_agents_detail a
inner join
(select date,agent_identification, sum(dif_time_sec) pause from bi_ops_cc_agent_times
where effective_time=0
group by 
date,agent_identification) b
on a.date=b.date and a.identification=b.agent_identification
set a.total_pause_time=b.pause
where a.date>=@max_day;

SELECT  'Datos de conección',now();
update  bi_ops_cc_agents_detail a
inner join
(select date,agent_identification, sum(dif_time_sec) time from bi_ops_cc_agent_times
where dif_time_sec is not null
group by 
date,agent_identification) b
on a.date=b.date and a.identification=b.agent_identification
set a.total_connected_time=b.time
where a.date>=@max_day;

SELECT  'Disponibilidad agentes',now();

update bi_ops_cc_agents_detail a 
inner join bi_ops_cc_shift_details b
on a.shift=b.shift
set total_available_time=entre_semana_seg
where is_weekday=1 and is_holiday=0;

update bi_ops_cc_agents_detail a 
inner join bi_ops_cc_shift_details b
on a.shift=b.shift
set total_available_time=sabado_seg
where is_saturday=1 and is_holiday=0;

update bi_ops_cc_agents_detail a 
inner join bi_ops_cc_shift_details b
on a.shift=b.shift
set total_available_time=domingo_seg
where is_sunday=1 and is_holiday=0;

update bi_ops_cc_agents_detail a 
inner join bi_ops_cc_shift_details b
on a.shift=b.shift
set total_available_time=domingo_seg
where is_holiday=1;

SELECT  'Datos onphone',now();
update bi_ops_cc_agents_detail a
inner join
(select date_format(event_date,'%Y%m'),event_date,agent_identification, sum(call_duration_seg) call_duration
from bi_ops_cc
where agent_identification is not null
and net_event=1
and agent_identification<>""
group by event_date, agent_identification) b
on a.date=b.event_date and a.identification=b.agent_identification
set a.total_onphone_time=b.call_duration
where a.date>=@max_day;

SELECT  'Fin rutina bi_ops_cc_agents_detail',now();


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `bi_ops_cc_agent_sales`()
BEGIN

SELECT  'Inicio rutina bi_ops_cc_agent_sales',now();

#Zona horaria de Colombia

set time_zone='-5:00';

#truncate table bi_ops_cc_agent_sales; 

set @max_day=cast((select max(date_ordered) from bi_ops_cc_agent_sales) as date);

delete from bi_ops_cc_agent_sales
where date_ordered>=@max_day;
#truncate table bi_ops_cc;


SELECT  'Insertar datos de las ventas',now();
insert into bi_ops_cc_agent_sales(date_ordered,date,hour,minute,campaign,assisted_sales_operator,channel,channel_group,
order_nr, Item,total_value,
obc,oac,returned,rejected,status_item,coupon_code)
select * from
(select date_ordered,date,hour(hour) hour, minute(hour) minute,campaign,assisted_sales_operator,channel,channel_group,
order_nr, Item, paid_price_after_vat+shipping_fee_after_vat as total_value,
obc,oac,returned,rejected,status_item,coupon_code
 from bazayaco.tbl_order_detail
where assisted_sales_operator is not null and assisted_sales_operator<>""
and date_ordered>=@max_day) as t;



SELECT  'Actualizar estados de acuerdo a order_detail',now();
update bi_ops_cc_agent_sales a 
inner join bazayaco.tbl_order_detail b
on a.Item=b.Item
set 
a.status_item=b.status_item,
a.oac=b.oac,
a.obc=b.obc,
a.returned=b.returned,
a.rejected=b.rejected;


SELECT  'Actualizar extensiones de agentes',now();
update bi_ops_cc_agent_sales a
left join relacion_agentes b
on a.campaign=b.campaign
set
a.agent_ext=b.cod_agente,
a.identification=b.identification
where agent_ext is null;

update bi_ops_cc_agent_sales a
inner join relacion_agentes b
on upper(a.assisted_sales_operator)=b.sales_operator
set a.agent_ext=b.cod_agente,
a.identification=b.identification
where agent_ext is null;


#Borrar ventas corporativas
delete from bi_ops_cc_agent_sales where assisted_sales_operator in ('paola.correa','wilmar.murcia');


SELECT  'Actualizar nombres segun la matriz',now();
update bi_ops_cc_agent_sales a
inner join bi_ops_CS_matrix_antext b
on a.identification=b.identification
set
a.agent_name=b.name,
a.coordinator=b.coordinator,
a.process=b.process
where a.agent_name is null
and date_ordered<='2013-12-09';

update bi_ops_cc_agent_sales a
inner join bi_ops_CS_matrix b
on a.identification=b.identification
set
a.agent_name=b.name,
a.coordinator=b.coordinator,
a.process=b.process,
a.agent_ext=b.phone
where a.agent_name is null
and date_ordered>'2013-12-09';


SELECT  'Agregar datos de tbl_order_detail',now();
update bi_ops_cc_agent_sales a
inner join bazayaco.tbl_order_detail b on a.Item=b.Item
set
a.buyer=b.buyer,
a.cat1=b.new_cat1,
a.cat2=b.new_cat2,
a.cat3=b.new_cat3,
a.brand=b.brand,
a.product_name=b.product_name,
a.sku=b.sku,
a.proveedor=b.proveedor,
a.pvp_neto=b.unit_price_after_vat,
a.pvp_sin_cupon=b.paid_price_after_vat,
a.total_prevat=b.unit_price_after_vat+b.shipping_fee_after_vat,
a.cupon_value=b.coupon_money_after_vat,
a.costo_neto=cost_oms+IF(isnull(b.delivery_cost_supplier),0.0,b.delivery_cost_supplier),
a.pc2_costs=b.CS+b.WH+b.shipping_cost+b.payment_cost,
a.cs=b.CS,
a.wh=b.WH,
a.shipping_cost=b.shipping_cost,
a.payment_cost=b.payment_cost,
a.inbound_cost=b.delivery_cost_supplier,
a.pc15_costs=b.shipping_cost+b.payment_cost,
a.pvp_full=b.unit_price,
a.source_medium=b.`source/medium`
where a.buyer is null;


delete from bi_ops_cc_agent_sales
where source_medium not in (
'CS / Inbound',
'Telesales / CS',
'facebook / sm_call',
'CS / reorder',
'CS / invalid',
'CS / invalidcrossell',
'CS / upsell',
'CS / cross_sell',
'facebook / socialmedia',
'CS / replace',
'CS / reactivation',
'CS / call',
'CS / invalidupsell',
'CS / OutBound',
'facebook / socialmediaads',
'facebook / retargeting',
'facebook / socialmedia_fashion',
'CS / CrossSale',
'CS / promesa_linio',
'CS / pomesa_linio',
'facebook.com / referral',
'blog / socialmedia',
'twitter / socialmedia');


update bi_ops_cc_agent_sales
set initial_channel=if(source_medium in (
'CS / Inbound',
'Telesales / CS',
'CS / call'),'Inbound',if(source_medium in(
'facebook / sm_call',
'facebook / socialmedia',
'facebook / socialmediaads',
'facebook / retargeting',
'facebook / socialmedia_fashion',
'facebook.com / referral',
'blog / socialmedia',
'twitter / socialmedia'),'Social Media', if(source_medium=
'CS / reorder','Reorder', if(source_medium=
'CS / invalid','Invalid',  if(source_medium=
'CS / invalidcrossell', 'InvalidCrossell', if(source_medium=
'CS / upsell', 'Upsell', if(source_medium=
'CS / cross_sell', 'Crossell', if(source_medium=
'CS / replace', 'Replace',  if(source_medium=
'CS / reactivation','Reactivation' , if(source_medium=
'CS / invalidupsell','InvalidUpsell', if(source_medium=
'CS / OutBound','Outbound', if(source_medium=
'CS / CrossSale','Crossell', if(source_medium in(
'CS / promesa_linio',
'CS / pomesa_linio'),'Promesa Linio','Not Asigned')))))))))))))
where initial_channel is null;


update bi_ops_cc_agent_sales a inner join
tbl_bi_ops_chats b on a.agent_ext=b.agent_ext
and a.date_ordered=b.date
set initial_channel='Chat'
where b.agent_ext<>0
and a.initial_channel in ('Inbound','Not Asigned');


update
bi_ops_cc_agent_sales
set agent_ext=0
 where agent_ext in (0,1);


update bi_ops_cc_agent_sales
set telesales=if(channel_group='Tele Sales',1,0),
other_channels=if(channel_group<>'Tele Sales',1,0);


UPDATE bi_ops_cc_agent_sales  
INNER JOIN bazayaco.tbl_order_detail
ON bi_ops_cc_agent_sales.item=tbl_order_detail.item
SET bi_ops_cc_agent_sales.new_customers=tbl_order_detail.new_customers;



update bi_ops_cc_agent_sales  
inner join bazayaco.tbl_order_detail 
on bi_ops_cc_agent_sales.item = tbl_order_detail.item
set pc1_costs = cost_oms + delivery_cost_supplier;

update bi_ops_cc_agent_sales  
inner join bazayaco.tbl_order_detail 
on bi_ops_cc_agent_sales.item = tbl_order_detail.item
set pc2_costs = tbl_order_detail.shipping_cost + tbl_order_detail.payment_cost
 + tbl_order_detail.cs + tbl_order_detail.wh;

update bi_ops_cc_agent_sales 
inner join bazayaco.tbl_order_detail
on bi_ops_cc_agent_sales.item = tbl_order_detail.item
set pc3_costs = tbl_order_detail.acquisition_costs;


update bi_ops_cc_agent_sales a
inner join relacion_agentes b
on a.campaign=b.campaign
set
a.agent_name=b.name;

update bi_ops_cc_agent_sales a
inner join relacion_agentes b
on upper(a.assisted_sales_operator)=b.sales_operator
set
a.agent_name=b.name,
a.agent_ext=b.cod_agente
where a.campaign not in (select campaign from relacion_agentes where campaign not in ('inbound','(not set)','NN'));

update bi_ops_cc_agent_sales a
inner join bi_ops_CS_matrix b
on a.agent_name=b.name
set 
a.coordinator=b.coordinator,
a.process=b.process;



update bi_ops_cc_agent_sales a
inner join bi_ops_cc_apoyo b
on a.campaign=b.campaign
set 
a.agent_name=b.nombre,
a.coordinator='Apoyo',
a.process=b.area
where date_ordered>='2013-11-28' and date_ordered<'2013-12-03'
and agent_ext is null;

update bi_ops_cc_agent_sales a
inner join bi_ops_cc_apoyo b
on upper(a.assisted_sales_operator)=b.sales_operator
set 
a.agent_name=b.nombre,
a.coordinator='Apoyo',
a.process=b.area
where date_ordered>='2013-11-28' and date_ordered<'2013-12-03'
and agent_ext is null;


update bi_ops_cc_agent_sales a
set initial_channel='Inbound'
where date_ordered>='2013-11-28' 
and initial_channel='Not asigned'
and process in ('VENTAS','INBOUND','BACK OFFICE','SAC')
and source_medium is null;

update bi_ops_cc_agent_sales 
set initial_channel='Inbound'
where date>='2013-11-29'
and initial_channel='Not Asigned'
and assisted_sales_operator='telesales';

SELECT  'Fin rutina bi_ops_cc_sales',now();


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `bi_ops_cc_agent_times`()
BEGIN

SELECT  'Inicio rutina bi_ops_cc_agent_times',now();


#Zona horaria de Colombia

set time_zone='-5:00';

set @max_day=cast((select max(date) from bi_ops_cc_agent_times) as date);

delete from bi_ops_cc_agent_times
where date>=@max_day;
#truncate table bi_ops_cc_agent_times; 


SELECT  'Insertar datos de los tiempos',now();
insert into 
bi_ops_cc_agent_times (queue_stats_id, queue, datetime, agent,event)
select * from
(select a.queue_stats_id, queue,
a.datetime,d.agent,event
FROM new_queue_stats a inner join qevent b on a.qevent=b.event_id
inner join qname2 c on a.qname=c.queue_id inner join qagent2 d on a.qagent=d.agent_id 
where 
event IN ('PAUSE','UNPAUSE','PAUSEALL','UNPAUSEALL','ADDMEMBER','REMOVEMEMBER') 
and 
date(a.datetime)>=@max_day
and 
agent<>'NONE' 
group by datetime,agent
ORDER BY qagent asc,datetime asc) as sub;



SELECT  'Insertar datos de los tiempos',now();
update
bi_ops_cc_agent_times a
inner join
(select 
a.datetime,d.agent,event, info1
FROM queue_stats a inner join qevent b on a.qevent=b.event_id
inner join qname c on a.qname=c.queue_id inner join qagent d on a.qagent=d.agent_id 
where 
year(datetime)=2013 
and event IN ('PAUSE','UNPAUSE','PAUSEALL','UNPAUSEALL','ADDMEMBER','REMOVEMEMBER') 
and 
date(a.datetime)>=@max_day
and 
queue='NONE' 
and info1<>""
group by datetime,agent) b 
on a.datetime=b.datetime and a.agent=b.agent
set a.type=b.info1;


SELECT  'Actualizar tipos de cola',now();
update
bi_ops_cc_agent_times a
inner join
(select c.queue,
a.datetime,d.agent,event
FROM queue_stats a inner join qevent b on a.qevent=b.event_id
inner join qname c on a.qname=c.queue_id inner join qagent d on a.qagent=d.agent_id 
where 
year(datetime)=2013 
and event IN ('PAUSE','UNPAUSE','PAUSEALL','UNPAUSEALL','ADDMEMBER','REMOVEMEMBER') 
and 
date(a.datetime)>=@max_day
and 
queue in ('ivr_cola1','ivr_cola2','ivr_cola3','facebook','outbound','backoffice')
group by datetime,agent) b 
on a.datetime=b.datetime and a.agent=b.agent
set a.queue=b.queue,
a.event=b.event;


SELECT  'Actualizar diferencias de tiempos',now();
UPDATE bi_ops_cc_agent_times f1
inner JOIN bi_ops_cc_agent_times f2 ON f1.id_tbl_agent_times = (f2.id_tbl_agent_times-1)
SET
f1.dif_time= SEC_TO_TIME( UNIX_TIMESTAMP( f2.datetime ) - UNIX_TIMESTAMP( f1.datetime ) ) ,
f1.type_pause=concat(f1.event,"-",f2.event)
where date(f1.datetime)=date(f2.datetime)
and f1.agent=f2.agent;



SELECT  'Actualizar tiempos efectivos de gestión',now();
update bi_ops_cc_agent_times 
set effective_time=0
where 
(type_pause like 'PAUSE%' 
or type_pause like 'REMOVEMEMBER%');

update bi_ops_cc_agent_times
set
type='Sesion Cerrada'
where 
type_pause like 'REMOVEMEMBER%';

update bi_ops_cc_agent_times
set
type='Pause'
where 
type_pause like 'PAUSE%'
and type_pause not in ('Outbound',
'Receso',
'Pause',
'Sesion Cerrada',
'Almuerzo',
'Capacitacion',
'Reorder',
'Replace',
'Invalid',
'Reactivations');



SELECT  'Actualizar otros campos',now();
update bi_ops_cc_agent_times 
set
yrmonth=concat(year(datetime),if(month(datetime)<10,concat(0,month(datetime)),month(datetime))),
year=year(datetime),
week=weekofyear(datetime),
dow=weekday(datetime),
date=date(datetime),
shift=if(minute(datetime)<30,concat(hour(datetime),':','00'),concat(hour(datetime),':','30'))
where yrmonth is null;


SELECT  'Actualizar nombres de los agentes',now();
update bi_ops_cc_agent_times a inner join bi_ops_agent_names b on a.agent=b.agent 
inner join bi_ops_CS_matrix c on b.ext=c.phone
set a.agent_name=c.name,
a.agent_ext=b.ext
where c.status='ACTIVO' and a.agent_name is null;

update bi_ops_cc_agent_times
set dif_time_sec=time_to_sec(dif_time);

update bi_ops_cc_agent_times 
set effective_time=1 where
type in ('Outbound','Replace','Invalid','Reorder');




update bi_ops_cc_agent_times
set effective_time=1
where type in
('Outbound',
'Receso',
'Reorder',
'Replace',
'Invalid',
'Reactivations' );

SELECT  'Actualizar holidays',now();
update bi_ops_cc_agent_times a inner join calendar b on a.date=b.dt
set holiday=b.isholiday;

update bi_ops_cc_agent_times
set workinghour=if(dow=6 or holiday=1,
	if(hour(datetime)>=10 and hour(datetime)<18,
		1,0),
		if(dow=5,
			if(hour(datetime)>=9 and hour(datetime)<21,
			1,0),
			if(hour(datetime)>=8 and hour(datetime)<21,
				1,0)))
;


#Effective times
update bi_ops_cc_agent_times 
set effective_time=0
where type in
('Pause',
'Sesion Cerrada',
'Receso',
'Capacitacion',
'Almuerzo');

SELECT  'Insertar nombres',now();
update bi_ops_cc_agent_times a inner join bi_ops_agent_names b on a.agent=b.agent inner join 
bi_ops_CS_matrix c on b.ext=c.phone
set a.agent_name=c.name,
a.agent_ext=b.ext,
a.agent_identification=c.identification
where c.status='ACTIVO'
and process in ('SAC','VENTAS','INBOUND')
and date>'2013-12-09';

update  bi_ops_cc_agent_times a inner join bi_ops_agent_names b on a.agent=b.agent inner join 
bi_ops_CS_matrix c on b.ext=c.phone
set a.agent_name=c.name,
a.agent_ext=b.ext,
a.agent_identification=c.identification
where a.agent_name is null
and date>'2013-12-09';

update  bi_ops_cc_agent_times a inner join bi_ops_agent_names b on a.agent=b.agent inner join 
bi_ops_CS_matrix_antext c on b.ext=c.phone
set a.agent_name=c.name,
a.agent_ext=b.ext,
a.agent_identification=c.identification
where c.status='ACTIVO'
and process in ('SAC','VENTAS','INBOUND')
and a.agent_name is null
and date<='2013-12-09';

update  bi_ops_cc_agent_times a inner join bi_ops_agent_names b on a.agent=b.agent inner join 
bi_ops_CS_matrix_antext  c on b.ext=c.phone
set a.agent_name=c.name,
a.agent_ext=b.ext,
a.agent_identification=c.identification
where a.agent_name is null
and date<='2013-12-09';

#Tipos de pausa
drop temporary table if exists bi_ops_cc_pauses;
 
CREATE temporary table bi_ops_cc_pauses
SELECT datetime, info1,agent FROM customer_service_co.queue_stats a 
inner join qevent b on a.qevent=b.event_id
inner join qagent c on a.qagent=c.agent_id
where info1 is not null and info1<>""
and event ='PAUSE';

 

update bi_ops_cc_agent_times t1
inner join 
bi_ops_cc_pauses t2
on t1.agent=t2.agent
and t1.datetime=t2.datetime
set type=info1;

SELECT  'Fin de rutina bi_ops_cc_agent_times',now();

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `bi_ops_cc_chats_performance`()
BEGIN

SELECT  'Inicio rutina de chats',now();

#Zona horaria de Colombia

set time_zone='-5:00';

set @last_day=(SELECT max(date) FROM tbl_bi_ops_cc_chats_performance);

SELECT  'Insertar datos a la tabla de chats',now();

insert into tbl_bi_ops_cc_chats_performance (date,chats)
SELECT date, sum(invitations) FROM customer_service.tbl_invitations_live_chat
where date>@last_day
group by date;


/*SELECT  'Insertar tiempo promedio',now();

update tbl_bi_ops_cc_chats_performance a
inner join tbl_performance_live_chat b
on a.date=b.date
set act=time
where mail='Total Average'
and date>@last_day;*/


SELECT  'Insertar datos del performance de visitantes',now();
update tbl_bi_ops_cc_chats_performance a
inner join customer_service.tbl_queue_live_chat b
on a.date=b.date
set 
wait_in_queue=waiting_visitors,
proceed_to_chat=attended_visitors,
abandon_queue=abandon
where a.date>@last_day;

SELECT  'Insertar datos de tiempos de espera del visitante',now();
update tbl_bi_ops_cc_chats_performance a
inner join customer_service.tbl_timebefore_proceed b
on a.date=b.date
set 
waiting_time_max=mwt_before_proceed,
waiting_time_avg=awt_before_proceed,
waiting_time_min=iwt_before_proceed
where a.date>@last_day;

SELECT  'Insertar datos de tiempos de espera antes de abandono',now();
update tbl_bi_ops_cc_chats_performance a
inner join customer_service.tbl_timebefore_abandonment b
on a.date=b.date
set 
time_to_abandon_max=mwt_before_abandonment,
time_to_abandon_avg=awt_before_abandonment,
time_to_abandon_min=iwt_before_abandonment
where a.date>@last_day;

SELECT  'Insertar datos de satisfacción',now();
update tbl_bi_ops_cc_chats_performance a
inner join (select date, sum(chats_rated) chats_rated, sum(rated_good) rated_good,
sum(rated_bad) rated_bad from customer_service.tbl_rating_live_chat where name<>'Total Average' group by date) b
on a.date=b.date
set 
a.chats_rated=b.chats_rated,
a.rated_good=b.rated_good,
a.rated_bad=b.rated_bad
where 
a.date>@last_day;

update tbl_bi_ops_cc_chats_performance
set
queued=chats+abandon_queue;

SELECT  'Insertar datos por agente',now();

insert into customer_service_co.tbl_bi_ops_cc_chats_performance_agents
(date,operator_name, chats, total_chatting_sec,
chatting_sec, available_sec, away_sec )
SELECT * FROM customer_service.tbl_availability_live_chat
where date>@last_day
and number_of_chats<>'-0';

SELECT  'Insertar act por agente',now();

update customer_service_co.tbl_bi_ops_cc_chats_performance_agents
set
act_sec=total_chatting_sec/chats 
where act_sec is null;

#Actualizar calificaciones por agente
update
customer_service_co.tbl_bi_ops_cc_chats_performance_agents a
inner join customer_service.tbl_rating_live_chat b
on a.date=b.date and a.operator_name=b.name
set 
a.rated=b.chats_rated,
a.rated_good=b.rated_good,
a.rated_bad=b.rated_bad
where rated is null;

#Actualizar act general (Valor aproximado)
update
customer_service_co.tbl_bi_ops_cc_chats_performance a
inner join (SELECT date, sum(chats*act_sec)/sum(chats) act 
FROM tbl_bi_ops_cc_chats_performance_agents
group by date) b
on a.date=b.date
set a.act_sec=b.act
where a.act_sec is null;

#Agregar tiempo promedio de espera
update
customer_service_co.tbl_bi_ops_cc_chats_performance 
set waiting_time_sec=(time_to_sec(waiting_time_avg)*chats+time_to_sec(time_to_abandon_avg)*abandon_queue)/(chats+abandon_queue)
where waiting_time_sec is null;


SELECT  'Fin rutina de chats',now();


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `bi_ops_cc_dyalogo_test`()
BEGIN

SELECT  'Inicio rutina bi_ops_cc_dyalogo_test',now();

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'customer_service_co.bi_ops_cc_dyalogo',
  'start',
  NOW(),
  MAX(event_date),
  count(*),
  count(*)
FROM
  customer_service_co.bi_ops_cc_dyalogo;

set @max_day=cast((select max(event_date) from bi_ops_cc_dyalogo)  as date);
set time_zone='-5:00';

delete from bi_ops_cc_dyalogo
where event_date>=@max_day;
#truncate table bi_ops_cc_dyalogo;

SELECT  'Ingresar datos de dyalogo Colombia',now();

insert into customer_service_co.bi_ops_cc_dyalogo (queue_stats_id, queue, agent_name,agent_identification, agent_ext, event, call_duration_seg,
hold_duration_seg, agent_hold_time_seg, uniqueid, number, record)
SELECT a.id, campana, agente,  b.identificacion, extension, resultado,
 duracion_al_aire, tiempo_espera, tiempo_timbrando, unique_id, numero_telefonico,grabacion
  FROM CS_dyalogo.dy_llamadas_espejo a left join
CS_dyalogo.dy_agentes b on a.id_agente_espejo=b.id
where date(fecha_hora)>=@max_day
and sentido='Entrante'
and campana is not null;

SELECT  'Llamadas transferidas',now();

#Ingresar llamadas transferidas y cantidad de veces
DROP  TEMPORARY TABLE IF EXISTS calls_transfered_to_queue;
CREATE TEMPORARY TABLE calls_transfered_to_queue

#Llenar los datos de las llamadas transferidas 
select unique_id,count(1) cant, max(fecha_hora) max_fecha_hora, null as cola
 from CS_dyalogo.v_queue_log where evento in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON') group by unique_id having count(1)>1;

#Ingresar base general de las llamadas transferidas
truncate table calls_transfered;
insert into calls_transfered

select b.*,a.cant, replace(left(right(agente,length(agente)-instr(agente,'|')),instr(right(agente,length(agente)-instr(agente,'|')),'|')-1),'_',' ') name,
right(agente,if(instr(right(agente,length(agente)-instr(agente,'|')),'|')=0,null,length(right(agente,length(agente)-instr(agente,'|'))) -instr(right(agente,length(agente)-instr(agente,'|')),'|'))) ext, 
left(datos,instr(datos,'|')-1) info1,left(right(datos,length(datos)-instr(datos,'|')),instr(right(datos,length(datos)-instr(datos,'|')),'|')-1) info2,
right(datos,if(instr(right(datos,length(datos)-instr(datos,'|')),'|')=0,null,length(right(datos,length(datos)-instr(datos,'|'))) -instr(right(datos,length(datos)-instr(datos,'|')),'|')))
  info3, null as info4, null as info5, null as info6, null as queue1, null as queue2, null as queue3, null as sequence
from  CS_dyalogo.v_queue_log b
inner join calls_transfered_to_queue a
on a.unique_id=b.unique_id
where evento in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON');

update customer_service_co.calls_transfered
set
info4=left(info3,instr(info3,'|')-1), 
info5=left(right(info3,length(info3)-instr(info3,'|')),instr(right(info3,length(info3)-instr(info3,'|')),'|')-1),
info6=right(info3,if(instr(right(info3,length(info3)-instr(info3,'|')),'|')=0,null,length(right(info3,length(info3)-instr(info3,'|'))) -instr(right(info3,length(info3)-instr(info3,'|')),'|')))
;

#Ingresar secuencias de transferencia
drop temporary table if exists sequence_transfered;
create temporary table sequence_transfered
select unique_id, group_concat( distinct cola order by fecha_hora asc ) sequence from calls_transfered group by unique_id;

update calls_transfered a
inner join sequence_transfered b
on a.unique_id=b.unique_id
set a.sequence=b.sequence;

update customer_service_co.calls_transfered
set
queue1=left(sequence,instr(sequence,',')-1), 
queue2=if(cant=2,right(sequence,length(sequence)-instr(sequence,',')),left(right(sequence,length(sequence)-instr(sequence,',')),instr(right(sequence,length(sequence)-instr(sequence,',')),',')-1)),
queue3=right(sequence,if(instr(right(sequence,length(sequence)-instr(sequence,',')),',')=0,null,length(right(sequence,length(sequence)-instr(sequence,','))) -instr(right(sequence,length(sequence)-instr(sequence,',')),',')))
;

#Identificar llamadas transferidas en la tabla general y borrarlas
update bi_ops_cc_dyalogo a 
inner join calls_transfered b
on a.uniqueid=b.unique_id
set transfered_from_queue=1;

delete from bi_ops_cc_dyalogo
where transfered_from_queue=1
and date(fecha_hora)>=@max_day;

SELECT  'Ingresar llamadas transferidas a bi_ops_cc',now();
#Agregar llamadas transferidas a bi_ops_cc
insert into customer_service_co.bi_ops_cc_dyalogo (queue_stats_id, queue, agent_name, agent_ext, event, call_duration_seg,
hold_duration_seg, uniqueid)
SELECT id,  cola, name,  ext, if(evento='ABANDON','Abandonada','Contestada'),
if(evento='ABANDON',info3,if(evento='TRANSFER',info4,info1)) , if(evento='ABANDON',0,if(evento='TRANSFER',info5,info2)),  unique_id
  FROM calls_transfered
where date(fecha_hora)>=@max_day;

update bi_ops_cc_dyalogo a 
inner join calls_transfered b
on a.uniqueid=b.unique_id
set
initial_queue=queue1,
second_queue=queue2,
third_queue=queue3,
sequence_queue=sequence,
transfered_to_queue=if(queue=queue1,1,if(queue=queue2 and cant>=3,1,0)),
transfered_from_queue=if(queue<>queue1,1,0);

SELECT  'Fecha hora de conexión',now();

drop table if exists fecha_time_connected;

create temporary table fecha_time_connected

select unique_id, fecha_hora, cola, right(datos,if(instr(right(datos,length(datos)-instr(datos,'|')),'|')=0,null,length(right(datos,length(datos)-instr(datos,'|'))) -instr(right(datos,length(datos)-instr(datos,'|')),'|'))) agent_hold_time
from CS_dyalogo.v_queue_log where evento='ENTERQUEUE';

create index unique_id on  fecha_time_connected (unique_id);

update customer_service_co.bi_ops_cc_dyalogo
set
queue=replace(queue," ","");

update customer_service_co.bi_ops_cc_dyalogo a
inner join fecha_time_connected b
on a.uniqueid=b.unique_id
set 
event_datetime=fecha_hora,
event_date=date(fecha_hora),
event_hour=hour(fecha_hora), 
event_minute=minute(fecha_hora),
event_weekday= weekday(fecha_hora),
event_week=weekofyear(fecha_hora),
event_month=monthname(fecha_hora),
event_year=year(fecha_hora),
a.agent_hold_time_seg=if(a.agent_hold_time_seg is null,b.agent_hold_time,a.agent_hold_time_seg)
where a.queue=b.cola;



SELECT  'Actualizar shifts',now();

update bi_ops_cc_dyalogo 
set event_shift=if(event_minute<30,concat(event_hour,':','00'),concat(event_hour,':','30'))
where event_shift is null;

SELECT  'Actualizar holidays',now();
update bi_ops_cc_dyalogo a inner join operations_co.calendar b on a.event_date=b.dt
set holiday=b.isholiday
where holiday is null;

SELECT  'Actualizar horas laborales',now();
update bi_ops_cc_dyalogo
set workinghour=if(event_weekday=6 or holiday=1,
	if(event_hour>=10 and event_hour<18,
		1,0),
		if(event_weekday=5,
			if(event_hour>=9 and event_hour<21,
			1,0),
			if(event_hour>=8 and event_hour<21,
				1,0)))
where event_date>=@max_day
;

update bi_ops_cc_dyalogo
set workinghour=1
where holiday=0 and event_weekday<5
and event_shift='07:30:00'
and event_date>=@max_day
;

SELECT  'Actualizar net_events',now();
update bi_ops_cc_dyalogo
set net_event=1
where event in ('Contestada','Abandonada') 
and workinghour=1
and event_date>=@max_day
;

SELECT  'Actualizar binarios de tiempos',now();
update bi_ops_cc_dyalogo
set 
unanswered_in_wh=if(event in ('Abandonada'),1,0),
unanswered_in_wh_cust=if(event in ('Abandonada') and hold_duration_seg<=5,1,0),
answered_in_wh=if(event in ('Contestada'),1,0),
answered_in_wh_20=IF(event in ('Contestada') and hold_duration_seg<=20,1,0),
no_answer=if(event='No Contestada',1,0),
agent_q=if(agent_name is not null,1,0)
where net_event=1
and event_date>=@max_day
;

#Llamadas pos
update bi_ops_cc_dyalogo
set 
pos=1,
pos_answ=if(answered_in_wh=1,1,0)
where net_event=1
and queue in ('Devolucionesytramites','Reclamos','Estadodepedido','Cancelaciones')
and event_date>=@max_day
;

#Llamadas pre
update bi_ops_cc_dyalogo
set 
pre=1,
pre_answ=if(answered_in_wh=1,1,0)
where net_event=1
and queue ='Ventaspersonasnaturales'
and event_date>=@max_day
;

SELECT  'Actualizar cedulas faltantes',now();
update bi_ops_cc_dyalogo a
inner join CS_dyalogo.dy_agentes b on a.agent_name=b.nombre
set agent_identification=identificacion
where agent_identification is null and event='Contestada';


INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'customer_service_co.bi_ops_cc_dyalogo',
  'finish',
  NOW(),
  MAX(event_date),
  count(*),
  count(*)
FROM
  customer_service_co.bi_ops_cc_dyalogo;


SELECT  'Fin rutina bi_ops_cc',now();



END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `bi_ops_cc_evaluation`()
BEGIN

SELECT  'Insertar datos del mes inbound preventa',now();

#Zona horaria de Colombia

set time_zone='-5:00';

insert into bi_ops_cc_evaluation_inb (yrmonth,agent,identification,agent_ext, agente, tiempo_llamada_valor,transferencias_valor,encuesta_valor, total_llamadas)
select  date_format(event_date,"%Y%m") yrmonth,
agent,agent_identification, agent_ext,agent_name, (avg(call_duration_seg))/60 call_duration_seg,sum(transfered_survey)/sum(net_event) transfered_survey,
sum(answered_2)/sum(transfered_survey) survey_level, sum(net_event)
 from bi_ops_cc where net_event=1 and pre=1 
AND date_format(event_date,"%Y%m")=date_format(curdate(),"%Y%m")-1
and agent<>'NONE' and transfered_to_queue<>1
group by date_format(event_date,"%Y%m"),agent_identification;


SELECT  'Agregar coordinador',now();

update bi_ops_cc_evaluation_inb a
inner join bi_ops_CS_matrix_history b on a.identification=b.identification 
set a.coordinador=b.coordinator,
a.proceso=b.process
where yrmonth=date_format(curdate(),"%Y%m")-1;

SELECT  'Datos de retirados',now();

update  bi_ops_cc_evaluation_inb a 
inner join bi_ops_CS_matrix b
on a.identification=b.identification 
set
a.coordinador=b.coordinator,
a.proceso=b.process
where a.yrmonth=date_format(curdate(),"%Y%m")-1
and a.coordinador is null;


SELECT  'Conversion',now();

update bi_ops_cc_evaluation_inb a inner join
(select date_format(date,"%Y%m") yrmonth,
identification,count(distinct(order_nr)) 'order_telesales' from
bi_ops_cc_agent_sales where 
initial_channel='Inbound'
and obc=1 and telesales=1 
AND date_format(date,"%Y%m")=date_format(curdate(),"%Y%m")-1
group by date_format(date,"%Y%m"),identification) b
on a.identification=b.identification and a.yrmonth=b.yrmonth
set a.total_ordenes=b.order_telesales;

#Ordenes de acuerdo a A_master y al rpoceso regional
/*
update bi_ops_cc_evaluation_inb a inner join
(select date_format(Date,"%Y%m") yrmonth,
identification,count(distinct(OrderNum)) 'order_telesales' from
customer_service.tbl_telesales_co where 
OrderBeforeCan=1 
AND date_format(Date,"%Y%m")=date_format(curdate(),"%Y%m")-1
group by date_format(Date,"%Y%m"),identification) b
on a.identification=b.identification and a.yrmonth=b.yrmonth
set a.total_ordenes=b.order_telesales;;*/

update bi_ops_cc_evaluation_inb 
set 
    conversion_valor = total_ordenes/total_llamadas
where yrmonth=date_format(curdate(),"%Y%m")-1;


SELECT  'Atencion',now();

update bi_ops_cc_evaluation_inb a inner join
(select t1.yrmonth,t1.agent_identification,t1.calls/(t1.calls+t2.no_answer) atencion  from
(select  date_format(event_date,"%Y%m") yrmonth,
agent_identification,sum(net_event) as calls
 from bi_ops_cc where net_event=1 and pre=1 
AND date_format(event_date,"%Y%m")=date_format(curdate(),"%Y%m")-1
group by date_format(event_date,"%Y%m"),agent_identification) t1
inner join
(select  date_format(event_date,"%Y%m") yrmonth,
agent_identification,sum(no_answer) as no_answer
 from bi_ops_cc where pre=1 
AND date_format(event_date,"%Y%m")=date_format(curdate(),"%Y%m")-1
group by date_format(event_date,"%Y%m"),agent_identification) t2
on t1.agent_identification=t2.agent_identification) b
on a.identification=b.agent_identification and a.yrmonth=b.yrmonth
set a.atencion_valor=b.atencion;


SELECT  'Activity y encuesta',now();

update bi_ops_cc_evaluation_inb a inner join
(select yrmonth,identification, if((sum(survey)/sum(calls_total))>1,1,sum(survey)/sum(calls_total)) survey,
if((sum(activity)/sum(calls_total))>1,1,sum(activity)/sum(calls_total)) activity from bi_ops_cc_agents_detail 
where yrmonth=date_format(curdate(),'%Y%m')-1
group by identification) b
on a.identification=b.identification
and a.yrmonth=b.yrmonth
set a.activity_valor=b.activity,
a.transferencias_valor=b.survey;

SELECT  'Calificacion encuesta',now();

update bi_ops_cc_evaluation_inb a inner join
(select date_format(call_date,'%Y%m') yrmonth, agent_identification, sum(answer2_option_1)/sum(answer_2) calification
from bi_ops_cc_survey
where date_format(call_date,'%Y%m')=date_format(curdate(),'%Y%m')-1
group by agent_identification) b
on a.identification=b.agent_identification
and a.yrmonth=b.yrmonth
set encuesta_valor=calification;

delete FROM bi_ops_cc_evaluation_inb
where yrmonth=date_format(curdate(),"%Y%m")-1 AND proceso not like '%VENTAS%' ;

SELECT  'Insertar datos del mes inbound postventa',now();

insert into bi_ops_cc_evaluation_inb (yrmonth,agent,identification,agent_ext, agente, tiempo_llamada_valor,transferencias_valor,encuesta_valor)
select  date_format(event_date,"%Y%m") yrmonth,
agent,agent_identification, agent_ext,agent_name, (avg(call_duration_seg))/60 call_duration_seg,sum(transfered_survey)/sum(net_event) transfered_survey,
sum(answered_2)/sum(transfered_survey) survey_level
 from bi_ops_cc where net_event=1 and pos=1 
AND date_format(event_date,"%Y%m")=date_format(curdate(),"%Y%m")-1
and agent<>'NONE' and transfered_to_queue<>1
group by date_format(event_date,"%Y%m"),agent_identification;


SELECT  'Agregar coordinador',now();

update bi_ops_cc_evaluation_inb a
inner join bi_ops_CS_matrix_history b on a.identification=b.identification 
set a.coordinador=b.coordinator,
a.proceso=b.process
where proceso is null;

SELECT  'Datos de retirados',now();

update  bi_ops_cc_evaluation_inb a 
inner join bi_ops_CS_matrix b
on a.identification=b.identification 
set
a.coordinador=b.coordinator,
a.proceso=b.process
where a.yrmonth=date_format(curdate(),"%Y%m")-1
and a.coordinador is null;


SELECT  'Atencion',now();

update bi_ops_cc_evaluation_inb a inner join
(select t1.yrmonth,t1.agent_identification,t1.calls/(t2.no_answer+t1.calls) atencion  from
(select  date_format(event_date,"%Y%m") yrmonth,
agent_identification,sum(net_event) as calls
 from bi_ops_cc where net_event=1 and pos=1 
AND date_format(event_date,"%Y%m")=date_format(curdate(),"%Y%m")-1
group by date_format(event_date,"%Y%m"),agent_identification) t1
inner join
(select  date_format(event_date,"%Y%m") yrmonth,
agent_identification,sum(no_answer) as no_answer
 from bi_ops_cc where pos=1 
AND date_format(event_date,"%Y%m")=date_format(curdate(),"%Y%m")-1
group by date_format(event_date,"%Y%m"),agent_identification) t2
on t1.agent_identification=t2.agent_identification) b
on a.identification=b.agent_identification and a.yrmonth=b.yrmonth
set a.atencion_valor=b.atencion
where a.atencion_valor is null;


SELECT  'Activity y encuesta',now();

update bi_ops_cc_evaluation_inb a inner join
(select yrmonth,identification, if((sum(survey)/sum(calls_total))>1,1,sum(survey)/sum(calls_total)) survey,
if((sum(activity)/sum(calls_total))>1,1,sum(activity)/sum(calls_total)) activity from bi_ops_cc_agents_detail 
where yrmonth=date_format(curdate(),'%Y%m')-1
group by identification) b
on a.identification=b.identification
and a.yrmonth=b.yrmonth
set a.activity_valor=b.activity,
a.transferencias_valor=b.survey;

SELECT  'Calificacion encuesta',now();

update bi_ops_cc_evaluation_inb a inner join
(select date_format(call_date,'%Y%m') yrmonth, agent_identification, sum(answer2_option_1)/sum(answer_2) calification
from bi_ops_cc_survey
where date_format(call_date,'%Y%m')=date_format(curdate(),'%Y%m')-1
group by agent_identification) b
on a.identification=b.agent_identification
and a.yrmonth=b.yrmonth
set encuesta_valor=calification;

delete FROM bi_ops_cc_evaluation_inb
where yrmonth=date_format(curdate(),"%Y%m")-1 AND proceso not like '%SAC%' 
and conversion_valor is null;


DELETE FROM bi_ops_cc_evaluation_inb
where yrmonth=date_format(curdate(),"%Y%m")-1 AND conversion_valor is null and proceso like '%Coordinador%' ;

DELETE FROM bi_ops_cc_evaluation_inb
where yrmonth=date_format(curdate(),"%Y%m")-1 AND coordinador is null;

SELECT  'Metas del mes inbound ventas',now();


update bi_ops_cc_evaluation_inb
set conversion_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Preventa'
and indicador='conversion'
and yrmonth=date_format(curdate(),"%Y%m")-1)
where yrmonth=date_format(curdate(),"%Y%m")-1
 AND proceso like '%VENTAS%' ;


update bi_ops_cc_evaluation_inb
set productividad_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Preventa'
and indicador='productividad'
and yrmonth=date_format(curdate(),"%Y%m")-1)
where yrmonth=date_format(curdate(),"%Y%m")-1
 AND proceso like '%VENTAS%' ;


update bi_ops_cc_evaluation_inb
set tiempo_llamada_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Preventa'
and indicador='tiempo_llamada'
and yrmonth=date_format(curdate(),"%Y%m")-1)
where yrmonth=date_format(curdate(),"%Y%m")-1
 AND proceso like '%VENTAS%' ;


update bi_ops_cc_evaluation_inb
set atencion_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Preventa'
and indicador='atencion'
and yrmonth=date_format(curdate(),"%Y%m")-1)
where yrmonth=date_format(curdate(),"%Y%m")-1
 AND proceso like '%VENTAS%' ;


update bi_ops_cc_evaluation_inb
set calidad_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Preventa'
and indicador='calidad'
and yrmonth=date_format(curdate(),"%Y%m")-1)
where yrmonth=date_format(curdate(),"%Y%m")-1
 AND proceso like '%VENTAS%' ;


update bi_ops_cc_evaluation_inb
set conocimientos_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Preventa'
and indicador='conocimientos'
and yrmonth=date_format(curdate(),"%Y%m")-1)
where yrmonth=date_format(curdate(),"%Y%m")-1
 AND proceso like '%VENTAS%' ;


update bi_ops_cc_evaluation_inb
set transferencias_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Preventa'
and indicador='transferencias'
and yrmonth=date_format(curdate(),"%Y%m")-1)
where yrmonth=date_format(curdate(),"%Y%m")-1
 AND proceso like '%VENTAS%' ;


update bi_ops_cc_evaluation_inb
set encuesta_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Preventa'
and indicador='calificacion'
and yrmonth=date_format(curdate(),"%Y%m")-1)
where yrmonth=date_format(curdate(),"%Y%m")-1
 AND proceso like '%VENTAS%' ;


update bi_ops_cc_evaluation_inb
set activity_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Preventa'
and indicador='activity'
and yrmonth=date_format(curdate(),"%Y%m")-1)
where yrmonth=date_format(curdate(),"%Y%m")-1
 AND proceso like '%VENTAS%' ;



SELECT  'Metas del mes inbound SAC',now();


update bi_ops_cc_evaluation_inb
set productividad_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Posventa'
and indicador='productividad'
and yrmonth=date_format(curdate(),"%Y%m")-1)
where yrmonth=date_format(curdate(),"%Y%m")-1
 AND proceso like '%SAC%' ;


update bi_ops_cc_evaluation_inb
set tiempo_llamada_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Posventa'
and indicador='tiempo llamada'
and yrmonth=date_format(curdate(),"%Y%m")-1)
where yrmonth=date_format(curdate(),"%Y%m")-1
 AND proceso like '%SAC%' ;


update bi_ops_cc_evaluation_inb
set atencion_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Posventa'
and indicador='atencion'
and yrmonth=date_format(curdate(),"%Y%m")-1)
where yrmonth=date_format(curdate(),"%Y%m")-1
 AND proceso like '%SAC%' ;


update bi_ops_cc_evaluation_inb
set calidad_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Posventa'
and indicador='calidad'
and yrmonth=date_format(curdate(),"%Y%m")-1)
where yrmonth=date_format(curdate(),"%Y%m")-1
 AND proceso like '%SAC%' ;


update bi_ops_cc_evaluation_inb
set conocimientos_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Posventa'
and indicador='conocimientos'
and yrmonth=date_format(curdate(),"%Y%m")-1)
where yrmonth=date_format(curdate(),"%Y%m")-1
 AND proceso like '%SAC%' ;


update bi_ops_cc_evaluation_inb
set transferencias_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Posventa'
and indicador='transferencias'
and yrmonth=date_format(curdate(),"%Y%m")-1)
where yrmonth=date_format(curdate(),"%Y%m")-1
 AND proceso like '%SAC%' ;


update bi_ops_cc_evaluation_inb
set encuesta_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Posventa'
and indicador='calificacion'
and yrmonth=date_format(curdate(),"%Y%m")-1)
where yrmonth=date_format(curdate(),"%Y%m")-1
 AND proceso like '%SAC%' ;

SELECT  'Tiempos disponibles',now();

update bi_ops_cc_evaluation_inb a inner join
(select identification,sum(total_onphone_time) onphone, sum(total_pause_time) pause,
sum(total_connected_time) connected, sum(total_available_time) available from bi_ops_cc_agents_detail 
where yrmonth=date_format(curdate(),'%Y%m')-1
group by identification) b
on a.identification=b.identification
set
total_onphone_time=onphone,
total_pause_time=pause,
total_connected_time=connected,
total_available_time=available
where yrmonth=date_format(curdate(),'%Y%m')-1;

update bi_ops_cc_evaluation_inb
set activity_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Posventa'
and indicador='activity'
and yrmonth=date_format(curdate(),'%Y%m')-1)
where yrmonth=date_format(curdate(),'%Y%m')-1
 AND proceso like '%SAC%' ;

SELECT  'Valor de productividad',now();

update bi_ops_cc_evaluation_inb 
set productividad_valor=(total_available_time-total_pause_time)/total_available_time 
where yrmonth=date_format(curdate(),'%Y%m')-1;

SELECT  'Llenar datos null',now();


update bi_ops_cc_evaluation_inb
set atencion_valor=1
where yrmonth=date_format(curdate(),'%Y%m')-1
and atencion_valor is null;


update bi_ops_cc_evaluation_inb
set transferencias_valor=0
where yrmonth=date_format(curdate(),'%Y%m')-1
and transferencias_valor is null;


update bi_ops_cc_evaluation_inb
set activity_valor=0
where yrmonth=date_format(curdate(),'%Y%m')-1
and activity_valor is null;


SELECT  'Actualizar datos de los indicadores',now();
update bi_ops_cc_evaluation_inb
set 
conversion_indicador=if(conversion_valor>=conversion_meta,1,conversion_valor/conversion_meta),
productividad_indicador=if(productividad_valor>=productividad_meta,1,productividad_valor/productividad_meta),
tiempo_llamada_indicador=if(tiempo_llamada_valor>=tiempo_llamada_meta,tiempo_llamada_meta/tiempo_llamada_valor,tiempo_llamada_valor/tiempo_llamada_meta),
atencion_indicador=if(atencion_valor>=atencion_meta,1,atencion_valor/atencion_meta),
calidad_indicador=if(calidad_valor>=calidad_meta,1,calidad_valor/calidad_meta),
conocimientos_indicador=if(conocimientos_valor>conocimientos_meta,1,conocimientos_valor/conocimientos_meta),
transferencias_indicador=if(transferencias_valor>transferencias_meta,1,transferencias_valor/transferencias_meta),
encuesta_indicador=if(encuesta_valor>encuesta_meta,1,encuesta_valor/encuesta_meta),
activity_indicador=if(activity_valor>activity_meta,1,activity_valor/activity_meta),
retrasos_indicador=if(retrasos_valor=1,'0.9',if(retrasos_valor=2,'0.8',if(retrasos_valor>2,0,1))),
inasistencias_indicador=if(inasistencias_valor=1,'0.5',if(inasistencias_valor>1,0,1))
where yrmonth=date_format(curdate(),'%Y%m')-1;


update bi_ops_cc_evaluation_inb
SET
total_productividad = conversion_indicador*0.3 + atencion_indicador*0.2 + productividad_indicador*0.25 + tiempo_llamada_indicador*0.25,
total_calidad = calidad_indicador*0.3 + conocimientos_indicador*0.2 + transferencias_indicador*0.1 + encuesta_indicador*0.3 + activity_indicador*0.1,
total_asistencias =  retrasos_indicador*0.5+inasistencias_indicador*0.5
where yrmonth=date_format(curdate(),'%Y%m')-1
and proceso like '%VENTAS%';

update bi_ops_cc_evaluation_inb
SET
total_productividad = atencion_indicador*0.35 + productividad_indicador*0.35 + tiempo_llamada_indicador*0.3,
total_calidad = calidad_indicador*0.3 + conocimientos_indicador*0.2 + transferencias_indicador*0.1 + encuesta_indicador*0.3 + activity_indicador*0.1,
total_asistencias =  retrasos_indicador*0.5+inasistencias_indicador*0.5
where yrmonth=date_format(curdate(),'%Y%m')-1
and proceso like '%SAC%';


update bi_ops_cc_evaluation_inb
set
total_calidad=if(calidad_indicador is null and conocimientos_indicador is null and encuesta_indicador is null,
					transferencias_indicador*0.5 + activity_indicador*0.5, if(calidad_indicador is not null and 
					conocimientos_indicador is null and encuesta_indicador is null,calidad_indicador*0.6 + 
					transferencias_indicador*0.2 + activity_indicador*0.2, if(calidad_indicador is null and 
					conocimientos_indicador is not null and encuesta_indicador is null,conocimientos_indicador*0.5 + 
					transferencias_indicador*0.25 + activity_indicador*0.25, if(calidad_indicador is null and 
					conocimientos_indicador is null and encuesta_indicador is not null,encuesta_indicador*0.6 + 
					transferencias_indicador*0.2 + activity_indicador*0.2, if(calidad_indicador is not null and 
					conocimientos_indicador is null and encuesta_indicador is not null,calidad_indicador*0.375 + 
					encuesta_indicador*0.375 + transferencias_indicador*0.125 + activity_indicador*0.125,
					if(calidad_indicador is null and conocimientos_indicador is not null and 
					encuesta_indicador is not null,conocimientos_indicador*(0.2/0.7) + 
					encuesta_indicador*(0.3/0.7) + transferencias_indicador*(0.1/0.7) + activity_indicador*(0.1/0.7),
					if(calidad_indicador is not null and conocimientos_indicador is not null and 
					encuesta_indicador is null,conocimientos_indicador*(0.2/0.7) + 
					calidad_indicador*(0.3/0.7) + transferencias_indicador*(0.1/0.7) + activity_indicador*(0.1/0.7),
					total_calidad)))))))
where total_calidad is null;


SELECT  'Actualizar indicador de eva general',now();
update bi_ops_cc_evaluation_inb
set total_eva = total_productividad*0.4 + total_calidad*0.5 + total_asistencias*0.1
where yrmonth=date_format(curdate(),'%Y%m')-1;


SELECT  'Insertar datos de la tabla de coordinadores ventas',now();

insert into bi_ops_cc_evaluation_inb_coord (yrmonth, coordinador, conversion_indicador, productividad_indicador, 
tiempo_llamada_indicador, atencion, calidad_indicador, conocimientos_promedio_grupo, transferencias_indicador,
encuesta_indicador, activity_indicador, retrasos_indicador, inasistencias_indicador)
select date_format(curdate(),'%Y%m')-1,
coordinador,avg(conversion_indicador), avg(productividad_indicador),avg(tiempo_llamada_indicador),
avg(atencion_indicador),avg(calidad_indicador), avg(conocimientos_indicador), avg(transferencias_indicador),
avg(encuesta_indicador), avg(activity_indicador), avg(retrasos_indicador), avg(inasistencias_indicador)
from bi_ops_cc_evaluation_inb
where yrmonth=date_format(curdate(),'%Y%m')-1
and proceso like '%VENTAS%'
group by coordinador;


SELECT  'Ingresar indicadores de servicio y abandono',now();
update bi_ops_cc_evaluation_inb_coord
set
nivel_de_servicio_indicador=(select sum(answered_in_wh_20)/sum(net_event)
 from bi_ops_cc where net_event=1 and pre=1
and date_format(event_date,'%Y%m')=
date_format(curdate(),'%Y%m')-1
group by year(event_date),month(event_date)),
abandono_indicador=(select sum(unanswered_in_wh)/sum(net_event)
 from bi_ops_cc where net_event=1 and pre=1
and date_format(event_date,'%Y%m')=
date_format(curdate(),'%Y%m')-1
group by year(event_date),month(event_date))
where yrmonth=date_format(curdate(),'%Y%m')-1;

update bi_ops_cc_evaluation_inb_coord
set
nivel_de_servicio_indicador=if(nivel_de_servicio_indicador>=0.8,1,0),
abandono_indicador=if(abandono_indicador<=0.05,1,0)
where yrmonth=date_format(curdate(),'%Y%m')-1;


SELECT  'Totales por coordinador',now();
update bi_ops_cc_evaluation_inb_coord
set 
total_productividad = conversion_indicador*0.25 + atencion*0.15 + productividad_indicador*0.15 + tiempo_llamada_indicador*0.15
						+ nivel_de_servicio_indicador*0.15 + abandono_indicador*0.15,
total_calidad = calidad_indicador*0.3 + conocimientos_indicador*0.2 + transferencias_indicador*0.1 + encuesta_indicador*0.2 +
						activity_indicador*0.1 + puntualidad*0.05 + feedback*0.05,
total_asistencias = retrasos_indicador*0.5 + inasistencias_indicador*0.5
where yrmonth=date_format(curdate(),'%Y%m')-1;


SELECT  'Insertar datos de la tabla de coordinadores SAC',now();

insert into bi_ops_cc_evaluation_inb_coord (yrmonth, coordinador, productividad_indicador, 
tiempo_llamada_indicador, atencion, calidad_indicador, conocimientos_promedio_grupo, transferencias_indicador,
encuesta_indicador, activity_indicador, retrasos_indicador, inasistencias_indicador)
select date_format(curdate(),'%Y%m')-1,
coordinador, avg(productividad_indicador),avg(tiempo_llamada_indicador),
avg(atencion_indicador),avg(calidad_indicador), avg(conocimientos_indicador), avg(transferencias_indicador),
avg(encuesta_indicador), avg(activity_indicador), avg(retrasos_indicador), avg(inasistencias_indicador)
from bi_ops_cc_evaluation_inb
where yrmonth=date_format(curdate(),'%Y%m')-1
and proceso like '%SAC%'
group by coordinador;


SELECT  'Ingresar indicadores de servicio y abandono',now();
update bi_ops_cc_evaluation_inb_coord
set
nivel_de_servicio_indicador=(select sum(answered_in_wh_20)/sum(net_event)
 from bi_ops_cc where net_event=1 and pos=1
and date_format(event_date,'%Y%m')=
date_format(curdate(),'%Y%m')-1
group by year(event_date),month(event_date)),
abandono_indicador=(select sum(unanswered_in_wh)/sum(net_event)
 from bi_ops_cc where net_event=1 and pos=1
and date_format(event_date,'%Y%m')=
date_format(curdate(),'%Y%m')-1
group by year(event_date),month(event_date))
where yrmonth=date_format(curdate(),'%Y%m')-1
AND nivel_de_servicio_indicador is null;

update bi_ops_cc_evaluation_inb_coord
set
nivel_de_servicio_indicador=if(nivel_de_servicio_indicador>=0.8,1,0),
abandono_indicador=if(abandono_indicador<=0.05,1,0)
where yrmonth=date_format(curdate(),'%Y%m')-1
AND conversion_indicador is null;


SELECT  'Totales por coordinador',now();
update bi_ops_cc_evaluation_inb_coord
set 
total_productividad = atencion*0.2 + productividad_indicador*0.2 + tiempo_llamada_indicador*0.2
						+ nivel_de_servicio_indicador*0.2 + abandono_indicador*0.2,
total_calidad = calidad_indicador*0.3 + conocimientos_indicador*0.2 + transferencias_indicador*0.1 + encuesta_indicador*0.2 +
						activity_indicador*0.1 + puntualidad*0.05 + feedback*0.05,
total_asistencias = retrasos_indicador*0.5 + inasistencias_indicador*0.5
where yrmonth=date_format(curdate(),'%Y%m')-1
AND conversion_indicador is null;


SELECT  'Actualizar indicador de eva general de coord',now();
update bi_ops_cc_evaluation_inb_coord
set total_eva = total_productividad*0.4 + total_calidad*0.5 + total_asistencias*0.1
where yrmonth=date_format(curdate(),'%Y%m')-1;

#Datos de chats de live chat
SELECT  'Insertar datos del mes chat',now();
insert into  bi_ops_cc_evaluation_chat (yrmonth,identification,productividad_valor, tiempo_chat_valor, nsu_valor)
select date_format(date, "%Y%m") yrmonth, b.identification,sum(available_sec)/(sum(available_sec)+sum(away_sec)) productividad, avg(act_sec)/60 act, sum(rated_good)/sum(rated) survey
from tbl_bi_ops_cc_chats_performance_agents a
inner join tbl_bi_ops_cc_chats_agents_parameters b 
on a.operator_name=b.operator_name
where date_format(date, "%Y%m")=date_format(curdate(), "%Y%m")-1
group by b.identification;

SELECT  'Insertar datos de ordenes brutas de chats',now();
update bi_ops_cc_evaluation_chat t1
inner join (select AssistedSalesOperator, b.identification,count(distinct(OrderNum)) orders from customer_service.tbl_telesales_co a 
inner join tbl_bi_ops_cc_chats_agents_parameters b 
on a.AssistedSalesOperator=b.sales_operator
where date_format(Date, "%Y%m")=date_format(curdate(), "%Y%m")-1
and OrderBeforeCan=1
group by AssistedSalesOperator) t2
on t1.identification=t2.identification
set
total_orders=orders
where yrmonth=date_format(curdate(), "%Y%m")-1;

SELECT  'Indicador de conversión',now();
update bi_ops_cc_evaluation_chat
set conversion_valor=total_orders/total_chats
where yrmonth=date_format(curdate(), "%Y%m")-1;

SELECT  'Insertar datos de activity',now();
update bi_ops_cc_evaluation_chat t1
inner join (SELECT c.username,count(1) cant
FROM activities a 
left join users c on a.user_id=c.id where canal='chat'
and date_format(date_time,"%Y%m")= date_format(curdate(),"%Y%m")-1
group by c.username) t2
on t1.identification=t2.username
set
total_activity=cant
where yrmonth=date_format(curdate(), "%Y%m")-1;

SELECT  'Indicador de activity',now();
update bi_ops_cc_evaluation_chat
set activity_valor=total_activity/total_chats
where yrmonth=date_format(curdate(), "%Y%m")-1;

SELECT  'Actualizar datos',now();
update bi_ops_cc_evaluation_chat a
inner join bi_ops_CS_matrix_history b on a.identification=b.identification
set 
a.agent_ext=b.phone,
agente=name,
coordinador=coordinator,
proceso=process
 where 
agente is null;


delete from bi_ops_cc_evaluation_chat
where (proceso not like '%CHAT%'
or proceso is null)
and yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


update bi_ops_cc_evaluation_chat a
set nivel_de_servicio_valor= (SELECT sum(chats)/sum(queued) as service FROM tbl_bi_ops_cc_chats_performance
where date_format(date, "%Y%m")=date_format(curdate(), "%Y%m")-1)
where yrmonth=date_format(curdate(), "%Y%m")-1;

SELECT  'Actualizar metas de chat',now();

update bi_ops_cc_evaluation_chat
set conversion_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Chat'
and indicador='conversion'
and yrmonth=date_format(curdate(),"%Y%m")-1)
where yrmonth=date_format(curdate(),"%Y%m")-1;


update bi_ops_cc_evaluation_chat
set productividad_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Chat'
and indicador='productividad'
and yrmonth=date_format(curdate(),"%Y%m")-1)
where yrmonth=date_format(curdate(),"%Y%m")-1;


update bi_ops_cc_evaluation_chat
set tiempo_chat_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Chat'
and indicador='tiempo de gestion'
and yrmonth=date_format(curdate(),"%Y%m")-1)
where yrmonth=date_format(curdate(),"%Y%m")-1;


update bi_ops_cc_evaluation_chat
set nivel_de_servicio_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Chat'
and indicador='atencion'
and yrmonth=date_format(curdate(),"%Y%m")-1)
where yrmonth=date_format(curdate(),"%Y%m")-1;


update bi_ops_cc_evaluation_chat
set calidad_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Chat'
and indicador='calidad'
and yrmonth=date_format(curdate(),"%Y%m")-1)
where yrmonth=date_format(curdate(),"%Y%m")-1;


update bi_ops_cc_evaluation_chat
set conocimientos_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Chat'
and indicador='conocimientos'
and yrmonth=date_format(curdate(),"%Y%m")-1)
where yrmonth=date_format(curdate(),"%Y%m")-1;


update bi_ops_cc_evaluation_chat
set nsu_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Chat'
and indicador='calificacion'
and yrmonth=date_format(curdate(),"%Y%m")-1)
where yrmonth=date_format(curdate(),"%Y%m")-1;


update bi_ops_cc_evaluation_chat
set activity_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Chat'
and indicador='activity'
and yrmonth=date_format(curdate(),"%Y%m")-1)
where yrmonth=date_format(curdate(),"%Y%m")-1;


SELECT  'Actualizar datos de los indicadores chat',now();
update bi_ops_cc_evaluation_chat
set 
conversion_indicador=if(conversion_valor>=conversion_meta,1,conversion_valor/conversion_meta),
productividad_indicador=if(productividad_valor>=productividad_meta,1,productividad_valor/productividad_meta),
tiempo_chat_indicador=if(tiempo_chat_valor>=tiempo_chat_meta,tiempo_chat_meta/tiempo_chat_valor,1),
nivel_de_servicio_indicador=if(nivel_de_servicio_valor>=nivel_de_servicio_meta,1,0),
calidad_indicador=if(calidad_valor>=calidad_meta,1,calidad_valor/calidad_meta),
conocimientos_indicador=if(conocimientos_valor>conocimientos_meta,1,conocimientos_valor/conocimientos_meta),
nsu_indicador=if(nsu_valor>nsu_meta,1,nsu_valor/nsu_meta),
activity_indicador=if(activity_valor>activity_meta,1,activity_valor/activity_meta),
retrasos_indicador=if(retrasos_valor=1,'0.9',if(retrasos_valor=2,'0.8',if(retrasos_valor>2,0,1))),
inasistencias_indicador=if(inasistencias_valor=1,'0.5',if(inasistencias_valor>1,0,1))
where yrmonth=date_format(curdate(),"%Y%m")-1;


update bi_ops_cc_evaluation_chat
SET
total_productividad = conversion_indicador*0.3 + productividad_indicador*0.25 + tiempo_chat_indicador*0.25 + nivel_de_servicio_indicador*0.2,
total_calidad = calidad_indicador*0.3 + conocimientos_indicador*0.2 + nsu_indicador*0.4 + activity_indicador*0.1,
total_asistencias =  retrasos_indicador*0.5+inasistencias_indicador*0.5
where yrmonth=date_format(curdate(),"%Y%m")-1;


SELECT  'Actualizar indicador de eva general',now();
update bi_ops_cc_evaluation_chat
set total_eva = total_productividad*0.4 + total_calidad*0.5 + total_asistencias*0.1
where yrmonth=date_format(curdate(),"%Y%m")-1;



SELECT  'Insertar datos de backoffice',now();


insert into bi_ops_cc_evaluation_bo (yrmonth,identification,respuesta_valor)
select a.yrmonth,a.identification,sum(a.avg_sum)/sum(cant) first_reply_time_minutes from
(select t1.yrmonth,t2.identification,t1.cant,t1.first_reply_time_minutes,t1.cant*t1.first_reply_time_minutes as avg_sum from
(select  date_format(date_creation,'%Y%m') yrmonth,
asignee, count(1) cant, avg(first_reply_time_minutes)/60 first_reply_time_minutes from bi_ops_tickets
where date_format(date_creation,'%Y%m')=
date_format(curdate(),'%Y%m')-1
and first_reply_time_minutes<>""
group by asignee) t1 inner join
(select * from bi_ops_tickets_agents where identification is not null) t2 on t1.asignee=t2.name) a
group by a.identification;


update bi_ops_cc_evaluation_bo a
inner join
(select a.identification,sum(cumple)/sum(solved) solved_on_time from
(select t2.identification,t1.cumple,t1.solved from
(select asignee, sum(cumple) cumple,sum(solved) solved from bi_ops_tickets
where date_format(date_solved,"%Y%m")=
date_format(curdate(),"%Y%m")-1
group by asignee) t1 inner join
(select * from bi_ops_tickets_agents where identification is not null) t2 on t1.asignee=t2.name) a
group by a.identification) b
on a.identification=b.identification
set solucion_valor=solved_on_time
where a.yrmonth=date_format(curdate(),"%Y%m")-1;



SELECT  'Actualizar datos de asesores backoffice',now();
update bi_ops_cc_evaluation_bo a
inner join bi_ops_CS_matrix_history b on a.identification=b.identification
and a.yrmonth=b.yrmonth
set 
a.agent_ext=b.phone,
agente=name,
coordinador=coordinator,
proceso=process
 where 
agente is null;

SELECT  'Llenar meta de bo',now();

update bi_ops_cc_evaluation_bo
set ocupacion_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='BO'
and indicador='ocupacion'
and yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


update bi_ops_cc_evaluation_bo
set respuesta_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='BO'
and indicador='respuesta'
and yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


update bi_ops_cc_evaluation_bo
set solucion_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='BO'
and indicador='solucion'
and yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


update bi_ops_cc_evaluation_bo
set calidad_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='BO'
and indicador='calidad'
and yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


update bi_ops_cc_evaluation_bo
set conocimientos_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='BO'
and indicador='conocimientos'
and yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;

insert into bi_ops_tickets_occupancy_times (yrmonth,identification,total_time_sec)
select concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1 yrmonth,
identification,sum(time_total) total_sec from
(select * from
(select asignee, sum(time_total) time_total from
(select a.asignee,a.tipo_solicitud,(a.cant*b.tmo*60) time_total from
(select asignee,tipo_solicitud, sum(summation_column) cant from bi_ops_tickets
where concat(year(date_solved),if(month(date_solved)<10,concat(0,month(date_solved)),month(date_solved)))=
concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1
group by asignee,tipo_solicitud, tipo_solicitud) a
left join
bi_ops_tickets_solicitudes b
on a.tipo_solicitud=b.solicitud) t
group by asignee) t1
inner join bi_ops_tickets_agents t2
on t1.asignee=t2.name) h
group by identification;


#Ocupacion Stephanie Martinez
update bi_ops_tickets_occupancy_times
set total_time_sec=total_time_sec+(select count(1)*960 from bob_live_co.sales_order_item_status_history a
inner join  bob_live_co.sales_order_item_status b on a.fk_sales_order_item_status=b.id_sales_order_item_status
inner join bob_live_co.acl_user c on a.fk_acl_user=c.id_acl_user
where b.name in ('refunded','store_credit_issued')
and date_format(a.created_at,'%Y%m')=date_format(curdate(),'%Y%m')-1
and username='angela.clavijo')
where yrmonth=date_format(curdate(),'%Y%m')-1
and identification='1016020100';

#Ocupacion Yuri Cortes
update bi_ops_tickets_occupancy_times
set total_time_sec=total_time_sec+(select count(1)*120 from solicitud
where date_format(FechaHora,'%Y%m')=date_format(curdate(),'%Y%m')-1
and RazonReembolso='Cancelacion')
where yrmonth=date_format(curdate(),'%Y%m')-1
and identification='1012373390';

#Agregar turnos por asesor
update bi_ops_tickets_occupancy_times a
inner join bi_ops_CS_matrix b
on a.identification=b.identification
set a.shift=b.shift_pattern
where a.yrmonth=date_format(curdate(),'%Y%m')-1;

#Agregar tiempo disponible en el mes 
update bi_ops_tickets_occupancy_times a
inner join bi_ops_cc_shift_details b
on a.shift=b.shift
set
total_time_available=mes_segundos
where a.yrmonth=date_format(curdate(),'%Y%m')-1;

update bi_ops_tickets_occupancy_times
set occupancy_rate=total_time_sec/total_time_available
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;

update bi_ops_cc_evaluation_bo a
inner join bi_ops_tickets_occupancy_times b
on a.identification=b.identification
and a.yrmonth=b.yrmonth
set ocupacion_valor=occupancy_rate
where a.yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


SELECT  'Actualizar indicadores',now();
update bi_ops_cc_evaluation_bo
set 
ocupacion_indicador=if(ocupacion_valor>=ocupacion_meta,1,ocupacion_valor/ocupacion_meta),
respuesta_indicador=if(respuesta_valor<=respuesta_meta,1,respuesta_meta/respuesta_valor),
solucion_indicador=solucion_valor,
calidad_indicador=if(calidad_valor>=calidad_meta,1,calidad_valor/calidad_meta),
conocimientos_indicador=if(conocimientos_valor>conocimientos_meta,1,conocimientos_valor/conocimientos_meta),
retrasos_indicador=if(retrasos_valor=1,'0.9',if(retrasos_valor=2,'0.8',if(retrasos_valor>2,0,1))),
inasistencias_indicador=if(inasistencias_valor=1,'0.5',if(inasistencias_valor>1,0,1))
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;



update bi_ops_cc_evaluation_bo
SET
total_productividad = ocupacion_indicador*0.4 + respuesta_indicador*0.3 + solucion_indicador*0.3,
total_calidad = if(calidad_indicador is null,conocimientos_indicador,if(conocimientos_indicador is null,calidad_indicador,calidad_indicador*0.6 + conocimientos_indicador*0.4)),
total_asistencias =  retrasos_indicador*0.5+inasistencias_indicador*0.5
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1 ;


SELECT  'Actualizar indicador de eva general bo',now();
update bi_ops_cc_evaluation_bo
set total_eva = if(total_calidad is null,total_productividad*0.8 + total_asistencias*0.2,total_productividad*0.4 + total_calidad*0.5 + total_asistencias*0.1)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


SELECT  'Insertar datos de eva de coordinador bo',now();

insert into bi_ops_cc_evaluation_bo_coord (yrmonth,coordinador, productividad,respuesta,solucion, conversion, tiempo_llamada, 
servicio, calidad_indicador, conocimientos_indicador, encuesta_chat,activity,retrasos_indicador, inasistencias_indicador)
select yrmonth,coordinador,productividad/(cant_bo+cant_chat) productividad, respuesta_bo, solucion_bo, conversion_chat conversion,
tiempo_chat,servicio_chat,
calidad/(cant_bo+cant_chat) calidad, conocimientos/(cant_bo+cant_chat) conocimientos, encuesta_chat, activity_chat,
retrasos/(cant_bo+cant_chat)
retrasos, inasistencias/(cant_bo+cant_chat) inasistencias from
(SELECT a.yrmonth,a.coordinador,a.cant_bo, b.cant_chat, a.ocupacion_bo*a.cant_bo+b.productividad_chat*b.cant_chat productividad,
a.respuesta_bo, a.solucion_bo, b.conversion_chat,
b.tiempo_chat, b.servicio_chat, a.calidad_bo*a.cant_bo+b.calidad_chat*b.cant_chat calidad,
a.conocimientos_bo*a.cant_bo+b.conocimientos_chat*b.cant_chat conocimientos, encuesta_chat, activity_chat,
a.retrasos_bo*a.cant_bo+b.retrasos_chat*b.cant_chat retrasos, 
a.inasistencias_bo*a.cant_bo+b.inasistencias_chat*b.cant_chat inasistencias from
(select yrmonth,coordinador,count(1) cant_bo,avg(ocupacion_indicador) ocupacion_bo, avg(respuesta_indicador) respuesta_bo, 
avg(solucion_indicador) solucion_bo,
avg(calidad_indicador) calidad_bo,avg(conocimientos_indicador) conocimientos_bo, avg(inasistencias_indicador) inasistencias_bo,
avg(retrasos_indicador) retrasos_bo 
from bi_ops_cc_evaluation_bo 
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1
group by coordinador) a
join
(select yrmonth as yrmonth2,coordinador as coordinador2,count(1) cant_chat, avg(conversion_indicador) conversion_chat, avg(productividad_indicador) 
productividad_chat, avg(tiempo_chat_indicador) tiempo_chat, avg(nivel_de_servicio_indicador) servicio_chat,
avg(calidad_indicador) calidad_chat,avg(nsu_indicador) encuesta_chat, avg(conocimientos_indicador) conocimientos_chat, avg(activity_indicador)
activity_chat,
avg(inasistencias_indicador) 
inasistencias_chat,
avg(retrasos_indicador) retrasos_chat 
FROM bi_ops_cc_evaluation_chat
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1
group by coordinador) b) h;


SELECT  'Backlog promedio diario',now();
update bi_ops_cc_evaluation_bo_coord 
set backlog_valor=
(SELECT avg(backlog_inc) FROM bi_ops_tickets_backlog
where concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)))=
concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


update bi_ops_cc_evaluation_bo_coord 
set backlog_meta=30
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


SELECT  'Encuesta bo',now();
update bi_ops_cc_evaluation_bo_coord 
set encuesta_bo_valor=
(SELECT sum(good)/sum(satisfaction) FROM bi_ops_tickets
where concat(year(date_creation),if(month(date_creation)<10,concat(0,month(date_creation)),month(date_creation)))=
concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


update bi_ops_cc_evaluation_bo_coord 
set encuesta_bo_meta=0.9
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;

SELECT  'Actualizar indicadores',now();
update bi_ops_cc_evaluation_bo_coord
set 
backlog_indicador=if(backlog_valor<=backlog_meta,1,backlog_meta/backlog_valor),
encuesta_bo_indicador=if(encuesta_bo_valor>=encuesta_bo_meta,1,encuesta_bo_valor/encuesta_bo_meta)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


update bi_ops_cc_evaluation_bo_coord
SET
total_productividad = productividad*0.15 + respuesta*0.15 + solucion*0.15 + backlog_indicador*0.1 + tiempo_llamada*0.15 + servicio*0.15 + conversion*0.15,
total_calidad = calidad_indicador*0.3 + conocimientos_indicador*0.2 + encuesta_bo_indicador*0.15 + encuesta_chat*0.15 + activity*0.1 + puntualidad*0.05 + feedback*0.05,
total_asistencias =  retrasos_indicador*0.5+inasistencias_indicador*0.5
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1 ;


SELECT  'Actualizar indicador de eva general bo coord',now();
update bi_ops_cc_evaluation_bo_coord
set total_eva = total_productividad*0.4 + total_calidad*0.5 + total_asistencias*0.1
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


SELECT  'Insertar datos senior coordinator Inbound',now();
insert into bi_ops_cc_evaluation_senior_coord (yrmonth, coordinador, cant_inb, productividad_inb,  calidad_inb,encuesta_inb)
select concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1, "Catalina Olaya",
count(1), avg(productividad_indicador),
avg(calidad_indicador), avg(encuesta_indicador)
from bi_ops_cc_evaluation_inb
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1
and proceso like '%SAC%';

SELECT  'Insertar datos senior coordinator Chat',now();
update bi_ops_cc_evaluation_senior_coord a
inner join
(select yrmonth,count(1) cant, avg(conversion_indicador) conversion, avg(productividad_indicador) productividad, 
avg(nivel_de_servicio_indicador) servicio,
avg(calidad_indicador) calidad,avg(nsu_indicador) encuesta
FROM bi_ops_cc_evaluation_chat
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1) b
on a.yrmonth=b.yrmonth
set
a.cant_chat= b.cant,
a.conversion = b.conversion,
a.productividad_chat = b.productividad,
a.atencion = b.servicio,
a.calidad_chat = b.calidad,
a.encuesta_chat = b.encuesta;

SELECT  'Insertar datos senior coordinator BO',now();
update bi_ops_cc_evaluation_senior_coord a
INNER JOIN
(select yrmonth,count(1) cant_bo,avg(ocupacion_indicador) ocupacion,  
avg(solucion_indicador) solucion,
avg(calidad_indicador) calidad
from bi_ops_cc_evaluation_bo 
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1) b
set
a.cant_bo= b.cant_bo,
a.ocupacion_bo = b.ocupacion,
a.solucion = b.solucion,
a.calidad_chat = b.calidad
where a.yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;

SELECT  'Insertar datos senior coordinator servicio y abandono',now();
update bi_ops_cc_evaluation_senior_coord a
INNER JOIN
(SELECT yrmonth,avg(nivel_de_servicio_indicador) servicio,avg(abandono_indicador) abandono FROM bi_ops_cc_evaluation_inb_coord
where conversion_indicador is null
and yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1) b
on a.yrmonth=b.yrmonth
set
a.servicio=b.servicio,
a.abandono=b.abandono;

SELECT  'Insertar datos senior coordinator encuesta_bo',now();
update bi_ops_cc_evaluation_senior_coord 
set
encuesta_bo=(SELECT encuesta_bo_indicador FROM bi_ops_cc_evaluation_bo_coord
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;

SELECT  'Insertar datos senior coordinator ocupacion chat',now();
update bi_ops_cc_evaluation_senior_coord 
set
ocupacion_chat_valor=(select avg(t1.ocupacion) from
(SELECT concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) yrmonth,
agent_ext,sum(chatting_sec)/(sum(available_sec)+sum(away_sec)) ocupacion 
FROM tbl_bi_ops_cc_chats_performance_agents
where concat(year(date),if(month(date)<10,concat(0,month(date)),month(date)))=concat(year(curdate()),if(month(curdate())<10,
concat(0,month(curdate())),month(curdate())))-1 group by agent_ext) t1
left join
(select h.yrmonth,phone, sum(h.canti) canti from
(SELECT concat(year(t.date_time),if(month(t.date_time)<10,concat(0,month(t.date_time)),month(t.date_time))) yrmonth,
date(t.date_time) date,t.phone, count(t.date_time) canti FROM 
(SELECT a.date_time,a.macro_proceso,a.telefono_email,c.username,c.email,c.phone,c.canal 
FROM activities a 
left join users c on a.user_id=c.id where canal='chat' ) as t
where t.phone is not null and t.phone<>'0000'
group by date(date_time),t.phone) h
where h.yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate()))) -1
group by h.yrmonth,h.phone) t2 on t1.yrmonth=t2.yrmonth and t1.agent_ext=t2.phone
where t1.agent_ext<>0)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;

#Actualizado 18/12/2013
SELECT  'Insertar datos senior coordinator ocupacion inbound',now();
update bi_ops_cc_evaluation_senior_coord
set ocupacion_inb_valor=(select sum(relacion)/sum(llamadas) from
(select t1.ext,t1.ocupacion*t2.llamadas relacion,t2.llamadas from
(select a.agent,ext,process,sum(total_call_duration)/sum(real_available_time) ocupacion
from customer_service_co.bi_ops_cc_occupancy_2 a inner join bi_ops_agent_names b 
on a.agent=b.agent
inner join bi_ops_CS_matrix_history c on b.ext=c.phone and
date_format(date,'%Y%m')=c.yrmonth
where date_format(date,'%Y%m')=date_format(curdate(),'%Y%m')-1
and ext<>0
and process='SAC'
group by a.agent) t1
left join
(select agent_ext,sum(net_event) llamadas from bi_ops_cc
where date_format(event_date,'%Y%m')=date_format(curdate(),'%Y%m')-1
group by agent_ext) t2
on t1.ext=t2.agent_ext) h)
where yrmonth=date_format(curdate(),'%Y%m')-1;


SELECT  'Actualizar indicadores',now();
update bi_ops_cc_evaluation_senior_coord
set 
ocupacion_inb=if(ocupacion_inb_valor>=0.7,1,ocupacion_inb_valor/0.7),
ocupacion_chat=if(ocupacion_chat_valor>=0.8,1,ocupacion_chat_valor/0.8),
contact_ratio_pos=if(contact_ratio_pos_valor<=0.5,1,0.5/contact_ratio_pos_valor),
contact_ratio_chats=if(contact_ratio_chats_valor<=0.5,1,0.7/contact_ratio_chats_valor),
contact_ratio_bo=if(contact_ratio_bo_valor<=0.5,1,0.1/contact_ratio_bo_valor)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;

update bi_ops_cc_evaluation_senior_coord
set 
ocupacion=(ocupacion_inb*cant_inb+ocupacion_chat*cant_chat+ocupacion_bo*cant_bo)/(cant_inb+cant_chat+cant_bo),
calidad_indicador=if(calidad_bo is null,(calidad_inb*cant_inb+calidad_chat*cant_chat)/(cant_inb+cant_chat),(calidad_inb*cant_inb+calidad_chat*cant_chat+calidad_bo*cant_bo)/(calidad_inb+calidad_chat+calidad_bo)),
productividad=(productividad_inb*cant_inb+productividad_chat*cant_chat)/(cant_inb+cant_chat)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;

update bi_ops_cc_evaluation_senior_coord
SET
total_productividad = ocupacion*0.15 + productividad*0.15 + servicio*0.15 + abandono*0.15 + atencion*0.15 + solucion*0.1 + conversion*0.15,
total_calidad = calidad_indicador*0.5 + encuesta_inb*0.25 + encuesta_chat*0.125 + encuesta_inb*0.125,
total_contact = contact_ratio_pos*0.4 + contact_ratio_chats*0.3 + contact_ratio_bo*0.3
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1 ;


SELECT  'Actualizar indicador de eva general senior coord',now();
update bi_ops_cc_evaluation_senior_coord
set total_eva = total_productividad*0.4 + total_calidad*0.5 + total_contact*0.1
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;

SELECT  'Insertar datos de outbound',now();
insert into bi_ops_cc_evaluation_outbound(yrmonth,agent_ext,agente,conversion_valor, contactabilidad_valor,call_duration_sec)

select t1.yrmonth,t1.agent_ext,t1.agent_name, t2.orders/t1.calls conversion, t1.contactability, t1.call_duration_seg from
((select concat(year(event_date),if(month(event_date)<10,concat(0,month(event_date)),month(event_date))) yrmonth,
sum(answered) calls,
a.agent_ext,a.agent_name,a.agent, 
sum(a.call_duration_seg) as 'call_duration_seg', 
sum(a.answered)/sum(a.calls) as 'contactability' from (SELECT
  `bi_ops_cc_outbound`.`event_date`,
  `bi_ops_cc_outbound`.`agent`,
  `bi_ops_cc_outbound`.`agent_name`,
  `bi_ops_cc_outbound`.`agent_ext`,
  Sum(`bi_ops_cc_outbound`.`call_duration_seg`) AS `call_duration_seg`,
  `bi_ops_cc_outbound`.`event_shift`,
  Sum(`bi_ops_cc_outbound`.`answered`) AS `answered`,
  Sum(`bi_ops_cc_outbound`.`calls`) AS `calls`,
  `bi_ops_cc_outbound`.`available_time`
FROM
  `bi_ops_cc_outbound`    
where calls=1 and
concat(year(event_date),if(month(event_date)<10,concat(0,month(event_date)),month(event_date)))=
concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1
GROUP BY
  `bi_ops_cc_outbound`.`event_date`,`bi_ops_cc_outbound`.`agent`,`bi_ops_cc_outbound`.`event_shift`) as a
group by a.agent_ext )) t1
left join
(select concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))) yrmonth,
agent_ext,count(distinct(order_nr)) 'orders' from
bi_ops_cc_agent_sales where 
obc=1 
and month(date)=month(curdate())-1
and year(date)=year(curdate())
group by concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))),agent_ext) t2
on t1.agent_ext=t2.agent_ext;

SELECT  'Agregar coordinador',now();

update bi_ops_cc_evaluation_outbound a
inner join bi_ops_CS_matrix_history b on a.agent_ext=b.phone 
and a.yrmonth=b.yrmonth
set a.coordinador=b.coordinator,
a.proceso=b.process;


update bi_ops_cc_evaluation_outbound a 
inner join bi_ops_CS_matrix b
on a.agent_ext=b.phone
set
a.agente=b.name,
a.coordinador=b.coordinator,
a.proceso=b.process
where a.yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1
and a.coordinador is null;


delete FROM bi_ops_cc_evaluation_outbound
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1 
AND proceso not like '%OUTBOUND%' ;

delete FROM bi_ops_cc_evaluation_outbound
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1 
and proceso IS NULL;


SELECT  'Ocupacion',now();
update bi_ops_cc_evaluation_outbound
set ocupacion_valor=call_duration_sec/available_time_sec
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1 ;

SELECT  'Metas del mes outbound',now();


update bi_ops_cc_evaluation_outbound
set conversion_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Outbound'
and indicador='conversion'
and yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


update bi_ops_cc_evaluation_outbound
set cum_meta_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Outbound'
and indicador='meta ventas'
and yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


update bi_ops_cc_evaluation_outbound
set ocupacion_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Outbound'
and indicador='ocupacion'
and yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


update bi_ops_cc_evaluation_outbound
set contactabilidad_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Outbound'
and indicador='contactabilidad'
and yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


update bi_ops_cc_evaluation_outbound
set calidad_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Outbound'
and indicador='calidad'
and yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


update bi_ops_cc_evaluation_outbound
set conocimientos_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Outbound'
and indicador='conocimientos'
and yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


SELECT  'Actualizar datos de los indicadores',now();
update bi_ops_cc_evaluation_outbound
set 
conversion_indicador=if(conversion_valor>=conversion_meta,1,conversion_valor/conversion_meta),
cump_meta=if(cum_meta_valor>=cum_meta_meta,1,cum_meta_valor/cum_meta_meta),
contactabilidad=if(contactabilidad_valor>=contactabilidad_meta,1,contactabilidad_valor/contactabilidad_meta),
ocupacion=if(ocupacion_valor>=ocupacion_meta,1,ocupacion_valor/ocupacion_meta),
calidad_indicador=if(calidad_valor>=calidad_meta,1,calidad_valor/calidad_meta),
conocimientos_indicador=if(conocimientos_valor>conocimientos_meta,1,conocimientos_valor/conocimientos_meta),
retrasos_indicador=if(retrasos_valor=1,'0.9',if(retrasos_valor=2,'0.8',if(retrasos_valor>2,0,1))),
inasistencias_indicador=if(inasistencias_valor=1,'0.5',if(inasistencias_valor>1,0,1))
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


update bi_ops_cc_evaluation_outbound
SET
total_comercial = conversion_indicador*0.3 + cump_meta*0.7,
total_productividad = ocupacion*0 + contactabilidad*1,
total_calidad = if(calidad_indicador is null, conocimientos_indicador,if(conocimientos_indicador is null,calidad_indicador,calidad_indicador*0.6 + conocimientos_indicador*0.4)),
total_asistencias =  if(retrasos_indicador<inasistencias_indicador,retrasos_indicador,inasistencias_indicador)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1 ;


SELECT  'Actualizar indicador de eva general',now();
update bi_ops_cc_evaluation_outbound
set total_eva = if(total_calidad is null, total_comercial*(0.35/0.7) + total_productividad*(0.25/0.7) +  total_asistencias*(0.1/0.7), total_comercial*0.35 + total_productividad*0.25 + total_calidad*0.3 + total_asistencias*0.1)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


SELECT  'Insertar datos de outbound coordinador',now();
insert into bi_ops_cc_evaluation_outbound_coord (yrmonth, coordinador, cump_meta, conversion_indicador, ocupacion, contactabilidad,
calidad_indicador, conocimientos_promedio_grupo, retrasos_indicador, inasistencias_indicador)
SELECT concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1,
coordinador, avg(cump_meta),avg(conversion_indicador),avg(ocupacion), avg(contactabilidad),
avg(calidad_indicador), avg(conocimientos_indicador),
avg(retrasos_indicador), avg(inasistencias_indicador) FROM bi_ops_cc_evaluation_outbound
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1 
group by coordinador
;


update bi_ops_cc_evaluation_outbound_coord
SET
total_comercial = conversion_indicador*0.3 + cump_meta*0.7,
total_productividad = ocupacion*0 + contactabilidad*1,
total_calidad = calidad_indicador*0.5 + conocimientos_indicador*0.4 + puntualidad*0.05 + feedback*0.05,
total_asistencias = retrasos_indicador*0.5 + inasistencias_indicador*0.5
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1 ;


SELECT  'Actualizar indicador de eva general',now();
update bi_ops_cc_evaluation_outbound_coord
set total_eva = total_comercial*0.35 + total_productividad*0.25 + total_calidad*0.3 + total_asistencias*0.1
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;

SELECT  'Metas del mes quality',now();


update bi_ops_cc_evaluation_training
set conocimientos_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Training'
and indicador='conocimientos'
and yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


update bi_ops_cc_evaluation_training
set comportamiento_global_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Training'
and indicador='comportamiento global'
and yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


update bi_ops_cc_evaluation_training
set error_fatal_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Training'
and indicador='precision error fatal'
and yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


update bi_ops_cc_evaluation_training
set capacitacion_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Training'
and indicador='capacitacion nuevos'
and yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


update bi_ops_cc_evaluation_training
set puntualidad_meta=(SELECT meta FROM bi_ops_cc_evaluation_parameters
where proceso='Training'
and indicador='puntualidad'
and yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


SELECT  'Actualizar datos de los indicadores quality',now();
update bi_ops_cc_evaluation_training
set 
conocimientos_indicador=if(conocimientos_valor>=conocimientos_meta,1,conocimientos_valor/conocimientos_meta),
comportamiento_global=if(comportamiento_global_valor>=comportamiento_global_meta,1,comportamiento_global_valor/comportamiento_global_meta),
error_fatal=if(error_fatal_valor>=error_fatal_meta,1,error_fatal_valor/error_fatal_meta),
capacitacion=if(capacitacion_valor>=capacitacion_meta,1,capacitacion_valor/capacitacion_meta),
puntualidad=if(puntualidad_valor>puntualidad_meta,1,puntualidad_valor/puntualidad_meta)
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;


update bi_ops_cc_evaluation_training
SET
total = conocimientos_indicador*0.3 + comportamiento_global*0.3 + error_fatal*0.15 + capacitacion*0.15 + puntualidad*0.1,
total_eva = conocimientos_indicador*0.3 + comportamiento_global*0.3 + error_fatal*0.15 + capacitacion*0.15 + puntualidad*0.1
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1 ;

SELECT  'Insertar datos generales de asesores',now();

insert into bi_ops_cc_evaluation_general (yrmonth, agent_ext,agente,coordinador,productividad, calidad, asistencias, total_eva,proceso)
select yrmonth, agent_ext,agente,coordinador,total_productividad,total_calidad,total_asistencias,total_eva,proceso from bi_ops_cc_evaluation_inb 
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;

insert into bi_ops_cc_evaluation_general (yrmonth, agent_ext,agente,coordinador,productividad, calidad, asistencias, total_eva,proceso)
select yrmonth, agent_ext,agente,coordinador,total_productividad,total_calidad,total_asistencias,total_eva,proceso 
from bi_ops_cc_evaluation_chat
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;

insert into bi_ops_cc_evaluation_general (yrmonth, agent_ext,agente,coordinador,productividad, calidad, asistencias, total_eva,proceso)
select yrmonth, agent_ext,agente,coordinador,total_productividad,total_calidad,total_asistencias,total_eva,proceso 
from bi_ops_cc_evaluation_bo
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;

insert into bi_ops_cc_evaluation_general (yrmonth, agent_ext,agente,coordinador,comercial,productividad, calidad, asistencias, total_eva,proceso)
select yrmonth, agent_ext,agente,coordinador,total_comercial,total_productividad,total_calidad,total_asistencias,total_eva,proceso 
from bi_ops_cc_evaluation_outbound
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;

SELECT  'Insertar datos generales de coordinadores',now();

insert into bi_ops_cc_evaluation_coord_general (yrmonth, coordinador,total_productividad, total_calidad, total_asistencias, total_eva,proceso)
select yrmonth, coordinador,total_productividad,total_calidad,total_asistencias,total_eva,proceso 
from bi_ops_cc_evaluation_inb_coord
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;

insert into bi_ops_cc_evaluation_coord_general (yrmonth, coordinador,total_productividad, total_calidad, total_asistencias, total_eva,proceso)
select yrmonth, coordinador,total_productividad,total_calidad,total_asistencias,total_eva,'BACK OFFICE'
from bi_ops_cc_evaluation_bo_coord
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;

insert into bi_ops_cc_evaluation_coord_general (yrmonth, coordinador,total_comercial,
total_productividad, total_calidad, total_asistencias, 
total_eva,proceso)
select yrmonth, coordinador,total_comercial,total_productividad,total_calidad,total_asistencias,total_eva,'OUTBOUND'
from bi_ops_cc_evaluation_outbound_coord
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;

insert into bi_ops_cc_evaluation_coord_general (yrmonth, coordinador,
total_productividad, total_calidad, total_contact, 
total_eva,proceso)
select yrmonth, coordinador,total_productividad,total_calidad,total_contact,total_eva,'SAC'
from bi_ops_cc_evaluation_senior_coord
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;

insert into bi_ops_cc_evaluation_coord_general (yrmonth, coordinador,total_eva,proceso)
select yrmonth, coordinador,total_eva,'TRAINING'
from bi_ops_cc_evaluation_training
where yrmonth=concat(year(curdate()),if(month(curdate())<10,concat(0,month(curdate())),month(curdate())))-1;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `bi_ops_cc_facebook`()
BEGIN

SELECT  'Inicio rutina CC Inbound Facebook',now();

#Zona horaria de Colombia

set time_zone='-5:00';

truncate table new_queue_stats;

insert into new_queue_stats(queue_stats_id,uniqueid,datetime,qname,qagent,qevent,info1,info2,info3,info4,info5) SELECT * FROM queue_stats;


SELECT  'Registro de datos new_queue_stats',now();
update new_queue_stats a inner join qevent b on a.qevent=b.event_id
inner join qname c on a.qname=c.queue_id
set call_time=if(event in ('COMPLETEAGENT','COMPLETECALLER'),info2,IF(event='TRANSFER',left(info4,instr(info4,'|')-1),null)),
hold_time=if(event in ('COMPLETEAGENT','COMPLETECALLER'),info1,if(event in ('ABANDON','EXITWITHTIMEOUT'),info3,if(event='TRANSFER',info3,null))),
start_position= if(event in ('COMPLETEAGENT','COMPLETECALLER'),info3,IF(event='TRANSFER', right(info4,length(info4)-instr(info4,'|')),if(event in ('ABANDON','EXITWITHTIMEOUT'),info2,null))),
end_position=if(event in ('ABANDON','EXITWITHTIMEOUT'),info1,null)
where b.event in ('ABANDON','COMPLETEAGENT','COMPLETECALLER','CONNECT','TRANSFER','EXITWITHTIMEOUT','RINGNOANSWER')
and c.queue in ('ivr_facebook','facebook');

truncate table bi_ops_cc_facebook;


SELECT  'Insertar datos bi_ops_cc_facebook',now();
insert into bi_ops_cc_facebook (queue_stats_id, uniqueid, event_datetime, event_date, queue, agent, event, call_duration_seg, hold_duration_seg, start_position, end_position,  event_hour, event_minute, event_weekday, event_week, event_month, event_year) 
SELECT a.queue_stats_id, a.uniqueid, a.datetime,date(a.datetime),c.queue,d.agent,b.event, a.call_time,a.hold_time,a.start_position,a.end_position,hour(a.datetime), minute(a.datetime),
weekday(a.datetime),weekofyear(a.datetime),monthname(a.datetime),year(datetime) FROM new_queue_stats a inner join qevent b on a.qevent=b.event_id
inner join qname c on a.qname=c.queue_id inner join qagent d on a.qagent=d.agent_id 
where b.event in ('ABANDON','COMPLETEAGENT','COMPLETECALLER','CONNECT','TRANSFER','RINGNOANSWER')
and c.queue in ('ivr_facebook','facebook') and date(a.datetime)>='2013-08-01'
group by uniqueid,event,queue;


SELECT  'Actualizar grabaciones',now();
update bi_ops_cc_facebook a inner join cdr b on a.uniqueid=b.uniqueid
set 
a.record=b.record;

delete from bi_ops_agent_names where agent in ('SIP/facebook1','SIP/facebook2','SIP/facebook3');

insert into bi_ops_agent_names (agent,ext) values ('SIP/facebook1',8001);
insert into bi_ops_agent_names (agent,ext) values ('SIP/facebook2',8002);
insert into bi_ops_agent_names (agent,ext) values ('SIP/facebook3',8003);

SELECT  'Insertar nombres',now();
update bi_ops_cc_facebook a inner join bi_ops_agent_names b on a.agent=b.agent inner join bi_ops_CS_matrix c on b.ext=c.phone
set a.agent_name=c.name,
a.agent_ext=b.ext
where c.status='ACTIVO';

update bi_ops_cc_facebook
set 
event_minute=minute(event_datetime),
event_hour=hour(event_datetime);


update bi_ops_cc_facebook
set event_shift=if(event_minute<30,concat(event_hour,':','00'),concat(event_hour,':','30'));


SELECT  'Actualizar holidays',now();
update bi_ops_cc_facebook a inner join calendar b on a.event_date=b.dt
set holiday=b.isholiday;

SELECT  'Actualizar horas laborales',now();
update bi_ops_cc_facebook
set workinghour=if(event_weekday=6 or holiday=1,
	if(event_hour>=10 and event_hour<18,
		1,0),
		if(event_weekday=5,
			if(event_hour>=9 and event_hour<21,
			1,0),
			if(event_hour>=8 and event_hour<21,
				1,0)))
;


update bi_ops_cc_facebook
set net_event=1
where event in ('COMPLETECALLER','COMPLETEAGENT','TRANSFER','ABANDON','EXITWITHTIMEOUT') and workinghour=1;

insert into bi_ops_cc (queue_stats_id,event_date,queue,agent,agent_name,event,call_duration_seg,hold_duration_seg,
agent_hold_time_seg,start_position,end_position,event_hour, event_minute,event_shift,event_week,
event_month,event_year,holiday,workinghour,net_event, unanswered_in_wh,
unanswered_in_wh_cust,answered_in_wh,answered_in_wh_20,no_answer, agent_q,available_time,uniqueid,
number, record, pre,pos,pre_answ,pos_answ,facebook )
select queue_stats_id,event_date,queue,agent,agent_name,event,call_duration_seg,hold_duration_seg,
agent_hold_time_seg,start_position,end_position,event_hour, event_minute,event_shift,event_week,
event_month,event_year,holiday,workinghour,net_event, unanswered_in_wh,
unanswered_in_wh_cust,answered_in_wh,answered_in_wh_20,no_answer, agent_q,available_time,uniqueid,
number, record, pre,pos,pre_answ,pos_answ,1 from bi_ops_cc_facebook as a;


SELECT  'Fin rutina CC Inbound Facebook',now();



END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `bi_ops_cc_occupancy`()
BEGIN

#Zona horaria de Colombia

set time_zone='-5:00';

truncate table bi_ops_cc_occupancy;


SELECT  'Insertar datos pausas y despausas en una tabla',now();
insert into bi_ops_cc_occupancy (queue_stats_id, event_datetime, agent,event) 
select 
a.queue_stats_id,
a.datetime,d.agent,event
FROM new_queue_stats a inner join qevent b on a.qevent=b.event_id
inner join qname c on a.qname=c.queue_id inner join qagent d on a.qagent=d.agent_id 
where year(datetime)=2013 and queue in ('ivr_cola1','ivr_cola2','ivr_cola3') and event IN ('PAUSE','PAUSEALL','PAUSECUSTOM','UNPAUSE','UNPAUSEALL') 
and date(a.datetime)>='2013-07-01'
group by datetime,agent,event
ORDER BY qagent asc,datetime asc;

truncate table bi_ops_cc_occupancy_times;

SELECT  'Tiempos de pausa y despausa',now();
insert into bi_ops_cc_occupancy_times(fk_id_occupancy, pause_datetime, unpause_datetime, time_pause, agent, concept)
SELECT f1.id,f1.event_datetime, f2.event_datetime, SEC_TO_TIME( UNIX_TIMESTAMP( f2.event_datetime ) - UNIX_TIMESTAMP( f1.event_datetime ) ) AS dif,
f1.agent,concat(f1.event,"-",f2.event)
FROM bi_ops_cc_occupancy f1
inner JOIN bi_ops_cc_occupancy f2 ON f1.id < f2.id
and f1.event_datetime<f2.event_datetime and date(f1.event_datetime)=date(f2.event_datetime)
and f1.agent=f2.agent
GROUP BY f1.event_datetime,f1.agent
ORDER BY f1.id;

truncate table bi_ops_cc_pause_types;

SELECT  'Tipos de pausa',now();
insert into bi_ops_cc_pause_types
select 
a.queue_stats_id,
a.datetime,d.agent,info1
FROM new_queue_stats a inner join qevent b on a.qevent=b.event_id
inner join qname c on a.qname=c.queue_id inner join qagent d on a.qagent=d.agent_id 
where year(datetime)=2013 and queue in ('NONE') and event IN ('PAUSE') 
and 
date(a.datetime)>='2013-07-01'
group by datetime,agent,event
ORDER BY qagent asc,datetime asc;

SELECT  'Actualizar los tipos de pausa',now();
update
bi_ops_cc_occupancy_times a left join bi_ops_cc_pause_types b 
on a.pause_datetime = b.datetime and a.agent=b.agent 
set a.pause_type=b.info1
where a.concept='PAUSE-UNPAUSE';

SELECT  'Actualizar nombres agentes',now();
update
bi_ops_cc_occupancy_times a
inner join bi_ops_agent_names b on a.agent=b.agent inner join bi_ops_CS_matrix c on b.ext=c.phone
set a.agent_name=c.name,
a.agent_ext=b.ext
where c.status='ACTIVO' and c.process='INBOUND';

update
bi_ops_cc_occupancy_times a
inner join bi_ops_agent_names b on a.agent=b.agent inner join bi_ops_CS_matrix c on b.ext=c.phone
set a.agent_name=c.name,
a.agent_ext=b.ext
where a.agent_name is null;

update bi_ops_cc_occupancy_times
set time_sec=hour(time_pause)*3600+minute(time_pause)*60+second(time_pause);

SELECT  'Datos adicionales',now();
update bi_ops_cc_occupancy_times 
set
date=date(pause_datetime),
hour=hour(pause_datetime), 
minute=minute(pause_datetime),
weekday=weekday(pause_datetime),
weekofyear=weekofyear(pause_datetime),
monthname=monthname(pause_datetime),
year=year(pause_datetime);

update bi_ops_cc_occupancy_times 
set shift=if(minute<30,concat(hour,':','00'),concat(hour,':','30'));
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `bi_ops_cc_occupancy_2`()
BEGIN

SELECT  'Inicio rutina rutina bi_ops_cc_occupancy_2',now();

#Zona horaria de Colombia

set time_zone='-5:00';

truncate table bi_ops_cc_occupancy_2;


insert into bi_ops_cc_occupancy_2 (date,agent,total_call_duration)
select * from
(select event_date,agent,sum(call_duration_seg) as 'total_call_duration' from bi_ops_cc
group by event_date,agent) as a;


update bi_ops_cc_occupancy_2 a
inner join 
(select date,agent,sum(dif_time_sec) as 'total_available_time' from bi_ops_cc_agent_times where effective_time=1 and dif_time_sec is not null
group by date,agent) as b
on a.date=b.date
and a.agent=b.agent
set a.total_available_time=b.total_available_time;


update bi_ops_cc_occupancy_2 a
inner join 
(select date,agent,sum(dif_time_sec) as 'total_pause_time' from bi_ops_cc_agent_times where effective_time=0 and dif_time_sec is not null
group by date,agent) as c
on a.date=c.date
and a.agent=c.agent
set a.total_pause_time=c.total_pause_time;


delete from bi_ops_cc_occupancy_2 
where agent='NONE';


update bi_ops_cc_occupancy_2 a inner join 
calendar b on a.date=b.dt
set 
real_available_time=if(isholiday=1,18000,if(isweekend=1,18000,32400));

update bi_ops_cc_occupancy_2 
set
real_available_time=if(total_available_time>total_call_duration,total_available_time,
if(real_available_time-total_pause_time>total_call_duration,
real_available_time-total_pause_time,if(real_available_time=1800,16200,27000)));

SELECT  'Fin rutina bi_ops_cc_occupancy_2',now();


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `bi_ops_cc_outbound`()
BEGIN

SELECT  'Inicio rutina CC Outbound',now();

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'customer_service_co.bi_ops_cc_outbound',
  'start',
  NOW(),
  MAX(event_datetime),
  count(*),
  count(*)
FROM
  customer_service_co.bi_ops_cc_outbound;


#Zona horaria de Colombia

set time_zone='-5:00';

update cdr
set 
calldate=answer
 where calldate='0000-00-00 00:00:00' and uniqueid<>"";

truncate table queue_stats_outbound;

insert into queue_stats_outbound (uniqueid,call_date,ext,total_duration,call_duration,disposition,dialout,number,record)
SELECT uniqueid,calldate,src,duration,billsec,disposition,dialout,dst,record FROM cdr where (src like '20%' or src like '21%' or src like '18%' or src like '19%') and length(src)=4
and src <> '2000';


truncate table bi_ops_cc_outbound;


SELECT  'Insertar datos bi_ops_cc_outbound',now();
insert into bi_ops_cc_outbound (uniqueid, event_datetime, event_date, agent_ext, event, total_duration_seg, call_duration_seg, event_hour, event_minute, event_weekday, event_week, event_month, event_year,number) 
SELECT uniqueid, call_date,date(call_date),ext,disposition, total_duration,call_duration,hour(call_date), minute(call_date),
weekday(call_date),weekofyear(call_date),monthname(call_date),year(call_date),number FROM queue_stats_outbound;

update bi_ops_cc_outbound b
inner join cdr a
on b.uniqueid=a.uniqueid
set calldate=end
where event_date='0000-00-00';


SELECT  'Insertar nombres',now();

update bi_ops_cc_outbound a inner join bi_ops_agent_names b on a.agent_ext=b.ext inner join 
bi_ops_CS_matrix_antext c on b.ext=c.phone
set 
a.agent_name=c.name,
a.process=c.process,
a.agent=b.agent,
a.identification=c.identification
where event_date<='2013-12-09';

update bi_ops_cc_outbound a inner join bi_ops_agent_names b on a.agent_ext=b.ext inner join 
bi_ops_CS_matrix c on b.ext=c.phone
set 
a.agent_name=c.name,
a.process=c.process,
a.agent=b.agent,
a.identification=c.identification
where event_date>'2013-12-09';

update bi_ops_cc_outbound a inner join 
bi_ops_agent_names b on a.agent_ext=b.ext 
inner join bi_ops_CS_matrix_antext c on b.ext=c.phone
set 
a.agent_name=c.name,
a.process=c.process,
a.agent=b.agent,
a.identification=c.identification
where status='ACTIVO'
and event_date<='2013-12-09';

update bi_ops_cc_outbound a inner join 
bi_ops_agent_names b on a.agent_ext=b.ext 
inner join bi_ops_CS_matrix c on b.ext=c.phone
set 
a.agent_name=c.name,
a.process=c.process,
a.agent=b.agent,
a.identification=c.identification
where status='ACTIVO'
and event_date>'2013-12-09';

update bi_ops_cc_outbound
set event_shift=if(event_minute<30,concat(event_hour,':','00'),concat(event_hour,':','30')),
hold_duration_seg=total_duration_seg-call_duration_seg;


SELECT  'Actualizar binarios de tiempos',now();
update bi_ops_cc_outbound
set 
answered=if(event ='ANSWERED',1,0),
busy=if(event ='BUSY',1,0),
unanswered=if(event ='NO ANSWER',1,0),
calls=if(event ='FAILED',0,1);

UPDATE bi_ops_cc_outbound 
SET outbound=1 where 
process='OUTBOUND';

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'customer_service_co.bi_ops_cc_outbound',
  'finish',
  NOW(),
  MAX(event_datetime),
  count(*),
  count(*)
FROM
  customer_service_co.bi_ops_cc_outbound;



SELECT  'Fin rutina CC Outbound',now();


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `bi_ops_cc_outbound_dyalogo_test`()
BEGIN

SELECT  'Inicio rutina CC Outbound',now();

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'customer_service_co.bi_ops_cc_outbound',
  'start',
  NOW(),
  MAX(event_datetime),
  count(*),
  count(*)
FROM
  customer_service_co.bi_ops_cc_outbound;


#Zona horaria de Colombia

set time_zone='-5:00';



truncate table bi_ops_cc_outbound_dyalogo;


SELECT  'Insertar datos bi_ops_cc_outbound',now();
insert into bi_ops_cc_outbound_dyalogo 
(uniqueid, event_datetime, event_date, agent_name, agent_ext, identification, event, total_duration_seg, call_duration_seg,
hold_duration_seg, event_hour, event_minute, event_weekday, event_week, event_month, event_year, campana,type, number, record)
SELECT 
unique_id, fecha_hora, date(fecha_hora), agente, extension, identificacion, resultado, duracion_total, duracion_al_aire, 
tiempo_espera, hour(fecha_hora), minute(fecha_hora), weekday(fecha_hora),weekofyear(fecha_hora),monthname(fecha_hora),year(fecha_hora), campana,tipo_llamada,numero_telefonico, grabacion
FROM CS_dyalogo.dy_llamadas_espejo a left join
CS_dyalogo.dy_agentes b on a.id_agente_espejo=b.id
where sentido='Saliente'
and date(fecha_hora)>='2014-04-01';

SELECT  'Agregar proceso',now();

update bi_ops_cc_outbound_dyalogo a inner join 
bi_ops_matrix_ventas b on a.identification=b.identification 
set process_ventas=process;

update bi_ops_cc_outbound_dyalogo a inner join 
bi_ops_matrix_sac b on a.identification=b.identification 
set process_sac=process;


update bi_ops_cc_outbound_dyalogo
set event_shift=if(event_minute<30,concat(event_hour,':','00'),concat(event_hour,':','30'));

SELECT  'Actualizar binarios de tiempos',now();
update bi_ops_cc_outbound
set 
answered=if(event ='Contestada',1,0),
busy=if(event ='Ocupado',1,0),
unanswered=if(event ='No Contestada',1,0),
calls=if(event in ('Fallida','Ocupado'),0,1);

UPDATE bi_ops_cc_outbound_dyalogo
SET outbound=1 where 
process='OUTBOUND';

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'customer_service_co.bi_ops_cc_outbound',
  'finish',
  NOW(),
  MAX(event_datetime),
  count(*),
  count(*)
FROM
  customer_service_co.bi_ops_cc_outbound;



SELECT  'Fin rutina CC Outbound',now();


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `bi_ops_cc_queue2`()
BEGIN

SELECT  'Inicio rutina ivr_cola2',now();

#Zona horaria de Colombia

set time_zone='-5:00';

truncate table bi_ops_cc_queue2;

SELECT  'Insertar datos de asterisk.cdr',now();

insert into bi_ops_cc_queue2 (uniqueid,call_datetime,dst,dst_type,call_date,call_hour,call_minute,call_weekday,call_week,
call_month,call_year,agent,duration_seg,userfield)
select a.uniqueid,a.calldate,a.dst,b.dst_type,date(a.calldate),hour(a.calldate),minute(a.calldate),weekday(a.calldate),
weekofyear(a.calldate), monthname(a.calldate),year(a.calldate),
left(a.dstchannel,instr(a.dstchannel,'-')-1) ,a.duration,a.userfield from cdr  a inner join bi_ops_cc_dst_types b on a.dst=b.dst
where a.userfield like '%callerid%' and date(a.calldate)>='2013-07-27'and 
a.dst in ('2','ivr_cola2','s') and a.userfield not like '%pregunta%';

SELECT  'Datos de turnos',now();
update bi_ops_cc_queue2 
set 
call_shift=if(call_minute<30,concat(call_hour,':','00'),concat(call_hour,':','30')),
call_yrmonth=concat(call_year,month(call_date));

SELECT  'Datos de eventos',now();
update bi_ops_cc_queue2
set event=if(userfield LIKE '%status%','DATA_RETURNED',IF(userfield LIKE '%UNDEFINED%','ORDER_UNDEFINIED',
IF(userfield='-callerid_14872222','NOT_INFORMATION_CUSTOMER','NO_DATA_RETURNED'))),
is_transfered=if(agent like '%SIP%',1,0);

SELECT  'Datos de llamadas de ivr_cola1 e ivr_cola3',now();
update bi_ops_cc_queue2 a  inner join (select event_date,sum(net_event) as calls from bi_ops_cc where queue <>'ivr_cola2'
and event_date>='2013-07-27'
group by event_date) as b
on a.call_date=b.event_date
set a.calls_1_3=b.calls;

SELECT  'Datos de llamadas de abandonos ivr_cola2',now();
update bi_ops_cc_queue2 a  inner join (select event_date,sum(net_event) as calls from bi_ops_cc where queue ='ivr_cola2' 
and event='ABANDON'
and event_date>='2013-07-27'
group by event_date) as b
on a.call_date=b.event_date
set a.abandons2=b.calls;

update bi_ops_cc_queue2 a inner join (select call_date,sum(item_counter) as total from bi_ops_cc_queue2
group by call_date ) b on a.call_date=b.call_date
set
a.factor_calls_1_3=a.calls_1_3/b.total,
a.factor_abandons2=a.abandons2/b.total;

SELECT  'Fin rutina ivr_cola2',now();

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `bi_ops_cc_survey`()
BEGIN

SELECT  'Inicio rutina CC Inbound',now();

#Horario Colombia

set time_zone='-5:00';

#Datos ultimo día
set @max_day=cast((select max(call_date) from bi_ops_cc_survey) as date);

delete from bi_ops_cc_survey
where call_date>=@max_day;

#truncate table bi_ops_cc_survey;

SELECT  'Insertar datos a la tabla inicial',now();
insert into bi_ops_cc_survey(asteriskid,call_datetime,call_date,call_time) 
SELECT asteriskID, fecha, date(fecha), time(fecha) FROM evento where mensaje like '%callerid%'
and fecha>=@max_day;

SELECT  'Extraer uniqueid',now();

UPDATE bi_ops_cc_survey
set uniqueid=asteriskid
where uniqueid is null;

UPDATE bi_ops_cc_survey
set uniqueid= left(uniqueid,instr(uniqueid,'--')-1)  
where uniqueid like '%--%';

UPDATE bi_ops_cc_survey
set uniqueid= right(uniqueid,length(uniqueid)-instr(uniqueid,'-')) ;

update bi_ops_cc_survey
set 
uniqueid=left(uniqueid,instr(uniqueid,'-')-1)
where uniqueid like '%-%';


SELECT  'Agregar información de los asesores', now();
update bi_ops_cc_survey a inner join bi_ops_cc b on a.uniqueid=b.uniqueid
set a.agent=b.agent,
a.agent_identification=b.agent_identification,
a.agent_name=b.agent_name,
a.agent_ext=b.agent_ext,
a.queue=b.queue;

#Actualizar datos con cdr
update bi_ops_cc_survey a
inner join cdr b on a.uniqueid=b.uniqueid
set agent=dstchannel
where agent is null;

#extraer codigo de agente
UPDATE bi_ops_cc_survey
set agent= left(agent,instr(agent,'-')-1)
where agent_ext is null;


#agregar extension
update bi_ops_cc_survey a 
inner join bi_ops_agent_names b
on a.agent=b.agent
set agent_ext=ext
where agent_ext is null;

update bi_ops_cc_survey
set
agent_ext=if(agent="","",if(length(right(agent,length(agent)-9))=1,concat(200,right(agent,length(agent)-9)),
if(length(right(agent,length(agent)-9))=2,concat(20,right(agent,length(agent)-9)),
if(length(right(agent,length(agent)-9))=3,concat(2,right(agent,length(agent)-9)),""))))
where agent_ext is null;

#Agregar datos faltantes
update bi_ops_cc_survey  a
inner join cdr b on a.uniqueid=b.uniqueid
set
agent_ext=src
where date_format(call_date,"%Y%m")=201401
and agent_name is null
and dcontext<>'agentes'
and agent_ext is null
and length(src)=4;

SELECT  'Insertar nombres bi_ops_cc_survey',now();
update bi_ops_cc_survey a inner join 
bi_ops_CS_matrix c on a.agent_ext=c.phone
set a.agent_name=c.name,
a.agent_identification=c.identification
where c.status='ACTIVO'
and process in ('SAC','VENTAS','INBOUND')
and call_date>'2013-12-09'
and agent_ext<>0
and agent_name is null;

update bi_ops_cc_survey a inner join 
bi_ops_CS_matrix c on a.agent_ext=c.phone
set a.agent_name=c.name,
a.agent_identification=c.identification
where a.agent_name is null
and call_date>'2013-12-09'
and agent_ext<>0
and agent_name is null;

update bi_ops_cc_survey a inner join 
bi_ops_CS_matrix_antext c on a.agent_ext=c.phone
set a.agent_name=c.name,
a.agent_identification=c.identification
where c.status='ACTIVO'
and process in ('SAC','VENTAS','INBOUND')
and a.agent_name is null
and call_date<='2013-12-09'
and agent_ext<>0;

update bi_ops_cc_survey a inner join 
bi_ops_CS_matrix_antext  c on a.agent_ext=c.phone
set a.agent_name=c.name,
a.agent_identification=c.identification
where a.agent_name is null
and call_date<='2013-12-09'
and agent_ext<>0;

#Actualizar otras extensiones
update bi_ops_cc a inner join bi_ops_cc_survey b
on a.uniqueid=b.uniqueid
set
a.transfered_survey=1,
a.answered_2=answer_2,
a.good=answer2_option_1,
a.regular=answer2_option_2;

SELECT  'Tiempos de finalización de la llamada',now();

truncate table bi_ops_cc_survey_last;

insert into bi_ops_cc_survey_last(time,asteriskid,respuestas) SELECT 
time(fecha) as 'time',asteriskID,right(mensaje,length(mensaje)-instr(mensaje,'_')) as 'respuestas'
FROM evento where 
mensaje  like '%respuestas%';

update bi_ops_cc_survey a inner join bi_ops_cc_survey_last b on a.asteriskid=b.asteriskID
set a.time_end=b.time,
a.respuestas=b.respuestas;

truncate table bi_ops_cc_survey_last;

insert into bi_ops_cc_survey_last(fecha,asteriskid) SELECT max(fecha) as 'fecha',asteriskID FROM evento group by asteriskID;

update bi_ops_cc_survey a inner join bi_ops_cc_survey_last b on a.asteriskid=b.asteriskid 
set time_end=time(b.fecha)
where time_end is null;

#Determinar duracion de la llamada
SELECT  'Duracion de la llamada',now();
update bi_ops_cc_survey 
set call_duration=hour(subtime(time_end,call_time))*3600+minute(subtime(time_end,call_time))*60+second(subtime(time_end,call_time));

#actualizar respuestas
update bi_ops_cc_survey
set 
respuestas=0
where respuestas is null ;

update bi_ops_cc_survey
set 
abandon_in=1
where respuestas=0 ;


#Actualizar info de los agentes
SELECT  'Coordinadores',now();
UPDATE bi_ops_cc_survey a inner join bi_ops_CS_matrix b on a.agent_ext=b.phone
set
a.coordinator=b.coordinator;

SELECT  'Actualizar respuestas',now();
UPDATE bi_ops_cc_survey a inner join respuesta b on a.asteriskid=b.recordID
set
answer_1=1
 where b.id_pregunta=1;

UPDATE bi_ops_cc_survey a inner join respuesta b on a.asteriskid=b.recordID
set
answer1_option_1=1
 where b.id_pregunta=1 and b.respuesta=1;

UPDATE bi_ops_cc_survey a inner join respuesta b on a.asteriskid=b.recordID
set
answer1_option_2=1
 where b.id_pregunta=1 and b.respuesta=2;

UPDATE bi_ops_cc_survey a inner join respuesta b on a.asteriskid=b.recordID
set
answer_2=1
 where b.id_pregunta=2;

UPDATE bi_ops_cc_survey a inner join respuesta b on a.asteriskid=b.recordID
set
answer2_option_1=1
 where b.id_pregunta=2 and b.respuesta=1;

UPDATE bi_ops_cc_survey a inner join respuesta b on a.asteriskid=b.recordID
set
answer2_option_2=1
 where b.id_pregunta=2 and b.respuesta=2;

UPDATE bi_ops_cc_survey a inner join respuesta b on a.asteriskid=b.recordID
set
answer2_option_3=1
 where b.id_pregunta=2 and b.respuesta=3;

UPDATE bi_ops_cc_survey a inner join respuesta b on a.asteriskid=b.recordID
set
answer_3=1
 where b.id_pregunta=3;

UPDATE bi_ops_cc_survey a inner join respuesta b on a.asteriskid=b.recordID
set
answer3_option_1=1
 where b.id_pregunta=3 and b.respuesta=1;

UPDATE bi_ops_cc_survey a inner join respuesta b on a.asteriskid=b.recordID
set
answer3_option_2=1
 where b.id_pregunta=3 and b.respuesta=2;


UPDATE bi_ops_cc_survey a inner join respuesta b on a.asteriskid=b.recordID
set
answer3_option_3=1
 where b.id_pregunta=3 and b.respuesta=3;

SELECT  'Actualizar abandonos',now();
UPDATE bi_ops_cc_survey a inner join respuesta b on a.asteriskid=b.recordID
set
answer1_abandon=if((answer1_option_1+answer1_option_2)=1,0,1),
answer2_abandon=if((answer2_option_1+answer2_option_2+answer2_option_3)=1,0,1),
answer3_abandon=if((answer3_option_1+answer3_option_2+answer3_option_3)=1,0,1);

truncate table bi_ops_cc_survey_caller_id ;

#Caller_id
insert into bi_ops_cc_survey_caller_id (asteriskid,callerid) select asteriskID,right(mensaje,length(mensaje)-9) as 'caller_id'  FROM evento where mensaje like '%callerid_%' ;

update
 bi_ops_cc_survey a inner join bi_ops_cc_survey_caller_id b 
on a.asteriskid=b.asteriskID 
set a.callerid=b.callerid;

update bi_ops_cc a inner join bi_ops_cc_survey b
on a.uniqueid=b.uniqueid
set
a.transfered_survey=1,
a.answered_3=answer_2,
a.good=answer2_option_1,
a.regular=answer2_option_2
where a.event_date<'2014-03-19';

#Cambio de la encuesta de satisfacción/First contact resolution
update bi_ops_cc a inner join bi_ops_cc_survey b
on a.uniqueid=b.uniqueid
set
a.transfered_survey=1,
a.answered_3=answer_3,
a.good=answer3_option_1,
a.regular=answer3_option_2,
a.answ_1_2=if(answer_1=1 and answer_2=1,1,0),
a.first_contact=answer2_option_1,
a.resolution=answer1_option_1
where a.event_date>='2014-03-19';


#Datos de retirados faltantes
UPDATE bi_ops_cc_survey a inner join bi_ops_CS_matrix b
on a.agent_ext=b.phone
set
agent_name=name,
agent_identification=identification
where agent_identification is null
and agent_ext<>0;

SELECT  'Rutina finalizada',now();


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `bi_ops_cc_survey_dyalogo_test`()
BEGIN

SELECT  'Inicio rutina CC Survey',now();

#Horario Colombia

set time_zone='-5:00';

#Datos ultimo día
set @max_day=cast((select max(call_date) from bi_ops_cc_survey_dyalogo_test) as date);

delete from bi_ops_cc_survey_dyalogo_test
where call_date>=@max_day;

#truncate table bi_ops_cc_survey;

SELECT  'Insertar datos a la tabla inicial',now();
insert into bi_ops_cc_survey_dyalogo_test(uniqueid,call_datetime,call_date,call_time,agent_name,agent_ext) 
select unique_id,fecha_hora,date(fecha_hora), time(fecha_hora),
replace(left(right(nombre_agente,length(nombre_agente)-instr(nombre_agente,'|')),instr(right(nombre_agente,length(nombre_agente)-instr(nombre_agente,'|')),'|')-1),'_',' '),
right(nombre_agente,if(instr(right(nombre_agente,length(nombre_agente)-instr(nombre_agente,'|')),'|')=0,null,length(right(nombre_agente,length(nombre_agente)-instr(nombre_agente,'|'))) -instr(right(nombre_agente,length(nombre_agente)-instr(nombre_agente,'|')),'|')))
 from CS_dyalogo.dy_encuestas_resultados
where campana <>""
and date(fecha_hora)>=@max_day
group by unique_id;

SELECT  'Agregar información de los asesores', now();
update bi_ops_cc_survey_dyalogo_test a
inner join bi_ops_cc_dyalogo b 
on a.uniqueid=b.uniqueid
set
a.agent_identification=b.agent_identification,
a.callerid=b.number,
a.queue=b.queue
where transfered_to_queue<>1;

SELECT  'Pregunta 1', now();
update bi_ops_cc_survey_dyalogo_test a 
inner join CS_dyalogo.dy_encuestas_resultados b
on a.uniqueid=b.unique_id
set
answer_1=1,
answer1_option_1=if(respuesta=1,1,0),
answer1_option_2=if(respuesta=2,1,0)
where id_pregunta=1;

SELECT  'Pregunta 2', now();
update bi_ops_cc_survey_dyalogo_test a 
inner join CS_dyalogo.dy_encuestas_resultados b
on a.uniqueid=b.unique_id
set
answer_2=1,
answer2_option_1=if(respuesta=1,1,0),
answer2_option_2=if(respuesta=2,1,0)
where id_pregunta=2;

SELECT  'Pregunta 3', now();
update bi_ops_cc_survey_dyalogo_test a 
inner join CS_dyalogo.dy_encuestas_resultados b
on a.uniqueid=b.unique_id
set
answer_3=1,
answer3_option_1=if(respuesta=1,1,0),
answer3_option_2=if(respuesta=2,1,0),
answer3_option_3=if(respuesta=3,1,0)
where id_pregunta=3;

SELECT  'Actualizar respuestas', now();
update bi_ops_cc_survey_dyalogo_test
set
respuestas=answer_1+answer_2+answer_3;

#actualizar respuestas
update bi_ops_cc_survey
set 
respuestas=0
where respuestas is null ;

update bi_ops_cc_survey
set 
abandon_in=1
where respuestas=0 ;

SELECT  'Abandonados', now();
update bi_ops_cc_survey_dyalogo_test
set 
abandon_in=if(respuestas=0,1,0),
answer1_abandon=if(answer_1=0,1,0),
answer2_abandon=if(answer_2=0,1,0),
answer3_abandon=if(answer_3=0,1,0);

SELECT  'Ingresar datos en la tabla principal', now();

#Cambio de la encuesta de satisfacción/First contact resolution
update bi_ops_cc_dyalogo a inner join bi_ops_cc_survey_dyalogo_test b
on a.uniqueid=b.uniqueid
set
a.transfered_survey=1,
a.answered_3=answer_3,
a.good=answer3_option_1,
a.regular=answer3_option_2,
a.answ_1_2=if(answer_1=1 and answer_2=1,1,0),
a.first_contact=answer2_option_1,
a.resolution=answer1_option_1
where a.transfered_survey is null;


SELECT  'Rutina finalizada',now();


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `bi_ops_customer_detail`()
BEGIN

SELECT  'Inicio rutina customer_detail',now();

SELECT  'Insertar datos de tbl_order_detail a customer_detail',now();

#Seleccionar máxima fecha
set @max_day=cast((select max(fecha_orden) from tbl_bi_ops_customer_detail) as date);

#Borrar datos de esa fecha
delete from tbl_bi_ops_customer_detail 
where fecha_orden=@max_day;

insert into tbl_bi_ops_customer_detail (id_cliente, fecha_orden, año, mes, dia, ciudad, cat1, cat2, cat3, nombre_producto,
cupon, codigo_cupon, numero_orden, item_id, stockout, precio_unitario, valor_cupon, precio_pagado)
select a.CustId, a.date_ordered fecha_orden, year(a.date_ordered) año, month(a.date_ordered) mes, day(a.date_ordered) dia,
a.ciudad,
a.new_cat1 cat1, a.new_cat2 cat2, a.new_cat3 cat3, a.product_name nombre_producto, if (a.coupon_code is not null,1,0) cupon, 
a.coupon_code codigo_cupon, a.order_nr numero_orden, a.Item,
a.stockout, a.unit_price precio_unitario, a.coupon_money_value valor_cupon, a.paid_price precio_pagado
 from bazayaco.tbl_order_detail a
where a.date_ordered>=@max_day;

#Actualizar campos oac, returned, rejected
update customer_service_co.tbl_bi_ops_customer_detail a 
inner join bazayaco.tbl_order_detail b
on a.item_id=b.Item
set
a.oac=b.oac,
a.returned=b.returned,
a.rejected=b.rejected;

SELECT  'Agregar datos del cliente',now();

update tbl_bi_ops_customer_detail a
inner join bob_live_co.customer b on a.id_cliente=b.id_customer
set 
a.nombre=b.first_name, 
a.apellido=b.last_name, 
a.cc=b.national_registration_number, 
a.correo=b.email,
a.fecha_nacimiento=b.birthday
where fecha_orden>=@max_day;


SELECT  'Agregar direccion del cliente',now();

update tbl_bi_ops_customer_detail a
inner join wmsprod_co.pedidos_venda c on a.numero_orden=c.numero_pedido
set a.direccion=c.rua_cliente
where fecha_orden>=@max_day;

SELECT  'Agregar telefonos del cliente',now();

update tbl_bi_ops_customer_detail a
inner join (select 
            t.fk_customer,
            t.phone,
            t.mobile_phone,
            t.created_at
    from
        bob_live_co.customer_address t
    join (select 
        fk_customer, max(created_at) max
    from
        bob_live_co.customer_address
    group by fk_customer) t2 ON t.created_at = t2.max
        and t.fk_customer = t2.fk_customer) d on a.id_cliente=d.fk_customer
set 
a.telefono=d.phone,
a.celular=d.mobile_phone
where fecha_orden>=@max_day;

SELECT  'Stockout',now();

update tbl_bi_ops_customer_detail a
inner join bazayaco.tbl_order_detail b on a.item_id=b.Item
set 
a.stockout=b.stockout;

update customer_service_co.tbl_bi_ops_customer_detail a
inner join wmsprod_co.itens_venda b
on a.item_id=b.item_id
set stockout=1
where status in ('backorder_tratada', 'quebra tratada',
'quebrado');

SELECT  'Datos de nps',now();

update tbl_bi_ops_customer_detail a
inner join customer_service.questionProCO b
on a.numero_orden=b.ordernumber
set 
a.nps=1,
a.status_nps=b.`Response Status`,
a.calificacion_nps=b.NPS;

update tbl_bi_ops_customer_detail
set
cc=bazayaco.udf_cleanCreditNum(cc)
where fecha_orden>=@max_day;

update
tbl_bi_ops_customer_detail
set nps=0
where nps is null;

#Agrupar la tabla por cliente para sacar datos generales

SELECT  'Agrupacion cliente',now();

truncate table tbl_bi_ops_customer_detail_by_cc;

insert into tbl_bi_ops_customer_detail_by_cc (cc, compras, stockout, cant_stockout, nps)
SELECT cc, count(distinct(numero_orden)) compras, if(sum(stockout)>=1,1,0) stockout, 
sum(stockout) cant_stockout, if(sum(nps)>=1,1,0) nps
 FROM customer_service_co.tbl_bi_ops_customer_detail
group by cc;

#Sacar datos de promedio de calificación nps

update tbl_bi_ops_customer_detail_by_cc a  inner join
(select cc, avg(calificacion_nps) avg from
(select cc,numero_orden, calificacion_nps from tbl_bi_ops_customer_detail
where nps=1
group by numero_orden) t
group by cc) b
on a.cc=b.cc
set
promedio_nps=avg;


#Agregar datos por cliente a la tabla general

SELECT  'Datos por cliente',now();

update tbl_bi_ops_customer_detail a
inner join tbl_bi_ops_customer_detail_by_cc b
on a.cc=b.cc
set a.compras=b.compras,
a.stockout_cliente=b.stockout,
a.cant_stockout=b.cant_stockout,
a.nps_cliente=b.nps,
a.promedio_calificacion_cliente=b.promedio_nps;

#Arreglar datos nullos de nps
update tbl_bi_ops_customer_detail
set promedio_calificacion_cliente=0
where 
promedio_calificacion_cliente is null;


#Agregar datos por orden a la tabla general

SELECT  'Datos por orden',now();

update tbl_bi_ops_customer_detail a inner join
(select numero_orden, sum(precio_pagado) precio from tbl_bi_ops_customer_detail 
group by numero_orden) b
on a.numero_orden=b.numero_orden
set a.valor_orden=b.precio
where fecha_orden>=@max_day;

#Agregar datos maximos agrupado por cliente

SELECT  'Datos maximos',now();

update tbl_bi_ops_customer_detail a
inner join
(select * from
(select cc, ciudad, telefono,direccion from tbl_bi_ops_customer_detail
group by cc, ciudad, telefono,direccion
order by fecha_orden desc) t
group by cc) b
on a.cc=b.cc
set
a.max_ciudad_cliente=b.ciudad,
a.max_telefono_cliente=b.telefono,
a.max_direccion_cliente=b.direccion;


SELECT  'Fin rutina customer_detail',now();


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `bi_ops_general_cc`()
BEGIN

SELECT  'Inicio rutina principal',now();

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at)
SELECT 
  'Colombia', 
  'customer_service_co.bi_ops_general_cc',
  'start',
  NOW();

call bi_ops_cc;

call bi_ops_cc_outbound;

call bi_ops_cc_survey;

#call bi_ops_tickets;

#call bi_ops_cc_chats_performance;

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at)
SELECT 
  'Colombia', 
  'customer_service_co.bi_ops_general_cc',
  'finish',
  NOW();

SELECT  'Fin rutina principal',now();


END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `bi_ops_refunds`()
BEGIN

SELECT  'Inicio rutina CC Refunds',now();

#Zona horaria de Colombia

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'customer_service_co.tbl_bi_ops_refunds',
  'start',
  NOW(),
  MAX(date_request),
  count(*),
  count(*)
FROM
  customer_service_co.tbl_bi_ops_refunds;


set time_zone='-5:00';

SELECT  'Insertar datos de solicitudes de reembolsos de bob',now();
-- a
insert into tbl_bi_ops_refunds (Id_Solicitud, fecha_orden, order_nr, Item, status_item, paid_price,shipping_fee,
Valor, sku, Product_name, payment_method, ciudad, is_marketplace)
select id,date_ordered,order_nr, Item, status_item, paid_price,shipping_fee, 
Valor, sku,
Product_name,payment_method,ciudad, is_marketplace from 
(select  
concat(a.order_nr,"-",Item) id,a.date_ordered,a.order_nr, a.Item, a.status_item, a.paid_price,a.shipping_fee, 
a.paid_price+a.shipping_fee as 'Valor', a.sku,
a.Product_name,a.payment_method,a.ciudad, a.is_marketplace, b.Id_Solicitud
from bazayaco.tbl_order_detail a 
left join (select Id_Solicitud from tbl_bi_ops_refunds) b on concat(a.order_nr,"-",Item)=b.Id_Solicitud
where status_item like '%refund_needed%' or status_item like '%store_credit_needed%'
and date_ordered>='2013-08-01') t
where Id_Solicitud is null;

SELECT  'Insertar datos de solicitudes del aplicativo',now();

insert into tbl_bi_ops_refunds(Id_Solicitud, estado, uid, order_nr, Item, correo_cliente, razon_reembolso, motivo_devolucion, 
metodo_reembolso, user_customer_service, date_request, Fecha_estado, guia_devolucion)
select ID_Solicitud, Estado, uid, NumeroOrden, item_id, CorreoCliente, RazonReembolso, 
MotivoDevolucion, MetodoReembolso, name, FechaHora,   
Fecha_estado, guideNumber from
(select a.ID_Solicitud, e.Estado, a.uid, a.NumeroOrden, a.item_id, a.CorreoCliente, a.RazonReembolso, 
a.MotivoDevolucion, a.MetodoReembolso, users.name, a.FechaHora,
e.Fecha as 'Fecha_estado', a.guideNumber, d.Id_Solicitud id from solicitud a 
left join estado e on a.ID_Solicitud=e.ID_Solicitud 
left join users on a.user_id=users.id 
left join (select Id_Solicitud from tbl_bi_ops_refunds) d on 
a.Id_Solicitud=d.Id_Solicitud) as t
where t.id is null
and Id_Solicitud not like '000%'
and length(NumeroOrden)=9
and NumeroOrden like '2%'
and NumeroOrden <>'200000000';

SELECT  'Insertar datos de solicitudes de allice',now();
insert into tbl_bi_ops_refunds (Id_Solicitud, order_nr, Item, date_request, comment, razon_reembolso, 
motivo_devolucion, metodo_reembolso)
select id, order_nr,t.item_id, t.date_return, t.comment, t.dev,
t.reason, t.method from
(select concat(e.order_nr,"-",a.fk_sales_order_item) id, e.order_nr,a.fk_sales_order_item as item_id, 
a.date_created as 'date_return', a.comment, 'Devolucion' dev,
b.name as reason, c.name as method, d.Id_Solicitud id2 from bob_live_co.sales_order_item_return a 
left join bob_live_co.automatic_return_lists b on a.fk_return_list_reason=b.id_automatic_return_lists 
left join bob_live_co.automatic_return_lists c on a.fk_return_list_action=c.id_automatic_return_lists
left join bazayaco.tbl_order_detail e on a.fk_sales_order_item=e.Item 
left join (select Id_Solicitud from tbl_bi_ops_refunds) d on concat(e.order_nr,"-",a.fk_sales_order_item)=d.Id_Solicitud
where e.order_nr is not null
group by a.fk_sales_order_item) t
where t.id2 is null;

SELECT  'Actualizar datos de cuenta',now();
update tbl_bi_ops_refunds t1 inner join 
(select a.* from info_cuenta a inner join
(select ID_Solicitud, max(fecha) max from info_cuenta
group by ID_Solicitud) b
on a.ID_Solicitud=b.ID_Solicitud
and a.fecha=b.max) t2
on t1.Id_Solicitud=t2.ID_Solicitud
set
t1.titular_cuenta=t2.Titular, 
t1.cedula_titular_cuenta=t2.Cedula, 
t1.banco=t2.Banco, 
t1.tipo_cuenta=t2.Tipo, 
t1.numero_cuenta=t2.NumeroCuenta,
t1.ciudad_cuenta=t2.Ciudad, 
t1.fecha_cuenta=t2.Fecha;

SELECT  'Actualizar estado del item',now();
update tbl_bi_ops_refunds a inner join bazayaco.tbl_order_detail b
on a.Item=b.Item
set a.status_item=b.status_item;

SELECT  'Actualizar campos de la solicitud con tbl_order_detail',now();
update tbl_bi_ops_refunds a inner join bazayaco.tbl_order_detail b
on a.Item=b.Item
set 
a.fecha_orden=b.date_ordered,
a.paid_price=b.paid_price,
a.shipping_fee=b.shipping_fee,
a.Valor=b.paid_price+b.shipping_fee,
a.sku=b.sku,
a.Product_name=b.Product_name, 
a.payment_method=b.payment_method, 
a.ciudad=b.ciudad,
a.is_marketplace=b.is_marketplace
where a.fecha_orden is null;

update tbl_bi_ops_refunds a inner join bazayaco.tbl_order_detail b
on a.Item=b.Item
set 
a.CustId=b.CustId;

SELECT  'Actualizar campos de la solicitud con tbl_order_detail',now();
update  tbl_bi_ops_refunds a inner join
solicitud b on a.Id_Solicitud=b.ID_Solicitud
set aplicativo=1
where aplicativo=0;

SELECT  'Actualizar campos de la solicitud desde allice',now();
update tbl_bi_ops_refunds t1 inner join
(select concat(e.order_nr,"-",a.fk_sales_order_item) id, e.order_nr,a.fk_sales_order_item as item_id, 
a.date_created as 'date_return', a.comment, 'Devolucion' dev,
b.name as reason, c.name as method, d.Id_Solicitud id2 from bob_live_co.sales_order_item_return a 
left join bob_live_co.automatic_return_lists b on a.fk_return_list_reason=b.id_automatic_return_lists 
left join bob_live_co.automatic_return_lists c on a.fk_return_list_action=c.id_automatic_return_lists
left join bazayaco.tbl_order_detail e on a.fk_sales_order_item=e.Item 
left join (select Id_Solicitud from tbl_bi_ops_refunds) d on concat(e.order_nr,"-",a.fk_sales_order_item)=d.Id_Solicitud
group by a.fk_sales_order_item) t2 ON t1.Id_Solicitud = t2.id
set
t1.date_request=t2.date_return, 
t1.comment=t2.comment, 
t1.razon_reembolso=t2.dev, 
t1.motivo_devolucion=t2.reason, 
t1.metodo_reembolso=t2.method,
t1.allice=1;

SELECT  'Actualizar cedula de acuerdo a bob',now();
update tbl_bi_ops_refunds a
inner join bob_live_co.customer h on a.CustID=h.id_customer 
set
a.national_registration_number=h.national_registration_number;

SELECT  'Actualizar nombres y telefonos',now();

DROP   TEMPORARY TABLE IF EXISTS tbl_bi_ops_names_phones;
 CREATE TEMPORARY TABLE tbl_bi_ops_names_phones
select t.first_name, t.last_name, t.fk_customer, t.phone, t.mobile_phone, t.created_at 
from bob_live_co.customer_address t join 
(select fk_customer, max(created_at) max from bob_live_co.customer_address group by fk_customer) t2 on t.created_at=t2.max 
and t.fk_customer=t2.fk_customer;

create index idx_cust on tbl_bi_ops_names_phones(fk_customer);


update tbl_bi_ops_refunds a
inner join tbl_bi_ops_names_phones g on a.CustID=g.fk_customer 
set
a.first_name=g.first_name, 
a.last_name=g.last_name,
a.number=if(g.mobile_phone is null,g.phone, g.mobile_phone);

SELECT  'Actualizar cant items',now();

DROP   TEMPORARY TABLE IF EXISTS tbl_bi_ops_cant_items;
 CREATE TEMPORARY TABLE tbl_bi_ops_cant_items
select order_nr,count(Item) as 'cant_items' from bazayaco.tbl_order_detail group by order_nr;

create index idx_order on tbl_bi_ops_cant_items(order_nr);

update tbl_bi_ops_refunds a
inner join tbl_bi_ops_cant_items as k on 
a.order_nr=k.order_nr 
set
a.cant_items_orden=k.cant_items;


DROP   TEMPORARY TABLE IF EXISTS tbl_bi_ops_cant_items_sol;
 CREATE TEMPORARY TABLE tbl_bi_ops_cant_items_sol
select order_nr,count(Item) as 'cant_items' from tbl_bi_ops_refunds 
where metodo_reembolso='reversion' and
status_item is not null 
group by order_nr;

create index idx_order on tbl_bi_ops_cant_items_sol(order_nr);

update tbl_bi_ops_refunds a
inner join tbl_bi_ops_cant_items_sol as k on 
a.order_nr=k.order_nr 
set
a.cant_items_solicitud=k.cant_items;

update tbl_bi_ops_refunds 
set
cant_items_orden=if(cant_items_orden is null,0,cant_items_orden),
cant_items_solicitud=if(cant_items_solicitud is null,0,cant_items_solicitud);

SELECT  'Actualizar guia y estados de wms',now();

update tbl_bi_ops_refunds a
inner join wmsprod_co.inverselogistics_devolucion as w on 
a.Item=w.item_id 
set
a.guia_devolucion=w.carrier_tracking_code, 
a.status_wms_il=w.status, 
status_wms_money=w.money_status;

SELECT  'Actualizar estados del aplicativo',now();

update tbl_bi_ops_refunds a
inner join estado b on a.Id_Solicitud=b.ID_Solicitud
set a.estado=b.estado
where a.estado='Abierto';

SELECT  'Unificar nombres metodos de reembolso',now();

update tbl_bi_ops_refunds a
inner join tbl_bi_ops_refunds_metodo_reembolso b
on a.metodo_reembolso=b.nombre_allice
set a.metodo_reembolso=b.nombre_aplicativo ;

SELECT  'Actualizar estados de la solicitud',now();

update tbl_bi_ops_refunds
set 
estado='Cerrado',
fecha_estado=now()
where status_item in ('canceled','invalid','fraud_lost','cash_refunded','store_credit_issued','refund_issued');

SELECT  'Actualizar datos de efecty',now();

update tbl_bi_ops_refunds
set 
NOMBRE_EFECTY=upper(if(instr(first_name," ")=0,first_name,
left(first_name,instr(first_name," ")-1))),
APELLIDO1_EFECTY=upper(if(instr(last_name ," ")=0,last_name ,
left(last_name ,instr(last_name ," ")-1))),
APELLIDO2_EFECTY=upper(if(instr(last_name," ")=0,"N/A",
right(last_name,length(last_name)-instr(last_name," "))))
where first_name is not null;

update tbl_bi_ops_refunds
set 
TIPO_DOCUMENTO_EFECTY=IF(national_registration_number like "%-%","NIT",
"CC")
where national_registration_number is not null;

SELECT  'Revisar solicitudes que apliquen',now();

UPDATE tbl_bi_ops_refunds
set
aplica=0;

UPDATE tbl_bi_ops_refunds
set
aplica=1
where status_item in ('clarify_store_credit_not_issued','clarify_refund_needed','refund_needed','store_credit_needed');

#Agregar reembolsos de garantía de acuerdo al link de reembolsos
update tbl_bi_ops_refunds a inner join solicitud b
on a.ID_Solicitud=b.Id_Solicitud
set aplica=1
where Observaciones like '%GARANTIA CON REEMBOLSO%'
and status_item='closed';

SELECT  'Actualizar metodos de reembolso de acuerdo al aplicativo',now();

update tbl_bi_ops_refunds a
inner join solicitud b on a.Id_Solicitud=b.ID_Solicitud
set metodo_reembolso=MetodoReembolso
where a.metodo_reembolso<>b.MetodoReembolso
and MetodoReembolso<>""
and a.estado="Abierto";

#Se aplica devolución shipping fee en todos los casos de acuerdo a la ley 22/01/2013

SELECT  'Actualizar valores de acuerdo al shipping fee',now();

update tbl_bi_ops_refunds a
inner join tbl_bi_ops_refunds_shipping_fee as b on 
a.motivo_devolucion=b.motivo
set
a.aplica_shipping_fee=1;

update tbl_bi_ops_refunds a
set
Valor=if(aplica_shipping_fee=1,paid_price+shipping_fee,paid_price);

/*SELECT  'Revisar solicitudes closed que apliquen segun bodega',now();
update tbl_bi_ops_refunds 
set aplica=1,
novedad=0
WHERE id_solicitud in
(SELECT concat(order_nr,"-",item)
FROM bazayaco.tbl_bi_ops_refunds_change_status
where change_status="SI");*/

SELECT  'Actualizar nulls',now();
update tbl_bi_ops_refunds a 
inner join solicitud b on a.Id_solicitud=b.ID_Solicitud
set
a.uid=b.uid,
correo_cliente=CorreoCliente,
razon_reembolso=RazonReembolso,
motivo_devolucion=MotivoDevolucion,
metodo_reembolso=MetodoReembolso,
date_request=FechaHora
where metodo_reembolso is null;

SELECT  'Actualizar novedades',now();


UPDATE tbl_bi_ops_refunds
set
novedad=null,
tipo_novedad=null;


UPDATE tbl_bi_ops_refunds
set
novedad=5,
tipo_novedad='INE'
where status_item is null;


UPDATE tbl_bi_ops_refunds
set
novedad=8,
tipo_novedad='RYH'
where status_item in ('store_credit_issued','cash_refunded');


update tbl_bi_ops_refunds
set
novedad=6,
tipo_novedad='ONE'
where cant_items_orden is null
and novedad<>5;


update tbl_bi_ops_refunds
set novedad=1,
tipo_novedad='ENC'
where aplica=0
and (novedad is null  or novedad not in (5,6,8));


update tbl_bi_ops_refunds
set
novedad=3,
tipo_novedad='SSC'
where metodo_reembolso='consignacion'
and (novedad is null  or novedad not in (1,5,6,8))
and titular_cuenta is null;


update tbl_bi_ops_refunds 
set
novedad=2,
tipo_novedad='CNC'
where metodo_reembolso='consignacion'
and (novedad is null  or novedad not in (1,5,6,8,3))
and cedula_titular_cuenta<>national_registration_number;


update tbl_bi_ops_refunds
set
novedad=7,
tipo_novedad='NAR'
where 
metodo_reembolso='reversion' and
(novedad is null  or novedad not in (1,5,6,8,3,2))
and cant_items_solicitud<>cant_items_orden;


update tbl_bi_ops_refunds 
set
novedad=9,
tipo_novedad='NAE'
where metodo_reembolso='efecty'
and (Valor>500000 or Valor<20000)
and (novedad is null  or novedad not in (1,5,6,8,3,2,7));


UPDATE tbl_bi_ops_refunds 
set
novedad=10,
tipo_novedad='SNI'
where (novedad is null  or novedad not in (1,5,6,8,3,2,7,9))
and aplicativo+ allice=0;


update tbl_bi_ops_refunds 
set
novedad=4,
tipo_novedad='OK'
where (novedad is null  or novedad not in (1,5,6,8,3,2,7,9,10));


update tbl_bi_ops_refunds
set 
novedad=9,
tipo_novedad='SMR'
where metodo_reembolso="" and novedad=4;


update tbl_bi_ops_refunds
set need_refund=if(novedad=4,1,0);

SELECT  'Datos de consignacion para disfon',now();

update tbl_bi_ops_refunds 
set
TIPO_DOCUMENTO_CONSIG=if(TIPO_DOCUMENTO_EFECTY='CC','C','N'),
TIPO_CUENTA_CONSIG=IF(tipo_cuenta='Ahorros','02','01')
where need_refund=1
and metodo_reembolso="consignacion";

update tbl_bi_ops_refunds a inner join
tbl_bi_ops_refunds_bancos b on a.banco=b.nombre_aplicativo
set
a.CODIGO_BANCO_CONSIG=b.codigo
where need_refund=1
and metodo_reembolso="consignacion";

INSERT INTO production.table_monitoring_log (
  country, 
  table_name, 
  step,
  updated_at, 
  key_date, 
  total_rows, 
  total_rows_check)
SELECT 
  'Colombia', 
  'customer_service_co.tbl_bi_ops_refunds',
  'finish',
  NOW(),
  MAX(date_request),
  count(*),
  count(*)
FROM
  customer_service_co.tbl_bi_ops_refunds;


SELECT  'Fin rutina CC Refunds',now();



END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `bi_ops_refunds_order_item`()
BEGIN

DECLARE v_orderID VARCHAR(255) DEFAULT '';
DECLARE v_concat_item VARCHAR(255) DEFAULT '';
DECLARE itemID VARCHAR(255) DEFAULT '';
DECLARE outer_done boolean;
declare inner_done boolean;


DECLARE ordenes CURSOR FOR
  
SELECT DISTINCT order_nr FROM tbl_bi_ops_refunds
where need_refund=1;

DECLARE items CURSOR FOR
  
select distinct(Item) from tbl_bi_ops_refunds  where need_refund=1 and order_nr=v_orderID;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET outer_done = TRUE;

OPEN ordenes;

cust_loop: LOOP

BEGIN
DECLARE CONTINUE HANDLER FOR NOT FOUND SET inner_done = TRUE;

   FETCH ordenes INTO v_orderID;
   
   IF outer_done THEN
      
      LEAVE cust_loop;
    
   END IF;

   OPEN items;
   items_loop: LOOP
        FETCH items INTO itemID;
        
        IF inner_done THEN
      
           SET v_concat_item=CONCAT(v_concat_item,itemID);
           LEAVE items_loop;
    
        ELSE
           SET v_concat_item=CONCAT(v_concat_item,itemID,"-");
        END IF;        
   END LOOP items_loop;
   CLOSE items;
   
   INSERT INTO tbl_bi_ops_refunds_item_order VALUES (v_orderID, v_concat_item);

END;

END LOOP cust_loop;

CLOSE ordenes;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `bi_ops_refunds_test`()
BEGIN

SELECT  'Inicio rutina CC Refunds',now();

set time_zone='-5:00';

SELECT  'Insertar datos de solicitudes de reembolsos de bob',now();
-- a
insert into tbl_bi_ops_refunds_test (Id_Solicitud, fecha_orden, order_nr, Item, status_item, paid_price,shipping_fee,
Valor, sku, Product_name, payment_method, ciudad, is_marketplace)
select id,Date,OrderNum, ItemID, Status, PaidPrice,ShippingFee, 
Valor, SKUSimple,
SKUName,PaymentMethod,City, isMPlace from 
(select  
concat(a.OrderNum,"-",ItemID) id,a.Date,a.OrderNum, a.ItemId, 
a.Status, a.PaidPrice,a.ShippingFee, 
a.PaidPrice+a.ShippingFee as 'Valor', a.SKUSimple,
a.SKUName,a.PaymentMethod,a.City, a.isMPlace, b.Id_Solicitud
from development_co_project.A_Master a 
left join (select Id_Solicitud from tbl_bi_ops_refunds_test) b on concat(a.OrderNum,"-",ItemID)=b.Id_Solicitud
where Status like '%refund_needed%' or Status like '%store_credit_needed%'
and Date>='2013-08-01') t
where Id_Solicitud is null;

SELECT  'Insertar datos de solicitudes del aplicativo',now();

insert into tbl_bi_ops_refunds_test(Id_Solicitud, estado, uid, order_nr, Item, correo_cliente, razon_reembolso, motivo_devolucion, 
metodo_reembolso, user_customer_service, date_request, Fecha_estado, guia_devolucion)
select ID_Solicitud, Estado, uid, NumeroOrden, item_id, CorreoCliente, RazonReembolso, 
MotivoDevolucion, MetodoReembolso, name, FechaHora,   
Fecha_estado, guideNumber from
(select a.ID_Solicitud, e.Estado, a.uid, a.NumeroOrden, a.item_id, a.CorreoCliente, a.RazonReembolso, 
a.MotivoDevolucion, a.MetodoReembolso, users.name, a.FechaHora,
e.Fecha as 'Fecha_estado', a.guideNumber, d.Id_Solicitud id from solicitud a 
left join estado e on a.ID_Solicitud=e.ID_Solicitud 
left join users on a.user_id=users.id 
left join (select Id_Solicitud from tbl_bi_ops_refunds_test) d on 
a.Id_Solicitud=d.Id_Solicitud) as t
where t.id is null
and Id_Solicitud not like '000%'
and length(NumeroOrden)=9
and NumeroOrden like '2%'
and NumeroOrden <>'200000000';

SELECT  'Insertar datos de solicitudes de allice',now();
insert into tbl_bi_ops_refunds_test (Id_Solicitud, order_nr, Item, date_request, comment, razon_reembolso, 
motivo_devolucion, metodo_reembolso)
select id, OrderNum,t.item_id, t.date_return, t.comment, t.dev,
t.reason, t.method from
(select concat(e.OrderNum,"-",a.fk_sales_order_item) id, e.OrderNum,a.fk_sales_order_item as item_id, 
a.date_created as 'date_return', a.comment, 'Devolucion' dev,
b.name as reason, c.name as method, d.Id_Solicitud id2 from bob_live_co.sales_order_item_return a 
left join bob_live_co.automatic_return_lists b on a.fk_return_list_reason=b.id_automatic_return_lists 
left join bob_live_co.automatic_return_lists c on a.fk_return_list_action=c.id_automatic_return_lists
left join development_co_project.A_Master e on a.fk_sales_order_item=e.ItemID 
left join (select Id_Solicitud from tbl_bi_ops_refunds_test) d on concat(e.OrderNum,"-",a.fk_sales_order_item)=d.Id_Solicitud
group by a.fk_sales_order_item) t
where t.id2 is null;

SELECT  'Actualizar datos de cuenta',now();
update tbl_bi_ops_refunds_test t1 inner join 
(select a.* from info_cuenta a inner join
(select ID_Solicitud, max(fecha) max from info_cuenta
group by ID_Solicitud) b
on a.ID_Solicitud=b.ID_Solicitud
and a.fecha=b.max) t2
on t1.Id_Solicitud=t2.ID_Solicitud
set
t1.titular_cuenta=t2.Titular, 
t1.cedula_titular_cuenta=t2.Cedula, 
t1.banco=t2.Banco, 
t1.tipo_cuenta=t2.Tipo, 
t1.numero_cuenta=t2.NumeroCuenta,
t1.ciudad_cuenta=t2.Ciudad, 
t1.fecha_cuenta=t2.Fecha;

SELECT  'Actualizar estado del item',now();
update tbl_bi_ops_refunds_test a inner join development_co_project.A_Master b
on a.Item=b.ItemID
set a.status_item=b.Status;

SELECT  'Actualizar campos de la solicitud con tbl_order_detail',now();
update tbl_bi_ops_refunds_test a inner join development_co_project.A_Master b
on a.Item=b.ItemID
set 
a.fecha_orden=b.Date,
a.paid_price=b.PaidPrice,
a.shipping_fee=ShippingFee,
a.Valor=b.PaidPrice+b.ShippingFee,
a.sku=b.SKUSimple,
a.Product_name=b.SKUName, 
a.payment_method=b.PaymentMethod, 
a.ciudad=b.City,
a.is_marketplace=b.isMPlace
where a.fecha_orden is null;

update tbl_bi_ops_refunds_test a inner join development_co_project.A_Master b
on a.Item=b.ItemID
set 
a.CustId=b.CustomerNum;

SELECT  'Actualizar campos de la solicitud con tbl_order_detail',now();
update  tbl_bi_ops_refunds_test a inner join
solicitud b on a.Id_Solicitud=b.ID_Solicitud
set aplicativo=1
where aplicativo=0;

SELECT  'Actualizar campos de la solicitud desde allice',now();
update tbl_bi_ops_refunds_test t1 inner join
(select concat(e.OrderNum,"-",a.fk_sales_order_item) id, e.OrderNum,a.fk_sales_order_item as item_id, 
a.date_created as 'date_return', a.comment, 'Devolucion' dev,
b.name as reason, c.name as method, d.Id_Solicitud id2 from bob_live_co.sales_order_item_return a 
left join bob_live_co.automatic_return_lists b on a.fk_return_list_reason=b.id_automatic_return_lists 
left join bob_live_co.automatic_return_lists c on a.fk_return_list_action=c.id_automatic_return_lists
left join development_co_project.A_Master e on a.fk_sales_order_item=e.ItemID 
left join (select Id_Solicitud from tbl_bi_ops_refunds_test) d on concat(e.OrderNum,"-",a.fk_sales_order_item)=d.Id_Solicitud
group by a.fk_sales_order_item) t2 ON t1.Id_Solicitud = t2.id
set
t1.date_request=t2.date_return, 
t1.comment=t2.comment, 
t1.razon_reembolso=t2.dev, 
t1.motivo_devolucion=t2.reason, 
t1.metodo_reembolso=t2.method,
t1.allice=1;

SELECT  'Actualizar cedula de acuerdo a bob',now();
update tbl_bi_ops_refunds_test a
inner join bob_live_co.customer h on a.CustID=h.id_customer 
set
a.national_registration_number=h.national_registration_number;

SELECT  'Actualizar nombres y telefonos',now();

DROP   TEMPORARY TABLE IF EXISTS tbl_bi_ops_names_phones;
 CREATE TEMPORARY TABLE tbl_bi_ops_names_phones
select t.first_name, t.last_name, t.fk_customer, t.phone, t.mobile_phone, t.created_at 
from bob_live_co.customer_address t join 
(select fk_customer, max(created_at) max from bob_live_co.customer_address group by fk_customer) t2 on t.created_at=t2.max 
and t.fk_customer=t2.fk_customer;

create index idx_cust on tbl_bi_ops_names_phones(fk_customer);


update tbl_bi_ops_refunds_test a
inner join tbl_bi_ops_names_phones g on a.CustID=g.fk_customer 
set
a.first_name=g.first_name, 
a.last_name=g.last_name,
a.number=if(g.mobile_phone is null,g.phone, g.mobile_phone);

SELECT  'Actualizar cant items',now();

DROP   TEMPORARY TABLE IF EXISTS tbl_bi_ops_cant_items;
 CREATE TEMPORARY TABLE tbl_bi_ops_cant_items
select OrderNum,count(ItemID) as 'cant_items' from development_co_project.A_Master group by OrderNum;

create index idx_order on tbl_bi_ops_cant_items(OrderNum);

update tbl_bi_ops_refunds_test a
inner join tbl_bi_ops_cant_items as k on 
a.order_nr=k.OrderNum
set
a.cant_items_orden=k.cant_items;


DROP   TEMPORARY TABLE IF EXISTS tbl_bi_ops_cant_items_sol;
 CREATE TEMPORARY TABLE tbl_bi_ops_cant_items_sol
select order_nr,count(Item) as 'cant_items' from tbl_bi_ops_refunds_test 
where metodo_reembolso='reversion' and
status_item is not null 
group by order_nr;

create index idx_order on tbl_bi_ops_cant_items_sol(order_nr);

update tbl_bi_ops_refunds_test a
inner join tbl_bi_ops_cant_items_sol as k on 
a.order_nr=k.order_nr 
set
a.cant_items_solicitud=k.cant_items;

update tbl_bi_ops_refunds_test
set
cant_items_orden=if(cant_items_orden is null,0,cant_items_orden),
cant_items_solicitud=if(cant_items_solicitud is null,0,cant_items_solicitud);

SELECT  'Actualizar guia y estados de wms',now();

update tbl_bi_ops_refunds_test a
inner join wmsprod_co.inverselogistics_devolucion as w on 
a.Item=w.item_id 
set
a.guia_devolucion=w.carrier_tracking_code, 
a.status_wms_il=w.status, 
status_wms_money=w.money_status;

SELECT  'Actualizar estados del aplicativo',now();

update tbl_bi_ops_refunds_test a
inner join estado b on a.Id_Solicitud=b.ID_Solicitud
set a.estado=b.estado
where a.estado='Abierto';

SELECT  'Unificar nombres metodos de reembolso',now();

update tbl_bi_ops_refunds_test a
inner join tbl_bi_ops_refunds_metodo_reembolso b
on a.metodo_reembolso=b.nombre_allice
set a.metodo_reembolso=b.nombre_aplicativo ;

SELECT  'Actualizar estados de la solicitud',now();

update tbl_bi_ops_refunds_test
set 
estado='Cerrado',
fecha_estado=now()
where status_item in ('canceled','invalid','fraud_lost','cash_refunded','store_credit_issued','refund_issued');

SELECT  'Actualizar datos de efecty',now();

update tbl_bi_ops_refunds_test
set 
NOMBRE_EFECTY=upper(if(instr(first_name," ")=0,first_name,
left(first_name,instr(first_name," ")-1))),
APELLIDO1_EFECTY=upper(if(instr(last_name ," ")=0,last_name ,
left(last_name ,instr(last_name ," ")-1))),
APELLIDO2_EFECTY=upper(if(instr(last_name," ")=0,"N/A",
right(last_name,length(last_name)-instr(last_name," "))))
where first_name is not null;

update tbl_bi_ops_refunds_test
set 
TIPO_DOCUMENTO_EFECTY=IF(national_registration_number like "%-%","NIT",
"CC")
where national_registration_number is not null;

SELECT  'Revisar solicitudes que apliquen',now();

UPDATE tbl_bi_ops_refunds_test
set
aplica=0;

UPDATE tbl_bi_ops_refunds_test
set
aplica=1
where status_item in ('clarify_store_credit_not_issued','clarify_refund_needed','refund_needed','store_credit_needed');


SELECT  'Actualizar metodos de reembolso de acuerdo al aplicativo',now();

update tbl_bi_ops_refunds_test a
inner join solicitud b on a.Id_Solicitud=b.ID_Solicitud
set metodo_reembolso=MetodoReembolso
where a.metodo_reembolso<>b.MetodoReembolso
and MetodoReembolso<>""
and a.estado="Abierto";

#Se aplica devolución shipping fee en todos los casos de acuerdo a la ley 22/01/2013

SELECT  'Actualizar valores de acuerdo al shipping fee',now();

update tbl_bi_ops_refunds_test a
inner join tbl_bi_ops_refunds_shipping_fee as b on 
a.motivo_devolucion=b.motivo
set
a.aplica_shipping_fee=1;

update tbl_bi_ops_refunds_test a
set
Valor=if(aplica_shipping_fee=1,paid_price+shipping_fee,paid_price);

/*SELECT  'Revisar solicitudes closed que apliquen segun bodega',now();
update tbl_bi_ops_refunds 
set aplica=1,
novedad=0
WHERE id_solicitud in
(SELECT concat(order_nr,"-",item)
FROM bazayaco.tbl_bi_ops_refunds_change_status
where change_status="SI");*/

SELECT  'Actualizar nulls',now();
update tbl_bi_ops_refunds_test a 
inner join solicitud b on a.Id_solicitud=b.ID_Solicitud
set
a.uid=b.uid,
correo_cliente=CorreoCliente,
razon_reembolso=RazonReembolso,
motivo_devolucion=MotivoDevolucion,
metodo_reembolso=MetodoReembolso,
date_request=FechaHora
where metodo_reembolso is null;

SELECT  'Actualizar novedades',now();


UPDATE tbl_bi_ops_refunds_test
set
novedad=null,
tipo_novedad=null;


UPDATE tbl_bi_ops_refunds_test
set
novedad=5,
tipo_novedad='INE'
where status_item is null;


UPDATE tbl_bi_ops_refunds_test
set
novedad=8,
tipo_novedad='RYH'
where status_item in ('store_credit_issued','cash_refunded');


update tbl_bi_ops_refunds_test
set
novedad=6,
tipo_novedad='ONE'
where cant_items_orden is null
and novedad<>5;


update tbl_bi_ops_refunds_test
set novedad=1,
tipo_novedad='ENC'
where aplica=0
and (novedad is null  or novedad not in (5,6,8));


update tbl_bi_ops_refunds_test
set
novedad=3,
tipo_novedad='SSC'
where metodo_reembolso='consignacion'
and (novedad is null  or novedad not in (1,5,6,8))
and titular_cuenta is null;


update tbl_bi_ops_refunds_test
set
novedad=2,
tipo_novedad='CNC'
where metodo_reembolso='consignacion'
and (novedad is null  or novedad not in (1,5,6,8,3))
and cedula_titular_cuenta<>national_registration_number;


update tbl_bi_ops_refunds_test
set
novedad=7,
tipo_novedad='NAR'
where 
metodo_reembolso='reversion' and
(novedad is null  or novedad not in (1,5,6,8,3,2))
and cant_items_solicitud<>cant_items_orden;


update tbl_bi_ops_refunds_test
set
novedad=9,
tipo_novedad='NAE'
where metodo_reembolso='efecty'
and (Valor>500000 or Valor<20000)
and (novedad is null  or novedad not in (1,5,6,8,3,2,7));


UPDATE tbl_bi_ops_refunds_test
set
novedad=10,
tipo_novedad='SNI'
where (novedad is null  or novedad not in (1,5,6,8,3,2,7,9))
and aplicativo+ allice=0;


update tbl_bi_ops_refunds_test 
set
novedad=4,
tipo_novedad='OK'
where (novedad is null  or novedad not in (1,5,6,8,3,2,7,9,10));


update tbl_bi_ops_refunds_test
set 
novedad=9,
tipo_novedad='SMR'
where metodo_reembolso="" and novedad=4;


update tbl_bi_ops_refunds_test
set need_refund=if(novedad=4,1,0);

SELECT  'Datos de consignacion para disfon',now();

update tbl_bi_ops_refunds_test
set
TIPO_DOCUMENTO_CONSIG=if(TIPO_DOCUMENTO_EFECTY='CC','C','N'),
TIPO_CUENTA_CONSIG=IF(tipo_cuenta='Ahorros','02','01')
where need_refund=1
and metodo_reembolso="consignacion";

update tbl_bi_ops_refunds_test a inner join
tbl_bi_ops_refunds_bancos b on a.banco=b.nombre_aplicativo
set
a.CODIGO_BANCO_CONSIG=b.codigo
where need_refund=1
and metodo_reembolso="consignacion";


SELECT  'Fin rutina CC Refunds',now();



END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`bi_co`@`%`*/ /*!50003 PROCEDURE `bi_ops_tickets`()
BEGIN

SELECT  'Inicio rutina CC Tickets',now();

truncate table bi_ops_tickets;

#Agregar datos de tabla fuente
insert into bi_ops_tickets (summation_column,id_zendex,requester,requester_id,requester_external_id,requester_mail,requester_domain,submitter,
asignee,group_1,subject,tags,status,priority,via,tiquet_type,created_at,update_at,assigned_at,organization,due_type,initially_asigned_at,
solved_at,resolution_time,satisfation_score,group_stations,assignee_stations,reopens,replies,first_reply_time_minutes,
first_reply_time_minutes_bh,first_resolution_time,first_resolution_time_bh,full_resolution_time,full_resoluction_time_bh,
agent_wait_time_minutes,agent_wait_time_minutes_bh,requester_wait_time_minutes,requester_wait_time_minutes_bh,
on_hold_time_minutes,on_hold_time_minutes_bh,contacto_inicial,tipo_solicitud,order_id,motivo_devolucion_cancelacion,
product_return,quality_check,piezas_faltantes)

select `Summation column`,`Id`,`Requester`,`Requester id`,`Requester external id`,`Requester email`,`Requester domain`,`Submitter`,`Assignee`,
`Group`,`Subject`,`Tags`,`Status`,`Priority`,`Via`,`Ticket type`,`created_at`,`Updated at`,`Assigned at`,`Organization`,`Due date`,`Initially assigned at`,
`Solved at`,`Resolution time`,`Satisfaction Score`,`Group stations`,`Assignee stations`,`Reopens`,`Replies`,`First reply time in minutes`,
`First reply time in minutes within business hours`,`First resolution time in minutes`,`First resolution time in minutes within business hours`,
`Full resolution time in minutes`,`Full resolution time in minutes within business hours`,`Agent wait time in minutes`,
`Agent wait time in minutes within business hours`,`Requester wait time in minutes`,`Requester wait time in minutes within business hours`,
`On hold time in minutes`,`On hold time in minutes within business hours`,`Contacto Inicial [list]`,`Tipo de solicitud [list]`,
`Order ID (BOB) [int]`,`MOTIVO DE DEVOLUCIÓN O CANCELACION [list]`,`RETORNO DEL PRODUCTO [list]`,`RESPUESTA QUALITY [list]`,
`ESCALAMIENTO POR PIEZAS FALTANTES [list]`  from customer_service.tbl_zendesk_co;



SELECT  'Actualizar info clientes',now();
#update bi_ops_tickets
#set cliente=if(requester_domain='linio.com' or submitter in (select name from #bi_ops_agents_zendex),0,1)
#where contacto_inicial='Correo Electronico Entrante';

update bi_ops_tickets
set cliente=1
where via='Mail';

SELECT  'Actualizar fechas',now();
update bi_ops_tickets
set date_creation=date(created_at),
week_creation=weekofyear(created_at),
weekday_creation=weekday(created_at),
month_creation=monthname(created_at),
year_creation=year(created_at),
hour_creation=hour(created_at),
minute_creation=minute(created_at),
shift_creation=if(minute_creation<30,concat(hour_creation,':','00'),concat(hour_creation,':','30')),
date_asigned=date(assigned_at),
date_initialy_asigned=date(initially_asigned_at),
date_solved=date(solved_at);

SELECT  'Actualizar holidays',now();
update bi_ops_tickets a inner join calendar b on a.date_creation=b.dt
set holiday=b.isholiday;

SELECT  'Actualizar horas laborales',now();
update bi_ops_tickets
set working_hour=if(weekday_creation=6 or holiday=1,
	if(hour_creation>=10 and hour_creation<18,
		1,0),
		if(weekday_creation=5,
			if(hour_creation>=9 and hour_creation<21,
			1,0),
			if(hour_creation>=8 and hour_creation<21,
				1,0)))
;

SELECT  'Actualizar horas después de horariop',now();
update bi_ops_tickets
set after_time=if(working_hour=0 and hour_creation>=18,1,0)
;

SELECT  'Actualizar tiempos de solicitud',now();
update bi_ops_tickets a inner join bi_ops_tickets_solicitudes b on a.tipo_solicitud=b.solicitud
set 
a.time_solve=b.time_solve,
a.time_ticket=b.tmo,
a.process=b.proceso,
a.responsable=b.responsable;

#update bi_ops_tickets 
#set responsable='COORDINADORES'
#where requester_mail in (select correo from zendex_coordinadores) and date_creation>='2013-05-01' ;

update bi_ops_tickets 
set responsable='COORDINADORES'
where asignee='Admin';


update bi_ops_tickets
set
responsable='FRONT OFFICE' where responsable='-';

SELECT  'Actualizar tiempo entre asignado y resuelto',now();
update bi_ops_tickets 
set time_assigned_solved=timediff(solved_at,assigned_at) ;

SELECT  'Actualizar cumplimiento',now();
update bi_ops_tickets 
set cumple=if(hour(time_assigned_solved)<=time_solve,1,0)
#where responsable='BACK OFFICE'
;

SELECT  'Actualizar otros binarios',now();
update bi_ops_tickets 
set 
solved=if(status in ('Closed','Solved'),1,0),
pending=if(status='Pending',1,0),
same_day=if(date_creation=date_solved,1,0);


SELECT  'Ingresar info del backlog',now();
set @max_day=cast((select max(date) from bi_ops_tickets_backlog) as date);

delete from bi_ops_tickets_backlog where date=@max_day;

insert into bi_ops_tickets_backlog (date,new_tickets) 
select * from
(select date_creation,count(date_creation) 
FROM bi_ops_tickets 
group by date_creation) a
where a.date_creation>=@max_day;

SELECT  'Insertar nuevos tickets, resueltos, etc en tabla de backlog',now();
update bi_ops_tickets_backlog  a inner join 
(select date_creation,count(date_creation) as 'new' FROM bi_ops_tickets where cliente=1 group by date_creation) 
b on a.date=b.date_creation
set a.new_tickets_inc=b.new
where a.new_tickets_inc is null;

update bi_ops_tickets_backlog  a inner join (select date_creation,count(date_creation) as 'new' FROM bi_ops_tickets where responsable='BACK OFFICE' group by date_creation) b on a.date=b.date_creation
set a.new_tickets_bo=b.new
where a.new_tickets_bo is null;

update bi_ops_tickets_backlog  a inner join (select date_creation,count(date_creation) as 'new' FROM bi_ops_tickets where responsable='COORDINADORES' group by date_creation) b on a.date=b.date_creation
set a.new_tickets_coord=b.new
where a.new_tickets_coord is null;

update bi_ops_tickets_backlog  a inner join (select date_creation,count(date_creation) as 'new' FROM bi_ops_tickets where responsable='OUTBOUND' group by date_creation) b on a.date=b.date_creation
set a.new_tickets_out=b.new
where a.new_tickets_out is null;

/*update bi_ops_tickets_backlog  a inner join (select date_creation,count(date_creation) as 'new' FROM bi_ops_tickets where responsable='SPAM' group by date_creation) b on a.date=b.date_creation
set a.new_tickets_spam=b.new
where a.new_tickets_spam is null;*/

/*update bi_ops_tickets_backlog  a inner join (select date_creation,count(date_creation) as 'new' FROM bi_ops_tickets where responsable='LOGISTICA' group by date_creation) b on a.date=b.date_creation
set a.new_tickets_logistica=b.new
where a.new_tickets_logistica is null;*/

update bi_ops_tickets_backlog  a inner join (select date_creation,count(date_creation) as 'new' FROM bi_ops_tickets where responsable='FRONT OFFICE' group by date_creation) b on a.date=b.date_creation
set a.new_tickets_fo=b.new
where a.new_tickets_fo is null;


update bi_ops_tickets_backlog  a inner join (select date_solved,count(date_solved) as 'solved' FROM bi_ops_tickets  group by date_solved) b on a.date=b.date_solved
set a.solved_tickets=b.solved
where a.solved_tickets is null;

update bi_ops_tickets_backlog  a inner join (select date_solved,count(date_solved) as 'solved' FROM bi_ops_tickets where cliente=1 group by date_solved) b on a.date=b.date_solved
set a.solved_tickets_inc=b.solved
where a.solved_tickets_inc is null;

update bi_ops_tickets_backlog  a inner join (select date_solved,count(date_solved) as 'solved' FROM bi_ops_tickets where responsable='BACK OFFICE' group by date_solved) b on a.date=b.date_solved
set a.solved_tickets_bo=b.solved
where a.solved_tickets_bo is null;

update bi_ops_tickets_backlog  a inner join (select date_solved,count(date_solved) as 'solved' FROM bi_ops_tickets where responsable='COORDINADORES' group by date_solved) b on a.date=b.date_solved
set a.solved_tickets_coord=b.solved
where a.solved_tickets_coord is null;

update bi_ops_tickets_backlog  a inner join (select date_solved,count(date_solved) as 'solved' FROM bi_ops_tickets where responsable='OUTBOUND' group by date_solved) b on a.date=b.date_solved
set a.solved_tickets_out=b.solved
where a.solved_tickets_out is null;

/*update bi_ops_tickets_backlog  a inner join (select date_solved,count(date_solved) as 'solved' FROM bi_ops_tickets where responsable='SPAM' group by date_solved) b on a.date=b.date_solved
set a.solved_tickets_spam=b.solved
where a.solved_tickets_spam is null;*/

/*update bi_ops_tickets_backlog  a inner join (select date_solved,count(date_solved) as 'solved' FROM bi_ops_tickets where responsable='LOGISTICA' group by date_solved) b on a.date=b.date_solved
set a.solved_tickets_logistica=b.solved
where a.solved_tickets_logistica is null;*/

update bi_ops_tickets_backlog  a inner join (select date_solved,count(date_solved) as 'solved' FROM bi_ops_tickets where responsable='FRONT OFFICE' group by date_solved) b on a.date=b.date_solved
set a.solved_tickets_fo=b.solved
where a.solved_tickets_fo is null;

SELECT  'Actualizar backlog por día',now();
update bi_ops_tickets_backlog  
set 
backlog = backlog(date) - backlog_after(date),
backlog_inc=backlog2(date)-backlog_after2(date),
backlog_bo=backlog3(date)-backlog_after3(date),
backlog_coord=backlog4(date)-backlog_after4(date),
backlog_out=backlog5(date)-backlog_after5(date),
#backlog_spam=backlog6(date)-backlog_after6(date),
#backlog_logistica=backlog7(date)-backlog_after7(date)
backlog_fo=backlog8(date)-backlog_after8(date)
where date>=@max_day;


update  bi_ops_tickets_backlog  
set
time_to_clean=if(solved_tickets=0,backlog,backlog/solved_tickets),
time_to_clean_inc=if(solved_tickets_inc=0,backlog_inc,backlog_inc/solved_tickets_inc),
time_to_clean_bo=if(solved_tickets_bo=0,backlog_bo,backlog_bo/solved_tickets_bo),
time_to_clean_coord=if(solved_tickets_coord=0,backlog_coord,backlog_coord/solved_tickets_coord),
#time_to_clean_spam=if(solved_tickets_spam=0,backlog_bo,backlog_spam/solved_tickets_spam),
time_to_clean_out=if(solved_tickets_out=0,backlog_bo,backlog_out/solved_tickets_out),
#time_to_clean_logistica=if(solved_tickets_logistica=0,backlog_logistica,backlog_logistica/solved_tickets_logistica)
time_to_clean_fo=if(solved_tickets_fo=0,backlog_fo,backlog_fo/solved_tickets_fo)
where date>=@max_day;

SELECT  'Actualizar tickets por proceso',now();
truncate table bi_ops_tickets_by_process;

insert into bi_ops_tickets_by_process(date,new,solved,time_to_clean,backlog,process) SELECT date,new_tickets_bo,solved_tickets_bo,time_to_clean_bo,backlog_bo,'BACK OFFICE' FROM bi_ops_tickets_backlog;

insert into bi_ops_tickets_by_process(date,new,solved,time_to_clean,backlog,process) SELECT date,new_tickets_out,solved_tickets_out,time_to_clean_out,backlog_out,'OUTBOUND' FROM bi_ops_tickets_backlog;

#insert into bi_ops_tickets_by_process(date,new,solved,time_to_clean,backlog,process) SELECT date,new_tickets_spam,solved_tickets_spam,time_to_clean_spam,backlog_spam,'SPAMS' FROM bi_ops_tickets_backlog;

#insert into bi_ops_tickets_by_process(date,new,solved,time_to_clean,backlog,process) SELECT date,new_tickets_logistica,solved_tickets_logistica,time_to_clean_logistica,backlog_logistica,'LOGISTICA' FROM bi_ops_tickets_backlog;

insert into bi_ops_tickets_by_process(date,new,solved,time_to_clean,backlog,process) SELECT date,new_tickets_coord,solved_tickets_coord,time_to_clean_coord,backlog_coord,'COORDINADORES' FROM bi_ops_tickets_backlog;

insert into bi_ops_tickets_by_process(date,new,solved,time_to_clean,backlog,process) SELECT date,new_tickets_fo,solved_tickets_fo,time_to_clean_fo,backlog_fo,'FRONT OFFICE' FROM bi_ops_tickets_backlog;

SELECT  'Datos de satisfaccion',now();
update bi_ops_tickets
set
satisfaction=if(satisfation_score="Bad" or satisfation_score="Good",1,0),
good=if(satisfation_score="Good",1,0);

update bi_ops_tickets
set first_reply_time_h=if(first_reply_time_minutes="" or first_reply_time_minutes is null,
if(first_resolution_time="" or first_resolution_time is null,"",first_resolution_time/60),
first_reply_time_minutes/60);


SELECT  'Rutina finalizada',now();

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`paula.mendoza`@`%`*/ /*!50003 PROCEDURE `sp_time_in_office`()
BEGIN


#------------------------------------------------------------------------------
#POSTVENTA
#------------------------------------------------------------------------------

SELECT  'INICIO POSTVENTA',now();

#Seleccionar máxima fecha
set @max_day=cast((select max(fecha) from bi_ops_cc_adherencia_sac)  as date);
set time_zone='-5:00';

#Borrar datos del último día
delete from bi_ops_cc_adherencia_sac
where fecha>=@max_day;

#Insertar datos de los asesores
insert into bi_ops_cc_adherencia_sac (cedula, nombre, patron_de_turno, coordinador, fecha)
select b.*, a.date
from
(select identification, nombre, shift_pattern, coordinator from CS_dyalogo.dy_agentes a
inner join bi_ops_matrix_sac b on CAST(a.identificacion AS UNSIGNED)=cast(b.identification as unsigned)
where process like 'SAC%'
or process like '%BACK%'
or process like '%Facebook%'
or process like '%CHAT%'
or process like '%Mercado%'
and status='ACTIVO') b
join 
(select distinct(event_date) date from bi_ops_cc_dyalogo
order by event_date asc) a
where a.date>=@max_day;

#Insertar de conexión y desconexión
update bi_ops_cc_adherencia_sac a
inner join (SELECT 
agente_documento_id, campana_nombre_dyalogo, date(fecha_hora_inicio) fecha2, min(fecha_hora_inicio) hora_inicio, max(fecha_hora_fin) hora_fin
FROM 
CS_dyalogo.dy_v_historico_sesiones_por_campana 
group by date(fecha_hora_inicio), agente_documento_id) b
on cast(a.cedula as unsigned)=cast(b.agente_documento_id as unsigned) and a.fecha=b.fecha2
set 
hora_conexion=hora_inicio,
hora_desconexion=hora_fin,
campana_dyalogo=campana_nombre_dyalogo
where fecha>=@max_day;

#Insertar tiempo total de conexión
update customer_service_co.bi_ops_cc_adherencia_sac
set
tiempo_conexion_segundos=time_to_sec(TIMEDIFF(hora_desconexion, hora_conexion))
where fecha>=@max_day;

#Insertar descansos
update bi_ops_cc_adherencia_sac a
inner join (SELECT date(fecha_hora_inicio) fecha2, identificacion, sum(duracion) duracion FROM CS_dyalogo.dy_descansos a 
inner join CS_dyalogo.dy_tipos_descanso b
on a.id_tipo_descanso=b.id
inner join CS_dyalogo.dy_agentes c
on a.id_agente=c.id
where tipo='Break'
group by date(fecha_hora_inicio), identificacion) b
on cast(a.cedula as unsigned)=cast(b.identificacion as unsigned) and a.fecha=b.fecha2
set
total_break_segundos=duracion
where fecha>=@max_day;

update bi_ops_cc_adherencia_sac a
inner join (SELECT date(fecha_hora_inicio) fecha2, identificacion, sum(duracion) duracion FROM CS_dyalogo.dy_descansos a 
inner join CS_dyalogo.dy_tipos_descanso b
on a.id_tipo_descanso=b.id
inner join CS_dyalogo.dy_agentes c
on a.id_agente=c.id
where tipo='Bano'
group by date(fecha_hora_inicio), identificacion) b
on cast(a.cedula as unsigned)=cast(b.identificacion as unsigned) and a.fecha=b.fecha2
set
total_baño_segundos=duracion
where fecha>=@max_day;


update bi_ops_cc_adherencia_sac a
inner join (SELECT date(fecha_hora_inicio) fecha2, identificacion, sum(duracion) duracion FROM CS_dyalogo.dy_descansos a 
inner join CS_dyalogo.dy_tipos_descanso b
on a.id_tipo_descanso=b.id
inner join CS_dyalogo.dy_agentes c
on a.id_agente=c.id
where tipo='Almuerzo'
group by date(fecha_hora_inicio), identificacion) b
on cast(a.cedula as unsigned)=cast(b.identificacion as unsigned) and a.fecha=b.fecha2
set
total_almuerzo_segundos=duracion
where fecha>=@max_day;

update bi_ops_cc_adherencia_sac a
inner join (SELECT date(fecha_hora_inicio) fecha2, identificacion, sum(duracion) duracion FROM CS_dyalogo.dy_descansos a 
inner join CS_dyalogo.dy_tipos_descanso b
on a.id_tipo_descanso=b.id
inner join CS_dyalogo.dy_agentes c
on a.id_agente=c.id
where tipo='Capacitacion'
group by date(fecha_hora_inicio), identificacion) b
on cast(a.cedula as unsigned)=cast(b.identificacion as unsigned) and a.fecha=b.fecha2
set
total_capacitacion_segundos=duracion
where fecha>=@max_day;

update bi_ops_cc_adherencia_sac a
inner join (SELECT date(fecha_hora_inicio) fecha2, identificacion, sum(duracion) duracion FROM CS_dyalogo.dy_descansos a 
inner join CS_dyalogo.dy_tipos_descanso b
on a.id_tipo_descanso=b.id
inner join CS_dyalogo.dy_agentes c
on a.id_agente=c.id
where tipo='Feedback'
group by date(fecha_hora_inicio), identificacion) b
on cast(a.cedula as unsigned)=cast(b.identificacion as unsigned) and a.fecha=b.fecha2
set
total_feedback_segundos=duracion
where fecha>=@max_day;


update bi_ops_cc_adherencia_sac a
inner join (SELECT date(fecha_hora_inicio) fecha2, identificacion, sum(duracion) duracion FROM CS_dyalogo.dy_descansos a 
inner join CS_dyalogo.dy_tipos_descanso b
on a.id_tipo_descanso=b.id
inner join CS_dyalogo.dy_agentes c
on a.id_agente=c.id
where (tipo='Pausas activas' or tipo='Otros procesos')
group by date(fecha_hora_inicio), identificacion) b
on cast(a.cedula as unsigned)=cast(b.identificacion as unsigned) and a.fecha=b.fecha2
set
total_otros_procesos_segundos=duracion
where fecha>=@max_day;

#Actualizar tiempo efectivo
update bi_ops_cc_adherencia_sac
set
 total_tiempo_efectivo_segundos=tiempo_conexion_segundos-
(total_break_segundos+
total_baño_segundos+
total_capacitacion_segundos+
total_almuerzo_segundos+
total_feedback_segundos+
total_otros_procesos_segundos)
where fecha>=@max_day;

#Actualizar tiempos de conexión
update bi_ops_cc_adherencia_sac
set
tiempo_horario_segundos=tiempo_conexion_segundos
where novedad in ('Descanso','Incapacidad','Calamidad domestica','Lactancia','Enfermeria','Permiso','Tecnologia')
and fecha>=date_sub(@max_day, interval 7 day);

#Actualizar inasistencias y retrasos
update bi_ops_cc_adherencia_sac
set 
inasistencia=1
where hora_inicio_horario is not null and
tiempo_conexion_segundos is null
and fecha>=date_sub(@max_day, interval 7 day);

update bi_ops_cc_adherencia_sac a
set retraso=1
where time_to_sec(addtime(hora_conexion, -hora_inicio_horario))>120
and fecha>=date_sub(@max_day, interval 7 day);

SELECT 'Actualiza Semana';
UPDATE  bi_ops_cc_adherencia_sac SET Semana=date_format(fecha,'%Y-%u');

#------------------------------------------------------------------------------
#PREVENTA
#------------------------------------------------------------------------------

SELECT  'INICIO PREVENTA',now();

#Seleccionar máxima fecha
set @max_day=cast((select max(fecha) from bi_ops_cc_adherencia_ventas)  as date);
set time_zone='-5:00';

#Borrar datos del último día
delete from bi_ops_cc_adherencia_ventas
where fecha>=@max_day;

#Insertar datos de los asesores
insert into bi_ops_cc_adherencia_ventas (cedula, nombre, patron_de_turno, coordinador, fecha)
select b.*, a.date
from
(select identification, nombre, shift_pattern, coordinator from CS_dyalogo.dy_agentes a
inner join bi_ops_matrix_ventas b on CAST(a.identificacion AS UNSIGNED)=cast(b.identification as unsigned)
where process like 'VENTAS%'
or process like '%SAC%'
and status='ACTIVO') b
join 
(select distinct(event_date) date from bi_ops_cc_dyalogo
order by event_date asc) a
where a.date>=@max_day;

#Insertar de conexión y desconexión
update bi_ops_cc_adherencia_ventas a
inner join (SELECT 
agente_documento_id, campana_nombre_dyalogo, date(fecha_hora_inicio) fecha2, min(fecha_hora_inicio) hora_inicio, max(fecha_hora_fin) hora_fin
FROM 
CS_dyalogo.dy_v_historico_sesiones_por_campana 
group by date(fecha_hora_inicio), agente_documento_id) b
on cast(a.cedula as unsigned)=cast(b.agente_documento_id as unsigned) and a.fecha=b.fecha2
set 
hora_conexion=hora_inicio,
hora_desconexion=hora_fin,
campana_dyalogo=campana_nombre_dyalogo
where fecha>=@max_day;

#Insertar tiempo total de conexión
update bi_ops_cc_adherencia_ventas
set
tiempo_conexion_segundos=time_to_sec(TIMEDIFF(hora_desconexion, hora_conexion))
where fecha>=@max_day;

#Insertar descansos
update bi_ops_cc_adherencia_ventas a
inner join (SELECT date(fecha_hora_inicio) fecha2, identificacion, sum(duracion) duracion FROM CS_dyalogo.dy_descansos a 
inner join CS_dyalogo.dy_tipos_descanso b
on a.id_tipo_descanso=b.id
inner join CS_dyalogo.dy_agentes c
on a.id_agente=c.id
where tipo='Break'
group by date(fecha_hora_inicio), identificacion) b
on cast(a.cedula as unsigned)=cast(b.identificacion as unsigned) and a.fecha=b.fecha2
set
total_break_segundos=duracion
where fecha>=@max_day;

update bi_ops_cc_adherencia_ventas a
inner join (SELECT date(fecha_hora_inicio) fecha2, identificacion, sum(duracion) duracion FROM CS_dyalogo.dy_descansos a 
inner join CS_dyalogo.dy_tipos_descanso b
on a.id_tipo_descanso=b.id
inner join CS_dyalogo.dy_agentes c
on a.id_agente=c.id
where tipo='Bano'
group by date(fecha_hora_inicio), identificacion) b
on cast(a.cedula as unsigned)=cast(b.identificacion as unsigned) and a.fecha=b.fecha2
set
total_baño_segundos=duracion
where fecha>=@max_day;


update bi_ops_cc_adherencia_ventas a
inner join (SELECT date(fecha_hora_inicio) fecha2, identificacion, sum(duracion) duracion FROM CS_dyalogo.dy_descansos a 
inner join CS_dyalogo.dy_tipos_descanso b
on a.id_tipo_descanso=b.id
inner join CS_dyalogo.dy_agentes c
on a.id_agente=c.id
where tipo='Almuerzo'
group by date(fecha_hora_inicio), identificacion) b
on cast(a.cedula as unsigned)=cast(b.identificacion as unsigned) and a.fecha=b.fecha2
set
total_almuerzo_segundos=duracion
where fecha>=@max_day;

update bi_ops_cc_adherencia_ventas a
inner join (SELECT date(fecha_hora_inicio) fecha2, identificacion, sum(duracion) duracion FROM CS_dyalogo.dy_descansos a 
inner join CS_dyalogo.dy_tipos_descanso b
on a.id_tipo_descanso=b.id
inner join CS_dyalogo.dy_agentes c
on a.id_agente=c.id
where tipo='Capacitacion'
group by date(fecha_hora_inicio), identificacion) b
on cast(a.cedula as unsigned)=cast(b.identificacion as unsigned) and a.fecha=b.fecha2
set
total_capacitacion_segundos=duracion
where fecha>=@max_day;

update bi_ops_cc_adherencia_ventas a
inner join (SELECT date(fecha_hora_inicio) fecha2, identificacion, sum(duracion) duracion FROM CS_dyalogo.dy_descansos a 
inner join CS_dyalogo.dy_tipos_descanso b
on a.id_tipo_descanso=b.id
inner join CS_dyalogo.dy_agentes c
on a.id_agente=c.id
where tipo='Feedback'
group by date(fecha_hora_inicio), identificacion) b
on cast(a.cedula as unsigned)=cast(b.identificacion as unsigned) and a.fecha=b.fecha2
set
total_feedback_segundos=duracion
where fecha>=@max_day;


update bi_ops_cc_adherencia_ventas a
inner join (SELECT date(fecha_hora_inicio) fecha2, identificacion, sum(duracion) duracion FROM CS_dyalogo.dy_descansos a 
inner join CS_dyalogo.dy_tipos_descanso b
on a.id_tipo_descanso=b.id
inner join CS_dyalogo.dy_agentes c
on a.id_agente=c.id
where (tipo='Pausas activas' or tipo='Otros procesos')
group by date(fecha_hora_inicio), identificacion) b
on cast(a.cedula as unsigned)=cast(b.identificacion as unsigned) and a.fecha=b.fecha2
set
total_otros_procesos_segundos=duracion
where fecha>=@max_day;

#Actualizar tiempo efectivo
update bi_ops_cc_adherencia_ventas
set
 total_tiempo_efectivo_segundos=tiempo_conexion_segundos-
(total_break_segundos+total_break_segundos+
total_baño_segundos+
total_capacitacion_segundos+
total_almuerzo_segundos+
total_feedback_segundos+
total_otros_procesos_segundos)
where fecha>=@max_day;

#Actualizar tiempos de conexión
update bi_ops_cc_adherencia_ventas
set
tiempo_horario_segundos=tiempo_conexion_segundos
where novedad in ('Descanso','Incapacidad','Calamidad domestica','Lactancia','Enfermeria','Permiso','Tecnologia')
and fecha>=date_sub(@max_day, interval 7 day);

#Actualizar inasistencias y retrasos
update bi_ops_cc_adherencia_ventas
set 
inasistencia=1
where hora_inicio_horario is not null and
tiempo_conexion_segundos is null
and fecha>=date_sub(@max_day, interval 7 day);

update bi_ops_cc_adherencia_ventas a
set retraso=1
where time_to_sec(addtime(hora_conexion, -hora_inicio_horario))>120
and fecha>=date_sub(@max_day, interval 7 day);

SELECT 'Actualiza Semana';
UPDATE  bi_ops_cc_adherencia_ventas SET Semana=date_format(fecha,'%Y-%u');



END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:03:38
