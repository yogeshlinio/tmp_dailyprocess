-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: SEM
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'SEM'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`root`@`localhost`*/ /*!50003 FUNCTION `week_iso`(edate date) RETURNS varchar(20) CHARSET latin1
begin
 declare sdate varchar(20);
 if year(edate) = 2012 then set sdate = concat(year(edate), '-',(week(edate, 5))+1); else set sdate = date_format(edate, "%x-%v"); end if;
      return sdate;
end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `google_optimization_ad_group_co`()
begin

set @days:=90;

call SEM.sem_filter_co;

delete from SEM.google_optimization_ad_group_co where datediff(curdate(), date)<@days;

insert into SEM.google_optimization_ad_group_co(date, channel, campaign, ad_group, impressions, clicks, visits, bounce, cart, cost) select date, channel, campaign, ad_group, sum(impressions), sum(clicks), sum(visits), sum(bounce), sum(cart), sum(ad_cost) from SEM.sem_filter_campaign_ad_group_co where datediff(curdate(), date)<@days group by channel, date, campaign, ad_group;

insert into SEM.google_optimization_ad_group_co(date, channel, campaign, ad_group, impressions, clicks, visits, bounce, cart, cost) select date, channel, campaign, ad_group, sum(impressions), sum(clicks), sum(visits), sum(bounce), sum(cart), sum(ad_cost) from SEM.sem_filter_campaign_ad_group_fashion_co where datediff(curdate(), date)<@days group by channel, date, campaign, ad_group;

update SEM.google_optimization_ad_group_co set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_co set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=SEM.week_iso(date) where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_co x set gross_transactions =  (select count(distinct transaction_id) from SEM.sem_filter_transaction_id_co s, production_co.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_co x set net_transactions =  (select count(distinct transaction_id) from SEM.sem_filter_transaction_id_co s, production_co.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_co x set gross_revenue =  (select sum(t.paid_price) from SEM.sem_filter_transaction_id_co s, production_co.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_co x set net_revenue =  (select sum(t.paid_price) from SEM.sem_filter_transaction_id_co s, production_co.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_co x set new_customers =  (select sum(t.new_customers) from SEM.sem_filter_transaction_id_co s, production_co.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_co x set new_customers_gross =  (select sum(t.new_customers_gross) from SEM.sem_filter_transaction_id_co s, production_co.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_co x set category = substring_index(campaign, '.',1) where datediff(curdate(), date)<@days;

-- cohort

create table SEM.temporary_co(
date date,
campaign varchar(255),
ad_group varchar(255),
custid int,
transaction_id varchar(255)
);

create index temporary on SEM.temporary_co(date, ad_group, campaign);
create index transaction_id on SEM.temporary_co(transaction_id);

insert into SEM.temporary_co select distinct x.date, x.campaign, x.ad_group, x.customer_id, x.transaction_id from SEM.google_optimization_transaction_id_co x, production_co.tbl_order_detail t where x.transaction_id=t.order_nr and t.new_customers !=0 and t.oac=1 and datediff(curdate(), x.date)<@days;

insert into SEM.temporary_co select distinct x.date, x.campaign, x.ad_group, x.customer_id, x.transaction_id from SEM.google_optimization_transaction_id_fashion_co x, production_co.tbl_order_detail t where x.transaction_id=t.order_nr and t.new_customers !=0 and t.oac=1 and datediff(curdate(), x.date)<@days;

update SEM.google_optimization_ad_group_co a set net_transactions_30 = (select count(distinct t.order_nr) from production_co.tbl_order_detail t, SEM.temporary_co x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_co a set net_revenue_30 = (select sum(paid_price) from production_co.tbl_order_detail t, SEM.temporary_co x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_co a set net_transactions_60 = (select count(distinct t.order_nr) from production_co.tbl_order_detail t, SEM.temporary_co x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_co a set net_revenue_60 = (select sum(paid_price) from production_co.tbl_order_detail t, SEM.temporary_co x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_co a set net_transactions_90 = (select count(distinct t.order_nr) from production_co.tbl_order_detail t, SEM.temporary_co x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_co a set net_revenue_90 = (select sum(paid_price) from production_co.tbl_order_detail t, SEM.temporary_co x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_co a set net_transactions_120 = (select count(distinct t.order_nr) from production_co.tbl_order_detail t, SEM.temporary_co x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_co a set net_revenue_120 = (select sum(paid_price) from production_co.tbl_order_detail t, SEM.temporary_co x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_co a set net_transactions_150 = (select count(distinct t.order_nr) from production_co.tbl_order_detail t, SEM.temporary_co x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_co a set net_revenue_150 = (select sum(paid_price) from production_co.tbl_order_detail t, SEM.temporary_co x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_co a set net_transactions_180 = (select count(distinct t.order_nr) from production_co.tbl_order_detail t, SEM.temporary_co x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_co a set net_revenue_180 = (select sum(paid_price) from production_co.tbl_order_detail t, SEM.temporary_co x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days; 

drop table SEM.temporary_co;

update SEM.google_optimization_ad_group_co set net_revenue_30=0 where net_revenue_30 is null;

update SEM.google_optimization_ad_group_co set net_revenue_60=0 where net_revenue_60 is null;

update SEM.google_optimization_ad_group_co set net_revenue_90=0 where net_revenue_90 is null;

update SEM.google_optimization_ad_group_co set net_revenue_120=0 where net_revenue_120 is null;

update SEM.google_optimization_ad_group_co set net_revenue_150=0 where net_revenue_150 is null;

update SEM.google_optimization_ad_group_co set net_revenue_180=0 where net_revenue_180 is null;

-- cohort

update SEM.google_optimization_ad_group_co s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, net_revenue_30=net_revenue_30/xr, net_revenue_60=net_revenue_60/xr, net_revenue_90=net_revenue_90/xr, net_revenue_120=net_revenue_120/xr, net_revenue_150=net_revenue_150/xr, net_revenue_180=net_revenue_180/xr where r.country='COL' and datediff(curdate(), s.date)<@days;

update SEM.google_optimization_ad_group_co set cost=cost/2300 where yrmonth=201205 and datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_co set cost=cost/2230 where yrmonth=201206 and datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_co set cost=cost/2200 where yrmonth between 201207 and 201302 and datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_co set cost=cost/16.35 where yrmonth between 201303 and 201310 and datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_co set cost=cost/2450 where yrmonth>= 201311 and datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_co set CPC=cost/clicks, CPO=cost/net_transactions, CTR=clicks/impressions, CAC=cost/new_customers, CIR=cost/net_revenue, checkout_rate=cart/visits, conversion_rate=net_transactions/clicks, bounce_rate=bounce/clicks, transactions_to_cart=net_transactions/cart where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_co set gross_CPO=cost/gross_transactions, gross_CAC=cost/new_customers_gross, gross_CIR=cost/gross_revenue, gross_conversion_rate=gross_transactions/clicks, gross_transactions_to_cart=gross_transactions/cart where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_co set gross_revenue=0 where gross_revenue is null;

update SEM.google_optimization_ad_group_co set net_revenue=0 where net_revenue is null;

update SEM.google_optimization_ad_group_co set new_customers=0 where new_customers is null;

update SEM.google_optimization_ad_group_co set new_customers_gross=0 where new_customers_gross is null;

update SEM.google_optimization_ad_group_co set CPC=0 where CPC is null;

update SEM.google_optimization_ad_group_co set CPO=0 where CPO is null;

update SEM.google_optimization_ad_group_co set CTR=0 where CTR is null;

update SEM.google_optimization_ad_group_co set CAC=0 where CAC is null;

update SEM.google_optimization_ad_group_co set CIR=0 where CIR is null;

update SEM.google_optimization_ad_group_co set checkout_rate=0 where checkout_rate is null;

update SEM.google_optimization_ad_group_co set conversion_rate=0 where conversion_rate is null;

update SEM.google_optimization_ad_group_co set bounce_rate=0 where bounce_rate is null;

update SEM.google_optimization_ad_group_co set transactions_to_cart=0 where transactions_to_cart is null;

update SEM.google_optimization_ad_group_co set gross_CPO=0 where gross_CPO is null;

update SEM.google_optimization_ad_group_co set gross_CAC=0 where gross_CAC is null;

update SEM.google_optimization_ad_group_co set gross_CIR=0 where gross_CIR is null;

update SEM.google_optimization_ad_group_co set gross_conversion_rate=0 where gross_conversion_rate is null;

update SEM.google_optimization_ad_group_co set gross_transactions_to_cart=0 where gross_transactions_to_cart is null;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `google_optimization_ad_group_mx`()
begin

set @days:=90;

call SEM.sem_filter_mx;

delete from SEM.google_optimization_ad_group_mx where datediff(curdate(), date)<@days;

insert into SEM.google_optimization_ad_group_mx(date, channel, campaign, ad_group, impressions, clicks, visits, bounce, cart, cost) select date, channel, campaign, ad_group, sum(impressions), sum(clicks), sum(visits), sum(bounce), sum(cart), sum(ad_cost) from SEM.sem_filter_campaign_ad_group_mx where datediff(curdate(), date)<@days group by channel, date, campaign, ad_group;

update SEM.google_optimization_ad_group_mx set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_mx set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=SEM.week_iso(date) where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_mx x set gross_transactions =  (select count(distinct transaction_id) from SEM.sem_filter_transaction_id_mx s, production.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_mx x set net_transactions =  (select count(distinct transaction_id) from SEM.sem_filter_transaction_id_mx s, production.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_mx x set gross_revenue =  (select sum(t.actual_paid_price) from SEM.sem_filter_transaction_id_mx s, production.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_mx x set net_revenue =  (select sum(t.actual_paid_price) from SEM.sem_filter_transaction_id_mx s, production.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_mx x set new_customers =  (select sum(t.new_customers) from SEM.sem_filter_transaction_id_mx s, production.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_mx x set new_customers_gross =  (select sum(t.new_customers_gross) from SEM.sem_filter_transaction_id_mx s, production.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_mx x set category = substring_index(campaign, '.',1) where datediff(curdate(), date)<@days;

-- cohort

create table SEM.temporary_mx(
date date,
campaign varchar(255),
ad_group varchar(255),
custid int,
transaction_id varchar(255)
);

create index temporary on SEM.temporary_mx(date, ad_group, campaign);
create index transaction_id on SEM.temporary_mx(transaction_id);

insert into SEM.temporary_mx select distinct x.date, x.campaign, x.ad_group, x.customer_id, x.transaction_id from SEM.google_optimization_transaction_id_mx x, production.tbl_order_detail t where x.transaction_id=t.order_nr and t.new_customers is not null and t.oac=1 and datediff(curdate(), x.date)<@days;

update SEM.google_optimization_ad_group_mx a set net_transactions_30 = (select count(distinct t.order_nr) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_mx a set net_revenue_30 = (select sum(actual_paid_price) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_mx a set net_transactions_60 = (select count(distinct t.order_nr) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_mx a set net_revenue_60 = (select sum(actual_paid_price) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_mx a set net_transactions_90 = (select count(distinct t.order_nr) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_mx a set net_revenue_90 = (select sum(actual_paid_price) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_mx a set net_transactions_120 = (select count(distinct t.order_nr) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_mx a set net_revenue_120 = (select sum(actual_paid_price) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_mx a set net_transactions_150 = (select count(distinct t.order_nr) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_mx a set net_revenue_150 = (select sum(actual_paid_price) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_mx a set net_transactions_180 = (select count(distinct t.order_nr) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_mx a set net_revenue_180 = (select sum(actual_paid_price) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

drop table SEM.temporary_mx;

update SEM.google_optimization_ad_group_mx set net_revenue_30=0 where net_revenue_30 is null;

update SEM.google_optimization_ad_group_mx set net_revenue_60=0 where net_revenue_60 is null;

update SEM.google_optimization_ad_group_mx set net_revenue_90=0 where net_revenue_90 is null;

update SEM.google_optimization_ad_group_mx set net_revenue_120=0 where net_revenue_120 is null;

update SEM.google_optimization_ad_group_mx set net_revenue_150=0 where net_revenue_150 is null;

update SEM.google_optimization_ad_group_mx set net_revenue_180=0 where net_revenue_180 is null;

-- cohort

update SEM.google_optimization_ad_group_mx s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, net_revenue_30=net_revenue_30/xr, net_revenue_60=net_revenue_60/xr, net_revenue_90=net_revenue_90/xr, net_revenue_120=net_revenue_120/xr, net_revenue_150=net_revenue_150/xr, net_revenue_180=net_revenue_180/xr, cost=cost/xr where r.country='MEX' and datediff(curdate(), s.date)<@days;

update SEM.google_optimization_ad_group_mx set CPC=cost/clicks, CPO=cost/net_transactions, CTR=clicks/impressions, CAC=cost/new_customers, CIR=cost/net_revenue, checkout_rate=cart/visits, conversion_rate=net_transactions/clicks, bounce_rate=bounce/clicks, transactions_to_cart=net_transactions/cart where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_mx set gross_CPO=cost/gross_transactions, gross_CAC=cost/new_customers_gross, gross_CIR=cost/gross_revenue, gross_conversion_rate=gross_transactions/clicks, gross_transactions_to_cart=gross_transactions/cart where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_mx set gross_revenue=0 where gross_revenue is null;

update SEM.google_optimization_ad_group_mx set net_revenue=0 where net_revenue is null;

update SEM.google_optimization_ad_group_mx set new_customers=0 where new_customers is null;

update SEM.google_optimization_ad_group_mx set new_customers_gross=0 where new_customers_gross is null;

update SEM.google_optimization_ad_group_mx set CPC=0 where CPC is null;

update SEM.google_optimization_ad_group_mx set CPO=0 where CPO is null;

update SEM.google_optimization_ad_group_mx set CTR=0 where CTR is null;

update SEM.google_optimization_ad_group_mx set CAC=0 where CAC is null;

update SEM.google_optimization_ad_group_mx set CIR=0 where CIR is null;

update SEM.google_optimization_ad_group_mx set checkout_rate=0 where checkout_rate is null;

update SEM.google_optimization_ad_group_mx set conversion_rate=0 where conversion_rate is null;

update SEM.google_optimization_ad_group_mx set bounce_rate=0 where bounce_rate is null;

update SEM.google_optimization_ad_group_mx set transactions_to_cart=0 where transactions_to_cart is null;

update SEM.google_optimization_ad_group_mx set gross_CPO=0 where gross_CPO is null;

update SEM.google_optimization_ad_group_mx set gross_CAC=0 where gross_CAC is null;

update SEM.google_optimization_ad_group_mx set gross_CIR=0 where gross_CIR is null;

update SEM.google_optimization_ad_group_mx set gross_conversion_rate=0 where gross_conversion_rate is null;

update SEM.google_optimization_ad_group_mx set gross_transactions_to_cart=0 where gross_transactions_to_cart is null;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `google_optimization_ad_group_pe`()
begin

set @days:=90;

call SEM.sem_filter_pe;

delete from SEM.google_optimization_ad_group_pe where datediff(curdate(), date)<@days;

insert into SEM.google_optimization_ad_group_pe(date, channel, campaign, ad_group, impressions, clicks, visits, bounce, cart, cost) select date, channel, campaign, ad_group, sum(impressions), sum(clicks), sum(visits), sum(bounce), sum(cart), sum(ad_cost) from SEM.sem_filter_campaign_ad_group_pe where datediff(curdate(), date)<@days group by channel, date, campaign, ad_group;

update SEM.google_optimization_ad_group_pe set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_pe set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=SEM.week_iso(date) where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_pe x set gross_transactions =  (select count(distinct transaction_id) from SEM.sem_filter_transaction_id_pe s, production_pe.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe x set net_transactions =  (select count(distinct transaction_id) from SEM.sem_filter_transaction_id_pe s, production_pe.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe x set gross_revenue =  (select sum(t.paid_price) from SEM.sem_filter_transaction_id_pe s, production_pe.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe x set net_revenue =  (select sum(t.paid_price) from SEM.sem_filter_transaction_id_pe s, production_pe.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe x set new_customers =  (select sum(t.new_customers) from SEM.sem_filter_transaction_id_pe s, production_pe.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe x set new_customers_gross =  (select sum(t.new_customers_gross) from SEM.sem_filter_transaction_id_pe s, production_pe.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe x set category = substring_index(campaign, '.',1) where datediff(curdate(), date)<@days;

-- cohort

create table SEM.temporary_pe(
date date,
campaign varchar(255),
ad_group varchar(255),
custid int,
transaction_id varchar(255)
);

create index temporary on SEM.temporary_pe(date, ad_group, campaign);
create index transaction_id on SEM.temporary_pe(transaction_id);

insert into SEM.temporary_pe select distinct x.date, x.campaign, x.ad_group, x.customer_id, x.transaction_id from SEM.google_optimization_transaction_id_pe x, production_pe.tbl_order_detail t where x.transaction_id=t.order_nr and t.new_customers is not null and t.oac=1 and datediff(curdate(), x.date)<@days;

update SEM.google_optimization_ad_group_pe a set net_transactions_30 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe a set net_revenue_30 = (select sum(paid_price) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe a set net_transactions_60 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe a set net_revenue_60 = (select sum(paid_price) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe a set net_transactions_90 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe a set net_revenue_90 = (select sum(paid_price) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe a set net_transactions_120 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe a set net_revenue_120 = (select sum(paid_price) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe a set net_transactions_150 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe a set net_revenue_150 = (select sum(paid_price) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_pe a set net_transactions_180 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_pe a set net_revenue_180 = (select sum(paid_price) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

drop table SEM.temporary_pe;

update SEM.google_optimization_ad_group_pe set net_revenue_30=0 where net_revenue_30 is null;

update SEM.google_optimization_ad_group_pe set net_revenue_60=0 where net_revenue_60 is null;

update SEM.google_optimization_ad_group_pe set net_revenue_90=0 where net_revenue_90 is null;

update SEM.google_optimization_ad_group_pe set net_revenue_120=0 where net_revenue_120 is null;

update SEM.google_optimization_ad_group_pe set net_revenue_150=0 where net_revenue_150 is null;

update SEM.google_optimization_ad_group_pe set net_revenue_180=0 where net_revenue_180 is null;

-- cohort

update SEM.google_optimization_ad_group_pe s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, net_revenue_30=net_revenue_30/xr, net_revenue_60=net_revenue_60/xr, net_revenue_90=net_revenue_90/xr, net_revenue_120=net_revenue_120/xr, net_revenue_150=net_revenue_150/xr, net_revenue_180=net_revenue_180/xr where r.country='PER' and datediff(curdate(), s.date)<@days;

update SEM.google_optimization_ad_group_pe set cost=cost/1.23 where yrmonth<=201302 and datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_pe set cost=cost/16.35 where yrmonth between 201303 and 201311 and datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_pe set cost=cost/1.23 where yrmonth >= 201312 and datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_pe set CPC=cost/clicks, CPO=cost/net_transactions, CTR=clicks/impressions, CAC=cost/new_customers, CIR=cost/net_revenue, checkout_rate=cart/visits, conversion_rate=net_transactions/clicks, bounce_rate=bounce/clicks, transactions_to_cart=net_transactions/cart where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_pe set gross_CPO=cost/gross_transactions, gross_CAC=cost/new_customers_gross, gross_CIR=cost/gross_revenue, gross_conversion_rate=gross_transactions/clicks, gross_transactions_to_cart=gross_transactions/cart where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_pe set gross_revenue=0 where gross_revenue is null;

update SEM.google_optimization_ad_group_pe set net_revenue=0 where net_revenue is null;

update SEM.google_optimization_ad_group_pe set new_customers=0 where new_customers is null;

update SEM.google_optimization_ad_group_pe set new_customers_gross=0 where new_customers_gross is null;

update SEM.google_optimization_ad_group_pe set CPC=0 where CPC is null;

update SEM.google_optimization_ad_group_pe set CPO=0 where CPO is null;

update SEM.google_optimization_ad_group_pe set CTR=0 where CTR is null;

update SEM.google_optimization_ad_group_pe set CAC=0 where CAC is null;

update SEM.google_optimization_ad_group_pe set CIR=0 where CIR is null;

update SEM.google_optimization_ad_group_pe set checkout_rate=0 where checkout_rate is null;

update SEM.google_optimization_ad_group_pe set conversion_rate=0 where conversion_rate is null;

update SEM.google_optimization_ad_group_pe set bounce_rate=0 where bounce_rate is null;

update SEM.google_optimization_ad_group_pe set transactions_to_cart=0 where transactions_to_cart is null;

update SEM.google_optimization_ad_group_pe set gross_CPO=0 where gross_CPO is null;

update SEM.google_optimization_ad_group_pe set gross_CAC=0 where gross_CAC is null;

update SEM.google_optimization_ad_group_pe set gross_CIR=0 where gross_CIR is null;

update SEM.google_optimization_ad_group_pe set gross_conversion_rate=0 where gross_conversion_rate is null;

update SEM.google_optimization_ad_group_pe set gross_transactions_to_cart=0 where gross_transactions_to_cart is null;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `google_optimization_ad_group_ve`()
begin

set @days:=90;

call SEM.sem_filter_ve;

delete from SEM.google_optimization_ad_group_ve where datediff(curdate(), date)<@days;

insert into SEM.google_optimization_ad_group_ve(date, channel, campaign, ad_group, impressions, clicks, visits, bounce, cart, cost) select date, channel, campaign, ad_group, sum(impressions), sum(clicks), sum(visits), sum(bounce), sum(cart), sum(ad_cost) from SEM.sem_filter_campaign_ad_group_ve where datediff(curdate(), date)<@days group by channel, date, campaign, ad_group;

update SEM.google_optimization_ad_group_ve set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_ve set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=SEM.week_iso(date) where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_ve x set gross_transactions =  (select count(distinct transaction_id) from SEM.sem_filter_transaction_id_ve s, production_ve.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_ve x set net_transactions =  (select count(distinct transaction_id) from SEM.sem_filter_transaction_id_ve s, production_ve.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_ve x set gross_revenue =  (select sum(t.paid_price) from SEM.sem_filter_transaction_id_ve s, production_ve.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_ve x set net_revenue =  (select sum(t.paid_price) from SEM.sem_filter_transaction_id_ve s, production_ve.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_ve x set new_customers =  (select sum(t.new_customers) from SEM.sem_filter_transaction_id_ve s, production_ve.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_ve x set new_customers_gross =  (select sum(t.new_customers_gross) from SEM.sem_filter_transaction_id_ve s, production_ve.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_ve x set category = substring_index(campaign, '.',1) where datediff(curdate(), date)<@days;

-- cohort

create table SEM.temporary_ve(
date date,
campaign varchar(255),
ad_group varchar(255),
custid int,
transaction_id varchar(255)
);

create index temporary on SEM.temporary_ve(date, ad_group, campaign);
create index transaction_id on SEM.temporary_ve(transaction_id);

insert into SEM.temporary_ve select distinct x.date, x.campaign, x.ad_group, x.customer_id, x.transaction_id from SEM.google_optimization_transaction_id_ve x, production_ve.tbl_order_detail t where x.transaction_id=t.order_nr and t.new_customers is not null and t.oac=1 and datediff(curdate(), x.date)<@days;

update SEM.google_optimization_ad_group_ve a set net_transactions_30 = (select count(distinct t.order_nr) from production_ve.tbl_order_detail t, SEM.temporary_ve x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_ve a set net_revenue_30 = (select sum(paid_price) from production_ve.tbl_order_detail t, SEM.temporary_ve x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_ve a set net_transactions_60 = (select count(distinct t.order_nr) from production_ve.tbl_order_detail t, SEM.temporary_ve x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_ve a set net_revenue_60 = (select sum(paid_price) from production_ve.tbl_order_detail t, SEM.temporary_ve x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_ve a set net_transactions_90 = (select count(distinct t.order_nr) from production_ve.tbl_order_detail t, SEM.temporary_ve x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_ve a set net_revenue_90 = (select sum(paid_price) from production_ve.tbl_order_detail t, SEM.temporary_ve x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_ve a set net_transactions_120 = (select count(distinct t.order_nr) from production_ve.tbl_order_detail t, SEM.temporary_ve x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_ve a set net_revenue_120 = (select sum(paid_price) from production_ve.tbl_order_detail t, SEM.temporary_ve x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_ve a set net_transactions_150 = (select count(distinct t.order_nr) from production_ve.tbl_order_detail t, SEM.temporary_ve x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_ve a set net_revenue_150 = (select sum(paid_price) from production_ve.tbl_order_detail t, SEM.temporary_ve x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_ve a set net_transactions_180 = (select count(distinct t.order_nr) from production_ve.tbl_order_detail t, SEM.temporary_ve x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_ad_group_ve a set net_revenue_180 = (select sum(paid_price) from production_ve.tbl_order_detail t, SEM.temporary_ve x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

drop table SEM.temporary_ve;

update SEM.google_optimization_ad_group_ve set net_revenue_30=0 where net_revenue_30 is null;

update SEM.google_optimization_ad_group_ve set net_revenue_60=0 where net_revenue_60 is null;

update SEM.google_optimization_ad_group_ve set net_revenue_90=0 where net_revenue_90 is null;

update SEM.google_optimization_ad_group_ve set net_revenue_120=0 where net_revenue_120 is null;

update SEM.google_optimization_ad_group_ve set net_revenue_150=0 where net_revenue_150 is null;

update SEM.google_optimization_ad_group_ve set net_revenue_180=0 where net_revenue_180 is null;

-- cohort

update SEM.google_optimization_ad_group_ve s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, net_revenue_30=net_revenue_30/xr, net_revenue_60=net_revenue_60/xr, net_revenue_90=net_revenue_90/xr, net_revenue_120=net_revenue_120/xr, net_revenue_150=net_revenue_150/xr, net_revenue_180=net_revenue_180/xr where r.country='VEN' and datediff(curdate(), s.date)<@days;

update SEM.google_optimization_ad_group_ve set cost=cost/16.35 where yrmonth<=201311 and datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_ve set cost=cost/1.23 where yrmonth>=201312 and datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_ve set CPC=cost/clicks, CPO=cost/net_transactions, CTR=clicks/impressions, CAC=cost/new_customers, CIR=cost/net_revenue, checkout_rate=cart/visits, conversion_rate=net_transactions/clicks, bounce_rate=bounce/clicks, transactions_to_cart=net_transactions/cart where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_ve set gross_CPO=cost/gross_transactions, gross_CAC=cost/new_customers_gross, gross_CIR=cost/gross_revenue, gross_conversion_rate=gross_transactions/clicks, gross_transactions_to_cart=gross_transactions/cart where datediff(curdate(), date)<@days;

update SEM.google_optimization_ad_group_ve set gross_revenue=0 where gross_revenue is null;

update SEM.google_optimization_ad_group_ve set net_revenue=0 where net_revenue is null;

update SEM.google_optimization_ad_group_ve set new_customers=0 where new_customers is null;

update SEM.google_optimization_ad_group_ve set new_customers_gross=0 where new_customers_gross is null;

update SEM.google_optimization_ad_group_ve set CPC=0 where CPC is null;

update SEM.google_optimization_ad_group_ve set CPO=0 where CPO is null;

update SEM.google_optimization_ad_group_ve set CTR=0 where CTR is null;

update SEM.google_optimization_ad_group_ve set CAC=0 where CAC is null;

update SEM.google_optimization_ad_group_ve set CIR=0 where CIR is null;

update SEM.google_optimization_ad_group_ve set checkout_rate=0 where checkout_rate is null;

update SEM.google_optimization_ad_group_ve set conversion_rate=0 where conversion_rate is null;

update SEM.google_optimization_ad_group_ve set bounce_rate=0 where bounce_rate is null;

update SEM.google_optimization_ad_group_ve set transactions_to_cart=0 where transactions_to_cart is null;

update SEM.google_optimization_ad_group_ve set gross_CPO=0 where gross_CPO is null;

update SEM.google_optimization_ad_group_ve set gross_CAC=0 where gross_CAC is null;

update SEM.google_optimization_ad_group_ve set gross_CIR=0 where gross_CIR is null;

update SEM.google_optimization_ad_group_ve set gross_conversion_rate=0 where gross_conversion_rate is null;

update SEM.google_optimization_ad_group_ve set gross_transactions_to_cart=0 where gross_transactions_to_cart is null;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `google_optimization_keyword_co`()
begin

set @days:=90;

call SEM.sem_filter_keyword_co;

delete from SEM.google_optimization_keyword_co where datediff(curdate(), date)<@days;

insert into SEM.google_optimization_keyword_co(date, channel, campaign, ad_group, keyword, impressions, clicks, visits, bounce, cart, cost) select date, channel, campaign, ad_group, keyword, sum(impressions), sum(clicks), sum(visits), sum(bounce), sum(cart), sum(ad_cost) from SEM.sem_filter_campaign_keyword_co where datediff(curdate(), date)<@days group by channel, date, campaign, ad_group, keyword;

update SEM.google_optimization_keyword_co set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_co set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=SEM.week_iso(date) where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_co x set gross_transactions =  (select count(distinct transaction_id) from SEM.sem_filter_transaction_id_keyword_co s, production_co.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_co x set net_transactions =  (select count(distinct transaction_id) from SEM.sem_filter_transaction_id_keyword_co s, production_co.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_co x set gross_revenue =  (select sum(t.paid_price) from SEM.sem_filter_transaction_id_keyword_co s, production_co.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_co x set net_revenue =  (select sum(t.paid_price) from SEM.sem_filter_transaction_id_keyword_co s, production_co.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_co x set new_customers =  (select sum(t.new_customers) from SEM.sem_filter_transaction_id_keyword_co s, production_co.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_co x set new_customers_gross =  (select sum(t.new_customers_gross) from SEM.sem_filter_transaction_id_keyword_co s, production_co.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_co x set category = substring_index(campaign, '.',1) where datediff(curdate(), date)<@days;

-- cohort

create table SEM.temporary_co(
date date,
campaign varchar(255),
ad_group varchar(255),
keyword varchar(255),
custid int,
transaction_id varchar(255)
);

create index temporary on SEM.temporary_co(date, ad_group, campaign);
create index temporary2 on SEM.temporary_co(keyword);
create index transaction_id on SEM.temporary_co(transaction_id);

insert into SEM.temporary_co select distinct x.date, x.campaign, x.ad_group, x.keyword, x.customer_id, x.transaction_id from SEM.google_optimization_transaction_id_keyword_co x, production_co.tbl_order_detail t where x.transaction_id=t.order_nr and t.new_customers !=0 and t.oac=1 and datediff(curdate(), x.date)<@days;

update SEM.google_optimization_keyword_co a set net_transactions_30 = (select count(distinct t.order_nr) from production_co.tbl_order_detail t, SEM.temporary_co x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_co a set net_revenue_30 = (select sum(paid_price) from production_co.tbl_order_detail t, SEM.temporary_co x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_co a set net_transactions_60 = (select count(distinct t.order_nr) from production_co.tbl_order_detail t, SEM.temporary_co x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_co a set net_revenue_60 = (select sum(paid_price) from production_co.tbl_order_detail t, SEM.temporary_co x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.keyword=x.keyword and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_co a set net_transactions_90 = (select count(distinct t.order_nr) from production_co.tbl_order_detail t, SEM.temporary_co x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_co a set net_revenue_90 = (select sum(paid_price) from production_co.tbl_order_detail t, SEM.temporary_co x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_co a set net_transactions_120 = (select count(distinct t.order_nr) from production_co.tbl_order_detail t, SEM.temporary_co x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_co a set net_revenue_120 = (select sum(paid_price) from production_co.tbl_order_detail t, SEM.temporary_co x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_co a set net_transactions_150 = (select count(distinct t.order_nr) from production_co.tbl_order_detail t, SEM.temporary_co x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_co a set net_revenue_150 = (select sum(paid_price) from production_co.tbl_order_detail t, SEM.temporary_co x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_co a set net_transactions_180 = (select count(distinct t.order_nr) from production_co.tbl_order_detail t, SEM.temporary_co x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_co a set net_revenue_180 = (select sum(paid_price) from production_co.tbl_order_detail t, SEM.temporary_co x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers = 0 and t.oac=1) where datediff(curdate(), date)<@days; 

drop table SEM.temporary_co;

update SEM.google_optimization_keyword_co set net_revenue_30=0 where net_revenue_30 is null;

update SEM.google_optimization_keyword_co set net_revenue_60=0 where net_revenue_60 is null;

update SEM.google_optimization_keyword_co set net_revenue_90=0 where net_revenue_90 is null;

update SEM.google_optimization_keyword_co set net_revenue_120=0 where net_revenue_120 is null;

update SEM.google_optimization_keyword_co set net_revenue_150=0 where net_revenue_150 is null;

update SEM.google_optimization_keyword_co set net_revenue_180=0 where net_revenue_180 is null;

-- cohort

update SEM.google_optimization_keyword_co s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, net_revenue_30=net_revenue_30/xr, net_revenue_60=net_revenue_60/xr, net_revenue_90=net_revenue_90/xr, net_revenue_120=net_revenue_120/xr, net_revenue_150=net_revenue_150/xr, net_revenue_180=net_revenue_180/xr where r.country='COL' and datediff(curdate(), s.date)<@days;

update SEM.google_optimization_keyword_co set cost=cost/2300 where yrmonth=201205 and datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_co set cost=cost/2230 where yrmonth=201206 and datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_co set cost=cost/2200 where yrmonth between 201207 and 201302 and datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_co set cost=cost/16.35 where yrmonth between 201303 and 201310 and datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_co set cost=cost/2450 where yrmonth>= 201311 and datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_co set CPC=cost/clicks, CPO=cost/net_transactions, CTR=clicks/impressions, CAC=cost/new_customers, CIR=cost/net_revenue, checkout_rate=cart/visits, conversion_rate=net_transactions/clicks, bounce_rate=bounce/clicks, transactions_to_cart=net_transactions/cart where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_co set gross_CPO=cost/gross_transactions, gross_CAC=cost/new_customers_gross, gross_CIR=cost/gross_revenue, gross_conversion_rate=gross_transactions/clicks, gross_transactions_to_cart=gross_transactions/cart where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_co set gross_revenue=0 where gross_revenue is null;

update SEM.google_optimization_keyword_co set net_revenue=0 where net_revenue is null;

update SEM.google_optimization_keyword_co set new_customers=0 where new_customers is null;

update SEM.google_optimization_keyword_co set new_customers_gross=0 where new_customers_gross is null;

update SEM.google_optimization_keyword_co set CPC=0 where CPC is null;

update SEM.google_optimization_keyword_co set CPO=0 where CPO is null;

update SEM.google_optimization_keyword_co set CTR=0 where CTR is null;

update SEM.google_optimization_keyword_co set CAC=0 where CAC is null;

update SEM.google_optimization_keyword_co set CIR=0 where CIR is null;

update SEM.google_optimization_keyword_co set checkout_rate=0 where checkout_rate is null;

update SEM.google_optimization_keyword_co set conversion_rate=0 where conversion_rate is null;

update SEM.google_optimization_keyword_co set bounce_rate=0 where bounce_rate is null;

update SEM.google_optimization_keyword_co set transactions_to_cart=0 where transactions_to_cart is null;

update SEM.google_optimization_keyword_co set gross_CPO=0 where gross_CPO is null;

update SEM.google_optimization_keyword_co set gross_CAC=0 where gross_CAC is null;

update SEM.google_optimization_keyword_co set gross_CIR=0 where gross_CIR is null;

update SEM.google_optimization_keyword_co set gross_conversion_rate=0 where gross_conversion_rate is null;

update SEM.google_optimization_keyword_co set gross_transactions_to_cart=0 where gross_transactions_to_cart is null;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `google_optimization_keyword_mx`()
begin

set @days:=90;

call SEM.sem_filter_keyword_mx;

delete from SEM.google_optimization_keyword_mx where datediff(curdate(), date)<@days;

insert into SEM.google_optimization_keyword_mx(date, channel, campaign, ad_group, keyword, impressions, clicks, visits, bounce, cart, cost) select date, channel, campaign, ad_group, keyword, sum(impressions), sum(clicks), sum(visits), sum(bounce), sum(cart), sum(ad_cost) from SEM.sem_filter_campaign_keyword_mx where datediff(curdate(), date)<@days group by channel, date, campaign, ad_group, keyword;

update SEM.google_optimization_keyword_mx set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_mx set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=SEM.week_iso(date) where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_mx x set gross_transactions =  (select count(distinct transaction_id) from SEM.sem_filter_transaction_id_keyword_mx s, production.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx x set net_transactions =  (select count(distinct transaction_id) from SEM.sem_filter_transaction_id_keyword_mx s, production.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx x set gross_revenue =  (select sum(t.actual_paid_price) from SEM.sem_filter_transaction_id_keyword_mx s, production.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx x set net_revenue =  (select sum(t.actual_paid_price) from SEM.sem_filter_transaction_id_keyword_mx s, production.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx x set new_customers =  (select sum(t.new_customers) from SEM.sem_filter_transaction_id_keyword_mx s, production.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx x set new_customers_gross =  (select sum(t.new_customers_gross) from SEM.sem_filter_transaction_id_keyword_mx s, production.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx x set category = substring_index(campaign, '.',1) where datediff(curdate(), date)<@days;

-- cohort

create table SEM.temporary_mx(
date date,
campaign varchar(255),
ad_group varchar(255),
keyword varchar(255),
custid int,
transaction_id varchar(255)
);

create index temporary on SEM.temporary_mx(date, ad_group, campaign, keyword);
create index transaction_id on SEM.temporary_mx(transaction_id);

insert into SEM.temporary_mx select distinct x.date, x.campaign, x.ad_group, x.keyword, x.customer_id, x.transaction_id from SEM.google_optimization_transaction_id_keyword_mx x, production.tbl_order_detail t where x.transaction_id=t.order_nr and t.new_customers is not null and t.oac=1 and datediff(curdate(), x.date)<@days;

update SEM.google_optimization_keyword_mx a set net_transactions_30 = (select count(distinct t.order_nr) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx a set net_revenue_30 = (select sum(actual_paid_price) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx a set net_transactions_60 = (select count(distinct t.order_nr) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx a set net_revenue_60 = (select sum(actual_paid_price) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx a set net_transactions_90 = (select count(distinct t.order_nr) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx a set net_revenue_90 = (select sum(actual_paid_price) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx a set net_transactions_120 = (select count(distinct t.order_nr) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx a set net_revenue_120 = (select sum(actual_paid_price) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx a set net_transactions_150 = (select count(distinct t.order_nr) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx a set net_revenue_150 = (select sum(actual_paid_price) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_mx a set net_transactions_180 = (select count(distinct t.order_nr) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_mx a set net_revenue_180 = (select sum(actual_paid_price) from production.tbl_order_detail t, SEM.temporary_mx x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

drop table SEM.temporary_mx;

update SEM.google_optimization_keyword_mx set net_revenue_30=0 where net_revenue_30 is null;

update SEM.google_optimization_keyword_mx set net_revenue_60=0 where net_revenue_60 is null;

update SEM.google_optimization_keyword_mx set net_revenue_90=0 where net_revenue_90 is null;

update SEM.google_optimization_keyword_mx set net_revenue_120=0 where net_revenue_120 is null;

update SEM.google_optimization_keyword_mx set net_revenue_150=0 where net_revenue_150 is null;

update SEM.google_optimization_keyword_mx set net_revenue_180=0 where net_revenue_180 is null;

-- cohort

update SEM.google_optimization_keyword_mx s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, net_revenue_30=net_revenue_30/xr, net_revenue_60=net_revenue_60/xr, net_revenue_90=net_revenue_90/xr, net_revenue_120=net_revenue_120/xr, net_revenue_150=net_revenue_150/xr, net_revenue_180=net_revenue_180/xr, cost=cost/xr where r.country='MEX' and datediff(curdate(), s.date)<@days;

update SEM.google_optimization_keyword_mx set CPC=cost/clicks, CPO=cost/net_transactions, CTR=clicks/impressions, CAC=cost/new_customers, CIR=cost/net_revenue, checkout_rate=cart/visits, conversion_rate=net_transactions/clicks, bounce_rate=bounce/clicks, transactions_to_cart=net_transactions/cart where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_mx set gross_CPO=cost/gross_transactions, gross_CAC=cost/new_customers_gross, gross_CIR=cost/gross_revenue, gross_conversion_rate=gross_transactions/clicks, gross_transactions_to_cart=gross_transactions/cart where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_mx set gross_revenue=0 where gross_revenue is null;

update SEM.google_optimization_keyword_mx set net_revenue=0 where net_revenue is null;

update SEM.google_optimization_keyword_mx set new_customers=0 where new_customers is null;

update SEM.google_optimization_keyword_mx set new_customers_gross=0 where new_customers_gross is null;

update SEM.google_optimization_keyword_mx set CPC=0 where CPC is null;

update SEM.google_optimization_keyword_mx set CPO=0 where CPO is null;

update SEM.google_optimization_keyword_mx set CTR=0 where CTR is null;

update SEM.google_optimization_keyword_mx set CAC=0 where CAC is null;

update SEM.google_optimization_keyword_mx set CIR=0 where CIR is null;

update SEM.google_optimization_keyword_mx set checkout_rate=0 where checkout_rate is null;

update SEM.google_optimization_keyword_mx set conversion_rate=0 where conversion_rate is null;

update SEM.google_optimization_keyword_mx set bounce_rate=0 where bounce_rate is null;

update SEM.google_optimization_keyword_mx set transactions_to_cart=0 where transactions_to_cart is null;

update SEM.google_optimization_keyword_mx set gross_CPO=0 where gross_CPO is null;

update SEM.google_optimization_keyword_mx set gross_CAC=0 where gross_CAC is null;

update SEM.google_optimization_keyword_mx set gross_CIR=0 where gross_CIR is null;

update SEM.google_optimization_keyword_mx set gross_conversion_rate=0 where gross_conversion_rate is null;

update SEM.google_optimization_keyword_mx set gross_transactions_to_cart=0 where gross_transactions_to_cart is null;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `google_optimization_keyword_pe`()
begin

set @days:=90;

call SEM.sem_filter_keyword_pe;

delete from SEM.google_optimization_keyword_pe where datediff(curdate(), date)<@days;

insert into SEM.google_optimization_keyword_pe(date, channel, campaign, ad_group, keyword, impressions, clicks, visits, bounce, cart, cost) select date, channel, campaign, ad_group, keyword, sum(impressions), sum(clicks), sum(visits), sum(bounce), sum(cart), sum(ad_cost) from SEM.sem_filter_campaign_keyword_pe where datediff(curdate(), date)<@days group by channel, date, campaign, ad_group, keyword;

update SEM.google_optimization_keyword_pe set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_pe set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=SEM.week_iso(date) where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_pe x set gross_transactions =  (select count(distinct transaction_id) from SEM.sem_filter_transaction_id_keyword_pe s, production_pe.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_pe x set net_transactions =  (select count(distinct transaction_id) from SEM.sem_filter_transaction_id_keyword_pe s, production_pe.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_pe x set gross_revenue =  (select sum(t.paid_price) from SEM.sem_filter_transaction_id_keyword_pe s, production_pe.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_pe x set net_revenue =  (select sum(t.paid_price) from SEM.sem_filter_transaction_id_keyword_pe s, production_pe.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_pe x set new_customers =  (select sum(t.new_customers) from SEM.sem_filter_transaction_id_keyword_pe s, production_pe.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_pe x set new_customers_gross =  (select sum(t.new_customers_gross) from SEM.sem_filter_transaction_id_keyword_pe s, production_pe.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_pe x set category = substring_index(campaign, '.',1) where datediff(curdate(), date)<@days;

-- cohort

create table SEM.temporary_pe(
date date,
campaign varchar(255),
ad_group varchar(255),
keyword varchar(255),
custid int,
transaction_id varchar(255)
);

create index temporary on SEM.temporary_pe(date, ad_group, campaign);
create index temporary2 on SEM.temporary_pe(keyword);
create index transaction_id on SEM.temporary_pe(transaction_id);

insert into SEM.temporary_pe select distinct x.date, x.campaign, x.ad_group, x.keyword, x.customer_id, x.transaction_id from SEM.google_optimization_transaction_id_keyword_pe x, production_pe.tbl_order_detail t where x.transaction_id=t.order_nr and t.new_customers is not null and t.oac=1 and datediff(curdate(), x.date)<@days;

update SEM.google_optimization_keyword_pe a set net_transactions_30 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_pe a set net_revenue_30 = (select sum(paid_price) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.keyword=x.keyword and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_pe a set net_transactions_60 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.keyword=x.keyword and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_pe a set net_revenue_60 = (select sum(paid_price) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.keyword=x.keyword and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_pe a set net_transactions_90 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.keyword=x.keyword and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_pe a set net_revenue_90 = (select sum(paid_price) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.keyword=x.keyword and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_pe a set net_transactions_120 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.keyword=x.keyword and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_pe a set net_revenue_120 = (select sum(paid_price) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.keyword=x.keyword and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_pe a set net_transactions_150 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.keyword=x.keyword and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_pe a set net_revenue_150 = (select sum(paid_price) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.keyword=x.keyword and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_pe a set net_transactions_180 = (select count(distinct t.order_nr) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.keyword=x.keyword and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_pe a set net_revenue_180 = (select sum(paid_price) from production_pe.tbl_order_detail t, SEM.temporary_pe x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.keyword=x.keyword and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

drop table SEM.temporary_pe;

update SEM.google_optimization_keyword_pe set net_revenue_30=0 where net_revenue_30 is null;

update SEM.google_optimization_keyword_pe set net_revenue_60=0 where net_revenue_60 is null;

update SEM.google_optimization_keyword_pe set net_revenue_90=0 where net_revenue_90 is null;

update SEM.google_optimization_keyword_pe set net_revenue_120=0 where net_revenue_120 is null;

update SEM.google_optimization_keyword_pe set net_revenue_150=0 where net_revenue_150 is null;

update SEM.google_optimization_keyword_pe set net_revenue_180=0 where net_revenue_180 is null;

-- cohort

update SEM.google_optimization_keyword_pe s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, net_revenue_30=net_revenue_30/xr, net_revenue_60=net_revenue_60/xr, net_revenue_90=net_revenue_90/xr, net_revenue_120=net_revenue_120/xr, net_revenue_150=net_revenue_150/xr, net_revenue_180=net_revenue_180/xr where r.country='PER' and datediff(curdate(), s.date)<@days;

update SEM.google_optimization_keyword_pe set cost=cost/1.23 where yrmonth<=201302 and datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_pe set cost=cost/16.35 where yrmonth between 201303 and 201311 and datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_pe set cost=cost/1.23 where yrmonth >= 201312 and datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_pe set CPC=cost/clicks, CPO=cost/net_transactions, CTR=clicks/impressions, CAC=cost/new_customers, CIR=cost/net_revenue, checkout_rate=cart/visits, conversion_rate=net_transactions/clicks, bounce_rate=bounce/clicks, transactions_to_cart=net_transactions/cart where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_pe set gross_CPO=cost/gross_transactions, gross_CAC=cost/new_customers_gross, gross_CIR=cost/gross_revenue, gross_conversion_rate=gross_transactions/clicks, gross_transactions_to_cart=gross_transactions/cart where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_pe set gross_revenue=0 where gross_revenue is null;

update SEM.google_optimization_keyword_pe set net_revenue=0 where net_revenue is null;

update SEM.google_optimization_keyword_pe set new_customers=0 where new_customers is null;

update SEM.google_optimization_keyword_pe set new_customers_gross=0 where new_customers_gross is null;

update SEM.google_optimization_keyword_pe set CPC=0 where CPC is null;

update SEM.google_optimization_keyword_pe set CPO=0 where CPO is null;

update SEM.google_optimization_keyword_pe set CTR=0 where CTR is null;

update SEM.google_optimization_keyword_pe set CAC=0 where CAC is null;

update SEM.google_optimization_keyword_pe set CIR=0 where CIR is null;

update SEM.google_optimization_keyword_pe set checkout_rate=0 where checkout_rate is null;

update SEM.google_optimization_keyword_pe set conversion_rate=0 where conversion_rate is null;

update SEM.google_optimization_keyword_pe set bounce_rate=0 where bounce_rate is null;

update SEM.google_optimization_keyword_pe set transactions_to_cart=0 where transactions_to_cart is null;

update SEM.google_optimization_keyword_pe set gross_CPO=0 where gross_CPO is null;

update SEM.google_optimization_keyword_pe set gross_CAC=0 where gross_CAC is null;

update SEM.google_optimization_keyword_pe set gross_CIR=0 where gross_CIR is null;

update SEM.google_optimization_keyword_pe set gross_conversion_rate=0 where gross_conversion_rate is null;

update SEM.google_optimization_keyword_pe set gross_transactions_to_cart=0 where gross_transactions_to_cart is null;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `google_optimization_keyword_ve`()
begin

set @days:=90;

call SEM.sem_filter_keyword_ve;

delete from SEM.google_optimization_keyword_ve where datediff(curdate(), date)<@days;

insert into SEM.google_optimization_keyword_ve(date, channel, campaign, ad_group, keyword, impressions, clicks, visits, bounce, cart, cost) select date, channel, campaign, ad_group, keyword, sum(impressions), sum(clicks), sum(visits), sum(bounce), sum(cart), sum(ad_cost) from SEM.sem_filter_campaign_keyword_ve where datediff(curdate(), date)<@days group by channel, date, campaign, ad_group, keyword;

update SEM.google_optimization_keyword_ve set weekday = case when weekday(date) = 0 then 'Monday' when weekday(date) = 1 then 'Tuesday' when weekday(date) = 2 then 'Wednesday' when weekday(date) = 3 then 'Thursday' when weekday(date) = 4 then 'Friday' when weekday(date) = 5 then 'Saturday' when weekday(date) = 6 then 'Sunday' end where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_ve set yrmonth = concat(year(date),if(month(date)<10,concat(0,month(date)),month(date))), week=SEM.week_iso(date) where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_ve x set gross_transactions =  (select count(distinct transaction_id) from SEM.sem_filter_transaction_id_keyword_ve s, production_ve.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_ve x set net_transactions =  (select count(distinct transaction_id) from SEM.sem_filter_transaction_id_keyword_ve s, production_ve.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_ve x set gross_revenue =  (select sum(t.paid_price) from SEM.sem_filter_transaction_id_keyword_ve s, production_ve.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_ve x set net_revenue =  (select sum(t.paid_price) from SEM.sem_filter_transaction_id_keyword_ve s, production_ve.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_ve x set new_customers =  (select sum(t.new_customers) from SEM.sem_filter_transaction_id_keyword_ve s, production_ve.tbl_order_detail t where t.oac=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_ve x set new_customers_gross =  (select sum(t.new_customers_gross) from SEM.sem_filter_transaction_id_keyword_ve s, production_ve.tbl_order_detail t where t.obc=1 and t.order_nr=s.transaction_id and x.date=s.date and x.campaign=s.campaign and s.keyword=x.keyword and x.ad_group=s.ad_group) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_ve x set category = substring_index(campaign, '.',1) where datediff(curdate(), date)<@days;

-- cohort

create table SEM.temporary_ve(
date date,
campaign varchar(255),
ad_group varchar(255),
keyword varchar(255),
custid int,
transaction_id varchar(255)
);

create index temporary on SEM.temporary_ve(date, ad_group, campaign);
create index temporary2 on SEM.temporary_ve(keyword);
create index transaction_id on SEM.temporary_ve(transaction_id);

insert into SEM.temporary_ve select distinct x.date, x.campaign, x.ad_group, x.keyword, x.customer_id, x.transaction_id from SEM.google_optimization_transaction_id_keyword_ve x, production_ve.tbl_order_detail t where x.transaction_id=t.order_nr and t.new_customers is not null and t.oac=1 and datediff(curdate(), x.date)<@days;

update SEM.google_optimization_keyword_ve a set net_transactions_30 = (select count(distinct t.order_nr) from production_ve.tbl_order_detail t, SEM.temporary_ve x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_ve a set net_revenue_30 = (select sum(paid_price) from production_ve.tbl_order_detail t, SEM.temporary_ve x where t.custid=x.custid and a.ad_group=x.ad_group and a.campaign=x.campaign and a.keyword=x.keyword and a.date=x.date and t.date between x.date and adddate(x.date, interval 30 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_ve a set net_transactions_60 = (select count(distinct t.order_nr) from production_ve.tbl_order_detail t, SEM.temporary_ve x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_ve a set net_revenue_60 = (select sum(paid_price) from production_ve.tbl_order_detail t, SEM.temporary_ve x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 60 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_ve a set net_transactions_90 = (select count(distinct t.order_nr) from production_ve.tbl_order_detail t, SEM.temporary_ve x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_ve a set net_revenue_90 = (select sum(paid_price) from production_ve.tbl_order_detail t, SEM.temporary_ve x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 90 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_ve a set net_transactions_120 = (select count(distinct t.order_nr) from production_ve.tbl_order_detail t, SEM.temporary_ve x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_ve a set net_revenue_120 = (select sum(paid_price) from production_ve.tbl_order_detail t, SEM.temporary_ve x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 120 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_ve a set net_transactions_150 = (select count(distinct t.order_nr) from production_ve.tbl_order_detail t, SEM.temporary_ve x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_ve a set net_revenue_150 = (select sum(paid_price) from production_ve.tbl_order_detail t, SEM.temporary_ve x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 150 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_ve a set net_transactions_180 = (select count(distinct t.order_nr) from production_ve.tbl_order_detail t, SEM.temporary_ve x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

update SEM.google_optimization_keyword_ve a set net_revenue_180 = (select sum(paid_price) from production_ve.tbl_order_detail t, SEM.temporary_ve x where t.custid=x.custid and a.ad_group=x.ad_group and a.keyword=x.keyword and a.campaign=x.campaign and a.date=x.date and t.date between x.date and adddate(x.date, interval 180 day) and t.new_customers is null and t.oac=1) where datediff(curdate(), date)<@days; 

drop table SEM.temporary_ve;

update SEM.google_optimization_keyword_ve set net_revenue_30=0 where net_revenue_30 is null;

update SEM.google_optimization_keyword_ve set net_revenue_60=0 where net_revenue_60 is null;

update SEM.google_optimization_keyword_ve set net_revenue_90=0 where net_revenue_90 is null;

update SEM.google_optimization_keyword_ve set net_revenue_120=0 where net_revenue_120 is null;

update SEM.google_optimization_keyword_ve set net_revenue_150=0 where net_revenue_150 is null;

update SEM.google_optimization_keyword_ve set net_revenue_180=0 where net_revenue_180 is null;

-- cohort

update SEM.google_optimization_keyword_ve s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set gross_revenue=gross_revenue/xr, net_revenue=net_revenue/xr, net_revenue_30=net_revenue_30/xr, net_revenue_60=net_revenue_60/xr, net_revenue_90=net_revenue_90/xr, net_revenue_120=net_revenue_120/xr, net_revenue_150=net_revenue_150/xr, net_revenue_180=net_revenue_180/xr where r.country='VEN' and datediff(curdate(), s.date)<@days;

update SEM.google_optimization_keyword_ve set cost=cost/16.35 where yrmonth<=201311 and datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_ve set cost=cost/1.23 where yrmonth>=201312 and datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_ve set CPC=cost/clicks, CPO=cost/net_transactions, CTR=clicks/impressions, CAC=cost/new_customers, CIR=cost/net_revenue, checkout_rate=cart/visits, conversion_rate=net_transactions/clicks, bounce_rate=bounce/clicks, transactions_to_cart=net_transactions/cart where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_ve set gross_CPO=cost/gross_transactions, gross_CAC=cost/new_customers_gross, gross_CIR=cost/gross_revenue, gross_conversion_rate=gross_transactions/clicks, gross_transactions_to_cart=gross_transactions/cart where datediff(curdate(), date)<@days;

update SEM.google_optimization_keyword_ve set gross_revenue=0 where gross_revenue is null;

update SEM.google_optimization_keyword_ve set net_revenue=0 where net_revenue is null;

update SEM.google_optimization_keyword_ve set new_customers=0 where new_customers is null;

update SEM.google_optimization_keyword_ve set new_customers_gross=0 where new_customers_gross is null;

update SEM.google_optimization_keyword_ve set CPC=0 where CPC is null;

update SEM.google_optimization_keyword_ve set CPO=0 where CPO is null;

update SEM.google_optimization_keyword_ve set CTR=0 where CTR is null;

update SEM.google_optimization_keyword_ve set CAC=0 where CAC is null;

update SEM.google_optimization_keyword_ve set CIR=0 where CIR is null;

update SEM.google_optimization_keyword_ve set checkout_rate=0 where checkout_rate is null;

update SEM.google_optimization_keyword_ve set conversion_rate=0 where conversion_rate is null;

update SEM.google_optimization_keyword_ve set bounce_rate=0 where bounce_rate is null;

update SEM.google_optimization_keyword_ve set transactions_to_cart=0 where transactions_to_cart is null;

update SEM.google_optimization_keyword_ve set gross_CPO=0 where gross_CPO is null;

update SEM.google_optimization_keyword_ve set gross_CAC=0 where gross_CAC is null;

update SEM.google_optimization_keyword_ve set gross_CIR=0 where gross_CIR is null;

update SEM.google_optimization_keyword_ve set gross_conversion_rate=0 where gross_conversion_rate is null;

update SEM.google_optimization_keyword_ve set gross_transactions_to_cart=0 where gross_transactions_to_cart is null;

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `google_optimization_transaction_id_co`()
begin

delete from SEM.google_optimization_transaction_id_co;

insert into SEM.google_optimization_transaction_id_co select concat(year(x.date),if(month(x.date)<10,concat(0,month(x.date)),month(x.date))), SEM.week_iso(x.date), x.date, case when weekday(x.date) = 0 then 'Monday' when weekday(x.date) = 1 then 'Tuesday' when weekday(x.date) = 2 then 'Wednesday' when weekday(x.date) = 3 then 'Thursday' when weekday(x.date) = 4 then 'Friday' when weekday(x.date) = 5 then 'Saturday' when weekday(x.date) = 6 then 'Sunday' end, x.channel,  substring_index(x.campaign, '.',1), x.campaign, x.ad_group, t.custid, t.order_nr, t.sku, t.product_name, t.n1, t.paid_price, t.status_item, case when t.oac=1 then 'net' when t.status_item = 'invalid' then 'invalid' else 'gross' end, t.payment_method, case when t.new_customers !=0 then 1 else 0 end from SEM.sem_filter_transaction_id_co x, production_co.tbl_order_detail t where t.order_nr=x.transaction_id;

insert into SEM.google_optimization_transaction_id_co select concat(year(x.date),if(month(x.date)<10,concat(0,month(x.date)),month(x.date))), SEM.week_iso(x.date), x.date, case when weekday(x.date) = 0 then 'Monday' when weekday(x.date) = 1 then 'Tuesday' when weekday(x.date) = 2 then 'Wednesday' when weekday(x.date) = 3 then 'Thursday' when weekday(x.date) = 4 then 'Friday' when weekday(x.date) = 5 then 'Saturday' when weekday(x.date) = 6 then 'Sunday' end, x.channel,  substring_index(x.campaign, '.',1), x.campaign, x.ad_group, t.custid, t.order_nr, t.sku, t.product_name, t.n1, t.paid_price, t.status_item, case when t.oac=1 then 'net' when t.status_item = 'invalid' then 'invalid' else 'gross' end, t.payment_method, case when t.new_customers !=0 then 1 else 0 end from SEM.sem_filter_transaction_id_fashion_co x, production_co.tbl_order_detail t where t.order_nr=x.transaction_id;

update SEM.google_optimization_transaction_id_co s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr where r.country='COL';

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `google_optimization_transaction_id_keyword_co`()
begin

delete from SEM.google_optimization_transaction_id_keyword_co;

insert into SEM.google_optimization_transaction_id_keyword_co select concat(year(x.date),if(month(x.date)<10,concat(0,month(x.date)),month(x.date))), SEM.week_iso(x.date), x.date, case when weekday(x.date) = 0 then 'Monday' when weekday(x.date) = 1 then 'Tuesday' when weekday(x.date) = 2 then 'Wednesday' when weekday(x.date) = 3 then 'Thursday' when weekday(x.date) = 4 then 'Friday' when weekday(x.date) = 5 then 'Saturday' when weekday(x.date) = 6 then 'Sunday' end, x.channel,  substring_index(x.campaign, '.',1), x.campaign, x.ad_group, x.keyword, t.custid, t.order_nr, t.sku, t.product_name, t.n1, t.paid_price, t.status_item, case when t.oac=1 then 'net' when t.status_item = 'invalid' then 'invalid' else 'gross' end, t.payment_method, case when t.new_customers !=0 then 1 else 0 end from SEM.sem_filter_transaction_id_keyword_co x, production_co.tbl_order_detail t where t.order_nr=x.transaction_id;

update SEM.google_optimization_transaction_id_keyword_co s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr where r.country='COL';

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `google_optimization_transaction_id_keyword_mx`()
begin

delete from SEM.google_optimization_transaction_id_keyword_mx;

insert into SEM.google_optimization_transaction_id_keyword_mx select concat(year(x.date),if(month(x.date)<10,concat(0,month(x.date)),month(x.date))), SEM.week_iso(x.date), x.date, case when weekday(x.date) = 0 then 'Monday' when weekday(x.date) = 1 then 'Tuesday' when weekday(x.date) = 2 then 'Wednesday' when weekday(x.date) = 3 then 'Thursday' when weekday(x.date) = 4 then 'Friday' when weekday(x.date) = 5 then 'Saturday' when weekday(x.date) = 6 then 'Sunday' end, x.channel, substring_index(x.campaign, '.',1), x.campaign, x.ad_group, x.keyword, t.custid, t.order_nr, t.sku, t.product_name, t.n1, t.actual_paid_price, t.status_item, case when t.oac=1 then 'net' when t.status_item = 'invalid' then 'invalid' else 'gross' end, t.payment_method, case when t.new_customers is not null then 1 else 0 end from SEM.sem_filter_transaction_id_keyword_mx x, production.tbl_order_detail t where t.order_nr=x.transaction_id;

update SEM.google_optimization_transaction_id_keyword_mx s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr where r.country='MEX';

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `google_optimization_transaction_id_keyword_pe`()
begin

delete from SEM.google_optimization_transaction_id_keyword_pe;

insert into SEM.google_optimization_transaction_id_keyword_pe select concat(year(x.date),if(month(x.date)<10,concat(0,month(x.date)),month(x.date))), SEM.week_iso(x.date), x.date, case when weekday(x.date) = 0 then 'Monday' when weekday(x.date) = 1 then 'Tuesday' when weekday(x.date) = 2 then 'Wednesday' when weekday(x.date) = 3 then 'Thursday' when weekday(x.date) = 4 then 'Friday' when weekday(x.date) = 5 then 'Saturday' when weekday(x.date) = 6 then 'Sunday' end, x.channel, substring_index(x.campaign, '.',1), x.campaign, x.ad_group, x.keyword, t.custid, t.order_nr, t.sku, t.product_name, t.n1, t.paid_price, t.status_item, case when t.oac=1 then 'net' when t.status_item = 'invalid' then 'invalid' else 'gross' end, t.payment_method, case when t.new_customers is not null then 1 else 0 end from SEM.sem_filter_transaction_id_keyword_pe x, production_pe.tbl_order_detail t where t.order_nr=x.transaction_id;

update SEM.google_optimization_transaction_id_keyword_pe s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr where r.country='PER';

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `google_optimization_transaction_id_keyword_ve`()
begin

delete from SEM.google_optimization_transaction_id_keyword_ve;

insert into SEM.google_optimization_transaction_id_keyword_ve select concat(year(x.date),if(month(x.date)<10,concat(0,month(x.date)),month(x.date))), SEM.week_iso(x.date), x.date, case when weekday(x.date) = 0 then 'Monday' when weekday(x.date) = 1 then 'Tuesday' when weekday(x.date) = 2 then 'Wednesday' when weekday(x.date) = 3 then 'Thursday' when weekday(x.date) = 4 then 'Friday' when weekday(x.date) = 5 then 'Saturday' when weekday(x.date) = 6 then 'Sunday' end, x.channel, substring_index(x.campaign, '.',1), x.campaign, x.ad_group, x.keyword, t.custid, t.order_nr, t.sku, t.product_name, t.n1, t.paid_price, t.status_item, case when t.oac=1 then 'net' when t.status_item = 'invalid' then 'invalid' else 'gross' end, t.payment_method, case when t.new_customers is not null then 1 else 0 end from SEM.sem_filter_transaction_id_keyword_ve x, production_ve.tbl_order_detail t where t.order_nr=x.transaction_id; 

update SEM.google_optimization_transaction_id_keyword_ve s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr where r.country='VEN';

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `google_optimization_transaction_id_mx`()
begin

delete from SEM.google_optimization_transaction_id_mx;

insert into SEM.google_optimization_transaction_id_mx select concat(year(x.date),if(month(x.date)<10,concat(0,month(x.date)),month(x.date))), SEM.week_iso(x.date), x.date, case when weekday(x.date) = 0 then 'Monday' when weekday(x.date) = 1 then 'Tuesday' when weekday(x.date) = 2 then 'Wednesday' when weekday(x.date) = 3 then 'Thursday' when weekday(x.date) = 4 then 'Friday' when weekday(x.date) = 5 then 'Saturday' when weekday(x.date) = 6 then 'Sunday' end, x.channel, substring_index(x.campaign, '.',1), x.campaign, x.ad_group, t.custid, t.order_nr, t.sku, t.product_name, t.n1, t.actual_paid_price, t.status_item, case when t.oac=1 then 'net' when t.status_item = 'invalid' then 'invalid' else 'gross' end, t.payment_method, case when t.new_customers is not null then 1 else 0 end from SEM.sem_filter_transaction_id_mx x, production.tbl_order_detail t where t.order_nr=x.transaction_id;

update SEM.google_optimization_transaction_id_mx s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr where r.country='MEX';

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `google_optimization_transaction_id_pe`()
begin

delete from SEM.google_optimization_transaction_id_pe;

insert into SEM.google_optimization_transaction_id_pe select concat(year(x.date),if(month(x.date)<10,concat(0,month(x.date)),month(x.date))), SEM.week_iso(x.date), x.date, case when weekday(x.date) = 0 then 'Monday' when weekday(x.date) = 1 then 'Tuesday' when weekday(x.date) = 2 then 'Wednesday' when weekday(x.date) = 3 then 'Thursday' when weekday(x.date) = 4 then 'Friday' when weekday(x.date) = 5 then 'Saturday' when weekday(x.date) = 6 then 'Sunday' end, x.channel, substring_index(x.campaign, '.',1), x.campaign, x.ad_group, t.custid, t.order_nr, t.sku, t.product_name, t.n1, t.paid_price, t.status_item, case when t.oac=1 then 'net' when t.status_item = 'invalid' then 'invalid' else 'gross' end, t.payment_method, case when t.new_customers is not null then 1 else 0 end from SEM.sem_filter_transaction_id_pe x, production_pe.tbl_order_detail t where t.order_nr=x.transaction_id;

update SEM.google_optimization_transaction_id_pe s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr where r.country='PER';

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `google_optimization_transaction_id_ve`()
begin

delete from SEM.google_optimization_transaction_id_ve;

insert into SEM.google_optimization_transaction_id_ve select concat(year(x.date),if(month(x.date)<10,concat(0,month(x.date)),month(x.date))), SEM.week_iso(x.date), x.date, case when weekday(x.date) = 0 then 'Monday' when weekday(x.date) = 1 then 'Tuesday' when weekday(x.date) = 2 then 'Wednesday' when weekday(x.date) = 3 then 'Thursday' when weekday(x.date) = 4 then 'Friday' when weekday(x.date) = 5 then 'Saturday' when weekday(x.date) = 6 then 'Sunday' end, x.channel, substring_index(x.campaign, '.',1), x.campaign, x.ad_group, t.custid, t.order_nr, t.sku, t.product_name, t.n1, t.paid_price, t.status_item, case when t.oac=1 then 'net' when t.status_item = 'invalid' then 'invalid' else 'gross' end, t.payment_method, case when t.new_customers is not null then 1 else 0 end from SEM.sem_filter_transaction_id_ve x, production_ve.tbl_order_detail t where t.order_nr=x.transaction_id; 

update SEM.google_optimization_transaction_id_ve s inner join development_mx.A_E_BI_ExchangeRate r on r.month_num=s.yrmonth set net_revenue=net_revenue/xr where r.country='VEN';

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `optimization`()
begin

select  'SEM Optimization Tool: start',now();

call SEM.google_optimization_ad_group_mx;

call SEM.google_optimization_transaction_id_mx;

call SEM.google_optimization_ad_group_co;

call SEM.google_optimization_transaction_id_co;

call SEM.google_optimization_ad_group_pe;

call SEM.google_optimization_transaction_id_pe;

call SEM.google_optimization_ad_group_ve;

call SEM.google_optimization_transaction_id_ve;

select  'SEM Optimization Tool: end',now();

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`carlos.mondragon`@`%`*/ /*!50003 PROCEDURE `optimization_keyword`()
    SQL SECURITY INVOKER
begin

select  'SEM Optimization Tool: start',now();

call SEM.google_optimization_keyword_mx;

call SEM.google_optimization_transaction_id_keyword_mx;

call SEM.google_optimization_keyword_co;

call SEM.google_optimization_transaction_id_keyword_co;

call SEM.google_optimization_keyword_pe;

call SEM.google_optimization_transaction_id_keyword_pe;

call SEM.google_optimization_keyword_ve;

call SEM.google_optimization_transaction_id_keyword_ve;

select  'SEM Optimization Tool: end',now();

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `sem_filter_co`()
begin

delete from SEM.sem_filter_campaign_ad_group_co;

insert into SEM.sem_filter_campaign_ad_group_co select 'SEM', x.date, x.campaign, x.source, x.medium, x.ad_group, x.impressions, x.clicks, x.visits, x.ad_cost, x.bounce, x.cart from SEM.campaign_ad_group_co x where source='google' and medium='CPC' and campaign not like '%r.%' and campaign not like '%d.%' and campaign not like '%w.%' and campaign not like '%[D%' and campaign not like '%brandb.%' and campaign not like '|%';

insert into SEM.sem_filter_campaign_ad_group_co select 'GDN', x.date, x.campaign, x.source, x.medium, x.ad_group, x.impressions, x.clicks, x.visits, x.ad_cost, x.bounce, x.cart from SEM.campaign_ad_group_co x where source='google' and medium='cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%' or campaign like '|%');

delete from SEM.sem_filter_campaign_ad_group_fashion_co;

insert into SEM.sem_filter_campaign_ad_group_fashion_co select 'SEM', x.date, x.campaign, x.source, x.medium, x.ad_group, x.impressions, x.clicks, x.visits, x.ad_cost, x.bounce, x.cart from SEM.campaign_ad_group_fashion_co x where source='google' and medium='CPC' and campaign not like '%r.%' and campaign not like '%d.%' and campaign not like '%w.%' and campaign not like '%[D%' and campaign not like '%brandb.%' and campaign not like '|%';

insert into SEM.sem_filter_campaign_ad_group_fashion_co select 'GDN', x.date, x.campaign, x.source, x.medium, x.ad_group, x.impressions, x.clicks, x.visits, x.ad_cost, x.bounce, x.cart from SEM.campaign_ad_group_fashion_co x where source='google' and medium='cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%' or campaign like '|%');

delete from SEM.sem_filter_transaction_id_co;

insert into SEM.sem_filter_transaction_id_co select 'SEM', x.* from SEM.transaction_id_co x where source='google' and medium='CPC' and campaign not like '%r.%' and campaign not like '%d.%' and campaign not like '%w.%' and campaign not like '%[D%' and campaign not like '%brandb.%' and campaign not like '|%';

insert into SEM.sem_filter_transaction_id_co select 'GDN', x.* from SEM.transaction_id_co x where source='google' and medium='cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%' or campaign like '|%');

insert into SEM.sem_filter_transaction_id_co select 'SEM', x.* from SEM.transaction_id_fashion_co x where source='google' and medium='CPC' and campaign not like '%r.%' and campaign not like '%d.%' and campaign not like '%w.%' and campaign not like '%[D%' and campaign not like '%brandb.%' and campaign not like '|%';

insert into SEM.sem_filter_transaction_id_co select 'GDN', x.* from SEM.transaction_id_fashion_co x where source='google' and medium='cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%' or campaign like '|%');

delete from SEM.sem_filter_transaction_id_fashion_co;

insert into SEM.sem_filter_transaction_id_fashion_co select 'SEM', x.* from SEM.transaction_id_fashion_co x where source='google' and medium='CPC' and campaign not like '%r.%' and campaign not like '%d.%' and campaign not like '%w.%' and campaign not like '%[D%' and campaign not like '%brandb.%' and campaign not like '|%';

insert into SEM.sem_filter_transaction_id_fashion_co select 'GDN', x.* from SEM.transaction_id_fashion_co x where source='google' and medium='cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%' or campaign like '|%');

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `sem_filter_keyword_co`()
begin

delete from SEM.sem_filter_campaign_keyword_co;

insert into SEM.sem_filter_campaign_keyword_co select 'SEM', x.date, x.campaign, x.source, x.medium, x.ad_group, x.keyword, x.impressions, x.clicks, x.visits, x.ad_cost, x.bounce, x.cart from SEM.campaign_keyword_co x where source='google' and medium='CPC' and campaign not like '%r.%' and campaign not like '%d.%' and campaign not like '%w.%' and campaign not like '%[D%' and campaign not like '%brandb.%' and campaign not like '|%';

insert into SEM.sem_filter_campaign_keyword_co select 'GDN', x.date, x.campaign, x.source, x.medium, x.ad_group, x.keyword, x.impressions, x.clicks, x.visits, x.ad_cost, x.bounce, x.cart from SEM.campaign_keyword_co x where source='google' and medium='cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%' or campaign like '|%');

delete from SEM.sem_filter_transaction_id_keyword_co;

insert into SEM.sem_filter_transaction_id_keyword_co select 'SEM', x.* from SEM.transaction_id_keyword_co x where source='google' and medium='CPC' and campaign not like '%r.%' and campaign not like '%d.%' and campaign not like '%w.%' and campaign not like '%[D%' and campaign not like '%brandb.%' and campaign not like '|%';

insert into SEM.sem_filter_transaction_id_keyword_co select 'GDN', x.* from SEM.transaction_id_keyword_co x where source='google' and medium='cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%' or campaign like '|%');

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `sem_filter_keyword_mx`()
begin

delete from SEM.sem_filter_campaign_keyword_mx;

insert into SEM.sem_filter_campaign_keyword_mx select 'SEM', x.date, x.campaign, x.source, x.medium, x.ad_group, x.keyword, x.impressions, x.clicks, x.visits, x.ad_cost, x.bounce, x.cart from SEM.campaign_keyword_mx x where source='google' and medium='CPC' and campaign not like '%r.%' and campaign not like '%d.%' and campaign not like '%w.%' and campaign not like '%[D%' and campaign not like '%brandb.%' and campaign not like '|%';

insert into SEM.sem_filter_campaign_keyword_mx select 'GDN', x.date, x.campaign, x.source, x.medium, x.ad_group, x.keyword, x.impressions, x.clicks, x.visits, x.ad_cost, x.bounce, x.cart from SEM.campaign_keyword_mx x where source='google' and medium='cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%' or campaign like '|%');

delete from SEM.sem_filter_transaction_id_keyword_mx;

insert into SEM.sem_filter_transaction_id_keyword_mx select 'SEM', x.* from SEM.transaction_id_keyword_mx x where source='google' and medium='cpc' and campaign not like '%r.%' and campaign not like '%d.%' and campaign not like '%w.%' and campaign not like '%[D%' and campaign not like '%brandb.%' and campaign not like '|%';

insert into SEM.sem_filter_transaction_id_keyword_mx select 'GDN', x.* from SEM.transaction_id_keyword_mx x where source='google' and medium='cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%' or campaign like '|%');

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `sem_filter_keyword_pe`()
begin

delete from SEM.sem_filter_campaign_keyword_pe;

insert into SEM.sem_filter_campaign_keyword_pe select 'SEM', x.date, x.campaign, x.source, x.medium, x.ad_group, x.keyword, x.impressions, x.clicks, x.visits, x.ad_cost, x.bounce, x.cart from SEM.campaign_keyword_pe x where source='google' and medium='CPC' and campaign not like '%r.%' and campaign not like '%d.%' and campaign not like '%w.%' and campaign not like '%[D%' and campaign not like '%brandb.%' and campaign not like '|%';

insert into SEM.sem_filter_campaign_keyword_pe select 'GDN', x.date, x.campaign, x.source, x.medium, x.ad_group, x.keyword, x.impressions, x.clicks, x.visits, x.ad_cost, x.bounce, x.cart from SEM.campaign_keyword_pe x where source='google' and medium='cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%' or campaign like '|%');

delete from SEM.sem_filter_transaction_id_keyword_pe;

insert into SEM.sem_filter_transaction_id_keyword_pe select 'SEM', x.* from SEM.transaction_id_keyword_pe x where source='google' and medium='CPC' and campaign not like '%r.%' and campaign not like '%d.%' and campaign not like '%w.%' and campaign not like '%[D%' and campaign not like '%brandb.%' and campaign not like '|%';

insert into SEM.sem_filter_transaction_id_keyword_pe select 'GDN', x.* from SEM.transaction_id_keyword_pe x where source='google' and medium='cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%' or campaign like '|%');

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `sem_filter_keyword_ve`()
begin

delete from SEM.sem_filter_campaign_keyword_ve;

insert into SEM.sem_filter_campaign_keyword_ve select 'SEM', x.date, x.campaign, x.source, x.medium, x.ad_group, x.keyword, x.impressions, x.clicks, x.visits, x.ad_cost, x.bounce, x.cart from SEM.campaign_keyword_ve x where source='google' and medium='CPC' and campaign not like '%r.%' and campaign not like '%d.%' and campaign not like '%w.%' and campaign not like '%[D%' and campaign not like '%brandb.%' and campaign not like '|%';

insert into SEM.sem_filter_campaign_keyword_ve select 'GDN', x.date, x.campaign, x.source, x.medium, x.ad_group, x.keyword, x.impressions, x.clicks, x.visits, x.ad_cost, x.bounce, x.cart from SEM.campaign_keyword_ve x where source='google' and medium='cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%' or campaign like '|%');

delete from SEM.sem_filter_transaction_id_keyword_ve;

insert into SEM.sem_filter_transaction_id_keyword_ve select 'SEM', x.* from SEM.transaction_id_keyword_ve x where source='google' and medium='CPC' and campaign not like '%r.%' and campaign not like '%d.%' and campaign not like '%w.%' and campaign not like '%[D%' and campaign not like '%brandb.%' and campaign not like '|%';

insert into SEM.sem_filter_transaction_id_keyword_ve select 'GDN', x.* from SEM.transaction_id_keyword_ve x where source='google' and medium='cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%' or campaign like '|%');

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `sem_filter_mx`()
begin

delete from SEM.sem_filter_campaign_ad_group_mx;

insert into SEM.sem_filter_campaign_ad_group_mx select 'SEM', x.date, x.campaign, x.source, x.medium, x.ad_group, x.impressions, x.clicks, x.visits, x.ad_cost, x.bounce, x.cart from SEM.campaign_ad_group_mx x where source='google' and medium='CPC' and campaign not like '%r.%' and campaign not like '%d.%' and campaign not like '%w.%' and campaign not like '%[D%' and campaign not like '%brandb.%' and campaign not like '|%';

insert into SEM.sem_filter_campaign_ad_group_mx select 'GDN', x.date, x.campaign, x.source, x.medium, x.ad_group, x.impressions, x.clicks, x.visits, x.ad_cost, x.bounce, x.cart from SEM.campaign_ad_group_mx x where source='google' and medium='cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%' or campaign like '|%');

delete from SEM.sem_filter_transaction_id_mx;

insert into SEM.sem_filter_transaction_id_mx select 'SEM', x.* from SEM.transaction_id_mx x where source='google' and medium='cpc' and campaign not like '%r.%' and campaign not like '%d.%' and campaign not like '%w.%' and campaign not like '%[D%' and campaign not like '%brandb.%' and campaign not like '|%';

insert into SEM.sem_filter_transaction_id_mx select 'GDN', x.* from SEM.transaction_id_mx x where source='google' and medium='cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%' or campaign like '|%');

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `sem_filter_pe`()
begin

delete from SEM.sem_filter_campaign_ad_group_pe;

insert into SEM.sem_filter_campaign_ad_group_pe select 'SEM', x.date, x.campaign, x.source, x.medium, x.ad_group, x.impressions, x.clicks, x.visits, x.ad_cost, x.bounce, x.cart from SEM.campaign_ad_group_pe x where source='google' and medium='CPC' and campaign not like '%r.%' and campaign not like '%d.%' and campaign not like '%w.%' and campaign not like '%[D%' and campaign not like '%brandb.%' and campaign not like '|%';

insert into SEM.sem_filter_campaign_ad_group_pe select 'GDN', x.date, x.campaign, x.source, x.medium, x.ad_group, x.impressions, x.clicks, x.visits, x.ad_cost, x.bounce, x.cart from SEM.campaign_ad_group_pe x where source='google' and medium='cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%' or campaign like '|%');

delete from SEM.sem_filter_transaction_id_pe;

insert into SEM.sem_filter_transaction_id_pe select 'SEM', x.* from SEM.transaction_id_pe x where source='google' and medium='CPC' and campaign not like '%r.%' and campaign not like '%d.%' and campaign not like '%w.%' and campaign not like '%[D%' and campaign not like '%brandb.%' and campaign not like '|%';

insert into SEM.sem_filter_transaction_id_pe select 'GDN', x.* from SEM.transaction_id_pe x where source='google' and medium='cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%' or campaign like '|%');

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`charles`@`%`*/ /*!50003 PROCEDURE `sem_filter_ve`()
begin

delete from SEM.sem_filter_campaign_ad_group_ve;

insert into SEM.sem_filter_campaign_ad_group_ve select 'SEM', x.date, x.campaign, x.source, x.medium, x.ad_group, x.impressions, x.clicks, x.visits, x.ad_cost, x.bounce, x.cart from SEM.campaign_ad_group_ve x where source='google' and medium='CPC' and campaign not like '%r.%' and campaign not like '%d.%' and campaign not like '%w.%' and campaign not like '%[D%' and campaign not like '%brandb.%' and campaign not like '|%';

insert into SEM.sem_filter_campaign_ad_group_ve select 'GDN', x.date, x.campaign, x.source, x.medium, x.ad_group, x.impressions, x.clicks, x.visits, x.ad_cost, x.bounce, x.cart from SEM.campaign_ad_group_ve x where source='google' and medium='cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%' or campaign like '|%');

delete from SEM.sem_filter_transaction_id_ve;

insert into SEM.sem_filter_transaction_id_ve select 'SEM', x.* from SEM.transaction_id_ve x where source='google' and medium='CPC' and campaign not like '%r.%' and campaign not like '%d.%' and campaign not like '%w.%' and campaign not like '%[D%' and campaign not like '%brandb.%' and campaign not like '|%';

insert into SEM.sem_filter_transaction_id_ve select 'GDN', x.* from SEM.transaction_id_ve x where source='google' and medium='cpc' and 
(campaign like 'r.%' or campaign like '[D%' or campaign like 'er.%' or campaign like '%d.%' or campaign like '|%');

end */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:03:24
