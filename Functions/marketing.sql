-- MySQL dump 10.13  Distrib 5.1.69, for redhat-linux-gnu (x86_64)
--
-- Host: 172.17.12.191    Database: marketing
-- ------------------------------------------------------
-- Server version	5.1.69-log
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'marketing'
--
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50020 DEFINER=`javier.guana`@`%`*/ /*!50003 PROCEDURE `Daily_Growth_Mobile`()
BEGIN

SET @FechaFin:=CURDATE()- INTERVAL 1 DAY;
SET @FechaIni:= CAST('2014-01-01' AS DATE);
#SET @FechaIni:= CAST('2014-05-28' AS DATE);


While @FechaIni <=  @FechaFin do

INSERT INTO `marketing`.`daily_growth_mobile`
(`Country`,
`Type`,
`Date`,
`Category`,
`Current_Rev`,
`Previous_Rev`,
`USD_Current_Rev`,
`USD_Previous_Rev`,
`EUR_Current_Rev`,
`EUR_Previous_Rev`)

 -- MoM   WoW    *** COLOMBIA ***

Select p.country as "Country"
	, 'Month' as "Type"
	, date as "Date"
	, cat1 as "Category"
	, IFNULL(SUM(NetsCurrentMonth), 0) as "Current_Rev"
	, IFNULL(SUM(p.NetsPreviousMonth), 0) as "Previous_Rev"
	, IFNULL(SUM(NetsCurrentMonth)/((USDCur + USDPre)/2), 0) as "USD_Current_Rev"
	, IFNULL(SUM(p.NetsPreviousMonth)/((USDCur + USDPre)/2), 0) as "USD_Previous_Rev"
	, IFNULL(SUM(NetsCurrentMonth)/((EURCur + EURPre)/2), 0) as "EUR_Current_Rev"
	, IFNULL(SUM(p.NetsPreviousMonth)/((EURCur + EURPre)/2), 0)  as "EUR_Previous_Rev"
 from 
(select currentMonth.Country
	, IFNULL(currentMonth.cat1, previousMonth.cat1) cat1
	, cast((CAST( @FechaIni AS DATE)) as date) date
	, sum(NetsPreviousMonth) as NetsPreviousMonth
	, sum(NetsCurrentMonth) as NetsCurrentMonth
	, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate_USD Where Country = 'COL' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE)),'%Y%m') AS DECIMAL)) AS USDCur
	, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate_USD Where Country = 'COL' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE))- interval 1 month,'%Y%m') AS DECIMAL)) AS USDPre
	, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate Where Country = 'COL' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE)),'%Y%m') AS DECIMAL)) AS EURCur
	, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate Where Country = 'COL' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE))- interval 1 month,'%Y%m') AS DECIMAL)) AS EURPre 
from (select u.country
		,  min(u.date)
		,  u.cat1
		,  sum(u.rev) as NetsCurrentMonth 
		FROM (select a.country
				,  min(a.date) as date
				,  a.cat1
				,  sum(rev) as rev 
				from development_co_project.A_Master a 
				Inner join marketing.ga_transaction  x
				on a.OrderNum = x.transaction_id
				and a.country = x.country
				where orderaftercan = 1 and x.profile = 'mobileapp' and a.date >= cast((CAST( @FechaIni AS DATE)) as date) - interval 1 month AND a.date <= cast((CAST( @FechaIni AS DATE)) as date) 
				group by cat1 
				Union all 
				Select ps.country
					, ps.dt
					, ps.cat1
					, 0 
				From marketing.tmp_Date_Cat1_Possibility ps 
		Where ps.dt = CAST((CAST( @FechaIni AS DATE)) AS DATE) AND ps.country = 'COL') u 
		group by cat1) currentMonth 
 left join (select a.country
				, cat1
				, sum(rev) as NetsPreviousMonth 
				from development_co_project.A_Master a 
				Inner join marketing.ga_transaction  x
				on a.OrderNum = x.transaction_id
				and a.country = x.country
				where orderaftercan = 1 and x.profile = 'mobileapp' and a.date >= cast((CAST( @FechaIni AS DATE)) as date) - interval 2 month AND a.date < cast((CAST( @FechaIni AS DATE)) as date) - interval 1 Month 
				group by cat1) previousMonth 
on currentMonth.cat1 = previousMonth.cat1 
	group by currentMonth.cat1) p 
group by cat1 

UNION ALL 

Select p.country
	, 'Week'
	, date
	, cat1
	, IFNULL(SUM(NetsCurrentMonth), 0)
	, IFNULL(SUM(p.NetsPreviousMonth),0)
	, case when DAY( CAST( @FechaIni AS DATE)) > 6 then IFNULL(SUM(NetsCurrentMonth)/USDCur, 0) when DAY( CAST( @FechaIni AS DATE)) <= 6 then IFNULL((SUM(NetsCurrentMonth)/((USDCur + USDPre)/2)), 0) end 
	, case when DAY( CAST( @FechaIni AS DATE)) > 13 then IFNULL(SUM(p.NetsPreviousMonth) /USDPre, 0) when DAY( CAST( @FechaIni AS DATE)) <= 13 then IFNULL((SUM(p.NetsPreviousMonth)/((USDCur + USDPre)/2)), 0) end 
	, case when DAY( CAST( @FechaIni AS DATE)) > 6 then IFNULL(SUM(NetsCurrentMonth)/EURCur, 0) when DAY( CAST( @FechaIni AS DATE)) <= 6 then IFNULL((SUM(NetsCurrentMonth)/((EURCur + EURPre)/2)), 0) end
	, case when DAY( CAST( @FechaIni AS DATE)) > 13 then IFNULL(SUM(p.NetsPreviousMonth) /EURPre, 0) when DAY( CAST( @FechaIni AS DATE)) <= 13 then IFNULL((SUM(p.NetsPreviousMonth)/((EURCur + EURPre)/2)), 0) end 
 from (select currentMonth.Country
		, IFNULL(currentMonth.cat1, previousMonth.cat1) cat1
		, cast((CAST( @FechaIni AS DATE)) as date) date
		, sum(NetsPreviousMonth) as NetsPreviousMonth
		, sum(NetsCurrentMonth) as NetsCurrentMonth
		, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate_USD Where Country = 'COL' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE)),'%Y%m') AS DECIMAL)) AS USDCur
		, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate_USD Where Country = 'COL' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE))- interval 1 month,'%Y%m') AS DECIMAL)) AS USDPre
		, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate Where Country = 'COL' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE)),'%Y%m') AS DECIMAL)) AS EURCur
		, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate Where Country = 'COL' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE))- interval 1 month,'%Y%m') AS DECIMAL)) AS EURPre 
 from (select u.country
		,  min(u.date)
		,  u.cat1
		,  sum(u.rev) as NetsCurrentMonth 
		FROM (select a.country
				,  min(a.date) as date
				,  a.cat1
				,  sum(rev) as rev 
				from development_co_project.A_Master a 
				Inner join marketing.ga_transaction  x
				on a.OrderNum = x.transaction_id
				and a.country = x.country
				where orderaftercan = 1 and x.profile = 'mobileapp' and a.date >= cast((CAST( @FechaIni AS DATE)) as date) - interval 6 day AND a.date <= cast((CAST( @FechaIni AS DATE)) as date) 
				group by cat1 
				Union all 
				Select ps.country
				, ps.dt
				, ps.cat1
				, 0 
				From marketing.tmp_Date_Cat1_Possibility ps 
				Where ps.dt = CAST((CAST( @FechaIni AS DATE)) AS DATE) AND ps.country = 'COL') u 
		group by cat1) currentMonth 
 left join (select a.country
				, cat1
				, sum(rev) as NetsPreviousMonth 
				from development_co_project.A_Master a 
				Inner join marketing.ga_transaction  x
				on a.OrderNum = x.transaction_id
				and a.country = x.country
				where orderaftercan = 1 and x.profile = 'mobileapp' and a.date > cast((CAST( @FechaIni AS DATE)) as date) - interval 14 day AND a.date <= cast((CAST( @FechaIni AS DATE)) as date) - interval 7 day  
				group by cat1) previousMonth 
on currentMonth.cat1 = previousMonth.cat1 
	group by currentMonth.cat1) p 
group by cat1


UNION ALL 

 -- MoM   WoW    *** MEXICO ***


Select p.country as "Country"
	, 'Month' as "Type"
	, date as "Date"
	, cat1 as "Category"
	, IFNULL(SUM(NetsCurrentMonth), 0) as "Current_Rev"
	, IFNULL(SUM(p.NetsPreviousMonth), 0) as "Previous_Rev"
	, IFNULL(SUM(NetsCurrentMonth)/((USDCur + USDPre)/2), 0) as "USD_Current_Rev"
	, IFNULL(SUM(p.NetsPreviousMonth)/((USDCur + USDPre)/2), 0) as "USD_Previous_Rev"
	, IFNULL(SUM(NetsCurrentMonth)/((EURCur + EURPre)/2), 0) as "EUR_Current_Rev"
	, IFNULL(SUM(p.NetsPreviousMonth)/((EURCur + EURPre)/2), 0)  as "EUR_Previous_Rev"
 from 
(select currentMonth.Country
	, IFNULL(currentMonth.cat1, previousMonth.cat1) cat1
	, cast((CAST( @FechaIni AS DATE)) as date) date
	, sum(NetsPreviousMonth) as NetsPreviousMonth
	, sum(NetsCurrentMonth) as NetsCurrentMonth
	, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate_USD Where Country = 'MEX' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE)),'%Y%m') AS DECIMAL)) AS USDCur
	, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate_USD Where Country = 'MEX' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE))- interval 1 month,'%Y%m') AS DECIMAL)) AS USDPre
	, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate Where Country = 'MEX' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE)),'%Y%m') AS DECIMAL)) AS EURCur
	, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate Where Country = 'MEX' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE))- interval 1 month,'%Y%m') AS DECIMAL)) AS EURPre 
from (select u.country
		,  min(u.date)
		,  u.cat1
		,  sum(u.rev) as NetsCurrentMonth 
		FROM (select a.country
				,  min(a.date) as date
				,  a.cat1
				,  sum(rev) as rev 
				from development_mx.A_Master a 
				Inner join marketing.ga_transaction  x
				on a.OrderNum = x.transaction_id
				and a.country = x.country
				where orderaftercan = 1 and x.profile = 'mobileapp' and a.date >= cast((CAST( @FechaIni AS DATE)) as date) - interval 1 month AND a.date <= cast((CAST( @FechaIni AS DATE)) as date) 
				group by cat1 
				Union all 
				Select ps.country
					, ps.dt
					, ps.cat1
					, 0 
				From marketing.tmp_Date_Cat1_Possibility ps 
		Where ps.dt = CAST((CAST( @FechaIni AS DATE)) AS DATE) AND ps.country = 'MEX') u 
		group by cat1) currentMonth 
 left join (select a.country
				, cat1
				, sum(rev) as NetsPreviousMonth 
				from development_mx.A_Master a 
				Inner join marketing.ga_transaction  x
				on a.OrderNum = x.transaction_id
				and a.country = x.country
				where orderaftercan = 1 and x.profile = 'mobileapp' and a.date >= cast((CAST( @FechaIni AS DATE)) as date) - interval 2 month AND a.date < cast((CAST( @FechaIni AS DATE)) as date) - interval 1 Month 
				group by cat1) previousMonth 
on currentMonth.cat1 = previousMonth.cat1 
	group by currentMonth.cat1) p 
group by cat1 

UNION ALL 

Select p.country
	, 'Week'
	, date
	, cat1
	, IFNULL(SUM(NetsCurrentMonth), 0)
	, IFNULL(SUM(p.NetsPreviousMonth),0)
	, case when DAY( CAST( @FechaIni AS DATE)) > 6 then IFNULL(SUM(NetsCurrentMonth)/USDCur, 0) when DAY( CAST( @FechaIni AS DATE)) <= 6 then IFNULL((SUM(NetsCurrentMonth)/((USDCur + USDPre)/2)), 0) end 
	, case when DAY( CAST( @FechaIni AS DATE)) > 13 then IFNULL(SUM(p.NetsPreviousMonth) /USDPre, 0) when DAY( CAST( @FechaIni AS DATE)) <= 13 then IFNULL((SUM(p.NetsPreviousMonth)/((USDCur + USDPre)/2)), 0) end 
	, case when DAY( CAST( @FechaIni AS DATE)) > 6 then IFNULL(SUM(NetsCurrentMonth)/EURCur, 0) when DAY( CAST( @FechaIni AS DATE)) <= 6 then IFNULL((SUM(NetsCurrentMonth)/((EURCur + EURPre)/2)), 0) end
	, case when DAY( CAST( @FechaIni AS DATE)) > 13 then IFNULL(SUM(p.NetsPreviousMonth) /EURPre, 0) when DAY( CAST( @FechaIni AS DATE)) <= 13 then IFNULL((SUM(p.NetsPreviousMonth)/((EURCur + EURPre)/2)), 0) end 
 from (select currentMonth.Country
		, IFNULL(currentMonth.cat1, previousMonth.cat1) cat1
		, cast((CAST( @FechaIni AS DATE)) as date) date
		, sum(NetsPreviousMonth) as NetsPreviousMonth
		, sum(NetsCurrentMonth) as NetsCurrentMonth
		, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate_USD Where Country = 'MEX' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE)),'%Y%m') AS DECIMAL)) AS USDCur
		, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate_USD Where Country = 'MEX' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE))- interval 1 month,'%Y%m') AS DECIMAL)) AS USDPre
		, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate Where Country = 'MEX' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE)),'%Y%m') AS DECIMAL)) AS EURCur
		, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate Where Country = 'MEX' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE))- interval 1 month,'%Y%m') AS DECIMAL)) AS EURPre 
 from (select u.country
		,  min(u.date)
		,  u.cat1
		,  sum(u.rev) as NetsCurrentMonth 
		FROM (select a.country
				,  min(a.date) as date
				,  a.cat1
				,  sum(rev) as rev 
				from development_mx.A_Master a 
				Inner join marketing.ga_transaction  x
				on a.OrderNum = x.transaction_id
				and a.country = x.country  
				where orderaftercan = 1 and x.profile = 'mobileapp' and a.date >= cast((CAST( @FechaIni AS DATE)) as date) - interval 6 day AND a.date <= cast((CAST( @FechaIni AS DATE)) as date) 
				group by cat1 
				Union all 
				Select ps.country
				, ps.dt
				, ps.cat1
				, 0 
				From marketing.tmp_Date_Cat1_Possibility ps 
				Where ps.dt = CAST((CAST( @FechaIni AS DATE)) AS DATE) AND ps.country = 'MEX') u 
		group by cat1) currentMonth 
 left join (select a.country
				, cat1
				, sum(rev) as NetsPreviousMonth 
				from development_mx.A_Master a 
				Inner join marketing.ga_transaction  x
				on a.OrderNum = x.transaction_id
				and a.country = x.country
				where orderaftercan = 1 and x.profile = 'mobileapp' and a.date > cast((CAST( @FechaIni AS DATE)) as date) - interval 14 day AND a.date <= cast((CAST( @FechaIni AS DATE)) as date) - interval 7 day  
				group by cat1) previousMonth 
on currentMonth.cat1 = previousMonth.cat1 
	group by currentMonth.cat1) p 
group by cat1




UNION ALL 

 -- MoM   WoW    *** VENEZUELA ***


Select p.country as "Country"
	, 'Month' as "Type"
	, date as "Date"
	, cat1 as "Category"
	, IFNULL(SUM(NetsCurrentMonth), 0) as "Current_Rev"
	, IFNULL(SUM(p.NetsPreviousMonth), 0) as "Previous_Rev"
	, IFNULL(SUM(NetsCurrentMonth)/((USDCur + USDPre)/2), 0) as "USD_Current_Rev"
	, IFNULL(SUM(p.NetsPreviousMonth)/((USDCur + USDPre)/2), 0) as "USD_Previous_Rev"
	, IFNULL(SUM(NetsCurrentMonth)/((EURCur + EURPre)/2), 0) as "EUR_Current_Rev"
	, IFNULL(SUM(p.NetsPreviousMonth)/((EURCur + EURPre)/2), 0)  as "EUR_Previous_Rev"
 from 
(select currentMonth.Country
	, IFNULL(currentMonth.cat1, previousMonth.cat1) cat1
	, cast((CAST( @FechaIni AS DATE)) as date) date
	, sum(NetsPreviousMonth) as NetsPreviousMonth
	, sum(NetsCurrentMonth) as NetsCurrentMonth
	, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate_USD Where Country = 'VEN' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE)),'%Y%m') AS DECIMAL)) AS USDCur
	, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate_USD Where Country = 'VEN' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE))- interval 1 month,'%Y%m') AS DECIMAL)) AS USDPre
	, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate Where Country = 'VEN' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE)),'%Y%m') AS DECIMAL)) AS EURCur
	, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate Where Country = 'VEN' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE))- interval 1 month,'%Y%m') AS DECIMAL)) AS EURPre 
from (select u.country
		,  min(u.date)
		,  u.cat1
		,  sum(u.rev) as NetsCurrentMonth 
		FROM (select a.country
				,  min(a.date) as date
				,  a.cat1
				,  sum(rev) as rev 
				from development_ve.A_Master a 
				Inner join marketing.ga_transaction  x
				on a.OrderNum = x.transaction_id
				and a.country = x.country
				where orderaftercan = 1 and x.profile = 'mobileapp' and a.date >= cast((CAST( @FechaIni AS DATE)) as date) - interval 1 month AND a.date <= cast((CAST( @FechaIni AS DATE)) as date) 
				group by cat1 
				Union all 
				Select ps.country
					, ps.dt
					, ps.cat1
					, 0 
				From marketing.tmp_Date_Cat1_Possibility ps 
		Where ps.dt = CAST((CAST( @FechaIni AS DATE)) AS DATE) AND ps.country = 'VEN') u 
		group by cat1) currentMonth 
 left join (select a.country
				, cat1
				, sum(rev) as NetsPreviousMonth 
				from development_ve.A_Master a 
				Inner join marketing.ga_transaction  x
				on a.OrderNum = x.transaction_id
				and a.country = x.country
				where orderaftercan = 1 and x.profile = 'mobileapp' and a.date >= cast((CAST( @FechaIni AS DATE)) as date) - interval 2 month AND a.date < cast((CAST( @FechaIni AS DATE)) as date) - interval 1 Month 
				group by cat1) previousMonth 
on currentMonth.cat1 = previousMonth.cat1 
	group by currentMonth.cat1) p 
group by cat1 

UNION ALL 

Select p.country
	, 'Week'
	, date
	, cat1
	, IFNULL(SUM(NetsCurrentMonth), 0)
	, IFNULL(SUM(p.NetsPreviousMonth),0)
	, case when DAY( CAST( @FechaIni AS DATE)) > 6 then IFNULL(SUM(NetsCurrentMonth)/USDCur, 0) when DAY( CAST( @FechaIni AS DATE)) <= 6 then IFNULL((SUM(NetsCurrentMonth)/((USDCur + USDPre)/2)), 0) end 
	, case when DAY( CAST( @FechaIni AS DATE)) > 13 then IFNULL(SUM(p.NetsPreviousMonth) /USDPre, 0) when DAY( CAST( @FechaIni AS DATE)) <= 13 then IFNULL((SUM(p.NetsPreviousMonth)/((USDCur + USDPre)/2)), 0) end 
	, case when DAY( CAST( @FechaIni AS DATE)) > 6 then IFNULL(SUM(NetsCurrentMonth)/EURCur, 0) when DAY( CAST( @FechaIni AS DATE)) <= 6 then IFNULL((SUM(NetsCurrentMonth)/((EURCur + EURPre)/2)), 0) end
	, case when DAY( CAST( @FechaIni AS DATE)) > 13 then IFNULL(SUM(p.NetsPreviousMonth) /EURPre, 0) when DAY( CAST( @FechaIni AS DATE)) <= 13 then IFNULL((SUM(p.NetsPreviousMonth)/((EURCur + EURPre)/2)), 0) end 
 from (select currentMonth.Country
		, IFNULL(currentMonth.cat1, previousMonth.cat1) cat1
		, cast((CAST( @FechaIni AS DATE)) as date) date
		, sum(NetsPreviousMonth) as NetsPreviousMonth
		, sum(NetsCurrentMonth) as NetsCurrentMonth
		, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate_USD Where Country = 'VEN' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE)),'%Y%m') AS DECIMAL)) AS USDCur
		, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate_USD Where Country = 'VEN' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE))- interval 1 month,'%Y%m') AS DECIMAL)) AS USDPre
		, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate Where Country = 'VEN' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE)),'%Y%m') AS DECIMAL)) AS EURCur
		, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate Where Country = 'VEN' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE))- interval 1 month,'%Y%m') AS DECIMAL)) AS EURPre 
 from (select u.country
		,  min(u.date)
		,  u.cat1
		,  sum(u.rev) as NetsCurrentMonth 
		FROM (select a.country
				,  min(a.date) as date
				,  a.cat1
				,  sum(rev) as rev 
				from development_ve.A_Master a 
				Inner join marketing.ga_transaction  x
				on a.OrderNum = x.transaction_id
				and a.country = x.country
				where orderaftercan = 1 and x.profile = 'mobileapp' and a.date >= cast((CAST( @FechaIni AS DATE)) as date) - interval 6 day AND a.date <= cast((CAST( @FechaIni AS DATE)) as date) 
				group by cat1 
				Union all 
				Select ps.country
				, ps.dt
				, ps.cat1
				, 0 
				From marketing.tmp_Date_Cat1_Possibility ps 
				Where ps.dt = CAST((CAST( @FechaIni AS DATE)) AS DATE) AND ps.country = 'VEN') u 
		group by cat1) currentMonth 
 left join (select a.country
				, cat1
				, sum(rev) as NetsPreviousMonth 
				from development_ve.A_Master a 
				Inner join marketing.ga_transaction  x
				on a.OrderNum = x.transaction_id
				and a.country = x.country
				where orderaftercan = 1 and x.profile = 'mobileapp' and a.date > cast((CAST( @FechaIni AS DATE)) as date) - interval 14 day AND a.date <= cast((CAST( @FechaIni AS DATE)) as date) - interval 7 day  
				group by cat1) previousMonth 
on currentMonth.cat1 = previousMonth.cat1 
	group by currentMonth.cat1) p 
group by cat1





UNION ALL 

 -- MoM   WoW    *** PERU ***




Select p.country as "Country"
	, 'Month' as "Type"
	, date as "Date"
	, cat1 as "Category"
	, IFNULL(SUM(NetsCurrentMonth), 0) as "Current_Rev"
	, IFNULL(SUM(p.NetsPreviousMonth), 0) as "Previous_Rev"
	, IFNULL(SUM(NetsCurrentMonth)/((USDCur + USDPre)/2), 0) as "USD_Current_Rev"
	, IFNULL(SUM(p.NetsPreviousMonth)/((USDCur + USDPre)/2), 0) as "USD_Previous_Rev"
	, IFNULL(SUM(NetsCurrentMonth)/((EURCur + EURPre)/2), 0) as "EUR_Current_Rev"
	, IFNULL(SUM(p.NetsPreviousMonth)/((EURCur + EURPre)/2), 0)  as "EUR_Previous_Rev"
 from 
(select currentMonth.Country
	, IFNULL(currentMonth.cat1, previousMonth.cat1) cat1
	, cast((CAST( @FechaIni AS DATE)) as date) date
	, sum(NetsPreviousMonth) as NetsPreviousMonth
	, sum(NetsCurrentMonth) as NetsCurrentMonth
	, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate_USD Where Country = 'PER' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE)),'%Y%m') AS DECIMAL)) AS USDCur
	, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate_USD Where Country = 'PER' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE))- interval 1 month,'%Y%m') AS DECIMAL)) AS USDPre
	, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate Where Country = 'PER' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE)),'%Y%m') AS DECIMAL)) AS EURCur
	, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate Where Country = 'PER' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE))- interval 1 month,'%Y%m') AS DECIMAL)) AS EURPre 
from (select u.country
		,  min(u.date)
		,  u.cat1
		,  sum(u.rev) as NetsCurrentMonth 
		FROM (select a.country
				,  min(a.date) as date
				,  a.cat1
				,  sum(rev) as rev 
				from development_pe.A_Master a 
				Inner join marketing.ga_transaction  x
				on a.OrderNum = x.transaction_id
				and a.country = x.country
				where orderaftercan = 1 and x.profile = 'mobileapp' and a.date >= cast((CAST( @FechaIni AS DATE)) as date) - interval 1 month AND a.date <= cast((CAST( @FechaIni AS DATE)) as date) 
				group by cat1 
				Union all 
				Select ps.country
					, ps.dt
					, ps.cat1
					, 0 
				From marketing.tmp_Date_Cat1_Possibility ps 
		Where ps.dt = CAST((CAST( @FechaIni AS DATE)) AS DATE) AND ps.country = 'PER') u 
		group by cat1) currentMonth 
 left join (select a.country
				, cat1
				, sum(rev) as NetsPreviousMonth 
				from development_pe.A_Master a 
				Inner join marketing.ga_transaction  x
				on a.OrderNum = x.transaction_id
				and a.country = x.country
				where orderaftercan = 1 and x.profile = 'mobileapp' and a.date >= cast((CAST( @FechaIni AS DATE)) as date) - interval 2 month AND a.date < cast((CAST( @FechaIni AS DATE)) as date) - interval 1 Month 
				group by cat1) previousMonth 
on currentMonth.cat1 = previousMonth.cat1 
	group by currentMonth.cat1) p 
group by cat1 

UNION ALL 

Select p.country
	, 'Week'
	, date
	, cat1
	, IFNULL(SUM(NetsCurrentMonth), 0)
	, IFNULL(SUM(p.NetsPreviousMonth),0)
	, case when DAY( CAST( @FechaIni AS DATE)) > 6 then IFNULL(SUM(NetsCurrentMonth)/USDCur, 0) when DAY( CAST( @FechaIni AS DATE)) <= 6 then IFNULL((SUM(NetsCurrentMonth)/((USDCur + USDPre)/2)), 0) end 
	, case when DAY( CAST( @FechaIni AS DATE)) > 13 then IFNULL(SUM(p.NetsPreviousMonth) /USDPre, 0) when DAY( CAST( @FechaIni AS DATE)) <= 13 then IFNULL((SUM(p.NetsPreviousMonth)/((USDCur + USDPre)/2)), 0) end 
	, case when DAY( CAST( @FechaIni AS DATE)) > 6 then IFNULL(SUM(NetsCurrentMonth)/EURCur, 0) when DAY( CAST( @FechaIni AS DATE)) <= 6 then IFNULL((SUM(NetsCurrentMonth)/((EURCur + EURPre)/2)), 0) end
	, case when DAY( CAST( @FechaIni AS DATE)) > 13 then IFNULL(SUM(p.NetsPreviousMonth) /EURPre, 0) when DAY( CAST( @FechaIni AS DATE)) <= 13 then IFNULL((SUM(p.NetsPreviousMonth)/((EURCur + EURPre)/2)), 0) end 
 from (select currentMonth.Country
		, IFNULL(currentMonth.cat1, previousMonth.cat1) cat1
		, cast((CAST( @FechaIni AS DATE)) as date) date
		, sum(NetsPreviousMonth) as NetsPreviousMonth
		, sum(NetsCurrentMonth) as NetsCurrentMonth
		, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate_USD Where Country = 'PER' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE)),'%Y%m') AS DECIMAL)) AS USDCur
		, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate_USD Where Country = 'PER' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE))- interval 1 month,'%Y%m') AS DECIMAL)) AS USDPre
		, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate Where Country = 'PER' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE)),'%Y%m') AS DECIMAL)) AS EURCur
		, (SELECT XR FROM development_mx.A_E_BI_ExchangeRate Where Country = 'PER' AND Month_Num = CAST(date_format((CAST( @FechaIni AS DATE))- interval 1 month,'%Y%m') AS DECIMAL)) AS EURPre 
 from (select u.country
		,  min(u.date)
		,  u.cat1
		,  sum(u.rev) as NetsCurrentMonth 
		FROM (select a.country
				,  min(a.date) as date
				,  a.cat1
				,  sum(rev) as rev 
				from development_pe.A_Master a 
				Inner join marketing.ga_transaction  x
				on a.OrderNum = x.transaction_id
				and a.country = x.country
				where orderaftercan = 1 and x.profile = 'mobileapp' and a.date >= cast((CAST( @FechaIni AS DATE)) as date) - interval 6 day AND a.date <= cast((CAST( @FechaIni AS DATE)) as date) 
				group by cat1 
				Union all 
				Select ps.country
				, ps.dt
				, ps.cat1
				, 0 
				From marketing.tmp_Date_Cat1_Possibility ps 
				Where ps.dt = CAST((CAST( @FechaIni AS DATE)) AS DATE) AND ps.country = 'PER') u 
		group by cat1) currentMonth 
 left join (select a.country
				, cat1
				, sum(rev) as NetsPreviousMonth 
				from development_pe.A_Master a 
				Inner join marketing.ga_transaction  x
				on a.OrderNum = x.transaction_id
				and a.country = x.country
				where orderaftercan = 1 and x.profile = 'mobileapp' and a.date > cast((CAST( @FechaIni AS DATE)) as date) - interval 14 day AND a.date <= cast((CAST( @FechaIni AS DATE)) as date) - interval 7 day  
				group by cat1) previousMonth 
on currentMonth.cat1 = previousMonth.cat1 
	group by currentMonth.cat1) p 
group by cat1;


		SET @FechaIni:= CAST(@FechaIni AS DATE) + INTERVAL 1 DAY;
END While;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-01 23:04:02
